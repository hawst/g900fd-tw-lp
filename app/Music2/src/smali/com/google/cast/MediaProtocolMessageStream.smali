.class public Lcom/google/cast/MediaProtocolMessageStream;
.super Lcom/google/cast/MessageStream;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/cast/MediaProtocolMessageStream$PlayerState;
    }
.end annotation


# static fields
.field private static final a:Lcom/google/cast/Logger;


# instance fields
.field private b:Lcom/google/cast/MediaProtocolMessageStream$PlayerState;

.field private c:Ljava/lang/String;

.field private d:Lorg/json/JSONObject;

.field private e:D

.field private f:D

.field private g:D

.field private h:Z

.field private i:J

.field private j:Z

.field private k:Ljava/lang/String;

.field private l:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/cast/MediaTrack;",
            ">;"
        }
    .end annotation
.end field

.field private m:Landroid/net/Uri;

.field private n:J

.field private o:J

.field private p:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/cast/MediaProtocolCommand;",
            ">;"
        }
    .end annotation
.end field

.field private q:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 94
    new-instance v0, Lcom/google/cast/Logger;

    const-string v1, "MediaProtocolMessageStream"

    invoke-direct {v0, v1}, Lcom/google/cast/Logger;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/cast/MediaProtocolMessageStream;->a:Lcom/google/cast/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    .line 120
    const-string v0, "ramp"

    invoke-direct {p0, v0}, Lcom/google/cast/MessageStream;-><init>(Ljava/lang/String;)V

    .line 122
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/cast/MediaProtocolMessageStream;->l:Ljava/util/Map;

    .line 123
    iput-wide v2, p0, Lcom/google/cast/MediaProtocolMessageStream;->o:J

    .line 124
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/cast/MediaProtocolMessageStream;->p:Ljava/util/Map;

    .line 125
    iput-wide v2, p0, Lcom/google/cast/MediaProtocolMessageStream;->q:J

    .line 126
    return-void
.end method

.method private a(D)D
    .locals 5

    .prologue
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    const-wide/16 v0, 0x0

    .line 813
    cmpg-double v4, p1, v0

    if-gez v4, :cond_1

    move-wide p1, v0

    .line 819
    :cond_0
    :goto_0
    return-wide p1

    .line 816
    :cond_1
    cmpl-double v0, p1, v2

    if-lez v0, :cond_0

    move-wide p1, v2

    .line 817
    goto :goto_0
.end method

.method private a(Lorg/json/JSONObject;)V
    .locals 14
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    const-wide/16 v6, 0x0

    const/4 v8, 0x1

    const/4 v1, 0x0

    const/4 v9, 0x0

    .line 682
    const-string v0, "event_sequence"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 683
    const-string v0, "event_sequence"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 684
    iget-wide v4, p0, Lcom/google/cast/MediaProtocolMessageStream;->o:J

    cmp-long v0, v2, v4

    if-gez v0, :cond_0

    .line 785
    :goto_0
    return-void

    .line 687
    :cond_0
    iput-wide v2, p0, Lcom/google/cast/MediaProtocolMessageStream;->o:J

    .line 690
    :cond_1
    const-string v0, "state"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 691
    sget-object v0, Lcom/google/cast/MediaProtocolMessageStream$PlayerState;->IDLE:Lcom/google/cast/MediaProtocolMessageStream$PlayerState;

    .line 692
    const-string v0, "state"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 693
    packed-switch v0, :pswitch_data_0

    .line 704
    new-instance v1, Lorg/json/JSONException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "invalid state value: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lorg/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 695
    :pswitch_0
    sget-object v0, Lcom/google/cast/MediaProtocolMessageStream$PlayerState;->IDLE:Lcom/google/cast/MediaProtocolMessageStream$PlayerState;

    .line 707
    :goto_1
    iput-object v0, p0, Lcom/google/cast/MediaProtocolMessageStream;->b:Lcom/google/cast/MediaProtocolMessageStream$PlayerState;

    .line 710
    :cond_2
    const-string v0, "content_id"

    invoke-virtual {p1, v0, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 711
    iget-object v0, p0, Lcom/google/cast/MediaProtocolMessageStream;->c:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/cast/MediaProtocolMessageStream;->c:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    :cond_3
    iget-object v0, p0, Lcom/google/cast/MediaProtocolMessageStream;->c:Ljava/lang/String;

    if-nez v0, :cond_4

    if-eqz v2, :cond_6

    :cond_4
    move v0, v8

    .line 714
    :goto_2
    iput-object v2, p0, Lcom/google/cast/MediaProtocolMessageStream;->c:Ljava/lang/String;

    .line 715
    const-string v2, "content_info"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    iput-object v2, p0, Lcom/google/cast/MediaProtocolMessageStream;->d:Lorg/json/JSONObject;

    .line 716
    const-string v2, "current_time"

    invoke-virtual {p1, v2, v6, v7}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/cast/MediaProtocolMessageStream;->e:D

    .line 717
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/cast/MediaProtocolMessageStream;->i:J

    .line 718
    const-string v2, "duration"

    invoke-virtual {p1, v2, v6, v7}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/cast/MediaProtocolMessageStream;->f:D

    .line 719
    const-string v2, "time_progress"

    invoke-virtual {p1, v2, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/cast/MediaProtocolMessageStream;->j:Z

    .line 720
    const-string v2, "title"

    invoke-virtual {p1, v2, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/cast/MediaProtocolMessageStream;->k:Ljava/lang/String;

    .line 722
    iget-wide v2, p0, Lcom/google/cast/MediaProtocolMessageStream;->q:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-nez v2, :cond_5

    .line 723
    const-string v2, "volume"

    invoke-virtual {p1, v2, v6, v7}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v2

    invoke-direct {p0, v2, v3}, Lcom/google/cast/MediaProtocolMessageStream;->a(D)D

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/cast/MediaProtocolMessageStream;->g:D

    .line 725
    :cond_5
    const-string v2, "muted"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/cast/MediaProtocolMessageStream;->h:Z

    .line 728
    if-eqz v0, :cond_12

    .line 730
    iget-object v2, p0, Lcom/google/cast/MediaProtocolMessageStream;->l:Ljava/util/Map;

    monitor-enter v2

    .line 731
    :try_start_0
    iget-object v0, p0, Lcom/google/cast/MediaProtocolMessageStream;->l:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 733
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v0, v8

    .line 736
    :goto_3
    const-string v2, "image_url"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    const-string v2, "image_url"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    :goto_4
    iput-object v2, p0, Lcom/google/cast/MediaProtocolMessageStream;->m:Landroid/net/Uri;

    .line 738
    const-string v2, "tracks"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_11

    .line 739
    iget-object v11, p0, Lcom/google/cast/MediaProtocolMessageStream;->l:Ljava/util/Map;

    monitor-enter v11

    .line 740
    :try_start_1
    const-string v2, "tracks"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v12

    move v10, v1

    move v1, v0

    .line 741
    :goto_5
    invoke-virtual {v12}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-ge v10, v0, :cond_d

    .line 742
    invoke-virtual {v12, v10}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 744
    const-string v0, "id"

    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 745
    const-string v0, "selected"

    const/4 v5, 0x0

    invoke-virtual {v4, v0, v5}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v7

    .line 747
    iget-object v0, p0, Lcom/google/cast/MediaProtocolMessageStream;->l:Ljava/util/Map;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/cast/MediaTrack;

    .line 748
    if-eqz v0, :cond_8

    .line 749
    invoke-virtual {v0}, Lcom/google/cast/MediaTrack;->isEnabled()Z

    move-result v2

    if-eq v2, v7, :cond_10

    .line 751
    invoke-virtual {v0, v7}, Lcom/google/cast/MediaTrack;->a(Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move v0, v8

    .line 741
    :goto_6
    add-int/lit8 v1, v10, 0x1

    move v10, v1

    move v1, v0

    goto :goto_5

    .line 698
    :pswitch_1
    sget-object v0, Lcom/google/cast/MediaProtocolMessageStream$PlayerState;->STOPPED:Lcom/google/cast/MediaProtocolMessageStream$PlayerState;

    goto/16 :goto_1

    .line 701
    :pswitch_2
    sget-object v0, Lcom/google/cast/MediaProtocolMessageStream$PlayerState;->PLAYING:Lcom/google/cast/MediaProtocolMessageStream$PlayerState;

    goto/16 :goto_1

    :cond_6
    move v0, v1

    .line 711
    goto/16 :goto_2

    .line 733
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_7
    move-object v2, v9

    .line 736
    goto :goto_4

    .line 756
    :cond_8
    :try_start_3
    const-string v0, "type"

    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 757
    const-string v1, "name"

    const/4 v5, 0x0

    invoke-virtual {v4, v1, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 758
    const-string v1, "lang"

    const/4 v6, 0x0

    invoke-virtual {v4, v1, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 761
    const-string v1, "subtitles"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 762
    sget-object v4, Lcom/google/cast/MediaTrack$Type;->SUBTITLES:Lcom/google/cast/MediaTrack$Type;

    .line 771
    :goto_7
    if-nez v4, :cond_c

    .line 772
    new-instance v1, Lorg/json/JSONException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "invalid track type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lorg/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 778
    :catchall_1
    move-exception v0

    monitor-exit v11
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    .line 763
    :cond_9
    :try_start_4
    const-string v1, "captions"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 764
    sget-object v4, Lcom/google/cast/MediaTrack$Type;->CAPTIONS:Lcom/google/cast/MediaTrack$Type;

    goto :goto_7

    .line 765
    :cond_a
    const-string v1, "audio"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 766
    sget-object v4, Lcom/google/cast/MediaTrack$Type;->AUDIO:Lcom/google/cast/MediaTrack$Type;

    goto :goto_7

    .line 767
    :cond_b
    const-string v1, "video"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 768
    sget-object v4, Lcom/google/cast/MediaTrack$Type;->VIDEO:Lcom/google/cast/MediaTrack$Type;

    goto :goto_7

    .line 774
    :cond_c
    iget-object v0, p0, Lcom/google/cast/MediaProtocolMessageStream;->l:Ljava/util/Map;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    new-instance v1, Lcom/google/cast/MediaTrack;

    invoke-direct/range {v1 .. v7}, Lcom/google/cast/MediaTrack;-><init>(JLcom/google/cast/MediaTrack$Type;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-interface {v0, v13, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v0, v8

    .line 775
    goto/16 :goto_6

    .line 778
    :cond_d
    monitor-exit v11
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 780
    :goto_8
    if-eqz v1, :cond_e

    .line 781
    invoke-virtual {p0}, Lcom/google/cast/MediaProtocolMessageStream;->onTrackListUpdated()V

    .line 784
    :cond_e
    invoke-virtual {p0}, Lcom/google/cast/MediaProtocolMessageStream;->onStatusUpdated()V

    goto/16 :goto_0

    :cond_f
    move-object v4, v9

    goto :goto_7

    :cond_10
    move v0, v1

    goto/16 :goto_6

    :cond_11
    move v1, v0

    goto :goto_8

    :cond_12
    move v0, v1

    goto/16 :goto_3

    .line 693
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public final getContentId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 458
    iget-object v0, p0, Lcom/google/cast/MediaProtocolMessageStream;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final getContentInfo()Lorg/json/JSONObject;
    .locals 1

    .prologue
    .line 467
    iget-object v0, p0, Lcom/google/cast/MediaProtocolMessageStream;->d:Lorg/json/JSONObject;

    return-object v0
.end method

.method public final getImageUrl()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 551
    iget-object v0, p0, Lcom/google/cast/MediaProtocolMessageStream;->m:Landroid/net/Uri;

    return-object v0
.end method

.method public final getPlayerState()Lcom/google/cast/MediaProtocolMessageStream$PlayerState;
    .locals 1

    .prologue
    .line 449
    iget-object v0, p0, Lcom/google/cast/MediaProtocolMessageStream;->b:Lcom/google/cast/MediaProtocolMessageStream$PlayerState;

    return-object v0
.end method

.method public final getStreamDuration()D
    .locals 2

    .prologue
    .line 528
    iget-wide v0, p0, Lcom/google/cast/MediaProtocolMessageStream;->f:D

    return-wide v0
.end method

.method public final getStreamPosition()D
    .locals 6

    .prologue
    const-wide/16 v0, 0x0

    .line 486
    iget-wide v2, p0, Lcom/google/cast/MediaProtocolMessageStream;->i:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_1

    .line 507
    :cond_0
    :goto_0
    return-wide v0

    .line 490
    :cond_1
    iget-boolean v2, p0, Lcom/google/cast/MediaProtocolMessageStream;->j:Z

    if-nez v2, :cond_2

    .line 491
    iget-wide v0, p0, Lcom/google/cast/MediaProtocolMessageStream;->e:D

    goto :goto_0

    .line 496
    :cond_2
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/cast/MediaProtocolMessageStream;->i:J

    sub-long/2addr v2, v4

    long-to-double v2, v2

    const-wide v4, 0x408f400000000000L    # 1000.0

    div-double/2addr v2, v4

    .line 497
    cmpg-double v4, v2, v0

    if-gez v4, :cond_3

    .line 502
    :goto_1
    iget-wide v2, p0, Lcom/google/cast/MediaProtocolMessageStream;->e:D

    add-double/2addr v0, v2

    .line 503
    iget-wide v2, p0, Lcom/google/cast/MediaProtocolMessageStream;->f:D

    cmpl-double v2, v0, v2

    if-lez v2, :cond_0

    .line 504
    iget-wide v0, p0, Lcom/google/cast/MediaProtocolMessageStream;->f:D

    goto :goto_0

    :cond_3
    move-wide v0, v2

    goto :goto_1
.end method

.method public final getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 474
    iget-object v0, p0, Lcom/google/cast/MediaProtocolMessageStream;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final getVolume()D
    .locals 2

    .prologue
    .line 535
    iget-wide v0, p0, Lcom/google/cast/MediaProtocolMessageStream;->g:D

    return-wide v0
.end method

.method public final isStreamProgressing()Z
    .locals 1

    .prologue
    .line 518
    iget-boolean v0, p0, Lcom/google/cast/MediaProtocolMessageStream;->j:Z

    return v0
.end method

.method public final loadMedia(Ljava/lang/String;Lcom/google/cast/ContentMetadata;Z)Lcom/google/cast/MediaProtocolCommand;
    .locals 6
    .param p1, "contentId"    # Ljava/lang/String;
    .param p2, "contentMetadata"    # Lcom/google/cast/ContentMetadata;
    .param p3, "autoplay"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 183
    new-instance v0, Lcom/google/cast/MediaProtocolCommand;

    iget-wide v2, p0, Lcom/google/cast/MediaProtocolMessageStream;->n:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/cast/MediaProtocolMessageStream;->n:J

    const-string v1, "LOAD"

    invoke-direct {v0, v2, v3, v1}, Lcom/google/cast/MediaProtocolCommand;-><init>(JLjava/lang/String;)V

    .line 186
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 188
    :try_start_0
    const-string v2, "cmd_id"

    invoke-virtual {v0}, Lcom/google/cast/MediaProtocolCommand;->a()J

    move-result-wide v4

    invoke-virtual {v1, v2, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 189
    const-string v2, "type"

    invoke-virtual {v0}, Lcom/google/cast/MediaProtocolCommand;->getType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 190
    const-string v2, "src"

    invoke-virtual {v1, v2, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 191
    if-eqz p3, :cond_0

    .line 192
    const-string v2, "autoplay"

    invoke-virtual {v1, v2, p3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 194
    :cond_0
    if-eqz p2, :cond_1

    .line 195
    invoke-virtual {p2}, Lcom/google/cast/ContentMetadata;->getContentInfo()Lorg/json/JSONObject;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 196
    invoke-virtual {p2}, Lcom/google/cast/ContentMetadata;->getContentInfo()Lorg/json/JSONObject;

    move-result-object v2

    iput-object v2, p0, Lcom/google/cast/MediaProtocolMessageStream;->d:Lorg/json/JSONObject;

    .line 197
    const-string v2, "content_info"

    iget-object v3, p0, Lcom/google/cast/MediaProtocolMessageStream;->d:Lorg/json/JSONObject;

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 201
    :goto_0
    invoke-virtual {p2}, Lcom/google/cast/ContentMetadata;->getTitle()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 202
    invoke-virtual {p2}, Lcom/google/cast/ContentMetadata;->getTitle()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/cast/MediaProtocolMessageStream;->k:Ljava/lang/String;

    .line 203
    const-string v2, "title"

    iget-object v3, p0, Lcom/google/cast/MediaProtocolMessageStream;->k:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 207
    :goto_1
    invoke-virtual {p2}, Lcom/google/cast/ContentMetadata;->getImageUrl()Landroid/net/Uri;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 208
    invoke-virtual {p2}, Lcom/google/cast/ContentMetadata;->getImageUrl()Landroid/net/Uri;

    move-result-object v2

    iput-object v2, p0, Lcom/google/cast/MediaProtocolMessageStream;->m:Landroid/net/Uri;

    .line 209
    const-string v2, "image_url"

    iget-object v3, p0, Lcom/google/cast/MediaProtocolMessageStream;->m:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 216
    :cond_1
    :goto_2
    invoke-virtual {p0, v1}, Lcom/google/cast/MediaProtocolMessageStream;->sendMessage(Lorg/json/JSONObject;)V

    .line 217
    iget-object v1, p0, Lcom/google/cast/MediaProtocolMessageStream;->p:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/google/cast/MediaProtocolCommand;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 218
    return-object v0

    .line 199
    :cond_2
    const/4 v2, 0x0

    :try_start_1
    iput-object v2, p0, Lcom/google/cast/MediaProtocolMessageStream;->d:Lorg/json/JSONObject;

    goto :goto_0

    .line 214
    :catch_0
    move-exception v2

    goto :goto_2

    .line 205
    :cond_3
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/cast/MediaProtocolMessageStream;->k:Ljava/lang/String;

    goto :goto_1

    .line 211
    :cond_4
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/cast/MediaProtocolMessageStream;->m:Landroid/net/Uri;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method

.method public onDetached()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    .line 792
    iget-object v0, p0, Lcom/google/cast/MediaProtocolMessageStream;->p:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/cast/MediaProtocolCommand;

    .line 793
    invoke-virtual {v0}, Lcom/google/cast/MediaProtocolCommand;->c()V

    goto :goto_0

    .line 795
    :cond_0
    sget-object v0, Lcom/google/cast/MediaProtocolMessageStream$PlayerState;->IDLE:Lcom/google/cast/MediaProtocolMessageStream$PlayerState;

    iput-object v0, p0, Lcom/google/cast/MediaProtocolMessageStream;->b:Lcom/google/cast/MediaProtocolMessageStream$PlayerState;

    .line 796
    iput-object v2, p0, Lcom/google/cast/MediaProtocolMessageStream;->c:Ljava/lang/String;

    .line 797
    iput-object v2, p0, Lcom/google/cast/MediaProtocolMessageStream;->d:Lorg/json/JSONObject;

    .line 798
    iput-wide v4, p0, Lcom/google/cast/MediaProtocolMessageStream;->e:D

    .line 799
    iput-wide v4, p0, Lcom/google/cast/MediaProtocolMessageStream;->f:D

    .line 800
    iput-wide v4, p0, Lcom/google/cast/MediaProtocolMessageStream;->g:D

    .line 801
    iput-boolean v3, p0, Lcom/google/cast/MediaProtocolMessageStream;->h:Z

    .line 802
    iput-wide v6, p0, Lcom/google/cast/MediaProtocolMessageStream;->i:J

    .line 803
    iput-boolean v3, p0, Lcom/google/cast/MediaProtocolMessageStream;->j:Z

    .line 804
    iput-object v2, p0, Lcom/google/cast/MediaProtocolMessageStream;->k:Ljava/lang/String;

    .line 805
    iget-object v0, p0, Lcom/google/cast/MediaProtocolMessageStream;->l:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 806
    iput-object v2, p0, Lcom/google/cast/MediaProtocolMessageStream;->m:Landroid/net/Uri;

    .line 807
    iput-wide v6, p0, Lcom/google/cast/MediaProtocolMessageStream;->o:J

    .line 808
    iget-object v0, p0, Lcom/google/cast/MediaProtocolMessageStream;->p:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 809
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/cast/MediaProtocolMessageStream;->q:J

    .line 810
    return-void
.end method

.method protected onError(Ljava/lang/String;JLorg/json/JSONObject;)V
    .locals 0
    .param p1, "errorDomain"    # Ljava/lang/String;
    .param p2, "errorCode"    # J
    .param p4, "errorInfo"    # Lorg/json/JSONObject;

    .prologue
    .line 662
    return-void
.end method

.method protected onKeyRequested(JLjava/lang/String;[Ljava/lang/String;)V
    .locals 0
    .param p1, "requestId"    # J
    .param p3, "method"    # Ljava/lang/String;
    .param p4, "requests"    # [Ljava/lang/String;

    .prologue
    .line 675
    return-void
.end method

.method public final onMessageReceived(Lorg/json/JSONObject;)V
    .locals 10
    .param p1, "message"    # Lorg/json/JSONObject;

    .prologue
    const/4 v9, 0x1

    const/4 v1, 0x0

    .line 573
    sget-object v0, Lcom/google/cast/MediaProtocolMessageStream;->a:Lcom/google/cast/Logger;

    const-string v2, "Received message: \"%s\""

    new-array v3, v9, [Ljava/lang/Object;

    aput-object p1, v3, v1

    invoke-virtual {v0, v2, v3}, Lcom/google/cast/Logger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 575
    :try_start_0
    const-string v0, "type"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 577
    const-string v2, "RESPONSE"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 579
    const-string v0, "cmd_id"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 580
    iget-object v0, p0, Lcom/google/cast/MediaProtocolMessageStream;->p:Ljava/util/Map;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/cast/MediaProtocolCommand;

    .line 581
    iget-wide v4, p0, Lcom/google/cast/MediaProtocolMessageStream;->q:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    .line 582
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/google/cast/MediaProtocolMessageStream;->q:J

    .line 584
    :cond_0
    if-nez v0, :cond_2

    .line 585
    sget-object v0, Lcom/google/cast/MediaProtocolMessageStream;->a:Lcom/google/cast/Logger;

    const-string v2, "Got a response to an unknown request: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {v0, v2, v3}, Lcom/google/cast/Logger;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 636
    :cond_1
    :goto_0
    return-void

    .line 589
    :cond_2
    const-string v2, "status"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 590
    const-string v2, "status"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    .line 591
    invoke-direct {p0, v2}, Lcom/google/cast/MediaProtocolMessageStream;->a(Lorg/json/JSONObject;)V

    .line 593
    const-string v3, "error"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 594
    sget-object v3, Lcom/google/cast/MediaProtocolMessageStream;->a:Lcom/google/cast/Logger;

    const-string v4, "message has an error!"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v3, v4, v5}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 595
    const-string v3, "error"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    .line 596
    const-string v3, "domain"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 597
    const-string v4, "code"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 598
    const-string v6, "error_info"

    invoke-virtual {v2, v6}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    .line 599
    invoke-virtual {v0, v3, v4, v5, v2}, Lcom/google/cast/MediaProtocolCommand;->a(Ljava/lang/String;JLorg/json/JSONObject;)V

    .line 605
    :cond_3
    :goto_1
    invoke-virtual {v0}, Lcom/google/cast/MediaProtocolCommand;->b()V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 633
    :catch_0
    move-exception v0

    .line 634
    sget-object v2, Lcom/google/cast/MediaProtocolMessageStream;->a:Lcom/google/cast/Logger;

    const-string v3, "error parsing message: %s %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p1, v4, v1

    aput-object v0, v4, v9

    invoke-virtual {v2, v3, v4}, Lcom/google/cast/Logger;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 600
    :cond_4
    :try_start_1
    invoke-virtual {v0}, Lcom/google/cast/MediaProtocolCommand;->getType()Ljava/lang/String;

    move-result-object v2

    const-string v3, "LOAD"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 601
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/google/cast/MediaProtocolMessageStream;->e:D

    .line 602
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/google/cast/MediaProtocolMessageStream;->i:J

    goto :goto_1

    .line 606
    :cond_5
    const-string v2, "STATUS"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 608
    const-string v0, "status"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 609
    invoke-direct {p0, v0}, Lcom/google/cast/MediaProtocolMessageStream;->a(Lorg/json/JSONObject;)V

    .line 610
    const-string v2, "error"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 611
    const-string v2, "error"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 612
    const-string v2, "domain"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 613
    const-string v3, "code"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 614
    const-string v3, "error_info"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 615
    invoke-virtual {p0, v2, v4, v5, v0}, Lcom/google/cast/MediaProtocolMessageStream;->onError(Ljava/lang/String;JLorg/json/JSONObject;)V

    goto/16 :goto_0

    .line 617
    :cond_6
    const-string v2, "KEY_REQUEST"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 619
    const-string v0, "cmd_id"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 620
    const-string v0, "method"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 621
    const-string v0, "requests"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v5

    .line 624
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v6

    .line 625
    new-array v7, v6, [Ljava/lang/String;

    move v0, v1

    .line 626
    :goto_2
    if-ge v0, v6, :cond_7

    .line 627
    invoke-virtual {v5, v0}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v0

    .line 626
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 629
    :cond_7
    invoke-virtual {p0, v2, v3, v4, v7}, Lcom/google/cast/MediaProtocolMessageStream;->onKeyRequested(JLjava/lang/String;[Ljava/lang/String;)V

    goto/16 :goto_0

    .line 631
    :cond_8
    sget-object v2, Lcom/google/cast/MediaProtocolMessageStream;->a:Lcom/google/cast/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Ignoring message. Got a request with unknown request type="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v2, v0, v3}, Lcom/google/cast/Logger;->w(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method

.method protected onStatusUpdated()V
    .locals 0

    .prologue
    .line 643
    return-void
.end method

.method protected onTrackListUpdated()V
    .locals 0

    .prologue
    .line 650
    return-void
.end method

.method public final playFrom(D)Lcom/google/cast/MediaProtocolCommand;
    .locals 7
    .param p1, "position"    # D
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 286
    new-instance v0, Lcom/google/cast/MediaProtocolCommand;

    iget-wide v2, p0, Lcom/google/cast/MediaProtocolMessageStream;->n:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/cast/MediaProtocolMessageStream;->n:J

    const-string v1, "PLAY"

    invoke-direct {v0, v2, v3, v1}, Lcom/google/cast/MediaProtocolCommand;-><init>(JLjava/lang/String;)V

    .line 289
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 291
    :try_start_0
    const-string v2, "cmd_id"

    invoke-virtual {v0}, Lcom/google/cast/MediaProtocolCommand;->a()J

    move-result-wide v4

    invoke-virtual {v1, v2, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 292
    const-string v2, "type"

    invoke-virtual {v0}, Lcom/google/cast/MediaProtocolCommand;->getType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 293
    const-wide/16 v2, 0x0

    cmpl-double v2, p1, v2

    if-ltz v2, :cond_0

    .line 294
    const-string v2, "position"

    invoke-virtual {v1, v2, p1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 298
    :cond_0
    :goto_0
    invoke-virtual {p0, v1}, Lcom/google/cast/MediaProtocolMessageStream;->sendMessage(Lorg/json/JSONObject;)V

    .line 299
    iget-object v1, p0, Lcom/google/cast/MediaProtocolMessageStream;->p:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/google/cast/MediaProtocolCommand;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 300
    return-object v0

    .line 296
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method public final requestStatus()Lcom/google/cast/MediaProtocolCommand;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 139
    new-instance v0, Lcom/google/cast/MediaProtocolCommand;

    iget-wide v2, p0, Lcom/google/cast/MediaProtocolMessageStream;->n:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/cast/MediaProtocolMessageStream;->n:J

    const-string v1, "INFO"

    invoke-direct {v0, v2, v3, v1}, Lcom/google/cast/MediaProtocolCommand;-><init>(JLjava/lang/String;)V

    .line 142
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 144
    :try_start_0
    const-string v2, "cmd_id"

    invoke-virtual {v0}, Lcom/google/cast/MediaProtocolCommand;->a()J

    move-result-wide v4

    invoke-virtual {v1, v2, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 145
    const-string v2, "type"

    invoke-virtual {v0}, Lcom/google/cast/MediaProtocolCommand;->getType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 148
    :goto_0
    invoke-virtual {p0, v1}, Lcom/google/cast/MediaProtocolMessageStream;->sendMessage(Lorg/json/JSONObject;)V

    .line 149
    iget-object v1, p0, Lcom/google/cast/MediaProtocolMessageStream;->p:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/google/cast/MediaProtocolCommand;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 150
    return-object v0

    .line 146
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method public final resume()Lcom/google/cast/MediaProtocolCommand;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 258
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    invoke-virtual {p0, v0, v1}, Lcom/google/cast/MediaProtocolMessageStream;->playFrom(D)Lcom/google/cast/MediaProtocolCommand;

    move-result-object v0

    return-object v0
.end method

.method protected sendMessage(Lorg/json/JSONObject;)V
    .locals 4
    .param p1, "message"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 828
    sget-object v0, Lcom/google/cast/MediaProtocolMessageStream;->a:Lcom/google/cast/Logger;

    const-string v1, "Sending message: \"%s\""

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/cast/Logger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 829
    invoke-super {p0, p1}, Lcom/google/cast/MessageStream;->sendMessage(Lorg/json/JSONObject;)V

    .line 830
    return-void
.end method

.method public final setVolume(D)Lcom/google/cast/MediaProtocolCommand;
    .locals 7
    .param p1, "volume"    # D
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 329
    new-instance v0, Lcom/google/cast/MediaProtocolCommand;

    iget-wide v2, p0, Lcom/google/cast/MediaProtocolMessageStream;->n:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/cast/MediaProtocolMessageStream;->n:J

    const-string v1, "VOLUME"

    invoke-direct {v0, v2, v3, v1}, Lcom/google/cast/MediaProtocolCommand;-><init>(JLjava/lang/String;)V

    .line 332
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 334
    :try_start_0
    const-string v2, "cmd_id"

    invoke-virtual {v0}, Lcom/google/cast/MediaProtocolCommand;->a()J

    move-result-wide v4

    invoke-virtual {v1, v2, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 335
    const-string v2, "type"

    invoke-virtual {v0}, Lcom/google/cast/MediaProtocolCommand;->getType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 336
    const-string v2, "volume"

    invoke-direct {p0, p1, p2}, Lcom/google/cast/MediaProtocolMessageStream;->a(D)D

    move-result-wide v4

    invoke-virtual {v1, v2, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 339
    :goto_0
    invoke-virtual {p0, v1}, Lcom/google/cast/MediaProtocolMessageStream;->sendMessage(Lorg/json/JSONObject;)V

    .line 340
    iget-object v1, p0, Lcom/google/cast/MediaProtocolMessageStream;->p:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/google/cast/MediaProtocolCommand;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 342
    invoke-virtual {v0}, Lcom/google/cast/MediaProtocolCommand;->a()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/cast/MediaProtocolMessageStream;->q:J

    .line 343
    iput-wide p1, p0, Lcom/google/cast/MediaProtocolMessageStream;->g:D

    .line 344
    return-object v0

    .line 337
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method public final stop()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 310
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 312
    :try_start_0
    const-string v1, "cmd_id"

    iget-wide v2, p0, Lcom/google/cast/MediaProtocolMessageStream;->n:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/cast/MediaProtocolMessageStream;->n:J

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 313
    const-string v1, "type"

    const-string v2, "STOP"

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 316
    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/cast/MediaProtocolMessageStream;->sendMessage(Lorg/json/JSONObject;)V

    .line 317
    return-void

    .line 314
    :catch_0
    move-exception v1

    goto :goto_0
.end method
