.class Lcom/google/cast/m;
.super Lcom/google/cast/NetworkRequest;
.source "SourceFile"


# instance fields
.field private a:Landroid/net/Uri;

.field private b:Landroid/net/Uri;

.field private c:Lcom/google/cast/MimeData;


# direct methods
.method public constructor <init>(Lcom/google/cast/CastContext;Landroid/net/Uri;Lcom/google/cast/MimeData;)V
    .locals 2

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/google/cast/NetworkRequest;-><init>(Lcom/google/cast/CastContext;)V

    .line 42
    if-nez p2, :cond_0

    .line 43
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "must specify application url"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 45
    :cond_0
    iput-object p2, p0, Lcom/google/cast/m;->a:Landroid/net/Uri;

    .line 46
    iput-object p3, p0, Lcom/google/cast/m;->c:Lcom/google/cast/MimeData;

    .line 47
    return-void
.end method


# virtual methods
.method public final a()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/cast/m;->b:Landroid/net/Uri;

    return-object v0
.end method

.method public final execute()I
    .locals 4

    .prologue
    const/4 v0, -0x1

    .line 59
    :try_start_0
    iget-object v1, p0, Lcom/google/cast/m;->a:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/cast/m;->c:Lcom/google/cast/MimeData;

    sget v3, Lcom/google/cast/m;->DEFAULT_TIMEOUT:I

    invoke-virtual {p0, v1, v2, v3}, Lcom/google/cast/m;->performHttpPost(Landroid/net/Uri;Lcom/google/cast/MimeData;I)Lcom/google/cast/SimpleHttpRequest;

    move-result-object v1

    .line 62
    invoke-interface {v1}, Lcom/google/cast/SimpleHttpRequest;->getResponseStatus()I

    move-result v2

    .line 63
    const/16 v3, 0xc9

    if-ne v2, v3, :cond_2

    .line 64
    const-string v2, "Location"

    invoke-interface {v1, v2}, Lcom/google/cast/SimpleHttpRequest;->getResponseHeader(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 65
    if-nez v1, :cond_1

    .line 66
    const/4 v0, -0x3

    .line 80
    :cond_0
    :goto_0
    return v0

    .line 68
    :cond_1
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/google/cast/m;->b:Landroid/net/Uri;
    :try_end_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 69
    const/4 v0, 0x0

    goto :goto_0

    .line 70
    :cond_2
    const/16 v1, 0x194

    if-ne v2, v1, :cond_3

    .line 71
    const/4 v0, -0x4

    goto :goto_0

    .line 72
    :cond_3
    const/16 v1, 0x1f7

    if-ne v2, v1, :cond_0

    .line 73
    const/4 v0, -0x6

    goto :goto_0

    .line 77
    :catch_0
    move-exception v0

    .line 78
    const/4 v0, -0x2

    goto :goto_0

    .line 79
    :catch_1
    move-exception v1

    goto :goto_0
.end method
