.class Lcom/google/cast/SsdpScanner;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/cast/SsdpScanner$b;,
        Lcom/google/cast/SsdpScanner$a;,
        Lcom/google/cast/SsdpScanner$Listener;,
        Lcom/google/cast/SsdpScanner$SsdpResponse;,
        Lcom/google/cast/SsdpScanner$c;
    }
.end annotation


# instance fields
.field private a:Landroid/content/Context;

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/net/MulticastSocket;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/net/InetSocketAddress;

.field private d:Ljava/lang/String;

.field private e:I

.field private f:I

.field private g:Lcom/google/cast/SsdpScanner$Listener;

.field private h:Landroid/os/Handler;

.field private volatile i:Z

.field private j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Thread;",
            ">;"
        }
    .end annotation
.end field

.field private k:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private l:Landroid/net/ConnectivityManager;

.field private m:Lcom/google/cast/SsdpScanner$a;

.field private n:Landroid/net/wifi/WifiManager;

.field private o:Ljava/lang/String;

.field private p:Lcom/google/cast/SsdpScanner$c;

.field private volatile q:Z

.field private r:Lcom/google/cast/Logger;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;IILandroid/os/Handler;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "target"    # Ljava/lang/String;
    .param p3, "maxWaitTimeSeconds"    # I
    .param p4, "delayBetweenMessagesMillis"    # I
    .param p5, "handler"    # Landroid/os/Handler;

    .prologue
    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 105
    new-instance v0, Lcom/google/cast/Logger;

    const-string v1, "SsdpScanner"

    invoke-direct {v0, v1}, Lcom/google/cast/Logger;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/cast/SsdpScanner;->r:Lcom/google/cast/Logger;

    .line 107
    if-nez p2, :cond_0

    .line 108
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "target cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 110
    :cond_0
    const/4 v0, 0x1

    if-ge p3, v0, :cond_1

    .line 111
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "delay must be at least 1 second"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 113
    :cond_1
    iput-object p1, p0, Lcom/google/cast/SsdpScanner;->a:Landroid/content/Context;

    .line 114
    iput-object p2, p0, Lcom/google/cast/SsdpScanner;->d:Ljava/lang/String;

    .line 115
    iput p4, p0, Lcom/google/cast/SsdpScanner;->f:I

    .line 116
    iput p3, p0, Lcom/google/cast/SsdpScanner;->e:I

    .line 117
    iput-object p5, p0, Lcom/google/cast/SsdpScanner;->h:Landroid/os/Handler;

    .line 118
    new-instance v0, Ljava/net/InetSocketAddress;

    const-string v1, "239.255.255.250"

    const/16 v2, 0x76c

    invoke-direct {v0, v1, v2}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/google/cast/SsdpScanner;->c:Ljava/net/InetSocketAddress;

    .line 119
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lcom/google/cast/SsdpScanner;->k:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 120
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/cast/SsdpScanner;->b:Ljava/util/List;

    .line 121
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/cast/SsdpScanner;->j:Ljava/util/List;

    .line 123
    iget-object v0, p0, Lcom/google/cast/SsdpScanner;->a:Landroid/content/Context;

    const-string v1, "connectivity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, Lcom/google/cast/SsdpScanner;->l:Landroid/net/ConnectivityManager;

    .line 126
    iget-object v0, p0, Lcom/google/cast/SsdpScanner;->a:Landroid/content/Context;

    const-string v1, "android.permission.ACCESS_WIFI_STATE"

    invoke-virtual {v0, v1}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_2

    .line 128
    iget-object v0, p0, Lcom/google/cast/SsdpScanner;->a:Landroid/content/Context;

    const-string v1, "wifi"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    iput-object v0, p0, Lcom/google/cast/SsdpScanner;->n:Landroid/net/wifi/WifiManager;

    .line 132
    :goto_0
    return-void

    .line 130
    :cond_2
    iget-object v0, p0, Lcom/google/cast/SsdpScanner;->r:Lcom/google/cast/Logger;

    const-string v1, "Don\'t have Wifi permissions."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/cast/SsdpScanner;Lcom/google/cast/SsdpScanner$c;)Lcom/google/cast/SsdpScanner$c;
    .locals 0

    .prologue
    .line 45
    iput-object p1, p0, Lcom/google/cast/SsdpScanner;->p:Lcom/google/cast/SsdpScanner$c;

    return-object p1
.end method

.method static synthetic a(Lcom/google/cast/SsdpScanner;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 45
    iput-object p1, p0, Lcom/google/cast/SsdpScanner;->o:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcom/google/cast/SsdpScanner;)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/google/cast/SsdpScanner;->d()V

    return-void
.end method

.method static synthetic a(Lcom/google/cast/SsdpScanner;Ljava/net/MulticastSocket;)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0, p1}, Lcom/google/cast/SsdpScanner;->a(Ljava/net/MulticastSocket;)V

    return-void
.end method

.method static synthetic a(Lcom/google/cast/SsdpScanner;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0, p1}, Lcom/google/cast/SsdpScanner;->a(Ljava/util/List;)V

    return-void
.end method

.method private a(Ljava/net/MulticastSocket;)V
    .locals 7

    .prologue
    .line 366
    const/16 v0, 0x1000

    new-array v0, v0, [B

    .line 367
    new-instance v1, Ljava/net/DatagramPacket;

    array-length v2, v0

    invoke-direct {v1, v0, v2}, Ljava/net/DatagramPacket;-><init>([BI)V

    .line 370
    :cond_0
    :goto_0
    :try_start_0
    iget-boolean v0, p0, Lcom/google/cast/SsdpScanner;->i:Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v0, :cond_1

    .line 372
    :try_start_1
    invoke-virtual {p1, v1}, Ljava/net/MulticastSocket;->receive(Ljava/net/DatagramPacket;)V
    :try_end_1
    .catch Ljava/net/SocketTimeoutException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 376
    :try_start_2
    iget-boolean v0, p0, Lcom/google/cast/SsdpScanner;->i:Z

    if-eqz v0, :cond_2

    .line 426
    :cond_1
    :goto_1
    return-void

    .line 380
    :cond_2
    new-instance v0, Lcom/google/cast/SsdpScanner$SsdpResponse;

    invoke-direct {v0}, Lcom/google/cast/SsdpScanner$SsdpResponse;-><init>()V

    .line 382
    new-instance v2, Ljava/lang/String;

    invoke-virtual {v1}, Ljava/net/DatagramPacket;->getData()[B

    move-result-object v3

    invoke-virtual {v1}, Ljava/net/DatagramPacket;->getOffset()I

    move-result v4

    invoke-virtual {v1}, Ljava/net/DatagramPacket;->getLength()I

    move-result v5

    const-string v6, "UTF8"

    invoke-direct {v2, v3, v4, v5, v6}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    .line 384
    new-instance v3, Ljava/io/BufferedReader;

    new-instance v4, Ljava/io/StringReader;

    invoke-direct {v4, v2}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v3, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 386
    :cond_3
    :goto_2
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_a

    .line 387
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    .line 388
    const-string v5, "CACHE-CONTROL:"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 389
    const-string v4, "CACHE-CONTROL:"

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/cast/SsdpScanner$SsdpResponse;->a(Lcom/google/cast/SsdpScanner$SsdpResponse;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_2

    .line 420
    :catch_0
    move-exception v0

    .line 421
    iget-object v1, p0, Lcom/google/cast/SsdpScanner;->r:Lcom/google/cast/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Receive thread got an exception; mShouldStop="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/google/cast/SsdpScanner;->i:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/cast/Logger;->d(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 422
    iget-boolean v0, p0, Lcom/google/cast/SsdpScanner;->i:Z

    if-nez v0, :cond_1

    .line 423
    invoke-direct {p0}, Lcom/google/cast/SsdpScanner;->e()V

    goto :goto_1

    .line 391
    :cond_4
    :try_start_3
    const-string v5, "DATE:"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 392
    const-string v4, "DATE:"

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/cast/SsdpScanner$SsdpResponse;->b(Lcom/google/cast/SsdpScanner$SsdpResponse;Ljava/lang/String;)V

    goto :goto_2

    .line 393
    :cond_5
    const-string v5, "LOCATION:"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 394
    const-string v4, "LOCATION:"

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/cast/SsdpScanner$SsdpResponse;->c(Lcom/google/cast/SsdpScanner$SsdpResponse;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 395
    :cond_6
    const-string v5, "SERVER:"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 396
    const-string v4, "SERVER:"

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/cast/SsdpScanner$SsdpResponse;->d(Lcom/google/cast/SsdpScanner$SsdpResponse;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 397
    :cond_7
    const-string v5, "ST:"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 398
    const-string v4, "ST:"

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/cast/SsdpScanner$SsdpResponse;->e(Lcom/google/cast/SsdpScanner$SsdpResponse;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 400
    :cond_8
    const-string v5, "USN:"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 401
    const-string v4, "USN:"

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/cast/SsdpScanner$SsdpResponse;->f(Lcom/google/cast/SsdpScanner$SsdpResponse;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 402
    :cond_9
    const-string v5, "BOOTID.UPNP.ORG:"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 403
    const-string v4, "BOOTID.UPNP.ORG:"

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/cast/SsdpScanner$SsdpResponse;->g(Lcom/google/cast/SsdpScanner$SsdpResponse;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 408
    :cond_a
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V

    .line 410
    invoke-virtual {v0}, Lcom/google/cast/SsdpScanner$SsdpResponse;->isValidCastDevice()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/cast/SsdpScanner;->g:Lcom/google/cast/SsdpScanner$Listener;

    if-eqz v2, :cond_0

    .line 411
    iget-object v2, p0, Lcom/google/cast/SsdpScanner;->h:Landroid/os/Handler;

    new-instance v3, Lcom/google/cast/SsdpScanner$2;

    invoke-direct {v3, p0, v0}, Lcom/google/cast/SsdpScanner$2;-><init>(Lcom/google/cast/SsdpScanner;Lcom/google/cast/SsdpScanner$SsdpResponse;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    goto/16 :goto_0

    .line 373
    :catch_1
    move-exception v0

    goto/16 :goto_0
.end method

.method private a(Ljava/util/List;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/net/NetworkInterface;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 156
    iget-object v0, p0, Lcom/google/cast/SsdpScanner;->r:Lcom/google/cast/Logger;

    const-string v1, "startInternal"

    new-array v5, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v5}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 158
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 159
    :cond_0
    iget-object v0, p0, Lcom/google/cast/SsdpScanner;->r:Lcom/google/cast/Logger;

    const-string v1, "No suitable network interface!"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 160
    invoke-direct {p0}, Lcom/google/cast/SsdpScanner;->f()V

    .line 232
    :goto_0
    return-void

    .line 165
    :cond_1
    :try_start_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/NetworkInterface;

    .line 166
    new-instance v5, Ljava/net/MulticastSocket;

    invoke-direct {v5}, Ljava/net/MulticastSocket;-><init>()V

    .line 167
    invoke-virtual {v5, v0}, Ljava/net/MulticastSocket;->setNetworkInterface(Ljava/net/NetworkInterface;)V

    .line 168
    const/4 v0, 0x2

    invoke-virtual {v5, v0}, Ljava/net/MulticastSocket;->setTimeToLive(I)V

    .line 169
    iget-object v0, p0, Lcom/google/cast/SsdpScanner;->b:Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 171
    :catch_0
    move-exception v0

    .line 172
    :try_start_1
    iget-object v1, p0, Lcom/google/cast/SsdpScanner;->r:Lcom/google/cast/Logger;

    const-string v2, "couldn\'t create socket"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v4}, Lcom/google/cast/Logger;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 173
    invoke-direct {p0}, Lcom/google/cast/SsdpScanner;->f()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 176
    iput-object v3, p0, Lcom/google/cast/SsdpScanner;->p:Lcom/google/cast/SsdpScanner$c;

    goto :goto_0

    :cond_2
    iput-object v3, p0, Lcom/google/cast/SsdpScanner;->p:Lcom/google/cast/SsdpScanner$c;

    .line 179
    iput-boolean v4, p0, Lcom/google/cast/SsdpScanner;->q:Z

    .line 180
    iput-boolean v4, p0, Lcom/google/cast/SsdpScanner;->i:Z

    .line 181
    iget-object v0, p0, Lcom/google/cast/SsdpScanner;->k:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v4}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 183
    new-instance v0, Lcom/google/cast/SsdpScanner$1;

    invoke-direct {v0, p0}, Lcom/google/cast/SsdpScanner$1;-><init>(Lcom/google/cast/SsdpScanner;)V

    .line 189
    const-string v1, "SsdpScanner send thread"

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 190
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 191
    iget-object v1, p0, Lcom/google/cast/SsdpScanner;->j:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 194
    iget-object v0, p0, Lcom/google/cast/SsdpScanner;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    .line 195
    iget-object v0, p0, Lcom/google/cast/SsdpScanner;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v1, v2

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/MulticastSocket;

    .line 196
    new-instance v7, Ljava/lang/Thread;

    new-instance v8, Lcom/google/cast/SsdpScanner$b;

    invoke-direct {v8, p0, v0}, Lcom/google/cast/SsdpScanner$b;-><init>(Lcom/google/cast/SsdpScanner;Ljava/net/MulticastSocket;)V

    invoke-direct {v7, v8}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 197
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "SsdpScanner receive thread #"

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v8, " of "

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 198
    invoke-virtual {v7}, Ljava/lang/Thread;->start()V

    .line 199
    iget-object v0, p0, Lcom/google/cast/SsdpScanner;->j:Ljava/util/List;

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 200
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 201
    goto :goto_2

    .line 176
    :catchall_0
    move-exception v0

    iput-object v3, p0, Lcom/google/cast/SsdpScanner;->p:Lcom/google/cast/SsdpScanner$c;

    throw v0

    .line 204
    :cond_3
    iget-object v0, p0, Lcom/google/cast/SsdpScanner;->n:Landroid/net/wifi/WifiManager;

    if-eqz v0, :cond_5

    .line 209
    iget-object v0, p0, Lcom/google/cast/SsdpScanner;->n:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v0

    .line 210
    if-eqz v0, :cond_8

    .line 211
    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getBSSID()Ljava/lang/String;

    move-result-object v0

    .line 214
    :goto_3
    iget-object v1, p0, Lcom/google/cast/SsdpScanner;->o:Ljava/lang/String;

    if-eqz v1, :cond_4

    if-eqz v0, :cond_4

    iget-object v1, p0, Lcom/google/cast/SsdpScanner;->o:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 216
    :cond_4
    iget-object v1, p0, Lcom/google/cast/SsdpScanner;->r:Lcom/google/cast/Logger;

    const-string v3, "BSSID changed"

    new-array v5, v4, [Ljava/lang/Object;

    invoke-virtual {v1, v3, v5}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 219
    :goto_4
    iput-object v0, p0, Lcom/google/cast/SsdpScanner;->o:Ljava/lang/String;

    .line 227
    :cond_5
    if-eqz v2, :cond_6

    iget-object v0, p0, Lcom/google/cast/SsdpScanner;->g:Lcom/google/cast/SsdpScanner$Listener;

    if-eqz v0, :cond_6

    .line 228
    iget-object v0, p0, Lcom/google/cast/SsdpScanner;->g:Lcom/google/cast/SsdpScanner$Listener;

    invoke-interface {v0}, Lcom/google/cast/SsdpScanner$Listener;->onResultsInvalidated()V

    .line 231
    :cond_6
    iget-object v0, p0, Lcom/google/cast/SsdpScanner;->r:Lcom/google/cast/Logger;

    const-string v1, "scan started"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    :cond_7
    move v2, v4

    goto :goto_4

    :cond_8
    move-object v0, v3

    goto :goto_3
.end method

.method static synthetic b(Lcom/google/cast/SsdpScanner;)Lcom/google/cast/Logger;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/cast/SsdpScanner;->r:Lcom/google/cast/Logger;

    return-object v0
.end method

.method static synthetic c(Lcom/google/cast/SsdpScanner;)Lcom/google/cast/SsdpScanner$c;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/cast/SsdpScanner;->p:Lcom/google/cast/SsdpScanner$c;

    return-object v0
.end method

.method private c()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 240
    iget-object v0, p0, Lcom/google/cast/SsdpScanner;->p:Lcom/google/cast/SsdpScanner$c;

    if-eqz v0, :cond_0

    .line 241
    iget-object v0, p0, Lcom/google/cast/SsdpScanner;->p:Lcom/google/cast/SsdpScanner$c;

    invoke-virtual {v0, v1}, Lcom/google/cast/SsdpScanner$c;->cancel(Z)Z

    .line 242
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/cast/SsdpScanner;->p:Lcom/google/cast/SsdpScanner$c;

    .line 245
    :cond_0
    iget-object v0, p0, Lcom/google/cast/SsdpScanner;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 270
    :goto_0
    return-void

    .line 252
    :cond_1
    iput-boolean v1, p0, Lcom/google/cast/SsdpScanner;->i:Z

    .line 254
    iget-object v0, p0, Lcom/google/cast/SsdpScanner;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/MulticastSocket;

    .line 255
    invoke-virtual {v0}, Ljava/net/MulticastSocket;->close()V

    goto :goto_1

    .line 258
    :cond_2
    iget-object v0, p0, Lcom/google/cast/SsdpScanner;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Thread;

    .line 259
    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 262
    :goto_3
    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Thread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 264
    :catch_0
    move-exception v2

    goto :goto_3

    .line 267
    :cond_3
    iget-object v0, p0, Lcom/google/cast/SsdpScanner;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 269
    iget-object v0, p0, Lcom/google/cast/SsdpScanner;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    goto :goto_0
.end method

.method static synthetic d(Lcom/google/cast/SsdpScanner;)Lcom/google/cast/SsdpScanner$Listener;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/cast/SsdpScanner;->g:Lcom/google/cast/SsdpScanner$Listener;

    return-object v0
.end method

.method private d()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 335
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    const-string v1, "M-SEARCH * HTTP/1.1\r\nHOST: %s:%d\r\nMAN: \"ssdp:discover\"\r\nMX: %d\r\nST: %s\r\n\r\n"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "239.255.255.250"

    aput-object v3, v2, v6

    const/4 v3, 0x1

    const/16 v4, 0x76c

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget v4, p0, Lcom/google/cast/SsdpScanner;->e:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    iget-object v4, p0, Lcom/google/cast/SsdpScanner;->d:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 341
    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    .line 344
    :try_start_0
    new-instance v1, Ljava/net/DatagramPacket;

    array-length v2, v0

    iget-object v3, p0, Lcom/google/cast/SsdpScanner;->c:Ljava/net/InetSocketAddress;

    invoke-direct {v1, v0, v2, v3}, Ljava/net/DatagramPacket;-><init>([BILjava/net/SocketAddress;)V

    .line 345
    :cond_0
    :goto_0
    iget-boolean v0, p0, Lcom/google/cast/SsdpScanner;->i:Z

    if-nez v0, :cond_2

    .line 346
    iget-object v0, p0, Lcom/google/cast/SsdpScanner;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/MulticastSocket;

    .line 347
    invoke-virtual {v0, v1}, Ljava/net/MulticastSocket;->send(Ljava/net/DatagramPacket;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 349
    :try_start_1
    iget v0, p0, Lcom/google/cast/SsdpScanner;->f:I

    int-to-long v4, v0

    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 350
    :catch_0
    move-exception v0

    .line 351
    :try_start_2
    iget-boolean v0, p0, Lcom/google/cast/SsdpScanner;->i:Z
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    if-eqz v0, :cond_1

    goto :goto_0

    .line 357
    :catch_1
    move-exception v0

    .line 358
    iget-object v1, p0, Lcom/google/cast/SsdpScanner;->r:Lcom/google/cast/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Send thread got an exception; mShouldStop="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/google/cast/SsdpScanner;->i:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v3, v6, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/cast/Logger;->d(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 359
    iget-boolean v0, p0, Lcom/google/cast/SsdpScanner;->i:Z

    if-nez v0, :cond_2

    .line 360
    invoke-direct {p0}, Lcom/google/cast/SsdpScanner;->e()V

    .line 363
    :cond_2
    return-void
.end method

.method private e()V
    .locals 2

    .prologue
    .line 435
    iget-object v0, p0, Lcom/google/cast/SsdpScanner;->k:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 437
    iget-object v0, p0, Lcom/google/cast/SsdpScanner;->g:Lcom/google/cast/SsdpScanner$Listener;

    if-eqz v0, :cond_0

    .line 438
    iget-object v0, p0, Lcom/google/cast/SsdpScanner;->h:Landroid/os/Handler;

    new-instance v1, Lcom/google/cast/SsdpScanner$3;

    invoke-direct {v1, p0}, Lcom/google/cast/SsdpScanner$3;-><init>(Lcom/google/cast/SsdpScanner;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 447
    :cond_0
    return-void
.end method

.method static synthetic e(Lcom/google/cast/SsdpScanner;)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/google/cast/SsdpScanner;->f()V

    return-void
.end method

.method static synthetic f(Lcom/google/cast/SsdpScanner;)Landroid/net/ConnectivityManager;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/cast/SsdpScanner;->l:Landroid/net/ConnectivityManager;

    return-object v0
.end method

.method private f()V
    .locals 3

    .prologue
    .line 450
    iget-boolean v0, p0, Lcom/google/cast/SsdpScanner;->q:Z

    if-nez v0, :cond_0

    .line 451
    iget-object v0, p0, Lcom/google/cast/SsdpScanner;->r:Lcom/google/cast/Logger;

    const-string v1, "reportNetworkError; errorState now true"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 452
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/cast/SsdpScanner;->q:Z

    .line 453
    iget-object v0, p0, Lcom/google/cast/SsdpScanner;->g:Lcom/google/cast/SsdpScanner$Listener;

    if-eqz v0, :cond_0

    .line 454
    iget-object v0, p0, Lcom/google/cast/SsdpScanner;->g:Lcom/google/cast/SsdpScanner$Listener;

    invoke-interface {v0}, Lcom/google/cast/SsdpScanner$Listener;->onNetworkError()V

    .line 457
    :cond_0
    return-void
.end method

.method private g()V
    .locals 3

    .prologue
    .line 568
    iget-object v0, p0, Lcom/google/cast/SsdpScanner;->m:Lcom/google/cast/SsdpScanner$a;

    if-eqz v0, :cond_0

    .line 574
    :goto_0
    return-void

    .line 571
    :cond_0
    new-instance v0, Lcom/google/cast/SsdpScanner$a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/cast/SsdpScanner$a;-><init>(Lcom/google/cast/SsdpScanner;Lcom/google/cast/SsdpScanner$1;)V

    iput-object v0, p0, Lcom/google/cast/SsdpScanner;->m:Lcom/google/cast/SsdpScanner$a;

    .line 572
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 573
    iget-object v1, p0, Lcom/google/cast/SsdpScanner;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/cast/SsdpScanner;->m:Lcom/google/cast/SsdpScanner$a;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    goto :goto_0
.end method

.method static synthetic g(Lcom/google/cast/SsdpScanner;)Z
    .locals 1

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/google/cast/SsdpScanner;->q:Z

    return v0
.end method

.method private h()V
    .locals 2

    .prologue
    .line 577
    iget-object v0, p0, Lcom/google/cast/SsdpScanner;->m:Lcom/google/cast/SsdpScanner$a;

    if-nez v0, :cond_0

    .line 586
    :goto_0
    return-void

    .line 581
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/google/cast/SsdpScanner;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/cast/SsdpScanner;->m:Lcom/google/cast/SsdpScanner$a;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 585
    :goto_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/cast/SsdpScanner;->m:Lcom/google/cast/SsdpScanner$a;

    goto :goto_0

    .line 582
    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method static synthetic h(Lcom/google/cast/SsdpScanner;)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/google/cast/SsdpScanner;->c()V

    return-void
.end method

.method static synthetic i(Lcom/google/cast/SsdpScanner;)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/google/cast/SsdpScanner;->e()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 144
    iget-object v0, p0, Lcom/google/cast/SsdpScanner;->r:Lcom/google/cast/Logger;

    const-string v1, "start"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 145
    iget-object v0, p0, Lcom/google/cast/SsdpScanner;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/cast/SsdpScanner;->p:Lcom/google/cast/SsdpScanner$c;

    if-eqz v0, :cond_1

    .line 153
    :cond_0
    :goto_0
    return-void

    .line 149
    :cond_1
    invoke-direct {p0}, Lcom/google/cast/SsdpScanner;->g()V

    .line 151
    new-instance v0, Lcom/google/cast/SsdpScanner$c;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/cast/SsdpScanner$c;-><init>(Lcom/google/cast/SsdpScanner;Lcom/google/cast/SsdpScanner$1;)V

    iput-object v0, p0, Lcom/google/cast/SsdpScanner;->p:Lcom/google/cast/SsdpScanner$c;

    .line 152
    iget-object v0, p0, Lcom/google/cast/SsdpScanner;->p:Lcom/google/cast/SsdpScanner$c;

    new-array v1, v3, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/cast/SsdpScanner$c;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method public a(Lcom/google/cast/SsdpScanner$Listener;)V
    .locals 0

    .prologue
    .line 135
    iput-object p1, p0, Lcom/google/cast/SsdpScanner;->g:Lcom/google/cast/SsdpScanner$Listener;

    .line 136
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 235
    invoke-direct {p0}, Lcom/google/cast/SsdpScanner;->h()V

    .line 236
    invoke-direct {p0}, Lcom/google/cast/SsdpScanner;->c()V

    .line 237
    return-void
.end method
