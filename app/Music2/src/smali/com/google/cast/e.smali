.class Lcom/google/cast/e;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:[B

.field private b:I

.field private c:I

.field private d:I

.field private e:Z

.field private f:[Ljava/nio/ByteBuffer;

.field private g:[Ljava/nio/ByteBuffer;


# direct methods
.method public constructor <init>(I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    if-ge p1, v1, :cond_0

    .line 45
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "invalid capacity "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; must be > 0"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 48
    :cond_0
    new-array v0, p1, [B

    iput-object v0, p0, Lcom/google/cast/e;->a:[B

    .line 49
    new-array v0, v1, [Ljava/nio/ByteBuffer;

    iput-object v0, p0, Lcom/google/cast/e;->f:[Ljava/nio/ByteBuffer;

    .line 50
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/nio/ByteBuffer;

    iput-object v0, p0, Lcom/google/cast/e;->g:[Ljava/nio/ByteBuffer;

    .line 51
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/cast/e;->d:I

    .line 52
    iput-boolean v1, p0, Lcom/google/cast/e;->e:Z

    .line 53
    return-void
.end method

.method private a([BIILjava/nio/charset/CharsetDecoder;)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;,
            Lcom/google/cast/f;
        }
    .end annotation

    .prologue
    .line 619
    invoke-virtual {p4}, Ljava/nio/charset/CharsetDecoder;->reset()Ljava/nio/charset/CharsetDecoder;

    .line 621
    :try_start_0
    invoke-static {p1, p2, p3}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 622
    sget-object v1, Ljava/nio/charset/CodingErrorAction;->REPORT:Ljava/nio/charset/CodingErrorAction;

    invoke-virtual {p4, v1}, Ljava/nio/charset/CharsetDecoder;->onMalformedInput(Ljava/nio/charset/CodingErrorAction;)Ljava/nio/charset/CharsetDecoder;

    .line 623
    sget-object v1, Ljava/nio/charset/CodingErrorAction;->REPORT:Ljava/nio/charset/CodingErrorAction;

    invoke-virtual {p4, v1}, Ljava/nio/charset/CharsetDecoder;->onUnmappableCharacter(Ljava/nio/charset/CodingErrorAction;)Ljava/nio/charset/CharsetDecoder;

    .line 624
    invoke-virtual {p4, v0}, Ljava/nio/charset/CharsetDecoder;->decode(Ljava/nio/ByteBuffer;)Ljava/nio/CharBuffer;

    move-result-object v1

    .line 625
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 626
    new-instance v0, Lcom/google/cast/f;

    const-string v1, "unexpected data after end of string"

    invoke-direct {v0, v1}, Lcom/google/cast/f;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/nio/charset/IllegalCharsetNameException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/nio/charset/UnsupportedCharsetException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/nio/charset/MalformedInputException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/nio/charset/UnmappableCharacterException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/nio/charset/CharacterCodingException; {:try_start_0 .. :try_end_0} :catch_5

    .line 629
    :catch_0
    move-exception v0

    .line 630
    new-instance v1, Ljava/io/UnsupportedEncodingException;

    invoke-virtual {v0}, Ljava/nio/charset/IllegalCharsetNameException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/UnsupportedEncodingException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 628
    :cond_0
    :try_start_1
    invoke-virtual {v1}, Ljava/nio/CharBuffer;->toString()Ljava/lang/String;
    :try_end_1
    .catch Ljava/nio/charset/IllegalCharsetNameException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/nio/charset/UnsupportedCharsetException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/nio/charset/MalformedInputException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/nio/charset/UnmappableCharacterException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/nio/charset/CharacterCodingException; {:try_start_1 .. :try_end_1} :catch_5

    move-result-object v0

    return-object v0

    .line 631
    :catch_1
    move-exception v0

    .line 632
    new-instance v1, Ljava/io/UnsupportedEncodingException;

    invoke-virtual {v0}, Ljava/nio/charset/UnsupportedCharsetException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/UnsupportedEncodingException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 633
    :catch_2
    move-exception v0

    .line 634
    new-instance v1, Lcom/google/cast/f;

    invoke-direct {v1, v0}, Lcom/google/cast/f;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 635
    :catch_3
    move-exception v0

    .line 636
    new-instance v1, Lcom/google/cast/f;

    invoke-direct {v1, v0}, Lcom/google/cast/f;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 637
    :catch_4
    move-exception v0

    .line 638
    new-instance v1, Lcom/google/cast/f;

    invoke-direct {v1, v0}, Lcom/google/cast/f;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 639
    :catch_5
    move-exception v0

    .line 640
    new-instance v1, Lcom/google/cast/f;

    invoke-direct {v1, v0}, Lcom/google/cast/f;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private b(B)V
    .locals 2

    .prologue
    .line 610
    iget-object v0, p0, Lcom/google/cast/e;->a:[B

    iget v1, p0, Lcom/google/cast/e;->c:I

    aput-byte p1, v0, v1

    .line 611
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/cast/e;->d(I)V

    .line 612
    return-void
.end method

.method private c(I)V
    .locals 2

    .prologue
    .line 566
    iget v0, p0, Lcom/google/cast/e;->b:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/google/cast/e;->b:I

    .line 567
    iget v0, p0, Lcom/google/cast/e;->b:I

    iget-object v1, p0, Lcom/google/cast/e;->a:[B

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 568
    iget v0, p0, Lcom/google/cast/e;->b:I

    iget-object v1, p0, Lcom/google/cast/e;->a:[B

    array-length v1, v1

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/google/cast/e;->b:I

    .line 572
    :cond_0
    iget v0, p0, Lcom/google/cast/e;->b:I

    iget v1, p0, Lcom/google/cast/e;->c:I

    if-ne v0, v1, :cond_1

    .line 573
    iget v0, p0, Lcom/google/cast/e;->d:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_2

    .line 576
    invoke-virtual {p0}, Lcom/google/cast/e;->d()V

    .line 582
    :cond_1
    :goto_0
    return-void

    .line 579
    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/cast/e;->e:Z

    goto :goto_0
.end method

.method private d(I)V
    .locals 2

    .prologue
    .line 589
    iget v0, p0, Lcom/google/cast/e;->c:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/google/cast/e;->c:I

    .line 590
    iget v0, p0, Lcom/google/cast/e;->c:I

    iget-object v1, p0, Lcom/google/cast/e;->a:[B

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 591
    iget v0, p0, Lcom/google/cast/e;->c:I

    iget-object v1, p0, Lcom/google/cast/e;->a:[B

    array-length v1, v1

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/google/cast/e;->c:I

    .line 593
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/cast/e;->e:Z

    .line 594
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/cast/e;->d:I

    .line 595
    return-void
.end method

.method private l()I
    .locals 2

    .prologue
    .line 538
    iget-boolean v0, p0, Lcom/google/cast/e;->e:Z

    if-eqz v0, :cond_0

    .line 539
    const/4 v0, 0x0

    .line 543
    :goto_0
    return v0

    .line 540
    :cond_0
    iget v0, p0, Lcom/google/cast/e;->b:I

    iget v1, p0, Lcom/google/cast/e;->c:I

    if-ge v0, v1, :cond_1

    .line 541
    iget v0, p0, Lcom/google/cast/e;->c:I

    iget v1, p0, Lcom/google/cast/e;->b:I

    sub-int/2addr v0, v1

    goto :goto_0

    .line 543
    :cond_1
    iget-object v0, p0, Lcom/google/cast/e;->a:[B

    array-length v0, v0

    iget v1, p0, Lcom/google/cast/e;->b:I

    sub-int/2addr v0, v1

    goto :goto_0
.end method

.method private m()I
    .locals 2

    .prologue
    .line 552
    iget-boolean v0, p0, Lcom/google/cast/e;->e:Z

    if-eqz v0, :cond_0

    .line 553
    iget-object v0, p0, Lcom/google/cast/e;->a:[B

    array-length v0, v0

    .line 557
    :goto_0
    return v0

    .line 554
    :cond_0
    iget v0, p0, Lcom/google/cast/e;->c:I

    iget v1, p0, Lcom/google/cast/e;->b:I

    if-ge v0, v1, :cond_1

    .line 555
    iget v0, p0, Lcom/google/cast/e;->b:I

    iget v1, p0, Lcom/google/cast/e;->c:I

    sub-int/2addr v0, v1

    goto :goto_0

    .line 557
    :cond_1
    iget-object v0, p0, Lcom/google/cast/e;->a:[B

    array-length v0, v0

    iget v1, p0, Lcom/google/cast/e;->c:I

    sub-int/2addr v0, v1

    goto :goto_0
.end method

.method private n()B
    .locals 2

    .prologue
    .line 601
    iget-object v0, p0, Lcom/google/cast/e;->a:[B

    iget v1, p0, Lcom/google/cast/e;->b:I

    aget-byte v0, v0, v1

    .line 602
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/google/cast/e;->c(I)V

    .line 603
    return v0
.end method


# virtual methods
.method public a(Ljava/nio/channels/SocketChannel;)I
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 210
    invoke-virtual {p0}, Lcom/google/cast/e;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 230
    :goto_0
    return v0

    .line 216
    :cond_0
    iget v0, p0, Lcom/google/cast/e;->b:I

    iget v2, p0, Lcom/google/cast/e;->c:I

    if-ge v0, v2, :cond_1

    .line 218
    iget-object v0, p0, Lcom/google/cast/e;->f:[Ljava/nio/ByteBuffer;

    .line 219
    iget-object v2, p0, Lcom/google/cast/e;->a:[B

    iget v3, p0, Lcom/google/cast/e;->b:I

    iget v4, p0, Lcom/google/cast/e;->c:I

    iget v5, p0, Lcom/google/cast/e;->b:I

    sub-int/2addr v4, v5

    invoke-static {v2, v3, v4}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 227
    :goto_1
    invoke-virtual {p1, v0}, Ljava/nio/channels/SocketChannel;->write([Ljava/nio/ByteBuffer;)J

    move-result-wide v0

    long-to-int v0, v0

    .line 228
    if-lez v0, :cond_2

    .line 229
    invoke-direct {p0, v0}, Lcom/google/cast/e;->c(I)V

    goto :goto_0

    .line 222
    :cond_1
    iget-object v0, p0, Lcom/google/cast/e;->g:[Ljava/nio/ByteBuffer;

    .line 223
    iget-object v2, p0, Lcom/google/cast/e;->a:[B

    iget v3, p0, Lcom/google/cast/e;->b:I

    iget-object v4, p0, Lcom/google/cast/e;->a:[B

    array-length v4, v4

    iget v5, p0, Lcom/google/cast/e;->b:I

    sub-int/2addr v4, v5

    invoke-static {v2, v3, v4}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 224
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/cast/e;->a:[B

    iget v4, p0, Lcom/google/cast/e;->c:I

    invoke-static {v3, v1, v4}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v1

    aput-object v1, v0, v2

    goto :goto_1

    .line 235
    :cond_2
    new-instance v0, Ljava/nio/channels/ClosedChannelException;

    invoke-direct {v0}, Ljava/nio/channels/ClosedChannelException;-><init>()V

    throw v0
.end method

.method public a([B)I
    .locals 10

    .prologue
    const/4 v3, -0x1

    const/4 v1, 0x0

    .line 262
    invoke-virtual {p0}, Lcom/google/cast/e;->i()I

    move-result v0

    array-length v2, p1

    if-ge v0, v2, :cond_0

    .line 289
    :goto_0
    return v3

    .line 267
    :cond_0
    iget v0, p0, Lcom/google/cast/e;->b:I

    .line 269
    invoke-virtual {p0}, Lcom/google/cast/e;->i()I

    move-result v2

    array-length v4, p1

    add-int/lit8 v4, v4, -0x1

    sub-int v6, v2, v4

    move v4, v1

    move v5, v0

    move v0, v1

    .line 270
    :goto_1
    if-ge v4, v6, :cond_2

    move v2, v0

    move v0, v1

    .line 271
    :goto_2
    array-length v7, p1

    if-ge v0, v7, :cond_1

    .line 272
    const/4 v2, 0x1

    .line 273
    iget-object v7, p0, Lcom/google/cast/e;->a:[B

    add-int v8, v5, v0

    iget-object v9, p0, Lcom/google/cast/e;->a:[B

    array-length v9, v9

    rem-int/2addr v8, v9

    aget-byte v7, v7, v8

    aget-byte v8, p1, v0

    if-eq v7, v8, :cond_3

    move v2, v1

    .line 279
    :cond_1
    if-eqz v2, :cond_4

    move v0, v2

    .line 289
    :cond_2
    if-eqz v0, :cond_6

    array-length v0, p1

    add-int/2addr v0, v4

    :goto_3
    move v3, v0

    goto :goto_0

    .line 271
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 283
    :cond_4
    add-int/lit8 v0, v5, 0x1

    .line 284
    iget-object v5, p0, Lcom/google/cast/e;->a:[B

    array-length v5, v5

    if-ne v0, v5, :cond_5

    move v0, v1

    .line 270
    :cond_5
    add-int/lit8 v4, v4, 0x1

    move v5, v0

    move v0, v2

    goto :goto_1

    :cond_6
    move v0, v3

    .line 289
    goto :goto_3
.end method

.method public a()Ljava/lang/Byte;
    .locals 2

    .prologue
    .line 115
    invoke-virtual {p0}, Lcom/google/cast/e;->i()I

    move-result v0

    const/4 v1, 0x1

    if-ge v0, v1, :cond_0

    .line 116
    const/4 v0, 0x0

    .line 118
    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0}, Lcom/google/cast/e;->n()B

    move-result v0

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    goto :goto_0
.end method

.method public a(I[BLjava/nio/charset/CharsetDecoder;)Ljava/lang/String;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;,
            Lcom/google/cast/f;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 168
    invoke-virtual {p0}, Lcom/google/cast/e;->i()I

    move-result v0

    if-ge v0, p1, :cond_0

    .line 169
    const/4 v0, 0x0

    .line 197
    :goto_0
    return-object v0

    .line 172
    :cond_0
    invoke-direct {p0}, Lcom/google/cast/e;->l()I

    move-result v0

    .line 173
    if-lt v0, p1, :cond_2

    .line 175
    if-eqz p2, :cond_1

    .line 176
    iget v0, p0, Lcom/google/cast/e;->b:I

    .line 177
    :goto_1
    if-ge v1, p1, :cond_1

    .line 178
    iget-object v2, p0, Lcom/google/cast/e;->a:[B

    aget-byte v3, v2, v0

    array-length v4, p2

    rem-int v4, v1, v4

    aget-byte v4, p2, v4

    xor-int/2addr v3, v4

    int-to-byte v3, v3

    aput-byte v3, v2, v0

    .line 179
    add-int/lit8 v0, v0, 0x1

    .line 177
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 182
    :cond_1
    iget-object v0, p0, Lcom/google/cast/e;->a:[B

    iget v1, p0, Lcom/google/cast/e;->b:I

    invoke-direct {p0, v0, v1, p1, p3}, Lcom/google/cast/e;->a([BIILjava/nio/charset/CharsetDecoder;)Ljava/lang/String;

    move-result-object v0

    .line 183
    invoke-direct {p0, p1}, Lcom/google/cast/e;->c(I)V

    goto :goto_0

    .line 187
    :cond_2
    new-array v2, p1, [B

    .line 188
    iget-object v3, p0, Lcom/google/cast/e;->a:[B

    iget v4, p0, Lcom/google/cast/e;->b:I

    invoke-static {v3, v4, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 189
    invoke-direct {p0, v0}, Lcom/google/cast/e;->c(I)V

    .line 190
    iget-object v3, p0, Lcom/google/cast/e;->a:[B

    iget v4, p0, Lcom/google/cast/e;->b:I

    sub-int v5, p1, v0

    invoke-static {v3, v4, v2, v0, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 191
    sub-int v0, p1, v0

    invoke-direct {p0, v0}, Lcom/google/cast/e;->c(I)V

    .line 192
    if-eqz p2, :cond_3

    move v0, v1

    .line 193
    :goto_2
    if-ge v0, p1, :cond_3

    .line 194
    aget-byte v3, v2, v0

    array-length v4, p2

    rem-int v4, v0, v4

    aget-byte v4, p2, v4

    xor-int/2addr v3, v4

    int-to-byte v3, v3

    aput-byte v3, v2, v0

    .line 193
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 197
    :cond_3
    array-length v0, v2

    invoke-direct {p0, v2, v1, v0, p3}, Lcom/google/cast/e;->a([BIILjava/nio/charset/CharsetDecoder;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public a(B)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 354
    invoke-virtual {p0}, Lcom/google/cast/e;->h()I

    move-result v1

    if-ge v1, v0, :cond_0

    .line 355
    const/4 v0, 0x0

    .line 358
    :goto_0
    return v0

    .line 357
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/cast/e;->b(B)V

    goto :goto_0
.end method

.method public a(I)Z
    .locals 1

    .prologue
    .line 247
    invoke-virtual {p0}, Lcom/google/cast/e;->i()I

    move-result v0

    if-ge v0, p1, :cond_0

    .line 248
    const/4 v0, 0x0

    .line 251
    :goto_0
    return v0

    .line 250
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/cast/e;->c(I)V

    .line 251
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public a(J)Z
    .locals 5

    .prologue
    const/16 v4, 0x8

    const-wide/16 v2, 0xff

    .line 385
    invoke-virtual {p0}, Lcom/google/cast/e;->h()I

    move-result v0

    if-ge v0, v4, :cond_0

    .line 386
    const/4 v0, 0x0

    .line 396
    :goto_0
    return v0

    .line 388
    :cond_0
    const/16 v0, 0x38

    shr-long v0, p1, v0

    and-long/2addr v0, v2

    long-to-int v0, v0

    int-to-byte v0, v0

    invoke-direct {p0, v0}, Lcom/google/cast/e;->b(B)V

    .line 389
    const/16 v0, 0x30

    shr-long v0, p1, v0

    and-long/2addr v0, v2

    long-to-int v0, v0

    int-to-byte v0, v0

    invoke-direct {p0, v0}, Lcom/google/cast/e;->b(B)V

    .line 390
    const/16 v0, 0x28

    shr-long v0, p1, v0

    and-long/2addr v0, v2

    long-to-int v0, v0

    int-to-byte v0, v0

    invoke-direct {p0, v0}, Lcom/google/cast/e;->b(B)V

    .line 391
    const/16 v0, 0x20

    shr-long v0, p1, v0

    and-long/2addr v0, v2

    long-to-int v0, v0

    int-to-byte v0, v0

    invoke-direct {p0, v0}, Lcom/google/cast/e;->b(B)V

    .line 392
    const/16 v0, 0x18

    shr-long v0, p1, v0

    and-long/2addr v0, v2

    long-to-int v0, v0

    int-to-byte v0, v0

    invoke-direct {p0, v0}, Lcom/google/cast/e;->b(B)V

    .line 393
    const/16 v0, 0x10

    shr-long v0, p1, v0

    and-long/2addr v0, v2

    long-to-int v0, v0

    int-to-byte v0, v0

    invoke-direct {p0, v0}, Lcom/google/cast/e;->b(B)V

    .line 394
    shr-long v0, p1, v4

    and-long/2addr v0, v2

    long-to-int v0, v0

    int-to-byte v0, v0

    invoke-direct {p0, v0}, Lcom/google/cast/e;->b(B)V

    .line 395
    and-long v0, p1, v2

    long-to-int v0, v0

    int-to-byte v0, v0

    invoke-direct {p0, v0}, Lcom/google/cast/e;->b(B)V

    .line 396
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public a(Ljava/lang/String;[BLjava/nio/charset/Charset;)Z
    .locals 1

    .prologue
    .line 409
    invoke-virtual {p1, p3}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    .line 410
    invoke-virtual {p0, v0, p2}, Lcom/google/cast/e;->b([B[B)Z

    move-result v0

    return v0
.end method

.method public a([BII[B)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 85
    invoke-virtual {p0}, Lcom/google/cast/e;->i()I

    move-result v1

    if-ge v1, p3, :cond_0

    .line 105
    :goto_0
    return v0

    .line 90
    :cond_0
    invoke-direct {p0}, Lcom/google/cast/e;->l()I

    move-result v1

    invoke-static {p3, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 91
    iget-object v2, p0, Lcom/google/cast/e;->a:[B

    iget v3, p0, Lcom/google/cast/e;->b:I

    invoke-static {v2, v3, p1, p2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 92
    invoke-direct {p0, v1}, Lcom/google/cast/e;->c(I)V

    .line 93
    sub-int v2, p3, v1

    .line 94
    if-lez v2, :cond_1

    .line 95
    add-int/2addr v1, p2

    .line 96
    iget-object v3, p0, Lcom/google/cast/e;->a:[B

    iget v4, p0, Lcom/google/cast/e;->b:I

    invoke-static {v3, v4, p1, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 97
    invoke-direct {p0, v2}, Lcom/google/cast/e;->c(I)V

    .line 100
    :cond_1
    if-eqz p4, :cond_2

    .line 101
    :goto_1
    if-ge v0, p3, :cond_2

    .line 102
    aget-byte v1, p1, v0

    array-length v2, p4

    rem-int v2, v0, v2

    aget-byte v2, p4, v2

    xor-int/2addr v1, v2

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    .line 101
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 105
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public a([B[B)Z
    .locals 2

    .prologue
    .line 71
    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1, p2}, Lcom/google/cast/e;->a([BII[B)Z

    move-result v0

    return v0
.end method

.method public b(Ljava/nio/channels/SocketChannel;)I
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 422
    invoke-virtual {p0}, Lcom/google/cast/e;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 442
    :goto_0
    return v0

    .line 428
    :cond_0
    iget v0, p0, Lcom/google/cast/e;->c:I

    iget v2, p0, Lcom/google/cast/e;->b:I

    if-ge v0, v2, :cond_1

    .line 430
    iget-object v0, p0, Lcom/google/cast/e;->f:[Ljava/nio/ByteBuffer;

    .line 431
    iget-object v2, p0, Lcom/google/cast/e;->a:[B

    iget v3, p0, Lcom/google/cast/e;->c:I

    iget v4, p0, Lcom/google/cast/e;->b:I

    iget v5, p0, Lcom/google/cast/e;->c:I

    sub-int/2addr v4, v5

    invoke-static {v2, v3, v4}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 439
    :goto_1
    invoke-virtual {p1, v0}, Ljava/nio/channels/SocketChannel;->read([Ljava/nio/ByteBuffer;)J

    move-result-wide v0

    long-to-int v0, v0

    .line 440
    if-lez v0, :cond_2

    .line 441
    invoke-direct {p0, v0}, Lcom/google/cast/e;->d(I)V

    goto :goto_0

    .line 434
    :cond_1
    iget-object v0, p0, Lcom/google/cast/e;->g:[Ljava/nio/ByteBuffer;

    .line 435
    iget-object v2, p0, Lcom/google/cast/e;->a:[B

    iget v3, p0, Lcom/google/cast/e;->c:I

    iget-object v4, p0, Lcom/google/cast/e;->a:[B

    array-length v4, v4

    iget v5, p0, Lcom/google/cast/e;->c:I

    sub-int/2addr v4, v5

    invoke-static {v2, v3, v4}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 436
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/cast/e;->a:[B

    iget v4, p0, Lcom/google/cast/e;->b:I

    invoke-static {v3, v1, v4}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v1

    aput-object v1, v0, v2

    goto :goto_1

    .line 447
    :cond_2
    new-instance v0, Ljava/nio/channels/ClosedChannelException;

    invoke-direct {v0}, Ljava/nio/channels/ClosedChannelException;-><init>()V

    throw v0
.end method

.method public b()Ljava/lang/Integer;
    .locals 2

    .prologue
    .line 128
    invoke-virtual {p0}, Lcom/google/cast/e;->i()I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    .line 129
    const/4 v0, 0x0

    .line 131
    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0}, Lcom/google/cast/e;->n()B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x8

    invoke-direct {p0}, Lcom/google/cast/e;->n()B

    move-result v1

    and-int/lit16 v1, v1, 0xff

    or-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method

.method public b(I)Z
    .locals 2

    .prologue
    .line 369
    invoke-virtual {p0}, Lcom/google/cast/e;->h()I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    .line 370
    const/4 v0, 0x0

    .line 374
    :goto_0
    return v0

    .line 372
    :cond_0
    shr-int/lit8 v0, p1, 0x8

    and-int/lit16 v0, v0, 0xff

    int-to-byte v0, v0

    invoke-direct {p0, v0}, Lcom/google/cast/e;->b(B)V

    .line 373
    and-int/lit16 v0, p1, 0xff

    int-to-byte v0, v0

    invoke-direct {p0, v0}, Lcom/google/cast/e;->b(B)V

    .line 374
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public b([BII[B)Z
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 315
    invoke-virtual {p0}, Lcom/google/cast/e;->h()I

    move-result v1

    if-ge v1, p3, :cond_0

    .line 343
    :goto_0
    return v0

    .line 319
    :cond_0
    invoke-direct {p0}, Lcom/google/cast/e;->m()I

    move-result v1

    invoke-static {p3, v1}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 321
    iget-object v1, p0, Lcom/google/cast/e;->a:[B

    iget v3, p0, Lcom/google/cast/e;->c:I

    invoke-static {p1, p2, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 322
    if-eqz p4, :cond_1

    .line 323
    iget v1, p0, Lcom/google/cast/e;->c:I

    .line 324
    :goto_1
    if-ge v0, v2, :cond_1

    .line 325
    iget-object v3, p0, Lcom/google/cast/e;->a:[B

    aget-byte v4, v3, v1

    array-length v5, p4

    rem-int v5, v0, v5

    aget-byte v5, p4, v5

    xor-int/2addr v4, v5

    int-to-byte v4, v4

    aput-byte v4, v3, v1

    .line 326
    add-int/lit8 v1, v1, 0x1

    .line 324
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 329
    :cond_1
    invoke-direct {p0, v2}, Lcom/google/cast/e;->d(I)V

    .line 330
    add-int v1, p2, v2

    .line 331
    sub-int v2, p3, v2

    .line 332
    if-lez v2, :cond_3

    .line 333
    iget-object v3, p0, Lcom/google/cast/e;->a:[B

    iget v4, p0, Lcom/google/cast/e;->c:I

    invoke-static {p1, v1, v3, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 334
    if-eqz p4, :cond_2

    .line 335
    iget v1, p0, Lcom/google/cast/e;->c:I

    move v6, v1

    move v1, v0

    move v0, v6

    .line 336
    :goto_2
    if-ge v1, p3, :cond_2

    .line 337
    iget-object v3, p0, Lcom/google/cast/e;->a:[B

    aget-byte v4, v3, v0

    array-length v5, p4

    rem-int v5, v1, v5

    aget-byte v5, p4, v5

    xor-int/2addr v4, v5

    int-to-byte v4, v4

    aput-byte v4, v3, v0

    .line 338
    add-int/lit8 v0, v0, 0x1

    .line 336
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 341
    :cond_2
    invoke-direct {p0, v2}, Lcom/google/cast/e;->d(I)V

    .line 343
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public b([B[B)Z
    .locals 2

    .prologue
    .line 301
    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1, p2}, Lcom/google/cast/e;->b([BII[B)Z

    move-result v0

    return v0
.end method

.method public c()Ljava/lang/Long;
    .locals 6

    .prologue
    const/16 v5, 0x8

    .line 142
    invoke-virtual {p0}, Lcom/google/cast/e;->i()I

    move-result v0

    if-ge v0, v5, :cond_0

    .line 143
    const/4 v0, 0x0

    .line 145
    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0}, Lcom/google/cast/e;->n()B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    int-to-long v0, v0

    const/16 v2, 0x38

    shl-long/2addr v0, v2

    invoke-direct {p0}, Lcom/google/cast/e;->n()B

    move-result v2

    and-int/lit16 v2, v2, 0xff

    int-to-long v2, v2

    const/16 v4, 0x30

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    invoke-direct {p0}, Lcom/google/cast/e;->n()B

    move-result v2

    and-int/lit16 v2, v2, 0xff

    int-to-long v2, v2

    const/16 v4, 0x28

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    invoke-direct {p0}, Lcom/google/cast/e;->n()B

    move-result v2

    and-int/lit16 v2, v2, 0xff

    int-to-long v2, v2

    const/16 v4, 0x20

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    invoke-direct {p0}, Lcom/google/cast/e;->n()B

    move-result v2

    and-int/lit16 v2, v2, 0xff

    int-to-long v2, v2

    const/16 v4, 0x18

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    invoke-direct {p0}, Lcom/google/cast/e;->n()B

    move-result v2

    and-int/lit16 v2, v2, 0xff

    int-to-long v2, v2

    const/16 v4, 0x10

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    invoke-direct {p0}, Lcom/google/cast/e;->n()B

    move-result v2

    and-int/lit16 v2, v2, 0xff

    int-to-long v2, v2

    shl-long/2addr v2, v5

    or-long/2addr v0, v2

    invoke-direct {p0}, Lcom/google/cast/e;->n()B

    move-result v2

    and-int/lit16 v2, v2, 0xff

    int-to-long v2, v2

    or-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0
.end method

.method public d()V
    .locals 1

    .prologue
    .line 455
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/cast/e;->c:I

    iput v0, p0, Lcom/google/cast/e;->b:I

    .line 456
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/cast/e;->d:I

    .line 457
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/cast/e;->e:Z

    .line 458
    return-void
.end method

.method public e()V
    .locals 1

    .prologue
    .line 466
    iget v0, p0, Lcom/google/cast/e;->b:I

    iput v0, p0, Lcom/google/cast/e;->d:I

    .line 467
    return-void
.end method

.method public f()V
    .locals 1

    .prologue
    .line 473
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/cast/e;->d:I

    .line 474
    iget-boolean v0, p0, Lcom/google/cast/e;->e:Z

    if-eqz v0, :cond_0

    .line 475
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/cast/e;->c:I

    iput v0, p0, Lcom/google/cast/e;->b:I

    .line 477
    :cond_0
    return-void
.end method

.method public g()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 484
    iget v0, p0, Lcom/google/cast/e;->d:I

    if-eq v0, v2, :cond_1

    .line 485
    iget v0, p0, Lcom/google/cast/e;->b:I

    iget v1, p0, Lcom/google/cast/e;->d:I

    if-eq v0, v1, :cond_0

    .line 486
    iget v0, p0, Lcom/google/cast/e;->d:I

    iput v0, p0, Lcom/google/cast/e;->b:I

    .line 487
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/cast/e;->e:Z

    .line 489
    :cond_0
    iput v2, p0, Lcom/google/cast/e;->d:I

    .line 491
    :cond_1
    return-void
.end method

.method public h()I
    .locals 2

    .prologue
    .line 497
    iget-boolean v0, p0, Lcom/google/cast/e;->e:Z

    if-eqz v0, :cond_0

    .line 498
    iget-object v0, p0, Lcom/google/cast/e;->a:[B

    array-length v0, v0

    .line 502
    :goto_0
    return v0

    .line 499
    :cond_0
    iget v0, p0, Lcom/google/cast/e;->c:I

    iget v1, p0, Lcom/google/cast/e;->b:I

    if-ge v0, v1, :cond_1

    .line 500
    iget v0, p0, Lcom/google/cast/e;->b:I

    iget v1, p0, Lcom/google/cast/e;->c:I

    sub-int/2addr v0, v1

    goto :goto_0

    .line 502
    :cond_1
    iget-object v0, p0, Lcom/google/cast/e;->a:[B

    array-length v0, v0

    iget v1, p0, Lcom/google/cast/e;->c:I

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/google/cast/e;->b:I

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method public i()I
    .locals 2

    .prologue
    .line 510
    iget-boolean v0, p0, Lcom/google/cast/e;->e:Z

    if-eqz v0, :cond_0

    .line 511
    const/4 v0, 0x0

    .line 515
    :goto_0
    return v0

    .line 512
    :cond_0
    iget v0, p0, Lcom/google/cast/e;->b:I

    iget v1, p0, Lcom/google/cast/e;->c:I

    if-ge v0, v1, :cond_1

    .line 513
    iget v0, p0, Lcom/google/cast/e;->c:I

    iget v1, p0, Lcom/google/cast/e;->b:I

    sub-int/2addr v0, v1

    goto :goto_0

    .line 515
    :cond_1
    iget-object v0, p0, Lcom/google/cast/e;->a:[B

    array-length v0, v0

    iget v1, p0, Lcom/google/cast/e;->b:I

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/google/cast/e;->c:I

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method public j()Z
    .locals 1

    .prologue
    .line 523
    iget-boolean v0, p0, Lcom/google/cast/e;->e:Z

    return v0
.end method

.method public k()Z
    .locals 2

    .prologue
    .line 530
    iget-boolean v0, p0, Lcom/google/cast/e;->e:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/cast/e;->b:I

    iget v1, p0, Lcom/google/cast/e;->c:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
