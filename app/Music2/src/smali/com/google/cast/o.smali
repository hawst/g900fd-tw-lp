.class Lcom/google/cast/o;
.super Lcom/google/cast/NetworkRequest;
.source "SourceFile"


# instance fields
.field private a:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Lcom/google/cast/CastContext;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lcom/google/cast/NetworkRequest;-><init>(Lcom/google/cast/CastContext;)V

    .line 22
    iput-object p2, p0, Lcom/google/cast/o;->a:Landroid/net/Uri;

    .line 23
    return-void
.end method


# virtual methods
.method public final execute()I
    .locals 3

    .prologue
    const/4 v0, -0x1

    .line 28
    :try_start_0
    iget-object v1, p0, Lcom/google/cast/o;->a:Landroid/net/Uri;

    sget v2, Lcom/google/cast/o;->DEFAULT_TIMEOUT:I

    invoke-virtual {p0, v1, v2}, Lcom/google/cast/o;->performHttpDelete(Landroid/net/Uri;I)Lcom/google/cast/SimpleHttpRequest;

    move-result-object v1

    .line 30
    invoke-interface {v1}, Lcom/google/cast/SimpleHttpRequest;->getResponseStatus()I
    :try_end_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    .line 31
    const/16 v2, 0xc8

    if-ne v1, v2, :cond_1

    .line 32
    const/4 v0, 0x0

    .line 43
    :cond_0
    :goto_0
    return v0

    .line 33
    :cond_1
    const/16 v2, 0x194

    if-ne v1, v2, :cond_2

    .line 34
    const/4 v0, -0x4

    goto :goto_0

    .line 35
    :cond_2
    const/16 v2, 0x1f5

    if-ne v1, v2, :cond_0

    .line 36
    const/4 v0, -0x7

    goto :goto_0

    .line 40
    :catch_0
    move-exception v0

    .line 41
    const/4 v0, -0x2

    goto :goto_0

    .line 42
    :catch_1
    move-exception v1

    goto :goto_0
.end method
