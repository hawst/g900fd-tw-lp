.class final Lcom/google/cast/c;
.super Lcom/google/cast/b;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/cast/c$1;,
        Lcom/google/cast/c$b;,
        Lcom/google/cast/c$a;
    }
.end annotation


# static fields
.field private static final b:Lcom/google/cast/MimeData;

.field private static final c:[Ljava/lang/String;

.field private static final d:[Ljava/lang/String;

.field private static final e:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final f:Lcom/google/cast/Logger;


# instance fields
.field private g:Ljava/lang/String;

.field private h:Lcom/google/cast/MimeData;

.field private i:I

.field private j:Z


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 41
    const/4 v0, 0x0

    sput-object v0, Lcom/google/cast/c;->b:Lcom/google/cast/MimeData;

    .line 47
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "audio/*"

    aput-object v1, v0, v2

    const-string v1, "image/*"

    aput-object v1, v0, v3

    const-string v1, "video/*"

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/cast/c;->c:[Ljava/lang/String;

    .line 66
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "android.media.intent.action.PAUSE"

    aput-object v1, v0, v2

    const-string v1, "android.media.intent.action.RESUME"

    aput-object v1, v0, v3

    const-string v1, "android.media.intent.action.STOP"

    aput-object v1, v0, v4

    const-string v1, "android.media.intent.action.SEEK"

    aput-object v1, v0, v5

    const/4 v1, 0x4

    const-string v2, "android.media.intent.action.GET_STATUS"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/cast/c;->d:[Ljava/lang/String;

    .line 74
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/google/cast/c;->e:Ljava/util/Set;

    .line 77
    sget-object v0, Lcom/google/cast/c;->e:Ljava/util/Set;

    const-string v1, "android.media.intent.extra.ITEM_ID"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 78
    sget-object v0, Lcom/google/cast/c;->e:Ljava/util/Set;

    const-string v1, "android.media.intent.extra.ITEM_STATUS"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 79
    sget-object v0, Lcom/google/cast/c;->e:Ljava/util/Set;

    const-string v1, "android.media.intent.extra.ITEM_POSITION"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 80
    sget-object v0, Lcom/google/cast/c;->e:Ljava/util/Set;

    const-string v1, "android.media.intent.extra.ITEM_METADATA"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 81
    sget-object v0, Lcom/google/cast/c;->e:Ljava/util/Set;

    const-string v1, "android.media.intent.extra.ITEM_STATUS_UPDATE_RECEIVER"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 84
    new-instance v0, Lcom/google/cast/Logger;

    const-string v1, "CastMediaRouteProvider"

    invoke-direct {v0, v1}, Lcom/google/cast/Logger;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/cast/c;->f:Lcom/google/cast/Logger;

    return-void
.end method

.method public constructor <init>(Lcom/google/cast/CastContext;Ljava/lang/String;Lcom/google/cast/MimeData;IZ)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 108
    invoke-direct {p0, p1}, Lcom/google/cast/b;-><init>(Lcom/google/cast/CastContext;)V

    .line 86
    const-string v0, "GoogleCastPlayer"

    iput-object v0, p0, Lcom/google/cast/c;->g:Ljava/lang/String;

    .line 87
    sget-object v0, Lcom/google/cast/c;->b:Lcom/google/cast/MimeData;

    iput-object v0, p0, Lcom/google/cast/c;->h:Lcom/google/cast/MimeData;

    .line 110
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 111
    iput-object p2, p0, Lcom/google/cast/c;->g:Ljava/lang/String;

    .line 112
    iput-object p3, p0, Lcom/google/cast/c;->h:Lcom/google/cast/MimeData;

    .line 114
    :cond_0
    iput p4, p0, Lcom/google/cast/c;->i:I

    .line 115
    iput-boolean p5, p0, Lcom/google/cast/c;->j:Z

    .line 117
    and-int/lit8 v0, p4, 0x4

    const/4 v2, 0x4

    if-ne v0, v2, :cond_1

    const/4 v0, 0x1

    .line 119
    :goto_0
    invoke-static {}, Lcom/google/cast/r;->a()Lcom/google/cast/r;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/cast/CastContext;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, Lcom/google/cast/r;->a(Landroid/content/Context;Z)V

    .line 124
    new-instance v2, Landroid/content/IntentFilter;

    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    .line 125
    const-string v0, "android.media.intent.category.REMOTE_PLAYBACK"

    invoke-virtual {v2, v0}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    .line 126
    const-string v0, "android.media.intent.action.PLAY"

    invoke-virtual {v2, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 127
    const-string v0, "http"

    invoke-virtual {v2, v0}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 128
    const-string v0, "https"

    invoke-virtual {v2, v0}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 129
    sget-object v3, Lcom/google/cast/c;->c:[Ljava/lang/String;

    array-length v4, v3

    move v0, v1

    :goto_1
    if-ge v0, v4, :cond_2

    aget-object v5, v3, v0

    .line 130
    invoke-static {v2, v5}, Lcom/google/cast/c;->a(Landroid/content/IntentFilter;Ljava/lang/String;)V

    .line 129
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    move v0, v1

    .line 117
    goto :goto_0

    .line 132
    :cond_2
    invoke-virtual {p0, v2}, Lcom/google/cast/c;->a(Landroid/content/IntentFilter;)V

    .line 134
    sget-object v0, Lcom/google/cast/c;->d:[Ljava/lang/String;

    array-length v2, v0

    :goto_2
    if-ge v1, v2, :cond_3

    aget-object v3, v0, v1

    .line 135
    new-instance v4, Landroid/content/IntentFilter;

    invoke-direct {v4}, Landroid/content/IntentFilter;-><init>()V

    .line 136
    const-string v5, "android.media.intent.category.REMOTE_PLAYBACK"

    invoke-virtual {v4, v5}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    .line 137
    invoke-virtual {v4, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 138
    invoke-virtual {p0, v4}, Lcom/google/cast/c;->a(Landroid/content/IntentFilter;)V

    .line 134
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 141
    :cond_3
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 142
    const-string v1, "com.google.cast.CATEGORY_REMOTE_PLAYBACK_CAST_EXTENSIONS"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    .line 143
    const-string v1, "com.google.cast.ACTION_SYNC_STATUS"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 144
    invoke-virtual {p0, v0}, Lcom/google/cast/c;->a(Landroid/content/IntentFilter;)V

    .line 145
    return-void
.end method

.method private a(I)Landroid/os/Bundle;
    .locals 2

    .prologue
    .line 920
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 921
    const-string v1, "com.google.cast.EXTRA_ERROR_CODE"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 922
    return-object v0
.end method

.method static synthetic a(Lcom/google/cast/c;I)Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/google/cast/c;->a(I)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/cast/c;Lorg/json/JSONObject;)Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/google/cast/c;->a(Lorg/json/JSONObject;)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method private a(Lorg/json/JSONObject;)Landroid/os/Bundle;
    .locals 6

    .prologue
    .line 929
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 932
    invoke-virtual {p1}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v3

    .line 933
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 935
    :try_start_0
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 936
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 938
    sget-object v4, Lorg/json/JSONObject;->NULL:Ljava/lang/Object;

    if-ne v1, v4, :cond_1

    .line 939
    const/4 v1, 0x0

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto :goto_0

    .line 955
    :catch_0
    move-exception v0

    goto :goto_0

    .line 940
    :cond_1
    instance-of v4, v1, Ljava/lang/String;

    if-eqz v4, :cond_2

    .line 941
    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 942
    :cond_2
    instance-of v4, v1, Ljava/lang/Boolean;

    if-eqz v4, :cond_3

    .line 943
    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_0

    .line 944
    :cond_3
    instance-of v4, v1, Ljava/lang/Integer;

    if-eqz v4, :cond_4

    .line 945
    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_0

    .line 946
    :cond_4
    instance-of v4, v1, Ljava/lang/Long;

    if-eqz v4, :cond_5

    .line 947
    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v2, v0, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    goto :goto_0

    .line 948
    :cond_5
    instance-of v4, v1, Ljava/lang/Double;

    if-eqz v4, :cond_6

    .line 949
    check-cast v1, Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    invoke-virtual {v2, v0, v4, v5}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V

    goto :goto_0

    .line 950
    :cond_6
    instance-of v4, v1, Lorg/json/JSONObject;

    if-eqz v4, :cond_0

    .line 951
    check-cast v1, Lorg/json/JSONObject;

    invoke-direct {p0, v1}, Lcom/google/cast/c;->a(Lorg/json/JSONObject;)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 957
    :cond_7
    return-object v2
.end method

.method private a(Landroid/os/Bundle;Ljava/util/Set;)Lorg/json/JSONObject;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lorg/json/JSONObject;"
        }
    .end annotation

    .prologue
    .line 964
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 966
    invoke-virtual {p1}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 967
    invoke-interface {p2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 971
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 974
    :try_start_0
    instance-of v4, v1, Landroid/os/Bundle;

    if-eqz v4, :cond_1

    .line 975
    check-cast v1, Landroid/os/Bundle;

    invoke-direct {p0, v1, p2}, Lcom/google/cast/c;->a(Landroid/os/Bundle;Ljava/util/Set;)Lorg/json/JSONObject;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_0

    .line 979
    :catch_0
    move-exception v0

    goto :goto_0

    .line 977
    :cond_1
    invoke-virtual {v2, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 982
    :cond_2
    return-object v2
.end method

.method static synthetic a(Lcom/google/cast/c;Landroid/os/Bundle;Ljava/util/Set;)Lorg/json/JSONObject;
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Lcom/google/cast/c;->a(Landroid/os/Bundle;Ljava/util/Set;)Lorg/json/JSONObject;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/cast/c;)Z
    .locals 1

    .prologue
    .line 38
    iget-boolean v0, p0, Lcom/google/cast/c;->j:Z

    return v0
.end method

.method static synthetic b(Lcom/google/cast/c;)I
    .locals 1

    .prologue
    .line 38
    iget v0, p0, Lcom/google/cast/c;->i:I

    return v0
.end method

.method static synthetic b()Lcom/google/cast/Logger;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lcom/google/cast/c;->f:Lcom/google/cast/Logger;

    return-object v0
.end method

.method static synthetic c(Lcom/google/cast/c;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/cast/c;->g:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c()Ljava/util/Set;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lcom/google/cast/c;->e:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic d(Lcom/google/cast/c;)Lcom/google/cast/MimeData;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/cast/c;->h:Lcom/google/cast/MimeData;

    return-object v0
.end method


# virtual methods
.method protected a(Lcom/google/cast/CastDevice;Ljava/lang/String;)Lcom/google/cast/b$a;
    .locals 1

    .prologue
    .line 149
    new-instance v0, Lcom/google/cast/c$a;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/cast/c$a;-><init>(Lcom/google/cast/c;Lcom/google/cast/CastDevice;Ljava/lang/String;)V

    return-object v0
.end method
