.class Lcom/google/cast/ApplicationSession$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/cast/NetworkTask$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/cast/ApplicationSession;-><init>(Lcom/google/cast/CastContext;Lcom/google/cast/CastDevice;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/google/cast/ApplicationSession;


# direct methods
.method constructor <init>(Lcom/google/cast/ApplicationSession;)V
    .locals 0

    .prologue
    .line 169
    iput-object p1, p0, Lcom/google/cast/ApplicationSession$1;->a:Lcom/google/cast/ApplicationSession;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTaskCancelled()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 240
    iget-object v0, p0, Lcom/google/cast/ApplicationSession$1;->a:Lcom/google/cast/ApplicationSession;

    invoke-static {v0}, Lcom/google/cast/ApplicationSession;->b(Lcom/google/cast/ApplicationSession;)Lcom/google/cast/Logger;

    move-result-object v0

    const-string v1, "StartSessionTask cancelled"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 241
    iget-object v0, p0, Lcom/google/cast/ApplicationSession$1;->a:Lcom/google/cast/ApplicationSession;

    invoke-static {v0, v3}, Lcom/google/cast/ApplicationSession;->a(Lcom/google/cast/ApplicationSession;Lcom/google/cast/n;)Lcom/google/cast/n;

    .line 242
    iget-object v0, p0, Lcom/google/cast/ApplicationSession$1;->a:Lcom/google/cast/ApplicationSession;

    invoke-static {v0, v3}, Lcom/google/cast/ApplicationSession;->a(Lcom/google/cast/ApplicationSession;Lcom/google/cast/SessionError;)V

    .line 243
    return-void
.end method

.method public onTaskCompleted()V
    .locals 11

    .prologue
    const/4 v9, 0x1

    const/4 v6, 0x0

    const/4 v0, 0x0

    .line 172
    iget-object v1, p0, Lcom/google/cast/ApplicationSession$1;->a:Lcom/google/cast/ApplicationSession;

    invoke-static {v1}, Lcom/google/cast/ApplicationSession;->a(Lcom/google/cast/ApplicationSession;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 173
    iget-object v0, p0, Lcom/google/cast/ApplicationSession$1;->a:Lcom/google/cast/ApplicationSession;

    invoke-static {v0, v6}, Lcom/google/cast/ApplicationSession;->a(Lcom/google/cast/ApplicationSession;Lcom/google/cast/n;)Lcom/google/cast/n;

    .line 174
    iget-object v0, p0, Lcom/google/cast/ApplicationSession$1;->a:Lcom/google/cast/ApplicationSession;

    invoke-static {v0, v6}, Lcom/google/cast/ApplicationSession;->a(Lcom/google/cast/ApplicationSession;Lcom/google/cast/SessionError;)V

    .line 207
    :cond_0
    :goto_0
    return-void

    .line 178
    :cond_1
    iget-object v1, p0, Lcom/google/cast/ApplicationSession$1;->a:Lcom/google/cast/ApplicationSession;

    invoke-static {v1}, Lcom/google/cast/ApplicationSession;->b(Lcom/google/cast/ApplicationSession;)Lcom/google/cast/Logger;

    move-result-object v1

    const-string v2, "StartSessionTask completed"

    new-array v3, v0, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 179
    iget-object v1, p0, Lcom/google/cast/ApplicationSession$1;->a:Lcom/google/cast/ApplicationSession;

    invoke-static {v1}, Lcom/google/cast/ApplicationSession;->c(Lcom/google/cast/ApplicationSession;)Lcom/google/cast/n;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/cast/n;->c()J

    move-result-wide v4

    .line 180
    iget-object v1, p0, Lcom/google/cast/ApplicationSession$1;->a:Lcom/google/cast/ApplicationSession;

    invoke-static {v1}, Lcom/google/cast/ApplicationSession;->c(Lcom/google/cast/ApplicationSession;)Lcom/google/cast/n;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/cast/n;->b()Landroid/net/Uri;

    move-result-object v10

    .line 182
    iget-object v1, p0, Lcom/google/cast/ApplicationSession$1;->a:Lcom/google/cast/ApplicationSession;

    iget-object v2, p0, Lcom/google/cast/ApplicationSession$1;->a:Lcom/google/cast/ApplicationSession;

    invoke-static {v2}, Lcom/google/cast/ApplicationSession;->c(Lcom/google/cast/ApplicationSession;)Lcom/google/cast/n;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/cast/n;->e()Lcom/google/cast/ApplicationMetadata;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/cast/ApplicationSession;->a(Lcom/google/cast/ApplicationSession;Lcom/google/cast/ApplicationMetadata;)Lcom/google/cast/ApplicationMetadata;

    .line 185
    iget-object v1, p0, Lcom/google/cast/ApplicationSession$1;->a:Lcom/google/cast/ApplicationSession;

    iget-object v2, p0, Lcom/google/cast/ApplicationSession$1;->a:Lcom/google/cast/ApplicationSession;

    invoke-static {v2}, Lcom/google/cast/ApplicationSession;->c(Lcom/google/cast/ApplicationSession;)Lcom/google/cast/n;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/cast/n;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/cast/ApplicationSession;->a(Lcom/google/cast/ApplicationSession;Ljava/lang/String;)Ljava/lang/String;

    .line 186
    iget-object v1, p0, Lcom/google/cast/ApplicationSession$1;->a:Lcom/google/cast/ApplicationSession;

    iget-object v2, p0, Lcom/google/cast/ApplicationSession$1;->a:Lcom/google/cast/ApplicationSession;

    invoke-static {v2}, Lcom/google/cast/ApplicationSession;->c(Lcom/google/cast/ApplicationSession;)Lcom/google/cast/n;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/cast/n;->d()Landroid/net/Uri;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/cast/ApplicationSession;->a(Lcom/google/cast/ApplicationSession;Landroid/net/Uri;)Landroid/net/Uri;

    .line 188
    iget-object v1, p0, Lcom/google/cast/ApplicationSession$1;->a:Lcom/google/cast/ApplicationSession;

    invoke-static {v1, v6}, Lcom/google/cast/ApplicationSession;->a(Lcom/google/cast/ApplicationSession;Lcom/google/cast/n;)Lcom/google/cast/n;

    .line 190
    if-eqz v10, :cond_4

    .line 191
    iget-object v1, p0, Lcom/google/cast/ApplicationSession$1;->a:Lcom/google/cast/ApplicationSession;

    invoke-static {v1}, Lcom/google/cast/ApplicationSession;->b(Lcom/google/cast/ApplicationSession;)Lcom/google/cast/Logger;

    move-result-object v1

    const-string v2, "connecting channel at URL: %s"

    new-array v3, v9, [Ljava/lang/Object;

    aput-object v10, v3, v0

    invoke-virtual {v1, v2, v3}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 192
    iget-object v1, p0, Lcom/google/cast/ApplicationSession$1;->a:Lcom/google/cast/ApplicationSession;

    invoke-static {v1}, Lcom/google/cast/ApplicationSession;->d(Lcom/google/cast/ApplicationSession;)I

    move-result v1

    and-int/lit8 v1, v1, 0x4

    const/4 v2, 0x4

    if-ne v1, v2, :cond_2

    .line 194
    :goto_1
    iget-object v0, p0, Lcom/google/cast/ApplicationSession$1;->a:Lcom/google/cast/ApplicationSession;

    new-instance v1, Lcom/google/cast/ApplicationChannel;

    iget-object v2, p0, Lcom/google/cast/ApplicationSession$1;->a:Lcom/google/cast/ApplicationSession;

    invoke-static {v2}, Lcom/google/cast/ApplicationSession;->e(Lcom/google/cast/ApplicationSession;)Lcom/google/cast/CastContext;

    move-result-object v2

    iget-object v3, p0, Lcom/google/cast/ApplicationSession$1;->a:Lcom/google/cast/ApplicationSession;

    invoke-static {v3}, Lcom/google/cast/ApplicationSession;->f(Lcom/google/cast/ApplicationSession;)I

    move-result v3

    iget-object v6, p0, Lcom/google/cast/ApplicationSession$1;->a:Lcom/google/cast/ApplicationSession;

    invoke-static {v6}, Lcom/google/cast/ApplicationSession;->g(Lcom/google/cast/ApplicationSession;)Lcom/google/cast/ApplicationChannel$a;

    move-result-object v6

    iget-object v7, p0, Lcom/google/cast/ApplicationSession$1;->a:Lcom/google/cast/ApplicationSession;

    invoke-static {v7}, Lcom/google/cast/ApplicationSession;->h(Lcom/google/cast/ApplicationSession;)Landroid/os/Handler;

    move-result-object v7

    iget-object v8, p0, Lcom/google/cast/ApplicationSession$1;->a:Lcom/google/cast/ApplicationSession;

    invoke-static {v8}, Lcom/google/cast/ApplicationSession;->i(Lcom/google/cast/ApplicationSession;)Lcom/google/cast/CastDevice;

    move-result-object v8

    invoke-direct/range {v1 .. v9}, Lcom/google/cast/ApplicationChannel;-><init>(Lcom/google/cast/CastContext;IJLcom/google/cast/ApplicationChannel$a;Landroid/os/Handler;Lcom/google/cast/CastDevice;Z)V

    invoke-static {v0, v1}, Lcom/google/cast/ApplicationSession;->a(Lcom/google/cast/ApplicationSession;Lcom/google/cast/ApplicationChannel;)Lcom/google/cast/ApplicationChannel;

    .line 196
    iget-object v0, p0, Lcom/google/cast/ApplicationSession$1;->a:Lcom/google/cast/ApplicationSession;

    invoke-static {v0, v10}, Lcom/google/cast/ApplicationSession;->b(Lcom/google/cast/ApplicationSession;Landroid/net/Uri;)V

    .line 197
    iget-object v0, p0, Lcom/google/cast/ApplicationSession$1;->a:Lcom/google/cast/ApplicationSession;

    invoke-static {v0}, Lcom/google/cast/ApplicationSession;->j(Lcom/google/cast/ApplicationSession;)Lcom/google/cast/ApplicationMetadata;

    move-result-object v0

    const-string v1, "ramp"

    invoke-virtual {v0, v1}, Lcom/google/cast/ApplicationMetadata;->isProtocolSupported(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 199
    iget-object v0, p0, Lcom/google/cast/ApplicationSession$1;->a:Lcom/google/cast/ApplicationSession;

    invoke-static {v0}, Lcom/google/cast/ApplicationSession;->e(Lcom/google/cast/ApplicationSession;)Lcom/google/cast/CastContext;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/cast/CastContext;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v0, p0, Lcom/google/cast/ApplicationSession$1;->a:Lcom/google/cast/ApplicationSession;

    invoke-static {v0}, Lcom/google/cast/ApplicationSession;->k(Lcom/google/cast/ApplicationSession;)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/google/cast/ApplicationSession$1;->a:Lcom/google/cast/ApplicationSession;

    invoke-static {v0}, Lcom/google/cast/ApplicationSession;->l(Lcom/google/cast/ApplicationSession;)Landroid/net/Uri;

    move-result-object v0

    if-nez v0, :cond_3

    const-string v0, ""

    :goto_2
    iget-object v3, p0, Lcom/google/cast/ApplicationSession$1;->a:Lcom/google/cast/ApplicationSession;

    invoke-static {v3}, Lcom/google/cast/ApplicationSession;->d(Lcom/google/cast/ApplicationSession;)I

    move-result v3

    invoke-static {v1, v2, v0, v3}, Lcom/google/cast/d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    goto/16 :goto_0

    :cond_2
    move v9, v0

    .line 192
    goto :goto_1

    .line 199
    :cond_3
    iget-object v0, p0, Lcom/google/cast/ApplicationSession$1;->a:Lcom/google/cast/ApplicationSession;

    invoke-static {v0}, Lcom/google/cast/ApplicationSession;->l(Lcom/google/cast/ApplicationSession;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 205
    :cond_4
    iget-object v0, p0, Lcom/google/cast/ApplicationSession$1;->a:Lcom/google/cast/ApplicationSession;

    invoke-static {v0}, Lcom/google/cast/ApplicationSession;->m(Lcom/google/cast/ApplicationSession;)V

    goto/16 :goto_0
.end method

.method public onTaskFailed(I)V
    .locals 8
    .param p1, "status"    # I

    .prologue
    const/4 v0, 0x2

    const/4 v7, 0x0

    const/4 v6, 0x0

    const/4 v1, 0x1

    .line 211
    iget-object v2, p0, Lcom/google/cast/ApplicationSession$1;->a:Lcom/google/cast/ApplicationSession;

    invoke-static {v2}, Lcom/google/cast/ApplicationSession;->a(Lcom/google/cast/ApplicationSession;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 212
    iget-object v0, p0, Lcom/google/cast/ApplicationSession$1;->a:Lcom/google/cast/ApplicationSession;

    invoke-static {v0, v6}, Lcom/google/cast/ApplicationSession;->a(Lcom/google/cast/ApplicationSession;Lcom/google/cast/n;)Lcom/google/cast/n;

    .line 213
    iget-object v0, p0, Lcom/google/cast/ApplicationSession$1;->a:Lcom/google/cast/ApplicationSession;

    invoke-static {v0, v6}, Lcom/google/cast/ApplicationSession;->a(Lcom/google/cast/ApplicationSession;Lcom/google/cast/SessionError;)V

    .line 236
    :goto_0
    return-void

    .line 217
    :cond_0
    iget-object v2, p0, Lcom/google/cast/ApplicationSession$1;->a:Lcom/google/cast/ApplicationSession;

    invoke-static {v2}, Lcom/google/cast/ApplicationSession;->b(Lcom/google/cast/ApplicationSession;)Lcom/google/cast/Logger;

    move-result-object v2

    const-string v3, "StartSessionTask failed; status=%d"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {v2, v3, v4}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 218
    iget-object v2, p0, Lcom/google/cast/ApplicationSession$1;->a:Lcom/google/cast/ApplicationSession;

    invoke-static {v2, v6}, Lcom/google/cast/ApplicationSession;->a(Lcom/google/cast/ApplicationSession;Lcom/google/cast/n;)Lcom/google/cast/n;

    .line 221
    const/4 v2, -0x2

    if-ne p1, v2, :cond_2

    move v0, v1

    .line 232
    :cond_1
    :goto_1
    new-instance v2, Lcom/google/cast/SessionError;

    invoke-direct {v2, v1, v0}, Lcom/google/cast/SessionError;-><init>(II)V

    .line 234
    iget-object v0, p0, Lcom/google/cast/ApplicationSession$1;->a:Lcom/google/cast/ApplicationSession;

    invoke-static {v0}, Lcom/google/cast/ApplicationSession;->b(Lcom/google/cast/ApplicationSession;)Lcom/google/cast/Logger;

    move-result-object v0

    const-string v3, "StartSessionTask failed with error: %s"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v2}, Lcom/google/cast/SessionError;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v7

    invoke-virtual {v0, v3, v1}, Lcom/google/cast/Logger;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 235
    iget-object v0, p0, Lcom/google/cast/ApplicationSession$1;->a:Lcom/google/cast/ApplicationSession;

    invoke-static {v0, v2}, Lcom/google/cast/ApplicationSession;->a(Lcom/google/cast/ApplicationSession;Lcom/google/cast/SessionError;)V

    goto :goto_0

    .line 223
    :cond_2
    if-ne p1, v1, :cond_3

    .line 224
    const/4 v0, 0x4

    goto :goto_1

    .line 225
    :cond_3
    if-ne p1, v0, :cond_4

    .line 226
    const/4 v0, 0x5

    goto :goto_1

    .line 227
    :cond_4
    const/4 v2, 0x3

    if-ne p1, v2, :cond_1

    .line 228
    const/4 v0, 0x6

    goto :goto_1
.end method
