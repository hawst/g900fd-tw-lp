.class public final Lcom/google/cast/MediaRouteHelper;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Lcom/google/cast/q;


# direct methods
.method public static registerMediaRouteProvider(Lcom/google/cast/CastContext;Ljava/lang/String;Lcom/google/cast/MimeData;IZ)Z
    .locals 9
    .param p0, "castContext"    # Lcom/google/cast/CastContext;
    .param p1, "receiverApplicationName"    # Ljava/lang/String;
    .param p2, "receiverApplicationArgument"    # Lcom/google/cast/MimeData;
    .param p3, "sessionOptions"    # I
    .param p4, "stopApplicationWhenRouteDeselected"    # Z

    .prologue
    .line 135
    invoke-static {}, Lcom/google/cast/p;->a()V

    .line 137
    sget-object v0, Lcom/google/cast/MediaRouteHelper;->a:Lcom/google/cast/q;

    if-eqz v0, :cond_0

    .line 138
    const/4 v0, 0x0

    .line 151
    :goto_0
    return v0

    .line 140
    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 141
    const/4 p1, 0x0

    .line 144
    :cond_1
    invoke-virtual {p0}, Lcom/google/cast/CastContext;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    .line 145
    invoke-static {v6}, Landroid/support/v7/media/MediaRouter;->getInstance(Landroid/content/Context;)Landroid/support/v7/media/MediaRouter;

    move-result-object v7

    .line 146
    new-instance v8, Lcom/google/cast/q;

    new-instance v0, Lcom/google/cast/c;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/cast/c;-><init>(Lcom/google/cast/CastContext;Ljava/lang/String;Lcom/google/cast/MimeData;IZ)V

    invoke-direct {v8, v6, v0}, Lcom/google/cast/q;-><init>(Landroid/content/Context;Landroid/support/v7/media/MediaRouteProvider;)V

    sput-object v8, Lcom/google/cast/MediaRouteHelper;->a:Lcom/google/cast/q;

    .line 150
    sget-object v0, Lcom/google/cast/MediaRouteHelper;->a:Lcom/google/cast/q;

    invoke-virtual {v7, v0}, Landroid/support/v7/media/MediaRouter;->addProvider(Landroid/support/v7/media/MediaRouteProvider;)V

    .line 151
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static registerMinimalMediaRouteProvider(Lcom/google/cast/CastContext;Lcom/google/cast/MediaRouteAdapter;)Z
    .locals 4
    .param p0, "castContext"    # Lcom/google/cast/CastContext;
    .param p1, "adapter"    # Lcom/google/cast/MediaRouteAdapter;

    .prologue
    .line 168
    invoke-static {}, Lcom/google/cast/p;->a()V

    .line 170
    sget-object v0, Lcom/google/cast/MediaRouteHelper;->a:Lcom/google/cast/q;

    if-eqz v0, :cond_0

    .line 171
    const/4 v0, 0x0

    .line 178
    :goto_0
    return v0

    .line 173
    :cond_0
    invoke-virtual {p0}, Lcom/google/cast/CastContext;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 174
    invoke-static {v0}, Landroid/support/v7/media/MediaRouter;->getInstance(Landroid/content/Context;)Landroid/support/v7/media/MediaRouter;

    move-result-object v1

    .line 175
    new-instance v2, Lcom/google/cast/q;

    new-instance v3, Lcom/google/cast/j;

    invoke-direct {v3, p0, p1}, Lcom/google/cast/j;-><init>(Lcom/google/cast/CastContext;Lcom/google/cast/MediaRouteAdapter;)V

    invoke-direct {v2, v0, v3}, Lcom/google/cast/q;-><init>(Landroid/content/Context;Landroid/support/v7/media/MediaRouteProvider;)V

    sput-object v2, Lcom/google/cast/MediaRouteHelper;->a:Lcom/google/cast/q;

    .line 177
    sget-object v0, Lcom/google/cast/MediaRouteHelper;->a:Lcom/google/cast/q;

    invoke-virtual {v1, v0}, Landroid/support/v7/media/MediaRouter;->addProvider(Landroid/support/v7/media/MediaRouteProvider;)V

    .line 178
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static requestCastDeviceForRoute(Landroid/support/v7/media/MediaRouter$RouteInfo;)Z
    .locals 3
    .param p0, "route"    # Landroid/support/v7/media/MediaRouter$RouteInfo;

    .prologue
    .line 265
    sget-object v0, Lcom/google/cast/MediaRouteHelper;->a:Lcom/google/cast/q;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/cast/MediaRouteHelper;->a:Lcom/google/cast/q;

    invoke-virtual {v0}, Lcom/google/cast/q;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 267
    :cond_0
    const/4 v0, 0x0

    .line 275
    :goto_0
    return v0

    .line 270
    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.cast.ACTION_GET_DEVICE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 271
    const-string v1, "com.google.cast.EXTRA_ROUTE_ID"

    invoke-virtual {p0}, Landroid/support/v7/media/MediaRouter$RouteInfo;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 272
    const-string v1, "com.google.cast.CATEGORY_CAST"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 273
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/support/v7/media/MediaRouter$RouteInfo;->sendControlRequest(Landroid/content/Intent;Landroid/support/v7/media/MediaRouter$ControlRequestCallback;)V

    .line 275
    const/4 v0, 0x1

    goto :goto_0
.end method
