.class final Lcom/google/cast/k;
.super Lcom/google/cast/NetworkRequest;
.source "SourceFile"


# instance fields
.field private a:Landroid/net/Uri;

.field private b:Landroid/net/Uri;

.field private c:J


# direct methods
.method public constructor <init>(Lcom/google/cast/CastContext;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lcom/google/cast/NetworkRequest;-><init>(Lcom/google/cast/CastContext;)V

    .line 30
    iput-object p2, p0, Lcom/google/cast/k;->a:Landroid/net/Uri;

    .line 31
    return-void
.end method


# virtual methods
.method public a()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/cast/k;->b:Landroid/net/Uri;

    return-object v0
.end method

.method public b()J
    .locals 2

    .prologue
    .line 39
    iget-wide v0, p0, Lcom/google/cast/k;->c:J

    return-wide v0
.end method

.method public final execute()I
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v0, -0x1

    const/4 v1, -0x3

    .line 44
    .line 47
    :try_start_0
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V
    :try_end_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    .line 49
    :try_start_1
    const-string v4, "channel"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 54
    :goto_0
    :try_start_2
    iget-object v4, p0, Lcom/google/cast/k;->a:Landroid/net/Uri;

    invoke-static {v3}, Lcom/google/cast/MimeData;->createJsonData(Lorg/json/JSONObject;)Lcom/google/cast/MimeData;

    move-result-object v3

    sget v5, Lcom/google/cast/k;->DEFAULT_TIMEOUT:I

    invoke-virtual {p0, v4, v3, v5}, Lcom/google/cast/k;->performHttpPost(Landroid/net/Uri;Lcom/google/cast/MimeData;I)Lcom/google/cast/SimpleHttpRequest;

    move-result-object v3

    .line 57
    invoke-interface {v3}, Lcom/google/cast/SimpleHttpRequest;->getResponseStatus()I

    move-result v4

    .line 58
    const/16 v5, 0x194

    if-ne v4, v5, :cond_1

    .line 59
    const/4 v0, -0x4

    .line 95
    :cond_0
    :goto_1
    return v0

    .line 60
    :cond_1
    const/16 v5, 0x193

    if-ne v4, v5, :cond_2

    .line 61
    const/4 v0, -0x5

    goto :goto_1

    .line 62
    :cond_2
    const/16 v5, 0xc8

    if-ne v4, v5, :cond_0

    .line 66
    invoke-interface {v3}, Lcom/google/cast/SimpleHttpRequest;->getResponseData()Lcom/google/cast/MimeData;
    :try_end_2
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v0

    .line 74
    if-eqz v0, :cond_3

    const-string v3, "application/json"

    invoke-virtual {v0}, Lcom/google/cast/MimeData;->getType()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    :cond_3
    move v0, v1

    .line 75
    goto :goto_1

    .line 67
    :catch_0
    move-exception v0

    .line 68
    const/4 v0, -0x2

    goto :goto_1

    .line 77
    :cond_4
    invoke-virtual {v0}, Lcom/google/cast/MimeData;->getTextData()Ljava/lang/String;

    move-result-object v0

    .line 78
    if-nez v0, :cond_5

    move v0, v1

    .line 79
    goto :goto_1

    .line 82
    :cond_5
    :try_start_3
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 84
    const-string v0, "URL"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 85
    if-nez v0, :cond_6

    move v0, v1

    .line 86
    goto :goto_1

    .line 89
    :cond_6
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/cast/k;->b:Landroid/net/Uri;

    .line 90
    const-string v0, "pingInterval"

    const-wide/16 v4, 0x0

    invoke-virtual {v3, v0, v4, v5}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    mul-long/2addr v4, v6

    iput-wide v4, p0, Lcom/google/cast/k;->c:J
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_1

    move v0, v2

    .line 95
    goto :goto_1

    .line 91
    :catch_1
    move-exception v0

    move v0, v1

    .line 92
    goto :goto_1

    .line 69
    :catch_2
    move-exception v1

    goto :goto_1

    .line 50
    :catch_3
    move-exception v4

    goto :goto_0
.end method
