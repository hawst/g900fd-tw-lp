.class Lcom/google/cast/WebSocket;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/cast/WebSocket$Listener;
    }
.end annotation


# static fields
.field private static final a:[B

.field private static p:Lcom/google/cast/Logger;


# instance fields
.field private b:Landroid/net/Uri;

.field private c:I

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Lcom/google/cast/WebSocket$Listener;

.field private g:Ljava/nio/channels/SocketChannel;

.field private h:I

.field private i:I

.field private j:Lcom/google/cast/e;

.field private k:Lcom/google/cast/e;

.field private l:Lcom/google/cast/r;

.field private m:Ljava/nio/charset/Charset;

.field private n:Ljava/nio/charset/CharsetDecoder;

.field private o:Z

.field private q:I

.field private r:B

.field private s:Z

.field private t:Z

.field private u:Z

.field private v:[B

.field private w:Ljava/lang/String;

.field private x:I

.field private y:J

.field private z:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 81
    const/4 v0, 0x4

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/cast/WebSocket;->a:[B

    return-void

    nop

    :array_0
    .array-data 1
        0xdt
        0xat
        0xdt
        0xat
    .end array-data
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 2
    .param p1, "origin"    # Ljava/lang/String;
    .param p2, "protocol"    # Ljava/lang/String;
    .param p3, "bufferSize"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 218
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 219
    const/16 v0, 0x1000

    if-ge p3, v0, :cond_0

    .line 220
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "bufferSize < MIN_BUFFER_SIZE"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 223
    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/cast/WebSocket;->q:I

    .line 224
    iput-object p1, p0, Lcom/google/cast/WebSocket;->d:Ljava/lang/String;

    .line 225
    iput-object p2, p0, Lcom/google/cast/WebSocket;->e:Ljava/lang/String;

    .line 226
    invoke-static {}, Lcom/google/cast/r;->a()Lcom/google/cast/r;

    move-result-object v0

    iput-object v0, p0, Lcom/google/cast/WebSocket;->l:Lcom/google/cast/r;

    .line 227
    iput p3, p0, Lcom/google/cast/WebSocket;->h:I

    .line 228
    iget v0, p0, Lcom/google/cast/WebSocket;->h:I

    add-int/lit8 v0, v0, -0xe

    iput v0, p0, Lcom/google/cast/WebSocket;->i:I

    .line 229
    iget-object v0, p0, Lcom/google/cast/WebSocket;->l:Lcom/google/cast/r;

    invoke-virtual {v0}, Lcom/google/cast/r;->f()Ljava/nio/charset/Charset;

    move-result-object v0

    iput-object v0, p0, Lcom/google/cast/WebSocket;->m:Ljava/nio/charset/Charset;

    .line 230
    iget-object v0, p0, Lcom/google/cast/WebSocket;->m:Ljava/nio/charset/Charset;

    invoke-virtual {v0}, Ljava/nio/charset/Charset;->newDecoder()Ljava/nio/charset/CharsetDecoder;

    move-result-object v0

    iput-object v0, p0, Lcom/google/cast/WebSocket;->n:Ljava/nio/charset/CharsetDecoder;

    .line 231
    const-wide/16 v0, 0x1388

    iput-wide v0, p0, Lcom/google/cast/WebSocket;->z:J

    .line 232
    new-instance v0, Lcom/google/cast/Logger;

    const-string v1, "WebSocket"

    invoke-direct {v0, v1}, Lcom/google/cast/Logger;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/cast/WebSocket;->p:Lcom/google/cast/Logger;

    .line 233
    return-void
.end method

.method static a(Ljava/lang/String;Ljava/nio/charset/Charset;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 874
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "258EAFA5-E914-47DA-95CA-C5AB0DC85B11"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 875
    const-string v1, "SHA-1"

    invoke-static {v1}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v1

    .line 876
    invoke-virtual {v0, p1}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/security/MessageDigest;->update([B)V

    .line 877
    invoke-virtual {v1}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v0

    .line 878
    const/4 v1, 0x0

    array-length v2, v0

    const/4 v3, 0x2

    invoke-static {v0, v1, v2, v3}, Landroid/util/Base64;->encodeToString([BIII)Ljava/lang/String;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 882
    :goto_0
    return-object v0

    .line 879
    :catch_0
    move-exception v0

    .line 881
    sget-object v1, Lcom/google/cast/WebSocket;->p:Lcom/google/cast/Logger;

    const-string v2, "unexpected Base64 encoding error"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/cast/Logger;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 882
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(B[BZ)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 790
    if-nez p2, :cond_0

    const/4 v0, 0x0

    .line 791
    :goto_0
    iget-object v1, p0, Lcom/google/cast/WebSocket;->k:Lcom/google/cast/e;

    invoke-virtual {v1}, Lcom/google/cast/e;->h()I

    move-result v1

    add-int/lit8 v0, v0, 0xe

    if-ge v1, v0, :cond_1

    .line 792
    new-instance v0, Ljava/io/IOException;

    const-string v1, "no room in buffer"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 790
    :cond_0
    array-length v0, p2

    goto :goto_0

    .line 797
    :cond_1
    if-eqz p3, :cond_2

    .line 798
    or-int/lit8 v0, p1, -0x80

    int-to-byte p1, v0

    .line 800
    :cond_2
    iget-object v0, p0, Lcom/google/cast/WebSocket;->k:Lcom/google/cast/e;

    invoke-virtual {v0, p1}, Lcom/google/cast/e;->a(B)Z

    .line 803
    if-nez p2, :cond_4

    .line 804
    iget-object v0, p0, Lcom/google/cast/WebSocket;->k:Lcom/google/cast/e;

    const/16 v1, -0x80

    invoke-virtual {v0, v1}, Lcom/google/cast/e;->a(B)Z

    .line 816
    :goto_1
    iget-object v0, p0, Lcom/google/cast/WebSocket;->l:Lcom/google/cast/r;

    invoke-virtual {v0}, Lcom/google/cast/r;->e()[B

    move-result-object v0

    .line 817
    iget-object v1, p0, Lcom/google/cast/WebSocket;->k:Lcom/google/cast/e;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/google/cast/e;->b([B[B)Z

    .line 820
    if-eqz p2, :cond_3

    .line 821
    iget-object v1, p0, Lcom/google/cast/WebSocket;->k:Lcom/google/cast/e;

    invoke-virtual {v1, p2, v0}, Lcom/google/cast/e;->b([B[B)Z

    .line 823
    :cond_3
    return-void

    .line 805
    :cond_4
    array-length v0, p2

    const/16 v1, 0x7e

    if-ge v0, v1, :cond_5

    .line 806
    iget-object v0, p0, Lcom/google/cast/WebSocket;->k:Lcom/google/cast/e;

    array-length v1, p2

    or-int/lit8 v1, v1, -0x80

    int-to-byte v1, v1

    invoke-virtual {v0, v1}, Lcom/google/cast/e;->a(B)Z

    goto :goto_1

    .line 807
    :cond_5
    array-length v0, p2

    const/high16 v1, 0x10000

    if-ge v0, v1, :cond_6

    .line 808
    iget-object v0, p0, Lcom/google/cast/WebSocket;->k:Lcom/google/cast/e;

    const/4 v1, -0x2

    invoke-virtual {v0, v1}, Lcom/google/cast/e;->a(B)Z

    .line 809
    iget-object v0, p0, Lcom/google/cast/WebSocket;->k:Lcom/google/cast/e;

    array-length v1, p2

    invoke-virtual {v0, v1}, Lcom/google/cast/e;->b(I)Z

    goto :goto_1

    .line 811
    :cond_6
    iget-object v0, p0, Lcom/google/cast/WebSocket;->k:Lcom/google/cast/e;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/google/cast/e;->a(B)Z

    .line 812
    iget-object v0, p0, Lcom/google/cast/WebSocket;->k:Lcom/google/cast/e;

    array-length v1, p2

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Lcom/google/cast/e;->a(J)Z

    goto :goto_1
.end method

.method private a(S)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 588
    sget-object v0, Lcom/google/cast/WebSocket;->p:Lcom/google/cast/Logger;

    const-string v1, "writing closing handshake"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 589
    const/4 v0, 0x2

    new-array v0, v0, [B

    .line 590
    shr-int/lit8 v1, p1, 0x8

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    aput-byte v1, v0, v4

    .line 591
    and-int/lit16 v1, p1, 0xff

    int-to-byte v1, v1

    aput-byte v1, v0, v3

    .line 592
    const/16 v1, 0x8

    invoke-direct {p0, v1, v0, v3}, Lcom/google/cast/WebSocket;->a(B[BZ)V

    .line 593
    iput-boolean v3, p0, Lcom/google/cast/WebSocket;->s:Z

    .line 594
    return-void
.end method

.method private static b(Ljava/lang/String;)Ljava/net/InetAddress;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/ClassNotFoundException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 444
    invoke-static {p0}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v0

    .line 446
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 447
    new-instance v2, Ljava/io/ObjectOutputStream;

    invoke-direct {v2, v1}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 448
    invoke-virtual {v2, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 449
    invoke-virtual {v2}, Ljava/io/ObjectOutputStream;->close()V

    .line 451
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    .line 453
    array-length v1, v0

    add-int/lit8 v1, v1, 0x2

    new-array v1, v1, [B

    .line 454
    array-length v2, v0

    add-int/lit8 v2, v2, -0x2

    .line 455
    invoke-static {v0, v4, v1, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 458
    add-int/lit8 v0, v2, 0x1

    const/16 v3, 0x74

    aput-byte v3, v1, v2

    .line 460
    add-int/lit8 v2, v0, 0x1

    aput-byte v4, v1, v0

    .line 461
    add-int/lit8 v0, v2, 0x1

    aput-byte v4, v1, v2

    .line 463
    const/16 v2, 0x78

    aput-byte v2, v1, v0

    .line 466
    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-direct {v0, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 467
    new-instance v1, Ljava/io/ObjectInputStream;

    invoke-direct {v1, v0}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V

    .line 469
    invoke-virtual {v1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/Inet4Address;

    .line 470
    invoke-virtual {v1}, Ljava/io/ObjectInputStream;->close()V

    .line 471
    return-object v0
.end method

.method private b(I)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v6, 0x0

    const/4 v2, 0x1

    .line 832
    sget-object v0, Lcom/google/cast/WebSocket;->p:Lcom/google/cast/Logger;

    const-string v3, "doTeardown with error=%d"

    new-array v4, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-virtual {v0, v3, v4}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 834
    iget-object v0, p0, Lcom/google/cast/WebSocket;->g:Ljava/nio/channels/SocketChannel;

    if-eqz v0, :cond_0

    .line 836
    :try_start_0
    iget-object v0, p0, Lcom/google/cast/WebSocket;->g:Ljava/nio/channels/SocketChannel;

    invoke-virtual {v0}, Ljava/nio/channels/SocketChannel;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 838
    :goto_0
    iput-object v6, p0, Lcom/google/cast/WebSocket;->g:Ljava/nio/channels/SocketChannel;

    .line 841
    :cond_0
    iput-object v6, p0, Lcom/google/cast/WebSocket;->b:Landroid/net/Uri;

    .line 842
    iput-object v6, p0, Lcom/google/cast/WebSocket;->j:Lcom/google/cast/e;

    .line 843
    iput-object v6, p0, Lcom/google/cast/WebSocket;->k:Lcom/google/cast/e;

    .line 844
    iget v0, p0, Lcom/google/cast/WebSocket;->q:I

    if-eq v0, v2, :cond_1

    iget v0, p0, Lcom/google/cast/WebSocket;->q:I

    const/4 v3, 0x2

    if-ne v0, v3, :cond_3

    :cond_1
    move v0, v2

    .line 847
    :goto_1
    iput v1, p0, Lcom/google/cast/WebSocket;->q:I

    .line 848
    iput-boolean v2, p0, Lcom/google/cast/WebSocket;->o:Z

    .line 850
    iget-object v1, p0, Lcom/google/cast/WebSocket;->f:Lcom/google/cast/WebSocket$Listener;

    if-eqz v1, :cond_2

    .line 851
    if-eqz v0, :cond_4

    .line 852
    iget-object v0, p0, Lcom/google/cast/WebSocket;->f:Lcom/google/cast/WebSocket$Listener;

    invoke-interface {v0, p1}, Lcom/google/cast/WebSocket$Listener;->onConnectionFailed(I)V

    .line 857
    :cond_2
    :goto_2
    return-void

    :cond_3
    move v0, v1

    .line 844
    goto :goto_1

    .line 854
    :cond_4
    iget-object v0, p0, Lcom/google/cast/WebSocket;->f:Lcom/google/cast/WebSocket$Listener;

    iget v1, p0, Lcom/google/cast/WebSocket;->x:I

    invoke-interface {v0, p1, v1}, Lcom/google/cast/WebSocket$Listener;->onDisconnected(II)V

    goto :goto_2

    .line 837
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private j()V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 480
    iput v7, p0, Lcom/google/cast/WebSocket;->q:I

    .line 481
    iget-object v0, p0, Lcom/google/cast/WebSocket;->l:Lcom/google/cast/r;

    invoke-virtual {v0}, Lcom/google/cast/r;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/cast/WebSocket;->w:Ljava/lang/String;

    .line 482
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 483
    const-string v1, "GET %s HTTP/1.1\r\nHost: %s\r\nUpgrade: WebSocket\r\nConnection: Upgrade\r\nSec-WebSocket-Key: %s\r\nOrigin: %s\r\nSec-WebSocket-Version: 13\r\n"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/google/cast/WebSocket;->b:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/cast/WebSocket;->b:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->getQuery()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/google/cast/WebSocket;->b:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/google/cast/WebSocket;->c:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v6

    iget-object v3, p0, Lcom/google/cast/WebSocket;->w:Ljava/lang/String;

    aput-object v3, v2, v7

    const/4 v3, 0x3

    iget-object v4, p0, Lcom/google/cast/WebSocket;->d:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 489
    iget-object v1, p0, Lcom/google/cast/WebSocket;->e:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 490
    const-string v1, "Sec-WebSocket-Protocol"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/cast/WebSocket;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\r\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 492
    :cond_0
    const-string v1, "\r\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 493
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 495
    sget-object v1, Lcom/google/cast/WebSocket;->p:Lcom/google/cast/Logger;

    const-string v2, "sending handshake:\n%s"

    new-array v3, v6, [Ljava/lang/Object;

    aput-object v0, v3, v5

    invoke-virtual {v1, v2, v3}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 497
    iget-object v1, p0, Lcom/google/cast/WebSocket;->k:Lcom/google/cast/e;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/cast/WebSocket;->m:Ljava/nio/charset/Charset;

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/cast/e;->a(Ljava/lang/String;[BLjava/nio/charset/Charset;)Z

    .line 498
    return-void
.end method

.method private k()Z
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 510
    iget-object v0, p0, Lcom/google/cast/WebSocket;->j:Lcom/google/cast/e;

    sget-object v2, Lcom/google/cast/WebSocket;->a:[B

    invoke-virtual {v0, v2}, Lcom/google/cast/e;->a([B)I

    move-result v0

    .line 511
    if-gez v0, :cond_0

    .line 566
    :goto_0
    return v1

    .line 519
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/google/cast/WebSocket;->j:Lcom/google/cast/e;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/cast/WebSocket;->n:Ljava/nio/charset/CharsetDecoder;

    invoke-virtual {v2, v0, v4, v5}, Lcom/google/cast/e;->a(I[BLjava/nio/charset/CharsetDecoder;)Ljava/lang/String;

    move-result-object v0

    .line 520
    if-nez v0, :cond_1

    .line 521
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Invalid opening handshake reply"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 523
    :catch_0
    move-exception v0

    .line 524
    new-instance v1, Lcom/google/cast/f;

    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/cast/f;-><init>(Ljava/lang/String;)V

    throw v1

    .line 527
    :cond_1
    sget-object v2, Lcom/google/cast/WebSocket;->p:Lcom/google/cast/Logger;

    const-string v4, "handshake reply:\n%s"

    new-array v5, v1, [Ljava/lang/Object;

    aput-object v0, v5, v3

    invoke-virtual {v2, v4, v5}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 530
    const-string v2, "\r\n"

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 531
    array-length v0, v4

    if-nez v0, :cond_2

    .line 532
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Invalid opening handshake reply"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 535
    :cond_2
    aget-object v0, v4, v3

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    const-string v2, "^HTTP\\/[0-9\\.]+\\s+101\\s+.*$"

    invoke-virtual {v0, v2}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 536
    sget-object v0, Lcom/google/cast/WebSocket;->p:Lcom/google/cast/Logger;

    const-string v2, "Unexpected HTTP status: %s"

    new-array v1, v1, [Ljava/lang/Object;

    aget-object v4, v4, v3

    aput-object v4, v1, v3

    invoke-virtual {v0, v2, v1}, Lcom/google/cast/Logger;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    move v1, v3

    .line 537
    goto :goto_0

    :cond_3
    move v0, v1

    move v2, v3

    .line 541
    :goto_1
    array-length v5, v4

    if-ge v0, v5, :cond_7

    .line 542
    aget-object v5, v4, v0

    invoke-virtual {v5}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    .line 543
    const-string v6, "sec-websocket-accept: "

    invoke-virtual {v5, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 544
    aget-object v5, v4, v0

    const-string v6, "sec-websocket-accept: "

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    .line 546
    iget-object v6, p0, Lcom/google/cast/WebSocket;->w:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/cast/WebSocket;->m:Ljava/nio/charset/Charset;

    invoke-static {v6, v7}, Lcom/google/cast/WebSocket;->a(Ljava/lang/String;Ljava/nio/charset/Charset;)Ljava/lang/String;

    move-result-object v6

    .line 547
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    move v2, v1

    .line 541
    :cond_4
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 550
    :cond_5
    sget-object v7, Lcom/google/cast/WebSocket;->p:Lcom/google/cast/Logger;

    const-string v8, "Expected accept value <%s> but got <%s>"

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    aput-object v6, v9, v3

    aput-object v5, v9, v1

    invoke-virtual {v7, v8, v9}, Lcom/google/cast/Logger;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2

    .line 552
    :cond_6
    const-string v6, "sec-websocket-protocol: "

    invoke-virtual {v5, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 553
    aget-object v5, v4, v0

    const-string v6, "sec-websocket-protocol: "

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/cast/WebSocket;->e:Ljava/lang/String;

    goto :goto_2

    .line 557
    :cond_7
    if-eqz v2, :cond_8

    .line 558
    sget-object v0, Lcom/google/cast/WebSocket;->p:Lcom/google/cast/Logger;

    const-string v1, "Switching to STATE_CONNECTED"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v3}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 559
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/cast/WebSocket;->q:I

    .line 561
    iget-object v0, p0, Lcom/google/cast/WebSocket;->f:Lcom/google/cast/WebSocket$Listener;

    if-eqz v0, :cond_8

    .line 562
    iget-object v0, p0, Lcom/google/cast/WebSocket;->f:Lcom/google/cast/WebSocket$Listener;

    invoke-interface {v0}, Lcom/google/cast/WebSocket$Listener;->onConnected()V

    :cond_8
    move v1, v2

    .line 566
    goto/16 :goto_0
.end method

.method private l()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 576
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/cast/WebSocket;->v:[B

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/google/cast/WebSocket;->a(B[BZ)V

    .line 577
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/cast/WebSocket;->v:[B

    .line 578
    return-void
.end method

.method private m()Z
    .locals 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v12, 0x4

    const/4 v11, -0x1

    const/4 v10, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 604
    .line 605
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/cast/WebSocket;->j:Lcom/google/cast/e;

    invoke-virtual {v0}, Lcom/google/cast/e;->j()Z

    move-result v0

    if-nez v0, :cond_1

    .line 606
    iget-object v0, p0, Lcom/google/cast/WebSocket;->j:Lcom/google/cast/e;

    invoke-virtual {v0}, Lcom/google/cast/e;->e()V

    .line 609
    iget-object v0, p0, Lcom/google/cast/WebSocket;->j:Lcom/google/cast/e;

    invoke-virtual {v0}, Lcom/google/cast/e;->a()Ljava/lang/Byte;

    move-result-object v0

    .line 610
    if-nez v0, :cond_2

    move v2, v1

    .line 768
    :cond_1
    :goto_1
    if-eqz v2, :cond_1a

    .line 771
    iget-object v0, p0, Lcom/google/cast/WebSocket;->j:Lcom/google/cast/e;

    invoke-virtual {v0}, Lcom/google/cast/e;->g()V

    :goto_2
    move v2, v1

    .line 776
    :goto_3
    return v2

    .line 614
    :cond_2
    invoke-virtual {v0}, Ljava/lang/Byte;->byteValue()B

    move-result v3

    and-int/lit8 v3, v3, 0xf

    int-to-byte v6, v3

    .line 615
    invoke-virtual {v0}, Ljava/lang/Byte;->byteValue()B

    move-result v0

    and-int/lit8 v0, v0, -0x80

    if-eqz v0, :cond_3

    move v0, v1

    .line 618
    :goto_4
    iget-object v3, p0, Lcom/google/cast/WebSocket;->j:Lcom/google/cast/e;

    invoke-virtual {v3}, Lcom/google/cast/e;->a()Ljava/lang/Byte;

    move-result-object v4

    .line 619
    if-nez v4, :cond_4

    move v2, v1

    .line 621
    goto :goto_1

    :cond_3
    move v0, v2

    .line 615
    goto :goto_4

    .line 623
    :cond_4
    invoke-virtual {v4}, Ljava/lang/Byte;->byteValue()B

    move-result v3

    and-int/lit8 v3, v3, -0x80

    if-eqz v3, :cond_5

    move v3, v1

    .line 624
    :goto_5
    invoke-virtual {v4}, Ljava/lang/Byte;->byteValue()B

    move-result v4

    and-int/lit8 v4, v4, 0x7f

    int-to-long v4, v4

    .line 626
    const-wide/16 v8, 0x7e

    cmp-long v7, v4, v8

    if-nez v7, :cond_8

    .line 627
    iget-object v4, p0, Lcom/google/cast/WebSocket;->j:Lcom/google/cast/e;

    invoke-virtual {v4}, Lcom/google/cast/e;->b()Ljava/lang/Integer;

    move-result-object v4

    .line 628
    if-nez v4, :cond_6

    move v2, v1

    .line 630
    goto :goto_1

    :cond_5
    move v3, v2

    .line 623
    goto :goto_5

    .line 632
    :cond_6
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    int-to-long v4, v4

    .line 642
    :cond_7
    :goto_6
    iget v7, p0, Lcom/google/cast/WebSocket;->i:I

    int-to-long v8, v7

    cmp-long v7, v4, v8

    if-lez v7, :cond_a

    .line 643
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "message too large: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " bytes"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 633
    :cond_8
    const-wide/16 v8, 0x7f

    cmp-long v7, v4, v8

    if-nez v7, :cond_7

    .line 634
    iget-object v4, p0, Lcom/google/cast/WebSocket;->j:Lcom/google/cast/e;

    invoke-virtual {v4}, Lcom/google/cast/e;->c()Ljava/lang/Long;

    move-result-object v4

    .line 635
    if-nez v4, :cond_9

    move v2, v1

    .line 637
    goto/16 :goto_1

    .line 639
    :cond_9
    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    goto :goto_6

    .line 645
    :cond_a
    long-to-int v5, v4

    .line 648
    const/4 v4, 0x0

    .line 649
    if-eqz v3, :cond_b

    .line 650
    new-array v3, v12, [B

    .line 651
    iget-object v4, p0, Lcom/google/cast/WebSocket;->j:Lcom/google/cast/e;

    const/4 v7, 0x0

    invoke-virtual {v4, v3, v7}, Lcom/google/cast/e;->a([B[B)Z

    move-result v4

    if-nez v4, :cond_c

    move v2, v1

    .line 653
    goto/16 :goto_1

    :cond_b
    move-object v3, v4

    .line 658
    :cond_c
    iget-object v4, p0, Lcom/google/cast/WebSocket;->j:Lcom/google/cast/e;

    invoke-virtual {v4}, Lcom/google/cast/e;->i()I

    move-result v4

    if-ge v4, v5, :cond_d

    move v2, v1

    .line 660
    goto/16 :goto_1

    .line 663
    :cond_d
    packed-switch v6, :pswitch_data_0

    .line 758
    :pswitch_0
    iget-object v0, p0, Lcom/google/cast/WebSocket;->j:Lcom/google/cast/e;

    invoke-virtual {v0, v5}, Lcom/google/cast/e;->a(I)Z

    move-result v0

    if-nez v0, :cond_19

    move v2, v1

    .line 760
    goto/16 :goto_1

    .line 665
    :pswitch_1
    iget-byte v4, p0, Lcom/google/cast/WebSocket;->r:B

    if-ne v4, v1, :cond_f

    .line 666
    iget-object v4, p0, Lcom/google/cast/WebSocket;->j:Lcom/google/cast/e;

    iget-object v6, p0, Lcom/google/cast/WebSocket;->n:Ljava/nio/charset/CharsetDecoder;

    invoke-virtual {v4, v5, v3, v6}, Lcom/google/cast/e;->a(I[BLjava/nio/charset/CharsetDecoder;)Ljava/lang/String;

    move-result-object v3

    .line 667
    if-nez v3, :cond_e

    move v2, v1

    .line 669
    goto/16 :goto_1

    .line 671
    :cond_e
    iget-object v4, p0, Lcom/google/cast/WebSocket;->f:Lcom/google/cast/WebSocket$Listener;

    if-eqz v4, :cond_0

    .line 672
    iget-object v4, p0, Lcom/google/cast/WebSocket;->f:Lcom/google/cast/WebSocket$Listener;

    invoke-interface {v4, v3, v0}, Lcom/google/cast/WebSocket$Listener;->onContinuationMessageReceived(Ljava/lang/String;Z)V

    goto/16 :goto_0

    .line 674
    :cond_f
    iget-byte v4, p0, Lcom/google/cast/WebSocket;->r:B

    if-ne v4, v10, :cond_11

    .line 675
    new-array v4, v5, [B

    .line 676
    iget-object v5, p0, Lcom/google/cast/WebSocket;->j:Lcom/google/cast/e;

    invoke-virtual {v5, v4, v3}, Lcom/google/cast/e;->a([B[B)Z

    move-result v3

    if-nez v3, :cond_10

    move v2, v1

    .line 678
    goto/16 :goto_1

    .line 680
    :cond_10
    iget-object v3, p0, Lcom/google/cast/WebSocket;->f:Lcom/google/cast/WebSocket$Listener;

    if-eqz v3, :cond_0

    .line 681
    iget-object v3, p0, Lcom/google/cast/WebSocket;->f:Lcom/google/cast/WebSocket$Listener;

    invoke-interface {v3, v4, v0}, Lcom/google/cast/WebSocket$Listener;->onContinuationMessageReceived([BZ)V

    goto/16 :goto_0

    .line 684
    :cond_11
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Unexpected continuation frame received"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 690
    :pswitch_2
    sget-object v4, Lcom/google/cast/WebSocket;->p:Lcom/google/cast/Logger;

    const-string v7, "reading a text message of length %d, mask %h"

    new-array v8, v10, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v2

    aput-object v3, v8, v1

    invoke-virtual {v4, v7, v8}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 691
    iget-object v4, p0, Lcom/google/cast/WebSocket;->j:Lcom/google/cast/e;

    iget-object v7, p0, Lcom/google/cast/WebSocket;->n:Ljava/nio/charset/CharsetDecoder;

    invoke-virtual {v4, v5, v3, v7}, Lcom/google/cast/e;->a(I[BLjava/nio/charset/CharsetDecoder;)Ljava/lang/String;

    move-result-object v3

    .line 692
    if-nez v3, :cond_12

    move v2, v1

    .line 694
    goto/16 :goto_1

    .line 697
    :cond_12
    iget-object v4, p0, Lcom/google/cast/WebSocket;->f:Lcom/google/cast/WebSocket$Listener;

    if-eqz v4, :cond_13

    .line 698
    iget-object v4, p0, Lcom/google/cast/WebSocket;->f:Lcom/google/cast/WebSocket$Listener;

    invoke-interface {v4, v3, v0}, Lcom/google/cast/WebSocket$Listener;->onMessageReceived(Ljava/lang/String;Z)V

    .line 700
    :cond_13
    iput-byte v6, p0, Lcom/google/cast/WebSocket;->r:B

    goto/16 :goto_0

    .line 705
    :pswitch_3
    new-array v4, v5, [B

    .line 706
    iget-object v5, p0, Lcom/google/cast/WebSocket;->j:Lcom/google/cast/e;

    invoke-virtual {v5, v4, v3}, Lcom/google/cast/e;->a([B[B)Z

    move-result v3

    if-nez v3, :cond_14

    move v2, v1

    .line 708
    goto/16 :goto_1

    .line 711
    :cond_14
    iget-object v3, p0, Lcom/google/cast/WebSocket;->f:Lcom/google/cast/WebSocket$Listener;

    if-eqz v3, :cond_15

    .line 712
    iget-object v3, p0, Lcom/google/cast/WebSocket;->f:Lcom/google/cast/WebSocket$Listener;

    invoke-interface {v3, v4, v0}, Lcom/google/cast/WebSocket$Listener;->onMessageReceived([BZ)V

    .line 714
    :cond_15
    iput-byte v6, p0, Lcom/google/cast/WebSocket;->r:B

    goto/16 :goto_0

    .line 719
    :pswitch_4
    iput v12, p0, Lcom/google/cast/WebSocket;->q:I

    .line 720
    if-lt v5, v10, :cond_1b

    .line 721
    iget-object v0, p0, Lcom/google/cast/WebSocket;->j:Lcom/google/cast/e;

    invoke-virtual {v0}, Lcom/google/cast/e;->b()Ljava/lang/Integer;

    move-result-object v0

    .line 722
    if-nez v0, :cond_16

    move v2, v1

    .line 724
    goto/16 :goto_1

    .line 726
    :cond_16
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/google/cast/WebSocket;->x:I

    .line 727
    add-int/lit8 v0, v5, -0x2

    .line 729
    :goto_7
    if-lez v0, :cond_17

    .line 730
    iget-object v3, p0, Lcom/google/cast/WebSocket;->j:Lcom/google/cast/e;

    invoke-virtual {v3, v0}, Lcom/google/cast/e;->a(I)Z

    move-result v0

    if-nez v0, :cond_17

    move v2, v1

    .line 732
    goto/16 :goto_1

    .line 735
    :cond_17
    iput-boolean v1, p0, Lcom/google/cast/WebSocket;->t:Z

    .line 736
    iget-boolean v0, p0, Lcom/google/cast/WebSocket;->s:Z

    if-eqz v0, :cond_0

    goto/16 :goto_3

    .line 745
    :pswitch_5
    new-array v0, v5, [B

    iput-object v0, p0, Lcom/google/cast/WebSocket;->v:[B

    .line 746
    iget-object v0, p0, Lcom/google/cast/WebSocket;->j:Lcom/google/cast/e;

    iget-object v4, p0, Lcom/google/cast/WebSocket;->v:[B

    invoke-virtual {v0, v4, v3}, Lcom/google/cast/e;->a([B[B)Z

    move-result v0

    if-nez v0, :cond_18

    move v2, v1

    .line 748
    goto/16 :goto_1

    .line 750
    :cond_18
    iput-boolean v1, p0, Lcom/google/cast/WebSocket;->u:Z

    .line 751
    iput-byte v11, p0, Lcom/google/cast/WebSocket;->r:B

    goto/16 :goto_0

    .line 762
    :cond_19
    iput-byte v11, p0, Lcom/google/cast/WebSocket;->r:B

    goto/16 :goto_0

    .line 773
    :cond_1a
    iget-object v0, p0, Lcom/google/cast/WebSocket;->j:Lcom/google/cast/e;

    invoke-virtual {v0}, Lcom/google/cast/e;->f()V

    goto/16 :goto_2

    :cond_1b
    move v0, v5

    goto :goto_7

    .line 663
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private n()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 863
    iget v0, p0, Lcom/google/cast/WebSocket;->q:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    .line 864
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "not connected; state="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/cast/WebSocket;->q:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 866
    :cond_0
    return-void
.end method


# virtual methods
.method public declared-synchronized a()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 345
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/cast/WebSocket;->q:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 347
    const/4 v0, 0x4

    iput v0, p0, Lcom/google/cast/WebSocket;->q:I

    .line 348
    iget-boolean v0, p0, Lcom/google/cast/WebSocket;->s:Z

    if-nez v0, :cond_0

    .line 349
    const/16 v0, 0x3e8

    invoke-direct {p0, v0}, Lcom/google/cast/WebSocket;->a(S)V

    .line 356
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/cast/WebSocket;->l:Lcom/google/cast/r;

    invoke-virtual {v0}, Lcom/google/cast/r;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 357
    monitor-exit p0

    return-void

    .line 353
    :cond_1
    :try_start_1
    sget-object v0, Lcom/google/cast/WebSocket;->p:Lcom/google/cast/Logger;

    const-string v1, "force-closing socket in disconnect()"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 354
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/cast/WebSocket;->b(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 345
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(I)V
    .locals 1

    .prologue
    .line 365
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/cast/WebSocket;->b(I)V

    .line 366
    iget-object v0, p0, Lcom/google/cast/WebSocket;->l:Lcom/google/cast/r;

    invoke-virtual {v0}, Lcom/google/cast/r;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 367
    monitor-exit p0

    return-void

    .line 365
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Landroid/net/Uri;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 271
    const/16 v0, 0x1388

    invoke-virtual {p0, p1, v0}, Lcom/google/cast/WebSocket;->a(Landroid/net/Uri;I)V

    .line 272
    return-void
.end method

.method public declared-synchronized a(Landroid/net/Uri;I)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v6, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 282
    monitor-enter p0

    :try_start_0
    iget-object v3, p0, Lcom/google/cast/WebSocket;->g:Ljava/nio/channels/SocketChannel;

    if-eqz v3, :cond_0

    .line 283
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "already connected"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 282
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 286
    :cond_0
    :try_start_1
    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    const-string v4, "ws"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 287
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "scheme must be ws"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 290
    :cond_1
    const/16 v3, 0x3e8

    if-lt p2, v3, :cond_4

    int-to-long v4, p2

    :goto_0
    iput-wide v4, p0, Lcom/google/cast/WebSocket;->z:J

    .line 292
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/cast/WebSocket;->y:J

    .line 293
    iput-object p1, p0, Lcom/google/cast/WebSocket;->b:Landroid/net/Uri;

    .line 294
    invoke-static {}, Ljava/nio/channels/SocketChannel;->open()Ljava/nio/channels/SocketChannel;

    move-result-object v3

    iput-object v3, p0, Lcom/google/cast/WebSocket;->g:Ljava/nio/channels/SocketChannel;

    .line 295
    iget-object v3, p0, Lcom/google/cast/WebSocket;->g:Ljava/nio/channels/SocketChannel;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/nio/channels/SocketChannel;->configureBlocking(Z)Ljava/nio/channels/SelectableChannel;

    .line 298
    iget-object v3, p0, Lcom/google/cast/WebSocket;->b:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->getPort()I

    move-result v3

    iput v3, p0, Lcom/google/cast/WebSocket;->c:I

    .line 299
    iget v3, p0, Lcom/google/cast/WebSocket;->c:I

    if-ne v3, v6, :cond_2

    .line 300
    const/16 v3, 0x50

    iput v3, p0, Lcom/google/cast/WebSocket;->c:I

    .line 303
    :cond_2
    const/4 v3, 0x1

    iput v3, p0, Lcom/google/cast/WebSocket;->q:I

    .line 305
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/16 v4, 0xd

    if-gt v3, v4, :cond_7

    .line 307
    :try_start_2
    iget-object v3, p0, Lcom/google/cast/WebSocket;->b:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/cast/WebSocket;->b(Ljava/lang/String;)Ljava/net/InetAddress;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v2

    move-object v3, v2

    .line 314
    :goto_1
    if-eqz v3, :cond_5

    :try_start_3
    new-instance v2, Ljava/net/InetSocketAddress;

    iget v4, p0, Lcom/google/cast/WebSocket;->c:I

    invoke-direct {v2, v3, v4}, Ljava/net/InetSocketAddress;-><init>(Ljava/net/InetAddress;I)V

    .line 318
    :goto_2
    iget-object v3, p0, Lcom/google/cast/WebSocket;->g:Ljava/nio/channels/SocketChannel;

    invoke-virtual {v3, v2}, Ljava/nio/channels/SocketChannel;->connect(Ljava/net/SocketAddress;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 323
    :goto_3
    new-instance v1, Lcom/google/cast/e;

    iget v2, p0, Lcom/google/cast/WebSocket;->h:I

    invoke-direct {v1, v2}, Lcom/google/cast/e;-><init>(I)V

    iput-object v1, p0, Lcom/google/cast/WebSocket;->j:Lcom/google/cast/e;

    .line 324
    new-instance v1, Lcom/google/cast/e;

    iget v2, p0, Lcom/google/cast/WebSocket;->h:I

    invoke-direct {v1, v2}, Lcom/google/cast/e;-><init>(I)V

    iput-object v1, p0, Lcom/google/cast/WebSocket;->k:Lcom/google/cast/e;

    .line 326
    const/4 v1, -0x1

    iput-byte v1, p0, Lcom/google/cast/WebSocket;->r:B

    .line 327
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/cast/WebSocket;->u:Z

    iput-boolean v1, p0, Lcom/google/cast/WebSocket;->t:Z

    iput-boolean v1, p0, Lcom/google/cast/WebSocket;->s:Z

    .line 328
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/cast/WebSocket;->v:[B

    .line 329
    const/4 v1, 0x0

    iput v1, p0, Lcom/google/cast/WebSocket;->x:I

    .line 331
    iget-object v1, p0, Lcom/google/cast/WebSocket;->l:Lcom/google/cast/r;

    invoke-virtual {v1}, Lcom/google/cast/r;->b()V

    .line 332
    iget-object v1, p0, Lcom/google/cast/WebSocket;->l:Lcom/google/cast/r;

    invoke-virtual {v1, p0}, Lcom/google/cast/r;->a(Lcom/google/cast/WebSocket;)V

    .line 334
    if-eqz v0, :cond_3

    .line 335
    invoke-direct {p0}, Lcom/google/cast/WebSocket;->j()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 337
    :cond_3
    monitor-exit p0

    return-void

    .line 290
    :cond_4
    const-wide/16 v4, 0x3e8

    goto :goto_0

    .line 308
    :catch_0
    move-exception v3

    .line 309
    :try_start_4
    sget-object v3, Lcom/google/cast/WebSocket;->p:Lcom/google/cast/Logger;

    const-string v4, "Unable to create InetAddress object."

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v3, v4, v5}, Lcom/google/cast/Logger;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v3, v2

    .line 310
    goto :goto_1

    .line 314
    :cond_5
    new-instance v2, Ljava/net/InetSocketAddress;

    iget-object v3, p0, Lcom/google/cast/WebSocket;->b:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v3

    iget v4, p0, Lcom/google/cast/WebSocket;->c:I

    invoke-direct {v2, v3, v4}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_2

    :cond_6
    move v0, v1

    goto :goto_3

    :cond_7
    move-object v3, v2

    goto :goto_1
.end method

.method public a(Lcom/google/cast/WebSocket$Listener;)V
    .locals 0

    .prologue
    .line 239
    iput-object p1, p0, Lcom/google/cast/WebSocket;->f:Lcom/google/cast/WebSocket$Listener;

    .line 240
    return-void
.end method

.method public declared-synchronized a(Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 390
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/cast/WebSocket;->n()V

    .line 391
    if-nez p1, :cond_0

    .line 392
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "message cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 390
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 395
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/cast/WebSocket;->m:Ljava/nio/charset/Charset;

    invoke-virtual {p1, v0}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    .line 396
    const/4 v1, 0x1

    const/4 v2, 0x1

    invoke-direct {p0, v1, v0, v2}, Lcom/google/cast/WebSocket;->a(B[BZ)V

    .line 397
    iget-object v0, p0, Lcom/google/cast/WebSocket;->l:Lcom/google/cast/r;

    invoke-virtual {v0}, Lcom/google/cast/r;->c()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 398
    monitor-exit p0

    return-void
.end method

.method declared-synchronized a(Ljava/nio/channels/SelectionKey;J)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 896
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/cast/WebSocket;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-wide v2, p0, Lcom/google/cast/WebSocket;->y:J

    sub-long v2, p2, v2

    iget-wide v4, p0, Lcom/google/cast/WebSocket;->z:J

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    .line 897
    const/4 v1, -0x3

    invoke-direct {p0, v1}, Lcom/google/cast/WebSocket;->b(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 923
    :goto_0
    monitor-exit p0

    return v0

    .line 900
    :cond_0
    :try_start_1
    iget-boolean v2, p0, Lcom/google/cast/WebSocket;->o:Z

    if-eqz v2, :cond_1

    .line 901
    sget-object v1, Lcom/google/cast/WebSocket;->p:Lcom/google/cast/Logger;

    const-string v2, "Socket is no longer connected"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/google/cast/Logger;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 902
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/cast/WebSocket;->o:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 896
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 908
    :cond_1
    :try_start_2
    iget v2, p0, Lcom/google/cast/WebSocket;->q:I

    if-ne v2, v1, :cond_3

    iget-object v2, p0, Lcom/google/cast/WebSocket;->g:Ljava/nio/channels/SocketChannel;

    invoke-virtual {v2}, Ljava/nio/channels/SocketChannel;->isConnected()Z

    move-result v2

    if-nez v2, :cond_3

    .line 910
    const/16 v0, 0x8

    .line 922
    :cond_2
    :goto_1
    invoke-virtual {p1, v0}, Ljava/nio/channels/SelectionKey;->interestOps(I)Ljava/nio/channels/SelectionKey;

    move v0, v1

    .line 923
    goto :goto_0

    .line 911
    :cond_3
    iget v2, p0, Lcom/google/cast/WebSocket;->q:I

    if-eqz v2, :cond_2

    iget v2, p0, Lcom/google/cast/WebSocket;->q:I

    if-eq v2, v1, :cond_2

    .line 913
    iget-object v2, p0, Lcom/google/cast/WebSocket;->j:Lcom/google/cast/e;

    invoke-virtual {v2}, Lcom/google/cast/e;->k()Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 917
    :cond_4
    iget-object v2, p0, Lcom/google/cast/WebSocket;->k:Lcom/google/cast/e;

    invoke-virtual {v2}, Lcom/google/cast/e;->j()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v2

    if-nez v2, :cond_2

    .line 918
    or-int/lit8 v0, v0, 0x4

    goto :goto_1
.end method

.method public declared-synchronized c()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 377
    monitor-enter p0

    :try_start_0
    iget v1, p0, Lcom/google/cast/WebSocket;->q:I

    if-eq v1, v0, :cond_0

    iget v1, p0, Lcom/google/cast/WebSocket;->q:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized f()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 933
    monitor-enter p0

    :try_start_0
    sget-object v1, Lcom/google/cast/WebSocket;->p:Lcom/google/cast/Logger;

    const-string v2, "connectable; starting handshake"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 935
    :try_start_1
    iget-object v1, p0, Lcom/google/cast/WebSocket;->g:Ljava/nio/channels/SocketChannel;

    invoke-virtual {v1}, Ljava/nio/channels/SocketChannel;->finishConnect()Z

    .line 936
    invoke-direct {p0}, Lcom/google/cast/WebSocket;->j()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 937
    const/4 v0, 0x1

    .line 939
    :goto_0
    monitor-exit p0

    return v0

    .line 933
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 938
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method declared-synchronized g()Z
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v2, -0x2

    const/4 v8, 0x4

    const/4 v3, -0x1

    const/4 v0, 0x0

    .line 950
    monitor-enter p0

    :try_start_0
    sget-object v4, Lcom/google/cast/WebSocket;->p:Lcom/google/cast/Logger;

    const-string v5, "readable"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-virtual {v4, v5, v6}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 952
    :try_start_1
    iget-object v4, p0, Lcom/google/cast/WebSocket;->j:Lcom/google/cast/e;

    iget-object v5, p0, Lcom/google/cast/WebSocket;->g:Ljava/nio/channels/SocketChannel;

    invoke-virtual {v4, v5}, Lcom/google/cast/e;->b(Ljava/nio/channels/SocketChannel;)I

    .line 953
    iget v4, p0, Lcom/google/cast/WebSocket;->q:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    .line 954
    invoke-direct {p0}, Lcom/google/cast/WebSocket;->k()Z

    move-result v4

    if-nez v4, :cond_3

    .line 955
    sget-object v1, Lcom/google/cast/WebSocket;->p:Lcom/google/cast/Logger;

    const-string v4, "problem reading opening handshake reply"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v1, v4, v5}, Lcom/google/cast/Logger;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 956
    const/4 v1, -0x1

    invoke-direct {p0, v1}, Lcom/google/cast/WebSocket;->b(I)V
    :try_end_1
    .catch Ljava/nio/channels/ClosedChannelException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 977
    :goto_0
    monitor-exit p0

    return v0

    .line 960
    :cond_0
    :try_start_2
    invoke-direct {p0}, Lcom/google/cast/WebSocket;->m()Z

    move-result v4

    if-nez v4, :cond_3

    .line 961
    sget-object v1, Lcom/google/cast/WebSocket;->p:Lcom/google/cast/Logger;

    const-string v4, "readMessages() returned false"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v1, v4, v5}, Lcom/google/cast/Logger;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 963
    iget v1, p0, Lcom/google/cast/WebSocket;->q:I

    if-ne v1, v8, :cond_2

    move v1, v2

    :goto_1
    invoke-direct {p0, v1}, Lcom/google/cast/WebSocket;->b(I)V
    :try_end_2
    .catch Ljava/nio/channels/ClosedChannelException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 970
    :catch_0
    move-exception v1

    .line 971
    :try_start_3
    sget-object v2, Lcom/google/cast/WebSocket;->p:Lcom/google/cast/Logger;

    const-string v4, "ClosedChannelException when state was %d"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget v7, p0, Lcom/google/cast/WebSocket;->q:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v2, v1, v4, v5}, Lcom/google/cast/Logger;->d(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 972
    iget v1, p0, Lcom/google/cast/WebSocket;->q:I

    if-ne v1, v8, :cond_1

    move v3, v0

    :cond_1
    invoke-direct {p0, v3}, Lcom/google/cast/WebSocket;->b(I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 950
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    move v1, v3

    .line 963
    goto :goto_1

    :cond_3
    move v0, v1

    .line 969
    goto :goto_0

    .line 974
    :catch_1
    move-exception v1

    .line 975
    :try_start_4
    iget v1, p0, Lcom/google/cast/WebSocket;->q:I

    if-ne v1, v8, :cond_4

    move v1, v2

    :goto_2
    invoke-direct {p0, v1}, Lcom/google/cast/WebSocket;->b(I)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    :cond_4
    move v1, v3

    goto :goto_2
.end method

.method declared-synchronized h()Z
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v2, -0x1

    const/4 v8, 0x4

    const/4 v0, 0x0

    .line 988
    monitor-enter p0

    :try_start_0
    sget-object v3, Lcom/google/cast/WebSocket;->p:Lcom/google/cast/Logger;

    const-string v4, "writable"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v3, v4, v5}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 990
    :try_start_1
    iget-object v3, p0, Lcom/google/cast/WebSocket;->k:Lcom/google/cast/e;

    iget-object v4, p0, Lcom/google/cast/WebSocket;->g:Ljava/nio/channels/SocketChannel;

    invoke-virtual {v3, v4}, Lcom/google/cast/e;->a(Ljava/nio/channels/SocketChannel;)I

    .line 991
    iget-object v3, p0, Lcom/google/cast/WebSocket;->k:Lcom/google/cast/e;

    invoke-virtual {v3}, Lcom/google/cast/e;->j()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 992
    iget v3, p0, Lcom/google/cast/WebSocket;->q:I

    if-ne v3, v8, :cond_3

    .line 994
    iget-boolean v3, p0, Lcom/google/cast/WebSocket;->s:Z

    if-nez v3, :cond_1

    .line 997
    const/16 v3, 0x3e8

    invoke-direct {p0, v3}, Lcom/google/cast/WebSocket;->a(S)V
    :try_end_1
    .catch Ljava/nio/channels/ClosedChannelException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    :goto_0
    move v0, v1

    .line 1023
    :goto_1
    monitor-exit p0

    return v0

    .line 998
    :cond_1
    :try_start_2
    iget-boolean v3, p0, Lcom/google/cast/WebSocket;->t:Z

    if-eqz v3, :cond_0

    .line 1003
    sget-object v1, Lcom/google/cast/WebSocket;->p:Lcom/google/cast/Logger;

    const-string v3, "closing handshake complete"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v1, v3, v4}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1004
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/google/cast/WebSocket;->b(I)V
    :try_end_2
    .catch Ljava/nio/channels/ClosedChannelException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 1016
    :catch_0
    move-exception v1

    .line 1017
    :try_start_3
    sget-object v3, Lcom/google/cast/WebSocket;->p:Lcom/google/cast/Logger;

    const-string v4, "ClosedChannelException when state was %d"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget v7, p0, Lcom/google/cast/WebSocket;->q:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v3, v1, v4, v5}, Lcom/google/cast/Logger;->d(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1018
    iget v1, p0, Lcom/google/cast/WebSocket;->q:I

    if-ne v1, v8, :cond_2

    move v2, v0

    :cond_2
    invoke-direct {p0, v2}, Lcom/google/cast/WebSocket;->b(I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 988
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1007
    :cond_3
    :try_start_4
    iget v3, p0, Lcom/google/cast/WebSocket;->q:I

    const/4 v4, 0x3

    if-ne v3, v4, :cond_0

    .line 1009
    iget-boolean v3, p0, Lcom/google/cast/WebSocket;->u:Z

    if-eqz v3, :cond_0

    .line 1010
    invoke-direct {p0}, Lcom/google/cast/WebSocket;->l()V

    .line 1011
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/google/cast/WebSocket;->u:Z
    :try_end_4
    .catch Ljava/nio/channels/ClosedChannelException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 1020
    :catch_1
    move-exception v1

    .line 1021
    :try_start_5
    iget v1, p0, Lcom/google/cast/WebSocket;->q:I

    if-ne v1, v8, :cond_4

    const/4 v1, -0x2

    :goto_2
    invoke-direct {p0, v1}, Lcom/google/cast/WebSocket;->b(I)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_1

    :cond_4
    move v1, v2

    goto :goto_2
.end method

.method declared-synchronized i()Ljava/nio/channels/SocketChannel;
    .locals 1

    .prologue
    .line 1031
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/cast/WebSocket;->g:Ljava/nio/channels/SocketChannel;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
