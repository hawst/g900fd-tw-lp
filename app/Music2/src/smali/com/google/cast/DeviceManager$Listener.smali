.class public interface abstract Lcom/google/cast/DeviceManager$Listener;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/cast/DeviceManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Listener"
.end annotation


# virtual methods
.method public abstract onDeviceOffline(Lcom/google/cast/CastDevice;)V
.end method

.method public abstract onDeviceOnline(Lcom/google/cast/CastDevice;)V
.end method

.method public abstract onScanStateChanged(I)V
.end method
