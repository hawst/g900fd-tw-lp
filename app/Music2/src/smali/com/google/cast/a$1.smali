.class Lcom/google/cast/a$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/cast/NetworkTask$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/cast/a;->a(Lcom/google/cast/CastDevice;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/google/cast/g;

.field final synthetic b:Lcom/google/cast/CastDevice;

.field final synthetic c:Lcom/google/cast/NetworkTask;

.field final synthetic d:Lcom/google/cast/a;


# direct methods
.method constructor <init>(Lcom/google/cast/a;Lcom/google/cast/g;Lcom/google/cast/CastDevice;Lcom/google/cast/NetworkTask;)V
    .locals 0

    .prologue
    .line 185
    iput-object p1, p0, Lcom/google/cast/a$1;->d:Lcom/google/cast/a;

    iput-object p2, p0, Lcom/google/cast/a$1;->a:Lcom/google/cast/g;

    iput-object p3, p0, Lcom/google/cast/a$1;->b:Lcom/google/cast/CastDevice;

    iput-object p4, p0, Lcom/google/cast/a$1;->c:Lcom/google/cast/NetworkTask;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTaskCancelled()V
    .locals 2

    .prologue
    .line 217
    iget-object v0, p0, Lcom/google/cast/a$1;->d:Lcom/google/cast/a;

    invoke-static {v0}, Lcom/google/cast/a;->d(Lcom/google/cast/a;)Ljava/util/LinkedList;

    move-result-object v0

    iget-object v1, p0, Lcom/google/cast/a$1;->c:Lcom/google/cast/NetworkTask;

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 218
    return-void
.end method

.method public onTaskCompleted()V
    .locals 2

    .prologue
    .line 190
    const/4 v0, 0x1

    .line 192
    iget-object v1, p0, Lcom/google/cast/a$1;->d:Lcom/google/cast/a;

    invoke-static {v1}, Lcom/google/cast/a;->a(Lcom/google/cast/a;)Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 193
    iget-object v0, p0, Lcom/google/cast/a$1;->a:Lcom/google/cast/g;

    invoke-virtual {v0}, Lcom/google/cast/g;->a()Lcom/google/cast/ApplicationMetadata;

    move-result-object v0

    .line 194
    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/google/cast/a$1;->d:Lcom/google/cast/a;

    invoke-static {v1}, Lcom/google/cast/a;->a(Lcom/google/cast/a;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/cast/ApplicationMetadata;->areProtocolsSupported(Ljava/util/List;)Z

    move-result v0

    .line 199
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    .line 201
    iget-object v0, p0, Lcom/google/cast/a$1;->d:Lcom/google/cast/a;

    invoke-static {v0}, Lcom/google/cast/a;->b(Lcom/google/cast/a;)Ljava/util/LinkedList;

    move-result-object v0

    iget-object v1, p0, Lcom/google/cast/a$1;->b:Lcom/google/cast/CastDevice;

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 202
    iget-object v0, p0, Lcom/google/cast/a$1;->d:Lcom/google/cast/a;

    invoke-static {v0}, Lcom/google/cast/a;->c(Lcom/google/cast/a;)Lcom/google/cast/DeviceManager$Listener;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 203
    iget-object v0, p0, Lcom/google/cast/a$1;->d:Lcom/google/cast/a;

    invoke-static {v0}, Lcom/google/cast/a;->c(Lcom/google/cast/a;)Lcom/google/cast/DeviceManager$Listener;

    move-result-object v0

    iget-object v1, p0, Lcom/google/cast/a$1;->b:Lcom/google/cast/CastDevice;

    invoke-interface {v0, v1}, Lcom/google/cast/DeviceManager$Listener;->onDeviceOnline(Lcom/google/cast/CastDevice;)V

    .line 206
    :cond_1
    iget-object v0, p0, Lcom/google/cast/a$1;->d:Lcom/google/cast/a;

    invoke-static {v0}, Lcom/google/cast/a;->d(Lcom/google/cast/a;)Ljava/util/LinkedList;

    move-result-object v0

    iget-object v1, p0, Lcom/google/cast/a$1;->c:Lcom/google/cast/NetworkTask;

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 207
    return-void

    .line 194
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onTaskFailed(I)V
    .locals 5
    .param p1, "status"    # I

    .prologue
    .line 211
    invoke-static {}, Lcom/google/cast/a;->a()Lcom/google/cast/Logger;

    move-result-object v0

    const-string v1, "Device does not support app %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/cast/a$1;->d:Lcom/google/cast/a;

    invoke-static {v4}, Lcom/google/cast/a;->e(Lcom/google/cast/a;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 212
    iget-object v0, p0, Lcom/google/cast/a$1;->d:Lcom/google/cast/a;

    invoke-static {v0}, Lcom/google/cast/a;->d(Lcom/google/cast/a;)Ljava/util/LinkedList;

    move-result-object v0

    iget-object v1, p0, Lcom/google/cast/a$1;->c:Lcom/google/cast/NetworkTask;

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 213
    return-void
.end method
