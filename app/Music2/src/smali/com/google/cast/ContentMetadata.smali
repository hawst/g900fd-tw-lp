.class public Lcom/google/cast/ContentMetadata;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Lorg/json/JSONObject;

.field private b:Ljava/lang/String;

.field private c:Landroid/net/Uri;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object v0, p0, Lcom/google/cast/ContentMetadata;->a:Lorg/json/JSONObject;

    .line 19
    iput-object v0, p0, Lcom/google/cast/ContentMetadata;->b:Ljava/lang/String;

    .line 20
    iput-object v0, p0, Lcom/google/cast/ContentMetadata;->c:Landroid/net/Uri;

    .line 21
    return-void
.end method


# virtual methods
.method public getContentInfo()Lorg/json/JSONObject;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/cast/ContentMetadata;->a:Lorg/json/JSONObject;

    return-object v0
.end method

.method public getImageUrl()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/cast/ContentMetadata;->c:Landroid/net/Uri;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/cast/ContentMetadata;->b:Ljava/lang/String;

    return-object v0
.end method

.method public setContentInfo(Lorg/json/JSONObject;)V
    .locals 0
    .param p1, "contentInfo"    # Lorg/json/JSONObject;

    .prologue
    .line 27
    iput-object p1, p0, Lcom/google/cast/ContentMetadata;->a:Lorg/json/JSONObject;

    .line 28
    return-void
.end method

.method public setImageUrl(Landroid/net/Uri;)V
    .locals 0
    .param p1, "imageUrl"    # Landroid/net/Uri;

    .prologue
    .line 41
    iput-object p1, p0, Lcom/google/cast/ContentMetadata;->c:Landroid/net/Uri;

    .line 42
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 34
    iput-object p1, p0, Lcom/google/cast/ContentMetadata;->b:Ljava/lang/String;

    .line 35
    return-void
.end method
