.class public abstract Lcom/google/cast/MessageStream;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/cast/i;

.field private b:Ljava/lang/String;


# direct methods
.method protected constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "namespace"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    if-nez p1, :cond_0

    .line 33
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Namespace cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 36
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 37
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 38
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Namespace cannot be an empty string"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 40
    :cond_1
    iput-object v0, p0, Lcom/google/cast/MessageStream;->b:Ljava/lang/String;

    .line 41
    return-void
.end method


# virtual methods
.method a(Lcom/google/cast/i;)V
    .locals 0

    .prologue
    .line 104
    iput-object p1, p0, Lcom/google/cast/MessageStream;->a:Lcom/google/cast/i;

    .line 105
    return-void
.end method

.method public getNamespace()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/cast/MessageStream;->b:Ljava/lang/String;

    return-object v0
.end method

.method public onAttached()V
    .locals 0

    .prologue
    .line 90
    return-void
.end method

.method public onDetached()V
    .locals 0

    .prologue
    .line 97
    return-void
.end method

.method public abstract onMessageReceived(Lorg/json/JSONObject;)V
.end method

.method protected sendMessage(Lorg/json/JSONObject;)V
    .locals 2
    .param p1, "message"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/cast/MessageStream;->a:Lcom/google/cast/i;

    if-nez v0, :cond_0

    .line 61
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not attached to a channel"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 63
    :cond_0
    iget-object v0, p0, Lcom/google/cast/MessageStream;->a:Lcom/google/cast/i;

    iget-object v1, p0, Lcom/google/cast/MessageStream;->b:Ljava/lang/String;

    invoke-interface {v0, v1, p1}, Lcom/google/cast/i;->a(Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 64
    return-void
.end method
