.class Lcom/google/cast/DeviceManager$3;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/cast/NetworkTask$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/cast/DeviceManager;->a(Lcom/google/cast/SsdpScanner$SsdpResponse;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/google/cast/DeviceManager$a;

.field final synthetic b:Lcom/google/cast/DeviceManager;


# direct methods
.method constructor <init>(Lcom/google/cast/DeviceManager;Lcom/google/cast/DeviceManager$a;)V
    .locals 0

    .prologue
    .line 319
    iput-object p1, p0, Lcom/google/cast/DeviceManager$3;->b:Lcom/google/cast/DeviceManager;

    iput-object p2, p0, Lcom/google/cast/DeviceManager$3;->a:Lcom/google/cast/DeviceManager$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTaskCancelled()V
    .locals 3

    .prologue
    .line 349
    iget-object v0, p0, Lcom/google/cast/DeviceManager$3;->b:Lcom/google/cast/DeviceManager;

    invoke-static {v0}, Lcom/google/cast/DeviceManager;->c(Lcom/google/cast/DeviceManager;)Lcom/google/cast/DeviceManager$b;

    move-result-object v1

    monitor-enter v1

    .line 350
    :try_start_0
    iget-object v0, p0, Lcom/google/cast/DeviceManager$3;->b:Lcom/google/cast/DeviceManager;

    invoke-static {v0}, Lcom/google/cast/DeviceManager;->c(Lcom/google/cast/DeviceManager;)Lcom/google/cast/DeviceManager$b;

    move-result-object v0

    iget-object v0, v0, Lcom/google/cast/DeviceManager$b;->a:Ljava/util/List;

    iget-object v2, p0, Lcom/google/cast/DeviceManager$3;->a:Lcom/google/cast/DeviceManager$a;

    invoke-interface {v0, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 351
    monitor-exit v1

    .line 352
    return-void

    .line 351
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onTaskCompleted()V
    .locals 4

    .prologue
    .line 322
    .line 324
    iget-object v0, p0, Lcom/google/cast/DeviceManager$3;->b:Lcom/google/cast/DeviceManager;

    invoke-static {v0}, Lcom/google/cast/DeviceManager;->c(Lcom/google/cast/DeviceManager;)Lcom/google/cast/DeviceManager$b;

    move-result-object v1

    monitor-enter v1

    .line 325
    :try_start_0
    iget-object v0, p0, Lcom/google/cast/DeviceManager$3;->b:Lcom/google/cast/DeviceManager;

    invoke-static {v0}, Lcom/google/cast/DeviceManager;->c(Lcom/google/cast/DeviceManager;)Lcom/google/cast/DeviceManager$b;

    move-result-object v0

    iget-object v0, v0, Lcom/google/cast/DeviceManager$b;->a:Ljava/util/List;

    iget-object v2, p0, Lcom/google/cast/DeviceManager$3;->a:Lcom/google/cast/DeviceManager$a;

    invoke-interface {v0, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 326
    iget-object v0, p0, Lcom/google/cast/DeviceManager$3;->b:Lcom/google/cast/DeviceManager;

    invoke-static {v0}, Lcom/google/cast/DeviceManager;->c(Lcom/google/cast/DeviceManager;)Lcom/google/cast/DeviceManager$b;

    move-result-object v0

    iget-object v0, v0, Lcom/google/cast/DeviceManager$b;->b:Ljava/util/List;

    iget-object v2, p0, Lcom/google/cast/DeviceManager$3;->a:Lcom/google/cast/DeviceManager$a;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 327
    iget-object v0, p0, Lcom/google/cast/DeviceManager$3;->a:Lcom/google/cast/DeviceManager$a;

    const/4 v2, 0x0

    iput-object v2, v0, Lcom/google/cast/DeviceManager$a;->c:Lcom/google/cast/h;

    .line 328
    iget-object v0, p0, Lcom/google/cast/DeviceManager$3;->a:Lcom/google/cast/DeviceManager$a;

    const/4 v2, 0x0

    iput-object v2, v0, Lcom/google/cast/DeviceManager$a;->d:Lcom/google/cast/NetworkTask;

    .line 329
    iget-object v0, p0, Lcom/google/cast/DeviceManager$3;->a:Lcom/google/cast/DeviceManager$a;

    iget-object v2, v0, Lcom/google/cast/DeviceManager$a;->b:Lcom/google/cast/CastDevice;

    .line 330
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 332
    iget-object v0, p0, Lcom/google/cast/DeviceManager$3;->b:Lcom/google/cast/DeviceManager;

    invoke-static {v0}, Lcom/google/cast/DeviceManager;->d(Lcom/google/cast/DeviceManager;)Lcom/google/cast/Logger;

    move-result-object v0

    const-string v1, "onDeviceOnline"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v3}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 334
    iget-object v0, p0, Lcom/google/cast/DeviceManager$3;->b:Lcom/google/cast/DeviceManager;

    invoke-static {v0}, Lcom/google/cast/DeviceManager;->e(Lcom/google/cast/DeviceManager;)Ljava/util/List;

    move-result-object v0

    .line 335
    if-eqz v0, :cond_0

    .line 336
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/cast/DeviceManager$Listener;

    .line 337
    invoke-interface {v0, v2}, Lcom/google/cast/DeviceManager$Listener;->onDeviceOnline(Lcom/google/cast/CastDevice;)V

    goto :goto_0

    .line 330
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 340
    :cond_0
    return-void
.end method

.method public onTaskFailed(I)V
    .locals 2
    .param p1, "result"    # I

    .prologue
    .line 344
    iget-object v0, p0, Lcom/google/cast/DeviceManager$3;->a:Lcom/google/cast/DeviceManager$a;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/cast/DeviceManager$a;->f:Z

    .line 345
    return-void
.end method
