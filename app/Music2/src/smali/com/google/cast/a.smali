.class Lcom/google/cast/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/cast/DeviceManager$Listener;


# static fields
.field private static a:Lcom/google/cast/Logger;


# instance fields
.field private b:Lcom/google/cast/CastContext;

.field private c:Lcom/google/cast/DeviceManager$Listener;

.field private d:Ljava/lang/String;

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/google/cast/CastDevice;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/google/cast/NetworkTask;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 19
    new-instance v0, Lcom/google/cast/Logger;

    const-string v1, "ApplicationSupportFilterListener"

    invoke-direct {v0, v1}, Lcom/google/cast/Logger;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/cast/a;->a:Lcom/google/cast/Logger;

    return-void
.end method

.method public constructor <init>(Lcom/google/cast/CastContext;Ljava/lang/String;Ljava/util/List;Lcom/google/cast/DeviceManager$Listener;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/cast/CastContext;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/google/cast/DeviceManager$Listener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    iput-object p1, p0, Lcom/google/cast/a;->b:Lcom/google/cast/CastContext;

    .line 70
    iput-object p2, p0, Lcom/google/cast/a;->d:Ljava/lang/String;

    .line 71
    iput-object p4, p0, Lcom/google/cast/a;->c:Lcom/google/cast/DeviceManager$Listener;

    .line 72
    if-eqz p3, :cond_0

    invoke-interface {p3}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 p3, 0x0

    :cond_1
    iput-object p3, p0, Lcom/google/cast/a;->e:Ljava/util/List;

    .line 73
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/cast/a;->f:Ljava/util/LinkedList;

    .line 74
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/cast/a;->g:Ljava/util/LinkedList;

    .line 75
    return-void
.end method

.method static synthetic a()Lcom/google/cast/Logger;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/google/cast/a;->a:Lcom/google/cast/Logger;

    return-object v0
.end method

.method static synthetic a(Lcom/google/cast/a;)Ljava/util/List;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lcom/google/cast/a;->e:Ljava/util/List;

    return-object v0
.end method

.method private a(Lcom/google/cast/CastDevice;)V
    .locals 4

    .prologue
    .line 175
    invoke-virtual {p1}, Lcom/google/cast/CastDevice;->getApplicationUrl()Landroid/net/Uri;

    move-result-object v0

    .line 176
    if-nez v0, :cond_0

    .line 222
    :goto_0
    return-void

    .line 182
    :cond_0
    new-instance v1, Lcom/google/cast/g;

    iget-object v2, p0, Lcom/google/cast/a;->b:Lcom/google/cast/CastContext;

    iget-object v3, p0, Lcom/google/cast/a;->d:Ljava/lang/String;

    invoke-direct {v1, v2, v0, v3}, Lcom/google/cast/g;-><init>(Lcom/google/cast/CastContext;Landroid/net/Uri;Ljava/lang/String;)V

    .line 184
    new-instance v0, Lcom/google/cast/NetworkTask;

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/google/cast/NetworkRequest;

    const/4 v3, 0x0

    aput-object v1, v2, v3

    invoke-direct {v0, v2}, Lcom/google/cast/NetworkTask;-><init>([Lcom/google/cast/NetworkRequest;)V

    .line 185
    new-instance v2, Lcom/google/cast/a$1;

    invoke-direct {v2, p0, v1, p1, v0}, Lcom/google/cast/a$1;-><init>(Lcom/google/cast/a;Lcom/google/cast/g;Lcom/google/cast/CastDevice;Lcom/google/cast/NetworkTask;)V

    invoke-virtual {v0, v2}, Lcom/google/cast/NetworkTask;->setListener(Lcom/google/cast/NetworkTask$Listener;)V

    .line 220
    iget-object v1, p0, Lcom/google/cast/a;->g:Ljava/util/LinkedList;

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 221
    invoke-virtual {v0}, Lcom/google/cast/NetworkTask;->execute()V

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/cast/a;)Ljava/util/LinkedList;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lcom/google/cast/a;->f:Ljava/util/LinkedList;

    return-object v0
.end method

.method static synthetic c(Lcom/google/cast/a;)Lcom/google/cast/DeviceManager$Listener;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lcom/google/cast/a;->c:Lcom/google/cast/DeviceManager$Listener;

    return-object v0
.end method

.method static synthetic d(Lcom/google/cast/a;)Ljava/util/LinkedList;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lcom/google/cast/a;->g:Ljava/util/LinkedList;

    return-object v0
.end method

.method static synthetic e(Lcom/google/cast/a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lcom/google/cast/a;->d:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;Ljava/util/List;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 89
    .line 93
    if-nez p1, :cond_3

    move v0, v1

    :goto_0
    iget-object v3, p0, Lcom/google/cast/a;->d:Ljava/lang/String;

    if-nez v3, :cond_4

    move v3, v1

    :goto_1
    if-ne v0, v3, :cond_0

    iget-object v0, p0, Lcom/google/cast/a;->d:Ljava/lang/String;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/google/cast/a;->d:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_c

    .line 95
    :cond_0
    iput-object p1, p0, Lcom/google/cast/a;->d:Ljava/lang/String;

    move v3, v1

    .line 99
    :goto_2
    if-eqz p2, :cond_1

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 100
    const/4 p2, 0x0

    .line 102
    :cond_1
    if-nez p2, :cond_5

    move v0, v1

    :goto_3
    iget-object v4, p0, Lcom/google/cast/a;->e:Ljava/util/List;

    if-nez v4, :cond_2

    move v2, v1

    :cond_2
    if-eq v0, v2, :cond_6

    .line 103
    iput-object p2, p0, Lcom/google/cast/a;->e:Ljava/util/List;

    .line 120
    :goto_4
    if-eqz v1, :cond_a

    .line 121
    iget-object v0, p0, Lcom/google/cast/a;->g:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/cast/NetworkTask;

    .line 122
    invoke-virtual {v0}, Lcom/google/cast/NetworkTask;->cancel()V

    goto :goto_5

    :cond_3
    move v0, v2

    .line 93
    goto :goto_0

    :cond_4
    move v3, v2

    goto :goto_1

    :cond_5
    move v0, v2

    .line 102
    goto :goto_3

    .line 105
    :cond_6
    if-eqz p2, :cond_b

    .line 106
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    iget-object v2, p0, Lcom/google/cast/a;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-eq v0, v2, :cond_7

    .line 107
    iput-object p2, p0, Lcom/google/cast/a;->e:Ljava/util/List;

    goto :goto_4

    .line 110
    :cond_7
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_8
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 111
    iget-object v4, p0, Lcom/google/cast/a;->e:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 112
    iput-object p2, p0, Lcom/google/cast/a;->e:Ljava/util/List;

    goto :goto_4

    .line 124
    :cond_9
    iget-object v0, p0, Lcom/google/cast/a;->f:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 127
    :cond_a
    return v1

    :cond_b
    move v1, v3

    goto :goto_4

    :cond_c
    move v3, v2

    goto :goto_2
.end method

.method public onDeviceOffline(Lcom/google/cast/CastDevice;)V
    .locals 1
    .param p1, "device"    # Lcom/google/cast/CastDevice;

    .prologue
    .line 169
    iget-object v0, p0, Lcom/google/cast/a;->f:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/cast/a;->c:Lcom/google/cast/DeviceManager$Listener;

    if-eqz v0, :cond_0

    .line 170
    iget-object v0, p0, Lcom/google/cast/a;->c:Lcom/google/cast/DeviceManager$Listener;

    invoke-interface {v0, p1}, Lcom/google/cast/DeviceManager$Listener;->onDeviceOffline(Lcom/google/cast/CastDevice;)V

    .line 172
    :cond_0
    return-void
.end method

.method public onDeviceOnline(Lcom/google/cast/CastDevice;)V
    .locals 0
    .param p1, "device"    # Lcom/google/cast/CastDevice;

    .prologue
    .line 160
    invoke-direct {p0, p1}, Lcom/google/cast/a;->a(Lcom/google/cast/CastDevice;)V

    .line 161
    return-void
.end method

.method public onScanStateChanged(I)V
    .locals 2
    .param p1, "state"    # I

    .prologue
    .line 142
    if-eqz p1, :cond_0

    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    .line 144
    :cond_0
    iget-object v0, p0, Lcom/google/cast/a;->g:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/cast/NetworkTask;

    .line 145
    invoke-virtual {v0}, Lcom/google/cast/NetworkTask;->cancel()V

    goto :goto_0

    .line 149
    :cond_1
    iget-object v0, p0, Lcom/google/cast/a;->c:Lcom/google/cast/DeviceManager$Listener;

    if-eqz v0, :cond_2

    .line 150
    iget-object v0, p0, Lcom/google/cast/a;->c:Lcom/google/cast/DeviceManager$Listener;

    invoke-interface {v0, p1}, Lcom/google/cast/DeviceManager$Listener;->onScanStateChanged(I)V

    .line 152
    :cond_2
    return-void
.end method
