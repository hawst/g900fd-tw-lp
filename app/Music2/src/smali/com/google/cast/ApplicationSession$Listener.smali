.class public interface abstract Lcom/google/cast/ApplicationSession$Listener;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/cast/ApplicationSession;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Listener"
.end annotation


# virtual methods
.method public abstract onSessionEnded(Lcom/google/cast/SessionError;)V
.end method

.method public abstract onSessionStartFailed(Lcom/google/cast/SessionError;)V
.end method

.method public abstract onSessionStarted(Lcom/google/cast/ApplicationMetadata;)V
.end method
