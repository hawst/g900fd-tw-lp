.class public final Lcom/google/cast/MediaTrack;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/cast/MediaTrack$Type;
    }
.end annotation


# instance fields
.field private a:J

.field private b:Lcom/google/cast/MediaTrack$Type;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Z


# direct methods
.method constructor <init>(JLcom/google/cast/MediaTrack$Type;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1
    .param p1, "id"    # J
    .param p3, "type"    # Lcom/google/cast/MediaTrack$Type;
    .param p4, "name"    # Ljava/lang/String;
    .param p5, "languageCode"    # Ljava/lang/String;
    .param p6, "enabled"    # Z

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-wide p1, p0, Lcom/google/cast/MediaTrack;->a:J

    .line 34
    iput-object p3, p0, Lcom/google/cast/MediaTrack;->b:Lcom/google/cast/MediaTrack$Type;

    .line 35
    iput-object p4, p0, Lcom/google/cast/MediaTrack;->c:Ljava/lang/String;

    .line 36
    iput-object p5, p0, Lcom/google/cast/MediaTrack;->d:Ljava/lang/String;

    .line 37
    iput-boolean p6, p0, Lcom/google/cast/MediaTrack;->e:Z

    .line 38
    return-void
.end method


# virtual methods
.method a(Z)V
    .locals 0

    .prologue
    .line 76
    iput-boolean p1, p0, Lcom/google/cast/MediaTrack;->e:Z

    .line 77
    return-void
.end method

.method public clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 104
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public isEnabled()Z
    .locals 1

    .prologue
    .line 72
    iget-boolean v0, p0, Lcom/google/cast/MediaTrack;->e:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 84
    new-instance v0, Ljava/lang/StringBuffer;

    const/16 v1, 0x28

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 85
    const-string v1, "[MediaTrack #"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/cast/MediaTrack;->a:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    move-result-object v1

    const/16 v2, 0x20

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p0, Lcom/google/cast/MediaTrack;->b:Lcom/google/cast/MediaTrack$Type;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    .line 89
    iget-object v1, p0, Lcom/google/cast/MediaTrack;->c:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 90
    const-string v1, "; name: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p0, Lcom/google/cast/MediaTrack;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 93
    :cond_0
    iget-object v1, p0, Lcom/google/cast/MediaTrack;->d:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 94
    const-string v1, "; lang: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p0, Lcom/google/cast/MediaTrack;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 97
    :cond_1
    const/16 v1, 0x5d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 99
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
