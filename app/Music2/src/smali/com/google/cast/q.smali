.class final Lcom/google/cast/q;
.super Landroid/support/v7/media/MediaRouteProvider;
.source "SourceFile"


# instance fields
.field private final a:Landroid/support/v7/media/MediaRouteProvider;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/support/v7/media/MediaRouteProvider;)V
    .locals 2

    .prologue
    .line 22
    invoke-direct {p0, p1}, Landroid/support/v7/media/MediaRouteProvider;-><init>(Landroid/content/Context;)V

    .line 23
    iput-object p2, p0, Lcom/google/cast/q;->a:Landroid/support/v7/media/MediaRouteProvider;

    .line 24
    iget-object v0, p0, Lcom/google/cast/q;->a:Landroid/support/v7/media/MediaRouteProvider;

    new-instance v1, Lcom/google/cast/q$1;

    invoke-direct {v1, p0}, Lcom/google/cast/q$1;-><init>(Lcom/google/cast/q;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/media/MediaRouteProvider;->setCallback(Landroid/support/v7/media/MediaRouteProvider$Callback;)V

    .line 31
    iget-object v0, p0, Lcom/google/cast/q;->a:Landroid/support/v7/media/MediaRouteProvider;

    invoke-virtual {v0}, Landroid/support/v7/media/MediaRouteProvider;->getDescriptor()Landroid/support/v7/media/MediaRouteProviderDescriptor;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/cast/q;->setDescriptor(Landroid/support/v7/media/MediaRouteProviderDescriptor;)V

    .line 32
    return-void
.end method


# virtual methods
.method public a()Z
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/google/cast/q;->a:Landroid/support/v7/media/MediaRouteProvider;

    instance-of v0, v0, Lcom/google/cast/j;

    return v0
.end method

.method public onCreateRouteController(Ljava/lang/String;)Landroid/support/v7/media/MediaRouteProvider$RouteController;
    .locals 1
    .param p1, "routeId"    # Ljava/lang/String;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/cast/q;->a:Landroid/support/v7/media/MediaRouteProvider;

    invoke-virtual {v0, p1}, Landroid/support/v7/media/MediaRouteProvider;->onCreateRouteController(Ljava/lang/String;)Landroid/support/v7/media/MediaRouteProvider$RouteController;

    move-result-object v0

    return-object v0
.end method

.method public onDiscoveryRequestChanged(Landroid/support/v7/media/MediaRouteDiscoveryRequest;)V
    .locals 1
    .param p1, "request"    # Landroid/support/v7/media/MediaRouteDiscoveryRequest;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/cast/q;->a:Landroid/support/v7/media/MediaRouteProvider;

    invoke-virtual {v0, p1}, Landroid/support/v7/media/MediaRouteProvider;->setDiscoveryRequest(Landroid/support/v7/media/MediaRouteDiscoveryRequest;)V

    .line 42
    return-void
.end method
