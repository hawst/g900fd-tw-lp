.class Lcom/google/cast/ApplicationChannel$2;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/cast/ApplicationChannel;-><init>(Lcom/google/cast/CastContext;IJLcom/google/cast/ApplicationChannel$a;Landroid/os/Handler;Lcom/google/cast/CastDevice;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/google/cast/ApplicationChannel;


# direct methods
.method constructor <init>(Lcom/google/cast/ApplicationChannel;)V
    .locals 0

    .prologue
    .line 137
    iput-object p1, p0, Lcom/google/cast/ApplicationChannel$2;->a:Lcom/google/cast/ApplicationChannel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 141
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 142
    const-string v1, "type"

    const-string v2, "pong"

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 143
    iget-object v1, p0, Lcom/google/cast/ApplicationChannel$2;->a:Lcom/google/cast/ApplicationChannel;

    const-string v2, "cm"

    invoke-static {v1, v2, v0}, Lcom/google/cast/ApplicationChannel;->a(Lcom/google/cast/ApplicationChannel;Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 151
    iget-object v0, p0, Lcom/google/cast/ApplicationChannel$2;->a:Lcom/google/cast/ApplicationChannel;

    invoke-static {v0}, Lcom/google/cast/ApplicationChannel;->b(Lcom/google/cast/ApplicationChannel;)V

    .line 153
    :goto_0
    return-void

    .line 144
    :catch_0
    move-exception v0

    .line 151
    iget-object v0, p0, Lcom/google/cast/ApplicationChannel$2;->a:Lcom/google/cast/ApplicationChannel;

    invoke-static {v0}, Lcom/google/cast/ApplicationChannel;->b(Lcom/google/cast/ApplicationChannel;)V

    goto :goto_0

    .line 146
    :catch_1
    move-exception v0

    .line 147
    :try_start_1
    iget-object v0, p0, Lcom/google/cast/ApplicationChannel$2;->a:Lcom/google/cast/ApplicationChannel;

    const/4 v1, -0x1

    invoke-static {v0, v1}, Lcom/google/cast/ApplicationChannel;->a(Lcom/google/cast/ApplicationChannel;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 151
    iget-object v0, p0, Lcom/google/cast/ApplicationChannel$2;->a:Lcom/google/cast/ApplicationChannel;

    invoke-static {v0}, Lcom/google/cast/ApplicationChannel;->b(Lcom/google/cast/ApplicationChannel;)V

    goto :goto_0

    .line 148
    :catch_2
    move-exception v0

    .line 151
    iget-object v0, p0, Lcom/google/cast/ApplicationChannel$2;->a:Lcom/google/cast/ApplicationChannel;

    invoke-static {v0}, Lcom/google/cast/ApplicationChannel;->b(Lcom/google/cast/ApplicationChannel;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/cast/ApplicationChannel$2;->a:Lcom/google/cast/ApplicationChannel;

    invoke-static {v1}, Lcom/google/cast/ApplicationChannel;->b(Lcom/google/cast/ApplicationChannel;)V

    throw v0
.end method
