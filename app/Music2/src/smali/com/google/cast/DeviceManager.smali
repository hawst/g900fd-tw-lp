.class Lcom/google/cast/DeviceManager;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/cast/DeviceManager$b;,
        Lcom/google/cast/DeviceManager$a;,
        Lcom/google/cast/DeviceManager$Listener;
    }
.end annotation


# static fields
.field private static final a:I

.field private static final b:I

.field private static final c:I

.field private static final d:I


# instance fields
.field private e:Lcom/google/cast/CastContext;

.field private f:Lcom/google/cast/DeviceManager$b;

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/cast/DeviceManager$Listener;",
            ">;"
        }
    .end annotation
.end field

.field private h:Lcom/google/cast/SsdpScanner;

.field private i:Landroid/os/Handler;

.field private j:Ljava/lang/Runnable;

.field private k:I

.field private l:Lcom/google/cast/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 37
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v0

    long-to-int v0, v0

    sput v0, Lcom/google/cast/DeviceManager;->a:I

    .line 38
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    long-to-int v0, v0

    sput v0, Lcom/google/cast/DeviceManager;->b:I

    .line 40
    sget v0, Lcom/google/cast/DeviceManager;->b:I

    mul-int/lit8 v0, v0, 0xa

    sput v0, Lcom/google/cast/DeviceManager;->c:I

    .line 41
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x3

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    long-to-int v0, v0

    sput v0, Lcom/google/cast/DeviceManager;->d:I

    return-void
.end method

.method public constructor <init>(Lcom/google/cast/CastContext;)V
    .locals 2
    .param p1, "castContext"    # Lcom/google/cast/CastContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/cast/DeviceManager;->k:I

    .line 91
    new-instance v0, Lcom/google/cast/Logger;

    const-string v1, "DeviceManager"

    invoke-direct {v0, v1}, Lcom/google/cast/Logger;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/cast/DeviceManager;->l:Lcom/google/cast/Logger;

    .line 92
    if-nez p1, :cond_0

    .line 93
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "castContext cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 95
    :cond_0
    iput-object p1, p0, Lcom/google/cast/DeviceManager;->e:Lcom/google/cast/CastContext;

    .line 96
    new-instance v0, Lcom/google/cast/DeviceManager$b;

    invoke-direct {v0, p0}, Lcom/google/cast/DeviceManager$b;-><init>(Lcom/google/cast/DeviceManager;)V

    iput-object v0, p0, Lcom/google/cast/DeviceManager;->f:Lcom/google/cast/DeviceManager$b;

    .line 97
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/cast/DeviceManager;->g:Ljava/util/List;

    .line 98
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/cast/DeviceManager;->i:Landroid/os/Handler;

    .line 99
    new-instance v0, Lcom/google/cast/DeviceManager$1;

    invoke-direct {v0, p0}, Lcom/google/cast/DeviceManager$1;-><init>(Lcom/google/cast/DeviceManager;)V

    iput-object v0, p0, Lcom/google/cast/DeviceManager;->j:Ljava/lang/Runnable;

    .line 105
    return-void
.end method

.method private a(Ljava/lang/String;)Ljava/net/Inet4Address;
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v1, 0x0

    .line 454
    const-string v0, "\\."

    invoke-static {p1, v0}, Landroid/text/TextUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 455
    array-length v0, v2

    if-eq v0, v5, :cond_0

    move-object v0, v1

    .line 478
    :goto_0
    return-object v0

    .line 459
    :cond_0
    new-array v3, v5, [B

    .line 460
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v5, :cond_1

    .line 462
    :try_start_0
    aget-object v4, v2, v0

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    int-to-byte v4, v4

    aput-byte v4, v3, v0
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 460
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 463
    :catch_0
    move-exception v0

    move-object v0, v1

    .line 464
    goto :goto_0

    .line 470
    :cond_1
    :try_start_1
    invoke-static {v3}, Ljava/net/InetAddress;->getByAddress([B)Ljava/net/InetAddress;
    :try_end_1
    .catch Ljava/net/UnknownHostException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    .line 474
    instance-of v2, v0, Ljava/net/Inet4Address;

    if-nez v2, :cond_2

    move-object v0, v1

    .line 475
    goto :goto_0

    .line 471
    :catch_1
    move-exception v0

    move-object v0, v1

    .line 472
    goto :goto_0

    .line 478
    :cond_2
    check-cast v0, Ljava/net/Inet4Address;

    goto :goto_0
.end method

.method private a(I)V
    .locals 5

    .prologue
    .line 421
    iget v0, p0, Lcom/google/cast/DeviceManager;->k:I

    if-ne v0, p1, :cond_1

    .line 437
    :cond_0
    :goto_0
    return-void

    .line 424
    :cond_1
    iget-object v0, p0, Lcom/google/cast/DeviceManager;->l:Lcom/google/cast/Logger;

    const-string v1, "notifyStateChanged: %d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 425
    iput p1, p0, Lcom/google/cast/DeviceManager;->k:I

    .line 426
    invoke-direct {p0}, Lcom/google/cast/DeviceManager;->g()Ljava/util/List;

    move-result-object v0

    .line 427
    if-eqz v0, :cond_0

    .line 428
    iget-object v1, p0, Lcom/google/cast/DeviceManager;->i:Landroid/os/Handler;

    new-instance v2, Lcom/google/cast/DeviceManager$4;

    invoke-direct {v2, p0, v0, p1}, Lcom/google/cast/DeviceManager$4;-><init>(Lcom/google/cast/DeviceManager;Ljava/util/List;I)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/cast/DeviceManager;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/google/cast/DeviceManager;->e()V

    return-void
.end method

.method static synthetic a(Lcom/google/cast/DeviceManager;I)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcom/google/cast/DeviceManager;->a(I)V

    return-void
.end method

.method static synthetic a(Lcom/google/cast/DeviceManager;Lcom/google/cast/SsdpScanner$SsdpResponse;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcom/google/cast/DeviceManager;->a(Lcom/google/cast/SsdpScanner$SsdpResponse;)V

    return-void
.end method

.method private a(Lcom/google/cast/SsdpScanner$SsdpResponse;)V
    .locals 11

    .prologue
    .line 268
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 270
    invoke-virtual {p1}, Lcom/google/cast/SsdpScanner$SsdpResponse;->getLocation()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    .line 273
    invoke-virtual {v6}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/cast/DeviceManager;->a(Ljava/lang/String;)Ljava/net/Inet4Address;

    move-result-object v3

    .line 274
    if-nez v3, :cond_0

    .line 358
    :goto_0
    return-void

    .line 279
    :cond_0
    iget-object v7, p0, Lcom/google/cast/DeviceManager;->f:Lcom/google/cast/DeviceManager$b;

    monitor-enter v7

    .line 281
    :try_start_0
    iget-object v2, p0, Lcom/google/cast/DeviceManager;->f:Lcom/google/cast/DeviceManager$b;

    iget-object v2, v2, Lcom/google/cast/DeviceManager$b;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/cast/DeviceManager$a;

    .line 282
    iget-object v9, v2, Lcom/google/cast/DeviceManager$a;->a:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/cast/SsdpScanner$SsdpResponse;->getUsn()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 283
    iget-boolean v6, v2, Lcom/google/cast/DeviceManager$a;->f:Z

    if-nez v6, :cond_2

    .line 284
    iget-object v6, v2, Lcom/google/cast/DeviceManager$a;->b:Lcom/google/cast/CastDevice;

    invoke-virtual {v6}, Lcom/google/cast/CastDevice;->getIpAddress()Ljava/net/InetAddress;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/net/InetAddress;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 285
    iput-wide v4, v2, Lcom/google/cast/DeviceManager$a;->e:J

    .line 291
    :cond_2
    :goto_1
    monitor-exit v7

    goto :goto_0

    .line 357
    :catchall_0
    move-exception v2

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 288
    :cond_3
    const/4 v3, 0x1

    :try_start_1
    iput-boolean v3, v2, Lcom/google/cast/DeviceManager$a;->f:Z

    goto :goto_1

    .line 296
    :cond_4
    iget-object v2, p0, Lcom/google/cast/DeviceManager;->f:Lcom/google/cast/DeviceManager$b;

    iget-object v2, v2, Lcom/google/cast/DeviceManager$b;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_5
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/cast/DeviceManager$a;

    .line 297
    iget-object v9, v2, Lcom/google/cast/DeviceManager$a;->a:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/cast/SsdpScanner$SsdpResponse;->getUsn()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 298
    iget-boolean v6, v2, Lcom/google/cast/DeviceManager$a;->f:Z

    if-nez v6, :cond_6

    .line 299
    iget-object v6, v2, Lcom/google/cast/DeviceManager$a;->b:Lcom/google/cast/CastDevice;

    invoke-virtual {v6}, Lcom/google/cast/CastDevice;->getIpAddress()Ljava/net/InetAddress;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/net/InetAddress;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 300
    iput-wide v4, v2, Lcom/google/cast/DeviceManager$a;->e:J

    .line 306
    :cond_6
    :goto_2
    monitor-exit v7

    goto :goto_0

    .line 303
    :cond_7
    const/4 v3, 0x1

    iput-boolean v3, v2, Lcom/google/cast/DeviceManager$a;->f:Z

    goto :goto_2

    .line 311
    :cond_8
    new-instance v8, Lcom/google/cast/DeviceManager$a;

    const/4 v2, 0x0

    invoke-direct {v8, p0, v2}, Lcom/google/cast/DeviceManager$a;-><init>(Lcom/google/cast/DeviceManager;Lcom/google/cast/DeviceManager$1;)V

    .line 313
    new-instance v9, Lcom/google/cast/CastDevice;

    move-object v0, v3

    check-cast v0, Ljava/net/Inet4Address;

    move-object v2, v0

    invoke-direct {v9, v2}, Lcom/google/cast/CastDevice;-><init>(Ljava/net/Inet4Address;)V

    iput-object v9, v8, Lcom/google/cast/DeviceManager$a;->b:Lcom/google/cast/CastDevice;

    .line 314
    invoke-virtual {p1}, Lcom/google/cast/SsdpScanner$SsdpResponse;->getUsn()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v8, Lcom/google/cast/DeviceManager$a;->a:Ljava/lang/String;

    .line 315
    iput-wide v4, v8, Lcom/google/cast/DeviceManager$a;->e:J

    .line 316
    new-instance v2, Lcom/google/cast/h;

    iget-object v3, p0, Lcom/google/cast/DeviceManager;->e:Lcom/google/cast/CastContext;

    iget-object v4, v8, Lcom/google/cast/DeviceManager$a;->b:Lcom/google/cast/CastDevice;

    invoke-direct {v2, v3, v6, v4}, Lcom/google/cast/h;-><init>(Lcom/google/cast/CastContext;Landroid/net/Uri;Lcom/google/cast/CastDevice;)V

    iput-object v2, v8, Lcom/google/cast/DeviceManager$a;->c:Lcom/google/cast/h;

    .line 318
    new-instance v2, Lcom/google/cast/NetworkTask;

    const/4 v3, 0x1

    new-array v3, v3, [Lcom/google/cast/NetworkRequest;

    const/4 v4, 0x0

    iget-object v5, v8, Lcom/google/cast/DeviceManager$a;->c:Lcom/google/cast/h;

    aput-object v5, v3, v4

    invoke-direct {v2, v3}, Lcom/google/cast/NetworkTask;-><init>([Lcom/google/cast/NetworkRequest;)V

    iput-object v2, v8, Lcom/google/cast/DeviceManager$a;->d:Lcom/google/cast/NetworkTask;

    .line 319
    iget-object v2, v8, Lcom/google/cast/DeviceManager$a;->d:Lcom/google/cast/NetworkTask;

    new-instance v3, Lcom/google/cast/DeviceManager$3;

    invoke-direct {v3, p0, v8}, Lcom/google/cast/DeviceManager$3;-><init>(Lcom/google/cast/DeviceManager;Lcom/google/cast/DeviceManager$a;)V

    invoke-virtual {v2, v3}, Lcom/google/cast/NetworkTask;->setListener(Lcom/google/cast/NetworkTask$Listener;)V

    .line 355
    iget-object v2, p0, Lcom/google/cast/DeviceManager;->f:Lcom/google/cast/DeviceManager$b;

    iget-object v2, v2, Lcom/google/cast/DeviceManager$b;->a:Ljava/util/List;

    invoke-interface {v2, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 356
    iget-object v2, v8, Lcom/google/cast/DeviceManager$a;->d:Lcom/google/cast/NetworkTask;

    invoke-virtual {v2}, Lcom/google/cast/NetworkTask;->execute()V

    .line 357
    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0
.end method

.method static synthetic b(Lcom/google/cast/DeviceManager;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/google/cast/DeviceManager;->f()V

    return-void
.end method

.method static synthetic c()I
    .locals 1

    .prologue
    .line 34
    sget v0, Lcom/google/cast/DeviceManager;->c:I

    return v0
.end method

.method static synthetic c(Lcom/google/cast/DeviceManager;)Lcom/google/cast/DeviceManager$b;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/cast/DeviceManager;->f:Lcom/google/cast/DeviceManager$b;

    return-object v0
.end method

.method static synthetic d(Lcom/google/cast/DeviceManager;)Lcom/google/cast/Logger;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/cast/DeviceManager;->l:Lcom/google/cast/Logger;

    return-object v0
.end method

.method private d()V
    .locals 4

    .prologue
    .line 206
    iget-object v0, p0, Lcom/google/cast/DeviceManager;->h:Lcom/google/cast/SsdpScanner;

    if-nez v0, :cond_0

    .line 221
    :goto_0
    return-void

    .line 209
    :cond_0
    iget-object v0, p0, Lcom/google/cast/DeviceManager;->h:Lcom/google/cast/SsdpScanner;

    invoke-virtual {v0}, Lcom/google/cast/SsdpScanner;->b()V

    .line 211
    iget-object v0, p0, Lcom/google/cast/DeviceManager;->i:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 213
    iget-object v1, p0, Lcom/google/cast/DeviceManager;->f:Lcom/google/cast/DeviceManager$b;

    monitor-enter v1

    .line 214
    :try_start_0
    iget-object v0, p0, Lcom/google/cast/DeviceManager;->f:Lcom/google/cast/DeviceManager$b;

    iget-object v0, v0, Lcom/google/cast/DeviceManager$b;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/cast/DeviceManager$a;

    .line 215
    iget-object v3, v0, Lcom/google/cast/DeviceManager$a;->d:Lcom/google/cast/NetworkTask;

    if-eqz v3, :cond_1

    .line 216
    iget-object v0, v0, Lcom/google/cast/DeviceManager$a;->d:Lcom/google/cast/NetworkTask;

    invoke-virtual {v0}, Lcom/google/cast/NetworkTask;->cancel()V

    goto :goto_1

    .line 220
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 219
    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/google/cast/DeviceManager;->f:Lcom/google/cast/DeviceManager$b;

    iget-object v0, v0, Lcom/google/cast/DeviceManager$b;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 220
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method static synthetic e(Lcom/google/cast/DeviceManager;)Ljava/util/List;
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/google/cast/DeviceManager;->g()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private e()V
    .locals 7

    .prologue
    .line 361
    const/4 v1, 0x0

    .line 362
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 363
    iget-object v4, p0, Lcom/google/cast/DeviceManager;->f:Lcom/google/cast/DeviceManager$b;

    monitor-enter v4

    .line 364
    :try_start_0
    iget-object v0, p0, Lcom/google/cast/DeviceManager;->f:Lcom/google/cast/DeviceManager$b;

    iget-object v0, v0, Lcom/google/cast/DeviceManager$b;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .line 365
    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 366
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/cast/DeviceManager$a;

    .line 368
    invoke-virtual {v0, v2, v3}, Lcom/google/cast/DeviceManager$a;->a(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 369
    invoke-interface {v5}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 385
    :catchall_0
    move-exception v0

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 373
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/cast/DeviceManager;->f:Lcom/google/cast/DeviceManager$b;

    iget-object v0, v0, Lcom/google/cast/DeviceManager$b;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .line 374
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 375
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/cast/DeviceManager$a;

    .line 377
    iget-boolean v6, v0, Lcom/google/cast/DeviceManager$a;->f:Z

    if-nez v6, :cond_2

    invoke-virtual {v0, v2, v3}, Lcom/google/cast/DeviceManager$a;->a(J)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 378
    :cond_2
    if-nez v1, :cond_3

    .line 379
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 381
    :cond_3
    iget-object v0, v0, Lcom/google/cast/DeviceManager$a;->b:Lcom/google/cast/CastDevice;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 382
    invoke-interface {v5}, Ljava/util/Iterator;->remove()V

    :cond_4
    move-object v0, v1

    move-object v1, v0

    .line 384
    goto :goto_1

    .line 385
    :cond_5
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 387
    if-eqz v1, :cond_7

    .line 388
    invoke-direct {p0}, Lcom/google/cast/DeviceManager;->g()Ljava/util/List;

    move-result-object v2

    .line 389
    if-eqz v2, :cond_7

    .line 390
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_6
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/google/cast/CastDevice;

    .line 391
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/cast/DeviceManager$Listener;

    .line 392
    invoke-interface {v0, v1}, Lcom/google/cast/DeviceManager$Listener;->onDeviceOffline(Lcom/google/cast/CastDevice;)V

    goto :goto_2

    .line 398
    :cond_7
    iget-object v0, p0, Lcom/google/cast/DeviceManager;->i:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/cast/DeviceManager;->j:Ljava/lang/Runnable;

    sget v2, Lcom/google/cast/DeviceManager;->d:I

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 399
    return-void
.end method

.method private f()V
    .locals 7

    .prologue
    .line 402
    iget-object v0, p0, Lcom/google/cast/DeviceManager;->l:Lcom/google/cast/Logger;

    const-string v1, "flushing device list"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 403
    iget-object v2, p0, Lcom/google/cast/DeviceManager;->f:Lcom/google/cast/DeviceManager$b;

    monitor-enter v2

    .line 404
    :try_start_0
    iget-object v0, p0, Lcom/google/cast/DeviceManager;->f:Lcom/google/cast/DeviceManager$b;

    iget-object v0, v0, Lcom/google/cast/DeviceManager$b;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 406
    iget-object v0, p0, Lcom/google/cast/DeviceManager;->f:Lcom/google/cast/DeviceManager$b;

    iget-object v0, v0, Lcom/google/cast/DeviceManager$b;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 407
    invoke-direct {p0}, Lcom/google/cast/DeviceManager;->g()Ljava/util/List;

    move-result-object v3

    .line 408
    if-eqz v3, :cond_1

    .line 409
    iget-object v0, p0, Lcom/google/cast/DeviceManager;->f:Lcom/google/cast/DeviceManager$b;

    iget-object v0, v0, Lcom/google/cast/DeviceManager$b;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/cast/DeviceManager$a;

    .line 410
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/cast/DeviceManager$Listener;

    .line 411
    iget-object v6, v0, Lcom/google/cast/DeviceManager$a;->b:Lcom/google/cast/CastDevice;

    invoke-interface {v1, v6}, Lcom/google/cast/DeviceManager$Listener;->onDeviceOffline(Lcom/google/cast/CastDevice;)V

    goto :goto_0

    .line 417
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 415
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/cast/DeviceManager;->f:Lcom/google/cast/DeviceManager$b;

    iget-object v0, v0, Lcom/google/cast/DeviceManager$b;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 417
    :cond_2
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 418
    return-void
.end method

.method private g()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/cast/DeviceManager$Listener;",
            ">;"
        }
    .end annotation

    .prologue
    .line 440
    const/4 v0, 0x0

    .line 441
    iget-object v1, p0, Lcom/google/cast/DeviceManager;->g:Ljava/util/List;

    monitor-enter v1

    .line 442
    :try_start_0
    iget-object v2, p0, Lcom/google/cast/DeviceManager;->g:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 443
    new-instance v0, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/cast/DeviceManager;->g:Ljava/util/List;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 445
    :cond_0
    monitor-exit v1

    .line 446
    return-object v0

    .line 445
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public a()V
    .locals 6

    .prologue
    .line 161
    iget-object v0, p0, Lcom/google/cast/DeviceManager;->l:Lcom/google/cast/Logger;

    const-string v1, "startScan"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 162
    invoke-static {}, Lcom/google/cast/p;->a()V

    .line 163
    iget-object v0, p0, Lcom/google/cast/DeviceManager;->h:Lcom/google/cast/SsdpScanner;

    if-nez v0, :cond_0

    .line 164
    new-instance v0, Lcom/google/cast/SsdpScanner;

    iget-object v1, p0, Lcom/google/cast/DeviceManager;->e:Lcom/google/cast/CastContext;

    invoke-virtual {v1}, Lcom/google/cast/CastContext;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "urn:dial-multiscreen-org:service:dial:1"

    sget v3, Lcom/google/cast/DeviceManager;->a:I

    sget v4, Lcom/google/cast/DeviceManager;->b:I

    iget-object v5, p0, Lcom/google/cast/DeviceManager;->i:Landroid/os/Handler;

    invoke-direct/range {v0 .. v5}, Lcom/google/cast/SsdpScanner;-><init>(Landroid/content/Context;Ljava/lang/String;IILandroid/os/Handler;)V

    iput-object v0, p0, Lcom/google/cast/DeviceManager;->h:Lcom/google/cast/SsdpScanner;

    .line 166
    iget-object v0, p0, Lcom/google/cast/DeviceManager;->h:Lcom/google/cast/SsdpScanner;

    new-instance v1, Lcom/google/cast/DeviceManager$2;

    invoke-direct {v1, p0}, Lcom/google/cast/DeviceManager$2;-><init>(Lcom/google/cast/DeviceManager;)V

    invoke-virtual {v0, v1}, Lcom/google/cast/SsdpScanner;->a(Lcom/google/cast/SsdpScanner$Listener;)V

    .line 184
    :cond_0
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/cast/DeviceManager;->a(I)V

    .line 186
    iget-object v0, p0, Lcom/google/cast/DeviceManager;->h:Lcom/google/cast/SsdpScanner;

    invoke-virtual {v0}, Lcom/google/cast/SsdpScanner;->a()V

    .line 187
    iget-object v0, p0, Lcom/google/cast/DeviceManager;->i:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/cast/DeviceManager;->j:Ljava/lang/Runnable;

    sget v2, Lcom/google/cast/DeviceManager;->d:I

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 188
    return-void
.end method

.method public a(Lcom/google/cast/DeviceManager$Listener;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 114
    if-nez p1, :cond_0

    .line 115
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "listener cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 118
    :cond_0
    iget-object v1, p0, Lcom/google/cast/DeviceManager;->g:Ljava/util/List;

    monitor-enter v1

    .line 119
    :try_start_0
    iget-object v0, p0, Lcom/google/cast/DeviceManager;->g:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 120
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v2, "the same listener cannot be added twice"

    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 123
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 122
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/cast/DeviceManager;->g:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 123
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 124
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 197
    invoke-static {}, Lcom/google/cast/p;->a()V

    .line 198
    invoke-direct {p0}, Lcom/google/cast/DeviceManager;->d()V

    .line 199
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/cast/DeviceManager;->a(I)V

    .line 200
    return-void
.end method
