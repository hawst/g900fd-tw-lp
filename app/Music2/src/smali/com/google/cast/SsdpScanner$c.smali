.class Lcom/google/cast/SsdpScanner$c;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/cast/SsdpScanner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "c"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/util/List",
        "<",
        "Ljava/net/NetworkInterface;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/google/cast/SsdpScanner;


# direct methods
.method private constructor <init>(Lcom/google/cast/SsdpScanner;)V
    .locals 0

    .prologue
    .line 275
    iput-object p1, p0, Lcom/google/cast/SsdpScanner$c;->a:Lcom/google/cast/SsdpScanner;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/cast/SsdpScanner;Lcom/google/cast/SsdpScanner$1;)V
    .locals 0

    .prologue
    .line 275
    invoke-direct {p0, p1}, Lcom/google/cast/SsdpScanner$c;-><init>(Lcom/google/cast/SsdpScanner;)V

    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Void;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/net/NetworkInterface;",
            ">;"
        }
    .end annotation

    .prologue
    .line 279
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 281
    :try_start_0
    invoke-static {}, Ljava/net/NetworkInterface;->getNetworkInterfaces()Ljava/util/Enumeration;

    move-result-object v3

    .line 283
    if-eqz v3, :cond_1

    .line 284
    :cond_0
    invoke-interface {v3}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 285
    invoke-virtual {p0}, Lcom/google/cast/SsdpScanner$c;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 305
    :cond_1
    :goto_0
    return-object v2

    .line 288
    :cond_2
    invoke-interface {v3}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/NetworkInterface;

    .line 289
    invoke-virtual {v0}, Ljava/net/NetworkInterface;->isUp()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/net/NetworkInterface;->isLoopback()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Ljava/net/NetworkInterface;->isPointToPoint()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Ljava/net/NetworkInterface;->supportsMulticast()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 294
    invoke-virtual {v0}, Ljava/net/NetworkInterface;->getInterfaceAddresses()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/net/InterfaceAddress;

    .line 296
    invoke-virtual {v1}, Ljava/net/InterfaceAddress;->getAddress()Ljava/net/InetAddress;

    move-result-object v1

    instance-of v1, v1, Ljava/net/Inet4Address;

    if-eqz v1, :cond_3

    .line 297
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 302
    :catch_0
    move-exception v0

    .line 303
    iget-object v1, p0, Lcom/google/cast/SsdpScanner$c;->a:Lcom/google/cast/SsdpScanner;

    invoke-static {v1}, Lcom/google/cast/SsdpScanner;->b(Lcom/google/cast/SsdpScanner;)Lcom/google/cast/Logger;

    move-result-object v1

    const-string v3, "Exception while selecting network interface"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v3, v4}, Lcom/google/cast/Logger;->d(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method protected a(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/net/NetworkInterface;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 310
    iget-object v0, p0, Lcom/google/cast/SsdpScanner$c;->a:Lcom/google/cast/SsdpScanner;

    invoke-static {v0}, Lcom/google/cast/SsdpScanner;->c(Lcom/google/cast/SsdpScanner;)Lcom/google/cast/SsdpScanner$c;

    move-result-object v0

    if-eq v0, p0, :cond_0

    .line 323
    :goto_0
    return-void

    .line 317
    :cond_0
    if-eqz p1, :cond_1

    .line 318
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/NetworkInterface;

    .line 319
    iget-object v2, p0, Lcom/google/cast/SsdpScanner$c;->a:Lcom/google/cast/SsdpScanner;

    invoke-static {v2}, Lcom/google/cast/SsdpScanner;->b(Lcom/google/cast/SsdpScanner;)Lcom/google/cast/Logger;

    move-result-object v2

    const-string v3, "Multicast using: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v0}, Ljava/net/NetworkInterface;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-virtual {v2, v3, v4}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 322
    :cond_1
    iget-object v0, p0, Lcom/google/cast/SsdpScanner$c;->a:Lcom/google/cast/SsdpScanner;

    invoke-static {v0, p1}, Lcom/google/cast/SsdpScanner;->a(Lcom/google/cast/SsdpScanner;Ljava/util/List;)V

    goto :goto_0
.end method

.method protected b(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/net/NetworkInterface;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 327
    iget-object v0, p0, Lcom/google/cast/SsdpScanner$c;->a:Lcom/google/cast/SsdpScanner;

    invoke-static {v0}, Lcom/google/cast/SsdpScanner;->c(Lcom/google/cast/SsdpScanner;)Lcom/google/cast/SsdpScanner$c;

    move-result-object v0

    if-ne v0, p0, :cond_0

    .line 328
    iget-object v0, p0, Lcom/google/cast/SsdpScanner$c;->a:Lcom/google/cast/SsdpScanner;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/cast/SsdpScanner;->a(Lcom/google/cast/SsdpScanner;Lcom/google/cast/SsdpScanner$c;)Lcom/google/cast/SsdpScanner$c;

    .line 330
    :cond_0
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 275
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/cast/SsdpScanner$c;->a([Ljava/lang/Void;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onCancelled(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 275
    check-cast p1, Ljava/util/List;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/cast/SsdpScanner$c;->b(Ljava/util/List;)V

    return-void
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 275
    check-cast p1, Ljava/util/List;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/cast/SsdpScanner$c;->a(Ljava/util/List;)V

    return-void
.end method
