.class public Lcom/google/cast/ApplicationSession;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/cast/ApplicationSession$Listener;
    }
.end annotation


# instance fields
.field private a:Lcom/google/cast/Logger;

.field private b:Lcom/google/cast/CastContext;

.field private c:Lcom/google/cast/CastDevice;

.field private d:I

.field private e:Lcom/google/cast/ApplicationMetadata;

.field private f:Landroid/net/Uri;

.field private g:Ljava/lang/String;

.field private h:Lcom/google/cast/ApplicationSession$Listener;

.field private i:Lcom/google/cast/ApplicationChannel$a;

.field private j:Lcom/google/cast/ApplicationChannel;

.field private k:Landroid/os/Handler;

.field private l:Lcom/google/cast/n;

.field private m:Lcom/google/cast/NetworkTask$Listener;

.field private n:Lcom/google/cast/NetworkTask;

.field private o:Lcom/google/cast/NetworkTask$Listener;

.field private p:Landroid/os/AsyncTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/AsyncTask",
            "<",
            "Landroid/net/Uri;",
            "Ljava/lang/Void;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private q:Lcom/google/cast/SessionError;

.field private r:I

.field private s:I

.field private t:Z

.field private u:Z

.field private v:Z

.field private w:Z


# direct methods
.method public constructor <init>(Lcom/google/cast/CastContext;Lcom/google/cast/CastDevice;)V
    .locals 1
    .param p1, "castContext"    # Lcom/google/cast/CastContext;
    .param p2, "device"    # Lcom/google/cast/CastDevice;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 139
    const v0, 0x8000

    invoke-direct {p0, p1, p2, v0}, Lcom/google/cast/ApplicationSession;-><init>(Lcom/google/cast/CastContext;Lcom/google/cast/CastDevice;I)V

    .line 140
    return-void
.end method

.method public constructor <init>(Lcom/google/cast/CastContext;Lcom/google/cast/CastDevice;I)V
    .locals 2
    .param p1, "castContext"    # Lcom/google/cast/CastContext;
    .param p2, "device"    # Lcom/google/cast/CastDevice;
    .param p3, "bufferSize"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 151
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    const v0, 0x8000

    iput v0, p0, Lcom/google/cast/ApplicationSession;->d:I

    .line 152
    const/16 v0, 0x1000

    if-ge p3, v0, :cond_0

    .line 153
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "bufferSize too small"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 155
    :cond_0
    if-nez p1, :cond_1

    .line 156
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "castContext cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 158
    :cond_1
    if-nez p2, :cond_2

    .line 159
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "device cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 162
    :cond_2
    new-instance v0, Lcom/google/cast/Logger;

    const-string v1, "ApplicationSession"

    invoke-direct {v0, v1}, Lcom/google/cast/Logger;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/cast/ApplicationSession;->a:Lcom/google/cast/Logger;

    .line 163
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/cast/ApplicationSession;->s:I

    .line 164
    iput-object p1, p0, Lcom/google/cast/ApplicationSession;->b:Lcom/google/cast/CastContext;

    .line 165
    iput-object p2, p0, Lcom/google/cast/ApplicationSession;->c:Lcom/google/cast/CastDevice;

    .line 167
    iput p3, p0, Lcom/google/cast/ApplicationSession;->d:I

    .line 169
    new-instance v0, Lcom/google/cast/ApplicationSession$1;

    invoke-direct {v0, p0}, Lcom/google/cast/ApplicationSession$1;-><init>(Lcom/google/cast/ApplicationSession;)V

    iput-object v0, p0, Lcom/google/cast/ApplicationSession;->m:Lcom/google/cast/NetworkTask$Listener;

    .line 247
    new-instance v0, Lcom/google/cast/ApplicationSession$2;

    invoke-direct {v0, p0}, Lcom/google/cast/ApplicationSession$2;-><init>(Lcom/google/cast/ApplicationSession;)V

    iput-object v0, p0, Lcom/google/cast/ApplicationSession;->o:Lcom/google/cast/NetworkTask$Listener;

    .line 284
    new-instance v0, Lcom/google/cast/ApplicationSession$3;

    invoke-direct {v0, p0}, Lcom/google/cast/ApplicationSession$3;-><init>(Lcom/google/cast/ApplicationSession;)V

    iput-object v0, p0, Lcom/google/cast/ApplicationSession;->i:Lcom/google/cast/ApplicationChannel$a;

    .line 330
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/cast/ApplicationSession;->k:Landroid/os/Handler;

    .line 331
    return-void
.end method

.method static synthetic a(Lcom/google/cast/ApplicationSession;Landroid/net/Uri;)Landroid/net/Uri;
    .locals 0

    .prologue
    .line 32
    iput-object p1, p0, Lcom/google/cast/ApplicationSession;->f:Landroid/net/Uri;

    return-object p1
.end method

.method static synthetic a(Lcom/google/cast/ApplicationSession;Landroid/os/AsyncTask;)Landroid/os/AsyncTask;
    .locals 0

    .prologue
    .line 32
    iput-object p1, p0, Lcom/google/cast/ApplicationSession;->p:Landroid/os/AsyncTask;

    return-object p1
.end method

.method static synthetic a(Lcom/google/cast/ApplicationSession;Lcom/google/cast/ApplicationChannel;)Lcom/google/cast/ApplicationChannel;
    .locals 0

    .prologue
    .line 32
    iput-object p1, p0, Lcom/google/cast/ApplicationSession;->j:Lcom/google/cast/ApplicationChannel;

    return-object p1
.end method

.method static synthetic a(Lcom/google/cast/ApplicationSession;Lcom/google/cast/ApplicationMetadata;)Lcom/google/cast/ApplicationMetadata;
    .locals 0

    .prologue
    .line 32
    iput-object p1, p0, Lcom/google/cast/ApplicationSession;->e:Lcom/google/cast/ApplicationMetadata;

    return-object p1
.end method

.method static synthetic a(Lcom/google/cast/ApplicationSession;Lcom/google/cast/NetworkTask;)Lcom/google/cast/NetworkTask;
    .locals 0

    .prologue
    .line 32
    iput-object p1, p0, Lcom/google/cast/ApplicationSession;->n:Lcom/google/cast/NetworkTask;

    return-object p1
.end method

.method static synthetic a(Lcom/google/cast/ApplicationSession;Lcom/google/cast/n;)Lcom/google/cast/n;
    .locals 0

    .prologue
    .line 32
    iput-object p1, p0, Lcom/google/cast/ApplicationSession;->l:Lcom/google/cast/n;

    return-object p1
.end method

.method static synthetic a(Lcom/google/cast/ApplicationSession;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 32
    iput-object p1, p0, Lcom/google/cast/ApplicationSession;->g:Ljava/lang/String;

    return-object p1
.end method

.method private a()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 503
    iput v0, p0, Lcom/google/cast/ApplicationSession;->s:I

    .line 504
    iput-boolean v0, p0, Lcom/google/cast/ApplicationSession;->w:Z

    .line 505
    iput-boolean v0, p0, Lcom/google/cast/ApplicationSession;->v:Z

    .line 506
    iput-boolean v0, p0, Lcom/google/cast/ApplicationSession;->t:Z

    .line 507
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/cast/ApplicationSession;->q:Lcom/google/cast/SessionError;

    .line 508
    return-void
.end method

.method private a(Landroid/net/Uri;)V
    .locals 3

    .prologue
    .line 663
    new-instance v0, Lcom/google/cast/ApplicationSession$4;

    invoke-direct {v0, p0}, Lcom/google/cast/ApplicationSession$4;-><init>(Lcom/google/cast/ApplicationSession;)V

    iput-object v0, p0, Lcom/google/cast/ApplicationSession;->p:Landroid/os/AsyncTask;

    .line 698
    iget-object v0, p0, Lcom/google/cast/ApplicationSession;->p:Landroid/os/AsyncTask;

    const/4 v1, 0x1

    new-array v1, v1, [Landroid/net/Uri;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 699
    return-void
.end method

.method static synthetic a(Lcom/google/cast/ApplicationSession;Lcom/google/cast/SessionError;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/google/cast/ApplicationSession;->a(Lcom/google/cast/SessionError;)V

    return-void
.end method

.method private a(Lcom/google/cast/SessionError;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 623
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/cast/ApplicationSession;->s:I

    .line 625
    iget-object v0, p0, Lcom/google/cast/ApplicationSession;->q:Lcom/google/cast/SessionError;

    if-nez v0, :cond_0

    .line 626
    iput-object p1, p0, Lcom/google/cast/ApplicationSession;->q:Lcom/google/cast/SessionError;

    .line 630
    :cond_0
    iget-object v0, p0, Lcom/google/cast/ApplicationSession;->j:Lcom/google/cast/ApplicationChannel;

    if-eqz v0, :cond_2

    .line 631
    iget-object v0, p0, Lcom/google/cast/ApplicationSession;->j:Lcom/google/cast/ApplicationChannel;

    invoke-virtual {v0}, Lcom/google/cast/ApplicationChannel;->a()V

    .line 660
    :cond_1
    :goto_0
    return-void

    .line 635
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/cast/ApplicationSession;->e:Lcom/google/cast/ApplicationMetadata;

    .line 638
    iget-boolean v0, p0, Lcom/google/cast/ApplicationSession;->u:Z

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/google/cast/ApplicationSession;->v:Z

    if-nez v0, :cond_3

    .line 639
    iget-object v0, p0, Lcom/google/cast/ApplicationSession;->f:Landroid/net/Uri;

    if-eqz v0, :cond_3

    .line 640
    new-instance v0, Lcom/google/cast/NetworkTask;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/cast/NetworkRequest;

    new-instance v2, Lcom/google/cast/o;

    iget-object v3, p0, Lcom/google/cast/ApplicationSession;->b:Lcom/google/cast/CastContext;

    iget-object v4, p0, Lcom/google/cast/ApplicationSession;->f:Landroid/net/Uri;

    invoke-direct {v2, v3, v4}, Lcom/google/cast/o;-><init>(Lcom/google/cast/CastContext;Landroid/net/Uri;)V

    aput-object v2, v1, v5

    invoke-direct {v0, v1}, Lcom/google/cast/NetworkTask;-><init>([Lcom/google/cast/NetworkRequest;)V

    iput-object v0, p0, Lcom/google/cast/ApplicationSession;->n:Lcom/google/cast/NetworkTask;

    .line 642
    iget-object v0, p0, Lcom/google/cast/ApplicationSession;->n:Lcom/google/cast/NetworkTask;

    iget-object v1, p0, Lcom/google/cast/ApplicationSession;->o:Lcom/google/cast/NetworkTask$Listener;

    invoke-virtual {v0, v1}, Lcom/google/cast/NetworkTask;->setListener(Lcom/google/cast/NetworkTask$Listener;)V

    .line 643
    iget-object v0, p0, Lcom/google/cast/ApplicationSession;->n:Lcom/google/cast/NetworkTask;

    invoke-virtual {v0}, Lcom/google/cast/NetworkTask;->execute()V

    goto :goto_0

    .line 648
    :cond_3
    iput v5, p0, Lcom/google/cast/ApplicationSession;->s:I

    .line 650
    iget-boolean v0, p0, Lcom/google/cast/ApplicationSession;->w:Z

    if-nez v0, :cond_4

    iget-boolean v0, p0, Lcom/google/cast/ApplicationSession;->t:Z

    if-eqz v0, :cond_5

    .line 651
    :cond_4
    iget-object v0, p0, Lcom/google/cast/ApplicationSession;->h:Lcom/google/cast/ApplicationSession$Listener;

    if-eqz v0, :cond_1

    .line 652
    iget-object v0, p0, Lcom/google/cast/ApplicationSession;->h:Lcom/google/cast/ApplicationSession$Listener;

    iget-object v1, p0, Lcom/google/cast/ApplicationSession;->q:Lcom/google/cast/SessionError;

    invoke-interface {v0, v1}, Lcom/google/cast/ApplicationSession$Listener;->onSessionEnded(Lcom/google/cast/SessionError;)V

    goto :goto_0

    .line 656
    :cond_5
    iget-object v0, p0, Lcom/google/cast/ApplicationSession;->h:Lcom/google/cast/ApplicationSession$Listener;

    if-eqz v0, :cond_1

    .line 657
    iget-object v0, p0, Lcom/google/cast/ApplicationSession;->h:Lcom/google/cast/ApplicationSession$Listener;

    iget-object v1, p0, Lcom/google/cast/ApplicationSession;->q:Lcom/google/cast/SessionError;

    invoke-interface {v0, v1}, Lcom/google/cast/ApplicationSession$Listener;->onSessionStartFailed(Lcom/google/cast/SessionError;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/cast/ApplicationSession;)Z
    .locals 1

    .prologue
    .line 32
    iget-boolean v0, p0, Lcom/google/cast/ApplicationSession;->t:Z

    return v0
.end method

.method static synthetic a(Lcom/google/cast/ApplicationSession;Z)Z
    .locals 0

    .prologue
    .line 32
    iput-boolean p1, p0, Lcom/google/cast/ApplicationSession;->v:Z

    return p1
.end method

.method static synthetic b(Lcom/google/cast/ApplicationSession;)Lcom/google/cast/Logger;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/cast/ApplicationSession;->a:Lcom/google/cast/Logger;

    return-object v0
.end method

.method private b()V
    .locals 3

    .prologue
    .line 611
    iget-object v0, p0, Lcom/google/cast/ApplicationSession;->a:Lcom/google/cast/Logger;

    const-string v1, "Notifying listener(s) that startup is complete"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 612
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/cast/ApplicationSession;->w:Z

    .line 613
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/cast/ApplicationSession;->s:I

    .line 614
    iget-object v0, p0, Lcom/google/cast/ApplicationSession;->h:Lcom/google/cast/ApplicationSession$Listener;

    if-eqz v0, :cond_0

    .line 615
    iget-object v0, p0, Lcom/google/cast/ApplicationSession;->h:Lcom/google/cast/ApplicationSession$Listener;

    iget-object v1, p0, Lcom/google/cast/ApplicationSession;->e:Lcom/google/cast/ApplicationMetadata;

    invoke-interface {v0, v1}, Lcom/google/cast/ApplicationSession$Listener;->onSessionStarted(Lcom/google/cast/ApplicationMetadata;)V

    .line 617
    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/google/cast/ApplicationSession;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/google/cast/ApplicationSession;->a(Landroid/net/Uri;)V

    return-void
.end method

.method static synthetic c(Lcom/google/cast/ApplicationSession;)Lcom/google/cast/n;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/cast/ApplicationSession;->l:Lcom/google/cast/n;

    return-object v0
.end method

.method static synthetic d(Lcom/google/cast/ApplicationSession;)I
    .locals 1

    .prologue
    .line 32
    iget v0, p0, Lcom/google/cast/ApplicationSession;->r:I

    return v0
.end method

.method static synthetic e(Lcom/google/cast/ApplicationSession;)Lcom/google/cast/CastContext;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/cast/ApplicationSession;->b:Lcom/google/cast/CastContext;

    return-object v0
.end method

.method static synthetic f(Lcom/google/cast/ApplicationSession;)I
    .locals 1

    .prologue
    .line 32
    iget v0, p0, Lcom/google/cast/ApplicationSession;->d:I

    return v0
.end method

.method static synthetic g(Lcom/google/cast/ApplicationSession;)Lcom/google/cast/ApplicationChannel$a;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/cast/ApplicationSession;->i:Lcom/google/cast/ApplicationChannel$a;

    return-object v0
.end method

.method static synthetic h(Lcom/google/cast/ApplicationSession;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/cast/ApplicationSession;->k:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic i(Lcom/google/cast/ApplicationSession;)Lcom/google/cast/CastDevice;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/cast/ApplicationSession;->c:Lcom/google/cast/CastDevice;

    return-object v0
.end method

.method static synthetic j(Lcom/google/cast/ApplicationSession;)Lcom/google/cast/ApplicationMetadata;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/cast/ApplicationSession;->e:Lcom/google/cast/ApplicationMetadata;

    return-object v0
.end method

.method static synthetic k(Lcom/google/cast/ApplicationSession;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/cast/ApplicationSession;->g:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic l(Lcom/google/cast/ApplicationSession;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/cast/ApplicationSession;->f:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic m(Lcom/google/cast/ApplicationSession;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/google/cast/ApplicationSession;->b()V

    return-void
.end method

.method static synthetic n(Lcom/google/cast/ApplicationSession;)Lcom/google/cast/ApplicationChannel;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/cast/ApplicationSession;->j:Lcom/google/cast/ApplicationChannel;

    return-object v0
.end method


# virtual methods
.method public endSession()Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 560
    invoke-static {}, Lcom/google/cast/p;->a()V

    .line 562
    iput-boolean v1, p0, Lcom/google/cast/ApplicationSession;->t:Z

    .line 564
    iget v2, p0, Lcom/google/cast/ApplicationSession;->s:I

    packed-switch v2, :pswitch_data_0

    move v1, v0

    .line 588
    :cond_0
    :goto_0
    return v1

    .line 567
    :pswitch_0
    iget-object v2, p0, Lcom/google/cast/ApplicationSession;->p:Landroid/os/AsyncTask;

    if-eqz v2, :cond_1

    .line 568
    iget-object v0, p0, Lcom/google/cast/ApplicationSession;->p:Landroid/os/AsyncTask;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->cancel(Z)Z

    move v0, v1

    .line 572
    :cond_1
    iget-object v2, p0, Lcom/google/cast/ApplicationSession;->l:Lcom/google/cast/n;

    if-eqz v2, :cond_2

    .line 573
    iget-object v0, p0, Lcom/google/cast/ApplicationSession;->l:Lcom/google/cast/n;

    invoke-virtual {v0, v1}, Lcom/google/cast/n;->cancel(Z)Z

    move v0, v1

    .line 577
    :cond_2
    if-nez v0, :cond_0

    .line 578
    invoke-direct {p0, v3}, Lcom/google/cast/ApplicationSession;->a(Lcom/google/cast/SessionError;)V

    goto :goto_0

    .line 583
    :pswitch_1
    invoke-direct {p0, v3}, Lcom/google/cast/ApplicationSession;->a(Lcom/google/cast/SessionError;)V

    goto :goto_0

    .line 564
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getChannel()Lcom/google/cast/ApplicationChannel;
    .locals 1

    .prologue
    .line 539
    iget-object v0, p0, Lcom/google/cast/ApplicationSession;->j:Lcom/google/cast/ApplicationChannel;

    return-object v0
.end method

.method public isResumable()Z
    .locals 1

    .prologue
    .line 495
    iget v0, p0, Lcom/google/cast/ApplicationSession;->s:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/cast/ApplicationSession;->f:Landroid/net/Uri;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/cast/ApplicationSession;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isStarting()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 600
    iget v1, p0, Lcom/google/cast/ApplicationSession;->s:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public resumeSession()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 468
    invoke-static {}, Lcom/google/cast/p;->a()V

    .line 470
    iget v0, p0, Lcom/google/cast/ApplicationSession;->s:I

    if-eqz v0, :cond_0

    .line 471
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Session is not currently stopped"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 473
    :cond_0
    iget-object v0, p0, Lcom/google/cast/ApplicationSession;->f:Landroid/net/Uri;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/cast/ApplicationSession;->g:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 474
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No previous session to resume"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 477
    :cond_2
    invoke-direct {p0}, Lcom/google/cast/ApplicationSession;->a()V

    .line 478
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/cast/ApplicationSession;->s:I

    .line 480
    new-instance v0, Lcom/google/cast/n;

    iget-object v1, p0, Lcom/google/cast/ApplicationSession;->b:Lcom/google/cast/CastContext;

    iget-object v2, p0, Lcom/google/cast/ApplicationSession;->c:Lcom/google/cast/CastDevice;

    invoke-virtual {v2}, Lcom/google/cast/CastDevice;->getApplicationUrl()Landroid/net/Uri;

    move-result-object v2

    iget-object v3, p0, Lcom/google/cast/ApplicationSession;->g:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/cast/ApplicationSession;->f:Landroid/net/Uri;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/cast/n;-><init>(Lcom/google/cast/CastContext;Landroid/net/Uri;Ljava/lang/String;Landroid/net/Uri;)V

    iput-object v0, p0, Lcom/google/cast/ApplicationSession;->l:Lcom/google/cast/n;

    .line 482
    iget-object v0, p0, Lcom/google/cast/ApplicationSession;->l:Lcom/google/cast/n;

    iget-object v1, p0, Lcom/google/cast/ApplicationSession;->m:Lcom/google/cast/NetworkTask$Listener;

    invoke-virtual {v0, v1}, Lcom/google/cast/n;->setListener(Lcom/google/cast/NetworkTask$Listener;)V

    .line 483
    iget-object v0, p0, Lcom/google/cast/ApplicationSession;->l:Lcom/google/cast/n;

    invoke-virtual {v0}, Lcom/google/cast/n;->execute()V

    .line 484
    return-void
.end method

.method public setApplicationOptions(I)V
    .locals 3
    .param p1, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 346
    const/4 v0, 0x7

    if-gt p1, v0, :cond_0

    if-gez p1, :cond_1

    .line 347
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "invalid flags: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 349
    :cond_1
    iput p1, p0, Lcom/google/cast/ApplicationSession;->r:I

    .line 350
    return-void
.end method

.method public setListener(Lcom/google/cast/ApplicationSession$Listener;)V
    .locals 0
    .param p1, "listener"    # Lcom/google/cast/ApplicationSession$Listener;

    .prologue
    .line 388
    iput-object p1, p0, Lcom/google/cast/ApplicationSession;->h:Lcom/google/cast/ApplicationSession$Listener;

    .line 389
    return-void
.end method

.method public setStopApplicationWhenEnding(Z)V
    .locals 2
    .param p1, "stopApplication"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 378
    iget v0, p0, Lcom/google/cast/ApplicationSession;->s:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 379
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Call is not allowed while session is stopping"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 381
    :cond_0
    iput-boolean p1, p0, Lcom/google/cast/ApplicationSession;->u:Z

    .line 382
    return-void
.end method

.method public startSession(Ljava/lang/String;Lcom/google/cast/MimeData;)V
    .locals 3
    .param p1, "applicationName"    # Ljava/lang/String;
    .param p2, "applicationArgument"    # Lcom/google/cast/MimeData;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 441
    invoke-static {}, Lcom/google/cast/p;->a()V

    .line 443
    iget v0, p0, Lcom/google/cast/ApplicationSession;->s:I

    if-eqz v0, :cond_0

    .line 444
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Session is not currently stopped"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 447
    :cond_0
    invoke-direct {p0}, Lcom/google/cast/ApplicationSession;->a()V

    .line 448
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/cast/ApplicationSession;->s:I

    .line 450
    new-instance v0, Lcom/google/cast/n;

    iget-object v1, p0, Lcom/google/cast/ApplicationSession;->b:Lcom/google/cast/CastContext;

    iget-object v2, p0, Lcom/google/cast/ApplicationSession;->c:Lcom/google/cast/CastDevice;

    invoke-virtual {v2}, Lcom/google/cast/CastDevice;->getApplicationUrl()Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2, p1, p2}, Lcom/google/cast/n;-><init>(Lcom/google/cast/CastContext;Landroid/net/Uri;Ljava/lang/String;Lcom/google/cast/MimeData;)V

    iput-object v0, p0, Lcom/google/cast/ApplicationSession;->l:Lcom/google/cast/n;

    .line 452
    iget-object v0, p0, Lcom/google/cast/ApplicationSession;->l:Lcom/google/cast/n;

    iget-object v1, p0, Lcom/google/cast/ApplicationSession;->m:Lcom/google/cast/NetworkTask$Listener;

    invoke-virtual {v0, v1}, Lcom/google/cast/n;->setListener(Lcom/google/cast/NetworkTask$Listener;)V

    .line 453
    iget-object v0, p0, Lcom/google/cast/ApplicationSession;->l:Lcom/google/cast/n;

    invoke-virtual {v0}, Lcom/google/cast/n;->execute()V

    .line 454
    return-void
.end method
