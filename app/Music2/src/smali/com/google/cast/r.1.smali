.class Lcom/google/cast/r;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Lcom/google/cast/r;


# instance fields
.field private b:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/google/cast/WebSocket;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/google/cast/WebSocket;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/nio/channels/Selector;

.field private e:Ljava/security/SecureRandom;

.field private volatile f:Z

.field private volatile g:Z

.field private volatile h:Ljava/lang/Thread;

.field private i:Ljava/nio/charset/Charset;

.field private j:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private volatile k:Ljava/lang/Throwable;

.field private l:Lcom/google/cast/Logger;

.field private m:Landroid/os/PowerManager$WakeLock;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    new-instance v0, Lcom/google/cast/r;

    invoke-direct {v0}, Lcom/google/cast/r;-><init>()V

    sput-object v0, Lcom/google/cast/r;->a:Lcom/google/cast/r;

    return-void
.end method

.method private constructor <init>()V
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/cast/r;->b:Ljava/util/LinkedList;

    .line 54
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/cast/r;->c:Ljava/util/LinkedList;

    .line 55
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v5}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/google/cast/r;->j:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 56
    new-instance v0, Ljava/security/SecureRandom;

    invoke-direct {v0}, Ljava/security/SecureRandom;-><init>()V

    iput-object v0, p0, Lcom/google/cast/r;->e:Ljava/security/SecureRandom;

    .line 57
    new-instance v0, Lcom/google/cast/Logger;

    const-string v1, "WebSocketMultiplexer"

    invoke-direct {v0, v1}, Lcom/google/cast/Logger;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/cast/r;->l:Lcom/google/cast/Logger;

    .line 58
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/cast/r;->m:Landroid/os/PowerManager$WakeLock;

    .line 60
    :try_start_0
    const-string v0, "UTF-8"

    invoke-static {v0}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    iput-object v0, p0, Lcom/google/cast/r;->i:Ljava/nio/charset/Charset;
    :try_end_0
    .catch Ljava/nio/charset/IllegalCharsetNameException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/nio/charset/UnsupportedCharsetException; {:try_start_0 .. :try_end_0} :catch_1

    .line 68
    :goto_0
    return-void

    .line 61
    :catch_0
    move-exception v0

    .line 63
    iget-object v1, p0, Lcom/google/cast/r;->l:Lcom/google/cast/Logger;

    const-string v2, "Can\'t find charset %s"

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "UTF-8"

    aput-object v4, v3, v5

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/cast/Logger;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 64
    :catch_1
    move-exception v0

    .line 66
    iget-object v0, p0, Lcom/google/cast/r;->l:Lcom/google/cast/Logger;

    const-string v1, "Can\'t find charset %s"

    new-array v2, v3, [Ljava/lang/Object;

    const-string v3, "UTF-8"

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Lcom/google/cast/Logger;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static a()Lcom/google/cast/r;
    .locals 1

    .prologue
    .line 49
    sget-object v0, Lcom/google/cast/r;->a:Lcom/google/cast/r;

    return-object v0
.end method

.method static synthetic a(Lcom/google/cast/r;Ljava/lang/Thread;)Ljava/lang/Thread;
    .locals 0

    .prologue
    .line 25
    iput-object p1, p0, Lcom/google/cast/r;->h:Ljava/lang/Thread;

    return-object p1
.end method

.method static synthetic a(Lcom/google/cast/r;Ljava/lang/Throwable;)Ljava/lang/Throwable;
    .locals 0

    .prologue
    .line 25
    iput-object p1, p0, Lcom/google/cast/r;->k:Ljava/lang/Throwable;

    return-object p1
.end method

.method static synthetic a(Lcom/google/cast/r;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/google/cast/r;->g()V

    return-void
.end method

.method static synthetic a(Lcom/google/cast/r;Z)Z
    .locals 0

    .prologue
    .line 25
    iput-boolean p1, p0, Lcom/google/cast/r;->g:Z

    return p1
.end method

.method static synthetic b(Lcom/google/cast/r;)Lcom/google/cast/Logger;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/google/cast/r;->l:Lcom/google/cast/Logger;

    return-object v0
.end method

.method static synthetic c(Lcom/google/cast/r;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/google/cast/r;->j()V

    return-void
.end method

.method private g()V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 180
    :cond_0
    iget-boolean v0, p0, Lcom/google/cast/r;->f:Z

    if-nez v0, :cond_6

    .line 181
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 184
    iget-object v0, p0, Lcom/google/cast/r;->j:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 185
    iget-object v1, p0, Lcom/google/cast/r;->c:Ljava/util/LinkedList;

    monitor-enter v1

    .line 186
    :try_start_0
    iget-object v0, p0, Lcom/google/cast/r;->c:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/cast/WebSocket;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 188
    :try_start_1
    invoke-virtual {v0}, Lcom/google/cast/WebSocket;->i()Ljava/nio/channels/SocketChannel;

    move-result-object v6

    iget-object v7, p0, Lcom/google/cast/r;->d:Ljava/nio/channels/Selector;

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Ljava/nio/channels/SocketChannel;->register(Ljava/nio/channels/Selector;I)Ljava/nio/channels/SelectionKey;

    move-result-object v6

    .line 189
    invoke-virtual {v6, v0}, Ljava/nio/channels/SelectionKey;->attach(Ljava/lang/Object;)Ljava/lang/Object;

    .line 190
    iget-object v6, p0, Lcom/google/cast/r;->b:Ljava/util/LinkedList;

    invoke-virtual {v6, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 191
    :catch_0
    move-exception v0

    goto :goto_0

    .line 195
    :cond_1
    :try_start_2
    iget-object v0, p0, Lcom/google/cast/r;->c:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 196
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 201
    :cond_2
    iget-object v0, p0, Lcom/google/cast/r;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v2

    .line 202
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 203
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/cast/WebSocket;

    .line 204
    invoke-virtual {v0}, Lcom/google/cast/WebSocket;->i()Ljava/nio/channels/SocketChannel;

    move-result-object v6

    .line 206
    if-eqz v6, :cond_3

    iget-object v7, p0, Lcom/google/cast/r;->d:Ljava/nio/channels/Selector;

    invoke-virtual {v6, v7}, Ljava/nio/channels/SocketChannel;->keyFor(Ljava/nio/channels/Selector;)Ljava/nio/channels/SelectionKey;

    move-result-object v6

    invoke-virtual {v0, v6, v4, v5}, Lcom/google/cast/WebSocket;->a(Ljava/nio/channels/SelectionKey;J)Z

    move-result v6

    if-nez v6, :cond_4

    .line 208
    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    move v0, v1

    :goto_2
    move v1, v0

    .line 212
    goto :goto_1

    .line 196
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    .line 209
    :cond_4
    invoke-virtual {v0}, Lcom/google/cast/WebSocket;->c()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 210
    const/4 v1, 0x1

    move v0, v1

    goto :goto_2

    .line 215
    :cond_5
    :try_start_4
    invoke-direct {p0}, Lcom/google/cast/r;->j()V
    :try_end_4
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_1

    .line 218
    :try_start_5
    iget-object v3, p0, Lcom/google/cast/r;->d:Ljava/nio/channels/Selector;

    if-eqz v1, :cond_7

    const-wide/16 v0, 0x3e8

    :goto_3
    invoke-virtual {v3, v0, v1}, Ljava/nio/channels/Selector;->select(J)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    move-result v0

    .line 220
    :try_start_6
    invoke-direct {p0}, Lcom/google/cast/r;->i()V
    :try_end_6
    .catch Ljava/lang/IllegalArgumentException; {:try_start_6 .. :try_end_6} :catch_1

    .line 222
    if-eqz v0, :cond_0

    .line 230
    :goto_4
    iget-boolean v0, p0, Lcom/google/cast/r;->f:Z

    if-eqz v0, :cond_8

    .line 266
    :cond_6
    return-void

    .line 218
    :cond_7
    const-wide/16 v0, 0x0

    goto :goto_3

    .line 220
    :catchall_1
    move-exception v0

    :try_start_7
    invoke-direct {p0}, Lcom/google/cast/r;->i()V

    throw v0
    :try_end_7
    .catch Ljava/lang/IllegalArgumentException; {:try_start_7 .. :try_end_7} :catch_1

    .line 226
    :catch_1
    move-exception v0

    goto :goto_4

    .line 234
    :cond_8
    iget-object v0, p0, Lcom/google/cast/r;->d:Ljava/nio/channels/Selector;

    invoke-virtual {v0}, Ljava/nio/channels/Selector;->selectedKeys()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 235
    :goto_5
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 237
    :try_start_8
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/nio/channels/SelectionKey;

    .line 238
    invoke-virtual {v0}, Ljava/nio/channels/SelectionKey;->attachment()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/cast/WebSocket;

    .line 240
    invoke-virtual {v0}, Ljava/nio/channels/SelectionKey;->isConnectable()Z

    move-result v4

    if-eqz v4, :cond_9

    .line 241
    invoke-virtual {v1}, Lcom/google/cast/WebSocket;->f()Z

    move-result v4

    if-nez v4, :cond_9

    .line 242
    iget-object v4, p0, Lcom/google/cast/r;->b:Ljava/util/LinkedList;

    invoke-virtual {v4, v1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 246
    :cond_9
    invoke-virtual {v0}, Ljava/nio/channels/SelectionKey;->isReadable()Z

    move-result v4

    if-eqz v4, :cond_a

    .line 247
    invoke-virtual {v1}, Lcom/google/cast/WebSocket;->g()Z

    move-result v4

    if-nez v4, :cond_a

    .line 248
    iget-object v4, p0, Lcom/google/cast/r;->b:Ljava/util/LinkedList;

    invoke-virtual {v4, v1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 252
    :cond_a
    invoke-virtual {v0}, Ljava/nio/channels/SelectionKey;->isWritable()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 253
    invoke-virtual {v1}, Lcom/google/cast/WebSocket;->h()Z

    move-result v0

    if-nez v0, :cond_b

    .line 254
    iget-object v0, p0, Lcom/google/cast/r;->b:Ljava/util/LinkedList;

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z
    :try_end_8
    .catch Ljava/nio/channels/CancelledKeyException; {:try_start_8 .. :try_end_8} :catch_2

    .line 263
    :cond_b
    :goto_6
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    goto :goto_5

    .line 257
    :catch_2
    move-exception v0

    goto :goto_6

    :cond_c
    move v0, v1

    goto/16 :goto_2
.end method

.method private h()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 272
    iget-object v0, p0, Lcom/google/cast/r;->h:Ljava/lang/Thread;

    if-nez v0, :cond_0

    .line 273
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "not started; call start()"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 277
    :cond_0
    iget-boolean v0, p0, Lcom/google/cast/r;->g:Z

    if-eqz v0, :cond_2

    .line 278
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 279
    const-string v1, "selector thread aborted due to "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 280
    iget-object v1, p0, Lcom/google/cast/r;->k:Ljava/lang/Throwable;

    if-eqz v1, :cond_1

    .line 281
    iget-object v1, p0, Lcom/google/cast/r;->k:Ljava/lang/Throwable;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 282
    iget-object v1, p0, Lcom/google/cast/r;->k:Ljava/lang/Throwable;

    invoke-virtual {v1}, Ljava/lang/Throwable;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v1

    .line 283
    const-string v2, " at "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    aget-object v3, v1, v4

    invoke-virtual {v3}, Ljava/lang/StackTraceElement;->getFileName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const/16 v3, 0x3a

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v2

    aget-object v1, v1, v4

    invoke-virtual {v1}, Ljava/lang/StackTraceElement;->getLineNumber()I

    move-result v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 288
    :goto_0
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 286
    :cond_1
    const-string v1, "unknown condition"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 290
    :cond_2
    return-void
.end method

.method private declared-synchronized i()V
    .locals 1

    .prologue
    .line 318
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/cast/r;->m:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    .line 319
    iget-object v0, p0, Lcom/google/cast/r;->m:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 321
    :cond_0
    monitor-exit p0

    return-void

    .line 318
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized j()V
    .locals 1

    .prologue
    .line 324
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/cast/r;->m:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/cast/r;->m:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 325
    iget-object v0, p0, Lcom/google/cast/r;->m:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 327
    :cond_0
    monitor-exit p0

    return-void

    .line 324
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public declared-synchronized a(Landroid/content/Context;Z)V
    .locals 3

    .prologue
    .line 160
    monitor-enter p0

    if-eqz p2, :cond_1

    .line 161
    :try_start_0
    iget-object v0, p0, Lcom/google/cast/r;->m:Landroid/os/PowerManager$WakeLock;

    if-nez v0, :cond_0

    .line 162
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "power"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 164
    const/4 v1, 0x1

    const-string v2, "WebSocketMultiplexer.mWakeLock"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/google/cast/r;->m:Landroid/os/PowerManager$WakeLock;

    .line 165
    iget-object v0, p0, Lcom/google/cast/r;->m:Landroid/os/PowerManager$WakeLock;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 173
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 168
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/cast/r;->m:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/cast/r;->m:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 169
    iget-object v0, p0, Lcom/google/cast/r;->m:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 171
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/cast/r;->m:Landroid/os/PowerManager$WakeLock;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 160
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Lcom/google/cast/WebSocket;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 135
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/cast/r;->h()V

    .line 137
    iget-object v1, p0, Lcom/google/cast/r;->c:Ljava/util/LinkedList;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 138
    :try_start_1
    iget-object v0, p0, Lcom/google/cast/r;->c:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 139
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 140
    :try_start_2
    iget-object v0, p0, Lcom/google/cast/r;->j:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 141
    iget-object v0, p0, Lcom/google/cast/r;->d:Ljava/nio/channels/Selector;

    invoke-virtual {v0}, Ljava/nio/channels/Selector;->wakeup()Ljava/nio/channels/Selector;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 142
    monitor-exit p0

    return-void

    .line 139
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 135
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 75
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/cast/r;->h:Ljava/lang/Thread;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 98
    :goto_0
    monitor-exit p0

    return-void

    .line 79
    :cond_0
    const/4 v0, 0x0

    :try_start_1
    iput-boolean v0, p0, Lcom/google/cast/r;->g:Z

    .line 80
    invoke-static {}, Ljava/nio/channels/Selector;->open()Ljava/nio/channels/Selector;

    move-result-object v0

    iput-object v0, p0, Lcom/google/cast/r;->d:Ljava/nio/channels/Selector;

    .line 81
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/google/cast/r$1;

    invoke-direct {v1, p0}, Lcom/google/cast/r$1;-><init>(Lcom/google/cast/r;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/google/cast/r;->h:Ljava/lang/Thread;

    .line 96
    invoke-direct {p0}, Lcom/google/cast/r;->i()V

    .line 97
    iget-object v0, p0, Lcom/google/cast/r;->h:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 75
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized c()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 148
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/cast/r;->h()V

    .line 150
    iget-object v0, p0, Lcom/google/cast/r;->d:Ljava/nio/channels/Selector;

    invoke-virtual {v0}, Ljava/nio/channels/Selector;->wakeup()Ljava/nio/channels/Selector;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 151
    monitor-exit p0

    return-void

    .line 148
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method d()Ljava/lang/String;
    .locals 4

    .prologue
    .line 296
    const/16 v0, 0x10

    new-array v0, v0, [B

    .line 297
    iget-object v1, p0, Lcom/google/cast/r;->e:Ljava/security/SecureRandom;

    invoke-virtual {v1, v0}, Ljava/security/SecureRandom;->nextBytes([B)V

    .line 298
    const/4 v1, 0x0

    array-length v2, v0

    const/4 v3, 0x2

    invoke-static {v0, v1, v2, v3}, Landroid/util/Base64;->encodeToString([BIII)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method e()[B
    .locals 2

    .prologue
    .line 305
    const/4 v0, 0x4

    new-array v0, v0, [B

    .line 306
    iget-object v1, p0, Lcom/google/cast/r;->e:Ljava/security/SecureRandom;

    invoke-virtual {v1, v0}, Ljava/security/SecureRandom;->nextBytes([B)V

    .line 307
    return-object v0
.end method

.method f()Ljava/nio/charset/Charset;
    .locals 1

    .prologue
    .line 314
    iget-object v0, p0, Lcom/google/cast/r;->i:Ljava/nio/charset/Charset;

    return-object v0
.end method
