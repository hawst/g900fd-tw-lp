.class Lcom/google/cast/g;
.super Lcom/google/cast/NetworkRequest;
.source "SourceFile"


# instance fields
.field protected a:Landroid/net/Uri;

.field protected b:Landroid/net/Uri;

.field private c:Lcom/google/cast/ApplicationMetadata;

.field private d:I

.field private e:Landroid/net/Uri;

.field private f:Landroid/net/Uri;

.field private g:Ljava/lang/String;

.field private h:Z

.field private i:Z

.field private j:Lcom/google/cast/Logger;


# direct methods
.method public constructor <init>(Lcom/google/cast/CastContext;Landroid/net/Uri;)V
    .locals 1

    .prologue
    .line 79
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/cast/g;-><init>(Lcom/google/cast/CastContext;Landroid/net/Uri;Ljava/lang/String;)V

    .line 80
    return-void
.end method

.method public constructor <init>(Lcom/google/cast/CastContext;Landroid/net/Uri;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 93
    invoke-direct {p0, p1}, Lcom/google/cast/NetworkRequest;-><init>(Lcom/google/cast/CastContext;)V

    .line 94
    if-nez p3, :cond_0

    .line 95
    iput-object p2, p0, Lcom/google/cast/g;->a:Landroid/net/Uri;

    .line 99
    :goto_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/cast/g;->d:I

    .line 100
    new-instance v0, Lcom/google/cast/Logger;

    const-string v1, "GetApplicationInfoRequest"

    invoke-direct {v0, v1}, Lcom/google/cast/Logger;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/cast/g;->j:Lcom/google/cast/Logger;

    .line 101
    return-void

    .line 97
    :cond_0
    invoke-virtual {p2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/cast/g;->a:Landroid/net/Uri;

    goto :goto_0
.end method

.method private a(Lorg/xmlpull/v1/XmlPullParser;Lcom/google/cast/ApplicationMetadata$a;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    .line 282
    const-string v0, "urn:chrome.google.com:cast"

    const-string v1, "servicedata"

    invoke-interface {p1, v2, v0, v1}, Lorg/xmlpull/v1/XmlPullParser;->require(ILjava/lang/String;Ljava/lang/String;)V

    .line 283
    :cond_0
    :goto_0
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_4

    .line 284
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v0

    if-ne v0, v2, :cond_0

    .line 287
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    .line 288
    const-string v1, "connectionSvcURL"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 289
    invoke-static {p1}, Lcom/google/cast/s;->b(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v0

    .line 290
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/cast/g;->f:Landroid/net/Uri;

    goto :goto_0

    .line 291
    :cond_1
    const-string v1, "applicationContext"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 292
    invoke-static {p1}, Lcom/google/cast/s;->b(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/cast/g;->g:Ljava/lang/String;

    goto :goto_0

    .line 293
    :cond_2
    const-string v1, "protocols"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 294
    invoke-direct {p0, p1, p2}, Lcom/google/cast/g;->c(Lorg/xmlpull/v1/XmlPullParser;Lcom/google/cast/ApplicationMetadata$a;)V

    goto :goto_0

    .line 296
    :cond_3
    invoke-static {p1}, Lcom/google/cast/s;->a(Lorg/xmlpull/v1/XmlPullParser;)V

    goto :goto_0

    .line 299
    :cond_4
    return-void
.end method

.method private b(Lorg/xmlpull/v1/XmlPullParser;Lcom/google/cast/ApplicationMetadata$a;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    .line 303
    const-string v0, "urn:chrome.google.com:cast"

    const-string v1, "activity-status"

    invoke-interface {p1, v2, v0, v1}, Lorg/xmlpull/v1/XmlPullParser;->require(ILjava/lang/String;Ljava/lang/String;)V

    .line 304
    :cond_0
    :goto_0
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_3

    .line 305
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v0

    if-ne v0, v2, :cond_0

    .line 308
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    .line 309
    const-string v1, "description"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 310
    invoke-static {p1}, Lcom/google/cast/s;->b(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/google/cast/ApplicationMetadata$a;->a(Ljava/lang/String;)Lcom/google/cast/ApplicationMetadata$a;

    goto :goto_0

    .line 311
    :cond_1
    const-string v1, "image"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 312
    const-string v0, ""

    const-string v1, "src"

    invoke-interface {p1, v0, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 313
    if-eqz v0, :cond_0

    .line 314
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/google/cast/ApplicationMetadata$a;->a(Landroid/net/Uri;)Lcom/google/cast/ApplicationMetadata$a;

    goto :goto_0

    .line 317
    :cond_2
    invoke-static {p1}, Lcom/google/cast/s;->a(Lorg/xmlpull/v1/XmlPullParser;)V

    goto :goto_0

    .line 320
    :cond_3
    return-void
.end method

.method private c(Lorg/xmlpull/v1/XmlPullParser;Lcom/google/cast/ApplicationMetadata$a;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x2

    .line 324
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 326
    :cond_0
    :goto_0
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v1

    const/4 v2, 0x3

    if-eq v1, v2, :cond_1

    .line 327
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v1

    if-ne v1, v3, :cond_0

    .line 330
    const/4 v1, 0x0

    const-string v2, "protocol"

    invoke-interface {p1, v3, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->require(ILjava/lang/String;Ljava/lang/String;)V

    .line 331
    invoke-static {p1}, Lcom/google/cast/s;->b(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v1

    .line 332
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 333
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 337
    :cond_1
    invoke-virtual {p2, v0}, Lcom/google/cast/ApplicationMetadata$a;->a(Ljava/util/List;)Lcom/google/cast/ApplicationMetadata$a;

    .line 338
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/cast/ApplicationMetadata;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/google/cast/g;->c:Lcom/google/cast/ApplicationMetadata;

    return-object v0
.end method

.method a(Landroid/net/Uri;Ljava/lang/String;)V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x3

    const/4 v1, 0x0

    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 223
    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v2

    .line 225
    new-instance v0, Ljava/io/StringReader;

    invoke-direct {v0, p2}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-interface {v2, v0}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    .line 226
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->nextTag()I

    .line 227
    const-string v0, "urn:dial-multiscreen-org:schemas:dial"

    const-string v3, "service"

    invoke-interface {v2, v7, v0, v3}, Lorg/xmlpull/v1/XmlPullParser;->require(ILjava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    .line 228
    :cond_0
    :goto_0
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v3

    if-eq v3, v8, :cond_b

    .line 229
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v3

    if-ne v3, v7, :cond_0

    .line 233
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v3

    .line 234
    const-string v4, "name"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 235
    invoke-static {v2}, Lcom/google/cast/s;->b(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v3

    .line 236
    if-eqz v0, :cond_1

    .line 237
    new-instance v0, Lorg/xmlpull/v1/XmlPullParserException;

    const-string v1, "Invalid XML, multiple application name received"

    invoke-direct {v0, v1}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 240
    :cond_1
    new-instance v0, Lcom/google/cast/ApplicationMetadata$a;

    invoke-direct {v0, v3}, Lcom/google/cast/ApplicationMetadata$a;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 242
    :cond_2
    const-string v4, "options"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 243
    const-string v3, "allowStop"

    invoke-interface {v2, v1, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 244
    const-string v4, "true"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 245
    iput-boolean v6, p0, Lcom/google/cast/g;->h:Z

    .line 247
    :cond_3
    invoke-static {v2}, Lcom/google/cast/s;->a(Lorg/xmlpull/v1/XmlPullParser;)V

    goto :goto_0

    .line 248
    :cond_4
    const-string v4, "state"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 249
    invoke-static {v2}, Lcom/google/cast/s;->b(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v3

    .line 250
    const-string v4, "running"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 251
    iput v6, p0, Lcom/google/cast/g;->d:I

    goto :goto_0

    .line 252
    :cond_5
    const-string v4, "stopped"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 253
    iput v7, p0, Lcom/google/cast/g;->d:I

    goto :goto_0

    .line 254
    :cond_6
    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    const-string v5, "installable"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 255
    iput v8, p0, Lcom/google/cast/g;->d:I

    .line 256
    const-string v4, "installable"

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    iput-object v3, p0, Lcom/google/cast/g;->e:Landroid/net/Uri;

    goto/16 :goto_0

    .line 258
    :cond_7
    const-string v4, "link"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 259
    const-string v3, "href"

    invoke-interface {v2, v1, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 262
    invoke-virtual {p1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    iput-object v3, p0, Lcom/google/cast/g;->b:Landroid/net/Uri;

    .line 263
    invoke-static {v2}, Lcom/google/cast/s;->a(Lorg/xmlpull/v1/XmlPullParser;)V

    goto/16 :goto_0

    .line 264
    :cond_8
    const-string v4, "servicedata"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 265
    iput-boolean v6, p0, Lcom/google/cast/g;->i:Z

    .line 266
    invoke-direct {p0, v2, v0}, Lcom/google/cast/g;->a(Lorg/xmlpull/v1/XmlPullParser;Lcom/google/cast/ApplicationMetadata$a;)V

    goto/16 :goto_0

    .line 267
    :cond_9
    const-string v4, "activity-status"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 268
    if-nez v0, :cond_a

    .line 269
    new-instance v0, Lorg/xmlpull/v1/XmlPullParserException;

    const-string v1, "missing name element."

    invoke-direct {v0, v1}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 271
    :cond_a
    invoke-direct {p0, v2, v0}, Lcom/google/cast/g;->b(Lorg/xmlpull/v1/XmlPullParser;Lcom/google/cast/ApplicationMetadata$a;)V

    goto/16 :goto_0

    .line 274
    :cond_b
    if-nez v0, :cond_c

    .line 275
    new-instance v0, Lorg/xmlpull/v1/XmlPullParserException;

    const-string v1, "missing name element."

    invoke-direct {v0, v1}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 277
    :cond_c
    invoke-virtual {v0}, Lcom/google/cast/ApplicationMetadata$a;->a()Lcom/google/cast/ApplicationMetadata;

    move-result-object v0

    iput-object v0, p0, Lcom/google/cast/g;->c:Lcom/google/cast/ApplicationMetadata;

    .line 278
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 116
    iget v0, p0, Lcom/google/cast/g;->d:I

    return v0
.end method

.method public final c()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/google/cast/g;->b:Landroid/net/Uri;

    return-object v0
.end method

.method public final d()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/google/cast/g;->f:Landroid/net/Uri;

    return-object v0
.end method

.method protected e()Z
    .locals 1

    .prologue
    .line 166
    iget-boolean v0, p0, Lcom/google/cast/g;->i:Z

    return v0
.end method

.method public final execute()I
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v1, -0x1

    const/4 v0, 0x0

    const/4 v2, -0x3

    .line 171
    .line 175
    :try_start_0
    iget-object v3, p0, Lcom/google/cast/g;->a:Landroid/net/Uri;

    sget v4, Lcom/google/cast/g;->DEFAULT_TIMEOUT:I

    invoke-virtual {p0, v3, v4}, Lcom/google/cast/g;->performHttpGet(Landroid/net/Uri;I)Lcom/google/cast/SimpleHttpRequest;

    move-result-object v3

    .line 177
    invoke-interface {v3}, Lcom/google/cast/SimpleHttpRequest;->getResponseStatus()I

    move-result v4

    .line 178
    const/16 v5, 0x194

    if-ne v4, v5, :cond_0

    .line 179
    const/4 v0, -0x4

    .line 217
    :goto_0
    return v0

    .line 180
    :cond_0
    const/16 v5, 0xcc

    if-ne v4, v5, :cond_1

    .line 181
    iget-object v2, p0, Lcom/google/cast/g;->j:Lcom/google/cast/Logger;

    const-string v3, "No current application running."

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v4}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 189
    :catch_0
    move-exception v0

    .line 190
    const/4 v0, -0x2

    goto :goto_0

    .line 183
    :cond_1
    const/16 v5, 0xc8

    if-eq v4, v5, :cond_2

    move v0, v1

    .line 184
    goto :goto_0

    .line 187
    :cond_2
    invoke-interface {v3}, Lcom/google/cast/SimpleHttpRequest;->getResponseData()Lcom/google/cast/MimeData;

    move-result-object v4

    .line 188
    invoke-interface {v3}, Lcom/google/cast/SimpleHttpRequest;->getFinalUri()Landroid/net/Uri;
    :try_end_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    .line 195
    if-nez v4, :cond_3

    move v0, v2

    .line 196
    goto :goto_0

    .line 191
    :catch_1
    move-exception v0

    move v0, v1

    .line 192
    goto :goto_0

    .line 200
    :cond_3
    if-eqz v4, :cond_4

    const-string v3, "application/xml"

    invoke-virtual {v4}, Lcom/google/cast/MimeData;->getType()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    :cond_4
    move v0, v2

    .line 201
    goto :goto_0

    .line 203
    :cond_5
    invoke-virtual {v4}, Lcom/google/cast/MimeData;->getTextData()Ljava/lang/String;

    move-result-object v3

    .line 204
    if-nez v3, :cond_6

    move v0, v2

    .line 205
    goto :goto_0

    .line 208
    :cond_6
    :try_start_1
    invoke-virtual {p0, v1, v3}, Lcom/google/cast/g;->a(Landroid/net/Uri;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 .. :try_end_1} :catch_3

    goto :goto_0

    .line 209
    :catch_2
    move-exception v1

    .line 210
    iget-object v3, p0, Lcom/google/cast/g;->j:Lcom/google/cast/Logger;

    const-string v4, "parse error"

    new-array v5, v6, [Ljava/lang/Object;

    aput-object v1, v5, v0

    invoke-virtual {v3, v4, v5}, Lcom/google/cast/Logger;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v2

    .line 211
    goto :goto_0

    .line 212
    :catch_3
    move-exception v1

    .line 213
    iget-object v3, p0, Lcom/google/cast/g;->j:Lcom/google/cast/Logger;

    const-string v4, "parse error"

    new-array v5, v6, [Ljava/lang/Object;

    aput-object v1, v5, v0

    invoke-virtual {v3, v4, v5}, Lcom/google/cast/Logger;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v2

    .line 214
    goto :goto_0
.end method
