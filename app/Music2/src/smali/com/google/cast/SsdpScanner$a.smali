.class Lcom/google/cast/SsdpScanner$a;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/cast/SsdpScanner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/google/cast/SsdpScanner;


# direct methods
.method private constructor <init>(Lcom/google/cast/SsdpScanner;)V
    .locals 0

    .prologue
    .line 588
    iput-object p1, p0, Lcom/google/cast/SsdpScanner$a;->a:Lcom/google/cast/SsdpScanner;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/cast/SsdpScanner;Lcom/google/cast/SsdpScanner$1;)V
    .locals 0

    .prologue
    .line 588
    invoke-direct {p0, p1}, Lcom/google/cast/SsdpScanner$a;-><init>(Lcom/google/cast/SsdpScanner;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 591
    iget-object v0, p0, Lcom/google/cast/SsdpScanner$a;->a:Lcom/google/cast/SsdpScanner;

    invoke-static {v0}, Lcom/google/cast/SsdpScanner;->f(Lcom/google/cast/SsdpScanner;)Landroid/net/ConnectivityManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 592
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 593
    :goto_0
    iget-object v3, p0, Lcom/google/cast/SsdpScanner$a;->a:Lcom/google/cast/SsdpScanner;

    invoke-static {v3}, Lcom/google/cast/SsdpScanner;->b(Lcom/google/cast/SsdpScanner;)Lcom/google/cast/Logger;

    move-result-object v3

    const-string v4, "connectivity state changed; connected? %b, errorState? %b"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v5, v2

    iget-object v6, p0, Lcom/google/cast/SsdpScanner$a;->a:Lcom/google/cast/SsdpScanner;

    invoke-static {v6}, Lcom/google/cast/SsdpScanner;->g(Lcom/google/cast/SsdpScanner;)Z

    move-result v6

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-virtual {v3, v4, v5}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 596
    if-nez v0, :cond_0

    .line 597
    iget-object v1, p0, Lcom/google/cast/SsdpScanner$a;->a:Lcom/google/cast/SsdpScanner;

    const/4 v3, 0x0

    invoke-static {v1, v3}, Lcom/google/cast/SsdpScanner;->a(Lcom/google/cast/SsdpScanner;Ljava/lang/String;)Ljava/lang/String;

    .line 601
    :cond_0
    iget-object v1, p0, Lcom/google/cast/SsdpScanner$a;->a:Lcom/google/cast/SsdpScanner;

    invoke-static {v1}, Lcom/google/cast/SsdpScanner;->h(Lcom/google/cast/SsdpScanner;)V

    .line 602
    if-eqz v0, :cond_3

    .line 604
    iget-object v0, p0, Lcom/google/cast/SsdpScanner$a;->a:Lcom/google/cast/SsdpScanner;

    invoke-static {v0}, Lcom/google/cast/SsdpScanner;->b(Lcom/google/cast/SsdpScanner;)Lcom/google/cast/Logger;

    move-result-object v0

    const-string v1, "re-established connectivity after connectivity changed; restarting scan"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 605
    iget-object v0, p0, Lcom/google/cast/SsdpScanner$a;->a:Lcom/google/cast/SsdpScanner;

    invoke-virtual {v0}, Lcom/google/cast/SsdpScanner;->a()V

    .line 613
    :cond_1
    :goto_1
    return-void

    :cond_2
    move v0, v2

    .line 592
    goto :goto_0

    .line 606
    :cond_3
    iget-object v0, p0, Lcom/google/cast/SsdpScanner$a;->a:Lcom/google/cast/SsdpScanner;

    invoke-static {v0}, Lcom/google/cast/SsdpScanner;->g(Lcom/google/cast/SsdpScanner;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 607
    iget-object v0, p0, Lcom/google/cast/SsdpScanner$a;->a:Lcom/google/cast/SsdpScanner;

    invoke-static {v0}, Lcom/google/cast/SsdpScanner;->b(Lcom/google/cast/SsdpScanner;)Lcom/google/cast/Logger;

    move-result-object v0

    const-string v1, "lost connectivity while scanning;"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 609
    iget-object v0, p0, Lcom/google/cast/SsdpScanner$a;->a:Lcom/google/cast/SsdpScanner;

    invoke-static {v0}, Lcom/google/cast/SsdpScanner;->i(Lcom/google/cast/SsdpScanner;)V

    goto :goto_1
.end method
