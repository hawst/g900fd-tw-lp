.class final Lcom/google/cast/j$a;
.super Lcom/google/cast/b$a;
.source "SourceFile"

# interfaces
.implements Lcom/google/cast/MediaRouteStateChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/cast/j;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "a"
.end annotation


# instance fields
.field d:Z

.field final synthetic e:Lcom/google/cast/j;

.field private f:Lcom/google/cast/MediaRouteAdapter;


# direct methods
.method public constructor <init>(Lcom/google/cast/j;Lcom/google/cast/CastDevice;Ljava/lang/String;Lcom/google/cast/MediaRouteAdapter;)V
    .locals 1

    .prologue
    .line 75
    iput-object p1, p0, Lcom/google/cast/j$a;->e:Lcom/google/cast/j;

    .line 76
    invoke-direct {p0, p1, p2, p3}, Lcom/google/cast/b$a;-><init>(Lcom/google/cast/b;Lcom/google/cast/CastDevice;Ljava/lang/String;)V

    .line 78
    iput-object p4, p0, Lcom/google/cast/j$a;->f:Lcom/google/cast/MediaRouteAdapter;

    .line 79
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/cast/j$a;->d:Z

    .line 80
    return-void
.end method


# virtual methods
.method public onControlRequest(Landroid/content/Intent;Landroid/support/v7/media/MediaRouter$ControlRequestCallback;)Z
    .locals 5
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "callback"    # Landroid/support/v7/media/MediaRouter$ControlRequestCallback;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 100
    invoke-static {}, Lcom/google/cast/j;->b()Lcom/google/cast/Logger;

    move-result-object v2

    const-string v3, "Received control request %s"

    new-array v4, v0, [Ljava/lang/Object;

    aput-object p1, v4, v1

    invoke-virtual {v2, v3, v4}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 102
    const-string v2, "com.google.cast.ACTION_GET_DEVICE"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 103
    const-string v1, "com.google.cast.EXTRA_ROUTE_ID"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 104
    iget-object v2, p0, Lcom/google/cast/j$a;->f:Lcom/google/cast/MediaRouteAdapter;

    iget-object v3, p0, Lcom/google/cast/j$a;->a:Lcom/google/cast/CastDevice;

    invoke-interface {v2, v3, v1, p0}, Lcom/google/cast/MediaRouteAdapter;->onDeviceAvailable(Lcom/google/cast/CastDevice;Ljava/lang/String;Lcom/google/cast/MediaRouteStateChangeListener;)V

    .line 108
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public onRelease()V
    .locals 1

    .prologue
    .line 84
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/cast/j$a;->f:Lcom/google/cast/MediaRouteAdapter;

    .line 85
    invoke-super {p0}, Lcom/google/cast/b$a;->onRelease()V

    .line 86
    return-void
.end method

.method public onSelect()V
    .locals 1

    .prologue
    .line 90
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/cast/j$a;->d:Z

    .line 91
    return-void
.end method

.method public onSetVolume(I)V
    .locals 6
    .param p1, "volume"    # I

    .prologue
    .line 113
    invoke-static {}, Lcom/google/cast/j;->b()Lcom/google/cast/Logger;

    move-result-object v0

    const-string v1, "onSetVolume: %d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 115
    iget-boolean v0, p0, Lcom/google/cast/j$a;->d:Z

    if-eqz v0, :cond_0

    .line 116
    iget-object v0, p0, Lcom/google/cast/j$a;->f:Lcom/google/cast/MediaRouteAdapter;

    int-to-double v2, p1

    const-wide/high16 v4, 0x4034000000000000L    # 20.0

    div-double/2addr v2, v4

    invoke-interface {v0, v2, v3}, Lcom/google/cast/MediaRouteAdapter;->onSetVolume(D)V

    .line 118
    :cond_0
    return-void
.end method

.method public onUnselect()V
    .locals 1

    .prologue
    .line 95
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/cast/j$a;->d:Z

    .line 96
    return-void
.end method

.method public onUpdateVolume(I)V
    .locals 6
    .param p1, "delta"    # I

    .prologue
    .line 122
    invoke-static {}, Lcom/google/cast/j;->b()Lcom/google/cast/Logger;

    move-result-object v0

    const-string v1, "onUpdateVolume: %d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 124
    iget-boolean v0, p0, Lcom/google/cast/j$a;->d:Z

    if-eqz v0, :cond_0

    .line 125
    iget-object v0, p0, Lcom/google/cast/j$a;->f:Lcom/google/cast/MediaRouteAdapter;

    int-to-double v2, p1

    const-wide/high16 v4, 0x4034000000000000L    # 20.0

    div-double/2addr v2, v4

    invoke-interface {v0, v2, v3}, Lcom/google/cast/MediaRouteAdapter;->onUpdateVolume(D)V

    .line 127
    :cond_0
    return-void
.end method

.method public onVolumeChanged(D)V
    .locals 1
    .param p1, "volume"    # D

    .prologue
    .line 134
    iget-boolean v0, p0, Lcom/google/cast/j$a;->d:Z

    if-eqz v0, :cond_0

    .line 135
    invoke-virtual {p0, p1, p2}, Lcom/google/cast/j$a;->a(D)V

    .line 137
    :cond_0
    return-void
.end method
