.class Lcom/google/cast/ApplicationSession$2;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/cast/NetworkTask$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/cast/ApplicationSession;-><init>(Lcom/google/cast/CastContext;Lcom/google/cast/CastDevice;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/google/cast/ApplicationSession;


# direct methods
.method constructor <init>(Lcom/google/cast/ApplicationSession;)V
    .locals 0

    .prologue
    .line 247
    iput-object p1, p0, Lcom/google/cast/ApplicationSession$2;->a:Lcom/google/cast/ApplicationSession;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTaskCancelled()V
    .locals 0

    .prologue
    .line 281
    return-void
.end method

.method public onTaskCompleted()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 250
    iget-object v0, p0, Lcom/google/cast/ApplicationSession$2;->a:Lcom/google/cast/ApplicationSession;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/cast/ApplicationSession;->a(Lcom/google/cast/ApplicationSession;Z)Z

    .line 251
    iget-object v0, p0, Lcom/google/cast/ApplicationSession$2;->a:Lcom/google/cast/ApplicationSession;

    invoke-static {v0, v2}, Lcom/google/cast/ApplicationSession;->a(Lcom/google/cast/ApplicationSession;Lcom/google/cast/NetworkTask;)Lcom/google/cast/NetworkTask;

    .line 252
    iget-object v0, p0, Lcom/google/cast/ApplicationSession$2;->a:Lcom/google/cast/ApplicationSession;

    invoke-static {v0, v2}, Lcom/google/cast/ApplicationSession;->a(Lcom/google/cast/ApplicationSession;Ljava/lang/String;)Ljava/lang/String;

    .line 253
    iget-object v0, p0, Lcom/google/cast/ApplicationSession$2;->a:Lcom/google/cast/ApplicationSession;

    invoke-static {v0, v2}, Lcom/google/cast/ApplicationSession;->a(Lcom/google/cast/ApplicationSession;Landroid/net/Uri;)Landroid/net/Uri;

    .line 254
    iget-object v0, p0, Lcom/google/cast/ApplicationSession$2;->a:Lcom/google/cast/ApplicationSession;

    invoke-static {v0, v2}, Lcom/google/cast/ApplicationSession;->a(Lcom/google/cast/ApplicationSession;Lcom/google/cast/SessionError;)V

    .line 255
    return-void
.end method

.method public onTaskFailed(I)V
    .locals 3
    .param p1, "status"    # I

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 259
    iget-object v2, p0, Lcom/google/cast/ApplicationSession$2;->a:Lcom/google/cast/ApplicationSession;

    invoke-static {v2, v0}, Lcom/google/cast/ApplicationSession;->a(Lcom/google/cast/ApplicationSession;Z)Z

    .line 260
    iget-object v2, p0, Lcom/google/cast/ApplicationSession$2;->a:Lcom/google/cast/ApplicationSession;

    invoke-static {v2, v1}, Lcom/google/cast/ApplicationSession;->a(Lcom/google/cast/ApplicationSession;Lcom/google/cast/NetworkTask;)Lcom/google/cast/NetworkTask;

    .line 262
    const/4 v2, -0x4

    if-ne p1, v2, :cond_0

    .line 265
    iget-object v0, p0, Lcom/google/cast/ApplicationSession$2;->a:Lcom/google/cast/ApplicationSession;

    invoke-static {v0, v1}, Lcom/google/cast/ApplicationSession;->a(Lcom/google/cast/ApplicationSession;Ljava/lang/String;)Ljava/lang/String;

    .line 266
    iget-object v0, p0, Lcom/google/cast/ApplicationSession$2;->a:Lcom/google/cast/ApplicationSession;

    invoke-static {v0, v1}, Lcom/google/cast/ApplicationSession;->a(Lcom/google/cast/ApplicationSession;Landroid/net/Uri;)Landroid/net/Uri;

    move-object v0, v1

    .line 275
    :goto_0
    iget-object v1, p0, Lcom/google/cast/ApplicationSession$2;->a:Lcom/google/cast/ApplicationSession;

    invoke-static {v1, v0}, Lcom/google/cast/ApplicationSession;->a(Lcom/google/cast/ApplicationSession;Lcom/google/cast/SessionError;)V

    .line 276
    return-void

    .line 268
    :cond_0
    const/4 v1, -0x2

    if-ne p1, v1, :cond_1

    .line 271
    :goto_1
    new-instance v1, Lcom/google/cast/SessionError;

    const/4 v2, 0x4

    invoke-direct {v1, v2, v0}, Lcom/google/cast/SessionError;-><init>(II)V

    move-object v0, v1

    goto :goto_0

    .line 268
    :cond_1
    const/4 v0, 0x2

    goto :goto_1
.end method
