.class public final Lcom/google/cast/SsdpScanner$SsdpResponse;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/cast/SsdpScanner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SsdpResponse"
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 479
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 480
    return-void
.end method

.method static synthetic a(Lcom/google/cast/SsdpScanner$SsdpResponse;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 462
    invoke-direct {p0, p1}, Lcom/google/cast/SsdpScanner$SsdpResponse;->a(Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 483
    iput-object p1, p0, Lcom/google/cast/SsdpScanner$SsdpResponse;->a:Ljava/lang/String;

    .line 484
    return-void
.end method

.method static synthetic b(Lcom/google/cast/SsdpScanner$SsdpResponse;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 462
    invoke-direct {p0, p1}, Lcom/google/cast/SsdpScanner$SsdpResponse;->b(Ljava/lang/String;)V

    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 491
    iput-object p1, p0, Lcom/google/cast/SsdpScanner$SsdpResponse;->b:Ljava/lang/String;

    .line 492
    return-void
.end method

.method static synthetic c(Lcom/google/cast/SsdpScanner$SsdpResponse;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 462
    invoke-direct {p0, p1}, Lcom/google/cast/SsdpScanner$SsdpResponse;->c(Ljava/lang/String;)V

    return-void
.end method

.method private c(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 499
    iput-object p1, p0, Lcom/google/cast/SsdpScanner$SsdpResponse;->c:Ljava/lang/String;

    .line 500
    return-void
.end method

.method static synthetic d(Lcom/google/cast/SsdpScanner$SsdpResponse;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 462
    invoke-direct {p0, p1}, Lcom/google/cast/SsdpScanner$SsdpResponse;->d(Ljava/lang/String;)V

    return-void
.end method

.method private d(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 507
    iput-object p1, p0, Lcom/google/cast/SsdpScanner$SsdpResponse;->d:Ljava/lang/String;

    .line 508
    return-void
.end method

.method static synthetic e(Lcom/google/cast/SsdpScanner$SsdpResponse;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 462
    invoke-direct {p0, p1}, Lcom/google/cast/SsdpScanner$SsdpResponse;->e(Ljava/lang/String;)V

    return-void
.end method

.method private e(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 515
    iput-object p1, p0, Lcom/google/cast/SsdpScanner$SsdpResponse;->e:Ljava/lang/String;

    .line 516
    return-void
.end method

.method static synthetic f(Lcom/google/cast/SsdpScanner$SsdpResponse;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 462
    invoke-direct {p0, p1}, Lcom/google/cast/SsdpScanner$SsdpResponse;->f(Ljava/lang/String;)V

    return-void
.end method

.method private f(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 523
    iput-object p1, p0, Lcom/google/cast/SsdpScanner$SsdpResponse;->f:Ljava/lang/String;

    .line 524
    return-void
.end method

.method static synthetic g(Lcom/google/cast/SsdpScanner$SsdpResponse;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 462
    invoke-direct {p0, p1}, Lcom/google/cast/SsdpScanner$SsdpResponse;->g(Ljava/lang/String;)V

    return-void
.end method

.method private g(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 531
    iput-object p1, p0, Lcom/google/cast/SsdpScanner$SsdpResponse;->g:Ljava/lang/String;

    .line 532
    return-void
.end method


# virtual methods
.method public getLocation()Ljava/lang/String;
    .locals 1

    .prologue
    .line 503
    iget-object v0, p0, Lcom/google/cast/SsdpScanner$SsdpResponse;->c:Ljava/lang/String;

    return-object v0
.end method

.method public getUsn()Ljava/lang/String;
    .locals 1

    .prologue
    .line 527
    iget-object v0, p0, Lcom/google/cast/SsdpScanner$SsdpResponse;->f:Ljava/lang/String;

    return-object v0
.end method

.method public isValidCastDevice()Z
    .locals 1

    .prologue
    .line 542
    iget-object v0, p0, Lcom/google/cast/SsdpScanner$SsdpResponse;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/cast/SsdpScanner$SsdpResponse;->f:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
