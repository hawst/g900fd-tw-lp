.class public Lcom/google/cast/NetworkTask;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/cast/NetworkTask$Listener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:[Ljava/lang/Object;


# instance fields
.field private b:[Lcom/google/cast/NetworkRequest;

.field private c:Lcom/google/cast/NetworkTask$Listener;

.field private d:Lcom/google/cast/Logger;

.field protected mCurrentRequest:Lcom/google/cast/NetworkRequest;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Void;

    sput-object v0, Lcom/google/cast/NetworkTask;->a:[Ljava/lang/Object;

    return-void
.end method

.method public varargs constructor <init>([Lcom/google/cast/NetworkRequest;)V
    .locals 2
    .param p1, "requests"    # [Lcom/google/cast/NetworkRequest;

    .prologue
    .line 52
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 53
    iput-object p1, p0, Lcom/google/cast/NetworkTask;->b:[Lcom/google/cast/NetworkRequest;

    .line 54
    new-instance v0, Lcom/google/cast/Logger;

    const-string v1, "NetworkTask"

    invoke-direct {v0, v1}, Lcom/google/cast/Logger;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/cast/NetworkTask;->d:Lcom/google/cast/Logger;

    .line 55
    return-void
.end method


# virtual methods
.method public final cancel()V
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/google/cast/NetworkTask;->mCurrentRequest:Lcom/google/cast/NetworkRequest;

    if-eqz v0, :cond_0

    .line 97
    iget-object v0, p0, Lcom/google/cast/NetworkTask;->mCurrentRequest:Lcom/google/cast/NetworkRequest;

    invoke-virtual {v0}, Lcom/google/cast/NetworkRequest;->cancel()V

    .line 99
    :cond_0
    const/4 v0, 0x1

    invoke-super {p0, v0}, Landroid/os/AsyncTask;->cancel(Z)Z

    .line 100
    return-void
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;
    .locals 5
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    const/4 v0, 0x0

    .line 111
    iget-object v1, p0, Lcom/google/cast/NetworkTask;->d:Lcom/google/cast/Logger;

    const-string v2, "doInBackground"

    new-array v3, v0, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 114
    iget-object v2, p0, Lcom/google/cast/NetworkTask;->b:[Lcom/google/cast/NetworkRequest;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v0, v2, v1

    .line 115
    invoke-virtual {p0}, Lcom/google/cast/NetworkTask;->isCancelled()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 116
    const/16 v0, -0x63

    .line 126
    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/cast/NetworkTask;->mCurrentRequest:Lcom/google/cast/NetworkRequest;

    .line 127
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0

    .line 119
    :cond_1
    iput-object v0, p0, Lcom/google/cast/NetworkTask;->mCurrentRequest:Lcom/google/cast/NetworkRequest;

    .line 120
    iget-object v0, p0, Lcom/google/cast/NetworkTask;->mCurrentRequest:Lcom/google/cast/NetworkRequest;

    invoke-virtual {v0}, Lcom/google/cast/NetworkRequest;->execute()I

    move-result v0

    .line 121
    if-nez v0, :cond_0

    .line 114
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 20
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/cast/NetworkTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final execute()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 68
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 73
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "executeOnExecutor"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    const-class v4, Ljava/util/concurrent/Executor;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-class v4, [Ljava/lang/Object;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 75
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    sget-object v3, Lcom/google/cast/NetworkTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    sget-object v3, Lcom/google/cast/NetworkTask;->a:[Ljava/lang/Object;

    aput-object v3, v1, v2

    invoke-virtual {v0, p0, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_3

    .line 89
    :goto_0
    return-void

    .line 77
    :catch_0
    move-exception v0

    .line 78
    iget-object v1, p0, Lcom/google/cast/NetworkTask;->d:Lcom/google/cast/Logger;

    const-string v2, "reflection failed"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/cast/Logger;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 88
    :cond_0
    :goto_1
    new-array v0, v5, [Ljava/lang/Void;

    invoke-super {p0, v0}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 79
    :catch_1
    move-exception v0

    .line 80
    iget-object v1, p0, Lcom/google/cast/NetworkTask;->d:Lcom/google/cast/Logger;

    const-string v2, "reflection failed"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/cast/Logger;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 81
    :catch_2
    move-exception v0

    .line 82
    iget-object v1, p0, Lcom/google/cast/NetworkTask;->d:Lcom/google/cast/Logger;

    const-string v2, "reflection failed"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/cast/Logger;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 83
    :catch_3
    move-exception v0

    .line 84
    iget-object v1, p0, Lcom/google/cast/NetworkTask;->d:Lcom/google/cast/Logger;

    const-string v2, "reflection failed"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/cast/Logger;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method

.method protected final onCancelled(Ljava/lang/Integer;)V
    .locals 1
    .param p1, "result"    # Ljava/lang/Integer;

    .prologue
    .line 151
    iget-object v0, p0, Lcom/google/cast/NetworkTask;->c:Lcom/google/cast/NetworkTask$Listener;

    if-eqz v0, :cond_0

    .line 152
    iget-object v0, p0, Lcom/google/cast/NetworkTask;->c:Lcom/google/cast/NetworkTask$Listener;

    invoke-interface {v0}, Lcom/google/cast/NetworkTask$Listener;->onTaskCancelled()V

    .line 154
    :cond_0
    return-void
.end method

.method protected bridge synthetic onCancelled(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 20
    check-cast p1, Ljava/lang/Integer;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/cast/NetworkTask;->onCancelled(Ljava/lang/Integer;)V

    return-void
.end method

.method protected final onPostExecute(Ljava/lang/Integer;)V
    .locals 4
    .param p1, "result"    # Ljava/lang/Integer;

    .prologue
    .line 132
    iget-object v0, p0, Lcom/google/cast/NetworkTask;->d:Lcom/google/cast/Logger;

    const-string v1, "onPostExecute result: %d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 133
    iget-object v0, p0, Lcom/google/cast/NetworkTask;->c:Lcom/google/cast/NetworkTask$Listener;

    if-eqz v0, :cond_0

    .line 134
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 142
    iget-object v0, p0, Lcom/google/cast/NetworkTask;->c:Lcom/google/cast/NetworkTask$Listener;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/cast/NetworkTask$Listener;->onTaskFailed(I)V

    .line 146
    :cond_0
    :goto_0
    return-void

    .line 136
    :sswitch_0
    iget-object v0, p0, Lcom/google/cast/NetworkTask;->c:Lcom/google/cast/NetworkTask$Listener;

    invoke-interface {v0}, Lcom/google/cast/NetworkTask$Listener;->onTaskCompleted()V

    goto :goto_0

    .line 139
    :sswitch_1
    iget-object v0, p0, Lcom/google/cast/NetworkTask;->c:Lcom/google/cast/NetworkTask$Listener;

    invoke-interface {v0}, Lcom/google/cast/NetworkTask$Listener;->onTaskCancelled()V

    goto :goto_0

    .line 134
    :sswitch_data_0
    .sparse-switch
        -0x63 -> :sswitch_1
        0x0 -> :sswitch_0
    .end sparse-switch
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 20
    check-cast p1, Ljava/lang/Integer;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/cast/NetworkTask;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method

.method public final setListener(Lcom/google/cast/NetworkTask$Listener;)V
    .locals 0
    .param p1, "listener"    # Lcom/google/cast/NetworkTask$Listener;

    .prologue
    .line 61
    iput-object p1, p0, Lcom/google/cast/NetworkTask;->c:Lcom/google/cast/NetworkTask$Listener;

    .line 62
    return-void
.end method
