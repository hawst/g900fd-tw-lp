.class abstract Lcom/google/cast/b;
.super Landroid/support/v7/media/MediaRouteProvider;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/cast/b$a;
    }
.end annotation


# static fields
.field private static final h:Lcom/google/cast/Logger;


# instance fields
.field protected a:Lcom/google/cast/CastContext;

.field private b:Lcom/google/cast/DeviceManager;

.field private c:Lcom/google/cast/DeviceManager$Listener;

.field private d:Lcom/google/cast/a;

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/content/IntentFilter;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/cast/CastDevice;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/cast/b$a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 41
    new-instance v0, Lcom/google/cast/Logger;

    const-string v1, "BaseCastMediaRouteProvider"

    invoke-direct {v0, v1}, Lcom/google/cast/Logger;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/cast/b;->h:Lcom/google/cast/Logger;

    return-void
.end method

.method protected constructor <init>(Lcom/google/cast/CastContext;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 44
    invoke-virtual {p1}, Lcom/google/cast/CastContext;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/support/v7/media/MediaRouteProvider;-><init>(Landroid/content/Context;)V

    .line 46
    iput-object p1, p0, Lcom/google/cast/b;->a:Lcom/google/cast/CastContext;

    .line 48
    new-instance v0, Lcom/google/cast/DeviceManager;

    iget-object v1, p0, Lcom/google/cast/b;->a:Lcom/google/cast/CastContext;

    invoke-direct {v0, v1}, Lcom/google/cast/DeviceManager;-><init>(Lcom/google/cast/CastContext;)V

    iput-object v0, p0, Lcom/google/cast/b;->b:Lcom/google/cast/DeviceManager;

    .line 49
    new-instance v0, Lcom/google/cast/b$1;

    invoke-direct {v0, p0}, Lcom/google/cast/b$1;-><init>(Lcom/google/cast/b;)V

    iput-object v0, p0, Lcom/google/cast/b;->c:Lcom/google/cast/DeviceManager$Listener;

    .line 77
    new-instance v0, Lcom/google/cast/a;

    iget-object v1, p0, Lcom/google/cast/b;->a:Lcom/google/cast/CastContext;

    iget-object v2, p0, Lcom/google/cast/b;->c:Lcom/google/cast/DeviceManager$Listener;

    invoke-direct {v0, v1, v3, v3, v2}, Lcom/google/cast/a;-><init>(Lcom/google/cast/CastContext;Ljava/lang/String;Ljava/util/List;Lcom/google/cast/DeviceManager$Listener;)V

    iput-object v0, p0, Lcom/google/cast/b;->d:Lcom/google/cast/a;

    .line 79
    iget-object v0, p0, Lcom/google/cast/b;->b:Lcom/google/cast/DeviceManager;

    iget-object v1, p0, Lcom/google/cast/b;->d:Lcom/google/cast/a;

    invoke-virtual {v0, v1}, Lcom/google/cast/DeviceManager;->a(Lcom/google/cast/DeviceManager$Listener;)V

    .line 80
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/cast/b;->f:Ljava/util/Map;

    .line 81
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/cast/b;->g:Ljava/util/Map;

    .line 82
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/cast/b;->e:Ljava/util/List;

    .line 84
    invoke-direct {p0}, Lcom/google/cast/b;->b()V

    .line 85
    return-void
.end method

.method static synthetic a()Lcom/google/cast/Logger;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/google/cast/b;->h:Lcom/google/cast/Logger;

    return-object v0
.end method

.method static synthetic a(Lcom/google/cast/b;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/cast/b;->g:Ljava/util/Map;

    return-object v0
.end method

.method protected static final a(Landroid/content/IntentFilter;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 226
    :try_start_0
    invoke-virtual {p0, p1}, Landroid/content/IntentFilter;->addDataType(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/content/IntentFilter$MalformedMimeTypeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 230
    return-void

    .line 227
    :catch_0
    move-exception v0

    .line 228
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private final a(Lcom/google/cast/CastDevice;)V
    .locals 2

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/cast/b;->f:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/google/cast/CastDevice;->getDeviceId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    return-void
.end method

.method static synthetic a(Lcom/google/cast/b;Lcom/google/cast/CastDevice;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/google/cast/b;->a(Lcom/google/cast/CastDevice;)V

    return-void
.end method

.method private b()V
    .locals 5

    .prologue
    .line 96
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 98
    iget-object v0, p0, Lcom/google/cast/b;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/cast/CastDevice;

    .line 99
    invoke-direct {p0, v0}, Lcom/google/cast/b;->c(Lcom/google/cast/CastDevice;)Landroid/support/v7/media/MediaRouteDescriptor;

    move-result-object v0

    .line 100
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 103
    :cond_0
    new-instance v0, Landroid/support/v7/media/MediaRouteProviderDescriptor$Builder;

    invoke-direct {v0}, Landroid/support/v7/media/MediaRouteProviderDescriptor$Builder;-><init>()V

    invoke-virtual {v0, v1}, Landroid/support/v7/media/MediaRouteProviderDescriptor$Builder;->addRoutes(Ljava/util/Collection;)Landroid/support/v7/media/MediaRouteProviderDescriptor$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/media/MediaRouteProviderDescriptor$Builder;->build()Landroid/support/v7/media/MediaRouteProviderDescriptor;

    move-result-object v0

    .line 106
    invoke-virtual {p0, v0}, Lcom/google/cast/b;->setDescriptor(Landroid/support/v7/media/MediaRouteProviderDescriptor;)V

    .line 108
    sget-object v0, Lcom/google/cast/b;->h:Lcom/google/cast/Logger;

    const-string v2, "published %d routes"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, v4

    invoke-virtual {v0, v2, v3}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 109
    return-void
.end method

.method private final b(Lcom/google/cast/CastDevice;)V
    .locals 2

    .prologue
    .line 92
    iget-object v0, p0, Lcom/google/cast/b;->f:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/google/cast/CastDevice;->getDeviceId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    return-void
.end method

.method static synthetic b(Lcom/google/cast/b;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/google/cast/b;->b()V

    return-void
.end method

.method static synthetic b(Lcom/google/cast/b;Lcom/google/cast/CastDevice;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/google/cast/b;->b(Lcom/google/cast/CastDevice;)V

    return-void
.end method

.method private c(Lcom/google/cast/CastDevice;)Landroid/support/v7/media/MediaRouteDescriptor;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 183
    .line 185
    iget-object v0, p0, Lcom/google/cast/b;->g:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/google/cast/CastDevice;->getDeviceId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/cast/b$a;

    .line 186
    if-eqz v0, :cond_0

    .line 187
    invoke-virtual {v0}, Lcom/google/cast/b$a;->a()I

    move-result v1

    .line 188
    invoke-virtual {v0}, Lcom/google/cast/b$a;->b()Z

    move-result v0

    .line 191
    :goto_0
    invoke-virtual {p1}, Lcom/google/cast/CastDevice;->getModelName()Ljava/lang/String;

    move-result-object v2

    .line 194
    const-string v3, "(Eureka|Chromekey)( Dongle)?"

    const-string v4, "Chromecast"

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 196
    new-instance v3, Landroid/support/v7/media/MediaRouteDescriptor$Builder;

    invoke-virtual {p1}, Lcom/google/cast/CastDevice;->getDeviceId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/google/cast/CastDevice;->getFriendlyName()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Landroid/support/v7/media/MediaRouteDescriptor$Builder;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Landroid/support/v7/media/MediaRouteDescriptor$Builder;->setDescription(Ljava/lang/String;)Landroid/support/v7/media/MediaRouteDescriptor$Builder;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/support/v7/media/MediaRouteDescriptor$Builder;->setConnecting(Z)Landroid/support/v7/media/MediaRouteDescriptor$Builder;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/support/v7/media/MediaRouteDescriptor$Builder;->setVolumeHandling(I)Landroid/support/v7/media/MediaRouteDescriptor$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/support/v7/media/MediaRouteDescriptor$Builder;->setVolume(I)Landroid/support/v7/media/MediaRouteDescriptor$Builder;

    move-result-object v0

    const/16 v1, 0x14

    invoke-virtual {v0, v1}, Landroid/support/v7/media/MediaRouteDescriptor$Builder;->setVolumeMax(I)Landroid/support/v7/media/MediaRouteDescriptor$Builder;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/support/v7/media/MediaRouteDescriptor$Builder;->setPlaybackType(I)Landroid/support/v7/media/MediaRouteDescriptor$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/cast/b;->e:Ljava/util/List;

    invoke-virtual {v0, v1}, Landroid/support/v7/media/MediaRouteDescriptor$Builder;->addControlFilters(Ljava/util/Collection;)Landroid/support/v7/media/MediaRouteDescriptor$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/media/MediaRouteDescriptor$Builder;->build()Landroid/support/v7/media/MediaRouteDescriptor;

    move-result-object v0

    .line 207
    return-object v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method static synthetic c(Lcom/google/cast/b;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/cast/b;->f:Ljava/util/Map;

    return-object v0
.end method


# virtual methods
.method protected abstract a(Lcom/google/cast/CastDevice;Ljava/lang/String;)Lcom/google/cast/b$a;
.end method

.method protected final a(Landroid/content/IntentFilter;)V
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/google/cast/b;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 113
    return-void
.end method

.method public onCreateRouteController(Ljava/lang/String;)Landroid/support/v7/media/MediaRouteProvider$RouteController;
    .locals 3
    .param p1, "routeId"    # Ljava/lang/String;

    .prologue
    .line 212
    iget-object v0, p0, Lcom/google/cast/b;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/cast/CastDevice;

    .line 213
    if-nez v0, :cond_0

    .line 214
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown route ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 216
    :cond_0
    iget-object v1, p0, Lcom/google/cast/b;->g:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/cast/b$a;

    .line 217
    if-nez v1, :cond_1

    .line 218
    invoke-virtual {p0, v0, p1}, Lcom/google/cast/b;->a(Lcom/google/cast/CastDevice;Ljava/lang/String;)Lcom/google/cast/b$a;

    move-result-object v1

    .line 219
    iget-object v0, p0, Lcom/google/cast/b;->g:Ljava/util/Map;

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 221
    :cond_1
    return-object v1
.end method

.method public onDiscoveryRequestChanged(Landroid/support/v7/media/MediaRouteDiscoveryRequest;)V
    .locals 14
    .param p1, "request"    # Landroid/support/v7/media/MediaRouteDiscoveryRequest;

    .prologue
    const/4 v6, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 117
    sget-object v0, Lcom/google/cast/b;->h:Lcom/google/cast/Logger;

    const-string v3, "in onDiscoveryRequestChanged: request=%s"

    new-array v4, v1, [Ljava/lang/Object;

    aput-object p1, v4, v2

    invoke-virtual {v0, v3, v4}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 124
    if-eqz p1, :cond_a

    .line 125
    invoke-virtual {p1}, Landroid/support/v7/media/MediaRouteDiscoveryRequest;->getSelector()Landroid/support/v7/media/MediaRouteSelector;

    move-result-object v0

    .line 126
    invoke-virtual {v0}, Landroid/support/v7/media/MediaRouteSelector;->getControlCategories()Ljava/util/List;

    move-result-object v9

    .line 128
    const-string v0, "com.google.cast.CATEGORY_CAST"

    invoke-interface {v9, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    move v0, v1

    move v3, v1

    .line 133
    :goto_0
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v10

    move v8, v2

    move v4, v0

    move-object v5, v6

    move-object v7, v6

    .line 134
    :goto_1
    if-ge v8, v10, :cond_4

    .line 135
    invoke-interface {v9, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 136
    const-string v11, "android.media.intent.category.REMOTE_PLAYBACK"

    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    move v0, v1

    move-object v4, v5

    move-object v5, v7

    .line 134
    :goto_2
    add-int/lit8 v7, v8, 0x1

    move v8, v7

    move-object v7, v5

    move-object v5, v4

    move v4, v0

    goto :goto_1

    .line 138
    :cond_0
    const-string v11, "com.google.cast.CATEGORY_CAST_APP_NAME:"

    invoke-virtual {v0, v11}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_1

    if-eqz v3, :cond_1

    .line 140
    if-nez v7, :cond_8

    .line 141
    const-string v7, "com.google.cast.CATEGORY_CAST_APP_NAME:"

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {v0, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    move v13, v4

    move-object v4, v5

    move-object v5, v0

    move v0, v13

    goto :goto_2

    .line 144
    :cond_1
    const-string v11, "com.google.cast.CATEGORY_CAST_APP_PROTOCOLS:"

    invoke-virtual {v0, v11}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_8

    if-eqz v3, :cond_8

    .line 147
    if-nez v5, :cond_8

    .line 148
    const-string v5, "com.google.cast.CATEGORY_CAST_APP_PROTOCOLS:"

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v0, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 150
    const-string v5, ","

    invoke-static {v0, v5}, Landroid/text/TextUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v11

    .line 151
    new-instance v0, Ljava/util/ArrayList;

    array-length v5, v11

    invoke-direct {v0, v5}, Ljava/util/ArrayList;-><init>(I)V

    move v5, v2

    .line 152
    :goto_3
    array-length v12, v11

    if-ge v5, v12, :cond_3

    .line 153
    aget-object v12, v11, v5

    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_2

    .line 154
    aget-object v12, v11, v5

    invoke-interface {v0, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 152
    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    .line 157
    :cond_3
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_7

    move v0, v4

    move-object v5, v7

    move-object v4, v6

    .line 158
    goto :goto_2

    .line 164
    :cond_4
    iget-object v0, p0, Lcom/google/cast/b;->d:Lcom/google/cast/a;

    invoke-virtual {v0, v7, v5}, Lcom/google/cast/a;->a(Ljava/lang/String;Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 166
    sget-object v0, Lcom/google/cast/b;->h:Lcom/google/cast/Logger;

    const-string v3, "filter criteria changed (name: %s, protocols: %s); flushing routes"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v7, v6, v2

    aput-object v5, v6, v1

    invoke-virtual {v0, v3, v6}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 168
    iget-object v0, p0, Lcom/google/cast/b;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 169
    invoke-direct {p0}, Lcom/google/cast/b;->b()V

    .line 173
    :cond_5
    :goto_4
    if-eqz v4, :cond_6

    .line 174
    sget-object v0, Lcom/google/cast/b;->h:Lcom/google/cast/Logger;

    const-string v1, "starting a scan"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 175
    iget-object v0, p0, Lcom/google/cast/b;->b:Lcom/google/cast/DeviceManager;

    invoke-virtual {v0}, Lcom/google/cast/DeviceManager;->a()V

    .line 179
    :goto_5
    return-void

    .line 177
    :cond_6
    iget-object v0, p0, Lcom/google/cast/b;->b:Lcom/google/cast/DeviceManager;

    invoke-virtual {v0}, Lcom/google/cast/DeviceManager;->b()V

    goto :goto_5

    :cond_7
    move-object v5, v7

    move-object v13, v0

    move v0, v4

    move-object v4, v13

    goto/16 :goto_2

    :cond_8
    move v0, v4

    move-object v4, v5

    move-object v5, v7

    goto/16 :goto_2

    :cond_9
    move v0, v2

    move v3, v2

    goto/16 :goto_0

    :cond_a
    move v4, v2

    goto :goto_4
.end method
