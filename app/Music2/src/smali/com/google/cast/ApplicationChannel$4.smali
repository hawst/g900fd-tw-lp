.class Lcom/google/cast/ApplicationChannel$4;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/cast/WebSocket$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/cast/ApplicationChannel;-><init>(Lcom/google/cast/CastContext;IJLcom/google/cast/ApplicationChannel$a;Landroid/os/Handler;Lcom/google/cast/CastDevice;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/google/cast/ApplicationChannel;


# direct methods
.method constructor <init>(Lcom/google/cast/ApplicationChannel;)V
    .locals 0

    .prologue
    .line 176
    iput-object p1, p0, Lcom/google/cast/ApplicationChannel$4;->a:Lcom/google/cast/ApplicationChannel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onConnected()V
    .locals 4

    .prologue
    .line 179
    iget-object v0, p0, Lcom/google/cast/ApplicationChannel$4;->a:Lcom/google/cast/ApplicationChannel;

    invoke-static {v0}, Lcom/google/cast/ApplicationChannel;->c(Lcom/google/cast/ApplicationChannel;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 180
    iget-object v0, p0, Lcom/google/cast/ApplicationChannel$4;->a:Lcom/google/cast/ApplicationChannel;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lcom/google/cast/ApplicationChannel;->a(Lcom/google/cast/ApplicationChannel;J)J

    .line 181
    iget-object v0, p0, Lcom/google/cast/ApplicationChannel$4;->a:Lcom/google/cast/ApplicationChannel;

    invoke-static {v0}, Lcom/google/cast/ApplicationChannel;->e(Lcom/google/cast/ApplicationChannel;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/cast/ApplicationChannel$4;->a:Lcom/google/cast/ApplicationChannel;

    invoke-static {v1}, Lcom/google/cast/ApplicationChannel;->d(Lcom/google/cast/ApplicationChannel;)Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 183
    :cond_0
    iget-object v0, p0, Lcom/google/cast/ApplicationChannel$4;->a:Lcom/google/cast/ApplicationChannel;

    invoke-static {v0}, Lcom/google/cast/ApplicationChannel;->f(Lcom/google/cast/ApplicationChannel;)V

    .line 184
    iget-object v0, p0, Lcom/google/cast/ApplicationChannel$4;->a:Lcom/google/cast/ApplicationChannel;

    invoke-static {v0}, Lcom/google/cast/ApplicationChannel;->e(Lcom/google/cast/ApplicationChannel;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/google/cast/ApplicationChannel$4$1;

    invoke-direct {v1, p0}, Lcom/google/cast/ApplicationChannel$4$1;-><init>(Lcom/google/cast/ApplicationChannel$4;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 194
    return-void
.end method

.method public onConnectionFailed(I)V
    .locals 4
    .param p1, "error"    # I

    .prologue
    .line 198
    iget-object v0, p0, Lcom/google/cast/ApplicationChannel$4;->a:Lcom/google/cast/ApplicationChannel;

    const-wide/16 v2, 0x0

    invoke-static {v0, v2, v3}, Lcom/google/cast/ApplicationChannel;->a(Lcom/google/cast/ApplicationChannel;J)J

    .line 199
    iget-object v0, p0, Lcom/google/cast/ApplicationChannel$4;->a:Lcom/google/cast/ApplicationChannel;

    invoke-static {v0}, Lcom/google/cast/ApplicationChannel;->f(Lcom/google/cast/ApplicationChannel;)V

    .line 200
    iget-object v0, p0, Lcom/google/cast/ApplicationChannel$4;->a:Lcom/google/cast/ApplicationChannel;

    invoke-static {v0}, Lcom/google/cast/ApplicationChannel;->e(Lcom/google/cast/ApplicationChannel;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/google/cast/ApplicationChannel$4$2;

    invoke-direct {v1, p0, p1}, Lcom/google/cast/ApplicationChannel$4$2;-><init>(Lcom/google/cast/ApplicationChannel$4;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 211
    return-void
.end method

.method public onContinuationMessageReceived(Ljava/lang/String;Z)V
    .locals 2
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "isFinal"    # Z

    .prologue
    .line 229
    iget-object v0, p0, Lcom/google/cast/ApplicationChannel$4;->a:Lcom/google/cast/ApplicationChannel;

    const/4 v1, 0x0

    invoke-static {v0, v1, p1}, Lcom/google/cast/ApplicationChannel;->a(Lcom/google/cast/ApplicationChannel;Ljava/lang/String;Ljava/lang/String;)V

    .line 230
    return-void
.end method

.method public onContinuationMessageReceived([BZ)V
    .locals 2
    .param p1, "message"    # [B
    .param p2, "isFinal"    # Z

    .prologue
    .line 234
    iget-object v0, p0, Lcom/google/cast/ApplicationChannel$4;->a:Lcom/google/cast/ApplicationChannel;

    const/4 v1, 0x0

    invoke-static {v0, v1, p1}, Lcom/google/cast/ApplicationChannel;->a(Lcom/google/cast/ApplicationChannel;Ljava/lang/String;[B)V

    .line 235
    return-void
.end method

.method public onDisconnected(II)V
    .locals 5
    .param p1, "error"    # I
    .param p2, "closeCode"    # I

    .prologue
    .line 239
    iget-object v0, p0, Lcom/google/cast/ApplicationChannel$4;->a:Lcom/google/cast/ApplicationChannel;

    invoke-static {v0}, Lcom/google/cast/ApplicationChannel;->h(Lcom/google/cast/ApplicationChannel;)Lcom/google/cast/Logger;

    move-result-object v0

    const-string v1, "onDisconnected(); error=%d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 240
    iget-object v0, p0, Lcom/google/cast/ApplicationChannel$4;->a:Lcom/google/cast/ApplicationChannel;

    invoke-static {v0}, Lcom/google/cast/ApplicationChannel;->f(Lcom/google/cast/ApplicationChannel;)V

    .line 241
    iget-object v0, p0, Lcom/google/cast/ApplicationChannel$4;->a:Lcom/google/cast/ApplicationChannel;

    invoke-static {v0}, Lcom/google/cast/ApplicationChannel;->e(Lcom/google/cast/ApplicationChannel;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/google/cast/ApplicationChannel$4$3;

    invoke-direct {v1, p0, p1}, Lcom/google/cast/ApplicationChannel$4$3;-><init>(Lcom/google/cast/ApplicationChannel$4;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 251
    return-void
.end method

.method public onMessageReceived(Ljava/lang/String;Z)V
    .locals 2
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "isFinal"    # Z

    .prologue
    .line 215
    if-nez p2, :cond_0

    .line 216
    iget-object v0, p0, Lcom/google/cast/ApplicationChannel$4;->a:Lcom/google/cast/ApplicationChannel;

    const/4 v1, 0x0

    invoke-static {v0, v1, p1}, Lcom/google/cast/ApplicationChannel;->a(Lcom/google/cast/ApplicationChannel;Ljava/lang/String;Ljava/lang/String;)V

    .line 220
    :goto_0
    return-void

    .line 218
    :cond_0
    iget-object v0, p0, Lcom/google/cast/ApplicationChannel$4;->a:Lcom/google/cast/ApplicationChannel;

    invoke-static {v0, p1}, Lcom/google/cast/ApplicationChannel;->a(Lcom/google/cast/ApplicationChannel;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onMessageReceived([BZ)V
    .locals 2
    .param p1, "message"    # [B
    .param p2, "isFinal"    # Z

    .prologue
    .line 224
    iget-object v0, p0, Lcom/google/cast/ApplicationChannel$4;->a:Lcom/google/cast/ApplicationChannel;

    const/4 v1, 0x0

    invoke-static {v0, v1, p1}, Lcom/google/cast/ApplicationChannel;->a(Lcom/google/cast/ApplicationChannel;Ljava/lang/String;[B)V

    .line 225
    return-void
.end method
