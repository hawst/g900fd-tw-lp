.class Lcom/google/cast/r$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/cast/r;->b()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/google/cast/r;


# direct methods
.method constructor <init>(Lcom/google/cast/r;)V
    .locals 0

    .prologue
    .line 81
    iput-object p1, p0, Lcom/google/cast/r$1;->a:Lcom/google/cast/r;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 85
    :try_start_0
    iget-object v0, p0, Lcom/google/cast/r$1;->a:Lcom/google/cast/r;

    invoke-static {v0}, Lcom/google/cast/r;->a(Lcom/google/cast/r;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 91
    iget-object v0, p0, Lcom/google/cast/r$1;->a:Lcom/google/cast/r;

    invoke-static {v0}, Lcom/google/cast/r;->c(Lcom/google/cast/r;)V

    .line 92
    iget-object v0, p0, Lcom/google/cast/r$1;->a:Lcom/google/cast/r;

    invoke-static {v0, v4}, Lcom/google/cast/r;->a(Lcom/google/cast/r;Ljava/lang/Thread;)Ljava/lang/Thread;

    .line 94
    :goto_0
    return-void

    .line 86
    :catch_0
    move-exception v0

    .line 87
    :try_start_1
    iget-object v1, p0, Lcom/google/cast/r$1;->a:Lcom/google/cast/r;

    invoke-static {v1}, Lcom/google/cast/r;->b(Lcom/google/cast/r;)Lcom/google/cast/Logger;

    move-result-object v1

    const-string v2, "Unexpected throwable in selector loop"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/cast/Logger;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 88
    iget-object v1, p0, Lcom/google/cast/r$1;->a:Lcom/google/cast/r;

    invoke-static {v1, v0}, Lcom/google/cast/r;->a(Lcom/google/cast/r;Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 89
    iget-object v0, p0, Lcom/google/cast/r$1;->a:Lcom/google/cast/r;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/cast/r;->a(Lcom/google/cast/r;Z)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 91
    iget-object v0, p0, Lcom/google/cast/r$1;->a:Lcom/google/cast/r;

    invoke-static {v0}, Lcom/google/cast/r;->c(Lcom/google/cast/r;)V

    .line 92
    iget-object v0, p0, Lcom/google/cast/r$1;->a:Lcom/google/cast/r;

    invoke-static {v0, v4}, Lcom/google/cast/r;->a(Lcom/google/cast/r;Ljava/lang/Thread;)Ljava/lang/Thread;

    goto :goto_0

    .line 91
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/cast/r$1;->a:Lcom/google/cast/r;

    invoke-static {v1}, Lcom/google/cast/r;->c(Lcom/google/cast/r;)V

    .line 92
    iget-object v1, p0, Lcom/google/cast/r$1;->a:Lcom/google/cast/r;

    invoke-static {v1, v4}, Lcom/google/cast/r;->a(Lcom/google/cast/r;Ljava/lang/Thread;)Ljava/lang/Thread;

    throw v0
.end method
