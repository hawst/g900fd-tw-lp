.class final Lcom/google/cast/c$a;
.super Lcom/google/cast/b$a;
.source "SourceFile"

# interfaces
.implements Lcom/google/cast/ApplicationSession$Listener;
.implements Lcom/google/cast/MediaProtocolCommand$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/cast/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "a"
.end annotation


# instance fields
.field final synthetic d:Lcom/google/cast/c;

.field private e:Lcom/google/cast/ApplicationSession;

.field private f:Lcom/google/cast/MediaProtocolMessageStream;

.field private g:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/google/cast/MediaProtocolCommand;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/app/PendingIntent;",
            ">;"
        }
    .end annotation
.end field

.field private i:Lcom/google/cast/c$b;

.field private j:Ljava/lang/String;

.field private k:D

.field private l:Z

.field private m:Z

.field private n:Ljava/lang/Runnable;

.field private o:J

.field private p:Z


# direct methods
.method public constructor <init>(Lcom/google/cast/c;Lcom/google/cast/CastDevice;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 167
    iput-object p1, p0, Lcom/google/cast/c$a;->d:Lcom/google/cast/c;

    .line 168
    invoke-direct {p0, p1, p2, p3}, Lcom/google/cast/b$a;-><init>(Lcom/google/cast/b;Lcom/google/cast/CastDevice;Ljava/lang/String;)V

    .line 169
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    iput-wide v0, p0, Lcom/google/cast/c$a;->k:D

    .line 170
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/cast/c$a;->g:Ljava/util/LinkedList;

    .line 171
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/cast/c$a;->h:Ljava/util/Map;

    .line 172
    new-instance v0, Lcom/google/cast/c$a$1;

    invoke-direct {v0, p0, p1}, Lcom/google/cast/c$a$1;-><init>(Lcom/google/cast/c$a;Lcom/google/cast/c;)V

    iput-object v0, p0, Lcom/google/cast/c$a;->n:Ljava/lang/Runnable;

    .line 178
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/cast/c$a;->o:J

    .line 179
    return-void
.end method

.method private a(J)D
    .locals 5

    .prologue
    .line 518
    long-to-double v0, p1

    const-wide v2, 0x408f400000000000L    # 1000.0

    div-double/2addr v0, v2

    return-wide v0
.end method

.method static synthetic a(Lcom/google/cast/c$a;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 152
    iput-object p1, p0, Lcom/google/cast/c$a;->j:Ljava/lang/String;

    return-object p1
.end method

.method private a(Lcom/google/cast/MediaProtocolCommand;Landroid/support/v7/media/MediaRouter$ControlRequestCallback;)V
    .locals 1

    .prologue
    .line 295
    invoke-virtual {p1, p0}, Lcom/google/cast/MediaProtocolCommand;->setListener(Lcom/google/cast/MediaProtocolCommand$Listener;)V

    .line 296
    if-eqz p2, :cond_0

    .line 297
    const-string v0, "cb"

    invoke-virtual {p1, v0, p2}, Lcom/google/cast/MediaProtocolCommand;->putUserObject(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 299
    :cond_0
    iget-object v0, p0, Lcom/google/cast/c$a;->g:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 300
    return-void
.end method

.method static synthetic a(Lcom/google/cast/c$a;)V
    .locals 0

    .prologue
    .line 152
    invoke-direct {p0}, Lcom/google/cast/c$a;->c()V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 287
    iget-object v0, p0, Lcom/google/cast/c$a;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/cast/c$a;->j:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 288
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "item ID does not match"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 290
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/google/cast/c$a;Z)Z
    .locals 0

    .prologue
    .line 152
    iput-boolean p1, p0, Lcom/google/cast/c$a;->l:Z

    return p1
.end method

.method private a(Lcom/google/cast/c$b;)Z
    .locals 14

    .prologue
    const-wide/16 v12, 0x0

    const/4 v6, 0x3

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 323
    invoke-static {}, Lcom/google/cast/c;->b()Lcom/google/cast/Logger;

    move-result-object v0

    const-string v3, "processRemotePlaybackRequest(); mSession=%s"

    new-array v4, v1, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/cast/c$a;->e:Lcom/google/cast/ApplicationSession;

    aput-object v5, v4, v2

    invoke-virtual {v0, v3, v4}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 324
    iget-object v0, p0, Lcom/google/cast/c$a;->e:Lcom/google/cast/ApplicationSession;

    if-nez v0, :cond_0

    .line 326
    iput-object p1, p0, Lcom/google/cast/c$a;->i:Lcom/google/cast/c$b;

    .line 327
    invoke-direct {p0}, Lcom/google/cast/c$a;->c()V

    move v0, v1

    .line 513
    :goto_0
    return v0

    .line 331
    :cond_0
    iget-object v0, p0, Lcom/google/cast/c$a;->e:Lcom/google/cast/ApplicationSession;

    invoke-virtual {v0}, Lcom/google/cast/ApplicationSession;->isStarting()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 332
    invoke-static {}, Lcom/google/cast/c;->b()Lcom/google/cast/Logger;

    move-result-object v0

    const-string v3, "session is starting, so saving request for later"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v3, v2}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 334
    iput-object p1, p0, Lcom/google/cast/c$a;->i:Lcom/google/cast/c$b;

    move v0, v1

    .line 335
    goto :goto_0

    .line 338
    :cond_1
    iget-object v4, p1, Lcom/google/cast/c$b;->b:Landroid/support/v7/media/MediaRouter$ControlRequestCallback;

    .line 340
    iget-object v0, p0, Lcom/google/cast/c$a;->f:Lcom/google/cast/MediaProtocolMessageStream;

    if-nez v0, :cond_2

    .line 341
    invoke-static {}, Lcom/google/cast/c;->b()Lcom/google/cast/Logger;

    move-result-object v0

    const-string v1, "ramp stream is null, so calling onError"

    new-array v3, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v3}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 342
    iget-object v0, p0, Lcom/google/cast/c$a;->d:Lcom/google/cast/c;

    invoke-virtual {v0}, Lcom/google/cast/c;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/google/cast/R$string;->error_no_session:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/cast/c$a;->d:Lcom/google/cast/c;

    invoke-static {v1, v6}, Lcom/google/cast/c;->a(Lcom/google/cast/c;I)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Landroid/support/v7/media/MediaRouter$ControlRequestCallback;->onError(Ljava/lang/String;Landroid/os/Bundle;)V

    move v0, v2

    .line 344
    goto :goto_0

    .line 347
    :cond_2
    iget-object v0, p1, Lcom/google/cast/c$b;->a:Landroid/content/Intent;

    .line 348
    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    .line 351
    :try_start_0
    const-string v5, "android.media.intent.action.PLAY"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_b

    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v5

    if-eqz v5, :cond_b

    .line 352
    invoke-direct {p0}, Lcom/google/cast/c$a;->d()V

    .line 354
    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v5

    .line 355
    if-nez v5, :cond_3

    move v0, v2

    .line 356
    goto :goto_0

    .line 358
    :cond_3
    invoke-static {}, Lcom/google/cast/c;->b()Lcom/google/cast/Logger;

    move-result-object v3

    const-string v6, "Device received play request, uri %s"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v5, v7, v8

    invoke-virtual {v3, v6, v7}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 360
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v6

    .line 362
    new-instance v7, Lcom/google/cast/ContentMetadata;

    invoke-direct {v7}, Lcom/google/cast/ContentMetadata;-><init>()V

    .line 363
    iget-object v3, p0, Lcom/google/cast/c$a;->d:Lcom/google/cast/c;

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v8

    invoke-static {}, Lcom/google/cast/c;->c()Ljava/util/Set;

    move-result-object v9

    invoke-static {v3, v8, v9}, Lcom/google/cast/c;->a(Lcom/google/cast/c;Landroid/os/Bundle;Ljava/util/Set;)Lorg/json/JSONObject;

    move-result-object v3

    .line 365
    invoke-virtual {v7, v3}, Lcom/google/cast/ContentMetadata;->setContentInfo(Lorg/json/JSONObject;)V

    .line 367
    const-string v8, "android.media.intent.extra.ITEM_METADATA"

    invoke-virtual {v0, v8}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v8

    .line 368
    if-eqz v8, :cond_9

    .line 369
    const-string v9, "android.media.metadata.TITLE"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 370
    const-string v9, "android.media.metadata.TITLE"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 371
    if-eqz v9, :cond_4

    .line 372
    invoke-virtual {v7, v9}, Lcom/google/cast/ContentMetadata;->setTitle(Ljava/lang/String;)V

    .line 375
    :cond_4
    const-string v9, "android.media.metadata.ARTWORK_URI"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 376
    const-string v9, "android.media.metadata.ARTWORK_URI"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 377
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_5

    .line 378
    invoke-static {v9}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    invoke-virtual {v7, v9}, Lcom/google/cast/ContentMetadata;->setImageUrl(Landroid/net/Uri;)V

    .line 381
    :cond_5
    const-string v9, "android.media.metadata.ARTIST"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 382
    const-string v9, "android.media.metadata.ARTIST"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v9

    .line 383
    if-eqz v9, :cond_6

    .line 385
    :try_start_1
    const-string v10, "artist"

    invoke-virtual {v3, v10, v9}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 389
    :cond_6
    :goto_1
    :try_start_2
    const-string v9, "android.media.metadata.ALBUM_TITLE"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 390
    const-string v9, "android.media.metadata.ALBUM_TITLE"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v8

    .line 391
    if-eqz v8, :cond_7

    .line 393
    :try_start_3
    const-string v9, "album_title"

    invoke-virtual {v3, v9, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/IllegalStateException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 401
    :cond_7
    :goto_2
    :try_start_4
    const-string v3, "android.media.intent.extra.ITEM_POSITION"

    const-wide/16 v8, 0x0

    invoke-virtual {v0, v3, v8, v9}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v8

    .line 407
    cmp-long v3, v8, v12

    if-lez v3, :cond_a

    .line 408
    invoke-direct {p0, v8, v9}, Lcom/google/cast/c$a;->a(J)D

    move-result-wide v8

    iput-wide v8, p0, Lcom/google/cast/c$a;->k:D

    move v3, v2

    .line 415
    :goto_3
    const-string v8, "android.media.intent.extra.ITEM_STATUS_UPDATE_RECEIVER"

    invoke-virtual {v0, v8}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    .line 417
    invoke-static {}, Lcom/google/cast/c;->b()Lcom/google/cast/Logger;

    move-result-object v8

    const-string v9, "Got a pending intent for status updates: %s"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object v0, v10, v11

    invoke-virtual {v8, v9, v10}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 419
    if-eqz v0, :cond_8

    .line 420
    iget-object v8, p0, Lcom/google/cast/c$a;->h:Ljava/util/Map;

    invoke-interface {v8, v6, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 423
    :cond_8
    iget-object v0, p0, Lcom/google/cast/c$a;->f:Lcom/google/cast/MediaProtocolMessageStream;

    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5, v7, v3}, Lcom/google/cast/MediaProtocolMessageStream;->loadMedia(Ljava/lang/String;Lcom/google/cast/ContentMetadata;Z)Lcom/google/cast/MediaProtocolCommand;

    move-result-object v0

    .line 425
    invoke-static {}, Lcom/google/cast/c;->b()Lcom/google/cast/Logger;

    move-result-object v3

    const-string v5, "attaching item ID to command: %s"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v6, v7, v8

    invoke-virtual {v3, v5, v7}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 426
    const-string v3, "itemid"

    invoke-virtual {v0, v3, v6}, Lcom/google/cast/MediaProtocolCommand;->putUserObject(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 427
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/google/cast/c$a;->m:Z

    .line 429
    const/4 v3, 0x0

    invoke-direct {p0, v0, v3}, Lcom/google/cast/c$a;->a(Lcom/google/cast/MediaProtocolCommand;Landroid/support/v7/media/MediaRouter$ControlRequestCallback;)V

    .line 431
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 432
    const-string v3, "android.media.intent.extra.ITEM_ID"

    invoke-virtual {v0, v3, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 434
    new-instance v3, Landroid/support/v7/media/MediaItemStatus$Builder;

    const/4 v5, 0x3

    invoke-direct {v3, v5}, Landroid/support/v7/media/MediaItemStatus$Builder;-><init>(I)V

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    invoke-virtual {v3, v6, v7}, Landroid/support/v7/media/MediaItemStatus$Builder;->setTimestamp(J)Landroid/support/v7/media/MediaItemStatus$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v7/media/MediaItemStatus$Builder;->build()Landroid/support/v7/media/MediaItemStatus;

    move-result-object v3

    .line 438
    const-string v5, "android.media.intent.extra.ITEM_STATUS"

    invoke-virtual {v3}, Landroid/support/v7/media/MediaItemStatus;->asBundle()Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v0, v5, v3}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 441
    invoke-virtual {v4, v0}, Landroid/support/v7/media/MediaRouter$ControlRequestCallback;->onResult(Landroid/os/Bundle;)V

    :goto_4
    move v0, v1

    .line 513
    goto/16 :goto_0

    .line 398
    :cond_9
    invoke-static {}, Lcom/google/cast/c;->b()Lcom/google/cast/Logger;

    move-result-object v3

    const-string v8, "No content metadata!"

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Object;

    invoke-virtual {v3, v8, v9}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_4
    .catch Ljava/lang/IllegalStateException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto/16 :goto_2

    .line 504
    :catch_0
    move-exception v0

    .line 505
    invoke-static {}, Lcom/google/cast/c;->b()Lcom/google/cast/Logger;

    move-result-object v3

    const-string v4, "can\'t process command; %s"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v2

    invoke-virtual {v3, v4, v1}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v2

    .line 506
    goto/16 :goto_0

    .line 411
    :cond_a
    const-wide/high16 v8, -0x4010000000000000L    # -1.0

    :try_start_5
    iput-wide v8, p0, Lcom/google/cast/c$a;->k:D

    move v3, v1

    .line 412
    goto/16 :goto_3

    .line 442
    :cond_b
    const-string v5, "android.media.intent.action.PAUSE"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_c

    .line 443
    iget-object v0, p0, Lcom/google/cast/c$a;->f:Lcom/google/cast/MediaProtocolMessageStream;

    invoke-virtual {v0}, Lcom/google/cast/MediaProtocolMessageStream;->stop()V

    .line 444
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/cast/c$a;->l:Z
    :try_end_5
    .catch Ljava/lang/IllegalStateException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_4

    .line 507
    :catch_1
    move-exception v0

    .line 508
    invoke-static {}, Lcom/google/cast/c;->b()Lcom/google/cast/Logger;

    move-result-object v1

    const-string v3, "can\'t send RAMP command"

    new-array v5, v2, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v3, v5}, Lcom/google/cast/Logger;->d(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 509
    iget-object v0, p0, Lcom/google/cast/c$a;->d:Lcom/google/cast/c;

    invoke-virtual {v0}, Lcom/google/cast/c;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/google/cast/R$string;->error_ramp_command_failed:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/cast/c$a;->d:Lcom/google/cast/c;

    const/4 v3, 0x4

    invoke-static {v1, v3}, Lcom/google/cast/c;->a(Lcom/google/cast/c;I)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Landroid/support/v7/media/MediaRouter$ControlRequestCallback;->onError(Ljava/lang/String;Landroid/os/Bundle;)V

    move v0, v2

    .line 511
    goto/16 :goto_0

    .line 445
    :cond_c
    :try_start_6
    const-string v5, "android.media.intent.action.RESUME"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_e

    .line 446
    invoke-direct {p0}, Lcom/google/cast/c$a;->d()V

    .line 449
    invoke-static {}, Lcom/google/cast/c;->b()Lcom/google/cast/Logger;

    move-result-object v0

    const-string v3, "resume; pending seek position is %f"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-wide v8, p0, Lcom/google/cast/c$a;->k:D

    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v0, v3, v5}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 450
    iget-wide v6, p0, Lcom/google/cast/c$a;->k:D

    const-wide/16 v8, 0x0

    cmpl-double v0, v6, v8

    if-ltz v0, :cond_d

    .line 451
    iget-wide v6, p0, Lcom/google/cast/c$a;->k:D

    .line 452
    const-wide/high16 v8, -0x4010000000000000L    # -1.0

    iput-wide v8, p0, Lcom/google/cast/c$a;->k:D

    .line 453
    iget-object v0, p0, Lcom/google/cast/c$a;->f:Lcom/google/cast/MediaProtocolMessageStream;

    invoke-virtual {v0, v6, v7}, Lcom/google/cast/MediaProtocolMessageStream;->playFrom(D)Lcom/google/cast/MediaProtocolCommand;

    move-result-object v0

    invoke-direct {p0, v0, v4}, Lcom/google/cast/c$a;->a(Lcom/google/cast/MediaProtocolCommand;Landroid/support/v7/media/MediaRouter$ControlRequestCallback;)V

    goto/16 :goto_4

    .line 455
    :cond_d
    iget-object v0, p0, Lcom/google/cast/c$a;->f:Lcom/google/cast/MediaProtocolMessageStream;

    invoke-virtual {v0}, Lcom/google/cast/MediaProtocolMessageStream;->resume()Lcom/google/cast/MediaProtocolCommand;

    move-result-object v0

    invoke-direct {p0, v0, v4}, Lcom/google/cast/c$a;->a(Lcom/google/cast/MediaProtocolCommand;Landroid/support/v7/media/MediaRouter$ControlRequestCallback;)V

    goto/16 :goto_4

    .line 457
    :cond_e
    const-string v5, "android.media.intent.action.STOP"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_f

    .line 460
    iget-object v0, p0, Lcom/google/cast/c$a;->f:Lcom/google/cast/MediaProtocolMessageStream;

    invoke-virtual {v0}, Lcom/google/cast/MediaProtocolMessageStream;->stop()V

    goto/16 :goto_4

    .line 461
    :cond_f
    const-string v5, "android.media.intent.action.SEEK"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_11

    .line 462
    invoke-direct {p0}, Lcom/google/cast/c$a;->d()V

    .line 463
    const-string v3, "android.media.intent.extra.ITEM_ID"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/google/cast/c$a;->a(Ljava/lang/String;)V

    .line 464
    const-string v3, "android.media.intent.extra.ITEM_POSITION"

    const-wide/16 v6, 0x0

    invoke-virtual {v0, v3, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v6

    .line 466
    iget-object v0, p0, Lcom/google/cast/c$a;->f:Lcom/google/cast/MediaProtocolMessageStream;

    invoke-virtual {v0}, Lcom/google/cast/MediaProtocolMessageStream;->getPlayerState()Lcom/google/cast/MediaProtocolMessageStream$PlayerState;

    move-result-object v0

    sget-object v3, Lcom/google/cast/MediaProtocolMessageStream$PlayerState;->PLAYING:Lcom/google/cast/MediaProtocolMessageStream$PlayerState;

    if-eq v0, v3, :cond_10

    .line 467
    invoke-static {}, Lcom/google/cast/c;->b()Lcom/google/cast/Logger;

    move-result-object v0

    const-string v3, "saving pending seek position of %d ms"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    aput-object v9, v5, v8

    invoke-virtual {v0, v3, v5}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 468
    invoke-direct {p0, v6, v7}, Lcom/google/cast/c$a;->a(J)D

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/cast/c$a;->k:D

    goto/16 :goto_4

    .line 470
    :cond_10
    invoke-static {}, Lcom/google/cast/c;->b()Lcom/google/cast/Logger;

    move-result-object v0

    const-string v3, "seeking to %d ms"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    aput-object v9, v5, v8

    invoke-virtual {v0, v3, v5}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 471
    iget-object v0, p0, Lcom/google/cast/c$a;->f:Lcom/google/cast/MediaProtocolMessageStream;

    invoke-direct {p0, v6, v7}, Lcom/google/cast/c$a;->a(J)D

    move-result-wide v6

    invoke-virtual {v0, v6, v7}, Lcom/google/cast/MediaProtocolMessageStream;->playFrom(D)Lcom/google/cast/MediaProtocolCommand;

    move-result-object v0

    invoke-direct {p0, v0, v4}, Lcom/google/cast/c$a;->a(Lcom/google/cast/MediaProtocolCommand;Landroid/support/v7/media/MediaRouter$ControlRequestCallback;)V

    goto/16 :goto_4

    .line 474
    :cond_11
    const-string v5, "android.media.intent.action.GET_STATUS"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_13

    .line 475
    const-string v3, "android.media.intent.extra.ITEM_ID"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 476
    invoke-direct {p0, v0}, Lcom/google/cast/c$a;->a(Ljava/lang/String;)V

    .line 478
    iget-object v0, p0, Lcom/google/cast/c$a;->f:Lcom/google/cast/MediaProtocolMessageStream;

    if-eqz v0, :cond_12

    .line 479
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 480
    const-string v3, "android.media.intent.extra.ITEM_STATUS"

    invoke-direct {p0}, Lcom/google/cast/c$a;->g()Landroid/os/Bundle;

    move-result-object v5

    invoke-virtual {v0, v3, v5}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 482
    invoke-virtual {v4, v0}, Landroid/support/v7/media/MediaRouter$ControlRequestCallback;->onResult(Landroid/os/Bundle;)V

    goto/16 :goto_4

    .line 484
    :cond_12
    iget-object v0, p0, Lcom/google/cast/c$a;->d:Lcom/google/cast/c;

    invoke-virtual {v0}, Lcom/google/cast/c;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v3, Lcom/google/cast/R$string;->error_no_session:I

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/google/cast/c$a;->d:Lcom/google/cast/c;

    const/4 v5, 0x3

    invoke-static {v3, v5}, Lcom/google/cast/c;->a(Lcom/google/cast/c;I)Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v4, v0, v3}, Landroid/support/v7/media/MediaRouter$ControlRequestCallback;->onError(Ljava/lang/String;Landroid/os/Bundle;)V

    goto/16 :goto_4

    .line 487
    :cond_13
    const-string v5, "com.google.cast.ACTION_SYNC_STATUS"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_16

    .line 488
    const-string v3, "android.media.intent.extra.ITEM_STATUS_UPDATE_RECEIVER"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    .line 490
    if-nez v0, :cond_14

    move v0, v2

    .line 491
    goto/16 :goto_0

    .line 493
    :cond_14
    iget-object v3, p0, Lcom/google/cast/c$a;->f:Lcom/google/cast/MediaProtocolMessageStream;

    if-eqz v3, :cond_15

    .line 494
    iget-object v3, p0, Lcom/google/cast/c$a;->f:Lcom/google/cast/MediaProtocolMessageStream;

    invoke-virtual {v3}, Lcom/google/cast/MediaProtocolMessageStream;->requestStatus()Lcom/google/cast/MediaProtocolCommand;

    move-result-object v3

    .line 495
    const-string v5, "sync"

    invoke-virtual {v3, v5, v0}, Lcom/google/cast/MediaProtocolCommand;->putUserObject(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 496
    invoke-direct {p0, v3, v4}, Lcom/google/cast/c$a;->a(Lcom/google/cast/MediaProtocolCommand;Landroid/support/v7/media/MediaRouter$ControlRequestCallback;)V

    goto/16 :goto_4

    .line 498
    :cond_15
    iget-object v0, p0, Lcom/google/cast/c$a;->d:Lcom/google/cast/c;

    invoke-virtual {v0}, Lcom/google/cast/c;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v3, Lcom/google/cast/R$string;->error_no_session:I

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/google/cast/c$a;->d:Lcom/google/cast/c;

    const/4 v5, 0x3

    invoke-static {v3, v5}, Lcom/google/cast/c;->a(Lcom/google/cast/c;I)Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v4, v0, v3}, Landroid/support/v7/media/MediaRouter$ControlRequestCallback;->onError(Ljava/lang/String;Landroid/os/Bundle;)V
    :try_end_6
    .catch Ljava/lang/IllegalStateException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1

    goto/16 :goto_4

    :cond_16
    move v0, v2

    .line 502
    goto/16 :goto_0

    .line 394
    :catch_2
    move-exception v3

    goto/16 :goto_2

    .line 386
    :catch_3
    move-exception v9

    goto/16 :goto_1
.end method

.method private b(D)J
    .locals 3

    .prologue
    .line 523
    const-wide v0, 0x408f400000000000L    # 1000.0

    mul-double/2addr v0, p1

    double-to-long v0, v0

    return-wide v0
.end method

.method static synthetic b(Lcom/google/cast/c$a;)Z
    .locals 1

    .prologue
    .line 152
    iget-boolean v0, p0, Lcom/google/cast/c$a;->m:Z

    return v0
.end method

.method static synthetic c(Lcom/google/cast/c$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/google/cast/c$a;->j:Ljava/lang/String;

    return-object v0
.end method

.method private c()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 245
    invoke-static {}, Lcom/google/cast/c;->b()Lcom/google/cast/Logger;

    move-result-object v0

    const-string v1, "startSession(); mSession=%s"

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/cast/c$a;->e:Lcom/google/cast/ApplicationSession;

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 246
    iget-object v0, p0, Lcom/google/cast/c$a;->e:Lcom/google/cast/ApplicationSession;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/cast/c$a;->e:Lcom/google/cast/ApplicationSession;

    invoke-virtual {v0}, Lcom/google/cast/ApplicationSession;->isResumable()Z

    move-result v0

    if-nez v0, :cond_2

    .line 248
    :cond_0
    invoke-static {}, Lcom/google/cast/c;->b()Lcom/google/cast/Logger;

    move-result-object v0

    const-string v1, "starting a new session"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 249
    new-instance v0, Lcom/google/cast/ApplicationSession;

    iget-object v1, p0, Lcom/google/cast/c$a;->d:Lcom/google/cast/c;

    iget-object v1, v1, Lcom/google/cast/c;->a:Lcom/google/cast/CastContext;

    iget-object v2, p0, Lcom/google/cast/c$a;->a:Lcom/google/cast/CastDevice;

    invoke-direct {v0, v1, v2}, Lcom/google/cast/ApplicationSession;-><init>(Lcom/google/cast/CastContext;Lcom/google/cast/CastDevice;)V

    iput-object v0, p0, Lcom/google/cast/c$a;->e:Lcom/google/cast/ApplicationSession;

    .line 250
    iget-object v0, p0, Lcom/google/cast/c$a;->e:Lcom/google/cast/ApplicationSession;

    invoke-virtual {v0, p0}, Lcom/google/cast/ApplicationSession;->setListener(Lcom/google/cast/ApplicationSession$Listener;)V

    .line 251
    iget-object v0, p0, Lcom/google/cast/c$a;->e:Lcom/google/cast/ApplicationSession;

    iget-object v1, p0, Lcom/google/cast/c$a;->d:Lcom/google/cast/c;

    invoke-static {v1}, Lcom/google/cast/c;->b(Lcom/google/cast/c;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/cast/ApplicationSession;->setApplicationOptions(I)V

    .line 254
    :try_start_0
    iget-object v0, p0, Lcom/google/cast/c$a;->e:Lcom/google/cast/ApplicationSession;

    iget-object v1, p0, Lcom/google/cast/c$a;->d:Lcom/google/cast/c;

    invoke-static {v1}, Lcom/google/cast/c;->c(Lcom/google/cast/c;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/cast/c$a;->d:Lcom/google/cast/c;

    invoke-static {v2}, Lcom/google/cast/c;->d(Lcom/google/cast/c;)Lcom/google/cast/MimeData;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/cast/ApplicationSession;->startSession(Ljava/lang/String;Lcom/google/cast/MimeData;)V

    .line 255
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/cast/c$a;->a(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 276
    :cond_1
    :goto_0
    return-void

    .line 256
    :catch_0
    move-exception v0

    .line 257
    iput-object v5, p0, Lcom/google/cast/c$a;->e:Lcom/google/cast/ApplicationSession;

    .line 261
    invoke-direct {p0}, Lcom/google/cast/c$a;->f()Z

    goto :goto_0

    .line 265
    :cond_2
    invoke-static {}, Lcom/google/cast/c;->b()Lcom/google/cast/Logger;

    move-result-object v0

    const-string v1, "resuming an existing session"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 267
    :try_start_1
    iget-object v0, p0, Lcom/google/cast/c$a;->e:Lcom/google/cast/ApplicationSession;

    invoke-virtual {v0}, Lcom/google/cast/ApplicationSession;->resumeSession()V

    .line 268
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/cast/c$a;->a(I)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 269
    :catch_1
    move-exception v0

    .line 270
    invoke-direct {p0}, Lcom/google/cast/c$a;->f()Z

    move-result v0

    if-nez v0, :cond_1

    .line 271
    invoke-direct {p0}, Lcom/google/cast/c$a;->e()V

    .line 272
    iput-object v5, p0, Lcom/google/cast/c$a;->e:Lcom/google/cast/ApplicationSession;

    goto :goto_0
.end method

.method static synthetic d(Lcom/google/cast/c$a;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/google/cast/c$a;->h:Ljava/util/Map;

    return-object v0
.end method

.method private d()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 280
    iget-object v0, p0, Lcom/google/cast/c$a;->g:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    const/16 v1, 0x32

    if-lt v0, v1, :cond_0

    .line 281
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "command backlog is full"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 283
    :cond_0
    return-void
.end method

.method static synthetic e(Lcom/google/cast/c$a;)Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 152
    invoke-direct {p0}, Lcom/google/cast/c$a;->g()Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method private e()V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v6, 0x0

    .line 564
    iget-object v0, p0, Lcom/google/cast/c$a;->j:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 565
    invoke-static {}, Lcom/google/cast/c;->b()Lcom/google/cast/Logger;

    move-result-object v0

    const-string v1, "invalidating item %s"

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/cast/c$a;->j:Ljava/lang/String;

    aput-object v3, v2, v6

    invoke-virtual {v0, v1, v2}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 566
    iget-object v0, p0, Lcom/google/cast/c$a;->h:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/cast/c$a;->j:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    .line 567
    if-eqz v0, :cond_0

    .line 568
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 569
    const-string v2, "android.media.intent.extra.ITEM_ID"

    iget-object v3, p0, Lcom/google/cast/c$a;->j:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 571
    new-instance v2, Landroid/support/v7/media/MediaItemStatus$Builder;

    const/4 v3, 0x6

    invoke-direct {v2, v3}, Landroid/support/v7/media/MediaItemStatus$Builder;-><init>(I)V

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Landroid/support/v7/media/MediaItemStatus$Builder;->setTimestamp(J)Landroid/support/v7/media/MediaItemStatus$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v7/media/MediaItemStatus$Builder;->build()Landroid/support/v7/media/MediaItemStatus;

    move-result-object v2

    .line 576
    const-string v3, "android.media.intent.extra.ITEM_STATUS"

    invoke-virtual {v2}, Landroid/support/v7/media/MediaItemStatus;->asBundle()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 579
    :try_start_0
    invoke-static {}, Lcom/google/cast/c;->b()Lcom/google/cast/Logger;

    move-result-object v2

    const-string v3, "Invoking the PendingIntent with: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    invoke-virtual {v2, v3, v4}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 580
    iget-object v2, p0, Lcom/google/cast/c$a;->d:Lcom/google/cast/c;

    invoke-virtual {v2}, Lcom/google/cast/c;->getContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3, v1}, Landroid/app/PendingIntent;->send(Landroid/content/Context;ILandroid/content/Intent;)V
    :try_end_0
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_0 .. :try_end_0} :catch_0

    .line 584
    :goto_0
    iget-object v0, p0, Lcom/google/cast/c$a;->h:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/cast/c$a;->j:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 586
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/cast/c$a;->j:Ljava/lang/String;

    .line 588
    :cond_1
    return-void

    .line 581
    :catch_0
    move-exception v0

    .line 582
    invoke-static {}, Lcom/google/cast/c;->b()Lcom/google/cast/Logger;

    move-result-object v1

    const-string v2, "exception while sending PendingIntent"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/cast/Logger;->d(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method static synthetic f(Lcom/google/cast/c$a;)Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 152
    invoke-direct {p0}, Lcom/google/cast/c$a;->h()Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method private f()Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 597
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .line 598
    iget-wide v4, p0, Lcom/google/cast/c$a;->o:J

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x2710

    cmp-long v2, v2, v4

    if-gez v2, :cond_0

    .line 599
    invoke-static {}, Lcom/google/cast/c;->b()Lcom/google/cast/Logger;

    move-result-object v2

    const-string v3, "Scheduling a reconnect attempt"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v1}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 600
    const/4 v1, 0x5

    invoke-virtual {p0, v1}, Lcom/google/cast/c$a;->a(I)V

    .line 601
    iget-object v1, p0, Lcom/google/cast/c$a;->d:Lcom/google/cast/c;

    invoke-virtual {v1}, Lcom/google/cast/c;->getHandler()Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/google/cast/c$a;->n:Ljava/lang/Runnable;

    const-wide/16 v4, 0xbb8

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 602
    iput-boolean v0, p0, Lcom/google/cast/c$a;->p:Z

    .line 609
    :goto_0
    return v0

    .line 606
    :cond_0
    invoke-static {}, Lcom/google/cast/c;->b()Lcom/google/cast/Logger;

    move-result-object v0

    const-string v2, "Giving up on reconnecting route\'s session."

    new-array v3, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v2, v3}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 607
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/cast/c$a;->a(I)V

    .line 608
    iput-boolean v1, p0, Lcom/google/cast/c$a;->p:Z

    move v0, v1

    .line 609
    goto :goto_0
.end method

.method private g()Landroid/os/Bundle;
    .locals 8

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x0

    .line 851
    iget-object v2, p0, Lcom/google/cast/c$a;->f:Lcom/google/cast/MediaProtocolMessageStream;

    invoke-virtual {v2}, Lcom/google/cast/MediaProtocolMessageStream;->getPlayerState()Lcom/google/cast/MediaProtocolMessageStream$PlayerState;

    move-result-object v2

    .line 854
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 856
    iget-boolean v4, p0, Lcom/google/cast/c$a;->l:Z

    if-eqz v4, :cond_0

    .line 877
    :goto_0
    :pswitch_0
    iget-object v1, p0, Lcom/google/cast/c$a;->f:Lcom/google/cast/MediaProtocolMessageStream;

    invoke-virtual {v1}, Lcom/google/cast/MediaProtocolMessageStream;->getStreamDuration()D

    move-result-wide v4

    invoke-direct {p0, v4, v5}, Lcom/google/cast/c$a;->b(D)J

    move-result-wide v4

    .line 878
    iget-object v1, p0, Lcom/google/cast/c$a;->f:Lcom/google/cast/MediaProtocolMessageStream;

    invoke-virtual {v1}, Lcom/google/cast/MediaProtocolMessageStream;->getStreamPosition()D

    move-result-wide v6

    invoke-direct {p0, v6, v7}, Lcom/google/cast/c$a;->b(D)J

    move-result-wide v6

    .line 880
    new-instance v1, Landroid/support/v7/media/MediaItemStatus$Builder;

    invoke-direct {v1, v0}, Landroid/support/v7/media/MediaItemStatus$Builder;-><init>(I)V

    invoke-virtual {v1, v4, v5}, Landroid/support/v7/media/MediaItemStatus$Builder;->setContentDuration(J)Landroid/support/v7/media/MediaItemStatus$Builder;

    move-result-object v0

    invoke-virtual {v0, v6, v7}, Landroid/support/v7/media/MediaItemStatus$Builder;->setContentPosition(J)Landroid/support/v7/media/MediaItemStatus$Builder;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/support/v7/media/MediaItemStatus$Builder;->setExtras(Landroid/os/Bundle;)Landroid/support/v7/media/MediaItemStatus$Builder;

    move-result-object v0

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/support/v7/media/MediaItemStatus$Builder;->setTimestamp(J)Landroid/support/v7/media/MediaItemStatus$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/media/MediaItemStatus$Builder;->build()Landroid/support/v7/media/MediaItemStatus;

    move-result-object v0

    .line 887
    invoke-virtual {v0}, Landroid/support/v7/media/MediaItemStatus;->asBundle()Landroid/os/Bundle;

    move-result-object v0

    return-object v0

    .line 859
    :cond_0
    sget-object v4, Lcom/google/cast/c$1;->a:[I

    invoke-virtual {v2}, Lcom/google/cast/MediaProtocolMessageStream$PlayerState;->ordinal()I

    move-result v2

    aget v2, v4, v2

    packed-switch v2, :pswitch_data_0

    move v0, v1

    goto :goto_0

    .line 861
    :pswitch_1
    invoke-static {}, Lcom/google/cast/c;->b()Lcom/google/cast/Logger;

    move-result-object v0

    const-string v2, "buildCurrentItemStatusBundle; player is IDLE, so FINISHED"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v2, v1}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 862
    const/4 v0, 0x4

    .line 863
    goto :goto_0

    .line 870
    :pswitch_2
    iget-object v0, p0, Lcom/google/cast/c$a;->f:Lcom/google/cast/MediaProtocolMessageStream;

    invoke-virtual {v0}, Lcom/google/cast/MediaProtocolMessageStream;->isStreamProgressing()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x3

    goto :goto_0

    .line 859
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method static synthetic g(Lcom/google/cast/c$a;)Lcom/google/cast/MediaProtocolMessageStream;
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/google/cast/c$a;->f:Lcom/google/cast/MediaProtocolMessageStream;

    return-object v0
.end method

.method private h()Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 894
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 896
    iget-object v1, p0, Lcom/google/cast/c$a;->f:Lcom/google/cast/MediaProtocolMessageStream;

    if-eqz v1, :cond_2

    .line 897
    iget-object v1, p0, Lcom/google/cast/c$a;->f:Lcom/google/cast/MediaProtocolMessageStream;

    invoke-virtual {v1}, Lcom/google/cast/MediaProtocolMessageStream;->getTitle()Ljava/lang/String;

    move-result-object v1

    .line 898
    if-eqz v1, :cond_0

    .line 899
    const-string v2, "android.media.metadata.TITLE"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 901
    :cond_0
    iget-object v1, p0, Lcom/google/cast/c$a;->f:Lcom/google/cast/MediaProtocolMessageStream;

    invoke-virtual {v1}, Lcom/google/cast/MediaProtocolMessageStream;->getImageUrl()Landroid/net/Uri;

    move-result-object v1

    .line 902
    if-eqz v1, :cond_1

    .line 903
    const-string v2, "android.media.metadata.ARTWORK_URI"

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 906
    :cond_1
    iget-object v1, p0, Lcom/google/cast/c$a;->f:Lcom/google/cast/MediaProtocolMessageStream;

    invoke-virtual {v1}, Lcom/google/cast/MediaProtocolMessageStream;->getContentInfo()Lorg/json/JSONObject;

    move-result-object v1

    .line 907
    if-eqz v1, :cond_2

    .line 908
    const-string v2, "artist"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 909
    if-eqz v1, :cond_2

    .line 910
    const-string v2, "android.media.metadata.ARTIST"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 915
    :cond_2
    return-object v0
.end method


# virtual methods
.method public onCancelled(Lcom/google/cast/MediaProtocolCommand;)V
    .locals 1
    .param p1, "command"    # Lcom/google/cast/MediaProtocolCommand;

    .prologue
    .line 748
    iget-object v0, p0, Lcom/google/cast/c$a;->g:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 749
    return-void
.end method

.method public onCompleted(Lcom/google/cast/MediaProtocolCommand;)V
    .locals 7
    .param p1, "command"    # Lcom/google/cast/MediaProtocolCommand;

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 753
    iget-object v0, p0, Lcom/google/cast/c$a;->g:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 754
    invoke-virtual {p1}, Lcom/google/cast/MediaProtocolCommand;->getType()Ljava/lang/String;

    move-result-object v1

    .line 755
    invoke-static {}, Lcom/google/cast/c;->b()Lcom/google/cast/Logger;

    move-result-object v0

    const-string v2, "onCompleted; RAMP cmd is %s"

    new-array v3, v5, [Ljava/lang/Object;

    aput-object v1, v3, v6

    invoke-virtual {v0, v2, v3}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 756
    const-string v0, "cb"

    invoke-virtual {p1, v0}, Lcom/google/cast/MediaProtocolCommand;->getUserObject(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/media/MediaRouter$ControlRequestCallback;

    .line 759
    const-string v2, "LOAD"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 760
    iput-boolean v6, p0, Lcom/google/cast/c$a;->m:Z

    .line 763
    :cond_0
    invoke-virtual {p1}, Lcom/google/cast/MediaProtocolCommand;->hasError()Z

    move-result v2

    if-nez v2, :cond_6

    .line 764
    invoke-static {}, Lcom/google/cast/c;->b()Lcom/google/cast/Logger;

    move-result-object v2

    const-string v3, "RAMP command %s completed successfully"

    new-array v4, v5, [Ljava/lang/Object;

    aput-object v1, v4, v6

    invoke-virtual {v2, v3, v4}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 766
    iget-object v2, p0, Lcom/google/cast/c$a;->f:Lcom/google/cast/MediaProtocolMessageStream;

    invoke-virtual {v2}, Lcom/google/cast/MediaProtocolMessageStream;->getPlayerState()Lcom/google/cast/MediaProtocolMessageStream$PlayerState;

    move-result-object v2

    sget-object v3, Lcom/google/cast/MediaProtocolMessageStream$PlayerState;->PLAYING:Lcom/google/cast/MediaProtocolMessageStream$PlayerState;

    if-ne v2, v3, :cond_1

    .line 768
    iput-boolean v6, p0, Lcom/google/cast/c$a;->l:Z

    .line 771
    :cond_1
    const-string v2, "LOAD"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 772
    const-string v1, "itemid"

    invoke-virtual {p1, v1}, Lcom/google/cast/MediaProtocolCommand;->getUserObject(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, Lcom/google/cast/c$a;->j:Ljava/lang/String;

    .line 773
    invoke-static {}, Lcom/google/cast/c;->b()Lcom/google/cast/Logger;

    move-result-object v1

    const-string v2, "mCurrentItemId is now %s"

    new-array v3, v5, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/cast/c$a;->j:Ljava/lang/String;

    aput-object v4, v3, v6

    invoke-virtual {v1, v2, v3}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 797
    :cond_2
    :goto_0
    iget-wide v2, p0, Lcom/google/cast/c$a;->k:D

    const-wide/16 v4, 0x0

    cmpl-double v1, v2, v4

    if-ltz v1, :cond_3

    .line 798
    iget-wide v2, p0, Lcom/google/cast/c$a;->k:D

    .line 799
    const-wide/high16 v4, -0x4010000000000000L    # -1.0

    iput-wide v4, p0, Lcom/google/cast/c$a;->k:D

    .line 801
    :try_start_0
    invoke-direct {p0}, Lcom/google/cast/c$a;->d()V

    .line 802
    iget-object v1, p0, Lcom/google/cast/c$a;->f:Lcom/google/cast/MediaProtocolMessageStream;

    invoke-virtual {v1, v2, v3}, Lcom/google/cast/MediaProtocolMessageStream;->playFrom(D)Lcom/google/cast/MediaProtocolCommand;

    move-result-object v1

    invoke-direct {p0, v1, v0}, Lcom/google/cast/c$a;->a(Lcom/google/cast/MediaProtocolCommand;Landroid/support/v7/media/MediaRouter$ControlRequestCallback;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 845
    :cond_3
    :goto_1
    return-void

    .line 774
    :cond_4
    const-string v2, "INFO"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 775
    const-string v1, "sync"

    invoke-virtual {p1, v1}, Lcom/google/cast/MediaProtocolCommand;->getUserObject(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/PendingIntent;

    .line 777
    if-eqz v1, :cond_2

    .line 778
    invoke-static {}, Lcom/google/cast/c;->b()Lcom/google/cast/Logger;

    move-result-object v2

    const-string v3, "GET_STATUS completed for a sync-up request"

    new-array v4, v6, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v4}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 779
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 780
    iget-object v3, p0, Lcom/google/cast/c$a;->f:Lcom/google/cast/MediaProtocolMessageStream;

    invoke-virtual {v3}, Lcom/google/cast/MediaProtocolMessageStream;->getContentId()Ljava/lang/String;

    move-result-object v3

    .line 781
    if-eqz v3, :cond_5

    .line 782
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/cast/c$a;->j:Ljava/lang/String;

    .line 783
    const-string v3, "android.media.intent.extra.ITEM_ID"

    iget-object v4, p0, Lcom/google/cast/c$a;->j:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 784
    iget-object v3, p0, Lcom/google/cast/c$a;->h:Ljava/util/Map;

    iget-object v4, p0, Lcom/google/cast/c$a;->j:Ljava/lang/String;

    invoke-interface {v3, v4, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 785
    const-string v1, "android.media.intent.extra.ITEM_STATUS"

    invoke-direct {p0}, Lcom/google/cast/c$a;->g()Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 787
    const-string v1, "android.media.intent.extra.ITEM_METADATA"

    invoke-direct {p0}, Lcom/google/cast/c$a;->h()Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 792
    :cond_5
    invoke-virtual {v0, v2}, Landroid/support/v7/media/MediaRouter$ControlRequestCallback;->onResult(Landroid/os/Bundle;)V

    goto :goto_0

    .line 803
    :catch_0
    move-exception v0

    .line 804
    invoke-static {}, Lcom/google/cast/c;->b()Lcom/google/cast/Logger;

    move-result-object v0

    const-string v1, "Cannot enqueue a pending seek; backlog full"

    new-array v2, v6, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 805
    :catch_1
    move-exception v0

    .line 806
    invoke-static {}, Lcom/google/cast/c;->b()Lcom/google/cast/Logger;

    move-result-object v1

    const-string v2, "Cannot enqueue a pending seek"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/cast/Logger;->d(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 810
    :cond_6
    invoke-static {}, Lcom/google/cast/c;->b()Lcom/google/cast/Logger;

    move-result-object v2

    const-string v3, "RAMP command %s failed"

    new-array v4, v5, [Ljava/lang/Object;

    aput-object v1, v4, v6

    invoke-virtual {v2, v3, v4}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 811
    if-eqz v0, :cond_7

    .line 812
    iget-object v2, p0, Lcom/google/cast/c$a;->d:Lcom/google/cast/c;

    invoke-virtual {v2}, Lcom/google/cast/c;->getContext()Landroid/content/Context;

    move-result-object v2

    sget v3, Lcom/google/cast/R$string;->error_ramp_command_failed:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/cast/c$a;->d:Lcom/google/cast/c;

    const/4 v4, 0x4

    invoke-static {v3, v4}, Lcom/google/cast/c;->a(Lcom/google/cast/c;I)Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/support/v7/media/MediaRouter$ControlRequestCallback;->onError(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 818
    :cond_7
    const-string v0, "LOAD"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/google/cast/MediaProtocolCommand;->hasError()Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "ramp"

    invoke-virtual {p1}, Lcom/google/cast/MediaProtocolCommand;->getErrorDomain()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 820
    const-string v0, "itemid"

    invoke-virtual {p1, v0}, Lcom/google/cast/MediaProtocolCommand;->getUserObject(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 821
    iget-object v1, p0, Lcom/google/cast/c$a;->h:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/PendingIntent;

    .line 822
    if-eqz v1, :cond_3

    .line 823
    new-instance v2, Landroid/support/v7/media/MediaItemStatus$Builder;

    const/4 v3, 0x7

    invoke-direct {v2, v3}, Landroid/support/v7/media/MediaItemStatus$Builder;-><init>(I)V

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Landroid/support/v7/media/MediaItemStatus$Builder;->setTimestamp(J)Landroid/support/v7/media/MediaItemStatus$Builder;

    move-result-object v2

    .line 827
    invoke-virtual {p1}, Lcom/google/cast/MediaProtocolCommand;->getErrorInfo()Lorg/json/JSONObject;

    move-result-object v3

    .line 828
    if-eqz v3, :cond_8

    .line 829
    iget-object v4, p0, Lcom/google/cast/c$a;->d:Lcom/google/cast/c;

    invoke-static {v4, v3}, Lcom/google/cast/c;->a(Lcom/google/cast/c;Lorg/json/JSONObject;)Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/support/v7/media/MediaItemStatus$Builder;->setExtras(Landroid/os/Bundle;)Landroid/support/v7/media/MediaItemStatus$Builder;

    .line 832
    :cond_8
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 833
    const-string v4, "android.media.intent.extra.ITEM_ID"

    invoke-virtual {v3, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 834
    const-string v0, "android.media.intent.extra.ITEM_STATUS"

    invoke-virtual {v2}, Landroid/support/v7/media/MediaItemStatus$Builder;->build()Landroid/support/v7/media/MediaItemStatus;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v7/media/MediaItemStatus;->asBundle()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v3, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 838
    :try_start_1
    iget-object v0, p0, Lcom/google/cast/c$a;->d:Lcom/google/cast/c;

    invoke-virtual {v0}, Lcom/google/cast/c;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2, v3}, Landroid/app/PendingIntent;->send(Landroid/content/Context;ILandroid/content/Intent;)V
    :try_end_1
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_1 .. :try_end_1} :catch_2

    goto/16 :goto_1

    .line 839
    :catch_2
    move-exception v0

    .line 840
    invoke-static {}, Lcom/google/cast/c;->b()Lcom/google/cast/Logger;

    move-result-object v1

    const-string v2, "exception while sending PendingIntent"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/cast/Logger;->d(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_1
.end method

.method public onControlRequest(Landroid/content/Intent;Landroid/support/v7/media/MediaRouter$ControlRequestCallback;)Z
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "callback"    # Landroid/support/v7/media/MediaRouter$ControlRequestCallback;

    .prologue
    const/4 v0, 0x0

    .line 304
    invoke-static {}, Lcom/google/cast/c;->b()Lcom/google/cast/Logger;

    move-result-object v1

    const-string v2, "Received control request %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p1, v3, v0

    invoke-virtual {v1, v2, v3}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 305
    iget-boolean v1, p0, Lcom/google/cast/c$a;->p:Z

    if-eqz v1, :cond_1

    .line 306
    invoke-static {}, Lcom/google/cast/c;->b()Lcom/google/cast/Logger;

    move-result-object v1

    const-string v2, "Got a request while reconnecting, so rejecting it"

    new-array v3, v0, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 307
    iget-object v1, p0, Lcom/google/cast/c$a;->d:Lcom/google/cast/c;

    invoke-virtual {v1}, Lcom/google/cast/c;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/google/cast/R$string;->error_no_session:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/cast/c$a;->d:Lcom/google/cast/c;

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/cast/c;->a(Lcom/google/cast/c;I)Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {p2, v1, v2}, Landroid/support/v7/media/MediaRouter$ControlRequestCallback;->onError(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 319
    :cond_0
    :goto_0
    return v0

    .line 312
    :cond_1
    const-string v1, "android.media.intent.category.REMOTE_PLAYBACK"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->hasCategory(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "com.google.cast.CATEGORY_REMOTE_PLAYBACK_CAST_EXTENSIONS"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->hasCategory(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 315
    :cond_2
    new-instance v0, Lcom/google/cast/c$b;

    iget-object v1, p0, Lcom/google/cast/c$a;->d:Lcom/google/cast/c;

    invoke-direct {v0, v1, p1, p2}, Lcom/google/cast/c$b;-><init>(Lcom/google/cast/c;Landroid/content/Intent;Landroid/support/v7/media/MediaRouter$ControlRequestCallback;)V

    .line 316
    invoke-direct {p0, v0}, Lcom/google/cast/c$a;->a(Lcom/google/cast/c$b;)Z

    move-result v0

    goto :goto_0
.end method

.method public onSelect()V
    .locals 3

    .prologue
    .line 183
    invoke-static {}, Lcom/google/cast/c;->b()Lcom/google/cast/Logger;

    move-result-object v0

    const-string v1, "Selected device"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 184
    invoke-direct {p0}, Lcom/google/cast/c$a;->c()V

    .line 185
    return-void
.end method

.method public onSessionEnded(Lcom/google/cast/SessionError;)V
    .locals 6
    .param p1, "error"    # Lcom/google/cast/SessionError;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 530
    invoke-static {}, Lcom/google/cast/c;->b()Lcom/google/cast/Logger;

    move-result-object v0

    const-string v1, "onSessionEnded; error=%s"

    new-array v2, v5, [Ljava/lang/Object;

    aput-object p1, v2, v4

    invoke-virtual {v0, v1, v2}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 531
    iget-object v0, p0, Lcom/google/cast/c$a;->i:Lcom/google/cast/c$b;

    if-eqz v0, :cond_0

    .line 532
    iget-object v0, p0, Lcom/google/cast/c$a;->i:Lcom/google/cast/c$b;

    iget-object v0, v0, Lcom/google/cast/c$b;->b:Landroid/support/v7/media/MediaRouter$ControlRequestCallback;

    iget-object v1, p0, Lcom/google/cast/c$a;->d:Lcom/google/cast/c;

    invoke-virtual {v1}, Lcom/google/cast/c;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/google/cast/R$string;->error_session_ended:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/cast/c$a;->d:Lcom/google/cast/c;

    invoke-static {v2, v5}, Lcom/google/cast/c;->a(Lcom/google/cast/c;I)Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/media/MediaRouter$ControlRequestCallback;->onError(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 535
    iput-object v3, p0, Lcom/google/cast/c$a;->i:Lcom/google/cast/c$b;

    .line 538
    :cond_0
    if-eqz p1, :cond_2

    .line 539
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/cast/c$a;->o:J

    .line 540
    invoke-direct {p0}, Lcom/google/cast/c$a;->f()Z

    move-result v0

    if-nez v0, :cond_1

    .line 541
    invoke-direct {p0}, Lcom/google/cast/c$a;->e()V

    .line 542
    iget-object v0, p0, Lcom/google/cast/c$a;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 543
    iput-object v3, p0, Lcom/google/cast/c$a;->e:Lcom/google/cast/ApplicationSession;

    .line 552
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/google/cast/c$a;->g:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 553
    iput-object v3, p0, Lcom/google/cast/c$a;->f:Lcom/google/cast/MediaProtocolMessageStream;

    .line 554
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    iput-wide v0, p0, Lcom/google/cast/c$a;->k:D

    .line 555
    iput-boolean v4, p0, Lcom/google/cast/c$a;->l:Z

    .line 556
    iput-boolean v4, p0, Lcom/google/cast/c$a;->m:Z

    .line 557
    return-void

    .line 546
    :cond_2
    iput-object v3, p0, Lcom/google/cast/c$a;->e:Lcom/google/cast/ApplicationSession;

    .line 547
    iput-object v3, p0, Lcom/google/cast/c$a;->j:Ljava/lang/String;

    .line 548
    iget-object v0, p0, Lcom/google/cast/c$a;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 549
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/cast/c$a;->a(I)V

    goto :goto_0
.end method

.method public onSessionStartFailed(Lcom/google/cast/SessionError;)V
    .locals 5
    .param p1, "error"    # Lcom/google/cast/SessionError;

    .prologue
    const/4 v4, 0x0

    .line 723
    invoke-static {}, Lcom/google/cast/c;->b()Lcom/google/cast/Logger;

    move-result-object v0

    const-string v1, "onSessionStartFailed"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 724
    iget-object v0, p0, Lcom/google/cast/c$a;->e:Lcom/google/cast/ApplicationSession;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/cast/c$a;->e:Lcom/google/cast/ApplicationSession;

    invoke-virtual {v0}, Lcom/google/cast/ApplicationSession;->isResumable()Z

    move-result v0

    if-nez v0, :cond_0

    .line 725
    iput-object v4, p0, Lcom/google/cast/c$a;->e:Lcom/google/cast/ApplicationSession;

    .line 727
    :cond_0
    iput-object v4, p0, Lcom/google/cast/c$a;->f:Lcom/google/cast/MediaProtocolMessageStream;

    .line 728
    iget-object v0, p0, Lcom/google/cast/c$a;->i:Lcom/google/cast/c$b;

    if-eqz v0, :cond_1

    .line 729
    iget-object v0, p0, Lcom/google/cast/c$a;->i:Lcom/google/cast/c$b;

    iget-object v0, v0, Lcom/google/cast/c$b;->b:Landroid/support/v7/media/MediaRouter$ControlRequestCallback;

    iget-object v1, p0, Lcom/google/cast/c$a;->d:Lcom/google/cast/c;

    invoke-virtual {v1}, Lcom/google/cast/c;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/google/cast/R$string;->error_start_session_failed:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/cast/c$a;->d:Lcom/google/cast/c;

    const/4 v3, 0x2

    invoke-static {v2, v3}, Lcom/google/cast/c;->a(Lcom/google/cast/c;I)Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/media/MediaRouter$ControlRequestCallback;->onError(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 732
    iput-object v4, p0, Lcom/google/cast/c$a;->i:Lcom/google/cast/c$b;

    .line 735
    :cond_1
    iget-wide v0, p0, Lcom/google/cast/c$a;->o:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_2

    .line 736
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/cast/c$a;->o:J

    .line 738
    :cond_2
    invoke-direct {p0}, Lcom/google/cast/c$a;->f()Z

    move-result v0

    if-nez v0, :cond_3

    .line 739
    invoke-direct {p0}, Lcom/google/cast/c$a;->e()V

    .line 740
    iput-object v4, p0, Lcom/google/cast/c$a;->e:Lcom/google/cast/ApplicationSession;

    .line 742
    :cond_3
    return-void
.end method

.method public onSessionStarted(Lcom/google/cast/ApplicationMetadata;)V
    .locals 5
    .param p1, "appMetadata"    # Lcom/google/cast/ApplicationMetadata;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 614
    invoke-static {}, Lcom/google/cast/c;->b()Lcom/google/cast/Logger;

    move-result-object v0

    const-string v1, "onSessionStarted"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 615
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/cast/c$a;->o:J

    .line 616
    iput-boolean v3, p0, Lcom/google/cast/c$a;->p:Z

    .line 617
    new-instance v0, Lcom/google/cast/c$a$2;

    invoke-direct {v0, p0}, Lcom/google/cast/c$a$2;-><init>(Lcom/google/cast/c$a;)V

    iput-object v0, p0, Lcom/google/cast/c$a;->f:Lcom/google/cast/MediaProtocolMessageStream;

    .line 698
    iget-object v0, p0, Lcom/google/cast/c$a;->e:Lcom/google/cast/ApplicationSession;

    invoke-virtual {v0}, Lcom/google/cast/ApplicationSession;->getChannel()Lcom/google/cast/ApplicationChannel;

    move-result-object v0

    .line 699
    if-eqz v0, :cond_0

    .line 700
    iget-object v1, p0, Lcom/google/cast/c$a;->f:Lcom/google/cast/MediaProtocolMessageStream;

    invoke-virtual {v0, v1}, Lcom/google/cast/ApplicationChannel;->attachMessageStream(Lcom/google/cast/MessageStream;)V

    .line 705
    :goto_0
    iget-object v0, p0, Lcom/google/cast/c$a;->i:Lcom/google/cast/c$b;

    if-eqz v0, :cond_1

    .line 706
    iget-object v0, p0, Lcom/google/cast/c$a;->i:Lcom/google/cast/c$b;

    invoke-direct {p0, v0}, Lcom/google/cast/c$a;->a(Lcom/google/cast/c$b;)Z

    .line 707
    iput-object v4, p0, Lcom/google/cast/c$a;->i:Lcom/google/cast/c$b;

    .line 718
    :goto_1
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/cast/c$a;->a(I)V

    .line 719
    return-void

    .line 702
    :cond_0
    invoke-static {}, Lcom/google/cast/c;->b()Lcom/google/cast/Logger;

    move-result-object v0

    const-string v1, "No channel in session!"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/cast/Logger;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 709
    :cond_1
    invoke-static {}, Lcom/google/cast/c;->b()Lcom/google/cast/Logger;

    move-result-object v0

    const-string v1, "requesting RAMP status"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 711
    :try_start_0
    iget-object v0, p0, Lcom/google/cast/c$a;->f:Lcom/google/cast/MediaProtocolMessageStream;

    invoke-virtual {v0}, Lcom/google/cast/MediaProtocolMessageStream;->requestStatus()Lcom/google/cast/MediaProtocolCommand;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/cast/c$a;->a(Lcom/google/cast/MediaProtocolCommand;Landroid/support/v7/media/MediaRouter$ControlRequestCallback;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 712
    :catch_0
    move-exception v0

    .line 713
    invoke-static {}, Lcom/google/cast/c;->b()Lcom/google/cast/Logger;

    move-result-object v1

    const-string v2, "Error while requesting RAMP status"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/cast/Logger;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public onSetVolume(I)V
    .locals 6
    .param p1, "volume"    # I

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 210
    invoke-static {}, Lcom/google/cast/c;->b()Lcom/google/cast/Logger;

    move-result-object v0

    const-string v1, "onSetVolume() volume=%d, mRampStream=%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/google/cast/c$a;->f:Lcom/google/cast/MediaProtocolMessageStream;

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 211
    iget-object v0, p0, Lcom/google/cast/c$a;->f:Lcom/google/cast/MediaProtocolMessageStream;

    if-nez v0, :cond_0

    .line 224
    :goto_0
    return-void

    .line 216
    :cond_0
    int-to-double v0, p1

    const-wide/high16 v2, 0x4034000000000000L    # 20.0

    div-double/2addr v0, v2

    .line 217
    :try_start_0
    invoke-direct {p0}, Lcom/google/cast/c$a;->d()V

    .line 218
    iget-object v2, p0, Lcom/google/cast/c$a;->f:Lcom/google/cast/MediaProtocolMessageStream;

    invoke-virtual {v2, v0, v1}, Lcom/google/cast/MediaProtocolMessageStream;->setVolume(D)Lcom/google/cast/MediaProtocolCommand;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/cast/c$a;->a(Lcom/google/cast/MediaProtocolCommand;Landroid/support/v7/media/MediaRouter$ControlRequestCallback;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 219
    :catch_0
    move-exception v0

    .line 220
    invoke-static {}, Lcom/google/cast/c;->b()Lcom/google/cast/Logger;

    move-result-object v1

    const-string v2, "can\'t process command; %s"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 221
    :catch_1
    move-exception v0

    .line 222
    invoke-static {}, Lcom/google/cast/c;->b()Lcom/google/cast/Logger;

    move-result-object v1

    const-string v2, "can\'t process command; %s"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public onUnselect()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 189
    invoke-static {}, Lcom/google/cast/c;->b()Lcom/google/cast/Logger;

    move-result-object v0

    const-string v1, "Unselected device"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 192
    iget-object v0, p0, Lcom/google/cast/c$a;->d:Lcom/google/cast/c;

    invoke-virtual {v0}, Lcom/google/cast/c;->getHandler()Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/cast/c$a;->n:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 195
    iget-object v0, p0, Lcom/google/cast/c$a;->e:Lcom/google/cast/ApplicationSession;

    if-eqz v0, :cond_0

    .line 197
    :try_start_0
    iget-object v0, p0, Lcom/google/cast/c$a;->e:Lcom/google/cast/ApplicationSession;

    iget-object v1, p0, Lcom/google/cast/c$a;->d:Lcom/google/cast/c;

    invoke-static {v1}, Lcom/google/cast/c;->a(Lcom/google/cast/c;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/cast/ApplicationSession;->setStopApplicationWhenEnding(Z)V

    .line 198
    iget-object v0, p0, Lcom/google/cast/c$a;->e:Lcom/google/cast/ApplicationSession;

    invoke-virtual {v0}, Lcom/google/cast/ApplicationSession;->endSession()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 199
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/google/cast/c$a;->a(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 206
    :cond_0
    :goto_0
    return-void

    .line 201
    :catch_0
    move-exception v0

    .line 202
    invoke-static {}, Lcom/google/cast/c;->b()Lcom/google/cast/Logger;

    move-result-object v1

    const-string v2, "Exception while ending session"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/cast/Logger;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 203
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/cast/c$a;->e:Lcom/google/cast/ApplicationSession;

    goto :goto_0
.end method

.method public onUpdateVolume(I)V
    .locals 6
    .param p1, "delta"    # I

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 228
    invoke-static {}, Lcom/google/cast/c;->b()Lcom/google/cast/Logger;

    move-result-object v0

    const-string v1, "onUpdateVolume() delta=%d, mRampStream=%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/google/cast/c$a;->f:Lcom/google/cast/MediaProtocolMessageStream;

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 229
    iget-object v0, p0, Lcom/google/cast/c$a;->f:Lcom/google/cast/MediaProtocolMessageStream;

    if-nez v0, :cond_0

    .line 242
    :goto_0
    return-void

    .line 234
    :cond_0
    :try_start_0
    invoke-direct {p0}, Lcom/google/cast/c$a;->d()V

    .line 235
    invoke-virtual {p0}, Lcom/google/cast/c$a;->a()I

    move-result v0

    add-int/2addr v0, p1

    int-to-double v0, v0

    const-wide/high16 v2, 0x4034000000000000L    # 20.0

    div-double/2addr v0, v2

    .line 236
    iget-object v2, p0, Lcom/google/cast/c$a;->f:Lcom/google/cast/MediaProtocolMessageStream;

    invoke-virtual {v2, v0, v1}, Lcom/google/cast/MediaProtocolMessageStream;->setVolume(D)Lcom/google/cast/MediaProtocolCommand;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/cast/c$a;->a(Lcom/google/cast/MediaProtocolCommand;Landroid/support/v7/media/MediaRouter$ControlRequestCallback;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 237
    :catch_0
    move-exception v0

    .line 238
    invoke-static {}, Lcom/google/cast/c;->b()Lcom/google/cast/Logger;

    move-result-object v1

    const-string v2, "can\'t process command; %s"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 239
    :catch_1
    move-exception v0

    .line 240
    invoke-static {}, Lcom/google/cast/c;->b()Lcom/google/cast/Logger;

    move-result-object v1

    const-string v2, "can\'t process command; %s"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
