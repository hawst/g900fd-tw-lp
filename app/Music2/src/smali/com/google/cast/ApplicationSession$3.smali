.class Lcom/google/cast/ApplicationSession$3;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/cast/ApplicationChannel$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/cast/ApplicationSession;-><init>(Lcom/google/cast/CastContext;Lcom/google/cast/CastDevice;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/google/cast/ApplicationSession;


# direct methods
.method constructor <init>(Lcom/google/cast/ApplicationSession;)V
    .locals 0

    .prologue
    .line 284
    iput-object p1, p0, Lcom/google/cast/ApplicationSession$3;->a:Lcom/google/cast/ApplicationSession;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/cast/ApplicationChannel;)V
    .locals 3

    .prologue
    .line 287
    iget-object v0, p0, Lcom/google/cast/ApplicationSession$3;->a:Lcom/google/cast/ApplicationSession;

    invoke-static {v0}, Lcom/google/cast/ApplicationSession;->b(Lcom/google/cast/ApplicationSession;)Lcom/google/cast/Logger;

    move-result-object v0

    const-string v1, "Channel is connected"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 289
    iget-object v0, p0, Lcom/google/cast/ApplicationSession$3;->a:Lcom/google/cast/ApplicationSession;

    invoke-static {v0}, Lcom/google/cast/ApplicationSession;->a(Lcom/google/cast/ApplicationSession;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 290
    iget-object v0, p0, Lcom/google/cast/ApplicationSession$3;->a:Lcom/google/cast/ApplicationSession;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/cast/ApplicationSession;->a(Lcom/google/cast/ApplicationSession;Lcom/google/cast/SessionError;)V

    .line 294
    :goto_0
    return-void

    .line 293
    :cond_0
    iget-object v0, p0, Lcom/google/cast/ApplicationSession$3;->a:Lcom/google/cast/ApplicationSession;

    invoke-static {v0}, Lcom/google/cast/ApplicationSession;->m(Lcom/google/cast/ApplicationSession;)V

    goto :goto_0
.end method

.method public a(Lcom/google/cast/ApplicationChannel;I)V
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 298
    iget-object v1, p0, Lcom/google/cast/ApplicationSession$3;->a:Lcom/google/cast/ApplicationSession;

    invoke-static {v1}, Lcom/google/cast/ApplicationSession;->b(Lcom/google/cast/ApplicationSession;)Lcom/google/cast/Logger;

    move-result-object v1

    const-string v2, "channel connection failed with error: %d"

    new-array v3, v0, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 299
    iget-object v1, p0, Lcom/google/cast/ApplicationSession$3;->a:Lcom/google/cast/ApplicationSession;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/google/cast/ApplicationSession;->a(Lcom/google/cast/ApplicationSession;Lcom/google/cast/ApplicationChannel;)Lcom/google/cast/ApplicationChannel;

    .line 301
    const/4 v1, -0x2

    if-ne p2, v1, :cond_0

    .line 304
    :goto_0
    iget-object v1, p0, Lcom/google/cast/ApplicationSession$3;->a:Lcom/google/cast/ApplicationSession;

    new-instance v2, Lcom/google/cast/SessionError;

    const/4 v3, 0x2

    invoke-direct {v2, v3, v0}, Lcom/google/cast/SessionError;-><init>(II)V

    invoke-static {v1, v2}, Lcom/google/cast/ApplicationSession;->a(Lcom/google/cast/ApplicationSession;Lcom/google/cast/SessionError;)V

    .line 305
    return-void

    .line 301
    :cond_0
    const/4 v0, 0x3

    goto :goto_0
.end method

.method public b(Lcom/google/cast/ApplicationChannel;I)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    const/4 v5, 0x3

    .line 309
    iget-object v0, p0, Lcom/google/cast/ApplicationSession$3;->a:Lcom/google/cast/ApplicationSession;

    invoke-static {v0}, Lcom/google/cast/ApplicationSession;->b(Lcom/google/cast/ApplicationSession;)Lcom/google/cast/Logger;

    move-result-object v0

    const-string v1, "channel disconnected with error: %d"

    new-array v2, v6, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 310
    iget-object v0, p0, Lcom/google/cast/ApplicationSession$3;->a:Lcom/google/cast/ApplicationSession;

    invoke-static {v0, v7}, Lcom/google/cast/ApplicationSession;->a(Lcom/google/cast/ApplicationSession;Lcom/google/cast/ApplicationChannel;)Lcom/google/cast/ApplicationChannel;

    .line 312
    packed-switch p2, :pswitch_data_0

    .line 325
    iget-object v0, p0, Lcom/google/cast/ApplicationSession$3;->a:Lcom/google/cast/ApplicationSession;

    invoke-static {v0, v7}, Lcom/google/cast/ApplicationSession;->a(Lcom/google/cast/ApplicationSession;Lcom/google/cast/SessionError;)V

    .line 328
    :goto_0
    return-void

    .line 314
    :pswitch_0
    iget-object v0, p0, Lcom/google/cast/ApplicationSession$3;->a:Lcom/google/cast/ApplicationSession;

    new-instance v1, Lcom/google/cast/SessionError;

    invoke-direct {v1, v5, v6}, Lcom/google/cast/SessionError;-><init>(II)V

    invoke-static {v0, v1}, Lcom/google/cast/ApplicationSession;->a(Lcom/google/cast/ApplicationSession;Lcom/google/cast/SessionError;)V

    goto :goto_0

    .line 319
    :pswitch_1
    iget-object v0, p0, Lcom/google/cast/ApplicationSession$3;->a:Lcom/google/cast/ApplicationSession;

    new-instance v1, Lcom/google/cast/SessionError;

    invoke-direct {v1, v5, v5}, Lcom/google/cast/SessionError;-><init>(II)V

    invoke-static {v0, v1}, Lcom/google/cast/ApplicationSession;->a(Lcom/google/cast/ApplicationSession;Lcom/google/cast/SessionError;)V

    goto :goto_0

    .line 312
    nop

    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
