.class final Lcom/google/cast/n;
.super Lcom/google/cast/NetworkTask;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/cast/CastContext;

.field private b:Ljava/lang/String;

.field private c:Lcom/google/cast/ApplicationMetadata;

.field private d:Lcom/google/cast/MimeData;

.field private e:Landroid/net/Uri;

.field private f:Landroid/net/Uri;

.field private g:Landroid/net/Uri;

.field private h:Landroid/net/Uri;

.field private i:J

.field private j:Z

.field private k:Lcom/google/cast/Logger;


# direct methods
.method public constructor <init>(Lcom/google/cast/CastContext;Landroid/net/Uri;Ljava/lang/String;Landroid/net/Uri;)V
    .locals 6

    .prologue
    .line 86
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/cast/NetworkRequest;

    invoke-direct {p0, v0}, Lcom/google/cast/NetworkTask;-><init>([Lcom/google/cast/NetworkRequest;)V

    .line 30
    new-instance v0, Lcom/google/cast/Logger;

    const-string v1, "StartSessionTask"

    invoke-direct {v0, v1}, Lcom/google/cast/Logger;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/cast/n;->k:Lcom/google/cast/Logger;

    .line 87
    if-nez p2, :cond_0

    .line 88
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "applicationBaseUrl cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 90
    :cond_0
    if-nez p3, :cond_1

    .line 91
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "applicationName cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 93
    :cond_1
    if-nez p4, :cond_2

    .line 94
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "applicationInstanceUrl cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 97
    :cond_2
    if-eqz p2, :cond_3

    if-nez p4, :cond_4

    .line 98
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "applicationUrl and applicationInstanceUrl cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 101
    :cond_4
    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/cast/n;->a(Lcom/google/cast/CastContext;Landroid/net/Uri;Ljava/lang/String;Lcom/google/cast/MimeData;Landroid/net/Uri;)V

    .line 102
    return-void
.end method

.method public constructor <init>(Lcom/google/cast/CastContext;Landroid/net/Uri;Ljava/lang/String;Lcom/google/cast/MimeData;)V
    .locals 6

    .prologue
    .line 63
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/cast/NetworkRequest;

    invoke-direct {p0, v0}, Lcom/google/cast/NetworkTask;-><init>([Lcom/google/cast/NetworkRequest;)V

    .line 30
    new-instance v0, Lcom/google/cast/Logger;

    const-string v1, "StartSessionTask"

    invoke-direct {v0, v1}, Lcom/google/cast/Logger;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/cast/n;->k:Lcom/google/cast/Logger;

    .line 64
    if-nez p2, :cond_0

    .line 65
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "applicationBaseUrl cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 67
    :cond_0
    if-eqz p4, :cond_1

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 68
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "applicationName cannot be null if applicationArgument is provided"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 71
    :cond_1
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/cast/n;->a(Lcom/google/cast/CastContext;Landroid/net/Uri;Ljava/lang/String;Lcom/google/cast/MimeData;Landroid/net/Uri;)V

    .line 72
    return-void
.end method

.method private a(Lcom/google/cast/CastContext;Landroid/net/Uri;Ljava/lang/String;Lcom/google/cast/MimeData;Landroid/net/Uri;)V
    .locals 2

    .prologue
    .line 109
    iput-object p1, p0, Lcom/google/cast/n;->a:Lcom/google/cast/CastContext;

    .line 110
    iput-object p2, p0, Lcom/google/cast/n;->e:Landroid/net/Uri;

    .line 111
    iput-object p3, p0, Lcom/google/cast/n;->b:Ljava/lang/String;

    .line 112
    iput-object p4, p0, Lcom/google/cast/n;->d:Lcom/google/cast/MimeData;

    .line 113
    iput-object p5, p0, Lcom/google/cast/n;->g:Landroid/net/Uri;

    .line 115
    if-eqz p3, :cond_0

    .line 116
    iget-object v0, p0, Lcom/google/cast/n;->e:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/cast/n;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/cast/n;->f:Landroid/net/Uri;

    .line 118
    :cond_0
    return-void
.end method

.method private a(Lcom/google/cast/g;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 163
    iget-boolean v0, p0, Lcom/google/cast/n;->j:Z

    if-eqz v0, :cond_0

    .line 175
    :goto_0
    return-void

    .line 167
    :cond_0
    invoke-virtual {p1}, Lcom/google/cast/g;->a()Lcom/google/cast/ApplicationMetadata;

    move-result-object v0

    iput-object v0, p0, Lcom/google/cast/n;->c:Lcom/google/cast/ApplicationMetadata;

    .line 168
    iget-object v0, p0, Lcom/google/cast/n;->c:Lcom/google/cast/ApplicationMetadata;

    if-eqz v0, :cond_1

    .line 169
    iget-object v0, p0, Lcom/google/cast/n;->c:Lcom/google/cast/ApplicationMetadata;

    invoke-virtual {v0}, Lcom/google/cast/ApplicationMetadata;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/cast/n;->b:Ljava/lang/String;

    .line 170
    iget-object v0, p0, Lcom/google/cast/n;->k:Lcom/google/cast/Logger;

    const-string v1, "got app name: %s"

    new-array v2, v5, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/cast/n;->b:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 171
    iget-object v0, p0, Lcom/google/cast/n;->e:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/cast/n;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/cast/n;->f:Landroid/net/Uri;

    .line 174
    :cond_1
    iput-boolean v5, p0, Lcom/google/cast/n;->j:Z

    goto :goto_0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/google/cast/n;->b:Ljava/lang/String;

    return-object v0
.end method

.method public b()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/google/cast/n;->h:Landroid/net/Uri;

    return-object v0
.end method

.method public c()J
    .locals 2

    .prologue
    .line 138
    iget-wide v0, p0, Lcom/google/cast/n;->i:J

    return-wide v0
.end method

.method public d()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/google/cast/n;->g:Landroid/net/Uri;

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;
    .locals 14
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    const-wide/16 v12, 0x3e8

    const/16 v10, -0x63

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 180
    const/4 v0, 0x0

    .line 184
    iget-object v1, p0, Lcom/google/cast/n;->g:Landroid/net/Uri;

    if-nez v1, :cond_7

    .line 185
    iget-object v1, p0, Lcom/google/cast/n;->b:Ljava/lang/String;

    if-nez v1, :cond_4

    .line 186
    iget-object v0, p0, Lcom/google/cast/n;->k:Lcom/google/cast/Logger;

    const-string v1, "No application name supplied, so peforming GetApplicationInfoRequest"

    new-array v2, v8, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 189
    new-instance v0, Lcom/google/cast/g;

    iget-object v1, p0, Lcom/google/cast/n;->a:Lcom/google/cast/CastContext;

    iget-object v2, p0, Lcom/google/cast/n;->e:Landroid/net/Uri;

    invoke-direct {v0, v1, v2}, Lcom/google/cast/g;-><init>(Lcom/google/cast/CastContext;Landroid/net/Uri;)V

    .line 191
    iput-object v0, p0, Lcom/google/cast/n;->mCurrentRequest:Lcom/google/cast/NetworkRequest;

    .line 192
    invoke-virtual {v0}, Lcom/google/cast/g;->execute()I

    move-result v1

    .line 194
    invoke-virtual {p0}, Lcom/google/cast/n;->isCancelled()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 195
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 330
    :goto_0
    return-object v0

    .line 198
    :cond_0
    const/4 v2, -0x4

    if-ne v1, v2, :cond_1

    .line 199
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 202
    :cond_1
    if-eqz v1, :cond_2

    .line 203
    iget-object v0, p0, Lcom/google/cast/n;->k:Lcom/google/cast/Logger;

    const-string v2, "GetApplicationInfoRequest failed with status: %d"

    new-array v3, v9, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v8

    invoke-virtual {v0, v2, v3}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 204
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 207
    :cond_2
    invoke-virtual {v0}, Lcom/google/cast/g;->a()Lcom/google/cast/ApplicationMetadata;

    move-result-object v1

    .line 208
    if-nez v1, :cond_3

    .line 211
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 213
    :cond_3
    invoke-direct {p0, v0}, Lcom/google/cast/n;->a(Lcom/google/cast/g;)V

    .line 214
    invoke-virtual {v0}, Lcom/google/cast/g;->d()Landroid/net/Uri;

    move-result-object v0

    .line 218
    :cond_4
    iget-object v1, p0, Lcom/google/cast/n;->k:Lcom/google/cast/Logger;

    const-string v2, "performing StartApplicationRequest on app url: %s"

    new-array v3, v9, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/cast/n;->f:Landroid/net/Uri;

    aput-object v4, v3, v8

    invoke-virtual {v1, v2, v3}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 219
    new-instance v1, Lcom/google/cast/m;

    iget-object v2, p0, Lcom/google/cast/n;->a:Lcom/google/cast/CastContext;

    iget-object v3, p0, Lcom/google/cast/n;->f:Landroid/net/Uri;

    iget-object v4, p0, Lcom/google/cast/n;->d:Lcom/google/cast/MimeData;

    invoke-direct {v1, v2, v3, v4}, Lcom/google/cast/m;-><init>(Lcom/google/cast/CastContext;Landroid/net/Uri;Lcom/google/cast/MimeData;)V

    .line 221
    iput-object v1, p0, Lcom/google/cast/n;->mCurrentRequest:Lcom/google/cast/NetworkRequest;

    .line 222
    invoke-virtual {v1}, Lcom/google/cast/m;->execute()I

    move-result v2

    .line 224
    invoke-virtual {p0}, Lcom/google/cast/n;->isCancelled()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 225
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 228
    :cond_5
    if-eqz v2, :cond_6

    .line 229
    iget-object v0, p0, Lcom/google/cast/n;->k:Lcom/google/cast/Logger;

    const-string v1, "StartApplicationRequest failed with status: %d"

    new-array v3, v9, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v8

    invoke-virtual {v0, v1, v3}, Lcom/google/cast/Logger;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 230
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 233
    :cond_6
    invoke-virtual {v1}, Lcom/google/cast/m;->a()Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/google/cast/n;->g:Landroid/net/Uri;

    .line 234
    iget-object v1, p0, Lcom/google/cast/n;->k:Lcom/google/cast/Logger;

    const-string v2, "got app instance url: %s"

    new-array v3, v9, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/cast/n;->g:Landroid/net/Uri;

    aput-object v4, v3, v8

    invoke-virtual {v1, v2, v3}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 240
    :cond_7
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 241
    const-wide/16 v4, 0x2710

    add-long/2addr v4, v2

    .line 243
    :cond_8
    cmp-long v1, v2, v4

    if-gez v1, :cond_e

    if-nez v0, :cond_e

    .line 244
    iget-object v0, p0, Lcom/google/cast/n;->k:Lcom/google/cast/Logger;

    const-string v1, "no connection service info yet; performing GetApplicationInfoRequest for %s"

    new-array v6, v9, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/google/cast/n;->f:Landroid/net/Uri;

    aput-object v7, v6, v8

    invoke-virtual {v0, v1, v6}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 246
    new-instance v1, Lcom/google/cast/g;

    iget-object v0, p0, Lcom/google/cast/n;->a:Lcom/google/cast/CastContext;

    iget-object v6, p0, Lcom/google/cast/n;->f:Landroid/net/Uri;

    invoke-direct {v1, v0, v6}, Lcom/google/cast/g;-><init>(Lcom/google/cast/CastContext;Landroid/net/Uri;)V

    .line 248
    iput-object v1, p0, Lcom/google/cast/n;->mCurrentRequest:Lcom/google/cast/NetworkRequest;

    .line 249
    invoke-virtual {v1}, Lcom/google/cast/g;->execute()I

    move-result v0

    .line 251
    invoke-virtual {p0}, Lcom/google/cast/n;->isCancelled()Z

    move-result v6

    if-eqz v6, :cond_9

    .line 252
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto/16 :goto_0

    .line 255
    :cond_9
    if-eqz v0, :cond_a

    .line 256
    iget-object v1, p0, Lcom/google/cast/n;->k:Lcom/google/cast/Logger;

    const-string v2, "GetApplicationInfoRequest failed with status: %d"

    new-array v3, v9, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v8

    invoke-virtual {v1, v2, v3}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 257
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto/16 :goto_0

    .line 260
    :cond_a
    invoke-virtual {v1}, Lcom/google/cast/g;->b()I

    move-result v0

    if-eq v0, v9, :cond_b

    .line 263
    iget-object v0, p0, Lcom/google/cast/n;->k:Lcom/google/cast/Logger;

    const-string v1, "Application is no longer running!"

    new-array v2, v8, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 264
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto/16 :goto_0

    .line 267
    :cond_b
    iget-object v0, p0, Lcom/google/cast/n;->g:Landroid/net/Uri;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/google/cast/n;->g:Landroid/net/Uri;

    invoke-virtual {v1}, Lcom/google/cast/g;->c()Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v0, v6}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_c

    .line 269
    iget-object v0, p0, Lcom/google/cast/n;->k:Lcom/google/cast/Logger;

    const-string v1, "Application instance is different!"

    new-array v2, v8, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 270
    const/4 v0, 0x2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto/16 :goto_0

    .line 273
    :cond_c
    invoke-virtual {v1}, Lcom/google/cast/g;->e()Z

    move-result v0

    if-nez v0, :cond_d

    .line 276
    iget-object v0, p0, Lcom/google/cast/n;->k:Lcom/google/cast/Logger;

    const-string v1, "Session does not support channels"

    new-array v2, v8, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 277
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto/16 :goto_0

    .line 279
    :cond_d
    invoke-virtual {v1}, Lcom/google/cast/g;->d()Landroid/net/Uri;

    move-result-object v0

    .line 280
    if-eqz v0, :cond_f

    .line 282
    iget-object v2, p0, Lcom/google/cast/n;->k:Lcom/google/cast/Logger;

    const-string v3, "Got connection service URL: %s"

    new-array v4, v9, [Ljava/lang/Object;

    aput-object v0, v4, v8

    invoke-virtual {v2, v3, v4}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 283
    invoke-direct {p0, v1}, Lcom/google/cast/n;->a(Lcom/google/cast/g;)V

    .line 305
    :cond_e
    if-nez v0, :cond_11

    .line 306
    iget-object v0, p0, Lcom/google/cast/n;->k:Lcom/google/cast/Logger;

    const-string v1, "Expected a channel but never got a connection service URL"

    new-array v2, v8, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/cast/Logger;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 307
    const/4 v0, 0x3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto/16 :goto_0

    .line 290
    :cond_f
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 291
    sub-long v2, v6, v2

    .line 292
    cmp-long v1, v2, v12

    if-gez v1, :cond_10

    .line 294
    sub-long v2, v12, v2

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 298
    :cond_10
    :goto_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 300
    invoke-virtual {p0}, Lcom/google/cast/n;->isCancelled()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 301
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto/16 :goto_0

    .line 311
    :cond_11
    iget-object v1, p0, Lcom/google/cast/n;->k:Lcom/google/cast/Logger;

    const-string v2, "performing an OpenChannelRequest"

    new-array v3, v8, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 312
    new-instance v1, Lcom/google/cast/k;

    iget-object v2, p0, Lcom/google/cast/n;->a:Lcom/google/cast/CastContext;

    invoke-direct {v1, v2, v0}, Lcom/google/cast/k;-><init>(Lcom/google/cast/CastContext;Landroid/net/Uri;)V

    .line 314
    iput-object v1, p0, Lcom/google/cast/n;->mCurrentRequest:Lcom/google/cast/NetworkRequest;

    .line 315
    invoke-virtual {v1}, Lcom/google/cast/k;->execute()I

    move-result v0

    .line 317
    invoke-virtual {p0}, Lcom/google/cast/n;->isCancelled()Z

    move-result v2

    if-eqz v2, :cond_12

    .line 318
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto/16 :goto_0

    .line 321
    :cond_12
    if-eqz v0, :cond_13

    .line 322
    iget-object v1, p0, Lcom/google/cast/n;->k:Lcom/google/cast/Logger;

    const-string v2, "OpenChannelRequest failed with status: %d"

    new-array v3, v9, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v8

    invoke-virtual {v1, v2, v3}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 323
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto/16 :goto_0

    .line 326
    :cond_13
    invoke-virtual {v1}, Lcom/google/cast/k;->a()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/cast/n;->h:Landroid/net/Uri;

    .line 327
    invoke-virtual {v1}, Lcom/google/cast/k;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/cast/n;->i:J

    .line 328
    iget-object v0, p0, Lcom/google/cast/n;->k:Lcom/google/cast/Logger;

    const-string v1, "got a channel URL: %s"

    new-array v2, v9, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/cast/n;->h:Landroid/net/Uri;

    aput-object v3, v2, v8

    invoke-virtual {v0, v1, v2}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 330
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto/16 :goto_0

    .line 295
    :catch_0
    move-exception v1

    goto :goto_1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 14
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/cast/n;->doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final e()Lcom/google/cast/ApplicationMetadata;
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lcom/google/cast/n;->c:Lcom/google/cast/ApplicationMetadata;

    return-object v0
.end method
