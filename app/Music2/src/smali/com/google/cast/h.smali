.class Lcom/google/cast/h;
.super Lcom/google/cast/NetworkRequest;
.source "SourceFile"


# instance fields
.field private a:Landroid/net/Uri;

.field private b:Lcom/google/cast/CastDevice;

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcom/google/cast/Logger;


# direct methods
.method public constructor <init>(Lcom/google/cast/CastContext;Landroid/net/Uri;Lcom/google/cast/CastDevice;)V
    .locals 2

    .prologue
    .line 60
    invoke-direct {p0, p1}, Lcom/google/cast/NetworkRequest;-><init>(Lcom/google/cast/CastContext;)V

    .line 61
    iput-object p2, p0, Lcom/google/cast/h;->a:Landroid/net/Uri;

    .line 62
    iput-object p3, p0, Lcom/google/cast/h;->b:Lcom/google/cast/CastDevice;

    .line 63
    new-instance v0, Lcom/google/cast/Logger;

    const-string v1, "GetDeviceDescriptorRequest"

    invoke-direct {v0, v1}, Lcom/google/cast/Logger;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/cast/h;->d:Lcom/google/cast/Logger;

    .line 64
    return-void
.end method

.method private a(Lorg/xmlpull/v1/XmlPullParser;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    .line 125
    const/4 v0, 0x0

    const-string v1, "root"

    invoke-interface {p1, v2, v0, v1}, Lorg/xmlpull/v1/XmlPullParser;->require(ILjava/lang/String;Ljava/lang/String;)V

    .line 126
    :cond_0
    :goto_0
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    .line 127
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v0

    if-ne v0, v2, :cond_0

    .line 130
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    .line 132
    const-string v1, "device"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 133
    invoke-direct {p0, p1}, Lcom/google/cast/h;->b(Lorg/xmlpull/v1/XmlPullParser;)V

    goto :goto_0

    .line 135
    :cond_1
    invoke-static {p1}, Lcom/google/cast/s;->a(Lorg/xmlpull/v1/XmlPullParser;)V

    goto :goto_0

    .line 138
    :cond_2
    return-void
.end method

.method private b(Lorg/xmlpull/v1/XmlPullParser;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    .line 141
    const/4 v0, 0x0

    const-string v1, "device"

    invoke-interface {p1, v2, v0, v1}, Lorg/xmlpull/v1/XmlPullParser;->require(ILjava/lang/String;Ljava/lang/String;)V

    .line 142
    :cond_0
    :goto_0
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_8

    .line 143
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v0

    if-ne v0, v2, :cond_0

    .line 146
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    .line 147
    const-string v1, "friendlyName"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 148
    iget-object v0, p0, Lcom/google/cast/h;->b:Lcom/google/cast/CastDevice;

    invoke-static {p1}, Lcom/google/cast/s;->b(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/cast/CastDevice;->setFriendlyName(Ljava/lang/String;)V

    goto :goto_0

    .line 149
    :cond_1
    const-string v1, "manufacturer"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 150
    iget-object v0, p0, Lcom/google/cast/h;->b:Lcom/google/cast/CastDevice;

    invoke-static {p1}, Lcom/google/cast/s;->b(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/cast/CastDevice;->setManufacturer(Ljava/lang/String;)V

    goto :goto_0

    .line 151
    :cond_2
    const-string v1, "modelName"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 152
    iget-object v0, p0, Lcom/google/cast/h;->b:Lcom/google/cast/CastDevice;

    invoke-static {p1}, Lcom/google/cast/s;->b(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/cast/CastDevice;->setModelName(Ljava/lang/String;)V

    goto :goto_0

    .line 153
    :cond_3
    const-string v1, "UDN"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 154
    invoke-static {p1}, Lcom/google/cast/s;->b(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v0

    .line 155
    const-string v1, "uuid:"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 157
    const-string v1, "uuid:"

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 159
    :cond_4
    iget-object v1, p0, Lcom/google/cast/h;->b:Lcom/google/cast/CastDevice;

    invoke-virtual {v1, v0}, Lcom/google/cast/CastDevice;->setDeviceId(Ljava/lang/String;)V

    goto :goto_0

    .line 160
    :cond_5
    const-string v1, "serviceList"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 161
    invoke-direct {p0, p1}, Lcom/google/cast/h;->c(Lorg/xmlpull/v1/XmlPullParser;)V

    goto :goto_0

    .line 162
    :cond_6
    const-string v1, "iconList"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 163
    invoke-direct {p0, p1}, Lcom/google/cast/h;->e(Lorg/xmlpull/v1/XmlPullParser;)V

    goto/16 :goto_0

    .line 165
    :cond_7
    invoke-static {p1}, Lcom/google/cast/s;->a(Lorg/xmlpull/v1/XmlPullParser;)V

    goto/16 :goto_0

    .line 168
    :cond_8
    return-void
.end method

.method private c(Lorg/xmlpull/v1/XmlPullParser;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    .line 172
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/cast/h;->c:Ljava/util/List;

    .line 173
    const/4 v0, 0x0

    const-string v1, "serviceList"

    invoke-interface {p1, v2, v0, v1}, Lorg/xmlpull/v1/XmlPullParser;->require(ILjava/lang/String;Ljava/lang/String;)V

    .line 174
    :cond_0
    :goto_0
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    .line 175
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v0

    if-ne v0, v2, :cond_0

    .line 178
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    .line 179
    const-string v1, "service"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 180
    invoke-direct {p0, p1}, Lcom/google/cast/h;->d(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v0

    .line 181
    if-eqz v0, :cond_0

    .line 182
    iget-object v1, p0, Lcom/google/cast/h;->c:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 185
    :cond_1
    invoke-static {p1}, Lcom/google/cast/s;->a(Lorg/xmlpull/v1/XmlPullParser;)V

    goto :goto_0

    .line 188
    :cond_2
    return-void
.end method

.method private d(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x2

    .line 192
    .line 193
    const-string v1, "service"

    invoke-interface {p1, v3, v0, v1}, Lorg/xmlpull/v1/XmlPullParser;->require(ILjava/lang/String;Ljava/lang/String;)V

    .line 194
    :cond_0
    :goto_0
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v1

    const/4 v2, 0x3

    if-eq v1, v2, :cond_2

    .line 195
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v1

    if-ne v1, v3, :cond_0

    .line 198
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v1

    .line 199
    const-string v2, "serviceType"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 200
    invoke-static {p1}, Lcom/google/cast/s;->b(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 202
    :cond_1
    invoke-static {p1}, Lcom/google/cast/s;->a(Lorg/xmlpull/v1/XmlPullParser;)V

    goto :goto_0

    .line 205
    :cond_2
    return-object v0
.end method

.method private e(Lorg/xmlpull/v1/XmlPullParser;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x2

    .line 210
    const/4 v0, 0x0

    const-string v1, "iconList"

    invoke-interface {p1, v3, v0, v1}, Lorg/xmlpull/v1/XmlPullParser;->require(ILjava/lang/String;Ljava/lang/String;)V

    .line 211
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 212
    :cond_0
    :goto_0
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v1

    const/4 v2, 0x3

    if-eq v1, v2, :cond_2

    .line 213
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v1

    if-ne v1, v3, :cond_0

    .line 216
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v1

    .line 217
    const-string v2, "icon"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 218
    invoke-direct {p0, p1}, Lcom/google/cast/h;->f(Lorg/xmlpull/v1/XmlPullParser;)Lcom/google/cast/CastDeviceIcon;

    move-result-object v1

    .line 219
    if-eqz v1, :cond_0

    .line 220
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 223
    :cond_1
    invoke-static {p1}, Lcom/google/cast/s;->a(Lorg/xmlpull/v1/XmlPullParser;)V

    goto :goto_0

    .line 226
    :cond_2
    iget-object v1, p0, Lcom/google/cast/h;->b:Lcom/google/cast/CastDevice;

    invoke-virtual {v1, v0}, Lcom/google/cast/CastDevice;->setIcons(Ljava/util/ArrayList;)V

    .line 227
    return-void
.end method

.method private f(Lorg/xmlpull/v1/XmlPullParser;)Lcom/google/cast/CastDeviceIcon;
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 231
    .line 234
    const-string v0, "icon"

    invoke-interface {p1, v8, v5, v0}, Lorg/xmlpull/v1/XmlPullParser;->require(ILjava/lang/String;Ljava/lang/String;)V

    move v0, v4

    move v1, v4

    move v2, v4

    move-object v3, v5

    .line 235
    :cond_0
    :goto_0
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v6

    const/4 v7, 0x3

    if-eq v6, v7, :cond_5

    .line 236
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v6

    if-ne v6, v8, :cond_0

    .line 239
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v6

    .line 240
    const-string v7, "width"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 241
    invoke-static {p1}, Lcom/google/cast/s;->b(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    goto :goto_0

    .line 242
    :cond_1
    const-string v7, "height"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 243
    invoke-static {p1}, Lcom/google/cast/s;->b(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    goto :goto_0

    .line 244
    :cond_2
    const-string v7, "depth"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 245
    invoke-static {p1}, Lcom/google/cast/s;->b(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    .line 246
    :cond_3
    const-string v7, "url"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 247
    invoke-static {p1}, Lcom/google/cast/s;->b(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 249
    :cond_4
    invoke-static {p1}, Lcom/google/cast/s;->a(Lorg/xmlpull/v1/XmlPullParser;)V

    goto :goto_0

    .line 253
    :cond_5
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_6

    if-lez v2, :cond_6

    if-lez v1, :cond_6

    if-gtz v0, :cond_7

    .line 254
    :cond_6
    iget-object v0, p0, Lcom/google/cast/h;->d:Lcom/google/cast/Logger;

    const-string v1, "Ignoring invalid icon entry"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 264
    :goto_1
    return-object v5

    .line 260
    :cond_7
    iget-object v4, p0, Lcom/google/cast/h;->a:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/net/URI;->create(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v4

    .line 261
    invoke-virtual {v4, v3}, Ljava/net/URI;->resolve(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v3

    .line 262
    invoke-virtual {v3}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 264
    new-instance v5, Lcom/google/cast/CastDeviceIcon;

    invoke-direct {v5, v2, v1, v0, v3}, Lcom/google/cast/CastDeviceIcon;-><init>(IIILandroid/net/Uri;)V

    goto :goto_1
.end method


# virtual methods
.method public final execute()I
    .locals 6

    .prologue
    const/4 v0, -0x1

    const/4 v2, 0x0

    const/4 v1, -0x3

    .line 76
    .line 79
    :try_start_0
    iget-object v3, p0, Lcom/google/cast/h;->a:Landroid/net/Uri;

    sget v4, Lcom/google/cast/h;->DEFAULT_TIMEOUT:I

    invoke-virtual {p0, v3, v4}, Lcom/google/cast/h;->performHttpGet(Landroid/net/Uri;I)Lcom/google/cast/SimpleHttpRequest;

    move-result-object v3

    .line 80
    invoke-interface {v3}, Lcom/google/cast/SimpleHttpRequest;->getResponseStatus()I

    move-result v4

    const/16 v5, 0xc8

    if-eq v4, v5, :cond_0

    .line 120
    :goto_0
    return v0

    .line 83
    :cond_0
    invoke-interface {v3}, Lcom/google/cast/SimpleHttpRequest;->getResponseData()Lcom/google/cast/MimeData;

    move-result-object v4

    .line 84
    const-string v5, "application-url"

    invoke-interface {v3, v5}, Lcom/google/cast/SimpleHttpRequest;->getResponseHeader(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 85
    if-nez v3, :cond_1

    move v0, v1

    .line 86
    goto :goto_0

    .line 88
    :cond_1
    iget-object v5, p0, Lcom/google/cast/h;->b:Lcom/google/cast/CastDevice;

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v5, v3}, Lcom/google/cast/CastDevice;->setApplicationUrl(Landroid/net/Uri;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_0

    .line 95
    invoke-virtual {p0}, Lcom/google/cast/h;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 96
    const/16 v0, -0x63

    goto :goto_0

    .line 91
    :catch_0
    move-exception v0

    .line 92
    const/4 v0, -0x2

    goto :goto_0

    .line 100
    :cond_2
    if-eqz v4, :cond_3

    const-string v0, "application/xml"

    invoke-virtual {v4}, Lcom/google/cast/MimeData;->getType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    :cond_3
    move v0, v1

    .line 101
    goto :goto_0

    .line 103
    :cond_4
    invoke-virtual {v4}, Lcom/google/cast/MimeData;->getTextData()Ljava/lang/String;

    move-result-object v0

    .line 104
    if-nez v0, :cond_5

    move v0, v1

    .line 105
    goto :goto_0

    .line 108
    :cond_5
    :try_start_1
    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v0

    .line 109
    new-instance v3, Ljava/io/StringReader;

    invoke-virtual {v4}, Lcom/google/cast/MimeData;->getTextData()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v3}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    .line 110
    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->nextTag()I

    .line 111
    invoke-direct {p0, v0}, Lcom/google/cast/h;->a(Lorg/xmlpull/v1/XmlPullParser;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 .. :try_end_1} :catch_2

    move v0, v2

    .line 120
    goto :goto_0

    .line 112
    :catch_1
    move-exception v0

    .line 113
    iget-object v3, p0, Lcom/google/cast/h;->d:Lcom/google/cast/Logger;

    const-string v4, "exception"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v3, v0, v4, v2}, Lcom/google/cast/Logger;->d(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 114
    goto :goto_0

    .line 115
    :catch_2
    move-exception v0

    .line 116
    iget-object v3, p0, Lcom/google/cast/h;->d:Lcom/google/cast/Logger;

    const-string v4, "exception"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v3, v0, v4, v2}, Lcom/google/cast/Logger;->d(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 117
    goto :goto_0

    .line 89
    :catch_3
    move-exception v1

    goto :goto_0
.end method
