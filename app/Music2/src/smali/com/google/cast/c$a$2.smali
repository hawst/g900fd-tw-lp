.class Lcom/google/cast/c$a$2;
.super Lcom/google/cast/MediaProtocolMessageStream;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/cast/c$a;->onSessionStarted(Lcom/google/cast/ApplicationMetadata;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/google/cast/c$a;


# direct methods
.method constructor <init>(Lcom/google/cast/c$a;)V
    .locals 0

    .prologue
    .line 617
    iput-object p1, p0, Lcom/google/cast/c$a$2;->a:Lcom/google/cast/c$a;

    invoke-direct {p0}, Lcom/google/cast/MediaProtocolMessageStream;-><init>()V

    return-void
.end method


# virtual methods
.method protected onError(Ljava/lang/String;JLorg/json/JSONObject;)V
    .locals 6
    .param p1, "errorDomain"    # Ljava/lang/String;
    .param p2, "errorCode"    # J
    .param p4, "errorInfo"    # Lorg/json/JSONObject;

    .prologue
    const/4 v5, 0x0

    .line 668
    const/4 v0, 0x0

    .line 669
    iget-object v1, p0, Lcom/google/cast/c$a$2;->a:Lcom/google/cast/c$a;

    invoke-static {v1}, Lcom/google/cast/c$a;->c(Lcom/google/cast/c$a;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 670
    iget-object v0, p0, Lcom/google/cast/c$a$2;->a:Lcom/google/cast/c$a;

    invoke-static {v0}, Lcom/google/cast/c$a;->d(Lcom/google/cast/c$a;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/google/cast/c$a$2;->a:Lcom/google/cast/c$a;

    invoke-static {v1}, Lcom/google/cast/c$a;->c(Lcom/google/cast/c$a;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    .line 673
    :cond_0
    if-eqz v0, :cond_2

    .line 674
    new-instance v1, Landroid/support/v7/media/MediaItemStatus$Builder;

    const/4 v2, 0x7

    invoke-direct {v1, v2}, Landroid/support/v7/media/MediaItemStatus$Builder;-><init>(I)V

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Landroid/support/v7/media/MediaItemStatus$Builder;->setTimestamp(J)Landroid/support/v7/media/MediaItemStatus$Builder;

    move-result-object v1

    .line 678
    if-eqz p4, :cond_1

    .line 679
    iget-object v2, p0, Lcom/google/cast/c$a$2;->a:Lcom/google/cast/c$a;

    iget-object v2, v2, Lcom/google/cast/c$a;->d:Lcom/google/cast/c;

    invoke-static {v2, p4}, Lcom/google/cast/c;->a(Lcom/google/cast/c;Lorg/json/JSONObject;)Landroid/os/Bundle;

    move-result-object v2

    .line 680
    invoke-virtual {v1, v2}, Landroid/support/v7/media/MediaItemStatus$Builder;->setExtras(Landroid/os/Bundle;)Landroid/support/v7/media/MediaItemStatus$Builder;

    .line 683
    :cond_1
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 684
    const-string v3, "android.media.intent.extra.ITEM_ID"

    iget-object v4, p0, Lcom/google/cast/c$a$2;->a:Lcom/google/cast/c$a;

    invoke-static {v4}, Lcom/google/cast/c$a;->c(Lcom/google/cast/c$a;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 685
    const-string v3, "android.media.intent.extra.ITEM_STATUS"

    invoke-virtual {v1}, Landroid/support/v7/media/MediaItemStatus$Builder;->build()Landroid/support/v7/media/MediaItemStatus;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v7/media/MediaItemStatus;->asBundle()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 689
    :try_start_0
    iget-object v1, p0, Lcom/google/cast/c$a$2;->a:Lcom/google/cast/c$a;

    iget-object v1, v1, Lcom/google/cast/c$a;->d:Lcom/google/cast/c;

    invoke-virtual {v1}, Lcom/google/cast/c;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3, v2}, Landroid/app/PendingIntent;->send(Landroid/content/Context;ILandroid/content/Intent;)V
    :try_end_0
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_0 .. :try_end_0} :catch_0

    .line 694
    :goto_0
    iget-object v0, p0, Lcom/google/cast/c$a$2;->a:Lcom/google/cast/c$a;

    invoke-static {v0}, Lcom/google/cast/c$a;->d(Lcom/google/cast/c$a;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/google/cast/c$a$2;->a:Lcom/google/cast/c$a;

    invoke-static {v1}, Lcom/google/cast/c$a;->c(Lcom/google/cast/c$a;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 696
    :cond_2
    return-void

    .line 690
    :catch_0
    move-exception v0

    .line 691
    invoke-static {}, Lcom/google/cast/c;->b()Lcom/google/cast/Logger;

    move-result-object v1

    const-string v2, "exception while sending PendingIntent"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/cast/Logger;->d(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method protected onStatusUpdated()V
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x2

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 620
    invoke-static {}, Lcom/google/cast/c;->b()Lcom/google/cast/Logger;

    move-result-object v0

    const-string v2, "onStatusUpdated; mLoadInProgress=%b, mCurrentItemId=%s, player state: %s"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/cast/c$a$2;->a:Lcom/google/cast/c$a;

    invoke-static {v4}, Lcom/google/cast/c$a;->b(Lcom/google/cast/c$a;)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v3, v7

    iget-object v4, p0, Lcom/google/cast/c$a$2;->a:Lcom/google/cast/c$a;

    invoke-static {v4}, Lcom/google/cast/c$a;->c(Lcom/google/cast/c$a;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {p0}, Lcom/google/cast/c$a$2;->getPlayerState()Lcom/google/cast/MediaProtocolMessageStream$PlayerState;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v0, v2, v3}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 623
    iget-object v0, p0, Lcom/google/cast/c$a$2;->a:Lcom/google/cast/c$a;

    invoke-static {v0}, Lcom/google/cast/c$a;->b(Lcom/google/cast/c$a;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 664
    :cond_0
    :goto_0
    return-void

    .line 628
    :cond_1
    invoke-virtual {p0}, Lcom/google/cast/c$a$2;->getPlayerState()Lcom/google/cast/MediaProtocolMessageStream$PlayerState;

    move-result-object v0

    sget-object v2, Lcom/google/cast/MediaProtocolMessageStream$PlayerState;->PLAYING:Lcom/google/cast/MediaProtocolMessageStream$PlayerState;

    if-ne v0, v2, :cond_2

    .line 629
    iget-object v0, p0, Lcom/google/cast/c$a$2;->a:Lcom/google/cast/c$a;

    invoke-static {v0, v7}, Lcom/google/cast/c$a;->a(Lcom/google/cast/c$a;Z)Z

    .line 632
    :cond_2
    iget-object v0, p0, Lcom/google/cast/c$a$2;->a:Lcom/google/cast/c$a;

    invoke-virtual {p0}, Lcom/google/cast/c$a$2;->getVolume()D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/cast/c$a;->a(D)V

    .line 635
    iget-object v0, p0, Lcom/google/cast/c$a$2;->a:Lcom/google/cast/c$a;

    invoke-static {v0}, Lcom/google/cast/c$a;->c(Lcom/google/cast/c$a;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 636
    iget-object v0, p0, Lcom/google/cast/c$a$2;->a:Lcom/google/cast/c$a;

    invoke-static {v0}, Lcom/google/cast/c$a;->d(Lcom/google/cast/c$a;)Ljava/util/Map;

    move-result-object v0

    iget-object v2, p0, Lcom/google/cast/c$a$2;->a:Lcom/google/cast/c$a;

    invoke-static {v2}, Lcom/google/cast/c$a;->c(Lcom/google/cast/c$a;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    .line 637
    invoke-static {}, Lcom/google/cast/c;->b()Lcom/google/cast/Logger;

    move-result-object v2

    const-string v3, "found a PendingIntent for item %s: %s"

    new-array v4, v5, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/cast/c$a$2;->a:Lcom/google/cast/c$a;

    invoke-static {v5}, Lcom/google/cast/c$a;->c(Lcom/google/cast/c$a;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    aput-object v0, v4, v6

    invoke-virtual {v2, v3, v4}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 641
    :goto_1
    if-eqz v0, :cond_3

    .line 642
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 643
    const-string v3, "android.media.intent.extra.ITEM_ID"

    iget-object v4, p0, Lcom/google/cast/c$a$2;->a:Lcom/google/cast/c$a;

    invoke-static {v4}, Lcom/google/cast/c$a;->c(Lcom/google/cast/c$a;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 644
    const-string v3, "android.media.intent.extra.ITEM_STATUS"

    iget-object v4, p0, Lcom/google/cast/c$a$2;->a:Lcom/google/cast/c$a;

    invoke-static {v4}, Lcom/google/cast/c$a;->e(Lcom/google/cast/c$a;)Landroid/os/Bundle;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 646
    const-string v3, "android.media.intent.extra.ITEM_METADATA"

    iget-object v4, p0, Lcom/google/cast/c$a$2;->a:Lcom/google/cast/c$a;

    invoke-static {v4}, Lcom/google/cast/c$a;->f(Lcom/google/cast/c$a;)Landroid/os/Bundle;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 650
    :try_start_0
    invoke-static {}, Lcom/google/cast/c;->b()Lcom/google/cast/Logger;

    move-result-object v3

    const-string v4, "Invoking the PendingIntent with: %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v2, v5, v6

    invoke-virtual {v3, v4, v5}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 651
    iget-object v3, p0, Lcom/google/cast/c$a$2;->a:Lcom/google/cast/c$a;

    iget-object v3, v3, Lcom/google/cast/c$a;->d:Lcom/google/cast/c;

    invoke-virtual {v3}, Lcom/google/cast/c;->getContext()Landroid/content/Context;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4, v2}, Landroid/app/PendingIntent;->send(Landroid/content/Context;ILandroid/content/Intent;)V
    :try_end_0
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_0 .. :try_end_0} :catch_0

    .line 658
    :cond_3
    :goto_2
    iget-object v0, p0, Lcom/google/cast/c$a$2;->a:Lcom/google/cast/c$a;

    invoke-static {v0}, Lcom/google/cast/c$a;->g(Lcom/google/cast/c$a;)Lcom/google/cast/MediaProtocolMessageStream;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/cast/MediaProtocolMessageStream;->getPlayerState()Lcom/google/cast/MediaProtocolMessageStream$PlayerState;

    move-result-object v0

    sget-object v2, Lcom/google/cast/MediaProtocolMessageStream$PlayerState;->IDLE:Lcom/google/cast/MediaProtocolMessageStream$PlayerState;

    if-ne v0, v2, :cond_0

    .line 660
    iget-object v0, p0, Lcom/google/cast/c$a$2;->a:Lcom/google/cast/c$a;

    invoke-static {v0}, Lcom/google/cast/c$a;->d(Lcom/google/cast/c$a;)Ljava/util/Map;

    move-result-object v0

    iget-object v2, p0, Lcom/google/cast/c$a$2;->a:Lcom/google/cast/c$a;

    invoke-static {v2}, Lcom/google/cast/c$a;->c(Lcom/google/cast/c$a;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 661
    invoke-static {}, Lcom/google/cast/c;->b()Lcom/google/cast/Logger;

    move-result-object v0

    const-string v2, "player state is now IDLE; clearing mCurrentItemId"

    new-array v3, v7, [Ljava/lang/Object;

    invoke-virtual {v0, v2, v3}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 662
    iget-object v0, p0, Lcom/google/cast/c$a$2;->a:Lcom/google/cast/c$a;

    invoke-static {v0, v1}, Lcom/google/cast/c$a;->a(Lcom/google/cast/c$a;Ljava/lang/String;)Ljava/lang/String;

    goto/16 :goto_0

    .line 652
    :catch_0
    move-exception v0

    .line 653
    invoke-static {}, Lcom/google/cast/c;->b()Lcom/google/cast/Logger;

    move-result-object v2

    const-string v3, "exception while sending PendingIntent"

    new-array v4, v7, [Ljava/lang/Object;

    invoke-virtual {v2, v0, v3, v4}, Lcom/google/cast/Logger;->d(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_1
.end method
