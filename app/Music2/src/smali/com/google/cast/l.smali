.class final Lcom/google/cast/l;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/cast/SimpleHttpRequest;


# static fields
.field public static final a:I

.field public static final b:I


# instance fields
.field private c:Lcom/google/cast/CastContext;

.field private d:I

.field private e:I

.field private f:Landroid/net/Uri;

.field private g:Landroid/net/Uri;

.field private h:I

.field private i:Lorg/apache/http/client/methods/HttpRequestBase;

.field private j:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private k:Lcom/google/cast/MimeData;

.field private l:[Lorg/apache/http/Header;

.field private m:Lcom/google/cast/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 48
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x3

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    long-to-int v0, v0

    sput v0, Lcom/google/cast/l;->a:I

    .line 49
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xa

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    long-to-int v0, v0

    sput v0, Lcom/google/cast/l;->b:I

    return-void
.end method

.method public constructor <init>(Lcom/google/cast/CastContext;II)V
    .locals 2

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    iput-object p1, p0, Lcom/google/cast/l;->c:Lcom/google/cast/CastContext;

    .line 70
    iput p2, p0, Lcom/google/cast/l;->d:I

    .line 71
    iput p3, p0, Lcom/google/cast/l;->e:I

    .line 72
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/cast/l;->j:Ljava/util/HashMap;

    .line 73
    new-instance v0, Lcom/google/cast/Logger;

    const-string v1, "SimpleHttpRequestImpl"

    invoke-direct {v0, v1}, Lcom/google/cast/Logger;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/cast/l;->m:Lcom/google/cast/Logger;

    .line 74
    return-void
.end method

.method private a(Lorg/apache/http/client/methods/HttpRequestBase;)V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/util/concurrent/TimeoutException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 151
    iput-object p1, p0, Lcom/google/cast/l;->i:Lorg/apache/http/client/methods/HttpRequestBase;

    .line 152
    iget-object v0, p0, Lcom/google/cast/l;->j:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 153
    iget-object v5, p0, Lcom/google/cast/l;->i:Lorg/apache/http/client/methods/HttpRequestBase;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v5, v1, v0}, Lorg/apache/http/client/methods/HttpRequestBase;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 155
    :cond_0
    new-instance v4, Lorg/apache/http/protocol/BasicHttpContext;

    invoke-direct {v4}, Lorg/apache/http/protocol/BasicHttpContext;-><init>()V

    .line 156
    iget-object v0, p0, Lcom/google/cast/l;->c:Lcom/google/cast/CastContext;

    invoke-virtual {v0}, Lcom/google/cast/CastContext;->a()Landroid/net/http/AndroidHttpClient;

    move-result-object v5

    .line 157
    invoke-virtual {v5}, Landroid/net/http/AndroidHttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v0

    iget v1, p0, Lcom/google/cast/l;->d:I

    invoke-static {v0, v1}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 158
    invoke-virtual {v5}, Landroid/net/http/AndroidHttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v0

    iget v1, p0, Lcom/google/cast/l;->e:I

    invoke-static {v0, v1}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 163
    :try_start_0
    iget-object v0, p0, Lcom/google/cast/l;->m:Lcom/google/cast/Logger;

    const-string v1, "executing http request: %s %s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/google/cast/l;->i:Lorg/apache/http/client/methods/HttpRequestBase;

    invoke-virtual {v8}, Lorg/apache/http/client/methods/HttpRequestBase;->getMethod()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    iget-object v8, p0, Lcom/google/cast/l;->i:Lorg/apache/http/client/methods/HttpRequestBase;

    invoke-virtual {v8}, Lorg/apache/http/client/methods/HttpRequestBase;->getURI()Ljava/net/URI;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v0, v1, v6}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    move v1, v3

    move-object v0, v2

    .line 166
    :cond_1
    :goto_1
    const/4 v3, 0x5

    if-ge v1, v3, :cond_7

    .line 167
    iget-object v0, p0, Lcom/google/cast/l;->i:Lorg/apache/http/client/methods/HttpRequestBase;

    invoke-virtual {v5, v0, v4}, Landroid/net/http/AndroidHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/protocol/HttpContext;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 168
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v3

    invoke-interface {v3}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v3

    .line 169
    const/16 v6, 0x12e

    if-eq v3, v6, :cond_2

    const/16 v6, 0x12d

    if-eq v3, v6, :cond_2

    move-object v3, v0

    .line 193
    :goto_2
    const-string v0, "http.request"

    invoke-interface {v4, v0}, Lorg/apache/http/protocol/HttpContext;->getAttribute(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/client/methods/HttpUriRequest;

    .line 195
    const-string v1, "http.target_host"

    invoke-interface {v4, v1}, Lorg/apache/http/protocol/HttpContext;->getAttribute(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/http/HttpHost;

    .line 196
    invoke-interface {v0}, Lorg/apache/http/client/methods/HttpUriRequest;->getURI()Ljava/net/URI;

    move-result-object v4

    invoke-virtual {v4}, Ljava/net/URI;->isAbsolute()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_3
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/cast/l;->g:Landroid/net/Uri;

    .line 198
    iget-object v0, p0, Lcom/google/cast/l;->m:Lcom/google/cast/Logger;

    const-string v1, "final URI: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/cast/l;->g:Landroid/net/Uri;

    aput-object v6, v4, v5

    invoke-virtual {v0, v1, v4}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 200
    invoke-interface {v3}, Lorg/apache/http/HttpResponse;->getAllHeaders()[Lorg/apache/http/Header;

    move-result-object v0

    iput-object v0, p0, Lcom/google/cast/l;->l:[Lorg/apache/http/Header;

    .line 201
    invoke-interface {v3}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v0

    .line 202
    iget-object v1, p0, Lcom/google/cast/l;->m:Lcom/google/cast/Logger;

    const-string v4, "status line: %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    invoke-virtual {v1, v4, v5}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 203
    invoke-interface {v0}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v0

    iput v0, p0, Lcom/google/cast/l;->h:I

    .line 205
    invoke-interface {v3}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v3

    .line 206
    if-eqz v3, :cond_6

    .line 207
    invoke-interface {v3}, Lorg/apache/http/HttpEntity;->getContentType()Lorg/apache/http/Header;

    move-result-object v0

    .line 208
    if-eqz v0, :cond_4

    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 209
    :goto_4
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 210
    const-string v0, "application/octet-stream"

    move-object v1, v0

    .line 212
    :goto_5
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 213
    invoke-interface {v3, v2}, Lorg/apache/http/HttpEntity;->writeTo(Ljava/io/OutputStream;)V

    .line 214
    invoke-interface {v3}, Lorg/apache/http/HttpEntity;->consumeContent()V

    .line 215
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 216
    new-instance v0, Lcom/google/cast/MimeData;

    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    invoke-direct {v0, v2, v1}, Lcom/google/cast/MimeData;-><init>([BLjava/lang/String;)V

    .line 218
    :goto_6
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/cast/l;->i:Lorg/apache/http/client/methods/HttpRequestBase;
    :try_end_0
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_3

    .line 227
    iput-object v0, p0, Lcom/google/cast/l;->k:Lcom/google/cast/MimeData;

    .line 228
    return-void

    .line 173
    :cond_2
    :try_start_1
    const-string v3, "Location"

    invoke-interface {v0, v3}, Lorg/apache/http/HttpResponse;->getHeaders(Ljava/lang/String;)[Lorg/apache/http/Header;

    move-result-object v3

    .line 174
    if-eqz v3, :cond_7

    array-length v6, v3
    :try_end_1
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_3

    if-eqz v6, :cond_7

    .line 176
    :try_start_2
    iget-object v6, p0, Lcom/google/cast/l;->i:Lorg/apache/http/client/methods/HttpRequestBase;

    new-instance v7, Ljava/net/URI;

    array-length v8, v3

    add-int/lit8 v8, v8, -0x1

    aget-object v3, v3, v8

    invoke-interface {v3}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v7, v3}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v7}, Lorg/apache/http/client/methods/HttpRequestBase;->setURI(Ljava/net/URI;)V
    :try_end_2
    .catch Ljava/net/URISyntaxException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_3

    .line 181
    add-int/lit8 v1, v1, 0x1

    .line 185
    :try_start_3
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v3

    .line 186
    if-eqz v3, :cond_1

    .line 187
    invoke-interface {v3}, Lorg/apache/http/HttpEntity;->consumeContent()V
    :try_end_3
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/IllegalStateException; {:try_start_3 .. :try_end_3} :catch_3

    goto/16 :goto_1

    .line 219
    :catch_0
    move-exception v0

    .line 220
    new-instance v0, Ljava/io/IOException;

    const-string v1, "client protocol error"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 177
    :catch_1
    move-exception v1

    .line 178
    :try_start_4
    iget-object v1, p0, Lcom/google/cast/l;->m:Lcom/google/cast/Logger;

    const-string v3, "Redirect failed. Unable to parse Location header into an URI"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v1, v3, v5}, Lcom/google/cast/Logger;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v3, v0

    .line 179
    goto/16 :goto_2

    .line 196
    :cond_3
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Lorg/apache/http/HttpHost;->toURI()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v0}, Lorg/apache/http/client/methods/HttpUriRequest;->getURI()Ljava/net/URI;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_4
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/lang/IllegalStateException; {:try_start_4 .. :try_end_4} :catch_3

    move-result-object v0

    goto/16 :goto_3

    :cond_4
    move-object v0, v2

    .line 208
    goto/16 :goto_4

    .line 221
    :catch_2
    move-exception v0

    .line 222
    new-instance v0, Ljava/util/concurrent/TimeoutException;

    invoke-direct {v0}, Ljava/util/concurrent/TimeoutException;-><init>()V

    throw v0

    .line 223
    :catch_3
    move-exception v0

    .line 224
    new-instance v0, Ljava/net/MalformedURLException;

    invoke-direct {v0}, Ljava/net/MalformedURLException;-><init>()V

    throw v0

    :cond_5
    move-object v1, v0

    goto/16 :goto_5

    :cond_6
    move-object v0, v2

    goto :goto_6

    :cond_7
    move-object v3, v0

    goto/16 :goto_2
.end method


# virtual methods
.method public cancel()V
    .locals 4

    .prologue
    .line 110
    iget-object v0, p0, Lcom/google/cast/l;->i:Lorg/apache/http/client/methods/HttpRequestBase;

    .line 111
    if-eqz v0, :cond_0

    .line 113
    :try_start_0
    iget-object v1, p0, Lcom/google/cast/l;->m:Lcom/google/cast/Logger;

    const-string v2, "aborting the HTTP request"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/google/cast/Logger;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 115
    invoke-interface {v0}, Lorg/apache/http/client/methods/HttpUriRequest;->abort()V
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0

    .line 118
    :cond_0
    :goto_0
    return-void

    .line 116
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public getFinalUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/google/cast/l;->g:Landroid/net/Uri;

    return-object v0
.end method

.method public getResponseData()Lcom/google/cast/MimeData;
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/google/cast/l;->k:Lcom/google/cast/MimeData;

    return-object v0
.end method

.method public getResponseHeader(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 142
    iget-object v1, p0, Lcom/google/cast/l;->l:[Lorg/apache/http/Header;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 143
    invoke-interface {v3}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 144
    invoke-interface {v3}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 147
    :goto_1
    return-object v0

    .line 142
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 147
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public getResponseStatus()I
    .locals 1

    .prologue
    .line 137
    iget v0, p0, Lcom/google/cast/l;->h:I

    return v0
.end method

.method public performDelete(Landroid/net/Uri;)V
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/util/concurrent/TimeoutException;
        }
    .end annotation

    .prologue
    .line 103
    iput-object p1, p0, Lcom/google/cast/l;->f:Landroid/net/Uri;

    .line 104
    new-instance v0, Lorg/apache/http/client/methods/HttpDelete;

    iget-object v1, p0, Lcom/google/cast/l;->f:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/http/client/methods/HttpDelete;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/google/cast/l;->a(Lorg/apache/http/client/methods/HttpRequestBase;)V

    .line 105
    return-void
.end method

.method public performGet(Landroid/net/Uri;)V
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/util/concurrent/TimeoutException;
        }
    .end annotation

    .prologue
    .line 83
    iput-object p1, p0, Lcom/google/cast/l;->f:Landroid/net/Uri;

    .line 84
    new-instance v0, Lorg/apache/http/client/methods/HttpGet;

    iget-object v1, p0, Lcom/google/cast/l;->f:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/google/cast/l;->a(Lorg/apache/http/client/methods/HttpRequestBase;)V

    .line 85
    return-void
.end method

.method public performPost(Landroid/net/Uri;Lcom/google/cast/MimeData;)V
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "data"    # Lcom/google/cast/MimeData;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/util/concurrent/TimeoutException;
        }
    .end annotation

    .prologue
    .line 89
    iput-object p1, p0, Lcom/google/cast/l;->f:Landroid/net/Uri;

    .line 91
    new-instance v0, Lorg/apache/http/client/methods/HttpPost;

    iget-object v1, p0, Lcom/google/cast/l;->f:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 92
    if-eqz p2, :cond_0

    .line 93
    new-instance v1, Lorg/apache/http/entity/ByteArrayEntity;

    invoke-virtual {p2}, Lcom/google/cast/MimeData;->getData()[B

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/apache/http/entity/ByteArrayEntity;-><init>([B)V

    .line 94
    invoke-virtual {p2}, Lcom/google/cast/MimeData;->getType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/apache/http/entity/AbstractHttpEntity;->setContentType(Ljava/lang/String;)V

    .line 95
    invoke-virtual {v0, v1}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 98
    :cond_0
    invoke-direct {p0, v0}, Lcom/google/cast/l;->a(Lorg/apache/http/client/methods/HttpRequestBase;)V

    .line 99
    return-void
.end method

.method public setRequestHeader(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "header"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/cast/l;->j:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    return-void
.end method
