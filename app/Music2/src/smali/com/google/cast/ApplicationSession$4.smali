.class Lcom/google/cast/ApplicationSession$4;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/cast/ApplicationSession;->a(Landroid/net/Uri;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Landroid/net/Uri;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/google/cast/ApplicationSession;


# direct methods
.method constructor <init>(Lcom/google/cast/ApplicationSession;)V
    .locals 0

    .prologue
    .line 663
    iput-object p1, p0, Lcom/google/cast/ApplicationSession$4;->a:Lcom/google/cast/ApplicationSession;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs a([Landroid/net/Uri;)Ljava/lang/Boolean;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 666
    .line 668
    :try_start_0
    iget-object v2, p0, Lcom/google/cast/ApplicationSession$4;->a:Lcom/google/cast/ApplicationSession;

    invoke-static {v2}, Lcom/google/cast/ApplicationSession;->b(Lcom/google/cast/ApplicationSession;)Lcom/google/cast/Logger;

    move-result-object v2

    const-string v3, "trying to connect channel to: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const/4 v6, 0x0

    aget-object v6, p1, v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 669
    iget-object v2, p0, Lcom/google/cast/ApplicationSession$4;->a:Lcom/google/cast/ApplicationSession;

    invoke-static {v2}, Lcom/google/cast/ApplicationSession;->n(Lcom/google/cast/ApplicationSession;)Lcom/google/cast/ApplicationChannel;

    move-result-object v2

    const/4 v3, 0x0

    aget-object v3, p1, v3

    invoke-virtual {v2, v3}, Lcom/google/cast/ApplicationChannel;->a(Landroid/net/Uri;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 674
    :goto_0
    iget-object v1, p0, Lcom/google/cast/ApplicationSession$4;->a:Lcom/google/cast/ApplicationSession;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/google/cast/ApplicationSession;->a(Lcom/google/cast/ApplicationSession;Landroid/os/AsyncTask;)Landroid/os/AsyncTask;

    .line 675
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    .line 670
    :catch_0
    move-exception v0

    .line 671
    iget-object v2, p0, Lcom/google/cast/ApplicationSession$4;->a:Lcom/google/cast/ApplicationSession;

    invoke-static {v2}, Lcom/google/cast/ApplicationSession;->b(Lcom/google/cast/ApplicationSession;)Lcom/google/cast/Logger;

    move-result-object v2

    const-string v3, "error while connecting channel"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-virtual {v2, v0, v3, v4}, Lcom/google/cast/Logger;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 672
    goto :goto_0
.end method

.method protected a(Ljava/lang/Boolean;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 686
    iget-object v0, p0, Lcom/google/cast/ApplicationSession$4;->a:Lcom/google/cast/ApplicationSession;

    invoke-static {v0}, Lcom/google/cast/ApplicationSession;->b(Lcom/google/cast/ApplicationSession;)Lcom/google/cast/Logger;

    move-result-object v0

    const-string v1, "ConnectChannelTask completed with success status: %b"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 687
    iget-object v0, p0, Lcom/google/cast/ApplicationSession$4;->a:Lcom/google/cast/ApplicationSession;

    invoke-static {v0, v4}, Lcom/google/cast/ApplicationSession;->a(Lcom/google/cast/ApplicationSession;Landroid/os/AsyncTask;)Landroid/os/AsyncTask;

    .line 688
    iget-object v0, p0, Lcom/google/cast/ApplicationSession$4;->a:Lcom/google/cast/ApplicationSession;

    invoke-static {v0}, Lcom/google/cast/ApplicationSession;->a(Lcom/google/cast/ApplicationSession;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 689
    iget-object v0, p0, Lcom/google/cast/ApplicationSession$4;->a:Lcom/google/cast/ApplicationSession;

    invoke-static {v0, v4}, Lcom/google/cast/ApplicationSession;->a(Lcom/google/cast/ApplicationSession;Lcom/google/cast/SessionError;)V

    .line 696
    :cond_0
    :goto_0
    return-void

    .line 692
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 693
    iget-object v0, p0, Lcom/google/cast/ApplicationSession$4;->a:Lcom/google/cast/ApplicationSession;

    new-instance v1, Lcom/google/cast/SessionError;

    const/4 v2, 0x2

    const/4 v3, 0x3

    invoke-direct {v1, v2, v3}, Lcom/google/cast/SessionError;-><init>(II)V

    invoke-static {v0, v1}, Lcom/google/cast/ApplicationSession;->a(Lcom/google/cast/ApplicationSession;Lcom/google/cast/SessionError;)V

    goto :goto_0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 663
    check-cast p1, [Landroid/net/Uri;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/cast/ApplicationSession$4;->a([Landroid/net/Uri;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected onCancelled()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 680
    iget-object v0, p0, Lcom/google/cast/ApplicationSession$4;->a:Lcom/google/cast/ApplicationSession;

    invoke-static {v0, v1}, Lcom/google/cast/ApplicationSession;->a(Lcom/google/cast/ApplicationSession;Landroid/os/AsyncTask;)Landroid/os/AsyncTask;

    .line 681
    iget-object v0, p0, Lcom/google/cast/ApplicationSession$4;->a:Lcom/google/cast/ApplicationSession;

    invoke-static {v0, v1}, Lcom/google/cast/ApplicationSession;->a(Lcom/google/cast/ApplicationSession;Lcom/google/cast/SessionError;)V

    .line 682
    return-void
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 663
    check-cast p1, Ljava/lang/Boolean;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/cast/ApplicationSession$4;->a(Ljava/lang/Boolean;)V

    return-void
.end method
