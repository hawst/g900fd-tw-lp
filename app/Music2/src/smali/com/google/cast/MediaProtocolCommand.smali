.class public final Lcom/google/cast/MediaProtocolCommand;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/cast/MediaProtocolCommand$Listener;
    }
.end annotation


# instance fields
.field private a:J

.field private b:Ljava/lang/String;

.field private c:Lcom/google/cast/MediaProtocolCommand$Listener;

.field private d:Z

.field private e:Z

.field private f:Z

.field private g:Ljava/lang/String;

.field private h:J

.field private i:Lorg/json/JSONObject;

.field private j:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(JLjava/lang/String;)V
    .locals 1
    .param p1, "id"    # J
    .param p3, "type"    # Ljava/lang/String;

    .prologue
    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    iput-wide p1, p0, Lcom/google/cast/MediaProtocolCommand;->a:J

    .line 86
    iput-object p3, p0, Lcom/google/cast/MediaProtocolCommand;->b:Ljava/lang/String;

    .line 87
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/cast/MediaProtocolCommand;->j:Ljava/util/HashMap;

    .line 88
    return-void
.end method


# virtual methods
.method a()J
    .locals 2

    .prologue
    .line 152
    iget-wide v0, p0, Lcom/google/cast/MediaProtocolCommand;->a:J

    return-wide v0
.end method

.method a(Ljava/lang/String;JLorg/json/JSONObject;)V
    .locals 2

    .prologue
    .line 156
    iput-object p1, p0, Lcom/google/cast/MediaProtocolCommand;->g:Ljava/lang/String;

    .line 157
    iput-wide p2, p0, Lcom/google/cast/MediaProtocolCommand;->h:J

    .line 158
    iput-object p4, p0, Lcom/google/cast/MediaProtocolCommand;->i:Lorg/json/JSONObject;

    .line 159
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/cast/MediaProtocolCommand;->f:Z

    .line 160
    return-void
.end method

.method b()V
    .locals 1

    .prologue
    .line 163
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/cast/MediaProtocolCommand;->d:Z

    .line 164
    iget-object v0, p0, Lcom/google/cast/MediaProtocolCommand;->c:Lcom/google/cast/MediaProtocolCommand$Listener;

    if-eqz v0, :cond_0

    .line 165
    iget-object v0, p0, Lcom/google/cast/MediaProtocolCommand;->c:Lcom/google/cast/MediaProtocolCommand$Listener;

    invoke-interface {v0, p0}, Lcom/google/cast/MediaProtocolCommand$Listener;->onCompleted(Lcom/google/cast/MediaProtocolCommand;)V

    .line 167
    :cond_0
    return-void
.end method

.method c()V
    .locals 1

    .prologue
    .line 170
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/cast/MediaProtocolCommand;->e:Z

    .line 171
    iget-object v0, p0, Lcom/google/cast/MediaProtocolCommand;->c:Lcom/google/cast/MediaProtocolCommand$Listener;

    if-eqz v0, :cond_0

    .line 172
    iget-object v0, p0, Lcom/google/cast/MediaProtocolCommand;->c:Lcom/google/cast/MediaProtocolCommand$Listener;

    invoke-interface {v0, p0}, Lcom/google/cast/MediaProtocolCommand$Listener;->onCancelled(Lcom/google/cast/MediaProtocolCommand;)V

    .line 174
    :cond_0
    return-void
.end method

.method public getErrorDomain()Ljava/lang/String;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/google/cast/MediaProtocolCommand;->g:Ljava/lang/String;

    return-object v0
.end method

.method public getErrorInfo()Lorg/json/JSONObject;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/google/cast/MediaProtocolCommand;->i:Lorg/json/JSONObject;

    return-object v0
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/google/cast/MediaProtocolCommand;->b:Ljava/lang/String;

    return-object v0
.end method

.method public getUserObject(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 194
    iget-object v0, p0, Lcom/google/cast/MediaProtocolCommand;->j:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public hasError()Z
    .locals 1

    .prologue
    .line 116
    iget-boolean v0, p0, Lcom/google/cast/MediaProtocolCommand;->f:Z

    return v0
.end method

.method public putUserObject(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "object"    # Ljava/lang/Object;

    .prologue
    .line 184
    iget-object v0, p0, Lcom/google/cast/MediaProtocolCommand;->j:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public setListener(Lcom/google/cast/MediaProtocolCommand$Listener;)V
    .locals 0
    .param p1, "listener"    # Lcom/google/cast/MediaProtocolCommand$Listener;

    .prologue
    .line 94
    iput-object p1, p0, Lcom/google/cast/MediaProtocolCommand;->c:Lcom/google/cast/MediaProtocolCommand$Listener;

    .line 95
    return-void
.end method
