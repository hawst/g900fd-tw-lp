.class public abstract Lcom/google/cast/NetworkRequest;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final DEFAULT_TIMEOUT:I


# instance fields
.field private a:Lcom/google/cast/SimpleHttpRequest;

.field private b:Ljava/lang/Object;

.field private c:Z

.field private d:Lcom/google/cast/Logger;

.field protected mCastContext:Lcom/google/cast/CastContext;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 26
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xa

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    long-to-int v0, v0

    sput v0, Lcom/google/cast/NetworkRequest;->DEFAULT_TIMEOUT:I

    return-void
.end method

.method protected constructor <init>(Lcom/google/cast/CastContext;)V
    .locals 2
    .param p1, "castContext"    # Lcom/google/cast/CastContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/cast/NetworkRequest;->b:Ljava/lang/Object;

    .line 93
    if-nez p1, :cond_0

    .line 94
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "cast context cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 96
    :cond_0
    iput-object p1, p0, Lcom/google/cast/NetworkRequest;->mCastContext:Lcom/google/cast/CastContext;

    .line 97
    new-instance v0, Lcom/google/cast/Logger;

    const-string v1, "NetworkRequest"

    invoke-direct {v0, v1}, Lcom/google/cast/Logger;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/cast/NetworkRequest;->d:Lcom/google/cast/Logger;

    .line 98
    return-void
.end method

.method private a(Lcom/google/cast/SimpleHttpRequest;Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/cast/SimpleHttpRequest;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 241
    if-nez p2, :cond_1

    .line 247
    :cond_0
    return-void

    .line 244
    :cond_1
    invoke-interface {p2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 245
    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {p1, v0, v1}, Lcom/google/cast/SimpleHttpRequest;->setRequestHeader(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public final cancel()V
    .locals 2

    .prologue
    .line 114
    iget-object v1, p0, Lcom/google/cast/NetworkRequest;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 115
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/cast/NetworkRequest;->c:Z

    .line 116
    iget-object v0, p0, Lcom/google/cast/NetworkRequest;->a:Lcom/google/cast/SimpleHttpRequest;

    if-eqz v0, :cond_0

    .line 117
    iget-object v0, p0, Lcom/google/cast/NetworkRequest;->a:Lcom/google/cast/SimpleHttpRequest;

    invoke-interface {v0}, Lcom/google/cast/SimpleHttpRequest;->cancel()V

    .line 119
    :cond_0
    monitor-exit v1

    .line 120
    return-void

    .line 119
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected constructHttpRequest(I)Lcom/google/cast/SimpleHttpRequest;
    .locals 2
    .param p1, "timeout"    # I

    .prologue
    .line 252
    new-instance v0, Lcom/google/cast/l;

    iget-object v1, p0, Lcom/google/cast/NetworkRequest;->mCastContext:Lcom/google/cast/CastContext;

    invoke-direct {v0, v1, p1, p1}, Lcom/google/cast/l;-><init>(Lcom/google/cast/CastContext;II)V

    return-object v0
.end method

.method public abstract execute()I
.end method

.method protected final isCancelled()Z
    .locals 1

    .prologue
    .line 129
    iget-boolean v0, p0, Lcom/google/cast/NetworkRequest;->c:Z

    return v0
.end method

.method protected final performHttpDelete(Landroid/net/Uri;I)Lcom/google/cast/SimpleHttpRequest;
    .locals 5
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "timeout"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/util/concurrent/TimeoutException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 218
    iget-object v0, p0, Lcom/google/cast/NetworkRequest;->d:Lcom/google/cast/Logger;

    const-string v1, "performDelete: %s"

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 219
    invoke-virtual {p0, p2}, Lcom/google/cast/NetworkRequest;->constructHttpRequest(I)Lcom/google/cast/SimpleHttpRequest;

    move-result-object v0

    .line 220
    const-string v1, "Origin"

    const-string v2, "https://www.google.com"

    invoke-interface {v0, v1, v2}, Lcom/google/cast/SimpleHttpRequest;->setRequestHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    :try_start_0
    iget-object v1, p0, Lcom/google/cast/NetworkRequest;->b:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 223
    :try_start_1
    iput-object v0, p0, Lcom/google/cast/NetworkRequest;->a:Lcom/google/cast/SimpleHttpRequest;

    .line 224
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 225
    :try_start_2
    invoke-interface {v0, p1}, Lcom/google/cast/SimpleHttpRequest;->performDelete(Landroid/net/Uri;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 233
    iget-object v1, p0, Lcom/google/cast/NetworkRequest;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 234
    const/4 v2, 0x0

    :try_start_3
    iput-object v2, p0, Lcom/google/cast/NetworkRequest;->a:Lcom/google/cast/SimpleHttpRequest;

    .line 235
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 237
    return-object v0

    .line 224
    :catchall_0
    move-exception v0

    :try_start_4
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    throw v0
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 226
    :catch_0
    move-exception v0

    .line 227
    :try_start_6
    iget-object v1, p0, Lcom/google/cast/NetworkRequest;->d:Lcom/google/cast/Logger;

    const-string v2, "HTTP DELETE error: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 228
    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 233
    :catchall_1
    move-exception v0

    iget-object v1, p0, Lcom/google/cast/NetworkRequest;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 234
    const/4 v2, 0x0

    :try_start_7
    iput-object v2, p0, Lcom/google/cast/NetworkRequest;->a:Lcom/google/cast/SimpleHttpRequest;

    .line 235
    monitor-exit v1
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    throw v0

    :catchall_2
    move-exception v0

    :try_start_8
    monitor-exit v1
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    throw v0

    .line 229
    :catch_1
    move-exception v0

    .line 230
    :try_start_9
    iget-object v1, p0, Lcom/google/cast/NetworkRequest;->d:Lcom/google/cast/Logger;

    const-string v2, "HTTP DELETE timeout: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 231
    throw v0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 235
    :catchall_3
    move-exception v0

    :try_start_a
    monitor-exit v1
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_3

    throw v0
.end method

.method protected final performHttpGet(Landroid/net/Uri;I)Lcom/google/cast/SimpleHttpRequest;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "timeout"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/util/concurrent/TimeoutException;
        }
    .end annotation

    .prologue
    .line 141
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2}, Lcom/google/cast/NetworkRequest;->performHttpGet(Landroid/net/Uri;Ljava/util/Map;I)Lcom/google/cast/SimpleHttpRequest;

    move-result-object v0

    return-object v0
.end method

.method protected final performHttpGet(Landroid/net/Uri;Ljava/util/Map;I)Lcom/google/cast/SimpleHttpRequest;
    .locals 5
    .param p1, "uri"    # Landroid/net/Uri;
    .param p3, "timeout"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;I)",
            "Lcom/google/cast/SimpleHttpRequest;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/util/concurrent/TimeoutException;
        }
    .end annotation

    .prologue
    .local p2, "extraHeaders":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 154
    iget-object v0, p0, Lcom/google/cast/NetworkRequest;->d:Lcom/google/cast/Logger;

    const-string v1, "performGet: %s"

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 155
    invoke-virtual {p0, p3}, Lcom/google/cast/NetworkRequest;->constructHttpRequest(I)Lcom/google/cast/SimpleHttpRequest;

    move-result-object v0

    .line 156
    const-string v1, "Origin"

    const-string v2, "https://www.google.com"

    invoke-interface {v0, v1, v2}, Lcom/google/cast/SimpleHttpRequest;->setRequestHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    invoke-direct {p0, v0, p2}, Lcom/google/cast/NetworkRequest;->a(Lcom/google/cast/SimpleHttpRequest;Ljava/util/Map;)V

    .line 159
    :try_start_0
    iget-object v1, p0, Lcom/google/cast/NetworkRequest;->b:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 160
    :try_start_1
    iput-object v0, p0, Lcom/google/cast/NetworkRequest;->a:Lcom/google/cast/SimpleHttpRequest;

    .line 161
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 162
    :try_start_2
    invoke-interface {v0, p1}, Lcom/google/cast/SimpleHttpRequest;->performGet(Landroid/net/Uri;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 170
    iget-object v1, p0, Lcom/google/cast/NetworkRequest;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 171
    const/4 v2, 0x0

    :try_start_3
    iput-object v2, p0, Lcom/google/cast/NetworkRequest;->a:Lcom/google/cast/SimpleHttpRequest;

    .line 172
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 174
    return-object v0

    .line 161
    :catchall_0
    move-exception v0

    :try_start_4
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    throw v0
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 163
    :catch_0
    move-exception v0

    .line 164
    :try_start_6
    iget-object v1, p0, Lcom/google/cast/NetworkRequest;->d:Lcom/google/cast/Logger;

    const-string v2, "HTTP GET error: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 165
    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 170
    :catchall_1
    move-exception v0

    iget-object v1, p0, Lcom/google/cast/NetworkRequest;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 171
    const/4 v2, 0x0

    :try_start_7
    iput-object v2, p0, Lcom/google/cast/NetworkRequest;->a:Lcom/google/cast/SimpleHttpRequest;

    .line 172
    monitor-exit v1
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    throw v0

    :catchall_2
    move-exception v0

    :try_start_8
    monitor-exit v1
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    throw v0

    .line 166
    :catch_1
    move-exception v0

    .line 167
    :try_start_9
    iget-object v1, p0, Lcom/google/cast/NetworkRequest;->d:Lcom/google/cast/Logger;

    const-string v2, "HTTP GET timeout: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 168
    throw v0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 172
    :catchall_3
    move-exception v0

    :try_start_a
    monitor-exit v1
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_3

    throw v0
.end method

.method protected final performHttpPost(Landroid/net/Uri;Lcom/google/cast/MimeData;I)Lcom/google/cast/SimpleHttpRequest;
    .locals 5
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "data"    # Lcom/google/cast/MimeData;
    .param p3, "timeout"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/util/concurrent/TimeoutException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 187
    iget-object v0, p0, Lcom/google/cast/NetworkRequest;->d:Lcom/google/cast/Logger;

    const-string v1, "performPost: %s"

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 188
    invoke-virtual {p0, p3}, Lcom/google/cast/NetworkRequest;->constructHttpRequest(I)Lcom/google/cast/SimpleHttpRequest;

    move-result-object v0

    .line 189
    const-string v1, "Origin"

    const-string v2, "https://www.google.com"

    invoke-interface {v0, v1, v2}, Lcom/google/cast/SimpleHttpRequest;->setRequestHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    :try_start_0
    iget-object v1, p0, Lcom/google/cast/NetworkRequest;->b:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 192
    :try_start_1
    iput-object v0, p0, Lcom/google/cast/NetworkRequest;->a:Lcom/google/cast/SimpleHttpRequest;

    .line 193
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 194
    :try_start_2
    invoke-interface {v0, p1, p2}, Lcom/google/cast/SimpleHttpRequest;->performPost(Landroid/net/Uri;Lcom/google/cast/MimeData;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 202
    iget-object v1, p0, Lcom/google/cast/NetworkRequest;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 203
    const/4 v2, 0x0

    :try_start_3
    iput-object v2, p0, Lcom/google/cast/NetworkRequest;->a:Lcom/google/cast/SimpleHttpRequest;

    .line 204
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 206
    return-object v0

    .line 193
    :catchall_0
    move-exception v0

    :try_start_4
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    throw v0
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 195
    :catch_0
    move-exception v0

    .line 196
    :try_start_6
    iget-object v1, p0, Lcom/google/cast/NetworkRequest;->d:Lcom/google/cast/Logger;

    const-string v2, "HTTP POST error: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 197
    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 202
    :catchall_1
    move-exception v0

    iget-object v1, p0, Lcom/google/cast/NetworkRequest;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 203
    const/4 v2, 0x0

    :try_start_7
    iput-object v2, p0, Lcom/google/cast/NetworkRequest;->a:Lcom/google/cast/SimpleHttpRequest;

    .line 204
    monitor-exit v1
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    throw v0

    :catchall_2
    move-exception v0

    :try_start_8
    monitor-exit v1
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    throw v0

    .line 198
    :catch_1
    move-exception v0

    .line 199
    :try_start_9
    iget-object v1, p0, Lcom/google/cast/NetworkRequest;->d:Lcom/google/cast/Logger;

    const-string v2, "HTTP POST timeout: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 200
    throw v0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 204
    :catchall_3
    move-exception v0

    :try_start_a
    monitor-exit v1
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_3

    throw v0
.end method
