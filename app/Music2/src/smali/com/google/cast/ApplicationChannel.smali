.class public final Lcom/google/cast/ApplicationChannel;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/cast/ApplicationChannel$a;
    }
.end annotation


# instance fields
.field private a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/cast/MessageStream;",
            ">;"
        }
    .end annotation
.end field

.field private b:Lcom/google/cast/WebSocket;

.field private c:I

.field private d:Lcom/google/cast/ApplicationChannel$a;

.field private e:Landroid/os/Handler;

.field private f:Lcom/google/cast/Logger;

.field private g:Lcom/google/cast/i;

.field private h:Lcom/google/cast/CastDevice;

.field private i:Lcom/google/cast/CastContext;

.field private j:J

.field private k:J

.field private l:Ljava/lang/Runnable;

.field private m:Ljava/lang/Runnable;

.field private n:Landroid/os/PowerManager$WakeLock;


# direct methods
.method constructor <init>(Lcom/google/cast/CastContext;IJLcom/google/cast/ApplicationChannel$a;Landroid/os/Handler;Lcom/google/cast/CastDevice;Z)V
    .locals 5
    .param p1, "castContext"    # Lcom/google/cast/CastContext;
    .param p2, "bufferSize"    # I
    .param p3, "pingIntervalMs"    # J
    .param p5, "listener"    # Lcom/google/cast/ApplicationChannel$a;
    .param p6, "handler"    # Landroid/os/Handler;
    .param p7, "castDevice"    # Lcom/google/cast/CastDevice;
    .param p8, "isWakeLockEnabled"    # Z

    .prologue
    .line 113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 114
    iput-object p1, p0, Lcom/google/cast/ApplicationChannel;->i:Lcom/google/cast/CastContext;

    .line 115
    iput p2, p0, Lcom/google/cast/ApplicationChannel;->c:I

    .line 116
    const-wide/16 v0, 0x2

    mul-long/2addr v0, p3

    iput-wide v0, p0, Lcom/google/cast/ApplicationChannel;->k:J

    .line 117
    iput-object p5, p0, Lcom/google/cast/ApplicationChannel;->d:Lcom/google/cast/ApplicationChannel$a;

    .line 118
    iput-object p6, p0, Lcom/google/cast/ApplicationChannel;->e:Landroid/os/Handler;

    .line 119
    iput-object p7, p0, Lcom/google/cast/ApplicationChannel;->h:Lcom/google/cast/CastDevice;

    .line 120
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/cast/ApplicationChannel;->a:Ljava/util/Map;

    .line 122
    if-eqz p8, :cond_0

    .line 123
    invoke-virtual {p1}, Lcom/google/cast/CastContext;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "power"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 125
    const/4 v1, 0x1

    const-string v2, "ApplicationChannel.mWakeLock"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/google/cast/ApplicationChannel;->n:Landroid/os/PowerManager$WakeLock;

    .line 126
    iget-object v0, p0, Lcom/google/cast/ApplicationChannel;->n:Landroid/os/PowerManager$WakeLock;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    .line 131
    :goto_0
    new-instance v0, Lcom/google/cast/ApplicationChannel$1;

    invoke-direct {v0, p0}, Lcom/google/cast/ApplicationChannel$1;-><init>(Lcom/google/cast/ApplicationChannel;)V

    iput-object v0, p0, Lcom/google/cast/ApplicationChannel;->l:Ljava/lang/Runnable;

    .line 137
    new-instance v0, Lcom/google/cast/ApplicationChannel$2;

    invoke-direct {v0, p0}, Lcom/google/cast/ApplicationChannel$2;-><init>(Lcom/google/cast/ApplicationChannel;)V

    iput-object v0, p0, Lcom/google/cast/ApplicationChannel;->m:Ljava/lang/Runnable;

    .line 156
    new-instance v0, Lcom/google/cast/Logger;

    const-string v1, "ApplicationChannel"

    invoke-direct {v0, v1}, Lcom/google/cast/Logger;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/cast/ApplicationChannel;->f:Lcom/google/cast/Logger;

    .line 158
    new-instance v0, Lcom/google/cast/ApplicationChannel$3;

    invoke-direct {v0, p0}, Lcom/google/cast/ApplicationChannel$3;-><init>(Lcom/google/cast/ApplicationChannel;)V

    iput-object v0, p0, Lcom/google/cast/ApplicationChannel;->g:Lcom/google/cast/i;

    .line 175
    new-instance v0, Lcom/google/cast/WebSocket;

    const-string v1, "https://www.google.com"

    const-string v2, ""

    iget v3, p0, Lcom/google/cast/ApplicationChannel;->c:I

    invoke-direct {v0, v1, v2, v3}, Lcom/google/cast/WebSocket;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/google/cast/ApplicationChannel;->b:Lcom/google/cast/WebSocket;

    .line 176
    iget-object v0, p0, Lcom/google/cast/ApplicationChannel;->b:Lcom/google/cast/WebSocket;

    new-instance v1, Lcom/google/cast/ApplicationChannel$4;

    invoke-direct {v1, p0}, Lcom/google/cast/ApplicationChannel$4;-><init>(Lcom/google/cast/ApplicationChannel;)V

    invoke-virtual {v0, v1}, Lcom/google/cast/WebSocket;->a(Lcom/google/cast/WebSocket$Listener;)V

    .line 253
    return-void

    .line 128
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/cast/ApplicationChannel;->n:Landroid/os/PowerManager$WakeLock;

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/cast/ApplicationChannel;J)J
    .locals 1

    .prologue
    .line 28
    iput-wide p1, p0, Lcom/google/cast/ApplicationChannel;->j:J

    return-wide p1
.end method

.method private a(I)V
    .locals 1

    .prologue
    .line 259
    invoke-direct {p0}, Lcom/google/cast/ApplicationChannel;->b()V

    .line 260
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/cast/ApplicationChannel;->b:Lcom/google/cast/WebSocket;

    .line 261
    invoke-virtual {p0}, Lcom/google/cast/ApplicationChannel;->detachAllMessageStreams()V

    .line 262
    iget-object v0, p0, Lcom/google/cast/ApplicationChannel;->d:Lcom/google/cast/ApplicationChannel$a;

    invoke-interface {v0, p0, p1}, Lcom/google/cast/ApplicationChannel$a;->b(Lcom/google/cast/ApplicationChannel;I)V

    .line 263
    return-void
.end method

.method static synthetic a(Lcom/google/cast/ApplicationChannel;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/google/cast/ApplicationChannel;->c()V

    return-void
.end method

.method static synthetic a(Lcom/google/cast/ApplicationChannel;I)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/google/cast/ApplicationChannel;->a(I)V

    return-void
.end method

.method static synthetic a(Lcom/google/cast/ApplicationChannel;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/google/cast/ApplicationChannel;->a(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/google/cast/ApplicationChannel;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Lcom/google/cast/ApplicationChannel;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/google/cast/ApplicationChannel;Ljava/lang/String;Lorg/json/JSONObject;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Lcom/google/cast/ApplicationChannel;->a(Ljava/lang/String;Lorg/json/JSONObject;)V

    return-void
.end method

.method static synthetic a(Lcom/google/cast/ApplicationChannel;Ljava/lang/String;[B)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Lcom/google/cast/ApplicationChannel;->a(Ljava/lang/String;[B)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 444
    :try_start_0
    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0, p1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 445
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v1

    const/4 v2, 0x2

    if-eq v1, v2, :cond_1

    .line 446
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lcom/google/cast/ApplicationChannel;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 489
    :cond_0
    :goto_0
    return-void

    .line 452
    :cond_1
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 453
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v2

    .line 455
    const-string v0, "cm"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 456
    const-string v0, "type"

    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 457
    const-string v1, "ping"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 458
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/cast/ApplicationChannel;->j:J

    .line 459
    invoke-direct {p0}, Lcom/google/cast/ApplicationChannel;->d()V

    .line 460
    iget-object v0, p0, Lcom/google/cast/ApplicationChannel;->e:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/cast/ApplicationChannel;->m:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 486
    :catch_0
    move-exception v0

    .line 487
    invoke-direct {p0, v4, p1, v0}, Lcom/google/cast/ApplicationChannel;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 465
    :cond_2
    :try_start_1
    iget-object v3, p0, Lcom/google/cast/ApplicationChannel;->a:Ljava/util/Map;

    monitor-enter v3
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    .line 466
    :try_start_2
    iget-object v0, p0, Lcom/google/cast/ApplicationChannel;->a:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/cast/MessageStream;

    .line 467
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 469
    if-nez v0, :cond_3

    .line 470
    :try_start_3
    invoke-direct {p0, v1, p1}, Lcom/google/cast/ApplicationChannel;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_0

    .line 467
    :catchall_0
    move-exception v0

    :try_start_4
    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    throw v0

    .line 475
    :cond_3
    invoke-direct {p0}, Lcom/google/cast/ApplicationChannel;->d()V

    .line 476
    iget-object v1, p0, Lcom/google/cast/ApplicationChannel;->e:Landroid/os/Handler;

    new-instance v3, Lcom/google/cast/ApplicationChannel$5;

    invoke-direct {v3, p0, v0, v2}, Lcom/google/cast/ApplicationChannel$5;-><init>(Lcom/google/cast/ApplicationChannel;Lcom/google/cast/MessageStream;Lorg/json/JSONObject;)V

    invoke-virtual {v1, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_5
    .catch Lorg/json/JSONException; {:try_start_5 .. :try_end_5} :catch_0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 503
    iget-object v0, p0, Lcom/google/cast/ApplicationChannel;->f:Lcom/google/cast/Logger;

    const-string v1, "Received unsupported text message with namespace %s: %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 504
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 495
    iget-object v0, p0, Lcom/google/cast/ApplicationChannel;->f:Lcom/google/cast/Logger;

    const-string v1, "Received unsupported text message with namespace %s: %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    invoke-virtual {v0, p3, v1, v2}, Lcom/google/cast/Logger;->d(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 496
    return-void
.end method

.method private a(Ljava/lang/String;Lorg/json/JSONObject;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 277
    iget-object v0, p0, Lcom/google/cast/ApplicationChannel;->b:Lcom/google/cast/WebSocket;

    if-nez v0, :cond_0

    .line 281
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Channel is not currently connected"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 284
    :cond_0
    const/4 v0, 0x0

    .line 286
    :try_start_0
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1}, Lorg/json/JSONArray;-><init>()V

    .line 287
    const/4 v2, 0x0

    invoke-virtual {v1, v2, p1}, Lorg/json/JSONArray;->put(ILjava/lang/Object;)Lorg/json/JSONArray;

    .line 288
    const/4 v2, 0x1

    invoke-virtual {v1, v2, p2}, Lorg/json/JSONArray;->put(ILjava/lang/Object;)Lorg/json/JSONArray;

    .line 289
    invoke-virtual {v1}, Lorg/json/JSONArray;->toString()Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 295
    :goto_0
    :try_start_1
    iget-object v1, p0, Lcom/google/cast/ApplicationChannel;->b:Lcom/google/cast/WebSocket;

    invoke-virtual {v1, v0}, Lcom/google/cast/WebSocket;->a(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0

    .line 304
    return-void

    .line 296
    :catch_0
    move-exception v0

    .line 302
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Channel has become disconnected"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 290
    :catch_1
    move-exception v1

    goto :goto_0
.end method

.method private a(Ljava/lang/String;[B)V
    .locals 4

    .prologue
    .line 512
    iget-object v0, p0, Lcom/google/cast/ApplicationChannel;->f:Lcom/google/cast/Logger;

    const-string v1, "Received unsupported binary message with namespace: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 513
    return-void
.end method

.method private b(I)I
    .locals 1

    .prologue
    .line 519
    packed-switch p1, :pswitch_data_0

    .line 529
    :pswitch_0
    const/4 v0, -0x1

    :goto_0
    return v0

    .line 523
    :pswitch_1
    const/4 v0, 0x0

    goto :goto_0

    .line 525
    :pswitch_2
    const/4 v0, -0x2

    goto :goto_0

    .line 519
    nop

    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic b(Lcom/google/cast/ApplicationChannel;I)I
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/google/cast/ApplicationChannel;->b(I)I

    move-result v0

    return v0
.end method

.method private b()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 427
    :try_start_0
    iget-wide v0, p0, Lcom/google/cast/ApplicationChannel;->k:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 428
    iget-object v0, p0, Lcom/google/cast/ApplicationChannel;->e:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/cast/ApplicationChannel;->l:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 429
    iget-object v0, p0, Lcom/google/cast/ApplicationChannel;->e:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/cast/ApplicationChannel;->m:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 430
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/cast/ApplicationChannel;->j:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 433
    :cond_0
    invoke-direct {p0}, Lcom/google/cast/ApplicationChannel;->e()V

    .line 435
    return-void

    .line 433
    :catchall_0
    move-exception v0

    invoke-direct {p0}, Lcom/google/cast/ApplicationChannel;->e()V

    throw v0
.end method

.method static synthetic b(Lcom/google/cast/ApplicationChannel;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/google/cast/ApplicationChannel;->e()V

    return-void
.end method

.method static synthetic c(Lcom/google/cast/ApplicationChannel;)J
    .locals 2

    .prologue
    .line 28
    iget-wide v0, p0, Lcom/google/cast/ApplicationChannel;->k:J

    return-wide v0
.end method

.method private c()V
    .locals 4

    .prologue
    .line 537
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 539
    iget-wide v2, p0, Lcom/google/cast/ApplicationChannel;->j:J

    sub-long/2addr v0, v2

    iget-wide v2, p0, Lcom/google/cast/ApplicationChannel;->k:J

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    .line 540
    iget-object v0, p0, Lcom/google/cast/ApplicationChannel;->f:Lcom/google/cast/Logger;

    const-string v1, "timeout waiting for ping; force-closing channel"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 541
    iget-object v0, p0, Lcom/google/cast/ApplicationChannel;->b:Lcom/google/cast/WebSocket;

    const/4 v1, -0x3

    invoke-virtual {v0, v1}, Lcom/google/cast/WebSocket;->a(I)V

    .line 545
    :goto_0
    return-void

    .line 543
    :cond_0
    iget-object v0, p0, Lcom/google/cast/ApplicationChannel;->e:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/cast/ApplicationChannel;->l:Ljava/lang/Runnable;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method static synthetic d(Lcom/google/cast/ApplicationChannel;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/cast/ApplicationChannel;->l:Ljava/lang/Runnable;

    return-object v0
.end method

.method private declared-synchronized d()V
    .locals 1

    .prologue
    .line 548
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/cast/ApplicationChannel;->n:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    .line 549
    iget-object v0, p0, Lcom/google/cast/ApplicationChannel;->n:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 551
    :cond_0
    monitor-exit p0

    return-void

    .line 548
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic e(Lcom/google/cast/ApplicationChannel;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/cast/ApplicationChannel;->e:Landroid/os/Handler;

    return-object v0
.end method

.method private declared-synchronized e()V
    .locals 1

    .prologue
    .line 554
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/cast/ApplicationChannel;->n:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/cast/ApplicationChannel;->n:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 555
    iget-object v0, p0, Lcom/google/cast/ApplicationChannel;->n:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 557
    :cond_0
    monitor-exit p0

    return-void

    .line 554
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic f(Lcom/google/cast/ApplicationChannel;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/google/cast/ApplicationChannel;->d()V

    return-void
.end method

.method static synthetic g(Lcom/google/cast/ApplicationChannel;)Lcom/google/cast/ApplicationChannel$a;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/cast/ApplicationChannel;->d:Lcom/google/cast/ApplicationChannel$a;

    return-object v0
.end method

.method static synthetic h(Lcom/google/cast/ApplicationChannel;)Lcom/google/cast/Logger;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/cast/ApplicationChannel;->f:Lcom/google/cast/Logger;

    return-object v0
.end method


# virtual methods
.method a()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 347
    iget-object v0, p0, Lcom/google/cast/ApplicationChannel;->f:Lcom/google/cast/Logger;

    const-string v1, "disconnect()"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 348
    invoke-direct {p0}, Lcom/google/cast/ApplicationChannel;->b()V

    .line 349
    invoke-virtual {p0}, Lcom/google/cast/ApplicationChannel;->detachAllMessageStreams()V

    .line 351
    iget-object v0, p0, Lcom/google/cast/ApplicationChannel;->b:Lcom/google/cast/WebSocket;

    if-eqz v0, :cond_0

    .line 353
    :try_start_0
    iget-object v0, p0, Lcom/google/cast/ApplicationChannel;->b:Lcom/google/cast/WebSocket;

    invoke-virtual {v0}, Lcom/google/cast/WebSocket;->a()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 357
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/cast/ApplicationChannel;->b:Lcom/google/cast/WebSocket;

    .line 359
    :cond_0
    return-void

    .line 354
    :catch_0
    move-exception v0

    .line 355
    iget-object v1, p0, Lcom/google/cast/ApplicationChannel;->f:Lcom/google/cast/Logger;

    const-string v2, "Error while disconnecting socket"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/cast/Logger;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method a(Landroid/net/Uri;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 272
    iget-object v0, p0, Lcom/google/cast/ApplicationChannel;->b:Lcom/google/cast/WebSocket;

    invoke-virtual {v0, p1}, Lcom/google/cast/WebSocket;->a(Landroid/net/Uri;)V

    .line 273
    return-void
.end method

.method public attachMessageStream(Lcom/google/cast/MessageStream;)V
    .locals 5
    .param p1, "stream"    # Lcom/google/cast/MessageStream;

    .prologue
    .line 369
    invoke-static {}, Lcom/google/cast/p;->a()V

    .line 371
    invoke-virtual {p1}, Lcom/google/cast/MessageStream;->getNamespace()Ljava/lang/String;

    move-result-object v0

    .line 372
    iget-object v1, p0, Lcom/google/cast/ApplicationChannel;->a:Ljava/util/Map;

    monitor-enter v1

    .line 373
    :try_start_0
    iget-object v2, p0, Lcom/google/cast/ApplicationChannel;->a:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 374
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MessageStream with namespace "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " already registered"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 379
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 377
    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/google/cast/ApplicationChannel;->a:Ljava/util/Map;

    invoke-interface {v2, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 378
    iget-object v0, p0, Lcom/google/cast/ApplicationChannel;->g:Lcom/google/cast/i;

    invoke-virtual {p1, v0}, Lcom/google/cast/MessageStream;->a(Lcom/google/cast/i;)V

    .line 379
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 380
    const-string v0, "ramp"

    invoke-virtual {p1}, Lcom/google/cast/MessageStream;->getNamespace()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 381
    iget-object v0, p0, Lcom/google/cast/ApplicationChannel;->i:Lcom/google/cast/CastContext;

    invoke-virtual {v0}, Lcom/google/cast/CastContext;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/cast/ApplicationChannel;->h:Lcom/google/cast/CastDevice;

    invoke-static {v0, v1}, Lcom/google/cast/d;->a(Landroid/content/Context;Lcom/google/cast/CastDevice;)V

    .line 384
    :cond_1
    invoke-virtual {p1}, Lcom/google/cast/MessageStream;->onAttached()V

    .line 385
    return-void
.end method

.method public detachAllMessageStreams()V
    .locals 3

    .prologue
    .line 410
    invoke-static {}, Lcom/google/cast/p;->a()V

    .line 414
    iget-object v1, p0, Lcom/google/cast/ApplicationChannel;->a:Ljava/util/Map;

    monitor-enter v1

    .line 415
    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/cast/ApplicationChannel;->a:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 416
    iget-object v2, p0, Lcom/google/cast/ApplicationChannel;->a:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->clear()V

    .line 417
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 419
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/cast/MessageStream;

    .line 420
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/google/cast/MessageStream;->a(Lcom/google/cast/i;)V

    .line 421
    invoke-virtual {v0}, Lcom/google/cast/MessageStream;->onDetached()V

    goto :goto_0

    .line 417
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 423
    :cond_0
    return-void
.end method
