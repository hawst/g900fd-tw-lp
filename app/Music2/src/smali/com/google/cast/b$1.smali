.class Lcom/google/cast/b$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/cast/DeviceManager$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/cast/b;-><init>(Lcom/google/cast/CastContext;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/google/cast/b;


# direct methods
.method constructor <init>(Lcom/google/cast/b;)V
    .locals 0

    .prologue
    .line 49
    iput-object p1, p0, Lcom/google/cast/b$1;->a:Lcom/google/cast/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDeviceOffline(Lcom/google/cast/CastDevice;)V
    .locals 2
    .param p1, "device"    # Lcom/google/cast/CastDevice;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/cast/b$1;->a:Lcom/google/cast/b;

    invoke-static {v0}, Lcom/google/cast/b;->a(Lcom/google/cast/b;)Ljava/util/Map;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/cast/CastDevice;->getDeviceId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/cast/b$a;

    .line 64
    if-eqz v0, :cond_0

    .line 65
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/cast/b$a;->a(Lcom/google/cast/b$a;Z)V

    .line 70
    :goto_0
    return-void

    .line 67
    :cond_0
    iget-object v0, p0, Lcom/google/cast/b$1;->a:Lcom/google/cast/b;

    invoke-static {v0, p1}, Lcom/google/cast/b;->b(Lcom/google/cast/b;Lcom/google/cast/CastDevice;)V

    .line 68
    iget-object v0, p0, Lcom/google/cast/b$1;->a:Lcom/google/cast/b;

    invoke-static {v0}, Lcom/google/cast/b;->b(Lcom/google/cast/b;)V

    goto :goto_0
.end method

.method public onDeviceOnline(Lcom/google/cast/CastDevice;)V
    .locals 2
    .param p1, "device"    # Lcom/google/cast/CastDevice;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/cast/b$1;->a:Lcom/google/cast/b;

    invoke-static {v0}, Lcom/google/cast/b;->a(Lcom/google/cast/b;)Ljava/util/Map;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/cast/CastDevice;->getDeviceId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/cast/b$a;

    .line 53
    if-eqz v0, :cond_0

    .line 54
    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/cast/b$a;->a(Lcom/google/cast/b$a;Z)V

    .line 59
    :goto_0
    return-void

    .line 56
    :cond_0
    iget-object v0, p0, Lcom/google/cast/b$1;->a:Lcom/google/cast/b;

    invoke-static {v0, p1}, Lcom/google/cast/b;->a(Lcom/google/cast/b;Lcom/google/cast/CastDevice;)V

    .line 57
    iget-object v0, p0, Lcom/google/cast/b$1;->a:Lcom/google/cast/b;

    invoke-static {v0}, Lcom/google/cast/b;->b(Lcom/google/cast/b;)V

    goto :goto_0
.end method

.method public onScanStateChanged(I)V
    .locals 0
    .param p1, "state"    # I

    .prologue
    .line 75
    return-void
.end method
