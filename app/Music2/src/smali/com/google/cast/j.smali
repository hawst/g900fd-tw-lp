.class final Lcom/google/cast/j;
.super Lcom/google/cast/b;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/cast/j$a;
    }
.end annotation


# static fields
.field private static final b:Lcom/google/cast/Logger;


# instance fields
.field private c:Lcom/google/cast/MediaRouteAdapter;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 19
    new-instance v0, Lcom/google/cast/Logger;

    const-string v1, "MinimalCastMediaRouteProvider"

    invoke-direct {v0, v1}, Lcom/google/cast/Logger;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/cast/j;->b:Lcom/google/cast/Logger;

    return-void
.end method

.method public constructor <init>(Lcom/google/cast/CastContext;Lcom/google/cast/MediaRouteAdapter;)V
    .locals 2

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/google/cast/b;-><init>(Lcom/google/cast/CastContext;)V

    .line 48
    if-nez p2, :cond_0

    .line 49
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "adapter cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 51
    :cond_0
    iput-object p2, p0, Lcom/google/cast/j;->c:Lcom/google/cast/MediaRouteAdapter;

    .line 53
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 54
    const-string v1, "com.google.cast.CATEGORY_CAST"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    .line 55
    const-string v1, "com.google.cast.ACTION_GET_DEVICE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 56
    invoke-virtual {p0, v0}, Lcom/google/cast/j;->a(Landroid/content/IntentFilter;)V

    .line 57
    return-void
.end method

.method static synthetic b()Lcom/google/cast/Logger;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/google/cast/j;->b:Lcom/google/cast/Logger;

    return-object v0
.end method


# virtual methods
.method protected a(Lcom/google/cast/CastDevice;Ljava/lang/String;)Lcom/google/cast/b$a;
    .locals 2

    .prologue
    .line 61
    new-instance v0, Lcom/google/cast/j$a;

    iget-object v1, p0, Lcom/google/cast/j;->c:Lcom/google/cast/MediaRouteAdapter;

    invoke-direct {v0, p0, p1, p2, v1}, Lcom/google/cast/j$a;-><init>(Lcom/google/cast/j;Lcom/google/cast/CastDevice;Ljava/lang/String;Lcom/google/cast/MediaRouteAdapter;)V

    return-object v0
.end method
