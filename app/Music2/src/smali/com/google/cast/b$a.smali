.class abstract Lcom/google/cast/b$a;
.super Landroid/support/v7/media/MediaRouteProvider$RouteController;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/cast/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x400
    name = "a"
.end annotation


# instance fields
.field protected final a:Lcom/google/cast/CastDevice;

.field protected final b:Ljava/lang/String;

.field final synthetic c:Lcom/google/cast/b;

.field private d:I

.field private e:Z

.field private f:Z

.field private g:Z


# direct methods
.method protected constructor <init>(Lcom/google/cast/b;Lcom/google/cast/CastDevice;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 243
    iput-object p1, p0, Lcom/google/cast/b$a;->c:Lcom/google/cast/b;

    invoke-direct {p0}, Landroid/support/v7/media/MediaRouteProvider$RouteController;-><init>()V

    .line 244
    iput-object p2, p0, Lcom/google/cast/b$a;->a:Lcom/google/cast/CastDevice;

    .line 245
    iput-object p3, p0, Lcom/google/cast/b$a;->b:Ljava/lang/String;

    .line 246
    iput v1, p0, Lcom/google/cast/b$a;->d:I

    .line 247
    iput-boolean v1, p0, Lcom/google/cast/b$a;->e:Z

    .line 248
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/cast/b$a;->f:Z

    .line 249
    iput-boolean v1, p0, Lcom/google/cast/b$a;->g:Z

    .line 250
    return-void
.end method

.method static synthetic a(Lcom/google/cast/b$a;Z)V
    .locals 0

    .prologue
    .line 235
    invoke-direct {p0, p1}, Lcom/google/cast/b$a;->a(Z)V

    return-void
.end method

.method private a(Z)V
    .locals 1

    .prologue
    .line 267
    iget-boolean v0, p0, Lcom/google/cast/b$a;->f:Z

    if-eq v0, p1, :cond_0

    .line 268
    iput-boolean p1, p0, Lcom/google/cast/b$a;->f:Z

    .line 269
    invoke-direct {p0}, Lcom/google/cast/b$a;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 270
    iget-object v0, p0, Lcom/google/cast/b$a;->c:Lcom/google/cast/b;

    invoke-static {v0}, Lcom/google/cast/b;->b(Lcom/google/cast/b;)V

    .line 273
    :cond_0
    return-void
.end method

.method private b(Z)Z
    .locals 1

    .prologue
    .line 300
    iget-boolean v0, p0, Lcom/google/cast/b$a;->g:Z

    if-eq v0, p1, :cond_0

    .line 301
    iput-boolean p1, p0, Lcom/google/cast/b$a;->g:Z

    .line 302
    invoke-direct {p0}, Lcom/google/cast/b$a;->c()Z

    move-result v0

    .line 305
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c()Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 313
    iget-boolean v0, p0, Lcom/google/cast/b$a;->f:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/cast/b$a;->g:Z

    if-eqz v0, :cond_1

    :cond_0
    move v0, v2

    .line 314
    :goto_0
    iget-object v3, p0, Lcom/google/cast/b$a;->c:Lcom/google/cast/b;

    invoke-static {v3}, Lcom/google/cast/b;->c(Lcom/google/cast/b;)Ljava/util/Map;

    move-result-object v3

    iget-object v4, p0, Lcom/google/cast/b$a;->a:Lcom/google/cast/CastDevice;

    invoke-virtual {v4}, Lcom/google/cast/CastDevice;->getDeviceId()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eq v0, v3, :cond_3

    .line 315
    if-eqz v0, :cond_2

    .line 316
    iget-object v0, p0, Lcom/google/cast/b$a;->c:Lcom/google/cast/b;

    iget-object v1, p0, Lcom/google/cast/b$a;->a:Lcom/google/cast/CastDevice;

    invoke-static {v0, v1}, Lcom/google/cast/b;->a(Lcom/google/cast/b;Lcom/google/cast/CastDevice;)V

    .line 322
    :goto_1
    return v2

    :cond_1
    move v0, v1

    .line 313
    goto :goto_0

    .line 318
    :cond_2
    iget-object v0, p0, Lcom/google/cast/b$a;->c:Lcom/google/cast/b;

    iget-object v1, p0, Lcom/google/cast/b$a;->a:Lcom/google/cast/CastDevice;

    invoke-static {v0, v1}, Lcom/google/cast/b;->b(Lcom/google/cast/b;Lcom/google/cast/CastDevice;)V

    goto :goto_1

    :cond_3
    move v2, v1

    .line 322
    goto :goto_1
.end method


# virtual methods
.method final a()I
    .locals 1

    .prologue
    .line 263
    iget v0, p0, Lcom/google/cast/b$a;->d:I

    return v0
.end method

.method protected final a(D)V
    .locals 7

    .prologue
    .line 253
    const-wide/high16 v0, 0x4034000000000000L    # 20.0

    mul-double/2addr v0, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-int v0, v0

    .line 254
    invoke-static {}, Lcom/google/cast/b;->a()Lcom/google/cast/Logger;

    move-result-object v1

    const-string v2, "setCurrentVolume to %d, was %d"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget v5, p0, Lcom/google/cast/b$a;->d:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 256
    iget v1, p0, Lcom/google/cast/b$a;->d:I

    if-eq v0, v1, :cond_0

    .line 257
    iput v0, p0, Lcom/google/cast/b$a;->d:I

    .line 258
    iget-object v0, p0, Lcom/google/cast/b$a;->c:Lcom/google/cast/b;

    invoke-static {v0}, Lcom/google/cast/b;->b(Lcom/google/cast/b;)V

    .line 260
    :cond_0
    return-void
.end method

.method protected final a(I)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 276
    .line 278
    if-ne p1, v1, :cond_3

    move v0, v1

    .line 279
    :goto_0
    iget-boolean v3, p0, Lcom/google/cast/b$a;->e:Z

    if-eq v0, v3, :cond_5

    .line 280
    iput-boolean v0, p0, Lcom/google/cast/b$a;->e:Z

    move v0, v1

    .line 284
    :goto_1
    if-eq p1, v1, :cond_0

    const/4 v3, 0x2

    if-ne p1, v3, :cond_1

    :cond_0
    move v2, v1

    .line 286
    :cond_1
    invoke-direct {p0, v2}, Lcom/google/cast/b$a;->b(Z)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 290
    :goto_2
    if-eqz v1, :cond_2

    .line 291
    iget-object v0, p0, Lcom/google/cast/b$a;->c:Lcom/google/cast/b;

    invoke-static {v0}, Lcom/google/cast/b;->b(Lcom/google/cast/b;)V

    .line 293
    :cond_2
    return-void

    :cond_3
    move v0, v2

    .line 278
    goto :goto_0

    :cond_4
    move v1, v0

    goto :goto_2

    :cond_5
    move v0, v2

    goto :goto_1
.end method

.method final b()Z
    .locals 1

    .prologue
    .line 326
    iget-boolean v0, p0, Lcom/google/cast/b$a;->e:Z

    return v0
.end method

.method public onRelease()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 331
    invoke-static {}, Lcom/google/cast/b;->a()Lcom/google/cast/Logger;

    move-result-object v0

    const-string v1, "Controller released"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/cast/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 332
    invoke-direct {p0, v3}, Lcom/google/cast/b$a;->b(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 333
    iget-object v0, p0, Lcom/google/cast/b$a;->c:Lcom/google/cast/b;

    invoke-static {v0}, Lcom/google/cast/b;->b(Lcom/google/cast/b;)V

    .line 335
    :cond_0
    iget-object v0, p0, Lcom/google/cast/b$a;->c:Lcom/google/cast/b;

    invoke-static {v0}, Lcom/google/cast/b;->a(Lcom/google/cast/b;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/google/cast/b$a;->a:Lcom/google/cast/CastDevice;

    invoke-virtual {v1}, Lcom/google/cast/CastDevice;->getDeviceId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 336
    return-void
.end method
