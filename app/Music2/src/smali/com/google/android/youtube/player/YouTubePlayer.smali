.class public interface abstract Lcom/google/android/youtube/player/YouTubePlayer;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;,
        Lcom/google/android/youtube/player/YouTubePlayer$PlayerStateChangeListener;,
        Lcom/google/android/youtube/player/YouTubePlayer$OnInitializedListener;,
        Lcom/google/android/youtube/player/YouTubePlayer$Provider;
    }
.end annotation


# virtual methods
.method public abstract addFullscreenControlFlag(I)V
.end method

.method public abstract loadVideo(Ljava/lang/String;)V
.end method

.method public abstract setPlayerStateChangeListener(Lcom/google/android/youtube/player/YouTubePlayer$PlayerStateChangeListener;)V
.end method

.method public abstract setShowFullscreenButton(Z)V
.end method
