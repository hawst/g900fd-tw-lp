.class public Lcom/google/android/play/playperf/measurements/PollingTimer;
.super Ljava/lang/Object;
.source "PollingTimer.java"


# instance fields
.field private final mPollerSet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/play/playperf/measurements/Poller;",
            ">;"
        }
    .end annotation
.end field

.field private final mPollingPeriodMillis:J

.field private mStartTime:J

.field private mStopped:Z

.field private final mSync:Ljava/lang/Object;

.field private final mTimer:Ljava/util/Timer;

.field private final mTimerTask:Ljava/util/TimerTask;


# direct methods
.method public varargs constructor <init>(J[Lcom/google/android/play/playperf/measurements/Poller;)V
    .locals 3
    .param p1, "pollingPeriodMillis"    # J
    .param p3, "pollers"    # [Lcom/google/android/play/playperf/measurements/Poller;

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/play/playperf/measurements/PollingTimer;->mSync:Ljava/lang/Object;

    .line 20
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/play/playperf/measurements/PollingTimer;->mPollerSet:Ljava/util/Set;

    .line 21
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/play/playperf/measurements/PollingTimer;->mStopped:Z

    .line 28
    iget-object v0, p0, Lcom/google/android/play/playperf/measurements/PollingTimer;->mPollerSet:Ljava/util/Set;

    invoke-static {p3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 29
    iput-wide p1, p0, Lcom/google/android/play/playperf/measurements/PollingTimer;->mPollingPeriodMillis:J

    .line 30
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/google/android/play/playperf/measurements/PollingTimer;->mTimer:Ljava/util/Timer;

    .line 31
    new-instance v0, Lcom/google/android/play/playperf/measurements/PollingTimer$1;

    invoke-direct {v0, p0}, Lcom/google/android/play/playperf/measurements/PollingTimer$1;-><init>(Lcom/google/android/play/playperf/measurements/PollingTimer;)V

    iput-object v0, p0, Lcom/google/android/play/playperf/measurements/PollingTimer;->mTimerTask:Ljava/util/TimerTask;

    .line 43
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/play/playperf/measurements/PollingTimer;)Ljava/util/Set;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/play/playperf/measurements/PollingTimer;

    .prologue
    .line 15
    iget-object v0, p0, Lcom/google/android/play/playperf/measurements/PollingTimer;->mPollerSet:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/play/playperf/measurements/PollingTimer;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/play/playperf/measurements/PollingTimer;

    .prologue
    .line 15
    iget-object v0, p0, Lcom/google/android/play/playperf/measurements/PollingTimer;->mSync:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/play/playperf/measurements/PollingTimer;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/play/playperf/measurements/PollingTimer;

    .prologue
    .line 15
    iget-boolean v0, p0, Lcom/google/android/play/playperf/measurements/PollingTimer;->mStopped:Z

    return v0
.end method


# virtual methods
.method public start()V
    .locals 6

    .prologue
    .line 49
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/play/playperf/measurements/PollingTimer;->mStartTime:J

    .line 50
    iget-object v0, p0, Lcom/google/android/play/playperf/measurements/PollingTimer;->mTimer:Ljava/util/Timer;

    iget-object v1, p0, Lcom/google/android/play/playperf/measurements/PollingTimer;->mTimerTask:Ljava/util/TimerTask;

    const-wide/16 v2, 0x0

    iget-wide v4, p0, Lcom/google/android/play/playperf/measurements/PollingTimer;->mPollingPeriodMillis:J

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->scheduleAtFixedRate(Ljava/util/TimerTask;JJ)V

    .line 51
    return-void
.end method

.method public stop()V
    .locals 2

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/play/playperf/measurements/PollingTimer;->mTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 60
    iget-object v1, p0, Lcom/google/android/play/playperf/measurements/PollingTimer;->mSync:Ljava/lang/Object;

    monitor-enter v1

    .line 61
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/play/playperf/measurements/PollingTimer;->mStopped:Z

    .line 62
    monitor-exit v1

    .line 63
    return-void

    .line 62
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
