.class Lcom/google/android/play/drawer/PlayDrawerAdapter$4;
.super Ljava/lang/Object;
.source "PlayDrawerAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/play/drawer/PlayDrawerAdapter;->getProfileContainer(Landroid/view/View;Landroid/view/ViewGroup;I)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/play/drawer/PlayDrawerAdapter;


# direct methods
.method constructor <init>(Lcom/google/android/play/drawer/PlayDrawerAdapter;)V
    .locals 0

    .prologue
    .line 390
    iput-object p1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$4;->this$0:Lcom/google/android/play/drawer/PlayDrawerAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 397
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 399
    .local v0, "pos":Ljava/lang/Integer;
    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$4;->this$0:Lcom/google/android/play/drawer/PlayDrawerAdapter;

    # getter for: Lcom/google/android/play/drawer/PlayDrawerAdapter;->mPlayDrawerContentClickListener:Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerContentClickListener;
    invoke-static {v1}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->access$600(Lcom/google/android/play/drawer/PlayDrawerAdapter;)Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerContentClickListener;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$4;->this$0:Lcom/google/android/play/drawer/PlayDrawerAdapter;

    # getter for: Lcom/google/android/play/drawer/PlayDrawerAdapter;->mNonCurrentAccounts:[Landroid/accounts/Account;
    invoke-static {v2}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->access$800(Lcom/google/android/play/drawer/PlayDrawerAdapter;)[Landroid/accounts/Account;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    aget-object v2, v2, v3

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v1, v2}, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerContentClickListener;->onSecondaryAccountClicked(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 401
    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$4;->this$0:Lcom/google/android/play/drawer/PlayDrawerAdapter;

    # getter for: Lcom/google/android/play/drawer/PlayDrawerAdapter;->mPlayDrawerLayout:Lcom/google/android/play/drawer/PlayDrawerLayout;
    invoke-static {v1}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->access$700(Lcom/google/android/play/drawer/PlayDrawerAdapter;)Lcom/google/android/play/drawer/PlayDrawerLayout;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/play/drawer/PlayDrawerLayout;->closeDrawer()V

    .line 403
    :cond_0
    return-void
.end method
