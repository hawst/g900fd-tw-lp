.class public final Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;
.super Lcom/google/protobuf/nano/MessageNano;
.source "ClientAnalytics.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/play/analytics/ClientAnalytics;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ClientInfo"
.end annotation


# instance fields
.field public androidClientInfo:Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;

.field public clientType:I

.field public desktopClientInfo:Lcom/google/android/play/analytics/ClientAnalytics$DesktopClientInfo;

.field public iosClientInfo:Lcom/google/android/play/analytics/ClientAnalytics$IosClientInfo;

.field public playCeClientInfo:Lcom/google/android/play/analytics/ClientAnalytics$PlayCeClientInfo;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1458
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 1459
    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;->clear()Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;

    .line 1460
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1463
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;->clientType:I

    .line 1464
    iput-object v1, p0, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;->androidClientInfo:Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;

    .line 1465
    iput-object v1, p0, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;->desktopClientInfo:Lcom/google/android/play/analytics/ClientAnalytics$DesktopClientInfo;

    .line 1466
    iput-object v1, p0, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;->iosClientInfo:Lcom/google/android/play/analytics/ClientAnalytics$IosClientInfo;

    .line 1467
    iput-object v1, p0, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;->playCeClientInfo:Lcom/google/android/play/analytics/ClientAnalytics$PlayCeClientInfo;

    .line 1468
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;->cachedSize:I

    .line 1469
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 1495
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 1496
    .local v0, "size":I
    iget v1, p0, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;->clientType:I

    if-eqz v1, :cond_0

    .line 1497
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;->clientType:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1500
    :cond_0
    iget-object v1, p0, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;->androidClientInfo:Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;

    if-eqz v1, :cond_1

    .line 1501
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;->androidClientInfo:Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1504
    :cond_1
    iget-object v1, p0, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;->desktopClientInfo:Lcom/google/android/play/analytics/ClientAnalytics$DesktopClientInfo;

    if-eqz v1, :cond_2

    .line 1505
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;->desktopClientInfo:Lcom/google/android/play/analytics/ClientAnalytics$DesktopClientInfo;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1508
    :cond_2
    iget-object v1, p0, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;->iosClientInfo:Lcom/google/android/play/analytics/ClientAnalytics$IosClientInfo;

    if-eqz v1, :cond_3

    .line 1509
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;->iosClientInfo:Lcom/google/android/play/analytics/ClientAnalytics$IosClientInfo;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1512
    :cond_3
    iget-object v1, p0, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;->playCeClientInfo:Lcom/google/android/play/analytics/ClientAnalytics$PlayCeClientInfo;

    if-eqz v1, :cond_4

    .line 1513
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;->playCeClientInfo:Lcom/google/android/play/analytics/ClientAnalytics$PlayCeClientInfo;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1516
    :cond_4
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1524
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 1525
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 1529
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1530
    :sswitch_0
    return-object p0

    .line 1535
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 1536
    .local v1, "value":I
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 1543
    :pswitch_0
    iput v1, p0, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;->clientType:I

    goto :goto_0

    .line 1549
    .end local v1    # "value":I
    :sswitch_2
    iget-object v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;->androidClientInfo:Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;

    if-nez v2, :cond_1

    .line 1550
    new-instance v2, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;

    invoke-direct {v2}, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;-><init>()V

    iput-object v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;->androidClientInfo:Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;

    .line 1552
    :cond_1
    iget-object v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;->androidClientInfo:Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1556
    :sswitch_3
    iget-object v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;->desktopClientInfo:Lcom/google/android/play/analytics/ClientAnalytics$DesktopClientInfo;

    if-nez v2, :cond_2

    .line 1557
    new-instance v2, Lcom/google/android/play/analytics/ClientAnalytics$DesktopClientInfo;

    invoke-direct {v2}, Lcom/google/android/play/analytics/ClientAnalytics$DesktopClientInfo;-><init>()V

    iput-object v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;->desktopClientInfo:Lcom/google/android/play/analytics/ClientAnalytics$DesktopClientInfo;

    .line 1559
    :cond_2
    iget-object v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;->desktopClientInfo:Lcom/google/android/play/analytics/ClientAnalytics$DesktopClientInfo;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1563
    :sswitch_4
    iget-object v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;->iosClientInfo:Lcom/google/android/play/analytics/ClientAnalytics$IosClientInfo;

    if-nez v2, :cond_3

    .line 1564
    new-instance v2, Lcom/google/android/play/analytics/ClientAnalytics$IosClientInfo;

    invoke-direct {v2}, Lcom/google/android/play/analytics/ClientAnalytics$IosClientInfo;-><init>()V

    iput-object v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;->iosClientInfo:Lcom/google/android/play/analytics/ClientAnalytics$IosClientInfo;

    .line 1566
    :cond_3
    iget-object v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;->iosClientInfo:Lcom/google/android/play/analytics/ClientAnalytics$IosClientInfo;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1570
    :sswitch_5
    iget-object v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;->playCeClientInfo:Lcom/google/android/play/analytics/ClientAnalytics$PlayCeClientInfo;

    if-nez v2, :cond_4

    .line 1571
    new-instance v2, Lcom/google/android/play/analytics/ClientAnalytics$PlayCeClientInfo;

    invoke-direct {v2}, Lcom/google/android/play/analytics/ClientAnalytics$PlayCeClientInfo;-><init>()V

    iput-object v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;->playCeClientInfo:Lcom/google/android/play/analytics/ClientAnalytics$PlayCeClientInfo;

    .line 1573
    :cond_4
    iget-object v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;->playCeClientInfo:Lcom/google/android/play/analytics/ClientAnalytics$PlayCeClientInfo;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1525
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch

    .line 1536
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1418
    invoke-virtual {p0, p1}, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1475
    iget v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;->clientType:I

    if-eqz v0, :cond_0

    .line 1476
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;->clientType:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1478
    :cond_0
    iget-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;->androidClientInfo:Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;

    if-eqz v0, :cond_1

    .line 1479
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;->androidClientInfo:Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1481
    :cond_1
    iget-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;->desktopClientInfo:Lcom/google/android/play/analytics/ClientAnalytics$DesktopClientInfo;

    if-eqz v0, :cond_2

    .line 1482
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;->desktopClientInfo:Lcom/google/android/play/analytics/ClientAnalytics$DesktopClientInfo;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1484
    :cond_2
    iget-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;->iosClientInfo:Lcom/google/android/play/analytics/ClientAnalytics$IosClientInfo;

    if-eqz v0, :cond_3

    .line 1485
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;->iosClientInfo:Lcom/google/android/play/analytics/ClientAnalytics$IosClientInfo;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1487
    :cond_3
    iget-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;->playCeClientInfo:Lcom/google/android/play/analytics/ClientAnalytics$PlayCeClientInfo;

    if-eqz v0, :cond_4

    .line 1488
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;->playCeClientInfo:Lcom/google/android/play/analytics/ClientAnalytics$PlayCeClientInfo;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1490
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 1491
    return-void
.end method
