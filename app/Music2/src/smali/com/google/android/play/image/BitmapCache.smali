.class public Lcom/google/android/play/image/BitmapCache;
.super Ljava/lang/Object;
.source "BitmapCache.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/play/image/BitmapCache$BitmapListLruCache;,
        Lcom/google/android/play/image/BitmapCache$BitmapCacheEntry;
    }
.end annotation


# instance fields
.field private mLruCache:Lcom/google/android/play/image/BitmapCache$BitmapListLruCache;


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1, "maxSizeInBytes"    # I

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    new-instance v0, Lcom/google/android/play/image/BitmapCache$BitmapListLruCache;

    invoke-direct {v0, p0, p1}, Lcom/google/android/play/image/BitmapCache$BitmapListLruCache;-><init>(Lcom/google/android/play/image/BitmapCache;I)V

    iput-object v0, p0, Lcom/google/android/play/image/BitmapCache;->mLruCache:Lcom/google/android/play/image/BitmapCache$BitmapListLruCache;

    .line 25
    return-void
.end method


# virtual methods
.method public get(Ljava/lang/String;II)Lcom/google/android/play/image/BitmapCache$BitmapCacheEntry;
    .locals 8
    .param p1, "cacheKey"    # Ljava/lang/String;
    .param p2, "requestedWidth"    # I
    .param p3, "requestedHeight"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v6, 0x0

    .line 88
    iget-object v7, p0, Lcom/google/android/play/image/BitmapCache;->mLruCache:Lcom/google/android/play/image/BitmapCache$BitmapListLruCache;

    invoke-virtual {v7, p1}, Lcom/google/android/play/image/BitmapCache$BitmapListLruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 90
    .local v0, "bitmapList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/play/image/BitmapCache$BitmapCacheEntry;>;"
    if-nez v0, :cond_1

    .line 91
    const/4 v3, 0x0

    .line 121
    :cond_0
    :goto_0
    return-object v3

    .line 95
    :cond_1
    if-eqz p2, :cond_6

    move v2, v1

    .line 96
    .local v2, "checkWidth":Z
    :goto_1
    if-eqz p2, :cond_7

    .line 101
    .local v1, "checkHeight":Z
    :goto_2
    const/4 v5, 0x0

    .line 102
    .local v5, "smallestLargerBitmap":Lcom/google/android/play/image/BitmapCache$BitmapCacheEntry;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_3
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v6

    if-ge v4, v6, :cond_8

    .line 103
    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/play/image/BitmapCache$BitmapCacheEntry;

    .line 106
    .local v3, "entry":Lcom/google/android/play/image/BitmapCache$BitmapCacheEntry;
    iget v6, v3, Lcom/google/android/play/image/BitmapCache$BitmapCacheEntry;->requestedWidth:I

    if-ne v6, p2, :cond_2

    iget v6, v3, Lcom/google/android/play/image/BitmapCache$BitmapCacheEntry;->requestedHeight:I

    if-eq v6, p3, :cond_0

    .line 109
    :cond_2
    if-nez v5, :cond_5

    .line 112
    if-eqz v2, :cond_3

    iget-object v6, v3, Lcom/google/android/play/image/BitmapCache$BitmapCacheEntry;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    if-lt v6, p2, :cond_5

    :cond_3
    if-eqz v1, :cond_4

    iget-object v6, v3, Lcom/google/android/play/image/BitmapCache$BitmapCacheEntry;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    if-lt v6, p3, :cond_5

    .line 114
    :cond_4
    move-object v5, v3

    .line 102
    :cond_5
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .end local v1    # "checkHeight":Z
    .end local v2    # "checkWidth":Z
    .end local v3    # "entry":Lcom/google/android/play/image/BitmapCache$BitmapCacheEntry;
    .end local v4    # "i":I
    .end local v5    # "smallestLargerBitmap":Lcom/google/android/play/image/BitmapCache$BitmapCacheEntry;
    :cond_6
    move v2, v6

    .line 95
    goto :goto_1

    .restart local v2    # "checkWidth":Z
    :cond_7
    move v1, v6

    .line 96
    goto :goto_2

    .line 121
    .restart local v1    # "checkHeight":Z
    .restart local v4    # "i":I
    .restart local v5    # "smallestLargerBitmap":Lcom/google/android/play/image/BitmapCache$BitmapCacheEntry;
    :cond_8
    if-eqz v5, :cond_9

    .end local v5    # "smallestLargerBitmap":Lcom/google/android/play/image/BitmapCache$BitmapCacheEntry;
    :goto_4
    move-object v3, v5

    goto :goto_0

    .restart local v5    # "smallestLargerBitmap":Lcom/google/android/play/image/BitmapCache$BitmapCacheEntry;
    :cond_9
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/play/image/BitmapCache$BitmapCacheEntry;

    move-object v5, v6

    goto :goto_4
.end method

.method public put(Ljava/lang/String;IILandroid/graphics/Bitmap;)V
    .locals 6
    .param p1, "cacheKey"    # Ljava/lang/String;
    .param p2, "requestedWidth"    # I
    .param p3, "requestedHeight"    # I
    .param p4, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 42
    iget-object v5, p0, Lcom/google/android/play/image/BitmapCache;->mLruCache:Lcom/google/android/play/image/BitmapCache$BitmapListLruCache;

    invoke-virtual {v5, p1}, Lcom/google/android/play/image/BitmapCache$BitmapListLruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 43
    .local v0, "bitmapList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/play/image/BitmapCache$BitmapCacheEntry;>;"
    if-nez v0, :cond_0

    .line 44
    new-instance v0, Ljava/util/ArrayList;

    .end local v0    # "bitmapList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/play/image/BitmapCache$BitmapCacheEntry;>;"
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 47
    .restart local v0    # "bitmapList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/play/image/BitmapCache$BitmapCacheEntry;>;"
    :cond_0
    const/4 v3, 0x0

    .line 48
    .local v3, "insertIndex":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v2, v5, :cond_1

    .line 49
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/play/image/BitmapCache$BitmapCacheEntry;

    iget-object v5, v5, Lcom/google/android/play/image/BitmapCache$BitmapCacheEntry;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    .line 50
    .local v1, "currentWidth":I
    invoke-virtual {p4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    .line 51
    .local v4, "newWidth":I
    if-lt v1, v4, :cond_3

    .line 52
    if-ne v1, v4, :cond_2

    .line 54
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 55
    move v3, v2

    .line 62
    .end local v1    # "currentWidth":I
    .end local v4    # "newWidth":I
    :cond_1
    :goto_1
    new-instance v5, Lcom/google/android/play/image/BitmapCache$BitmapCacheEntry;

    invoke-direct {v5, p4, p2, p3}, Lcom/google/android/play/image/BitmapCache$BitmapCacheEntry;-><init>(Landroid/graphics/Bitmap;II)V

    invoke-virtual {v0, v3, v5}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 63
    iget-object v5, p0, Lcom/google/android/play/image/BitmapCache;->mLruCache:Lcom/google/android/play/image/BitmapCache$BitmapListLruCache;

    invoke-virtual {v5, p1, v0}, Lcom/google/android/play/image/BitmapCache$BitmapListLruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    return-void

    .line 57
    .restart local v1    # "currentWidth":I
    .restart local v4    # "newWidth":I
    :cond_2
    add-int/lit8 v3, v2, 0x1

    .line 59
    goto :goto_1

    .line 48
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method
