.class public Lcom/google/android/play/dfe/api/PlayDfeApiImpl;
.super Ljava/lang/Object;
.source "PlayDfeApiImpl.java"

# interfaces
.implements Lcom/google/android/play/dfe/api/PlayDfeApi;


# static fields
.field private static final PLUS_PROFILE_BG_BACKOFF_MULT:F

.field private static final PLUS_PROFILE_BG_MAX_RETRIES:I

.field private static final PLUS_PROFILE_BG_TIMEOUT_MS:I


# instance fields
.field private final mApiContext:Lcom/google/android/play/dfe/api/PlayDfeApiContext;

.field private final mQueue:Lcom/android/volley/RequestQueue;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/google/android/play/utils/config/PlayG;->plusProfileBgTimeoutMs:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sput v0, Lcom/google/android/play/dfe/api/PlayDfeApiImpl;->PLUS_PROFILE_BG_TIMEOUT_MS:I

    .line 23
    sget-object v0, Lcom/google/android/play/utils/config/PlayG;->plusProfileBgMaxRetries:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sput v0, Lcom/google/android/play/dfe/api/PlayDfeApiImpl;->PLUS_PROFILE_BG_MAX_RETRIES:I

    .line 27
    sget-object v0, Lcom/google/android/play/utils/config/PlayG;->plusProfileBgBackoffMult:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    sput v0, Lcom/google/android/play/dfe/api/PlayDfeApiImpl;->PLUS_PROFILE_BG_BACKOFF_MULT:F

    return-void
.end method

.method public constructor <init>(Lcom/android/volley/RequestQueue;Lcom/google/android/play/dfe/api/PlayDfeApiContext;)V
    .locals 0
    .param p1, "queue"    # Lcom/android/volley/RequestQueue;
    .param p2, "apiContext"    # Lcom/google/android/play/dfe/api/PlayDfeApiContext;

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/google/android/play/dfe/api/PlayDfeApiImpl;->mQueue:Lcom/android/volley/RequestQueue;

    .line 35
    iput-object p2, p0, Lcom/google/android/play/dfe/api/PlayDfeApiImpl;->mApiContext:Lcom/google/android/play/dfe/api/PlayDfeApiContext;

    .line 36
    return-void
.end method


# virtual methods
.method public getPlusProfile(Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;Z)Lcom/android/volley/Request;
    .locals 6
    .param p2, "errorListener"    # Lcom/android/volley/Response$ErrorListener;
    .param p3, "isForegroundRequest"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            "Z)",
            "Lcom/android/volley/Request",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 56
    .local p1, "listener":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;>;"
    new-instance v0, Lcom/google/android/play/dfe/api/DfeRequest;

    sget-object v1, Lcom/google/android/play/dfe/api/PlayDfeApiImpl;->PLUS_PROFILE_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/play/dfe/api/PlayDfeApiImpl;->mApiContext:Lcom/google/android/play/dfe/api/PlayDfeApiContext;

    const-class v3, Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/play/dfe/api/DfeRequest;-><init>(Ljava/lang/String;Lcom/google/android/play/dfe/api/PlayDfeApiContext;Ljava/lang/Class;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 59
    .local v0, "request":Lcom/google/android/play/dfe/api/DfeRequest;, "Lcom/google/android/play/dfe/api/DfeRequest<Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;>;"
    if-nez p3, :cond_0

    .line 60
    new-instance v1, Lcom/google/android/play/dfe/api/DfeRetryPolicy;

    sget v2, Lcom/google/android/play/dfe/api/PlayDfeApiImpl;->PLUS_PROFILE_BG_TIMEOUT_MS:I

    sget v3, Lcom/google/android/play/dfe/api/PlayDfeApiImpl;->PLUS_PROFILE_BG_MAX_RETRIES:I

    sget v4, Lcom/google/android/play/dfe/api/PlayDfeApiImpl;->PLUS_PROFILE_BG_BACKOFF_MULT:F

    iget-object v5, p0, Lcom/google/android/play/dfe/api/PlayDfeApiImpl;->mApiContext:Lcom/google/android/play/dfe/api/PlayDfeApiContext;

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/play/dfe/api/DfeRetryPolicy;-><init>(IIFLcom/google/android/play/dfe/api/PlayDfeApiContext;)V

    invoke-virtual {v0, v1}, Lcom/google/android/play/dfe/api/DfeRequest;->setRetryPolicy(Lcom/android/volley/RetryPolicy;)Lcom/android/volley/Request;

    .line 63
    :cond_0
    iget-object v1, p0, Lcom/google/android/play/dfe/api/PlayDfeApiImpl;->mQueue:Lcom/android/volley/RequestQueue;

    invoke-virtual {v1, v0}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    move-result-object v1

    return-object v1
.end method
