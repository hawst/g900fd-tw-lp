.class public Lcom/google/android/play/playperf/measurements/remote/MeasurementService;
.super Landroid/app/IntentService;
.source "MeasurementService.java"


# static fields
.field private static final sFactoryMap:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/play/playperf/measurements/MeasurerFactory",
            "<*>;>;"
        }
    .end annotation
.end field

.field private static sInstance:Lcom/google/android/play/playperf/measurements/remote/MeasurementService;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lcom/google/android/play/playperf/measurements/remote/MeasurementService;->sFactoryMap:Ljava/util/concurrent/ConcurrentHashMap;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 41
    const-string v0, "MeasurementService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 42
    return-void
.end method

.method public static registerMeasurerFactory(Ljava/lang/String;Lcom/google/android/play/playperf/measurements/MeasurerFactory;)V
    .locals 3
    .param p0, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/play/playperf/measurements/MeasurerFactory",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 76
    .local p1, "factory":Lcom/google/android/play/playperf/measurements/MeasurerFactory;, "Lcom/google/android/play/playperf/measurements/MeasurerFactory<*>;"
    sget-object v0, Lcom/google/android/play/playperf/measurements/remote/MeasurementService;->sFactoryMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {p0}, Lcom/google/android/play/playperf/preconditions/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-static {p1}, Lcom/google/android/play/playperf/preconditions/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    const-string v0, "MeasurementService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " registered"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 78
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 9
    .param p1, "bindIntent"    # Landroid/content/Intent;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 93
    const-string v3, "com.google.android.play.playperf.BIND_MEASURER"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 94
    const-string v3, "FACTORY_NAME"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 95
    .local v0, "factoryName":Ljava/lang/String;
    const-string v3, "SECTION_NAME"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 96
    .local v2, "sectionName":Ljava/lang/String;
    const-string v3, "MeasurementService"

    const-string v4, "%s called with factory %s and section %s"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const-string v6, "com.google.android.play.playperf.BIND_MEASURER"

    aput-object v6, v5, v7

    aput-object v0, v5, v8

    const/4 v6, 0x2

    aput-object v2, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    sget-object v3, Lcom/google/android/play/playperf/measurements/remote/MeasurementService;->sFactoryMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v3, v0}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 99
    sget-object v3, Lcom/google/android/play/playperf/measurements/remote/MeasurementService;->sFactoryMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v3, v0}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/play/playperf/measurements/MeasurerFactory;

    invoke-interface {v3, v2}, Lcom/google/android/play/playperf/measurements/MeasurerFactory;->getMeasurerInstance(Ljava/lang/String;)Lcom/google/android/play/playperf/measurements/Measurer;

    move-result-object v1

    .line 101
    .local v1, "measurer":Lcom/google/android/play/playperf/measurements/Measurer;, "Lcom/google/android/play/playperf/measurements/Measurer<*>;"
    new-instance v3, Lcom/google/android/play/playperf/measurements/remote/MeasurerService;

    invoke-direct {v3, v1}, Lcom/google/android/play/playperf/measurements/remote/MeasurerService;-><init>(Lcom/google/android/play/playperf/measurements/Measurer;)V

    .line 108
    .end local v0    # "factoryName":Ljava/lang/String;
    .end local v1    # "measurer":Lcom/google/android/play/playperf/measurements/Measurer;, "Lcom/google/android/play/playperf/measurements/Measurer<*>;"
    .end local v2    # "sectionName":Ljava/lang/String;
    :goto_0
    return-object v3

    .line 103
    .restart local v0    # "factoryName":Ljava/lang/String;
    .restart local v2    # "sectionName":Ljava/lang/String;
    :cond_0
    new-instance v3, Lcom/google/android/play/playperf/exceptions/PlayPerfException;

    const-string v4, "factory %s does not exist"

    new-array v5, v8, [Ljava/lang/Object;

    aput-object v0, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/google/android/play/playperf/exceptions/PlayPerfException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 106
    .end local v0    # "factoryName":Ljava/lang/String;
    .end local v2    # "sectionName":Ljava/lang/String;
    :cond_1
    const-string v3, "com.google.android.play.playperf.BIND_PROCESS_CONTROL"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 107
    const-string v3, "MeasurementService"

    const-string v4, "%s called"

    new-array v5, v8, [Ljava/lang/Object;

    const-string v6, "com.google.android.play.playperf.BIND_PROCESS_CONTROL"

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 108
    new-instance v3, Lcom/google/android/play/playperf/measurements/remote/ProcessControlService;

    invoke-direct {v3}, Lcom/google/android/play/playperf/measurements/remote/ProcessControlService;-><init>()V

    goto :goto_0

    .line 110
    :cond_2
    new-instance v3, Lcom/google/android/play/playperf/exceptions/PlayPerfException;

    const-string v4, "%s is not a valid action"

    new-array v5, v8, [Ljava/lang/Object;

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/google/android/play/playperf/exceptions/PlayPerfException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method public onCreate()V
    .locals 3

    .prologue
    .line 49
    invoke-super {p0}, Landroid/app/IntentService;->onCreate()V

    .line 50
    sput-object p0, Lcom/google/android/play/playperf/measurements/remote/MeasurementService;->sInstance:Lcom/google/android/play/playperf/measurements/remote/MeasurementService;

    .line 51
    const-string v0, "MeasurementService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "service created "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 52
    return-void
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    .line 59
    const-string v0, "MeasurementService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "destroying "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 60
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/play/playperf/measurements/remote/MeasurementService;->sInstance:Lcom/google/android/play/playperf/measurements/remote/MeasurementService;

    .line 61
    invoke-super {p0}, Landroid/app/IntentService;->onDestroy()V

    .line 62
    return-void
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 0
    .param p1, "arg0"    # Landroid/content/Intent;

    .prologue
    .line 86
    return-void
.end method
