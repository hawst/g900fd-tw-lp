.class public Lcom/google/android/play/drawer/PlayDrawerLayout;
.super Landroid/support/v4/widget/DrawerLayout;
.source "PlayDrawerLayout.java"

# interfaces
.implements Landroid/support/v4/widget/DrawerLayout$DrawerListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerDownloadSwitchConfig;,
        Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;,
        Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;,
        Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerContentClickListener;
    }
.end annotation


# instance fields
.field private mDrawerAdapter:Lcom/google/android/play/drawer/PlayDrawerAdapter;

.field private mDrawerList:Landroid/widget/ListView;

.field private mDrawerListener:Landroid/support/v4/widget/DrawerLayout$DrawerListener;

.field private mDrawerSlideOffset:F

.field private mDrawerState:I

.field private mDrawerToggle:Landroid/support/v7/app/ActionBarDrawerToggle;

.field private mIsConfigured:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 286
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/play/drawer/PlayDrawerLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 287
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 290
    invoke-direct {p0, p1, p2}, Landroid/support/v4/widget/DrawerLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 292
    invoke-static {p1}, Lcom/google/android/play/utils/PlayUtils;->useLtr(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x3

    .line 293
    .local v0, "shadowGravity":I
    :goto_0
    sget v1, Lcom/google/android/play/R$drawable;->drawer_shadow:I

    invoke-virtual {p0, v1, v0}, Lcom/google/android/play/drawer/PlayDrawerLayout;->setDrawerShadow(II)V

    .line 297
    invoke-super {p0, p0}, Landroid/support/v4/widget/DrawerLayout;->setDrawerListener(Landroid/support/v4/widget/DrawerLayout$DrawerListener;)V

    .line 298
    return-void

    .line 292
    .end local v0    # "shadowGravity":I
    :cond_0
    const/4 v0, 0x5

    goto :goto_0
.end method

.method private checkIsConfigured()V
    .locals 2

    .prologue
    .line 408
    iget-boolean v0, p0, Lcom/google/android/play/drawer/PlayDrawerLayout;->mIsConfigured:Z

    if-nez v0, :cond_0

    .line 409
    const-string v0, "Play Drawer configure was not called"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/play/utils/PlayCommonLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 411
    :cond_0
    return-void
.end method


# virtual methods
.method public closeDrawer()V
    .locals 1

    .prologue
    .line 448
    invoke-direct {p0}, Lcom/google/android/play/drawer/PlayDrawerLayout;->checkIsConfigured()V

    .line 449
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerLayout;->mDrawerList:Landroid/widget/ListView;

    invoke-virtual {p0, v0}, Lcom/google/android/play/drawer/PlayDrawerLayout;->isDrawerOpen(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 450
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerLayout;->mDrawerList:Landroid/widget/ListView;

    invoke-virtual {p0, v0}, Lcom/google/android/play/drawer/PlayDrawerLayout;->closeDrawer(Landroid/view/View;)V

    .line 452
    :cond_0
    return-void
.end method

.method public configure(Landroid/app/Activity;ZIZLcom/google/android/play/dfe/api/PlayDfeApiProvider;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerContentClickListener;)V
    .locals 8
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "isAccountListExpanded"    # Z
    .param p3, "actionBarHeight"    # I
    .param p4, "isFloating"    # Z
    .param p5, "playDfeApiProvider"    # Lcom/google/android/play/dfe/api/PlayDfeApiProvider;
    .param p6, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;
    .param p7, "playDrawerContentClickListener"    # Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerContentClickListener;

    .prologue
    .line 336
    iget-boolean v0, p0, Lcom/google/android/play/drawer/PlayDrawerLayout;->mIsConfigured:Z

    if-eqz v0, :cond_0

    .line 337
    const-string v0, "PlayDrawer is already configured"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/play/utils/PlayCommonLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 339
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/drawer/PlayDrawerLayout;->mIsConfigured:Z

    .line 341
    invoke-virtual {p0, p3, p4}, Lcom/google/android/play/drawer/PlayDrawerLayout;->setActionBarHeight(IZ)V

    .line 343
    new-instance v0, Lcom/google/android/play/drawer/PlayDrawerAdapter;

    iget-object v7, p0, Lcom/google/android/play/drawer/PlayDrawerLayout;->mDrawerList:Landroid/widget/ListView;

    move-object v1, p1

    move v2, p2

    move-object v3, p7

    move-object v4, p5

    move-object v5, p6

    move-object v6, p0

    invoke-direct/range {v0 .. v7}, Lcom/google/android/play/drawer/PlayDrawerAdapter;-><init>(Landroid/content/Context;ZLcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerContentClickListener;Lcom/google/android/play/dfe/api/PlayDfeApiProvider;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/play/drawer/PlayDrawerLayout;Landroid/widget/ListView;)V

    iput-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerLayout;->mDrawerAdapter:Lcom/google/android/play/drawer/PlayDrawerAdapter;

    .line 347
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerLayout;->mDrawerList:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerLayout;->mDrawerAdapter:Lcom/google/android/play/drawer/PlayDrawerAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 349
    new-instance v0, Landroid/support/v7/app/ActionBarDrawerToggle;

    sget v1, Lcom/google/android/play/R$string;->play_drawer_open:I

    sget v2, Lcom/google/android/play/R$string;->play_drawer_close:I

    invoke-direct {v0, p1, p0, v1, v2}, Landroid/support/v7/app/ActionBarDrawerToggle;-><init>(Landroid/app/Activity;Landroid/support/v4/widget/DrawerLayout;II)V

    iput-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerLayout;->mDrawerToggle:Landroid/support/v7/app/ActionBarDrawerToggle;

    .line 354
    const v0, 0x800003

    sget v1, Lcom/google/android/play/R$string;->play_drawer_title:I

    invoke-virtual {p1, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/play/drawer/PlayDrawerLayout;->setDrawerTitle(ILjava/lang/CharSequence;)V

    .line 355
    return-void
.end method

.method public isDrawerOpen()Z
    .locals 1

    .prologue
    .line 418
    invoke-direct {p0}, Lcom/google/android/play/drawer/PlayDrawerLayout;->checkIsConfigured()V

    .line 419
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerLayout;->mDrawerList:Landroid/widget/ListView;

    invoke-virtual {p0, v0}, Lcom/google/android/play/drawer/PlayDrawerLayout;->isDrawerOpen(Landroid/view/View;)Z

    move-result v0

    return v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 509
    invoke-super {p0, p1}, Landroid/support/v4/widget/DrawerLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 510
    iget-boolean v0, p0, Lcom/google/android/play/drawer/PlayDrawerLayout;->mIsConfigured:Z

    if-eqz v0, :cond_0

    .line 511
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerLayout;->mDrawerToggle:Landroid/support/v7/app/ActionBarDrawerToggle;

    invoke-virtual {v0, p1}, Landroid/support/v7/app/ActionBarDrawerToggle;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 513
    :cond_0
    return-void
.end method

.method public onDrawerClosed(Landroid/view/View;)V
    .locals 1
    .param p1, "drawerView"    # Landroid/view/View;

    .prologue
    .line 533
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerLayout;->mDrawerToggle:Landroid/support/v7/app/ActionBarDrawerToggle;

    invoke-virtual {v0, p1}, Landroid/support/v7/app/ActionBarDrawerToggle;->onDrawerClosed(Landroid/view/View;)V

    .line 534
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerLayout;->mDrawerAdapter:Lcom/google/android/play/drawer/PlayDrawerAdapter;

    invoke-virtual {v0}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->collapseAccountListIfNeeded()V

    .line 536
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerLayout;->mDrawerListener:Landroid/support/v4/widget/DrawerLayout$DrawerListener;

    if-eqz v0, :cond_0

    .line 537
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerLayout;->mDrawerListener:Landroid/support/v4/widget/DrawerLayout$DrawerListener;

    invoke-interface {v0, p1}, Landroid/support/v4/widget/DrawerLayout$DrawerListener;->onDrawerClosed(Landroid/view/View;)V

    .line 539
    :cond_0
    return-void
.end method

.method public onDrawerOpened(Landroid/view/View;)V
    .locals 1
    .param p1, "drawerView"    # Landroid/view/View;

    .prologue
    .line 546
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerLayout;->mDrawerToggle:Landroid/support/v7/app/ActionBarDrawerToggle;

    invoke-virtual {v0, p1}, Landroid/support/v7/app/ActionBarDrawerToggle;->onDrawerOpened(Landroid/view/View;)V

    .line 548
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerLayout;->mDrawerListener:Landroid/support/v4/widget/DrawerLayout$DrawerListener;

    if-eqz v0, :cond_0

    .line 549
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerLayout;->mDrawerListener:Landroid/support/v4/widget/DrawerLayout$DrawerListener;

    invoke-interface {v0, p1}, Landroid/support/v4/widget/DrawerLayout$DrawerListener;->onDrawerOpened(Landroid/view/View;)V

    .line 551
    :cond_0
    return-void
.end method

.method public onDrawerSlide(Landroid/view/View;F)V
    .locals 2
    .param p1, "drawerView"    # Landroid/view/View;
    .param p2, "slideOffset"    # F

    .prologue
    .line 561
    iget v0, p0, Lcom/google/android/play/drawer/PlayDrawerLayout;->mDrawerState:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/play/drawer/PlayDrawerLayout;->mDrawerSlideOffset:F

    cmpg-float v0, p2, v0

    if-gez v0, :cond_0

    .line 562
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerLayout;->mDrawerAdapter:Lcom/google/android/play/drawer/PlayDrawerAdapter;

    invoke-virtual {v0}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->collapseAccountListIfNeeded()V

    .line 564
    :cond_0
    iput p2, p0, Lcom/google/android/play/drawer/PlayDrawerLayout;->mDrawerSlideOffset:F

    .line 565
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerLayout;->mDrawerToggle:Landroid/support/v7/app/ActionBarDrawerToggle;

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/app/ActionBarDrawerToggle;->onDrawerSlide(Landroid/view/View;F)V

    .line 567
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerLayout;->mDrawerListener:Landroid/support/v4/widget/DrawerLayout$DrawerListener;

    if-eqz v0, :cond_1

    .line 568
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerLayout;->mDrawerListener:Landroid/support/v4/widget/DrawerLayout$DrawerListener;

    invoke-interface {v0, p1, p2}, Landroid/support/v4/widget/DrawerLayout$DrawerListener;->onDrawerSlide(Landroid/view/View;F)V

    .line 570
    :cond_1
    return-void
.end method

.method public onDrawerStateChanged(I)V
    .locals 1
    .param p1, "newState"    # I

    .prologue
    .line 578
    iput p1, p0, Lcom/google/android/play/drawer/PlayDrawerLayout;->mDrawerState:I

    .line 579
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerLayout;->mDrawerToggle:Landroid/support/v7/app/ActionBarDrawerToggle;

    invoke-virtual {v0, p1}, Landroid/support/v7/app/ActionBarDrawerToggle;->onDrawerStateChanged(I)V

    .line 581
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerLayout;->mDrawerListener:Landroid/support/v4/widget/DrawerLayout$DrawerListener;

    if-eqz v0, :cond_0

    .line 582
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerLayout;->mDrawerListener:Landroid/support/v4/widget/DrawerLayout$DrawerListener;

    invoke-interface {v0, p1}, Landroid/support/v4/widget/DrawerLayout$DrawerListener;->onDrawerStateChanged(I)V

    .line 584
    :cond_0
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 302
    invoke-super {p0}, Landroid/support/v4/widget/DrawerLayout;->onFinishInflate()V

    .line 304
    sget v0, Lcom/google/android/play/R$id;->play_drawer_list:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/drawer/PlayDrawerLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerLayout;->mDrawerList:Landroid/widget/ListView;

    .line 305
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 599
    :try_start_0
    invoke-super {p0, p1}, Landroid/support/v4/widget/DrawerLayout;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 601
    :goto_0
    return v1

    .line 600
    :catch_0
    move-exception v0

    .line 601
    .local v0, "e":Ljava/lang/NullPointerException;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public openDrawer()V
    .locals 1

    .prologue
    .line 458
    invoke-direct {p0}, Lcom/google/android/play/drawer/PlayDrawerLayout;->checkIsConfigured()V

    .line 459
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerLayout;->mDrawerList:Landroid/widget/ListView;

    invoke-virtual {p0, v0}, Lcom/google/android/play/drawer/PlayDrawerLayout;->isDrawerOpen(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 460
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerLayout;->mDrawerList:Landroid/widget/ListView;

    invoke-virtual {p0, v0}, Lcom/google/android/play/drawer/PlayDrawerLayout;->openDrawer(Landroid/view/View;)V

    .line 462
    :cond_0
    return-void
.end method

.method public setActionBarHeight(IZ)V
    .locals 7
    .param p1, "customActionBarHeight"    # I
    .param p2, "isFloating"    # Z

    .prologue
    const/4 v6, 0x0

    .line 388
    invoke-direct {p0}, Lcom/google/android/play/drawer/PlayDrawerLayout;->checkIsConfigured()V

    .line 390
    invoke-virtual {p0}, Lcom/google/android/play/drawer/PlayDrawerLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 393
    .local v3, "res":Landroid/content/res/Resources;
    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    iget v4, v5, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 394
    .local v4, "screenWidth":I
    sget v5, Lcom/google/android/play/R$dimen;->play_drawer_max_width:I

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 395
    .local v2, "maxSize":I
    iget-object v5, p0, Lcom/google/android/play/drawer/PlayDrawerLayout;->mDrawerList:Landroid/widget/ListView;

    invoke-virtual {v5}, Landroid/widget/ListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/DrawerLayout$LayoutParams;

    .line 396
    .local v0, "lp":Landroid/support/v4/widget/DrawerLayout$LayoutParams;
    sub-int v5, v4, p1

    invoke-static {v2, v5}, Ljava/lang/Math;->min(II)I

    move-result v5

    iput v5, v0, Landroid/support/v4/widget/DrawerLayout$LayoutParams;->width:I

    .line 398
    if-nez p2, :cond_0

    .line 399
    iget-object v5, p0, Lcom/google/android/play/drawer/PlayDrawerLayout;->mDrawerList:Landroid/widget/ListView;

    invoke-virtual {v5}, Landroid/widget/ListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 401
    .local v1, "marginLayoutParams":Landroid/view/ViewGroup$MarginLayoutParams;
    invoke-virtual {v1, v6, p1, v6, v6}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 404
    .end local v1    # "marginLayoutParams":Landroid/view/ViewGroup$MarginLayoutParams;
    :cond_0
    iget-object v5, p0, Lcom/google/android/play/drawer/PlayDrawerLayout;->mDrawerList:Landroid/widget/ListView;

    invoke-virtual {v5}, Landroid/widget/ListView;->requestLayout()V

    .line 405
    return-void
.end method

.method public setDrawerIndicatorEnabled(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 476
    invoke-direct {p0}, Lcom/google/android/play/drawer/PlayDrawerLayout;->checkIsConfigured()V

    .line 477
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerLayout;->mDrawerToggle:Landroid/support/v7/app/ActionBarDrawerToggle;

    invoke-virtual {v0, p1}, Landroid/support/v7/app/ActionBarDrawerToggle;->setDrawerIndicatorEnabled(Z)V

    .line 478
    return-void
.end method

.method public final setDrawerListener(Landroid/support/v4/widget/DrawerLayout$DrawerListener;)V
    .locals 0
    .param p1, "listener"    # Landroid/support/v4/widget/DrawerLayout$DrawerListener;

    .prologue
    .line 591
    iput-object p1, p0, Lcom/google/android/play/drawer/PlayDrawerLayout;->mDrawerListener:Landroid/support/v4/widget/DrawerLayout$DrawerListener;

    .line 592
    return-void
.end method

.method public syncDrawerIndicator()V
    .locals 1

    .prologue
    .line 486
    invoke-direct {p0}, Lcom/google/android/play/drawer/PlayDrawerLayout;->checkIsConfigured()V

    .line 487
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerLayout;->mDrawerToggle:Landroid/support/v7/app/ActionBarDrawerToggle;

    invoke-virtual {v0}, Landroid/support/v7/app/ActionBarDrawerToggle;->syncState()V

    .line 488
    return-void
.end method

.method public updateContent(Ljava/lang/String;[Landroid/accounts/Account;Ljava/util/List;Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerDownloadSwitchConfig;Ljava/util/List;)V
    .locals 6
    .param p1, "currentAccountName"    # Ljava/lang/String;
    .param p2, "accounts"    # [Landroid/accounts/Account;
    .param p4, "downloadSwitchConfig"    # Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerDownloadSwitchConfig;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "[",
            "Landroid/accounts/Account;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;",
            ">;",
            "Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerDownloadSwitchConfig;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 372
    .local p3, "primaryActions":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;>;"
    .local p5, "secondaryActions":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;>;"
    invoke-direct {p0}, Lcom/google/android/play/drawer/PlayDrawerLayout;->checkIsConfigured()V

    .line 374
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerLayout;->mDrawerAdapter:Lcom/google/android/play/drawer/PlayDrawerAdapter;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->updateContent(Ljava/lang/String;[Landroid/accounts/Account;Ljava/util/List;Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerDownloadSwitchConfig;Ljava/util/List;)V

    .line 376
    return-void
.end method
