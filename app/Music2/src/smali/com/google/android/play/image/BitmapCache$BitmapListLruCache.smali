.class Lcom/google/android/play/image/BitmapCache$BitmapListLruCache;
.super Landroid/support/v4/util/LruCache;
.source "BitmapCache.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/play/image/BitmapCache;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BitmapListLruCache"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v4/util/LruCache",
        "<",
        "Ljava/lang/String;",
        "Ljava/util/ArrayList",
        "<",
        "Lcom/google/android/play/image/BitmapCache$BitmapCacheEntry;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/play/image/BitmapCache;


# direct methods
.method public constructor <init>(Lcom/google/android/play/image/BitmapCache;I)V
    .locals 0
    .param p2, "maxSizeInBytes"    # I

    .prologue
    .line 155
    iput-object p1, p0, Lcom/google/android/play/image/BitmapCache$BitmapListLruCache;->this$0:Lcom/google/android/play/image/BitmapCache;

    .line 156
    invoke-direct {p0, p2}, Landroid/support/v4/util/LruCache;-><init>(I)V

    .line 157
    return-void
.end method


# virtual methods
.method protected bridge synthetic sizeOf(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 149
    check-cast p1, Ljava/lang/String;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Ljava/util/ArrayList;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/play/image/BitmapCache$BitmapListLruCache;->sizeOf(Ljava/lang/String;Ljava/util/ArrayList;)I

    move-result v0

    return v0
.end method

.method protected sizeOf(Ljava/lang/String;Ljava/util/ArrayList;)I
    .locals 6
    .param p1, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/play/image/BitmapCache$BitmapCacheEntry;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 164
    .local p2, "bitmapArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/play/image/BitmapCache$BitmapCacheEntry;>;"
    const/4 v3, 0x0

    .line 165
    .local v3, "totalSize":I
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 167
    .local v0, "arraySize":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v0, :cond_0

    .line 168
    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/play/image/BitmapCache$BitmapCacheEntry;

    iget-object v1, v4, Lcom/google/android/play/image/BitmapCache$BitmapCacheEntry;->bitmap:Landroid/graphics/Bitmap;

    .line 169
    .local v1, "bitmap":Landroid/graphics/Bitmap;
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getRowBytes()I

    move-result v4

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    mul-int/2addr v4, v5

    add-int/2addr v3, v4

    .line 167
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 171
    .end local v1    # "bitmap":Landroid/graphics/Bitmap;
    :cond_0
    return v3
.end method
