.class public Lcom/google/android/play/search/PlaySearchSuggestionsList;
.super Landroid/widget/LinearLayout;
.source "PlaySearchSuggestionsList.java"

# interfaces
.implements Lcom/google/android/play/search/PlaySearchListener;


# instance fields
.field private mAdapter:Lcom/google/android/play/search/PlaySearchSuggestionAdapter;

.field private mAdapterDataObserver:Landroid/support/v7/widget/RecyclerView$AdapterDataObserver;

.field private mController:Lcom/google/android/play/search/PlaySearchController;

.field private mLayoutManager:Landroid/support/v7/widget/RecyclerView$LayoutManager;

.field private mRecyclerView:Landroid/support/v7/widget/RecyclerView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 27
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/play/search/PlaySearchSuggestionsList;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 31
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/play/search/PlaySearchSuggestionsList;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 35
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 36
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/play/search/PlaySearchSuggestionsList;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/play/search/PlaySearchSuggestionsList;

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/google/android/play/search/PlaySearchSuggestionsList;->updateVisibility()V

    return-void
.end method

.method private updateVisibility()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 77
    iget-object v1, p0, Lcom/google/android/play/search/PlaySearchSuggestionsList;->mController:Lcom/google/android/play/search/PlaySearchController;

    if-nez v1, :cond_0

    .line 87
    :goto_0
    return-void

    .line 81
    :cond_0
    iget-object v1, p0, Lcom/google/android/play/search/PlaySearchSuggestionsList;->mController:Lcom/google/android/play/search/PlaySearchController;

    invoke-virtual {v1}, Lcom/google/android/play/search/PlaySearchController;->getMode()I

    move-result v0

    .line 82
    .local v0, "mode":I
    iget-object v1, p0, Lcom/google/android/play/search/PlaySearchSuggestionsList;->mAdapter:Lcom/google/android/play/search/PlaySearchSuggestionAdapter;

    invoke-virtual {v1}, Lcom/google/android/play/search/PlaySearchSuggestionAdapter;->getItemCount()I

    move-result v1

    if-lt v1, v2, :cond_1

    if-ne v0, v2, :cond_2

    .line 83
    :cond_1
    const/16 v1, 0x8

    invoke-virtual {p0, v1}, Lcom/google/android/play/search/PlaySearchSuggestionsList;->setVisibility(I)V

    goto :goto_0

    .line 85
    :cond_2
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/play/search/PlaySearchSuggestionsList;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public onFinishInflate()V
    .locals 2

    .prologue
    .line 40
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 41
    sget v0, Lcom/google/android/play/R$id;->suggestion_list_recycler_view:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/search/PlaySearchSuggestionsList;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lcom/google/android/play/search/PlaySearchSuggestionsList;->mRecyclerView:Landroid/support/v7/widget/RecyclerView;

    .line 43
    new-instance v0, Lcom/google/android/play/search/PlaySearchSuggestionLayoutManager;

    invoke-virtual {p0}, Lcom/google/android/play/search/PlaySearchSuggestionsList;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/play/search/PlaySearchSuggestionLayoutManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/play/search/PlaySearchSuggestionsList;->mLayoutManager:Landroid/support/v7/widget/RecyclerView$LayoutManager;

    .line 44
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchSuggestionsList;->mRecyclerView:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/google/android/play/search/PlaySearchSuggestionsList;->mLayoutManager:Landroid/support/v7/widget/RecyclerView$LayoutManager;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 46
    new-instance v0, Lcom/google/android/play/search/PlaySearchSuggestionAdapter;

    invoke-direct {v0}, Lcom/google/android/play/search/PlaySearchSuggestionAdapter;-><init>()V

    iput-object v0, p0, Lcom/google/android/play/search/PlaySearchSuggestionsList;->mAdapter:Lcom/google/android/play/search/PlaySearchSuggestionAdapter;

    .line 47
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchSuggestionsList;->mRecyclerView:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/google/android/play/search/PlaySearchSuggestionsList;->mAdapter:Lcom/google/android/play/search/PlaySearchSuggestionAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 48
    new-instance v0, Lcom/google/android/play/search/PlaySearchSuggestionsList$1;

    invoke-direct {v0, p0}, Lcom/google/android/play/search/PlaySearchSuggestionsList$1;-><init>(Lcom/google/android/play/search/PlaySearchSuggestionsList;)V

    iput-object v0, p0, Lcom/google/android/play/search/PlaySearchSuggestionsList;->mAdapterDataObserver:Landroid/support/v7/widget/RecyclerView$AdapterDataObserver;

    .line 54
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchSuggestionsList;->mAdapter:Lcom/google/android/play/search/PlaySearchSuggestionAdapter;

    iget-object v1, p0, Lcom/google/android/play/search/PlaySearchSuggestionsList;->mAdapterDataObserver:Landroid/support/v7/widget/RecyclerView$AdapterDataObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/play/search/PlaySearchSuggestionAdapter;->registerAdapterDataObserver(Landroid/support/v7/widget/RecyclerView$AdapterDataObserver;)V

    .line 55
    invoke-direct {p0}, Lcom/google/android/play/search/PlaySearchSuggestionsList;->updateVisibility()V

    .line 56
    return-void
.end method

.method public onModeChanged(I)V
    .locals 0
    .param p1, "searchMode"    # I

    .prologue
    .line 91
    invoke-direct {p0}, Lcom/google/android/play/search/PlaySearchSuggestionsList;->updateVisibility()V

    .line 92
    return-void
.end method

.method public onQueryChanged(Ljava/lang/String;)V
    .locals 0
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 97
    return-void
.end method

.method public setPlaySearchController(Lcom/google/android/play/search/PlaySearchController;)V
    .locals 1
    .param p1, "playSearchController"    # Lcom/google/android/play/search/PlaySearchController;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchSuggestionsList;->mController:Lcom/google/android/play/search/PlaySearchController;

    if-eqz v0, :cond_0

    .line 63
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchSuggestionsList;->mController:Lcom/google/android/play/search/PlaySearchController;

    invoke-virtual {v0, p0}, Lcom/google/android/play/search/PlaySearchController;->removePlaySearchListener(Lcom/google/android/play/search/PlaySearchListener;)V

    .line 65
    :cond_0
    iput-object p1, p0, Lcom/google/android/play/search/PlaySearchSuggestionsList;->mController:Lcom/google/android/play/search/PlaySearchController;

    .line 66
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchSuggestionsList;->mController:Lcom/google/android/play/search/PlaySearchController;

    invoke-virtual {v0, p0}, Lcom/google/android/play/search/PlaySearchController;->addPlaySearchListener(Lcom/google/android/play/search/PlaySearchListener;)V

    .line 67
    return-void
.end method

.method public setSuggestions(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 73
    .local p1, "suggestions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchSuggestionsList;->mAdapter:Lcom/google/android/play/search/PlaySearchSuggestionAdapter;

    invoke-virtual {v0, p1}, Lcom/google/android/play/search/PlaySearchSuggestionAdapter;->updateData(Ljava/util/ArrayList;)V

    .line 74
    return-void
.end method
