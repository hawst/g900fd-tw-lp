.class Lcom/google/android/play/drawer/PlayDrawerAdapter$6;
.super Ljava/lang/Object;
.source "PlayDrawerAdapter.java"

# interfaces
.implements Lcom/android/volley/Response$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/play/drawer/PlayDrawerAdapter;->loadAllSecondaryAccountDocV2sOnce()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/android/volley/Response$Listener",
        "<",
        "Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/play/drawer/PlayDrawerAdapter;

.field final synthetic val$accountName:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/play/drawer/PlayDrawerAdapter;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 462
    iput-object p1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$6;->this$0:Lcom/google/android/play/drawer/PlayDrawerAdapter;

    iput-object p2, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$6;->val$accountName:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResponse(Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;)V
    .locals 5
    .param p1, "plusProfileResponse"    # Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;

    .prologue
    .line 468
    iget-object v2, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$6;->this$0:Lcom/google/android/play/drawer/PlayDrawerAdapter;

    # getter for: Lcom/google/android/play/drawer/PlayDrawerAdapter;->mAccountDocV2s:Ljava/util/Map;
    invoke-static {v2}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->access$000(Lcom/google/android/play/drawer/PlayDrawerAdapter;)Ljava/util/Map;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$6;->val$accountName:Ljava/lang/String;

    iget-object v4, p1, Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;->partialUserProfile:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 470
    iget-object v2, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$6;->this$0:Lcom/google/android/play/drawer/PlayDrawerAdapter;

    # getter for: Lcom/google/android/play/drawer/PlayDrawerAdapter;->mIsAccountDocLoaded:Ljava/util/Set;
    invoke-static {v2}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->access$100(Lcom/google/android/play/drawer/PlayDrawerAdapter;)Ljava/util/Set;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$6;->val$accountName:Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 474
    const/4 v1, 0x1

    .line 475
    .local v1, "isAllSeconaryDocV2sLoaded":Z
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$6;->this$0:Lcom/google/android/play/drawer/PlayDrawerAdapter;

    # getter for: Lcom/google/android/play/drawer/PlayDrawerAdapter;->mNonCurrentAccounts:[Landroid/accounts/Account;
    invoke-static {v2}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->access$800(Lcom/google/android/play/drawer/PlayDrawerAdapter;)[Landroid/accounts/Account;

    move-result-object v2

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 476
    iget-object v2, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$6;->this$0:Lcom/google/android/play/drawer/PlayDrawerAdapter;

    # getter for: Lcom/google/android/play/drawer/PlayDrawerAdapter;->mIsAccountDocLoaded:Ljava/util/Set;
    invoke-static {v2}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->access$100(Lcom/google/android/play/drawer/PlayDrawerAdapter;)Ljava/util/Set;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$6;->val$accountName:Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 477
    const/4 v1, 0x0

    .line 481
    :cond_0
    if-eqz v1, :cond_1

    .line 482
    iget-object v2, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$6;->this$0:Lcom/google/android/play/drawer/PlayDrawerAdapter;

    invoke-virtual {v2}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->notifyDataSetChanged()V

    .line 484
    :cond_1
    return-void

    .line 475
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 462
    check-cast p1, Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/play/drawer/PlayDrawerAdapter$6;->onResponse(Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;)V

    return-void
.end method
