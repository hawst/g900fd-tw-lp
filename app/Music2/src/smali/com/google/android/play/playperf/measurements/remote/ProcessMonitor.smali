.class public Lcom/google/android/play/playperf/measurements/remote/ProcessMonitor;
.super Ljava/lang/Object;
.source "ProcessMonitor.java"


# direct methods
.method public static triggerGarbageCollection(JZ)V
    .locals 10
    .param p0, "timeout"    # J
    .param p2, "throwException"    # Z

    .prologue
    .line 27
    new-instance v6, Lcom/google/android/play/playperf/measurements/memory/JavaMemorySnapshot;

    invoke-direct {v6}, Lcom/google/android/play/playperf/measurements/memory/JavaMemorySnapshot;-><init>()V

    invoke-virtual {v6}, Lcom/google/android/play/playperf/measurements/memory/JavaMemorySnapshot;->getJvmHeapUsedB()J

    move-result-wide v2

    .line 28
    .local v2, "reference":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 29
    .local v4, "startTime":J
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 30
    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long/2addr v6, v4

    cmp-long v6, v6, p0

    if-gez v6, :cond_2

    .line 31
    new-instance v6, Lcom/google/android/play/playperf/measurements/memory/JavaMemorySnapshot;

    invoke-direct {v6}, Lcom/google/android/play/playperf/measurements/memory/JavaMemorySnapshot;-><init>()V

    invoke-virtual {v6}, Lcom/google/android/play/playperf/measurements/memory/JavaMemorySnapshot;->getJvmHeapUsedB()J

    move-result-wide v0

    .line 32
    .local v0, "current":J
    cmp-long v6, v0, v2

    if-gez v6, :cond_1

    .line 47
    .end local v0    # "current":J
    :cond_0
    return-void

    .line 35
    .restart local v0    # "current":J
    :cond_1
    move-wide v2, v0

    .line 38
    const-wide/16 v6, 0x1f4

    :try_start_0
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 39
    :catch_0
    move-exception v6

    goto :goto_0

    .line 43
    .end local v0    # "current":J
    :cond_2
    if-eqz p2, :cond_0

    .line 44
    new-instance v6, Lcom/google/android/play/playperf/exceptions/PlayPerfException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Timed out waiting for garbage collection after "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " millis"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Lcom/google/android/play/playperf/exceptions/PlayPerfException;-><init>(Ljava/lang/String;)V

    throw v6
.end method
