.class Lcom/google/android/play/drawer/PlayDrawerAdapter$10;
.super Ljava/lang/Object;
.source "PlayDrawerAdapter.java"

# interfaces
.implements Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/play/drawer/PlayDrawerAdapter;->getDownloadToggleView(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/play/drawer/PlayDrawerAdapter;


# direct methods
.method constructor <init>(Lcom/google/android/play/drawer/PlayDrawerAdapter;)V
    .locals 0

    .prologue
    .line 635
    iput-object p1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$10;->this$0:Lcom/google/android/play/drawer/PlayDrawerAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;Z)V
    .locals 1
    .param p1, "row"    # Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;
    .param p2, "checked"    # Z

    .prologue
    .line 638
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$10;->this$0:Lcom/google/android/play/drawer/PlayDrawerAdapter;

    # setter for: Lcom/google/android/play/drawer/PlayDrawerAdapter;->mDownloadOnlyEnabled:Z
    invoke-static {v0, p2}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->access$1202(Lcom/google/android/play/drawer/PlayDrawerAdapter;Z)Z

    .line 639
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$10;->this$0:Lcom/google/android/play/drawer/PlayDrawerAdapter;

    # getter for: Lcom/google/android/play/drawer/PlayDrawerAdapter;->mPlayDrawerContentClickListener:Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerContentClickListener;
    invoke-static {v0}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->access$600(Lcom/google/android/play/drawer/PlayDrawerAdapter;)Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerContentClickListener;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerContentClickListener;->onDownloadToggleClicked(Z)V

    .line 640
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$10;->this$0:Lcom/google/android/play/drawer/PlayDrawerAdapter;

    invoke-virtual {v0}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->notifyDataSetChanged()V

    .line 641
    return-void
.end method
