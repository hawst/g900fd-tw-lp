.class public interface abstract Lcom/google/android/play/playperf/measurements/Measurer;
.super Ljava/lang/Object;
.source "Measurer.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/google/android/play/playperf/measurements/Recordable;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# virtual methods
.method public abstract getResult()Lcom/google/android/play/playperf/measurements/Recordable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method

.method public abstract start()V
.end method

.method public abstract stop()V
.end method
