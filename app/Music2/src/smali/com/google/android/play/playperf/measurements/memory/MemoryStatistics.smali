.class public Lcom/google/android/play/playperf/measurements/memory/MemoryStatistics;
.super Ljava/lang/Object;
.source "MemoryStatistics.java"

# interfaces
.implements Lcom/google/android/play/playperf/measurements/Recordable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/play/playperf/measurements/memory/MemoryStatistics;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mHeapTotalAvg:Lcom/google/android/play/playperf/data/DataPoint;

.field private final mHeapUsedAvg:Lcom/google/android/play/playperf/data/DataPoint;

.field private final mMaxHeapTotal:Lcom/google/android/play/playperf/data/DataPoint;

.field private final mMaxHeapUsed:Lcom/google/android/play/playperf/data/DataPoint;

.field private final mMinHeapTotal:Lcom/google/android/play/playperf/data/DataPoint;

.field private final mMinHeapUsed:Lcom/google/android/play/playperf/data/DataPoint;

.field private final mSectionName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 158
    new-instance v0, Lcom/google/android/play/playperf/measurements/memory/MemoryStatistics$1;

    invoke-direct {v0}, Lcom/google/android/play/playperf/measurements/memory/MemoryStatistics$1;-><init>()V

    sput-object v0, Lcom/google/android/play/playperf/measurements/memory/MemoryStatistics;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "parcel"    # Landroid/os/Parcel;

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/playperf/data/DataPoint;

    iput-object v0, p0, Lcom/google/android/play/playperf/measurements/memory/MemoryStatistics;->mMaxHeapTotal:Lcom/google/android/play/playperf/data/DataPoint;

    .line 62
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/playperf/data/DataPoint;

    iput-object v0, p0, Lcom/google/android/play/playperf/measurements/memory/MemoryStatistics;->mMinHeapTotal:Lcom/google/android/play/playperf/data/DataPoint;

    .line 63
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/playperf/data/DataPoint;

    iput-object v0, p0, Lcom/google/android/play/playperf/measurements/memory/MemoryStatistics;->mMaxHeapUsed:Lcom/google/android/play/playperf/data/DataPoint;

    .line 64
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/playperf/data/DataPoint;

    iput-object v0, p0, Lcom/google/android/play/playperf/measurements/memory/MemoryStatistics;->mMinHeapUsed:Lcom/google/android/play/playperf/data/DataPoint;

    .line 65
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/playperf/data/DataPoint;

    iput-object v0, p0, Lcom/google/android/play/playperf/measurements/memory/MemoryStatistics;->mHeapTotalAvg:Lcom/google/android/play/playperf/data/DataPoint;

    .line 66
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/playperf/data/DataPoint;

    iput-object v0, p0, Lcom/google/android/play/playperf/measurements/memory/MemoryStatistics;->mHeapUsedAvg:Lcom/google/android/play/playperf/data/DataPoint;

    .line 67
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/play/playperf/measurements/memory/MemoryStatistics;->mSectionName:Ljava/lang/String;

    .line 68
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/google/android/play/playperf/measurements/memory/MemoryStatistics$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/google/android/play/playperf/measurements/memory/MemoryStatistics$1;

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/google/android/play/playperf/measurements/memory/MemoryStatistics;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/play/playperf/data/DataPoint;Lcom/google/android/play/playperf/data/DataPoint;Lcom/google/android/play/playperf/data/DataPoint;Lcom/google/android/play/playperf/data/DataPoint;Lcom/google/android/play/playperf/data/DataPoint;Lcom/google/android/play/playperf/data/DataPoint;Ljava/lang/String;)V
    .locals 0
    .param p1, "maxHeapTotal"    # Lcom/google/android/play/playperf/data/DataPoint;
    .param p2, "minHeapTotal"    # Lcom/google/android/play/playperf/data/DataPoint;
    .param p3, "maxHeapUsed"    # Lcom/google/android/play/playperf/data/DataPoint;
    .param p4, "minHeapUsed"    # Lcom/google/android/play/playperf/data/DataPoint;
    .param p5, "heapTotalAvg"    # Lcom/google/android/play/playperf/data/DataPoint;
    .param p6, "heapUsedAvg"    # Lcom/google/android/play/playperf/data/DataPoint;
    .param p7, "sectionName"    # Ljava/lang/String;

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-object p1, p0, Lcom/google/android/play/playperf/measurements/memory/MemoryStatistics;->mMaxHeapTotal:Lcom/google/android/play/playperf/data/DataPoint;

    .line 52
    iput-object p2, p0, Lcom/google/android/play/playperf/measurements/memory/MemoryStatistics;->mMinHeapTotal:Lcom/google/android/play/playperf/data/DataPoint;

    .line 53
    iput-object p3, p0, Lcom/google/android/play/playperf/measurements/memory/MemoryStatistics;->mMaxHeapUsed:Lcom/google/android/play/playperf/data/DataPoint;

    .line 54
    iput-object p4, p0, Lcom/google/android/play/playperf/measurements/memory/MemoryStatistics;->mMinHeapUsed:Lcom/google/android/play/playperf/data/DataPoint;

    .line 55
    iput-object p5, p0, Lcom/google/android/play/playperf/measurements/memory/MemoryStatistics;->mHeapTotalAvg:Lcom/google/android/play/playperf/data/DataPoint;

    .line 56
    iput-object p6, p0, Lcom/google/android/play/playperf/measurements/memory/MemoryStatistics;->mHeapUsedAvg:Lcom/google/android/play/playperf/data/DataPoint;

    .line 57
    iput-object p7, p0, Lcom/google/android/play/playperf/measurements/memory/MemoryStatistics;->mSectionName:Ljava/lang/String;

    .line 58
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 178
    const/4 v0, 0x0

    return v0
.end method

.method public getHeapTotalAvg()Lcom/google/android/play/playperf/data/DataPoint;
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/google/android/play/playperf/measurements/memory/MemoryStatistics;->mHeapTotalAvg:Lcom/google/android/play/playperf/data/DataPoint;

    return-object v0
.end method

.method public getHeapUsedAvg()Lcom/google/android/play/playperf/data/DataPoint;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/google/android/play/playperf/measurements/memory/MemoryStatistics;->mHeapUsedAvg:Lcom/google/android/play/playperf/data/DataPoint;

    return-object v0
.end method

.method public getMaxHeapTotal()Lcom/google/android/play/playperf/data/DataPoint;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/google/android/play/playperf/measurements/memory/MemoryStatistics;->mMaxHeapTotal:Lcom/google/android/play/playperf/data/DataPoint;

    return-object v0
.end method

.method public getMaxHeapUsed()Lcom/google/android/play/playperf/data/DataPoint;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/google/android/play/playperf/measurements/memory/MemoryStatistics;->mMaxHeapUsed:Lcom/google/android/play/playperf/data/DataPoint;

    return-object v0
.end method

.method public getMinHeapTotal()Lcom/google/android/play/playperf/data/DataPoint;
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/google/android/play/playperf/measurements/memory/MemoryStatistics;->mMinHeapTotal:Lcom/google/android/play/playperf/data/DataPoint;

    return-object v0
.end method

.method public getMinHeapUsed()Lcom/google/android/play/playperf/data/DataPoint;
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/google/android/play/playperf/measurements/memory/MemoryStatistics;->mMinHeapUsed:Lcom/google/android/play/playperf/data/DataPoint;

    return-object v0
.end method

.method public getSectionName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lcom/google/android/play/playperf/measurements/memory/MemoryStatistics;->mSectionName:Ljava/lang/String;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "parcel"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 186
    invoke-virtual {p0}, Lcom/google/android/play/playperf/measurements/memory/MemoryStatistics;->getMaxHeapTotal()Lcom/google/android/play/playperf/data/DataPoint;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 187
    invoke-virtual {p0}, Lcom/google/android/play/playperf/measurements/memory/MemoryStatistics;->getMinHeapTotal()Lcom/google/android/play/playperf/data/DataPoint;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 188
    invoke-virtual {p0}, Lcom/google/android/play/playperf/measurements/memory/MemoryStatistics;->getMaxHeapUsed()Lcom/google/android/play/playperf/data/DataPoint;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 189
    invoke-virtual {p0}, Lcom/google/android/play/playperf/measurements/memory/MemoryStatistics;->getMinHeapUsed()Lcom/google/android/play/playperf/data/DataPoint;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 190
    invoke-virtual {p0}, Lcom/google/android/play/playperf/measurements/memory/MemoryStatistics;->getHeapTotalAvg()Lcom/google/android/play/playperf/data/DataPoint;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 191
    invoke-virtual {p0}, Lcom/google/android/play/playperf/measurements/memory/MemoryStatistics;->getHeapUsedAvg()Lcom/google/android/play/playperf/data/DataPoint;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 192
    invoke-virtual {p0}, Lcom/google/android/play/playperf/measurements/memory/MemoryStatistics;->getSectionName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 193
    return-void
.end method
