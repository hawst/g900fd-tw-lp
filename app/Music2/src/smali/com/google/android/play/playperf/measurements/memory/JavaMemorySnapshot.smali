.class public Lcom/google/android/play/playperf/measurements/memory/JavaMemorySnapshot;
.super Ljava/lang/Object;
.source "JavaMemorySnapshot.java"


# static fields
.field private static final RUNTIME:Ljava/lang/Runtime;


# instance fields
.field private final jvmHeapTotalB:J

.field private final jvmHeapUsedB:J

.field private final jvmMaxHeapSizeB:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 8
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v0

    sput-object v0, Lcom/google/android/play/playperf/measurements/memory/JavaMemorySnapshot;->RUNTIME:Ljava/lang/Runtime;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    sget-object v0, Lcom/google/android/play/playperf/measurements/memory/JavaMemorySnapshot;->RUNTIME:Ljava/lang/Runtime;

    invoke-virtual {v0}, Ljava/lang/Runtime;->totalMemory()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/play/playperf/measurements/memory/JavaMemorySnapshot;->jvmHeapTotalB:J

    .line 16
    sget-object v0, Lcom/google/android/play/playperf/measurements/memory/JavaMemorySnapshot;->RUNTIME:Ljava/lang/Runtime;

    invoke-virtual {v0}, Ljava/lang/Runtime;->maxMemory()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/play/playperf/measurements/memory/JavaMemorySnapshot;->jvmMaxHeapSizeB:J

    .line 17
    invoke-virtual {p0}, Lcom/google/android/play/playperf/measurements/memory/JavaMemorySnapshot;->getJvmHeapTotalB()J

    move-result-wide v0

    sget-object v2, Lcom/google/android/play/playperf/measurements/memory/JavaMemorySnapshot;->RUNTIME:Ljava/lang/Runtime;

    invoke-virtual {v2}, Ljava/lang/Runtime;->freeMemory()J

    move-result-wide v2

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/play/playperf/measurements/memory/JavaMemorySnapshot;->jvmHeapUsedB:J

    .line 18
    return-void
.end method

.method constructor <init>(JJJ)V
    .locals 1
    .param p1, "jvmHeapUsedB"    # J
    .param p3, "jvmHeapTotalB"    # J
    .param p5, "jvmMaxHeapSizeB"    # J

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-wide p1, p0, Lcom/google/android/play/playperf/measurements/memory/JavaMemorySnapshot;->jvmHeapUsedB:J

    .line 23
    iput-wide p3, p0, Lcom/google/android/play/playperf/measurements/memory/JavaMemorySnapshot;->jvmHeapTotalB:J

    .line 24
    iput-wide p5, p0, Lcom/google/android/play/playperf/measurements/memory/JavaMemorySnapshot;->jvmMaxHeapSizeB:J

    .line 25
    return-void
.end method


# virtual methods
.method public getJvmHeapTotalB()J
    .locals 2

    .prologue
    .line 57
    iget-wide v0, p0, Lcom/google/android/play/playperf/measurements/memory/JavaMemorySnapshot;->jvmHeapTotalB:J

    return-wide v0
.end method

.method public getJvmHeapUsedB()J
    .locals 2

    .prologue
    .line 50
    iget-wide v0, p0, Lcom/google/android/play/playperf/measurements/memory/JavaMemorySnapshot;->jvmHeapUsedB:J

    return-wide v0
.end method

.method public getJvmMaxHeapSizeB()J
    .locals 2

    .prologue
    .line 64
    iget-wide v0, p0, Lcom/google/android/play/playperf/measurements/memory/JavaMemorySnapshot;->jvmMaxHeapSizeB:J

    return-wide v0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 39
    const-string v0, "%s {%s = %d, %s = %d, %s = %d}"

    const/4 v1, 0x7

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "jvmHeapUsedB"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/android/play/playperf/measurements/memory/JavaMemorySnapshot;->getJvmHeapUsedB()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "jvmHeapTotalB"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/android/play/playperf/measurements/memory/JavaMemorySnapshot;->getJvmHeapTotalB()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "jvmMaxHeapSizeB"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    invoke-virtual {p0}, Lcom/google/android/play/playperf/measurements/memory/JavaMemorySnapshot;->getJvmMaxHeapSizeB()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
