.class public Lcom/google/android/play/search/PlaySearchSuggestionAdapter;
.super Landroid/support/v7/widget/RecyclerView$Adapter;
.source "PlaySearchSuggestionAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/play/search/PlaySearchSuggestionAdapter$ViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v7/widget/RecyclerView$Adapter",
        "<",
        "Lcom/google/android/play/search/PlaySearchSuggestionAdapter$ViewHolder;",
        ">;"
    }
.end annotation


# instance fields
.field private final mItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$Adapter;-><init>()V

    .line 20
    invoke-static {}, Lcom/google/android/play/utils/collections/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/play/search/PlaySearchSuggestionAdapter;->mItems:Ljava/util/ArrayList;

    .line 22
    return-void
.end method


# virtual methods
.method public getItemCount()I
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchSuggestionAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public bridge synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 0
    .param p1, "x0"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .param p2, "x1"    # I

    .prologue
    .line 18
    check-cast p1, Lcom/google/android/play/search/PlaySearchSuggestionAdapter$ViewHolder;

    .end local p1    # "x0":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/play/search/PlaySearchSuggestionAdapter;->onBindViewHolder(Lcom/google/android/play/search/PlaySearchSuggestionAdapter$ViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/google/android/play/search/PlaySearchSuggestionAdapter$ViewHolder;I)V
    .locals 3
    .param p1, "holder"    # Lcom/google/android/play/search/PlaySearchSuggestionAdapter$ViewHolder;
    .param p2, "position"    # I

    .prologue
    .line 41
    invoke-virtual {p0}, Lcom/google/android/play/search/PlaySearchSuggestionAdapter;->getItemCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge p2, v1, :cond_0

    const/4 v0, 0x1

    .line 42
    .local v0, "showDivider":Z
    :goto_0
    iget-object v2, p1, Lcom/google/android/play/search/PlaySearchSuggestionAdapter$ViewHolder;->oneSuggestionView:Lcom/google/android/play/search/PlaySearchOneSuggestion;

    iget-object v1, p0, Lcom/google/android/play/search/PlaySearchSuggestionAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v1, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1, v0}, Lcom/google/android/play/search/PlaySearchOneSuggestion;->bindSuggestion(Ljava/lang/String;Z)V

    .line 43
    return-void

    .line 41
    .end local v0    # "showDivider":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 1
    .param p1, "x0"    # Landroid/view/ViewGroup;
    .param p2, "x1"    # I

    .prologue
    .line 18
    invoke-virtual {p0, p1, p2}, Lcom/google/android/play/search/PlaySearchSuggestionAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/google/android/play/search/PlaySearchSuggestionAdapter$ViewHolder;

    move-result-object v0

    return-object v0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/google/android/play/search/PlaySearchSuggestionAdapter$ViewHolder;
    .locals 4
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I

    .prologue
    .line 33
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    sget v2, Lcom/google/android/play/R$layout;->play_search_one_suggestion:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 35
    .local v0, "view":Landroid/view/View;
    new-instance v1, Lcom/google/android/play/search/PlaySearchSuggestionAdapter$ViewHolder;

    check-cast v0, Lcom/google/android/play/search/PlaySearchOneSuggestion;

    .end local v0    # "view":Landroid/view/View;
    invoke-direct {v1, v0}, Lcom/google/android/play/search/PlaySearchSuggestionAdapter$ViewHolder;-><init>(Lcom/google/android/play/search/PlaySearchOneSuggestion;)V

    return-object v1
.end method

.method public updateData(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 54
    .local p1, "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchSuggestionAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 55
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchSuggestionAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 56
    invoke-virtual {p0}, Lcom/google/android/play/search/PlaySearchSuggestionAdapter;->notifyDataSetChanged()V

    .line 57
    return-void
.end method
