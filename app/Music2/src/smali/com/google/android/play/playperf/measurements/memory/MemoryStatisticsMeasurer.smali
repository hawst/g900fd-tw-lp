.class public Lcom/google/android/play/playperf/measurements/memory/MemoryStatisticsMeasurer;
.super Ljava/lang/Object;
.source "MemoryStatisticsMeasurer.java"

# interfaces
.implements Lcom/google/android/play/playperf/measurements/Measurer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/play/playperf/measurements/Measurer",
        "<",
        "Lcom/google/android/play/playperf/measurements/memory/MemoryStatistics;",
        ">;"
    }
.end annotation


# instance fields
.field private final mHeapTotalStatistics:Lcom/google/android/play/playperf/measurements/StatisticsAggregator;

.field private final mHeapUsedStatistics:Lcom/google/android/play/playperf/measurements/StatisticsAggregator;

.field private final mPollingTimer:Lcom/google/android/play/playperf/measurements/PollingTimer;

.field private final mSectionName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/play/playperf/measurements/PollingTimer;Lcom/google/android/play/playperf/measurements/StatisticsAggregator;Lcom/google/android/play/playperf/measurements/StatisticsAggregator;Ljava/lang/String;)V
    .locals 0
    .param p1, "timer"    # Lcom/google/android/play/playperf/measurements/PollingTimer;
    .param p2, "heapTotalAggregator"    # Lcom/google/android/play/playperf/measurements/StatisticsAggregator;
    .param p3, "heapUsedAggregator"    # Lcom/google/android/play/playperf/measurements/StatisticsAggregator;
    .param p4, "sectionName"    # Ljava/lang/String;

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/google/android/play/playperf/measurements/memory/MemoryStatisticsMeasurer;->mPollingTimer:Lcom/google/android/play/playperf/measurements/PollingTimer;

    .line 25
    iput-object p2, p0, Lcom/google/android/play/playperf/measurements/memory/MemoryStatisticsMeasurer;->mHeapTotalStatistics:Lcom/google/android/play/playperf/measurements/StatisticsAggregator;

    .line 26
    iput-object p3, p0, Lcom/google/android/play/playperf/measurements/memory/MemoryStatisticsMeasurer;->mHeapUsedStatistics:Lcom/google/android/play/playperf/measurements/StatisticsAggregator;

    .line 27
    iput-object p4, p0, Lcom/google/android/play/playperf/measurements/memory/MemoryStatisticsMeasurer;->mSectionName:Ljava/lang/String;

    .line 28
    return-void
.end method


# virtual methods
.method public bridge synthetic getResult()Lcom/google/android/play/playperf/measurements/Recordable;
    .locals 1

    .prologue
    .line 10
    invoke-virtual {p0}, Lcom/google/android/play/playperf/measurements/memory/MemoryStatisticsMeasurer;->getResult()Lcom/google/android/play/playperf/measurements/memory/MemoryStatistics;

    move-result-object v0

    return-object v0
.end method

.method public getResult()Lcom/google/android/play/playperf/measurements/memory/MemoryStatistics;
    .locals 8

    .prologue
    .line 53
    new-instance v0, Lcom/google/android/play/playperf/measurements/memory/MemoryStatistics;

    iget-object v1, p0, Lcom/google/android/play/playperf/measurements/memory/MemoryStatisticsMeasurer;->mHeapTotalStatistics:Lcom/google/android/play/playperf/measurements/StatisticsAggregator;

    invoke-virtual {v1}, Lcom/google/android/play/playperf/measurements/StatisticsAggregator;->getMax()Lcom/google/android/play/playperf/data/DataPoint;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/play/playperf/measurements/memory/MemoryStatisticsMeasurer;->mHeapTotalStatistics:Lcom/google/android/play/playperf/measurements/StatisticsAggregator;

    invoke-virtual {v2}, Lcom/google/android/play/playperf/measurements/StatisticsAggregator;->getMin()Lcom/google/android/play/playperf/data/DataPoint;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/play/playperf/measurements/memory/MemoryStatisticsMeasurer;->mHeapUsedStatistics:Lcom/google/android/play/playperf/measurements/StatisticsAggregator;

    invoke-virtual {v3}, Lcom/google/android/play/playperf/measurements/StatisticsAggregator;->getMax()Lcom/google/android/play/playperf/data/DataPoint;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/play/playperf/measurements/memory/MemoryStatisticsMeasurer;->mHeapUsedStatistics:Lcom/google/android/play/playperf/measurements/StatisticsAggregator;

    invoke-virtual {v4}, Lcom/google/android/play/playperf/measurements/StatisticsAggregator;->getMin()Lcom/google/android/play/playperf/data/DataPoint;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/play/playperf/measurements/memory/MemoryStatisticsMeasurer;->mHeapTotalStatistics:Lcom/google/android/play/playperf/measurements/StatisticsAggregator;

    invoke-virtual {v5}, Lcom/google/android/play/playperf/measurements/StatisticsAggregator;->getAverage()Lcom/google/android/play/playperf/data/DataPoint;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/play/playperf/measurements/memory/MemoryStatisticsMeasurer;->mHeapUsedStatistics:Lcom/google/android/play/playperf/measurements/StatisticsAggregator;

    invoke-virtual {v6}, Lcom/google/android/play/playperf/measurements/StatisticsAggregator;->getAverage()Lcom/google/android/play/playperf/data/DataPoint;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/play/playperf/measurements/memory/MemoryStatisticsMeasurer;->mSectionName:Ljava/lang/String;

    invoke-direct/range {v0 .. v7}, Lcom/google/android/play/playperf/measurements/memory/MemoryStatistics;-><init>(Lcom/google/android/play/playperf/data/DataPoint;Lcom/google/android/play/playperf/data/DataPoint;Lcom/google/android/play/playperf/data/DataPoint;Lcom/google/android/play/playperf/data/DataPoint;Lcom/google/android/play/playperf/data/DataPoint;Lcom/google/android/play/playperf/data/DataPoint;Ljava/lang/String;)V

    return-object v0
.end method

.method public start()V
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/play/playperf/measurements/memory/MemoryStatisticsMeasurer;->mHeapTotalStatistics:Lcom/google/android/play/playperf/measurements/StatisticsAggregator;

    invoke-virtual {v0}, Lcom/google/android/play/playperf/measurements/StatisticsAggregator;->clear()V

    .line 36
    iget-object v0, p0, Lcom/google/android/play/playperf/measurements/memory/MemoryStatisticsMeasurer;->mHeapUsedStatistics:Lcom/google/android/play/playperf/measurements/StatisticsAggregator;

    invoke-virtual {v0}, Lcom/google/android/play/playperf/measurements/StatisticsAggregator;->clear()V

    .line 37
    iget-object v0, p0, Lcom/google/android/play/playperf/measurements/memory/MemoryStatisticsMeasurer;->mPollingTimer:Lcom/google/android/play/playperf/measurements/PollingTimer;

    invoke-virtual {v0}, Lcom/google/android/play/playperf/measurements/PollingTimer;->start()V

    .line 38
    return-void
.end method

.method public stop()V
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/play/playperf/measurements/memory/MemoryStatisticsMeasurer;->mPollingTimer:Lcom/google/android/play/playperf/measurements/PollingTimer;

    invoke-virtual {v0}, Lcom/google/android/play/playperf/measurements/PollingTimer;->stop()V

    .line 46
    return-void
.end method
