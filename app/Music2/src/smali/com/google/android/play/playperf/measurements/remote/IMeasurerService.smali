.class public interface abstract Lcom/google/android/play/playperf/measurements/remote/IMeasurerService;
.super Ljava/lang/Object;
.source "IMeasurerService.java"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/play/playperf/measurements/remote/IMeasurerService$Stub;
    }
.end annotation


# virtual methods
.method public abstract getResult()Lcom/google/android/play/playperf/measurements/remote/RecordableWrapper;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract start()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract stop()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
