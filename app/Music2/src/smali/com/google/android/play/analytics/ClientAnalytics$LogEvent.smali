.class public final Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;
.super Lcom/google/protobuf/nano/MessageNano;
.source "ClientAnalytics.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/play/analytics/ClientAnalytics;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LogEvent"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;


# instance fields
.field public appUsage1P:Lcom/google/android/play/analytics/ClientAnalytics$AppUsage1pLogEvent;

.field public eventCode:I

.field public eventFlowId:I

.field public eventTimeMs:J

.field public exp:Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;

.field public isUserInitiated:Z

.field public sourceExtension:[B

.field public sourceExtensionJs:[B

.field public sourceExtensionJson:[B

.field public tag:Ljava/lang/String;

.field public testId:Ljava/lang/String;

.field public timezoneOffsetSeconds:J

.field public value:[Lcom/google/android/play/analytics/ClientAnalytics$LogEventKeyValues;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 377
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 378
    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->clear()Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;

    .line 379
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;
    .locals 2

    .prologue
    .line 327
    sget-object v0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->_emptyArray:[Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;

    if-nez v0, :cond_1

    .line 328
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 330
    :try_start_0
    sget-object v0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->_emptyArray:[Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;

    if-nez v0, :cond_0

    .line 331
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;

    sput-object v0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->_emptyArray:[Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;

    .line 333
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 335
    :cond_1
    sget-object v0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->_emptyArray:[Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;

    return-object v0

    .line 333
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 382
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->eventTimeMs:J

    .line 383
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->tag:Ljava/lang/String;

    .line 384
    iput v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->eventCode:I

    .line 385
    iput v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->eventFlowId:I

    .line 386
    iput-boolean v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->isUserInitiated:Z

    .line 387
    invoke-static {}, Lcom/google/android/play/analytics/ClientAnalytics$LogEventKeyValues;->emptyArray()[Lcom/google/android/play/analytics/ClientAnalytics$LogEventKeyValues;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->value:[Lcom/google/android/play/analytics/ClientAnalytics$LogEventKeyValues;

    .line 388
    iput-object v3, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->appUsage1P:Lcom/google/android/play/analytics/ClientAnalytics$AppUsage1pLogEvent;

    .line 389
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    iput-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->sourceExtension:[B

    .line 390
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    iput-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->sourceExtensionJs:[B

    .line 391
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    iput-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->sourceExtensionJson:[B

    .line 392
    iput-object v3, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->exp:Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;

    .line 393
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->testId:Ljava/lang/String;

    .line 394
    const-wide/32 v0, 0x2bf20

    iput-wide v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->timezoneOffsetSeconds:J

    .line 395
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->cachedSize:I

    .line 396
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 8

    .prologue
    .line 451
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 452
    .local v2, "size":I
    iget-wide v4, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->eventTimeMs:J

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-eqz v3, :cond_0

    .line 453
    const/4 v3, 0x1

    iget-wide v4, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->eventTimeMs:J

    invoke-static {v3, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v3

    add-int/2addr v2, v3

    .line 456
    :cond_0
    iget-object v3, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->tag:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 457
    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->tag:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 460
    :cond_1
    iget-object v3, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->value:[Lcom/google/android/play/analytics/ClientAnalytics$LogEventKeyValues;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->value:[Lcom/google/android/play/analytics/ClientAnalytics$LogEventKeyValues;

    array-length v3, v3

    if-lez v3, :cond_3

    .line 461
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->value:[Lcom/google/android/play/analytics/ClientAnalytics$LogEventKeyValues;

    array-length v3, v3

    if-ge v1, v3, :cond_3

    .line 462
    iget-object v3, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->value:[Lcom/google/android/play/analytics/ClientAnalytics$LogEventKeyValues;

    aget-object v0, v3, v1

    .line 463
    .local v0, "element":Lcom/google/android/play/analytics/ClientAnalytics$LogEventKeyValues;
    if-eqz v0, :cond_2

    .line 464
    const/4 v3, 0x3

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 461
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 469
    .end local v0    # "element":Lcom/google/android/play/analytics/ClientAnalytics$LogEventKeyValues;
    .end local v1    # "i":I
    :cond_3
    iget-object v3, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->sourceExtension:[B

    sget-object v4, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v3

    if-nez v3, :cond_4

    .line 470
    const/4 v3, 0x6

    iget-object v4, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->sourceExtension:[B

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBytesSize(I[B)I

    move-result v3

    add-int/2addr v2, v3

    .line 473
    :cond_4
    iget-object v3, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->exp:Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;

    if-eqz v3, :cond_5

    .line 474
    const/4 v3, 0x7

    iget-object v4, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->exp:Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 477
    :cond_5
    iget-object v3, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->sourceExtensionJs:[B

    sget-object v4, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v3

    if-nez v3, :cond_6

    .line 478
    const/16 v3, 0x8

    iget-object v4, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->sourceExtensionJs:[B

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBytesSize(I[B)I

    move-result v3

    add-int/2addr v2, v3

    .line 481
    :cond_6
    iget-object v3, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->appUsage1P:Lcom/google/android/play/analytics/ClientAnalytics$AppUsage1pLogEvent;

    if-eqz v3, :cond_7

    .line 482
    const/16 v3, 0x9

    iget-object v4, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->appUsage1P:Lcom/google/android/play/analytics/ClientAnalytics$AppUsage1pLogEvent;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 485
    :cond_7
    iget-boolean v3, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->isUserInitiated:Z

    if-eqz v3, :cond_8

    .line 486
    const/16 v3, 0xa

    iget-boolean v4, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->isUserInitiated:Z

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v2, v3

    .line 489
    :cond_8
    iget v3, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->eventCode:I

    if-eqz v3, :cond_9

    .line 490
    const/16 v3, 0xb

    iget v4, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->eventCode:I

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    .line 493
    :cond_9
    iget v3, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->eventFlowId:I

    if-eqz v3, :cond_a

    .line 494
    const/16 v3, 0xc

    iget v4, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->eventFlowId:I

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    .line 497
    :cond_a
    iget-object v3, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->sourceExtensionJson:[B

    sget-object v4, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v3

    if-nez v3, :cond_b

    .line 498
    const/16 v3, 0xd

    iget-object v4, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->sourceExtensionJson:[B

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBytesSize(I[B)I

    move-result v3

    add-int/2addr v2, v3

    .line 501
    :cond_b
    iget-object v3, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->testId:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_c

    .line 502
    const/16 v3, 0xe

    iget-object v4, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->testId:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 505
    :cond_c
    iget-wide v4, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->timezoneOffsetSeconds:J

    const-wide/32 v6, 0x2bf20

    cmp-long v3, v4, v6

    if-eqz v3, :cond_d

    .line 506
    const/16 v3, 0xf

    iget-wide v4, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->timezoneOffsetSeconds:J

    invoke-static {v3, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeSInt64Size(IJ)I

    move-result v3

    add-int/2addr v2, v3

    .line 509
    :cond_d
    return v2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;
    .locals 8
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 517
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 518
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 522
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 523
    :sswitch_0
    return-object p0

    .line 528
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->eventTimeMs:J

    goto :goto_0

    .line 532
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->tag:Ljava/lang/String;

    goto :goto_0

    .line 536
    :sswitch_3
    const/16 v5, 0x1a

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 538
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->value:[Lcom/google/android/play/analytics/ClientAnalytics$LogEventKeyValues;

    if-nez v5, :cond_2

    move v1, v4

    .line 539
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/play/analytics/ClientAnalytics$LogEventKeyValues;

    .line 541
    .local v2, "newArray":[Lcom/google/android/play/analytics/ClientAnalytics$LogEventKeyValues;
    if-eqz v1, :cond_1

    .line 542
    iget-object v5, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->value:[Lcom/google/android/play/analytics/ClientAnalytics$LogEventKeyValues;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 544
    :cond_1
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_3

    .line 545
    new-instance v5, Lcom/google/android/play/analytics/ClientAnalytics$LogEventKeyValues;

    invoke-direct {v5}, Lcom/google/android/play/analytics/ClientAnalytics$LogEventKeyValues;-><init>()V

    aput-object v5, v2, v1

    .line 546
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 547
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 544
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 538
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/play/analytics/ClientAnalytics$LogEventKeyValues;
    :cond_2
    iget-object v5, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->value:[Lcom/google/android/play/analytics/ClientAnalytics$LogEventKeyValues;

    array-length v1, v5

    goto :goto_1

    .line 550
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/play/analytics/ClientAnalytics$LogEventKeyValues;
    :cond_3
    new-instance v5, Lcom/google/android/play/analytics/ClientAnalytics$LogEventKeyValues;

    invoke-direct {v5}, Lcom/google/android/play/analytics/ClientAnalytics$LogEventKeyValues;-><init>()V

    aput-object v5, v2, v1

    .line 551
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 552
    iput-object v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->value:[Lcom/google/android/play/analytics/ClientAnalytics$LogEventKeyValues;

    goto :goto_0

    .line 556
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/play/analytics/ClientAnalytics$LogEventKeyValues;
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBytes()[B

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->sourceExtension:[B

    goto :goto_0

    .line 560
    :sswitch_5
    iget-object v5, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->exp:Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;

    if-nez v5, :cond_4

    .line 561
    new-instance v5, Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;

    invoke-direct {v5}, Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;-><init>()V

    iput-object v5, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->exp:Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;

    .line 563
    :cond_4
    iget-object v5, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->exp:Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 567
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBytes()[B

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->sourceExtensionJs:[B

    goto :goto_0

    .line 571
    :sswitch_7
    iget-object v5, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->appUsage1P:Lcom/google/android/play/analytics/ClientAnalytics$AppUsage1pLogEvent;

    if-nez v5, :cond_5

    .line 572
    new-instance v5, Lcom/google/android/play/analytics/ClientAnalytics$AppUsage1pLogEvent;

    invoke-direct {v5}, Lcom/google/android/play/analytics/ClientAnalytics$AppUsage1pLogEvent;-><init>()V

    iput-object v5, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->appUsage1P:Lcom/google/android/play/analytics/ClientAnalytics$AppUsage1pLogEvent;

    .line 574
    :cond_5
    iget-object v5, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->appUsage1P:Lcom/google/android/play/analytics/ClientAnalytics$AppUsage1pLogEvent;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 578
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v5

    iput-boolean v5, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->isUserInitiated:Z

    goto/16 :goto_0

    .line 582
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v5

    iput v5, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->eventCode:I

    goto/16 :goto_0

    .line 586
    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v5

    iput v5, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->eventFlowId:I

    goto/16 :goto_0

    .line 590
    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBytes()[B

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->sourceExtensionJson:[B

    goto/16 :goto_0

    .line 594
    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->testId:Ljava/lang/String;

    goto/16 :goto_0

    .line 598
    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readSInt64()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->timezoneOffsetSeconds:J

    goto/16 :goto_0

    .line 518
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x32 -> :sswitch_4
        0x3a -> :sswitch_5
        0x42 -> :sswitch_6
        0x4a -> :sswitch_7
        0x50 -> :sswitch_8
        0x58 -> :sswitch_9
        0x60 -> :sswitch_a
        0x6a -> :sswitch_b
        0x72 -> :sswitch_c
        0x78 -> :sswitch_d
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 321
    invoke-virtual {p0, p1}, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 6
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 402
    iget-wide v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->eventTimeMs:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    .line 403
    const/4 v2, 0x1

    iget-wide v4, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->eventTimeMs:J

    invoke-virtual {p1, v2, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 405
    :cond_0
    iget-object v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->tag:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 406
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->tag:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 408
    :cond_1
    iget-object v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->value:[Lcom/google/android/play/analytics/ClientAnalytics$LogEventKeyValues;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->value:[Lcom/google/android/play/analytics/ClientAnalytics$LogEventKeyValues;

    array-length v2, v2

    if-lez v2, :cond_3

    .line 409
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->value:[Lcom/google/android/play/analytics/ClientAnalytics$LogEventKeyValues;

    array-length v2, v2

    if-ge v1, v2, :cond_3

    .line 410
    iget-object v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->value:[Lcom/google/android/play/analytics/ClientAnalytics$LogEventKeyValues;

    aget-object v0, v2, v1

    .line 411
    .local v0, "element":Lcom/google/android/play/analytics/ClientAnalytics$LogEventKeyValues;
    if-eqz v0, :cond_2

    .line 412
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 409
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 416
    .end local v0    # "element":Lcom/google/android/play/analytics/ClientAnalytics$LogEventKeyValues;
    .end local v1    # "i":I
    :cond_3
    iget-object v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->sourceExtension:[B

    sget-object v3, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_4

    .line 417
    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->sourceExtension:[B

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBytes(I[B)V

    .line 419
    :cond_4
    iget-object v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->exp:Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;

    if-eqz v2, :cond_5

    .line 420
    const/4 v2, 0x7

    iget-object v3, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->exp:Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 422
    :cond_5
    iget-object v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->sourceExtensionJs:[B

    sget-object v3, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_6

    .line 423
    const/16 v2, 0x8

    iget-object v3, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->sourceExtensionJs:[B

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBytes(I[B)V

    .line 425
    :cond_6
    iget-object v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->appUsage1P:Lcom/google/android/play/analytics/ClientAnalytics$AppUsage1pLogEvent;

    if-eqz v2, :cond_7

    .line 426
    const/16 v2, 0x9

    iget-object v3, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->appUsage1P:Lcom/google/android/play/analytics/ClientAnalytics$AppUsage1pLogEvent;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 428
    :cond_7
    iget-boolean v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->isUserInitiated:Z

    if-eqz v2, :cond_8

    .line 429
    const/16 v2, 0xa

    iget-boolean v3, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->isUserInitiated:Z

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 431
    :cond_8
    iget v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->eventCode:I

    if-eqz v2, :cond_9

    .line 432
    const/16 v2, 0xb

    iget v3, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->eventCode:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 434
    :cond_9
    iget v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->eventFlowId:I

    if-eqz v2, :cond_a

    .line 435
    const/16 v2, 0xc

    iget v3, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->eventFlowId:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 437
    :cond_a
    iget-object v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->sourceExtensionJson:[B

    sget-object v3, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_b

    .line 438
    const/16 v2, 0xd

    iget-object v3, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->sourceExtensionJson:[B

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBytes(I[B)V

    .line 440
    :cond_b
    iget-object v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->testId:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    .line 441
    const/16 v2, 0xe

    iget-object v3, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->testId:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 443
    :cond_c
    iget-wide v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->timezoneOffsetSeconds:J

    const-wide/32 v4, 0x2bf20

    cmp-long v2, v2, v4

    if-eqz v2, :cond_d

    .line 444
    const/16 v2, 0xf

    iget-wide v4, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->timezoneOffsetSeconds:J

    invoke-virtual {p1, v2, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeSInt64(IJ)V

    .line 446
    :cond_d
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 447
    return-void
.end method
