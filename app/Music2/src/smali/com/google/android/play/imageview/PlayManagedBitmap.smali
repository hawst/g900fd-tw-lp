.class public Lcom/google/android/play/imageview/PlayManagedBitmap;
.super Ljava/lang/Object;
.source "PlayManagedBitmap.java"


# instance fields
.field public final bitmap:Landroid/graphics/Bitmap;

.field public final imageToken:Ljava/lang/Object;

.field private final mReferenceCount:Ljava/util/concurrent/atomic/AtomicInteger;

.field public final relatedBitmaps:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field public final requestedHeight:I

.field public final requestedWidth:I


# direct methods
.method public constructor <init>(Landroid/graphics/Bitmap;Ljava/lang/Object;II)V
    .locals 2
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "imageToken"    # Ljava/lang/Object;
    .param p3, "requestedWidth"    # I
    .param p4, "requestedHeight"    # I

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/play/imageview/PlayManagedBitmap;->mReferenceCount:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 51
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/play/imageview/PlayManagedBitmap;->relatedBitmaps:Ljava/util/List;

    .line 56
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 57
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "PlayManagedBitmap cannot have null bitmap or imageToken"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 60
    :cond_1
    iput-object p1, p0, Lcom/google/android/play/imageview/PlayManagedBitmap;->bitmap:Landroid/graphics/Bitmap;

    .line 61
    iput-object p2, p0, Lcom/google/android/play/imageview/PlayManagedBitmap;->imageToken:Ljava/lang/Object;

    .line 62
    iput p3, p0, Lcom/google/android/play/imageview/PlayManagedBitmap;->requestedWidth:I

    .line 63
    iput p4, p0, Lcom/google/android/play/imageview/PlayManagedBitmap;->requestedHeight:I

    .line 64
    return-void
.end method


# virtual methods
.method public release()Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 92
    iget-object v3, p0, Lcom/google/android/play/imageview/PlayManagedBitmap;->mReferenceCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v0

    .line 93
    .local v0, "remaining":I
    sget-boolean v3, Lcom/google/android/play/utils/PlayCommonLog;->DEBUG:Z

    if-eqz v3, :cond_0

    .line 94
    const-string v3, "release: bitmap=%s, remaining=%s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/android/play/imageview/PlayManagedBitmap;->bitmap:Landroid/graphics/Bitmap;

    aput-object v5, v4, v2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-static {v3, v4}, Lcom/google/android/play/utils/PlayCommonLog;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 96
    :cond_0
    if-gez v0, :cond_1

    .line 97
    const-string v3, "negative reference count in PlayManagedBitmap %s"

    new-array v4, v1, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/android/play/imageview/PlayManagedBitmap;->bitmap:Landroid/graphics/Bitmap;

    aput-object v5, v4, v2

    invoke-static {v3, v4}, Lcom/google/android/play/utils/PlayCommonLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 99
    :cond_1
    if-nez v0, :cond_2

    :goto_0
    return v1

    :cond_2
    move v1, v2

    goto :goto_0
.end method

.method public retain()V
    .locals 4

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/play/imageview/PlayManagedBitmap;->mReferenceCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 71
    sget-boolean v0, Lcom/google/android/play/utils/PlayCommonLog;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 72
    const-string v0, "retain: bitmap=%s, remaining=%s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/play/imageview/PlayManagedBitmap;->bitmap:Landroid/graphics/Bitmap;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/play/imageview/PlayManagedBitmap;->mReferenceCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/play/utils/PlayCommonLog;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 74
    :cond_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 104
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PlayManagedBitmap{mReferenceCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/play/imageview/PlayManagedBitmap;->mReferenceCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", bitmap="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/play/imageview/PlayManagedBitmap;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", imageToken="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/play/imageview/PlayManagedBitmap;->imageToken:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
