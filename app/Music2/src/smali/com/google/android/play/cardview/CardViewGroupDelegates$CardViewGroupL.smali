.class Lcom/google/android/play/cardview/CardViewGroupDelegates$CardViewGroupL;
.super Lcom/google/android/play/cardview/CardViewGroupDelegates$CardViewGroupEclairMr1;
.source "CardViewGroupDelegates.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x15
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/play/cardview/CardViewGroupDelegates;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CardViewGroupL"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 72
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/play/cardview/CardViewGroupDelegates$CardViewGroupEclairMr1;-><init>(Lcom/google/android/play/cardview/CardViewGroupDelegates$1;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/play/cardview/CardViewGroupDelegates$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/play/cardview/CardViewGroupDelegates$1;

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/google/android/play/cardview/CardViewGroupDelegates$CardViewGroupL;-><init>()V

    return-void
.end method


# virtual methods
.method protected getClipToOutline(Landroid/content/res/TypedArray;)Z
    .locals 2
    .param p1, "array"    # Landroid/content/res/TypedArray;

    .prologue
    .line 93
    sget v0, Lcom/google/android/play/R$styleable;->PlayCardViewGroup_playCardClipToOutline:I

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    return v0
.end method

.method public initialize(Lcom/google/android/play/cardview/CardViewGroup;Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6
    .param p1, "cardView"    # Lcom/google/android/play/cardview/CardViewGroup;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "attrs"    # Landroid/util/AttributeSet;
    .param p4, "defStyle"    # I

    .prologue
    .line 78
    invoke-virtual {p0, p2, p3, p4}, Lcom/google/android/play/cardview/CardViewGroupDelegates$CardViewGroupL;->getStyledAttrs(Landroid/content/Context;Landroid/util/AttributeSet;I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 80
    .local v0, "array":Landroid/content/res/TypedArray;
    new-instance v1, Lcom/google/android/play/cardview/RoundRectDrawable;

    invoke-virtual {p0, v0}, Lcom/google/android/play/cardview/CardViewGroupDelegates$CardViewGroupL;->getBackgroundColor(Landroid/content/res/TypedArray;)Landroid/content/res/ColorStateList;

    move-result-object v3

    invoke-virtual {p0, v0}, Lcom/google/android/play/cardview/CardViewGroupDelegates$CardViewGroupL;->getRadius(Landroid/content/res/TypedArray;)F

    move-result v4

    invoke-virtual {p0, v0}, Lcom/google/android/play/cardview/CardViewGroupDelegates$CardViewGroupL;->getInset(Landroid/content/res/TypedArray;)I

    move-result v5

    int-to-float v5, v5

    invoke-direct {v1, v3, v4, v5}, Lcom/google/android/play/cardview/RoundRectDrawable;-><init>(Landroid/content/res/ColorStateList;FF)V

    .local v1, "cardBackground":Landroid/graphics/drawable/Drawable;
    move-object v2, p1

    .line 85
    check-cast v2, Landroid/view/View;

    .line 86
    .local v2, "view":Landroid/view/View;
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/view/View;->setClipToOutline(Z)V

    .line 87
    invoke-virtual {p0, v0}, Lcom/google/android/play/cardview/CardViewGroupDelegates$CardViewGroupL;->getElevation(Landroid/content/res/TypedArray;)F

    move-result v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setElevation(F)V

    .line 88
    invoke-virtual {v2, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 89
    invoke-virtual {p0, v0}, Lcom/google/android/play/cardview/CardViewGroupDelegates$CardViewGroupL;->getClipToOutline(Landroid/content/res/TypedArray;)Z

    move-result v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setClipToOutline(Z)V

    .line 90
    return-void
.end method
