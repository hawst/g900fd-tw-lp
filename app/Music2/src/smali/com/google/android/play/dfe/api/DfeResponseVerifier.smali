.class public interface abstract Lcom/google/android/play/dfe/api/DfeResponseVerifier;
.super Ljava/lang/Object;
.source "DfeResponseVerifier.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/play/dfe/api/DfeResponseVerifier$DfeResponseVerifierException;
    }
.end annotation


# virtual methods
.method public abstract getSignatureRequest()Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/play/dfe/api/DfeResponseVerifier$DfeResponseVerifierException;
        }
    .end annotation
.end method

.method public abstract verify([BLjava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/play/dfe/api/DfeResponseVerifier$DfeResponseVerifierException;
        }
    .end annotation
.end method
