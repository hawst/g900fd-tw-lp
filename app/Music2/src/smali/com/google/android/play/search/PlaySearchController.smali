.class public final Lcom/google/android/play/search/PlaySearchController;
.super Ljava/lang/Object;
.source "PlaySearchController.java"


# instance fields
.field private mCurrentQuery:Ljava/lang/String;

.field private mCurrentSearchMode:I

.field private final mListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/play/search/PlaySearchListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    invoke-static {}, Lcom/google/android/play/utils/collections/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/play/search/PlaySearchController;->mListeners:Ljava/util/ArrayList;

    .line 16
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/play/search/PlaySearchController;->mCurrentQuery:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public addPlaySearchListener(Lcom/google/android/play/search/PlaySearchListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/google/android/play/search/PlaySearchListener;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchController;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 25
    return-void
.end method

.method public getMode()I
    .locals 1

    .prologue
    .line 38
    iget v0, p0, Lcom/google/android/play/search/PlaySearchController;->mCurrentSearchMode:I

    return v0
.end method

.method public getQuery()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchController;->mCurrentQuery:Ljava/lang/String;

    return-object v0
.end method

.method public removePlaySearchListener(Lcom/google/android/play/search/PlaySearchListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/google/android/play/search/PlaySearchListener;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchController;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 32
    return-void
.end method

.method public setMode(I)V
    .locals 2
    .param p1, "searchMode"    # I

    .prologue
    .line 53
    iget v1, p0, Lcom/google/android/play/search/PlaySearchController;->mCurrentSearchMode:I

    if-ne v1, p1, :cond_1

    .line 61
    :cond_0
    return-void

    .line 57
    :cond_1
    iput p1, p0, Lcom/google/android/play/search/PlaySearchController;->mCurrentSearchMode:I

    .line 58
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/google/android/play/search/PlaySearchController;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 59
    iget-object v1, p0, Lcom/google/android/play/search/PlaySearchController;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/play/search/PlaySearchListener;

    invoke-interface {v1, p1}, Lcom/google/android/play/search/PlaySearchListener;->onModeChanged(I)V

    .line 58
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public setQuery(Ljava/lang/String;)V
    .locals 2
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 68
    iget-object v1, p0, Lcom/google/android/play/search/PlaySearchController;->mCurrentQuery:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 76
    :cond_0
    return-void

    .line 72
    :cond_1
    iput-object p1, p0, Lcom/google/android/play/search/PlaySearchController;->mCurrentQuery:Ljava/lang/String;

    .line 73
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/google/android/play/search/PlaySearchController;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 74
    iget-object v1, p0, Lcom/google/android/play/search/PlaySearchController;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/play/search/PlaySearchListener;

    invoke-interface {v1, p1}, Lcom/google/android/play/search/PlaySearchListener;->onQueryChanged(Ljava/lang/String;)V

    .line 73
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
