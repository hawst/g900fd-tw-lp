.class public Lcom/google/android/play/search/PlaySearchPlate;
.super Landroid/widget/FrameLayout;
.source "PlaySearchPlate.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 17
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/play/search/PlaySearchPlate;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 18
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 21
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/play/search/PlaySearchPlate;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 22
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 25
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 26
    return-void
.end method


# virtual methods
.method public setPlaySearchController(Lcom/google/android/play/search/PlaySearchController;)V
    .locals 4
    .param p1, "playSearchController"    # Lcom/google/android/play/search/PlaySearchController;

    .prologue
    .line 32
    sget v3, Lcom/google/android/play/R$id;->navigation_button:I

    invoke-virtual {p0, v3}, Lcom/google/android/play/search/PlaySearchPlate;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/play/search/PlaySearchNavigationButton;

    .line 34
    .local v1, "navButton":Lcom/google/android/play/search/PlaySearchNavigationButton;
    sget v3, Lcom/google/android/play/R$id;->text_container:I

    invoke-virtual {p0, v3}, Lcom/google/android/play/search/PlaySearchPlate;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/play/search/PlaySearchPlateTextContainer;

    .line 36
    .local v2, "searchPlateTextContainer":Lcom/google/android/play/search/PlaySearchPlateTextContainer;
    sget v3, Lcom/google/android/play/R$id;->action_button:I

    invoke-virtual {p0, v3}, Lcom/google/android/play/search/PlaySearchPlate;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/search/PlaySearchActionButton;

    .line 38
    .local v0, "actionButton":Lcom/google/android/play/search/PlaySearchActionButton;
    invoke-virtual {v1, p1}, Lcom/google/android/play/search/PlaySearchNavigationButton;->setPlaySearchController(Lcom/google/android/play/search/PlaySearchController;)V

    .line 39
    invoke-virtual {v2, p1}, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->setPlaySearchController(Lcom/google/android/play/search/PlaySearchController;)V

    .line 40
    invoke-virtual {v0, p1}, Lcom/google/android/play/search/PlaySearchActionButton;->setPlaySearchController(Lcom/google/android/play/search/PlaySearchController;)V

    .line 41
    return-void
.end method
