.class public Lcom/google/android/play/playperf/measurements/memory/MemoryMeasurerFactory;
.super Ljava/lang/Object;
.source "MemoryMeasurerFactory.java"

# interfaces
.implements Lcom/google/android/play/playperf/measurements/MeasurerFactory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/play/playperf/measurements/MeasurerFactory",
        "<",
        "Lcom/google/android/play/playperf/measurements/memory/MemoryDelta;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getMeasurerInstance(Ljava/lang/String;)Lcom/google/android/play/playperf/measurements/Measurer;
    .locals 1
    .param p1, "sectionName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/android/play/playperf/measurements/Measurer",
            "<",
            "Lcom/google/android/play/playperf/measurements/memory/MemoryDelta;",
            ">;"
        }
    .end annotation

    .prologue
    .line 25
    new-instance v0, Lcom/google/android/play/playperf/measurements/memory/MemoryMeasurer;

    invoke-direct {v0, p1}, Lcom/google/android/play/playperf/measurements/memory/MemoryMeasurer;-><init>(Ljava/lang/String;)V

    return-object v0
.end method
