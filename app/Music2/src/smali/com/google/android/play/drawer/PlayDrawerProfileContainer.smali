.class Lcom/google/android/play/drawer/PlayDrawerProfileContainer;
.super Landroid/widget/LinearLayout;
.source "PlayDrawerProfileContainer.java"


# instance fields
.field private mSecondaryProfilesContainer:Lcom/google/android/play/drawer/ShrinkingItem;

.field private mSelectedProfileView:Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 23
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/play/drawer/PlayDrawerProfileContainer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 24
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    return-void
.end method


# virtual methods
.method public getSecondaryProfilesContainer()Lcom/google/android/play/drawer/ShrinkingItem;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerProfileContainer;->mSecondaryProfilesContainer:Lcom/google/android/play/drawer/ShrinkingItem;

    return-object v0
.end method

.method public getSelectedProfileView()Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerProfileContainer;->mSelectedProfileView:Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 32
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 34
    sget v0, Lcom/google/android/play/R$id;->selected_profile:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/drawer/PlayDrawerProfileContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;

    iput-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerProfileContainer;->mSelectedProfileView:Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;

    .line 35
    sget v0, Lcom/google/android/play/R$id;->secondary_profiles:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/drawer/PlayDrawerProfileContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/drawer/ShrinkingItem;

    iput-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerProfileContainer;->mSecondaryProfilesContainer:Lcom/google/android/play/drawer/ShrinkingItem;

    .line 36
    return-void
.end method
