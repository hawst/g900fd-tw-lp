.class public final Lcom/google/android/play/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/play/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final play_accessibility_search_plate_back_button:I = 0x7f0b003c

.field public static final play_accessibility_search_plate_clear:I = 0x7f0b0039

.field public static final play_accessibility_search_plate_menu_button:I = 0x7f0b003d

.field public static final play_accessibility_search_plate_voice_search_button:I = 0x7f0b003b

.field public static final play_content_description_hide_account_list_button:I = 0x7f0b0032

.field public static final play_content_description_page_indicator:I = 0x7f0b0036

.field public static final play_content_description_show_account_list_button:I = 0x7f0b0031

.field public static final play_download_is_requested:I = 0x7f0b0034

.field public static final play_download_not_requested:I = 0x7f0b0035

.field public static final play_drawer_close:I = 0x7f0b0030

.field public static final play_drawer_open:I = 0x7f0b002f

.field public static final play_drawer_title:I = 0x7f0b002e

.field public static final play_onboard_interstitial_grid_cell:I = 0x7f0b002d

.field public static final play_percent_downloaded:I = 0x7f0b0033
