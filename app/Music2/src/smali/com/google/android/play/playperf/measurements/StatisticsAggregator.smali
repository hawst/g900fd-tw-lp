.class public Lcom/google/android/play/playperf/measurements/StatisticsAggregator;
.super Ljava/lang/Object;
.source "StatisticsAggregator.java"


# instance fields
.field private mAvg:D

.field private mMax:Lcom/google/android/play/playperf/data/DataPoint;

.field private mMin:Lcom/google/android/play/playperf/data/DataPoint;

.field private mSampleCount:J

.field private mTimeMax:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static getRunningAvg(DJD)D
    .locals 4
    .param p0, "runningAvg"    # D
    .param p2, "sampleCount"    # J
    .param p4, "sample"    # D

    .prologue
    .line 44
    const-wide/16 v0, 0x1

    sub-long v0, p2, v0

    long-to-double v0, v0

    mul-double/2addr v0, p0

    add-double/2addr v0, p4

    long-to-double v2, p2

    div-double/2addr v0, v2

    return-wide v0
.end method


# virtual methods
.method public addSample(Lcom/google/android/play/playperf/data/DataPoint;)V
    .locals 6
    .param p1, "sample"    # Lcom/google/android/play/playperf/data/DataPoint;

    .prologue
    const-wide/16 v4, 0x1

    .line 26
    iget-wide v0, p0, Lcom/google/android/play/playperf/measurements/StatisticsAggregator;->mSampleCount:J

    add-long/2addr v0, v4

    iput-wide v0, p0, Lcom/google/android/play/playperf/measurements/StatisticsAggregator;->mSampleCount:J

    .line 29
    iget-wide v0, p0, Lcom/google/android/play/playperf/measurements/StatisticsAggregator;->mSampleCount:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/play/playperf/data/DataPoint;->getValue()D

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/play/playperf/measurements/StatisticsAggregator;->mMax:Lcom/google/android/play/playperf/data/DataPoint;

    invoke-virtual {v2}, Lcom/google/android/play/playperf/data/DataPoint;->getValue()D

    move-result-wide v2

    cmpl-double v0, v0, v2

    if-lez v0, :cond_1

    .line 30
    :cond_0
    iput-object p1, p0, Lcom/google/android/play/playperf/measurements/StatisticsAggregator;->mMax:Lcom/google/android/play/playperf/data/DataPoint;

    .line 32
    :cond_1
    iget-wide v0, p0, Lcom/google/android/play/playperf/measurements/StatisticsAggregator;->mSampleCount:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/android/play/playperf/data/DataPoint;->getValue()D

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/play/playperf/measurements/StatisticsAggregator;->mMin:Lcom/google/android/play/playperf/data/DataPoint;

    invoke-virtual {v2}, Lcom/google/android/play/playperf/data/DataPoint;->getValue()D

    move-result-wide v2

    cmpg-double v0, v0, v2

    if-gez v0, :cond_3

    .line 33
    :cond_2
    iput-object p1, p0, Lcom/google/android/play/playperf/measurements/StatisticsAggregator;->mMin:Lcom/google/android/play/playperf/data/DataPoint;

    .line 35
    :cond_3
    iget-wide v0, p0, Lcom/google/android/play/playperf/measurements/StatisticsAggregator;->mSampleCount:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/google/android/play/playperf/data/DataPoint;->getTimeStamp()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/play/playperf/measurements/StatisticsAggregator;->mTimeMax:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_5

    .line 36
    :cond_4
    invoke-virtual {p1}, Lcom/google/android/play/playperf/data/DataPoint;->getTimeStamp()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/play/playperf/measurements/StatisticsAggregator;->mTimeMax:J

    .line 40
    :cond_5
    iget-wide v0, p0, Lcom/google/android/play/playperf/measurements/StatisticsAggregator;->mAvg:D

    iget-wide v2, p0, Lcom/google/android/play/playperf/measurements/StatisticsAggregator;->mSampleCount:J

    invoke-virtual {p1}, Lcom/google/android/play/playperf/data/DataPoint;->getValue()D

    move-result-wide v4

    invoke-static/range {v0 .. v5}, Lcom/google/android/play/playperf/measurements/StatisticsAggregator;->getRunningAvg(DJD)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/play/playperf/measurements/StatisticsAggregator;->mAvg:D

    .line 41
    return-void
.end method

.method public clear()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 52
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/play/playperf/measurements/StatisticsAggregator;->mSampleCount:J

    .line 53
    iput-object v2, p0, Lcom/google/android/play/playperf/measurements/StatisticsAggregator;->mMax:Lcom/google/android/play/playperf/data/DataPoint;

    .line 54
    iput-object v2, p0, Lcom/google/android/play/playperf/measurements/StatisticsAggregator;->mMin:Lcom/google/android/play/playperf/data/DataPoint;

    .line 55
    return-void
.end method

.method public getAverage()Lcom/google/android/play/playperf/data/DataPoint;
    .locals 6

    .prologue
    .line 76
    iget-wide v0, p0, Lcom/google/android/play/playperf/measurements/StatisticsAggregator;->mSampleCount:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    new-instance v0, Lcom/google/android/play/playperf/data/DataPoint;

    iget-wide v2, p0, Lcom/google/android/play/playperf/measurements/StatisticsAggregator;->mTimeMax:J

    iget-wide v4, p0, Lcom/google/android/play/playperf/measurements/StatisticsAggregator;->mAvg:D

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/google/android/play/playperf/data/DataPoint;-><init>(JD)V

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getMax()Lcom/google/android/play/playperf/data/DataPoint;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/play/playperf/measurements/StatisticsAggregator;->mMax:Lcom/google/android/play/playperf/data/DataPoint;

    return-object v0
.end method

.method public getMin()Lcom/google/android/play/playperf/data/DataPoint;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/play/playperf/measurements/StatisticsAggregator;->mMin:Lcom/google/android/play/playperf/data/DataPoint;

    return-object v0
.end method
