.class Lcom/google/android/play/playperf/measurements/PollingTimer$1;
.super Ljava/util/TimerTask;
.source "PollingTimer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/play/playperf/measurements/PollingTimer;-><init>(J[Lcom/google/android/play/playperf/measurements/Poller;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/play/playperf/measurements/PollingTimer;


# direct methods
.method constructor <init>(Lcom/google/android/play/playperf/measurements/PollingTimer;)V
    .locals 0

    .prologue
    .line 31
    iput-object p1, p0, Lcom/google/android/play/playperf/measurements/PollingTimer$1;->this$0:Lcom/google/android/play/playperf/measurements/PollingTimer;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 34
    iget-object v2, p0, Lcom/google/android/play/playperf/measurements/PollingTimer$1;->this$0:Lcom/google/android/play/playperf/measurements/PollingTimer;

    # getter for: Lcom/google/android/play/playperf/measurements/PollingTimer;->mPollerSet:Ljava/util/Set;
    invoke-static {v2}, Lcom/google/android/play/playperf/measurements/PollingTimer;->access$000(Lcom/google/android/play/playperf/measurements/PollingTimer;)Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/play/playperf/measurements/Poller;

    .line 35
    .local v1, "poller":Lcom/google/android/play/playperf/measurements/Poller;
    iget-object v2, p0, Lcom/google/android/play/playperf/measurements/PollingTimer$1;->this$0:Lcom/google/android/play/playperf/measurements/PollingTimer;

    # getter for: Lcom/google/android/play/playperf/measurements/PollingTimer;->mSync:Ljava/lang/Object;
    invoke-static {v2}, Lcom/google/android/play/playperf/measurements/PollingTimer;->access$100(Lcom/google/android/play/playperf/measurements/PollingTimer;)Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3

    .line 36
    :try_start_0
    iget-object v2, p0, Lcom/google/android/play/playperf/measurements/PollingTimer$1;->this$0:Lcom/google/android/play/playperf/measurements/PollingTimer;

    # getter for: Lcom/google/android/play/playperf/measurements/PollingTimer;->mStopped:Z
    invoke-static {v2}, Lcom/google/android/play/playperf/measurements/PollingTimer;->access$200(Lcom/google/android/play/playperf/measurements/PollingTimer;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 37
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-interface {v1, v4, v5}, Lcom/google/android/play/playperf/measurements/Poller;->poll(J)V

    .line 39
    :cond_0
    monitor-exit v3

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 41
    .end local v1    # "poller":Lcom/google/android/play/playperf/measurements/Poller;
    :cond_1
    return-void
.end method
