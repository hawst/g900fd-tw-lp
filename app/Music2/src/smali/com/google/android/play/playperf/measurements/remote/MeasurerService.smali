.class Lcom/google/android/play/playperf/measurements/remote/MeasurerService;
.super Lcom/google/android/play/playperf/measurements/remote/IMeasurerService$Stub;
.source "MeasurerService.java"


# instance fields
.field private final mMeasurer:Lcom/google/android/play/playperf/measurements/Measurer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/playperf/measurements/Measurer",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/google/android/play/playperf/measurements/Measurer;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/play/playperf/measurements/Measurer",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 10
    .local p1, "measurer":Lcom/google/android/play/playperf/measurements/Measurer;, "Lcom/google/android/play/playperf/measurements/Measurer<*>;"
    invoke-direct {p0}, Lcom/google/android/play/playperf/measurements/remote/IMeasurerService$Stub;-><init>()V

    .line 11
    iput-object p1, p0, Lcom/google/android/play/playperf/measurements/remote/MeasurerService;->mMeasurer:Lcom/google/android/play/playperf/measurements/Measurer;

    .line 12
    return-void
.end method


# virtual methods
.method public getResult()Lcom/google/android/play/playperf/measurements/remote/RecordableWrapper;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 35
    new-instance v0, Lcom/google/android/play/playperf/measurements/remote/RecordableWrapper;

    iget-object v1, p0, Lcom/google/android/play/playperf/measurements/remote/MeasurerService;->mMeasurer:Lcom/google/android/play/playperf/measurements/Measurer;

    invoke-interface {v1}, Lcom/google/android/play/playperf/measurements/Measurer;->getResult()Lcom/google/android/play/playperf/measurements/Recordable;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/play/playperf/measurements/remote/RecordableWrapper;-><init>(Lcom/google/android/play/playperf/measurements/Recordable;)V

    return-object v0
.end method

.method public start()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 19
    iget-object v0, p0, Lcom/google/android/play/playperf/measurements/remote/MeasurerService;->mMeasurer:Lcom/google/android/play/playperf/measurements/Measurer;

    invoke-interface {v0}, Lcom/google/android/play/playperf/measurements/Measurer;->start()V

    .line 20
    return-void
.end method

.method public stop()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/play/playperf/measurements/remote/MeasurerService;->mMeasurer:Lcom/google/android/play/playperf/measurements/Measurer;

    invoke-interface {v0}, Lcom/google/android/play/playperf/measurements/Measurer;->stop()V

    .line 28
    return-void
.end method
