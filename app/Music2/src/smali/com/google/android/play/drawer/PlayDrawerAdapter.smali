.class Lcom/google/android/play/drawer/PlayDrawerAdapter;
.super Landroid/widget/BaseAdapter;
.source "PlayDrawerAdapter.java"


# static fields
.field private static final SUPPORTS_ANIMATIONS:Z


# instance fields
.field private final mAccountDocV2s:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/finsky/protos/DocumentV2$DocV2;",
            ">;"
        }
    .end annotation
.end field

.field private mAccountListAnimator:Landroid/animation/ObjectAnimator;

.field private mAccountListExpanded:Z

.field private final mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

.field private mContext:Landroid/content/Context;

.field private mCurrentAccount:Landroid/accounts/Account;

.field private mDownloadOnlyEnabled:Z

.field private mDownloadSwitchConfig:Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerDownloadSwitchConfig;

.field private mHasAccounts:Z

.field private final mInflater:Landroid/view/LayoutInflater;

.field private final mIsAccountDocLoaded:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mListView:Landroid/widget/ListView;

.field private mNonCurrentAccounts:[Landroid/accounts/Account;

.field private mPlayDfeApiProvider:Lcom/google/android/play/dfe/api/PlayDfeApiProvider;

.field private mPlayDrawerContentClickListener:Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerContentClickListener;

.field private mPlayDrawerLayout:Lcom/google/android/play/drawer/PlayDrawerLayout;

.field private final mPrimaryActions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;",
            ">;"
        }
    .end annotation
.end field

.field private mProfileContainerPosition:I

.field private final mSecondaryActions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;",
            ">;"
        }
    .end annotation
.end field

.field private mSecondaryProfilesContainer:Lcom/google/android/play/drawer/ShrinkingItem;

.field private mSelectedProfileView:Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;

.field private mShowDownloadOnlyToggle:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 54
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->SUPPORTS_ANIMATIONS:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;ZLcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerContentClickListener;Lcom/google/android/play/dfe/api/PlayDfeApiProvider;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/play/drawer/PlayDrawerLayout;Landroid/widget/ListView;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "isAccountListExpanded"    # Z
    .param p3, "playDrawerContentClickListener"    # Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerContentClickListener;
    .param p4, "playDfeApiProvider"    # Lcom/google/android/play/dfe/api/PlayDfeApiProvider;
    .param p5, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;
    .param p6, "playDrawerLayout"    # Lcom/google/android/play/drawer/PlayDrawerLayout;
    .param p7, "listView"    # Landroid/widget/ListView;

    .prologue
    .line 97
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 79
    invoke-static {}, Lcom/google/android/play/utils/collections/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mPrimaryActions:Ljava/util/List;

    .line 80
    invoke-static {}, Lcom/google/android/play/utils/collections/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mSecondaryActions:Ljava/util/List;

    .line 86
    invoke-static {}, Lcom/google/android/play/utils/collections/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mAccountDocV2s:Ljava/util/Map;

    .line 87
    invoke-static {}, Lcom/google/android/play/utils/collections/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mIsAccountDocLoaded:Ljava/util/Set;

    .line 99
    iput-object p1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mContext:Landroid/content/Context;

    .line 100
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 101
    iput-object p4, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mPlayDfeApiProvider:Lcom/google/android/play/dfe/api/PlayDfeApiProvider;

    .line 102
    iput-object p5, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    .line 103
    iput-object p3, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mPlayDrawerContentClickListener:Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerContentClickListener;

    .line 104
    iput-object p6, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mPlayDrawerLayout:Lcom/google/android/play/drawer/PlayDrawerLayout;

    .line 105
    iput-object p7, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mListView:Landroid/widget/ListView;

    .line 107
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mProfileContainerPosition:I

    .line 108
    iput-boolean p2, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mAccountListExpanded:Z

    .line 109
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/play/drawer/PlayDrawerAdapter;)Ljava/util/Map;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/play/drawer/PlayDrawerAdapter;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mAccountDocV2s:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/play/drawer/PlayDrawerAdapter;)Ljava/util/Set;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/play/drawer/PlayDrawerAdapter;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mIsAccountDocLoaded:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/android/play/drawer/PlayDrawerAdapter;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/play/drawer/PlayDrawerAdapter;

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->toggleAccountsList()V

    return-void
.end method

.method static synthetic access$1100(Lcom/google/android/play/drawer/PlayDrawerAdapter;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/play/drawer/PlayDrawerAdapter;

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->loadAllSecondaryAccountDocV2sOnce()V

    return-void
.end method

.method static synthetic access$1202(Lcom/google/android/play/drawer/PlayDrawerAdapter;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/play/drawer/PlayDrawerAdapter;
    .param p1, "x1"    # Z

    .prologue
    .line 53
    iput-boolean p1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mDownloadOnlyEnabled:Z

    return p1
.end method

.method static synthetic access$200(Lcom/google/android/play/drawer/PlayDrawerAdapter;)Landroid/accounts/Account;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/play/drawer/PlayDrawerAdapter;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mCurrentAccount:Landroid/accounts/Account;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/play/drawer/PlayDrawerAdapter;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/play/drawer/PlayDrawerAdapter;

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->isProfileContainerVisible()Z

    move-result v0

    return v0
.end method

.method static synthetic access$400(Lcom/google/android/play/drawer/PlayDrawerAdapter;)Lcom/google/android/play/image/BitmapLoader;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/play/drawer/PlayDrawerAdapter;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/play/drawer/PlayDrawerAdapter;)Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/play/drawer/PlayDrawerAdapter;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mSelectedProfileView:Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/play/drawer/PlayDrawerAdapter;)Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerContentClickListener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/play/drawer/PlayDrawerAdapter;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mPlayDrawerContentClickListener:Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerContentClickListener;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/play/drawer/PlayDrawerAdapter;)Lcom/google/android/play/drawer/PlayDrawerLayout;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/play/drawer/PlayDrawerAdapter;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mPlayDrawerLayout:Lcom/google/android/play/drawer/PlayDrawerLayout;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/play/drawer/PlayDrawerAdapter;)[Landroid/accounts/Account;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/play/drawer/PlayDrawerAdapter;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mNonCurrentAccounts:[Landroid/accounts/Account;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/play/drawer/PlayDrawerAdapter;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/play/drawer/PlayDrawerAdapter;

    .prologue
    .line 53
    iget-boolean v0, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mAccountListExpanded:Z

    return v0
.end method

.method private getDownloadToggleView(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1, "convertView"    # Landroid/view/View;
    .param p2, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 626
    if-nez p1, :cond_0

    .line 628
    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mInflater:Landroid/view/LayoutInflater;

    sget v2, Lcom/google/android/play/R$layout;->play_drawer_download_toggle:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;

    .line 633
    .local v0, "toggleRow":Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;
    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mDownloadSwitchConfig:Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerDownloadSwitchConfig;

    invoke-virtual {v0, v1}, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->configure(Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerDownloadSwitchConfig;)V

    .line 634
    new-instance v1, Lcom/google/android/play/drawer/PlayDrawerAdapter$10;

    invoke-direct {v1, p0}, Lcom/google/android/play/drawer/PlayDrawerAdapter$10;-><init>(Lcom/google/android/play/drawer/PlayDrawerAdapter;)V

    invoke-virtual {v0, v1}, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->setOnCheckedChangeListener(Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow$OnCheckedChangeListener;)V

    .line 646
    :goto_0
    iget-boolean v1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mDownloadOnlyEnabled:Z

    invoke-virtual {v0, v1}, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->setCheckedNoCallbacks(Z)V

    .line 648
    return-object v0

    .end local v0    # "toggleRow":Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;
    :cond_0
    move-object v0, p1

    .line 644
    check-cast v0, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;

    .restart local v0    # "toggleRow":Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;
    goto :goto_0
.end method

.method private getPrimaryActionView(Landroid/view/View;Landroid/view/ViewGroup;Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;ZZ)Landroid/view/View;
    .locals 7
    .param p1, "convertView"    # Landroid/view/View;
    .param p2, "parent"    # Landroid/view/ViewGroup;
    .param p3, "primaryAction"    # Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;
    .param p4, "active"    # Z
    .param p5, "disabled"    # Z

    .prologue
    const/4 v6, 0x0

    .line 547
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 550
    .local v3, "res":Landroid/content/res/Resources;
    if-eqz p4, :cond_0

    .line 551
    sget v2, Lcom/google/android/play/R$layout;->play_drawer_primary_action_active:I

    .line 558
    .local v2, "layoutId":I
    :goto_0
    if-eqz p1, :cond_2

    move-object v4, p1

    :goto_1
    check-cast v4, Landroid/widget/TextView;

    move-object v0, v4

    check-cast v0, Landroid/widget/TextView;

    .line 560
    .local v0, "destinationRow":Landroid/widget/TextView;
    iget-object v4, p3, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;->actionText:Ljava/lang/String;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 561
    iget v4, p3, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;->iconResId:I

    if-lez v4, :cond_5

    .line 563
    if-eqz p4, :cond_3

    iget v4, p3, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;->activeIconResId:I

    if-lez v4, :cond_3

    .line 564
    iget v4, p3, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;->activeIconResId:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 568
    .local v1, "drawable":Landroid/graphics/drawable/Drawable;
    :goto_2
    if-eqz p5, :cond_4

    .line 569
    const/16 v4, 0x42

    invoke-virtual {v1, v4}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 573
    :goto_3
    iget-object v4, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/google/android/play/utils/PlayUtils;->useLtr(Landroid/content/Context;)Z

    move-result v4

    invoke-static {v0, v1, v4}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->setDrawableStart(Landroid/widget/TextView;Landroid/graphics/drawable/Drawable;Z)V

    .line 578
    .end local v1    # "drawable":Landroid/graphics/drawable/Drawable;
    :goto_4
    if-eqz p4, :cond_6

    iget v4, p3, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;->activeTextColorResId:I

    if-lez v4, :cond_6

    .line 579
    iget v4, p3, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;->activeTextColorResId:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 585
    :goto_5
    new-instance v4, Lcom/google/android/play/drawer/PlayDrawerAdapter$8;

    invoke-direct {v4, p0, p3}, Lcom/google/android/play/drawer/PlayDrawerAdapter$8;-><init>(Lcom/google/android/play/drawer/PlayDrawerAdapter;Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;)V

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 593
    iget-object v4, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/google/android/play/utils/PlayUtils;->useLtr(Landroid/content/Context;)Z

    move-result v4

    invoke-static {v0, v4}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->setTextAlignmentStart(Landroid/widget/TextView;Z)V

    .line 594
    return-object v0

    .line 552
    .end local v0    # "destinationRow":Landroid/widget/TextView;
    .end local v2    # "layoutId":I
    :cond_0
    if-eqz p5, :cond_1

    .line 553
    sget v2, Lcom/google/android/play/R$layout;->play_drawer_primary_action_disabled:I

    .restart local v2    # "layoutId":I
    goto :goto_0

    .line 555
    .end local v2    # "layoutId":I
    :cond_1
    sget v2, Lcom/google/android/play/R$layout;->play_drawer_primary_action_regular:I

    .restart local v2    # "layoutId":I
    goto :goto_0

    .line 558
    :cond_2
    iget-object v4, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mInflater:Landroid/view/LayoutInflater;

    const/4 v5, 0x0

    invoke-virtual {v4, v2, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    goto :goto_1

    .line 566
    .restart local v0    # "destinationRow":Landroid/widget/TextView;
    :cond_3
    iget v4, p3, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;->iconResId:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .restart local v1    # "drawable":Landroid/graphics/drawable/Drawable;
    goto :goto_2

    .line 571
    :cond_4
    const/16 v4, 0xff

    invoke-virtual {v1, v4}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    goto :goto_3

    .line 575
    .end local v1    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_5
    invoke-virtual {v0, v6, v6, v6, v6}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto :goto_4

    .line 580
    :cond_6
    if-eqz p5, :cond_7

    .line 581
    sget v4, Lcom/google/android/play/R$color;->play_disabled_grey:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_5

    .line 583
    :cond_7
    sget v4, Lcom/google/android/play/R$color;->play_fg_primary:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_5
.end method

.method private getPrimaryActionsTopSpacing(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1, "convertView"    # Landroid/view/View;
    .param p2, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 540
    if-eqz p1, :cond_0

    move-object v0, p1

    .line 542
    .local v0, "spacingView":Landroid/view/View;
    :goto_0
    return-object v0

    .line 540
    .end local v0    # "spacingView":Landroid/view/View;
    :cond_0
    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mInflater:Landroid/view/LayoutInflater;

    sget v2, Lcom/google/android/play/R$layout;->play_drawer_primary_actions_top_spacing:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method private getProfileContainer(Landroid/view/View;Landroid/view/ViewGroup;I)Landroid/view/View;
    .locals 12
    .param p1, "convertView"    # Landroid/view/View;
    .param p2, "parent"    # Landroid/view/ViewGroup;
    .param p3, "position"    # I

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 312
    if-eqz p1, :cond_0

    move-object v7, p1

    :goto_0
    check-cast v7, Lcom/google/android/play/drawer/PlayDrawerProfileContainer;

    move-object v5, v7

    check-cast v5, Lcom/google/android/play/drawer/PlayDrawerProfileContainer;

    .line 316
    .local v5, "profileContainer":Lcom/google/android/play/drawer/PlayDrawerProfileContainer;
    invoke-virtual {v5}, Lcom/google/android/play/drawer/PlayDrawerProfileContainer;->getSelectedProfileView()Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;

    move-result-object v7

    iput-object v7, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mSelectedProfileView:Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;

    .line 317
    invoke-virtual {v5}, Lcom/google/android/play/drawer/PlayDrawerProfileContainer;->getSecondaryProfilesContainer()Lcom/google/android/play/drawer/ShrinkingItem;

    move-result-object v7

    iput-object v7, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mSecondaryProfilesContainer:Lcom/google/android/play/drawer/ShrinkingItem;

    .line 318
    iput p3, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mProfileContainerPosition:I

    .line 322
    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mCurrentAccount:Landroid/accounts/Account;

    .line 323
    .local v1, "finalCurrentAccount":Landroid/accounts/Account;
    iget-object v7, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mCurrentAccount:Landroid/accounts/Account;

    iget-object v2, v7, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 328
    .local v2, "finalCurrentAccountName":Ljava/lang/String;
    iget-object v10, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mSelectedProfileView:Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;

    iget-object v7, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mAccountDocV2s:Ljava/util/Map;

    invoke-interface {v7, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v11, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    invoke-virtual {v10, v7, v2, v11}, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->configure(Lcom/google/android/finsky/protos/DocumentV2$DocV2;Ljava/lang/String;Lcom/google/android/play/image/BitmapLoader;)V

    .line 334
    iget-object v7, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mPlayDfeApiProvider:Lcom/google/android/play/dfe/api/PlayDfeApiProvider;

    invoke-interface {v7, v1}, Lcom/google/android/play/dfe/api/PlayDfeApiProvider;->getPlayDfeApi(Landroid/accounts/Account;)Lcom/google/android/play/dfe/api/PlayDfeApi;

    move-result-object v7

    new-instance v10, Lcom/google/android/play/drawer/PlayDrawerAdapter$1;

    invoke-direct {v10, p0, v2}, Lcom/google/android/play/drawer/PlayDrawerAdapter$1;-><init>(Lcom/google/android/play/drawer/PlayDrawerAdapter;Ljava/lang/String;)V

    new-instance v11, Lcom/google/android/play/drawer/PlayDrawerAdapter$2;

    invoke-direct {v11, p0}, Lcom/google/android/play/drawer/PlayDrawerAdapter$2;-><init>(Lcom/google/android/play/drawer/PlayDrawerAdapter;)V

    invoke-interface {v7, v10, v11, v8}, Lcom/google/android/play/dfe/api/PlayDfeApi;->getPlusProfile(Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;Z)Lcom/android/volley/Request;

    .line 370
    iget-object v7, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mSelectedProfileView:Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;

    iget-boolean v10, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mAccountListExpanded:Z

    invoke-virtual {v7, v10}, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->setAccountListExpanded(Z)V

    .line 372
    iget-object v10, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mSelectedProfileView:Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;

    iget-object v7, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mNonCurrentAccounts:[Landroid/accounts/Account;

    array-length v7, v7

    if-lez v7, :cond_1

    move v7, v8

    :goto_1
    invoke-virtual {v10, v7}, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->setAccountListEnabled(Z)V

    .line 374
    iget-object v7, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mSelectedProfileView:Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;

    new-instance v8, Lcom/google/android/play/drawer/PlayDrawerAdapter$3;

    invoke-direct {v8, p0}, Lcom/google/android/play/drawer/PlayDrawerAdapter$3;-><init>(Lcom/google/android/play/drawer/PlayDrawerAdapter;)V

    invoke-virtual {v7, v8}, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 387
    iget-object v7, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mSecondaryProfilesContainer:Lcom/google/android/play/drawer/ShrinkingItem;

    invoke-virtual {v7}, Lcom/google/android/play/drawer/ShrinkingItem;->removeAllViews()V

    .line 388
    iget-object v8, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mSecondaryProfilesContainer:Lcom/google/android/play/drawer/ShrinkingItem;

    iget-boolean v7, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mAccountListExpanded:Z

    if-eqz v7, :cond_2

    const/high16 v7, 0x3f800000    # 1.0f

    :goto_2
    invoke-virtual {v8, v7}, Lcom/google/android/play/drawer/ShrinkingItem;->setAnimatedHeightFraction(F)V

    .line 390
    new-instance v4, Lcom/google/android/play/drawer/PlayDrawerAdapter$4;

    invoke-direct {v4, p0}, Lcom/google/android/play/drawer/PlayDrawerAdapter$4;-><init>(Lcom/google/android/play/drawer/PlayDrawerAdapter;)V

    .line 409
    .local v4, "mChildClickListener":Landroid/view/View$OnClickListener;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_3
    iget-object v7, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mNonCurrentAccounts:[Landroid/accounts/Account;

    array-length v7, v7

    if-ge v3, v7, :cond_3

    .line 410
    iget-object v7, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mInflater:Landroid/view/LayoutInflater;

    sget v8, Lcom/google/android/play/R$layout;->play_drawer_account_row:I

    invoke-virtual {v7, v8, v5, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/google/android/play/drawer/PlayDrawerAccountRow;

    .line 413
    .local v6, "secondaryAccountRow":Lcom/google/android/play/drawer/PlayDrawerAccountRow;
    iget-object v7, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mNonCurrentAccounts:[Landroid/accounts/Account;

    aget-object v7, v7, v3

    iget-object v0, v7, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 415
    .local v0, "accountName":Ljava/lang/String;
    iget-object v7, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mAccountDocV2s:Ljava/util/Map;

    invoke-interface {v7, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v8, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    invoke-virtual {v6, v7, v0, v8}, Lcom/google/android/play/drawer/PlayDrawerAccountRow;->bind(Lcom/google/android/finsky/protos/DocumentV2$DocV2;Ljava/lang/String;Lcom/google/android/play/image/BitmapLoader;)V

    .line 417
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/google/android/play/drawer/PlayDrawerAccountRow;->setTag(Ljava/lang/Object;)V

    .line 418
    invoke-virtual {v6, v4}, Lcom/google/android/play/drawer/PlayDrawerAccountRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 420
    iget-object v7, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mSecondaryProfilesContainer:Lcom/google/android/play/drawer/ShrinkingItem;

    invoke-virtual {v7, v6}, Lcom/google/android/play/drawer/ShrinkingItem;->addView(Landroid/view/View;)V

    .line 409
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 312
    .end local v0    # "accountName":Ljava/lang/String;
    .end local v1    # "finalCurrentAccount":Landroid/accounts/Account;
    .end local v2    # "finalCurrentAccountName":Ljava/lang/String;
    .end local v3    # "i":I
    .end local v4    # "mChildClickListener":Landroid/view/View$OnClickListener;
    .end local v5    # "profileContainer":Lcom/google/android/play/drawer/PlayDrawerProfileContainer;
    .end local v6    # "secondaryAccountRow":Lcom/google/android/play/drawer/PlayDrawerAccountRow;
    :cond_0
    iget-object v7, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mInflater:Landroid/view/LayoutInflater;

    sget v10, Lcom/google/android/play/R$layout;->play_drawer_profile_container:I

    invoke-virtual {v7, v10, p2, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v7

    goto/16 :goto_0

    .restart local v1    # "finalCurrentAccount":Landroid/accounts/Account;
    .restart local v2    # "finalCurrentAccountName":Ljava/lang/String;
    .restart local v5    # "profileContainer":Lcom/google/android/play/drawer/PlayDrawerProfileContainer;
    :cond_1
    move v7, v9

    .line 372
    goto :goto_1

    .line 388
    :cond_2
    const/4 v7, 0x0

    goto :goto_2

    .line 425
    .restart local v3    # "i":I
    .restart local v4    # "mChildClickListener":Landroid/view/View$OnClickListener;
    :cond_3
    iget-object v7, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mSelectedProfileView:Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;

    new-instance v8, Lcom/google/android/play/drawer/PlayDrawerAdapter$5;

    invoke-direct {v8, p0}, Lcom/google/android/play/drawer/PlayDrawerAdapter$5;-><init>(Lcom/google/android/play/drawer/PlayDrawerAdapter;)V

    invoke-virtual {v7, v8}, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->setAccountTogglerListener(Landroid/view/View$OnClickListener;)V

    .line 446
    iget-boolean v7, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mAccountListExpanded:Z

    if-eqz v7, :cond_4

    .line 447
    invoke-direct {p0}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->loadAllSecondaryAccountDocV2sOnce()V

    .line 450
    :cond_4
    return-object v5
.end method

.method private getSecondaryActionView(Landroid/view/View;Landroid/view/ViewGroup;Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;)Landroid/view/View;
    .locals 4
    .param p1, "convertView"    # Landroid/view/View;
    .param p2, "parent"    # Landroid/view/ViewGroup;
    .param p3, "secondaryAction"    # Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;

    .prologue
    .line 606
    if-eqz p1, :cond_0

    move-object v1, p1

    :goto_0
    check-cast v1, Landroid/widget/TextView;

    move-object v0, v1

    check-cast v0, Landroid/widget/TextView;

    .line 610
    .local v0, "secondaryRow":Landroid/widget/TextView;
    iget-object v1, p3, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;->actionText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 612
    new-instance v1, Lcom/google/android/play/drawer/PlayDrawerAdapter$9;

    invoke-direct {v1, p0, p3}, Lcom/google/android/play/drawer/PlayDrawerAdapter$9;-><init>(Lcom/google/android/play/drawer/PlayDrawerAdapter;Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 620
    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/play/utils/PlayUtils;->useLtr(Landroid/content/Context;)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->setTextAlignmentStart(Landroid/widget/TextView;Z)V

    .line 621
    return-object v0

    .line 606
    .end local v0    # "secondaryRow":Landroid/widget/TextView;
    :cond_0
    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mInflater:Landroid/view/LayoutInflater;

    sget v2, Lcom/google/android/play/R$layout;->play_drawer_secondary_action:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    goto :goto_0
.end method

.method private getSecondaryActionsTopSeparator(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1, "convertView"    # Landroid/view/View;
    .param p2, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 598
    if-eqz p1, :cond_0

    move-object v0, p1

    .line 601
    .local v0, "separatorView":Landroid/view/View;
    :goto_0
    return-object v0

    .line 598
    .end local v0    # "separatorView":Landroid/view/View;
    :cond_0
    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mInflater:Landroid/view/LayoutInflater;

    sget v2, Lcom/google/android/play/R$layout;->play_drawer_secondary_actions_top_separator:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method private isProfileContainerVisible()Z
    .locals 4

    .prologue
    .line 658
    iget-object v3, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mListView:Landroid/widget/ListView;

    invoke-virtual {v3}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v0

    .line 659
    .local v0, "firstVisiblePosition":I
    iget-object v3, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mListView:Landroid/widget/ListView;

    invoke-virtual {v3}, Landroid/widget/ListView;->getChildCount()I

    move-result v3

    add-int/2addr v3, v0

    add-int/lit8 v2, v3, -0x1

    .line 660
    .local v2, "lastVisiblePosition":I
    iget v3, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mProfileContainerPosition:I

    if-lt v3, v0, :cond_0

    iget v3, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mProfileContainerPosition:I

    if-gt v3, v2, :cond_0

    const/4 v1, 0x1

    .line 662
    .local v1, "isProfileContainerVisible":Z
    :goto_0
    return v1

    .line 660
    .end local v1    # "isProfileContainerVisible":Z
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private loadAllSecondaryAccountDocV2sOnce()V
    .locals 7

    .prologue
    .line 454
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mNonCurrentAccounts:[Landroid/accounts/Account;

    array-length v3, v3

    if-ge v2, v3, :cond_1

    .line 455
    iget-object v3, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mNonCurrentAccounts:[Landroid/accounts/Account;

    aget-object v0, v3, v2

    .line 456
    .local v0, "account":Landroid/accounts/Account;
    iget-object v1, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 457
    .local v1, "accountName":Ljava/lang/String;
    iget-object v3, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mIsAccountDocLoaded:Ljava/util/Set;

    invoke-interface {v3, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 454
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 461
    :cond_0
    iget-object v3, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mPlayDfeApiProvider:Lcom/google/android/play/dfe/api/PlayDfeApiProvider;

    invoke-interface {v3, v0}, Lcom/google/android/play/dfe/api/PlayDfeApiProvider;->getPlayDfeApi(Landroid/accounts/Account;)Lcom/google/android/play/dfe/api/PlayDfeApi;

    move-result-object v3

    new-instance v4, Lcom/google/android/play/drawer/PlayDrawerAdapter$6;

    invoke-direct {v4, p0, v1}, Lcom/google/android/play/drawer/PlayDrawerAdapter$6;-><init>(Lcom/google/android/play/drawer/PlayDrawerAdapter;Ljava/lang/String;)V

    new-instance v5, Lcom/google/android/play/drawer/PlayDrawerAdapter$7;

    invoke-direct {v5, p0}, Lcom/google/android/play/drawer/PlayDrawerAdapter$7;-><init>(Lcom/google/android/play/drawer/PlayDrawerAdapter;)V

    const/4 v6, 0x1

    invoke-interface {v3, v4, v5, v6}, Lcom/google/android/play/dfe/api/PlayDfeApi;->getPlusProfile(Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;Z)Lcom/android/volley/Request;

    goto :goto_1

    .line 493
    .end local v0    # "account":Landroid/accounts/Account;
    .end local v1    # "accountName":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method public static setDrawableStart(Landroid/widget/TextView;Landroid/graphics/drawable/Drawable;Z)V
    .locals 1
    .param p0, "textView"    # Landroid/widget/TextView;
    .param p1, "drawable"    # Landroid/graphics/drawable/Drawable;
    .param p2, "useLtr"    # Z

    .prologue
    const/4 v0, 0x0

    .line 676
    if-eqz p2, :cond_0

    .line 677
    invoke-virtual {p0, p1, v0, v0, v0}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 681
    :goto_0
    return-void

    .line 679
    :cond_0
    invoke-virtual {p0, v0, v0, p1, v0}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public static setTextAlignmentStart(Landroid/widget/TextView;Z)V
    .locals 1
    .param p0, "textView"    # Landroid/widget/TextView;
    .param p1, "useLtr"    # Z

    .prologue
    .line 692
    if-eqz p1, :cond_0

    .line 693
    const/16 v0, 0x13

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setGravity(I)V

    .line 697
    :goto_0
    return-void

    .line 695
    :cond_0
    const/16 v0, 0x15

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setGravity(I)V

    goto :goto_0
.end method

.method private toggleAccountsList()V
    .locals 8
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 501
    iget-boolean v3, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mAccountListExpanded:Z

    if-nez v3, :cond_0

    move v3, v4

    :goto_0
    iput-boolean v3, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mAccountListExpanded:Z

    .line 503
    invoke-direct {p0}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->isProfileContainerVisible()Z

    move-result v3

    if-nez v3, :cond_1

    .line 507
    invoke-virtual {p0}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->notifyDataSetChanged()V

    .line 537
    :goto_1
    return-void

    :cond_0
    move v3, v5

    .line 501
    goto :goto_0

    .line 513
    :cond_1
    iget-object v3, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mSelectedProfileView:Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;

    iget-boolean v6, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mAccountListExpanded:Z

    invoke-virtual {v3, v6}, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->setAccountListExpanded(Z)V

    .line 515
    iget-object v3, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mSecondaryProfilesContainer:Lcom/google/android/play/drawer/ShrinkingItem;

    invoke-virtual {v3}, Lcom/google/android/play/drawer/ShrinkingItem;->getAnimatedHeightFraction()F

    move-result v2

    .line 516
    .local v2, "start":F
    iget-boolean v3, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mAccountListExpanded:Z

    if-eqz v3, :cond_3

    const/high16 v0, 0x3f800000    # 1.0f

    .line 517
    .local v0, "end":F
    :goto_2
    sget-boolean v3, Lcom/google/android/play/drawer/PlayDrawerAdapter;->SUPPORTS_ANIMATIONS:Z

    if-eqz v3, :cond_4

    .line 518
    iget-object v3, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mSecondaryProfilesContainer:Lcom/google/android/play/drawer/ShrinkingItem;

    const-string v6, "animatedHeightFraction"

    const/4 v7, 0x2

    new-array v7, v7, [F

    aput v2, v7, v5

    aput v0, v7, v4

    invoke-static {v3, v6, v7}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 523
    .local v1, "heightAnimator":Landroid/animation/ObjectAnimator;
    const/high16 v3, 0x43160000    # 150.0f

    sub-float v4, v2, v0

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    mul-float/2addr v3, v4

    float-to-long v4, v3

    invoke-virtual {v1, v4, v5}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 526
    iget-object v3, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mAccountListAnimator:Landroid/animation/ObjectAnimator;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mAccountListAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v3}, Landroid/animation/ObjectAnimator;->isRunning()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 527
    iget-object v3, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mAccountListAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v3}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 529
    :cond_2
    iput-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mAccountListAnimator:Landroid/animation/ObjectAnimator;

    .line 531
    new-instance v3, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v3}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v1, v3}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 532
    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->start()V

    goto :goto_1

    .line 516
    .end local v0    # "end":F
    .end local v1    # "heightAnimator":Landroid/animation/ObjectAnimator;
    :cond_3
    const/4 v0, 0x0

    goto :goto_2

    .line 534
    .restart local v0    # "end":F
    :cond_4
    iget-object v3, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mSecondaryProfilesContainer:Lcom/google/android/play/drawer/ShrinkingItem;

    invoke-virtual {v3, v0}, Lcom/google/android/play/drawer/ShrinkingItem;->setAnimatedHeightFraction(F)V

    goto :goto_1
.end method


# virtual methods
.method public collapseAccountListIfNeeded()V
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mNonCurrentAccounts:[Landroid/accounts/Account;

    array-length v0, v0

    if-lez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mAccountListExpanded:Z

    if-eqz v0, :cond_0

    .line 165
    invoke-direct {p0}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->toggleAccountsList()V

    .line 167
    :cond_0
    return-void
.end method

.method public getCount()I
    .locals 2

    .prologue
    .line 172
    iget-boolean v1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mHasAccounts:Z

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    .line 174
    .local v0, "result":I
    :goto_0
    add-int/lit8 v0, v0, 0x1

    .line 176
    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mPrimaryActions:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    .line 178
    add-int/lit8 v0, v0, 0x1

    .line 180
    iget-boolean v1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mShowDownloadOnlyToggle:Z

    if-eqz v1, :cond_0

    .line 181
    add-int/lit8 v0, v0, 0x1

    .line 184
    :cond_0
    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mSecondaryActions:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    .line 185
    return v0

    .line 172
    .end local v0    # "result":I
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 3
    .param p1, "position"    # I

    .prologue
    const/4 v1, 0x0

    .line 190
    iget-boolean v2, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mHasAccounts:Z

    if-eqz v2, :cond_2

    .line 191
    if-nez p1, :cond_1

    .line 192
    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mCurrentAccount:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 223
    :cond_0
    :goto_0
    return-object v1

    .line 194
    :cond_1
    add-int/lit8 p1, p1, -0x1

    .line 198
    :cond_2
    if-eqz p1, :cond_0

    .line 201
    add-int/lit8 p1, p1, -0x1

    .line 204
    iget-object v2, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mPrimaryActions:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    .line 205
    .local v0, "primaryActionsCount":I
    if-ge p1, v0, :cond_3

    .line 206
    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mPrimaryActions:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    goto :goto_0

    .line 208
    :cond_3
    sub-int/2addr p1, v0

    .line 210
    iget-boolean v2, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mShowDownloadOnlyToggle:Z

    if-eqz v2, :cond_5

    .line 211
    if-nez p1, :cond_4

    .line 212
    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mDownloadSwitchConfig:Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerDownloadSwitchConfig;

    goto :goto_0

    .line 214
    :cond_4
    add-int/lit8 p1, p1, -0x1

    .line 218
    :cond_5
    if-eqz p1, :cond_0

    .line 221
    add-int/lit8 p1, p1, -0x1

    .line 223
    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mSecondaryActions:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 228
    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 233
    iget-boolean v2, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mHasAccounts:Z

    if-eqz v2, :cond_1

    .line 234
    if-nez p1, :cond_0

    .line 235
    const/4 v2, 0x0

    .line 272
    :goto_0
    return v2

    .line 237
    :cond_0
    add-int/lit8 p1, p1, -0x1

    .line 240
    :cond_1
    if-nez p1, :cond_2

    .line 241
    const/4 v2, 0x1

    goto :goto_0

    .line 243
    :cond_2
    add-int/lit8 p1, p1, -0x1

    .line 245
    iget-object v2, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mPrimaryActions:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    .line 246
    .local v1, "primaryActionsCount":I
    if-ge p1, v1, :cond_5

    .line 247
    iget-object v2, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mPrimaryActions:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;

    .line 248
    .local v0, "action":Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;
    iget-boolean v2, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mShowDownloadOnlyToggle:Z

    if-eqz v2, :cond_3

    iget-boolean v2, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mDownloadOnlyEnabled:Z

    if-eqz v2, :cond_3

    iget-boolean v2, v0, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;->isAvailableInDownloadOnly:Z

    if-nez v2, :cond_3

    .line 250
    const/4 v2, 0x4

    goto :goto_0

    .line 251
    :cond_3
    iget-boolean v2, v0, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;->isActive:Z

    if-eqz v2, :cond_4

    .line 252
    const/4 v2, 0x2

    goto :goto_0

    .line 254
    :cond_4
    const/4 v2, 0x3

    goto :goto_0

    .line 257
    .end local v0    # "action":Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;
    :cond_5
    iget-object v2, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mPrimaryActions:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    sub-int/2addr p1, v2

    .line 259
    if-nez p1, :cond_6

    .line 260
    const/4 v2, 0x5

    goto :goto_0

    .line 262
    :cond_6
    add-int/lit8 p1, p1, -0x1

    .line 264
    iget-boolean v2, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mShowDownloadOnlyToggle:Z

    if-eqz v2, :cond_8

    .line 265
    if-nez p1, :cond_7

    .line 267
    const/4 v2, 0x7

    goto :goto_0

    .line 269
    :cond_7
    add-int/lit8 p1, p1, -0x1

    .line 272
    :cond_8
    const/4 v2, 0x6

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 282
    invoke-virtual {p0, p1}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->getItemViewType(I)I

    move-result v7

    .line 283
    .local v7, "viewType":I
    invoke-virtual {p0, p1}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v6

    .line 285
    .local v6, "data":Ljava/lang/Object;
    packed-switch v7, :pswitch_data_0

    .line 307
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "View type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not supported"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 287
    :pswitch_0
    invoke-direct {p0, p2, p3, p1}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->getProfileContainer(Landroid/view/View;Landroid/view/ViewGroup;I)Landroid/view/View;

    move-result-object v0

    .line 304
    .end local v6    # "data":Ljava/lang/Object;
    :goto_0
    return-object v0

    .line 289
    .restart local v6    # "data":Ljava/lang/Object;
    :pswitch_1
    invoke-direct {p0, p2, p3}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->getPrimaryActionsTopSpacing(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    move-object v3, v6

    .line 291
    check-cast v3, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;

    const/4 v4, 0x1

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->getPrimaryActionView(Landroid/view/View;Landroid/view/ViewGroup;Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;ZZ)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :pswitch_3
    move-object v3, v6

    .line 294
    check-cast v3, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->getPrimaryActionView(Landroid/view/View;Landroid/view/ViewGroup;Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;ZZ)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :pswitch_4
    move-object v3, v6

    .line 297
    check-cast v3, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;

    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->getPrimaryActionView(Landroid/view/View;Landroid/view/ViewGroup;Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;ZZ)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 300
    :pswitch_5
    invoke-direct {p0, p2, p3}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->getSecondaryActionsTopSeparator(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 302
    :pswitch_6
    invoke-direct {p0, p2, p3}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->getDownloadToggleView(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 304
    :pswitch_7
    check-cast v6, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;

    .end local v6    # "data":Ljava/lang/Object;
    invoke-direct {p0, p2, p3, v6}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->getSecondaryActionView(Landroid/view/View;Landroid/view/ViewGroup;Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 285
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_7
        :pswitch_6
    .end packed-switch
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 277
    const/16 v0, 0x8

    return v0
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 653
    const/4 v0, 0x0

    return v0
.end method

.method public isAccountListExpanded()Z
    .locals 1

    .prologue
    .line 112
    iget-boolean v0, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mAccountListExpanded:Z

    return v0
.end method

.method public updateContent(Ljava/lang/String;[Landroid/accounts/Account;Ljava/util/List;Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerDownloadSwitchConfig;Ljava/util/List;)V
    .locals 9
    .param p1, "currentAccountName"    # Ljava/lang/String;
    .param p2, "accounts"    # [Landroid/accounts/Account;
    .param p4, "downloadSwitchConfig"    # Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerDownloadSwitchConfig;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "[",
            "Landroid/accounts/Account;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;",
            ">;",
            "Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerDownloadSwitchConfig;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 120
    .local p3, "primaryActions":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;>;"
    .local p5, "secondaryActions":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;>;"
    array-length v6, p2

    .line 121
    .local v6, "numAccounts":I
    if-nez v6, :cond_1

    .line 122
    const/4 v7, 0x0

    iput-object v7, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mCurrentAccount:Landroid/accounts/Account;

    .line 123
    const/4 v7, 0x0

    new-array v7, v7, [Landroid/accounts/Account;

    iput-object v7, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mNonCurrentAccounts:[Landroid/accounts/Account;

    .line 144
    :cond_0
    :goto_0
    if-lez v6, :cond_4

    const/4 v7, 0x1

    :goto_1
    iput-boolean v7, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mHasAccounts:Z

    .line 147
    iget-object v7, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mPrimaryActions:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->clear()V

    .line 148
    iget-object v7, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mSecondaryActions:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->clear()V

    .line 149
    iget-object v7, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mPrimaryActions:Ljava/util/List;

    invoke-interface {v7, p3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 150
    iget-object v7, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mSecondaryActions:Ljava/util/List;

    invoke-interface {v7, p5}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 152
    iput-object p4, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mDownloadSwitchConfig:Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerDownloadSwitchConfig;

    .line 153
    if-eqz p4, :cond_5

    const/4 v7, 0x1

    :goto_2
    iput-boolean v7, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mShowDownloadOnlyToggle:Z

    .line 154
    iget-boolean v7, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mShowDownloadOnlyToggle:Z

    if-eqz v7, :cond_6

    iget-boolean v7, p4, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerDownloadSwitchConfig;->isChecked:Z

    :goto_3
    iput-boolean v7, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mDownloadOnlyEnabled:Z

    .line 156
    invoke-virtual {p0}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->notifyDataSetChanged()V

    .line 157
    return-void

    .line 126
    :cond_1
    add-int/lit8 v7, v6, -0x1

    new-array v7, v7, [Landroid/accounts/Account;

    iput-object v7, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mNonCurrentAccounts:[Landroid/accounts/Account;

    .line 127
    const/4 v4, 0x0

    .line 128
    .local v4, "nonCurrentIndex":I
    move-object v1, p2

    .local v1, "arr$":[Landroid/accounts/Account;
    array-length v3, v1

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    move v5, v4

    .end local v4    # "nonCurrentIndex":I
    .local v5, "nonCurrentIndex":I
    :goto_4
    if-ge v2, v3, :cond_0

    aget-object v0, v1, v2

    .line 129
    .local v0, "account":Landroid/accounts/Account;
    iget-object v7, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {p1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 130
    iput-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mCurrentAccount:Landroid/accounts/Account;

    move v4, v5

    .line 128
    .end local v5    # "nonCurrentIndex":I
    .restart local v4    # "nonCurrentIndex":I
    :goto_5
    add-int/lit8 v2, v2, 0x1

    move v5, v4

    .end local v4    # "nonCurrentIndex":I
    .restart local v5    # "nonCurrentIndex":I
    goto :goto_4

    .line 133
    :cond_2
    add-int/lit8 v7, v6, -0x1

    if-ne v5, v7, :cond_3

    .line 135
    const-string v7, "current account not found in accounts"

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v7, v8}, Lcom/google/android/play/utils/PlayCommonLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 137
    iput-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mCurrentAccount:Landroid/accounts/Account;

    goto :goto_0

    .line 140
    :cond_3
    iget-object v7, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mNonCurrentAccounts:[Landroid/accounts/Account;

    add-int/lit8 v4, v5, 0x1

    .end local v5    # "nonCurrentIndex":I
    .restart local v4    # "nonCurrentIndex":I
    aput-object v0, v7, v5

    goto :goto_5

    .line 144
    .end local v0    # "account":Landroid/accounts/Account;
    .end local v1    # "arr$":[Landroid/accounts/Account;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    .end local v4    # "nonCurrentIndex":I
    :cond_4
    const/4 v7, 0x0

    goto :goto_1

    .line 153
    :cond_5
    const/4 v7, 0x0

    goto :goto_2

    .line 154
    :cond_6
    const/4 v7, 0x0

    goto :goto_3
.end method
