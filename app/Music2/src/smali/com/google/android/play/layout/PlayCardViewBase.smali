.class public abstract Lcom/google/android/play/layout/PlayCardViewBase;
.super Lcom/google/android/play/layout/ForegroundRelativeLayout;
.source "PlayCardViewBase.java"

# interfaces
.implements Lcom/google/android/play/cardview/CardViewGroup;


# static fields
.field protected static final DISABLE_NESTED_FOCUS_TRAVERSAL:Z


# instance fields
.field protected final mAvatarSnippetMarginLeft:I

.field protected mBackendId:I

.field private final mCardInset:I

.field protected mData:Ljava/lang/Object;

.field protected mDescription:Lcom/google/android/play/layout/PlayTextView;

.field private mDisabledDrawable:Landroid/graphics/drawable/Drawable;

.field protected mIsItemOwned:Z

.field protected mItemBadge:Lcom/google/android/play/layout/PlayTextView;

.field protected mLabel:Lcom/google/android/play/layout/PlayCardLabelView;

.field protected mLoadingIndicator:Landroid/view/View;

.field protected mLoggingData:Ljava/lang/Object;

.field private final mOldOverflowArea:Landroid/graphics/Rect;

.field protected mOverflow:Landroid/widget/ImageView;

.field private final mOverflowArea:Landroid/graphics/Rect;

.field private final mOverflowTouchExtend:I

.field private final mOwnershipRenderingType:I

.field protected mRatingBar:Lcom/google/android/play/layout/StarRatingBar;

.field private final mShowInlineCreatorBadge:Z

.field protected mSnippet1:Lcom/google/android/play/layout/PlayCardSnippet;

.field protected mSnippet2:Lcom/google/android/play/layout/PlayCardSnippet;

.field protected mSubtitle:Lcom/google/android/play/layout/PlayTextView;

.field protected mSupportsSubtitleAndRating:Z

.field protected final mTextOnlySnippetMarginLeft:I

.field protected mThumbnail:Lcom/google/android/play/layout/PlayCardThumbnail;

.field protected mThumbnailAspectRatio:F

.field protected mTitle:Landroid/widget/TextView;

.field private mToDisplayAsDisabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 43
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xd

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/play/layout/PlayCardViewBase;->DISABLE_NESTED_FOCUS_TRAVERSAL:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 122
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/play/layout/PlayCardViewBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 123
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 126
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/play/layout/PlayCardViewBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 127
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v4, 0x0

    .line 130
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/play/layout/ForegroundRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 132
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/play/R$dimen;->play_card_overflow_touch_extend:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mOverflowTouchExtend:I

    .line 134
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mOverflowArea:Landroid/graphics/Rect;

    .line 135
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mOldOverflowArea:Landroid/graphics/Rect;

    .line 137
    sget-object v1, Lcom/google/android/play/R$styleable;->PlayCardBaseView:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 139
    .local v0, "viewAttrs":Landroid/content/res/TypedArray;
    sget v1, Lcom/google/android/play/R$styleable;->PlayCardBaseView_card_show_inline_creator_badge:I

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mShowInlineCreatorBadge:Z

    .line 141
    sget v1, Lcom/google/android/play/R$styleable;->PlayCardBaseView_card_supports_subtitle_and_rating:I

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mSupportsSubtitleAndRating:Z

    .line 143
    sget v1, Lcom/google/android/play/R$styleable;->PlayCardBaseView_card_text_only_snippet_margin_left:I

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/play/R$dimen;->play_card_snippet_text_extra_margin_left:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mTextOnlySnippetMarginLeft:I

    .line 147
    sget v1, Lcom/google/android/play/R$styleable;->PlayCardBaseView_card_avatar_snippet_margin_left:I

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mAvatarSnippetMarginLeft:I

    .line 149
    sget v1, Lcom/google/android/play/R$styleable;->PlayCardBaseView_card_owned_status_rendering_type:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mOwnershipRenderingType:I

    .line 152
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 154
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/play/R$dimen;->play_card_default_inset:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mCardInset:I

    .line 155
    iget v1, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mCardInset:I

    iget v2, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mCardInset:I

    iget v3, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mCardInset:I

    iget v4, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mCardInset:I

    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/google/android/play/layout/PlayCardViewBase;->setForegroundPadding(IIII)V

    .line 157
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayCardViewBase;->getCardViewGroupDelegate()Lcom/google/android/play/cardview/CardViewGroupDelegate;

    move-result-object v1

    invoke-interface {v1, p0, p1, p2, p3}, Lcom/google/android/play/cardview/CardViewGroupDelegate;->initialize(Lcom/google/android/play/cardview/CardViewGroup;Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 158
    return-void
.end method


# virtual methods
.method public dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 6
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 516
    invoke-super {p0, p1}, Lcom/google/android/play/layout/ForegroundRelativeLayout;->dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v0

    .line 520
    .local v0, "result":Z
    iget-boolean v4, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mToDisplayAsDisabled:Z

    if-eqz v4, :cond_1

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v4

    const/16 v5, 0x8

    if-ne v4, v5, :cond_1

    move v1, v2

    .line 522
    .local v1, "shouldMarkAsDisabled":Z
    :goto_0
    if-eqz v1, :cond_0

    .line 523
    invoke-virtual {p1, v3}, Landroid/view/accessibility/AccessibilityEvent;->setEnabled(Z)V

    move v0, v2

    .line 527
    .end local v0    # "result":Z
    :cond_0
    return v0

    .end local v1    # "shouldMarkAsDisabled":Z
    .restart local v0    # "result":Z
    :cond_1
    move v1, v3

    .line 520
    goto :goto_0
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v5, 0x0

    .line 494
    invoke-super {p0, p1}, Lcom/google/android/play/layout/ForegroundRelativeLayout;->draw(Landroid/graphics/Canvas;)V

    .line 496
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayCardViewBase;->getWidth()I

    move-result v1

    .line 497
    .local v1, "width":I
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayCardViewBase;->getHeight()I

    move-result v0

    .line 499
    .local v0, "height":I
    iget-boolean v2, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mToDisplayAsDisabled:Z

    if-eqz v2, :cond_1

    .line 500
    iget-object v2, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mDisabledDrawable:Landroid/graphics/drawable/Drawable;

    if-nez v2, :cond_0

    .line 501
    new-instance v2, Landroid/graphics/drawable/PaintDrawable;

    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayCardViewBase;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/google/android/play/R$color;->play_dismissed_overlay:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-direct {v2, v3}, Landroid/graphics/drawable/PaintDrawable;-><init>(I)V

    iput-object v2, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mDisabledDrawable:Landroid/graphics/drawable/Drawable;

    .line 504
    :cond_0
    iget-object v2, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mDisabledDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, v5, v5, v1, v0}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 505
    iget-object v2, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mDisabledDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 507
    :cond_1
    return-void
.end method

.method public getCardViewGroupDelegate()Lcom/google/android/play/cardview/CardViewGroupDelegate;
    .locals 1

    .prologue
    .line 176
    sget-object v0, Lcom/google/android/play/cardview/CardViewGroupDelegates;->IMPL:Lcom/google/android/play/cardview/CardViewGroupDelegate;

    return-object v0
.end method

.method protected measureThumbnailSpanningHeight(I)V
    .locals 8
    .param p1, "heightMeasureSpec"    # I

    .prologue
    .line 341
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 343
    .local v0, "availableHeight":I
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayCardViewBase;->getPaddingTop()I

    move-result v2

    .line 344
    .local v2, "paddingTop":I
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayCardViewBase;->getPaddingBottom()I

    move-result v1

    .line 346
    .local v1, "paddingBottom":I
    iget-object v6, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mThumbnail:Lcom/google/android/play/layout/PlayCardThumbnail;

    invoke-virtual {v6}, Lcom/google/android/play/layout/PlayCardThumbnail;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 348
    .local v4, "thumbnailLp":Landroid/view/ViewGroup$MarginLayoutParams;
    iget-object v6, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mThumbnail:Lcom/google/android/play/layout/PlayCardThumbnail;

    invoke-virtual {v6}, Lcom/google/android/play/layout/PlayCardThumbnail;->getVisibility()I

    move-result v6

    const/16 v7, 0x8

    if-eq v6, v7, :cond_0

    .line 349
    sub-int v6, v0, v2

    sub-int/2addr v6, v1

    iget v7, v4, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    sub-int/2addr v6, v7

    iget v7, v4, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    sub-int v3, v6, v7

    .line 351
    .local v3, "thumbnailHeight":I
    int-to-float v6, v3

    iget v7, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mThumbnailAspectRatio:F

    div-float/2addr v6, v7

    float-to-int v5, v6

    .line 352
    .local v5, "thumbnailWidth":I
    iput v5, v4, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    .line 356
    .end local v3    # "thumbnailHeight":I
    .end local v5    # "thumbnailWidth":I
    :goto_0
    return-void

    .line 354
    :cond_0
    const/4 v6, 0x0

    iput v6, v4, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    goto :goto_0
.end method

.method protected measureThumbnailSpanningWidth(I)V
    .locals 8
    .param p1, "widthMeasureSpec"    # I

    .prologue
    .line 323
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 325
    .local v0, "availableWidth":I
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayCardViewBase;->getPaddingLeft()I

    move-result v1

    .line 326
    .local v1, "paddingLeft":I
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayCardViewBase;->getPaddingRight()I

    move-result v2

    .line 328
    .local v2, "paddingRight":I
    iget-object v6, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mThumbnail:Lcom/google/android/play/layout/PlayCardThumbnail;

    invoke-virtual {v6}, Lcom/google/android/play/layout/PlayCardThumbnail;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 330
    .local v4, "thumbnailLp":Landroid/view/ViewGroup$MarginLayoutParams;
    iget-object v6, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mThumbnail:Lcom/google/android/play/layout/PlayCardThumbnail;

    invoke-virtual {v6}, Lcom/google/android/play/layout/PlayCardThumbnail;->getVisibility()I

    move-result v6

    const/16 v7, 0x8

    if-eq v6, v7, :cond_0

    .line 331
    sub-int v6, v0, v1

    sub-int/2addr v6, v2

    iget v7, v4, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    sub-int/2addr v6, v7

    iget v7, v4, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    sub-int v5, v6, v7

    .line 333
    .local v5, "thumbnailWidth":I
    iget v6, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mThumbnailAspectRatio:F

    int-to-float v7, v5

    mul-float/2addr v6, v7

    float-to-int v3, v6

    .line 334
    .local v3, "thumbnailHeight":I
    iput v3, v4, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 338
    .end local v3    # "thumbnailHeight":I
    .end local v5    # "thumbnailWidth":I
    :goto_0
    return-void

    .line 336
    :cond_0
    const/4 v6, 0x0

    iput v6, v4, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    goto :goto_0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 162
    invoke-super {p0}, Lcom/google/android/play/layout/ForegroundRelativeLayout;->onAttachedToWindow()V

    .line 164
    invoke-static {}, Lcom/google/android/play/layout/PlayCardWindowLifecycleTracker;->getInstance()Lcom/google/android/play/layout/PlayCardWindowLifecycleTracker;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/play/layout/PlayCardWindowLifecycleTracker;->cardAttachedToWindow(Lcom/google/android/play/layout/PlayCardViewBase;)V

    .line 165
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 169
    invoke-super {p0}, Lcom/google/android/play/layout/ForegroundRelativeLayout;->onDetachedFromWindow()V

    .line 171
    invoke-static {}, Lcom/google/android/play/layout/PlayCardWindowLifecycleTracker;->getInstance()Lcom/google/android/play/layout/PlayCardWindowLifecycleTracker;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/play/layout/PlayCardWindowLifecycleTracker;->cardDetachedFromWindow(Lcom/google/android/play/layout/PlayCardViewBase;)V

    .line 172
    return-void
.end method

.method protected onFinishInflate()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 297
    invoke-super {p0}, Lcom/google/android/play/layout/ForegroundRelativeLayout;->onFinishInflate()V

    .line 299
    sget v0, Lcom/google/android/play/R$id;->li_thumbnail_frame:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/layout/PlayCardViewBase;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/PlayCardThumbnail;

    iput-object v0, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mThumbnail:Lcom/google/android/play/layout/PlayCardThumbnail;

    .line 300
    sget v0, Lcom/google/android/play/R$id;->li_title:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/layout/PlayCardViewBase;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mTitle:Landroid/widget/TextView;

    .line 301
    sget v0, Lcom/google/android/play/R$id;->li_subtitle:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/layout/PlayCardViewBase;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/PlayTextView;

    iput-object v0, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mSubtitle:Lcom/google/android/play/layout/PlayTextView;

    .line 302
    sget v0, Lcom/google/android/play/R$id;->li_rating:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/layout/PlayCardViewBase;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/StarRatingBar;

    iput-object v0, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mRatingBar:Lcom/google/android/play/layout/StarRatingBar;

    .line 303
    sget v0, Lcom/google/android/play/R$id;->li_badge:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/layout/PlayCardViewBase;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/PlayTextView;

    iput-object v0, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mItemBadge:Lcom/google/android/play/layout/PlayTextView;

    .line 304
    sget v0, Lcom/google/android/play/R$id;->li_description:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/layout/PlayCardViewBase;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/PlayTextView;

    iput-object v0, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mDescription:Lcom/google/android/play/layout/PlayTextView;

    .line 305
    sget v0, Lcom/google/android/play/R$id;->li_overflow:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/layout/PlayCardViewBase;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mOverflow:Landroid/widget/ImageView;

    .line 306
    sget v0, Lcom/google/android/play/R$id;->li_label:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/layout/PlayCardViewBase;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/PlayCardLabelView;

    iput-object v0, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mLabel:Lcom/google/android/play/layout/PlayCardLabelView;

    .line 307
    sget v0, Lcom/google/android/play/R$id;->li_snippet_1:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/layout/PlayCardViewBase;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/PlayCardSnippet;

    iput-object v0, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mSnippet1:Lcom/google/android/play/layout/PlayCardSnippet;

    .line 308
    sget v0, Lcom/google/android/play/R$id;->li_snippet_2:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/layout/PlayCardViewBase;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/PlayCardSnippet;

    iput-object v0, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mSnippet2:Lcom/google/android/play/layout/PlayCardSnippet;

    .line 309
    sget v0, Lcom/google/android/play/R$id;->loading_progress_bar:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/layout/PlayCardViewBase;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mLoadingIndicator:Landroid/view/View;

    .line 313
    sget-boolean v0, Lcom/google/android/play/layout/PlayCardViewBase;->DISABLE_NESTED_FOCUS_TRAVERSAL:Z

    if-eqz v0, :cond_0

    .line 314
    invoke-virtual {p0, v2}, Lcom/google/android/play/layout/PlayCardViewBase;->setNextFocusRightId(I)V

    .line 315
    iget-object v0, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mOverflow:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 316
    iget-object v0, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mOverflow:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    .line 317
    iget-object v0, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mOverflow:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setNextFocusLeftId(I)V

    .line 320
    :cond_0
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 1
    .param p1, "info"    # Landroid/view/accessibility/AccessibilityNodeInfo;
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 533
    invoke-super {p0, p1}, Lcom/google/android/play/layout/ForegroundRelativeLayout;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    .line 535
    iget-boolean v0, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mToDisplayAsDisabled:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setEnabled(Z)V

    .line 536
    return-void

    .line 535
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 476
    iget-boolean v0, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mToDisplayAsDisabled:Z

    if-eqz v0, :cond_0

    .line 477
    const/4 v0, 0x1

    .line 480
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/play/layout/ForegroundRelativeLayout;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 0
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 446
    invoke-super/range {p0 .. p5}, Lcom/google/android/play/layout/ForegroundRelativeLayout;->onLayout(ZIIII)V

    .line 448
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayCardViewBase;->recomputeOverflowAreaIfNeeded()V

    .line 449
    return-void
.end method

.method protected onMeasure(II)V
    .locals 6
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 420
    invoke-super {p0, p1, p2}, Lcom/google/android/play/layout/ForegroundRelativeLayout;->onMeasure(II)V

    .line 422
    iget-object v4, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mDescription:Lcom/google/android/play/layout/PlayTextView;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mDescription:Lcom/google/android/play/layout/PlayTextView;

    invoke-virtual {v4}, Lcom/google/android/play/layout/PlayTextView;->getVisibility()I

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mDescription:Lcom/google/android/play/layout/PlayTextView;

    invoke-virtual {v4}, Lcom/google/android/play/layout/PlayTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 426
    iget-object v4, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mDescription:Lcom/google/android/play/layout/PlayTextView;

    invoke-virtual {v4}, Lcom/google/android/play/layout/PlayTextView;->getMeasuredHeight()I

    move-result v1

    .line 427
    .local v1, "descriptionHeight":I
    iget-object v4, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mDescription:Lcom/google/android/play/layout/PlayTextView;

    invoke-virtual {v4}, Lcom/google/android/play/layout/PlayTextView;->getLayout()Landroid/text/Layout;

    move-result-object v2

    .line 428
    .local v2, "layout":Landroid/text/Layout;
    if-nez v2, :cond_1

    .line 442
    .end local v1    # "descriptionHeight":I
    .end local v2    # "layout":Landroid/text/Layout;
    :cond_0
    :goto_0
    return-void

    .line 431
    .restart local v1    # "descriptionHeight":I
    .restart local v2    # "layout":Landroid/text/Layout;
    :cond_1
    const/4 v3, 0x0

    .local v3, "line":I
    :goto_1
    invoke-virtual {v2}, Landroid/text/Layout;->getLineCount()I

    move-result v4

    if-ge v3, v4, :cond_0

    .line 432
    invoke-virtual {v2, v3}, Landroid/text/Layout;->getLineBottom(I)I

    move-result v0

    .line 433
    .local v0, "currLineBottom":I
    if-le v0, v1, :cond_3

    .line 437
    iget-object v5, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mDescription:Lcom/google/android/play/layout/PlayTextView;

    const/4 v4, 0x2

    if-lt v3, v4, :cond_2

    const/4 v4, 0x0

    :goto_2
    invoke-virtual {v5, v4}, Lcom/google/android/play/layout/PlayTextView;->setVisibility(I)V

    goto :goto_0

    :cond_2
    const/4 v4, 0x4

    goto :goto_2

    .line 431
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 485
    iget-boolean v0, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mToDisplayAsDisabled:Z

    if-eqz v0, :cond_0

    .line 486
    const/4 v0, 0x1

    .line 489
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/play/layout/ForegroundRelativeLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected final recomputeOverflowAreaIfNeeded()V
    .locals 3

    .prologue
    .line 452
    iget-object v0, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mOverflow:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mOverflow:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_1

    .line 472
    :cond_0
    :goto_0
    return-void

    .line 458
    :cond_1
    iget-object v0, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mOverflow:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mOverflowArea:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->getHitRect(Landroid/graphics/Rect;)V

    .line 459
    iget-object v0, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mOverflowArea:Landroid/graphics/Rect;

    iget v1, v0, Landroid/graphics/Rect;->top:I

    iget v2, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mOverflowTouchExtend:I

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 460
    iget-object v0, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mOverflowArea:Landroid/graphics/Rect;

    iget v1, v0, Landroid/graphics/Rect;->bottom:I

    iget v2, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mOverflowTouchExtend:I

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 461
    iget-object v0, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mOverflowArea:Landroid/graphics/Rect;

    iget v1, v0, Landroid/graphics/Rect;->left:I

    iget v2, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mOverflowTouchExtend:I

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 462
    iget-object v0, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mOverflowArea:Landroid/graphics/Rect;

    iget v1, v0, Landroid/graphics/Rect;->right:I

    iget v2, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mOverflowTouchExtend:I

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 463
    iget-object v0, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mOverflowArea:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    iget-object v1, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mOldOverflowArea:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mOverflowArea:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    iget-object v1, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mOldOverflowArea:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mOverflowArea:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget-object v1, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mOldOverflowArea:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mOverflowArea:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    iget-object v1, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mOldOverflowArea:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    if-eq v0, v1, :cond_0

    .line 470
    :cond_2
    new-instance v0, Lcom/google/android/play/utils/PlayTouchDelegate;

    iget-object v1, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mOverflowArea:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mOverflow:Landroid/widget/ImageView;

    invoke-direct {v0, v1, v2}, Lcom/google/android/play/utils/PlayTouchDelegate;-><init>(Landroid/graphics/Rect;Landroid/view/View;)V

    invoke-virtual {p0, v0}, Lcom/google/android/play/layout/PlayCardViewBase;->setTouchDelegate(Landroid/view/TouchDelegate;)V

    .line 471
    iget-object v0, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mOldOverflowArea:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mOverflowArea:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    goto :goto_0
.end method

.method public setBackgroundColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 181
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayCardViewBase;->getCardViewGroupDelegate()Lcom/google/android/play/cardview/CardViewGroupDelegate;

    move-result-object v0

    invoke-interface {v0, p0, p1}, Lcom/google/android/play/cardview/CardViewGroupDelegate;->setBackgroundColor(Lcom/google/android/play/cardview/CardViewGroup;I)V

    .line 182
    return-void
.end method

.method public setBackgroundResource(I)V
    .locals 1
    .param p1, "resId"    # I

    .prologue
    .line 186
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayCardViewBase;->getCardViewGroupDelegate()Lcom/google/android/play/cardview/CardViewGroupDelegate;

    move-result-object v0

    invoke-interface {v0, p0, p1}, Lcom/google/android/play/cardview/CardViewGroupDelegate;->setBackgroundResource(Lcom/google/android/play/cardview/CardViewGroup;I)V

    .line 187
    return-void
.end method

.method public setData(Ljava/lang/Object;I)V
    .locals 0
    .param p1, "data"    # Ljava/lang/Object;
    .param p2, "backendId"    # I

    .prologue
    .line 198
    iput-object p1, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mData:Ljava/lang/Object;

    .line 199
    iput p2, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mBackendId:I

    .line 200
    return-void
.end method

.method public setDisplayAsDisabled(Z)V
    .locals 1
    .param p1, "displayAsDisabled"    # Z

    .prologue
    .line 282
    iget-boolean v0, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mToDisplayAsDisabled:Z

    if-ne v0, p1, :cond_0

    .line 289
    :goto_0
    return-void

    .line 285
    :cond_0
    iput-boolean p1, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mToDisplayAsDisabled:Z

    .line 286
    iget-boolean v0, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mToDisplayAsDisabled:Z

    if-eqz v0, :cond_1

    const/high16 v0, 0x60000

    :goto_1
    invoke-virtual {p0, v0}, Lcom/google/android/play/layout/PlayCardViewBase;->setDescendantFocusability(I)V

    .line 288
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayCardViewBase;->invalidate()V

    goto :goto_0

    .line 286
    :cond_1
    const/high16 v0, 0x20000

    goto :goto_1
.end method

.method public setItemOwned(Z)V
    .locals 0
    .param p1, "isItemOwned"    # Z

    .prologue
    .line 292
    iput-boolean p1, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mIsItemOwned:Z

    .line 293
    return-void
.end method

.method public setLoggingData(Ljava/lang/Object;)V
    .locals 0
    .param p1, "loggingData"    # Ljava/lang/Object;

    .prologue
    .line 210
    iput-object p1, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mLoggingData:Ljava/lang/Object;

    .line 211
    return-void
.end method

.method public setThumbnailAspectRatio(F)V
    .locals 1
    .param p1, "aspectRatio"    # F

    .prologue
    .line 364
    iget v0, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mThumbnailAspectRatio:F

    cmpl-float v0, v0, p1

    if-eqz v0, :cond_0

    .line 365
    iput p1, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mThumbnailAspectRatio:F

    .line 366
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayCardViewBase;->requestLayout()V

    .line 368
    :cond_0
    return-void
.end method
