.class final Lcom/google/android/play/utils/config/GservicesValue$3;
.super Lcom/google/android/play/utils/config/GservicesValue;
.source "GservicesValue.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/config/GservicesValue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/play/utils/config/GservicesValue",
        "<",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$defaultValue:Ljava/lang/Integer;

.field final synthetic val$key:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V
    .locals 1
    .param p1, "x0"    # Ljava/lang/String;

    .prologue
    .line 87
    iput-object p2, p0, Lcom/google/android/play/utils/config/GservicesValue$3;->val$key:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/play/utils/config/GservicesValue$3;->val$defaultValue:Ljava/lang/Integer;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/play/utils/config/GservicesValue;-><init>(Ljava/lang/String;Lcom/google/android/play/utils/config/GservicesValue$1;)V

    return-void
.end method


# virtual methods
.method protected retrieve()Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 90
    # getter for: Lcom/google/android/play/utils/config/GservicesValue;->sGservicesReader:Lcom/google/android/play/utils/config/GservicesValue$GservicesReader;
    invoke-static {}, Lcom/google/android/play/utils/config/GservicesValue;->access$200()Lcom/google/android/play/utils/config/GservicesValue$GservicesReader;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/play/utils/config/GservicesValue$3;->val$key:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/play/utils/config/GservicesValue$3;->val$defaultValue:Ljava/lang/Integer;

    invoke-interface {v0, v1, v2}, Lcom/google/android/play/utils/config/GservicesValue$GservicesReader;->getInt(Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic retrieve()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 87
    invoke-virtual {p0}, Lcom/google/android/play/utils/config/GservicesValue$3;->retrieve()Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method
