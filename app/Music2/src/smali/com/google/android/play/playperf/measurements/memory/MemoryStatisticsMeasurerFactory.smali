.class public Lcom/google/android/play/playperf/measurements/memory/MemoryStatisticsMeasurerFactory;
.super Ljava/lang/Object;
.source "MemoryStatisticsMeasurerFactory.java"

# interfaces
.implements Lcom/google/android/play/playperf/measurements/MeasurerFactory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/play/playperf/measurements/MeasurerFactory",
        "<",
        "Lcom/google/android/play/playperf/measurements/memory/MemoryStatistics;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getMeasurerInstance(Ljava/lang/String;)Lcom/google/android/play/playperf/measurements/Measurer;
    .locals 8
    .param p1, "sectionName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/android/play/playperf/measurements/Measurer",
            "<",
            "Lcom/google/android/play/playperf/measurements/memory/MemoryStatistics;",
            ">;"
        }
    .end annotation

    .prologue
    .line 31
    new-instance v1, Lcom/google/android/play/playperf/measurements/StatisticsAggregator;

    invoke-direct {v1}, Lcom/google/android/play/playperf/measurements/StatisticsAggregator;-><init>()V

    .line 32
    .local v1, "heapUsedStatistics":Lcom/google/android/play/playperf/measurements/StatisticsAggregator;
    new-instance v0, Lcom/google/android/play/playperf/measurements/StatisticsAggregator;

    invoke-direct {v0}, Lcom/google/android/play/playperf/measurements/StatisticsAggregator;-><init>()V

    .line 33
    .local v0, "heapTotalStatistics":Lcom/google/android/play/playperf/measurements/StatisticsAggregator;
    new-instance v2, Lcom/google/android/play/playperf/measurements/memory/MemoryStatisticsMeasurerFactory$1;

    invoke-direct {v2, p0, v1, v0}, Lcom/google/android/play/playperf/measurements/memory/MemoryStatisticsMeasurerFactory$1;-><init>(Lcom/google/android/play/playperf/measurements/memory/MemoryStatisticsMeasurerFactory;Lcom/google/android/play/playperf/measurements/StatisticsAggregator;Lcom/google/android/play/playperf/measurements/StatisticsAggregator;)V

    .line 43
    .local v2, "poller":Lcom/google/android/play/playperf/measurements/Poller;
    new-instance v3, Lcom/google/android/play/playperf/measurements/PollingTimer;

    const-wide/16 v4, 0x32

    const/4 v6, 0x1

    new-array v6, v6, [Lcom/google/android/play/playperf/measurements/Poller;

    const/4 v7, 0x0

    aput-object v2, v6, v7

    invoke-direct {v3, v4, v5, v6}, Lcom/google/android/play/playperf/measurements/PollingTimer;-><init>(J[Lcom/google/android/play/playperf/measurements/Poller;)V

    .line 44
    .local v3, "timer":Lcom/google/android/play/playperf/measurements/PollingTimer;
    new-instance v4, Lcom/google/android/play/playperf/measurements/memory/MemoryStatisticsMeasurer;

    invoke-direct {v4, v3, v0, v1, p1}, Lcom/google/android/play/playperf/measurements/memory/MemoryStatisticsMeasurer;-><init>(Lcom/google/android/play/playperf/measurements/PollingTimer;Lcom/google/android/play/playperf/measurements/StatisticsAggregator;Lcom/google/android/play/playperf/measurements/StatisticsAggregator;Ljava/lang/String;)V

    return-object v4
.end method
