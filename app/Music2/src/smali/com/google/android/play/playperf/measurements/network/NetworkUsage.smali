.class public Lcom/google/android/play/playperf/measurements/network/NetworkUsage;
.super Ljava/lang/Object;
.source "NetworkUsage.java"

# interfaces
.implements Lcom/google/android/play/playperf/measurements/Recordable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/play/playperf/measurements/network/NetworkUsage;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mReceived:Lcom/google/android/play/playperf/data/DataPoint;

.field private final mSectionName:Ljava/lang/String;

.field private final mSent:Lcom/google/android/play/playperf/data/DataPoint;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 87
    new-instance v0, Lcom/google/android/play/playperf/measurements/network/NetworkUsage$1;

    invoke-direct {v0}, Lcom/google/android/play/playperf/measurements/network/NetworkUsage$1;-><init>()V

    sput-object v0, Lcom/google/android/play/playperf/measurements/network/NetworkUsage;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "parcel"    # Landroid/os/Parcel;

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/playperf/data/DataPoint;

    iput-object v0, p0, Lcom/google/android/play/playperf/measurements/network/NetworkUsage;->mReceived:Lcom/google/android/play/playperf/data/DataPoint;

    .line 31
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/playperf/data/DataPoint;

    iput-object v0, p0, Lcom/google/android/play/playperf/measurements/network/NetworkUsage;->mSent:Lcom/google/android/play/playperf/data/DataPoint;

    .line 32
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/play/playperf/measurements/network/NetworkUsage;->mSectionName:Ljava/lang/String;

    .line 33
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/google/android/play/playperf/measurements/network/NetworkUsage$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/google/android/play/playperf/measurements/network/NetworkUsage$1;

    .prologue
    .line 14
    invoke-direct {p0, p1}, Lcom/google/android/play/playperf/measurements/network/NetworkUsage;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 76
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "parcel"    # Landroid/os/Parcel;
    .param p2, "flag"    # I

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/play/playperf/measurements/network/NetworkUsage;->mReceived:Lcom/google/android/play/playperf/data/DataPoint;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 83
    iget-object v0, p0, Lcom/google/android/play/playperf/measurements/network/NetworkUsage;->mSent:Lcom/google/android/play/playperf/data/DataPoint;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 84
    iget-object v0, p0, Lcom/google/android/play/playperf/measurements/network/NetworkUsage;->mSectionName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 85
    return-void
.end method
