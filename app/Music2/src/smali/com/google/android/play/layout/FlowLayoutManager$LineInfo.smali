.class abstract Lcom/google/android/play/layout/FlowLayoutManager$LineInfo;
.super Lcom/google/android/play/layout/FlowLayoutManager$MultiItemInfo;
.source "FlowLayoutManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/play/layout/FlowLayoutManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40a
    name = "LineInfo"
.end annotation


# instance fields
.field public mOffsetStart:I


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1101
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/play/layout/FlowLayoutManager$MultiItemInfo;-><init>(Lcom/google/android/play/layout/FlowLayoutManager$1;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/play/layout/FlowLayoutManager$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/play/layout/FlowLayoutManager$1;

    .prologue
    .line 1101
    invoke-direct {p0}, Lcom/google/android/play/layout/FlowLayoutManager$LineInfo;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract clearMeasuredInCurrentPass()V
.end method

.method abstract debugPrint(ILjava/lang/StringBuilder;)V
.end method

.method public abstract getItemTopOffset(I)I
.end method

.method public abstract recycle()V
.end method
