.class Lcom/google/android/play/playperf/measurements/remote/RecordableWrapper;
.super Ljava/lang/Object;
.source "RecordableWrapper.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/play/playperf/measurements/remote/RecordableWrapper;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mInstance:Lcom/google/android/play/playperf/measurements/Recordable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    new-instance v0, Lcom/google/android/play/playperf/measurements/remote/RecordableWrapper$1;

    invoke-direct {v0}, Lcom/google/android/play/playperf/measurements/remote/RecordableWrapper$1;-><init>()V

    sput-object v0, Lcom/google/android/play/playperf/measurements/remote/RecordableWrapper;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "parcel"    # Landroid/os/Parcel;

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    const-class v0, Lcom/google/android/play/playperf/measurements/remote/RecordableWrapper;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/playperf/measurements/Recordable;

    iput-object v0, p0, Lcom/google/android/play/playperf/measurements/remote/RecordableWrapper;->mInstance:Lcom/google/android/play/playperf/measurements/Recordable;

    .line 24
    return-void
.end method

.method constructor <init>(Lcom/google/android/play/playperf/measurements/Recordable;)V
    .locals 0
    .param p1, "instance"    # Lcom/google/android/play/playperf/measurements/Recordable;

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/google/android/play/playperf/measurements/remote/RecordableWrapper;->mInstance:Lcom/google/android/play/playperf/measurements/Recordable;

    .line 20
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 33
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "parcel"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/play/playperf/measurements/remote/RecordableWrapper;->mInstance:Lcom/google/android/play/playperf/measurements/Recordable;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 39
    return-void
.end method
