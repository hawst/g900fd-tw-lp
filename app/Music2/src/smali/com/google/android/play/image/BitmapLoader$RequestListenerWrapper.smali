.class Lcom/google/android/play/image/BitmapLoader$RequestListenerWrapper;
.super Ljava/lang/Object;
.source "BitmapLoader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/play/image/BitmapLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RequestListenerWrapper"
.end annotation


# instance fields
.field private handlers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/play/image/BitmapLoader$BitmapContainer;",
            ">;"
        }
    .end annotation
.end field

.field private request:Lcom/android/volley/Request;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/android/volley/Request",
            "<*>;"
        }
    .end annotation
.end field

.field private responseBitmap:Landroid/graphics/Bitmap;

.field final synthetic this$0:Lcom/google/android/play/image/BitmapLoader;


# direct methods
.method public constructor <init>(Lcom/google/android/play/image/BitmapLoader;Lcom/android/volley/Request;Lcom/google/android/play/image/BitmapLoader$BitmapContainer;)V
    .locals 1
    .param p3, "container"    # Lcom/google/android/play/image/BitmapLoader$BitmapContainer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/volley/Request",
            "<*>;",
            "Lcom/google/android/play/image/BitmapLoader$BitmapContainer;",
            ")V"
        }
    .end annotation

    .prologue
    .line 481
    .local p2, "request":Lcom/android/volley/Request;, "Lcom/android/volley/Request<*>;"
    iput-object p1, p0, Lcom/google/android/play/image/BitmapLoader$RequestListenerWrapper;->this$0:Lcom/google/android/play/image/BitmapLoader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 474
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/play/image/BitmapLoader$RequestListenerWrapper;->handlers:Ljava/util/List;

    .line 482
    iput-object p2, p0, Lcom/google/android/play/image/BitmapLoader$RequestListenerWrapper;->request:Lcom/android/volley/Request;

    .line 483
    iget-object v0, p0, Lcom/google/android/play/image/BitmapLoader$RequestListenerWrapper;->handlers:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 484
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/play/image/BitmapLoader$RequestListenerWrapper;)Lcom/android/volley/Request;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/play/image/BitmapLoader$RequestListenerWrapper;

    .prologue
    .line 466
    iget-object v0, p0, Lcom/google/android/play/image/BitmapLoader$RequestListenerWrapper;->request:Lcom/android/volley/Request;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/play/image/BitmapLoader$RequestListenerWrapper;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/play/image/BitmapLoader$RequestListenerWrapper;

    .prologue
    .line 466
    iget-object v0, p0, Lcom/google/android/play/image/BitmapLoader$RequestListenerWrapper;->responseBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$402(Lcom/google/android/play/image/BitmapLoader$RequestListenerWrapper;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/play/image/BitmapLoader$RequestListenerWrapper;
    .param p1, "x1"    # Landroid/graphics/Bitmap;

    .prologue
    .line 466
    iput-object p1, p0, Lcom/google/android/play/image/BitmapLoader$RequestListenerWrapper;->responseBitmap:Landroid/graphics/Bitmap;

    return-object p1
.end method

.method static synthetic access$700(Lcom/google/android/play/image/BitmapLoader$RequestListenerWrapper;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/play/image/BitmapLoader$RequestListenerWrapper;

    .prologue
    .line 466
    iget-object v0, p0, Lcom/google/android/play/image/BitmapLoader$RequestListenerWrapper;->handlers:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public addHandler(Lcom/google/android/play/image/BitmapLoader$BitmapContainer;)V
    .locals 1
    .param p1, "container"    # Lcom/google/android/play/image/BitmapLoader$BitmapContainer;

    .prologue
    .line 491
    iget-object v0, p0, Lcom/google/android/play/image/BitmapLoader$RequestListenerWrapper;->handlers:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 492
    return-void
.end method

.method public removeHandlerAndCancelIfNecessary(Lcom/google/android/play/image/BitmapLoader$BitmapContainer;)Z
    .locals 1
    .param p1, "container"    # Lcom/google/android/play/image/BitmapLoader$BitmapContainer;

    .prologue
    .line 501
    iget-object v0, p0, Lcom/google/android/play/image/BitmapLoader$RequestListenerWrapper;->handlers:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 502
    iget-object v0, p0, Lcom/google/android/play/image/BitmapLoader$RequestListenerWrapper;->handlers:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 503
    iget-object v0, p0, Lcom/google/android/play/image/BitmapLoader$RequestListenerWrapper;->request:Lcom/android/volley/Request;

    invoke-virtual {v0}, Lcom/android/volley/Request;->cancel()V

    .line 504
    const/4 v0, 0x1

    .line 506
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
