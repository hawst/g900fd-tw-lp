.class public Lcom/google/android/play/playperf/data/DataPoint;
.super Ljava/lang/Object;
.source "DataPoint.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private mTimeStamp:J

.field private mValue:D


# direct methods
.method public constructor <init>(JD)V
    .locals 1
    .param p1, "timeStamp"    # J
    .param p3, "value"    # D

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-wide p1, p0, Lcom/google/android/play/playperf/data/DataPoint;->mTimeStamp:J

    .line 24
    iput-wide p3, p0, Lcom/google/android/play/playperf/data/DataPoint;->mValue:D

    .line 25
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 6
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 47
    instance-of v0, p1, Lcom/google/android/play/playperf/data/DataPoint;

    if-eqz v0, :cond_1

    .line 48
    invoke-virtual {p0}, Lcom/google/android/play/playperf/data/DataPoint;->getValue()D

    move-result-wide v2

    move-object v0, p1

    check-cast v0, Lcom/google/android/play/playperf/data/DataPoint;

    invoke-virtual {v0}, Lcom/google/android/play/playperf/data/DataPoint;->getValue()D

    move-result-wide v4

    cmpl-double v0, v2, v4

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/play/playperf/data/DataPoint;->getTimeStamp()J

    move-result-wide v2

    check-cast p1, Lcom/google/android/play/playperf/data/DataPoint;

    .end local p1    # "o":Ljava/lang/Object;
    invoke-virtual {p1}, Lcom/google/android/play/playperf/data/DataPoint;->getTimeStamp()J

    move-result-wide v4

    cmp-long v0, v2, v4

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 51
    :goto_0
    return v0

    :cond_0
    move v0, v1

    .line 48
    goto :goto_0

    .restart local p1    # "o":Ljava/lang/Object;
    :cond_1
    move v0, v1

    .line 51
    goto :goto_0
.end method

.method public getTimeStamp()J
    .locals 2

    .prologue
    .line 38
    iget-wide v0, p0, Lcom/google/android/play/playperf/data/DataPoint;->mTimeStamp:J

    return-wide v0
.end method

.method public getValue()D
    .locals 2

    .prologue
    .line 31
    iget-wide v0, p0, Lcom/google/android/play/playperf/data/DataPoint;->mValue:D

    return-wide v0
.end method

.method public hashCode()I
    .locals 7

    .prologue
    const/16 v6, 0x20

    .line 61
    const/16 v0, 0x11

    .line 62
    .local v0, "result":I
    invoke-virtual {p0}, Lcom/google/android/play/playperf/data/DataPoint;->getTimeStamp()J

    move-result-wide v2

    invoke-virtual {p0}, Lcom/google/android/play/playperf/data/DataPoint;->getTimeStamp()J

    move-result-wide v4

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/lit16 v0, v1, 0x20f

    .line 63
    mul-int/lit8 v1, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/android/play/playperf/data/DataPoint;->getValue()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    invoke-virtual {p0}, Lcom/google/android/play/playperf/data/DataPoint;->getValue()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v4

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int v0, v1, v2

    .line 65
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 74
    const-string v0, "DataPoint {timestamp=%d, value=%f}"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/play/playperf/data/DataPoint;->getTimeStamp()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/android/play/playperf/data/DataPoint;->getValue()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
