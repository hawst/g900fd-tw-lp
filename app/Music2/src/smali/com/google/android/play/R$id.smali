.class public final Lcom/google/android/play/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/play/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final account_list_button_divider:I = 0x7f0e020c

.field public static final account_name:I = 0x7f0e0204

.field public static final action_bar:I = 0x7f0e00c2

.field public static final action_bar_container:I = 0x7f0e00c1

.field public static final action_button:I = 0x7f0e0230

.field public static final action_text:I = 0x7f0e0206

.field public static final alt_play_background:I = 0x7f0e020f

.field public static final avatar:I = 0x7f0e0203

.field public static final background_container:I = 0x7f0e020e

.field public static final caption:I = 0x7f0e021e

.field public static final content_container:I = 0x7f0e00e3

.field public static final controls_container:I = 0x7f0e0143

.field public static final cover_photo:I = 0x7f0e020a

.field public static final display_name:I = 0x7f0e020d

.field public static final end_button:I = 0x7f0e0221

.field public static final flm_paddingEnd:I = 0x7f0e000d

.field public static final flm_paddingStart:I = 0x7f0e000c

.field public static final flm_width:I = 0x7f0e000e

.field public static final header_shadow:I = 0x7f0e0215

.field public static final hero_container:I = 0x7f0e0210

.field public static final icon:I = 0x7f0e007c

.field public static final li_badge:I = 0x7f0e01f9

.field public static final li_description:I = 0x7f0e0109

.field public static final li_label:I = 0x7f0e01f4

.field public static final li_overflow:I = 0x7f0e010b

.field public static final li_rating:I = 0x7f0e01f3

.field public static final li_snippet_1:I = 0x7f0e01f6

.field public static final li_snippet_2:I = 0x7f0e01f5

.field public static final li_snippet_avatar:I = 0x7f0e01ff

.field public static final li_snippet_text:I = 0x7f0e0200

.field public static final li_subtitle:I = 0x7f0e017e

.field public static final li_thumbnail:I = 0x7f0e01f2

.field public static final li_thumbnail_frame:I = 0x7f0e01f1

.field public static final li_title:I = 0x7f0e0108

.field public static final loading_progress_bar:I = 0x7f0e01f7

.field public static final navigation_button:I = 0x7f0e022c

.field public static final page_indicator:I = 0x7f0e0220

.field public static final pager_tab_strip:I = 0x7f0e01e9

.field public static final play_drawer_list:I = 0x7f0e0205

.field public static final play_header_banner:I = 0x7f0e0217

.field public static final play_header_list_tab_container:I = 0x7f0e0213

.field public static final play_header_list_tab_scroll:I = 0x7f0e0212

.field public static final play_header_listview:I = 0x7f0e0012

.field public static final play_header_spacer:I = 0x7f0e0014

.field public static final play_header_toolbar:I = 0x7f0e0216

.field public static final play_header_viewpager:I = 0x7f0e0013

.field public static final play_onboard__OnboardSimpleQuizPage_title:I = 0x7f0e001c

.field public static final play_onboard__OnboardTutorialPage_backgroundColor:I = 0x7f0e0018

.field public static final play_onboard__OnboardTutorialPage_bodyText:I = 0x7f0e001a

.field public static final play_onboard__OnboardTutorialPage_iconDrawableId:I = 0x7f0e001b

.field public static final play_onboard__OnboardTutorialPage_titleText:I = 0x7f0e0019

.field public static final play_search_plate:I = 0x7f0e0227

.field public static final play_search_suggestions_list:I = 0x7f0e0228

.field public static final rating_badge_container:I = 0x7f0e01f8

.field public static final scroll_proxy:I = 0x7f0e021a

.field public static final search_box_idle_text:I = 0x7f0e022e

.field public static final search_box_text_input:I = 0x7f0e022f

.field public static final secondary_profiles:I = 0x7f0e0209

.field public static final selected_profile:I = 0x7f0e0208

.field public static final start_button:I = 0x7f0e021f

.field public static final suggest_text:I = 0x7f0e022a

.field public static final suggestion_divider:I = 0x7f0e022b

.field public static final suggestion_list_recycler_view:I = 0x7f0e0233

.field public static final swipe_refresh_layout:I = 0x7f0e0219

.field public static final swipe_refresh_layout_parent:I = 0x7f0e0218

.field public static final switch_button:I = 0x7f0e0103

.field public static final tab_bar:I = 0x7f0e0211

.field public static final tab_bar_title:I = 0x7f0e0214

.field public static final text_container:I = 0x7f0e022d

.field public static final toggle_account_list_button:I = 0x7f0e020b
