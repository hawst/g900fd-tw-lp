.class public Lcom/google/android/play/headerlist/PlayHeaderListOnScrollListener;
.super Ljava/lang/Object;
.source "PlayHeaderListOnScrollListener.java"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# instance fields
.field private mAbsoluteY:I

.field private mAdapter:Landroid/widget/Adapter;

.field private final mLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

.field private final mObserver:Landroid/database/DataSetObserver;

.field private mPreviousRelativeTopIndex:I

.field private final mRelativeTops:[Landroid/util/SparseIntArray;

.field protected mScrollState:I


# direct methods
.method public constructor <init>(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)V
    .locals 3
    .param p1, "layout"    # Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    const/4 v0, 0x2

    new-array v0, v0, [Landroid/util/SparseIntArray;

    const/4 v1, 0x0

    new-instance v2, Landroid/util/SparseIntArray;

    invoke-direct {v2}, Landroid/util/SparseIntArray;-><init>()V

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-instance v2, Landroid/util/SparseIntArray;

    invoke-direct {v2}, Landroid/util/SparseIntArray;-><init>()V

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListOnScrollListener;->mRelativeTops:[Landroid/util/SparseIntArray;

    .line 37
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListOnScrollListener;->mAbsoluteY:I

    .line 50
    iput-object p1, p0, Lcom/google/android/play/headerlist/PlayHeaderListOnScrollListener;->mLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    .line 51
    new-instance v0, Lcom/google/android/play/headerlist/PlayHeaderListOnScrollListener$1;

    invoke-direct {v0, p0}, Lcom/google/android/play/headerlist/PlayHeaderListOnScrollListener$1;-><init>(Lcom/google/android/play/headerlist/PlayHeaderListOnScrollListener;)V

    iput-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListOnScrollListener;->mObserver:Landroid/database/DataSetObserver;

    .line 68
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/play/headerlist/PlayHeaderListOnScrollListener;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/play/headerlist/PlayHeaderListOnScrollListener;
    .param p1, "x1"    # Z

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/google/android/play/headerlist/PlayHeaderListOnScrollListener;->reset(Z)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/play/headerlist/PlayHeaderListOnScrollListener;)Lcom/google/android/play/headerlist/PlayHeaderListLayout;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/play/headerlist/PlayHeaderListOnScrollListener;

    .prologue
    .line 17
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListOnScrollListener;->mLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    return-object v0
.end method

.method private computeRelativeTops(Landroid/widget/AbsListView;IILandroid/util/SparseIntArray;)Landroid/util/SparseIntArray;
    .locals 2
    .param p1, "view"    # Landroid/widget/AbsListView;
    .param p2, "firstVisibleItem"    # I
    .param p3, "visibleItemCount"    # I
    .param p4, "relativeTops"    # Landroid/util/SparseIntArray;

    .prologue
    .line 117
    invoke-virtual {p4}, Landroid/util/SparseIntArray;->clear()V

    .line 118
    move v0, p2

    .local v0, "i":I
    :goto_0
    add-int v1, p2, p3

    if-ge v0, v1, :cond_0

    .line 119
    sub-int v1, v0, p2

    invoke-virtual {p1, v1}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    invoke-virtual {p4, v0, v1}, Landroid/util/SparseIntArray;->put(II)V

    .line 118
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 121
    :cond_0
    return-object p4
.end method

.method private currentRelativeTops()Landroid/util/SparseIntArray;
    .locals 2

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListOnScrollListener;->mRelativeTops:[Landroid/util/SparseIntArray;

    iget v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListOnScrollListener;->mPreviousRelativeTopIndex:I

    add-int/lit8 v1, v1, 0x1

    rem-int/lit8 v1, v1, 0x2

    aget-object v0, v0, v1

    return-object v0
.end method

.method private previousRelativeTops()Landroid/util/SparseIntArray;
    .locals 2

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListOnScrollListener;->mRelativeTops:[Landroid/util/SparseIntArray;

    iget v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListOnScrollListener;->mPreviousRelativeTopIndex:I

    aget-object v0, v0, v1

    return-object v0
.end method

.method private reset(Z)V
    .locals 1
    .param p1, "resetAdapter"    # Z

    .prologue
    .line 83
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListOnScrollListener;->previousRelativeTops()Landroid/util/SparseIntArray;

    move-result-object v0

    invoke-virtual {v0}, Landroid/util/SparseIntArray;->clear()V

    .line 84
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListOnScrollListener;->mAbsoluteY:I

    .line 85
    if-eqz p1, :cond_0

    .line 86
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/play/headerlist/PlayHeaderListOnScrollListener;->updateAdapter(Landroid/widget/Adapter;)V

    .line 88
    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListOnScrollListener;->mScrollState:I

    .line 89
    return-void
.end method

.method private updateAdapter(Landroid/widget/Adapter;)V
    .locals 2
    .param p1, "adapter"    # Landroid/widget/Adapter;

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListOnScrollListener;->mAdapter:Landroid/widget/Adapter;

    if-ne v0, p1, :cond_0

    .line 104
    :goto_0
    return-void

    .line 96
    :cond_0
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListOnScrollListener;->mAdapter:Landroid/widget/Adapter;

    if-eqz v0, :cond_1

    .line 97
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListOnScrollListener;->mAdapter:Landroid/widget/Adapter;

    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListOnScrollListener;->mObserver:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Landroid/widget/Adapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 99
    :cond_1
    iput-object p1, p0, Lcom/google/android/play/headerlist/PlayHeaderListOnScrollListener;->mAdapter:Landroid/widget/Adapter;

    .line 100
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListOnScrollListener;->mAdapter:Landroid/widget/Adapter;

    if-eqz v0, :cond_2

    .line 101
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListOnScrollListener;->mAdapter:Landroid/widget/Adapter;

    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListOnScrollListener;->mObserver:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Landroid/widget/Adapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 103
    :cond_2
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/play/headerlist/PlayHeaderListOnScrollListener;->reset(Z)V

    goto :goto_0
.end method


# virtual methods
.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 8
    .param p1, "view"    # Landroid/widget/AbsListView;
    .param p2, "firstVisibleItem"    # I
    .param p3, "visibleItemCount"    # I
    .param p4, "totalItemCount"    # I

    .prologue
    const/4 v6, -0x1

    .line 138
    invoke-static {p3, p4}, Ljava/lang/Math;->min(II)I

    move-result p3

    .line 139
    invoke-virtual {p1}, Landroid/widget/AbsListView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/google/android/play/headerlist/PlayHeaderListOnScrollListener;->updateAdapter(Landroid/widget/Adapter;)V

    .line 142
    const/4 v1, 0x0

    .line 143
    .local v1, "deltaY":I
    const/4 v2, -0x1

    .line 144
    .local v2, "foundTop":I
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListOnScrollListener;->previousRelativeTops()Landroid/util/SparseIntArray;

    move-result-object v4

    .line 145
    .local v4, "previousRelativeTops":Landroid/util/SparseIntArray;
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListOnScrollListener;->currentRelativeTops()Landroid/util/SparseIntArray;

    move-result-object v0

    .line 146
    .local v0, "currentRelativeTops":Landroid/util/SparseIntArray;
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/play/headerlist/PlayHeaderListOnScrollListener;->computeRelativeTops(Landroid/widget/AbsListView;IILandroid/util/SparseIntArray;)Landroid/util/SparseIntArray;

    .line 147
    move v3, p2

    .local v3, "i":I
    :goto_0
    add-int v5, p2, p3

    if-ge v3, v5, :cond_0

    .line 148
    invoke-virtual {v4, v3, v6}, Landroid/util/SparseIntArray;->get(II)I

    move-result v2

    .line 149
    if-eq v2, v6, :cond_3

    .line 150
    invoke-virtual {v0, v3}, Landroid/util/SparseIntArray;->get(I)I

    move-result v5

    sub-int v1, v2, v5

    .line 155
    :cond_0
    iget v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListOnScrollListener;->mPreviousRelativeTopIndex:I

    add-int/lit8 v5, v5, 0x1

    rem-int/lit8 v5, v5, 0x2

    iput v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListOnScrollListener;->mPreviousRelativeTopIndex:I

    .line 158
    iget v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListOnScrollListener;->mAbsoluteY:I

    if-eq v5, v6, :cond_1

    if-ne v2, v6, :cond_4

    .line 161
    :cond_1
    iget-object v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListOnScrollListener;->mLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-virtual {v5, p1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->tryGetCollectionViewAbsoluteY(Landroid/view/ViewGroup;)I

    move-result v5

    iput v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListOnScrollListener;->mAbsoluteY:I

    .line 166
    :goto_1
    iget-object v6, p0, Lcom/google/android/play/headerlist/PlayHeaderListOnScrollListener;->mLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    iget v7, p0, Lcom/google/android/play/headerlist/PlayHeaderListOnScrollListener;->mScrollState:I

    invoke-virtual {p1}, Landroid/widget/AbsListView;->getChildCount()I

    move-result v5

    if-nez v5, :cond_5

    const/4 v5, 0x0

    :goto_2
    invoke-virtual {v6, v7, v1, v5}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->onScroll(III)V

    .line 167
    iget-object v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListOnScrollListener;->mLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    iget-object v5, v5, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mAppListViewOnScrollListener:Landroid/widget/AbsListView$OnScrollListener;

    if-eqz v5, :cond_2

    .line 168
    iget-object v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListOnScrollListener;->mLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    iget-object v5, v5, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mAppListViewOnScrollListener:Landroid/widget/AbsListView$OnScrollListener;

    invoke-interface {v5, p1, p2, p3, p4}, Landroid/widget/AbsListView$OnScrollListener;->onScroll(Landroid/widget/AbsListView;III)V

    .line 171
    :cond_2
    return-void

    .line 147
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 163
    :cond_4
    iget v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListOnScrollListener;->mAbsoluteY:I

    add-int/2addr v5, v1

    iput v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListOnScrollListener;->mAbsoluteY:I

    goto :goto_1

    .line 166
    :cond_5
    iget v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListOnScrollListener;->mAbsoluteY:I

    goto :goto_2
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 2
    .param p1, "view"    # Landroid/widget/AbsListView;
    .param p2, "scrollState"    # I

    .prologue
    .line 126
    invoke-virtual {p1}, Landroid/widget/AbsListView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/play/headerlist/PlayHeaderListOnScrollListener;->updateAdapter(Landroid/widget/Adapter;)V

    .line 127
    iput p2, p0, Lcom/google/android/play/headerlist/PlayHeaderListOnScrollListener;->mScrollState:I

    .line 128
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListOnScrollListener;->mLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-virtual {v0, p2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->onScrollStateChanged(I)V

    .line 129
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListOnScrollListener;->mLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    iget-object v0, v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mAppListViewOnScrollListener:Landroid/widget/AbsListView$OnScrollListener;

    if-eqz v0, :cond_0

    .line 130
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListOnScrollListener;->mLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    iget-object v0, v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mAppListViewOnScrollListener:Landroid/widget/AbsListView$OnScrollListener;

    iget v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListOnScrollListener;->mScrollState:I

    invoke-interface {v0, p1, v1}, Landroid/widget/AbsListView$OnScrollListener;->onScrollStateChanged(Landroid/widget/AbsListView;I)V

    .line 132
    :cond_0
    return-void
.end method

.method reset()V
    .locals 1

    .prologue
    .line 75
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/play/headerlist/PlayHeaderListOnScrollListener;->reset(Z)V

    .line 76
    return-void
.end method
