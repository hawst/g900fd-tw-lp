.class public final Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;
.super Lcom/google/protobuf/nano/MessageNano;
.source "ClientAnalytics.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/play/analytics/ClientAnalytics;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ActiveExperiments"
.end annotation


# instance fields
.field public clientAlteringExperiment:[Ljava/lang/String;

.field public gwsExperiment:[I

.field public otherExperiment:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 133
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 134
    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;->clear()Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;

    .line 135
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;
    .locals 1

    .prologue
    .line 138
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;->clientAlteringExperiment:[Ljava/lang/String;

    .line 139
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;->otherExperiment:[Ljava/lang/String;

    .line 140
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_INT_ARRAY:[I

    iput-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;->gwsExperiment:[I

    .line 141
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;->cachedSize:I

    .line 142
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 6

    .prologue
    .line 174
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v4

    .line 175
    .local v4, "size":I
    iget-object v5, p0, Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;->clientAlteringExperiment:[Ljava/lang/String;

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;->clientAlteringExperiment:[Ljava/lang/String;

    array-length v5, v5

    if-lez v5, :cond_2

    .line 176
    const/4 v0, 0x0

    .line 177
    .local v0, "dataCount":I
    const/4 v1, 0x0

    .line 178
    .local v1, "dataSize":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v5, p0, Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;->clientAlteringExperiment:[Ljava/lang/String;

    array-length v5, v5

    if-ge v3, v5, :cond_1

    .line 179
    iget-object v5, p0, Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;->clientAlteringExperiment:[Ljava/lang/String;

    aget-object v2, v5, v3

    .line 180
    .local v2, "element":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 181
    add-int/lit8 v0, v0, 0x1

    .line 182
    invoke-static {v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v1, v5

    .line 178
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 186
    .end local v2    # "element":Ljava/lang/String;
    :cond_1
    add-int/2addr v4, v1

    .line 187
    mul-int/lit8 v5, v0, 0x1

    add-int/2addr v4, v5

    .line 189
    .end local v0    # "dataCount":I
    .end local v1    # "dataSize":I
    .end local v3    # "i":I
    :cond_2
    iget-object v5, p0, Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;->otherExperiment:[Ljava/lang/String;

    if-eqz v5, :cond_5

    iget-object v5, p0, Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;->otherExperiment:[Ljava/lang/String;

    array-length v5, v5

    if-lez v5, :cond_5

    .line 190
    const/4 v0, 0x0

    .line 191
    .restart local v0    # "dataCount":I
    const/4 v1, 0x0

    .line 192
    .restart local v1    # "dataSize":I
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_1
    iget-object v5, p0, Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;->otherExperiment:[Ljava/lang/String;

    array-length v5, v5

    if-ge v3, v5, :cond_4

    .line 193
    iget-object v5, p0, Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;->otherExperiment:[Ljava/lang/String;

    aget-object v2, v5, v3

    .line 194
    .restart local v2    # "element":Ljava/lang/String;
    if-eqz v2, :cond_3

    .line 195
    add-int/lit8 v0, v0, 0x1

    .line 196
    invoke-static {v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v1, v5

    .line 192
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 200
    .end local v2    # "element":Ljava/lang/String;
    :cond_4
    add-int/2addr v4, v1

    .line 201
    mul-int/lit8 v5, v0, 0x1

    add-int/2addr v4, v5

    .line 203
    .end local v0    # "dataCount":I
    .end local v1    # "dataSize":I
    .end local v3    # "i":I
    :cond_5
    iget-object v5, p0, Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;->gwsExperiment:[I

    if-eqz v5, :cond_7

    iget-object v5, p0, Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;->gwsExperiment:[I

    array-length v5, v5

    if-lez v5, :cond_7

    .line 204
    const/4 v1, 0x0

    .line 205
    .restart local v1    # "dataSize":I
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_2
    iget-object v5, p0, Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;->gwsExperiment:[I

    array-length v5, v5

    if-ge v3, v5, :cond_6

    .line 206
    iget-object v5, p0, Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;->gwsExperiment:[I

    aget v2, v5, v3

    .line 207
    .local v2, "element":I
    invoke-static {v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32SizeNoTag(I)I

    move-result v5

    add-int/2addr v1, v5

    .line 205
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 210
    .end local v2    # "element":I
    :cond_6
    add-int/2addr v4, v1

    .line 211
    iget-object v5, p0, Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;->gwsExperiment:[I

    array-length v5, v5

    mul-int/lit8 v5, v5, 0x1

    add-int/2addr v4, v5

    .line 213
    .end local v1    # "dataSize":I
    .end local v3    # "i":I
    :cond_7
    return v4
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;
    .locals 9
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 221
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v6

    .line 222
    .local v6, "tag":I
    sparse-switch v6, :sswitch_data_0

    .line 226
    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v8

    if-nez v8, :cond_0

    .line 227
    :sswitch_0
    return-object p0

    .line 232
    :sswitch_1
    const/16 v8, 0xa

    invoke-static {p1, v8}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 234
    .local v0, "arrayLength":I
    iget-object v8, p0, Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;->clientAlteringExperiment:[Ljava/lang/String;

    if-nez v8, :cond_2

    move v1, v7

    .line 235
    .local v1, "i":I
    :goto_1
    add-int v8, v1, v0

    new-array v4, v8, [Ljava/lang/String;

    .line 236
    .local v4, "newArray":[Ljava/lang/String;
    if-eqz v1, :cond_1

    .line 237
    iget-object v8, p0, Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;->clientAlteringExperiment:[Ljava/lang/String;

    invoke-static {v8, v7, v4, v7, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 239
    :cond_1
    :goto_2
    array-length v8, v4

    add-int/lit8 v8, v8, -0x1

    if-ge v1, v8, :cond_3

    .line 240
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v4, v1

    .line 241
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 239
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 234
    .end local v1    # "i":I
    .end local v4    # "newArray":[Ljava/lang/String;
    :cond_2
    iget-object v8, p0, Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;->clientAlteringExperiment:[Ljava/lang/String;

    array-length v1, v8

    goto :goto_1

    .line 244
    .restart local v1    # "i":I
    .restart local v4    # "newArray":[Ljava/lang/String;
    :cond_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v4, v1

    .line 245
    iput-object v4, p0, Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;->clientAlteringExperiment:[Ljava/lang/String;

    goto :goto_0

    .line 249
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v4    # "newArray":[Ljava/lang/String;
    :sswitch_2
    const/16 v8, 0x12

    invoke-static {p1, v8}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 251
    .restart local v0    # "arrayLength":I
    iget-object v8, p0, Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;->otherExperiment:[Ljava/lang/String;

    if-nez v8, :cond_5

    move v1, v7

    .line 252
    .restart local v1    # "i":I
    :goto_3
    add-int v8, v1, v0

    new-array v4, v8, [Ljava/lang/String;

    .line 253
    .restart local v4    # "newArray":[Ljava/lang/String;
    if-eqz v1, :cond_4

    .line 254
    iget-object v8, p0, Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;->otherExperiment:[Ljava/lang/String;

    invoke-static {v8, v7, v4, v7, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 256
    :cond_4
    :goto_4
    array-length v8, v4

    add-int/lit8 v8, v8, -0x1

    if-ge v1, v8, :cond_6

    .line 257
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v4, v1

    .line 258
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 256
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 251
    .end local v1    # "i":I
    .end local v4    # "newArray":[Ljava/lang/String;
    :cond_5
    iget-object v8, p0, Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;->otherExperiment:[Ljava/lang/String;

    array-length v1, v8

    goto :goto_3

    .line 261
    .restart local v1    # "i":I
    .restart local v4    # "newArray":[Ljava/lang/String;
    :cond_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v4, v1

    .line 262
    iput-object v4, p0, Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;->otherExperiment:[Ljava/lang/String;

    goto :goto_0

    .line 266
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v4    # "newArray":[Ljava/lang/String;
    :sswitch_3
    const/16 v8, 0x18

    invoke-static {p1, v8}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 268
    .restart local v0    # "arrayLength":I
    iget-object v8, p0, Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;->gwsExperiment:[I

    if-nez v8, :cond_8

    move v1, v7

    .line 269
    .restart local v1    # "i":I
    :goto_5
    add-int v8, v1, v0

    new-array v4, v8, [I

    .line 270
    .local v4, "newArray":[I
    if-eqz v1, :cond_7

    .line 271
    iget-object v8, p0, Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;->gwsExperiment:[I

    invoke-static {v8, v7, v4, v7, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 273
    :cond_7
    :goto_6
    array-length v8, v4

    add-int/lit8 v8, v8, -0x1

    if-ge v1, v8, :cond_9

    .line 274
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v8

    aput v8, v4, v1

    .line 275
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 273
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 268
    .end local v1    # "i":I
    .end local v4    # "newArray":[I
    :cond_8
    iget-object v8, p0, Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;->gwsExperiment:[I

    array-length v1, v8

    goto :goto_5

    .line 278
    .restart local v1    # "i":I
    .restart local v4    # "newArray":[I
    :cond_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v8

    aput v8, v4, v1

    .line 279
    iput-object v4, p0, Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;->gwsExperiment:[I

    goto/16 :goto_0

    .line 283
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v4    # "newArray":[I
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readRawVarint32()I

    move-result v2

    .line 284
    .local v2, "length":I
    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->pushLimit(I)I

    move-result v3

    .line 286
    .local v3, "limit":I
    const/4 v0, 0x0

    .line 287
    .restart local v0    # "arrayLength":I
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->getPosition()I

    move-result v5

    .line 288
    .local v5, "startPos":I
    :goto_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->getBytesUntilLimit()I

    move-result v8

    if-lez v8, :cond_a

    .line 289
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    .line 290
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    .line 292
    :cond_a
    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->rewindToPosition(I)V

    .line 293
    iget-object v8, p0, Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;->gwsExperiment:[I

    if-nez v8, :cond_c

    move v1, v7

    .line 294
    .restart local v1    # "i":I
    :goto_8
    add-int v8, v1, v0

    new-array v4, v8, [I

    .line 295
    .restart local v4    # "newArray":[I
    if-eqz v1, :cond_b

    .line 296
    iget-object v8, p0, Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;->gwsExperiment:[I

    invoke-static {v8, v7, v4, v7, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 298
    :cond_b
    :goto_9
    array-length v8, v4

    if-ge v1, v8, :cond_d

    .line 299
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v8

    aput v8, v4, v1

    .line 298
    add-int/lit8 v1, v1, 0x1

    goto :goto_9

    .line 293
    .end local v1    # "i":I
    .end local v4    # "newArray":[I
    :cond_c
    iget-object v8, p0, Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;->gwsExperiment:[I

    array-length v1, v8

    goto :goto_8

    .line 301
    .restart local v1    # "i":I
    .restart local v4    # "newArray":[I
    :cond_d
    iput-object v4, p0, Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;->gwsExperiment:[I

    .line 302
    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->popLimit(I)V

    goto/16 :goto_0

    .line 222
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x1a -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 107
    invoke-virtual {p0, p1}, Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 148
    iget-object v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;->clientAlteringExperiment:[Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;->clientAlteringExperiment:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_1

    .line 149
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;->clientAlteringExperiment:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_1

    .line 150
    iget-object v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;->clientAlteringExperiment:[Ljava/lang/String;

    aget-object v0, v2, v1

    .line 151
    .local v0, "element":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 152
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 149
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 156
    .end local v0    # "element":Ljava/lang/String;
    .end local v1    # "i":I
    :cond_1
    iget-object v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;->otherExperiment:[Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;->otherExperiment:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_3

    .line 157
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;->otherExperiment:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_3

    .line 158
    iget-object v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;->otherExperiment:[Ljava/lang/String;

    aget-object v0, v2, v1

    .line 159
    .restart local v0    # "element":Ljava/lang/String;
    if-eqz v0, :cond_2

    .line 160
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 157
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 164
    .end local v0    # "element":Ljava/lang/String;
    .end local v1    # "i":I
    :cond_3
    iget-object v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;->gwsExperiment:[I

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;->gwsExperiment:[I

    array-length v2, v2

    if-lez v2, :cond_4

    .line 165
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_2
    iget-object v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;->gwsExperiment:[I

    array-length v2, v2

    if-ge v1, v2, :cond_4

    .line 166
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;->gwsExperiment:[I

    aget v3, v3, v1

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 165
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 169
    .end local v1    # "i":I
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 170
    return-void
.end method
