.class public final Lcom/google/android/play/R$color;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/play/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final play_action_button_text:I = 0x7f0c0135

.field public static final play_apps_primary_text:I = 0x7f0c0136

.field public static final play_avatar_focused_outline:I = 0x7f0c004f

.field public static final play_avatar_outline:I = 0x7f0c004c

.field public static final play_avatar_pressed_fill:I = 0x7f0c004d

.field public static final play_avatar_pressed_outline:I = 0x7f0c004e

.field public static final play_books_primary_text:I = 0x7f0c0138

.field public static final play_card_shadow_end_color:I = 0x7f0c0059

.field public static final play_card_shadow_start_color:I = 0x7f0c0058

.field public static final play_default_download_arc_color_offline:I = 0x7f0c0050

.field public static final play_disabled_grey:I = 0x7f0c001e

.field public static final play_dismissed_overlay:I = 0x7f0c0046

.field public static final play_fg_primary:I = 0x7f0c0028

.field public static final play_fg_secondary:I = 0x7f0c0029

.field public static final play_header_list_tab_text_color:I = 0x7f0c013a

.field public static final play_header_list_tab_underline_color:I = 0x7f0c0051

.field public static final play_main_background:I = 0x7f0c0022

.field public static final play_movies_primary_text:I = 0x7f0c013b

.field public static final play_multi_primary_text:I = 0x7f0c013d

.field public static final play_music_primary_text:I = 0x7f0c013f

.field public static final play_newsstand_primary_text:I = 0x7f0c0141

.field public static final play_onboard__page_indicator_dot_active:I = 0x7f0c005f

.field public static final play_onboard__page_indicator_dot_inactive:I = 0x7f0c0060

.field public static final play_onboard_accent_color_a:I = 0x7f0c0065

.field public static final play_onboard_accent_color_b:I = 0x7f0c0066

.field public static final play_onboard_accent_color_c:I = 0x7f0c0067

.field public static final play_onboard_accent_color_d:I = 0x7f0c0068

.field public static final play_onboard_app_color_dark:I = 0x7f0c005e

.field public static final play_onboard_simple_quiz_background:I = 0x7f0c0064

.field public static final play_reason_separator:I = 0x7f0c0023

.field public static final play_search_plate_navigation_button_color:I = 0x7f0c005b

.field public static final play_tab_strip_text_selected:I = 0x7f0c0027

.field public static final play_transparent:I = 0x7f0c001f

.field public static final play_white:I = 0x7f0c001a
