.class public Lcom/google/android/play/search/PlaySearchOneSuggestion;
.super Landroid/widget/RelativeLayout;
.source "PlaySearchOneSuggestion.java"


# instance fields
.field private mDivider:Landroid/view/View;

.field private mIcon:Landroid/widget/ImageView;

.field private mSuggestText:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 23
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/play/search/PlaySearchOneSuggestion;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 24
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 27
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/play/search/PlaySearchOneSuggestion;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 31
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 32
    return-void
.end method


# virtual methods
.method public bindSuggestion(Ljava/lang/String;Z)V
    .locals 2
    .param p1, "suggestion"    # Ljava/lang/String;
    .param p2, "showDivider"    # Z

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchOneSuggestion;->mSuggestText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 46
    iget-object v1, p0, Lcom/google/android/play/search/PlaySearchOneSuggestion;->mDivider:Landroid/view/View;

    if-eqz p2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 47
    return-void

    .line 46
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public onFinishInflate()V
    .locals 1

    .prologue
    .line 36
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 37
    sget v0, Lcom/google/android/play/R$id;->icon:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/search/PlaySearchOneSuggestion;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/play/search/PlaySearchOneSuggestion;->mIcon:Landroid/widget/ImageView;

    .line 38
    sget v0, Lcom/google/android/play/R$id;->suggest_text:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/search/PlaySearchOneSuggestion;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/play/search/PlaySearchOneSuggestion;->mSuggestText:Landroid/widget/TextView;

    .line 39
    sget v0, Lcom/google/android/play/R$id;->suggestion_divider:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/search/PlaySearchOneSuggestion;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/play/search/PlaySearchOneSuggestion;->mDivider:Landroid/view/View;

    .line 42
    return-void
.end method
