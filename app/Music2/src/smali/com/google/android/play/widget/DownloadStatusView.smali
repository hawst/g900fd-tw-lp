.class public Lcom/google/android/play/widget/DownloadStatusView;
.super Landroid/view/View;
.source "DownloadStatusView.java"


# instance fields
.field protected mArcPaintOffline:Landroid/graphics/Paint;

.field protected mArcPaintOnline:Landroid/graphics/Paint;

.field protected final mArcRect:Landroid/graphics/RectF;

.field protected mBackground:Landroid/graphics/drawable/Drawable;

.field protected mCompleteOverlay:Landroid/graphics/drawable/Drawable;

.field protected mDefaultOverlay:Landroid/graphics/drawable/Drawable;

.field protected mDocumentId:Ljava/lang/String;

.field protected mDownloadFraction:F

.field protected mDownloadRequested:Z

.field protected mIsOnline:Z

.field protected mMinInProgressDisplayFraction:F

.field protected mProgressArcInset:I

.field protected mProgressOverlay:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 93
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/play/widget/DownloadStatusView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 94
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 89
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/play/widget/DownloadStatusView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 90
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 84
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 62
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/play/widget/DownloadStatusView;->mDownloadRequested:Z

    .line 63
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/widget/DownloadStatusView;->mIsOnline:Z

    .line 64
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/play/widget/DownloadStatusView;->mDownloadFraction:F

    .line 80
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/play/widget/DownloadStatusView;->mArcRect:Landroid/graphics/RectF;

    .line 85
    invoke-direct {p0, p1, p2}, Lcom/google/android/play/widget/DownloadStatusView;->initialize(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 86
    return-void
.end method

.method private getArcPaintSwitched()Landroid/graphics/Paint;
    .locals 1

    .prologue
    .line 179
    iget-boolean v0, p0, Lcom/google/android/play/widget/DownloadStatusView;->mIsOnline:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/play/widget/DownloadStatusView;->mArcPaintOnline:Landroid/graphics/Paint;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/play/widget/DownloadStatusView;->mArcPaintOffline:Landroid/graphics/Paint;

    goto :goto_0
.end method

.method private initialize(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 28
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attributeSet"    # Landroid/util/AttributeSet;

    .prologue
    .line 97
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v23

    .line 98
    .local v23, "resources":Landroid/content/res/Resources;
    sget v25, Lcom/google/android/play/R$integer;->play_download_arc_min_percent:I

    move-object/from16 v0, v23

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v16

    .line 108
    .local v16, "fallbackMinInProgressDisplayPercent":I
    if-eqz p2, :cond_0

    .line 109
    sget-object v25, Lcom/google/android/play/R$styleable;->DownloadStatusView:[I

    const/16 v26, 0x0

    const/16 v27, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, v25

    move/from16 v3, v26

    move/from16 v4, v27

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v5

    .line 111
    .local v5, "a":Landroid/content/res/TypedArray;
    sget v25, Lcom/google/android/play/R$styleable;->DownloadStatusView_playDownloadArcColorOnline:I

    sget v26, Lcom/google/android/play/R$color;->play_default_download_arc_color_offline:I

    move/from16 v0, v25

    move/from16 v1, v26

    invoke-virtual {v5, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v7

    .line 113
    .local v7, "arcColorOnline":I
    sget v25, Lcom/google/android/play/R$styleable;->DownloadStatusView_playDownloadArcColorOffline:I

    sget v26, Lcom/google/android/play/R$color;->play_default_download_arc_color_offline:I

    move/from16 v0, v25

    move/from16 v1, v26

    invoke-virtual {v5, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v6

    .line 115
    .local v6, "arcColorOffline":I
    sget v25, Lcom/google/android/play/R$styleable;->DownloadStatusView_playDownloadArcMinPercent:I

    move/from16 v0, v25

    move/from16 v1, v16

    invoke-virtual {v5, v0, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v21

    .line 118
    .local v21, "minInProgressDisplayPercent":I
    sget v25, Lcom/google/android/play/R$styleable;->DownloadStatusView_playDownloadBackground:I

    sget v26, Lcom/google/android/play/R$drawable;->play_ic_download_background_medium:I

    move/from16 v0, v25

    move/from16 v1, v26

    invoke-virtual {v5, v0, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v13

    .line 121
    .local v13, "backgroundId":I
    sget v25, Lcom/google/android/play/R$styleable;->DownloadStatusView_playDownloadDefaultIcon:I

    sget v26, Lcom/google/android/play/R$drawable;->play_ic_download_default_medium:I

    move/from16 v0, v25

    move/from16 v1, v26

    invoke-virtual {v5, v0, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v15

    .line 124
    .local v15, "defaultId":I
    sget v25, Lcom/google/android/play/R$styleable;->DownloadStatusView_playDownloadProgressIcon:I

    sget v26, Lcom/google/android/play/R$drawable;->play_ic_download_progress_medium:I

    move/from16 v0, v25

    move/from16 v1, v26

    invoke-virtual {v5, v0, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v22

    .line 127
    .local v22, "progressId":I
    sget v25, Lcom/google/android/play/R$styleable;->DownloadStatusView_playDownloadCompleteIcon:I

    sget v26, Lcom/google/android/play/R$drawable;->play_ic_download_complete_medium:I

    move/from16 v0, v25

    move/from16 v1, v26

    invoke-virtual {v5, v0, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v14

    .line 130
    .local v14, "completeId":I
    sget v25, Lcom/google/android/play/R$styleable;->DownloadStatusView_playDownloadArcInset:I

    sget v26, Lcom/google/android/play/R$dimen;->play_download_button_asset_inset:I

    move/from16 v0, v25

    move/from16 v1, v26

    invoke-virtual {v5, v0, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v9

    .line 133
    .local v9, "arcInsetId":I
    invoke-virtual {v5}, Landroid/content/res/TypedArray;->recycle()V

    .line 144
    .end local v5    # "a":Landroid/content/res/TypedArray;
    :goto_0
    move-object/from16 v0, v23

    invoke-virtual {v0, v13}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/play/widget/DownloadStatusView;->mBackground:Landroid/graphics/drawable/Drawable;

    .line 145
    move-object/from16 v0, v23

    invoke-virtual {v0, v15}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/play/widget/DownloadStatusView;->mDefaultOverlay:Landroid/graphics/drawable/Drawable;

    .line 146
    move-object/from16 v0, v23

    invoke-virtual {v0, v14}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/play/widget/DownloadStatusView;->mCompleteOverlay:Landroid/graphics/drawable/Drawable;

    .line 147
    move-object/from16 v0, v23

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/play/widget/DownloadStatusView;->mProgressOverlay:Landroid/graphics/drawable/Drawable;

    .line 148
    move-object/from16 v0, v23

    invoke-virtual {v0, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v25

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/play/widget/DownloadStatusView;->mProgressArcInset:I

    .line 150
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/widget/DownloadStatusView;->getPaddingLeft()I

    move-result v20

    .line 151
    .local v20, "left":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/widget/DownloadStatusView;->getPaddingTop()I

    move-result v24

    .line 152
    .local v24, "top":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/widget/DownloadStatusView;->mBackground:Landroid/graphics/drawable/Drawable;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v19

    .line 153
    .local v19, "iconWidth":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/widget/DownloadStatusView;->mBackground:Landroid/graphics/drawable/Drawable;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v17

    .line 154
    .local v17, "iconHeight":I
    new-instance v18, Landroid/graphics/Rect;

    add-int v25, v20, v19

    add-int v26, v24, v17

    move-object/from16 v0, v18

    move/from16 v1, v20

    move/from16 v2, v24

    move/from16 v3, v25

    move/from16 v4, v26

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 156
    .local v18, "iconRect":Landroid/graphics/Rect;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/widget/DownloadStatusView;->mBackground:Landroid/graphics/drawable/Drawable;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 157
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/widget/DownloadStatusView;->mDefaultOverlay:Landroid/graphics/drawable/Drawable;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 158
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/widget/DownloadStatusView;->mCompleteOverlay:Landroid/graphics/drawable/Drawable;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 159
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/widget/DownloadStatusView;->mProgressOverlay:Landroid/graphics/drawable/Drawable;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 161
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/play/widget/DownloadStatusView;->mProgressArcInset:I

    move/from16 v25, v0

    add-int v25, v25, v20

    move/from16 v0, v25

    int-to-float v10, v0

    .line 162
    .local v10, "arcLeft":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/play/widget/DownloadStatusView;->mProgressArcInset:I

    move/from16 v25, v0

    add-int v25, v25, v24

    move/from16 v0, v25

    int-to-float v11, v0

    .line 163
    .local v11, "arcTop":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/play/widget/DownloadStatusView;->mProgressArcInset:I

    move/from16 v25, v0

    mul-int/lit8 v25, v25, 0x2

    sub-int v25, v19, v25

    move/from16 v0, v25

    int-to-float v12, v0

    .line 164
    .local v12, "arcWidth":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/play/widget/DownloadStatusView;->mProgressArcInset:I

    move/from16 v25, v0

    mul-int/lit8 v25, v25, 0x2

    sub-int v25, v17, v25

    move/from16 v0, v25

    int-to-float v8, v0

    .line 165
    .local v8, "arcHeight":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/widget/DownloadStatusView;->mArcRect:Landroid/graphics/RectF;

    move-object/from16 v25, v0

    add-float v26, v10, v12

    add-float v27, v11, v8

    move-object/from16 v0, v25

    move/from16 v1, v26

    move/from16 v2, v27

    invoke-virtual {v0, v10, v11, v1, v2}, Landroid/graphics/RectF;->set(FFFF)V

    .line 167
    new-instance v25, Landroid/graphics/Paint;

    invoke-direct/range {v25 .. v25}, Landroid/graphics/Paint;-><init>()V

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/play/widget/DownloadStatusView;->mArcPaintOnline:Landroid/graphics/Paint;

    .line 168
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/widget/DownloadStatusView;->mArcPaintOnline:Landroid/graphics/Paint;

    move-object/from16 v25, v0

    sget-object v26, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual/range {v25 .. v26}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 169
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/widget/DownloadStatusView;->mArcPaintOnline:Landroid/graphics/Paint;

    move-object/from16 v25, v0

    const/16 v26, 0x1

    invoke-virtual/range {v25 .. v26}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 170
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/widget/DownloadStatusView;->mArcPaintOnline:Landroid/graphics/Paint;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-virtual {v0, v7}, Landroid/graphics/Paint;->setColor(I)V

    .line 172
    new-instance v25, Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/widget/DownloadStatusView;->mArcPaintOnline:Landroid/graphics/Paint;

    move-object/from16 v26, v0

    invoke-direct/range {v25 .. v26}, Landroid/graphics/Paint;-><init>(Landroid/graphics/Paint;)V

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/play/widget/DownloadStatusView;->mArcPaintOffline:Landroid/graphics/Paint;

    .line 173
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/widget/DownloadStatusView;->mArcPaintOffline:Landroid/graphics/Paint;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-virtual {v0, v6}, Landroid/graphics/Paint;->setColor(I)V

    .line 175
    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v25, v0

    const/high16 v26, 0x42c80000    # 100.0f

    div-float v25, v25, v26

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/play/widget/DownloadStatusView;->mMinInProgressDisplayFraction:F

    .line 176
    return-void

    .line 135
    .end local v6    # "arcColorOffline":I
    .end local v7    # "arcColorOnline":I
    .end local v8    # "arcHeight":F
    .end local v9    # "arcInsetId":I
    .end local v10    # "arcLeft":F
    .end local v11    # "arcTop":F
    .end local v12    # "arcWidth":F
    .end local v13    # "backgroundId":I
    .end local v14    # "completeId":I
    .end local v15    # "defaultId":I
    .end local v17    # "iconHeight":I
    .end local v18    # "iconRect":Landroid/graphics/Rect;
    .end local v19    # "iconWidth":I
    .end local v20    # "left":I
    .end local v21    # "minInProgressDisplayPercent":I
    .end local v22    # "progressId":I
    .end local v24    # "top":I
    :cond_0
    sget v25, Lcom/google/android/play/R$color;->play_default_download_arc_color_offline:I

    move-object/from16 v0, v23

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    .line 136
    .restart local v7    # "arcColorOnline":I
    sget v25, Lcom/google/android/play/R$color;->play_default_download_arc_color_offline:I

    move-object/from16 v0, v23

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    .line 137
    .restart local v6    # "arcColorOffline":I
    move/from16 v21, v16

    .line 138
    .restart local v21    # "minInProgressDisplayPercent":I
    sget v13, Lcom/google/android/play/R$drawable;->play_ic_download_background_medium:I

    .line 139
    .restart local v13    # "backgroundId":I
    sget v15, Lcom/google/android/play/R$drawable;->play_ic_download_default_medium:I

    .line 140
    .restart local v15    # "defaultId":I
    sget v22, Lcom/google/android/play/R$drawable;->play_ic_download_progress_medium:I

    .line 141
    .restart local v22    # "progressId":I
    sget v14, Lcom/google/android/play/R$drawable;->play_ic_download_complete_medium:I

    .line 142
    .restart local v14    # "completeId":I
    sget v9, Lcom/google/android/play/R$dimen;->play_download_button_asset_inset:I

    .restart local v9    # "arcInsetId":I
    goto/16 :goto_0
.end method


# virtual methods
.method public getContentDescription()Ljava/lang/CharSequence;
    .locals 8

    .prologue
    .line 256
    invoke-virtual {p0}, Lcom/google/android/play/widget/DownloadStatusView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 257
    .local v2, "res":Landroid/content/res/Resources;
    iget v3, p0, Lcom/google/android/play/widget/DownloadStatusView;->mDownloadFraction:F

    const/high16 v4, 0x42c80000    # 100.0f

    mul-float/2addr v3, v4

    float-to-int v1, v3

    .line 258
    .local v1, "percentDownloaded":I
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    sget v4, Lcom/google/android/play/R$string;->play_percent_downloaded:I

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 261
    .local v0, "descriptionStringBuilder":Ljava/lang/StringBuilder;
    const/16 v3, 0x64

    if-ge v1, v3, :cond_0

    .line 262
    const-string v3, ", "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v3, p0, Lcom/google/android/play/widget/DownloadStatusView;->mDownloadRequested:Z

    if-eqz v3, :cond_1

    sget v3, Lcom/google/android/play/R$string;->play_download_is_requested:I

    :goto_0
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 266
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 262
    :cond_1
    sget v3, Lcom/google/android/play/R$string;->play_download_not_requested:I

    goto :goto_0
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 10
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v4, 0x1

    const/high16 v3, 0x43b40000    # 360.0f

    const/high16 v2, 0x43870000    # 270.0f

    .line 212
    iget-object v0, p0, Lcom/google/android/play/widget/DownloadStatusView;->mBackground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 215
    iget v0, p0, Lcom/google/android/play/widget/DownloadStatusView;->mDownloadFraction:F

    float-to-double v0, v0

    const-wide v8, 0x3e7ad7f29abcaf48L    # 1.0E-7

    cmpg-double v0, v0, v8

    if-gez v0, :cond_1

    .line 216
    iget-boolean v0, p0, Lcom/google/android/play/widget/DownloadStatusView;->mDownloadRequested:Z

    if-eqz v0, :cond_0

    .line 217
    iget-object v7, p0, Lcom/google/android/play/widget/DownloadStatusView;->mProgressOverlay:Landroid/graphics/drawable/Drawable;

    .line 218
    .local v7, "overlay":Landroid/graphics/drawable/Drawable;
    iget-object v1, p0, Lcom/google/android/play/widget/DownloadStatusView;->mArcRect:Landroid/graphics/RectF;

    iget v0, p0, Lcom/google/android/play/widget/DownloadStatusView;->mMinInProgressDisplayFraction:F

    mul-float/2addr v3, v0

    invoke-direct {p0}, Lcom/google/android/play/widget/DownloadStatusView;->getArcPaintSwitched()Landroid/graphics/Paint;

    move-result-object v5

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 235
    :goto_0
    invoke-virtual {v7, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 236
    return-void

    .line 221
    .end local v7    # "overlay":Landroid/graphics/drawable/Drawable;
    :cond_0
    iget-object v7, p0, Lcom/google/android/play/widget/DownloadStatusView;->mDefaultOverlay:Landroid/graphics/drawable/Drawable;

    .restart local v7    # "overlay":Landroid/graphics/drawable/Drawable;
    goto :goto_0

    .line 223
    .end local v7    # "overlay":Landroid/graphics/drawable/Drawable;
    :cond_1
    iget v0, p0, Lcom/google/android/play/widget/DownloadStatusView;->mDownloadFraction:F

    float-to-double v0, v0

    const-wide v8, 0x3fefffffca501acbL    # 0.9999999

    cmpg-double v0, v0, v8

    if-gez v0, :cond_2

    .line 224
    iget v0, p0, Lcom/google/android/play/widget/DownloadStatusView;->mDownloadFraction:F

    iget v1, p0, Lcom/google/android/play/widget/DownloadStatusView;->mMinInProgressDisplayFraction:F

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v6

    .line 226
    .local v6, "downloadFraction":F
    iget-object v1, p0, Lcom/google/android/play/widget/DownloadStatusView;->mArcRect:Landroid/graphics/RectF;

    mul-float/2addr v3, v6

    invoke-direct {p0}, Lcom/google/android/play/widget/DownloadStatusView;->getArcPaintSwitched()Landroid/graphics/Paint;

    move-result-object v5

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 227
    iget-object v7, p0, Lcom/google/android/play/widget/DownloadStatusView;->mProgressOverlay:Landroid/graphics/drawable/Drawable;

    .line 228
    .restart local v7    # "overlay":Landroid/graphics/drawable/Drawable;
    goto :goto_0

    .line 230
    .end local v6    # "downloadFraction":F
    .end local v7    # "overlay":Landroid/graphics/drawable/Drawable;
    :cond_2
    iget-object v0, p0, Lcom/google/android/play/widget/DownloadStatusView;->mArcRect:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/google/android/play/widget/DownloadStatusView;->mArcPaintOnline:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawOval(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 231
    iget-object v7, p0, Lcom/google/android/play/widget/DownloadStatusView;->mCompleteOverlay:Landroid/graphics/drawable/Drawable;

    .restart local v7    # "overlay":Landroid/graphics/drawable/Drawable;
    goto :goto_0
.end method

.method public onMeasure(II)V
    .locals 8
    .param p1, "widthSpec"    # I
    .param p2, "heightSpec"    # I

    .prologue
    .line 240
    invoke-virtual {p0}, Lcom/google/android/play/widget/DownloadStatusView;->getPaddingLeft()I

    move-result v2

    .line 241
    .local v2, "paddingLeft":I
    invoke-virtual {p0}, Lcom/google/android/play/widget/DownloadStatusView;->getPaddingTop()I

    move-result v3

    .line 242
    .local v3, "paddingTop":I
    iget-object v6, p0, Lcom/google/android/play/widget/DownloadStatusView;->mBackground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    .line 243
    .local v1, "backgroundWidth":I
    iget-object v6, p0, Lcom/google/android/play/widget/DownloadStatusView;->mBackground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    .line 245
    .local v0, "backgroundHeight":I
    add-int v6, v1, v2

    invoke-virtual {p0}, Lcom/google/android/play/widget/DownloadStatusView;->getPaddingRight()I

    move-result v7

    add-int v5, v6, v7

    .line 246
    .local v5, "viewWidth":I
    add-int v6, v0, v3

    invoke-virtual {p0}, Lcom/google/android/play/widget/DownloadStatusView;->getPaddingBottom()I

    move-result v7

    add-int v4, v6, v7

    .line 248
    .local v4, "viewHeight":I
    invoke-virtual {p0, v5, v4}, Lcom/google/android/play/widget/DownloadStatusView;->setMeasuredDimension(II)V

    .line 249
    return-void
.end method

.method public setDocumentId(Ljava/lang/String;)V
    .locals 0
    .param p1, "documentId"    # Ljava/lang/String;

    .prologue
    .line 270
    iput-object p1, p0, Lcom/google/android/play/widget/DownloadStatusView;->mDocumentId:Ljava/lang/String;

    .line 271
    return-void
.end method

.method public setDownloadFraction(F)V
    .locals 0
    .param p1, "downloadFraction"    # F

    .prologue
    .line 195
    iput p1, p0, Lcom/google/android/play/widget/DownloadStatusView;->mDownloadFraction:F

    .line 196
    invoke-virtual {p0}, Lcom/google/android/play/widget/DownloadStatusView;->invalidate()V

    .line 197
    return-void
.end method

.method public setDownloadRequested(Z)V
    .locals 0
    .param p1, "downloadRequested"    # Z

    .prologue
    .line 186
    iput-boolean p1, p0, Lcom/google/android/play/widget/DownloadStatusView;->mDownloadRequested:Z

    .line 187
    invoke-virtual {p0}, Lcom/google/android/play/widget/DownloadStatusView;->invalidate()V

    .line 188
    return-void
.end method

.method public setOnline(Z)V
    .locals 0
    .param p1, "isOnline"    # Z

    .prologue
    .line 203
    iput-boolean p1, p0, Lcom/google/android/play/widget/DownloadStatusView;->mIsOnline:Z

    .line 204
    invoke-virtual {p0}, Lcom/google/android/play/widget/DownloadStatusView;->invalidate()V

    .line 205
    return-void
.end method
