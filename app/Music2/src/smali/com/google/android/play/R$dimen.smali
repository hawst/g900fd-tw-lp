.class public final Lcom/google/android/play/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/play/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final abc_action_bar_default_height_material:I = 0x7f0f026b

.field public static final play_avatar_decoration_threshold_max:I = 0x7f0f0033

.field public static final play_avatar_decoration_threshold_min:I = 0x7f0f0032

.field public static final play_avatar_drop_shadow_max:I = 0x7f0f0037

.field public static final play_avatar_drop_shadow_min:I = 0x7f0f0036

.field public static final play_avatar_noring_outline:I = 0x7f0f0038

.field public static final play_avatar_ring_size_max:I = 0x7f0f0035

.field public static final play_avatar_ring_size_min:I = 0x7f0f0034

.field public static final play_card_default_inset:I = 0x7f0f0052

.field public static final play_card_extra_vspace:I = 0x7f0f0018

.field public static final play_card_label_icon_gap:I = 0x7f0f0027

.field public static final play_card_label_texts_gap:I = 0x7f0f0028

.field public static final play_card_overflow_touch_extend:I = 0x7f0f001b

.field public static final play_card_snippet_avatar_large_size:I = 0x7f0f001f

.field public static final play_card_snippet_avatar_size:I = 0x7f0f001e

.field public static final play_card_snippet_text_extra_margin_left:I = 0x7f0f0023

.field public static final play_download_button_asset_inset:I = 0x7f0f0039

.field public static final play_drawer_max_width:I = 0x7f0f0030

.field public static final play_hairline_separator_thickness:I = 0x7f0f0013

.field public static final play_header_list_banner_height:I = 0x7f0f003a

.field public static final play_header_list_floating_elevation:I = 0x7f0f0040

.field public static final play_header_list_tab_floating_padding:I = 0x7f0f0042

.field public static final play_header_list_tab_strip_height:I = 0x7f0f003b

.field public static final play_header_list_tab_strip_selected_underline_height:I = 0x7f0f003c

.field public static final play_medium_size:I = 0x7f0f0065

.field public static final play_mini_card_content_height:I = 0x7f0f0025

.field public static final play_mini_card_label_threshold:I = 0x7f0f0026

.field public static final play_onboard__onboard_nav_footer_height:I = 0x7f0f0086

.field public static final play_onboard__onboard_simple_quiz_row_spacing:I = 0x7f0f008c

.field public static final play_onboard__page_indicator_dot_diameter:I = 0x7f0f0088

.field public static final play_search_one_suggestion_height:I = 0x7f0f005f

.field public static final play_small_card_content_min_height:I = 0x7f0f0024

.field public static final play_snippet_large_size:I = 0x7f0f006c

.field public static final play_snippet_regular_size:I = 0x7f0f006b

.field public static final play_star_height_default:I = 0x7f0f004f

.field public static final play_tab_strip_selected_underline_height:I = 0x7f0f000e

.field public static final play_tab_strip_title_offset:I = 0x7f0f0011

.field public static final play_text_view_fadeout:I = 0x7f0f0014

.field public static final play_text_view_fadeout_hint_margin:I = 0x7f0f0015

.field public static final play_text_view_outline:I = 0x7f0f0016
