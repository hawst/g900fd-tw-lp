.class public Lcom/google/android/play/playperf/measurements/time/ElapsedTime;
.super Ljava/lang/Object;
.source "ElapsedTime.java"

# interfaces
.implements Lcom/google/android/play/playperf/measurements/Recordable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/play/playperf/measurements/time/ElapsedTime;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mSectionName:Ljava/lang/String;

.field private final mTicks:Lcom/google/android/play/playperf/data/DataPoint;

.field private final mUnits:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 83
    new-instance v0, Lcom/google/android/play/playperf/measurements/time/ElapsedTime$1;

    invoke-direct {v0}, Lcom/google/android/play/playperf/measurements/time/ElapsedTime$1;-><init>()V

    sput-object v0, Lcom/google/android/play/playperf/measurements/time/ElapsedTime;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "parcel"    # Landroid/os/Parcel;

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/playperf/data/DataPoint;

    iput-object v0, p0, Lcom/google/android/play/playperf/measurements/time/ElapsedTime;->mTicks:Lcom/google/android/play/playperf/data/DataPoint;

    .line 29
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/play/playperf/measurements/time/ElapsedTime;->mSectionName:Ljava/lang/String;

    .line 30
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/play/playperf/measurements/time/ElapsedTime;->mUnits:Ljava/lang/String;

    .line 31
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 70
    const/4 v0, 0x0

    return v0
.end method

.method public getSectionName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/play/playperf/measurements/time/ElapsedTime;->mSectionName:Ljava/lang/String;

    return-object v0
.end method

.method public getTicks()Lcom/google/android/play/playperf/data/DataPoint;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/play/playperf/measurements/time/ElapsedTime;->mTicks:Lcom/google/android/play/playperf/data/DataPoint;

    return-object v0
.end method

.method public getUnits()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/play/playperf/measurements/time/ElapsedTime;->mUnits:Ljava/lang/String;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 78
    invoke-virtual {p0}, Lcom/google/android/play/playperf/measurements/time/ElapsedTime;->getTicks()Lcom/google/android/play/playperf/data/DataPoint;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 79
    invoke-virtual {p0}, Lcom/google/android/play/playperf/measurements/time/ElapsedTime;->getSectionName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 80
    invoke-virtual {p0}, Lcom/google/android/play/playperf/measurements/time/ElapsedTime;->getUnits()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 81
    return-void
.end method
