.class public Lcom/google/android/play/playperf/measurements/remote/ProcessControlService;
.super Lcom/google/android/play/playperf/measurements/remote/IProcessControlService$Stub;
.source "ProcessControlService.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/google/android/play/playperf/measurements/remote/IProcessControlService$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public triggerGarbageCollection(JZ)V
    .locals 1
    .param p1, "timeout"    # J
    .param p3, "throwException"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 13
    invoke-static {p1, p2, p3}, Lcom/google/android/play/playperf/measurements/remote/ProcessMonitor;->triggerGarbageCollection(JZ)V

    .line 14
    return-void
.end method
