.class Lcom/google/android/play/image/BitmapLoader$2;
.super Ljava/lang/Object;
.source "BitmapLoader.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/play/image/BitmapLoader;->batchResponse(Ljava/lang/String;Lcom/google/android/play/image/BitmapLoader$RequestListenerWrapper;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/play/image/BitmapLoader;


# direct methods
.method constructor <init>(Lcom/google/android/play/image/BitmapLoader;)V
    .locals 0

    .prologue
    .line 519
    iput-object p1, p0, Lcom/google/android/play/image/BitmapLoader$2;->this$0:Lcom/google/android/play/image/BitmapLoader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 522
    iget-object v6, p0, Lcom/google/android/play/image/BitmapLoader$2;->this$0:Lcom/google/android/play/image/BitmapLoader;

    # getter for: Lcom/google/android/play/image/BitmapLoader;->mBatchedResponses:Ljava/util/HashMap;
    invoke-static {v6}, Lcom/google/android/play/image/BitmapLoader;->access$600(Lcom/google/android/play/image/BitmapLoader;)Ljava/util/HashMap;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/play/image/BitmapLoader$RequestListenerWrapper;

    .line 523
    .local v5, "wrapper":Lcom/google/android/play/image/BitmapLoader$RequestListenerWrapper;
    # getter for: Lcom/google/android/play/image/BitmapLoader$RequestListenerWrapper;->handlers:Ljava/util/List;
    invoke-static {v5}, Lcom/google/android/play/image/BitmapLoader$RequestListenerWrapper;->access$700(Lcom/google/android/play/image/BitmapLoader$RequestListenerWrapper;)Ljava/util/List;

    move-result-object v2

    .line 524
    .local v2, "containers":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/play/image/BitmapLoader$BitmapContainer;>;"
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    .line 525
    .local v1, "containerCount":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v1, :cond_0

    .line 526
    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/image/BitmapLoader$BitmapContainer;

    .line 527
    .local v0, "container":Lcom/google/android/play/image/BitmapLoader$BitmapContainer;
    # getter for: Lcom/google/android/play/image/BitmapLoader$RequestListenerWrapper;->responseBitmap:Landroid/graphics/Bitmap;
    invoke-static {v5}, Lcom/google/android/play/image/BitmapLoader$RequestListenerWrapper;->access$400(Lcom/google/android/play/image/BitmapLoader$RequestListenerWrapper;)Landroid/graphics/Bitmap;

    move-result-object v6

    # setter for: Lcom/google/android/play/image/BitmapLoader$BitmapContainer;->mBitmap:Landroid/graphics/Bitmap;
    invoke-static {v0, v6}, Lcom/google/android/play/image/BitmapLoader$BitmapContainer;->access$802(Lcom/google/android/play/image/BitmapLoader$BitmapContainer;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 528
    # getter for: Lcom/google/android/play/image/BitmapLoader$BitmapContainer;->mBitmapLoaded:Lcom/google/android/play/image/BitmapLoader$BitmapLoadedHandler;
    invoke-static {v0}, Lcom/google/android/play/image/BitmapLoader$BitmapContainer;->access$900(Lcom/google/android/play/image/BitmapLoader$BitmapContainer;)Lcom/google/android/play/image/BitmapLoader$BitmapLoadedHandler;

    move-result-object v6

    invoke-interface {v6, v0}, Lcom/google/android/play/image/BitmapLoader$BitmapLoadedHandler;->onResponse(Lcom/google/android/play/image/BitmapLoader$BitmapContainer;)V

    .line 525
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 531
    .end local v0    # "container":Lcom/google/android/play/image/BitmapLoader$BitmapContainer;
    .end local v1    # "containerCount":I
    .end local v2    # "containers":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/play/image/BitmapLoader$BitmapContainer;>;"
    .end local v3    # "i":I
    .end local v5    # "wrapper":Lcom/google/android/play/image/BitmapLoader$RequestListenerWrapper;
    :cond_1
    iget-object v6, p0, Lcom/google/android/play/image/BitmapLoader$2;->this$0:Lcom/google/android/play/image/BitmapLoader;

    # getter for: Lcom/google/android/play/image/BitmapLoader;->mBatchedResponses:Ljava/util/HashMap;
    invoke-static {v6}, Lcom/google/android/play/image/BitmapLoader;->access$600(Lcom/google/android/play/image/BitmapLoader;)Ljava/util/HashMap;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/HashMap;->clear()V

    .line 532
    iget-object v6, p0, Lcom/google/android/play/image/BitmapLoader$2;->this$0:Lcom/google/android/play/image/BitmapLoader;

    const/4 v7, 0x0

    # setter for: Lcom/google/android/play/image/BitmapLoader;->mRunnable:Ljava/lang/Runnable;
    invoke-static {v6, v7}, Lcom/google/android/play/image/BitmapLoader;->access$1002(Lcom/google/android/play/image/BitmapLoader;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 533
    return-void
.end method
