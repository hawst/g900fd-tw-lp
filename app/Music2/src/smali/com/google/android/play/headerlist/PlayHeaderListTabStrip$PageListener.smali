.class Lcom/google/android/play/headerlist/PlayHeaderListTabStrip$PageListener;
.super Landroid/database/DataSetObserver;
.source "PlayHeaderListTabStrip.java"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PageListener"
.end annotation


# instance fields
.field private mScrollState:I

.field final synthetic this$0:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;


# direct methods
.method private constructor <init>(Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;)V
    .locals 0

    .prologue
    .line 376
    iput-object p1, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip$PageListener;->this$0:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;Lcom/google/android/play/headerlist/PlayHeaderListTabStrip$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;
    .param p2, "x1"    # Lcom/google/android/play/headerlist/PlayHeaderListTabStrip$1;

    .prologue
    .line 376
    invoke-direct {p0, p1}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip$PageListener;-><init>(Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;)V

    return-void
.end method


# virtual methods
.method public onChanged()V
    .locals 1

    .prologue
    .line 429
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip$PageListener;->this$0:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    # invokes: Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->updateTabs()V
    invoke-static {v0}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->access$700(Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;)V

    .line 430
    return-void
.end method

.method public onPageScrollStateChanged(I)V
    .locals 2
    .param p1, "state"    # I

    .prologue
    .line 416
    iput p1, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip$PageListener;->mScrollState:I

    .line 417
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip$PageListener;->this$0:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    # getter for: Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mExternalPageChangeListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;
    invoke-static {v0}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->access$600(Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;)Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 418
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip$PageListener;->this$0:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    # getter for: Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mExternalPageChangeListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;
    invoke-static {v0}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->access$600(Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;)Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/support/v4/view/ViewPager$OnPageChangeListener;->onPageScrollStateChanged(I)V

    .line 420
    :cond_0
    iget v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip$PageListener;->mScrollState:I

    if-nez v0, :cond_1

    .line 423
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip$PageListener;->this$0:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mEnablePagerScrollSync:Z
    invoke-static {v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->access$502(Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;Z)Z

    .line 425
    :cond_1
    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 10
    .param p1, "visualPosition"    # I
    .param p2, "visualPositionOffset"    # F
    .param p3, "visualPositionOffsetPixels"    # I

    .prologue
    const/4 v7, 0x0

    .line 382
    iget-object v8, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip$PageListener;->this$0:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    # getter for: Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mExternalPageChangeListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;
    invoke-static {v8}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->access$600(Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;)Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    move-result-object v8

    if-eqz v8, :cond_0

    .line 383
    iget-object v8, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip$PageListener;->this$0:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    # getter for: Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mExternalPageChangeListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;
    invoke-static {v8}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->access$600(Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;)Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    move-result-object v8

    invoke-interface {v8, p1, p2, p3}, Landroid/support/v4/view/ViewPager$OnPageChangeListener;->onPageScrolled(IFI)V

    .line 386
    :cond_0
    iget-object v8, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip$PageListener;->this$0:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    # getter for: Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mEnablePagerScrollSync:Z
    invoke-static {v8}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->access$500(Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 403
    :cond_1
    :goto_0
    return-void

    .line 389
    :cond_2
    iget-object v8, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip$PageListener;->this$0:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    # getter for: Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mTabContainer:Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;
    invoke-static {v8}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->access$300(Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;)Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->getChildCount()I

    move-result v6

    .line 390
    .local v6, "tabStripChildCount":I
    if-eqz v6, :cond_1

    if-ltz p1, :cond_1

    if-ge p1, v6, :cond_1

    .line 394
    iget-object v8, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip$PageListener;->this$0:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    # getter for: Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mTabContainer:Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;
    invoke-static {v8}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->access$300(Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;)Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;

    move-result-object v8

    invoke-virtual {v8, p1, p2}, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->onPageScrolled(IF)V

    .line 395
    iget-object v8, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip$PageListener;->this$0:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    # getter for: Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mTabContainer:Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;
    invoke-static {v8}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->access$300(Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;)Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;

    move-result-object v8

    invoke-virtual {v8, p1}, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 396
    .local v4, "selectedTitle":Landroid/view/View;
    if-nez v4, :cond_3

    move v5, v7

    .line 397
    .local v5, "selectedTitleWidth":I
    :goto_1
    add-int/lit8 v2, p1, 0x1

    .line 398
    .local v2, "nextTitleVisualPosition":I
    iget-object v8, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip$PageListener;->this$0:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    # getter for: Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mTabContainer:Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;
    invoke-static {v8}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->access$300(Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;)Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;

    move-result-object v8

    invoke-virtual {v8, v2}, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 399
    .local v1, "nextTitle":Landroid/view/View;
    if-nez v1, :cond_4

    move v3, v7

    .line 400
    .local v3, "nextTitleWidth":I
    :goto_2
    add-int v8, v5, v3

    int-to-float v8, v8

    mul-float/2addr v8, p2

    const/high16 v9, 0x3f000000    # 0.5f

    mul-float/2addr v8, v9

    float-to-int v0, v8

    .line 402
    .local v0, "extraOffset":I
    iget-object v8, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip$PageListener;->this$0:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    # invokes: Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->scrollToVisualPosition(IIZ)V
    invoke-static {v8, p1, v0, v7}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->access$200(Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;IIZ)V

    goto :goto_0

    .line 396
    .end local v0    # "extraOffset":I
    .end local v1    # "nextTitle":Landroid/view/View;
    .end local v2    # "nextTitleVisualPosition":I
    .end local v3    # "nextTitleWidth":I
    .end local v5    # "selectedTitleWidth":I
    :cond_3
    invoke-virtual {v4}, Landroid/view/View;->getWidth()I

    move-result v5

    goto :goto_1

    .line 399
    .restart local v1    # "nextTitle":Landroid/view/View;
    .restart local v2    # "nextTitleVisualPosition":I
    .restart local v5    # "selectedTitleWidth":I
    :cond_4
    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v3

    goto :goto_2
.end method

.method public onPageSelected(I)V
    .locals 2
    .param p1, "visualPosition"    # I

    .prologue
    .line 407
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip$PageListener;->this$0:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    # getter for: Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mExternalPageChangeListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;
    invoke-static {v0}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->access$600(Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;)Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 408
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip$PageListener;->this$0:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    # getter for: Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mExternalPageChangeListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;
    invoke-static {v0}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->access$600(Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;)Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/support/v4/view/ViewPager$OnPageChangeListener;->onPageSelected(I)V

    .line 410
    :cond_0
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip$PageListener;->this$0:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    # getter for: Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mTabContainer:Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;
    invoke-static {v0}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->access$300(Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;)Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->onPageSelected(I)V

    .line 411
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip$PageListener;->this$0:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->updateSelectedTab(Z)V

    .line 412
    return-void
.end method
