.class Lcom/google/android/play/playperf/measurements/memory/MemoryStatisticsMeasurerFactory$1;
.super Ljava/lang/Object;
.source "MemoryStatisticsMeasurerFactory.java"

# interfaces
.implements Lcom/google/android/play/playperf/measurements/Poller;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/play/playperf/measurements/memory/MemoryStatisticsMeasurerFactory;->getMeasurerInstance(Ljava/lang/String;)Lcom/google/android/play/playperf/measurements/Measurer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/play/playperf/measurements/memory/MemoryStatisticsMeasurerFactory;

.field final synthetic val$heapTotalStatistics:Lcom/google/android/play/playperf/measurements/StatisticsAggregator;

.field final synthetic val$heapUsedStatistics:Lcom/google/android/play/playperf/measurements/StatisticsAggregator;


# direct methods
.method constructor <init>(Lcom/google/android/play/playperf/measurements/memory/MemoryStatisticsMeasurerFactory;Lcom/google/android/play/playperf/measurements/StatisticsAggregator;Lcom/google/android/play/playperf/measurements/StatisticsAggregator;)V
    .locals 0

    .prologue
    .line 33
    iput-object p1, p0, Lcom/google/android/play/playperf/measurements/memory/MemoryStatisticsMeasurerFactory$1;->this$0:Lcom/google/android/play/playperf/measurements/memory/MemoryStatisticsMeasurerFactory;

    iput-object p2, p0, Lcom/google/android/play/playperf/measurements/memory/MemoryStatisticsMeasurerFactory$1;->val$heapUsedStatistics:Lcom/google/android/play/playperf/measurements/StatisticsAggregator;

    iput-object p3, p0, Lcom/google/android/play/playperf/measurements/memory/MemoryStatisticsMeasurerFactory$1;->val$heapTotalStatistics:Lcom/google/android/play/playperf/measurements/StatisticsAggregator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public poll(J)V
    .locals 7
    .param p1, "currentTime"    # J

    .prologue
    .line 36
    new-instance v0, Lcom/google/android/play/playperf/measurements/memory/JavaMemorySnapshot;

    invoke-direct {v0}, Lcom/google/android/play/playperf/measurements/memory/JavaMemorySnapshot;-><init>()V

    .line 37
    .local v0, "memShot":Lcom/google/android/play/playperf/measurements/memory/JavaMemorySnapshot;
    iget-object v1, p0, Lcom/google/android/play/playperf/measurements/memory/MemoryStatisticsMeasurerFactory$1;->val$heapUsedStatistics:Lcom/google/android/play/playperf/measurements/StatisticsAggregator;

    new-instance v2, Lcom/google/android/play/playperf/data/DataPoint;

    invoke-virtual {v0}, Lcom/google/android/play/playperf/measurements/memory/JavaMemorySnapshot;->getJvmHeapUsedB()J

    move-result-wide v4

    long-to-double v4, v4

    invoke-direct {v2, p1, p2, v4, v5}, Lcom/google/android/play/playperf/data/DataPoint;-><init>(JD)V

    invoke-virtual {v1, v2}, Lcom/google/android/play/playperf/measurements/StatisticsAggregator;->addSample(Lcom/google/android/play/playperf/data/DataPoint;)V

    .line 39
    iget-object v1, p0, Lcom/google/android/play/playperf/measurements/memory/MemoryStatisticsMeasurerFactory$1;->val$heapTotalStatistics:Lcom/google/android/play/playperf/measurements/StatisticsAggregator;

    new-instance v2, Lcom/google/android/play/playperf/data/DataPoint;

    invoke-virtual {v0}, Lcom/google/android/play/playperf/measurements/memory/JavaMemorySnapshot;->getJvmHeapTotalB()J

    move-result-wide v4

    long-to-double v4, v4

    invoke-direct {v2, p1, p2, v4, v5}, Lcom/google/android/play/playperf/data/DataPoint;-><init>(JD)V

    invoke-virtual {v1, v2}, Lcom/google/android/play/playperf/measurements/StatisticsAggregator;->addSample(Lcom/google/android/play/playperf/data/DataPoint;)V

    .line 41
    return-void
.end method
