.class Lcom/google/android/play/drawer/PlayDrawerAdapter$5;
.super Ljava/lang/Object;
.source "PlayDrawerAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/play/drawer/PlayDrawerAdapter;->getProfileContainer(Landroid/view/View;Landroid/view/ViewGroup;I)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/play/drawer/PlayDrawerAdapter;


# direct methods
.method constructor <init>(Lcom/google/android/play/drawer/PlayDrawerAdapter;)V
    .locals 0

    .prologue
    .line 425
    iput-object p1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$5;->this$0:Lcom/google/android/play/drawer/PlayDrawerAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 429
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$5;->this$0:Lcom/google/android/play/drawer/PlayDrawerAdapter;

    # getter for: Lcom/google/android/play/drawer/PlayDrawerAdapter;->mPlayDrawerContentClickListener:Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerContentClickListener;
    invoke-static {v0}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->access$600(Lcom/google/android/play/drawer/PlayDrawerAdapter;)Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerContentClickListener;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$5;->this$0:Lcom/google/android/play/drawer/PlayDrawerAdapter;

    # getter for: Lcom/google/android/play/drawer/PlayDrawerAdapter;->mAccountListExpanded:Z
    invoke-static {v0}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->access$900(Lcom/google/android/play/drawer/PlayDrawerAdapter;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-interface {v1, v0}, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerContentClickListener;->onAccountListToggleButtonClicked(Z)V

    .line 431
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$5;->this$0:Lcom/google/android/play/drawer/PlayDrawerAdapter;

    # invokes: Lcom/google/android/play/drawer/PlayDrawerAdapter;->toggleAccountsList()V
    invoke-static {v0}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->access$1000(Lcom/google/android/play/drawer/PlayDrawerAdapter;)V

    .line 438
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$5;->this$0:Lcom/google/android/play/drawer/PlayDrawerAdapter;

    invoke-virtual {v0}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->isAccountListExpanded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 439
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$5;->this$0:Lcom/google/android/play/drawer/PlayDrawerAdapter;

    # invokes: Lcom/google/android/play/drawer/PlayDrawerAdapter;->loadAllSecondaryAccountDocV2sOnce()V
    invoke-static {v0}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->access$1100(Lcom/google/android/play/drawer/PlayDrawerAdapter;)V

    .line 441
    :cond_0
    return-void

    .line 429
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
