.class public Lcom/google/android/play/playperf/measurements/memory/AndroidMemorySnapshot;
.super Lcom/google/android/play/playperf/measurements/memory/JavaMemorySnapshot;
.source "AndroidMemorySnapshot.java"


# instance fields
.field private final jvmPrivateDirtyB:J

.field private final jvmPssB:J

.field public final nativeHeapAllocatedB:J

.field private final otherPrivateDirtyB:J

.field private final otherPssB:J


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/google/android/play/playperf/measurements/memory/JavaMemorySnapshot;-><init>()V

    .line 28
    new-instance v0, Landroid/os/Debug$MemoryInfo;

    invoke-direct {v0}, Landroid/os/Debug$MemoryInfo;-><init>()V

    .line 29
    .local v0, "memoryInfo":Landroid/os/Debug$MemoryInfo;
    invoke-static {v0}, Landroid/os/Debug;->getMemoryInfo(Landroid/os/Debug$MemoryInfo;)V

    .line 31
    invoke-static {}, Landroid/os/Debug;->getNativeHeapAllocatedSize()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/play/playperf/measurements/memory/AndroidMemorySnapshot;->nativeHeapAllocatedB:J

    .line 32
    iget v1, v0, Landroid/os/Debug$MemoryInfo;->otherPrivateDirty:I

    int-to-long v2, v1

    invoke-static {v2, v3}, Lcom/google/android/play/playperf/measurements/memory/AndroidMemorySnapshot;->kbToB(J)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/play/playperf/measurements/memory/AndroidMemorySnapshot;->otherPrivateDirtyB:J

    .line 33
    iget v1, v0, Landroid/os/Debug$MemoryInfo;->otherPss:I

    int-to-long v2, v1

    invoke-static {v2, v3}, Lcom/google/android/play/playperf/measurements/memory/AndroidMemorySnapshot;->kbToB(J)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/play/playperf/measurements/memory/AndroidMemorySnapshot;->otherPssB:J

    .line 34
    iget v1, v0, Landroid/os/Debug$MemoryInfo;->dalvikPrivateDirty:I

    int-to-long v2, v1

    invoke-static {v2, v3}, Lcom/google/android/play/playperf/measurements/memory/AndroidMemorySnapshot;->kbToB(J)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/play/playperf/measurements/memory/AndroidMemorySnapshot;->jvmPrivateDirtyB:J

    .line 35
    iget v1, v0, Landroid/os/Debug$MemoryInfo;->dalvikPss:I

    int-to-long v2, v1

    invoke-static {v2, v3}, Lcom/google/android/play/playperf/measurements/memory/AndroidMemorySnapshot;->kbToB(J)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/play/playperf/measurements/memory/AndroidMemorySnapshot;->jvmPssB:J

    .line 36
    return-void
.end method

.method constructor <init>(JJJJJJJJ)V
    .locals 11
    .param p1, "otherPrivateDirtyB"    # J
    .param p3, "otherPssB"    # J
    .param p5, "jvmPrivateDirtyB"    # J
    .param p7, "jvmPssB"    # J
    .param p9, "jvmHeapUsedB"    # J
    .param p11, "jvmHeapTotalB"    # J
    .param p13, "jvmMaxHeapSizeB"    # J
    .param p15, "nativeHeapAllocatedB"    # J

    .prologue
    .line 41
    move-object v3, p0

    move-wide/from16 v4, p9

    move-wide/from16 v6, p11

    move-wide/from16 v8, p13

    invoke-direct/range {v3 .. v9}, Lcom/google/android/play/playperf/measurements/memory/JavaMemorySnapshot;-><init>(JJJ)V

    .line 43
    iput-wide p1, p0, Lcom/google/android/play/playperf/measurements/memory/AndroidMemorySnapshot;->otherPrivateDirtyB:J

    .line 44
    iput-wide p3, p0, Lcom/google/android/play/playperf/measurements/memory/AndroidMemorySnapshot;->otherPssB:J

    .line 45
    move-wide/from16 v0, p5

    iput-wide v0, p0, Lcom/google/android/play/playperf/measurements/memory/AndroidMemorySnapshot;->jvmPrivateDirtyB:J

    .line 46
    move-wide/from16 v0, p7

    iput-wide v0, p0, Lcom/google/android/play/playperf/measurements/memory/AndroidMemorySnapshot;->jvmPssB:J

    .line 48
    move-wide/from16 v0, p15

    iput-wide v0, p0, Lcom/google/android/play/playperf/measurements/memory/AndroidMemorySnapshot;->nativeHeapAllocatedB:J

    .line 49
    return-void
.end method

.method private static kbToB(J)J
    .locals 2
    .param p0, "kb"    # J

    .prologue
    .line 89
    const-wide/16 v0, 0x400

    mul-long/2addr v0, p0

    return-wide v0
.end method


# virtual methods
.method public add(Lcom/google/android/play/playperf/measurements/memory/AndroidMemorySnapshot;)Lcom/google/android/play/playperf/measurements/memory/AndroidMemorySnapshot;
    .locals 22
    .param p1, "base"    # Lcom/google/android/play/playperf/measurements/memory/AndroidMemorySnapshot;

    .prologue
    .line 55
    new-instance v3, Lcom/google/android/play/playperf/measurements/memory/AndroidMemorySnapshot;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/playperf/measurements/memory/AndroidMemorySnapshot;->getOtherPrivateDirtyB()J

    move-result-wide v4

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/play/playperf/measurements/memory/AndroidMemorySnapshot;->getOtherPrivateDirtyB()J

    move-result-wide v6

    add-long/2addr v4, v6

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/playperf/measurements/memory/AndroidMemorySnapshot;->getOtherPssB()J

    move-result-wide v6

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/play/playperf/measurements/memory/AndroidMemorySnapshot;->getOtherPssB()J

    move-result-wide v8

    add-long/2addr v6, v8

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/playperf/measurements/memory/AndroidMemorySnapshot;->getJvmPrivateDirtyB()J

    move-result-wide v8

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/play/playperf/measurements/memory/AndroidMemorySnapshot;->getJvmPrivateDirtyB()J

    move-result-wide v10

    add-long/2addr v8, v10

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/playperf/measurements/memory/AndroidMemorySnapshot;->getJvmPssB()J

    move-result-wide v10

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/play/playperf/measurements/memory/AndroidMemorySnapshot;->getJvmPssB()J

    move-result-wide v12

    add-long/2addr v10, v12

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/playperf/measurements/memory/AndroidMemorySnapshot;->getJvmHeapUsedB()J

    move-result-wide v12

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/play/playperf/measurements/memory/AndroidMemorySnapshot;->getJvmHeapUsedB()J

    move-result-wide v14

    add-long/2addr v12, v14

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/playperf/measurements/memory/AndroidMemorySnapshot;->getJvmHeapTotalB()J

    move-result-wide v14

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/play/playperf/measurements/memory/AndroidMemorySnapshot;->getJvmHeapTotalB()J

    move-result-wide v16

    add-long v14, v14, v16

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/playperf/measurements/memory/AndroidMemorySnapshot;->getJvmMaxHeapSizeB()J

    move-result-wide v16

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/play/playperf/measurements/memory/AndroidMemorySnapshot;->getJvmMaxHeapSizeB()J

    move-result-wide v18

    add-long v16, v16, v18

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/play/playperf/measurements/memory/AndroidMemorySnapshot;->nativeHeapAllocatedB:J

    move-wide/from16 v18, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/google/android/play/playperf/measurements/memory/AndroidMemorySnapshot;->nativeHeapAllocatedB:J

    move-wide/from16 v20, v0

    add-long v18, v18, v20

    invoke-direct/range {v3 .. v19}, Lcom/google/android/play/playperf/measurements/memory/AndroidMemorySnapshot;-><init>(JJJJJJJJ)V

    return-object v3
.end method

.method public getJvmPrivateDirtyB()J
    .locals 2

    .prologue
    .line 110
    iget-wide v0, p0, Lcom/google/android/play/playperf/measurements/memory/AndroidMemorySnapshot;->jvmPrivateDirtyB:J

    return-wide v0
.end method

.method public getJvmPssB()J
    .locals 2

    .prologue
    .line 117
    iget-wide v0, p0, Lcom/google/android/play/playperf/measurements/memory/AndroidMemorySnapshot;->jvmPssB:J

    return-wide v0
.end method

.method public getOtherPrivateDirtyB()J
    .locals 2

    .prologue
    .line 96
    iget-wide v0, p0, Lcom/google/android/play/playperf/measurements/memory/AndroidMemorySnapshot;->otherPrivateDirtyB:J

    return-wide v0
.end method

.method public getOtherPssB()J
    .locals 2

    .prologue
    .line 103
    iget-wide v0, p0, Lcom/google/android/play/playperf/measurements/memory/AndroidMemorySnapshot;->otherPssB:J

    return-wide v0
.end method

.method public multiply(D)Lcom/google/android/play/playperf/measurements/memory/AndroidMemorySnapshot;
    .locals 21
    .param p1, "scalar"    # D

    .prologue
    .line 77
    new-instance v3, Lcom/google/android/play/playperf/measurements/memory/AndroidMemorySnapshot;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/playperf/measurements/memory/AndroidMemorySnapshot;->getOtherPrivateDirtyB()J

    move-result-wide v4

    long-to-double v4, v4

    mul-double v4, v4, p1

    double-to-long v4, v4

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/playperf/measurements/memory/AndroidMemorySnapshot;->getOtherPssB()J

    move-result-wide v6

    long-to-double v6, v6

    mul-double v6, v6, p1

    double-to-long v6, v6

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/playperf/measurements/memory/AndroidMemorySnapshot;->getJvmPrivateDirtyB()J

    move-result-wide v8

    long-to-double v8, v8

    mul-double v8, v8, p1

    double-to-long v8, v8

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/playperf/measurements/memory/AndroidMemorySnapshot;->getJvmPssB()J

    move-result-wide v10

    long-to-double v10, v10

    mul-double v10, v10, p1

    double-to-long v10, v10

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/playperf/measurements/memory/AndroidMemorySnapshot;->getJvmHeapUsedB()J

    move-result-wide v12

    long-to-double v12, v12

    mul-double v12, v12, p1

    double-to-long v12, v12

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/playperf/measurements/memory/AndroidMemorySnapshot;->getJvmHeapTotalB()J

    move-result-wide v14

    long-to-double v14, v14

    mul-double v14, v14, p1

    double-to-long v14, v14

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/playperf/measurements/memory/AndroidMemorySnapshot;->getJvmMaxHeapSizeB()J

    move-result-wide v16

    move-wide/from16 v0, v16

    long-to-double v0, v0

    move-wide/from16 v16, v0

    mul-double v16, v16, p1

    move-wide/from16 v0, v16

    double-to-long v0, v0

    move-wide/from16 v16, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/play/playperf/measurements/memory/AndroidMemorySnapshot;->nativeHeapAllocatedB:J

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    long-to-double v0, v0

    move-wide/from16 v18, v0

    mul-double v18, v18, p1

    move-wide/from16 v0, v18

    double-to-long v0, v0

    move-wide/from16 v18, v0

    invoke-direct/range {v3 .. v19}, Lcom/google/android/play/playperf/measurements/memory/AndroidMemorySnapshot;-><init>(JJJJJJJJ)V

    return-object v3
.end method

.method public subtract(Lcom/google/android/play/playperf/measurements/memory/AndroidMemorySnapshot;)Lcom/google/android/play/playperf/measurements/memory/AndroidMemorySnapshot;
    .locals 2
    .param p1, "base"    # Lcom/google/android/play/playperf/measurements/memory/AndroidMemorySnapshot;

    .prologue
    .line 70
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    invoke-virtual {p1, v0, v1}, Lcom/google/android/play/playperf/measurements/memory/AndroidMemorySnapshot;->multiply(D)Lcom/google/android/play/playperf/measurements/memory/AndroidMemorySnapshot;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/play/playperf/measurements/memory/AndroidMemorySnapshot;->add(Lcom/google/android/play/playperf/measurements/memory/AndroidMemorySnapshot;)Lcom/google/android/play/playperf/measurements/memory/AndroidMemorySnapshot;

    move-result-object v0

    return-object v0
.end method
