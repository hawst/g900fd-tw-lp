.class public Lcom/google/android/play/playperf/measurements/cpu/CpuUsageStatistics;
.super Ljava/lang/Object;
.source "CpuUsageStatistics.java"

# interfaces
.implements Lcom/google/android/play/playperf/measurements/Recordable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/play/playperf/measurements/cpu/CpuUsageStatistics;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mAvg:Lcom/google/android/play/playperf/data/DataPoint;

.field private final mMax:Lcom/google/android/play/playperf/data/DataPoint;

.field private final mMin:Lcom/google/android/play/playperf/data/DataPoint;

.field private final mSectionName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 121
    new-instance v0, Lcom/google/android/play/playperf/measurements/cpu/CpuUsageStatistics$1;

    invoke-direct {v0}, Lcom/google/android/play/playperf/measurements/cpu/CpuUsageStatistics$1;-><init>()V

    sput-object v0, Lcom/google/android/play/playperf/measurements/cpu/CpuUsageStatistics;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "parcel"    # Landroid/os/Parcel;

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/playperf/data/DataPoint;

    iput-object v0, p0, Lcom/google/android/play/playperf/measurements/cpu/CpuUsageStatistics;->mMax:Lcom/google/android/play/playperf/data/DataPoint;

    .line 45
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/playperf/data/DataPoint;

    iput-object v0, p0, Lcom/google/android/play/playperf/measurements/cpu/CpuUsageStatistics;->mMin:Lcom/google/android/play/playperf/data/DataPoint;

    .line 46
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/playperf/data/DataPoint;

    iput-object v0, p0, Lcom/google/android/play/playperf/measurements/cpu/CpuUsageStatistics;->mAvg:Lcom/google/android/play/playperf/data/DataPoint;

    .line 47
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/play/playperf/measurements/cpu/CpuUsageStatistics;->mSectionName:Ljava/lang/String;

    .line 48
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/google/android/play/playperf/measurements/cpu/CpuUsageStatistics$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/google/android/play/playperf/measurements/cpu/CpuUsageStatistics$1;

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/google/android/play/playperf/measurements/cpu/CpuUsageStatistics;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 107
    const/4 v0, 0x0

    return v0
.end method

.method public getSectionName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/play/playperf/measurements/cpu/CpuUsageStatistics;->mSectionName:Ljava/lang/String;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "parcel"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 115
    iget-object v0, p0, Lcom/google/android/play/playperf/measurements/cpu/CpuUsageStatistics;->mMax:Lcom/google/android/play/playperf/data/DataPoint;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 116
    iget-object v0, p0, Lcom/google/android/play/playperf/measurements/cpu/CpuUsageStatistics;->mMin:Lcom/google/android/play/playperf/data/DataPoint;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 117
    iget-object v0, p0, Lcom/google/android/play/playperf/measurements/cpu/CpuUsageStatistics;->mAvg:Lcom/google/android/play/playperf/data/DataPoint;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 118
    invoke-virtual {p0}, Lcom/google/android/play/playperf/measurements/cpu/CpuUsageStatistics;->getSectionName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 119
    return-void
.end method
