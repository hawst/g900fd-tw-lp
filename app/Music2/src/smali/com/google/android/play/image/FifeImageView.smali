.class public Lcom/google/android/play/image/FifeImageView;
.super Landroid/widget/ImageView;
.source "FifeImageView.java"

# interfaces
.implements Lcom/google/android/play/image/BitmapLoader$BitmapLoadedHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/play/image/FifeImageView$OnLoadedListener;
    }
.end annotation


# static fields
.field private static IS_HC_OR_ABOVE:Z

.field private static IS_ICS_OR_ABOVE:Z


# instance fields
.field mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

.field private mBitmapTransformation:Lcom/google/android/play/image/BitmapTransformation;

.field private mBlockLayout:Z

.field private mDefaultZoom:F

.field private mDesiredHeight:I

.field private mDesiredWidth:I

.field private mFadeInAnimation:Landroid/view/animation/Animation;

.field private mFadeInAnimationListener:Landroid/view/animation/Animation$AnimationListener;

.field private mFadeInAnimatorListener:Landroid/animation/Animator$AnimatorListener;

.field private mFadeInDuration:I

.field private final mFocusPoint:Landroid/graphics/PointF;

.field private mForegroundBoundsChanged:Z

.field private mForegroundDrawable:Landroid/graphics/drawable/Drawable;

.field private mHandler:Landroid/os/Handler;

.field private mHasDefaultZoom:Z

.field private mIsFrozen:Z

.field mIsLoaded:Z

.field private final mMatrix:Landroid/graphics/Matrix;

.field private mMayBlockLayout:Z

.field private mOnLoadedListener:Lcom/google/android/play/image/FifeImageView$OnLoadedListener;

.field private mRequestScaleFactor:F

.field private final mSelfBounds:Landroid/graphics/Rect;

.field private mSupportsFifeUrlOptions:Z

.field private mToFadeInAfterLoad:Z

.field private mUrl:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 43
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-lt v0, v3, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/google/android/play/image/FifeImageView;->IS_HC_OR_ABOVE:Z

    .line 45
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xe

    if-lt v0, v3, :cond_1

    :goto_1
    sput-boolean v1, Lcom/google/android/play/image/FifeImageView;->IS_ICS_OR_ABOVE:Z

    return-void

    :cond_0
    move v0, v2

    .line 43
    goto :goto_0

    :cond_1
    move v1, v2

    .line 45
    goto :goto_1
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 85
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/play/image/FifeImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 86
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 89
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/play/image/FifeImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 90
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/high16 v8, 0x3f000000    # 0.5f

    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 93
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 55
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    iput-object v4, p0, Lcom/google/android/play/image/FifeImageView;->mSelfBounds:Landroid/graphics/Rect;

    .line 56
    iput-boolean v6, p0, Lcom/google/android/play/image/FifeImageView;->mForegroundBoundsChanged:Z

    .line 64
    new-instance v4, Landroid/graphics/PointF;

    invoke-direct {v4, v8, v8}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v4, p0, Lcom/google/android/play/image/FifeImageView;->mFocusPoint:Landroid/graphics/PointF;

    .line 65
    new-instance v4, Landroid/graphics/Matrix;

    invoke-direct {v4}, Landroid/graphics/Matrix;-><init>()V

    iput-object v4, p0, Lcom/google/android/play/image/FifeImageView;->mMatrix:Landroid/graphics/Matrix;

    .line 95
    sget-object v4, Lcom/google/android/play/R$styleable;->FifeImageView:[I

    invoke-virtual {p1, p2, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v3

    .line 97
    .local v3, "viewAttrs":Landroid/content/res/TypedArray;
    sget v4, Lcom/google/android/play/R$styleable;->FifeImageView_fade_in_after_load:I

    invoke-virtual {v3, v4, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v4

    iput-boolean v4, p0, Lcom/google/android/play/image/FifeImageView;->mToFadeInAfterLoad:Z

    .line 99
    sget v4, Lcom/google/android/play/R$styleable;->FifeImageView_fixed_bounds:I

    invoke-virtual {v3, v4, v6}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v4

    iput-boolean v4, p0, Lcom/google/android/play/image/FifeImageView;->mMayBlockLayout:Z

    .line 101
    sget v4, Lcom/google/android/play/R$styleable;->FifeImageView_zoom:I

    invoke-virtual {v3, v4}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v4

    iput-boolean v4, p0, Lcom/google/android/play/image/FifeImageView;->mHasDefaultZoom:Z

    .line 102
    iget-boolean v4, p0, Lcom/google/android/play/image/FifeImageView;->mHasDefaultZoom:Z

    if-eqz v4, :cond_2

    .line 103
    sget v4, Lcom/google/android/play/R$styleable;->FifeImageView_zoom:I

    invoke-virtual {v3, v4, v5, v5, v7}, Landroid/content/res/TypedArray;->getFraction(IIIF)F

    move-result v4

    iput v4, p0, Lcom/google/android/play/image/FifeImageView;->mDefaultZoom:F

    .line 104
    sget-object v4, Landroid/widget/ImageView$ScaleType;->MATRIX:Landroid/widget/ImageView$ScaleType;

    invoke-super {p0, v4}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 109
    :goto_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 110
    .local v2, "res":Landroid/content/res/Resources;
    sget v4, Lcom/google/android/play/R$styleable;->FifeImageView_is_avatar:I

    invoke-virtual {v3, v4, v6}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 111
    invoke-static {v2}, Lcom/google/android/play/image/AvatarCropTransformation;->getFullAvatarCrop(Landroid/content/res/Resources;)Lcom/google/android/play/image/AvatarCropTransformation;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/play/image/FifeImageView;->mBitmapTransformation:Lcom/google/android/play/image/BitmapTransformation;

    .line 114
    :cond_0
    sget v4, Lcom/google/android/play/R$styleable;->FifeImageView_request_scale_factor:I

    invoke-virtual {v3, v4, v5, v5, v7}, Landroid/content/res/TypedArray;->getFraction(IIIF)F

    move-result v4

    iput v4, p0, Lcom/google/android/play/image/FifeImageView;->mRequestScaleFactor:F

    .line 117
    invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V

    .line 119
    new-array v4, v5, [I

    const v5, 0x1010109

    aput v5, v4, v6

    invoke-virtual {p1, p2, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 121
    .local v0, "foregroundAttrs":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v6}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 122
    .local v1, "foregroundDrawable":Landroid/graphics/drawable/Drawable;
    if-eqz v1, :cond_1

    .line 123
    invoke-virtual {p0, v1}, Lcom/google/android/play/image/FifeImageView;->setForeground(Landroid/graphics/drawable/Drawable;)V

    .line 125
    :cond_1
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 127
    const/high16 v4, 0x10e0000

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v4

    iput v4, p0, Lcom/google/android/play/image/FifeImageView;->mFadeInDuration:I

    .line 129
    new-instance v4, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v4, p0, Lcom/google/android/play/image/FifeImageView;->mHandler:Landroid/os/Handler;

    .line 130
    return-void

    .line 106
    .end local v0    # "foregroundAttrs":Landroid/content/res/TypedArray;
    .end local v1    # "foregroundDrawable":Landroid/graphics/drawable/Drawable;
    .end local v2    # "res":Landroid/content/res/Resources;
    :cond_2
    iput v7, p0, Lcom/google/android/play/image/FifeImageView;->mDefaultZoom:F

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/google/android/play/image/FifeImageView;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/play/image/FifeImageView;

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/google/android/play/image/FifeImageView;->invokeOnFadeInDone()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/play/image/FifeImageView;Landroid/graphics/Bitmap;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/play/image/FifeImageView;
    .param p1, "x1"    # Landroid/graphics/Bitmap;
    .param p2, "x2"    # Z

    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Lcom/google/android/play/image/FifeImageView;->loadFromCache(Landroid/graphics/Bitmap;Z)V

    return-void
.end method

.method private blockLayoutIfPossible()V
    .locals 1

    .prologue
    .line 641
    iget-boolean v0, p0, Lcom/google/android/play/image/FifeImageView;->mMayBlockLayout:Z

    if-eqz v0, :cond_0

    .line 642
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/image/FifeImageView;->mBlockLayout:Z

    .line 644
    :cond_0
    return-void
.end method

.method private computeDesiredWidthAndHeight()V
    .locals 9

    .prologue
    const/4 v5, 0x1

    const/4 v8, -0x1

    const/4 v6, 0x0

    .line 445
    iput v6, p0, Lcom/google/android/play/image/FifeImageView;->mDesiredWidth:I

    .line 446
    iput v6, p0, Lcom/google/android/play/image/FifeImageView;->mDesiredHeight:I

    .line 448
    invoke-virtual {p0}, Lcom/google/android/play/image/FifeImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 449
    .local v0, "layoutParams":Landroid/view/ViewGroup$LayoutParams;
    iget v7, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    if-ne v7, v8, :cond_3

    move v2, v5

    .line 450
    .local v2, "matchParentWidth":Z
    :goto_0
    iget v7, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-ne v7, v8, :cond_4

    move v1, v5

    .line 451
    .local v1, "matchParentHeight":Z
    :goto_1
    if-eqz v2, :cond_5

    if-eqz v1, :cond_5

    .line 452
    invoke-virtual {p0}, Lcom/google/android/play/image/FifeImageView;->getWidth()I

    move-result v5

    iput v5, p0, Lcom/google/android/play/image/FifeImageView;->mDesiredWidth:I

    .line 453
    invoke-virtual {p0}, Lcom/google/android/play/image/FifeImageView;->getHeight()I

    move-result v5

    iput v5, p0, Lcom/google/android/play/image/FifeImageView;->mDesiredHeight:I

    .line 473
    :cond_0
    :goto_2
    iget-object v5, p0, Lcom/google/android/play/image/FifeImageView;->mBitmapTransformation:Lcom/google/android/play/image/BitmapTransformation;

    if-eqz v5, :cond_9

    iget-object v5, p0, Lcom/google/android/play/image/FifeImageView;->mBitmapTransformation:Lcom/google/android/play/image/BitmapTransformation;

    iget v6, p0, Lcom/google/android/play/image/FifeImageView;->mDesiredWidth:I

    iget v7, p0, Lcom/google/android/play/image/FifeImageView;->mDesiredHeight:I

    invoke-interface {v5, v6, v7}, Lcom/google/android/play/image/BitmapTransformation;->getTransformationInset(II)I

    move-result v4

    .line 475
    .local v4, "transformationInset":I
    :goto_3
    iget v5, p0, Lcom/google/android/play/image/FifeImageView;->mDesiredWidth:I

    if-lez v5, :cond_1

    .line 476
    iget v5, p0, Lcom/google/android/play/image/FifeImageView;->mDesiredWidth:I

    sub-int/2addr v5, v4

    iput v5, p0, Lcom/google/android/play/image/FifeImageView;->mDesiredWidth:I

    .line 478
    :cond_1
    iget v5, p0, Lcom/google/android/play/image/FifeImageView;->mDesiredHeight:I

    if-lez v5, :cond_2

    .line 479
    iget v5, p0, Lcom/google/android/play/image/FifeImageView;->mDesiredHeight:I

    sub-int/2addr v5, v4

    iput v5, p0, Lcom/google/android/play/image/FifeImageView;->mDesiredHeight:I

    .line 483
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/play/image/FifeImageView;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/play/image/FifeUrlUtils;->getNetworkScaleFactor(Landroid/content/Context;)F

    move-result v3

    .line 485
    .local v3, "networkScaleFactor":F
    iget v5, p0, Lcom/google/android/play/image/FifeImageView;->mRequestScaleFactor:F

    mul-float/2addr v5, v3

    iget v6, p0, Lcom/google/android/play/image/FifeImageView;->mDesiredWidth:I

    int-to-float v6, v6

    mul-float/2addr v5, v6

    float-to-int v5, v5

    iput v5, p0, Lcom/google/android/play/image/FifeImageView;->mDesiredWidth:I

    .line 486
    iget v5, p0, Lcom/google/android/play/image/FifeImageView;->mRequestScaleFactor:F

    mul-float/2addr v5, v3

    iget v6, p0, Lcom/google/android/play/image/FifeImageView;->mDesiredHeight:I

    int-to-float v6, v6

    mul-float/2addr v5, v6

    float-to-int v5, v5

    iput v5, p0, Lcom/google/android/play/image/FifeImageView;->mDesiredHeight:I

    .line 487
    return-void

    .end local v1    # "matchParentHeight":Z
    .end local v2    # "matchParentWidth":Z
    .end local v3    # "networkScaleFactor":F
    .end local v4    # "transformationInset":I
    :cond_3
    move v2, v6

    .line 449
    goto :goto_0

    .restart local v2    # "matchParentWidth":Z
    :cond_4
    move v1, v6

    .line 450
    goto :goto_1

    .line 454
    .restart local v1    # "matchParentHeight":Z
    :cond_5
    if-eqz v2, :cond_6

    .line 455
    invoke-virtual {p0}, Lcom/google/android/play/image/FifeImageView;->getWidth()I

    move-result v5

    iput v5, p0, Lcom/google/android/play/image/FifeImageView;->mDesiredWidth:I

    goto :goto_2

    .line 456
    :cond_6
    iget v5, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    if-lez v5, :cond_7

    .line 457
    invoke-virtual {p0}, Lcom/google/android/play/image/FifeImageView;->getWidth()I

    move-result v5

    iput v5, p0, Lcom/google/android/play/image/FifeImageView;->mDesiredWidth:I

    .line 458
    iget v5, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-lez v5, :cond_0

    .line 459
    iget v5, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    iput v5, p0, Lcom/google/android/play/image/FifeImageView;->mDesiredHeight:I

    goto :goto_2

    .line 461
    :cond_7
    if-eqz v1, :cond_8

    .line 462
    invoke-virtual {p0}, Lcom/google/android/play/image/FifeImageView;->getHeight()I

    move-result v5

    iput v5, p0, Lcom/google/android/play/image/FifeImageView;->mDesiredHeight:I

    goto :goto_2

    .line 464
    :cond_8
    invoke-virtual {p0}, Lcom/google/android/play/image/FifeImageView;->getWidth()I

    move-result v5

    iput v5, p0, Lcom/google/android/play/image/FifeImageView;->mDesiredWidth:I

    .line 465
    invoke-virtual {p0}, Lcom/google/android/play/image/FifeImageView;->getHeight()I

    move-result v5

    iput v5, p0, Lcom/google/android/play/image/FifeImageView;->mDesiredHeight:I

    goto :goto_2

    :cond_9
    move v4, v6

    .line 473
    goto :goto_3
.end method

.method private drawForeground(Landroid/graphics/Canvas;)V
    .locals 5
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v4, 0x0

    .line 564
    iget-object v3, p0, Lcom/google/android/play/image/FifeImageView;->mForegroundDrawable:Landroid/graphics/drawable/Drawable;

    if-nez v3, :cond_0

    .line 581
    :goto_0
    return-void

    .line 568
    :cond_0
    iget-boolean v3, p0, Lcom/google/android/play/image/FifeImageView;->mForegroundBoundsChanged:Z

    if-eqz v3, :cond_1

    .line 569
    iput-boolean v4, p0, Lcom/google/android/play/image/FifeImageView;->mForegroundBoundsChanged:Z

    .line 570
    iget-object v1, p0, Lcom/google/android/play/image/FifeImageView;->mSelfBounds:Landroid/graphics/Rect;

    .line 572
    .local v1, "selfBounds":Landroid/graphics/Rect;
    invoke-virtual {p0}, Lcom/google/android/play/image/FifeImageView;->getWidth()I

    move-result v2

    .line 573
    .local v2, "w":I
    invoke-virtual {p0}, Lcom/google/android/play/image/FifeImageView;->getHeight()I

    move-result v0

    .line 575
    .local v0, "h":I
    invoke-virtual {v1, v4, v4, v2, v0}, Landroid/graphics/Rect;->set(IIII)V

    .line 577
    iget-object v3, p0, Lcom/google/android/play/image/FifeImageView;->mForegroundDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 580
    .end local v0    # "h":I
    .end local v1    # "selfBounds":Landroid/graphics/Rect;
    .end local v2    # "w":I
    :cond_1
    iget-object v3, p0, Lcom/google/android/play/image/FifeImageView;->mForegroundDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method private fadeInIcs()V
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 284
    iget-object v0, p0, Lcom/google/android/play/image/FifeImageView;->mFadeInAnimatorListener:Landroid/animation/Animator$AnimatorListener;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/play/image/FifeImageView;->mOnLoadedListener:Lcom/google/android/play/image/FifeImageView$OnLoadedListener;

    if-eqz v0, :cond_0

    .line 285
    new-instance v0, Lcom/google/android/play/image/FifeImageView$2;

    invoke-direct {v0, p0}, Lcom/google/android/play/image/FifeImageView$2;-><init>(Lcom/google/android/play/image/FifeImageView;)V

    iput-object v0, p0, Lcom/google/android/play/image/FifeImageView;->mFadeInAnimatorListener:Landroid/animation/Animator$AnimatorListener;

    .line 292
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/play/image/FifeImageView;->setAlpha(F)V

    .line 293
    invoke-virtual {p0}, Lcom/google/android/play/image/FifeImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget v1, p0, Lcom/google/android/play/image/FifeImageView;->mFadeInDuration:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/play/image/FifeImageView;->mFadeInAnimatorListener:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 295
    return-void
.end method

.method private fadeInPreIcs()V
    .locals 2

    .prologue
    .line 256
    iget-object v0, p0, Lcom/google/android/play/image/FifeImageView;->mFadeInAnimation:Landroid/view/animation/Animation;

    if-nez v0, :cond_0

    .line 257
    invoke-virtual {p0}, Lcom/google/android/play/image/FifeImageView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/google/android/play/R$anim;->play_fade_in:I

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/play/image/FifeImageView;->mFadeInAnimation:Landroid/view/animation/Animation;

    .line 260
    :cond_0
    iget-object v0, p0, Lcom/google/android/play/image/FifeImageView;->mFadeInAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/play/image/FifeImageView;->mOnLoadedListener:Lcom/google/android/play/image/FifeImageView$OnLoadedListener;

    if-eqz v0, :cond_1

    .line 261
    new-instance v0, Lcom/google/android/play/image/FifeImageView$1;

    invoke-direct {v0, p0}, Lcom/google/android/play/image/FifeImageView$1;-><init>(Lcom/google/android/play/image/FifeImageView;)V

    iput-object v0, p0, Lcom/google/android/play/image/FifeImageView;->mFadeInAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    .line 274
    :cond_1
    iget-object v0, p0, Lcom/google/android/play/image/FifeImageView;->mFadeInAnimation:Landroid/view/animation/Animation;

    iget-object v1, p0, Lcom/google/android/play/image/FifeImageView;->mFadeInAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 275
    iget-object v0, p0, Lcom/google/android/play/image/FifeImageView;->mFadeInAnimation:Landroid/view/animation/Animation;

    invoke-virtual {p0, v0}, Lcom/google/android/play/image/FifeImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 276
    return-void
.end method

.method private invokeOnFadeInDone()V
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lcom/google/android/play/image/FifeImageView;->mOnLoadedListener:Lcom/google/android/play/image/FifeImageView$OnLoadedListener;

    if-eqz v0, :cond_0

    .line 210
    iget-object v0, p0, Lcom/google/android/play/image/FifeImageView;->mOnLoadedListener:Lcom/google/android/play/image/FifeImageView$OnLoadedListener;

    invoke-interface {v0, p0}, Lcom/google/android/play/image/FifeImageView$OnLoadedListener;->onLoadedAndFadedIn(Lcom/google/android/play/image/FifeImageView;)V

    .line 212
    :cond_0
    return-void
.end method

.method private static isFinalBitmapLoaded(Landroid/graphics/Bitmap;)Z
    .locals 1
    .param p0, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 430
    if-eqz p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private loadFromCache(Landroid/graphics/Bitmap;Z)V
    .locals 1
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "isLoaded"    # Z

    .prologue
    .line 434
    iget-boolean v0, p0, Lcom/google/android/play/image/FifeImageView;->mIsFrozen:Z

    if-eqz v0, :cond_0

    .line 439
    :goto_0
    return-void

    .line 437
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/play/image/FifeImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 438
    invoke-virtual {p0, p2, p1}, Lcom/google/android/play/image/FifeImageView;->setLoaded(ZLandroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method private loadImageIfNecessary(Z)V
    .locals 12
    .param p1, "inLayoutPass"    # Z

    .prologue
    const/4 v11, 0x0

    .line 326
    iget-boolean v9, p0, Lcom/google/android/play/image/FifeImageView;->mIsFrozen:Z

    if-eqz v9, :cond_1

    .line 402
    :cond_0
    :goto_0
    return-void

    .line 331
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/play/image/FifeImageView;->getWidth()I

    move-result v8

    .line 332
    .local v8, "viewWidth":I
    invoke-virtual {p0}, Lcom/google/android/play/image/FifeImageView;->getHeight()I

    move-result v7

    .line 333
    .local v7, "viewHeight":I
    if-nez v8, :cond_2

    if-eqz v7, :cond_0

    .line 338
    :cond_2
    invoke-direct {p0}, Lcom/google/android/play/image/FifeImageView;->computeDesiredWidthAndHeight()V

    .line 343
    iget-object v9, p0, Lcom/google/android/play/image/FifeImageView;->mUrl:Ljava/lang/String;

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_3

    iget v9, p0, Lcom/google/android/play/image/FifeImageView;->mDesiredWidth:I

    if-gtz v9, :cond_4

    iget v9, p0, Lcom/google/android/play/image/FifeImageView;->mDesiredHeight:I

    if-gtz v9, :cond_4

    .line 345
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/play/image/FifeImageView;->getTag()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/play/image/BitmapLoader$BitmapContainer;

    .line 346
    .local v4, "oldContainer":Lcom/google/android/play/image/BitmapLoader$BitmapContainer;
    if-eqz v4, :cond_0

    .line 347
    invoke-virtual {v4}, Lcom/google/android/play/image/BitmapLoader$BitmapContainer;->cancelRequest()V

    .line 348
    invoke-virtual {p0, v11}, Lcom/google/android/play/image/FifeImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 354
    .end local v4    # "oldContainer":Lcom/google/android/play/image/BitmapLoader$BitmapContainer;
    :cond_4
    const/4 v6, 0x0

    .line 355
    .local v6, "requestWidth":I
    const/4 v5, 0x0

    .line 356
    .local v5, "requestHeight":I
    iget-boolean v9, p0, Lcom/google/android/play/image/FifeImageView;->mSupportsFifeUrlOptions:Z

    if-eqz v9, :cond_5

    .line 357
    iget v6, p0, Lcom/google/android/play/image/FifeImageView;->mDesiredWidth:I

    .line 358
    iget v5, p0, Lcom/google/android/play/image/FifeImageView;->mDesiredHeight:I

    .line 363
    :cond_5
    invoke-virtual {p0}, Lcom/google/android/play/image/FifeImageView;->getTag()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/play/image/BitmapLoader$BitmapContainer;

    .line 364
    .restart local v4    # "oldContainer":Lcom/google/android/play/image/BitmapLoader$BitmapContainer;
    if-eqz v4, :cond_7

    invoke-virtual {v4}, Lcom/google/android/play/image/BitmapLoader$BitmapContainer;->getRequestUrl()Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_7

    .line 365
    invoke-virtual {v4}, Lcom/google/android/play/image/BitmapLoader$BitmapContainer;->getRequestUrl()Ljava/lang/String;

    move-result-object v9

    iget-object v10, p0, Lcom/google/android/play/image/FifeImageView;->mUrl:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    invoke-virtual {v4}, Lcom/google/android/play/image/BitmapLoader$BitmapContainer;->getRequestWidth()I

    move-result v9

    if-ne v9, v6, :cond_6

    invoke-virtual {v4}, Lcom/google/android/play/image/BitmapLoader$BitmapContainer;->getRequestHeight()I

    move-result v9

    if-eq v9, v5, :cond_0

    .line 370
    :cond_6
    invoke-virtual {v4}, Lcom/google/android/play/image/BitmapLoader$BitmapContainer;->cancelRequest()V

    .line 375
    :cond_7
    iget-object v9, p0, Lcom/google/android/play/image/FifeImageView;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    iget-object v10, p0, Lcom/google/android/play/image/FifeImageView;->mUrl:Ljava/lang/String;

    invoke-virtual {v9, v10, v6, v5, p0}, Lcom/google/android/play/image/BitmapLoader;->get(Ljava/lang/String;IILcom/google/android/play/image/BitmapLoader$BitmapLoadedHandler;)Lcom/google/android/play/image/BitmapLoader$BitmapContainer;

    move-result-object v3

    .line 376
    .local v3, "newContainer":Lcom/google/android/play/image/BitmapLoader$BitmapContainer;
    invoke-virtual {p0, v3}, Lcom/google/android/play/image/FifeImageView;->setTag(Ljava/lang/Object;)V

    .line 377
    invoke-virtual {v3}, Lcom/google/android/play/image/BitmapLoader$BitmapContainer;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 378
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_b

    .line 379
    iget-object v9, p0, Lcom/google/android/play/image/FifeImageView;->mBitmapTransformation:Lcom/google/android/play/image/BitmapTransformation;

    if-eqz v9, :cond_8

    .line 380
    iget-object v9, p0, Lcom/google/android/play/image/FifeImageView;->mBitmapTransformation:Lcom/google/android/play/image/BitmapTransformation;

    invoke-virtual {p0}, Lcom/google/android/play/image/FifeImageView;->getWidth()I

    move-result v10

    invoke-virtual {p0}, Lcom/google/android/play/image/FifeImageView;->getHeight()I

    move-result v11

    invoke-interface {v9, v0, v10, v11}, Lcom/google/android/play/image/BitmapTransformation;->transform(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 383
    :cond_8
    move-object v1, v0

    .line 384
    .local v1, "finalBitmap":Landroid/graphics/Bitmap;
    invoke-static {v1}, Lcom/google/android/play/image/FifeImageView;->isFinalBitmapLoaded(Landroid/graphics/Bitmap;)Z

    move-result v2

    .line 389
    .local v2, "isLoaded":Z
    if-eqz p1, :cond_9

    iget-boolean v9, p0, Lcom/google/android/play/image/FifeImageView;->mMayBlockLayout:Z

    if-eqz v9, :cond_a

    .line 390
    :cond_9
    invoke-direct {p0, v0, v2}, Lcom/google/android/play/image/FifeImageView;->loadFromCache(Landroid/graphics/Bitmap;Z)V

    goto/16 :goto_0

    .line 392
    :cond_a
    iget-object v9, p0, Lcom/google/android/play/image/FifeImageView;->mHandler:Landroid/os/Handler;

    new-instance v10, Lcom/google/android/play/image/FifeImageView$3;

    invoke-direct {v10, p0, v1, v2}, Lcom/google/android/play/image/FifeImageView$3;-><init>(Lcom/google/android/play/image/FifeImageView;Landroid/graphics/Bitmap;Z)V

    invoke-virtual {v9, v10}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_0

    .line 400
    .end local v1    # "finalBitmap":Landroid/graphics/Bitmap;
    .end local v2    # "isLoaded":Z
    :cond_b
    invoke-virtual {p0, v11}, Lcom/google/android/play/image/FifeImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0
.end method

.method private updateMatrix()V
    .locals 26

    .prologue
    .line 647
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/image/FifeImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v11

    .line 648
    .local v11, "d":Landroid/graphics/drawable/Drawable;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/image/FifeImageView;->getWidth()I

    move-result v21

    move/from16 v0, v21

    int-to-float v14, v0

    .line 649
    .local v14, "dstWidth":F
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/image/FifeImageView;->getHeight()I

    move-result v21

    move/from16 v0, v21

    int-to-float v12, v0

    .line 650
    .local v12, "dstHeight":F
    if-eqz v11, :cond_0

    const/16 v21, 0x0

    cmpl-float v21, v14, v21

    if-eqz v21, :cond_0

    const/16 v21, 0x0

    cmpl-float v21, v12, v21

    if-nez v21, :cond_1

    .line 688
    :cond_0
    :goto_0
    return-void

    .line 654
    :cond_1
    invoke-virtual {v11}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v21

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v20, v0

    .line 655
    .local v20, "srcWidth":F
    invoke-virtual {v11}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v21

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v18, v0

    .line 656
    .local v18, "srcHeight":F
    const/16 v21, 0x0

    cmpg-float v21, v20, v21

    if-lez v21, :cond_2

    const/16 v21, 0x0

    cmpg-float v21, v18, v21

    if-gtz v21, :cond_3

    .line 659
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/image/FifeImageView;->mMatrix:Landroid/graphics/Matrix;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/graphics/Matrix;->reset()V

    .line 687
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/image/FifeImageView;->mMatrix:Landroid/graphics/Matrix;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-super {v0, v1}, Landroid/widget/ImageView;->setImageMatrix(Landroid/graphics/Matrix;)V

    goto :goto_0

    .line 663
    :cond_3
    div-float v21, v14, v20

    div-float v22, v12, v18

    invoke-static/range {v21 .. v22}, Ljava/lang/Math;->max(FF)F

    move-result v17

    .line 668
    .local v17, "scale":F
    div-float v21, v14, v17

    sub-float v21, v20, v21

    const/16 v22, 0x0

    invoke-static/range {v21 .. v22}, Ljava/lang/Math;->max(FF)F

    move-result v9

    .line 669
    .local v9, "cropX":F
    div-float v21, v12, v17

    sub-float v21, v18, v21

    const/16 v22, 0x0

    invoke-static/range {v21 .. v22}, Ljava/lang/Math;->max(FF)F

    move-result v10

    .line 672
    .local v10, "cropY":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/image/FifeImageView;->mFocusPoint:Landroid/graphics/PointF;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v21, v0

    mul-float v6, v9, v21

    .line 673
    .local v6, "cropL":F
    sub-float v7, v9, v6

    .line 674
    .local v7, "cropR":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/image/FifeImageView;->mFocusPoint:Landroid/graphics/PointF;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v21, v0

    mul-float v8, v10, v21

    .line 675
    .local v8, "cropT":F
    sub-float v5, v10, v8

    .line 678
    .local v5, "cropB":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/play/image/FifeImageView;->mDefaultZoom:F

    move/from16 v21, v0

    const/high16 v22, 0x3f800000    # 1.0f

    sub-float v21, v21, v22

    mul-float v16, v20, v21

    .line 679
    .local v16, "extraSrcWidth":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/play/image/FifeImageView;->mDefaultZoom:F

    move/from16 v21, v0

    const/high16 v22, 0x3f800000    # 1.0f

    sub-float v21, v21, v22

    mul-float v15, v18, v21

    .line 680
    .local v15, "extraSrcHeight":F
    new-instance v19, Landroid/graphics/RectF;

    const/high16 v21, 0x40000000    # 2.0f

    div-float v21, v16, v21

    add-float v21, v21, v6

    const/high16 v22, 0x40000000    # 2.0f

    div-float v22, v15, v22

    add-float v22, v22, v8

    sub-float v23, v20, v7

    const/high16 v24, 0x40000000    # 2.0f

    div-float v24, v16, v24

    sub-float v23, v23, v24

    sub-float v24, v18, v5

    const/high16 v25, 0x40000000    # 2.0f

    div-float v25, v15, v25

    sub-float v24, v24, v25

    move-object/from16 v0, v19

    move/from16 v1, v21

    move/from16 v2, v22

    move/from16 v3, v23

    move/from16 v4, v24

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 683
    .local v19, "srcRect":Landroid/graphics/RectF;
    new-instance v13, Landroid/graphics/RectF;

    const/16 v21, 0x0

    const/16 v22, 0x0

    move/from16 v0, v21

    move/from16 v1, v22

    invoke-direct {v13, v0, v1, v14, v12}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 685
    .local v13, "dstRect":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/image/FifeImageView;->mMatrix:Landroid/graphics/Matrix;

    move-object/from16 v21, v0

    sget-object v22, Landroid/graphics/Matrix$ScaleToFit;->FILL:Landroid/graphics/Matrix$ScaleToFit;

    move-object/from16 v0, v21

    move-object/from16 v1, v19

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v13, v2}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    goto/16 :goto_1
.end method


# virtual methods
.method public clearCachedState()V
    .locals 1

    .prologue
    .line 317
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/play/image/FifeImageView;->setTag(Ljava/lang/Object;)V

    .line 318
    return-void
.end method

.method public drawableHotspotChanged(FF)V
    .locals 1
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 524
    invoke-super {p0, p1, p2}, Landroid/widget/ImageView;->drawableHotspotChanged(FF)V

    .line 526
    iget-object v0, p0, Lcom/google/android/play/image/FifeImageView;->mForegroundDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 527
    iget-object v0, p0, Lcom/google/android/play/image/FifeImageView;->mForegroundDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/drawable/Drawable;->setHotspot(FF)V

    .line 529
    :cond_0
    return-void
.end method

.method protected drawableStateChanged()V
    .locals 2

    .prologue
    .line 513
    invoke-super {p0}, Landroid/widget/ImageView;->drawableStateChanged()V

    .line 514
    iget-object v0, p0, Lcom/google/android/play/image/FifeImageView;->mForegroundDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/play/image/FifeImageView;->mForegroundDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 515
    iget-object v0, p0, Lcom/google/android/play/image/FifeImageView;->mForegroundDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/google/android/play/image/FifeImageView;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 517
    :cond_0
    iget-object v0, p0, Lcom/google/android/play/image/FifeImageView;->mBitmapTransformation:Lcom/google/android/play/image/BitmapTransformation;

    if-eqz v0, :cond_1

    .line 518
    invoke-virtual {p0}, Lcom/google/android/play/image/FifeImageView;->invalidate()V

    .line 520
    :cond_1
    return-void
.end method

.method public jumpDrawablesToCurrentState()V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 505
    invoke-super {p0}, Landroid/widget/ImageView;->jumpDrawablesToCurrentState()V

    .line 506
    sget-boolean v0, Lcom/google/android/play/image/FifeImageView;->IS_HC_OR_ABOVE:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/play/image/FifeImageView;->mForegroundDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 507
    iget-object v0, p0, Lcom/google/android/play/image/FifeImageView;->mForegroundDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->jumpToCurrentState()V

    .line 509
    :cond_0
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 543
    invoke-super {p0, p1}, Landroid/widget/ImageView;->onDraw(Landroid/graphics/Canvas;)V

    .line 545
    invoke-virtual {p0}, Lcom/google/android/play/image/FifeImageView;->getWidth()I

    move-result v2

    .line 546
    .local v2, "width":I
    invoke-virtual {p0}, Lcom/google/android/play/image/FifeImageView;->getHeight()I

    move-result v1

    .line 548
    .local v1, "height":I
    iget-object v3, p0, Lcom/google/android/play/image/FifeImageView;->mBitmapTransformation:Lcom/google/android/play/image/BitmapTransformation;

    if-nez v3, :cond_1

    .line 549
    invoke-direct {p0, p1}, Lcom/google/android/play/image/FifeImageView;->drawForeground(Landroid/graphics/Canvas;)V

    .line 561
    :cond_0
    :goto_0
    return-void

    .line 553
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/play/image/FifeImageView;->isPressed()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {p0}, Lcom/google/android/play/image/FifeImageView;->isDuplicateParentStateEnabled()Z

    move-result v3

    if-nez v3, :cond_2

    invoke-virtual {p0}, Lcom/google/android/play/image/FifeImageView;->isClickable()Z

    move-result v3

    if-eqz v3, :cond_4

    :cond_2
    const/4 v0, 0x1

    .line 554
    .local v0, "drawPressed":Z
    :goto_1
    if-eqz v0, :cond_3

    .line 555
    iget-object v3, p0, Lcom/google/android/play/image/FifeImageView;->mBitmapTransformation:Lcom/google/android/play/image/BitmapTransformation;

    invoke-interface {v3, p1, v2, v1}, Lcom/google/android/play/image/BitmapTransformation;->drawPressedOverlay(Landroid/graphics/Canvas;II)V

    .line 558
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/play/image/FifeImageView;->isFocused()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 559
    iget-object v3, p0, Lcom/google/android/play/image/FifeImageView;->mBitmapTransformation:Lcom/google/android/play/image/BitmapTransformation;

    invoke-interface {v3, p1, v2, v1}, Lcom/google/android/play/image/BitmapTransformation;->drawFocusedOverlay(Landroid/graphics/Canvas;II)V

    goto :goto_0

    .line 553
    .end local v0    # "drawPressed":Z
    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method

.method protected onLayout(ZIIII)V
    .locals 1
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    const/4 v0, 0x1

    .line 491
    invoke-super/range {p0 .. p5}, Landroid/widget/ImageView;->onLayout(ZIIII)V

    .line 492
    invoke-direct {p0, v0}, Lcom/google/android/play/image/FifeImageView;->loadImageIfNecessary(Z)V

    .line 494
    iput-boolean v0, p0, Lcom/google/android/play/image/FifeImageView;->mForegroundBoundsChanged:Z

    .line 495
    return-void
.end method

.method public onResponse(Lcom/google/android/play/image/BitmapLoader$BitmapContainer;)V
    .locals 7
    .param p1, "result"    # Lcom/google/android/play/image/BitmapLoader$BitmapContainer;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 216
    iget-boolean v4, p0, Lcom/google/android/play/image/FifeImageView;->mIsFrozen:Z

    if-eqz v4, :cond_0

    .line 249
    :goto_0
    return-void

    .line 220
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/play/image/BitmapLoader$BitmapContainer;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 222
    .local v1, "response":Landroid/graphics/Bitmap;
    if-nez v1, :cond_1

    .line 223
    const/4 v2, 0x0

    invoke-virtual {p0, v3, v2}, Lcom/google/android/play/image/FifeImageView;->setLoaded(ZLandroid/graphics/Bitmap;)V

    goto :goto_0

    .line 227
    :cond_1
    iget-object v4, p0, Lcom/google/android/play/image/FifeImageView;->mBitmapTransformation:Lcom/google/android/play/image/BitmapTransformation;

    if-eqz v4, :cond_2

    .line 228
    iget-object v4, p0, Lcom/google/android/play/image/FifeImageView;->mBitmapTransformation:Lcom/google/android/play/image/BitmapTransformation;

    invoke-virtual {p0}, Lcom/google/android/play/image/FifeImageView;->getWidth()I

    move-result v5

    invoke-virtual {p0}, Lcom/google/android/play/image/FifeImageView;->getHeight()I

    move-result v6

    invoke-interface {v4, v1, v5, v6}, Lcom/google/android/play/image/BitmapTransformation;->transform(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 232
    :cond_2
    iget-boolean v4, p0, Lcom/google/android/play/image/FifeImageView;->mIsLoaded:Z

    if-nez v4, :cond_3

    move v0, v2

    .line 234
    .local v0, "fadeIn":Z
    :goto_1
    invoke-virtual {p0, v1}, Lcom/google/android/play/image/FifeImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 235
    if-eqz v1, :cond_4

    :goto_2
    invoke-virtual {p0, v2, v1}, Lcom/google/android/play/image/FifeImageView;->setLoaded(ZLandroid/graphics/Bitmap;)V

    .line 237
    if-eqz v0, :cond_6

    iget-boolean v2, p0, Lcom/google/android/play/image/FifeImageView;->mToFadeInAfterLoad:Z

    if-eqz v2, :cond_6

    .line 238
    sget-boolean v2, Lcom/google/android/play/image/FifeImageView;->IS_ICS_OR_ABOVE:Z

    if-eqz v2, :cond_5

    .line 239
    invoke-direct {p0}, Lcom/google/android/play/image/FifeImageView;->fadeInIcs()V

    goto :goto_0

    .end local v0    # "fadeIn":Z
    :cond_3
    move v0, v3

    .line 232
    goto :goto_1

    .restart local v0    # "fadeIn":Z
    :cond_4
    move v2, v3

    .line 235
    goto :goto_2

    .line 241
    :cond_5
    invoke-direct {p0}, Lcom/google/android/play/image/FifeImageView;->fadeInPreIcs()V

    goto :goto_0

    .line 247
    :cond_6
    invoke-direct {p0}, Lcom/google/android/play/image/FifeImageView;->invokeOnFadeInDone()V

    goto :goto_0
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 42
    check-cast p1, Lcom/google/android/play/image/BitmapLoader$BitmapContainer;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/play/image/FifeImageView;->onResponse(Lcom/google/android/play/image/BitmapLoader$BitmapContainer;)V

    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 1
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    .line 605
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ImageView;->onSizeChanged(IIII)V

    .line 606
    iget-boolean v0, p0, Lcom/google/android/play/image/FifeImageView;->mHasDefaultZoom:Z

    if-eqz v0, :cond_0

    .line 607
    invoke-direct {p0}, Lcom/google/android/play/image/FifeImageView;->updateMatrix()V

    .line 610
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/image/FifeImageView;->mForegroundBoundsChanged:Z

    .line 611
    return-void
.end method

.method public requestLayout()V
    .locals 1

    .prologue
    .line 622
    iget-boolean v0, p0, Lcom/google/android/play/image/FifeImageView;->mBlockLayout:Z

    if-nez v0, :cond_0

    .line 623
    invoke-super {p0}, Landroid/widget/ImageView;->requestLayout()V

    .line 625
    :cond_0
    return-void
.end method

.method public setBitmapTransformation(Lcom/google/android/play/image/BitmapTransformation;)V
    .locals 1
    .param p1, "bitmapTransformation"    # Lcom/google/android/play/image/BitmapTransformation;

    .prologue
    .line 185
    iput-object p1, p0, Lcom/google/android/play/image/FifeImageView;->mBitmapTransformation:Lcom/google/android/play/image/BitmapTransformation;

    .line 186
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/play/image/FifeImageView;->setWillNotDraw(Z)V

    .line 187
    return-void
.end method

.method public setDefaultZoom(F)V
    .locals 1
    .param p1, "defaultZoom"    # F

    .prologue
    .line 628
    iget-boolean v0, p0, Lcom/google/android/play/image/FifeImageView;->mHasDefaultZoom:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/play/image/FifeImageView;->mDefaultZoom:F

    cmpl-float v0, v0, p1

    if-eqz v0, :cond_1

    .line 629
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/image/FifeImageView;->mHasDefaultZoom:Z

    .line 630
    iput p1, p0, Lcom/google/android/play/image/FifeImageView;->mDefaultZoom:F

    .line 631
    sget-object v0, Landroid/widget/ImageView$ScaleType;->MATRIX:Landroid/widget/ImageView$ScaleType;

    invoke-super {p0, v0}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 632
    invoke-direct {p0}, Lcom/google/android/play/image/FifeImageView;->updateMatrix()V

    .line 634
    :cond_1
    return-void
.end method

.method public setFocusPoint(FF)V
    .locals 1
    .param p1, "focusX"    # F
    .param p2, "focusY"    # F

    .prologue
    .line 637
    iget-object v0, p0, Lcom/google/android/play/image/FifeImageView;->mFocusPoint:Landroid/graphics/PointF;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/PointF;->set(FF)V

    .line 638
    return-void
.end method

.method public setForeground(Landroid/graphics/drawable/Drawable;)V
    .locals 2
    .param p1, "d"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/android/play/image/FifeImageView;->mForegroundDrawable:Landroid/graphics/drawable/Drawable;

    if-eq v0, p1, :cond_2

    .line 134
    iget-object v0, p0, Lcom/google/android/play/image/FifeImageView;->mForegroundDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 135
    iget-object v0, p0, Lcom/google/android/play/image/FifeImageView;->mForegroundDrawable:Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 136
    iget-object v0, p0, Lcom/google/android/play/image/FifeImageView;->mForegroundDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v0}, Lcom/google/android/play/image/FifeImageView;->unscheduleDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 139
    :cond_0
    iput-object p1, p0, Lcom/google/android/play/image/FifeImageView;->mForegroundDrawable:Landroid/graphics/drawable/Drawable;

    .line 141
    if-eqz p1, :cond_3

    .line 142
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/play/image/FifeImageView;->setWillNotDraw(Z)V

    .line 143
    invoke-virtual {p1, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 144
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 145
    invoke-virtual {p0}, Lcom/google/android/play/image/FifeImageView;->getDrawableState()[I

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 150
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/play/image/FifeImageView;->requestLayout()V

    .line 151
    invoke-virtual {p0}, Lcom/google/android/play/image/FifeImageView;->invalidate()V

    .line 153
    :cond_2
    return-void

    .line 148
    :cond_3
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/play/image/FifeImageView;->setWillNotDraw(Z)V

    goto :goto_0
.end method

.method public setHasFixedBounds(Z)V
    .locals 0
    .param p1, "hasFixedBounds"    # Z

    .prologue
    .line 190
    iput-boolean p1, p0, Lcom/google/android/play/image/FifeImageView;->mMayBlockLayout:Z

    .line 191
    return-void
.end method

.method public setImage(Ljava/lang/String;ZLcom/google/android/play/image/BitmapLoader;)V
    .locals 3
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "supportsFifeUrlOptions"    # Z
    .param p3, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;

    .prologue
    const/4 v2, 0x0

    .line 156
    iget-object v1, p0, Lcom/google/android/play/image/FifeImageView;->mUrl:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 158
    .local v0, "isSameData":Z
    if-nez v0, :cond_0

    .line 159
    iput-object p1, p0, Lcom/google/android/play/image/FifeImageView;->mUrl:Ljava/lang/String;

    .line 160
    iput-boolean p2, p0, Lcom/google/android/play/image/FifeImageView;->mSupportsFifeUrlOptions:Z

    .line 161
    const/4 v1, 0x0

    invoke-virtual {p0, v2, v1}, Lcom/google/android/play/image/FifeImageView;->setLoaded(ZLandroid/graphics/Bitmap;)V

    .line 163
    :cond_0
    iput-object p3, p0, Lcom/google/android/play/image/FifeImageView;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    .line 165
    invoke-direct {p0, v2}, Lcom/google/android/play/image/FifeImageView;->loadImageIfNecessary(Z)V

    .line 166
    return-void
.end method

.method public setImageDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1, "drawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 585
    invoke-direct {p0}, Lcom/google/android/play/image/FifeImageView;->blockLayoutIfPossible()V

    .line 586
    invoke-super {p0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 587
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/play/image/FifeImageView;->mBlockLayout:Z

    .line 588
    iget-boolean v0, p0, Lcom/google/android/play/image/FifeImageView;->mHasDefaultZoom:Z

    if-eqz v0, :cond_0

    .line 589
    invoke-direct {p0}, Lcom/google/android/play/image/FifeImageView;->updateMatrix()V

    .line 591
    :cond_0
    return-void
.end method

.method public setImageMatrix(Landroid/graphics/Matrix;)V
    .locals 2
    .param p1, "matrix"    # Landroid/graphics/Matrix;

    .prologue
    .line 307
    iget-boolean v0, p0, Lcom/google/android/play/image/FifeImageView;->mHasDefaultZoom:Z

    if-eqz v0, :cond_0

    .line 308
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Can\'t mix scale type and custom zoom"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 310
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/ImageView;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 311
    return-void
.end method

.method public setImageResource(I)V
    .locals 1
    .param p1, "resId"    # I

    .prologue
    .line 595
    invoke-direct {p0}, Lcom/google/android/play/image/FifeImageView;->blockLayoutIfPossible()V

    .line 596
    invoke-super {p0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 597
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/play/image/FifeImageView;->mBlockLayout:Z

    .line 598
    iget-boolean v0, p0, Lcom/google/android/play/image/FifeImageView;->mHasDefaultZoom:Z

    if-eqz v0, :cond_0

    .line 599
    invoke-direct {p0}, Lcom/google/android/play/image/FifeImageView;->updateMatrix()V

    .line 601
    :cond_0
    return-void
.end method

.method public setImageURI(Landroid/net/Uri;)V
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 615
    invoke-direct {p0}, Lcom/google/android/play/image/FifeImageView;->blockLayoutIfPossible()V

    .line 616
    invoke-super {p0, p1}, Landroid/widget/ImageView;->setImageURI(Landroid/net/Uri;)V

    .line 617
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/play/image/FifeImageView;->mBlockLayout:Z

    .line 618
    return-void
.end method

.method declared-synchronized setLoaded(ZLandroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "isLoaded"    # Z
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 198
    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lcom/google/android/play/image/FifeImageView;->mIsLoaded:Z

    .line 199
    iget-boolean v0, p0, Lcom/google/android/play/image/FifeImageView;->mIsLoaded:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/play/image/FifeImageView;->mOnLoadedListener:Lcom/google/android/play/image/FifeImageView$OnLoadedListener;

    if-eqz v0, :cond_0

    .line 200
    iget-object v0, p0, Lcom/google/android/play/image/FifeImageView;->mOnLoadedListener:Lcom/google/android/play/image/FifeImageView$OnLoadedListener;

    invoke-interface {v0, p0, p2}, Lcom/google/android/play/image/FifeImageView$OnLoadedListener;->onLoaded(Lcom/google/android/play/image/FifeImageView;Landroid/graphics/Bitmap;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 202
    :cond_0
    monitor-exit p0

    return-void

    .line 198
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setOnLoadedListener(Lcom/google/android/play/image/FifeImageView$OnLoadedListener;)V
    .locals 0
    .param p1, "onLoadedListener"    # Lcom/google/android/play/image/FifeImageView$OnLoadedListener;

    .prologue
    .line 181
    iput-object p1, p0, Lcom/google/android/play/image/FifeImageView;->mOnLoadedListener:Lcom/google/android/play/image/FifeImageView$OnLoadedListener;

    .line 182
    return-void
.end method

.method public setScaleType(Landroid/widget/ImageView$ScaleType;)V
    .locals 2
    .param p1, "scaleType"    # Landroid/widget/ImageView$ScaleType;

    .prologue
    .line 299
    iget-boolean v0, p0, Lcom/google/android/play/image/FifeImageView;->mHasDefaultZoom:Z

    if-eqz v0, :cond_0

    .line 300
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Can\'t mix scale type and custom zoom"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 302
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 303
    return-void
.end method

.method public setToFadeInAfterLoad(Z)V
    .locals 0
    .param p1, "toFadeInAfterLoad"    # Z

    .prologue
    .line 205
    iput-boolean p1, p0, Lcom/google/android/play/image/FifeImageView;->mToFadeInAfterLoad:Z

    .line 206
    return-void
.end method

.method public setVisibility(I)V
    .locals 3
    .param p1, "visibility"    # I

    .prologue
    const/4 v1, 0x0

    .line 533
    invoke-super {p0, p1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 535
    iget-object v2, p0, Lcom/google/android/play/image/FifeImageView;->mForegroundDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_0

    .line 536
    if-nez p1, :cond_1

    const/4 v0, 0x1

    .line 537
    .local v0, "isVisible":Z
    :goto_0
    iget-object v2, p0, Lcom/google/android/play/image/FifeImageView;->mForegroundDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, v0, v1}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    .line 539
    .end local v0    # "isVisible":Z
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 536
    goto :goto_0
.end method

.method protected verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1
    .param p1, "who"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 499
    invoke-super {p0, p1}, Landroid/widget/ImageView;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/play/image/FifeImageView;->mForegroundDrawable:Landroid/graphics/drawable/Drawable;

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
