.class public Lcom/google/android/play/imageview/AvatarTransform;
.super Ljava/lang/Object;
.source "AvatarTransform.java"

# interfaces
.implements Lcom/google/android/play/imageview/PlayImageViewTransform;


# static fields
.field private static sInstance:Lcom/google/android/play/imageview/AvatarTransform;


# instance fields
.field private mAvatarCropTransformation:Lcom/google/android/play/image/AvatarCropTransformation;


# direct methods
.method private constructor <init>(Landroid/content/res/Resources;)V
    .locals 1
    .param p1, "res"    # Landroid/content/res/Resources;

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    invoke-static {p1}, Lcom/google/android/play/image/AvatarCropTransformation;->getFullAvatarCrop(Landroid/content/res/Resources;)Lcom/google/android/play/image/AvatarCropTransformation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/play/imageview/AvatarTransform;->mAvatarCropTransformation:Lcom/google/android/play/image/AvatarCropTransformation;

    .line 38
    return-void
.end method

.method public static getInstance(Landroid/content/res/Resources;)Lcom/google/android/play/imageview/AvatarTransform;
    .locals 2
    .param p0, "res"    # Landroid/content/res/Resources;

    .prologue
    .line 26
    sget-object v0, Lcom/google/android/play/imageview/AvatarTransform;->sInstance:Lcom/google/android/play/imageview/AvatarTransform;

    if-nez v0, :cond_1

    .line 27
    const-class v1, Lcom/google/android/play/imageview/AvatarTransform;

    monitor-enter v1

    .line 28
    :try_start_0
    sget-object v0, Lcom/google/android/play/imageview/AvatarTransform;->sInstance:Lcom/google/android/play/imageview/AvatarTransform;

    if-nez v0, :cond_0

    .line 29
    new-instance v0, Lcom/google/android/play/imageview/AvatarTransform;

    invoke-direct {v0, p0}, Lcom/google/android/play/imageview/AvatarTransform;-><init>(Landroid/content/res/Resources;)V

    sput-object v0, Lcom/google/android/play/imageview/AvatarTransform;->sInstance:Lcom/google/android/play/imageview/AvatarTransform;

    .line 31
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 33
    :cond_1
    sget-object v0, Lcom/google/android/play/imageview/AvatarTransform;->sInstance:Lcom/google/android/play/imageview/AvatarTransform;

    return-object v0

    .line 31
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public drawFocusedOverlay(Landroid/graphics/Canvas;II)V
    .locals 1
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/play/imageview/AvatarTransform;->mAvatarCropTransformation:Lcom/google/android/play/image/AvatarCropTransformation;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/play/image/AvatarCropTransformation;->drawFocusedOverlay(Landroid/graphics/Canvas;II)V

    .line 67
    return-void
.end method

.method public drawPressedOverlay(Landroid/graphics/Canvas;II)V
    .locals 1
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/play/imageview/AvatarTransform;->mAvatarCropTransformation:Lcom/google/android/play/image/AvatarCropTransformation;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/play/image/AvatarCropTransformation;->drawPressedOverlay(Landroid/graphics/Canvas;II)V

    .line 72
    return-void
.end method

.method public getTransformationInset(II)I
    .locals 1
    .param p1, "viewWidth"    # I
    .param p2, "viewHeight"    # I

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/play/imageview/AvatarTransform;->mAvatarCropTransformation:Lcom/google/android/play/image/AvatarCropTransformation;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/play/image/AvatarCropTransformation;->getTransformationInset(II)I

    move-result v0

    return v0
.end method

.method public transform(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;
    .locals 2
    .param p1, "source"    # Landroid/graphics/Bitmap;
    .param p2, "viewWidth"    # I
    .param p3, "viewHeight"    # I

    .prologue
    .line 55
    invoke-static {p2, p3}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 56
    .local v0, "boundsMin":I
    iget-object v1, p0, Lcom/google/android/play/imageview/AvatarTransform;->mAvatarCropTransformation:Lcom/google/android/play/image/AvatarCropTransformation;

    invoke-virtual {v1, p1, v0, v0}, Lcom/google/android/play/image/AvatarCropTransformation;->transform(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v1

    return-object v1
.end method

.method public transform(Lcom/google/android/play/imageview/PlayManagedBitmap;II)Landroid/graphics/Bitmap;
    .locals 2
    .param p1, "bitmap"    # Lcom/google/android/play/imageview/PlayManagedBitmap;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    .line 42
    iget-object v1, p1, Lcom/google/android/play/imageview/PlayManagedBitmap;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v1, p2, p3}, Lcom/google/android/play/imageview/AvatarTransform;->transform(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 43
    .local v0, "result":Landroid/graphics/Bitmap;
    iget-object v1, p1, Lcom/google/android/play/imageview/PlayManagedBitmap;->relatedBitmaps:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 44
    return-object v0
.end method
