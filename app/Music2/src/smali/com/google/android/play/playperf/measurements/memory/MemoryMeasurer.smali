.class public Lcom/google/android/play/playperf/measurements/memory/MemoryMeasurer;
.super Ljava/lang/Object;
.source "MemoryMeasurer.java"

# interfaces
.implements Lcom/google/android/play/playperf/measurements/Measurer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/play/playperf/measurements/Measurer",
        "<",
        "Lcom/google/android/play/playperf/measurements/memory/MemoryDelta;",
        ">;"
    }
.end annotation


# instance fields
.field private mMemoryStateEnd:Lcom/google/android/play/playperf/measurements/memory/AndroidMemorySnapshot;

.field private mMemoryStateStart:Lcom/google/android/play/playperf/measurements/memory/AndroidMemorySnapshot;

.field private final mSectionName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "sectionName"    # Ljava/lang/String;

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/google/android/play/playperf/measurements/memory/MemoryMeasurer;->mSectionName:Ljava/lang/String;

    .line 18
    return-void
.end method


# virtual methods
.method public bridge synthetic getResult()Lcom/google/android/play/playperf/measurements/Recordable;
    .locals 1

    .prologue
    .line 8
    invoke-virtual {p0}, Lcom/google/android/play/playperf/measurements/memory/MemoryMeasurer;->getResult()Lcom/google/android/play/playperf/measurements/memory/MemoryDelta;

    move-result-object v0

    return-object v0
.end method

.method public getResult()Lcom/google/android/play/playperf/measurements/memory/MemoryDelta;
    .locals 7

    .prologue
    .line 49
    new-instance v1, Lcom/google/android/play/playperf/measurements/memory/MemoryDelta;

    iget-object v2, p0, Lcom/google/android/play/playperf/measurements/memory/MemoryMeasurer;->mMemoryStateStart:Lcom/google/android/play/playperf/measurements/memory/AndroidMemorySnapshot;

    iget-object v3, p0, Lcom/google/android/play/playperf/measurements/memory/MemoryMeasurer;->mMemoryStateEnd:Lcom/google/android/play/playperf/measurements/memory/AndroidMemorySnapshot;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-object v6, p0, Lcom/google/android/play/playperf/measurements/memory/MemoryMeasurer;->mSectionName:Ljava/lang/String;

    invoke-direct/range {v1 .. v6}, Lcom/google/android/play/playperf/measurements/memory/MemoryDelta;-><init>(Lcom/google/android/play/playperf/measurements/memory/AndroidMemorySnapshot;Lcom/google/android/play/playperf/measurements/memory/AndroidMemorySnapshot;JLjava/lang/String;)V

    return-object v1
.end method

.method public start()V
    .locals 2

    .prologue
    .line 25
    iget-object v0, p0, Lcom/google/android/play/playperf/measurements/memory/MemoryMeasurer;->mMemoryStateStart:Lcom/google/android/play/playperf/measurements/memory/AndroidMemorySnapshot;

    if-eqz v0, :cond_0

    .line 26
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "start() has already been called"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 28
    :cond_0
    new-instance v0, Lcom/google/android/play/playperf/measurements/memory/AndroidMemorySnapshot;

    invoke-direct {v0}, Lcom/google/android/play/playperf/measurements/memory/AndroidMemorySnapshot;-><init>()V

    iput-object v0, p0, Lcom/google/android/play/playperf/measurements/memory/MemoryMeasurer;->mMemoryStateStart:Lcom/google/android/play/playperf/measurements/memory/AndroidMemorySnapshot;

    .line 29
    return-void
.end method

.method public stop()V
    .locals 2

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/play/playperf/measurements/memory/MemoryMeasurer;->mMemoryStateStart:Lcom/google/android/play/playperf/measurements/memory/AndroidMemorySnapshot;

    if-nez v0, :cond_0

    .line 37
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "start() has not been called"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 38
    :cond_0
    iget-object v0, p0, Lcom/google/android/play/playperf/measurements/memory/MemoryMeasurer;->mMemoryStateEnd:Lcom/google/android/play/playperf/measurements/memory/AndroidMemorySnapshot;

    if-eqz v0, :cond_1

    .line 39
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "stop() has already been called"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 41
    :cond_1
    new-instance v0, Lcom/google/android/play/playperf/measurements/memory/AndroidMemorySnapshot;

    invoke-direct {v0}, Lcom/google/android/play/playperf/measurements/memory/AndroidMemorySnapshot;-><init>()V

    iput-object v0, p0, Lcom/google/android/play/playperf/measurements/memory/MemoryMeasurer;->mMemoryStateEnd:Lcom/google/android/play/playperf/measurements/memory/AndroidMemorySnapshot;

    .line 42
    return-void
.end method
