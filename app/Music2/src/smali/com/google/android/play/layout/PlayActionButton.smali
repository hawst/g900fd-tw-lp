.class public Lcom/google/android/play/layout/PlayActionButton;
.super Landroid/widget/Button;
.source "PlayActionButton.java"


# instance fields
.field private mActionBottomPadding:I

.field private mActionStyle:I

.field private mActionTopPadding:I

.field private mActionXPadding:I

.field private mDrawAsLabel:Z

.field private mOriginalBackendId:I

.field private mOriginalText:Ljava/lang/String;

.field private mPriority:I

.field private mUseAllCapsInLabelMode:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 55
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/play/layout/PlayActionButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 56
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v3, 0x0

    .line 59
    invoke-direct {p0, p1, p2}, Landroid/widget/Button;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 61
    sget-object v1, Lcom/google/android/play/R$styleable;->PlayActionButton:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 63
    .local v0, "viewAttrs":Landroid/content/res/TypedArray;
    sget v1, Lcom/google/android/play/R$styleable;->PlayActionButton_draw_as_label:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/play/layout/PlayActionButton;->mDrawAsLabel:Z

    .line 65
    sget v1, Lcom/google/android/play/R$styleable;->PlayActionButton_use_all_caps_in_label_mode:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/play/layout/PlayActionButton;->mUseAllCapsInLabelMode:Z

    .line 67
    sget v1, Lcom/google/android/play/R$styleable;->PlayActionButton_action_style:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/play/layout/PlayActionButton;->mActionStyle:I

    .line 69
    sget v1, Lcom/google/android/play/R$styleable;->PlayActionButton_action_xpadding:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/play/layout/PlayActionButton;->mActionXPadding:I

    .line 71
    sget v1, Lcom/google/android/play/R$styleable;->PlayActionButton_action_top_padding:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/play/layout/PlayActionButton;->mActionTopPadding:I

    .line 73
    sget v1, Lcom/google/android/play/R$styleable;->PlayActionButton_action_bottom_padding:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/play/layout/PlayActionButton;->mActionBottomPadding:I

    .line 75
    sget v1, Lcom/google/android/play/R$styleable;->PlayActionButton_local_priority:I

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/play/layout/PlayActionButton;->mPriority:I

    .line 76
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 77
    return-void
.end method

.method private syncAppearance()V
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 132
    iget-object v5, p0, Lcom/google/android/play/layout/PlayActionButton;->mOriginalText:Ljava/lang/String;

    if-eqz v5, :cond_0

    iget-boolean v5, p0, Lcom/google/android/play/layout/PlayActionButton;->mDrawAsLabel:Z

    if-eqz v5, :cond_2

    iget-boolean v5, p0, Lcom/google/android/play/layout/PlayActionButton;->mUseAllCapsInLabelMode:Z

    if-nez v5, :cond_2

    :cond_0
    iget-object v5, p0, Lcom/google/android/play/layout/PlayActionButton;->mOriginalText:Ljava/lang/String;

    :goto_0
    invoke-virtual {p0, v5}, Lcom/google/android/play/layout/PlayActionButton;->setText(Ljava/lang/CharSequence;)V

    .line 135
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayActionButton;->getPaddingTop()I

    move-result v3

    .line 136
    .local v3, "paddingTop":I
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayActionButton;->getPaddingBottom()I

    move-result v2

    .line 139
    .local v2, "paddingBottom":I
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayActionButton;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 140
    .local v1, "context":Landroid/content/Context;
    iget-boolean v5, p0, Lcom/google/android/play/layout/PlayActionButton;->mDrawAsLabel:Z

    if-eqz v5, :cond_4

    .line 141
    invoke-virtual {p0, v6}, Lcom/google/android/play/layout/PlayActionButton;->setBackgroundResource(I)V

    .line 142
    iget v5, p0, Lcom/google/android/play/layout/PlayActionButton;->mActionStyle:I

    const/4 v7, 0x2

    if-ne v5, v7, :cond_3

    move v0, v6

    .line 144
    .local v0, "backendForColor":I
    :goto_1
    invoke-static {v1, v0}, Lcom/google/android/play/utils/PlayCorpusUtils;->getPrimaryTextColor(Landroid/content/Context;I)Landroid/content/res/ColorStateList;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/google/android/play/layout/PlayActionButton;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 166
    .end local v0    # "backendForColor":I
    :goto_2
    iget-boolean v5, p0, Lcom/google/android/play/layout/PlayActionButton;->mDrawAsLabel:Z

    if-eqz v5, :cond_5

    move v4, v6

    .line 167
    .local v4, "xPadding":I
    :goto_3
    iget-boolean v5, p0, Lcom/google/android/play/layout/PlayActionButton;->mDrawAsLabel:Z

    if-nez v5, :cond_1

    iget v5, p0, Lcom/google/android/play/layout/PlayActionButton;->mActionTopPadding:I

    if-lez v5, :cond_1

    iget v5, p0, Lcom/google/android/play/layout/PlayActionButton;->mActionBottomPadding:I

    if-lez v5, :cond_1

    .line 168
    iget v3, p0, Lcom/google/android/play/layout/PlayActionButton;->mActionTopPadding:I

    .line 169
    iget v2, p0, Lcom/google/android/play/layout/PlayActionButton;->mActionBottomPadding:I

    .line 171
    :cond_1
    invoke-virtual {p0, v4, v3, v4, v2}, Lcom/google/android/play/layout/PlayActionButton;->setPadding(IIII)V

    .line 172
    return-void

    .line 132
    .end local v1    # "context":Landroid/content/Context;
    .end local v2    # "paddingBottom":I
    .end local v3    # "paddingTop":I
    .end local v4    # "xPadding":I
    :cond_2
    iget-object v5, p0, Lcom/google/android/play/layout/PlayActionButton;->mOriginalText:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v5

    goto :goto_0

    .line 142
    .restart local v1    # "context":Landroid/content/Context;
    .restart local v2    # "paddingBottom":I
    .restart local v3    # "paddingTop":I
    :cond_3
    iget v0, p0, Lcom/google/android/play/layout/PlayActionButton;->mOriginalBackendId:I

    goto :goto_1

    .line 146
    :cond_4
    iget v5, p0, Lcom/google/android/play/layout/PlayActionButton;->mActionStyle:I

    packed-switch v5, :pswitch_data_0

    goto :goto_2

    .line 148
    :pswitch_0
    iget v5, p0, Lcom/google/android/play/layout/PlayActionButton;->mOriginalBackendId:I

    invoke-static {v1, v5}, Lcom/google/android/play/utils/PlayCorpusUtils;->getPlayActionButtonBackgroundDrawable(Landroid/content/Context;I)I

    move-result v5

    invoke-virtual {p0, v5}, Lcom/google/android/play/layout/PlayActionButton;->setBackgroundResource(I)V

    .line 150
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayActionButton;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget v7, Lcom/google/android/play/R$color;->play_action_button_text:I

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/google/android/play/layout/PlayActionButton;->setTextColor(Landroid/content/res/ColorStateList;)V

    goto :goto_2

    .line 153
    :pswitch_1
    sget v5, Lcom/google/android/play/R$drawable;->play_action_button_secondary:I

    invoke-virtual {p0, v5}, Lcom/google/android/play/layout/PlayActionButton;->setBackgroundResource(I)V

    .line 154
    iget v5, p0, Lcom/google/android/play/layout/PlayActionButton;->mOriginalBackendId:I

    invoke-static {v1, v5}, Lcom/google/android/play/utils/PlayCorpusUtils;->getPrimaryTextColor(Landroid/content/Context;I)Landroid/content/res/ColorStateList;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/google/android/play/layout/PlayActionButton;->setTextColor(Landroid/content/res/ColorStateList;)V

    goto :goto_2

    .line 157
    :pswitch_2
    iget v5, p0, Lcom/google/android/play/layout/PlayActionButton;->mOriginalBackendId:I

    invoke-static {v1, v5}, Lcom/google/android/play/utils/PlayCorpusUtils;->getPlayActionButtonBackgroundSecondaryDrawable(Landroid/content/Context;I)I

    move-result v5

    invoke-virtual {p0, v5}, Lcom/google/android/play/layout/PlayActionButton;->setBackgroundResource(I)V

    .line 160
    iget v5, p0, Lcom/google/android/play/layout/PlayActionButton;->mOriginalBackendId:I

    invoke-static {v1, v5}, Lcom/google/android/play/utils/PlayCorpusUtils;->getPrimaryTextColor(Landroid/content/Context;I)Landroid/content/res/ColorStateList;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/google/android/play/layout/PlayActionButton;->setTextColor(Landroid/content/res/ColorStateList;)V

    goto :goto_2

    .line 166
    :cond_5
    iget v4, p0, Lcom/google/android/play/layout/PlayActionButton;->mActionXPadding:I

    goto :goto_3

    .line 146
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public setActionStyle(I)V
    .locals 1
    .param p1, "actionStyle"    # I

    .prologue
    .line 90
    iget v0, p0, Lcom/google/android/play/layout/PlayActionButton;->mActionStyle:I

    if-eq v0, p1, :cond_0

    .line 91
    iput p1, p0, Lcom/google/android/play/layout/PlayActionButton;->mActionStyle:I

    .line 94
    invoke-direct {p0}, Lcom/google/android/play/layout/PlayActionButton;->syncAppearance()V

    .line 96
    :cond_0
    return-void
.end method

.method public setDrawAsLabel(Z)V
    .locals 1
    .param p1, "drawAsLabel"    # Z

    .prologue
    .line 80
    iget-boolean v0, p0, Lcom/google/android/play/layout/PlayActionButton;->mDrawAsLabel:Z

    if-eq v0, p1, :cond_0

    .line 81
    iput-boolean p1, p0, Lcom/google/android/play/layout/PlayActionButton;->mDrawAsLabel:Z

    .line 85
    invoke-direct {p0}, Lcom/google/android/play/layout/PlayActionButton;->syncAppearance()V

    .line 87
    :cond_0
    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 3
    .param p1, "l"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 100
    const-string v0, "Don\'t call PlayActionButton.setOnClickListener() directly"

    .line 101
    .local v0, "error":Ljava/lang/String;
    const-string v1, "PlayCommon"

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    const-string v1, "PlayCommon"

    invoke-static {v1, v0}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    new-instance v1, Ljava/lang/UnsupportedOperationException;

    const-string v2, "Call PlayActionButton.configure()"

    invoke-direct {v1, v2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v1
.end method
