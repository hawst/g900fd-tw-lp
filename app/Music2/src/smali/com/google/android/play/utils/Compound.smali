.class public final Lcom/google/android/play/utils/Compound;
.super Ljava/lang/Object;
.source "Compound.java"


# static fields
.field private static final sTypedValue:Landroid/util/TypedValue;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    sput-object v0, Lcom/google/android/play/utils/Compound;->sTypedValue:Landroid/util/TypedValue;

    return-void
.end method

.method public static getCompound(Landroid/content/res/TypedArray;Ljava/lang/String;IZ)I
    .locals 10
    .param p0, "typedArray"    # Landroid/content/res/TypedArray;
    .param p1, "attrName"    # Ljava/lang/String;
    .param p2, "index"    # I
    .param p3, "isSize"    # Z

    .prologue
    const/4 v0, 0x0

    const/high16 v9, -0x1000000

    const/4 v8, 0x0

    .line 127
    sget-object v6, Lcom/google/android/play/utils/Compound;->sTypedValue:Landroid/util/TypedValue;

    .line 128
    .local v6, "value":Landroid/util/TypedValue;
    invoke-virtual {p0, p2, v6}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 129
    new-instance v7, Ljava/lang/IllegalArgumentException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/content/res/TypedArray;->getPositionDescription()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ": missing "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 132
    :cond_0
    iget v7, v6, Landroid/util/TypedValue;->type:I

    sparse-switch v7, :sswitch_data_0

    .line 190
    new-instance v7, Ljava/lang/IllegalArgumentException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/content/res/TypedArray;->getPositionDescription()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ": unaccepted value for "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ": "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v6}, Landroid/util/TypedValue;->coerceToString()Ljava/lang/CharSequence;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 136
    :sswitch_0
    invoke-virtual {v6}, Landroid/util/TypedValue;->getFloat()F

    move-result v1

    .line 137
    .local v1, "floatLength":F
    if-eqz p3, :cond_1

    cmpg-float v7, v1, v8

    if-gez v7, :cond_1

    .line 138
    new-instance v7, Ljava/lang/IllegalArgumentException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/content/res/TypedArray;->getPositionDescription()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ": negative float length not allowed for size attribute "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 142
    :cond_1
    cmpl-float v7, v1, v8

    if-nez v7, :cond_2

    .line 143
    .local v0, "bits":I
    :goto_0
    invoke-static {v0}, Lcom/google/android/play/utils/Compound;->isCompoundFloat(I)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 183
    .end local v0    # "bits":I
    .end local v1    # "floatLength":F
    :goto_1
    return v0

    .line 142
    .restart local v1    # "floatLength":F
    :cond_2
    invoke-virtual {v6}, Landroid/util/TypedValue;->getFloat()F

    move-result v7

    invoke-static {v7}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    goto :goto_0

    .line 146
    .restart local v0    # "bits":I
    :cond_3
    new-instance v7, Ljava/lang/IllegalArgumentException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/content/res/TypedArray;->getPositionDescription()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ": out-of-range float length for "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 153
    .end local v0    # "bits":I
    .end local v1    # "floatLength":F
    :sswitch_1
    invoke-virtual {p0}, Landroid/content/res/TypedArray;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/util/TypedValue;->getDimension(Landroid/util/DisplayMetrics;)F

    move-result v4

    .line 156
    .local v4, "pixelLength":F
    if-eqz p3, :cond_4

    const/high16 v7, 0x3f800000    # 1.0f

    cmpl-float v7, v4, v7

    if-ltz v7, :cond_6

    .line 157
    :cond_4
    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 167
    .local v2, "intLength":I
    :goto_2
    if-lt v2, v9, :cond_5

    const v7, 0xffffff

    if-le v2, v7, :cond_9

    .line 168
    :cond_5
    new-instance v7, Ljava/lang/IllegalArgumentException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/content/res/TypedArray;->getPositionDescription()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ": out-of-range dimension length for "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 158
    .end local v2    # "intLength":I
    :cond_6
    cmpg-float v7, v4, v8

    if-gez v7, :cond_7

    .line 159
    new-instance v7, Ljava/lang/IllegalArgumentException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/content/res/TypedArray;->getPositionDescription()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ": negative dimen length not allowed for size attribute "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 165
    :cond_7
    cmpl-float v7, v4, v8

    if-nez v7, :cond_8

    move v2, v0

    .restart local v2    # "intLength":I
    :goto_3
    goto :goto_2

    .end local v2    # "intLength":I
    :cond_8
    const/4 v2, 0x1

    goto :goto_3

    .restart local v2    # "intLength":I
    :cond_9
    move v0, v2

    .line 171
    goto/16 :goto_1

    .line 178
    .end local v2    # "intLength":I
    .end local v4    # "pixelLength":F
    :sswitch_2
    iget v3, v6, Landroid/util/TypedValue;->data:I

    .line 179
    .local v3, "intValue":I
    and-int v5, v3, v9

    .line 180
    .local v5, "type":I
    const/high16 v7, 0x7f000000

    if-eq v5, v7, :cond_a

    if-eqz p3, :cond_b

    if-ne v5, v9, :cond_b

    :cond_a
    move v0, v3

    .line 183
    goto/16 :goto_1

    .line 185
    :cond_b
    new-instance v7, Ljava/lang/IllegalArgumentException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/content/res/TypedArray;->getPositionDescription()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ": invalid enum value "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " for "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 132
    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x5 -> :sswitch_1
        0x10 -> :sswitch_2
        0x11 -> :sswitch_2
    .end sparse-switch
.end method

.method public static isCompoundFloat(I)Z
    .locals 1
    .param p0, "compound"    # I

    .prologue
    .line 68
    const/high16 v0, -0x1000000

    and-int/2addr v0, p0

    sparse-switch v0, :sswitch_data_0

    .line 75
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 73
    :sswitch_0
    const/4 v0, 0x0

    goto :goto_0

    .line 68
    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_0
        -0x1000000 -> :sswitch_0
        0x0 -> :sswitch_0
        0x7f000000 -> :sswitch_0
    .end sparse-switch
.end method
