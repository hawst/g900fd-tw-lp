.class public Lcom/google/android/play/layout/StarRatingBar;
.super Landroid/view/View;
.source "StarRatingBar.java"


# instance fields
.field private mGap:F

.field private mHalfStarWidth:F

.field private mLeftHalfStarPath:Landroid/graphics/Path;

.field private mRadius:D

.field private mRange:F

.field private mRating:F

.field private mRightHalfStarPath:Landroid/graphics/Path;

.field private mShortRadius:D

.field private mStarBackgroundPaint:Landroid/graphics/Paint;

.field private final mStarHeight:D

.field private mStarPaint:Landroid/graphics/Paint;

.field private mStarPath:Landroid/graphics/Path;

.field private mVertices:[Landroid/graphics/PointF;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 131
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/play/layout/StarRatingBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 132
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 135
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 137
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 138
    .local v1, "res":Landroid/content/res/Resources;
    sget-object v5, Lcom/google/android/play/R$styleable;->StarRatingBar:[I

    invoke-virtual {p1, p2, v5}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v4

    .line 140
    .local v4, "viewAttrs":Landroid/content/res/TypedArray;
    sget v5, Lcom/google/android/play/R$styleable;->StarRatingBar_gap:I

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v5

    int-to-float v5, v5

    iput v5, p0, Lcom/google/android/play/layout/StarRatingBar;->mGap:F

    .line 142
    sget v5, Lcom/google/android/play/R$styleable;->StarRatingBar_rating:I

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v5

    iput v5, p0, Lcom/google/android/play/layout/StarRatingBar;->mRating:F

    .line 143
    sget v5, Lcom/google/android/play/R$styleable;->StarRatingBar_star_height:I

    sget v6, Lcom/google/android/play/R$dimen;->play_star_height_default:I

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v5

    int-to-double v6, v5

    iput-wide v6, p0, Lcom/google/android/play/layout/StarRatingBar;->mStarHeight:D

    .line 145
    sget v5, Lcom/google/android/play/R$styleable;->StarRatingBar_range:I

    const/4 v6, 0x5

    invoke-virtual {v4, v5, v6}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v5

    int-to-float v5, v5

    iput v5, p0, Lcom/google/android/play/layout/StarRatingBar;->mRange:F

    .line 146
    sget v5, Lcom/google/android/play/R$styleable;->StarRatingBar_star_color:I

    sget v6, Lcom/google/android/play/R$color;->play_white:I

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v3

    .line 148
    .local v3, "starColor":I
    sget v5, Lcom/google/android/play/R$styleable;->StarRatingBar_star_bg_color:I

    sget v6, Lcom/google/android/play/R$color;->play_transparent:I

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    .line 150
    .local v2, "starBackgroundColor":I
    invoke-virtual {v4}, Landroid/content/res/TypedArray;->recycle()V

    .line 152
    new-instance v5, Landroid/graphics/Paint;

    const/4 v6, 0x1

    invoke-direct {v5, v6}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v5, p0, Lcom/google/android/play/layout/StarRatingBar;->mStarPaint:Landroid/graphics/Paint;

    .line 153
    iget-object v5, p0, Lcom/google/android/play/layout/StarRatingBar;->mStarPaint:Landroid/graphics/Paint;

    invoke-virtual {v5, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 154
    iget-object v5, p0, Lcom/google/android/play/layout/StarRatingBar;->mStarPaint:Landroid/graphics/Paint;

    sget-object v6, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 155
    new-instance v5, Landroid/graphics/Paint;

    const/4 v6, 0x1

    invoke-direct {v5, v6}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v5, p0, Lcom/google/android/play/layout/StarRatingBar;->mStarBackgroundPaint:Landroid/graphics/Paint;

    .line 156
    iget-object v5, p0, Lcom/google/android/play/layout/StarRatingBar;->mStarBackgroundPaint:Landroid/graphics/Paint;

    invoke-virtual {v5, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 157
    iget-object v5, p0, Lcom/google/android/play/layout/StarRatingBar;->mStarBackgroundPaint:Landroid/graphics/Paint;

    sget-object v6, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 159
    new-instance v5, Landroid/graphics/Path;

    invoke-direct {v5}, Landroid/graphics/Path;-><init>()V

    iput-object v5, p0, Lcom/google/android/play/layout/StarRatingBar;->mStarPath:Landroid/graphics/Path;

    .line 160
    iget-object v5, p0, Lcom/google/android/play/layout/StarRatingBar;->mStarPath:Landroid/graphics/Path;

    sget-object v6, Landroid/graphics/Path$FillType;->EVEN_ODD:Landroid/graphics/Path$FillType;

    invoke-virtual {v5, v6}, Landroid/graphics/Path;->setFillType(Landroid/graphics/Path$FillType;)V

    .line 161
    new-instance v5, Landroid/graphics/Path;

    invoke-direct {v5}, Landroid/graphics/Path;-><init>()V

    iput-object v5, p0, Lcom/google/android/play/layout/StarRatingBar;->mLeftHalfStarPath:Landroid/graphics/Path;

    .line 162
    iget-object v5, p0, Lcom/google/android/play/layout/StarRatingBar;->mLeftHalfStarPath:Landroid/graphics/Path;

    sget-object v6, Landroid/graphics/Path$FillType;->EVEN_ODD:Landroid/graphics/Path$FillType;

    invoke-virtual {v5, v6}, Landroid/graphics/Path;->setFillType(Landroid/graphics/Path$FillType;)V

    .line 163
    new-instance v5, Landroid/graphics/Path;

    invoke-direct {v5}, Landroid/graphics/Path;-><init>()V

    iput-object v5, p0, Lcom/google/android/play/layout/StarRatingBar;->mRightHalfStarPath:Landroid/graphics/Path;

    .line 164
    iget-object v5, p0, Lcom/google/android/play/layout/StarRatingBar;->mRightHalfStarPath:Landroid/graphics/Path;

    sget-object v6, Landroid/graphics/Path$FillType;->EVEN_ODD:Landroid/graphics/Path$FillType;

    invoke-virtual {v5, v6}, Landroid/graphics/Path;->setFillType(Landroid/graphics/Path$FillType;)V

    .line 166
    iget-wide v6, p0, Lcom/google/android/play/layout/StarRatingBar;->mStarHeight:D

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    const-wide v10, 0x3fee28c731eb6950L    # 0.9424777960769379

    invoke-static {v10, v11}, Ljava/lang/Math;->sin(D)D

    move-result-wide v10

    add-double/2addr v8, v10

    div-double/2addr v6, v8

    iput-wide v6, p0, Lcom/google/android/play/layout/StarRatingBar;->mRadius:D

    .line 167
    const-wide v6, 0x3fd921fb54442d18L    # 0.39269908169872414

    invoke-static {v6, v7}, Ljava/lang/Math;->sin(D)D

    move-result-wide v6

    iget-wide v8, p0, Lcom/google/android/play/layout/StarRatingBar;->mRadius:D

    mul-double/2addr v6, v8

    const-wide v8, 0x4000f6f00c146b3dL    # 2.1205750411731104

    invoke-static {v8, v9}, Ljava/lang/Math;->sin(D)D

    move-result-wide v8

    div-double/2addr v6, v8

    iput-wide v6, p0, Lcom/google/android/play/layout/StarRatingBar;->mShortRadius:D

    .line 170
    iget-wide v6, p0, Lcom/google/android/play/layout/StarRatingBar;->mRadius:D

    const-wide v8, 0x3ff41b2f769cf0e0L    # 1.2566370614359172

    invoke-static {v8, v9}, Ljava/lang/Math;->sin(D)D

    move-result-wide v8

    mul-double/2addr v6, v8

    double-to-float v5, v6

    iput v5, p0, Lcom/google/android/play/layout/StarRatingBar;->mHalfStarWidth:F

    .line 172
    const/16 v5, 0xa

    new-array v5, v5, [Landroid/graphics/PointF;

    iput-object v5, p0, Lcom/google/android/play/layout/StarRatingBar;->mVertices:[Landroid/graphics/PointF;

    .line 173
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v5, 0xa

    if-ge v0, v5, :cond_0

    .line 174
    iget-object v5, p0, Lcom/google/android/play/layout/StarRatingBar;->mVertices:[Landroid/graphics/PointF;

    new-instance v6, Landroid/graphics/PointF;

    invoke-direct {v6}, Landroid/graphics/PointF;-><init>()V

    aput-object v6, v5, v0

    .line 173
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 177
    :cond_0
    invoke-direct {p0}, Lcom/google/android/play/layout/StarRatingBar;->initializeStarVertices()V

    .line 178
    invoke-direct {p0}, Lcom/google/android/play/layout/StarRatingBar;->initializeStarPaths()V

    .line 180
    const/4 v5, 0x0

    invoke-virtual {p0, v5}, Lcom/google/android/play/layout/StarRatingBar;->setWillNotDraw(Z)V

    .line 181
    return-void
.end method

.method private initializeStarPaths()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 294
    iget-object v1, p0, Lcom/google/android/play/layout/StarRatingBar;->mStarPath:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->reset()V

    .line 296
    iget-object v1, p0, Lcom/google/android/play/layout/StarRatingBar;->mStarPath:Landroid/graphics/Path;

    iget-object v2, p0, Lcom/google/android/play/layout/StarRatingBar;->mVertices:[Landroid/graphics/PointF;

    aget-object v2, v2, v4

    iget v2, v2, Landroid/graphics/PointF;->x:F

    iget-object v3, p0, Lcom/google/android/play/layout/StarRatingBar;->mVertices:[Landroid/graphics/PointF;

    aget-object v3, v3, v4

    iget v3, v3, Landroid/graphics/PointF;->y:F

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 298
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/google/android/play/layout/StarRatingBar;->mVertices:[Landroid/graphics/PointF;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 299
    iget-object v1, p0, Lcom/google/android/play/layout/StarRatingBar;->mStarPath:Landroid/graphics/Path;

    iget-object v2, p0, Lcom/google/android/play/layout/StarRatingBar;->mVertices:[Landroid/graphics/PointF;

    aget-object v2, v2, v0

    iget v2, v2, Landroid/graphics/PointF;->x:F

    iget-object v3, p0, Lcom/google/android/play/layout/StarRatingBar;->mVertices:[Landroid/graphics/PointF;

    aget-object v3, v3, v0

    iget v3, v3, Landroid/graphics/PointF;->y:F

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 298
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 301
    :cond_0
    iget-object v1, p0, Lcom/google/android/play/layout/StarRatingBar;->mStarPath:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->close()V

    .line 304
    iget-object v1, p0, Lcom/google/android/play/layout/StarRatingBar;->mLeftHalfStarPath:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->reset()V

    .line 306
    iget-object v1, p0, Lcom/google/android/play/layout/StarRatingBar;->mLeftHalfStarPath:Landroid/graphics/Path;

    iget-object v2, p0, Lcom/google/android/play/layout/StarRatingBar;->mVertices:[Landroid/graphics/PointF;

    aget-object v2, v2, v4

    iget v2, v2, Landroid/graphics/PointF;->x:F

    iget-object v3, p0, Lcom/google/android/play/layout/StarRatingBar;->mVertices:[Landroid/graphics/PointF;

    aget-object v3, v3, v4

    iget v3, v3, Landroid/graphics/PointF;->y:F

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 308
    const/4 v0, 0x5

    :goto_1
    iget-object v1, p0, Lcom/google/android/play/layout/StarRatingBar;->mVertices:[Landroid/graphics/PointF;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 309
    iget-object v1, p0, Lcom/google/android/play/layout/StarRatingBar;->mLeftHalfStarPath:Landroid/graphics/Path;

    iget-object v2, p0, Lcom/google/android/play/layout/StarRatingBar;->mVertices:[Landroid/graphics/PointF;

    aget-object v2, v2, v0

    iget v2, v2, Landroid/graphics/PointF;->x:F

    iget-object v3, p0, Lcom/google/android/play/layout/StarRatingBar;->mVertices:[Landroid/graphics/PointF;

    aget-object v3, v3, v0

    iget v3, v3, Landroid/graphics/PointF;->y:F

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 308
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 311
    :cond_1
    iget-object v1, p0, Lcom/google/android/play/layout/StarRatingBar;->mLeftHalfStarPath:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->close()V

    .line 314
    iget-object v1, p0, Lcom/google/android/play/layout/StarRatingBar;->mRightHalfStarPath:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->reset()V

    .line 316
    iget-object v1, p0, Lcom/google/android/play/layout/StarRatingBar;->mRightHalfStarPath:Landroid/graphics/Path;

    iget-object v2, p0, Lcom/google/android/play/layout/StarRatingBar;->mVertices:[Landroid/graphics/PointF;

    aget-object v2, v2, v4

    iget v2, v2, Landroid/graphics/PointF;->x:F

    iget-object v3, p0, Lcom/google/android/play/layout/StarRatingBar;->mVertices:[Landroid/graphics/PointF;

    aget-object v3, v3, v4

    iget v3, v3, Landroid/graphics/PointF;->y:F

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 318
    const/4 v0, 0x1

    :goto_2
    const/4 v1, 0x5

    if-gt v0, v1, :cond_2

    .line 319
    iget-object v1, p0, Lcom/google/android/play/layout/StarRatingBar;->mRightHalfStarPath:Landroid/graphics/Path;

    iget-object v2, p0, Lcom/google/android/play/layout/StarRatingBar;->mVertices:[Landroid/graphics/PointF;

    aget-object v2, v2, v0

    iget v2, v2, Landroid/graphics/PointF;->x:F

    iget-object v3, p0, Lcom/google/android/play/layout/StarRatingBar;->mVertices:[Landroid/graphics/PointF;

    aget-object v3, v3, v0

    iget v3, v3, Landroid/graphics/PointF;->y:F

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 318
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 321
    :cond_2
    iget-object v1, p0, Lcom/google/android/play/layout/StarRatingBar;->mRightHalfStarPath:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->close()V

    .line 322
    return-void
.end method

.method private initializeStarVertices()V
    .locals 13

    .prologue
    const/4 v12, 0x2

    const/4 v7, 0x1

    const-wide v10, 0x3ff41b2f769cf0e0L    # 1.2566370614359172

    const-wide v8, 0x3fe41b2f769cf0e0L    # 0.6283185307179586

    const/high16 v6, -0x40800000    # -1.0f

    .line 258
    iget-object v0, p0, Lcom/google/android/play/layout/StarRatingBar;->mVertices:[Landroid/graphics/PointF;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    const/4 v1, 0x0

    iput v1, v0, Landroid/graphics/PointF;->x:F

    .line 259
    iget-object v0, p0, Lcom/google/android/play/layout/StarRatingBar;->mVertices:[Landroid/graphics/PointF;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    iget-wide v2, p0, Lcom/google/android/play/layout/StarRatingBar;->mRadius:D

    double-to-float v1, v2

    mul-float/2addr v1, v6

    iput v1, v0, Landroid/graphics/PointF;->y:F

    .line 261
    iget-object v0, p0, Lcom/google/android/play/layout/StarRatingBar;->mVertices:[Landroid/graphics/PointF;

    aget-object v0, v0, v7

    iget-wide v2, p0, Lcom/google/android/play/layout/StarRatingBar;->mShortRadius:D

    invoke-static {v8, v9}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    mul-double/2addr v2, v4

    double-to-float v1, v2

    iput v1, v0, Landroid/graphics/PointF;->x:F

    .line 262
    iget-object v0, p0, Lcom/google/android/play/layout/StarRatingBar;->mVertices:[Landroid/graphics/PointF;

    aget-object v0, v0, v7

    iget-wide v2, p0, Lcom/google/android/play/layout/StarRatingBar;->mShortRadius:D

    invoke-static {v8, v9}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    mul-double/2addr v2, v4

    double-to-float v1, v2

    mul-float/2addr v1, v6

    iput v1, v0, Landroid/graphics/PointF;->y:F

    .line 264
    iget-object v0, p0, Lcom/google/android/play/layout/StarRatingBar;->mVertices:[Landroid/graphics/PointF;

    aget-object v0, v0, v12

    iget-wide v2, p0, Lcom/google/android/play/layout/StarRatingBar;->mRadius:D

    invoke-static {v10, v11}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    mul-double/2addr v2, v4

    double-to-float v1, v2

    iput v1, v0, Landroid/graphics/PointF;->x:F

    .line 265
    iget-object v0, p0, Lcom/google/android/play/layout/StarRatingBar;->mVertices:[Landroid/graphics/PointF;

    aget-object v0, v0, v12

    iget-wide v2, p0, Lcom/google/android/play/layout/StarRatingBar;->mRadius:D

    invoke-static {v10, v11}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    mul-double/2addr v2, v4

    double-to-float v1, v2

    mul-float/2addr v1, v6

    iput v1, v0, Landroid/graphics/PointF;->y:F

    .line 267
    iget-object v0, p0, Lcom/google/android/play/layout/StarRatingBar;->mVertices:[Landroid/graphics/PointF;

    const/4 v1, 0x3

    aget-object v0, v0, v1

    iget-wide v2, p0, Lcom/google/android/play/layout/StarRatingBar;->mShortRadius:D

    invoke-static {v10, v11}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    mul-double/2addr v2, v4

    double-to-float v1, v2

    iput v1, v0, Landroid/graphics/PointF;->x:F

    .line 268
    iget-object v0, p0, Lcom/google/android/play/layout/StarRatingBar;->mVertices:[Landroid/graphics/PointF;

    const/4 v1, 0x3

    aget-object v0, v0, v1

    iget-wide v2, p0, Lcom/google/android/play/layout/StarRatingBar;->mShortRadius:D

    invoke-static {v10, v11}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    mul-double/2addr v2, v4

    double-to-float v1, v2

    iput v1, v0, Landroid/graphics/PointF;->y:F

    .line 270
    iget-object v0, p0, Lcom/google/android/play/layout/StarRatingBar;->mVertices:[Landroid/graphics/PointF;

    const/4 v1, 0x4

    aget-object v0, v0, v1

    iget-wide v2, p0, Lcom/google/android/play/layout/StarRatingBar;->mRadius:D

    invoke-static {v8, v9}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    mul-double/2addr v2, v4

    double-to-float v1, v2

    iput v1, v0, Landroid/graphics/PointF;->x:F

    .line 271
    iget-object v0, p0, Lcom/google/android/play/layout/StarRatingBar;->mVertices:[Landroid/graphics/PointF;

    const/4 v1, 0x4

    aget-object v0, v0, v1

    iget-wide v2, p0, Lcom/google/android/play/layout/StarRatingBar;->mRadius:D

    double-to-float v1, v2

    float-to-double v2, v1

    invoke-static {v8, v9}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    mul-double/2addr v2, v4

    double-to-float v1, v2

    iput v1, v0, Landroid/graphics/PointF;->y:F

    .line 273
    iget-object v0, p0, Lcom/google/android/play/layout/StarRatingBar;->mVertices:[Landroid/graphics/PointF;

    const/4 v1, 0x5

    aget-object v0, v0, v1

    const/4 v1, 0x0

    iput v1, v0, Landroid/graphics/PointF;->x:F

    .line 274
    iget-object v0, p0, Lcom/google/android/play/layout/StarRatingBar;->mVertices:[Landroid/graphics/PointF;

    const/4 v1, 0x5

    aget-object v0, v0, v1

    iget-wide v2, p0, Lcom/google/android/play/layout/StarRatingBar;->mShortRadius:D

    double-to-float v1, v2

    iput v1, v0, Landroid/graphics/PointF;->y:F

    .line 276
    iget-object v0, p0, Lcom/google/android/play/layout/StarRatingBar;->mVertices:[Landroid/graphics/PointF;

    const/4 v1, 0x6

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/google/android/play/layout/StarRatingBar;->mVertices:[Landroid/graphics/PointF;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    iget v1, v1, Landroid/graphics/PointF;->x:F

    mul-float/2addr v1, v6

    iput v1, v0, Landroid/graphics/PointF;->x:F

    .line 277
    iget-object v0, p0, Lcom/google/android/play/layout/StarRatingBar;->mVertices:[Landroid/graphics/PointF;

    const/4 v1, 0x6

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/google/android/play/layout/StarRatingBar;->mVertices:[Landroid/graphics/PointF;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    iget v1, v1, Landroid/graphics/PointF;->y:F

    iput v1, v0, Landroid/graphics/PointF;->y:F

    .line 279
    iget-object v0, p0, Lcom/google/android/play/layout/StarRatingBar;->mVertices:[Landroid/graphics/PointF;

    const/4 v1, 0x7

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/google/android/play/layout/StarRatingBar;->mVertices:[Landroid/graphics/PointF;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    iget v1, v1, Landroid/graphics/PointF;->x:F

    mul-float/2addr v1, v6

    iput v1, v0, Landroid/graphics/PointF;->x:F

    .line 280
    iget-object v0, p0, Lcom/google/android/play/layout/StarRatingBar;->mVertices:[Landroid/graphics/PointF;

    const/4 v1, 0x7

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/google/android/play/layout/StarRatingBar;->mVertices:[Landroid/graphics/PointF;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    iget v1, v1, Landroid/graphics/PointF;->y:F

    iput v1, v0, Landroid/graphics/PointF;->y:F

    .line 282
    iget-object v0, p0, Lcom/google/android/play/layout/StarRatingBar;->mVertices:[Landroid/graphics/PointF;

    const/16 v1, 0x8

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/google/android/play/layout/StarRatingBar;->mVertices:[Landroid/graphics/PointF;

    aget-object v1, v1, v12

    iget v1, v1, Landroid/graphics/PointF;->x:F

    mul-float/2addr v1, v6

    iput v1, v0, Landroid/graphics/PointF;->x:F

    .line 283
    iget-object v0, p0, Lcom/google/android/play/layout/StarRatingBar;->mVertices:[Landroid/graphics/PointF;

    const/16 v1, 0x8

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/google/android/play/layout/StarRatingBar;->mVertices:[Landroid/graphics/PointF;

    aget-object v1, v1, v12

    iget v1, v1, Landroid/graphics/PointF;->y:F

    iput v1, v0, Landroid/graphics/PointF;->y:F

    .line 285
    iget-object v0, p0, Lcom/google/android/play/layout/StarRatingBar;->mVertices:[Landroid/graphics/PointF;

    const/16 v1, 0x9

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/google/android/play/layout/StarRatingBar;->mVertices:[Landroid/graphics/PointF;

    aget-object v1, v1, v7

    iget v1, v1, Landroid/graphics/PointF;->x:F

    mul-float/2addr v1, v6

    iput v1, v0, Landroid/graphics/PointF;->x:F

    .line 286
    iget-object v0, p0, Lcom/google/android/play/layout/StarRatingBar;->mVertices:[Landroid/graphics/PointF;

    const/16 v1, 0x9

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/google/android/play/layout/StarRatingBar;->mVertices:[Landroid/graphics/PointF;

    aget-object v1, v1, v7

    iget v1, v1, Landroid/graphics/PointF;->y:F

    iput v1, v0, Landroid/graphics/PointF;->y:F

    .line 287
    return-void
.end method


# virtual methods
.method public getBaseline()I
    .locals 1

    .prologue
    .line 194
    invoke-virtual {p0}, Lcom/google/android/play/layout/StarRatingBar;->getMeasuredHeight()I

    move-result v0

    return v0
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 13
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v7, 0x1

    const/high16 v12, 0x40000000    # 2.0f

    .line 199
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 200
    invoke-virtual {p0}, Lcom/google/android/play/layout/StarRatingBar;->getPaddingTop()I

    move-result v4

    .line 201
    .local v4, "paddingTop":I
    invoke-virtual {p0}, Lcom/google/android/play/layout/StarRatingBar;->getPaddingLeft()I

    move-result v3

    .line 203
    .local v3, "paddingLeft":I
    iget v8, p0, Lcom/google/android/play/layout/StarRatingBar;->mRating:F

    float-to-int v1, v8

    .line 204
    .local v1, "numOfFullStars":I
    iget v8, p0, Lcom/google/android/play/layout/StarRatingBar;->mRating:F

    const/high16 v9, 0x3f800000    # 1.0f

    rem-float/2addr v8, v9

    const/4 v9, 0x0

    invoke-static {v8, v9}, Ljava/lang/Float;->compare(FF)I

    move-result v8

    if-lez v8, :cond_0

    move v2, v7

    .line 206
    .local v2, "numOfHalfStar":I
    :goto_0
    const/4 v0, 0x0

    .line 207
    .local v0, "currentStarToDraw":I
    int-to-float v8, v3

    iget v9, p0, Lcom/google/android/play/layout/StarRatingBar;->mHalfStarWidth:F

    add-float v5, v8, v9

    .line 208
    .local v5, "x":F
    int-to-float v8, v4

    iget-wide v10, p0, Lcom/google/android/play/layout/StarRatingBar;->mRadius:D

    double-to-float v9, v10

    add-float v6, v8, v9

    .line 211
    .local v6, "y":F
    :goto_1
    if-ge v0, v1, :cond_1

    .line 212
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 213
    int-to-float v8, v0

    iget v9, p0, Lcom/google/android/play/layout/StarRatingBar;->mHalfStarWidth:F

    mul-float/2addr v9, v12

    iget v10, p0, Lcom/google/android/play/layout/StarRatingBar;->mGap:F

    add-float/2addr v9, v10

    mul-float/2addr v8, v9

    add-float/2addr v8, v5

    invoke-virtual {p1, v8, v6}, Landroid/graphics/Canvas;->translate(FF)V

    .line 214
    iget-object v8, p0, Lcom/google/android/play/layout/StarRatingBar;->mStarPath:Landroid/graphics/Path;

    iget-object v9, p0, Lcom/google/android/play/layout/StarRatingBar;->mStarPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v8, v9}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 215
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 211
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 204
    .end local v0    # "currentStarToDraw":I
    .end local v2    # "numOfHalfStar":I
    .end local v5    # "x":F
    .end local v6    # "y":F
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 219
    .restart local v0    # "currentStarToDraw":I
    .restart local v2    # "numOfHalfStar":I
    .restart local v5    # "x":F
    .restart local v6    # "y":F
    :cond_1
    if-ne v2, v7, :cond_2

    .line 220
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 221
    int-to-float v7, v0

    iget v8, p0, Lcom/google/android/play/layout/StarRatingBar;->mHalfStarWidth:F

    mul-float/2addr v8, v12

    iget v9, p0, Lcom/google/android/play/layout/StarRatingBar;->mGap:F

    add-float/2addr v8, v9

    mul-float/2addr v7, v8

    add-float/2addr v7, v5

    invoke-virtual {p1, v7, v6}, Landroid/graphics/Canvas;->translate(FF)V

    .line 222
    iget-object v7, p0, Lcom/google/android/play/layout/StarRatingBar;->mLeftHalfStarPath:Landroid/graphics/Path;

    iget-object v8, p0, Lcom/google/android/play/layout/StarRatingBar;->mStarPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v7, v8}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 223
    iget-object v7, p0, Lcom/google/android/play/layout/StarRatingBar;->mRightHalfStarPath:Landroid/graphics/Path;

    iget-object v8, p0, Lcom/google/android/play/layout/StarRatingBar;->mStarBackgroundPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v7, v8}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 224
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 225
    add-int/lit8 v0, v0, 0x1

    .line 229
    :cond_2
    :goto_2
    int-to-float v7, v0

    iget v8, p0, Lcom/google/android/play/layout/StarRatingBar;->mRange:F

    cmpg-float v7, v7, v8

    if-gez v7, :cond_3

    .line 231
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 232
    int-to-float v7, v0

    iget v8, p0, Lcom/google/android/play/layout/StarRatingBar;->mHalfStarWidth:F

    mul-float/2addr v8, v12

    iget v9, p0, Lcom/google/android/play/layout/StarRatingBar;->mGap:F

    add-float/2addr v8, v9

    mul-float/2addr v7, v8

    add-float/2addr v7, v5

    invoke-virtual {p1, v7, v6}, Landroid/graphics/Canvas;->translate(FF)V

    .line 233
    iget-object v7, p0, Lcom/google/android/play/layout/StarRatingBar;->mStarPath:Landroid/graphics/Path;

    iget-object v8, p0, Lcom/google/android/play/layout/StarRatingBar;->mStarBackgroundPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v7, v8}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 234
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 229
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 236
    :cond_3
    return-void
.end method

.method protected onMeasure(II)V
    .locals 6
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    .line 240
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v2

    if-ne v2, v3, :cond_0

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v2

    if-ne v2, v3, :cond_0

    .line 242
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    invoke-virtual {p0, v2, v3}, Lcom/google/android/play/layout/StarRatingBar;->setMeasuredDimension(II)V

    .line 252
    :goto_0
    return-void

    .line 247
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/play/layout/StarRatingBar;->getPaddingLeft()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/play/layout/StarRatingBar;->getPaddingRight()I

    move-result v3

    add-int/2addr v2, v3

    int-to-float v2, v2

    iget v3, p0, Lcom/google/android/play/layout/StarRatingBar;->mRange:F

    const/high16 v4, 0x40000000    # 2.0f

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/google/android/play/layout/StarRatingBar;->mHalfStarWidth:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iget v3, p0, Lcom/google/android/play/layout/StarRatingBar;->mRange:F

    const/high16 v4, 0x3f800000    # 1.0f

    sub-float/2addr v3, v4

    iget v4, p0, Lcom/google/android/play/layout/StarRatingBar;->mGap:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    float-to-int v1, v2

    .line 249
    .local v1, "contentWidth":I
    invoke-virtual {p0}, Lcom/google/android/play/layout/StarRatingBar;->getPaddingTop()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/play/layout/StarRatingBar;->getPaddingBottom()I

    move-result v3

    add-int/2addr v2, v3

    int-to-double v2, v2

    iget-wide v4, p0, Lcom/google/android/play/layout/StarRatingBar;->mStarHeight:D

    add-double/2addr v2, v4

    double-to-int v0, v2

    .line 251
    .local v0, "contentHeight":I
    invoke-virtual {p0, v1, v0}, Lcom/google/android/play/layout/StarRatingBar;->setMeasuredDimension(II)V

    goto :goto_0
.end method

.method public setRating(F)V
    .locals 0
    .param p1, "rating"    # F

    .prologue
    .line 188
    iput p1, p0, Lcom/google/android/play/layout/StarRatingBar;->mRating:F

    .line 189
    invoke-virtual {p0}, Lcom/google/android/play/layout/StarRatingBar;->invalidate()V

    .line 190
    return-void
.end method
