.class Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener$1;
.super Landroid/database/DataSetObserver;
.source "PlayHeaderScrollableContentListener.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener;-><init>(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener;


# direct methods
.method constructor <init>(Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener;)V
    .locals 0

    .prologue
    .line 31
    iput-object p1, p0, Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener$1;->this$0:Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener;

    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public onChanged()V
    .locals 2

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener$1;->this$0:Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener;

    const/4 v1, 0x0

    # invokes: Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener;->reset(Z)V
    invoke-static {v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener;->access$000(Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener;Z)V

    .line 40
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener$1;->this$0:Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener;

    # getter for: Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener;->mLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;
    invoke-static {v0}, Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener;->access$100(Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener;)Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->syncCurrentListViewOnNextScroll()V

    .line 41
    return-void
.end method

.method public onInvalidated()V
    .locals 0

    .prologue
    .line 45
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener$1;->onChanged()V

    .line 46
    return-void
.end method
