.class public Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;
.super Landroid/widget/LinearLayout;
.source "PlayHeaderListTabContainer.java"


# static fields
.field private static sNextLeftRight:[I

.field private static sSelectedLeftRight:[I


# instance fields
.field private mIndexForSelection:I

.field private mPaddingValidForTabs:Z

.field private final mSelectedUnderlinePaint:Landroid/graphics/Paint;

.field private final mSelectedUnderlineThickness:I

.field private mSelectionOffset:F

.field private mStripWidth:I

.field private mUseFloatingTabPadding:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 21
    new-array v0, v1, [I

    sput-object v0, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->sSelectedLeftRight:[I

    .line 22
    new-array v0, v1, [I

    sput-object v0, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->sNextLeftRight:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 36
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 42
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->setWillNotDraw(Z)V

    .line 44
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 46
    .local v0, "res":Landroid/content/res/Resources;
    sget v1, Lcom/google/android/play/R$dimen;->play_header_list_tab_strip_selected_underline_height:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->mSelectedUnderlineThickness:I

    .line 49
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->mSelectedUnderlinePaint:Landroid/graphics/Paint;

    .line 50
    return-void
.end method

.method private static getUnderlineEdges(Landroid/view/View;[I)V
    .locals 6
    .param p0, "view"    # Landroid/view/View;
    .param p1, "outLeftRight"    # [I

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 167
    instance-of v2, p0, Landroid/widget/TextView;

    if-nez v2, :cond_0

    .line 168
    invoke-virtual {p0}, Landroid/view/View;->getLeft()I

    move-result v2

    aput v2, p1, v4

    .line 169
    invoke-virtual {p0}, Landroid/view/View;->getRight()I

    move-result v2

    aput v2, p1, v5

    .line 181
    :goto_0
    return-void

    :cond_0
    move-object v1, p0

    .line 172
    check-cast v1, Landroid/widget/TextView;

    .line 173
    .local v1, "textView":Landroid/widget/TextView;
    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    .line 174
    .local v0, "text":Ljava/lang/CharSequence;
    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-nez v2, :cond_2

    .line 175
    :cond_1
    invoke-virtual {v1}, Landroid/widget/TextView;->getLeft()I

    move-result v2

    invoke-virtual {v1}, Landroid/widget/TextView;->getRight()I

    move-result v3

    add-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    aput v2, p1, v4

    .line 176
    aget v2, p1, v4

    aput v2, p1, v5

    goto :goto_0

    .line 178
    :cond_2
    invoke-virtual {v1}, Landroid/widget/TextView;->getLeft()I

    move-result v2

    aput v2, p1, v4

    .line 179
    invoke-virtual {v1}, Landroid/widget/TextView;->getRight()I

    move-result v2

    aput v2, p1, v5

    goto :goto_0
.end method

.method private updatePadding()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, -0x2

    .line 105
    const/4 v3, 0x0

    .line 106
    .local v3, "leftPadding":I
    const/4 v4, 0x0

    .line 107
    .local v4, "rightPadding":I
    iget-boolean v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->mUseFloatingTabPadding:Z

    if-eqz v5, :cond_1

    .line 108
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget v6, Lcom/google/android/play/R$dimen;->play_header_list_tab_floating_padding:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 110
    move v4, v3

    .line 124
    :cond_0
    :goto_0
    invoke-virtual {p0, v3, v7, v4, v7}, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->setPadding(IIII)V

    .line 125
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->mPaddingValidForTabs:Z

    .line 126
    return-void

    .line 112
    :cond_1
    iget v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->mStripWidth:I

    div-int/lit8 v1, v5, 0x2

    .line 113
    .local v1, "halfWidth":I
    invoke-virtual {p0, v7}, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 114
    .local v0, "firstChild":Landroid/view/View;
    if-eqz v0, :cond_2

    .line 115
    invoke-virtual {v0, v6, v6}, Landroid/view/View;->measure(II)V

    .line 116
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    sub-int v3, v1, v5

    .line 118
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->getChildCount()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {p0, v5}, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 119
    .local v2, "lastChild":Landroid/view/View;
    if-eqz v2, :cond_0

    .line 120
    invoke-virtual {v2, v6, v6}, Landroid/view/View;->measure(II)V

    .line 121
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    sub-int v4, v1, v5

    goto :goto_0
.end method


# virtual methods
.method invalidateTabPadding()V
    .locals 1

    .prologue
    .line 97
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->mPaddingValidForTabs:Z

    .line 98
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 10
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/high16 v9, 0x3f800000    # 1.0f

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 138
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->getChildCount()I

    move-result v6

    .line 141
    .local v6, "childCount":I
    if-lez v6, :cond_1

    iget v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->mIndexForSelection:I

    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 142
    iget v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->mIndexForSelection:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    sget-object v1, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->sSelectedLeftRight:[I

    invoke-static {v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->getUnderlineEdges(Landroid/view/View;[I)V

    .line 143
    iget v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->mSelectionOffset:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    iget v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->mIndexForSelection:I

    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_0

    .line 145
    iget v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->mIndexForSelection:I

    add-int/lit8 v8, v0, 0x1

    .line 146
    .local v8, "nextPosition":I
    invoke-virtual {p0, v8}, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    sget-object v1, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->sNextLeftRight:[I

    invoke-static {v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->getUnderlineEdges(Landroid/view/View;[I)V

    .line 147
    sget-object v0, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->sSelectedLeftRight:[I

    iget v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->mSelectionOffset:F

    sget-object v2, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->sNextLeftRight:[I

    aget v2, v2, v4

    int-to-float v2, v2

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->mSelectionOffset:F

    sub-float v2, v9, v2

    sget-object v3, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->sSelectedLeftRight:[I

    aget v3, v3, v4

    int-to-float v3, v3

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    float-to-int v1, v1

    aput v1, v0, v4

    .line 150
    sget-object v0, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->sSelectedLeftRight:[I

    iget v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->mSelectionOffset:F

    sget-object v2, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->sNextLeftRight:[I

    aget v2, v2, v5

    int-to-float v2, v2

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->mSelectionOffset:F

    sub-float v2, v9, v2

    sget-object v3, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->sSelectedLeftRight:[I

    aget v3, v3, v5

    int-to-float v3, v3

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    float-to-int v1, v1

    aput v1, v0, v5

    .line 154
    .end local v8    # "nextPosition":I
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->getHeight()I

    move-result v7

    .line 155
    .local v7, "height":I
    sget-object v0, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->sSelectedLeftRight:[I

    aget v0, v0, v4

    int-to-float v1, v0

    iget v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->mSelectedUnderlineThickness:I

    sub-int v0, v7, v0

    int-to-float v2, v0

    sget-object v0, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->sSelectedLeftRight:[I

    aget v0, v0, v5

    int-to-float v3, v0

    int-to-float v4, v7

    iget-object v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->mSelectedUnderlinePaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 159
    .end local v7    # "height":I
    :cond_1
    return-void
.end method

.method onPageScrolled(IF)V
    .locals 0
    .param p1, "position"    # I
    .param p2, "positionOffset"    # F

    .prologue
    .line 65
    iput p1, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->mIndexForSelection:I

    .line 66
    iput p2, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->mSelectionOffset:F

    .line 67
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->invalidate()V

    .line 68
    return-void
.end method

.method onPageSelected(I)V
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 75
    iput p1, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->mIndexForSelection:I

    .line 76
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->mSelectionOffset:F

    .line 77
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->invalidate()V

    .line 78
    return-void
.end method

.method public setSelectedIndicatorColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->mSelectedUnderlinePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 57
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->invalidate()V

    .line 58
    return-void
.end method

.method setStripWidth(I)V
    .locals 1
    .param p1, "width"    # I

    .prologue
    .line 87
    iget v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->mStripWidth:I

    if-ne v0, p1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->mPaddingValidForTabs:Z

    if-nez v0, :cond_1

    .line 88
    :cond_0
    iput p1, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->mStripWidth:I

    .line 89
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->updatePadding()V

    .line 91
    :cond_1
    return-void
.end method

.method public setUseFloatingTabPadding(Z)V
    .locals 1
    .param p1, "useFloatingTabPadding"    # Z

    .prologue
    .line 130
    iget-boolean v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->mUseFloatingTabPadding:Z

    if-eq v0, p1, :cond_0

    .line 131
    iput-boolean p1, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->mUseFloatingTabPadding:Z

    .line 132
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->updatePadding()V

    .line 134
    :cond_0
    return-void
.end method
