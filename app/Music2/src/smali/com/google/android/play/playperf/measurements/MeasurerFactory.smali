.class public interface abstract Lcom/google/android/play/playperf/measurements/MeasurerFactory;
.super Ljava/lang/Object;
.source "MeasurerFactory.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/google/android/play/playperf/measurements/Recordable;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# virtual methods
.method public abstract getMeasurerInstance(Ljava/lang/String;)Lcom/google/android/play/playperf/measurements/Measurer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/android/play/playperf/measurements/Measurer",
            "<TT;>;"
        }
    .end annotation
.end method
