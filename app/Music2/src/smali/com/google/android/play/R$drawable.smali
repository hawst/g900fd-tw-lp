.class public final Lcom/google/android/play/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/play/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final bg_default_profile_art:I = 0x7f02003b

.field public static final drawer_shadow:I = 0x7f0200bb

.field public static final ic_down_white_16:I = 0x7f0200e2

.field public static final ic_mic_dark:I = 0x7f020115

.field public static final ic_profile_none:I = 0x7f020136

.field public static final ic_up_white_16:I = 0x7f02016e

.field public static final overlay_focused_blue:I = 0x7f0201fb

.field public static final play_action_button_apps:I = 0x7f020201

.field public static final play_action_button_apps_secondary:I = 0x7f020203

.field public static final play_action_button_books:I = 0x7f020205

.field public static final play_action_button_books_secondary:I = 0x7f020207

.field public static final play_action_button_movies:I = 0x7f020209

.field public static final play_action_button_movies_secondary:I = 0x7f02020b

.field public static final play_action_button_multi:I = 0x7f02020d

.field public static final play_action_button_multi_secondary:I = 0x7f02020f

.field public static final play_action_button_music:I = 0x7f020211

.field public static final play_action_button_music_secondary:I = 0x7f020213

.field public static final play_action_button_newsstand:I = 0x7f020215

.field public static final play_action_button_newsstand_secondary:I = 0x7f020217

.field public static final play_action_button_secondary:I = 0x7f020219

.field public static final play_header_list_tab_high_contrast_bg:I = 0x7f020220

.field public static final play_ic_clear:I = 0x7f02022c

.field public static final play_ic_download_background_medium:I = 0x7f02022e

.field public static final play_ic_download_complete_medium:I = 0x7f020231

.field public static final play_ic_download_default_medium:I = 0x7f020234

.field public static final play_ic_download_progress_medium:I = 0x7f020237

.field public static final play_onboard_page_indicator_dot:I = 0x7f02023b

.field public static final play_overlay_pressed_dark:I = 0x7f020242
