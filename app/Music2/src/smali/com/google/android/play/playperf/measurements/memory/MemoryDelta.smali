.class public Lcom/google/android/play/playperf/measurements/memory/MemoryDelta;
.super Ljava/lang/Object;
.source "MemoryDelta.java"

# interfaces
.implements Lcom/google/android/play/playperf/measurements/Recordable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/play/playperf/measurements/memory/MemoryDelta;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mJvmHeapTotal:Lcom/google/android/play/playperf/data/DataPoint;

.field private final mJvmPrivateDirty:Lcom/google/android/play/playperf/data/DataPoint;

.field private final mNativeHeapAllocated:Lcom/google/android/play/playperf/data/DataPoint;

.field private final mOtherPrivateDirty:Lcom/google/android/play/playperf/data/DataPoint;

.field private final mSectionName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 120
    new-instance v0, Lcom/google/android/play/playperf/measurements/memory/MemoryDelta$1;

    invoke-direct {v0}, Lcom/google/android/play/playperf/measurements/memory/MemoryDelta$1;-><init>()V

    sput-object v0, Lcom/google/android/play/playperf/measurements/memory/MemoryDelta;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "parcel"    # Landroid/os/Parcel;

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/playperf/data/DataPoint;

    iput-object v0, p0, Lcom/google/android/play/playperf/measurements/memory/MemoryDelta;->mJvmHeapTotal:Lcom/google/android/play/playperf/data/DataPoint;

    .line 52
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/playperf/data/DataPoint;

    iput-object v0, p0, Lcom/google/android/play/playperf/measurements/memory/MemoryDelta;->mNativeHeapAllocated:Lcom/google/android/play/playperf/data/DataPoint;

    .line 53
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/playperf/data/DataPoint;

    iput-object v0, p0, Lcom/google/android/play/playperf/measurements/memory/MemoryDelta;->mJvmPrivateDirty:Lcom/google/android/play/playperf/data/DataPoint;

    .line 54
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/playperf/data/DataPoint;

    iput-object v0, p0, Lcom/google/android/play/playperf/measurements/memory/MemoryDelta;->mOtherPrivateDirty:Lcom/google/android/play/playperf/data/DataPoint;

    .line 55
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/play/playperf/measurements/memory/MemoryDelta;->mSectionName:Ljava/lang/String;

    .line 56
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/google/android/play/playperf/measurements/memory/MemoryDelta$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/google/android/play/playperf/measurements/memory/MemoryDelta$1;

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lcom/google/android/play/playperf/measurements/memory/MemoryDelta;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/play/playperf/measurements/memory/AndroidMemorySnapshot;Lcom/google/android/play/playperf/measurements/memory/AndroidMemorySnapshot;JLjava/lang/String;)V
    .locals 5
    .param p1, "memoryStart"    # Lcom/google/android/play/playperf/measurements/memory/AndroidMemorySnapshot;
    .param p2, "memoryEnd"    # Lcom/google/android/play/playperf/measurements/memory/AndroidMemorySnapshot;
    .param p3, "time"    # J
    .param p5, "sectionName"    # Ljava/lang/String;

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    invoke-virtual {p2, p1}, Lcom/google/android/play/playperf/measurements/memory/AndroidMemorySnapshot;->subtract(Lcom/google/android/play/playperf/measurements/memory/AndroidMemorySnapshot;)Lcom/google/android/play/playperf/measurements/memory/AndroidMemorySnapshot;

    move-result-object v0

    .line 43
    .local v0, "changeSnapshot":Lcom/google/android/play/playperf/measurements/memory/AndroidMemorySnapshot;
    new-instance v1, Lcom/google/android/play/playperf/data/DataPoint;

    invoke-virtual {v0}, Lcom/google/android/play/playperf/measurements/memory/AndroidMemorySnapshot;->getJvmHeapTotalB()J

    move-result-wide v2

    long-to-double v2, v2

    invoke-direct {v1, p3, p4, v2, v3}, Lcom/google/android/play/playperf/data/DataPoint;-><init>(JD)V

    iput-object v1, p0, Lcom/google/android/play/playperf/measurements/memory/MemoryDelta;->mJvmHeapTotal:Lcom/google/android/play/playperf/data/DataPoint;

    .line 44
    new-instance v1, Lcom/google/android/play/playperf/data/DataPoint;

    iget-wide v2, v0, Lcom/google/android/play/playperf/measurements/memory/AndroidMemorySnapshot;->nativeHeapAllocatedB:J

    long-to-double v2, v2

    invoke-direct {v1, p3, p4, v2, v3}, Lcom/google/android/play/playperf/data/DataPoint;-><init>(JD)V

    iput-object v1, p0, Lcom/google/android/play/playperf/measurements/memory/MemoryDelta;->mNativeHeapAllocated:Lcom/google/android/play/playperf/data/DataPoint;

    .line 45
    new-instance v1, Lcom/google/android/play/playperf/data/DataPoint;

    invoke-virtual {v0}, Lcom/google/android/play/playperf/measurements/memory/AndroidMemorySnapshot;->getJvmPrivateDirtyB()J

    move-result-wide v2

    long-to-double v2, v2

    invoke-direct {v1, p3, p4, v2, v3}, Lcom/google/android/play/playperf/data/DataPoint;-><init>(JD)V

    iput-object v1, p0, Lcom/google/android/play/playperf/measurements/memory/MemoryDelta;->mJvmPrivateDirty:Lcom/google/android/play/playperf/data/DataPoint;

    .line 46
    new-instance v1, Lcom/google/android/play/playperf/data/DataPoint;

    invoke-virtual {v0}, Lcom/google/android/play/playperf/measurements/memory/AndroidMemorySnapshot;->getOtherPrivateDirtyB()J

    move-result-wide v2

    long-to-double v2, v2

    invoke-direct {v1, p3, p4, v2, v3}, Lcom/google/android/play/playperf/data/DataPoint;-><init>(JD)V

    iput-object v1, p0, Lcom/google/android/play/playperf/measurements/memory/MemoryDelta;->mOtherPrivateDirty:Lcom/google/android/play/playperf/data/DataPoint;

    .line 47
    iput-object p5, p0, Lcom/google/android/play/playperf/measurements/memory/MemoryDelta;->mSectionName:Ljava/lang/String;

    .line 48
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 140
    const/4 v0, 0x0

    return v0
.end method

.method public getJvmHeapTotal()Lcom/google/android/play/playperf/data/DataPoint;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/android/play/playperf/measurements/memory/MemoryDelta;->mJvmHeapTotal:Lcom/google/android/play/playperf/data/DataPoint;

    return-object v0
.end method

.method public getJvmPrivateDirty()Lcom/google/android/play/playperf/data/DataPoint;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/google/android/play/playperf/measurements/memory/MemoryDelta;->mJvmPrivateDirty:Lcom/google/android/play/playperf/data/DataPoint;

    return-object v0
.end method

.method public getNativeHeapAllocated()Lcom/google/android/play/playperf/data/DataPoint;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/google/android/play/playperf/measurements/memory/MemoryDelta;->mNativeHeapAllocated:Lcom/google/android/play/playperf/data/DataPoint;

    return-object v0
.end method

.method public getOtherPrivateDirty()Lcom/google/android/play/playperf/data/DataPoint;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/google/android/play/playperf/measurements/memory/MemoryDelta;->mOtherPrivateDirty:Lcom/google/android/play/playperf/data/DataPoint;

    return-object v0
.end method

.method public getSectionName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/google/android/play/playperf/measurements/memory/MemoryDelta;->mSectionName:Ljava/lang/String;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "parcel"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 148
    invoke-virtual {p0}, Lcom/google/android/play/playperf/measurements/memory/MemoryDelta;->getJvmHeapTotal()Lcom/google/android/play/playperf/data/DataPoint;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 149
    invoke-virtual {p0}, Lcom/google/android/play/playperf/measurements/memory/MemoryDelta;->getNativeHeapAllocated()Lcom/google/android/play/playperf/data/DataPoint;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 150
    invoke-virtual {p0}, Lcom/google/android/play/playperf/measurements/memory/MemoryDelta;->getJvmPrivateDirty()Lcom/google/android/play/playperf/data/DataPoint;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 151
    invoke-virtual {p0}, Lcom/google/android/play/playperf/measurements/memory/MemoryDelta;->getOtherPrivateDirty()Lcom/google/android/play/playperf/data/DataPoint;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 152
    invoke-virtual {p0}, Lcom/google/android/play/playperf/measurements/memory/MemoryDelta;->getSectionName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 153
    return-void
.end method
