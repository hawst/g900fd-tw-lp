.class public Lcom/google/android/libraries/bind/data/DataList;
.super Ljava/lang/Object;
.source "DataList.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/libraries/bind/data/DataList$DataListListener;
    }
.end annotation


# instance fields
.field protected currentRefreshTask:Lcom/google/android/libraries/bind/data/RefreshTask;

.field private dataDirty:Z

.field private dataListListeners:Ljava/util/concurrent/CopyOnWriteArraySet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArraySet",
            "<",
            "Lcom/google/android/libraries/bind/data/DataList$DataListListener;",
            ">;"
        }
    .end annotation
.end field

.field private dataVersion:I

.field private final invalidSnapshot:Lcom/google/android/libraries/bind/data/Snapshot;

.field private isAutoRefreshing:Z

.field protected logd:Lcom/google/android/libraries/bind/logging/Logd;

.field private final observable:Lcom/google/android/libraries/bind/data/PriorityDataObservable;

.field protected final primaryKey:I

.field private registeredForInvalidation:Z

.field private snapshot:Lcom/google/android/libraries/bind/data/Snapshot;

.field protected stopAutoRefreshAfterRefresh:Z


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1, "primaryKey"    # I

    .prologue
    .line 61
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/libraries/bind/data/DataList;-><init>(ILjava/util/List;)V

    .line 62
    return-void
.end method

.method public constructor <init>(ILjava/util/List;)V
    .locals 1
    .param p1, "primaryKey"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 64
    .local p2, "optInitialData":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    new-instance v0, Lcom/google/android/libraries/bind/data/PriorityDataObservable;

    invoke-direct {v0}, Lcom/google/android/libraries/bind/data/PriorityDataObservable;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/bind/data/DataList;->observable:Lcom/google/android/libraries/bind/data/PriorityDataObservable;

    .line 45
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/bind/data/DataList;->dataListListeners:Ljava/util/concurrent/CopyOnWriteArraySet;

    .line 65
    iput p1, p0, Lcom/google/android/libraries/bind/data/DataList;->primaryKey:I

    .line 66
    new-instance v0, Lcom/google/android/libraries/bind/data/Snapshot;

    invoke-direct {v0, p1}, Lcom/google/android/libraries/bind/data/Snapshot;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/libraries/bind/data/DataList;->invalidSnapshot:Lcom/google/android/libraries/bind/data/Snapshot;

    .line 67
    if-eqz p2, :cond_0

    .line 68
    new-instance v0, Lcom/google/android/libraries/bind/data/Snapshot;

    invoke-direct {v0, p1, p2}, Lcom/google/android/libraries/bind/data/Snapshot;-><init>(ILjava/util/List;)V

    iput-object v0, p0, Lcom/google/android/libraries/bind/data/DataList;->snapshot:Lcom/google/android/libraries/bind/data/Snapshot;

    .line 69
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/libraries/bind/data/DataList;->dataVersion:I

    .line 73
    :goto_0
    return-void

    .line 71
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/DataList;->invalidSnapshot:Lcom/google/android/libraries/bind/data/Snapshot;

    iput-object v0, p0, Lcom/google/android/libraries/bind/data/DataList;->snapshot:Lcom/google/android/libraries/bind/data/Snapshot;

    goto :goto_0
.end method

.method private startRefreshTask()V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 528
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/DataList;->logd()Lcom/google/android/libraries/bind/logging/Logd;

    move-result-object v1

    const-string v2, "startRefreshTask"

    new-array v3, v0, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/libraries/bind/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 529
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/DataList;->stopRefreshTask()V

    .line 530
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/DataList;->makeRefreshTask()Lcom/google/android/libraries/bind/data/RefreshTask;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/libraries/bind/data/DataList;->currentRefreshTask:Lcom/google/android/libraries/bind/data/RefreshTask;

    .line 531
    iget-object v1, p0, Lcom/google/android/libraries/bind/data/DataList;->currentRefreshTask:Lcom/google/android/libraries/bind/data/RefreshTask;

    if-nez v1, :cond_1

    .line 533
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/DataList;->logd()Lcom/google/android/libraries/bind/logging/Logd;

    move-result-object v1

    const-string v2, "no refresh task"

    new-array v3, v0, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/libraries/bind/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 534
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/DataList;->hasRefreshedOnce()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/bind/data/DataList;->setDirty(Z)V

    .line 538
    :goto_0
    return-void

    .line 536
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/DataList;->currentRefreshTask:Lcom/google/android/libraries/bind/data/RefreshTask;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/RefreshTask;->execute()V

    goto :goto_0
.end method


# virtual methods
.method public addListener(Lcom/google/android/libraries/bind/data/DataList$DataListListener;)V
    .locals 1
    .param p1, "dataListListener"    # Lcom/google/android/libraries/bind/data/DataList$DataListListener;

    .prologue
    .line 117
    invoke-static {}, Lcom/google/android/libraries/bind/async/AsyncUtil;->checkMainThread()V

    .line 118
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/DataList;->dataListListeners:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArraySet;->add(Ljava/lang/Object;)Z

    .line 119
    return-void
.end method

.method public dataVersion()I
    .locals 1

    .prologue
    .line 426
    iget v0, p0, Lcom/google/android/libraries/bind/data/DataList;->dataVersion:I

    return v0
.end method

.method public didLastRefreshFail()Z
    .locals 1

    .prologue
    .line 450
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/DataList;->snapshot:Lcom/google/android/libraries/bind/data/Snapshot;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/Snapshot;->hasException()Z

    move-result v0

    return v0
.end method

.method protected equalityFields()[I
    .locals 1

    .prologue
    .line 375
    const/4 v0, 0x0

    return-object v0
.end method

.method public filter([ILcom/google/android/libraries/bind/data/Filter;)Lcom/google/android/libraries/bind/data/DataList;
    .locals 1
    .param p1, "equalityFields"    # [I
    .param p2, "filter"    # Lcom/google/android/libraries/bind/data/Filter;

    .prologue
    .line 223
    new-instance v0, Lcom/google/android/libraries/bind/data/FilteredDataList;

    invoke-direct {v0, p0, p2, p1}, Lcom/google/android/libraries/bind/data/FilteredDataList;-><init>(Lcom/google/android/libraries/bind/data/DataList;Lcom/google/android/libraries/bind/data/Filter;[I)V

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/FilteredDataList;->startAutoRefresh()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v0

    return-object v0
.end method

.method public filterRow(Ljava/lang/Object;Lcom/google/android/libraries/bind/data/Filter;[I)Lcom/google/android/libraries/bind/data/FilteredDataRow;
    .locals 1
    .param p1, "rowId"    # Ljava/lang/Object;
    .param p2, "filter"    # Lcom/google/android/libraries/bind/data/Filter;
    .param p3, "equalityFields"    # [I

    .prologue
    .line 256
    new-instance v0, Lcom/google/android/libraries/bind/data/FilteredDataRow;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/libraries/bind/data/FilteredDataRow;-><init>(Lcom/google/android/libraries/bind/data/DataList;Ljava/lang/Object;Lcom/google/android/libraries/bind/data/Filter;[I)V

    .line 257
    .local v0, "filteredRow":Lcom/google/android/libraries/bind/data/FilteredDataRow;
    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/FilteredDataRow;->startAutoRefresh()Lcom/google/android/libraries/bind/data/DataList;

    .line 258
    return-object v0
.end method

.method protected finalize()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 92
    iget-boolean v0, p0, Lcom/google/android/libraries/bind/data/DataList;->registeredForInvalidation:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/bind/data/DataList;->observable:Lcom/google/android/libraries/bind/data/PriorityDataObservable;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/PriorityDataObservable;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 93
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/DataList;->logd()Lcom/google/android/libraries/bind/logging/Logd;

    move-result-object v0

    const-string v1, "Leaked datalist"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/logging/Logd;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 94
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/DataList;->logd()Lcom/google/android/libraries/bind/logging/Logd;

    move-result-object v0

    const-string v1, "  Observables: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/libraries/bind/data/DataList;->observable:Lcom/google/android/libraries/bind/data/PriorityDataObservable;

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/logging/Logd;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 96
    :cond_1
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 97
    return-void
.end method

.method public findPositionForId(Ljava/lang/Object;)I
    .locals 1
    .param p1, "id"    # Ljava/lang/Object;

    .prologue
    .line 198
    invoke-static {}, Lcom/google/android/libraries/bind/async/AsyncUtil;->checkMainThread()V

    .line 199
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/DataList;->snapshot:Lcom/google/android/libraries/bind/data/Snapshot;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/bind/data/Snapshot;->findPositionForPrimaryValue(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 160
    invoke-static {}, Lcom/google/android/libraries/bind/async/AsyncUtil;->checkMainThread()V

    .line 161
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/DataList;->snapshot:Lcom/google/android/libraries/bind/data/Snapshot;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/Snapshot;->getCount()I

    move-result v0

    return v0
.end method

.method public getData(I)Lcom/google/android/libraries/bind/data/Data;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 168
    invoke-static {}, Lcom/google/android/libraries/bind/async/AsyncUtil;->checkMainThread()V

    .line 169
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/DataList;->snapshot:Lcom/google/android/libraries/bind/data/Snapshot;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/bind/data/Snapshot;->getData(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 176
    invoke-static {}, Lcom/google/android/libraries/bind/async/AsyncUtil;->checkMainThread()V

    .line 177
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/DataList;->snapshot:Lcom/google/android/libraries/bind/data/Snapshot;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/bind/data/Snapshot;->getItemId(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getSnapshot()Lcom/google/android/libraries/bind/data/Snapshot;
    .locals 1

    .prologue
    .line 266
    invoke-static {}, Lcom/google/android/libraries/bind/async/AsyncUtil;->checkMainThread()V

    .line 267
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/DataList;->snapshot:Lcom/google/android/libraries/bind/data/Snapshot;

    return-object v0
.end method

.method public hasDataSetObservers()Z
    .locals 1

    .prologue
    .line 311
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/DataList;->observable:Lcom/google/android/libraries/bind/data/PriorityDataObservable;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/PriorityDataObservable;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasRefreshedOnce()Z
    .locals 2

    .prologue
    .line 438
    invoke-static {}, Lcom/google/android/libraries/bind/async/AsyncUtil;->checkMainThread()V

    .line 440
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/DataList;->snapshot:Lcom/google/android/libraries/bind/data/Snapshot;

    iget-object v1, p0, Lcom/google/android/libraries/bind/data/DataList;->invalidSnapshot:Lcom/google/android/libraries/bind/data/Snapshot;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public invalidateData()V
    .locals 1

    .prologue
    .line 134
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/bind/data/DataList;->invalidateData(Z)V

    .line 135
    return-void
.end method

.method public invalidateData(Z)V
    .locals 6
    .param p1, "clearList"    # Z

    .prologue
    const/4 v5, 0x1

    .line 145
    invoke-static {}, Lcom/google/android/libraries/bind/async/AsyncUtil;->checkMainThread()V

    .line 146
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/DataList;->logd()Lcom/google/android/libraries/bind/logging/Logd;

    move-result-object v0

    const-string v1, "invalidateData(clearList=%b)"

    new-array v2, v5, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 147
    if-eqz p1, :cond_0

    .line 148
    const/4 v0, 0x0

    sget-object v1, Lcom/google/android/libraries/bind/data/DataChange;->INVALIDATION:Lcom/google/android/libraries/bind/data/DataChange;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/libraries/bind/data/DataList;->update(Lcom/google/android/libraries/bind/data/Snapshot;Lcom/google/android/libraries/bind/data/DataChange;)V

    .line 150
    :cond_0
    invoke-virtual {p0, v5}, Lcom/google/android/libraries/bind/data/DataList;->setDirty(Z)V

    .line 151
    iget-boolean v0, p0, Lcom/google/android/libraries/bind/data/DataList;->isAutoRefreshing:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/DataList;->hasDataSetObservers()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 152
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/DataList;->refresh()V

    .line 154
    :cond_1
    return-void
.end method

.method public isDirty()Z
    .locals 1

    .prologue
    .line 322
    iget-boolean v0, p0, Lcom/google/android/libraries/bind/data/DataList;->dataDirty:Z

    return v0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 184
    invoke-static {}, Lcom/google/android/libraries/bind/async/AsyncUtil;->checkMainThread()V

    .line 185
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/DataList;->snapshot:Lcom/google/android/libraries/bind/data/Snapshot;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/Snapshot;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public isInvalidPosition(I)Z
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 192
    invoke-static {}, Lcom/google/android/libraries/bind/async/AsyncUtil;->checkMainThread()V

    .line 193
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/DataList;->snapshot:Lcom/google/android/libraries/bind/data/Snapshot;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/bind/data/Snapshot;->isInvalidPosition(I)Z

    move-result v0

    return v0
.end method

.method public lastRefreshException()Lcom/google/android/libraries/bind/data/DataException;
    .locals 1

    .prologue
    .line 458
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/DataList;->snapshot:Lcom/google/android/libraries/bind/data/Snapshot;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/Snapshot;->getException()Lcom/google/android/libraries/bind/data/DataException;

    move-result-object v0

    return-object v0
.end method

.method protected logd()Lcom/google/android/libraries/bind/logging/Logd;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/DataList;->logd:Lcom/google/android/libraries/bind/logging/Logd;

    if-nez v0, :cond_0

    .line 108
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/libraries/bind/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/android/libraries/bind/logging/Logd;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/bind/data/DataList;->logd:Lcom/google/android/libraries/bind/logging/Logd;

    .line 110
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/DataList;->logd:Lcom/google/android/libraries/bind/logging/Logd;

    return-object v0
.end method

.method protected makeRefreshTask()Lcom/google/android/libraries/bind/data/RefreshTask;
    .locals 1

    .prologue
    .line 554
    const/4 v0, 0x0

    return-object v0
.end method

.method protected notifyDataChanged(Lcom/google/android/libraries/bind/data/DataChange;)V
    .locals 3
    .param p1, "change"    # Lcom/google/android/libraries/bind/data/DataChange;

    .prologue
    .line 430
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/DataList;->logd()Lcom/google/android/libraries/bind/logging/Logd;

    move-result-object v0

    const-string v1, "notifyDataChanged"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 431
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/DataList;->observable:Lcom/google/android/libraries/bind/data/PriorityDataObservable;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/bind/data/PriorityDataObservable;->notifyDataChanged(Lcom/google/android/libraries/bind/data/DataChange;)V

    .line 432
    return-void
.end method

.method protected onRegisterForInvalidation()V
    .locals 0

    .prologue
    .line 358
    return-void
.end method

.method protected onUnregisterForInvalidation()V
    .locals 0

    .prologue
    .line 366
    return-void
.end method

.method public postRefresh(Lcom/google/android/libraries/bind/data/RefreshTask;Lcom/google/android/libraries/bind/data/Snapshot;Lcom/google/android/libraries/bind/data/DataChange;Ljava/lang/Integer;)V
    .locals 6
    .param p1, "refreshTask"    # Lcom/google/android/libraries/bind/data/RefreshTask;
    .param p2, "snapshot"    # Lcom/google/android/libraries/bind/data/Snapshot;
    .param p3, "change"    # Lcom/google/android/libraries/bind/data/DataChange;
    .param p4, "optDataVersion"    # Ljava/lang/Integer;

    .prologue
    .line 409
    new-instance v0, Lcom/google/android/libraries/bind/data/DataList$1;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/libraries/bind/data/DataList$1;-><init>(Lcom/google/android/libraries/bind/data/DataList;Lcom/google/android/libraries/bind/data/RefreshTask;Lcom/google/android/libraries/bind/data/Snapshot;Lcom/google/android/libraries/bind/data/DataChange;Ljava/lang/Integer;)V

    .line 418
    .local v0, "updateRunnable":Ljava/lang/Runnable;
    invoke-static {}, Lcom/google/android/libraries/bind/async/AsyncUtil;->isMainThread()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 419
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 423
    :goto_0
    return-void

    .line 421
    :cond_0
    invoke-static {}, Lcom/google/android/libraries/bind/async/AsyncUtil;->mainThreadHandler()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public primaryKey()I
    .locals 1

    .prologue
    .line 76
    iget v0, p0, Lcom/google/android/libraries/bind/data/DataList;->primaryKey:I

    return v0
.end method

.method public refresh()V
    .locals 3

    .prologue
    .line 510
    invoke-static {}, Lcom/google/android/libraries/bind/async/AsyncUtil;->checkMainThread()V

    .line 511
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/DataList;->logd()Lcom/google/android/libraries/bind/logging/Logd;

    move-result-object v0

    const-string v1, "refresh"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 512
    invoke-direct {p0}, Lcom/google/android/libraries/bind/data/DataList;->startRefreshTask()V

    .line 513
    return-void
.end method

.method public registerDataObserver(Lcom/google/android/libraries/bind/data/DataObserver;)V
    .locals 1
    .param p1, "observer"    # Lcom/google/android/libraries/bind/data/DataObserver;

    .prologue
    .line 274
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/libraries/bind/data/DataList;->registerDataObserver(Lcom/google/android/libraries/bind/data/DataObserver;I)V

    .line 275
    return-void
.end method

.method public registerDataObserver(Lcom/google/android/libraries/bind/data/DataObserver;I)V
    .locals 5
    .param p1, "observer"    # Lcom/google/android/libraries/bind/data/DataObserver;
    .param p2, "priority"    # I

    .prologue
    .line 282
    invoke-static {}, Lcom/google/android/libraries/bind/async/AsyncUtil;->checkMainThread()V

    .line 283
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/DataList;->observable:Lcom/google/android/libraries/bind/data/PriorityDataObservable;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/libraries/bind/data/PriorityDataObservable;->add(Lcom/google/android/libraries/bind/data/DataObserver;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 284
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/DataList;->registerForInvalidation()V

    .line 286
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/DataList;->logd()Lcom/google/android/libraries/bind/logging/Logd;

    move-result-object v0

    const-string v1, "registerDataSetObserver - count: %d, registeredForInvalidation: %b"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/libraries/bind/data/DataList;->observable:Lcom/google/android/libraries/bind/data/PriorityDataObservable;

    invoke-virtual {v4}, Lcom/google/android/libraries/bind/data/PriorityDataObservable;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-boolean v4, p0, Lcom/google/android/libraries/bind/data/DataList;->registeredForInvalidation:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 288
    return-void
.end method

.method protected final registerForInvalidation()V
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 330
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/DataList;->logd()Lcom/google/android/libraries/bind/logging/Logd;

    move-result-object v4

    const-string v5, "registerForInvalidation"

    new-array v6, v2, [Ljava/lang/Object;

    invoke-virtual {v4, v5, v6}, Lcom/google/android/libraries/bind/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 331
    iget-boolean v4, p0, Lcom/google/android/libraries/bind/data/DataList;->registeredForInvalidation:Z

    if-nez v4, :cond_0

    move v2, v3

    :cond_0
    invoke-static {v2}, Lcom/google/android/libraries/bind/util/Util;->checkPrecondition(Z)V

    .line 332
    iput-boolean v3, p0, Lcom/google/android/libraries/bind/data/DataList;->registeredForInvalidation:Z

    .line 333
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/DataList;->onRegisterForInvalidation()V

    .line 334
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/DataList;->isDirty()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-boolean v2, p0, Lcom/google/android/libraries/bind/data/DataList;->isAutoRefreshing:Z

    if-nez v2, :cond_1

    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/DataList;->hasRefreshedOnce()Z

    move-result v2

    if-nez v2, :cond_2

    .line 335
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/DataList;->refresh()V

    .line 337
    :cond_2
    iget-object v2, p0, Lcom/google/android/libraries/bind/data/DataList;->dataListListeners:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/bind/data/DataList$DataListListener;

    .line 338
    .local v0, "dataListListener":Lcom/google/android/libraries/bind/data/DataList$DataListListener;
    invoke-interface {v0}, Lcom/google/android/libraries/bind/data/DataList$DataListListener;->onDataListRegisteredForInvalidation()V

    goto :goto_0

    .line 340
    .end local v0    # "dataListListener":Lcom/google/android/libraries/bind/data/DataList$DataListListener;
    :cond_3
    return-void
.end method

.method protected setDirty(Z)V
    .locals 0
    .param p1, "dirty"    # Z

    .prologue
    .line 326
    iput-boolean p1, p0, Lcom/google/android/libraries/bind/data/DataList;->dataDirty:Z

    .line 327
    return-void
.end method

.method public startAutoRefresh()Lcom/google/android/libraries/bind/data/DataList;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 478
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/DataList;->logd()Lcom/google/android/libraries/bind/logging/Logd;

    move-result-object v0

    const-string v1, "startAutoRefresh"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 479
    iput-boolean v3, p0, Lcom/google/android/libraries/bind/data/DataList;->stopAutoRefreshAfterRefresh:Z

    .line 480
    iget-boolean v0, p0, Lcom/google/android/libraries/bind/data/DataList;->isAutoRefreshing:Z

    if-nez v0, :cond_0

    .line 481
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/bind/data/DataList;->isAutoRefreshing:Z

    .line 482
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/DataList;->isDirty()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/DataList;->hasDataSetObservers()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 483
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/DataList;->refresh()V

    .line 486
    :cond_0
    return-object p0
.end method

.method public stopAutoRefresh()Lcom/google/android/libraries/bind/data/DataList;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 467
    invoke-static {}, Lcom/google/android/libraries/bind/async/AsyncUtil;->checkMainThread()V

    .line 468
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/DataList;->logd()Lcom/google/android/libraries/bind/logging/Logd;

    move-result-object v0

    const-string v1, "stopAutoRefresh"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 469
    iput-boolean v3, p0, Lcom/google/android/libraries/bind/data/DataList;->isAutoRefreshing:Z

    .line 470
    return-object p0
.end method

.method protected stopRefreshTask()V
    .locals 3

    .prologue
    .line 541
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/DataList;->logd()Lcom/google/android/libraries/bind/logging/Logd;

    move-result-object v0

    const-string v1, "stopRefreshTask"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 542
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/DataList;->currentRefreshTask:Lcom/google/android/libraries/bind/data/RefreshTask;

    if-eqz v0, :cond_0

    .line 543
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/DataList;->currentRefreshTask:Lcom/google/android/libraries/bind/data/RefreshTask;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/RefreshTask;->cancel()V

    .line 544
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/libraries/bind/data/DataList;->currentRefreshTask:Lcom/google/android/libraries/bind/data/RefreshTask;

    .line 546
    :cond_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 81
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "%s - primaryKey: %s, size: %d, exception: %s"

    const/4 v0, 0x4

    new-array v3, v0, [Ljava/lang/Object;

    const/4 v0, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v0, 0x1

    iget v4, p0, Lcom/google/android/libraries/bind/data/DataList;->primaryKey:I

    invoke-static {v4}, Lcom/google/android/libraries/bind/data/Data;->keyName(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/DataList;->getCount()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v4, 0x3

    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/DataList;->didLastRefreshFail()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/bind/data/DataList;->snapshot:Lcom/google/android/libraries/bind/data/Snapshot;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/Snapshot;->getException()Lcom/google/android/libraries/bind/data/DataException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/DataException;->getMessage()Ljava/lang/String;

    move-result-object v0

    :goto_0
    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, "no"

    goto :goto_0
.end method

.method public unregisterDataObserver(Lcom/google/android/libraries/bind/data/DataObserver;)V
    .locals 5
    .param p1, "observer"    # Lcom/google/android/libraries/bind/data/DataObserver;

    .prologue
    .line 294
    invoke-static {}, Lcom/google/android/libraries/bind/async/AsyncUtil;->checkMainThread()V

    .line 295
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/DataList;->observable:Lcom/google/android/libraries/bind/data/PriorityDataObservable;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/bind/data/PriorityDataObservable;->remove(Lcom/google/android/libraries/bind/data/DataObserver;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 296
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/DataList;->unregisterForInvalidation()V

    .line 297
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/DataList;->stopRefreshTask()V

    .line 299
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/DataList;->logd()Lcom/google/android/libraries/bind/logging/Logd;

    move-result-object v0

    const-string v1, "unregisterDataSetObserver - count: %d, registeredForInvalidation: %b"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/libraries/bind/data/DataList;->observable:Lcom/google/android/libraries/bind/data/PriorityDataObservable;

    invoke-virtual {v4}, Lcom/google/android/libraries/bind/data/PriorityDataObservable;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-boolean v4, p0, Lcom/google/android/libraries/bind/data/DataList;->registeredForInvalidation:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 301
    return-void
.end method

.method protected final unregisterForInvalidation()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 343
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/DataList;->logd()Lcom/google/android/libraries/bind/logging/Logd;

    move-result-object v2

    const-string v3, "unregisterForInvalidation"

    new-array v4, v5, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/bind/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 344
    iget-boolean v2, p0, Lcom/google/android/libraries/bind/data/DataList;->registeredForInvalidation:Z

    invoke-static {v2}, Lcom/google/android/libraries/bind/util/Util;->checkPrecondition(Z)V

    .line 345
    iput-boolean v5, p0, Lcom/google/android/libraries/bind/data/DataList;->registeredForInvalidation:Z

    .line 346
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/DataList;->onUnregisterForInvalidation()V

    .line 347
    iget-object v2, p0, Lcom/google/android/libraries/bind/data/DataList;->dataListListeners:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/bind/data/DataList$DataListListener;

    .line 348
    .local v0, "dataListListener":Lcom/google/android/libraries/bind/data/DataList$DataListListener;
    invoke-interface {v0}, Lcom/google/android/libraries/bind/data/DataList$DataListListener;->onDataListUnregisteredForInvalidation()V

    goto :goto_0

    .line 350
    .end local v0    # "dataListListener":Lcom/google/android/libraries/bind/data/DataList$DataListListener;
    :cond_0
    return-void
.end method

.method public update(Lcom/google/android/libraries/bind/data/Snapshot;Lcom/google/android/libraries/bind/data/DataChange;)V
    .locals 1
    .param p1, "newSnapshot"    # Lcom/google/android/libraries/bind/data/Snapshot;
    .param p2, "change"    # Lcom/google/android/libraries/bind/data/DataChange;

    .prologue
    .line 383
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/libraries/bind/data/DataList;->update(Lcom/google/android/libraries/bind/data/Snapshot;Lcom/google/android/libraries/bind/data/DataChange;Ljava/lang/Integer;)V

    .line 384
    return-void
.end method

.method protected update(Lcom/google/android/libraries/bind/data/Snapshot;Lcom/google/android/libraries/bind/data/DataChange;Ljava/lang/Integer;)V
    .locals 5
    .param p1, "newSnapshot"    # Lcom/google/android/libraries/bind/data/Snapshot;
    .param p2, "change"    # Lcom/google/android/libraries/bind/data/DataChange;
    .param p3, "optNewDataVersion"    # Ljava/lang/Integer;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 388
    invoke-static {}, Lcom/google/android/libraries/bind/async/AsyncUtil;->checkMainThread()V

    .line 389
    if-nez p1, :cond_0

    .line 390
    iget-object p1, p0, Lcom/google/android/libraries/bind/data/DataList;->invalidSnapshot:Lcom/google/android/libraries/bind/data/Snapshot;

    .line 392
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/libraries/bind/data/Snapshot;->hasException()Z

    move-result v2

    if-nez v2, :cond_3

    move v2, v3

    :goto_0
    invoke-virtual {p0, v2}, Lcom/google/android/libraries/bind/data/DataList;->setDirty(Z)V

    .line 393
    iget-object v2, p0, Lcom/google/android/libraries/bind/data/DataList;->invalidSnapshot:Lcom/google/android/libraries/bind/data/Snapshot;

    if-ne p1, v2, :cond_4

    move v1, v3

    .line 394
    .local v1, "newSnapshotInvalid":Z
    :goto_1
    iget-object v2, p0, Lcom/google/android/libraries/bind/data/DataList;->snapshot:Lcom/google/android/libraries/bind/data/Snapshot;

    invoke-virtual {v2}, Lcom/google/android/libraries/bind/data/Snapshot;->hasException()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {p1}, Lcom/google/android/libraries/bind/data/Snapshot;->hasException()Z

    move-result v2

    if-eqz v2, :cond_5

    :cond_1
    move v0, v3

    .line 395
    .local v0, "exceptionTransition":Z
    :goto_2
    if-eqz v1, :cond_6

    iget-object v2, p0, Lcom/google/android/libraries/bind/data/DataList;->snapshot:Lcom/google/android/libraries/bind/data/Snapshot;

    iget-object v3, p0, Lcom/google/android/libraries/bind/data/DataList;->invalidSnapshot:Lcom/google/android/libraries/bind/data/Snapshot;

    if-ne v2, v3, :cond_6

    if-nez v0, :cond_6

    .line 405
    :cond_2
    :goto_3
    return-void

    .end local v0    # "exceptionTransition":Z
    .end local v1    # "newSnapshotInvalid":Z
    :cond_3
    move v2, v4

    .line 392
    goto :goto_0

    :cond_4
    move v1, v4

    .line 393
    goto :goto_1

    .restart local v1    # "newSnapshotInvalid":Z
    :cond_5
    move v0, v4

    .line 394
    goto :goto_2

    .line 399
    .restart local v0    # "exceptionTransition":Z
    :cond_6
    iput-object p1, p0, Lcom/google/android/libraries/bind/data/DataList;->snapshot:Lcom/google/android/libraries/bind/data/Snapshot;

    .line 400
    if-nez p3, :cond_7

    iget v2, p0, Lcom/google/android/libraries/bind/data/DataList;->dataVersion:I

    add-int/lit8 v2, v2, 0x1

    :goto_4
    iput v2, p0, Lcom/google/android/libraries/bind/data/DataList;->dataVersion:I

    .line 401
    invoke-virtual {p0, p2}, Lcom/google/android/libraries/bind/data/DataList;->notifyDataChanged(Lcom/google/android/libraries/bind/data/DataChange;)V

    .line 402
    if-nez v1, :cond_2

    iget-boolean v2, p0, Lcom/google/android/libraries/bind/data/DataList;->stopAutoRefreshAfterRefresh:Z

    if-eqz v2, :cond_2

    .line 403
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/DataList;->stopAutoRefresh()Lcom/google/android/libraries/bind/data/DataList;

    goto :goto_3

    .line 400
    :cond_7
    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result v2

    goto :goto_4
.end method
