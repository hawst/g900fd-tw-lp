.class public Lcom/google/android/libraries/bind/widget/BindingRelativeLayout;
.super Lcom/google/android/libraries/bind/widget/BoundRelativeLayout;
.source "BindingRelativeLayout.java"

# interfaces
.implements Lcom/google/android/libraries/bind/data/BindingViewGroup;


# instance fields
.field protected final bindingViewGroupHelper:Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 25
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/libraries/bind/widget/BindingRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 29
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/libraries/bind/widget/BindingRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 33
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/libraries/bind/widget/BoundRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 34
    new-instance v1, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;

    invoke-direct {v1, p0}, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;-><init>(Lcom/google/android/libraries/bind/data/DataView;)V

    iput-object v1, p0, Lcom/google/android/libraries/bind/widget/BindingRelativeLayout;->bindingViewGroupHelper:Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;

    .line 35
    sget-object v1, Lcom/google/android/libraries/bind/R$styleable;->BindingRelativeLayout:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 36
    .local v0, "a":Landroid/content/res/TypedArray;
    iget-object v1, p0, Lcom/google/android/libraries/bind/widget/BindingRelativeLayout;->bindingViewGroupHelper:Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;

    sget v2, Lcom/google/android/libraries/bind/R$styleable;->BindingRelativeLayout_bind__isOwnedByParent:I

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->setOwnedByParent(Z)V

    .line 38
    iget-object v1, p0, Lcom/google/android/libraries/bind/widget/BindingRelativeLayout;->bindingViewGroupHelper:Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;

    sget v2, Lcom/google/android/libraries/bind/R$styleable;->BindingRelativeLayout_bind__supportsAnimationCapture:I

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->setSupportsAnimationCapture(Z)V

    .line 40
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 41
    return-void
.end method


# virtual methods
.method public blendCapturedBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;JLcom/google/android/libraries/bind/data/BindingViewGroup$BlendMode;)V
    .locals 7
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "bitmapRect"    # Landroid/graphics/Rect;
    .param p3, "animationDuration"    # J
    .param p5, "blendMode"    # Lcom/google/android/libraries/bind/data/BindingViewGroup$BlendMode;

    .prologue
    .line 143
    iget-object v1, p0, Lcom/google/android/libraries/bind/widget/BindingRelativeLayout;->bindingViewGroupHelper:Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move-object v6, p5

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->blendCapturedBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;JLcom/google/android/libraries/bind/data/BindingViewGroup$BlendMode;)V

    .line 144
    return-void
.end method

.method public captureToBitmap(Landroid/graphics/Bitmap;FF)Z
    .locals 1
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "left"    # F
    .param p3, "top"    # F

    .prologue
    .line 148
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingRelativeLayout;->bindingViewGroupHelper:Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->captureToBitmap(Landroid/graphics/Bitmap;FF)Z

    move-result v0

    return v0
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 1
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 127
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingRelativeLayout;->bindingViewGroupHelper:Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->draw(Landroid/graphics/Canvas;)V

    .line 128
    return-void
.end method

.method public getDataRow()Lcom/google/android/libraries/bind/data/DataList;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingRelativeLayout;->bindingViewGroupHelper:Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->getDataRow()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v0

    return-object v0
.end method

.method public isOwnedByParent()Z
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingRelativeLayout;->bindingViewGroupHelper:Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->isOwnedByParent()Z

    move-result v0

    return v0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 104
    invoke-super {p0}, Lcom/google/android/libraries/bind/widget/BoundRelativeLayout;->onAttachedToWindow()V

    .line 105
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingRelativeLayout;->bindingViewGroupHelper:Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->onAttachedToWindow()V

    .line 106
    return-void
.end method

.method public onDataUpdated(Lcom/google/android/libraries/bind/data/Data;)V
    .locals 1
    .param p1, "data"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    .line 122
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingRelativeLayout;->bindingViewGroupHelper:Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->onDataUpdated(Lcom/google/android/libraries/bind/data/Data;)V

    .line 123
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 98
    invoke-super {p0}, Lcom/google/android/libraries/bind/widget/BoundRelativeLayout;->onDetachedFromWindow()V

    .line 99
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingRelativeLayout;->bindingViewGroupHelper:Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->onDetachedFromWindow()V

    .line 100
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 92
    invoke-super {p0}, Lcom/google/android/libraries/bind/widget/BoundRelativeLayout;->onFinishInflate()V

    .line 93
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingRelativeLayout;->bindingViewGroupHelper:Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->onFinishInflate()V

    .line 94
    return-void
.end method

.method public onFinishTemporaryDetach()V
    .locals 1

    .prologue
    .line 116
    invoke-super {p0}, Lcom/google/android/libraries/bind/widget/BoundRelativeLayout;->onFinishTemporaryDetach()V

    .line 117
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingRelativeLayout;->bindingViewGroupHelper:Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->onFinishTemporaryDetach()V

    .line 118
    return-void
.end method

.method protected onMeasure(II)V
    .locals 1
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 171
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingRelativeLayout;->bindingViewGroupHelper:Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->onMeasure(II)V

    .line 172
    invoke-super {p0, p1, p2}, Lcom/google/android/libraries/bind/widget/BoundRelativeLayout;->onMeasure(II)V

    .line 173
    return-void
.end method

.method public onStartTemporaryDetach()V
    .locals 1

    .prologue
    .line 110
    invoke-super {p0}, Lcom/google/android/libraries/bind/widget/BoundRelativeLayout;->onStartTemporaryDetach()V

    .line 111
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingRelativeLayout;->bindingViewGroupHelper:Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->onStartTemporaryDetach()V

    .line 112
    return-void
.end method

.method public prepareForRecycling()V
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingRelativeLayout;->bindingViewGroupHelper:Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->prepareForRecycling()V

    .line 78
    return-void
.end method

.method public setCardGroup(Lcom/google/android/libraries/bind/card/CardGroup;I)V
    .locals 1
    .param p1, "cardGroup"    # Lcom/google/android/libraries/bind/card/CardGroup;
    .param p2, "cardGroupPosition"    # I

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingRelativeLayout;->bindingViewGroupHelper:Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->setCardGroup(Lcom/google/android/libraries/bind/card/CardGroup;I)V

    .line 83
    return-void
.end method

.method public setDataRow(Lcom/google/android/libraries/bind/data/DataList;)V
    .locals 1
    .param p1, "dataRow"    # Lcom/google/android/libraries/bind/data/DataList;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingRelativeLayout;->bindingViewGroupHelper:Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->setDataRow(Lcom/google/android/libraries/bind/data/DataList;)V

    .line 52
    return-void
.end method

.method public final setMeasuredDimensionProxy(II)V
    .locals 0
    .param p1, "measuredWidth"    # I
    .param p2, "measuredHeight"    # I

    .prologue
    .line 166
    invoke-virtual {p0, p1, p2}, Lcom/google/android/libraries/bind/widget/BindingRelativeLayout;->setMeasuredDimension(II)V

    .line 167
    return-void
.end method

.method public setOwnedByParent(Z)V
    .locals 1
    .param p1, "value"    # Z

    .prologue
    .line 132
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingRelativeLayout;->bindingViewGroupHelper:Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->setOwnedByParent(Z)V

    .line 133
    return-void
.end method

.method public startEditingIfPossible()Z
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingRelativeLayout;->bindingViewGroupHelper:Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->startEditingIfPossible()Z

    move-result v0

    return v0
.end method

.method public final superDrawProxy(Landroid/graphics/Canvas;)V
    .locals 0
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 161
    invoke-super {p0, p1}, Lcom/google/android/libraries/bind/widget/BoundRelativeLayout;->draw(Landroid/graphics/Canvas;)V

    .line 162
    return-void
.end method

.method public final updateBoundDataProxy(Lcom/google/android/libraries/bind/data/Data;)V
    .locals 0
    .param p1, "data"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    .line 156
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/bind/widget/BindingRelativeLayout;->updateBoundData(Lcom/google/android/libraries/bind/data/Data;)V

    .line 157
    return-void
.end method
