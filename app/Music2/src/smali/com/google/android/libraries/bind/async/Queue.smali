.class public Lcom/google/android/libraries/bind/async/Queue;
.super Ljava/lang/Object;
.source "Queue.java"

# interfaces
.implements Ljava/util/concurrent/Executor;


# static fields
.field private static final LOGD:Lcom/google/android/libraries/bind/logging/Logd;

.field protected static final queues:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/async/Queue;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field protected executor:Ljava/util/concurrent/Executor;

.field public final fallbackIfMain:Ljava/util/concurrent/Executor;

.field private final name:Ljava/lang/String;

.field public final poolSize:I

.field protected final threadGroup:Ljava/lang/ThreadGroup;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lcom/google/android/libraries/bind/async/Queues;

    invoke-static {v0}, Lcom/google/android/libraries/bind/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/android/libraries/bind/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/bind/async/Queue;->LOGD:Lcom/google/android/libraries/bind/logging/Logd;

    .line 24
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/google/android/libraries/bind/async/Queue;->queues:Ljava/util/List;

    .line 43
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;IZ)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "poolSize"    # I
    .param p3, "jankLocked"    # Z

    .prologue
    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    new-instance v0, Lcom/google/android/libraries/bind/async/Queue$2;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/bind/async/Queue$2;-><init>(Lcom/google/android/libraries/bind/async/Queue;)V

    iput-object v0, p0, Lcom/google/android/libraries/bind/async/Queue;->fallbackIfMain:Ljava/util/concurrent/Executor;

    .line 70
    iput-object p1, p0, Lcom/google/android/libraries/bind/async/Queue;->name:Ljava/lang/String;

    .line 71
    sget-object v0, Lcom/google/android/libraries/bind/async/Queue;->queues:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 72
    iput p2, p0, Lcom/google/android/libraries/bind/async/Queue;->poolSize:I

    .line 73
    new-instance v0, Ljava/lang/ThreadGroup;

    invoke-direct {v0, p1}, Ljava/lang/ThreadGroup;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/libraries/bind/async/Queue;->threadGroup:Ljava/lang/ThreadGroup;

    .line 74
    invoke-direct {p0, p3}, Lcom/google/android/libraries/bind/async/Queue;->makeExecutor(Z)Ljava/util/concurrent/ThreadPoolExecutor;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/bind/async/Queue;->executor:Ljava/util/concurrent/Executor;

    .line 75
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/concurrent/Executor;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "executor"    # Ljava/util/concurrent/Executor;

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    new-instance v0, Lcom/google/android/libraries/bind/async/Queue$2;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/bind/async/Queue$2;-><init>(Lcom/google/android/libraries/bind/async/Queue;)V

    iput-object v0, p0, Lcom/google/android/libraries/bind/async/Queue;->fallbackIfMain:Ljava/util/concurrent/Executor;

    .line 78
    iput-object p1, p0, Lcom/google/android/libraries/bind/async/Queue;->name:Ljava/lang/String;

    .line 79
    sget-object v0, Lcom/google/android/libraries/bind/async/Queue;->queues:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 80
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/libraries/bind/async/Queue;->poolSize:I

    .line 81
    new-instance v0, Ljava/lang/ThreadGroup;

    invoke-direct {v0, p1}, Ljava/lang/ThreadGroup;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/libraries/bind/async/Queue;->threadGroup:Ljava/lang/ThreadGroup;

    .line 82
    iput-object p2, p0, Lcom/google/android/libraries/bind/async/Queue;->executor:Ljava/util/concurrent/Executor;

    .line 83
    return-void
.end method

.method private makeExecutor(Z)Ljava/util/concurrent/ThreadPoolExecutor;
    .locals 10
    .param p1, "jankLocked"    # Z

    .prologue
    .line 93
    new-instance v8, Lcom/google/android/libraries/bind/async/Queue$3;

    invoke-direct {v8, p0}, Lcom/google/android/libraries/bind/async/Queue$3;-><init>(Lcom/google/android/libraries/bind/async/Queue;)V

    .line 113
    .local v8, "threadFactory":Ljava/util/concurrent/ThreadFactory;
    new-instance v0, Lcom/google/android/libraries/bind/async/Queue$4;

    iget v2, p0, Lcom/google/android/libraries/bind/async/Queue;->poolSize:I

    iget v3, p0, Lcom/google/android/libraries/bind/async/Queue;->poolSize:I

    const-wide/16 v4, 0xa

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v7, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v7}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    move-object v1, p0

    move v9, p1

    invoke-direct/range {v0 .. v9}, Lcom/google/android/libraries/bind/async/Queue$4;-><init>(Lcom/google/android/libraries/bind/async/Queue;IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;Z)V

    return-object v0
.end method


# virtual methods
.method public execute(Ljava/lang/Runnable;)V
    .locals 1
    .param p1, "runnable"    # Ljava/lang/Runnable;

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/android/libraries/bind/async/Queue;->executor:Ljava/util/concurrent/Executor;

    invoke-interface {v0, p1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 88
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/google/android/libraries/bind/async/Queue;->name:Ljava/lang/String;

    return-object v0
.end method
