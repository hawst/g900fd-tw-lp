.class public abstract Lcom/google/android/libraries/bind/data/DataAdapter;
.super Ljava/lang/Object;
.source "DataAdapter.java"

# interfaces
.implements Landroid/widget/ListAdapter;
.implements Landroid/widget/SpinnerAdapter;


# instance fields
.field protected dataList:Lcom/google/android/libraries/bind/data/DataList;

.field private dataListRegistered:Z

.field protected emptyViewProvider:Lcom/google/android/libraries/bind/data/ViewProvider;

.field protected errorViewProvider:Lcom/google/android/libraries/bind/data/ViewProvider;

.field protected loadingViewProvider:Lcom/google/android/libraries/bind/data/ViewProvider;

.field private notifyOnlyIfPrimaryKeyAffected:Z

.field private final observable:Lcom/google/android/libraries/bind/data/PriorityDataSetObservable;

.field private final observer:Lcom/google/android/libraries/bind/data/DataObserver;

.field private supportsEmptyView:Z

.field private supportsErrorView:Z

.field private supportsLoadingView:Z

.field protected final viewHeap:Lcom/google/android/libraries/bind/view/ViewHeap;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/bind/view/ViewHeap;)V
    .locals 2
    .param p1, "viewHeap"    # Lcom/google/android/libraries/bind/view/ViewHeap;

    .prologue
    const/4 v0, 0x1

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    new-instance v1, Lcom/google/android/libraries/bind/data/PriorityDataSetObservable;

    invoke-direct {v1}, Lcom/google/android/libraries/bind/data/PriorityDataSetObservable;-><init>()V

    iput-object v1, p0, Lcom/google/android/libraries/bind/data/DataAdapter;->observable:Lcom/google/android/libraries/bind/data/PriorityDataSetObservable;

    .line 32
    iput-boolean v0, p0, Lcom/google/android/libraries/bind/data/DataAdapter;->notifyOnlyIfPrimaryKeyAffected:Z

    .line 34
    iput-boolean v0, p0, Lcom/google/android/libraries/bind/data/DataAdapter;->supportsLoadingView:Z

    .line 35
    iput-boolean v0, p0, Lcom/google/android/libraries/bind/data/DataAdapter;->supportsEmptyView:Z

    .line 36
    iput-boolean v0, p0, Lcom/google/android/libraries/bind/data/DataAdapter;->supportsErrorView:Z

    .line 37
    sget-object v1, Lcom/google/android/libraries/bind/data/ViewProvider;->DEFAULT_LOADING_VIEW_PROVIDER:Lcom/google/android/libraries/bind/data/ViewProvider;

    iput-object v1, p0, Lcom/google/android/libraries/bind/data/DataAdapter;->loadingViewProvider:Lcom/google/android/libraries/bind/data/ViewProvider;

    .line 38
    sget-object v1, Lcom/google/android/libraries/bind/data/ViewProvider;->DEFAULT_EMPTY_VIEW_PROVIDER:Lcom/google/android/libraries/bind/data/ViewProvider;

    iput-object v1, p0, Lcom/google/android/libraries/bind/data/DataAdapter;->emptyViewProvider:Lcom/google/android/libraries/bind/data/ViewProvider;

    .line 39
    sget-object v1, Lcom/google/android/libraries/bind/data/ViewProvider;->DEFAULT_ERROR_VIEW_PROVIDER:Lcom/google/android/libraries/bind/data/ViewProvider;

    iput-object v1, p0, Lcom/google/android/libraries/bind/data/DataAdapter;->errorViewProvider:Lcom/google/android/libraries/bind/data/ViewProvider;

    .line 42
    if-eqz p1, :cond_0

    :goto_0
    invoke-static {v0}, Lcom/google/android/libraries/bind/util/Util;->checkPrecondition(Z)V

    .line 43
    iput-object p1, p0, Lcom/google/android/libraries/bind/data/DataAdapter;->viewHeap:Lcom/google/android/libraries/bind/view/ViewHeap;

    .line 44
    new-instance v0, Lcom/google/android/libraries/bind/data/DataAdapter$1;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/bind/data/DataAdapter$1;-><init>(Lcom/google/android/libraries/bind/data/DataAdapter;)V

    iput-object v0, p0, Lcom/google/android/libraries/bind/data/DataAdapter;->observer:Lcom/google/android/libraries/bind/data/DataObserver;

    .line 55
    return-void

    .line 42
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/google/android/libraries/bind/data/DataAdapter;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/libraries/bind/data/DataAdapter;

    .prologue
    .line 20
    iget-boolean v0, p0, Lcom/google/android/libraries/bind/data/DataAdapter;->notifyOnlyIfPrimaryKeyAffected:Z

    return v0
.end method

.method private getEmptyView(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 307
    iget-object v1, p0, Lcom/google/android/libraries/bind/data/DataAdapter;->emptyViewProvider:Lcom/google/android/libraries/bind/data/ViewProvider;

    iget-object v2, p0, Lcom/google/android/libraries/bind/data/DataAdapter;->viewHeap:Lcom/google/android/libraries/bind/view/ViewHeap;

    invoke-interface {v1, p1, v2}, Lcom/google/android/libraries/bind/data/ViewProvider;->getView(Landroid/view/ViewGroup;Lcom/google/android/libraries/bind/view/ViewHeap;)Landroid/view/View;

    move-result-object v0

    .line 308
    .local v0, "emptyView":Landroid/view/View;
    invoke-static {p1}, Lcom/google/android/libraries/bind/data/DataAdapter;->getFullScreenLayoutParams(Landroid/view/ViewGroup;)Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 309
    return-object v0
.end method

.method private getErrorView(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2
    .param p1, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 313
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/DataAdapter;->errorViewProvider:Lcom/google/android/libraries/bind/data/ViewProvider;

    iget-object v1, p0, Lcom/google/android/libraries/bind/data/DataAdapter;->viewHeap:Lcom/google/android/libraries/bind/view/ViewHeap;

    invoke-interface {v0, p1, v1}, Lcom/google/android/libraries/bind/data/ViewProvider;->getView(Landroid/view/ViewGroup;Lcom/google/android/libraries/bind/view/ViewHeap;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public static getFullScreenLayoutParams(Landroid/view/ViewGroup;)Landroid/view/ViewGroup$LayoutParams;
    .locals 5
    .param p0, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 317
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getPaddingLeft()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getPaddingRight()I

    move-result v3

    sub-int v1, v2, v3

    .line 318
    .local v1, "width":I
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getHeight()I

    move-result v2

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getPaddingTop()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getPaddingBottom()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/google/android/libraries/bind/R$dimen;->bind__card_list_view_padding:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    mul-int/lit8 v3, v3, 0x2

    sub-int v0, v2, v3

    .line 320
    .local v0, "height":I
    new-instance v2, Landroid/widget/AbsListView$LayoutParams;

    invoke-direct {v2, v1, v0}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    return-object v2
.end method

.method public static objectToLong(Ljava/lang/Object;)J
    .locals 2
    .param p0, "value"    # Ljava/lang/Object;

    .prologue
    .line 59
    instance-of v0, p0, Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 60
    check-cast p0, Ljava/lang/Long;

    .end local p0    # "value":Ljava/lang/Object;
    invoke-virtual {p0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 66
    .restart local p0    # "value":Ljava/lang/Object;
    :goto_0
    return-wide v0

    .line 61
    :cond_0
    instance-of v0, p0, Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 62
    check-cast p0, Ljava/lang/Integer;

    .end local p0    # "value":Ljava/lang/Object;
    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v0, v0

    goto :goto_0

    .line 63
    .restart local p0    # "value":Ljava/lang/Object;
    :cond_1
    if-eqz p0, :cond_2

    .line 64
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/libraries/bind/util/StringUtil;->getLongHash(Ljava/lang/String;)J

    move-result-wide v0

    goto :goto_0

    .line 66
    :cond_2
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method private updateDataListRegistration()V
    .locals 2

    .prologue
    .line 110
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/DataAdapter;->dataList:Lcom/google/android/libraries/bind/data/DataList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/bind/data/DataAdapter;->observable:Lcom/google/android/libraries/bind/data/PriorityDataSetObservable;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/PriorityDataSetObservable;->size()I

    move-result v0

    if-nez v0, :cond_2

    .line 112
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/libraries/bind/data/DataAdapter;->dataListRegistered:Z

    if-eqz v0, :cond_1

    .line 113
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/DataAdapter;->dataList:Lcom/google/android/libraries/bind/data/DataList;

    iget-object v1, p0, Lcom/google/android/libraries/bind/data/DataAdapter;->observer:Lcom/google/android/libraries/bind/data/DataObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/data/DataList;->unregisterDataObserver(Lcom/google/android/libraries/bind/data/DataObserver;)V

    .line 114
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/bind/data/DataAdapter;->dataListRegistered:Z

    .line 123
    :cond_1
    :goto_0
    return-void

    .line 118
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/libraries/bind/data/DataAdapter;->dataListRegistered:Z

    if-nez v0, :cond_1

    .line 119
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/DataAdapter;->dataList:Lcom/google/android/libraries/bind/data/DataList;

    iget-object v1, p0, Lcom/google/android/libraries/bind/data/DataAdapter;->observer:Lcom/google/android/libraries/bind/data/DataObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/data/DataList;->registerDataObserver(Lcom/google/android/libraries/bind/data/DataObserver;)V

    .line 120
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/bind/data/DataAdapter;->dataListRegistered:Z

    goto :goto_0
.end method


# virtual methods
.method public areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 273
    const/4 v0, 0x1

    return v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 204
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/DataAdapter;->showOutOfBandView()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/DataAdapter;->dataList:Lcom/google/android/libraries/bind/data/DataList;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/DataAdapter;->dataList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/DataList;->getCount()I

    move-result v0

    goto :goto_0
.end method

.method public getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 238
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/libraries/bind/data/DataAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getItem(I)Lcom/google/android/libraries/bind/data/Data;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 209
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/DataAdapter;->showOutOfBandView()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/DataAdapter;->dataList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/bind/data/DataList;->getData(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    goto :goto_0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 20
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/bind/data/DataAdapter;->getItem(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 214
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/DataAdapter;->showLoadingView()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 215
    const-wide v0, 0x7fffffffffffffffL

    .line 223
    :goto_0
    return-wide v0

    .line 217
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/DataAdapter;->showEmptyView()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 218
    const-wide v0, 0x7ffffffffffffffeL

    goto :goto_0

    .line 220
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/DataAdapter;->showErrorView()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 221
    const-wide v0, 0x7ffffffffffffffdL

    goto :goto_0

    .line 223
    :cond_2
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/DataAdapter;->dataList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/bind/data/DataList;->getItemId(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/libraries/bind/data/DataAdapter;->objectToLong(Ljava/lang/Object;)J

    move-result-wide v0

    goto :goto_0
.end method

.method public getItemViewType(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 228
    const/4 v0, 0x0

    return v0
.end method

.method protected getLoadingView(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 301
    iget-object v1, p0, Lcom/google/android/libraries/bind/data/DataAdapter;->loadingViewProvider:Lcom/google/android/libraries/bind/data/ViewProvider;

    iget-object v2, p0, Lcom/google/android/libraries/bind/data/DataAdapter;->viewHeap:Lcom/google/android/libraries/bind/view/ViewHeap;

    invoke-interface {v1, p1, v2}, Lcom/google/android/libraries/bind/data/ViewProvider;->getView(Landroid/view/ViewGroup;Lcom/google/android/libraries/bind/view/ViewHeap;)Landroid/view/View;

    move-result-object v0

    .line 302
    .local v0, "loadingView":Landroid/view/View;
    invoke-static {p1}, Lcom/google/android/libraries/bind/data/DataAdapter;->getFullScreenLayoutParams(Landroid/view/ViewGroup;)Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 303
    return-object v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 283
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/DataAdapter;->showLoadingView()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 284
    invoke-virtual {p0, p3}, Lcom/google/android/libraries/bind/data/DataAdapter;->getLoadingView(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 297
    :goto_0
    return-object v0

    .line 286
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/DataAdapter;->showEmptyView()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 287
    invoke-direct {p0, p3}, Lcom/google/android/libraries/bind/data/DataAdapter;->getEmptyView(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 289
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/DataAdapter;->showErrorView()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 290
    invoke-direct {p0, p3}, Lcom/google/android/libraries/bind/data/DataAdapter;->getErrorView(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 292
    :cond_2
    iget-object v1, p0, Lcom/google/android/libraries/bind/data/DataAdapter;->dataList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v1, p1}, Lcom/google/android/libraries/bind/data/DataList;->isInvalidPosition(I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 293
    const/4 v0, 0x0

    goto :goto_0

    .line 295
    :cond_3
    iget-object v1, p0, Lcom/google/android/libraries/bind/data/DataAdapter;->dataList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v1, p1}, Lcom/google/android/libraries/bind/data/DataList;->getData(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v1

    invoke-virtual {p0, p1, p2, p3, v1}, Lcom/google/android/libraries/bind/data/DataAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;Lcom/google/android/libraries/bind/data/Data;)Landroid/view/View;

    move-result-object v0

    .line 296
    .local v0, "view":Landroid/view/View;
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/bind/data/DataAdapter;->validateLayoutParams(Landroid/view/View;)V

    goto :goto_0
.end method

.method public abstract getView(ILandroid/view/View;Landroid/view/ViewGroup;Lcom/google/android/libraries/bind/data/Data;)Landroid/view/View;
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 233
    const/4 v0, 0x1

    return v0
.end method

.method public hasRefreshedOnce()Z
    .locals 1

    .prologue
    .line 252
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/DataAdapter;->dataList:Lcom/google/android/libraries/bind/data/DataList;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/DataAdapter;->dataList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/DataList;->hasRefreshedOnce()Z

    move-result v0

    goto :goto_0
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 243
    const/4 v0, 0x1

    return v0
.end method

.method public isEmpty()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 248
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/DataAdapter;->showOutOfBandView()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/libraries/bind/data/DataAdapter;->dataList:Lcom/google/android/libraries/bind/data/DataList;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/libraries/bind/data/DataAdapter;->dataList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v1}, Lcom/google/android/libraries/bind/data/DataList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isEnabled(I)Z
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 278
    const/4 v0, 0x1

    return v0
.end method

.method protected isLoading()Z
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/DataAdapter;->dataList:Lcom/google/android/libraries/bind/data/DataList;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/DataAdapter;->hasRefreshedOnce()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/DataAdapter;->showErrorView()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public makeLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 3

    .prologue
    .line 93
    new-instance v0, Landroid/widget/AbsListView$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    return-object v0
.end method

.method protected notifyOnChanged()V
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/DataAdapter;->observable:Lcom/google/android/libraries/bind/data/PriorityDataSetObservable;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/PriorityDataSetObservable;->notifyChanged()V

    .line 127
    return-void
.end method

.method protected notifyOnInvalidated()V
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/DataAdapter;->observable:Lcom/google/android/libraries/bind/data/PriorityDataSetObservable;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/PriorityDataSetObservable;->notifyInvalidated()V

    .line 131
    return-void
.end method

.method public registerDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1
    .param p1, "observer"    # Landroid/database/DataSetObserver;

    .prologue
    .line 262
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/libraries/bind/data/DataAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;I)V

    .line 263
    return-void
.end method

.method public registerDataSetObserver(Landroid/database/DataSetObserver;I)V
    .locals 1
    .param p1, "observer"    # Landroid/database/DataSetObserver;
    .param p2, "priority"    # I

    .prologue
    .line 256
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/DataAdapter;->observable:Lcom/google/android/libraries/bind/data/PriorityDataSetObservable;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/libraries/bind/data/PriorityDataSetObservable;->add(Landroid/database/DataSetObserver;I)Z

    .line 257
    invoke-direct {p0}, Lcom/google/android/libraries/bind/data/DataAdapter;->updateDataListRegistration()V

    .line 258
    return-void
.end method

.method public setDataList(Lcom/google/android/libraries/bind/data/DataList;)Lcom/google/android/libraries/bind/data/DataAdapter;
    .locals 2
    .param p1, "dataList"    # Lcom/google/android/libraries/bind/data/DataList;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/DataAdapter;->dataList:Lcom/google/android/libraries/bind/data/DataList;

    if-ne p1, v0, :cond_0

    .line 85
    :goto_0
    return-object p0

    .line 74
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/DataAdapter;->dataList:Lcom/google/android/libraries/bind/data/DataList;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/libraries/bind/data/DataAdapter;->dataListRegistered:Z

    if-eqz v0, :cond_1

    .line 75
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/DataAdapter;->dataList:Lcom/google/android/libraries/bind/data/DataList;

    iget-object v1, p0, Lcom/google/android/libraries/bind/data/DataAdapter;->observer:Lcom/google/android/libraries/bind/data/DataObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/data/DataList;->unregisterDataObserver(Lcom/google/android/libraries/bind/data/DataObserver;)V

    .line 76
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/bind/data/DataAdapter;->dataListRegistered:Z

    .line 78
    :cond_1
    iput-object p1, p0, Lcom/google/android/libraries/bind/data/DataAdapter;->dataList:Lcom/google/android/libraries/bind/data/DataList;

    .line 79
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/google/android/libraries/bind/data/DataList;->hasRefreshedOnce()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 80
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/DataAdapter;->notifyOnChanged()V

    .line 84
    :goto_1
    invoke-direct {p0}, Lcom/google/android/libraries/bind/data/DataAdapter;->updateDataListRegistration()V

    goto :goto_0

    .line 82
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/DataAdapter;->notifyOnInvalidated()V

    goto :goto_1
.end method

.method protected setNotifyOnlyIfPrimaryKeyAffected(Z)V
    .locals 0
    .param p1, "notifyOnlyIfPrimaryKeyAffected"    # Z

    .prologue
    .line 97
    iput-boolean p1, p0, Lcom/google/android/libraries/bind/data/DataAdapter;->notifyOnlyIfPrimaryKeyAffected:Z

    .line 98
    return-void
.end method

.method protected showEmptyView()Z
    .locals 1

    .prologue
    .line 176
    iget-boolean v0, p0, Lcom/google/android/libraries/bind/data/DataAdapter;->supportsEmptyView:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/libraries/bind/data/DataAdapter;->dataList:Lcom/google/android/libraries/bind/data/DataList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/bind/data/DataAdapter;->dataList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/DataList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/DataAdapter;->showLoadingView()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/DataAdapter;->showErrorView()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected showErrorView()Z
    .locals 1

    .prologue
    .line 181
    iget-boolean v0, p0, Lcom/google/android/libraries/bind/data/DataAdapter;->supportsErrorView:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/bind/data/DataAdapter;->dataList:Lcom/google/android/libraries/bind/data/DataList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/bind/data/DataAdapter;->dataList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/DataList;->didLastRefreshFail()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected showLoadingView()Z
    .locals 1

    .prologue
    .line 172
    iget-boolean v0, p0, Lcom/google/android/libraries/bind/data/DataAdapter;->supportsLoadingView:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/DataAdapter;->isLoading()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected showOutOfBandView()Z
    .locals 1

    .prologue
    .line 189
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/DataAdapter;->showLoadingView()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/DataAdapter;->showEmptyView()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/DataAdapter;->showErrorView()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public unregisterDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1
    .param p1, "observer"    # Landroid/database/DataSetObserver;

    .prologue
    .line 267
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/DataAdapter;->observable:Lcom/google/android/libraries/bind/data/PriorityDataSetObservable;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/bind/data/PriorityDataSetObservable;->remove(Landroid/database/DataSetObserver;)Z

    .line 268
    invoke-direct {p0}, Lcom/google/android/libraries/bind/data/DataAdapter;->updateDataListRegistration()V

    .line 269
    return-void
.end method

.method protected validateLayoutParams(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 101
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 103
    .local v0, "layoutParams":Landroid/view/ViewGroup$LayoutParams;
    if-eqz v0, :cond_0

    instance-of v2, v0, Landroid/widget/AbsListView$LayoutParams;

    if-nez v2, :cond_0

    .line 104
    new-instance v1, Landroid/widget/AbsListView$LayoutParams;

    invoke-direct {v1, v0}, Landroid/widget/AbsListView$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 105
    .end local v0    # "layoutParams":Landroid/view/ViewGroup$LayoutParams;
    .local v1, "layoutParams":Landroid/view/ViewGroup$LayoutParams;
    invoke-virtual {p1, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    move-object v0, v1

    .line 107
    .end local v1    # "layoutParams":Landroid/view/ViewGroup$LayoutParams;
    .restart local v0    # "layoutParams":Landroid/view/ViewGroup$LayoutParams;
    :cond_0
    return-void
.end method
