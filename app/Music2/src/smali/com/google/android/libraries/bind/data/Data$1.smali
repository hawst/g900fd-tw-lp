.class final Lcom/google/android/libraries/bind/data/Data$1;
.super Ljava/lang/Object;
.source "Data.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/libraries/bind/data/Data;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/android/libraries/bind/data/Data;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 306
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/google/android/libraries/bind/data/Data;
    .locals 2
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 310
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readSparseArray(Ljava/lang/ClassLoader;)Landroid/util/SparseArray;

    move-result-object v0

    .line 311
    .local v0, "values":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/Object;>;"
    new-instance v1, Lcom/google/android/libraries/bind/data/Data;

    invoke-direct {v1, v0}, Lcom/google/android/libraries/bind/data/Data;-><init>(Landroid/util/SparseArray;)V

    return-object v1
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Landroid/os/Parcel;

    .prologue
    .line 306
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/bind/data/Data$1;->createFromParcel(Landroid/os/Parcel;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/google/android/libraries/bind/data/Data;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 316
    new-array v0, p1, [Lcom/google/android/libraries/bind/data/Data;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 306
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/bind/data/Data$1;->newArray(I)[Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    return-object v0
.end method
