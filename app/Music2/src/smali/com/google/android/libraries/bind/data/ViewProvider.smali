.class public interface abstract Lcom/google/android/libraries/bind/data/ViewProvider;
.super Ljava/lang/Object;
.source "ViewProvider.java"


# static fields
.field public static final DEFAULT_EMPTY_VIEW_PROVIDER:Lcom/google/android/libraries/bind/data/ViewProvider;

.field public static final DEFAULT_ERROR_VIEW_PROVIDER:Lcom/google/android/libraries/bind/data/ViewProvider;

.field public static final DEFAULT_LOADING_VIEW_PROVIDER:Lcom/google/android/libraries/bind/data/ViewProvider;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    new-instance v0, Lcom/google/android/libraries/bind/data/ViewProvider$1;

    invoke-direct {v0}, Lcom/google/android/libraries/bind/data/ViewProvider$1;-><init>()V

    sput-object v0, Lcom/google/android/libraries/bind/data/ViewProvider;->DEFAULT_EMPTY_VIEW_PROVIDER:Lcom/google/android/libraries/bind/data/ViewProvider;

    .line 22
    new-instance v0, Lcom/google/android/libraries/bind/data/ViewProvider$2;

    invoke-direct {v0}, Lcom/google/android/libraries/bind/data/ViewProvider$2;-><init>()V

    sput-object v0, Lcom/google/android/libraries/bind/data/ViewProvider;->DEFAULT_LOADING_VIEW_PROVIDER:Lcom/google/android/libraries/bind/data/ViewProvider;

    .line 29
    new-instance v0, Lcom/google/android/libraries/bind/data/ViewProvider$3;

    invoke-direct {v0}, Lcom/google/android/libraries/bind/data/ViewProvider$3;-><init>()V

    sput-object v0, Lcom/google/android/libraries/bind/data/ViewProvider;->DEFAULT_ERROR_VIEW_PROVIDER:Lcom/google/android/libraries/bind/data/ViewProvider;

    return-void
.end method


# virtual methods
.method public abstract getView(Landroid/view/ViewGroup;Lcom/google/android/libraries/bind/view/ViewHeap;)Landroid/view/View;
.end method
