.class public Lcom/google/android/libraries/bind/bidi/BidiPagingHelper;
.super Ljava/lang/Object;
.source "BidiPagingHelper.java"


# direct methods
.method public static getLogicalPosition(Landroid/support/v4/view/PagerAdapter;I)I
    .locals 1
    .param p0, "pagerAdapter"    # Landroid/support/v4/view/PagerAdapter;
    .param p1, "visualPosition"    # I

    .prologue
    .line 27
    instance-of v0, p0, Lcom/google/android/libraries/bind/bidi/BidiAwarePagerAdapter;

    if-eqz v0, :cond_0

    move-object v0, p0

    check-cast v0, Lcom/google/android/libraries/bind/bidi/BidiAwarePagerAdapter;

    invoke-interface {v0}, Lcom/google/android/libraries/bind/bidi/BidiAwarePagerAdapter;->isRtl()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 29
    invoke-static {p0, p1}, Lcom/google/android/libraries/bind/bidi/BidiPagingHelper;->swapPosition(Landroid/support/v4/view/PagerAdapter;I)I

    move-result p1

    .line 31
    .end local p1    # "visualPosition":I
    :cond_0
    return p1
.end method

.method public static getVisualPosition(Landroid/support/v4/view/PagerAdapter;I)I
    .locals 1
    .param p0, "pagerAdapter"    # Landroid/support/v4/view/PagerAdapter;
    .param p1, "logicalPosition"    # I

    .prologue
    .line 35
    instance-of v0, p0, Lcom/google/android/libraries/bind/bidi/BidiAwarePagerAdapter;

    if-eqz v0, :cond_0

    move-object v0, p0

    check-cast v0, Lcom/google/android/libraries/bind/bidi/BidiAwarePagerAdapter;

    invoke-interface {v0}, Lcom/google/android/libraries/bind/bidi/BidiAwarePagerAdapter;->isRtl()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 37
    invoke-static {p0, p1}, Lcom/google/android/libraries/bind/bidi/BidiPagingHelper;->swapPosition(Landroid/support/v4/view/PagerAdapter;I)I

    move-result p1

    .line 39
    .end local p1    # "logicalPosition":I
    :cond_0
    return p1
.end method

.method protected static swapPosition(Landroid/support/v4/view/PagerAdapter;I)I
    .locals 1
    .param p0, "pagerAdapter"    # Landroid/support/v4/view/PagerAdapter;
    .param p1, "initialPosition"    # I

    .prologue
    .line 19
    if-gez p1, :cond_0

    .end local p1    # "initialPosition":I
    :goto_0
    return p1

    .restart local p1    # "initialPosition":I
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/view/PagerAdapter;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    sub-int p1, v0, p1

    goto :goto_0
.end method
