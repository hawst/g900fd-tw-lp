.class public final Lcom/google/android/libraries/bind/data/Data;
.super Ljava/lang/Object;
.source "Data.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private frozen:Z

.field private values:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 306
    new-instance v0, Lcom/google/android/libraries/bind/data/Data$1;

    invoke-direct {v0}, Lcom/google/android/libraries/bind/data/Data$1;-><init>()V

    sput-object v0, Lcom/google/android/libraries/bind/data/Data;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 61
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/google/android/libraries/bind/data/Data;-><init>(I)V

    .line 62
    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0, p1}, Landroid/util/SparseArray;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/libraries/bind/data/Data;->values:Landroid/util/SparseArray;

    .line 71
    return-void
.end method

.method public constructor <init>(Landroid/util/SparseArray;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 87
    .local p1, "from":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/Object;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    .line 90
    invoke-virtual {p1}, Landroid/util/SparseArray;->clone()Landroid/util/SparseArray;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/bind/data/Data;->values:Landroid/util/SparseArray;

    .line 95
    :goto_0
    return-void

    .line 92
    :cond_0
    new-instance v0, Landroid/util/SparseArray;

    invoke-virtual {p1}, Landroid/util/SparseArray;->size()I

    move-result v1

    invoke-direct {v0, v1}, Landroid/util/SparseArray;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/libraries/bind/data/Data;->values:Landroid/util/SparseArray;

    .line 93
    invoke-direct {p0, p1}, Lcom/google/android/libraries/bind/data/Data;->putAll(Landroid/util/SparseArray;)V

    goto :goto_0
.end method

.method private frozenCheck()V
    .locals 2

    .prologue
    .line 134
    iget-boolean v0, p0, Lcom/google/android/libraries/bind/data/Data;->frozen:Z

    if-eqz v0, :cond_0

    .line 135
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Data is locked"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 137
    :cond_0
    return-void
.end method

.method public static keyName(I)Ljava/lang/String;
    .locals 1
    .param p0, "key"    # I

    .prologue
    .line 332
    invoke-static {p0}, Lcom/google/android/libraries/bind/util/Util;->getResourceName(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private putAll(Landroid/util/SparseArray;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 150
    .local p1, "otherValues":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/Object;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Landroid/util/SparseArray;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 151
    iget-object v1, p0, Lcom/google/android/libraries/bind/data/Data;->values:Landroid/util/SparseArray;

    invoke-virtual {p1, v0}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v2

    invoke-virtual {p1, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 150
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 153
    :cond_0
    return-void
.end method


# virtual methods
.method public containsKey(I)Z
    .locals 1
    .param p1, "key"    # I

    .prologue
    .line 189
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/Data;->values:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public copy()Lcom/google/android/libraries/bind/data/Data;
    .locals 2

    .prologue
    .line 46
    new-instance v0, Lcom/google/android/libraries/bind/data/Data;

    iget-object v1, p0, Lcom/google/android/libraries/bind/data/Data;->values:Landroid/util/SparseArray;

    invoke-direct {v0, v1}, Lcom/google/android/libraries/bind/data/Data;-><init>(Landroid/util/SparseArray;)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 322
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    .line 114
    instance-of v0, p1, Lcom/google/android/libraries/bind/data/Data;

    if-nez v0, :cond_0

    .line 115
    const/4 v0, 0x0

    .line 117
    .end local p1    # "object":Ljava/lang/Object;
    :goto_0
    return v0

    .restart local p1    # "object":Ljava/lang/Object;
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/Data;->values:Landroid/util/SparseArray;

    check-cast p1, Lcom/google/android/libraries/bind/data/Data;

    .end local p1    # "object":Ljava/lang/Object;
    iget-object v1, p1, Lcom/google/android/libraries/bind/data/Data;->values:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public equalsField(Lcom/google/android/libraries/bind/data/Data;I)Z
    .locals 2
    .param p1, "other"    # Lcom/google/android/libraries/bind/data/Data;
    .param p2, "key"    # I

    .prologue
    .line 29
    invoke-virtual {p0, p2}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, p2}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/libraries/bind/util/Util;->objectsEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public freeze()V
    .locals 1

    .prologue
    .line 130
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/bind/data/Data;->frozen:Z

    .line 131
    return-void
.end method

.method public get(I)Ljava/lang/Object;
    .locals 2
    .param p1, "key"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(I)TT;"
        }
    .end annotation

    .prologue
    .line 200
    iget-object v1, p0, Lcom/google/android/libraries/bind/data/Data;->values:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 201
    .local v0, "value":Ljava/lang/Object;
    instance-of v1, v0, Lcom/google/android/libraries/bind/data/DataProperty;

    if-eqz v1, :cond_0

    .line 202
    check-cast v0, Lcom/google/android/libraries/bind/data/DataProperty;

    .end local v0    # "value":Ljava/lang/Object;
    invoke-interface {v0, p0}, Lcom/google/android/libraries/bind/data/DataProperty;->apply(Lcom/google/android/libraries/bind/data/Data;)Ljava/lang/Object;

    move-result-object v0

    .line 204
    .restart local v0    # "value":Ljava/lang/Object;
    :cond_0
    return-object v0
.end method

.method public getAsBoolean(IZ)Z
    .locals 1
    .param p1, "key"    # I
    .param p2, "defaultValue"    # Z

    .prologue
    .line 302
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 303
    .local v0, "value":Ljava/lang/Object;
    if-nez v0, :cond_0

    .end local v0    # "value":Ljava/lang/Object;
    .end local p2    # "defaultValue":Z
    :goto_0
    return p2

    .restart local v0    # "value":Ljava/lang/Object;
    .restart local p2    # "defaultValue":Z
    :cond_0
    check-cast v0, Ljava/lang/Boolean;

    .end local v0    # "value":Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    goto :goto_0
.end method

.method public getAsInteger(I)Ljava/lang/Integer;
    .locals 2
    .param p1, "key"    # I

    .prologue
    .line 247
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 248
    .local v0, "value":Ljava/lang/Object;
    if-eqz v0, :cond_0

    check-cast v0, Ljava/lang/Number;

    .end local v0    # "value":Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    :goto_0
    return-object v1

    .restart local v0    # "value":Ljava/lang/Object;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getAsString(I)Ljava/lang/String;
    .locals 2
    .param p1, "key"    # I

    .prologue
    .line 225
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 226
    .local v0, "value":Ljava/lang/Object;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/Data;->values:Landroid/util/SparseArray;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public put(ILjava/lang/Object;)V
    .locals 1
    .param p1, "key"    # I
    .param p2, "value"    # Ljava/lang/Object;

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/google/android/libraries/bind/data/Data;->frozenCheck()V

    .line 25
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/Data;->values:Landroid/util/SparseArray;

    invoke-virtual {v0, p1, p2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 26
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 342
    iget-object v4, p0, Lcom/google/android/libraries/bind/data/Data;->values:Landroid/util/SparseArray;

    invoke-virtual {v4}, Landroid/util/SparseArray;->size()I

    move-result v4

    if-nez v4, :cond_0

    .line 343
    const-string v4, "Data is empty"

    .line 355
    :goto_0
    return-object v4

    .line 345
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 346
    .local v2, "sb":Ljava/lang/StringBuilder;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v4, p0, Lcom/google/android/libraries/bind/data/Data;->values:Landroid/util/SparseArray;

    invoke-virtual {v4}, Landroid/util/SparseArray;->size()I

    move-result v4

    if-ge v0, v4, :cond_2

    .line 347
    iget-object v4, p0, Lcom/google/android/libraries/bind/data/Data;->values:Landroid/util/SparseArray;

    invoke-virtual {v4, v0}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v1

    .line 348
    .local v1, "key":I
    iget-object v4, p0, Lcom/google/android/libraries/bind/data/Data;->values:Landroid/util/SparseArray;

    invoke-virtual {v4, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v3

    .line 349
    .local v3, "value":Ljava/lang/Object;
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    if-lez v4, :cond_1

    .line 350
    const-string v4, " "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 353
    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v1}, Lcom/google/android/libraries/bind/data/Data;->keyName(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 346
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 355
    .end local v1    # "key":I
    .end local v3    # "value":Ljava/lang/Object;
    :cond_2
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "parcel"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 328
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/Data;->values:Landroid/util/SparseArray;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSparseArray(Landroid/util/SparseArray;)V

    .line 329
    return-void
.end method
