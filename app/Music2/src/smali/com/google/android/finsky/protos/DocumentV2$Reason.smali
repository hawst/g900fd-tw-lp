.class public final Lcom/google/android/finsky/protos/DocumentV2$Reason;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DocumentV2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/DocumentV2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Reason"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/finsky/protos/DocumentV2$Reason;


# instance fields
.field public descriptionHtml:Ljava/lang/String;

.field public dismissal:Lcom/google/android/finsky/protos/DocumentV2$Dismissal;

.field public hasDescriptionHtml:Z

.field public reasonPlusOne:Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;

.field public reasonReview:Lcom/google/android/finsky/protos/DocumentV2$ReasonReview;

.field public reasonUserAction:Lcom/google/android/finsky/protos/DocumentV2$ReasonUserAction;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2011
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 2012
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocumentV2$Reason;->clear()Lcom/google/android/finsky/protos/DocumentV2$Reason;

    .line 2013
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/finsky/protos/DocumentV2$Reason;
    .locals 2

    .prologue
    .line 1984
    sget-object v0, Lcom/google/android/finsky/protos/DocumentV2$Reason;->_emptyArray:[Lcom/google/android/finsky/protos/DocumentV2$Reason;

    if-nez v0, :cond_1

    .line 1985
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 1987
    :try_start_0
    sget-object v0, Lcom/google/android/finsky/protos/DocumentV2$Reason;->_emptyArray:[Lcom/google/android/finsky/protos/DocumentV2$Reason;

    if-nez v0, :cond_0

    .line 1988
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/finsky/protos/DocumentV2$Reason;

    sput-object v0, Lcom/google/android/finsky/protos/DocumentV2$Reason;->_emptyArray:[Lcom/google/android/finsky/protos/DocumentV2$Reason;

    .line 1990
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1992
    :cond_1
    sget-object v0, Lcom/google/android/finsky/protos/DocumentV2$Reason;->_emptyArray:[Lcom/google/android/finsky/protos/DocumentV2$Reason;

    return-object v0

    .line 1990
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/DocumentV2$Reason;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2016
    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Reason;->dismissal:Lcom/google/android/finsky/protos/DocumentV2$Dismissal;

    .line 2017
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Reason;->descriptionHtml:Ljava/lang/String;

    .line 2018
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Reason;->hasDescriptionHtml:Z

    .line 2019
    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Reason;->reasonPlusOne:Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;

    .line 2020
    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Reason;->reasonReview:Lcom/google/android/finsky/protos/DocumentV2$ReasonReview;

    .line 2021
    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Reason;->reasonUserAction:Lcom/google/android/finsky/protos/DocumentV2$ReasonUserAction;

    .line 2022
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Reason;->cachedSize:I

    .line 2023
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 2049
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 2050
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Reason;->hasDescriptionHtml:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Reason;->descriptionHtml:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2051
    :cond_0
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Reason;->descriptionHtml:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2054
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Reason;->reasonPlusOne:Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;

    if-eqz v1, :cond_2

    .line 2055
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Reason;->reasonPlusOne:Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2058
    :cond_2
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Reason;->reasonReview:Lcom/google/android/finsky/protos/DocumentV2$ReasonReview;

    if-eqz v1, :cond_3

    .line 2059
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Reason;->reasonReview:Lcom/google/android/finsky/protos/DocumentV2$ReasonReview;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2062
    :cond_3
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Reason;->dismissal:Lcom/google/android/finsky/protos/DocumentV2$Dismissal;

    if-eqz v1, :cond_4

    .line 2063
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Reason;->dismissal:Lcom/google/android/finsky/protos/DocumentV2$Dismissal;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2066
    :cond_4
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Reason;->reasonUserAction:Lcom/google/android/finsky/protos/DocumentV2$ReasonUserAction;

    if-eqz v1, :cond_5

    .line 2067
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Reason;->reasonUserAction:Lcom/google/android/finsky/protos/DocumentV2$ReasonUserAction;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2070
    :cond_5
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocumentV2$Reason;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2078
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 2079
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 2083
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2084
    :sswitch_0
    return-object p0

    .line 2089
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Reason;->descriptionHtml:Ljava/lang/String;

    .line 2090
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Reason;->hasDescriptionHtml:Z

    goto :goto_0

    .line 2094
    :sswitch_2
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Reason;->reasonPlusOne:Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;

    if-nez v1, :cond_1

    .line 2095
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Reason;->reasonPlusOne:Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;

    .line 2097
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Reason;->reasonPlusOne:Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 2101
    :sswitch_3
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Reason;->reasonReview:Lcom/google/android/finsky/protos/DocumentV2$ReasonReview;

    if-nez v1, :cond_2

    .line 2102
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV2$ReasonReview;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV2$ReasonReview;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Reason;->reasonReview:Lcom/google/android/finsky/protos/DocumentV2$ReasonReview;

    .line 2104
    :cond_2
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Reason;->reasonReview:Lcom/google/android/finsky/protos/DocumentV2$ReasonReview;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 2108
    :sswitch_4
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Reason;->dismissal:Lcom/google/android/finsky/protos/DocumentV2$Dismissal;

    if-nez v1, :cond_3

    .line 2109
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV2$Dismissal;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV2$Dismissal;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Reason;->dismissal:Lcom/google/android/finsky/protos/DocumentV2$Dismissal;

    .line 2111
    :cond_3
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Reason;->dismissal:Lcom/google/android/finsky/protos/DocumentV2$Dismissal;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 2115
    :sswitch_5
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Reason;->reasonUserAction:Lcom/google/android/finsky/protos/DocumentV2$ReasonUserAction;

    if-nez v1, :cond_4

    .line 2116
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV2$ReasonUserAction;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV2$ReasonUserAction;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Reason;->reasonUserAction:Lcom/google/android/finsky/protos/DocumentV2$ReasonUserAction;

    .line 2118
    :cond_4
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Reason;->reasonUserAction:Lcom/google/android/finsky/protos/DocumentV2$ReasonUserAction;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 2079
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1a -> :sswitch_1
        0x22 -> :sswitch_2
        0x2a -> :sswitch_3
        0x3a -> :sswitch_4
        0x4a -> :sswitch_5
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1978
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/DocumentV2$Reason;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocumentV2$Reason;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2029
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Reason;->hasDescriptionHtml:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Reason;->descriptionHtml:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2030
    :cond_0
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Reason;->descriptionHtml:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2032
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Reason;->reasonPlusOne:Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;

    if-eqz v0, :cond_2

    .line 2033
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Reason;->reasonPlusOne:Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2035
    :cond_2
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Reason;->reasonReview:Lcom/google/android/finsky/protos/DocumentV2$ReasonReview;

    if-eqz v0, :cond_3

    .line 2036
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Reason;->reasonReview:Lcom/google/android/finsky/protos/DocumentV2$ReasonReview;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2038
    :cond_3
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Reason;->dismissal:Lcom/google/android/finsky/protos/DocumentV2$Dismissal;

    if-eqz v0, :cond_4

    .line 2039
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Reason;->dismissal:Lcom/google/android/finsky/protos/DocumentV2$Dismissal;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2041
    :cond_4
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Reason;->reasonUserAction:Lcom/google/android/finsky/protos/DocumentV2$ReasonUserAction;

    if-eqz v0, :cond_5

    .line 2042
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Reason;->reasonUserAction:Lcom/google/android/finsky/protos/DocumentV2$ReasonUserAction;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2044
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 2045
    return-void
.end method
