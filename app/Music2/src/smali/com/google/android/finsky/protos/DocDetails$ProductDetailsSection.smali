.class public final Lcom/google/android/finsky/protos/DocDetails$ProductDetailsSection;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DocDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/DocDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ProductDetailsSection"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/finsky/protos/DocDetails$ProductDetailsSection;


# instance fields
.field public description:[Lcom/google/android/finsky/protos/DocDetails$ProductDetailsDescription;

.field public hasTitle:Z

.field public title:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 463
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 464
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocDetails$ProductDetailsSection;->clear()Lcom/google/android/finsky/protos/DocDetails$ProductDetailsSection;

    .line 465
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/finsky/protos/DocDetails$ProductDetailsSection;
    .locals 2

    .prologue
    .line 445
    sget-object v0, Lcom/google/android/finsky/protos/DocDetails$ProductDetailsSection;->_emptyArray:[Lcom/google/android/finsky/protos/DocDetails$ProductDetailsSection;

    if-nez v0, :cond_1

    .line 446
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 448
    :try_start_0
    sget-object v0, Lcom/google/android/finsky/protos/DocDetails$ProductDetailsSection;->_emptyArray:[Lcom/google/android/finsky/protos/DocDetails$ProductDetailsSection;

    if-nez v0, :cond_0

    .line 449
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/finsky/protos/DocDetails$ProductDetailsSection;

    sput-object v0, Lcom/google/android/finsky/protos/DocDetails$ProductDetailsSection;->_emptyArray:[Lcom/google/android/finsky/protos/DocDetails$ProductDetailsSection;

    .line 451
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 453
    :cond_1
    sget-object v0, Lcom/google/android/finsky/protos/DocDetails$ProductDetailsSection;->_emptyArray:[Lcom/google/android/finsky/protos/DocDetails$ProductDetailsSection;

    return-object v0

    .line 451
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/DocDetails$ProductDetailsSection;
    .locals 1

    .prologue
    .line 468
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$ProductDetailsSection;->title:Ljava/lang/String;

    .line 469
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/DocDetails$ProductDetailsSection;->hasTitle:Z

    .line 470
    invoke-static {}, Lcom/google/android/finsky/protos/DocDetails$ProductDetailsDescription;->emptyArray()[Lcom/google/android/finsky/protos/DocDetails$ProductDetailsDescription;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$ProductDetailsSection;->description:[Lcom/google/android/finsky/protos/DocDetails$ProductDetailsDescription;

    .line 471
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/DocDetails$ProductDetailsSection;->cachedSize:I

    .line 472
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 5

    .prologue
    .line 494
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 495
    .local v2, "size":I
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/DocDetails$ProductDetailsSection;->hasTitle:Z

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocDetails$ProductDetailsSection;->title:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 496
    :cond_0
    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocDetails$ProductDetailsSection;->title:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 499
    :cond_1
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocDetails$ProductDetailsSection;->description:[Lcom/google/android/finsky/protos/DocDetails$ProductDetailsDescription;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocDetails$ProductDetailsSection;->description:[Lcom/google/android/finsky/protos/DocDetails$ProductDetailsDescription;

    array-length v3, v3

    if-lez v3, :cond_3

    .line 500
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocDetails$ProductDetailsSection;->description:[Lcom/google/android/finsky/protos/DocDetails$ProductDetailsDescription;

    array-length v3, v3

    if-ge v1, v3, :cond_3

    .line 501
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocDetails$ProductDetailsSection;->description:[Lcom/google/android/finsky/protos/DocDetails$ProductDetailsDescription;

    aget-object v0, v3, v1

    .line 502
    .local v0, "element":Lcom/google/android/finsky/protos/DocDetails$ProductDetailsDescription;
    if-eqz v0, :cond_2

    .line 503
    const/4 v3, 0x3

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 500
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 508
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocDetails$ProductDetailsDescription;
    .end local v1    # "i":I
    :cond_3
    return v2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocDetails$ProductDetailsSection;
    .locals 6
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 516
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 517
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 521
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 522
    :sswitch_0
    return-object p0

    .line 527
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$ProductDetailsSection;->title:Ljava/lang/String;

    .line 528
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/google/android/finsky/protos/DocDetails$ProductDetailsSection;->hasTitle:Z

    goto :goto_0

    .line 532
    :sswitch_2
    const/16 v5, 0x1a

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 534
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$ProductDetailsSection;->description:[Lcom/google/android/finsky/protos/DocDetails$ProductDetailsDescription;

    if-nez v5, :cond_2

    move v1, v4

    .line 535
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/DocDetails$ProductDetailsDescription;

    .line 537
    .local v2, "newArray":[Lcom/google/android/finsky/protos/DocDetails$ProductDetailsDescription;
    if-eqz v1, :cond_1

    .line 538
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$ProductDetailsSection;->description:[Lcom/google/android/finsky/protos/DocDetails$ProductDetailsDescription;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 540
    :cond_1
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_3

    .line 541
    new-instance v5, Lcom/google/android/finsky/protos/DocDetails$ProductDetailsDescription;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocDetails$ProductDetailsDescription;-><init>()V

    aput-object v5, v2, v1

    .line 542
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 543
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 540
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 534
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocDetails$ProductDetailsDescription;
    :cond_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$ProductDetailsSection;->description:[Lcom/google/android/finsky/protos/DocDetails$ProductDetailsDescription;

    array-length v1, v5

    goto :goto_1

    .line 546
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/DocDetails$ProductDetailsDescription;
    :cond_3
    new-instance v5, Lcom/google/android/finsky/protos/DocDetails$ProductDetailsDescription;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocDetails$ProductDetailsDescription;-><init>()V

    aput-object v5, v2, v1

    .line 547
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 548
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$ProductDetailsSection;->description:[Lcom/google/android/finsky/protos/DocDetails$ProductDetailsDescription;

    goto :goto_0

    .line 517
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x1a -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 439
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/DocDetails$ProductDetailsSection;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocDetails$ProductDetailsSection;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 478
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$ProductDetailsSection;->hasTitle:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$ProductDetailsSection;->title:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 479
    :cond_0
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocDetails$ProductDetailsSection;->title:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 481
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$ProductDetailsSection;->description:[Lcom/google/android/finsky/protos/DocDetails$ProductDetailsDescription;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$ProductDetailsSection;->description:[Lcom/google/android/finsky/protos/DocDetails$ProductDetailsDescription;

    array-length v2, v2

    if-lez v2, :cond_3

    .line 482
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$ProductDetailsSection;->description:[Lcom/google/android/finsky/protos/DocDetails$ProductDetailsDescription;

    array-length v2, v2

    if-ge v1, v2, :cond_3

    .line 483
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$ProductDetailsSection;->description:[Lcom/google/android/finsky/protos/DocDetails$ProductDetailsDescription;

    aget-object v0, v2, v1

    .line 484
    .local v0, "element":Lcom/google/android/finsky/protos/DocDetails$ProductDetailsDescription;
    if-eqz v0, :cond_2

    .line 485
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 482
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 489
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocDetails$ProductDetailsDescription;
    .end local v1    # "i":I
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 490
    return-void
.end method
