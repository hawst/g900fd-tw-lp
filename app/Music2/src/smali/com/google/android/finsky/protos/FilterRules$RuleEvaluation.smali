.class public final Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;
.super Lcom/google/protobuf/nano/MessageNano;
.source "FilterRules.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/FilterRules;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RuleEvaluation"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;


# instance fields
.field public actualBoolValue:[Z

.field public actualDoubleValue:[D

.field public actualLongValue:[J

.field public actualStringValue:[Ljava/lang/String;

.field public rule:Lcom/google/android/finsky/protos/FilterRules$Rule;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 712
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 713
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->clear()Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;

    .line 714
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;
    .locals 2

    .prologue
    .line 686
    sget-object v0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->_emptyArray:[Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;

    if-nez v0, :cond_1

    .line 687
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 689
    :try_start_0
    sget-object v0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->_emptyArray:[Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;

    if-nez v0, :cond_0

    .line 690
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;

    sput-object v0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->_emptyArray:[Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;

    .line 692
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 694
    :cond_1
    sget-object v0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->_emptyArray:[Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;

    return-object v0

    .line 692
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;
    .locals 1

    .prologue
    .line 717
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->rule:Lcom/google/android/finsky/protos/FilterRules$Rule;

    .line 718
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualStringValue:[Ljava/lang/String;

    .line 719
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_LONG_ARRAY:[J

    iput-object v0, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualLongValue:[J

    .line 720
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BOOLEAN_ARRAY:[Z

    iput-object v0, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualBoolValue:[Z

    .line 721
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_DOUBLE_ARRAY:[D

    iput-object v0, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualDoubleValue:[D

    .line 722
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->cachedSize:I

    .line 723
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 8

    .prologue
    .line 760
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v5

    .line 761
    .local v5, "size":I
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->rule:Lcom/google/android/finsky/protos/FilterRules$Rule;

    if-eqz v6, :cond_0

    .line 762
    const/4 v6, 0x1

    iget-object v7, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->rule:Lcom/google/android/finsky/protos/FilterRules$Rule;

    invoke-static {v6, v7}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v6

    add-int/2addr v5, v6

    .line 765
    :cond_0
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualStringValue:[Ljava/lang/String;

    if-eqz v6, :cond_3

    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualStringValue:[Ljava/lang/String;

    array-length v6, v6

    if-lez v6, :cond_3

    .line 766
    const/4 v0, 0x0

    .line 767
    .local v0, "dataCount":I
    const/4 v1, 0x0

    .line 768
    .local v1, "dataSize":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualStringValue:[Ljava/lang/String;

    array-length v6, v6

    if-ge v4, v6, :cond_2

    .line 769
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualStringValue:[Ljava/lang/String;

    aget-object v2, v6, v4

    .line 770
    .local v2, "element":Ljava/lang/String;
    if-eqz v2, :cond_1

    .line 771
    add-int/lit8 v0, v0, 0x1

    .line 772
    invoke-static {v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v6

    add-int/2addr v1, v6

    .line 768
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 776
    .end local v2    # "element":Ljava/lang/String;
    :cond_2
    add-int/2addr v5, v1

    .line 777
    mul-int/lit8 v6, v0, 0x1

    add-int/2addr v5, v6

    .line 779
    .end local v0    # "dataCount":I
    .end local v1    # "dataSize":I
    .end local v4    # "i":I
    :cond_3
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualLongValue:[J

    if-eqz v6, :cond_5

    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualLongValue:[J

    array-length v6, v6

    if-lez v6, :cond_5

    .line 780
    const/4 v1, 0x0

    .line 781
    .restart local v1    # "dataSize":I
    const/4 v4, 0x0

    .restart local v4    # "i":I
    :goto_1
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualLongValue:[J

    array-length v6, v6

    if-ge v4, v6, :cond_4

    .line 782
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualLongValue:[J

    aget-wide v2, v6, v4

    .line 783
    .local v2, "element":J
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64SizeNoTag(J)I

    move-result v6

    add-int/2addr v1, v6

    .line 781
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 786
    .end local v2    # "element":J
    :cond_4
    add-int/2addr v5, v1

    .line 787
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualLongValue:[J

    array-length v6, v6

    mul-int/lit8 v6, v6, 0x1

    add-int/2addr v5, v6

    .line 789
    .end local v1    # "dataSize":I
    .end local v4    # "i":I
    :cond_5
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualBoolValue:[Z

    if-eqz v6, :cond_6

    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualBoolValue:[Z

    array-length v6, v6

    if-lez v6, :cond_6

    .line 790
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualBoolValue:[Z

    array-length v6, v6

    mul-int/lit8 v1, v6, 0x1

    .line 791
    .restart local v1    # "dataSize":I
    add-int/2addr v5, v1

    .line 792
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualBoolValue:[Z

    array-length v6, v6

    mul-int/lit8 v6, v6, 0x1

    add-int/2addr v5, v6

    .line 794
    .end local v1    # "dataSize":I
    :cond_6
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualDoubleValue:[D

    if-eqz v6, :cond_7

    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualDoubleValue:[D

    array-length v6, v6

    if-lez v6, :cond_7

    .line 795
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualDoubleValue:[D

    array-length v6, v6

    mul-int/lit8 v1, v6, 0x8

    .line 796
    .restart local v1    # "dataSize":I
    add-int/2addr v5, v1

    .line 797
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualDoubleValue:[D

    array-length v6, v6

    mul-int/lit8 v6, v6, 0x1

    add-int/2addr v5, v6

    .line 799
    .end local v1    # "dataSize":I
    :cond_7
    return v5
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;
    .locals 10
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 807
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v6

    .line 808
    .local v6, "tag":I
    sparse-switch v6, :sswitch_data_0

    .line 812
    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v8

    if-nez v8, :cond_0

    .line 813
    :sswitch_0
    return-object p0

    .line 818
    :sswitch_1
    iget-object v8, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->rule:Lcom/google/android/finsky/protos/FilterRules$Rule;

    if-nez v8, :cond_1

    .line 819
    new-instance v8, Lcom/google/android/finsky/protos/FilterRules$Rule;

    invoke-direct {v8}, Lcom/google/android/finsky/protos/FilterRules$Rule;-><init>()V

    iput-object v8, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->rule:Lcom/google/android/finsky/protos/FilterRules$Rule;

    .line 821
    :cond_1
    iget-object v8, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->rule:Lcom/google/android/finsky/protos/FilterRules$Rule;

    invoke-virtual {p1, v8}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 825
    :sswitch_2
    const/16 v8, 0x12

    invoke-static {p1, v8}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 827
    .local v0, "arrayLength":I
    iget-object v8, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualStringValue:[Ljava/lang/String;

    if-nez v8, :cond_3

    move v1, v7

    .line 828
    .local v1, "i":I
    :goto_1
    add-int v8, v1, v0

    new-array v4, v8, [Ljava/lang/String;

    .line 829
    .local v4, "newArray":[Ljava/lang/String;
    if-eqz v1, :cond_2

    .line 830
    iget-object v8, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualStringValue:[Ljava/lang/String;

    invoke-static {v8, v7, v4, v7, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 832
    :cond_2
    :goto_2
    array-length v8, v4

    add-int/lit8 v8, v8, -0x1

    if-ge v1, v8, :cond_4

    .line 833
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v4, v1

    .line 834
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 832
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 827
    .end local v1    # "i":I
    .end local v4    # "newArray":[Ljava/lang/String;
    :cond_3
    iget-object v8, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualStringValue:[Ljava/lang/String;

    array-length v1, v8

    goto :goto_1

    .line 837
    .restart local v1    # "i":I
    .restart local v4    # "newArray":[Ljava/lang/String;
    :cond_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v4, v1

    .line 838
    iput-object v4, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualStringValue:[Ljava/lang/String;

    goto :goto_0

    .line 842
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v4    # "newArray":[Ljava/lang/String;
    :sswitch_3
    const/16 v8, 0x18

    invoke-static {p1, v8}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 844
    .restart local v0    # "arrayLength":I
    iget-object v8, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualLongValue:[J

    if-nez v8, :cond_6

    move v1, v7

    .line 845
    .restart local v1    # "i":I
    :goto_3
    add-int v8, v1, v0

    new-array v4, v8, [J

    .line 846
    .local v4, "newArray":[J
    if-eqz v1, :cond_5

    .line 847
    iget-object v8, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualLongValue:[J

    invoke-static {v8, v7, v4, v7, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 849
    :cond_5
    :goto_4
    array-length v8, v4

    add-int/lit8 v8, v8, -0x1

    if-ge v1, v8, :cond_7

    .line 850
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v8

    aput-wide v8, v4, v1

    .line 851
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 849
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 844
    .end local v1    # "i":I
    .end local v4    # "newArray":[J
    :cond_6
    iget-object v8, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualLongValue:[J

    array-length v1, v8

    goto :goto_3

    .line 854
    .restart local v1    # "i":I
    .restart local v4    # "newArray":[J
    :cond_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v8

    aput-wide v8, v4, v1

    .line 855
    iput-object v4, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualLongValue:[J

    goto/16 :goto_0

    .line 859
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v4    # "newArray":[J
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readRawVarint32()I

    move-result v2

    .line 860
    .local v2, "length":I
    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->pushLimit(I)I

    move-result v3

    .line 862
    .local v3, "limit":I
    const/4 v0, 0x0

    .line 863
    .restart local v0    # "arrayLength":I
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->getPosition()I

    move-result v5

    .line 864
    .local v5, "startPos":I
    :goto_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->getBytesUntilLimit()I

    move-result v8

    if-lez v8, :cond_8

    .line 865
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    .line 866
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 868
    :cond_8
    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->rewindToPosition(I)V

    .line 869
    iget-object v8, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualLongValue:[J

    if-nez v8, :cond_a

    move v1, v7

    .line 870
    .restart local v1    # "i":I
    :goto_6
    add-int v8, v1, v0

    new-array v4, v8, [J

    .line 871
    .restart local v4    # "newArray":[J
    if-eqz v1, :cond_9

    .line 872
    iget-object v8, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualLongValue:[J

    invoke-static {v8, v7, v4, v7, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 874
    :cond_9
    :goto_7
    array-length v8, v4

    if-ge v1, v8, :cond_b

    .line 875
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v8

    aput-wide v8, v4, v1

    .line 874
    add-int/lit8 v1, v1, 0x1

    goto :goto_7

    .line 869
    .end local v1    # "i":I
    .end local v4    # "newArray":[J
    :cond_a
    iget-object v8, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualLongValue:[J

    array-length v1, v8

    goto :goto_6

    .line 877
    .restart local v1    # "i":I
    .restart local v4    # "newArray":[J
    :cond_b
    iput-object v4, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualLongValue:[J

    .line 878
    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->popLimit(I)V

    goto/16 :goto_0

    .line 882
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "length":I
    .end local v3    # "limit":I
    .end local v4    # "newArray":[J
    .end local v5    # "startPos":I
    :sswitch_5
    const/16 v8, 0x20

    invoke-static {p1, v8}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 884
    .restart local v0    # "arrayLength":I
    iget-object v8, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualBoolValue:[Z

    if-nez v8, :cond_d

    move v1, v7

    .line 885
    .restart local v1    # "i":I
    :goto_8
    add-int v8, v1, v0

    new-array v4, v8, [Z

    .line 886
    .local v4, "newArray":[Z
    if-eqz v1, :cond_c

    .line 887
    iget-object v8, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualBoolValue:[Z

    invoke-static {v8, v7, v4, v7, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 889
    :cond_c
    :goto_9
    array-length v8, v4

    add-int/lit8 v8, v8, -0x1

    if-ge v1, v8, :cond_e

    .line 890
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v8

    aput-boolean v8, v4, v1

    .line 891
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 889
    add-int/lit8 v1, v1, 0x1

    goto :goto_9

    .line 884
    .end local v1    # "i":I
    .end local v4    # "newArray":[Z
    :cond_d
    iget-object v8, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualBoolValue:[Z

    array-length v1, v8

    goto :goto_8

    .line 894
    .restart local v1    # "i":I
    .restart local v4    # "newArray":[Z
    :cond_e
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v8

    aput-boolean v8, v4, v1

    .line 895
    iput-object v4, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualBoolValue:[Z

    goto/16 :goto_0

    .line 899
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v4    # "newArray":[Z
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readRawVarint32()I

    move-result v2

    .line 900
    .restart local v2    # "length":I
    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->pushLimit(I)I

    move-result v3

    .line 902
    .restart local v3    # "limit":I
    const/4 v0, 0x0

    .line 903
    .restart local v0    # "arrayLength":I
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->getPosition()I

    move-result v5

    .line 904
    .restart local v5    # "startPos":I
    :goto_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->getBytesUntilLimit()I

    move-result v8

    if-lez v8, :cond_f

    .line 905
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    .line 906
    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    .line 908
    :cond_f
    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->rewindToPosition(I)V

    .line 909
    iget-object v8, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualBoolValue:[Z

    if-nez v8, :cond_11

    move v1, v7

    .line 910
    .restart local v1    # "i":I
    :goto_b
    add-int v8, v1, v0

    new-array v4, v8, [Z

    .line 911
    .restart local v4    # "newArray":[Z
    if-eqz v1, :cond_10

    .line 912
    iget-object v8, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualBoolValue:[Z

    invoke-static {v8, v7, v4, v7, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 914
    :cond_10
    :goto_c
    array-length v8, v4

    if-ge v1, v8, :cond_12

    .line 915
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v8

    aput-boolean v8, v4, v1

    .line 914
    add-int/lit8 v1, v1, 0x1

    goto :goto_c

    .line 909
    .end local v1    # "i":I
    .end local v4    # "newArray":[Z
    :cond_11
    iget-object v8, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualBoolValue:[Z

    array-length v1, v8

    goto :goto_b

    .line 917
    .restart local v1    # "i":I
    .restart local v4    # "newArray":[Z
    :cond_12
    iput-object v4, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualBoolValue:[Z

    .line 918
    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->popLimit(I)V

    goto/16 :goto_0

    .line 922
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "length":I
    .end local v3    # "limit":I
    .end local v4    # "newArray":[Z
    .end local v5    # "startPos":I
    :sswitch_7
    const/16 v8, 0x29

    invoke-static {p1, v8}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 924
    .restart local v0    # "arrayLength":I
    iget-object v8, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualDoubleValue:[D

    if-nez v8, :cond_14

    move v1, v7

    .line 925
    .restart local v1    # "i":I
    :goto_d
    add-int v8, v1, v0

    new-array v4, v8, [D

    .line 926
    .local v4, "newArray":[D
    if-eqz v1, :cond_13

    .line 927
    iget-object v8, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualDoubleValue:[D

    invoke-static {v8, v7, v4, v7, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 929
    :cond_13
    :goto_e
    array-length v8, v4

    add-int/lit8 v8, v8, -0x1

    if-ge v1, v8, :cond_15

    .line 930
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readDouble()D

    move-result-wide v8

    aput-wide v8, v4, v1

    .line 931
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 929
    add-int/lit8 v1, v1, 0x1

    goto :goto_e

    .line 924
    .end local v1    # "i":I
    .end local v4    # "newArray":[D
    :cond_14
    iget-object v8, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualDoubleValue:[D

    array-length v1, v8

    goto :goto_d

    .line 934
    .restart local v1    # "i":I
    .restart local v4    # "newArray":[D
    :cond_15
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readDouble()D

    move-result-wide v8

    aput-wide v8, v4, v1

    .line 935
    iput-object v4, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualDoubleValue:[D

    goto/16 :goto_0

    .line 939
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v4    # "newArray":[D
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readRawVarint32()I

    move-result v2

    .line 940
    .restart local v2    # "length":I
    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->pushLimit(I)I

    move-result v3

    .line 941
    .restart local v3    # "limit":I
    div-int/lit8 v0, v2, 0x8

    .line 942
    .restart local v0    # "arrayLength":I
    iget-object v8, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualDoubleValue:[D

    if-nez v8, :cond_17

    move v1, v7

    .line 943
    .restart local v1    # "i":I
    :goto_f
    add-int v8, v1, v0

    new-array v4, v8, [D

    .line 944
    .restart local v4    # "newArray":[D
    if-eqz v1, :cond_16

    .line 945
    iget-object v8, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualDoubleValue:[D

    invoke-static {v8, v7, v4, v7, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 947
    :cond_16
    :goto_10
    array-length v8, v4

    if-ge v1, v8, :cond_18

    .line 948
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readDouble()D

    move-result-wide v8

    aput-wide v8, v4, v1

    .line 947
    add-int/lit8 v1, v1, 0x1

    goto :goto_10

    .line 942
    .end local v1    # "i":I
    .end local v4    # "newArray":[D
    :cond_17
    iget-object v8, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualDoubleValue:[D

    array-length v1, v8

    goto :goto_f

    .line 950
    .restart local v1    # "i":I
    .restart local v4    # "newArray":[D
    :cond_18
    iput-object v4, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualDoubleValue:[D

    .line 951
    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->popLimit(I)V

    goto/16 :goto_0

    .line 808
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x1a -> :sswitch_4
        0x20 -> :sswitch_5
        0x22 -> :sswitch_6
        0x29 -> :sswitch_7
        0x2a -> :sswitch_8
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 680
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 6
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 729
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->rule:Lcom/google/android/finsky/protos/FilterRules$Rule;

    if-eqz v2, :cond_0

    .line 730
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->rule:Lcom/google/android/finsky/protos/FilterRules$Rule;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 732
    :cond_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualStringValue:[Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualStringValue:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_2

    .line 733
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualStringValue:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_2

    .line 734
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualStringValue:[Ljava/lang/String;

    aget-object v0, v2, v1

    .line 735
    .local v0, "element":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 736
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 733
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 740
    .end local v0    # "element":Ljava/lang/String;
    .end local v1    # "i":I
    :cond_2
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualLongValue:[J

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualLongValue:[J

    array-length v2, v2

    if-lez v2, :cond_3

    .line 741
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualLongValue:[J

    array-length v2, v2

    if-ge v1, v2, :cond_3

    .line 742
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualLongValue:[J

    aget-wide v4, v3, v1

    invoke-virtual {p1, v2, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 741
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 745
    .end local v1    # "i":I
    :cond_3
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualBoolValue:[Z

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualBoolValue:[Z

    array-length v2, v2

    if-lez v2, :cond_4

    .line 746
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_2
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualBoolValue:[Z

    array-length v2, v2

    if-ge v1, v2, :cond_4

    .line 747
    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualBoolValue:[Z

    aget-boolean v3, v3, v1

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 746
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 750
    .end local v1    # "i":I
    :cond_4
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualDoubleValue:[D

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualDoubleValue:[D

    array-length v2, v2

    if-lez v2, :cond_5

    .line 751
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_3
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualDoubleValue:[D

    array-length v2, v2

    if-ge v1, v2, :cond_5

    .line 752
    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualDoubleValue:[D

    aget-wide v4, v3, v1

    invoke-virtual {p1, v2, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeDouble(ID)V

    .line 751
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 755
    .end local v1    # "i":I
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 756
    return-void
.end method
