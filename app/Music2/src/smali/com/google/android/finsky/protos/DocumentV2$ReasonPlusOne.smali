.class public final Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DocumentV2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/DocumentV2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ReasonPlusOne"
.end annotation


# instance fields
.field public hasLocalizedDescriptionHtml:Z

.field public localizedDescriptionHtml:Ljava/lang/String;

.field public oBSOLETEPlusProfile:[Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

.field public person:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2164
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 2165
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;->clear()Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;

    .line 2166
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;
    .locals 1

    .prologue
    .line 2169
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;->localizedDescriptionHtml:Ljava/lang/String;

    .line 2170
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;->hasLocalizedDescriptionHtml:Z

    .line 2171
    invoke-static {}, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->emptyArray()[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;->person:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 2172
    invoke-static {}, Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;->emptyArray()[Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;->oBSOLETEPlusProfile:[Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

    .line 2173
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;->cachedSize:I

    .line 2174
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 5

    .prologue
    .line 2204
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 2205
    .local v2, "size":I
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;->hasLocalizedDescriptionHtml:Z

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;->localizedDescriptionHtml:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 2206
    :cond_0
    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;->localizedDescriptionHtml:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 2209
    :cond_1
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;->oBSOLETEPlusProfile:[Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;->oBSOLETEPlusProfile:[Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

    array-length v3, v3

    if-lez v3, :cond_3

    .line 2210
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;->oBSOLETEPlusProfile:[Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

    array-length v3, v3

    if-ge v1, v3, :cond_3

    .line 2211
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;->oBSOLETEPlusProfile:[Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

    aget-object v0, v3, v1

    .line 2212
    .local v0, "element":Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;
    if-eqz v0, :cond_2

    .line 2213
    const/4 v3, 0x2

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 2210
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2218
    .end local v0    # "element":Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;
    .end local v1    # "i":I
    :cond_3
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;->person:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;->person:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v3, v3

    if-lez v3, :cond_5

    .line 2219
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;->person:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v3, v3

    if-ge v1, v3, :cond_5

    .line 2220
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;->person:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    aget-object v0, v3, v1

    .line 2221
    .local v0, "element":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    if-eqz v0, :cond_4

    .line 2222
    const/4 v3, 0x3

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 2219
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2227
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    .end local v1    # "i":I
    :cond_5
    return v2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;
    .locals 6
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 2235
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 2236
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 2240
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 2241
    :sswitch_0
    return-object p0

    .line 2246
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;->localizedDescriptionHtml:Ljava/lang/String;

    .line 2247
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;->hasLocalizedDescriptionHtml:Z

    goto :goto_0

    .line 2251
    :sswitch_2
    const/16 v5, 0x12

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 2253
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;->oBSOLETEPlusProfile:[Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

    if-nez v5, :cond_2

    move v1, v4

    .line 2254
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

    .line 2256
    .local v2, "newArray":[Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;
    if-eqz v1, :cond_1

    .line 2257
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;->oBSOLETEPlusProfile:[Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2259
    :cond_1
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_3

    .line 2260
    new-instance v5, Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;-><init>()V

    aput-object v5, v2, v1

    .line 2261
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 2262
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 2259
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 2253
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;
    :cond_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;->oBSOLETEPlusProfile:[Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

    array-length v1, v5

    goto :goto_1

    .line 2265
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;
    :cond_3
    new-instance v5, Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;-><init>()V

    aput-object v5, v2, v1

    .line 2266
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 2267
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;->oBSOLETEPlusProfile:[Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

    goto :goto_0

    .line 2271
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;
    :sswitch_3
    const/16 v5, 0x1a

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 2273
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;->person:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-nez v5, :cond_5

    move v1, v4

    .line 2274
    .restart local v1    # "i":I
    :goto_3
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 2276
    .local v2, "newArray":[Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    if-eqz v1, :cond_4

    .line 2277
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;->person:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2279
    :cond_4
    :goto_4
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_6

    .line 2280
    new-instance v5, Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocumentV2$DocV2;-><init>()V

    aput-object v5, v2, v1

    .line 2281
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 2282
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 2279
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 2273
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    :cond_5
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;->person:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v1, v5

    goto :goto_3

    .line 2285
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    :cond_6
    new-instance v5, Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocumentV2$DocV2;-><init>()V

    aput-object v5, v2, v1

    .line 2286
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 2287
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;->person:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    goto/16 :goto_0

    .line 2236
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2137
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2180
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;->hasLocalizedDescriptionHtml:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;->localizedDescriptionHtml:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2181
    :cond_0
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;->localizedDescriptionHtml:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2183
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;->oBSOLETEPlusProfile:[Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;->oBSOLETEPlusProfile:[Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

    array-length v2, v2

    if-lez v2, :cond_3

    .line 2184
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;->oBSOLETEPlusProfile:[Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

    array-length v2, v2

    if-ge v1, v2, :cond_3

    .line 2185
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;->oBSOLETEPlusProfile:[Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

    aget-object v0, v2, v1

    .line 2186
    .local v0, "element":Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;
    if-eqz v0, :cond_2

    .line 2187
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2184
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2191
    .end local v0    # "element":Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;
    .end local v1    # "i":I
    :cond_3
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;->person:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;->person:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v2, v2

    if-lez v2, :cond_5

    .line 2192
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;->person:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v2, v2

    if-ge v1, v2, :cond_5

    .line 2193
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;->person:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    aget-object v0, v2, v1

    .line 2194
    .local v0, "element":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    if-eqz v0, :cond_4

    .line 2195
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2192
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2199
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    .end local v1    # "i":I
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 2200
    return-void
.end method
