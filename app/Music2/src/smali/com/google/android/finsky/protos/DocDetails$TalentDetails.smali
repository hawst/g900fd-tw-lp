.class public final Lcom/google/android/finsky/protos/DocDetails$TalentDetails;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DocDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/DocDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TalentDetails"
.end annotation


# instance fields
.field public externalLinks:Lcom/google/android/finsky/protos/DocDetails$TalentExternalLinks;

.field public hasPrimaryRoleId:Z

.field public primaryRoleId:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4648
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 4649
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocDetails$TalentDetails;->clear()Lcom/google/android/finsky/protos/DocDetails$TalentDetails;

    .line 4650
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/DocDetails$TalentDetails;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 4653
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$TalentDetails;->externalLinks:Lcom/google/android/finsky/protos/DocDetails$TalentExternalLinks;

    .line 4654
    iput v1, p0, Lcom/google/android/finsky/protos/DocDetails$TalentDetails;->primaryRoleId:I

    .line 4655
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$TalentDetails;->hasPrimaryRoleId:Z

    .line 4656
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/DocDetails$TalentDetails;->cachedSize:I

    .line 4657
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 4674
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 4675
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$TalentDetails;->externalLinks:Lcom/google/android/finsky/protos/DocDetails$TalentExternalLinks;

    if-eqz v1, :cond_0

    .line 4676
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$TalentDetails;->externalLinks:Lcom/google/android/finsky/protos/DocDetails$TalentExternalLinks;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4679
    :cond_0
    iget v1, p0, Lcom/google/android/finsky/protos/DocDetails$TalentDetails;->primaryRoleId:I

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$TalentDetails;->hasPrimaryRoleId:Z

    if-eqz v1, :cond_2

    .line 4680
    :cond_1
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/finsky/protos/DocDetails$TalentDetails;->primaryRoleId:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4683
    :cond_2
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocDetails$TalentDetails;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4691
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 4692
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 4696
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 4697
    :sswitch_0
    return-object p0

    .line 4702
    :sswitch_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$TalentDetails;->externalLinks:Lcom/google/android/finsky/protos/DocDetails$TalentExternalLinks;

    if-nez v2, :cond_1

    .line 4703
    new-instance v2, Lcom/google/android/finsky/protos/DocDetails$TalentExternalLinks;

    invoke-direct {v2}, Lcom/google/android/finsky/protos/DocDetails$TalentExternalLinks;-><init>()V

    iput-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$TalentDetails;->externalLinks:Lcom/google/android/finsky/protos/DocDetails$TalentExternalLinks;

    .line 4705
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$TalentDetails;->externalLinks:Lcom/google/android/finsky/protos/DocDetails$TalentExternalLinks;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 4709
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 4710
    .local v1, "value":I
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 4718
    :pswitch_0
    iput v1, p0, Lcom/google/android/finsky/protos/DocDetails$TalentDetails;->primaryRoleId:I

    .line 4719
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$TalentDetails;->hasPrimaryRoleId:Z

    goto :goto_0

    .line 4692
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch

    .line 4710
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4624
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/DocDetails$TalentDetails;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocDetails$TalentDetails;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4663
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$TalentDetails;->externalLinks:Lcom/google/android/finsky/protos/DocDetails$TalentExternalLinks;

    if-eqz v0, :cond_0

    .line 4664
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$TalentDetails;->externalLinks:Lcom/google/android/finsky/protos/DocDetails$TalentExternalLinks;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 4666
    :cond_0
    iget v0, p0, Lcom/google/android/finsky/protos/DocDetails$TalentDetails;->primaryRoleId:I

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocDetails$TalentDetails;->hasPrimaryRoleId:Z

    if-eqz v0, :cond_2

    .line 4667
    :cond_1
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/finsky/protos/DocDetails$TalentDetails;->primaryRoleId:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 4669
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 4670
    return-void
.end method
