.class public final Lcom/google/android/finsky/protos/Rating$AggregateRating;
.super Lcom/google/protobuf/nano/MessageNano;
.source "Rating.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/Rating;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AggregateRating"
.end annotation


# instance fields
.field public bayesianMeanRating:D

.field public commentCount:J

.field public fiveStarRatings:J

.field public fourStarRatings:J

.field public hasBayesianMeanRating:Z

.field public hasCommentCount:Z

.field public hasFiveStarRatings:Z

.field public hasFourStarRatings:Z

.field public hasOneStarRatings:Z

.field public hasRatingsCount:Z

.field public hasStarRating:Z

.field public hasThreeStarRatings:Z

.field public hasThumbsDownCount:Z

.field public hasThumbsUpCount:Z

.field public hasTwoStarRatings:Z

.field public hasType:Z

.field public oneStarRatings:J

.field public ratingsCount:J

.field public starRating:F

.field public threeStarRatings:J

.field public thumbsDownCount:J

.field public thumbsUpCount:J

.field public twoStarRatings:J

.field public type:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 175
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 176
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Rating$AggregateRating;->clear()Lcom/google/android/finsky/protos/Rating$AggregateRating;

    .line 177
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/Rating$AggregateRating;
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    .line 180
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->type:I

    .line 181
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->hasType:Z

    .line 182
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->starRating:F

    .line 183
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->hasStarRating:Z

    .line 184
    iput-wide v4, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->ratingsCount:J

    .line 185
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->hasRatingsCount:Z

    .line 186
    iput-wide v4, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->commentCount:J

    .line 187
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->hasCommentCount:Z

    .line 188
    iput-wide v4, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->oneStarRatings:J

    .line 189
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->hasOneStarRatings:Z

    .line 190
    iput-wide v4, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->twoStarRatings:J

    .line 191
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->hasTwoStarRatings:Z

    .line 192
    iput-wide v4, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->threeStarRatings:J

    .line 193
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->hasThreeStarRatings:Z

    .line 194
    iput-wide v4, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->fourStarRatings:J

    .line 195
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->hasFourStarRatings:Z

    .line 196
    iput-wide v4, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->fiveStarRatings:J

    .line 197
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->hasFiveStarRatings:Z

    .line 198
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->bayesianMeanRating:D

    .line 199
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->hasBayesianMeanRating:Z

    .line 200
    iput-wide v4, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->thumbsUpCount:J

    .line 201
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->hasThumbsUpCount:Z

    .line 202
    iput-wide v4, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->thumbsDownCount:J

    .line 203
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->hasThumbsDownCount:Z

    .line 204
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->cachedSize:I

    .line 205
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 6

    .prologue
    const/4 v2, 0x1

    const-wide/16 v4, 0x0

    .line 254
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 255
    .local v0, "size":I
    iget v1, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->type:I

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->hasType:Z

    if-eqz v1, :cond_1

    .line 256
    :cond_0
    iget v1, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->type:I

    invoke-static {v2, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 259
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->hasStarRating:Z

    if-nez v1, :cond_2

    iget v1, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->starRating:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    if-eq v1, v2, :cond_3

    .line 261
    :cond_2
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->starRating:F

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeFloatSize(IF)I

    move-result v1

    add-int/2addr v0, v1

    .line 264
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->hasRatingsCount:Z

    if-nez v1, :cond_4

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->ratingsCount:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_5

    .line 265
    :cond_4
    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->ratingsCount:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeUInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 268
    :cond_5
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->hasOneStarRatings:Z

    if-nez v1, :cond_6

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->oneStarRatings:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_7

    .line 269
    :cond_6
    const/4 v1, 0x4

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->oneStarRatings:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeUInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 272
    :cond_7
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->hasTwoStarRatings:Z

    if-nez v1, :cond_8

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->twoStarRatings:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_9

    .line 273
    :cond_8
    const/4 v1, 0x5

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->twoStarRatings:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeUInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 276
    :cond_9
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->hasThreeStarRatings:Z

    if-nez v1, :cond_a

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->threeStarRatings:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_b

    .line 277
    :cond_a
    const/4 v1, 0x6

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->threeStarRatings:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeUInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 280
    :cond_b
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->hasFourStarRatings:Z

    if-nez v1, :cond_c

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->fourStarRatings:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_d

    .line 281
    :cond_c
    const/4 v1, 0x7

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->fourStarRatings:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeUInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 284
    :cond_d
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->hasFiveStarRatings:Z

    if-nez v1, :cond_e

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->fiveStarRatings:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_f

    .line 285
    :cond_e
    const/16 v1, 0x8

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->fiveStarRatings:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeUInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 288
    :cond_f
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->hasThumbsUpCount:Z

    if-nez v1, :cond_10

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->thumbsUpCount:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_11

    .line 289
    :cond_10
    const/16 v1, 0x9

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->thumbsUpCount:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeUInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 292
    :cond_11
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->hasThumbsDownCount:Z

    if-nez v1, :cond_12

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->thumbsDownCount:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_13

    .line 293
    :cond_12
    const/16 v1, 0xa

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->thumbsDownCount:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeUInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 296
    :cond_13
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->hasCommentCount:Z

    if-nez v1, :cond_14

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->commentCount:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_15

    .line 297
    :cond_14
    const/16 v1, 0xb

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->commentCount:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeUInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 300
    :cond_15
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->hasBayesianMeanRating:Z

    if-nez v1, :cond_16

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->bayesianMeanRating:D

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-eqz v1, :cond_17

    .line 302
    :cond_16
    const/16 v1, 0xc

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->bayesianMeanRating:D

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeDoubleSize(ID)I

    move-result v1

    add-int/2addr v0, v1

    .line 305
    :cond_17
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Rating$AggregateRating;
    .locals 5
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 313
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 314
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 318
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 319
    :sswitch_0
    return-object p0

    .line 324
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 325
    .local v1, "value":I
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 329
    :pswitch_0
    iput v1, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->type:I

    .line 330
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->hasType:Z

    goto :goto_0

    .line 336
    .end local v1    # "value":I
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readFloat()F

    move-result v2

    iput v2, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->starRating:F

    .line 337
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->hasStarRating:Z

    goto :goto_0

    .line 341
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readUInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->ratingsCount:J

    .line 342
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->hasRatingsCount:Z

    goto :goto_0

    .line 346
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readUInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->oneStarRatings:J

    .line 347
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->hasOneStarRatings:Z

    goto :goto_0

    .line 351
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readUInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->twoStarRatings:J

    .line 352
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->hasTwoStarRatings:Z

    goto :goto_0

    .line 356
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readUInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->threeStarRatings:J

    .line 357
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->hasThreeStarRatings:Z

    goto :goto_0

    .line 361
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readUInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->fourStarRatings:J

    .line 362
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->hasFourStarRatings:Z

    goto :goto_0

    .line 366
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readUInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->fiveStarRatings:J

    .line 367
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->hasFiveStarRatings:Z

    goto :goto_0

    .line 371
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readUInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->thumbsUpCount:J

    .line 372
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->hasThumbsUpCount:Z

    goto :goto_0

    .line 376
    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readUInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->thumbsDownCount:J

    .line 377
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->hasThumbsDownCount:Z

    goto :goto_0

    .line 381
    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readUInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->commentCount:J

    .line 382
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->hasCommentCount:Z

    goto :goto_0

    .line 386
    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readDouble()D

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->bayesianMeanRating:D

    .line 387
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->hasBayesianMeanRating:Z

    goto :goto_0

    .line 314
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x15 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
        0x61 -> :sswitch_c
    .end sparse-switch

    .line 325
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 105
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/Rating$AggregateRating;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Rating$AggregateRating;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 6
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const-wide/16 v4, 0x0

    .line 211
    iget v0, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->type:I

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->hasType:Z

    if-eqz v0, :cond_1

    .line 212
    :cond_0
    iget v0, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->type:I

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 214
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->hasStarRating:Z

    if-nez v0, :cond_2

    iget v0, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->starRating:F

    invoke-static {v0}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    if-eq v0, v1, :cond_3

    .line 216
    :cond_2
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->starRating:F

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeFloat(IF)V

    .line 218
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->hasRatingsCount:Z

    if-nez v0, :cond_4

    iget-wide v0, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->ratingsCount:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_5

    .line 219
    :cond_4
    const/4 v0, 0x3

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->ratingsCount:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeUInt64(IJ)V

    .line 221
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->hasOneStarRatings:Z

    if-nez v0, :cond_6

    iget-wide v0, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->oneStarRatings:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_7

    .line 222
    :cond_6
    const/4 v0, 0x4

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->oneStarRatings:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeUInt64(IJ)V

    .line 224
    :cond_7
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->hasTwoStarRatings:Z

    if-nez v0, :cond_8

    iget-wide v0, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->twoStarRatings:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_9

    .line 225
    :cond_8
    const/4 v0, 0x5

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->twoStarRatings:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeUInt64(IJ)V

    .line 227
    :cond_9
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->hasThreeStarRatings:Z

    if-nez v0, :cond_a

    iget-wide v0, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->threeStarRatings:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_b

    .line 228
    :cond_a
    const/4 v0, 0x6

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->threeStarRatings:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeUInt64(IJ)V

    .line 230
    :cond_b
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->hasFourStarRatings:Z

    if-nez v0, :cond_c

    iget-wide v0, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->fourStarRatings:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_d

    .line 231
    :cond_c
    const/4 v0, 0x7

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->fourStarRatings:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeUInt64(IJ)V

    .line 233
    :cond_d
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->hasFiveStarRatings:Z

    if-nez v0, :cond_e

    iget-wide v0, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->fiveStarRatings:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_f

    .line 234
    :cond_e
    const/16 v0, 0x8

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->fiveStarRatings:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeUInt64(IJ)V

    .line 236
    :cond_f
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->hasThumbsUpCount:Z

    if-nez v0, :cond_10

    iget-wide v0, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->thumbsUpCount:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_11

    .line 237
    :cond_10
    const/16 v0, 0x9

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->thumbsUpCount:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeUInt64(IJ)V

    .line 239
    :cond_11
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->hasThumbsDownCount:Z

    if-nez v0, :cond_12

    iget-wide v0, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->thumbsDownCount:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_13

    .line 240
    :cond_12
    const/16 v0, 0xa

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->thumbsDownCount:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeUInt64(IJ)V

    .line 242
    :cond_13
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->hasCommentCount:Z

    if-nez v0, :cond_14

    iget-wide v0, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->commentCount:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_15

    .line 243
    :cond_14
    const/16 v0, 0xb

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->commentCount:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeUInt64(IJ)V

    .line 245
    :cond_15
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->hasBayesianMeanRating:Z

    if-nez v0, :cond_16

    iget-wide v0, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->bayesianMeanRating:D

    invoke-static {v0, v1}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_17

    .line 247
    :cond_16
    const/16 v0, 0xc

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->bayesianMeanRating:D

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeDouble(ID)V

    .line 249
    :cond_17
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 250
    return-void
.end method
