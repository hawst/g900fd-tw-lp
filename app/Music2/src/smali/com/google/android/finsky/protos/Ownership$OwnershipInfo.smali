.class public final Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;
.super Lcom/google/protobuf/nano/MessageNano;
.source "Ownership.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/Ownership;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "OwnershipInfo"
.end annotation


# instance fields
.field public autoRenewing:Z

.field public developerPurchaseInfo:Lcom/google/android/finsky/protos/Common$SignedData;

.field public groupLicenseInfo:Lcom/google/android/finsky/protos/GroupLicense$GroupLicenseInfo;

.field public hasAutoRenewing:Z

.field public hasHidden:Z

.field public hasInitiationTimestampMsec:Z

.field public hasLibraryExpirationTimestampMsec:Z

.field public hasPostDeliveryRefundWindowMsec:Z

.field public hasPreordered:Z

.field public hasQuantity:Z

.field public hasRefundTimeoutTimestampMsec:Z

.field public hasValidUntilTimestampMsec:Z

.field public hidden:Z

.field public initiationTimestampMsec:J

.field public libraryExpirationTimestampMsec:J

.field public libraryVoucher:Lcom/google/android/finsky/protos/Voucher$LibraryVoucher;

.field public licensedDocumentInfo:Lcom/google/android/finsky/protos/Common$LicensedDocumentInfo;

.field public postDeliveryRefundWindowMsec:J

.field public preordered:Z

.field public quantity:I

.field public refundTimeoutTimestampMsec:J

.field public rentalTerms:Lcom/google/android/finsky/protos/Common$RentalTerms;

.field public validUntilTimestampMsec:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 76
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 77
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->clear()Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;

    .line 78
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 81
    iput-wide v4, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->initiationTimestampMsec:J

    .line 82
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->hasInitiationTimestampMsec:Z

    .line 83
    iput-wide v4, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->validUntilTimestampMsec:J

    .line 84
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->hasValidUntilTimestampMsec:Z

    .line 85
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->autoRenewing:Z

    .line 86
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->hasAutoRenewing:Z

    .line 87
    iput-wide v4, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->libraryExpirationTimestampMsec:J

    .line 88
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->hasLibraryExpirationTimestampMsec:Z

    .line 89
    iput-wide v4, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->refundTimeoutTimestampMsec:J

    .line 90
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->hasRefundTimeoutTimestampMsec:Z

    .line 91
    iput-wide v4, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->postDeliveryRefundWindowMsec:J

    .line 92
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->hasPostDeliveryRefundWindowMsec:Z

    .line 93
    iput-object v2, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->developerPurchaseInfo:Lcom/google/android/finsky/protos/Common$SignedData;

    .line 94
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->preordered:Z

    .line 95
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->hasPreordered:Z

    .line 96
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->hidden:Z

    .line 97
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->hasHidden:Z

    .line 98
    iput-object v2, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->rentalTerms:Lcom/google/android/finsky/protos/Common$RentalTerms;

    .line 99
    iput-object v2, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->groupLicenseInfo:Lcom/google/android/finsky/protos/GroupLicense$GroupLicenseInfo;

    .line 100
    iput-object v2, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->licensedDocumentInfo:Lcom/google/android/finsky/protos/Common$LicensedDocumentInfo;

    .line 101
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->quantity:I

    .line 102
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->hasQuantity:Z

    .line 103
    iput-object v2, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->libraryVoucher:Lcom/google/android/finsky/protos/Voucher$LibraryVoucher;

    .line 104
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->cachedSize:I

    .line 105
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 7

    .prologue
    const/4 v6, 0x1

    const-wide/16 v4, 0x0

    .line 158
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 159
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->hasInitiationTimestampMsec:Z

    if-nez v1, :cond_0

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->initiationTimestampMsec:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    .line 160
    :cond_0
    iget-wide v2, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->initiationTimestampMsec:J

    invoke-static {v6, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 163
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->hasValidUntilTimestampMsec:Z

    if-nez v1, :cond_2

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->validUntilTimestampMsec:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_3

    .line 164
    :cond_2
    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->validUntilTimestampMsec:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 167
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->hasAutoRenewing:Z

    if-nez v1, :cond_4

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->autoRenewing:Z

    if-eqz v1, :cond_5

    .line 168
    :cond_4
    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->autoRenewing:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 171
    :cond_5
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->hasRefundTimeoutTimestampMsec:Z

    if-nez v1, :cond_6

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->refundTimeoutTimestampMsec:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_7

    .line 172
    :cond_6
    const/4 v1, 0x4

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->refundTimeoutTimestampMsec:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 175
    :cond_7
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->hasPostDeliveryRefundWindowMsec:Z

    if-nez v1, :cond_8

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->postDeliveryRefundWindowMsec:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_9

    .line 176
    :cond_8
    const/4 v1, 0x5

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->postDeliveryRefundWindowMsec:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 179
    :cond_9
    iget-object v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->developerPurchaseInfo:Lcom/google/android/finsky/protos/Common$SignedData;

    if-eqz v1, :cond_a

    .line 180
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->developerPurchaseInfo:Lcom/google/android/finsky/protos/Common$SignedData;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 183
    :cond_a
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->hasPreordered:Z

    if-nez v1, :cond_b

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->preordered:Z

    if-eqz v1, :cond_c

    .line 184
    :cond_b
    const/4 v1, 0x7

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->preordered:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 187
    :cond_c
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->hasHidden:Z

    if-nez v1, :cond_d

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->hidden:Z

    if-eqz v1, :cond_e

    .line 188
    :cond_d
    const/16 v1, 0x8

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->hidden:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 191
    :cond_e
    iget-object v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->rentalTerms:Lcom/google/android/finsky/protos/Common$RentalTerms;

    if-eqz v1, :cond_f

    .line 192
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->rentalTerms:Lcom/google/android/finsky/protos/Common$RentalTerms;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 195
    :cond_f
    iget-object v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->groupLicenseInfo:Lcom/google/android/finsky/protos/GroupLicense$GroupLicenseInfo;

    if-eqz v1, :cond_10

    .line 196
    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->groupLicenseInfo:Lcom/google/android/finsky/protos/GroupLicense$GroupLicenseInfo;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 199
    :cond_10
    iget-object v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->licensedDocumentInfo:Lcom/google/android/finsky/protos/Common$LicensedDocumentInfo;

    if-eqz v1, :cond_11

    .line 200
    const/16 v1, 0xb

    iget-object v2, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->licensedDocumentInfo:Lcom/google/android/finsky/protos/Common$LicensedDocumentInfo;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 203
    :cond_11
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->hasQuantity:Z

    if-nez v1, :cond_12

    iget v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->quantity:I

    if-eq v1, v6, :cond_13

    .line 204
    :cond_12
    const/16 v1, 0xc

    iget v2, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->quantity:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 207
    :cond_13
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->hasLibraryExpirationTimestampMsec:Z

    if-nez v1, :cond_14

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->libraryExpirationTimestampMsec:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_15

    .line 208
    :cond_14
    const/16 v1, 0xe

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->libraryExpirationTimestampMsec:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 211
    :cond_15
    iget-object v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->libraryVoucher:Lcom/google/android/finsky/protos/Voucher$LibraryVoucher;

    if-eqz v1, :cond_16

    .line 212
    const/16 v1, 0xf

    iget-object v2, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->libraryVoucher:Lcom/google/android/finsky/protos/Voucher$LibraryVoucher;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 215
    :cond_16
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;
    .locals 5
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 223
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 224
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 228
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 229
    :sswitch_0
    return-object p0

    .line 234
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->initiationTimestampMsec:J

    .line 235
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->hasInitiationTimestampMsec:Z

    goto :goto_0

    .line 239
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->validUntilTimestampMsec:J

    .line 240
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->hasValidUntilTimestampMsec:Z

    goto :goto_0

    .line 244
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->autoRenewing:Z

    .line 245
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->hasAutoRenewing:Z

    goto :goto_0

    .line 249
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->refundTimeoutTimestampMsec:J

    .line 250
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->hasRefundTimeoutTimestampMsec:Z

    goto :goto_0

    .line 254
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->postDeliveryRefundWindowMsec:J

    .line 255
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->hasPostDeliveryRefundWindowMsec:Z

    goto :goto_0

    .line 259
    :sswitch_6
    iget-object v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->developerPurchaseInfo:Lcom/google/android/finsky/protos/Common$SignedData;

    if-nez v1, :cond_1

    .line 260
    new-instance v1, Lcom/google/android/finsky/protos/Common$SignedData;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/Common$SignedData;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->developerPurchaseInfo:Lcom/google/android/finsky/protos/Common$SignedData;

    .line 262
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->developerPurchaseInfo:Lcom/google/android/finsky/protos/Common$SignedData;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 266
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->preordered:Z

    .line 267
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->hasPreordered:Z

    goto :goto_0

    .line 271
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->hidden:Z

    .line 272
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->hasHidden:Z

    goto :goto_0

    .line 276
    :sswitch_9
    iget-object v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->rentalTerms:Lcom/google/android/finsky/protos/Common$RentalTerms;

    if-nez v1, :cond_2

    .line 277
    new-instance v1, Lcom/google/android/finsky/protos/Common$RentalTerms;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/Common$RentalTerms;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->rentalTerms:Lcom/google/android/finsky/protos/Common$RentalTerms;

    .line 279
    :cond_2
    iget-object v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->rentalTerms:Lcom/google/android/finsky/protos/Common$RentalTerms;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 283
    :sswitch_a
    iget-object v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->groupLicenseInfo:Lcom/google/android/finsky/protos/GroupLicense$GroupLicenseInfo;

    if-nez v1, :cond_3

    .line 284
    new-instance v1, Lcom/google/android/finsky/protos/GroupLicense$GroupLicenseInfo;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/GroupLicense$GroupLicenseInfo;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->groupLicenseInfo:Lcom/google/android/finsky/protos/GroupLicense$GroupLicenseInfo;

    .line 286
    :cond_3
    iget-object v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->groupLicenseInfo:Lcom/google/android/finsky/protos/GroupLicense$GroupLicenseInfo;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 290
    :sswitch_b
    iget-object v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->licensedDocumentInfo:Lcom/google/android/finsky/protos/Common$LicensedDocumentInfo;

    if-nez v1, :cond_4

    .line 291
    new-instance v1, Lcom/google/android/finsky/protos/Common$LicensedDocumentInfo;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/Common$LicensedDocumentInfo;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->licensedDocumentInfo:Lcom/google/android/finsky/protos/Common$LicensedDocumentInfo;

    .line 293
    :cond_4
    iget-object v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->licensedDocumentInfo:Lcom/google/android/finsky/protos/Common$LicensedDocumentInfo;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 297
    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->quantity:I

    .line 298
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->hasQuantity:Z

    goto/16 :goto_0

    .line 302
    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->libraryExpirationTimestampMsec:J

    .line 303
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->hasLibraryExpirationTimestampMsec:Z

    goto/16 :goto_0

    .line 307
    :sswitch_e
    iget-object v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->libraryVoucher:Lcom/google/android/finsky/protos/Voucher$LibraryVoucher;

    if-nez v1, :cond_5

    .line 308
    new-instance v1, Lcom/google/android/finsky/protos/Voucher$LibraryVoucher;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/Voucher$LibraryVoucher;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->libraryVoucher:Lcom/google/android/finsky/protos/Voucher$LibraryVoucher;

    .line 310
    :cond_5
    iget-object v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->libraryVoucher:Lcom/google/android/finsky/protos/Voucher$LibraryVoucher;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 224
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x60 -> :sswitch_c
        0x70 -> :sswitch_d
        0x7a -> :sswitch_e
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 7
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const-wide/16 v4, 0x0

    .line 111
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->hasInitiationTimestampMsec:Z

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->initiationTimestampMsec:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_1

    .line 112
    :cond_0
    iget-wide v0, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->initiationTimestampMsec:J

    invoke-virtual {p1, v6, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 114
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->hasValidUntilTimestampMsec:Z

    if-nez v0, :cond_2

    iget-wide v0, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->validUntilTimestampMsec:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_3

    .line 115
    :cond_2
    const/4 v0, 0x2

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->validUntilTimestampMsec:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 117
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->hasAutoRenewing:Z

    if-nez v0, :cond_4

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->autoRenewing:Z

    if-eqz v0, :cond_5

    .line 118
    :cond_4
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->autoRenewing:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 120
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->hasRefundTimeoutTimestampMsec:Z

    if-nez v0, :cond_6

    iget-wide v0, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->refundTimeoutTimestampMsec:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_7

    .line 121
    :cond_6
    const/4 v0, 0x4

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->refundTimeoutTimestampMsec:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 123
    :cond_7
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->hasPostDeliveryRefundWindowMsec:Z

    if-nez v0, :cond_8

    iget-wide v0, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->postDeliveryRefundWindowMsec:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_9

    .line 124
    :cond_8
    const/4 v0, 0x5

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->postDeliveryRefundWindowMsec:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 126
    :cond_9
    iget-object v0, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->developerPurchaseInfo:Lcom/google/android/finsky/protos/Common$SignedData;

    if-eqz v0, :cond_a

    .line 127
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->developerPurchaseInfo:Lcom/google/android/finsky/protos/Common$SignedData;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 129
    :cond_a
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->hasPreordered:Z

    if-nez v0, :cond_b

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->preordered:Z

    if-eqz v0, :cond_c

    .line 130
    :cond_b
    const/4 v0, 0x7

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->preordered:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 132
    :cond_c
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->hasHidden:Z

    if-nez v0, :cond_d

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->hidden:Z

    if-eqz v0, :cond_e

    .line 133
    :cond_d
    const/16 v0, 0x8

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->hidden:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 135
    :cond_e
    iget-object v0, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->rentalTerms:Lcom/google/android/finsky/protos/Common$RentalTerms;

    if-eqz v0, :cond_f

    .line 136
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->rentalTerms:Lcom/google/android/finsky/protos/Common$RentalTerms;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 138
    :cond_f
    iget-object v0, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->groupLicenseInfo:Lcom/google/android/finsky/protos/GroupLicense$GroupLicenseInfo;

    if-eqz v0, :cond_10

    .line 139
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->groupLicenseInfo:Lcom/google/android/finsky/protos/GroupLicense$GroupLicenseInfo;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 141
    :cond_10
    iget-object v0, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->licensedDocumentInfo:Lcom/google/android/finsky/protos/Common$LicensedDocumentInfo;

    if-eqz v0, :cond_11

    .line 142
    const/16 v0, 0xb

    iget-object v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->licensedDocumentInfo:Lcom/google/android/finsky/protos/Common$LicensedDocumentInfo;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 144
    :cond_11
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->hasQuantity:Z

    if-nez v0, :cond_12

    iget v0, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->quantity:I

    if-eq v0, v6, :cond_13

    .line 145
    :cond_12
    const/16 v0, 0xc

    iget v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->quantity:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 147
    :cond_13
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->hasLibraryExpirationTimestampMsec:Z

    if-nez v0, :cond_14

    iget-wide v0, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->libraryExpirationTimestampMsec:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_15

    .line 148
    :cond_14
    const/16 v0, 0xe

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->libraryExpirationTimestampMsec:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 150
    :cond_15
    iget-object v0, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->libraryVoucher:Lcom/google/android/finsky/protos/Voucher$LibraryVoucher;

    if-eqz v0, :cond_16

    .line 151
    const/16 v0, 0xf

    iget-object v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->libraryVoucher:Lcom/google/android/finsky/protos/Voucher$LibraryVoucher;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 153
    :cond_16
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 154
    return-void
.end method
