.class public final Lcom/google/android/finsky/protos/DocumentV2$CallToAction;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DocumentV2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/DocumentV2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CallToAction"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/finsky/protos/DocumentV2$CallToAction;


# instance fields
.field public buttonIcon:Lcom/google/android/finsky/protos/Common$Image;

.field public buttonText:Ljava/lang/String;

.field public dismissalUrl:Ljava/lang/String;

.field public hasButtonText:Z

.field public hasDismissalUrl:Z

.field public hasType:Z

.field public link:Lcom/google/android/finsky/protos/DocAnnotations$Link;

.field public type:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5542
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 5543
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocumentV2$CallToAction;->clear()Lcom/google/android/finsky/protos/DocumentV2$CallToAction;

    .line 5544
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/finsky/protos/DocumentV2$CallToAction;
    .locals 2

    .prologue
    .line 5513
    sget-object v0, Lcom/google/android/finsky/protos/DocumentV2$CallToAction;->_emptyArray:[Lcom/google/android/finsky/protos/DocumentV2$CallToAction;

    if-nez v0, :cond_1

    .line 5514
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 5516
    :try_start_0
    sget-object v0, Lcom/google/android/finsky/protos/DocumentV2$CallToAction;->_emptyArray:[Lcom/google/android/finsky/protos/DocumentV2$CallToAction;

    if-nez v0, :cond_0

    .line 5517
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/finsky/protos/DocumentV2$CallToAction;

    sput-object v0, Lcom/google/android/finsky/protos/DocumentV2$CallToAction;->_emptyArray:[Lcom/google/android/finsky/protos/DocumentV2$CallToAction;

    .line 5519
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5521
    :cond_1
    sget-object v0, Lcom/google/android/finsky/protos/DocumentV2$CallToAction;->_emptyArray:[Lcom/google/android/finsky/protos/DocumentV2$CallToAction;

    return-object v0

    .line 5519
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/DocumentV2$CallToAction;
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 5547
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/finsky/protos/DocumentV2$CallToAction;->type:I

    .line 5548
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$CallToAction;->hasType:Z

    .line 5549
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$CallToAction;->buttonText:Ljava/lang/String;

    .line 5550
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$CallToAction;->hasButtonText:Z

    .line 5551
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$CallToAction;->buttonIcon:Lcom/google/android/finsky/protos/Common$Image;

    .line 5552
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$CallToAction;->dismissalUrl:Ljava/lang/String;

    .line 5553
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$CallToAction;->hasDismissalUrl:Z

    .line 5554
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$CallToAction;->link:Lcom/google/android/finsky/protos/DocAnnotations$Link;

    .line 5555
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/DocumentV2$CallToAction;->cachedSize:I

    .line 5556
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 5582
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 5583
    .local v0, "size":I
    iget v1, p0, Lcom/google/android/finsky/protos/DocumentV2$CallToAction;->type:I

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$CallToAction;->hasType:Z

    if-eqz v1, :cond_1

    .line 5584
    :cond_0
    iget v1, p0, Lcom/google/android/finsky/protos/DocumentV2$CallToAction;->type:I

    invoke-static {v2, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5587
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$CallToAction;->hasButtonText:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$CallToAction;->buttonText:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 5588
    :cond_2
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$CallToAction;->buttonText:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5591
    :cond_3
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$CallToAction;->buttonIcon:Lcom/google/android/finsky/protos/Common$Image;

    if-eqz v1, :cond_4

    .line 5592
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$CallToAction;->buttonIcon:Lcom/google/android/finsky/protos/Common$Image;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5595
    :cond_4
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$CallToAction;->hasDismissalUrl:Z

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$CallToAction;->dismissalUrl:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 5596
    :cond_5
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$CallToAction;->dismissalUrl:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5599
    :cond_6
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$CallToAction;->link:Lcom/google/android/finsky/protos/DocAnnotations$Link;

    if-eqz v1, :cond_7

    .line 5600
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$CallToAction;->link:Lcom/google/android/finsky/protos/DocAnnotations$Link;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5603
    :cond_7
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocumentV2$CallToAction;
    .locals 4
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 5611
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 5612
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 5616
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 5617
    :sswitch_0
    return-object p0

    .line 5622
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 5623
    .local v1, "value":I
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 5627
    :pswitch_0
    iput v1, p0, Lcom/google/android/finsky/protos/DocumentV2$CallToAction;->type:I

    .line 5628
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/DocumentV2$CallToAction;->hasType:Z

    goto :goto_0

    .line 5634
    .end local v1    # "value":I
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$CallToAction;->buttonText:Ljava/lang/String;

    .line 5635
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/DocumentV2$CallToAction;->hasButtonText:Z

    goto :goto_0

    .line 5639
    :sswitch_3
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$CallToAction;->buttonIcon:Lcom/google/android/finsky/protos/Common$Image;

    if-nez v2, :cond_1

    .line 5640
    new-instance v2, Lcom/google/android/finsky/protos/Common$Image;

    invoke-direct {v2}, Lcom/google/android/finsky/protos/Common$Image;-><init>()V

    iput-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$CallToAction;->buttonIcon:Lcom/google/android/finsky/protos/Common$Image;

    .line 5642
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$CallToAction;->buttonIcon:Lcom/google/android/finsky/protos/Common$Image;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 5646
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$CallToAction;->dismissalUrl:Ljava/lang/String;

    .line 5647
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/DocumentV2$CallToAction;->hasDismissalUrl:Z

    goto :goto_0

    .line 5651
    :sswitch_5
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$CallToAction;->link:Lcom/google/android/finsky/protos/DocAnnotations$Link;

    if-nez v2, :cond_2

    .line 5652
    new-instance v2, Lcom/google/android/finsky/protos/DocAnnotations$Link;

    invoke-direct {v2}, Lcom/google/android/finsky/protos/DocAnnotations$Link;-><init>()V

    iput-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$CallToAction;->link:Lcom/google/android/finsky/protos/DocAnnotations$Link;

    .line 5654
    :cond_2
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$CallToAction;->link:Lcom/google/android/finsky/protos/DocAnnotations$Link;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 5612
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch

    .line 5623
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5507
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/DocumentV2$CallToAction;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocumentV2$CallToAction;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 5562
    iget v0, p0, Lcom/google/android/finsky/protos/DocumentV2$CallToAction;->type:I

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocumentV2$CallToAction;->hasType:Z

    if-eqz v0, :cond_1

    .line 5563
    :cond_0
    iget v0, p0, Lcom/google/android/finsky/protos/DocumentV2$CallToAction;->type:I

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 5565
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocumentV2$CallToAction;->hasButtonText:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$CallToAction;->buttonText:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 5566
    :cond_2
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$CallToAction;->buttonText:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 5568
    :cond_3
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$CallToAction;->buttonIcon:Lcom/google/android/finsky/protos/Common$Image;

    if-eqz v0, :cond_4

    .line 5569
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$CallToAction;->buttonIcon:Lcom/google/android/finsky/protos/Common$Image;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 5571
    :cond_4
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocumentV2$CallToAction;->hasDismissalUrl:Z

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$CallToAction;->dismissalUrl:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 5572
    :cond_5
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$CallToAction;->dismissalUrl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 5574
    :cond_6
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$CallToAction;->link:Lcom/google/android/finsky/protos/DocAnnotations$Link;

    if-eqz v0, :cond_7

    .line 5575
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$CallToAction;->link:Lcom/google/android/finsky/protos/DocAnnotations$Link;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 5577
    :cond_7
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 5578
    return-void
.end method
