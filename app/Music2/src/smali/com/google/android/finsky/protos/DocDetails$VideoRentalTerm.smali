.class public final Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DocDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/DocDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "VideoRentalTerm"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm$Term;
    }
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;


# instance fields
.field public hasOfferAbbreviation:Z

.field public hasOfferType:Z

.field public hasRentalHeader:Z

.field public offerAbbreviation:Ljava/lang/String;

.field public offerType:I

.field public rentalHeader:Ljava/lang/String;

.field public term:[Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm$Term;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3711
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 3712
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;->clear()Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;

    .line 3713
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;
    .locals 2

    .prologue
    .line 3685
    sget-object v0, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;->_emptyArray:[Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;

    if-nez v0, :cond_1

    .line 3686
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 3688
    :try_start_0
    sget-object v0, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;->_emptyArray:[Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;

    if-nez v0, :cond_0

    .line 3689
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;

    sput-object v0, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;->_emptyArray:[Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;

    .line 3691
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3693
    :cond_1
    sget-object v0, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;->_emptyArray:[Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;

    return-object v0

    .line 3691
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 3716
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;->offerType:I

    .line 3717
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;->hasOfferType:Z

    .line 3718
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;->offerAbbreviation:Ljava/lang/String;

    .line 3719
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;->hasOfferAbbreviation:Z

    .line 3720
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;->rentalHeader:Ljava/lang/String;

    .line 3721
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;->hasRentalHeader:Z

    .line 3722
    invoke-static {}, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm$Term;->emptyArray()[Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm$Term;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;->term:[Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm$Term;

    .line 3723
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;->cachedSize:I

    .line 3724
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 3752
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 3753
    .local v2, "size":I
    iget v3, p0, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;->offerType:I

    if-ne v3, v4, :cond_0

    iget-boolean v3, p0, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;->hasOfferType:Z

    if-eqz v3, :cond_1

    .line 3754
    :cond_0
    iget v3, p0, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;->offerType:I

    invoke-static {v4, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    .line 3757
    :cond_1
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;->hasOfferAbbreviation:Z

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;->offerAbbreviation:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 3758
    :cond_2
    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;->offerAbbreviation:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 3761
    :cond_3
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;->hasRentalHeader:Z

    if-nez v3, :cond_4

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;->rentalHeader:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 3762
    :cond_4
    const/4 v3, 0x3

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;->rentalHeader:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 3765
    :cond_5
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;->term:[Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm$Term;

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;->term:[Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm$Term;

    array-length v3, v3

    if-lez v3, :cond_7

    .line 3766
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;->term:[Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm$Term;

    array-length v3, v3

    if-ge v1, v3, :cond_7

    .line 3767
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;->term:[Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm$Term;

    aget-object v0, v3, v1

    .line 3768
    .local v0, "element":Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm$Term;
    if-eqz v0, :cond_6

    .line 3769
    const/4 v3, 0x4

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeGroupSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 3766
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 3774
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm$Term;
    .end local v1    # "i":I
    :cond_7
    return v2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;
    .locals 9
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x1

    const/4 v5, 0x0

    .line 3782
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 3783
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 3787
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v6

    if-nez v6, :cond_0

    .line 3788
    :sswitch_0
    return-object p0

    .line 3793
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v4

    .line 3794
    .local v4, "value":I
    packed-switch v4, :pswitch_data_0

    goto :goto_0

    .line 3807
    :pswitch_0
    iput v4, p0, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;->offerType:I

    .line 3808
    iput-boolean v7, p0, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;->hasOfferType:Z

    goto :goto_0

    .line 3814
    .end local v4    # "value":I
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;->offerAbbreviation:Ljava/lang/String;

    .line 3815
    iput-boolean v7, p0, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;->hasOfferAbbreviation:Z

    goto :goto_0

    .line 3819
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;->rentalHeader:Ljava/lang/String;

    .line 3820
    iput-boolean v7, p0, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;->hasRentalHeader:Z

    goto :goto_0

    .line 3824
    :sswitch_4
    const/16 v6, 0x23

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 3826
    .local v0, "arrayLength":I
    iget-object v6, p0, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;->term:[Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm$Term;

    if-nez v6, :cond_2

    move v1, v5

    .line 3827
    .local v1, "i":I
    :goto_1
    add-int v6, v1, v0

    new-array v2, v6, [Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm$Term;

    .line 3829
    .local v2, "newArray":[Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm$Term;
    if-eqz v1, :cond_1

    .line 3830
    iget-object v6, p0, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;->term:[Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm$Term;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3832
    :cond_1
    :goto_2
    array-length v6, v2

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_3

    .line 3833
    new-instance v6, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm$Term;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm$Term;-><init>()V

    aput-object v6, v2, v1

    .line 3834
    aget-object v6, v2, v1

    invoke-virtual {p1, v6, v8}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readGroup(Lcom/google/protobuf/nano/MessageNano;I)V

    .line 3835
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 3832
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 3826
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm$Term;
    :cond_2
    iget-object v6, p0, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;->term:[Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm$Term;

    array-length v1, v6

    goto :goto_1

    .line 3838
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm$Term;
    :cond_3
    new-instance v6, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm$Term;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm$Term;-><init>()V

    aput-object v6, v2, v1

    .line 3839
    aget-object v6, v2, v1

    invoke-virtual {p1, v6, v8}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readGroup(Lcom/google/protobuf/nano/MessageNano;I)V

    .line 3840
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;->term:[Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm$Term;

    goto :goto_0

    .line 3783
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x23 -> :sswitch_4
    .end sparse-switch

    .line 3794
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3574
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 3730
    iget v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;->offerType:I

    if-ne v2, v3, :cond_0

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;->hasOfferType:Z

    if-eqz v2, :cond_1

    .line 3731
    :cond_0
    iget v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;->offerType:I

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 3733
    :cond_1
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;->hasOfferAbbreviation:Z

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;->offerAbbreviation:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 3734
    :cond_2
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;->offerAbbreviation:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 3736
    :cond_3
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;->hasRentalHeader:Z

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;->rentalHeader:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 3737
    :cond_4
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;->rentalHeader:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 3739
    :cond_5
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;->term:[Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm$Term;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;->term:[Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm$Term;

    array-length v2, v2

    if-lez v2, :cond_7

    .line 3740
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;->term:[Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm$Term;

    array-length v2, v2

    if-ge v1, v2, :cond_7

    .line 3741
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;->term:[Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm$Term;

    aget-object v0, v2, v1

    .line 3742
    .local v0, "element":Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm$Term;
    if-eqz v0, :cond_6

    .line 3743
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeGroup(ILcom/google/protobuf/nano/MessageNano;)V

    .line 3740
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 3747
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm$Term;
    .end local v1    # "i":I
    :cond_7
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 3748
    return-void
.end method
