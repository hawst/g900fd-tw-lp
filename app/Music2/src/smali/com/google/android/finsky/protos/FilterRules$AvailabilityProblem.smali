.class public final Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;
.super Lcom/google/protobuf/nano/MessageNano;
.source "FilterRules.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/FilterRules;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AvailabilityProblem"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;


# instance fields
.field public hasProblemType:Z

.field public missingValue:[Ljava/lang/String;

.field public problemType:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1114
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 1115
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;->clear()Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;

    .line 1116
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;
    .locals 2

    .prologue
    .line 1096
    sget-object v0, Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;->_emptyArray:[Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;

    if-nez v0, :cond_1

    .line 1097
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 1099
    :try_start_0
    sget-object v0, Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;->_emptyArray:[Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;

    if-nez v0, :cond_0

    .line 1100
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;

    sput-object v0, Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;->_emptyArray:[Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;

    .line 1102
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1104
    :cond_1
    sget-object v0, Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;->_emptyArray:[Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;

    return-object v0

    .line 1102
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;
    .locals 1

    .prologue
    .line 1119
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;->problemType:I

    .line 1120
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;->hasProblemType:Z

    .line 1121
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;->missingValue:[Ljava/lang/String;

    .line 1122
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;->cachedSize:I

    .line 1123
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 1145
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v4

    .line 1146
    .local v4, "size":I
    iget v5, p0, Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;->problemType:I

    if-ne v5, v6, :cond_0

    iget-boolean v5, p0, Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;->hasProblemType:Z

    if-eqz v5, :cond_1

    .line 1147
    :cond_0
    iget v5, p0, Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;->problemType:I

    invoke-static {v6, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v5

    add-int/2addr v4, v5

    .line 1150
    :cond_1
    iget-object v5, p0, Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;->missingValue:[Ljava/lang/String;

    if-eqz v5, :cond_4

    iget-object v5, p0, Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;->missingValue:[Ljava/lang/String;

    array-length v5, v5

    if-lez v5, :cond_4

    .line 1151
    const/4 v0, 0x0

    .line 1152
    .local v0, "dataCount":I
    const/4 v1, 0x0

    .line 1153
    .local v1, "dataSize":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v5, p0, Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;->missingValue:[Ljava/lang/String;

    array-length v5, v5

    if-ge v3, v5, :cond_3

    .line 1154
    iget-object v5, p0, Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;->missingValue:[Ljava/lang/String;

    aget-object v2, v5, v3

    .line 1155
    .local v2, "element":Ljava/lang/String;
    if-eqz v2, :cond_2

    .line 1156
    add-int/lit8 v0, v0, 0x1

    .line 1157
    invoke-static {v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v1, v5

    .line 1153
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1161
    .end local v2    # "element":Ljava/lang/String;
    :cond_3
    add-int/2addr v4, v1

    .line 1162
    mul-int/lit8 v5, v0, 0x1

    add-int/2addr v4, v5

    .line 1164
    .end local v0    # "dataCount":I
    .end local v1    # "dataSize":I
    .end local v3    # "i":I
    :cond_4
    return v4
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;
    .locals 7
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 1172
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 1173
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 1177
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v6

    if-nez v6, :cond_0

    .line 1178
    :sswitch_0
    return-object p0

    .line 1183
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v4

    .line 1184
    .local v4, "value":I
    packed-switch v4, :pswitch_data_0

    goto :goto_0

    .line 1193
    :pswitch_0
    iput v4, p0, Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;->problemType:I

    .line 1194
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;->hasProblemType:Z

    goto :goto_0

    .line 1200
    .end local v4    # "value":I
    :sswitch_2
    const/16 v6, 0x12

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 1202
    .local v0, "arrayLength":I
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;->missingValue:[Ljava/lang/String;

    if-nez v6, :cond_2

    move v1, v5

    .line 1203
    .local v1, "i":I
    :goto_1
    add-int v6, v1, v0

    new-array v2, v6, [Ljava/lang/String;

    .line 1204
    .local v2, "newArray":[Ljava/lang/String;
    if-eqz v1, :cond_1

    .line 1205
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;->missingValue:[Ljava/lang/String;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1207
    :cond_1
    :goto_2
    array-length v6, v2

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_3

    .line 1208
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v1

    .line 1209
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 1207
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1202
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :cond_2
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;->missingValue:[Ljava/lang/String;

    array-length v1, v6

    goto :goto_1

    .line 1212
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Ljava/lang/String;
    :cond_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v1

    .line 1213
    iput-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;->missingValue:[Ljava/lang/String;

    goto :goto_0

    .line 1173
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch

    .line 1184
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1080
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 1129
    iget v2, p0, Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;->problemType:I

    if-ne v2, v3, :cond_0

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;->hasProblemType:Z

    if-eqz v2, :cond_1

    .line 1130
    :cond_0
    iget v2, p0, Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;->problemType:I

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1132
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;->missingValue:[Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;->missingValue:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_3

    .line 1133
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;->missingValue:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_3

    .line 1134
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;->missingValue:[Ljava/lang/String;

    aget-object v0, v2, v1

    .line 1135
    .local v0, "element":Ljava/lang/String;
    if-eqz v0, :cond_2

    .line 1136
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1133
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1140
    .end local v0    # "element":Ljava/lang/String;
    .end local v1    # "i":I
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 1141
    return-void
.end method
