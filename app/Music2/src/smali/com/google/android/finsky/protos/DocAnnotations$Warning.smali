.class public final Lcom/google/android/finsky/protos/DocAnnotations$Warning;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DocAnnotations.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/DocAnnotations;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Warning"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/finsky/protos/DocAnnotations$Warning;


# instance fields
.field public hasLocalizedMessage:Z

.field public localizedMessage:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 823
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 824
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocAnnotations$Warning;->clear()Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    .line 825
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/finsky/protos/DocAnnotations$Warning;
    .locals 2

    .prologue
    .line 808
    sget-object v0, Lcom/google/android/finsky/protos/DocAnnotations$Warning;->_emptyArray:[Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    if-nez v0, :cond_1

    .line 809
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 811
    :try_start_0
    sget-object v0, Lcom/google/android/finsky/protos/DocAnnotations$Warning;->_emptyArray:[Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    if-nez v0, :cond_0

    .line 812
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    sput-object v0, Lcom/google/android/finsky/protos/DocAnnotations$Warning;->_emptyArray:[Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    .line 814
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 816
    :cond_1
    sget-object v0, Lcom/google/android/finsky/protos/DocAnnotations$Warning;->_emptyArray:[Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    return-object v0

    .line 814
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/DocAnnotations$Warning;
    .locals 1

    .prologue
    .line 828
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$Warning;->localizedMessage:Ljava/lang/String;

    .line 829
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$Warning;->hasLocalizedMessage:Z

    .line 830
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$Warning;->cachedSize:I

    .line 831
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 845
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 846
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocAnnotations$Warning;->hasLocalizedMessage:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocAnnotations$Warning;->localizedMessage:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 847
    :cond_0
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocAnnotations$Warning;->localizedMessage:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 850
    :cond_1
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocAnnotations$Warning;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 858
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 859
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 863
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 864
    :sswitch_0
    return-object p0

    .line 869
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocAnnotations$Warning;->localizedMessage:Ljava/lang/String;

    .line 870
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocAnnotations$Warning;->hasLocalizedMessage:Z

    goto :goto_0

    .line 859
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 802
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/DocAnnotations$Warning;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 837
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$Warning;->hasLocalizedMessage:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$Warning;->localizedMessage:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 838
    :cond_0
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocAnnotations$Warning;->localizedMessage:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 840
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 841
    return-void
.end method
