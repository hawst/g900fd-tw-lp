.class public final Lcom/google/android/finsky/protos/Common$MonthAndDay;
.super Lcom/google/protobuf/nano/MessageNano;
.source "Common.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/Common;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "MonthAndDay"
.end annotation


# instance fields
.field public day:I

.field public hasDay:Z

.field public hasMonth:Z

.field public month:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2206
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 2207
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Common$MonthAndDay;->clear()Lcom/google/android/finsky/protos/Common$MonthAndDay;

    .line 2208
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/Common$MonthAndDay;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2211
    iput v0, p0, Lcom/google/android/finsky/protos/Common$MonthAndDay;->month:I

    .line 2212
    iput-boolean v0, p0, Lcom/google/android/finsky/protos/Common$MonthAndDay;->hasMonth:Z

    .line 2213
    iput v0, p0, Lcom/google/android/finsky/protos/Common$MonthAndDay;->day:I

    .line 2214
    iput-boolean v0, p0, Lcom/google/android/finsky/protos/Common$MonthAndDay;->hasDay:Z

    .line 2215
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Common$MonthAndDay;->cachedSize:I

    .line 2216
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 2233
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 2234
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Common$MonthAndDay;->hasMonth:Z

    if-nez v1, :cond_0

    iget v1, p0, Lcom/google/android/finsky/protos/Common$MonthAndDay;->month:I

    if-eqz v1, :cond_1

    .line 2235
    :cond_0
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/finsky/protos/Common$MonthAndDay;->month:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeUInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2238
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Common$MonthAndDay;->hasDay:Z

    if-nez v1, :cond_2

    iget v1, p0, Lcom/google/android/finsky/protos/Common$MonthAndDay;->day:I

    if-eqz v1, :cond_3

    .line 2239
    :cond_2
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/finsky/protos/Common$MonthAndDay;->day:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeUInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2242
    :cond_3
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Common$MonthAndDay;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 2250
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 2251
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 2255
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2256
    :sswitch_0
    return-object p0

    .line 2261
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readUInt32()I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/protos/Common$MonthAndDay;->month:I

    .line 2262
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Common$MonthAndDay;->hasMonth:Z

    goto :goto_0

    .line 2266
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readUInt32()I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/protos/Common$MonthAndDay;->day:I

    .line 2267
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Common$MonthAndDay;->hasDay:Z

    goto :goto_0

    .line 2251
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2181
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/Common$MonthAndDay;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Common$MonthAndDay;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2222
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Common$MonthAndDay;->hasMonth:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/finsky/protos/Common$MonthAndDay;->month:I

    if-eqz v0, :cond_1

    .line 2223
    :cond_0
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/finsky/protos/Common$MonthAndDay;->month:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeUInt32(II)V

    .line 2225
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Common$MonthAndDay;->hasDay:Z

    if-nez v0, :cond_2

    iget v0, p0, Lcom/google/android/finsky/protos/Common$MonthAndDay;->day:I

    if-eqz v0, :cond_3

    .line 2226
    :cond_2
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/finsky/protos/Common$MonthAndDay;->day:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeUInt32(II)V

    .line 2228
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 2229
    return-void
.end method
