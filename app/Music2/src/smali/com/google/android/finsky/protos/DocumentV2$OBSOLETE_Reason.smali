.class public final Lcom/google/android/finsky/protos/DocumentV2$OBSOLETE_Reason;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DocumentV2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/DocumentV2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "OBSOLETE_Reason"
.end annotation


# instance fields
.field public briefReason:Ljava/lang/String;

.field public hasBriefReason:Z

.field public hasOBSOLETEDetailedReason:Z

.field public hasUniqueId:Z

.field public oBSOLETEDetailedReason:Ljava/lang/String;

.field public uniqueId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5267
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 5268
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocumentV2$OBSOLETE_Reason;->clear()Lcom/google/android/finsky/protos/DocumentV2$OBSOLETE_Reason;

    .line 5269
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/DocumentV2$OBSOLETE_Reason;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 5272
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$OBSOLETE_Reason;->briefReason:Ljava/lang/String;

    .line 5273
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$OBSOLETE_Reason;->hasBriefReason:Z

    .line 5274
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$OBSOLETE_Reason;->oBSOLETEDetailedReason:Ljava/lang/String;

    .line 5275
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$OBSOLETE_Reason;->hasOBSOLETEDetailedReason:Z

    .line 5276
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$OBSOLETE_Reason;->uniqueId:Ljava/lang/String;

    .line 5277
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$OBSOLETE_Reason;->hasUniqueId:Z

    .line 5278
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/DocumentV2$OBSOLETE_Reason;->cachedSize:I

    .line 5279
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 5299
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 5300
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$OBSOLETE_Reason;->hasBriefReason:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$OBSOLETE_Reason;->briefReason:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 5301
    :cond_0
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$OBSOLETE_Reason;->briefReason:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5304
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$OBSOLETE_Reason;->hasOBSOLETEDetailedReason:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$OBSOLETE_Reason;->oBSOLETEDetailedReason:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 5305
    :cond_2
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$OBSOLETE_Reason;->oBSOLETEDetailedReason:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5308
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$OBSOLETE_Reason;->hasUniqueId:Z

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$OBSOLETE_Reason;->uniqueId:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 5309
    :cond_4
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$OBSOLETE_Reason;->uniqueId:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5312
    :cond_5
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocumentV2$OBSOLETE_Reason;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 5320
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 5321
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 5325
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 5326
    :sswitch_0
    return-object p0

    .line 5331
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$OBSOLETE_Reason;->briefReason:Ljava/lang/String;

    .line 5332
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV2$OBSOLETE_Reason;->hasBriefReason:Z

    goto :goto_0

    .line 5336
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$OBSOLETE_Reason;->oBSOLETEDetailedReason:Ljava/lang/String;

    .line 5337
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV2$OBSOLETE_Reason;->hasOBSOLETEDetailedReason:Z

    goto :goto_0

    .line 5341
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$OBSOLETE_Reason;->uniqueId:Ljava/lang/String;

    .line 5342
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV2$OBSOLETE_Reason;->hasUniqueId:Z

    goto :goto_0

    .line 5321
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5238
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/DocumentV2$OBSOLETE_Reason;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocumentV2$OBSOLETE_Reason;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5285
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocumentV2$OBSOLETE_Reason;->hasBriefReason:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$OBSOLETE_Reason;->briefReason:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 5286
    :cond_0
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$OBSOLETE_Reason;->briefReason:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 5288
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocumentV2$OBSOLETE_Reason;->hasOBSOLETEDetailedReason:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$OBSOLETE_Reason;->oBSOLETEDetailedReason:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 5289
    :cond_2
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$OBSOLETE_Reason;->oBSOLETEDetailedReason:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 5291
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocumentV2$OBSOLETE_Reason;->hasUniqueId:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$OBSOLETE_Reason;->uniqueId:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 5292
    :cond_4
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$OBSOLETE_Reason;->uniqueId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 5294
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 5295
    return-void
.end method
