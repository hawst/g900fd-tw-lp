.class public final Lcom/google/android/finsky/protos/Common$SeasonalSubscriptionInfo;
.super Lcom/google/protobuf/nano/MessageNano;
.source "Common.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/Common;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SeasonalSubscriptionInfo"
.end annotation


# instance fields
.field public periodEnd:Lcom/google/android/finsky/protos/Common$MonthAndDay;

.field public periodStart:Lcom/google/android/finsky/protos/Common$MonthAndDay;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2309
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 2310
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Common$SeasonalSubscriptionInfo;->clear()Lcom/google/android/finsky/protos/Common$SeasonalSubscriptionInfo;

    .line 2311
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/Common$SeasonalSubscriptionInfo;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2314
    iput-object v0, p0, Lcom/google/android/finsky/protos/Common$SeasonalSubscriptionInfo;->periodStart:Lcom/google/android/finsky/protos/Common$MonthAndDay;

    .line 2315
    iput-object v0, p0, Lcom/google/android/finsky/protos/Common$SeasonalSubscriptionInfo;->periodEnd:Lcom/google/android/finsky/protos/Common$MonthAndDay;

    .line 2316
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Common$SeasonalSubscriptionInfo;->cachedSize:I

    .line 2317
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 2334
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 2335
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$SeasonalSubscriptionInfo;->periodStart:Lcom/google/android/finsky/protos/Common$MonthAndDay;

    if-eqz v1, :cond_0

    .line 2336
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$SeasonalSubscriptionInfo;->periodStart:Lcom/google/android/finsky/protos/Common$MonthAndDay;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2339
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$SeasonalSubscriptionInfo;->periodEnd:Lcom/google/android/finsky/protos/Common$MonthAndDay;

    if-eqz v1, :cond_1

    .line 2340
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$SeasonalSubscriptionInfo;->periodEnd:Lcom/google/android/finsky/protos/Common$MonthAndDay;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2343
    :cond_1
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Common$SeasonalSubscriptionInfo;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2351
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 2352
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 2356
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2357
    :sswitch_0
    return-object p0

    .line 2362
    :sswitch_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$SeasonalSubscriptionInfo;->periodStart:Lcom/google/android/finsky/protos/Common$MonthAndDay;

    if-nez v1, :cond_1

    .line 2363
    new-instance v1, Lcom/google/android/finsky/protos/Common$MonthAndDay;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/Common$MonthAndDay;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/Common$SeasonalSubscriptionInfo;->periodStart:Lcom/google/android/finsky/protos/Common$MonthAndDay;

    .line 2365
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$SeasonalSubscriptionInfo;->periodStart:Lcom/google/android/finsky/protos/Common$MonthAndDay;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 2369
    :sswitch_2
    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$SeasonalSubscriptionInfo;->periodEnd:Lcom/google/android/finsky/protos/Common$MonthAndDay;

    if-nez v1, :cond_2

    .line 2370
    new-instance v1, Lcom/google/android/finsky/protos/Common$MonthAndDay;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/Common$MonthAndDay;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/Common$SeasonalSubscriptionInfo;->periodEnd:Lcom/google/android/finsky/protos/Common$MonthAndDay;

    .line 2372
    :cond_2
    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$SeasonalSubscriptionInfo;->periodEnd:Lcom/google/android/finsky/protos/Common$MonthAndDay;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 2352
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2286
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/Common$SeasonalSubscriptionInfo;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Common$SeasonalSubscriptionInfo;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2323
    iget-object v0, p0, Lcom/google/android/finsky/protos/Common$SeasonalSubscriptionInfo;->periodStart:Lcom/google/android/finsky/protos/Common$MonthAndDay;

    if-eqz v0, :cond_0

    .line 2324
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$SeasonalSubscriptionInfo;->periodStart:Lcom/google/android/finsky/protos/Common$MonthAndDay;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2326
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/protos/Common$SeasonalSubscriptionInfo;->periodEnd:Lcom/google/android/finsky/protos/Common$MonthAndDay;

    if-eqz v0, :cond_1

    .line 2327
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$SeasonalSubscriptionInfo;->periodEnd:Lcom/google/android/finsky/protos/Common$MonthAndDay;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2329
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 2330
    return-void
.end method
