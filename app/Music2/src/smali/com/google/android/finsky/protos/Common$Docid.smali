.class public final Lcom/google/android/finsky/protos/Common$Docid;
.super Lcom/google/protobuf/nano/MessageNano;
.source "Common.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/Common;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Docid"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/finsky/protos/Common$Docid;


# instance fields
.field public backend:I

.field public backendDocid:Ljava/lang/String;

.field public hasBackend:Z

.field public hasBackendDocid:Z

.field public hasType:Z

.field public type:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 267
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 268
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Common$Docid;->clear()Lcom/google/android/finsky/protos/Common$Docid;

    .line 269
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/finsky/protos/Common$Docid;
    .locals 2

    .prologue
    .line 244
    sget-object v0, Lcom/google/android/finsky/protos/Common$Docid;->_emptyArray:[Lcom/google/android/finsky/protos/Common$Docid;

    if-nez v0, :cond_1

    .line 245
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 247
    :try_start_0
    sget-object v0, Lcom/google/android/finsky/protos/Common$Docid;->_emptyArray:[Lcom/google/android/finsky/protos/Common$Docid;

    if-nez v0, :cond_0

    .line 248
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/finsky/protos/Common$Docid;

    sput-object v0, Lcom/google/android/finsky/protos/Common$Docid;->_emptyArray:[Lcom/google/android/finsky/protos/Common$Docid;

    .line 250
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 252
    :cond_1
    sget-object v0, Lcom/google/android/finsky/protos/Common$Docid;->_emptyArray:[Lcom/google/android/finsky/protos/Common$Docid;

    return-object v0

    .line 250
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/Common$Docid;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 272
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Common$Docid;->backendDocid:Ljava/lang/String;

    .line 273
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Docid;->hasBackendDocid:Z

    .line 274
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Common$Docid;->type:I

    .line 275
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Docid;->hasType:Z

    .line 276
    iput v1, p0, Lcom/google/android/finsky/protos/Common$Docid;->backend:I

    .line 277
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Docid;->hasBackend:Z

    .line 278
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Common$Docid;->cachedSize:I

    .line 279
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 299
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 300
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Docid;->hasBackendDocid:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$Docid;->backendDocid:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 301
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$Docid;->backendDocid:Ljava/lang/String;

    invoke-static {v3, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 304
    :cond_1
    iget v1, p0, Lcom/google/android/finsky/protos/Common$Docid;->type:I

    if-ne v1, v3, :cond_2

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Docid;->hasType:Z

    if-eqz v1, :cond_3

    .line 305
    :cond_2
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/finsky/protos/Common$Docid;->type:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 308
    :cond_3
    iget v1, p0, Lcom/google/android/finsky/protos/Common$Docid;->backend:I

    if-nez v1, :cond_4

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Docid;->hasBackend:Z

    if-eqz v1, :cond_5

    .line 309
    :cond_4
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/finsky/protos/Common$Docid;->backend:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 312
    :cond_5
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Common$Docid;
    .locals 4
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 320
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 321
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 325
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 326
    :sswitch_0
    return-object p0

    .line 331
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/Common$Docid;->backendDocid:Ljava/lang/String;

    .line 332
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/Common$Docid;->hasBackendDocid:Z

    goto :goto_0

    .line 336
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 337
    .local v1, "value":I
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 370
    :pswitch_0
    iput v1, p0, Lcom/google/android/finsky/protos/Common$Docid;->type:I

    .line 371
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/Common$Docid;->hasType:Z

    goto :goto_0

    .line 377
    .end local v1    # "value":I
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 378
    .restart local v1    # "value":I
    packed-switch v1, :pswitch_data_1

    :pswitch_1
    goto :goto_0

    .line 390
    :pswitch_2
    iput v1, p0, Lcom/google/android/finsky/protos/Common$Docid;->backend:I

    .line 391
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/Common$Docid;->hasBackend:Z

    goto :goto_0

    .line 321
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch

    .line 337
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 378
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 238
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/Common$Docid;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Common$Docid;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 3
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 285
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Common$Docid;->hasBackendDocid:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/protos/Common$Docid;->backendDocid:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 286
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/protos/Common$Docid;->backendDocid:Ljava/lang/String;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 288
    :cond_1
    iget v0, p0, Lcom/google/android/finsky/protos/Common$Docid;->type:I

    if-ne v0, v2, :cond_2

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Common$Docid;->hasType:Z

    if-eqz v0, :cond_3

    .line 289
    :cond_2
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/finsky/protos/Common$Docid;->type:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 291
    :cond_3
    iget v0, p0, Lcom/google/android/finsky/protos/Common$Docid;->backend:I

    if-nez v0, :cond_4

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Common$Docid;->hasBackend:Z

    if-eqz v0, :cond_5

    .line 292
    :cond_4
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/android/finsky/protos/Common$Docid;->backend:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 294
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 295
    return-void
.end method
