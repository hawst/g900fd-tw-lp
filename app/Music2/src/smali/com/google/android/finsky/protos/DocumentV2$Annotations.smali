.class public final Lcom/google/android/finsky/protos/DocumentV2$Annotations;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DocumentV2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/DocumentV2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Annotations"
.end annotation


# instance fields
.field public attributionHtml:Ljava/lang/String;

.field public badgeForContentRating:Lcom/google/android/finsky/protos/DocAnnotations$Badge;

.field public badgeForCreator:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

.field public badgeForDoc:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

.field public creatorDoc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

.field public docBadgeContainer:[Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;

.field public hasAttributionHtml:Z

.field public hasOfferNote:Z

.field public hasPrivacyPolicyUrl:Z

.field public link:Lcom/google/android/finsky/protos/DocAnnotations$Link;

.field public oBSOLETEReason:Lcom/google/android/finsky/protos/DocumentV2$OBSOLETE_Reason;

.field public offerNote:Ljava/lang/String;

.field public optimalDeviceClassWarning:Lcom/google/android/finsky/protos/DocAnnotations$Warning;

.field public overflowLink:[Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;

.field public plusOneData:Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;

.field public privacyPolicyUrl:Ljava/lang/String;

.field public promotedDoc:[Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;

.field public purchaseHistoryDetails:Lcom/google/android/finsky/protos/DocAnnotations$PurchaseHistoryDetails;

.field public sectionBodyOfWork:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

.field public sectionCoreContent:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

.field public sectionCrossSell:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

.field public sectionMoreBy:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

.field public sectionPurchaseCrossSell:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

.field public sectionRateAndReview:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

.field public sectionRelated:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

.field public sectionRelatedDocType:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

.field public sectionSuggestForRating:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

.field public subscription:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

.field public suggestionReasons:Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;

.field public template:Lcom/google/android/finsky/protos/DocumentV2$Template;

.field public warning:[Lcom/google/android/finsky/protos/DocAnnotations$Warning;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 787
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 788
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->clear()Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    .line 789
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/DocumentV2$Annotations;
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 792
    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionRelated:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    .line 793
    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionRelatedDocType:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    .line 794
    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionMoreBy:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    .line 795
    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionBodyOfWork:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    .line 796
    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionCoreContent:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    .line 797
    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionCrossSell:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    .line 798
    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionPurchaseCrossSell:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    .line 799
    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionSuggestForRating:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    .line 800
    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionRateAndReview:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    .line 801
    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->plusOneData:Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;

    .line 802
    invoke-static {}, Lcom/google/android/finsky/protos/DocAnnotations$Warning;->emptyArray()[Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->warning:[Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    .line 803
    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->optimalDeviceClassWarning:Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    .line 804
    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->link:Lcom/google/android/finsky/protos/DocAnnotations$Link;

    .line 805
    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->template:Lcom/google/android/finsky/protos/DocumentV2$Template;

    .line 806
    invoke-static {}, Lcom/google/android/finsky/protos/DocAnnotations$Badge;->emptyArray()[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForCreator:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    .line 807
    invoke-static {}, Lcom/google/android/finsky/protos/DocAnnotations$Badge;->emptyArray()[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForDoc:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    .line 808
    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForContentRating:Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    .line 809
    invoke-static {}, Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;->emptyArray()[Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->docBadgeContainer:[Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;

    .line 810
    invoke-static {}, Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;->emptyArray()[Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->promotedDoc:[Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;

    .line 811
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->offerNote:Ljava/lang/String;

    .line 812
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->hasOfferNote:Z

    .line 813
    invoke-static {}, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->emptyArray()[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->subscription:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 814
    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->suggestionReasons:Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;

    .line 815
    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->oBSOLETEReason:Lcom/google/android/finsky/protos/DocumentV2$OBSOLETE_Reason;

    .line 816
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->privacyPolicyUrl:Ljava/lang/String;

    .line 817
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->hasPrivacyPolicyUrl:Z

    .line 818
    invoke-static {}, Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;->emptyArray()[Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->overflowLink:[Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;

    .line 819
    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->creatorDoc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 820
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->attributionHtml:Ljava/lang/String;

    .line 821
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->hasAttributionHtml:Z

    .line 822
    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->purchaseHistoryDetails:Lcom/google/android/finsky/protos/DocAnnotations$PurchaseHistoryDetails;

    .line 823
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->cachedSize:I

    .line 824
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 5

    .prologue
    .line 954
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 955
    .local v2, "size":I
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionRelated:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-eqz v3, :cond_0

    .line 956
    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionRelated:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 959
    :cond_0
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionMoreBy:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-eqz v3, :cond_1

    .line 960
    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionMoreBy:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 963
    :cond_1
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->plusOneData:Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;

    if-eqz v3, :cond_2

    .line 964
    const/4 v3, 0x3

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->plusOneData:Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 967
    :cond_2
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->warning:[Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->warning:[Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    array-length v3, v3

    if-lez v3, :cond_4

    .line 968
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->warning:[Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    array-length v3, v3

    if-ge v1, v3, :cond_4

    .line 969
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->warning:[Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    aget-object v0, v3, v1

    .line 970
    .local v0, "element":Lcom/google/android/finsky/protos/DocAnnotations$Warning;
    if-eqz v0, :cond_3

    .line 971
    const/4 v3, 0x4

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 968
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 976
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocAnnotations$Warning;
    .end local v1    # "i":I
    :cond_4
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionBodyOfWork:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-eqz v3, :cond_5

    .line 977
    const/4 v3, 0x5

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionBodyOfWork:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 980
    :cond_5
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionCoreContent:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-eqz v3, :cond_6

    .line 981
    const/4 v3, 0x6

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionCoreContent:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 984
    :cond_6
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->template:Lcom/google/android/finsky/protos/DocumentV2$Template;

    if-eqz v3, :cond_7

    .line 985
    const/4 v3, 0x7

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->template:Lcom/google/android/finsky/protos/DocumentV2$Template;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 988
    :cond_7
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForCreator:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    if-eqz v3, :cond_9

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForCreator:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    array-length v3, v3

    if-lez v3, :cond_9

    .line 989
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForCreator:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    array-length v3, v3

    if-ge v1, v3, :cond_9

    .line 990
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForCreator:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    aget-object v0, v3, v1

    .line 991
    .local v0, "element":Lcom/google/android/finsky/protos/DocAnnotations$Badge;
    if-eqz v0, :cond_8

    .line 992
    const/16 v3, 0x8

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 989
    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 997
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocAnnotations$Badge;
    .end local v1    # "i":I
    :cond_9
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForDoc:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    if-eqz v3, :cond_b

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForDoc:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    array-length v3, v3

    if-lez v3, :cond_b

    .line 998
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_2
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForDoc:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    array-length v3, v3

    if-ge v1, v3, :cond_b

    .line 999
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForDoc:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    aget-object v0, v3, v1

    .line 1000
    .restart local v0    # "element":Lcom/google/android/finsky/protos/DocAnnotations$Badge;
    if-eqz v0, :cond_a

    .line 1001
    const/16 v3, 0x9

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 998
    :cond_a
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1006
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocAnnotations$Badge;
    .end local v1    # "i":I
    :cond_b
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->link:Lcom/google/android/finsky/protos/DocAnnotations$Link;

    if-eqz v3, :cond_c

    .line 1007
    const/16 v3, 0xa

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->link:Lcom/google/android/finsky/protos/DocAnnotations$Link;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1010
    :cond_c
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionCrossSell:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-eqz v3, :cond_d

    .line 1011
    const/16 v3, 0xb

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionCrossSell:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1014
    :cond_d
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionRelatedDocType:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-eqz v3, :cond_e

    .line 1015
    const/16 v3, 0xc

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionRelatedDocType:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1018
    :cond_e
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->promotedDoc:[Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;

    if-eqz v3, :cond_10

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->promotedDoc:[Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;

    array-length v3, v3

    if-lez v3, :cond_10

    .line 1019
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_3
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->promotedDoc:[Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;

    array-length v3, v3

    if-ge v1, v3, :cond_10

    .line 1020
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->promotedDoc:[Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;

    aget-object v0, v3, v1

    .line 1021
    .local v0, "element":Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;
    if-eqz v0, :cond_f

    .line 1022
    const/16 v3, 0xd

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1019
    :cond_f
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 1027
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;
    .end local v1    # "i":I
    :cond_10
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->hasOfferNote:Z

    if-nez v3, :cond_11

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->offerNote:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_12

    .line 1028
    :cond_11
    const/16 v3, 0xe

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->offerNote:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1031
    :cond_12
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->subscription:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-eqz v3, :cond_14

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->subscription:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v3, v3

    if-lez v3, :cond_14

    .line 1032
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_4
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->subscription:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v3, v3

    if-ge v1, v3, :cond_14

    .line 1033
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->subscription:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    aget-object v0, v3, v1

    .line 1034
    .local v0, "element":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    if-eqz v0, :cond_13

    .line 1035
    const/16 v3, 0x10

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1032
    :cond_13
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 1040
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    .end local v1    # "i":I
    :cond_14
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->oBSOLETEReason:Lcom/google/android/finsky/protos/DocumentV2$OBSOLETE_Reason;

    if-eqz v3, :cond_15

    .line 1041
    const/16 v3, 0x11

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->oBSOLETEReason:Lcom/google/android/finsky/protos/DocumentV2$OBSOLETE_Reason;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1044
    :cond_15
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->hasPrivacyPolicyUrl:Z

    if-nez v3, :cond_16

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->privacyPolicyUrl:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_17

    .line 1045
    :cond_16
    const/16 v3, 0x12

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->privacyPolicyUrl:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1048
    :cond_17
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->suggestionReasons:Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;

    if-eqz v3, :cond_18

    .line 1049
    const/16 v3, 0x13

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->suggestionReasons:Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1052
    :cond_18
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->optimalDeviceClassWarning:Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    if-eqz v3, :cond_19

    .line 1053
    const/16 v3, 0x14

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->optimalDeviceClassWarning:Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1056
    :cond_19
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->docBadgeContainer:[Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;

    if-eqz v3, :cond_1b

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->docBadgeContainer:[Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;

    array-length v3, v3

    if-lez v3, :cond_1b

    .line 1057
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_5
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->docBadgeContainer:[Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;

    array-length v3, v3

    if-ge v1, v3, :cond_1b

    .line 1058
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->docBadgeContainer:[Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;

    aget-object v0, v3, v1

    .line 1059
    .local v0, "element":Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;
    if-eqz v0, :cond_1a

    .line 1060
    const/16 v3, 0x15

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1057
    :cond_1a
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 1065
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;
    .end local v1    # "i":I
    :cond_1b
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionSuggestForRating:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-eqz v3, :cond_1c

    .line 1066
    const/16 v3, 0x16

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionSuggestForRating:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1069
    :cond_1c
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionRateAndReview:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-eqz v3, :cond_1d

    .line 1070
    const/16 v3, 0x17

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionRateAndReview:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1073
    :cond_1d
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionPurchaseCrossSell:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-eqz v3, :cond_1e

    .line 1074
    const/16 v3, 0x18

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionPurchaseCrossSell:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1077
    :cond_1e
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->overflowLink:[Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;

    if-eqz v3, :cond_20

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->overflowLink:[Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;

    array-length v3, v3

    if-lez v3, :cond_20

    .line 1078
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_6
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->overflowLink:[Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;

    array-length v3, v3

    if-ge v1, v3, :cond_20

    .line 1079
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->overflowLink:[Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;

    aget-object v0, v3, v1

    .line 1080
    .local v0, "element":Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;
    if-eqz v0, :cond_1f

    .line 1081
    const/16 v3, 0x19

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1078
    :cond_1f
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 1086
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;
    .end local v1    # "i":I
    :cond_20
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->creatorDoc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-eqz v3, :cond_21

    .line 1087
    const/16 v3, 0x1a

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->creatorDoc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1090
    :cond_21
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->hasAttributionHtml:Z

    if-nez v3, :cond_22

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->attributionHtml:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_23

    .line 1091
    :cond_22
    const/16 v3, 0x1b

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->attributionHtml:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1094
    :cond_23
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->purchaseHistoryDetails:Lcom/google/android/finsky/protos/DocAnnotations$PurchaseHistoryDetails;

    if-eqz v3, :cond_24

    .line 1095
    const/16 v3, 0x1c

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->purchaseHistoryDetails:Lcom/google/android/finsky/protos/DocAnnotations$PurchaseHistoryDetails;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1098
    :cond_24
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForContentRating:Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    if-eqz v3, :cond_25

    .line 1099
    const/16 v3, 0x1d

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForContentRating:Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1102
    :cond_25
    return v2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocumentV2$Annotations;
    .locals 7
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v4, 0x0

    .line 1110
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 1111
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 1115
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1116
    :sswitch_0
    return-object p0

    .line 1121
    :sswitch_1
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionRelated:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-nez v5, :cond_1

    .line 1122
    new-instance v5, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionRelated:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    .line 1124
    :cond_1
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionRelated:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1128
    :sswitch_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionMoreBy:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-nez v5, :cond_2

    .line 1129
    new-instance v5, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionMoreBy:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    .line 1131
    :cond_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionMoreBy:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1135
    :sswitch_3
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->plusOneData:Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;

    if-nez v5, :cond_3

    .line 1136
    new-instance v5, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->plusOneData:Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;

    .line 1138
    :cond_3
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->plusOneData:Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1142
    :sswitch_4
    const/16 v5, 0x22

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 1144
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->warning:[Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    if-nez v5, :cond_5

    move v1, v4

    .line 1145
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    .line 1147
    .local v2, "newArray":[Lcom/google/android/finsky/protos/DocAnnotations$Warning;
    if-eqz v1, :cond_4

    .line 1148
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->warning:[Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1150
    :cond_4
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_6

    .line 1151
    new-instance v5, Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocAnnotations$Warning;-><init>()V

    aput-object v5, v2, v1

    .line 1152
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1153
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 1150
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1144
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocAnnotations$Warning;
    :cond_5
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->warning:[Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    array-length v1, v5

    goto :goto_1

    .line 1156
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/DocAnnotations$Warning;
    :cond_6
    new-instance v5, Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocAnnotations$Warning;-><init>()V

    aput-object v5, v2, v1

    .line 1157
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1158
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->warning:[Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    goto :goto_0

    .line 1162
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocAnnotations$Warning;
    :sswitch_5
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionBodyOfWork:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-nez v5, :cond_7

    .line 1163
    new-instance v5, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionBodyOfWork:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    .line 1165
    :cond_7
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionBodyOfWork:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1169
    :sswitch_6
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionCoreContent:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-nez v5, :cond_8

    .line 1170
    new-instance v5, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionCoreContent:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    .line 1172
    :cond_8
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionCoreContent:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1176
    :sswitch_7
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->template:Lcom/google/android/finsky/protos/DocumentV2$Template;

    if-nez v5, :cond_9

    .line 1177
    new-instance v5, Lcom/google/android/finsky/protos/DocumentV2$Template;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocumentV2$Template;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->template:Lcom/google/android/finsky/protos/DocumentV2$Template;

    .line 1179
    :cond_9
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->template:Lcom/google/android/finsky/protos/DocumentV2$Template;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1183
    :sswitch_8
    const/16 v5, 0x42

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 1185
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForCreator:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    if-nez v5, :cond_b

    move v1, v4

    .line 1186
    .restart local v1    # "i":I
    :goto_3
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    .line 1188
    .local v2, "newArray":[Lcom/google/android/finsky/protos/DocAnnotations$Badge;
    if-eqz v1, :cond_a

    .line 1189
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForCreator:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1191
    :cond_a
    :goto_4
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_c

    .line 1192
    new-instance v5, Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocAnnotations$Badge;-><init>()V

    aput-object v5, v2, v1

    .line 1193
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1194
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 1191
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 1185
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocAnnotations$Badge;
    :cond_b
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForCreator:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    array-length v1, v5

    goto :goto_3

    .line 1197
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/DocAnnotations$Badge;
    :cond_c
    new-instance v5, Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocAnnotations$Badge;-><init>()V

    aput-object v5, v2, v1

    .line 1198
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1199
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForCreator:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    goto/16 :goto_0

    .line 1203
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocAnnotations$Badge;
    :sswitch_9
    const/16 v5, 0x4a

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 1205
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForDoc:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    if-nez v5, :cond_e

    move v1, v4

    .line 1206
    .restart local v1    # "i":I
    :goto_5
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    .line 1208
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/DocAnnotations$Badge;
    if-eqz v1, :cond_d

    .line 1209
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForDoc:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1211
    :cond_d
    :goto_6
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_f

    .line 1212
    new-instance v5, Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocAnnotations$Badge;-><init>()V

    aput-object v5, v2, v1

    .line 1213
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1214
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 1211
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 1205
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocAnnotations$Badge;
    :cond_e
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForDoc:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    array-length v1, v5

    goto :goto_5

    .line 1217
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/DocAnnotations$Badge;
    :cond_f
    new-instance v5, Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocAnnotations$Badge;-><init>()V

    aput-object v5, v2, v1

    .line 1218
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1219
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForDoc:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    goto/16 :goto_0

    .line 1223
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocAnnotations$Badge;
    :sswitch_a
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->link:Lcom/google/android/finsky/protos/DocAnnotations$Link;

    if-nez v5, :cond_10

    .line 1224
    new-instance v5, Lcom/google/android/finsky/protos/DocAnnotations$Link;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocAnnotations$Link;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->link:Lcom/google/android/finsky/protos/DocAnnotations$Link;

    .line 1226
    :cond_10
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->link:Lcom/google/android/finsky/protos/DocAnnotations$Link;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1230
    :sswitch_b
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionCrossSell:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-nez v5, :cond_11

    .line 1231
    new-instance v5, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionCrossSell:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    .line 1233
    :cond_11
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionCrossSell:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1237
    :sswitch_c
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionRelatedDocType:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-nez v5, :cond_12

    .line 1238
    new-instance v5, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionRelatedDocType:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    .line 1240
    :cond_12
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionRelatedDocType:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1244
    :sswitch_d
    const/16 v5, 0x6a

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 1246
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->promotedDoc:[Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;

    if-nez v5, :cond_14

    move v1, v4

    .line 1247
    .restart local v1    # "i":I
    :goto_7
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;

    .line 1249
    .local v2, "newArray":[Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;
    if-eqz v1, :cond_13

    .line 1250
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->promotedDoc:[Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1252
    :cond_13
    :goto_8
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_15

    .line 1253
    new-instance v5, Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;-><init>()V

    aput-object v5, v2, v1

    .line 1254
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1255
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 1252
    add-int/lit8 v1, v1, 0x1

    goto :goto_8

    .line 1246
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;
    :cond_14
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->promotedDoc:[Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;

    array-length v1, v5

    goto :goto_7

    .line 1258
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;
    :cond_15
    new-instance v5, Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;-><init>()V

    aput-object v5, v2, v1

    .line 1259
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1260
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->promotedDoc:[Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;

    goto/16 :goto_0

    .line 1264
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;
    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->offerNote:Ljava/lang/String;

    .line 1265
    iput-boolean v6, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->hasOfferNote:Z

    goto/16 :goto_0

    .line 1269
    :sswitch_f
    const/16 v5, 0x82

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 1271
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->subscription:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-nez v5, :cond_17

    move v1, v4

    .line 1272
    .restart local v1    # "i":I
    :goto_9
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 1274
    .local v2, "newArray":[Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    if-eqz v1, :cond_16

    .line 1275
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->subscription:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1277
    :cond_16
    :goto_a
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_18

    .line 1278
    new-instance v5, Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocumentV2$DocV2;-><init>()V

    aput-object v5, v2, v1

    .line 1279
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1280
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 1277
    add-int/lit8 v1, v1, 0x1

    goto :goto_a

    .line 1271
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    :cond_17
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->subscription:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v1, v5

    goto :goto_9

    .line 1283
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    :cond_18
    new-instance v5, Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocumentV2$DocV2;-><init>()V

    aput-object v5, v2, v1

    .line 1284
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1285
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->subscription:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    goto/16 :goto_0

    .line 1289
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    :sswitch_10
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->oBSOLETEReason:Lcom/google/android/finsky/protos/DocumentV2$OBSOLETE_Reason;

    if-nez v5, :cond_19

    .line 1290
    new-instance v5, Lcom/google/android/finsky/protos/DocumentV2$OBSOLETE_Reason;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocumentV2$OBSOLETE_Reason;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->oBSOLETEReason:Lcom/google/android/finsky/protos/DocumentV2$OBSOLETE_Reason;

    .line 1292
    :cond_19
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->oBSOLETEReason:Lcom/google/android/finsky/protos/DocumentV2$OBSOLETE_Reason;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1296
    :sswitch_11
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->privacyPolicyUrl:Ljava/lang/String;

    .line 1297
    iput-boolean v6, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->hasPrivacyPolicyUrl:Z

    goto/16 :goto_0

    .line 1301
    :sswitch_12
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->suggestionReasons:Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;

    if-nez v5, :cond_1a

    .line 1302
    new-instance v5, Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->suggestionReasons:Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;

    .line 1304
    :cond_1a
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->suggestionReasons:Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1308
    :sswitch_13
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->optimalDeviceClassWarning:Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    if-nez v5, :cond_1b

    .line 1309
    new-instance v5, Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocAnnotations$Warning;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->optimalDeviceClassWarning:Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    .line 1311
    :cond_1b
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->optimalDeviceClassWarning:Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1315
    :sswitch_14
    const/16 v5, 0xaa

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 1317
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->docBadgeContainer:[Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;

    if-nez v5, :cond_1d

    move v1, v4

    .line 1318
    .restart local v1    # "i":I
    :goto_b
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;

    .line 1320
    .local v2, "newArray":[Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;
    if-eqz v1, :cond_1c

    .line 1321
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->docBadgeContainer:[Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1323
    :cond_1c
    :goto_c
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_1e

    .line 1324
    new-instance v5, Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;-><init>()V

    aput-object v5, v2, v1

    .line 1325
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1326
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 1323
    add-int/lit8 v1, v1, 0x1

    goto :goto_c

    .line 1317
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;
    :cond_1d
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->docBadgeContainer:[Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;

    array-length v1, v5

    goto :goto_b

    .line 1329
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;
    :cond_1e
    new-instance v5, Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;-><init>()V

    aput-object v5, v2, v1

    .line 1330
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1331
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->docBadgeContainer:[Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;

    goto/16 :goto_0

    .line 1335
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;
    :sswitch_15
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionSuggestForRating:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-nez v5, :cond_1f

    .line 1336
    new-instance v5, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionSuggestForRating:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    .line 1338
    :cond_1f
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionSuggestForRating:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1342
    :sswitch_16
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionRateAndReview:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-nez v5, :cond_20

    .line 1343
    new-instance v5, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionRateAndReview:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    .line 1345
    :cond_20
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionRateAndReview:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1349
    :sswitch_17
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionPurchaseCrossSell:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-nez v5, :cond_21

    .line 1350
    new-instance v5, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionPurchaseCrossSell:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    .line 1352
    :cond_21
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionPurchaseCrossSell:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1356
    :sswitch_18
    const/16 v5, 0xca

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 1358
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->overflowLink:[Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;

    if-nez v5, :cond_23

    move v1, v4

    .line 1359
    .restart local v1    # "i":I
    :goto_d
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;

    .line 1361
    .local v2, "newArray":[Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;
    if-eqz v1, :cond_22

    .line 1362
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->overflowLink:[Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1364
    :cond_22
    :goto_e
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_24

    .line 1365
    new-instance v5, Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;-><init>()V

    aput-object v5, v2, v1

    .line 1366
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1367
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 1364
    add-int/lit8 v1, v1, 0x1

    goto :goto_e

    .line 1358
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;
    :cond_23
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->overflowLink:[Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;

    array-length v1, v5

    goto :goto_d

    .line 1370
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;
    :cond_24
    new-instance v5, Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;-><init>()V

    aput-object v5, v2, v1

    .line 1371
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1372
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->overflowLink:[Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;

    goto/16 :goto_0

    .line 1376
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;
    :sswitch_19
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->creatorDoc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-nez v5, :cond_25

    .line 1377
    new-instance v5, Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocumentV2$DocV2;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->creatorDoc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 1379
    :cond_25
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->creatorDoc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1383
    :sswitch_1a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->attributionHtml:Ljava/lang/String;

    .line 1384
    iput-boolean v6, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->hasAttributionHtml:Z

    goto/16 :goto_0

    .line 1388
    :sswitch_1b
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->purchaseHistoryDetails:Lcom/google/android/finsky/protos/DocAnnotations$PurchaseHistoryDetails;

    if-nez v5, :cond_26

    .line 1389
    new-instance v5, Lcom/google/android/finsky/protos/DocAnnotations$PurchaseHistoryDetails;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocAnnotations$PurchaseHistoryDetails;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->purchaseHistoryDetails:Lcom/google/android/finsky/protos/DocAnnotations$PurchaseHistoryDetails;

    .line 1391
    :cond_26
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->purchaseHistoryDetails:Lcom/google/android/finsky/protos/DocAnnotations$PurchaseHistoryDetails;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1395
    :sswitch_1c
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForContentRating:Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    if-nez v5, :cond_27

    .line 1396
    new-instance v5, Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocAnnotations$Badge;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForContentRating:Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    .line 1398
    :cond_27
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForContentRating:Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1111
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
        0x72 -> :sswitch_e
        0x82 -> :sswitch_f
        0x8a -> :sswitch_10
        0x92 -> :sswitch_11
        0x9a -> :sswitch_12
        0xa2 -> :sswitch_13
        0xaa -> :sswitch_14
        0xb2 -> :sswitch_15
        0xba -> :sswitch_16
        0xc2 -> :sswitch_17
        0xca -> :sswitch_18
        0xd2 -> :sswitch_19
        0xda -> :sswitch_1a
        0xe2 -> :sswitch_1b
        0xea -> :sswitch_1c
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 683
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 830
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionRelated:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-eqz v2, :cond_0

    .line 831
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionRelated:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 833
    :cond_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionMoreBy:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-eqz v2, :cond_1

    .line 834
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionMoreBy:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 836
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->plusOneData:Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;

    if-eqz v2, :cond_2

    .line 837
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->plusOneData:Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 839
    :cond_2
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->warning:[Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->warning:[Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    array-length v2, v2

    if-lez v2, :cond_4

    .line 840
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->warning:[Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    array-length v2, v2

    if-ge v1, v2, :cond_4

    .line 841
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->warning:[Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    aget-object v0, v2, v1

    .line 842
    .local v0, "element":Lcom/google/android/finsky/protos/DocAnnotations$Warning;
    if-eqz v0, :cond_3

    .line 843
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 840
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 847
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocAnnotations$Warning;
    .end local v1    # "i":I
    :cond_4
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionBodyOfWork:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-eqz v2, :cond_5

    .line 848
    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionBodyOfWork:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 850
    :cond_5
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionCoreContent:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-eqz v2, :cond_6

    .line 851
    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionCoreContent:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 853
    :cond_6
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->template:Lcom/google/android/finsky/protos/DocumentV2$Template;

    if-eqz v2, :cond_7

    .line 854
    const/4 v2, 0x7

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->template:Lcom/google/android/finsky/protos/DocumentV2$Template;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 856
    :cond_7
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForCreator:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForCreator:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    array-length v2, v2

    if-lez v2, :cond_9

    .line 857
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForCreator:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    array-length v2, v2

    if-ge v1, v2, :cond_9

    .line 858
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForCreator:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    aget-object v0, v2, v1

    .line 859
    .local v0, "element":Lcom/google/android/finsky/protos/DocAnnotations$Badge;
    if-eqz v0, :cond_8

    .line 860
    const/16 v2, 0x8

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 857
    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 864
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocAnnotations$Badge;
    .end local v1    # "i":I
    :cond_9
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForDoc:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    if-eqz v2, :cond_b

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForDoc:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    array-length v2, v2

    if-lez v2, :cond_b

    .line 865
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_2
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForDoc:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    array-length v2, v2

    if-ge v1, v2, :cond_b

    .line 866
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForDoc:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    aget-object v0, v2, v1

    .line 867
    .restart local v0    # "element":Lcom/google/android/finsky/protos/DocAnnotations$Badge;
    if-eqz v0, :cond_a

    .line 868
    const/16 v2, 0x9

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 865
    :cond_a
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 872
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocAnnotations$Badge;
    .end local v1    # "i":I
    :cond_b
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->link:Lcom/google/android/finsky/protos/DocAnnotations$Link;

    if-eqz v2, :cond_c

    .line 873
    const/16 v2, 0xa

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->link:Lcom/google/android/finsky/protos/DocAnnotations$Link;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 875
    :cond_c
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionCrossSell:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-eqz v2, :cond_d

    .line 876
    const/16 v2, 0xb

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionCrossSell:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 878
    :cond_d
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionRelatedDocType:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-eqz v2, :cond_e

    .line 879
    const/16 v2, 0xc

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionRelatedDocType:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 881
    :cond_e
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->promotedDoc:[Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;

    if-eqz v2, :cond_10

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->promotedDoc:[Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;

    array-length v2, v2

    if-lez v2, :cond_10

    .line 882
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_3
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->promotedDoc:[Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;

    array-length v2, v2

    if-ge v1, v2, :cond_10

    .line 883
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->promotedDoc:[Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;

    aget-object v0, v2, v1

    .line 884
    .local v0, "element":Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;
    if-eqz v0, :cond_f

    .line 885
    const/16 v2, 0xd

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 882
    :cond_f
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 889
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;
    .end local v1    # "i":I
    :cond_10
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->hasOfferNote:Z

    if-nez v2, :cond_11

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->offerNote:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_12

    .line 890
    :cond_11
    const/16 v2, 0xe

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->offerNote:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 892
    :cond_12
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->subscription:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-eqz v2, :cond_14

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->subscription:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v2, v2

    if-lez v2, :cond_14

    .line 893
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_4
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->subscription:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v2, v2

    if-ge v1, v2, :cond_14

    .line 894
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->subscription:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    aget-object v0, v2, v1

    .line 895
    .local v0, "element":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    if-eqz v0, :cond_13

    .line 896
    const/16 v2, 0x10

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 893
    :cond_13
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 900
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    .end local v1    # "i":I
    :cond_14
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->oBSOLETEReason:Lcom/google/android/finsky/protos/DocumentV2$OBSOLETE_Reason;

    if-eqz v2, :cond_15

    .line 901
    const/16 v2, 0x11

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->oBSOLETEReason:Lcom/google/android/finsky/protos/DocumentV2$OBSOLETE_Reason;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 903
    :cond_15
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->hasPrivacyPolicyUrl:Z

    if-nez v2, :cond_16

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->privacyPolicyUrl:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_17

    .line 904
    :cond_16
    const/16 v2, 0x12

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->privacyPolicyUrl:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 906
    :cond_17
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->suggestionReasons:Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;

    if-eqz v2, :cond_18

    .line 907
    const/16 v2, 0x13

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->suggestionReasons:Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 909
    :cond_18
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->optimalDeviceClassWarning:Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    if-eqz v2, :cond_19

    .line 910
    const/16 v2, 0x14

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->optimalDeviceClassWarning:Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 912
    :cond_19
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->docBadgeContainer:[Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;

    if-eqz v2, :cond_1b

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->docBadgeContainer:[Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;

    array-length v2, v2

    if-lez v2, :cond_1b

    .line 913
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_5
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->docBadgeContainer:[Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;

    array-length v2, v2

    if-ge v1, v2, :cond_1b

    .line 914
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->docBadgeContainer:[Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;

    aget-object v0, v2, v1

    .line 915
    .local v0, "element":Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;
    if-eqz v0, :cond_1a

    .line 916
    const/16 v2, 0x15

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 913
    :cond_1a
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 920
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;
    .end local v1    # "i":I
    :cond_1b
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionSuggestForRating:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-eqz v2, :cond_1c

    .line 921
    const/16 v2, 0x16

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionSuggestForRating:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 923
    :cond_1c
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionRateAndReview:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-eqz v2, :cond_1d

    .line 924
    const/16 v2, 0x17

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionRateAndReview:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 926
    :cond_1d
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionPurchaseCrossSell:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-eqz v2, :cond_1e

    .line 927
    const/16 v2, 0x18

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionPurchaseCrossSell:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 929
    :cond_1e
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->overflowLink:[Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;

    if-eqz v2, :cond_20

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->overflowLink:[Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;

    array-length v2, v2

    if-lez v2, :cond_20

    .line 930
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_6
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->overflowLink:[Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;

    array-length v2, v2

    if-ge v1, v2, :cond_20

    .line 931
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->overflowLink:[Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;

    aget-object v0, v2, v1

    .line 932
    .local v0, "element":Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;
    if-eqz v0, :cond_1f

    .line 933
    const/16 v2, 0x19

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 930
    :cond_1f
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 937
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;
    .end local v1    # "i":I
    :cond_20
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->creatorDoc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-eqz v2, :cond_21

    .line 938
    const/16 v2, 0x1a

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->creatorDoc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 940
    :cond_21
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->hasAttributionHtml:Z

    if-nez v2, :cond_22

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->attributionHtml:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_23

    .line 941
    :cond_22
    const/16 v2, 0x1b

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->attributionHtml:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 943
    :cond_23
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->purchaseHistoryDetails:Lcom/google/android/finsky/protos/DocAnnotations$PurchaseHistoryDetails;

    if-eqz v2, :cond_24

    .line 944
    const/16 v2, 0x1c

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->purchaseHistoryDetails:Lcom/google/android/finsky/protos/DocAnnotations$PurchaseHistoryDetails;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 946
    :cond_24
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForContentRating:Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    if-eqz v2, :cond_25

    .line 947
    const/16 v2, 0x1d

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForContentRating:Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 949
    :cond_25
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 950
    return-void
.end method
