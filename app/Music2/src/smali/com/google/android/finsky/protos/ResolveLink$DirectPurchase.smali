.class public final Lcom/google/android/finsky/protos/ResolveLink$DirectPurchase;
.super Lcom/google/protobuf/nano/MessageNano;
.source "ResolveLink.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/ResolveLink;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DirectPurchase"
.end annotation


# instance fields
.field public detailsUrl:Ljava/lang/String;

.field public hasDetailsUrl:Z

.field public hasOfferType:Z

.field public hasParentDocid:Z

.field public hasPurchaseDocid:Z

.field public offerType:I

.field public parentDocid:Ljava/lang/String;

.field public purchaseDocid:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 323
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 324
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ResolveLink$DirectPurchase;->clear()Lcom/google/android/finsky/protos/ResolveLink$DirectPurchase;

    .line 325
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/ResolveLink$DirectPurchase;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 328
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/ResolveLink$DirectPurchase;->detailsUrl:Ljava/lang/String;

    .line 329
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/ResolveLink$DirectPurchase;->hasDetailsUrl:Z

    .line 330
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/ResolveLink$DirectPurchase;->purchaseDocid:Ljava/lang/String;

    .line 331
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/ResolveLink$DirectPurchase;->hasPurchaseDocid:Z

    .line 332
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/ResolveLink$DirectPurchase;->parentDocid:Ljava/lang/String;

    .line 333
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/ResolveLink$DirectPurchase;->hasParentDocid:Z

    .line 334
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/finsky/protos/ResolveLink$DirectPurchase;->offerType:I

    .line 335
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/ResolveLink$DirectPurchase;->hasOfferType:Z

    .line 336
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/ResolveLink$DirectPurchase;->cachedSize:I

    .line 337
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 360
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 361
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/ResolveLink$DirectPurchase;->hasDetailsUrl:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/protos/ResolveLink$DirectPurchase;->detailsUrl:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 362
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/protos/ResolveLink$DirectPurchase;->detailsUrl:Ljava/lang/String;

    invoke-static {v3, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 365
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/ResolveLink$DirectPurchase;->hasPurchaseDocid:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/finsky/protos/ResolveLink$DirectPurchase;->purchaseDocid:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 366
    :cond_2
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/ResolveLink$DirectPurchase;->purchaseDocid:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 369
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/ResolveLink$DirectPurchase;->hasParentDocid:Z

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/google/android/finsky/protos/ResolveLink$DirectPurchase;->parentDocid:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 370
    :cond_4
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/ResolveLink$DirectPurchase;->parentDocid:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 373
    :cond_5
    iget v1, p0, Lcom/google/android/finsky/protos/ResolveLink$DirectPurchase;->offerType:I

    if-ne v1, v3, :cond_6

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/ResolveLink$DirectPurchase;->hasOfferType:Z

    if-eqz v1, :cond_7

    .line 374
    :cond_6
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/android/finsky/protos/ResolveLink$DirectPurchase;->offerType:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 377
    :cond_7
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/ResolveLink$DirectPurchase;
    .locals 4
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 385
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 386
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 390
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 391
    :sswitch_0
    return-object p0

    .line 396
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/ResolveLink$DirectPurchase;->detailsUrl:Ljava/lang/String;

    .line 397
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/ResolveLink$DirectPurchase;->hasDetailsUrl:Z

    goto :goto_0

    .line 401
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/ResolveLink$DirectPurchase;->purchaseDocid:Ljava/lang/String;

    .line 402
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/ResolveLink$DirectPurchase;->hasPurchaseDocid:Z

    goto :goto_0

    .line 406
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/ResolveLink$DirectPurchase;->parentDocid:Ljava/lang/String;

    .line 407
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/ResolveLink$DirectPurchase;->hasParentDocid:Z

    goto :goto_0

    .line 411
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 412
    .local v1, "value":I
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 425
    :pswitch_0
    iput v1, p0, Lcom/google/android/finsky/protos/ResolveLink$DirectPurchase;->offerType:I

    .line 426
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/ResolveLink$DirectPurchase;->hasOfferType:Z

    goto :goto_0

    .line 386
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch

    .line 412
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 290
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/ResolveLink$DirectPurchase;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/ResolveLink$DirectPurchase;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 3
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 343
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ResolveLink$DirectPurchase;->hasDetailsUrl:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/protos/ResolveLink$DirectPurchase;->detailsUrl:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 344
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/protos/ResolveLink$DirectPurchase;->detailsUrl:Ljava/lang/String;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 346
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ResolveLink$DirectPurchase;->hasPurchaseDocid:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/protos/ResolveLink$DirectPurchase;->purchaseDocid:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 347
    :cond_2
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/ResolveLink$DirectPurchase;->purchaseDocid:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 349
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ResolveLink$DirectPurchase;->hasParentDocid:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/finsky/protos/ResolveLink$DirectPurchase;->parentDocid:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 350
    :cond_4
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/ResolveLink$DirectPurchase;->parentDocid:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 352
    :cond_5
    iget v0, p0, Lcom/google/android/finsky/protos/ResolveLink$DirectPurchase;->offerType:I

    if-ne v0, v2, :cond_6

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ResolveLink$DirectPurchase;->hasOfferType:Z

    if-eqz v0, :cond_7

    .line 353
    :cond_6
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/android/finsky/protos/ResolveLink$DirectPurchase;->offerType:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 355
    :cond_7
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 356
    return-void
.end method
