.class public final Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DocumentV2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/DocumentV2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "OverflowLink"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;


# instance fields
.field public hasTitle:Z

.field public link:Lcom/google/android/finsky/protos/DocAnnotations$Link;

.field public title:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1441
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 1442
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;->clear()Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;

    .line 1443
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;
    .locals 2

    .prologue
    .line 1423
    sget-object v0, Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;->_emptyArray:[Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;

    if-nez v0, :cond_1

    .line 1424
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 1426
    :try_start_0
    sget-object v0, Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;->_emptyArray:[Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;

    if-nez v0, :cond_0

    .line 1427
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;

    sput-object v0, Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;->_emptyArray:[Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;

    .line 1429
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1431
    :cond_1
    sget-object v0, Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;->_emptyArray:[Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;

    return-object v0

    .line 1429
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;
    .locals 1

    .prologue
    .line 1446
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;->title:Ljava/lang/String;

    .line 1447
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;->hasTitle:Z

    .line 1448
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;->link:Lcom/google/android/finsky/protos/DocAnnotations$Link;

    .line 1449
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;->cachedSize:I

    .line 1450
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 1467
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 1468
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;->hasTitle:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;->title:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1469
    :cond_0
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;->title:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1472
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;->link:Lcom/google/android/finsky/protos/DocAnnotations$Link;

    if-eqz v1, :cond_2

    .line 1473
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;->link:Lcom/google/android/finsky/protos/DocAnnotations$Link;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1476
    :cond_2
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1484
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 1485
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 1489
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1490
    :sswitch_0
    return-object p0

    .line 1495
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;->title:Ljava/lang/String;

    .line 1496
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;->hasTitle:Z

    goto :goto_0

    .line 1500
    :sswitch_2
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;->link:Lcom/google/android/finsky/protos/DocAnnotations$Link;

    if-nez v1, :cond_1

    .line 1501
    new-instance v1, Lcom/google/android/finsky/protos/DocAnnotations$Link;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocAnnotations$Link;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;->link:Lcom/google/android/finsky/protos/DocAnnotations$Link;

    .line 1503
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;->link:Lcom/google/android/finsky/protos/DocAnnotations$Link;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1485
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1417
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1456
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;->hasTitle:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;->title:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1457
    :cond_0
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;->title:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1459
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;->link:Lcom/google/android/finsky/protos/DocAnnotations$Link;

    if-eqz v0, :cond_2

    .line 1460
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;->link:Lcom/google/android/finsky/protos/DocAnnotations$Link;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1462
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 1463
    return-void
.end method
