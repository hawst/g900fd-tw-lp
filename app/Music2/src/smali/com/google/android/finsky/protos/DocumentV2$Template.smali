.class public final Lcom/google/android/finsky/protos/DocumentV2$Template;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DocumentV2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/DocumentV2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Template"
.end annotation


# instance fields
.field public actionBanner:Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;

.field public addToCirclesContainer:Lcom/google/android/finsky/protos/DocumentV2$AddToCirclesContainer;

.field public containerWithBanner:Lcom/google/android/finsky/protos/DocumentV2$ContainerWithBanner;

.field public dealOfTheDay:Lcom/google/android/finsky/protos/DocumentV2$DealOfTheDay;

.field public editorialSeriesContainer:Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;

.field public emptyContainer:Lcom/google/android/finsky/protos/DocumentV2$EmptyContainer;

.field public moreByCreatorContainer:Lcom/google/android/finsky/protos/DocumentV2$MoreByCreatorContainer;

.field public myCirclesContainer:Lcom/google/android/finsky/protos/DocumentV2$MyCirclesContainer;

.field public nextBanner:Lcom/google/android/finsky/protos/DocumentV2$NextBanner;

.field public purchaseHistoryContainer:Lcom/google/android/finsky/protos/DocumentV2$PurchaseHistoryContainer;

.field public rateAndSuggestContainer:Lcom/google/android/finsky/protos/DocumentV2$RateAndSuggestContainer;

.field public rateContainer:Lcom/google/android/finsky/protos/DocumentV2$RateContainer;

.field public recommendationsContainer:Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainer;

.field public recommendationsContainerWithHeader:Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;

.field public seriesAntenna:Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;

.field public singleCardContainer:Lcom/google/android/finsky/protos/DocumentV2$SingleCardContainer;

.field public tileDetailsReflectedGraphic2X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

.field public tileFourBlock4X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

.field public tileGraphic2X1:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

.field public tileGraphic4X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

.field public tileGraphicColoredTitle2X1:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

.field public tileGraphicColoredTitle4X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

.field public tileGraphicUpperLeftTitle2X1:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

.field public trustedSourceContainer:Lcom/google/android/finsky/protos/DocumentV2$TrustedSourceContainer;

.field public warmWelcome:Lcom/google/android/finsky/protos/DocumentV2$WarmWelcome;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2929
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 2930
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocumentV2$Template;->clear()Lcom/google/android/finsky/protos/DocumentV2$Template;

    .line 2931
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/DocumentV2$Template;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2934
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->seriesAntenna:Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;

    .line 2935
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphic2X1:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    .line 2936
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphic4X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    .line 2937
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphicColoredTitle2X1:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    .line 2938
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphicUpperLeftTitle2X1:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    .line 2939
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileDetailsReflectedGraphic2X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    .line 2940
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileFourBlock4X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    .line 2941
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphicColoredTitle4X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    .line 2942
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->containerWithBanner:Lcom/google/android/finsky/protos/DocumentV2$ContainerWithBanner;

    .line 2943
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->dealOfTheDay:Lcom/google/android/finsky/protos/DocumentV2$DealOfTheDay;

    .line 2944
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->editorialSeriesContainer:Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;

    .line 2945
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->recommendationsContainer:Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainer;

    .line 2946
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->recommendationsContainerWithHeader:Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;

    .line 2947
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->nextBanner:Lcom/google/android/finsky/protos/DocumentV2$NextBanner;

    .line 2948
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->rateContainer:Lcom/google/android/finsky/protos/DocumentV2$RateContainer;

    .line 2949
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->rateAndSuggestContainer:Lcom/google/android/finsky/protos/DocumentV2$RateAndSuggestContainer;

    .line 2950
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->addToCirclesContainer:Lcom/google/android/finsky/protos/DocumentV2$AddToCirclesContainer;

    .line 2951
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->myCirclesContainer:Lcom/google/android/finsky/protos/DocumentV2$MyCirclesContainer;

    .line 2952
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->trustedSourceContainer:Lcom/google/android/finsky/protos/DocumentV2$TrustedSourceContainer;

    .line 2953
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->actionBanner:Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;

    .line 2954
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->warmWelcome:Lcom/google/android/finsky/protos/DocumentV2$WarmWelcome;

    .line 2955
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->moreByCreatorContainer:Lcom/google/android/finsky/protos/DocumentV2$MoreByCreatorContainer;

    .line 2956
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->emptyContainer:Lcom/google/android/finsky/protos/DocumentV2$EmptyContainer;

    .line 2957
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->singleCardContainer:Lcom/google/android/finsky/protos/DocumentV2$SingleCardContainer;

    .line 2958
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->purchaseHistoryContainer:Lcom/google/android/finsky/protos/DocumentV2$PurchaseHistoryContainer;

    .line 2959
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->cachedSize:I

    .line 2960
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 3046
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 3047
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->seriesAntenna:Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;

    if-eqz v1, :cond_0

    .line 3048
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->seriesAntenna:Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3051
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphic2X1:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    if-eqz v1, :cond_1

    .line 3052
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphic2X1:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3055
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphic4X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    if-eqz v1, :cond_2

    .line 3056
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphic4X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3059
    :cond_2
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphicColoredTitle2X1:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    if-eqz v1, :cond_3

    .line 3060
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphicColoredTitle2X1:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3063
    :cond_3
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphicUpperLeftTitle2X1:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    if-eqz v1, :cond_4

    .line 3064
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphicUpperLeftTitle2X1:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3067
    :cond_4
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileDetailsReflectedGraphic2X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    if-eqz v1, :cond_5

    .line 3068
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileDetailsReflectedGraphic2X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3071
    :cond_5
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileFourBlock4X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    if-eqz v1, :cond_6

    .line 3072
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileFourBlock4X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3075
    :cond_6
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->containerWithBanner:Lcom/google/android/finsky/protos/DocumentV2$ContainerWithBanner;

    if-eqz v1, :cond_7

    .line 3076
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->containerWithBanner:Lcom/google/android/finsky/protos/DocumentV2$ContainerWithBanner;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3079
    :cond_7
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->dealOfTheDay:Lcom/google/android/finsky/protos/DocumentV2$DealOfTheDay;

    if-eqz v1, :cond_8

    .line 3080
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->dealOfTheDay:Lcom/google/android/finsky/protos/DocumentV2$DealOfTheDay;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3083
    :cond_8
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphicColoredTitle4X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    if-eqz v1, :cond_9

    .line 3084
    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphicColoredTitle4X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3087
    :cond_9
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->editorialSeriesContainer:Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;

    if-eqz v1, :cond_a

    .line 3088
    const/16 v1, 0xb

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->editorialSeriesContainer:Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3091
    :cond_a
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->recommendationsContainer:Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainer;

    if-eqz v1, :cond_b

    .line 3092
    const/16 v1, 0xc

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->recommendationsContainer:Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainer;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3095
    :cond_b
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->nextBanner:Lcom/google/android/finsky/protos/DocumentV2$NextBanner;

    if-eqz v1, :cond_c

    .line 3096
    const/16 v1, 0xd

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->nextBanner:Lcom/google/android/finsky/protos/DocumentV2$NextBanner;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3099
    :cond_c
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->rateContainer:Lcom/google/android/finsky/protos/DocumentV2$RateContainer;

    if-eqz v1, :cond_d

    .line 3100
    const/16 v1, 0xe

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->rateContainer:Lcom/google/android/finsky/protos/DocumentV2$RateContainer;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3103
    :cond_d
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->addToCirclesContainer:Lcom/google/android/finsky/protos/DocumentV2$AddToCirclesContainer;

    if-eqz v1, :cond_e

    .line 3104
    const/16 v1, 0xf

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->addToCirclesContainer:Lcom/google/android/finsky/protos/DocumentV2$AddToCirclesContainer;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3107
    :cond_e
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->trustedSourceContainer:Lcom/google/android/finsky/protos/DocumentV2$TrustedSourceContainer;

    if-eqz v1, :cond_f

    .line 3108
    const/16 v1, 0x10

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->trustedSourceContainer:Lcom/google/android/finsky/protos/DocumentV2$TrustedSourceContainer;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3111
    :cond_f
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->rateAndSuggestContainer:Lcom/google/android/finsky/protos/DocumentV2$RateAndSuggestContainer;

    if-eqz v1, :cond_10

    .line 3112
    const/16 v1, 0x11

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->rateAndSuggestContainer:Lcom/google/android/finsky/protos/DocumentV2$RateAndSuggestContainer;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3115
    :cond_10
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->actionBanner:Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;

    if-eqz v1, :cond_11

    .line 3116
    const/16 v1, 0x12

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->actionBanner:Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3119
    :cond_11
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->warmWelcome:Lcom/google/android/finsky/protos/DocumentV2$WarmWelcome;

    if-eqz v1, :cond_12

    .line 3120
    const/16 v1, 0x13

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->warmWelcome:Lcom/google/android/finsky/protos/DocumentV2$WarmWelcome;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3123
    :cond_12
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->recommendationsContainerWithHeader:Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;

    if-eqz v1, :cond_13

    .line 3124
    const/16 v1, 0x14

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->recommendationsContainerWithHeader:Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3127
    :cond_13
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->emptyContainer:Lcom/google/android/finsky/protos/DocumentV2$EmptyContainer;

    if-eqz v1, :cond_14

    .line 3128
    const/16 v1, 0x15

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->emptyContainer:Lcom/google/android/finsky/protos/DocumentV2$EmptyContainer;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3131
    :cond_14
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->myCirclesContainer:Lcom/google/android/finsky/protos/DocumentV2$MyCirclesContainer;

    if-eqz v1, :cond_15

    .line 3132
    const/16 v1, 0x16

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->myCirclesContainer:Lcom/google/android/finsky/protos/DocumentV2$MyCirclesContainer;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3135
    :cond_15
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->singleCardContainer:Lcom/google/android/finsky/protos/DocumentV2$SingleCardContainer;

    if-eqz v1, :cond_16

    .line 3136
    const/16 v1, 0x17

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->singleCardContainer:Lcom/google/android/finsky/protos/DocumentV2$SingleCardContainer;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3139
    :cond_16
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->moreByCreatorContainer:Lcom/google/android/finsky/protos/DocumentV2$MoreByCreatorContainer;

    if-eqz v1, :cond_17

    .line 3140
    const/16 v1, 0x18

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->moreByCreatorContainer:Lcom/google/android/finsky/protos/DocumentV2$MoreByCreatorContainer;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3143
    :cond_17
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->purchaseHistoryContainer:Lcom/google/android/finsky/protos/DocumentV2$PurchaseHistoryContainer;

    if-eqz v1, :cond_18

    .line 3144
    const/16 v1, 0x19

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->purchaseHistoryContainer:Lcom/google/android/finsky/protos/DocumentV2$PurchaseHistoryContainer;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3147
    :cond_18
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocumentV2$Template;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3155
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 3156
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 3160
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 3161
    :sswitch_0
    return-object p0

    .line 3166
    :sswitch_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->seriesAntenna:Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;

    if-nez v1, :cond_1

    .line 3167
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->seriesAntenna:Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;

    .line 3169
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->seriesAntenna:Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 3173
    :sswitch_2
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphic2X1:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    if-nez v1, :cond_2

    .line 3174
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphic2X1:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    .line 3176
    :cond_2
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphic2X1:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 3180
    :sswitch_3
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphic4X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    if-nez v1, :cond_3

    .line 3181
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphic4X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    .line 3183
    :cond_3
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphic4X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 3187
    :sswitch_4
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphicColoredTitle2X1:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    if-nez v1, :cond_4

    .line 3188
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphicColoredTitle2X1:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    .line 3190
    :cond_4
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphicColoredTitle2X1:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 3194
    :sswitch_5
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphicUpperLeftTitle2X1:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    if-nez v1, :cond_5

    .line 3195
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphicUpperLeftTitle2X1:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    .line 3197
    :cond_5
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphicUpperLeftTitle2X1:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 3201
    :sswitch_6
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileDetailsReflectedGraphic2X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    if-nez v1, :cond_6

    .line 3202
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileDetailsReflectedGraphic2X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    .line 3204
    :cond_6
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileDetailsReflectedGraphic2X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 3208
    :sswitch_7
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileFourBlock4X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    if-nez v1, :cond_7

    .line 3209
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileFourBlock4X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    .line 3211
    :cond_7
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileFourBlock4X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 3215
    :sswitch_8
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->containerWithBanner:Lcom/google/android/finsky/protos/DocumentV2$ContainerWithBanner;

    if-nez v1, :cond_8

    .line 3216
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV2$ContainerWithBanner;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV2$ContainerWithBanner;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->containerWithBanner:Lcom/google/android/finsky/protos/DocumentV2$ContainerWithBanner;

    .line 3218
    :cond_8
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->containerWithBanner:Lcom/google/android/finsky/protos/DocumentV2$ContainerWithBanner;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 3222
    :sswitch_9
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->dealOfTheDay:Lcom/google/android/finsky/protos/DocumentV2$DealOfTheDay;

    if-nez v1, :cond_9

    .line 3223
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV2$DealOfTheDay;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV2$DealOfTheDay;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->dealOfTheDay:Lcom/google/android/finsky/protos/DocumentV2$DealOfTheDay;

    .line 3225
    :cond_9
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->dealOfTheDay:Lcom/google/android/finsky/protos/DocumentV2$DealOfTheDay;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 3229
    :sswitch_a
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphicColoredTitle4X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    if-nez v1, :cond_a

    .line 3230
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphicColoredTitle4X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    .line 3232
    :cond_a
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphicColoredTitle4X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 3236
    :sswitch_b
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->editorialSeriesContainer:Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;

    if-nez v1, :cond_b

    .line 3237
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->editorialSeriesContainer:Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;

    .line 3239
    :cond_b
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->editorialSeriesContainer:Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 3243
    :sswitch_c
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->recommendationsContainer:Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainer;

    if-nez v1, :cond_c

    .line 3244
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainer;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainer;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->recommendationsContainer:Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainer;

    .line 3246
    :cond_c
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->recommendationsContainer:Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainer;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 3250
    :sswitch_d
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->nextBanner:Lcom/google/android/finsky/protos/DocumentV2$NextBanner;

    if-nez v1, :cond_d

    .line 3251
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV2$NextBanner;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV2$NextBanner;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->nextBanner:Lcom/google/android/finsky/protos/DocumentV2$NextBanner;

    .line 3253
    :cond_d
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->nextBanner:Lcom/google/android/finsky/protos/DocumentV2$NextBanner;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 3257
    :sswitch_e
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->rateContainer:Lcom/google/android/finsky/protos/DocumentV2$RateContainer;

    if-nez v1, :cond_e

    .line 3258
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV2$RateContainer;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV2$RateContainer;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->rateContainer:Lcom/google/android/finsky/protos/DocumentV2$RateContainer;

    .line 3260
    :cond_e
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->rateContainer:Lcom/google/android/finsky/protos/DocumentV2$RateContainer;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 3264
    :sswitch_f
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->addToCirclesContainer:Lcom/google/android/finsky/protos/DocumentV2$AddToCirclesContainer;

    if-nez v1, :cond_f

    .line 3265
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV2$AddToCirclesContainer;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV2$AddToCirclesContainer;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->addToCirclesContainer:Lcom/google/android/finsky/protos/DocumentV2$AddToCirclesContainer;

    .line 3267
    :cond_f
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->addToCirclesContainer:Lcom/google/android/finsky/protos/DocumentV2$AddToCirclesContainer;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 3271
    :sswitch_10
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->trustedSourceContainer:Lcom/google/android/finsky/protos/DocumentV2$TrustedSourceContainer;

    if-nez v1, :cond_10

    .line 3272
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV2$TrustedSourceContainer;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV2$TrustedSourceContainer;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->trustedSourceContainer:Lcom/google/android/finsky/protos/DocumentV2$TrustedSourceContainer;

    .line 3274
    :cond_10
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->trustedSourceContainer:Lcom/google/android/finsky/protos/DocumentV2$TrustedSourceContainer;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 3278
    :sswitch_11
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->rateAndSuggestContainer:Lcom/google/android/finsky/protos/DocumentV2$RateAndSuggestContainer;

    if-nez v1, :cond_11

    .line 3279
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV2$RateAndSuggestContainer;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV2$RateAndSuggestContainer;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->rateAndSuggestContainer:Lcom/google/android/finsky/protos/DocumentV2$RateAndSuggestContainer;

    .line 3281
    :cond_11
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->rateAndSuggestContainer:Lcom/google/android/finsky/protos/DocumentV2$RateAndSuggestContainer;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 3285
    :sswitch_12
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->actionBanner:Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;

    if-nez v1, :cond_12

    .line 3286
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->actionBanner:Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;

    .line 3288
    :cond_12
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->actionBanner:Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 3292
    :sswitch_13
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->warmWelcome:Lcom/google/android/finsky/protos/DocumentV2$WarmWelcome;

    if-nez v1, :cond_13

    .line 3293
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV2$WarmWelcome;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV2$WarmWelcome;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->warmWelcome:Lcom/google/android/finsky/protos/DocumentV2$WarmWelcome;

    .line 3295
    :cond_13
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->warmWelcome:Lcom/google/android/finsky/protos/DocumentV2$WarmWelcome;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 3299
    :sswitch_14
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->recommendationsContainerWithHeader:Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;

    if-nez v1, :cond_14

    .line 3300
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->recommendationsContainerWithHeader:Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;

    .line 3302
    :cond_14
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->recommendationsContainerWithHeader:Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 3306
    :sswitch_15
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->emptyContainer:Lcom/google/android/finsky/protos/DocumentV2$EmptyContainer;

    if-nez v1, :cond_15

    .line 3307
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV2$EmptyContainer;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV2$EmptyContainer;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->emptyContainer:Lcom/google/android/finsky/protos/DocumentV2$EmptyContainer;

    .line 3309
    :cond_15
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->emptyContainer:Lcom/google/android/finsky/protos/DocumentV2$EmptyContainer;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 3313
    :sswitch_16
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->myCirclesContainer:Lcom/google/android/finsky/protos/DocumentV2$MyCirclesContainer;

    if-nez v1, :cond_16

    .line 3314
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV2$MyCirclesContainer;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV2$MyCirclesContainer;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->myCirclesContainer:Lcom/google/android/finsky/protos/DocumentV2$MyCirclesContainer;

    .line 3316
    :cond_16
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->myCirclesContainer:Lcom/google/android/finsky/protos/DocumentV2$MyCirclesContainer;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 3320
    :sswitch_17
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->singleCardContainer:Lcom/google/android/finsky/protos/DocumentV2$SingleCardContainer;

    if-nez v1, :cond_17

    .line 3321
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV2$SingleCardContainer;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV2$SingleCardContainer;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->singleCardContainer:Lcom/google/android/finsky/protos/DocumentV2$SingleCardContainer;

    .line 3323
    :cond_17
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->singleCardContainer:Lcom/google/android/finsky/protos/DocumentV2$SingleCardContainer;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 3327
    :sswitch_18
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->moreByCreatorContainer:Lcom/google/android/finsky/protos/DocumentV2$MoreByCreatorContainer;

    if-nez v1, :cond_18

    .line 3328
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV2$MoreByCreatorContainer;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV2$MoreByCreatorContainer;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->moreByCreatorContainer:Lcom/google/android/finsky/protos/DocumentV2$MoreByCreatorContainer;

    .line 3330
    :cond_18
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->moreByCreatorContainer:Lcom/google/android/finsky/protos/DocumentV2$MoreByCreatorContainer;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 3334
    :sswitch_19
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->purchaseHistoryContainer:Lcom/google/android/finsky/protos/DocumentV2$PurchaseHistoryContainer;

    if-nez v1, :cond_19

    .line 3335
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV2$PurchaseHistoryContainer;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV2$PurchaseHistoryContainer;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->purchaseHistoryContainer:Lcom/google/android/finsky/protos/DocumentV2$PurchaseHistoryContainer;

    .line 3337
    :cond_19
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->purchaseHistoryContainer:Lcom/google/android/finsky/protos/DocumentV2$PurchaseHistoryContainer;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 3156
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
        0x72 -> :sswitch_e
        0x7a -> :sswitch_f
        0x82 -> :sswitch_10
        0x8a -> :sswitch_11
        0x92 -> :sswitch_12
        0x9a -> :sswitch_13
        0xa2 -> :sswitch_14
        0xaa -> :sswitch_15
        0xb2 -> :sswitch_16
        0xba -> :sswitch_17
        0xc2 -> :sswitch_18
        0xca -> :sswitch_19
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2837
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/DocumentV2$Template;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocumentV2$Template;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2966
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->seriesAntenna:Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;

    if-eqz v0, :cond_0

    .line 2967
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->seriesAntenna:Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2969
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphic2X1:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    if-eqz v0, :cond_1

    .line 2970
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphic2X1:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2972
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphic4X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    if-eqz v0, :cond_2

    .line 2973
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphic4X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2975
    :cond_2
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphicColoredTitle2X1:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    if-eqz v0, :cond_3

    .line 2976
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphicColoredTitle2X1:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2978
    :cond_3
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphicUpperLeftTitle2X1:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    if-eqz v0, :cond_4

    .line 2979
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphicUpperLeftTitle2X1:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2981
    :cond_4
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileDetailsReflectedGraphic2X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    if-eqz v0, :cond_5

    .line 2982
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileDetailsReflectedGraphic2X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2984
    :cond_5
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileFourBlock4X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    if-eqz v0, :cond_6

    .line 2985
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileFourBlock4X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2987
    :cond_6
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->containerWithBanner:Lcom/google/android/finsky/protos/DocumentV2$ContainerWithBanner;

    if-eqz v0, :cond_7

    .line 2988
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->containerWithBanner:Lcom/google/android/finsky/protos/DocumentV2$ContainerWithBanner;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2990
    :cond_7
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->dealOfTheDay:Lcom/google/android/finsky/protos/DocumentV2$DealOfTheDay;

    if-eqz v0, :cond_8

    .line 2991
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->dealOfTheDay:Lcom/google/android/finsky/protos/DocumentV2$DealOfTheDay;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2993
    :cond_8
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphicColoredTitle4X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    if-eqz v0, :cond_9

    .line 2994
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphicColoredTitle4X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2996
    :cond_9
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->editorialSeriesContainer:Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;

    if-eqz v0, :cond_a

    .line 2997
    const/16 v0, 0xb

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->editorialSeriesContainer:Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2999
    :cond_a
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->recommendationsContainer:Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainer;

    if-eqz v0, :cond_b

    .line 3000
    const/16 v0, 0xc

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->recommendationsContainer:Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainer;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 3002
    :cond_b
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->nextBanner:Lcom/google/android/finsky/protos/DocumentV2$NextBanner;

    if-eqz v0, :cond_c

    .line 3003
    const/16 v0, 0xd

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->nextBanner:Lcom/google/android/finsky/protos/DocumentV2$NextBanner;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 3005
    :cond_c
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->rateContainer:Lcom/google/android/finsky/protos/DocumentV2$RateContainer;

    if-eqz v0, :cond_d

    .line 3006
    const/16 v0, 0xe

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->rateContainer:Lcom/google/android/finsky/protos/DocumentV2$RateContainer;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 3008
    :cond_d
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->addToCirclesContainer:Lcom/google/android/finsky/protos/DocumentV2$AddToCirclesContainer;

    if-eqz v0, :cond_e

    .line 3009
    const/16 v0, 0xf

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->addToCirclesContainer:Lcom/google/android/finsky/protos/DocumentV2$AddToCirclesContainer;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 3011
    :cond_e
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->trustedSourceContainer:Lcom/google/android/finsky/protos/DocumentV2$TrustedSourceContainer;

    if-eqz v0, :cond_f

    .line 3012
    const/16 v0, 0x10

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->trustedSourceContainer:Lcom/google/android/finsky/protos/DocumentV2$TrustedSourceContainer;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 3014
    :cond_f
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->rateAndSuggestContainer:Lcom/google/android/finsky/protos/DocumentV2$RateAndSuggestContainer;

    if-eqz v0, :cond_10

    .line 3015
    const/16 v0, 0x11

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->rateAndSuggestContainer:Lcom/google/android/finsky/protos/DocumentV2$RateAndSuggestContainer;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 3017
    :cond_10
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->actionBanner:Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;

    if-eqz v0, :cond_11

    .line 3018
    const/16 v0, 0x12

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->actionBanner:Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 3020
    :cond_11
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->warmWelcome:Lcom/google/android/finsky/protos/DocumentV2$WarmWelcome;

    if-eqz v0, :cond_12

    .line 3021
    const/16 v0, 0x13

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->warmWelcome:Lcom/google/android/finsky/protos/DocumentV2$WarmWelcome;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 3023
    :cond_12
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->recommendationsContainerWithHeader:Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;

    if-eqz v0, :cond_13

    .line 3024
    const/16 v0, 0x14

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->recommendationsContainerWithHeader:Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 3026
    :cond_13
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->emptyContainer:Lcom/google/android/finsky/protos/DocumentV2$EmptyContainer;

    if-eqz v0, :cond_14

    .line 3027
    const/16 v0, 0x15

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->emptyContainer:Lcom/google/android/finsky/protos/DocumentV2$EmptyContainer;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 3029
    :cond_14
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->myCirclesContainer:Lcom/google/android/finsky/protos/DocumentV2$MyCirclesContainer;

    if-eqz v0, :cond_15

    .line 3030
    const/16 v0, 0x16

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->myCirclesContainer:Lcom/google/android/finsky/protos/DocumentV2$MyCirclesContainer;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 3032
    :cond_15
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->singleCardContainer:Lcom/google/android/finsky/protos/DocumentV2$SingleCardContainer;

    if-eqz v0, :cond_16

    .line 3033
    const/16 v0, 0x17

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->singleCardContainer:Lcom/google/android/finsky/protos/DocumentV2$SingleCardContainer;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 3035
    :cond_16
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->moreByCreatorContainer:Lcom/google/android/finsky/protos/DocumentV2$MoreByCreatorContainer;

    if-eqz v0, :cond_17

    .line 3036
    const/16 v0, 0x18

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->moreByCreatorContainer:Lcom/google/android/finsky/protos/DocumentV2$MoreByCreatorContainer;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 3038
    :cond_17
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->purchaseHistoryContainer:Lcom/google/android/finsky/protos/DocumentV2$PurchaseHistoryContainer;

    if-eqz v0, :cond_18

    .line 3039
    const/16 v0, 0x19

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->purchaseHistoryContainer:Lcom/google/android/finsky/protos/DocumentV2$PurchaseHistoryContainer;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 3041
    :cond_18
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 3042
    return-void
.end method
