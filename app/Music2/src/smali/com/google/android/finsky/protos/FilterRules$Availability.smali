.class public final Lcom/google/android/finsky/protos/FilterRules$Availability;
.super Lcom/google/protobuf/nano/MessageNano;
.source "FilterRules.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/FilterRules;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Availability"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;
    }
.end annotation


# instance fields
.field public availabilityProblem:[Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;

.field public availableIfOwned:Z

.field public filterInfo:Lcom/google/android/finsky/protos/FilterRules$FilterEvaluationInfo;

.field public hasAvailableIfOwned:Z

.field public hasHidden:Z

.field public hasOfferType:Z

.field public hasRestriction:Z

.field public hidden:Z

.field public install:[Lcom/google/android/finsky/protos/Common$Install;

.field public offerType:I

.field public ownershipInfo:Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;

.field public perDeviceAvailabilityRestriction:[Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;

.field public restriction:I

.field public rule:Lcom/google/android/finsky/protos/FilterRules$Rule;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1466
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 1467
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/FilterRules$Availability;->clear()Lcom/google/android/finsky/protos/FilterRules$Availability;

    .line 1468
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/FilterRules$Availability;
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1471
    iput v3, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->restriction:I

    .line 1472
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->hasRestriction:Z

    .line 1473
    invoke-static {}, Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;->emptyArray()[Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->availabilityProblem:[Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;

    .line 1474
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->availableIfOwned:Z

    .line 1475
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->hasAvailableIfOwned:Z

    .line 1476
    iput v3, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->offerType:I

    .line 1477
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->hasOfferType:Z

    .line 1478
    iput-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->ownershipInfo:Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;

    .line 1479
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->hidden:Z

    .line 1480
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->hasHidden:Z

    .line 1481
    invoke-static {}, Lcom/google/android/finsky/protos/Common$Install;->emptyArray()[Lcom/google/android/finsky/protos/Common$Install;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->install:[Lcom/google/android/finsky/protos/Common$Install;

    .line 1482
    iput-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->rule:Lcom/google/android/finsky/protos/FilterRules$Rule;

    .line 1483
    invoke-static {}, Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;->emptyArray()[Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->perDeviceAvailabilityRestriction:[Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;

    .line 1484
    iput-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->filterInfo:Lcom/google/android/finsky/protos/FilterRules$FilterEvaluationInfo;

    .line 1485
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->cachedSize:I

    .line 1486
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 1542
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 1543
    .local v2, "size":I
    iget v3, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->restriction:I

    if-ne v3, v5, :cond_0

    iget-boolean v3, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->hasRestriction:Z

    if-eqz v3, :cond_1

    .line 1544
    :cond_0
    const/4 v3, 0x5

    iget v4, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->restriction:I

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    .line 1547
    :cond_1
    iget v3, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->offerType:I

    if-ne v3, v5, :cond_2

    iget-boolean v3, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->hasOfferType:Z

    if-eqz v3, :cond_3

    .line 1548
    :cond_2
    const/4 v3, 0x6

    iget v4, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->offerType:I

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    .line 1551
    :cond_3
    iget-object v3, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->rule:Lcom/google/android/finsky/protos/FilterRules$Rule;

    if-eqz v3, :cond_4

    .line 1552
    const/4 v3, 0x7

    iget-object v4, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->rule:Lcom/google/android/finsky/protos/FilterRules$Rule;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1555
    :cond_4
    iget-object v3, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->perDeviceAvailabilityRestriction:[Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->perDeviceAvailabilityRestriction:[Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;

    array-length v3, v3

    if-lez v3, :cond_6

    .line 1556
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->perDeviceAvailabilityRestriction:[Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;

    array-length v3, v3

    if-ge v1, v3, :cond_6

    .line 1557
    iget-object v3, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->perDeviceAvailabilityRestriction:[Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;

    aget-object v0, v3, v1

    .line 1558
    .local v0, "element":Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;
    if-eqz v0, :cond_5

    .line 1559
    const/16 v3, 0x9

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeGroupSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1556
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1564
    .end local v0    # "element":Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;
    .end local v1    # "i":I
    :cond_6
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->hasAvailableIfOwned:Z

    if-nez v3, :cond_7

    iget-boolean v3, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->availableIfOwned:Z

    if-eqz v3, :cond_8

    .line 1565
    :cond_7
    const/16 v3, 0xd

    iget-boolean v4, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->availableIfOwned:Z

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v2, v3

    .line 1568
    :cond_8
    iget-object v3, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->install:[Lcom/google/android/finsky/protos/Common$Install;

    if-eqz v3, :cond_a

    iget-object v3, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->install:[Lcom/google/android/finsky/protos/Common$Install;

    array-length v3, v3

    if-lez v3, :cond_a

    .line 1569
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v3, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->install:[Lcom/google/android/finsky/protos/Common$Install;

    array-length v3, v3

    if-ge v1, v3, :cond_a

    .line 1570
    iget-object v3, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->install:[Lcom/google/android/finsky/protos/Common$Install;

    aget-object v0, v3, v1

    .line 1571
    .local v0, "element":Lcom/google/android/finsky/protos/Common$Install;
    if-eqz v0, :cond_9

    .line 1572
    const/16 v3, 0xe

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1569
    :cond_9
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1577
    .end local v0    # "element":Lcom/google/android/finsky/protos/Common$Install;
    .end local v1    # "i":I
    :cond_a
    iget-object v3, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->filterInfo:Lcom/google/android/finsky/protos/FilterRules$FilterEvaluationInfo;

    if-eqz v3, :cond_b

    .line 1578
    const/16 v3, 0x10

    iget-object v4, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->filterInfo:Lcom/google/android/finsky/protos/FilterRules$FilterEvaluationInfo;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1581
    :cond_b
    iget-object v3, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->ownershipInfo:Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;

    if-eqz v3, :cond_c

    .line 1582
    const/16 v3, 0x11

    iget-object v4, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->ownershipInfo:Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1585
    :cond_c
    iget-object v3, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->availabilityProblem:[Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;

    if-eqz v3, :cond_e

    iget-object v3, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->availabilityProblem:[Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;

    array-length v3, v3

    if-lez v3, :cond_e

    .line 1586
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_2
    iget-object v3, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->availabilityProblem:[Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;

    array-length v3, v3

    if-ge v1, v3, :cond_e

    .line 1587
    iget-object v3, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->availabilityProblem:[Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;

    aget-object v0, v3, v1

    .line 1588
    .local v0, "element":Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;
    if-eqz v0, :cond_d

    .line 1589
    const/16 v3, 0x12

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1586
    :cond_d
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1594
    .end local v0    # "element":Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;
    .end local v1    # "i":I
    :cond_e
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->hasHidden:Z

    if-nez v3, :cond_f

    iget-boolean v3, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->hidden:Z

    if-eqz v3, :cond_10

    .line 1595
    :cond_f
    const/16 v3, 0x15

    iget-boolean v4, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->hidden:Z

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v2, v3

    .line 1598
    :cond_10
    return v2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/FilterRules$Availability;
    .locals 9
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v8, 0x9

    const/4 v7, 0x1

    const/4 v5, 0x0

    .line 1606
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 1607
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 1611
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v6

    if-nez v6, :cond_0

    .line 1612
    :sswitch_0
    return-object p0

    .line 1617
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v4

    .line 1618
    .local v4, "value":I
    packed-switch v4, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 1637
    :pswitch_1
    iput v4, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->restriction:I

    .line 1638
    iput-boolean v7, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->hasRestriction:Z

    goto :goto_0

    .line 1644
    .end local v4    # "value":I
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v4

    .line 1645
    .restart local v4    # "value":I
    packed-switch v4, :pswitch_data_1

    goto :goto_0

    .line 1658
    :pswitch_2
    iput v4, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->offerType:I

    .line 1659
    iput-boolean v7, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->hasOfferType:Z

    goto :goto_0

    .line 1665
    .end local v4    # "value":I
    :sswitch_3
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->rule:Lcom/google/android/finsky/protos/FilterRules$Rule;

    if-nez v6, :cond_1

    .line 1666
    new-instance v6, Lcom/google/android/finsky/protos/FilterRules$Rule;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/FilterRules$Rule;-><init>()V

    iput-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->rule:Lcom/google/android/finsky/protos/FilterRules$Rule;

    .line 1668
    :cond_1
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->rule:Lcom/google/android/finsky/protos/FilterRules$Rule;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1672
    :sswitch_4
    const/16 v6, 0x4b

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 1674
    .local v0, "arrayLength":I
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->perDeviceAvailabilityRestriction:[Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;

    if-nez v6, :cond_3

    move v1, v5

    .line 1675
    .local v1, "i":I
    :goto_1
    add-int v6, v1, v0

    new-array v2, v6, [Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;

    .line 1677
    .local v2, "newArray":[Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;
    if-eqz v1, :cond_2

    .line 1678
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->perDeviceAvailabilityRestriction:[Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1680
    :cond_2
    :goto_2
    array-length v6, v2

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_4

    .line 1681
    new-instance v6, Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;-><init>()V

    aput-object v6, v2, v1

    .line 1682
    aget-object v6, v2, v1

    invoke-virtual {p1, v6, v8}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readGroup(Lcom/google/protobuf/nano/MessageNano;I)V

    .line 1683
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 1680
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1674
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;
    :cond_3
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->perDeviceAvailabilityRestriction:[Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;

    array-length v1, v6

    goto :goto_1

    .line 1686
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;
    :cond_4
    new-instance v6, Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;-><init>()V

    aput-object v6, v2, v1

    .line 1687
    aget-object v6, v2, v1

    invoke-virtual {p1, v6, v8}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readGroup(Lcom/google/protobuf/nano/MessageNano;I)V

    .line 1688
    iput-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->perDeviceAvailabilityRestriction:[Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;

    goto :goto_0

    .line 1692
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->availableIfOwned:Z

    .line 1693
    iput-boolean v7, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->hasAvailableIfOwned:Z

    goto/16 :goto_0

    .line 1697
    :sswitch_6
    const/16 v6, 0x72

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 1699
    .restart local v0    # "arrayLength":I
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->install:[Lcom/google/android/finsky/protos/Common$Install;

    if-nez v6, :cond_6

    move v1, v5

    .line 1700
    .restart local v1    # "i":I
    :goto_3
    add-int v6, v1, v0

    new-array v2, v6, [Lcom/google/android/finsky/protos/Common$Install;

    .line 1702
    .local v2, "newArray":[Lcom/google/android/finsky/protos/Common$Install;
    if-eqz v1, :cond_5

    .line 1703
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->install:[Lcom/google/android/finsky/protos/Common$Install;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1705
    :cond_5
    :goto_4
    array-length v6, v2

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_7

    .line 1706
    new-instance v6, Lcom/google/android/finsky/protos/Common$Install;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/Common$Install;-><init>()V

    aput-object v6, v2, v1

    .line 1707
    aget-object v6, v2, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1708
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 1705
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 1699
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/Common$Install;
    :cond_6
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->install:[Lcom/google/android/finsky/protos/Common$Install;

    array-length v1, v6

    goto :goto_3

    .line 1711
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/Common$Install;
    :cond_7
    new-instance v6, Lcom/google/android/finsky/protos/Common$Install;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/Common$Install;-><init>()V

    aput-object v6, v2, v1

    .line 1712
    aget-object v6, v2, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1713
    iput-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->install:[Lcom/google/android/finsky/protos/Common$Install;

    goto/16 :goto_0

    .line 1717
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/Common$Install;
    :sswitch_7
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->filterInfo:Lcom/google/android/finsky/protos/FilterRules$FilterEvaluationInfo;

    if-nez v6, :cond_8

    .line 1718
    new-instance v6, Lcom/google/android/finsky/protos/FilterRules$FilterEvaluationInfo;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/FilterRules$FilterEvaluationInfo;-><init>()V

    iput-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->filterInfo:Lcom/google/android/finsky/protos/FilterRules$FilterEvaluationInfo;

    .line 1720
    :cond_8
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->filterInfo:Lcom/google/android/finsky/protos/FilterRules$FilterEvaluationInfo;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1724
    :sswitch_8
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->ownershipInfo:Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;

    if-nez v6, :cond_9

    .line 1725
    new-instance v6, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;-><init>()V

    iput-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->ownershipInfo:Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;

    .line 1727
    :cond_9
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->ownershipInfo:Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1731
    :sswitch_9
    const/16 v6, 0x92

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 1733
    .restart local v0    # "arrayLength":I
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->availabilityProblem:[Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;

    if-nez v6, :cond_b

    move v1, v5

    .line 1734
    .restart local v1    # "i":I
    :goto_5
    add-int v6, v1, v0

    new-array v2, v6, [Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;

    .line 1736
    .local v2, "newArray":[Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;
    if-eqz v1, :cond_a

    .line 1737
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->availabilityProblem:[Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1739
    :cond_a
    :goto_6
    array-length v6, v2

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_c

    .line 1740
    new-instance v6, Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;-><init>()V

    aput-object v6, v2, v1

    .line 1741
    aget-object v6, v2, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1742
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 1739
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 1733
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;
    :cond_b
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->availabilityProblem:[Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;

    array-length v1, v6

    goto :goto_5

    .line 1745
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;
    :cond_c
    new-instance v6, Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;-><init>()V

    aput-object v6, v2, v1

    .line 1746
    aget-object v6, v2, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1747
    iput-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->availabilityProblem:[Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;

    goto/16 :goto_0

    .line 1751
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;
    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->hidden:Z

    .line 1752
    iput-boolean v7, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->hasHidden:Z

    goto/16 :goto_0

    .line 1607
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x28 -> :sswitch_1
        0x30 -> :sswitch_2
        0x3a -> :sswitch_3
        0x4b -> :sswitch_4
        0x68 -> :sswitch_5
        0x72 -> :sswitch_6
        0x82 -> :sswitch_7
        0x8a -> :sswitch_8
        0x92 -> :sswitch_9
        0xa8 -> :sswitch_a
    .end sparse-switch

    .line 1618
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    .line 1645
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1232
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/FilterRules$Availability;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/FilterRules$Availability;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 5
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 1492
    iget v2, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->restriction:I

    if-ne v2, v4, :cond_0

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->hasRestriction:Z

    if-eqz v2, :cond_1

    .line 1493
    :cond_0
    const/4 v2, 0x5

    iget v3, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->restriction:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1495
    :cond_1
    iget v2, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->offerType:I

    if-ne v2, v4, :cond_2

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->hasOfferType:Z

    if-eqz v2, :cond_3

    .line 1496
    :cond_2
    const/4 v2, 0x6

    iget v3, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->offerType:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1498
    :cond_3
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->rule:Lcom/google/android/finsky/protos/FilterRules$Rule;

    if-eqz v2, :cond_4

    .line 1499
    const/4 v2, 0x7

    iget-object v3, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->rule:Lcom/google/android/finsky/protos/FilterRules$Rule;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1501
    :cond_4
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->perDeviceAvailabilityRestriction:[Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->perDeviceAvailabilityRestriction:[Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;

    array-length v2, v2

    if-lez v2, :cond_6

    .line 1502
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->perDeviceAvailabilityRestriction:[Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;

    array-length v2, v2

    if-ge v1, v2, :cond_6

    .line 1503
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->perDeviceAvailabilityRestriction:[Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;

    aget-object v0, v2, v1

    .line 1504
    .local v0, "element":Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;
    if-eqz v0, :cond_5

    .line 1505
    const/16 v2, 0x9

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeGroup(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1502
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1509
    .end local v0    # "element":Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;
    .end local v1    # "i":I
    :cond_6
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->hasAvailableIfOwned:Z

    if-nez v2, :cond_7

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->availableIfOwned:Z

    if-eqz v2, :cond_8

    .line 1510
    :cond_7
    const/16 v2, 0xd

    iget-boolean v3, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->availableIfOwned:Z

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 1512
    :cond_8
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->install:[Lcom/google/android/finsky/protos/Common$Install;

    if-eqz v2, :cond_a

    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->install:[Lcom/google/android/finsky/protos/Common$Install;

    array-length v2, v2

    if-lez v2, :cond_a

    .line 1513
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->install:[Lcom/google/android/finsky/protos/Common$Install;

    array-length v2, v2

    if-ge v1, v2, :cond_a

    .line 1514
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->install:[Lcom/google/android/finsky/protos/Common$Install;

    aget-object v0, v2, v1

    .line 1515
    .local v0, "element":Lcom/google/android/finsky/protos/Common$Install;
    if-eqz v0, :cond_9

    .line 1516
    const/16 v2, 0xe

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1513
    :cond_9
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1520
    .end local v0    # "element":Lcom/google/android/finsky/protos/Common$Install;
    .end local v1    # "i":I
    :cond_a
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->filterInfo:Lcom/google/android/finsky/protos/FilterRules$FilterEvaluationInfo;

    if-eqz v2, :cond_b

    .line 1521
    const/16 v2, 0x10

    iget-object v3, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->filterInfo:Lcom/google/android/finsky/protos/FilterRules$FilterEvaluationInfo;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1523
    :cond_b
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->ownershipInfo:Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;

    if-eqz v2, :cond_c

    .line 1524
    const/16 v2, 0x11

    iget-object v3, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->ownershipInfo:Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1526
    :cond_c
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->availabilityProblem:[Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;

    if-eqz v2, :cond_e

    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->availabilityProblem:[Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;

    array-length v2, v2

    if-lez v2, :cond_e

    .line 1527
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_2
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->availabilityProblem:[Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;

    array-length v2, v2

    if-ge v1, v2, :cond_e

    .line 1528
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->availabilityProblem:[Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;

    aget-object v0, v2, v1

    .line 1529
    .local v0, "element":Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;
    if-eqz v0, :cond_d

    .line 1530
    const/16 v2, 0x12

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1527
    :cond_d
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1534
    .end local v0    # "element":Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;
    .end local v1    # "i":I
    :cond_e
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->hasHidden:Z

    if-nez v2, :cond_f

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->hidden:Z

    if-eqz v2, :cond_10

    .line 1535
    :cond_f
    const/16 v2, 0x15

    iget-boolean v3, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->hidden:Z

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 1537
    :cond_10
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 1538
    return-void
.end method
