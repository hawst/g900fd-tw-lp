.class public final Lcom/google/android/finsky/protos/Common$GroupLicenseKey;
.super Lcom/google/protobuf/nano/MessageNano;
.source "Common.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/Common;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GroupLicenseKey"
.end annotation


# instance fields
.field public dasherCustomerId:J

.field public docid:Lcom/google/android/finsky/protos/Common$Docid;

.field public hasDasherCustomerId:Z

.field public hasLicensedOfferType:Z

.field public hasRentalPeriodDays:Z

.field public hasType:Z

.field public licensedOfferType:I

.field public rentalPeriodDays:I

.field public type:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3537
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 3538
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->clear()Lcom/google/android/finsky/protos/Common$GroupLicenseKey;

    .line 3539
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/Common$GroupLicenseKey;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 3542
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->dasherCustomerId:J

    .line 3543
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->hasDasherCustomerId:Z

    .line 3544
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    .line 3545
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->licensedOfferType:I

    .line 3546
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->hasLicensedOfferType:Z

    .line 3547
    iput v2, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->type:I

    .line 3548
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->hasType:Z

    .line 3549
    iput v2, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->rentalPeriodDays:I

    .line 3550
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->hasRentalPeriodDays:Z

    .line 3551
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->cachedSize:I

    .line 3552
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 3578
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 3579
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->hasDasherCustomerId:Z

    if-nez v1, :cond_0

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->dasherCustomerId:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    .line 3580
    :cond_0
    iget-wide v2, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->dasherCustomerId:J

    invoke-static {v6, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeFixed64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 3583
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    if-eqz v1, :cond_2

    .line 3584
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3587
    :cond_2
    iget v1, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->licensedOfferType:I

    if-ne v1, v6, :cond_3

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->hasLicensedOfferType:Z

    if-eqz v1, :cond_4

    .line 3588
    :cond_3
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->licensedOfferType:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3591
    :cond_4
    iget v1, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->type:I

    if-nez v1, :cond_5

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->hasType:Z

    if-eqz v1, :cond_6

    .line 3592
    :cond_5
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->type:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3595
    :cond_6
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->hasRentalPeriodDays:Z

    if-nez v1, :cond_7

    iget v1, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->rentalPeriodDays:I

    if-eqz v1, :cond_8

    .line 3596
    :cond_7
    const/4 v1, 0x5

    iget v2, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->rentalPeriodDays:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3599
    :cond_8
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Common$GroupLicenseKey;
    .locals 5
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 3607
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 3608
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 3612
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 3613
    :sswitch_0
    return-object p0

    .line 3618
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readFixed64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->dasherCustomerId:J

    .line 3619
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->hasDasherCustomerId:Z

    goto :goto_0

    .line 3623
    :sswitch_2
    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    if-nez v2, :cond_1

    .line 3624
    new-instance v2, Lcom/google/android/finsky/protos/Common$Docid;

    invoke-direct {v2}, Lcom/google/android/finsky/protos/Common$Docid;-><init>()V

    iput-object v2, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    .line 3626
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 3630
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 3631
    .local v1, "value":I
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 3644
    :pswitch_0
    iput v1, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->licensedOfferType:I

    .line 3645
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->hasLicensedOfferType:Z

    goto :goto_0

    .line 3651
    .end local v1    # "value":I
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 3652
    .restart local v1    # "value":I
    packed-switch v1, :pswitch_data_1

    goto :goto_0

    .line 3656
    :pswitch_1
    iput v1, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->type:I

    .line 3657
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->hasType:Z

    goto :goto_0

    .line 3663
    .end local v1    # "value":I
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v2

    iput v2, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->rentalPeriodDays:I

    .line 3664
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->hasRentalPeriodDays:Z

    goto :goto_0

    .line 3608
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x9 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch

    .line 3631
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 3652
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3501
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Common$GroupLicenseKey;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 5
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 3558
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->hasDasherCustomerId:Z

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->dasherCustomerId:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 3559
    :cond_0
    iget-wide v0, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->dasherCustomerId:J

    invoke-virtual {p1, v4, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeFixed64(IJ)V

    .line 3561
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    if-eqz v0, :cond_2

    .line 3562
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 3564
    :cond_2
    iget v0, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->licensedOfferType:I

    if-ne v0, v4, :cond_3

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->hasLicensedOfferType:Z

    if-eqz v0, :cond_4

    .line 3565
    :cond_3
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->licensedOfferType:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 3567
    :cond_4
    iget v0, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->type:I

    if-nez v0, :cond_5

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->hasType:Z

    if-eqz v0, :cond_6

    .line 3568
    :cond_5
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->type:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 3570
    :cond_6
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->hasRentalPeriodDays:Z

    if-nez v0, :cond_7

    iget v0, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->rentalPeriodDays:I

    if-eqz v0, :cond_8

    .line 3571
    :cond_7
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->rentalPeriodDays:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 3573
    :cond_8
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 3574
    return-void
.end method
