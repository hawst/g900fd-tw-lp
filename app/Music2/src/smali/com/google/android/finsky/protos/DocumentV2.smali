.class public interface abstract Lcom/google/android/finsky/protos/DocumentV2;
.super Ljava/lang/Object;
.source "DocumentV2.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/protos/DocumentV2$CallToAction;,
        Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;,
        Lcom/google/android/finsky/protos/DocumentV2$OBSOLETE_Reason;,
        Lcom/google/android/finsky/protos/DocumentV2$PurchaseHistoryContainer;,
        Lcom/google/android/finsky/protos/DocumentV2$NextBanner;,
        Lcom/google/android/finsky/protos/DocumentV2$TrustedSourceContainer;,
        Lcom/google/android/finsky/protos/DocumentV2$MyCirclesContainer;,
        Lcom/google/android/finsky/protos/DocumentV2$AddToCirclesContainer;,
        Lcom/google/android/finsky/protos/DocumentV2$RateAndSuggestContainer;,
        Lcom/google/android/finsky/protos/DocumentV2$RateContainer;,
        Lcom/google/android/finsky/protos/DocumentV2$SingleCardContainer;,
        Lcom/google/android/finsky/protos/DocumentV2$EmptyContainer;,
        Lcom/google/android/finsky/protos/DocumentV2$MoreByCreatorContainer;,
        Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;,
        Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainer;,
        Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;,
        Lcom/google/android/finsky/protos/DocumentV2$DealOfTheDay;,
        Lcom/google/android/finsky/protos/DocumentV2$ContainerWithBanner;,
        Lcom/google/android/finsky/protos/DocumentV2$WarmWelcome;,
        Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;,
        Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;,
        Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;,
        Lcom/google/android/finsky/protos/DocumentV2$Template;,
        Lcom/google/android/finsky/protos/DocumentV2$Review;,
        Lcom/google/android/finsky/protos/DocumentV2$ReasonUserAction;,
        Lcom/google/android/finsky/protos/DocumentV2$ReasonReview;,
        Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;,
        Lcom/google/android/finsky/protos/DocumentV2$Reason;,
        Lcom/google/android/finsky/protos/DocumentV2$Dismissal;,
        Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;,
        Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;,
        Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;,
        Lcom/google/android/finsky/protos/DocumentV2$Annotations;,
        Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    }
.end annotation
