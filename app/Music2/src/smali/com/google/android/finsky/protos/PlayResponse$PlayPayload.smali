.class public final Lcom/google/android/finsky/protos/PlayResponse$PlayPayload;
.super Lcom/google/protobuf/nano/MessageNano;
.source "PlayResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/PlayResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PlayPayload"
.end annotation


# instance fields
.field public oBSOLETEPlusProfileResponse:Lcom/google/android/finsky/protos/PlayPlusProfile$PlayPlusProfileResponse;

.field public plusProfileResponse:Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 195
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 196
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/PlayResponse$PlayPayload;->clear()Lcom/google/android/finsky/protos/PlayResponse$PlayPayload;

    .line 197
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/PlayResponse$PlayPayload;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 200
    iput-object v0, p0, Lcom/google/android/finsky/protos/PlayResponse$PlayPayload;->oBSOLETEPlusProfileResponse:Lcom/google/android/finsky/protos/PlayPlusProfile$PlayPlusProfileResponse;

    .line 201
    iput-object v0, p0, Lcom/google/android/finsky/protos/PlayResponse$PlayPayload;->plusProfileResponse:Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;

    .line 202
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/PlayResponse$PlayPayload;->cachedSize:I

    .line 203
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 220
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 221
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/android/finsky/protos/PlayResponse$PlayPayload;->oBSOLETEPlusProfileResponse:Lcom/google/android/finsky/protos/PlayPlusProfile$PlayPlusProfileResponse;

    if-eqz v1, :cond_0

    .line 222
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/PlayResponse$PlayPayload;->oBSOLETEPlusProfileResponse:Lcom/google/android/finsky/protos/PlayPlusProfile$PlayPlusProfileResponse;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 225
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/protos/PlayResponse$PlayPayload;->plusProfileResponse:Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;

    if-eqz v1, :cond_1

    .line 226
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/PlayResponse$PlayPayload;->plusProfileResponse:Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 229
    :cond_1
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/PlayResponse$PlayPayload;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 237
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 238
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 242
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 243
    :sswitch_0
    return-object p0

    .line 248
    :sswitch_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/PlayResponse$PlayPayload;->oBSOLETEPlusProfileResponse:Lcom/google/android/finsky/protos/PlayPlusProfile$PlayPlusProfileResponse;

    if-nez v1, :cond_1

    .line 249
    new-instance v1, Lcom/google/android/finsky/protos/PlayPlusProfile$PlayPlusProfileResponse;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/PlayPlusProfile$PlayPlusProfileResponse;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/PlayResponse$PlayPayload;->oBSOLETEPlusProfileResponse:Lcom/google/android/finsky/protos/PlayPlusProfile$PlayPlusProfileResponse;

    .line 251
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/PlayResponse$PlayPayload;->oBSOLETEPlusProfileResponse:Lcom/google/android/finsky/protos/PlayPlusProfile$PlayPlusProfileResponse;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 255
    :sswitch_2
    iget-object v1, p0, Lcom/google/android/finsky/protos/PlayResponse$PlayPayload;->plusProfileResponse:Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;

    if-nez v1, :cond_2

    .line 256
    new-instance v1, Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/PlayResponse$PlayPayload;->plusProfileResponse:Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;

    .line 258
    :cond_2
    iget-object v1, p0, Lcom/google/android/finsky/protos/PlayResponse$PlayPayload;->plusProfileResponse:Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 238
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 172
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/PlayResponse$PlayPayload;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/PlayResponse$PlayPayload;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 209
    iget-object v0, p0, Lcom/google/android/finsky/protos/PlayResponse$PlayPayload;->oBSOLETEPlusProfileResponse:Lcom/google/android/finsky/protos/PlayPlusProfile$PlayPlusProfileResponse;

    if-eqz v0, :cond_0

    .line 210
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/PlayResponse$PlayPayload;->oBSOLETEPlusProfileResponse:Lcom/google/android/finsky/protos/PlayPlusProfile$PlayPlusProfileResponse;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 212
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/protos/PlayResponse$PlayPayload;->plusProfileResponse:Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;

    if-eqz v0, :cond_1

    .line 213
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/PlayResponse$PlayPayload;->plusProfileResponse:Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 215
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 216
    return-void
.end method
