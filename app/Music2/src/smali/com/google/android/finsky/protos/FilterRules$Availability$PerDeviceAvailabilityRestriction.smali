.class public final Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;
.super Lcom/google/protobuf/nano/MessageNano;
.source "FilterRules.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/FilterRules$Availability;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PerDeviceAvailabilityRestriction"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;


# instance fields
.field public androidId:J

.field public channelId:J

.field public deviceRestriction:I

.field public filterInfo:Lcom/google/android/finsky/protos/FilterRules$FilterEvaluationInfo;

.field public hasAndroidId:Z

.field public hasChannelId:Z

.field public hasDeviceRestriction:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1287
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 1288
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;->clear()Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;

    .line 1289
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;
    .locals 2

    .prologue
    .line 1261
    sget-object v0, Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;->_emptyArray:[Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;

    if-nez v0, :cond_1

    .line 1262
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 1264
    :try_start_0
    sget-object v0, Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;->_emptyArray:[Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;

    if-nez v0, :cond_0

    .line 1265
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;

    sput-object v0, Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;->_emptyArray:[Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;

    .line 1267
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1269
    :cond_1
    sget-object v0, Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;->_emptyArray:[Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;

    return-object v0

    .line 1267
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 1292
    iput-wide v2, p0, Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;->androidId:J

    .line 1293
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;->hasAndroidId:Z

    .line 1294
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;->deviceRestriction:I

    .line 1295
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;->hasDeviceRestriction:Z

    .line 1296
    iput-wide v2, p0, Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;->channelId:J

    .line 1297
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;->hasChannelId:Z

    .line 1298
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;->filterInfo:Lcom/google/android/finsky/protos/FilterRules$FilterEvaluationInfo;

    .line 1299
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;->cachedSize:I

    .line 1300
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 1323
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 1324
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;->hasAndroidId:Z

    if-nez v1, :cond_0

    iget-wide v2, p0, Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;->androidId:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    .line 1325
    :cond_0
    const/16 v1, 0xa

    iget-wide v2, p0, Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;->androidId:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeFixed64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1328
    :cond_1
    iget v1, p0, Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;->deviceRestriction:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;->hasDeviceRestriction:Z

    if-eqz v1, :cond_3

    .line 1329
    :cond_2
    const/16 v1, 0xb

    iget v2, p0, Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;->deviceRestriction:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1332
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;->hasChannelId:Z

    if-nez v1, :cond_4

    iget-wide v2, p0, Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;->channelId:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_5

    .line 1333
    :cond_4
    const/16 v1, 0xc

    iget-wide v2, p0, Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;->channelId:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1336
    :cond_5
    iget-object v1, p0, Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;->filterInfo:Lcom/google/android/finsky/protos/FilterRules$FilterEvaluationInfo;

    if-eqz v1, :cond_6

    .line 1337
    const/16 v1, 0xf

    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;->filterInfo:Lcom/google/android/finsky/protos/FilterRules$FilterEvaluationInfo;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1340
    :cond_6
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;
    .locals 5
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 1348
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 1349
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 1353
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1354
    :sswitch_0
    return-object p0

    .line 1359
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readFixed64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;->androidId:J

    .line 1360
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;->hasAndroidId:Z

    goto :goto_0

    .line 1364
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 1365
    .local v1, "value":I
    packed-switch v1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 1384
    :pswitch_1
    iput v1, p0, Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;->deviceRestriction:I

    .line 1385
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;->hasDeviceRestriction:Z

    goto :goto_0

    .line 1391
    .end local v1    # "value":I
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;->channelId:J

    .line 1392
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;->hasChannelId:Z

    goto :goto_0

    .line 1396
    :sswitch_4
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;->filterInfo:Lcom/google/android/finsky/protos/FilterRules$FilterEvaluationInfo;

    if-nez v2, :cond_1

    .line 1397
    new-instance v2, Lcom/google/android/finsky/protos/FilterRules$FilterEvaluationInfo;

    invoke-direct {v2}, Lcom/google/android/finsky/protos/FilterRules$FilterEvaluationInfo;-><init>()V

    iput-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;->filterInfo:Lcom/google/android/finsky/protos/FilterRules$FilterEvaluationInfo;

    .line 1399
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;->filterInfo:Lcom/google/android/finsky/protos/FilterRules$FilterEvaluationInfo;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1349
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x51 -> :sswitch_1
        0x58 -> :sswitch_2
        0x60 -> :sswitch_3
        0x7a -> :sswitch_4
    .end sparse-switch

    .line 1365
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1255
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 6
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v4, 0x0

    .line 1306
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;->hasAndroidId:Z

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;->androidId:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_1

    .line 1307
    :cond_0
    const/16 v0, 0xa

    iget-wide v2, p0, Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;->androidId:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeFixed64(IJ)V

    .line 1309
    :cond_1
    iget v0, p0, Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;->deviceRestriction:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;->hasDeviceRestriction:Z

    if-eqz v0, :cond_3

    .line 1310
    :cond_2
    const/16 v0, 0xb

    iget v1, p0, Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;->deviceRestriction:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1312
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;->hasChannelId:Z

    if-nez v0, :cond_4

    iget-wide v0, p0, Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;->channelId:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_5

    .line 1313
    :cond_4
    const/16 v0, 0xc

    iget-wide v2, p0, Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;->channelId:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 1315
    :cond_5
    iget-object v0, p0, Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;->filterInfo:Lcom/google/android/finsky/protos/FilterRules$FilterEvaluationInfo;

    if-eqz v0, :cond_6

    .line 1316
    const/16 v0, 0xf

    iget-object v1, p0, Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;->filterInfo:Lcom/google/android/finsky/protos/FilterRules$FilterEvaluationInfo;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1318
    :cond_6
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 1319
    return-void
.end method
