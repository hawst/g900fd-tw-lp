.class final Lcom/google/android/gms/tagmanager/bf;
.super Ljava/lang/Object;


# static fields
.field static aFN:Lcom/google/android/gms/tagmanager/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/tagmanager/x;

    invoke-direct {v0}, Lcom/google/android/gms/tagmanager/x;-><init>()V

    sput-object v0, Lcom/google/android/gms/tagmanager/bf;->aFN:Lcom/google/android/gms/tagmanager/Logger;

    return-void
.end method

.method public static e(Ljava/lang/String;)V
    .locals 1
    .param p0, "message"    # Ljava/lang/String;

    .prologue
    sget-object v0, Lcom/google/android/gms/tagmanager/bf;->aFN:Lcom/google/android/gms/tagmanager/Logger;

    invoke-interface {v0, p0}, Lcom/google/android/gms/tagmanager/Logger;->e(Ljava/lang/String;)V

    return-void
.end method

.method public static e(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1
    .param p0, "message"    # Ljava/lang/String;
    .param p1, "t"    # Ljava/lang/Throwable;

    .prologue
    sget-object v0, Lcom/google/android/gms/tagmanager/bf;->aFN:Lcom/google/android/gms/tagmanager/Logger;

    invoke-interface {v0, p0, p1}, Lcom/google/android/gms/tagmanager/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method public static i(Ljava/lang/String;)V
    .locals 1
    .param p0, "message"    # Ljava/lang/String;

    .prologue
    sget-object v0, Lcom/google/android/gms/tagmanager/bf;->aFN:Lcom/google/android/gms/tagmanager/Logger;

    invoke-interface {v0, p0}, Lcom/google/android/gms/tagmanager/Logger;->i(Ljava/lang/String;)V

    return-void
.end method

.method public static v(Ljava/lang/String;)V
    .locals 1
    .param p0, "message"    # Ljava/lang/String;

    .prologue
    sget-object v0, Lcom/google/android/gms/tagmanager/bf;->aFN:Lcom/google/android/gms/tagmanager/Logger;

    invoke-interface {v0, p0}, Lcom/google/android/gms/tagmanager/Logger;->v(Ljava/lang/String;)V

    return-void
.end method

.method public static w(Ljava/lang/String;)V
    .locals 1
    .param p0, "message"    # Ljava/lang/String;

    .prologue
    sget-object v0, Lcom/google/android/gms/tagmanager/bf;->aFN:Lcom/google/android/gms/tagmanager/Logger;

    invoke-interface {v0, p0}, Lcom/google/android/gms/tagmanager/Logger;->w(Ljava/lang/String;)V

    return-void
.end method
