.class Lcom/google/android/gms/appdatasearch/util/AppDataSearchDbOpenHelperBase$NamesHelper;
.super Ljava/lang/Object;
.source "AppDataSearchDbOpenHelperBase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/appdatasearch/util/AppDataSearchDbOpenHelperBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "NamesHelper"
.end annotation


# direct methods
.method public static getDeleteTriggerName(Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;)Ljava/lang/String;
    .locals 2
    .param p0, "table"    # Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;

    .prologue
    .line 411
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;->getCorpusName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_delete_trigger_appdatasearch"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getExistingSequenceTables(Landroid/database/sqlite/SQLiteDatabase;)Ljava/util/Set;
    .locals 3
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 419
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_seqno_table_appdatasearch"

    aput-object v2, v0, v1

    invoke-static {p0, v0}, Lcom/google/android/gms/appdatasearch/util/MasterTableHelper;->getTables(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public static getExistingTriggers(Landroid/database/sqlite/SQLiteDatabase;)Ljava/util/Set;
    .locals 3
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 423
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_trigger_appdatasearch"

    aput-object v2, v0, v1

    invoke-static {p0, v0}, Lcom/google/android/gms/appdatasearch/util/MasterTableHelper;->getTriggers(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public static getInsertTriggerName(Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;)Ljava/lang/String;
    .locals 2
    .param p0, "table"    # Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;

    .prologue
    .line 407
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;->getCorpusName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_insert_trigger_appdatasearch"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getSequenceTableName(Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;)Ljava/lang/String;
    .locals 2
    .param p0, "table"    # Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;

    .prologue
    .line 403
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;->getCorpusName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_seqno_table_appdatasearch"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getUpdateTriggerName(Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;)Ljava/lang/String;
    .locals 2
    .param p0, "table"    # Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;

    .prologue
    .line 415
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;->getCorpusName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_update_trigger_appdatasearch"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
