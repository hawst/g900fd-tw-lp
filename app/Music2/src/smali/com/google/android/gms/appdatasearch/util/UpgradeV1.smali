.class Lcom/google/android/gms/appdatasearch/util/UpgradeV1;
.super Ljava/lang/Object;
.source "UpgradeV1.java"


# direct methods
.method static getOldSequenceTables(Landroid/database/sqlite/SQLiteDatabase;)Ljava/util/Set;
    .locals 3
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 32
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_appdatasearch_seqno_table"

    aput-object v2, v0, v1

    invoke-static {p0, v0}, Lcom/google/android/gms/appdatasearch/util/MasterTableHelper;->getTables(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method static getOldTriggers(Landroid/database/sqlite/SQLiteDatabase;)Ljava/util/Set;
    .locals 3
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 27
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_appdatasearch_insert_trigger"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "_appdatasearch_delete_trigger"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "_appdatasearch_update_trigger"

    aput-object v2, v0, v1

    invoke-static {p0, v0}, Lcom/google/android/gms/appdatasearch/util/MasterTableHelper;->getTriggers(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public static upgrade(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 4
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 18
    invoke-static {p0}, Lcom/google/android/gms/appdatasearch/util/UpgradeV1;->getOldSequenceTables(Landroid/database/sqlite/SQLiteDatabase;)Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 19
    .local v1, "table":Ljava/lang/String;
    invoke-static {p0, v1}, Lcom/google/android/gms/appdatasearch/util/MasterTableHelper;->dropTable(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    goto :goto_0

    .line 21
    .end local v1    # "table":Ljava/lang/String;
    :cond_0
    invoke-static {p0}, Lcom/google/android/gms/appdatasearch/util/UpgradeV1;->getOldTriggers(Landroid/database/sqlite/SQLiteDatabase;)Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 22
    .local v2, "trigger":Ljava/lang/String;
    invoke-static {p0, v2}, Lcom/google/android/gms/appdatasearch/util/MasterTableHelper;->dropTrigger(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    goto :goto_1

    .line 24
    .end local v2    # "trigger":Ljava/lang/String;
    :cond_1
    return-void
.end method
