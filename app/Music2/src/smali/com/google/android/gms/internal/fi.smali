.class public final Lcom/google/android/gms/internal/fi;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lcom/google/android/gms/internal/fe;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/internal/fi$a;
    }
.end annotation


# direct methods
.method public static a(Landroid/content/Context;Lcom/google/android/gms/internal/z;Lcom/google/android/gms/internal/ge$a;Lcom/google/android/gms/internal/ha;Lcom/google/android/gms/internal/cy;Lcom/google/android/gms/internal/fi$a;)Lcom/google/android/gms/internal/gl;
    .locals 6

    iget-object v0, p2, Lcom/google/android/gms/internal/ge$a;->vv:Lcom/google/android/gms/internal/fp;

    iget-boolean v0, v0, Lcom/google/android/gms/internal/fp;->tS:Z

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/gms/internal/fs;

    new-instance v3, Lcom/google/android/gms/internal/an;

    invoke-direct {v3}, Lcom/google/android/gms/internal/an;-><init>()V

    move-object v1, p0

    move-object v2, p1

    move-object v4, p2

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/internal/fs;-><init>(Landroid/content/Context;Lcom/google/android/gms/internal/z;Lcom/google/android/gms/internal/an;Lcom/google/android/gms/internal/ge$a;Lcom/google/android/gms/internal/fi$a;)V

    :goto_0
    invoke-virtual {v0}, Lcom/google/android/gms/internal/gl;->start()V

    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/gms/internal/fj;

    move-object v1, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/internal/fj;-><init>(Landroid/content/Context;Lcom/google/android/gms/internal/ge$a;Lcom/google/android/gms/internal/ha;Lcom/google/android/gms/internal/cy;Lcom/google/android/gms/internal/fi$a;)V

    goto :goto_0
.end method
