.class public final Lcom/google/android/gms/internal/fl$a;
.super Lcom/google/android/gms/internal/fl;


# annotations
.annotation runtime Lcom/google/android/gms/internal/fe;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/internal/fl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/internal/fn;Lcom/google/android/gms/internal/fk$a;)V
    .locals 0

    invoke-direct {p0, p2, p3}, Lcom/google/android/gms/internal/fl;-><init>(Lcom/google/android/gms/internal/fn;Lcom/google/android/gms/internal/fk$a;)V

    iput-object p1, p0, Lcom/google/android/gms/internal/fl$a;->mContext:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public cB()V
    .locals 0

    return-void
.end method

.method public cC()Lcom/google/android/gms/internal/fr;
    .locals 5

    invoke-static {}, Lcom/google/android/gms/internal/gg;->bD()Landroid/os/Bundle;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/internal/br;

    const-string v2, "gads:sdk_core_location"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "gads:sdk_core_experiment_id"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "gads:block_autoclicks_experiment_id"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v3, v0}, Lcom/google/android/gms/internal/br;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/internal/fl$a;->mContext:Landroid/content/Context;

    new-instance v2, Lcom/google/android/gms/internal/co;

    invoke-direct {v2}, Lcom/google/android/gms/internal/co;-><init>()V

    new-instance v3, Lcom/google/android/gms/internal/gd;

    invoke-direct {v3}, Lcom/google/android/gms/internal/gd;-><init>()V

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gms/internal/fw;->a(Landroid/content/Context;Lcom/google/android/gms/internal/br;Lcom/google/android/gms/internal/cn;Lcom/google/android/gms/internal/gc;)Lcom/google/android/gms/internal/fw;

    move-result-object v0

    return-object v0
.end method
