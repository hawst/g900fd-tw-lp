.class Lcom/google/android/gms/internal/z$b;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lcom/google/android/gms/internal/fe;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/internal/z;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "b"
.end annotation


# instance fields
.field public final lC:Lcom/google/android/gms/internal/z$a;

.field public final lD:Ljava/lang/String;

.field public final lE:Landroid/content/Context;

.field public final lF:Lcom/google/android/gms/internal/k;

.field public final lG:Lcom/google/android/gms/internal/gy;

.field public lH:Lcom/google/android/gms/internal/bh;

.field public lI:Lcom/google/android/gms/internal/gl;

.field public lJ:Lcom/google/android/gms/internal/gl;

.field public lK:Lcom/google/android/gms/internal/bd;

.field public lL:Lcom/google/android/gms/internal/ge;

.field public lM:Lcom/google/android/gms/internal/ge$a;

.field public lN:Lcom/google/android/gms/internal/gf;

.field public lO:Lcom/google/android/gms/internal/bk;

.field public lP:Lcom/google/android/gms/internal/eq;

.field public lQ:Lcom/google/android/gms/internal/em;

.field public lR:Lcom/google/android/gms/internal/ey;

.field public lS:Lcom/google/android/gms/internal/ez;

.field public lT:Lcom/google/android/gms/internal/by;

.field public lU:Lcom/google/android/gms/internal/bz;

.field public lV:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public lW:Lcom/google/android/gms/internal/ej;

.field public lX:Lcom/google/android/gms/internal/gj;

.field public lY:Landroid/view/View;

.field public lZ:I

.field public ma:Z

.field private mb:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/google/android/gms/internal/gf;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/internal/bd;Ljava/lang/String;Lcom/google/android/gms/internal/gy;)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/google/android/gms/internal/z$b;->lX:Lcom/google/android/gms/internal/gj;

    iput-object v1, p0, Lcom/google/android/gms/internal/z$b;->lY:Landroid/view/View;

    iput v0, p0, Lcom/google/android/gms/internal/z$b;->lZ:I

    iput-boolean v0, p0, Lcom/google/android/gms/internal/z$b;->ma:Z

    iput-object v1, p0, Lcom/google/android/gms/internal/z$b;->mb:Ljava/util/HashSet;

    iget-boolean v0, p2, Lcom/google/android/gms/internal/bd;->oi:Z

    if-eqz v0, :cond_0

    iput-object v1, p0, Lcom/google/android/gms/internal/z$b;->lC:Lcom/google/android/gms/internal/z$a;

    :goto_0
    iput-object p2, p0, Lcom/google/android/gms/internal/z$b;->lK:Lcom/google/android/gms/internal/bd;

    iput-object p3, p0, Lcom/google/android/gms/internal/z$b;->lD:Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/gms/internal/z$b;->lE:Landroid/content/Context;

    iput-object p4, p0, Lcom/google/android/gms/internal/z$b;->lG:Lcom/google/android/gms/internal/gy;

    new-instance v0, Lcom/google/android/gms/internal/k;

    new-instance v1, Lcom/google/android/gms/internal/ab;

    invoke-direct {v1, p0}, Lcom/google/android/gms/internal/ab;-><init>(Lcom/google/android/gms/internal/z$b;)V

    invoke-direct {v0, v1}, Lcom/google/android/gms/internal/k;-><init>(Lcom/google/android/gms/internal/g;)V

    iput-object v0, p0, Lcom/google/android/gms/internal/z$b;->lF:Lcom/google/android/gms/internal/k;

    return-void

    :cond_0
    new-instance v0, Lcom/google/android/gms/internal/z$a;

    invoke-direct {v0, p1}, Lcom/google/android/gms/internal/z$a;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/internal/z$b;->lC:Lcom/google/android/gms/internal/z$a;

    iget-object v0, p0, Lcom/google/android/gms/internal/z$b;->lC:Lcom/google/android/gms/internal/z$a;

    iget v1, p2, Lcom/google/android/gms/internal/bd;->widthPixels:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/z$a;->setMinimumWidth(I)V

    iget-object v0, p0, Lcom/google/android/gms/internal/z$b;->lC:Lcom/google/android/gms/internal/z$a;

    iget v1, p2, Lcom/google/android/gms/internal/bd;->heightPixels:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/z$a;->setMinimumHeight(I)V

    iget-object v0, p0, Lcom/google/android/gms/internal/z$b;->lC:Lcom/google/android/gms/internal/z$a;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/z$a;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/util/HashSet;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashSet",
            "<",
            "Lcom/google/android/gms/internal/gf;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/gms/internal/z$b;->mb:Ljava/util/HashSet;

    return-void
.end method

.method public au()Ljava/util/HashSet;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashSet",
            "<",
            "Lcom/google/android/gms/internal/gf;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/internal/z$b;->mb:Ljava/util/HashSet;

    return-object v0
.end method
