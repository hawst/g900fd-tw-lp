.class public final Lcom/google/android/gms/internal/bl;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lcom/google/android/gms/internal/fe;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/internal/bl$a;
    }
.end annotation


# static fields
.field public static final DEVICE_ID_EMULATOR:Ljava/lang/String;


# instance fields
.field private final d:Ljava/util/Date;

.field private final f:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Landroid/location/Location;

.field private final on:Ljava/lang/String;

.field private final oo:I

.field private final op:Z

.field private final oq:Landroid/os/Bundle;

.field private final or:Ljava/util/Map;

.field private final os:Ljava/lang/String;

.field private final ot:Lcom/google/android/gms/ads/search/SearchAdRequest;

.field private final ou:I

.field private final ov:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "emulator"

    invoke-static {v0}, Lcom/google/android/gms/internal/gw;->W(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/bl;->DEVICE_ID_EMULATOR:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/internal/bl$a;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/internal/bl;-><init>(Lcom/google/android/gms/internal/bl$a;Lcom/google/android/gms/ads/search/SearchAdRequest;)V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/internal/bl$a;Lcom/google/android/gms/ads/search/SearchAdRequest;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/gms/internal/bl$a;->a(Lcom/google/android/gms/internal/bl$a;)Ljava/util/Date;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/bl;->d:Ljava/util/Date;

    invoke-static {p1}, Lcom/google/android/gms/internal/bl$a;->b(Lcom/google/android/gms/internal/bl$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/bl;->on:Ljava/lang/String;

    invoke-static {p1}, Lcom/google/android/gms/internal/bl$a;->c(Lcom/google/android/gms/internal/bl$a;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/internal/bl;->oo:I

    invoke-static {p1}, Lcom/google/android/gms/internal/bl$a;->d(Lcom/google/android/gms/internal/bl$a;)Ljava/util/HashSet;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/bl;->f:Ljava/util/Set;

    invoke-static {p1}, Lcom/google/android/gms/internal/bl$a;->e(Lcom/google/android/gms/internal/bl$a;)Landroid/location/Location;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/bl;->h:Landroid/location/Location;

    invoke-static {p1}, Lcom/google/android/gms/internal/bl$a;->f(Lcom/google/android/gms/internal/bl$a;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/internal/bl;->op:Z

    invoke-static {p1}, Lcom/google/android/gms/internal/bl$a;->g(Lcom/google/android/gms/internal/bl$a;)Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/bl;->oq:Landroid/os/Bundle;

    invoke-static {p1}, Lcom/google/android/gms/internal/bl$a;->h(Lcom/google/android/gms/internal/bl$a;)Ljava/util/HashMap;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/bl;->or:Ljava/util/Map;

    invoke-static {p1}, Lcom/google/android/gms/internal/bl$a;->i(Lcom/google/android/gms/internal/bl$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/bl;->os:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/gms/internal/bl;->ot:Lcom/google/android/gms/ads/search/SearchAdRequest;

    invoke-static {p1}, Lcom/google/android/gms/internal/bl$a;->j(Lcom/google/android/gms/internal/bl$a;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/internal/bl;->ou:I

    invoke-static {p1}, Lcom/google/android/gms/internal/bl$a;->k(Lcom/google/android/gms/internal/bl$a;)Ljava/util/HashSet;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/bl;->ov:Ljava/util/Set;

    return-void
.end method


# virtual methods
.method public bd()Lcom/google/android/gms/ads/search/SearchAdRequest;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/bl;->ot:Lcom/google/android/gms/ads/search/SearchAdRequest;

    return-object v0
.end method

.method public be()Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/bl;->or:Ljava/util/Map;

    return-object v0
.end method

.method public bf()Landroid/os/Bundle;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/bl;->oq:Landroid/os/Bundle;

    return-object v0
.end method

.method public bg()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/internal/bl;->ou:I

    return v0
.end method

.method public getBirthday()Ljava/util/Date;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/bl;->d:Ljava/util/Date;

    return-object v0
.end method

.method public getContentUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/bl;->on:Ljava/lang/String;

    return-object v0
.end method

.method public getGender()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/internal/bl;->oo:I

    return v0
.end method

.method public getKeywords()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/internal/bl;->f:Ljava/util/Set;

    return-object v0
.end method

.method public getLocation()Landroid/location/Location;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/bl;->h:Landroid/location/Location;

    return-object v0
.end method

.method public getManualImpressionsEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/internal/bl;->op:Z

    return v0
.end method

.method public getNetworkExtrasBundle(Ljava/lang/Class;)Landroid/os/Bundle;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/google/android/gms/ads/mediation/MediationAdapter;",
            ">;)",
            "Landroid/os/Bundle;"
        }
    .end annotation

    .prologue
    .local p1, "adapterClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/google/android/gms/ads/mediation/MediationAdapter;>;"
    iget-object v0, p0, Lcom/google/android/gms/internal/bl;->oq:Landroid/os/Bundle;

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public getPublisherProvidedId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/bl;->os:Ljava/lang/String;

    return-object v0
.end method

.method public isTestDevice(Landroid/content/Context;)Z
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    iget-object v0, p0, Lcom/google/android/gms/internal/bl;->ov:Ljava/util/Set;

    invoke-static {p1}, Lcom/google/android/gms/internal/gw;->v(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
