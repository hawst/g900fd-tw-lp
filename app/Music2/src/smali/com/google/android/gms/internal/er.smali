.class public final Lcom/google/android/gms/internal/er;
.super Lcom/google/android/gms/internal/em$a;


# annotations
.annotation runtime Lcom/google/android/gms/internal/fe;
.end annotation


# instance fields
.field private final oE:Lcom/google/android/gms/ads/purchase/InAppPurchaseListener;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/ads/purchase/InAppPurchaseListener;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/internal/em$a;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/internal/er;->oE:Lcom/google/android/gms/ads/purchase/InAppPurchaseListener;

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/gms/internal/el;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/internal/er;->oE:Lcom/google/android/gms/ads/purchase/InAppPurchaseListener;

    new-instance v1, Lcom/google/android/gms/internal/eu;

    invoke-direct {v1, p1}, Lcom/google/android/gms/internal/eu;-><init>(Lcom/google/android/gms/internal/el;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/ads/purchase/InAppPurchaseListener;->onInAppPurchaseRequested(Lcom/google/android/gms/ads/purchase/InAppPurchase;)V

    return-void
.end method
