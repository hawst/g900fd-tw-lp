.class public final Lcom/google/android/gms/internal/fc;
.super Lcom/google/android/gms/internal/ez$a;


# annotations
.annotation runtime Lcom/google/android/gms/internal/fe;
.end annotation


# instance fields
.field private final oH:Lcom/google/android/gms/ads/doubleclick/PublisherInterstitialAd;

.field private final oI:Lcom/google/android/gms/ads/doubleclick/c;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/ads/doubleclick/c;Lcom/google/android/gms/ads/doubleclick/PublisherInterstitialAd;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/internal/ez$a;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/internal/fc;->oI:Lcom/google/android/gms/ads/doubleclick/c;

    iput-object p2, p0, Lcom/google/android/gms/internal/fc;->oH:Lcom/google/android/gms/ads/doubleclick/PublisherInterstitialAd;

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/gms/internal/ex;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/internal/fc;->oI:Lcom/google/android/gms/ads/doubleclick/c;

    iget-object v1, p0, Lcom/google/android/gms/internal/fc;->oH:Lcom/google/android/gms/ads/doubleclick/PublisherInterstitialAd;

    new-instance v2, Lcom/google/android/gms/internal/fa;

    invoke-direct {v2, p1}, Lcom/google/android/gms/internal/fa;-><init>(Lcom/google/android/gms/internal/ex;)V

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/ads/doubleclick/c;->a(Lcom/google/android/gms/ads/doubleclick/PublisherInterstitialAd;Lcom/google/android/gms/ads/doubleclick/a;)V

    return-void
.end method

.method public f(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/internal/fc;->oI:Lcom/google/android/gms/ads/doubleclick/c;

    iget-object v1, p0, Lcom/google/android/gms/internal/fc;->oH:Lcom/google/android/gms/ads/doubleclick/PublisherInterstitialAd;

    invoke-interface {v0, v1, p1, p2}, Lcom/google/android/gms/ads/doubleclick/c;->a(Lcom/google/android/gms/ads/doubleclick/PublisherInterstitialAd;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
