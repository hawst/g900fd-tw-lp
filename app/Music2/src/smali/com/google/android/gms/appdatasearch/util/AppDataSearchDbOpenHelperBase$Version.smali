.class Lcom/google/android/gms/appdatasearch/util/AppDataSearchDbOpenHelperBase$Version;
.super Ljava/lang/Object;
.source "AppDataSearchDbOpenHelperBase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/appdatasearch/util/AppDataSearchDbOpenHelperBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Version"
.end annotation


# instance fields
.field private final mVersion:I


# direct methods
.method public constructor <init>(I)V
    .locals 0
    .param p1, "version"    # I

    .prologue
    .line 438
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 439
    iput p1, p0, Lcom/google/android/gms/appdatasearch/util/AppDataSearchDbOpenHelperBase$Version;->mVersion:I

    .line 440
    return-void
.end method

.method static createVersionTable(Landroid/database/sqlite/SQLiteDatabase;I)V
    .locals 8
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "version"    # I

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 489
    const-string v2, "CREATE TABLE IF NOT EXISTS [%s] ([%s] INTEGER)"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "version_appdatasearch"

    aput-object v5, v3, v4

    const-string v4, "version"

    aput-object v4, v3, v7

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 491
    .local v0, "cmd":Ljava/lang/String;
    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 492
    const-string v2, "version_appdatasearch"

    invoke-virtual {p0, v2, v6, v6}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 493
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1, v7}, Landroid/content/ContentValues;-><init>(I)V

    .line 494
    .local v1, "values":Landroid/content/ContentValues;
    const-string v2, "version"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 495
    const-string v2, "version_appdatasearch"

    invoke-virtual {p0, v2, v6, v1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 496
    return-void
.end method

.method static getVersion(Landroid/database/sqlite/SQLiteDatabase;)I
    .locals 10
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    const/4 v3, 0x0

    const/4 v9, 0x0

    .line 465
    const-string v0, "version_appdatasearch"

    invoke-static {p0, v0}, Lcom/google/android/gms/appdatasearch/util/MasterTableHelper;->tableExists(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 468
    invoke-static {p0, v9}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchDbOpenHelperBase$Version;->createVersionTable(Landroid/database/sqlite/SQLiteDatabase;I)V

    move v0, v9

    .line 484
    :goto_0
    return v0

    .line 471
    :cond_0
    const-string v1, "version_appdatasearch"

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "version"

    aput-object v0, v2, v9

    move-object v0, p0

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 473
    .local v8, "cursor":Landroid/database/Cursor;
    if-nez v8, :cond_1

    .line 474
    const-string v0, "AppDataSearchHelper"

    const-string v1, "Empty version table."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v9

    .line 475
    goto :goto_0

    .line 478
    :cond_1
    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_2

    .line 479
    const-string v0, "AppDataSearchHelper"

    const-string v1, "Empty version table."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 484
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    move v0, v9

    goto :goto_0

    .line 482
    :cond_2
    :try_start_1
    const-string v0, "version"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    .line 484
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method static updateVersion(Landroid/database/sqlite/SQLiteDatabase;I)V
    .locals 4
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "version"    # I

    .prologue
    const/4 v3, 0x0

    .line 459
    new-instance v0, Landroid/content/ContentValues;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/content/ContentValues;-><init>(I)V

    .line 460
    .local v0, "values":Landroid/content/ContentValues;
    const-string v1, "version"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 461
    const-string v1, "version_appdatasearch"

    invoke-virtual {p0, v1, v0, v3, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 462
    return-void
.end method


# virtual methods
.method public create(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 443
    iget v0, p0, Lcom/google/android/gms/appdatasearch/util/AppDataSearchDbOpenHelperBase$Version;->mVersion:I

    invoke-static {p1, v0}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchDbOpenHelperBase$Version;->createVersionTable(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 444
    return-void
.end method

.method public upgradeIfRequired(Landroid/database/sqlite/SQLiteDatabase;)Z
    .locals 3
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    const/4 v1, 0x1

    .line 447
    invoke-static {p1}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchDbOpenHelperBase$Version;->getVersion(Landroid/database/sqlite/SQLiteDatabase;)I

    move-result v0

    .line 448
    .local v0, "currentVersion":I
    iget v2, p0, Lcom/google/android/gms/appdatasearch/util/AppDataSearchDbOpenHelperBase$Version;->mVersion:I

    if-ge v0, v2, :cond_1

    .line 449
    if-ge v0, v1, :cond_0

    .line 450
    invoke-static {p1}, Lcom/google/android/gms/appdatasearch/util/UpgradeV1;->upgrade(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 452
    :cond_0
    iget v2, p0, Lcom/google/android/gms/appdatasearch/util/AppDataSearchDbOpenHelperBase$Version;->mVersion:I

    invoke-static {p1, v2}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchDbOpenHelperBase$Version;->updateVersion(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 455
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method
