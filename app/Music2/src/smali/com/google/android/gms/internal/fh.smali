.class public Lcom/google/android/gms/internal/fh;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation runtime Lcom/google/android/gms/internal/fe;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/internal/fh$a;
    }
.end annotation


# instance fields
.field private final le:I

.field private final lf:I

.field protected final mg:Lcom/google/android/gms/internal/ha;

.field private final td:Landroid/os/Handler;

.field private final te:J

.field private tf:J

.field private tg:Lcom/google/android/gms/internal/hb$a;

.field protected th:Z

.field protected ti:Z


# direct methods
.method public constructor <init>(Lcom/google/android/gms/internal/hb$a;Lcom/google/android/gms/internal/ha;II)V
    .locals 10

    const-wide/16 v6, 0xc8

    const-wide/16 v8, 0x32

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v1 .. v9}, Lcom/google/android/gms/internal/fh;-><init>(Lcom/google/android/gms/internal/hb$a;Lcom/google/android/gms/internal/ha;IIJJ)V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/internal/hb$a;Lcom/google/android/gms/internal/ha;IIJJ)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p5, p0, Lcom/google/android/gms/internal/fh;->te:J

    iput-wide p7, p0, Lcom/google/android/gms/internal/fh;->tf:J

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/gms/internal/fh;->td:Landroid/os/Handler;

    iput-object p2, p0, Lcom/google/android/gms/internal/fh;->mg:Lcom/google/android/gms/internal/ha;

    iput-object p1, p0, Lcom/google/android/gms/internal/fh;->tg:Lcom/google/android/gms/internal/hb$a;

    iput-boolean v2, p0, Lcom/google/android/gms/internal/fh;->th:Z

    iput-boolean v2, p0, Lcom/google/android/gms/internal/fh;->ti:Z

    iput p4, p0, Lcom/google/android/gms/internal/fh;->lf:I

    iput p3, p0, Lcom/google/android/gms/internal/fh;->le:I

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/internal/fh;)I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/internal/fh;->le:I

    return v0
.end method

.method static synthetic b(Lcom/google/android/gms/internal/fh;)I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/internal/fh;->lf:I

    return v0
.end method

.method static synthetic c(Lcom/google/android/gms/internal/fh;)J
    .locals 4

    iget-wide v0, p0, Lcom/google/android/gms/internal/fh;->tf:J

    const-wide/16 v2, 0x1

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/gms/internal/fh;->tf:J

    return-wide v0
.end method

.method static synthetic d(Lcom/google/android/gms/internal/fh;)J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/gms/internal/fh;->tf:J

    return-wide v0
.end method

.method static synthetic e(Lcom/google/android/gms/internal/fh;)Lcom/google/android/gms/internal/hb$a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/fh;->tg:Lcom/google/android/gms/internal/hb$a;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/gms/internal/fh;)J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/gms/internal/fh;->te:J

    return-wide v0
.end method

.method static synthetic g(Lcom/google/android/gms/internal/fh;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/fh;->td:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/google/android/gms/internal/fp;Lcom/google/android/gms/internal/hf;)V
    .locals 6

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/google/android/gms/internal/fh;->mg:Lcom/google/android/gms/internal/ha;

    invoke-virtual {v0, p2}, Lcom/google/android/gms/internal/ha;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    iget-object v0, p0, Lcom/google/android/gms/internal/fh;->mg:Lcom/google/android/gms/internal/ha;

    iget-object v1, p1, Lcom/google/android/gms/internal/fp;->rP:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    move-object v1, v5

    :goto_0
    iget-object v2, p1, Lcom/google/android/gms/internal/fp;->tG:Ljava/lang/String;

    const-string v3, "text/html"

    const-string v4, "UTF-8"

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/internal/ha;->loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    iget-object v1, p1, Lcom/google/android/gms/internal/fp;->rP:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/gms/internal/go;->Q(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public b(Lcom/google/android/gms/internal/fp;)V
    .locals 3

    new-instance v0, Lcom/google/android/gms/internal/hf;

    iget-object v1, p0, Lcom/google/android/gms/internal/fh;->mg:Lcom/google/android/gms/internal/ha;

    iget-object v2, p1, Lcom/google/android/gms/internal/fp;->tP:Ljava/lang/String;

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/gms/internal/hf;-><init>(Lcom/google/android/gms/internal/fh;Lcom/google/android/gms/internal/ha;Ljava/lang/String;)V

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/internal/fh;->a(Lcom/google/android/gms/internal/fp;Lcom/google/android/gms/internal/hf;)V

    return-void
.end method

.method public cA()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/internal/fh;->ti:Z

    return v0
.end method

.method public cx()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/gms/internal/fh;->td:Landroid/os/Handler;

    iget-wide v2, p0, Lcom/google/android/gms/internal/fh;->te:J

    invoke-virtual {v0, p0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public declared-synchronized cy()V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/gms/internal/fh;->th:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized cz()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/internal/fh;->th:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public run()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/internal/fh;->mg:Lcom/google/android/gms/internal/ha;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/internal/fh;->cz()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/internal/fh;->tg:Lcom/google/android/gms/internal/hb$a;

    iget-object v1, p0, Lcom/google/android/gms/internal/fh;->mg:Lcom/google/android/gms/internal/ha;

    invoke-interface {v0, v1}, Lcom/google/android/gms/internal/hb$a;->a(Lcom/google/android/gms/internal/ha;)V

    :goto_0
    return-void

    :cond_1
    new-instance v0, Lcom/google/android/gms/internal/fh$a;

    iget-object v1, p0, Lcom/google/android/gms/internal/fh;->mg:Lcom/google/android/gms/internal/ha;

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/internal/fh$a;-><init>(Lcom/google/android/gms/internal/fh;Landroid/webkit/WebView;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/fh$a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method
