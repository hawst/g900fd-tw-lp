.class public abstract Lcom/google/android/gms/appdatasearch/util/AppDataSearchProviderBase;
.super Landroid/content/ContentProvider;
.source "AppDataSearchProviderBase.java"

# interfaces
.implements Lcom/google/android/gms/appdatasearch/util/AppDataSearchDatabase$TableChangeListener;


# instance fields
.field private mDatabase:Lcom/google/android/gms/appdatasearch/util/AppDataSearchDatabase;

.field private final mDatabaseLock:Ljava/lang/Object;

.field protected mSearchClient:Lcom/google/android/gms/appdatasearch/AppDataSearchClient;

.field private final mUriMatcher:Landroid/content/UriMatcher;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 47
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    .line 68
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProviderBase;->mUriMatcher:Landroid/content/UriMatcher;

    .line 70
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProviderBase;->mDatabaseLock:Ljava/lang/Object;

    return-void
.end method

.method protected static final getUriPathFor(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "corpusName"    # Ljava/lang/String;

    .prologue
    .line 137
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "appdatasearch/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p0}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected abstract createDatabase(Lcom/google/android/gms/appdatasearch/util/AppDataSearchDatabase$TableChangeListener;)Lcom/google/android/gms/appdatasearch/util/AppDataSearchDatabase;
.end method

.method protected abstract doGetType(Landroid/net/Uri;)Ljava/lang/String;
.end method

.method protected abstract doOnCreate()Z
.end method

.method protected abstract doQuery(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
.end method

.method protected doQuery(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/CancellationSignal;)Landroid/database/Cursor;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;
    .param p6, "signal"    # Landroid/os/CancellationSignal;

    .prologue
    .line 93
    invoke-virtual/range {p0 .. p5}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProviderBase;->doQuery(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method protected final executeWithConnection(Ljava/util/concurrent/Callable;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6
    .param p2, "callerMethodName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Callable",
            "<TT;>;",
            "Ljava/lang/String;",
            "TT;)TT;"
        }
    .end annotation

    .prologue
    .line 282
    .local p1, "function":Ljava/util/concurrent/Callable;, "Ljava/util/concurrent/Callable<TT;>;"
    .local p3, "failureValue":Ljava/lang/Object;, "TT;"
    invoke-virtual {p0}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProviderBase;->getContext()Landroid/content/Context;

    move-result-object v2

    if-nez v2, :cond_0

    .line 283
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " can\'t be called before onCreate"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 285
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProviderBase;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    if-ne v2, v3, :cond_1

    .line 286
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " can\'t be called on main thread"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 289
    :cond_1
    iget-object v3, p0, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProviderBase;->mSearchClient:Lcom/google/android/gms/appdatasearch/AppDataSearchClient;

    monitor-enter v3

    .line 290
    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProviderBase;->mSearchClient:Lcom/google/android/gms/appdatasearch/AppDataSearchClient;

    const-wide/16 v4, 0x7530

    invoke-virtual {v2, v4, v5}, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->connectWithTimeout(J)Lcom/google/android/gms/common/ConnectionResult;

    move-result-object v0

    .line 292
    .local v0, "connectionResult":Lcom/google/android/gms/common/ConnectionResult;
    invoke-virtual {v0}, Lcom/google/android/gms/common/ConnectionResult;->isSuccess()Z

    move-result v2

    if-nez v2, :cond_2

    .line 293
    const-string v2, ".AppDataSearchProvider"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Could not connect to AppDataSearchClient for "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", error "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/google/android/gms/common/ConnectionResult;->getErrorCode()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 295
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 303
    .end local p3    # "failureValue":Ljava/lang/Object;, "TT;"
    :goto_0
    return-object p3

    .line 299
    .restart local p3    # "failureValue":Ljava/lang/Object;, "TT;"
    :cond_2
    :try_start_1
    invoke-interface {p1}, Ljava/util/concurrent/Callable;->call()Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object p3

    .line 303
    .end local p3    # "failureValue":Ljava/lang/Object;, "TT;"
    :try_start_2
    iget-object v2, p0, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProviderBase;->mSearchClient:Lcom/google/android/gms/appdatasearch/AppDataSearchClient;

    invoke-virtual {v2}, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->disconnect()V

    monitor-exit v3

    goto :goto_0

    .line 305
    .end local v0    # "connectionResult":Lcom/google/android/gms/common/ConnectionResult;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2

    .line 300
    .restart local v0    # "connectionResult":Lcom/google/android/gms/common/ConnectionResult;
    .restart local p3    # "failureValue":Ljava/lang/Object;, "TT;"
    :catch_0
    move-exception v1

    .line 301
    .local v1, "e":Ljava/lang/Exception;
    :try_start_3
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 303
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_1
    move-exception v2

    :try_start_4
    iget-object v4, p0, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProviderBase;->mSearchClient:Lcom/google/android/gms/appdatasearch/AppDataSearchClient;

    invoke-virtual {v4}, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->disconnect()V

    throw v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
.end method

.method protected abstract getContentProviderAuthority()Ljava/lang/String;
.end method

.method protected getCorpusNames()[Ljava/lang/String;
    .locals 4

    .prologue
    .line 128
    invoke-virtual {p0}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProviderBase;->getTableMappings()[Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;

    move-result-object v2

    .line 129
    .local v2, "tableMappings":[Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;
    array-length v3, v2

    new-array v1, v3, [Ljava/lang/String;

    .line 130
    .local v1, "result":[Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_0

    .line 131
    aget-object v3, v2, v0

    invoke-virtual {v3}, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;->getCorpusName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v0

    .line 130
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 133
    :cond_0
    return-object v1
.end method

.method protected final getDatabase()Lcom/google/android/gms/appdatasearch/util/AppDataSearchDatabase;
    .locals 2

    .prologue
    .line 99
    iget-object v1, p0, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProviderBase;->mDatabaseLock:Ljava/lang/Object;

    monitor-enter v1

    .line 100
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProviderBase;->mDatabase:Lcom/google/android/gms/appdatasearch/util/AppDataSearchDatabase;

    if-nez v0, :cond_0

    .line 101
    invoke-virtual {p0, p0}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProviderBase;->createDatabase(Lcom/google/android/gms/appdatasearch/util/AppDataSearchDatabase$TableChangeListener;)Lcom/google/android/gms/appdatasearch/util/AppDataSearchDatabase;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProviderBase;->mDatabase:Lcom/google/android/gms/appdatasearch/util/AppDataSearchDatabase;

    .line 103
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 104
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProviderBase;->mDatabase:Lcom/google/android/gms/appdatasearch/util/AppDataSearchDatabase;

    return-object v0

    .line 103
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getTableMappings()[Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;
    .locals 1

    .prologue
    .line 108
    invoke-virtual {p0}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProviderBase;->getDatabase()Lcom/google/android/gms/appdatasearch/util/AppDataSearchDatabase;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchDatabase;->getTableMappings()[Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;

    move-result-object v0

    return-object v0
.end method

.method public final getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 179
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProviderBase;->mUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 180
    invoke-virtual {p0, p1}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProviderBase;->doGetType(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 183
    :goto_0
    return-object v0

    .line 182
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProviderBase;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->verifyContentProviderClient(Landroid/content/Context;)V

    .line 183
    const-string v0, "vnd.android.cursor.dir/vnd.goodle.appdatasearch"

    goto :goto_0
.end method

.method public onCreate()Z
    .locals 6

    .prologue
    .line 113
    new-instance v4, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;

    invoke-virtual {p0}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProviderBase;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProviderBase;->mSearchClient:Lcom/google/android/gms/appdatasearch/AppDataSearchClient;

    .line 116
    invoke-virtual {p0}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProviderBase;->doOnCreate()Z

    move-result v3

    .line 118
    .local v3, "subclassSuccessful":Z
    invoke-virtual {p0}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProviderBase;->getContentProviderAuthority()Ljava/lang/String;

    move-result-object v0

    .line 119
    .local v0, "authority":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProviderBase;->getCorpusNames()[Ljava/lang/String;

    move-result-object v1

    .line 120
    .local v1, "corpusNames":[Ljava/lang/String;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v4, v1

    if-ge v2, v4, :cond_0

    .line 121
    iget-object v4, p0, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProviderBase;->mUriMatcher:Landroid/content/UriMatcher;

    aget-object v5, v1, v2

    invoke-static {v5}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProviderBase;->getUriPathFor(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v0, v5, v2}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 120
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 124
    :cond_0
    return v3
.end method

.method public final onTableChanged(Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;)Z
    .locals 4
    .param p1, "table"    # Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;

    .prologue
    .line 210
    invoke-virtual {p0}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProviderBase;->getTableMappings()[Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 211
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "The table "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;->getCorpusName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " does not have a registered CorpusTableMapping."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 215
    :cond_0
    new-instance v0, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProviderBase$1;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProviderBase$1;-><init>(Lcom/google/android/gms/appdatasearch/util/AppDataSearchProviderBase;Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;)V

    .line 222
    .local v0, "function":Ljava/util/concurrent/Callable;, "Ljava/util/concurrent/Callable<Ljava/lang/Boolean;>;"
    const-string v1, "onTableChanged"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProviderBase;->executeWithConnection(Ljava/util/concurrent/Callable;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    return v1
.end method

.method public final query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 7
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 143
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProviderBase;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/CancellationSignal;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public final query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/CancellationSignal;)Landroid/database/Cursor;
    .locals 8
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;
    .param p6, "signal"    # Landroid/os/CancellationSignal;

    .prologue
    .line 149
    iget-object v2, p0, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProviderBase;->mUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v2, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v6

    .line 150
    .local v6, "match":I
    const/4 v2, -0x1

    if-ne v6, v2, :cond_0

    .line 151
    invoke-virtual/range {p0 .. p6}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProviderBase;->doQuery(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/CancellationSignal;)Landroid/database/Cursor;

    move-result-object v2

    .line 173
    :goto_0
    return-object v2

    .line 153
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProviderBase;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->verifyContentProviderClient(Landroid/content/Context;)V

    .line 154
    invoke-static {p1, p2, p3, p4, p5}, Lcom/google/android/gms/appdatasearch/SyncContentProviderHelper$SyncQuery;->parse(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/SyncContentProviderHelper$SyncQuery;

    move-result-object v7

    .line 158
    .local v7, "query":Lcom/google/android/gms/appdatasearch/SyncContentProviderHelper$SyncQuery;
    invoke-virtual {p0}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProviderBase;->getDatabase()Lcom/google/android/gms/appdatasearch/util/AppDataSearchDatabase;

    move-result-object v0

    .line 159
    .local v0, "manager":Lcom/google/android/gms/appdatasearch/util/AppDataSearchDatabase;
    invoke-virtual {p0}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProviderBase;->getTableMappings()[Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;

    move-result-object v2

    aget-object v1, v2, v6

    .line 161
    .local v1, "tableSpec":Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;
    invoke-virtual {v7}, Lcom/google/android/gms/appdatasearch/SyncContentProviderHelper$SyncQuery;->wantsFullSync()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 165
    invoke-interface {v0, v1}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchDatabase;->recreateSequenceTable(Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;)V

    .line 168
    :cond_1
    invoke-virtual {v7}, Lcom/google/android/gms/appdatasearch/SyncContentProviderHelper$SyncQuery;->isValidDocumentsQuery()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 169
    invoke-virtual {v7}, Lcom/google/android/gms/appdatasearch/SyncContentProviderHelper$SyncQuery;->getLastSeqNo()J

    move-result-wide v2

    invoke-virtual {v7}, Lcom/google/android/gms/appdatasearch/SyncContentProviderHelper$SyncQuery;->getLimit()J

    move-result-wide v4

    invoke-interface/range {v0 .. v5}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchDatabase;->querySequenceTable(Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;JJ)Landroid/database/Cursor;

    move-result-object v2

    goto :goto_0

    .line 170
    :cond_2
    invoke-virtual {v7}, Lcom/google/android/gms/appdatasearch/SyncContentProviderHelper$SyncQuery;->isValidTagsQuery()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 171
    invoke-virtual {v7}, Lcom/google/android/gms/appdatasearch/SyncContentProviderHelper$SyncQuery;->getLastSeqNo()J

    move-result-wide v2

    invoke-virtual {v7}, Lcom/google/android/gms/appdatasearch/SyncContentProviderHelper$SyncQuery;->getLimit()J

    move-result-wide v4

    invoke-interface/range {v0 .. v5}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchDatabase;->queryTagsTable(Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;JJ)Landroid/database/Cursor;

    move-result-object v2

    goto :goto_0

    .line 173
    :cond_3
    const/4 v2, 0x0

    goto :goto_0
.end method

.method protected requestIndexingIfRequired(Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;)Z
    .locals 8
    .param p1, "table"    # Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;

    .prologue
    .line 317
    iget-object v5, p0, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProviderBase;->mSearchClient:Lcom/google/android/gms/appdatasearch/AppDataSearchClient;

    invoke-virtual {p1}, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;->getCorpusName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->getCorpusStatus(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/CorpusStatus;

    move-result-object v0

    .line 318
    .local v0, "corpusStatus":Lcom/google/android/gms/appdatasearch/CorpusStatus;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/appdatasearch/CorpusStatus;->found()Z

    move-result v5

    if-nez v5, :cond_2

    .line 319
    :cond_0
    if-nez v0, :cond_1

    const-string v4, "Couldn\'t fetch status for"

    .line 320
    .local v4, "msg":Ljava/lang/String;
    :goto_0
    const-string v5, ".AppDataSearchProvider"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " corpus \'"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p1}, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;->getCorpusName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\'"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 321
    const/4 v5, 0x0

    .line 332
    .end local v4    # "msg":Ljava/lang/String;
    :goto_1
    return v5

    .line 319
    :cond_1
    const-string v4, "Couldn\'t find"

    goto :goto_0

    .line 324
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProviderBase;->getDatabase()Lcom/google/android/gms/appdatasearch/util/AppDataSearchDatabase;

    move-result-object v1

    .line 325
    .local v1, "manager":Lcom/google/android/gms/appdatasearch/util/AppDataSearchDatabase;
    invoke-virtual {v0}, Lcom/google/android/gms/appdatasearch/CorpusStatus;->getLastCommittedSeqno()J

    move-result-wide v6

    invoke-interface {v1, p1, v6, v7}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchDatabase;->cleanSequenceTable(Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;J)V

    .line 327
    invoke-interface {v1, p1}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchDatabase;->getMaxSeqno(Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;)J

    move-result-wide v2

    .line 328
    .local v2, "maxSeqno":J
    invoke-virtual {v0}, Lcom/google/android/gms/appdatasearch/CorpusStatus;->getLastIndexedSeqno()J

    move-result-wide v6

    cmp-long v5, v2, v6

    if-lez v5, :cond_3

    .line 330
    iget-object v5, p0, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProviderBase;->mSearchClient:Lcom/google/android/gms/appdatasearch/AppDataSearchClient;

    invoke-virtual {p1}, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;->getCorpusName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6, v2, v3}, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->requestIndexing(Ljava/lang/String;J)Z

    move-result v5

    goto :goto_1

    .line 332
    :cond_3
    const/4 v5, 0x1

    goto :goto_1
.end method
