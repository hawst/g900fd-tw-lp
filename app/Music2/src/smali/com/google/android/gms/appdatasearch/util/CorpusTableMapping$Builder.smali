.class public Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;
.super Ljava/lang/Object;
.source "CorpusTableMapping.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field protected mCondition:Ljava/lang/String;

.field protected mCorpusName:Ljava/lang/String;

.field protected mCreatedTimestampMsSpec:Ljava/lang/String;

.field protected mIsUriColumnUnique:Z

.field protected mScore:Ljava/lang/String;

.field protected final mSectionToColumnNameMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected mTableName:Ljava/lang/String;

.field protected mUriCol:Ljava/lang/String;

.field protected mViewName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 126
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 127
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;->mSectionToColumnNameMap:Ljava/util/Map;

    .line 128
    return-void
.end method


# virtual methods
.method public build()Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;
    .locals 10

    .prologue
    .line 226
    new-instance v0, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;

    iget-object v1, p0, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;->mCorpusName:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;->mTableName:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;->mUriCol:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;->mScore:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;->mCreatedTimestampMsSpec:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;->mSectionToColumnNameMap:Ljava/util/Map;

    iget-object v7, p0, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;->mCondition:Ljava/lang/String;

    iget-object v8, p0, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;->mViewName:Ljava/lang/String;

    iget-boolean v9, p0, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;->mIsUriColumnUnique:Z

    invoke-direct/range {v0 .. v9}, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Z)V

    return-object v0
.end method

.method protected checkSectionNotAlreadyAdded(Ljava/lang/String;)V
    .locals 3
    .param p1, "sectionName"    # Ljava/lang/String;

    .prologue
    .line 202
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;->mSectionToColumnNameMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 203
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Corpus map already contains mapping for section "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 206
    :cond_0
    return-void
.end method

.method public columnForSection(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;
    .locals 1
    .param p1, "section"    # Ljava/lang/String;
    .param p2, "column"    # Ljava/lang/String;

    .prologue
    .line 209
    invoke-virtual {p0, p1}, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;->checkSectionNotAlreadyAdded(Ljava/lang/String;)V

    .line 210
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;->mSectionToColumnNameMap:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 211
    return-object p0
.end method

.method public condition(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;
    .locals 0
    .param p1, "condition"    # Ljava/lang/String;

    .prologue
    .line 221
    iput-object p1, p0, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;->mCondition:Ljava/lang/String;

    .line 222
    return-object p0
.end method

.method public corpus(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;
    .locals 2
    .param p1, "corpusId"    # Ljava/lang/String;

    .prologue
    .line 162
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 163
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "A valid corpus ID must be supplied"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 165
    :cond_0
    iput-object p1, p0, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;->mCorpusName:Ljava/lang/String;

    .line 166
    return-object p0
.end method

.method public score(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;
    .locals 0
    .param p1, "scoreSpec"    # Ljava/lang/String;

    .prologue
    .line 192
    iput-object p1, p0, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;->mScore:Ljava/lang/String;

    .line 193
    return-object p0
.end method

.method public table(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;
    .locals 2
    .param p1, "tableName"    # Ljava/lang/String;

    .prologue
    .line 135
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 136
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "A valid tableName must be supplied"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 138
    :cond_0
    iput-object p1, p0, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;->mTableName:Ljava/lang/String;

    .line 139
    return-object p0
.end method

.method public uriColumn(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;
    .locals 1
    .param p1, "columnName"    # Ljava/lang/String;

    .prologue
    .line 175
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;->uriColumn(Ljava/lang/String;Z)Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;

    move-result-object v0

    return-object v0
.end method

.method public uriColumn(Ljava/lang/String;Z)Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;
    .locals 0
    .param p1, "column"    # Ljava/lang/String;
    .param p2, "isUnique"    # Z

    .prologue
    .line 186
    iput-object p1, p0, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;->mUriCol:Ljava/lang/String;

    .line 187
    iput-boolean p2, p0, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;->mIsUriColumnUnique:Z

    .line 188
    return-object p0
.end method
