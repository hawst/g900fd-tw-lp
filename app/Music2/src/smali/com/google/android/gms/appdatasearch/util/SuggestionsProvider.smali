.class public final Lcom/google/android/gms/appdatasearch/util/SuggestionsProvider;
.super Landroid/content/ContentProvider;
.source "SuggestionsProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/appdatasearch/util/SuggestionsProvider$CursorBuilder;,
        Lcom/google/android/gms/appdatasearch/util/SuggestionsProvider$IcingConnector;
    }
.end annotation


# instance fields
.field private mIcingConnector:Lcom/google/android/gms/appdatasearch/util/SuggestionsProvider$IcingConnector;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    .line 173
    return-void
.end method

.method private static checkUriCorrect(Landroid/net/Uri;)Z
    .locals 2
    .param p0, "uri"    # Landroid/net/Uri;

    .prologue
    .line 98
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    const-string v1, "/search_suggest_query"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static getLimit(Landroid/net/Uri;)I
    .locals 2
    .param p0, "uri"    # Landroid/net/Uri;

    .prologue
    .line 102
    const-string v1, "limit"

    invoke-virtual {p0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 103
    .local v0, "param":Ljava/lang/String;
    if-nez v0, :cond_0

    const/16 v1, 0xa

    .line 104
    :goto_0
    return v1

    :cond_0
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    goto :goto_0
.end method

.method private static getQuery(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p0, "uri"    # Landroid/net/Uri;

    .prologue
    .line 108
    invoke-virtual {p0}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    .line 109
    .local v0, "query":Ljava/lang/String;
    if-nez v0, :cond_0

    const-string v0, ""

    .end local v0    # "query":Ljava/lang/String;
    :cond_0
    return-object v0
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 82
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 66
    invoke-static {p1}, Lcom/google/android/gms/appdatasearch/util/SuggestionsProvider;->checkUriCorrect(Landroid/net/Uri;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 67
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "vnd.android.cursor.dir/vnd.android.search.suggest"

    goto :goto_0
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 72
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public onCreate()Z
    .locals 2

    .prologue
    .line 60
    new-instance v0, Lcom/google/android/gms/appdatasearch/util/SuggestionsProvider$IcingConnector;

    invoke-virtual {p0}, Lcom/google/android/gms/appdatasearch/util/SuggestionsProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/appdatasearch/util/SuggestionsProvider$IcingConnector;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/appdatasearch/util/SuggestionsProvider;->mIcingConnector:Lcom/google/android/gms/appdatasearch/util/SuggestionsProvider$IcingConnector;

    .line 61
    const/4 v0, 0x1

    return v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 4
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 88
    invoke-virtual {p0}, Lcom/google/android/gms/appdatasearch/util/SuggestionsProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "android.permission.GLOBAL_SEARCH"

    const-string v3, "Access Denied"

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingPermission(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    invoke-static {p1}, Lcom/google/android/gms/appdatasearch/util/SuggestionsProvider;->checkUriCorrect(Landroid/net/Uri;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Bad Uri."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 92
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/appdatasearch/util/SuggestionsProvider;->mIcingConnector:Lcom/google/android/gms/appdatasearch/util/SuggestionsProvider$IcingConnector;

    invoke-static {p1}, Lcom/google/android/gms/appdatasearch/util/SuggestionsProvider;->getQuery(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1}, Lcom/google/android/gms/appdatasearch/util/SuggestionsProvider;->getLimit(Landroid/net/Uri;)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/appdatasearch/util/SuggestionsProvider$IcingConnector;->getSuggestions(Ljava/lang/String;I)Lcom/google/android/gms/appdatasearch/SearchResults;

    move-result-object v0

    .line 94
    .local v0, "suggestions":Lcom/google/android/gms/appdatasearch/SearchResults;
    # invokes: Lcom/google/android/gms/appdatasearch/util/SuggestionsProvider$CursorBuilder;->createSuggestionsCursor(Lcom/google/android/gms/appdatasearch/SearchResults;)Landroid/database/Cursor;
    invoke-static {v0}, Lcom/google/android/gms/appdatasearch/util/SuggestionsProvider$CursorBuilder;->access$000(Lcom/google/android/gms/appdatasearch/SearchResults;)Landroid/database/Cursor;

    move-result-object v1

    return-object v1
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 77
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
