.class public abstract Lcom/google/android/gms/internal/fl;
.super Lcom/google/android/gms/internal/gl;


# annotations
.annotation runtime Lcom/google/android/gms/internal/fe;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/internal/fl$b;,
        Lcom/google/android/gms/internal/fl$a;
    }
.end annotation


# instance fields
.field private final pR:Lcom/google/android/gms/internal/fn;

.field private final tu:Lcom/google/android/gms/internal/fk$a;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/internal/fn;Lcom/google/android/gms/internal/fk$a;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/internal/gl;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/internal/fl;->pR:Lcom/google/android/gms/internal/fn;

    iput-object p2, p0, Lcom/google/android/gms/internal/fl;->tu:Lcom/google/android/gms/internal/fk$a;

    return-void
.end method

.method private static a(Lcom/google/android/gms/internal/fr;Lcom/google/android/gms/internal/fn;)Lcom/google/android/gms/internal/fp;
    .locals 3

    const/4 v0, 0x0

    :try_start_0
    invoke-interface {p0, p1}, Lcom/google/android/gms/internal/fr;->b(Lcom/google/android/gms/internal/fn;)Lcom/google/android/gms/internal/fp;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_3

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    const-string v2, "Could not fetch ad response from ad request service."

    invoke-static {v2, v1}, Lcom/google/android/gms/internal/gx;->w(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :catch_1
    move-exception v1

    const-string v2, "Could not fetch ad response from ad request service due to an Exception."

    invoke-static {v2, v1}, Lcom/google/android/gms/internal/gx;->w(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :catch_2
    move-exception v1

    const-string v2, "Could not fetch ad response from ad request service due to an Exception."

    invoke-static {v2, v1}, Lcom/google/android/gms/internal/gx;->w(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :catch_3
    move-exception v1

    invoke-static {v1}, Lcom/google/android/gms/internal/gg;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public abstract cB()V
.end method

.method public abstract cC()Lcom/google/android/gms/internal/fr;
.end method

.method public final cn()V
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/internal/fl;->cC()Lcom/google/android/gms/internal/fr;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/internal/fp;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/gms/internal/fp;-><init>(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/internal/fl;->cB()V

    iget-object v1, p0, Lcom/google/android/gms/internal/fl;->tu:Lcom/google/android/gms/internal/fk$a;

    invoke-interface {v1, v0}, Lcom/google/android/gms/internal/fk$a;->a(Lcom/google/android/gms/internal/fp;)V

    return-void

    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/google/android/gms/internal/fl;->pR:Lcom/google/android/gms/internal/fn;

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/fl;->a(Lcom/google/android/gms/internal/fr;Lcom/google/android/gms/internal/fn;)Lcom/google/android/gms/internal/fp;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/gms/internal/fp;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/gms/internal/fp;-><init>(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/gms/internal/fl;->cB()V

    throw v0
.end method

.method public final onStop()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/gms/internal/fl;->cB()V

    return-void
.end method
