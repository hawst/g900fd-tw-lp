.class public Lcom/google/android/gms/internal/dl;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lcom/google/android/gms/internal/fe;
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mJ:Landroid/view/WindowManager;

.field private final mg:Lcom/google/android/gms/internal/ha;

.field private final rh:Lcom/google/android/gms/internal/bq;

.field ri:Landroid/util/DisplayMetrics;

.field private rj:F

.field rk:I

.field rl:I

.field private rm:I

.field private rn:I

.field private ro:I

.field private rp:[I


# direct methods
.method public constructor <init>(Lcom/google/android/gms/internal/ha;Landroid/content/Context;Lcom/google/android/gms/internal/bq;)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/google/android/gms/internal/dl;->rk:I

    iput v0, p0, Lcom/google/android/gms/internal/dl;->rl:I

    iput v0, p0, Lcom/google/android/gms/internal/dl;->rn:I

    iput v0, p0, Lcom/google/android/gms/internal/dl;->ro:I

    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/gms/internal/dl;->rp:[I

    iput-object p1, p0, Lcom/google/android/gms/internal/dl;->mg:Lcom/google/android/gms/internal/ha;

    iput-object p2, p0, Lcom/google/android/gms/internal/dl;->mContext:Landroid/content/Context;

    iput-object p3, p0, Lcom/google/android/gms/internal/dl;->rh:Lcom/google/android/gms/internal/bq;

    const-string v0, "window"

    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, Lcom/google/android/gms/internal/dl;->mJ:Landroid/view/WindowManager;

    invoke-direct {p0}, Lcom/google/android/gms/internal/dl;->bL()V

    invoke-virtual {p0}, Lcom/google/android/gms/internal/dl;->bM()V

    invoke-direct {p0}, Lcom/google/android/gms/internal/dl;->bN()V

    return-void
.end method

.method private bL()V
    .locals 2

    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/dl;->ri:Landroid/util/DisplayMetrics;

    iget-object v0, p0, Lcom/google/android/gms/internal/dl;->mJ:Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/internal/dl;->ri:Landroid/util/DisplayMetrics;

    invoke-virtual {v0, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    iget-object v1, p0, Lcom/google/android/gms/internal/dl;->ri:Landroid/util/DisplayMetrics;

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    iput v1, p0, Lcom/google/android/gms/internal/dl;->rj:F

    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/internal/dl;->rm:I

    return-void
.end method

.method private bN()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/gms/internal/dl;->mg:Lcom/google/android/gms/internal/ha;

    iget-object v1, p0, Lcom/google/android/gms/internal/dl;->rp:[I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/ha;->getLocationOnScreen([I)V

    iget-object v0, p0, Lcom/google/android/gms/internal/dl;->mg:Lcom/google/android/gms/internal/ha;

    invoke-virtual {v0, v2, v2}, Lcom/google/android/gms/internal/ha;->measure(II)V

    const/high16 v0, 0x43200000    # 160.0f

    iget-object v1, p0, Lcom/google/android/gms/internal/dl;->ri:Landroid/util/DisplayMetrics;

    iget v1, v1, Landroid/util/DisplayMetrics;->densityDpi:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/gms/internal/dl;->mg:Lcom/google/android/gms/internal/ha;

    invoke-virtual {v1}, Lcom/google/android/gms/internal/ha;->getMeasuredWidth()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v0

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/internal/dl;->rn:I

    iget-object v1, p0, Lcom/google/android/gms/internal/dl;->mg:Lcom/google/android/gms/internal/ha;

    invoke-virtual {v1}, Lcom/google/android/gms/internal/ha;->getMeasuredHeight()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/internal/dl;->ro:I

    return-void
.end method

.method private bT()Lcom/google/android/gms/internal/dk;
    .locals 2

    new-instance v0, Lcom/google/android/gms/internal/dk$a;

    invoke-direct {v0}, Lcom/google/android/gms/internal/dk$a;-><init>()V

    iget-object v1, p0, Lcom/google/android/gms/internal/dl;->rh:Lcom/google/android/gms/internal/bq;

    invoke-virtual {v1}, Lcom/google/android/gms/internal/bq;->bj()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/dk$a;->j(Z)Lcom/google/android/gms/internal/dk$a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/internal/dl;->rh:Lcom/google/android/gms/internal/bq;

    invoke-virtual {v1}, Lcom/google/android/gms/internal/bq;->bk()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/dk$a;->i(Z)Lcom/google/android/gms/internal/dk$a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/internal/dl;->rh:Lcom/google/android/gms/internal/bq;

    invoke-virtual {v1}, Lcom/google/android/gms/internal/bq;->bo()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/dk$a;->k(Z)Lcom/google/android/gms/internal/dk$a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/internal/dl;->rh:Lcom/google/android/gms/internal/bq;

    invoke-virtual {v1}, Lcom/google/android/gms/internal/bq;->bl()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/dk$a;->l(Z)Lcom/google/android/gms/internal/dk$a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/internal/dl;->rh:Lcom/google/android/gms/internal/bq;

    invoke-virtual {v1}, Lcom/google/android/gms/internal/bq;->bm()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/dk$a;->m(Z)Lcom/google/android/gms/internal/dk$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/internal/dk$a;->bK()Lcom/google/android/gms/internal/dk;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method bM()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/internal/dl;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/internal/go;->s(Landroid/content/Context;)I

    move-result v0

    const/high16 v1, 0x43200000    # 160.0f

    iget-object v2, p0, Lcom/google/android/gms/internal/dl;->ri:Landroid/util/DisplayMetrics;

    iget v2, v2, Landroid/util/DisplayMetrics;->densityDpi:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    iget-object v2, p0, Lcom/google/android/gms/internal/dl;->ri:Landroid/util/DisplayMetrics;

    iget v2, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v2, v2

    mul-float/2addr v2, v1

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    iput v2, p0, Lcom/google/android/gms/internal/dl;->rk:I

    iget-object v2, p0, Lcom/google/android/gms/internal/dl;->ri:Landroid/util/DisplayMetrics;

    iget v2, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    sub-int v0, v2, v0

    int-to-float v0, v0

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/internal/dl;->rl:I

    return-void
.end method

.method public bO()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/gms/internal/dl;->bR()V

    invoke-virtual {p0}, Lcom/google/android/gms/internal/dl;->bS()V

    invoke-virtual {p0}, Lcom/google/android/gms/internal/dl;->bQ()V

    invoke-virtual {p0}, Lcom/google/android/gms/internal/dl;->bP()V

    return-void
.end method

.method public bP()V
    .locals 3

    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/internal/gx;->u(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Dispatching Ready Event."

    invoke-static {v0}, Lcom/google/android/gms/internal/gx;->i(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/internal/dl;->mg:Lcom/google/android/gms/internal/ha;

    const-string v1, "onReadyEventReceived"

    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/internal/ha;->b(Ljava/lang/String;Lorg/json/JSONObject;)V

    return-void
.end method

.method public bQ()V
    .locals 4

    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    const-string v1, "x"

    iget-object v2, p0, Lcom/google/android/gms/internal/dl;->rp:[I

    const/4 v3, 0x0

    aget v2, v2, v3

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "y"

    iget-object v2, p0, Lcom/google/android/gms/internal/dl;->rp:[I

    const/4 v3, 0x1

    aget v2, v2, v3

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "width"

    iget v2, p0, Lcom/google/android/gms/internal/dl;->rn:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "height"

    iget v2, p0, Lcom/google/android/gms/internal/dl;->ro:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/internal/dl;->mg:Lcom/google/android/gms/internal/ha;

    const-string v2, "onDefaultPositionReceived"

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/internal/ha;->b(Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "Error occured while dispatching default position."

    invoke-static {v1, v0}, Lcom/google/android/gms/internal/gx;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public bR()V
    .locals 4

    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    const-string v1, "width"

    iget v2, p0, Lcom/google/android/gms/internal/dl;->rk:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "height"

    iget v2, p0, Lcom/google/android/gms/internal/dl;->rl:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "density"

    iget v2, p0, Lcom/google/android/gms/internal/dl;->rj:F

    float-to-double v2, v2

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "rotation"

    iget v2, p0, Lcom/google/android/gms/internal/dl;->rm:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/internal/dl;->mg:Lcom/google/android/gms/internal/ha;

    const-string v2, "onScreenInfoChanged"

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/internal/ha;->b(Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "Error occured while obtaining screen information."

    invoke-static {v1, v0}, Lcom/google/android/gms/internal/gx;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public bS()V
    .locals 3

    invoke-direct {p0}, Lcom/google/android/gms/internal/dl;->bT()Lcom/google/android/gms/internal/dk;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/internal/dl;->mg:Lcom/google/android/gms/internal/ha;

    const-string v2, "onDeviceFeaturesReceived"

    invoke-virtual {v0}, Lcom/google/android/gms/internal/dk;->toJson()Lorg/json/JSONObject;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/internal/ha;->b(Ljava/lang/String;Lorg/json/JSONObject;)V

    return-void
.end method
