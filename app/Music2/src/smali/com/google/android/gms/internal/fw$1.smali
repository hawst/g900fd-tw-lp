.class final Lcom/google/android/gms/internal/fw$1;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/gms/internal/fw;->a(Landroid/content/Context;Lcom/google/android/gms/internal/br;Lcom/google/android/gms/internal/cn;Lcom/google/android/gms/internal/gc;Lcom/google/android/gms/internal/fn;)Lcom/google/android/gms/internal/fp;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic mY:Landroid/content/Context;

.field final synthetic uk:Lcom/google/android/gms/internal/fn;

.field final synthetic ul:Lcom/google/android/gms/internal/fy;

.field final synthetic um:Lcom/google/android/gms/internal/hb$a;

.field final synthetic un:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/google/android/gms/internal/fn;Lcom/google/android/gms/internal/fy;Lcom/google/android/gms/internal/hb$a;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/internal/fw$1;->mY:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/gms/internal/fw$1;->uk:Lcom/google/android/gms/internal/fn;

    iput-object p3, p0, Lcom/google/android/gms/internal/fw$1;->ul:Lcom/google/android/gms/internal/fy;

    iput-object p4, p0, Lcom/google/android/gms/internal/fw$1;->um:Lcom/google/android/gms/internal/hb$a;

    iput-object p5, p0, Lcom/google/android/gms/internal/fw$1;->un:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/gms/internal/fw$1;->mY:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/internal/bd;

    invoke-direct {v1}, Lcom/google/android/gms/internal/bd;-><init>()V

    const/4 v4, 0x0

    iget-object v3, p0, Lcom/google/android/gms/internal/fw$1;->uk:Lcom/google/android/gms/internal/fn;

    iget-object v5, v3, Lcom/google/android/gms/internal/fn;->lG:Lcom/google/android/gms/internal/gy;

    move v3, v2

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/internal/ha;->a(Landroid/content/Context;Lcom/google/android/gms/internal/bd;ZZLcom/google/android/gms/internal/k;Lcom/google/android/gms/internal/gy;)Lcom/google/android/gms/internal/ha;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/ha;->setWillNotDraw(Z)V

    iget-object v1, p0, Lcom/google/android/gms/internal/fw$1;->ul:Lcom/google/android/gms/internal/fy;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/internal/fy;->b(Lcom/google/android/gms/internal/ha;)V

    invoke-virtual {v0}, Lcom/google/android/gms/internal/ha;->dt()Lcom/google/android/gms/internal/hb;

    move-result-object v1

    const-string v2, "/invalidRequest"

    iget-object v3, p0, Lcom/google/android/gms/internal/fw$1;->ul:Lcom/google/android/gms/internal/fy;

    iget-object v3, v3, Lcom/google/android/gms/internal/fy;->us:Lcom/google/android/gms/internal/cd;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/internal/hb;->a(Ljava/lang/String;Lcom/google/android/gms/internal/cd;)V

    const-string v2, "/loadAdURL"

    iget-object v3, p0, Lcom/google/android/gms/internal/fw$1;->ul:Lcom/google/android/gms/internal/fy;

    iget-object v3, v3, Lcom/google/android/gms/internal/fy;->ut:Lcom/google/android/gms/internal/cd;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/internal/hb;->a(Ljava/lang/String;Lcom/google/android/gms/internal/cd;)V

    const-string v2, "/log"

    sget-object v3, Lcom/google/android/gms/internal/cc;->pH:Lcom/google/android/gms/internal/cd;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/internal/hb;->a(Ljava/lang/String;Lcom/google/android/gms/internal/cd;)V

    iget-object v2, p0, Lcom/google/android/gms/internal/fw$1;->um:Lcom/google/android/gms/internal/hb$a;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/internal/hb;->a(Lcom/google/android/gms/internal/hb$a;)V

    const-string v1, "Loading the JS library."

    invoke-static {v1}, Lcom/google/android/gms/internal/gx;->d(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/internal/fw$1;->un:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/ha;->loadUrl(Ljava/lang/String;)V

    return-void
.end method
