.class Lcom/google/android/gms/appdatasearch/util/AppDataSearchProviderBase$1;
.super Ljava/lang/Object;
.source "AppDataSearchProviderBase.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/gms/appdatasearch/util/AppDataSearchProviderBase;->onTableChanged(Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/gms/appdatasearch/util/AppDataSearchProviderBase;

.field final synthetic val$table:Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;


# direct methods
.method constructor <init>(Lcom/google/android/gms/appdatasearch/util/AppDataSearchProviderBase;Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;)V
    .locals 0

    .prologue
    .line 215
    iput-object p1, p0, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProviderBase$1;->this$0:Lcom/google/android/gms/appdatasearch/util/AppDataSearchProviderBase;

    iput-object p2, p0, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProviderBase$1;->val$table:Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 218
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProviderBase$1;->this$0:Lcom/google/android/gms/appdatasearch/util/AppDataSearchProviderBase;

    iget-object v1, p0, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProviderBase$1;->val$table:Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProviderBase;->requestIndexingIfRequired(Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 215
    invoke-virtual {p0}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProviderBase$1;->call()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
