.class Lcom/google/android/gms/tagmanager/ServiceManagerImpl;
.super Lcom/google/android/gms/tagmanager/ct;


# static fields
.field private static aHv:Lcom/google/android/gms/tagmanager/ServiceManagerImpl;

.field private static final yb:Ljava/lang/Object;


# instance fields
.field private aHm:Lcom/google/android/gms/tagmanager/ar;

.field private volatile aHn:Lcom/google/android/gms/tagmanager/ap;

.field private aHo:I

.field private aHp:Z

.field private aHq:Z

.field private aHr:Z

.field private aHs:Lcom/google/android/gms/tagmanager/as;

.field private aHu:Z

.field private connected:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/gms/tagmanager/ServiceManagerImpl;->yb:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-direct {p0}, Lcom/google/android/gms/tagmanager/ct;-><init>()V

    const v0, 0x1b7740

    iput v0, p0, Lcom/google/android/gms/tagmanager/ServiceManagerImpl;->aHo:I

    iput-boolean v1, p0, Lcom/google/android/gms/tagmanager/ServiceManagerImpl;->aHp:Z

    iput-boolean v2, p0, Lcom/google/android/gms/tagmanager/ServiceManagerImpl;->aHq:Z

    iput-boolean v1, p0, Lcom/google/android/gms/tagmanager/ServiceManagerImpl;->connected:Z

    iput-boolean v1, p0, Lcom/google/android/gms/tagmanager/ServiceManagerImpl;->aHr:Z

    new-instance v0, Lcom/google/android/gms/tagmanager/ServiceManagerImpl$1;

    invoke-direct {v0, p0}, Lcom/google/android/gms/tagmanager/ServiceManagerImpl$1;-><init>(Lcom/google/android/gms/tagmanager/ServiceManagerImpl;)V

    iput-object v0, p0, Lcom/google/android/gms/tagmanager/ServiceManagerImpl;->aHs:Lcom/google/android/gms/tagmanager/as;

    iput-boolean v2, p0, Lcom/google/android/gms/tagmanager/ServiceManagerImpl;->aHu:Z

    return-void
.end method

.method static synthetic e(Lcom/google/android/gms/tagmanager/ServiceManagerImpl;)Lcom/google/android/gms/tagmanager/ar;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/tagmanager/ServiceManagerImpl;->aHm:Lcom/google/android/gms/tagmanager/ar;

    return-object v0
.end method

.method public static getInstance()Lcom/google/android/gms/tagmanager/ServiceManagerImpl;
    .locals 1

    sget-object v0, Lcom/google/android/gms/tagmanager/ServiceManagerImpl;->aHv:Lcom/google/android/gms/tagmanager/ServiceManagerImpl;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/gms/tagmanager/ServiceManagerImpl;

    invoke-direct {v0}, Lcom/google/android/gms/tagmanager/ServiceManagerImpl;-><init>()V

    sput-object v0, Lcom/google/android/gms/tagmanager/ServiceManagerImpl;->aHv:Lcom/google/android/gms/tagmanager/ServiceManagerImpl;

    :cond_0
    sget-object v0, Lcom/google/android/gms/tagmanager/ServiceManagerImpl;->aHv:Lcom/google/android/gms/tagmanager/ServiceManagerImpl;

    return-object v0
.end method


# virtual methods
.method public declared-synchronized dispatch()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/tagmanager/ServiceManagerImpl;->aHq:Z

    if-nez v0, :cond_0

    const-string v0, "Dispatch call queued. Dispatch will run once initialization is complete."

    invoke-static {v0}, Lcom/google/android/gms/tagmanager/bf;->v(Ljava/lang/String;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/tagmanager/ServiceManagerImpl;->aHp:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/tagmanager/ServiceManagerImpl;->aHn:Lcom/google/android/gms/tagmanager/ap;

    new-instance v1, Lcom/google/android/gms/tagmanager/ServiceManagerImpl$3;

    invoke-direct {v1, p0}, Lcom/google/android/gms/tagmanager/ServiceManagerImpl$3;-><init>(Lcom/google/android/gms/tagmanager/ServiceManagerImpl;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/tagmanager/ap;->b(Ljava/lang/Runnable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
