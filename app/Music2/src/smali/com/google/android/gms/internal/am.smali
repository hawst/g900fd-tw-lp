.class public interface abstract Lcom/google/android/gms/internal/am;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/internal/am$a;
    }
.end annotation


# virtual methods
.method public abstract a(Lcom/google/android/gms/internal/am$a;)V
.end method

.method public abstract a(Lcom/google/android/gms/internal/y;Lcom/google/android/gms/internal/ds;Lcom/google/android/gms/internal/cb;Lcom/google/android/gms/internal/dv;ZLcom/google/android/gms/internal/ce;)V
.end method

.method public abstract a(Ljava/lang/String;Lcom/google/android/gms/internal/cd;)V
.end method

.method public abstract a(Ljava/lang/String;Lorg/json/JSONObject;)V
.end method

.method public abstract destroy()V
.end method

.method public abstract h(Ljava/lang/String;)V
.end method

.method public abstract j(Ljava/lang/String;)V
.end method

.method public abstract pause()V
.end method

.method public abstract resume()V
.end method
