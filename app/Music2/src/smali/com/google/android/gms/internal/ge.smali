.class public final Lcom/google/android/gms/internal/ge;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lcom/google/android/gms/internal/fe;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/internal/ge$a;
    }
.end annotation


# instance fields
.field public final errorCode:I

.field public final orientation:I

.field public final qA:Lcom/google/android/gms/internal/cz;

.field public final qB:Ljava/lang/String;

.field public final qC:Lcom/google/android/gms/internal/ct;

.field public final qg:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final qh:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final qk:J

.field public final qz:Lcom/google/android/gms/internal/cq;

.field public final rN:Lcom/google/android/gms/internal/ha;

.field public final tA:Ljava/lang/String;

.field public final tH:J

.field public final tI:Z

.field public final tJ:J

.field public final tK:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final tN:Ljava/lang/String;

.field public final tx:Lcom/google/android/gms/internal/ba;

.field public final vo:Lorg/json/JSONObject;

.field public final vp:Lcom/google/android/gms/internal/cr;

.field public final vq:Lcom/google/android/gms/internal/bd;

.field public final vr:J

.field public final vs:J

.field public final vt:Lcom/google/android/gms/internal/bv$a;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/internal/ba;Lcom/google/android/gms/internal/ha;Ljava/util/List;ILjava/util/List;Ljava/util/List;IJLjava/lang/String;ZLcom/google/android/gms/internal/cq;Lcom/google/android/gms/internal/cz;Ljava/lang/String;Lcom/google/android/gms/internal/cr;Lcom/google/android/gms/internal/ct;JLcom/google/android/gms/internal/bd;JJJLjava/lang/String;Lorg/json/JSONObject;Lcom/google/android/gms/internal/bv$a;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/internal/ba;",
            "Lcom/google/android/gms/internal/ha;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;IJ",
            "Ljava/lang/String;",
            "Z",
            "Lcom/google/android/gms/internal/cq;",
            "Lcom/google/android/gms/internal/cz;",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/internal/cr;",
            "Lcom/google/android/gms/internal/ct;",
            "J",
            "Lcom/google/android/gms/internal/bd;",
            "JJJ",
            "Ljava/lang/String;",
            "Lorg/json/JSONObject;",
            "Lcom/google/android/gms/internal/bv$a;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/internal/ge;->tx:Lcom/google/android/gms/internal/ba;

    iput-object p2, p0, Lcom/google/android/gms/internal/ge;->rN:Lcom/google/android/gms/internal/ha;

    if-eqz p3, :cond_0

    invoke-static {p3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    :goto_0
    iput-object v2, p0, Lcom/google/android/gms/internal/ge;->qg:Ljava/util/List;

    iput p4, p0, Lcom/google/android/gms/internal/ge;->errorCode:I

    if-eqz p5, :cond_1

    invoke-static {p5}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    :goto_1
    iput-object v2, p0, Lcom/google/android/gms/internal/ge;->qh:Ljava/util/List;

    if-eqz p6, :cond_2

    invoke-static {p6}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    :goto_2
    iput-object v2, p0, Lcom/google/android/gms/internal/ge;->tK:Ljava/util/List;

    iput p7, p0, Lcom/google/android/gms/internal/ge;->orientation:I

    iput-wide p8, p0, Lcom/google/android/gms/internal/ge;->qk:J

    iput-object p10, p0, Lcom/google/android/gms/internal/ge;->tA:Ljava/lang/String;

    iput-boolean p11, p0, Lcom/google/android/gms/internal/ge;->tI:Z

    move-object/from16 v0, p12

    iput-object v0, p0, Lcom/google/android/gms/internal/ge;->qz:Lcom/google/android/gms/internal/cq;

    move-object/from16 v0, p13

    iput-object v0, p0, Lcom/google/android/gms/internal/ge;->qA:Lcom/google/android/gms/internal/cz;

    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/google/android/gms/internal/ge;->qB:Ljava/lang/String;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/google/android/gms/internal/ge;->vp:Lcom/google/android/gms/internal/cr;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/google/android/gms/internal/ge;->qC:Lcom/google/android/gms/internal/ct;

    move-wide/from16 v0, p17

    iput-wide v0, p0, Lcom/google/android/gms/internal/ge;->tJ:J

    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/google/android/gms/internal/ge;->vq:Lcom/google/android/gms/internal/bd;

    move-wide/from16 v0, p20

    iput-wide v0, p0, Lcom/google/android/gms/internal/ge;->tH:J

    move-wide/from16 v0, p22

    iput-wide v0, p0, Lcom/google/android/gms/internal/ge;->vr:J

    move-wide/from16 v0, p24

    iput-wide v0, p0, Lcom/google/android/gms/internal/ge;->vs:J

    move-object/from16 v0, p26

    iput-object v0, p0, Lcom/google/android/gms/internal/ge;->tN:Ljava/lang/String;

    move-object/from16 v0, p27

    iput-object v0, p0, Lcom/google/android/gms/internal/ge;->vo:Lorg/json/JSONObject;

    move-object/from16 v0, p28

    iput-object v0, p0, Lcom/google/android/gms/internal/ge;->vt:Lcom/google/android/gms/internal/bv$a;

    return-void

    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    goto :goto_2
.end method

.method public constructor <init>(Lcom/google/android/gms/internal/ge$a;Lcom/google/android/gms/internal/ha;Lcom/google/android/gms/internal/cq;Lcom/google/android/gms/internal/cz;Ljava/lang/String;Lcom/google/android/gms/internal/ct;Lcom/google/android/gms/internal/bv$a;)V
    .locals 31

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/gms/internal/ge$a;->vu:Lcom/google/android/gms/internal/fn;

    iget-object v3, v2, Lcom/google/android/gms/internal/fn;->tx:Lcom/google/android/gms/internal/ba;

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/gms/internal/ge$a;->vv:Lcom/google/android/gms/internal/fp;

    iget-object v5, v2, Lcom/google/android/gms/internal/fp;->qg:Ljava/util/List;

    move-object/from16 v0, p1

    iget v6, v0, Lcom/google/android/gms/internal/ge$a;->errorCode:I

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/gms/internal/ge$a;->vv:Lcom/google/android/gms/internal/fp;

    iget-object v7, v2, Lcom/google/android/gms/internal/fp;->qh:Ljava/util/List;

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/gms/internal/ge$a;->vv:Lcom/google/android/gms/internal/fp;

    iget-object v8, v2, Lcom/google/android/gms/internal/fp;->tK:Ljava/util/List;

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/gms/internal/ge$a;->vv:Lcom/google/android/gms/internal/fp;

    iget v9, v2, Lcom/google/android/gms/internal/fp;->orientation:I

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/gms/internal/ge$a;->vv:Lcom/google/android/gms/internal/fp;

    iget-wide v10, v2, Lcom/google/android/gms/internal/fp;->qk:J

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/gms/internal/ge$a;->vu:Lcom/google/android/gms/internal/fn;

    iget-object v12, v2, Lcom/google/android/gms/internal/fn;->tA:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/gms/internal/ge$a;->vv:Lcom/google/android/gms/internal/fp;

    iget-boolean v13, v2, Lcom/google/android/gms/internal/fp;->tI:Z

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/gms/internal/ge$a;->vp:Lcom/google/android/gms/internal/cr;

    move-object/from16 v17, v0

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/gms/internal/ge$a;->vv:Lcom/google/android/gms/internal/fp;

    iget-wide v0, v2, Lcom/google/android/gms/internal/fp;->tJ:J

    move-wide/from16 v19, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/gms/internal/ge$a;->lK:Lcom/google/android/gms/internal/bd;

    move-object/from16 v21, v0

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/gms/internal/ge$a;->vv:Lcom/google/android/gms/internal/fp;

    iget-wide v0, v2, Lcom/google/android/gms/internal/fp;->tH:J

    move-wide/from16 v22, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/google/android/gms/internal/ge$a;->vr:J

    move-wide/from16 v24, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/google/android/gms/internal/ge$a;->vs:J

    move-wide/from16 v26, v0

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/gms/internal/ge$a;->vv:Lcom/google/android/gms/internal/fp;

    iget-object v0, v2, Lcom/google/android/gms/internal/fp;->tN:Ljava/lang/String;

    move-object/from16 v28, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/gms/internal/ge$a;->vo:Lorg/json/JSONObject;

    move-object/from16 v29, v0

    move-object/from16 v2, p0

    move-object/from16 v4, p2

    move-object/from16 v14, p3

    move-object/from16 v15, p4

    move-object/from16 v16, p5

    move-object/from16 v18, p6

    move-object/from16 v30, p7

    invoke-direct/range {v2 .. v30}, Lcom/google/android/gms/internal/ge;-><init>(Lcom/google/android/gms/internal/ba;Lcom/google/android/gms/internal/ha;Ljava/util/List;ILjava/util/List;Ljava/util/List;IJLjava/lang/String;ZLcom/google/android/gms/internal/cq;Lcom/google/android/gms/internal/cz;Ljava/lang/String;Lcom/google/android/gms/internal/cr;Lcom/google/android/gms/internal/ct;JLcom/google/android/gms/internal/bd;JJJLjava/lang/String;Lorg/json/JSONObject;Lcom/google/android/gms/internal/bv$a;)V

    return-void
.end method
