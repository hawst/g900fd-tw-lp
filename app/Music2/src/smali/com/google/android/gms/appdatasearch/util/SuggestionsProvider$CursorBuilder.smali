.class Lcom/google/android/gms/appdatasearch/util/SuggestionsProvider$CursorBuilder;
.super Ljava/lang/Object;
.source "SuggestionsProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/appdatasearch/util/SuggestionsProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CursorBuilder"
.end annotation


# static fields
.field private static final ICING_SECTIONS:[Ljava/lang/String;

.field private static final SEARCHABLE_COLUMNS:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 175
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "suggest_text_1"

    aput-object v1, v0, v3

    const-string v1, "suggest_text_2"

    aput-object v1, v0, v4

    const-string v1, "suggest_icon_1"

    aput-object v1, v0, v5

    const-string v1, "suggest_intent_action"

    aput-object v1, v0, v6

    const-string v1, "suggest_intent_data"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "suggest_intent_data_id"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "suggest_intent_extra_data"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "suggest_shortcut_id"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/appdatasearch/util/SuggestionsProvider$CursorBuilder;->SEARCHABLE_COLUMNS:[Ljava/lang/String;

    .line 182
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "text1"

    aput-object v1, v0, v3

    const-string v1, "text2"

    aput-object v1, v0, v4

    const-string v1, "icon"

    aput-object v1, v0, v5

    const-string v1, "intent_action"

    aput-object v1, v0, v6

    const-string v1, "intent_data"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "intent_data_id"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "intent_extra_data"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/appdatasearch/util/SuggestionsProvider$CursorBuilder;->ICING_SECTIONS:[Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/gms/appdatasearch/SearchResults;)Landroid/database/Cursor;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/gms/appdatasearch/SearchResults;

    .prologue
    .line 173
    invoke-static {p0}, Lcom/google/android/gms/appdatasearch/util/SuggestionsProvider$CursorBuilder;->createSuggestionsCursor(Lcom/google/android/gms/appdatasearch/SearchResults;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method private static createSuggestionsCursor(Lcom/google/android/gms/appdatasearch/SearchResults;)Landroid/database/Cursor;
    .locals 10
    .param p0, "suggestions"    # Lcom/google/android/gms/appdatasearch/SearchResults;

    .prologue
    .line 190
    if-nez p0, :cond_1

    new-instance v1, Landroid/database/MatrixCursor;

    sget-object v8, Lcom/google/android/gms/appdatasearch/util/SuggestionsProvider$CursorBuilder;->SEARCHABLE_COLUMNS:[Ljava/lang/String;

    const/4 v9, 0x0

    invoke-direct {v1, v8, v9}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;I)V

    .line 201
    :cond_0
    return-object v1

    .line 191
    :cond_1
    new-instance v1, Landroid/database/MatrixCursor;

    sget-object v8, Lcom/google/android/gms/appdatasearch/util/SuggestionsProvider$CursorBuilder;->SEARCHABLE_COLUMNS:[Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/gms/appdatasearch/SearchResults;->getNumResults()I

    move-result v9

    invoke-direct {v1, v8, v9}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;I)V

    .line 192
    .local v1, "cursor":Landroid/database/MatrixCursor;
    new-instance v5, Ljava/util/ArrayList;

    sget-object v8, Lcom/google/android/gms/appdatasearch/util/SuggestionsProvider$CursorBuilder;->SEARCHABLE_COLUMNS:[Ljava/lang/String;

    array-length v8, v8

    invoke-direct {v5, v8}, Ljava/util/ArrayList;-><init>(I)V

    .line 193
    .local v5, "row":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p0}, Lcom/google/android/gms/appdatasearch/SearchResults;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/gms/appdatasearch/SearchResults$Result;

    .line 194
    .local v7, "suggestion":Lcom/google/android/gms/appdatasearch/SearchResults$Result;
    invoke-interface {v5}, Ljava/util/List;->clear()V

    .line 195
    sget-object v0, Lcom/google/android/gms/appdatasearch/util/SuggestionsProvider$CursorBuilder;->ICING_SECTIONS:[Ljava/lang/String;

    .local v0, "arr$":[Ljava/lang/String;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_1
    if-ge v3, v4, :cond_2

    aget-object v6, v0, v3

    .line 196
    .local v6, "sectionName":Ljava/lang/String;
    invoke-virtual {v7, v6}, Lcom/google/android/gms/appdatasearch/SearchResults$Result;->getSection(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v5, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 195
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 198
    .end local v6    # "sectionName":Ljava/lang/String;
    :cond_2
    const-string v8, "SUGGEST_NEVER_MAKE_SHORTCUT"

    invoke-interface {v5, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 199
    invoke-virtual {v1, v5}, Landroid/database/MatrixCursor;->addRow(Ljava/lang/Iterable;)V

    goto :goto_0
.end method
