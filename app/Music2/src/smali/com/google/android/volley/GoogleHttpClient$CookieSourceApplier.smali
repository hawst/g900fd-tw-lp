.class final Lcom/google/android/volley/GoogleHttpClient$CookieSourceApplier;
.super Ljava/lang/Object;
.source "GoogleHttpClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/volley/GoogleHttpClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "CookieSourceApplier"
.end annotation


# instance fields
.field private final mClient:Lcom/google/android/volley/AndroidHttpClient;

.field private final mCookieSource:Lcom/google/android/pseudonymous_http/PseudonymousCookieSource;

.field final synthetic this$0:Lcom/google/android/volley/GoogleHttpClient;


# direct methods
.method private constructor <init>(Lcom/google/android/volley/GoogleHttpClient;Lcom/google/android/volley/AndroidHttpClient;Lcom/google/android/pseudonymous_http/PseudonymousCookieSource;)V
    .locals 0
    .param p2, "client"    # Lcom/google/android/volley/AndroidHttpClient;
    .param p3, "cookieSource"    # Lcom/google/android/pseudonymous_http/PseudonymousCookieSource;

    .prologue
    .line 412
    iput-object p1, p0, Lcom/google/android/volley/GoogleHttpClient$CookieSourceApplier;->this$0:Lcom/google/android/volley/GoogleHttpClient;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 413
    iput-object p2, p0, Lcom/google/android/volley/GoogleHttpClient$CookieSourceApplier;->mClient:Lcom/google/android/volley/AndroidHttpClient;

    .line 414
    iput-object p3, p0, Lcom/google/android/volley/GoogleHttpClient$CookieSourceApplier;->mCookieSource:Lcom/google/android/pseudonymous_http/PseudonymousCookieSource;

    .line 415
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/volley/GoogleHttpClient;Lcom/google/android/volley/AndroidHttpClient;Lcom/google/android/pseudonymous_http/PseudonymousCookieSource;Lcom/google/android/volley/GoogleHttpClient$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/volley/GoogleHttpClient;
    .param p2, "x1"    # Lcom/google/android/volley/AndroidHttpClient;
    .param p3, "x2"    # Lcom/google/android/pseudonymous_http/PseudonymousCookieSource;
    .param p4, "x3"    # Lcom/google/android/volley/GoogleHttpClient$1;

    .prologue
    .line 407
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/volley/GoogleHttpClient$CookieSourceApplier;-><init>(Lcom/google/android/volley/GoogleHttpClient;Lcom/google/android/volley/AndroidHttpClient;Lcom/google/android/pseudonymous_http/PseudonymousCookieSource;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/volley/GoogleHttpClient$CookieSourceApplier;Lorg/apache/http/HttpHost;Lorg/apache/http/HttpRequest;Lorg/apache/http/client/ResponseHandler;Lorg/apache/http/protocol/HttpContext;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/volley/GoogleHttpClient$CookieSourceApplier;
    .param p1, "x1"    # Lorg/apache/http/HttpHost;
    .param p2, "x2"    # Lorg/apache/http/HttpRequest;
    .param p3, "x3"    # Lorg/apache/http/client/ResponseHandler;
    .param p4, "x4"    # Lorg/apache/http/protocol/HttpContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/apache/http/client/ClientProtocolException;
        }
    .end annotation

    .prologue
    .line 407
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/volley/GoogleHttpClient$CookieSourceApplier;->execute(Lorg/apache/http/HttpHost;Lorg/apache/http/HttpRequest;Lorg/apache/http/client/ResponseHandler;Lorg/apache/http/protocol/HttpContext;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/volley/GoogleHttpClient$CookieSourceApplier;Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/protocol/HttpContext;)Lorg/apache/http/HttpResponse;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/volley/GoogleHttpClient$CookieSourceApplier;
    .param p1, "x1"    # Lorg/apache/http/client/methods/HttpUriRequest;
    .param p2, "x2"    # Lorg/apache/http/protocol/HttpContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 407
    invoke-direct {p0, p1, p2}, Lcom/google/android/volley/GoogleHttpClient$CookieSourceApplier;->execute(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/protocol/HttpContext;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/volley/GoogleHttpClient$CookieSourceApplier;Lorg/apache/http/HttpHost;Lorg/apache/http/HttpRequest;)Lorg/apache/http/HttpResponse;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/volley/GoogleHttpClient$CookieSourceApplier;
    .param p1, "x1"    # Lorg/apache/http/HttpHost;
    .param p2, "x2"    # Lorg/apache/http/HttpRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 407
    invoke-direct {p0, p1, p2}, Lcom/google/android/volley/GoogleHttpClient$CookieSourceApplier;->execute(Lorg/apache/http/HttpHost;Lorg/apache/http/HttpRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/volley/GoogleHttpClient$CookieSourceApplier;Lorg/apache/http/HttpHost;Lorg/apache/http/HttpRequest;Lorg/apache/http/protocol/HttpContext;)Lorg/apache/http/HttpResponse;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/volley/GoogleHttpClient$CookieSourceApplier;
    .param p1, "x1"    # Lorg/apache/http/HttpHost;
    .param p2, "x2"    # Lorg/apache/http/HttpRequest;
    .param p3, "x3"    # Lorg/apache/http/protocol/HttpContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 407
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/volley/GoogleHttpClient$CookieSourceApplier;->execute(Lorg/apache/http/HttpHost;Lorg/apache/http/HttpRequest;Lorg/apache/http/protocol/HttpContext;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/volley/GoogleHttpClient$CookieSourceApplier;Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/client/ResponseHandler;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/volley/GoogleHttpClient$CookieSourceApplier;
    .param p1, "x1"    # Lorg/apache/http/client/methods/HttpUriRequest;
    .param p2, "x2"    # Lorg/apache/http/client/ResponseHandler;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 407
    invoke-direct {p0, p1, p2}, Lcom/google/android/volley/GoogleHttpClient$CookieSourceApplier;->execute(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/client/ResponseHandler;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/volley/GoogleHttpClient$CookieSourceApplier;Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/client/ResponseHandler;Lorg/apache/http/protocol/HttpContext;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/volley/GoogleHttpClient$CookieSourceApplier;
    .param p1, "x1"    # Lorg/apache/http/client/methods/HttpUriRequest;
    .param p2, "x2"    # Lorg/apache/http/client/ResponseHandler;
    .param p3, "x3"    # Lorg/apache/http/protocol/HttpContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/apache/http/client/ClientProtocolException;
        }
    .end annotation

    .prologue
    .line 407
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/volley/GoogleHttpClient$CookieSourceApplier;->execute(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/client/ResponseHandler;Lorg/apache/http/protocol/HttpContext;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/volley/GoogleHttpClient$CookieSourceApplier;Lorg/apache/http/HttpHost;Lorg/apache/http/HttpRequest;Lorg/apache/http/client/ResponseHandler;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/volley/GoogleHttpClient$CookieSourceApplier;
    .param p1, "x1"    # Lorg/apache/http/HttpHost;
    .param p2, "x2"    # Lorg/apache/http/HttpRequest;
    .param p3, "x3"    # Lorg/apache/http/client/ResponseHandler;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/apache/http/client/ClientProtocolException;
        }
    .end annotation

    .prologue
    .line 407
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/volley/GoogleHttpClient$CookieSourceApplier;->execute(Lorg/apache/http/HttpHost;Lorg/apache/http/HttpRequest;Lorg/apache/http/client/ResponseHandler;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private execute(Lorg/apache/http/HttpHost;Lorg/apache/http/HttpRequest;Lorg/apache/http/client/ResponseHandler;)Ljava/lang/Object;
    .locals 6
    .param p1, "target"    # Lorg/apache/http/HttpHost;
    .param p2, "request"    # Lorg/apache/http/HttpRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lorg/apache/http/HttpHost;",
            "Lorg/apache/http/HttpRequest;",
            "Lorg/apache/http/client/ResponseHandler",
            "<TT;>;)TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/apache/http/client/ClientProtocolException;
        }
    .end annotation

    .prologue
    .line 476
    .local p3, "responseHandler":Lorg/apache/http/client/ResponseHandler;, "Lorg/apache/http/client/ResponseHandler<TT;>;"
    # invokes: Lcom/google/android/volley/GoogleHttpClient;->wrapRequest(Lorg/apache/http/HttpRequest;)Lorg/apache/http/impl/client/RequestWrapper;
    invoke-static {p2}, Lcom/google/android/volley/GoogleHttpClient;->access$1100(Lorg/apache/http/HttpRequest;)Lorg/apache/http/impl/client/RequestWrapper;

    move-result-object v3

    .line 477
    .local v3, "wrappedRequest":Lorg/apache/http/client/methods/HttpUriRequest;
    new-instance v0, Lcom/google/android/volley/GoogleHttpClient$SetCookie;

    iget-object v1, p0, Lcom/google/android/volley/GoogleHttpClient$CookieSourceApplier;->this$0:Lcom/google/android/volley/GoogleHttpClient;

    iget-object v4, p0, Lcom/google/android/volley/GoogleHttpClient$CookieSourceApplier;->mCookieSource:Lcom/google/android/pseudonymous_http/PseudonymousCookieSource;

    const/4 v5, 0x0

    move-object v2, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/volley/GoogleHttpClient$SetCookie;-><init>(Lcom/google/android/volley/GoogleHttpClient;Lorg/apache/http/client/ResponseHandler;Lorg/apache/http/client/methods/HttpUriRequest;Lcom/google/android/pseudonymous_http/PseudonymousCookieSource;Lcom/google/android/volley/GoogleHttpClient$1;)V

    .line 479
    .local v0, "setCookie":Lorg/apache/http/client/ResponseHandler;, "Lorg/apache/http/client/ResponseHandler<TT;>;"
    iget-object v1, p0, Lcom/google/android/volley/GoogleHttpClient$CookieSourceApplier;->mClient:Lcom/google/android/volley/AndroidHttpClient;

    iget-object v2, p0, Lcom/google/android/volley/GoogleHttpClient$CookieSourceApplier;->mCookieSource:Lcom/google/android/pseudonymous_http/PseudonymousCookieSource;

    invoke-static {v3, v2}, Lcom/google/android/pseudonymous_http/PseudonymousCookieSource$Helper;->setRequestCookie(Lorg/apache/http/client/methods/HttpUriRequest;Lcom/google/android/pseudonymous_http/PseudonymousCookieSource;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v2

    invoke-virtual {v1, p1, v2, v0}, Lcom/google/android/volley/AndroidHttpClient;->execute(Lorg/apache/http/HttpHost;Lorg/apache/http/HttpRequest;Lorg/apache/http/client/ResponseHandler;)Ljava/lang/Object;

    move-result-object v1

    return-object v1
.end method

.method private execute(Lorg/apache/http/HttpHost;Lorg/apache/http/HttpRequest;Lorg/apache/http/client/ResponseHandler;Lorg/apache/http/protocol/HttpContext;)Ljava/lang/Object;
    .locals 6
    .param p1, "target"    # Lorg/apache/http/HttpHost;
    .param p2, "request"    # Lorg/apache/http/HttpRequest;
    .param p4, "context"    # Lorg/apache/http/protocol/HttpContext;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lorg/apache/http/HttpHost;",
            "Lorg/apache/http/HttpRequest;",
            "Lorg/apache/http/client/ResponseHandler",
            "<TT;>;",
            "Lorg/apache/http/protocol/HttpContext;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/apache/http/client/ClientProtocolException;
        }
    .end annotation

    .prologue
    .line 487
    .local p3, "responseHandler":Lorg/apache/http/client/ResponseHandler;, "Lorg/apache/http/client/ResponseHandler<TT;>;"
    # invokes: Lcom/google/android/volley/GoogleHttpClient;->wrapRequest(Lorg/apache/http/HttpRequest;)Lorg/apache/http/impl/client/RequestWrapper;
    invoke-static {p2}, Lcom/google/android/volley/GoogleHttpClient;->access$1100(Lorg/apache/http/HttpRequest;)Lorg/apache/http/impl/client/RequestWrapper;

    move-result-object v3

    .line 488
    .local v3, "wrappedRequest":Lorg/apache/http/client/methods/HttpUriRequest;
    new-instance v0, Lcom/google/android/volley/GoogleHttpClient$SetCookie;

    iget-object v1, p0, Lcom/google/android/volley/GoogleHttpClient$CookieSourceApplier;->this$0:Lcom/google/android/volley/GoogleHttpClient;

    iget-object v4, p0, Lcom/google/android/volley/GoogleHttpClient$CookieSourceApplier;->mCookieSource:Lcom/google/android/pseudonymous_http/PseudonymousCookieSource;

    const/4 v5, 0x0

    move-object v2, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/volley/GoogleHttpClient$SetCookie;-><init>(Lcom/google/android/volley/GoogleHttpClient;Lorg/apache/http/client/ResponseHandler;Lorg/apache/http/client/methods/HttpUriRequest;Lcom/google/android/pseudonymous_http/PseudonymousCookieSource;Lcom/google/android/volley/GoogleHttpClient$1;)V

    .line 490
    .local v0, "setCookie":Lorg/apache/http/client/ResponseHandler;, "Lorg/apache/http/client/ResponseHandler<TT;>;"
    iget-object v1, p0, Lcom/google/android/volley/GoogleHttpClient$CookieSourceApplier;->mClient:Lcom/google/android/volley/AndroidHttpClient;

    iget-object v2, p0, Lcom/google/android/volley/GoogleHttpClient$CookieSourceApplier;->mCookieSource:Lcom/google/android/pseudonymous_http/PseudonymousCookieSource;

    invoke-static {v3, v2}, Lcom/google/android/pseudonymous_http/PseudonymousCookieSource$Helper;->setRequestCookie(Lorg/apache/http/client/methods/HttpUriRequest;Lcom/google/android/pseudonymous_http/PseudonymousCookieSource;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v2

    invoke-virtual {v1, p1, v2, v0, p4}, Lcom/google/android/volley/AndroidHttpClient;->execute(Lorg/apache/http/HttpHost;Lorg/apache/http/HttpRequest;Lorg/apache/http/client/ResponseHandler;Lorg/apache/http/protocol/HttpContext;)Ljava/lang/Object;

    move-result-object v1

    return-object v1
.end method

.method private execute(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/client/ResponseHandler;)Ljava/lang/Object;
    .locals 6
    .param p1, "request"    # Lorg/apache/http/client/methods/HttpUriRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lorg/apache/http/client/methods/HttpUriRequest;",
            "Lorg/apache/http/client/ResponseHandler",
            "<TT;>;)TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 458
    .local p2, "responseHandler":Lorg/apache/http/client/ResponseHandler;, "Lorg/apache/http/client/ResponseHandler<TT;>;"
    new-instance v0, Lcom/google/android/volley/GoogleHttpClient$SetCookie;

    iget-object v1, p0, Lcom/google/android/volley/GoogleHttpClient$CookieSourceApplier;->this$0:Lcom/google/android/volley/GoogleHttpClient;

    iget-object v4, p0, Lcom/google/android/volley/GoogleHttpClient$CookieSourceApplier;->mCookieSource:Lcom/google/android/pseudonymous_http/PseudonymousCookieSource;

    const/4 v5, 0x0

    move-object v2, p2

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/volley/GoogleHttpClient$SetCookie;-><init>(Lcom/google/android/volley/GoogleHttpClient;Lorg/apache/http/client/ResponseHandler;Lorg/apache/http/client/methods/HttpUriRequest;Lcom/google/android/pseudonymous_http/PseudonymousCookieSource;Lcom/google/android/volley/GoogleHttpClient$1;)V

    .line 459
    .local v0, "setCookie":Lorg/apache/http/client/ResponseHandler;, "Lorg/apache/http/client/ResponseHandler<TT;>;"
    iget-object v1, p0, Lcom/google/android/volley/GoogleHttpClient$CookieSourceApplier;->mClient:Lcom/google/android/volley/AndroidHttpClient;

    iget-object v2, p0, Lcom/google/android/volley/GoogleHttpClient$CookieSourceApplier;->mCookieSource:Lcom/google/android/pseudonymous_http/PseudonymousCookieSource;

    invoke-static {p1, v2}, Lcom/google/android/pseudonymous_http/PseudonymousCookieSource$Helper;->setRequestCookie(Lorg/apache/http/client/methods/HttpUriRequest;Lcom/google/android/pseudonymous_http/PseudonymousCookieSource;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/google/android/volley/AndroidHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/client/ResponseHandler;)Ljava/lang/Object;

    move-result-object v1

    return-object v1
.end method

.method private execute(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/client/ResponseHandler;Lorg/apache/http/protocol/HttpContext;)Ljava/lang/Object;
    .locals 6
    .param p1, "request"    # Lorg/apache/http/client/methods/HttpUriRequest;
    .param p3, "context"    # Lorg/apache/http/protocol/HttpContext;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lorg/apache/http/client/methods/HttpUriRequest;",
            "Lorg/apache/http/client/ResponseHandler",
            "<TT;>;",
            "Lorg/apache/http/protocol/HttpContext;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/apache/http/client/ClientProtocolException;
        }
    .end annotation

    .prologue
    .line 468
    .local p2, "responseHandler":Lorg/apache/http/client/ResponseHandler;, "Lorg/apache/http/client/ResponseHandler<TT;>;"
    new-instance v0, Lcom/google/android/volley/GoogleHttpClient$SetCookie;

    iget-object v1, p0, Lcom/google/android/volley/GoogleHttpClient$CookieSourceApplier;->this$0:Lcom/google/android/volley/GoogleHttpClient;

    iget-object v4, p0, Lcom/google/android/volley/GoogleHttpClient$CookieSourceApplier;->mCookieSource:Lcom/google/android/pseudonymous_http/PseudonymousCookieSource;

    const/4 v5, 0x0

    move-object v2, p2

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/volley/GoogleHttpClient$SetCookie;-><init>(Lcom/google/android/volley/GoogleHttpClient;Lorg/apache/http/client/ResponseHandler;Lorg/apache/http/client/methods/HttpUriRequest;Lcom/google/android/pseudonymous_http/PseudonymousCookieSource;Lcom/google/android/volley/GoogleHttpClient$1;)V

    .line 469
    .local v0, "setCookie":Lorg/apache/http/client/ResponseHandler;, "Lorg/apache/http/client/ResponseHandler<TT;>;"
    iget-object v1, p0, Lcom/google/android/volley/GoogleHttpClient$CookieSourceApplier;->mClient:Lcom/google/android/volley/AndroidHttpClient;

    invoke-virtual {v1, p1, v0, p3}, Lcom/google/android/volley/AndroidHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/client/ResponseHandler;Lorg/apache/http/protocol/HttpContext;)Ljava/lang/Object;

    move-result-object v1

    return-object v1
.end method

.method private execute(Lorg/apache/http/HttpHost;Lorg/apache/http/HttpRequest;)Lorg/apache/http/HttpResponse;
    .locals 5
    .param p1, "target"    # Lorg/apache/http/HttpHost;
    .param p2, "request"    # Lorg/apache/http/HttpRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 437
    # invokes: Lcom/google/android/volley/GoogleHttpClient;->wrapRequest(Lorg/apache/http/HttpRequest;)Lorg/apache/http/impl/client/RequestWrapper;
    invoke-static {p2}, Lcom/google/android/volley/GoogleHttpClient;->access$1100(Lorg/apache/http/HttpRequest;)Lorg/apache/http/impl/client/RequestWrapper;

    move-result-object v1

    .line 438
    .local v1, "wrappedRequest":Lorg/apache/http/client/methods/HttpUriRequest;
    iget-object v2, p0, Lcom/google/android/volley/GoogleHttpClient$CookieSourceApplier;->mClient:Lcom/google/android/volley/AndroidHttpClient;

    # invokes: Lcom/google/android/volley/GoogleHttpClient;->wrapRequest(Lorg/apache/http/HttpRequest;)Lorg/apache/http/impl/client/RequestWrapper;
    invoke-static {p2}, Lcom/google/android/volley/GoogleHttpClient;->access$1100(Lorg/apache/http/HttpRequest;)Lorg/apache/http/impl/client/RequestWrapper;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/volley/GoogleHttpClient$CookieSourceApplier;->mCookieSource:Lcom/google/android/pseudonymous_http/PseudonymousCookieSource;

    invoke-static {v3, v4}, Lcom/google/android/pseudonymous_http/PseudonymousCookieSource$Helper;->setRequestCookie(Lorg/apache/http/client/methods/HttpUriRequest;Lcom/google/android/pseudonymous_http/PseudonymousCookieSource;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v3

    invoke-virtual {v2, p1, v3}, Lcom/google/android/volley/AndroidHttpClient;->execute(Lorg/apache/http/HttpHost;Lorg/apache/http/HttpRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 441
    .local v0, "response":Lorg/apache/http/HttpResponse;
    iget-object v2, p0, Lcom/google/android/volley/GoogleHttpClient$CookieSourceApplier;->mCookieSource:Lcom/google/android/pseudonymous_http/PseudonymousCookieSource;

    invoke-static {v1, v0, v2}, Lcom/google/android/pseudonymous_http/PseudonymousCookieSource$Helper;->updateFromResponseCookie(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/HttpResponse;Lcom/google/android/pseudonymous_http/PseudonymousCookieSource;)Lorg/apache/http/HttpResponse;

    move-result-object v2

    return-object v2
.end method

.method private execute(Lorg/apache/http/HttpHost;Lorg/apache/http/HttpRequest;Lorg/apache/http/protocol/HttpContext;)Lorg/apache/http/HttpResponse;
    .locals 5
    .param p1, "target"    # Lorg/apache/http/HttpHost;
    .param p2, "request"    # Lorg/apache/http/HttpRequest;
    .param p3, "context"    # Lorg/apache/http/protocol/HttpContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 447
    # invokes: Lcom/google/android/volley/GoogleHttpClient;->wrapRequest(Lorg/apache/http/HttpRequest;)Lorg/apache/http/impl/client/RequestWrapper;
    invoke-static {p2}, Lcom/google/android/volley/GoogleHttpClient;->access$1100(Lorg/apache/http/HttpRequest;)Lorg/apache/http/impl/client/RequestWrapper;

    move-result-object v1

    .line 448
    .local v1, "wrappedRequest":Lorg/apache/http/client/methods/HttpUriRequest;
    iget-object v2, p0, Lcom/google/android/volley/GoogleHttpClient$CookieSourceApplier;->mClient:Lcom/google/android/volley/AndroidHttpClient;

    # invokes: Lcom/google/android/volley/GoogleHttpClient;->wrapRequest(Lorg/apache/http/HttpRequest;)Lorg/apache/http/impl/client/RequestWrapper;
    invoke-static {p2}, Lcom/google/android/volley/GoogleHttpClient;->access$1100(Lorg/apache/http/HttpRequest;)Lorg/apache/http/impl/client/RequestWrapper;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/volley/GoogleHttpClient$CookieSourceApplier;->mCookieSource:Lcom/google/android/pseudonymous_http/PseudonymousCookieSource;

    invoke-static {v3, v4}, Lcom/google/android/pseudonymous_http/PseudonymousCookieSource$Helper;->setRequestCookie(Lorg/apache/http/client/methods/HttpUriRequest;Lcom/google/android/pseudonymous_http/PseudonymousCookieSource;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v3

    invoke-virtual {v2, p1, v3, p3}, Lcom/google/android/volley/AndroidHttpClient;->execute(Lorg/apache/http/HttpHost;Lorg/apache/http/HttpRequest;Lorg/apache/http/protocol/HttpContext;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 451
    .local v0, "response":Lorg/apache/http/HttpResponse;
    iget-object v2, p0, Lcom/google/android/volley/GoogleHttpClient$CookieSourceApplier;->mCookieSource:Lcom/google/android/pseudonymous_http/PseudonymousCookieSource;

    invoke-static {v1, v0, v2}, Lcom/google/android/pseudonymous_http/PseudonymousCookieSource$Helper;->updateFromResponseCookie(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/HttpResponse;Lcom/google/android/pseudonymous_http/PseudonymousCookieSource;)Lorg/apache/http/HttpResponse;

    move-result-object v2

    return-object v2
.end method

.method private execute(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/protocol/HttpContext;)Lorg/apache/http/HttpResponse;
    .locals 3
    .param p1, "request"    # Lorg/apache/http/client/methods/HttpUriRequest;
    .param p2, "context"    # Lorg/apache/http/protocol/HttpContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 423
    iget-object v1, p0, Lcom/google/android/volley/GoogleHttpClient$CookieSourceApplier;->mClient:Lcom/google/android/volley/AndroidHttpClient;

    iget-object v2, p0, Lcom/google/android/volley/GoogleHttpClient$CookieSourceApplier;->mCookieSource:Lcom/google/android/pseudonymous_http/PseudonymousCookieSource;

    invoke-static {p1, v2}, Lcom/google/android/pseudonymous_http/PseudonymousCookieSource$Helper;->setRequestCookie(Lorg/apache/http/client/methods/HttpUriRequest;Lcom/google/android/pseudonymous_http/PseudonymousCookieSource;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/volley/AndroidHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 426
    .local v0, "response":Lorg/apache/http/HttpResponse;
    iget-object v1, p0, Lcom/google/android/volley/GoogleHttpClient$CookieSourceApplier;->mCookieSource:Lcom/google/android/pseudonymous_http/PseudonymousCookieSource;

    invoke-static {p1, v0, v1}, Lcom/google/android/pseudonymous_http/PseudonymousCookieSource$Helper;->updateFromResponseCookie(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/HttpResponse;Lcom/google/android/pseudonymous_http/PseudonymousCookieSource;)Lorg/apache/http/HttpResponse;

    move-result-object v1

    return-object v1
.end method
