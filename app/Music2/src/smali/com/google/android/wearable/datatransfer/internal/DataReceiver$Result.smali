.class final Lcom/google/android/wearable/datatransfer/internal/DataReceiver$Result;
.super Ljava/lang/Object;
.source "DataReceiver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/wearable/datatransfer/internal/DataReceiver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "Result"
.end annotation


# instance fields
.field public final appErrorCode:I

.field public final connectionRefused:Z

.field public final serverStatusCode:I

.field public final success:Z

.field public final timeout:Z


# direct methods
.method constructor <init>(ZZZII)V
    .locals 0
    .param p1, "success"    # Z
    .param p2, "connectionRefused"    # Z
    .param p3, "timeout"    # Z
    .param p4, "serverStatusCode"    # I
    .param p5, "appErrorCode"    # I

    .prologue
    .line 385
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 386
    iput-boolean p1, p0, Lcom/google/android/wearable/datatransfer/internal/DataReceiver$Result;->success:Z

    .line 387
    iput-boolean p2, p0, Lcom/google/android/wearable/datatransfer/internal/DataReceiver$Result;->connectionRefused:Z

    .line 388
    iput-boolean p3, p0, Lcom/google/android/wearable/datatransfer/internal/DataReceiver$Result;->timeout:Z

    .line 389
    iput p4, p0, Lcom/google/android/wearable/datatransfer/internal/DataReceiver$Result;->serverStatusCode:I

    .line 390
    iput p5, p0, Lcom/google/android/wearable/datatransfer/internal/DataReceiver$Result;->appErrorCode:I

    .line 391
    return-void
.end method

.method static forConnectionRefused(II)Lcom/google/android/wearable/datatransfer/internal/DataReceiver$Result;
    .locals 6
    .param p0, "serverStatusCode"    # I
    .param p1, "appErrorCode"    # I

    .prologue
    const/4 v1, 0x0

    .line 402
    new-instance v0, Lcom/google/android/wearable/datatransfer/internal/DataReceiver$Result;

    const/4 v2, 0x1

    move v3, v1

    move v4, p0

    move v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/wearable/datatransfer/internal/DataReceiver$Result;-><init>(ZZZII)V

    return-object v0
.end method

.method static forError()Lcom/google/android/wearable/datatransfer/internal/DataReceiver$Result;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 394
    new-instance v0, Lcom/google/android/wearable/datatransfer/internal/DataReceiver$Result;

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/wearable/datatransfer/internal/DataReceiver$Result;-><init>(ZZZII)V

    return-object v0
.end method

.method static forSuccess()Lcom/google/android/wearable/datatransfer/internal/DataReceiver$Result;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 406
    new-instance v0, Lcom/google/android/wearable/datatransfer/internal/DataReceiver$Result;

    const/4 v1, 0x1

    move v3, v2

    move v4, v2

    move v5, v2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/wearable/datatransfer/internal/DataReceiver$Result;-><init>(ZZZII)V

    return-object v0
.end method

.method static forTimeout()Lcom/google/android/wearable/datatransfer/internal/DataReceiver$Result;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 398
    new-instance v0, Lcom/google/android/wearable/datatransfer/internal/DataReceiver$Result;

    const/4 v3, 0x1

    move v2, v1

    move v4, v1

    move v5, v1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/wearable/datatransfer/internal/DataReceiver$Result;-><init>(ZZZII)V

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 411
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Result{success="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/wearable/datatransfer/internal/DataReceiver$Result;->success:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", connectionRefused="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/wearable/datatransfer/internal/DataReceiver$Result;->connectionRefused:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", timeout="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/wearable/datatransfer/internal/DataReceiver$Result;->timeout:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", serverStatusCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/wearable/datatransfer/internal/DataReceiver$Result;->serverStatusCode:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", appErrorCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/wearable/datatransfer/internal/DataReceiver$Result;->appErrorCode:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
