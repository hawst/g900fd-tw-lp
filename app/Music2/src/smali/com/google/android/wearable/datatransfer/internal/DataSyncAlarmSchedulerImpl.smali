.class final Lcom/google/android/wearable/datatransfer/internal/DataSyncAlarmSchedulerImpl;
.super Ljava/lang/Object;
.source "DataSyncAlarmSchedulerImpl.java"

# interfaces
.implements Lcom/google/android/wearable/datatransfer/internal/DataSyncAlarmScheduler;


# instance fields
.field private final mContext:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const-string v0, "context"

    invoke-static {p1, v0}, Lcom/google/android/wearable/datatransfer/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncAlarmSchedulerImpl;->mContext:Landroid/content/Context;

    .line 23
    return-void
.end method


# virtual methods
.method public removeAlarm()V
    .locals 6

    .prologue
    .line 37
    iget-object v3, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncAlarmSchedulerImpl;->mContext:Landroid/content/Context;

    const-string v4, "alarm"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 38
    .local v0, "alarmManager":Landroid/app/AlarmManager;
    iget-object v3, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncAlarmSchedulerImpl;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->getStartIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    .line 39
    .local v1, "intent":Landroid/content/Intent;
    iget-object v3, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncAlarmSchedulerImpl;->mContext:Landroid/content/Context;

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-static {v3, v4, v1, v5}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 40
    .local v2, "pendingIntent":Landroid/app/PendingIntent;
    invoke-virtual {v0, v2}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 41
    return-void
.end method

.method public setAlarm(J)V
    .locals 7
    .param p1, "triggerAtMillis"    # J

    .prologue
    const/4 v5, 0x0

    .line 27
    iget-object v3, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncAlarmSchedulerImpl;->mContext:Landroid/content/Context;

    const-string v4, "alarm"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 28
    .local v0, "alarmManager":Landroid/app/AlarmManager;
    iget-object v3, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncAlarmSchedulerImpl;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->getStartIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    .line 29
    .local v1, "intent":Landroid/content/Intent;
    iget-object v3, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncAlarmSchedulerImpl;->mContext:Landroid/content/Context;

    const/4 v4, 0x1

    invoke-static {v3, v4, v1, v5}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 32
    .local v2, "pendingIntent":Landroid/app/PendingIntent;
    invoke-virtual {v0, v5, p1, p2, v2}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 33
    return-void
.end method
