.class final Lcom/google/android/wearable/datatransfer/WearableDataApiClient$Event;
.super Ljava/lang/Object;
.source "WearableDataApiClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/wearable/datatransfer/WearableDataApiClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "Event"
.end annotation


# instance fields
.field final messageEvent:Lcom/google/android/gms/wearable/MessageEvent;

.field final openConnectionRequest:Lcom/google/android/wearable/datatransfer/WearableDataApiClient$OpenConnectionRequest;

.field final stopAllServerConnectionsEvent:Lcom/google/android/wearable/datatransfer/WearableDataApiClient$StopAllServerConnectionsEvent;


# direct methods
.method constructor <init>(Lcom/google/android/gms/wearable/MessageEvent;)V
    .locals 1
    .param p1, "messageEvent"    # Lcom/google/android/gms/wearable/MessageEvent;

    .prologue
    const/4 v0, 0x0

    .line 375
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 376
    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/WearableDataApiClient$Event;->openConnectionRequest:Lcom/google/android/wearable/datatransfer/WearableDataApiClient$OpenConnectionRequest;

    .line 377
    iput-object p1, p0, Lcom/google/android/wearable/datatransfer/WearableDataApiClient$Event;->messageEvent:Lcom/google/android/gms/wearable/MessageEvent;

    .line 378
    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/WearableDataApiClient$Event;->stopAllServerConnectionsEvent:Lcom/google/android/wearable/datatransfer/WearableDataApiClient$StopAllServerConnectionsEvent;

    .line 379
    return-void
.end method

.method constructor <init>(Lcom/google/android/wearable/datatransfer/WearableDataApiClient$OpenConnectionRequest;)V
    .locals 1
    .param p1, "openConnectionRequest"    # Lcom/google/android/wearable/datatransfer/WearableDataApiClient$OpenConnectionRequest;

    .prologue
    const/4 v0, 0x0

    .line 369
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 370
    iput-object p1, p0, Lcom/google/android/wearable/datatransfer/WearableDataApiClient$Event;->openConnectionRequest:Lcom/google/android/wearable/datatransfer/WearableDataApiClient$OpenConnectionRequest;

    .line 371
    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/WearableDataApiClient$Event;->messageEvent:Lcom/google/android/gms/wearable/MessageEvent;

    .line 372
    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/WearableDataApiClient$Event;->stopAllServerConnectionsEvent:Lcom/google/android/wearable/datatransfer/WearableDataApiClient$StopAllServerConnectionsEvent;

    .line 373
    return-void
.end method
