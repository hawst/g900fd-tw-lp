.class Lcom/google/android/wearable/datatransfer/internal/DataTransferServer$MyDataSenderCallbacks;
.super Ljava/lang/Object;
.source "DataTransferServer.java"

# interfaces
.implements Lcom/google/android/wearable/datatransfer/internal/DataSender$Callbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyDataSenderCallbacks"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;


# direct methods
.method private constructor <init>(Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;)V
    .locals 0

    .prologue
    .line 235
    iput-object p1, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferServer$MyDataSenderCallbacks;->this$0:Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;Lcom/google/android/wearable/datatransfer/internal/DataTransferServer$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;
    .param p2, "x1"    # Lcom/google/android/wearable/datatransfer/internal/DataTransferServer$1;

    .prologue
    .line 235
    invoke-direct {p0, p1}, Lcom/google/android/wearable/datatransfer/internal/DataTransferServer$MyDataSenderCallbacks;-><init>(Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;)V

    return-void
.end method


# virtual methods
.method public onConnectionTerminated(J)V
    .locals 3
    .param p1, "connectionId"    # J

    .prologue
    .line 252
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferServer$MyDataSenderCallbacks;->this$0:Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;

    # getter for: Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;->mShutdown:Z
    invoke-static {v0}, Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;->access$100(Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 253
    const-string v0, "DataTransferServer"

    invoke-static {v0}, Lcom/google/android/wearable/datatransfer/internal/DebugLog;->isLoggable(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 254
    const-string v0, "DataTransferServer"

    const-string v1, "already shutdown"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 264
    :cond_0
    :goto_0
    return-void

    .line 258
    :cond_1
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferServer$MyDataSenderCallbacks;->this$0:Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;

    # getter for: Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;->mLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;->access$200(Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 259
    :try_start_0
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferServer$MyDataSenderCallbacks;->this$0:Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;

    # getter for: Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;->mDataSenders:Landroid/util/LongSparseArray;
    invoke-static {v0}, Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;->access$300(Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;)Landroid/util/LongSparseArray;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/util/LongSparseArray;->delete(J)V

    .line 260
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 261
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferServer$MyDataSenderCallbacks;->this$0:Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;

    # getter for: Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;->mOpenConnections:Ljava/util/concurrent/atomic/AtomicInteger;
    invoke-static {v0}, Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;->access$400(Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v0

    if-nez v0, :cond_0

    .line 262
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferServer$MyDataSenderCallbacks;->this$0:Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;

    # getter for: Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;->mCallback:Lcom/google/android/wearable/datatransfer/internal/DataTransferServer$Callback;
    invoke-static {v0}, Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;->access$500(Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;)Lcom/google/android/wearable/datatransfer/internal/DataTransferServer$Callback;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/wearable/datatransfer/internal/DataTransferServer$Callback;->onAllConnectionsTerminated()V

    goto :goto_0

    .line 260
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public onSendStarted(JLcom/google/android/wearable/datatransfer/internal/DataSender;)V
    .locals 3
    .param p1, "connectionId"    # J
    .param p3, "sender"    # Lcom/google/android/wearable/datatransfer/internal/DataSender;

    .prologue
    .line 239
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferServer$MyDataSenderCallbacks;->this$0:Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;

    # getter for: Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;->mShutdown:Z
    invoke-static {v0}, Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;->access$100(Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 240
    const-string v0, "DataTransferServer"

    invoke-static {v0}, Lcom/google/android/wearable/datatransfer/internal/DebugLog;->isLoggable(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 241
    const-string v0, "DataTransferServer"

    const-string v1, "already shutdown"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 248
    :cond_0
    :goto_0
    return-void

    .line 245
    :cond_1
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferServer$MyDataSenderCallbacks;->this$0:Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;

    # getter for: Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;->mLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;->access$200(Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 246
    :try_start_0
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferServer$MyDataSenderCallbacks;->this$0:Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;

    # getter for: Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;->mDataSenders:Landroid/util/LongSparseArray;
    invoke-static {v0}, Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;->access$300(Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;)Landroid/util/LongSparseArray;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Landroid/util/LongSparseArray;->append(JLjava/lang/Object;)V

    .line 247
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
