.class public final Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;
.super Lcom/google/protobuf/nano/MessageNano;
.source "PacketProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/wearable/datatransfer/proto/PacketProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DataTransferPacket"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket$FirstPacketParameters;
    }
.end annotation


# instance fields
.field public clientConnectionId:J

.field public connectionRefused:Z

.field public connectionRefusedAppStatusCode:I

.field public connectionRefusedStatusCode:I

.field public finalPacket:Z

.field public firstPacketParameters:Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket$FirstPacketParameters;

.field public serverConnectionId:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 244
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 245
    invoke-virtual {p0}, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;->clear()Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;

    .line 246
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v0, 0x0

    .line 249
    iput-wide v2, p0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;->clientConnectionId:J

    .line 250
    iput-wide v2, p0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;->serverConnectionId:J

    .line 251
    iput-boolean v0, p0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;->finalPacket:Z

    .line 252
    iput-boolean v0, p0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;->connectionRefused:Z

    .line 253
    iput v0, p0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;->connectionRefusedStatusCode:I

    .line 254
    iput v0, p0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;->connectionRefusedAppStatusCode:I

    .line 255
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;->firstPacketParameters:Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket$FirstPacketParameters;

    .line 256
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;->cachedSize:I

    .line 257
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 289
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 290
    .local v0, "size":I
    iget-wide v2, p0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;->clientConnectionId:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 291
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;->clientConnectionId:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeFixed64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 294
    :cond_0
    iget-wide v2, p0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;->serverConnectionId:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    .line 295
    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;->serverConnectionId:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeFixed64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 298
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;->finalPacket:Z

    if-eqz v1, :cond_2

    .line 299
    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;->finalPacket:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 302
    :cond_2
    iget-boolean v1, p0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;->connectionRefused:Z

    if-eqz v1, :cond_3

    .line 303
    const/4 v1, 0x4

    iget-boolean v2, p0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;->connectionRefused:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 306
    :cond_3
    iget v1, p0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;->connectionRefusedStatusCode:I

    if-eqz v1, :cond_4

    .line 307
    const/4 v1, 0x5

    iget v2, p0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;->connectionRefusedStatusCode:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 310
    :cond_4
    iget v1, p0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;->connectionRefusedAppStatusCode:I

    if-eqz v1, :cond_5

    .line 311
    const/4 v1, 0x6

    iget v2, p0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;->connectionRefusedAppStatusCode:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 314
    :cond_5
    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;->firstPacketParameters:Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket$FirstPacketParameters;

    if-eqz v1, :cond_6

    .line 315
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;->firstPacketParameters:Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket$FirstPacketParameters;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 318
    :cond_6
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;
    .locals 4
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 326
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 327
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 331
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 332
    :sswitch_0
    return-object p0

    .line 337
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readFixed64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;->clientConnectionId:J

    goto :goto_0

    .line 341
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readFixed64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;->serverConnectionId:J

    goto :goto_0

    .line 345
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;->finalPacket:Z

    goto :goto_0

    .line 349
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;->connectionRefused:Z

    goto :goto_0

    .line 353
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;->connectionRefusedStatusCode:I

    goto :goto_0

    .line 357
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;->connectionRefusedAppStatusCode:I

    goto :goto_0

    .line 361
    :sswitch_7
    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;->firstPacketParameters:Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket$FirstPacketParameters;

    if-nez v1, :cond_1

    .line 362
    new-instance v1, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket$FirstPacketParameters;

    invoke-direct {v1}, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket$FirstPacketParameters;-><init>()V

    iput-object v1, p0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;->firstPacketParameters:Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket$FirstPacketParameters;

    .line 364
    :cond_1
    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;->firstPacketParameters:Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket$FirstPacketParameters;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 327
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x9 -> :sswitch_1
        0x11 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 122
    invoke-virtual {p0, p1}, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 6
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v4, 0x0

    .line 263
    iget-wide v0, p0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;->clientConnectionId:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_0

    .line 264
    const/4 v0, 0x1

    iget-wide v2, p0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;->clientConnectionId:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeFixed64(IJ)V

    .line 266
    :cond_0
    iget-wide v0, p0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;->serverConnectionId:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_1

    .line 267
    const/4 v0, 0x2

    iget-wide v2, p0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;->serverConnectionId:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeFixed64(IJ)V

    .line 269
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;->finalPacket:Z

    if-eqz v0, :cond_2

    .line 270
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;->finalPacket:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 272
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;->connectionRefused:Z

    if-eqz v0, :cond_3

    .line 273
    const/4 v0, 0x4

    iget-boolean v1, p0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;->connectionRefused:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 275
    :cond_3
    iget v0, p0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;->connectionRefusedStatusCode:I

    if-eqz v0, :cond_4

    .line 276
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;->connectionRefusedStatusCode:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 278
    :cond_4
    iget v0, p0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;->connectionRefusedAppStatusCode:I

    if-eqz v0, :cond_5

    .line 279
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;->connectionRefusedAppStatusCode:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 281
    :cond_5
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;->firstPacketParameters:Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket$FirstPacketParameters;

    if-eqz v0, :cond_6

    .line 282
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;->firstPacketParameters:Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket$FirstPacketParameters;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 284
    :cond_6
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 285
    return-void
.end method
