.class final Lcom/google/android/wearable/datatransfer/internal/ConnectionImpl;
.super Ljava/lang/Object;
.source "ConnectionImpl.java"

# interfaces
.implements Lcom/google/android/wearable/datatransfer/Connection;


# instance fields
.field private final mCancelToken:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$ICancelToken;",
            ">;"
        }
    .end annotation
.end field

.field private final mInputStream:Ljava/io/InputStream;

.field private final mTransferSize:J


# direct methods
.method constructor <init>(Ljava/io/InputStream;Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$ICancelToken;J)V
    .locals 1
    .param p1, "inputStream"    # Ljava/io/InputStream;
    .param p2, "cancelToken"    # Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$ICancelToken;
    .param p3, "transferSize"    # J

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/ConnectionImpl;->mCancelToken:Ljava/util/concurrent/atomic/AtomicReference;

    .line 44
    const-string v0, "inputStream"

    invoke-static {p1, v0}, Lcom/google/android/wearable/datatransfer/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/InputStream;

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/ConnectionImpl;->mInputStream:Ljava/io/InputStream;

    .line 45
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/ConnectionImpl;->mCancelToken:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, p2}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 46
    iput-wide p3, p0, Lcom/google/android/wearable/datatransfer/internal/ConnectionImpl;->mTransferSize:J

    .line 47
    return-void
.end method


# virtual methods
.method public close()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 61
    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/internal/ConnectionImpl;->mCancelToken:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicReference;->getAndSet(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$ICancelToken;

    .line 62
    .local v0, "cancelToken":Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$ICancelToken;
    if-eqz v0, :cond_0

    .line 63
    invoke-interface {v0}, Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$ICancelToken;->cancel()V

    .line 65
    :cond_0
    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/internal/ConnectionImpl;->mInputStream:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 66
    return-void
.end method

.method public getInputStream()Ljava/io/InputStream;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/ConnectionImpl;->mInputStream:Ljava/io/InputStream;

    return-object v0
.end method

.method public getSize()J
    .locals 2

    .prologue
    .line 56
    iget-wide v0, p0, Lcom/google/android/wearable/datatransfer/internal/ConnectionImpl;->mTransferSize:J

    return-wide v0
.end method
