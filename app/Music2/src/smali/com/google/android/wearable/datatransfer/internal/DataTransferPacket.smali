.class final Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;
.super Ljava/lang/Object;
.source "DataTransferPacket.java"


# instance fields
.field private mPayload:[B

.field private mPayloadLength:I

.field public final proto:Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    new-instance v0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;

    invoke-direct {v0}, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;-><init>()V

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;->proto:Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;

    .line 37
    return-void
.end method

.method public static newPacket()Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;
    .locals 1

    .prologue
    .line 41
    new-instance v0, Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;

    invoke-direct {v0}, Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;-><init>()V

    return-object v0
.end method

.method private setPayload([BII)V
    .locals 2
    .param p1, "payload"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I

    .prologue
    .line 207
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;->mPayload:[B

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;->mPayload:[B

    array-length v0, v0

    if-ge v0, p3, :cond_1

    .line 208
    :cond_0
    add-int v0, p2, p3

    invoke-static {p1, p2, v0}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;->mPayload:[B

    .line 212
    :goto_0
    iput p3, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;->mPayloadLength:I

    .line 213
    return-void

    .line 210
    :cond_1
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;->mPayload:[B

    const/4 v1, 0x0

    invoke-static {p1, p2, v0, v1, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0
.end method


# virtual methods
.method public getPayloadLength()I
    .locals 1

    .prologue
    .line 203
    iget v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;->mPayloadLength:I

    return v0
.end method

.method public mutateFromBytes([B)V
    .locals 7
    .param p1, "data"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/wearable/datatransfer/internal/InvalidRequestException;
        }
    .end annotation

    .prologue
    .line 51
    invoke-static {p1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 52
    .local v0, "buffer":Ljava/nio/ByteBuffer;
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v2

    .line 53
    .local v2, "headerSize":I
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v5

    if-le v2, v5, :cond_0

    .line 54
    new-instance v5, Lcom/google/android/wearable/datatransfer/internal/InvalidRequestException;

    const-string v6, "packet too small to contain header"

    invoke-direct {v5, v6}, Lcom/google/android/wearable/datatransfer/internal/InvalidRequestException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 56
    :cond_0
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result v5

    add-int v4, v5, v2

    .line 57
    .local v4, "payloadStart":I
    array-length v5, p1

    sub-int v3, v5, v4

    .line 60
    .local v3, "payloadSize":I
    :try_start_0
    iget-object v5, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;->proto:Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;

    invoke-virtual {v5}, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;->clear()Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;

    .line 61
    iget-object v5, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;->proto:Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result v6

    invoke-static {v5, p1, v6, v2}, Lcom/google/protobuf/nano/MessageNano;->mergeFrom(Lcom/google/protobuf/nano/MessageNano;[BII)Lcom/google/protobuf/nano/MessageNano;
    :try_end_0
    .catch Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException; {:try_start_0 .. :try_end_0} :catch_0

    .line 66
    invoke-direct {p0, p1, v4, v3}, Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;->setPayload([BII)V

    .line 67
    return-void

    .line 62
    :catch_0
    move-exception v1

    .line 63
    .local v1, "e":Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;
    new-instance v5, Lcom/google/android/wearable/datatransfer/internal/InvalidRequestException;

    const-string v6, "Invalid DataTransferPacket proto"

    invoke-direct {v5, v6, v1}, Lcom/google/android/wearable/datatransfer/internal/InvalidRequestException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v5
.end method

.method public mutateHeaderOnlyFromBytes([B)V
    .locals 5
    .param p1, "data"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/wearable/datatransfer/internal/InvalidRequestException;
        }
    .end annotation

    .prologue
    .line 71
    invoke-static {p1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 72
    .local v0, "buffer":Ljava/nio/ByteBuffer;
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v2

    .line 73
    .local v2, "headerSize":I
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v3

    if-le v2, v3, :cond_0

    .line 74
    new-instance v3, Lcom/google/android/wearable/datatransfer/internal/InvalidRequestException;

    const-string v4, "packet too small to contain header"

    invoke-direct {v3, v4}, Lcom/google/android/wearable/datatransfer/internal/InvalidRequestException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 78
    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;->proto:Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;

    invoke-virtual {v3}, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;->clear()Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;

    .line 79
    iget-object v3, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;->proto:Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result v4

    invoke-static {v3, p1, v4, v2}, Lcom/google/protobuf/nano/MessageNano;->mergeFrom(Lcom/google/protobuf/nano/MessageNano;[BII)Lcom/google/protobuf/nano/MessageNano;
    :try_end_0
    .catch Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException; {:try_start_0 .. :try_end_0} :catch_0

    .line 83
    return-void

    .line 80
    :catch_0
    move-exception v1

    .line 81
    .local v1, "e":Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;
    new-instance v3, Lcom/google/android/wearable/datatransfer/internal/InvalidRequestException;

    const-string v4, "Invalid DataTransferPacket proto"

    invoke-direct {v3, v4, v1}, Lcom/google/android/wearable/datatransfer/internal/InvalidRequestException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3
.end method

.method public mutateToConnectionRefusedPacket(JJII)V
    .locals 3
    .param p1, "clientConnectionId"    # J
    .param p3, "serverConnectionId"    # J
    .param p5, "statusCode"    # I
    .param p6, "appStatusCode"    # I

    .prologue
    const/4 v1, 0x1

    .line 130
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;->proto:Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;

    invoke-virtual {v0}, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;->clear()Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;

    .line 131
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;->proto:Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;

    iput-wide p3, v0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;->serverConnectionId:J

    .line 132
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;->proto:Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;

    iput-wide p1, v0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;->clientConnectionId:J

    .line 133
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;->proto:Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;

    iput-boolean v1, v0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;->finalPacket:Z

    .line 134
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;->proto:Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;

    iput-boolean v1, v0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;->connectionRefused:Z

    .line 135
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;->proto:Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;

    iput p5, v0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;->connectionRefusedStatusCode:I

    .line 136
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;->proto:Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;

    iput p6, v0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;->connectionRefusedAppStatusCode:I

    .line 137
    return-void
.end method

.method public mutateToDataPacket(JJZ[BII)V
    .locals 5
    .param p1, "clientConnectionId"    # J
    .param p3, "serverConnectionId"    # J
    .param p5, "fin"    # Z
    .param p6, "payload"    # [B
    .param p7, "offset"    # I
    .param p8, "length"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 105
    if-ltz p8, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "length must not be negative"

    invoke-static {v0, v3}, Lcom/google/android/wearable/datatransfer/internal/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    .line 106
    if-ltz p7, :cond_1

    move v0, v1

    :goto_1
    const-string v3, "offset must not be negative"

    invoke-static {v0, v3}, Lcom/google/android/wearable/datatransfer/internal/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    .line 107
    add-int v0, p7, p8

    array-length v3, p6

    if-gt v0, v3, :cond_2

    :goto_2
    const-string v0, "offset + size must be within payload"

    invoke-static {v1, v0}, Lcom/google/android/wearable/datatransfer/internal/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    .line 109
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;->proto:Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;

    invoke-virtual {v0}, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;->clear()Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;

    .line 110
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;->proto:Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;

    iput-wide p3, v0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;->serverConnectionId:J

    .line 111
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;->proto:Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;

    iput-wide p1, v0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;->clientConnectionId:J

    .line 112
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;->proto:Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;

    iput-boolean p5, v0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;->finalPacket:Z

    .line 114
    invoke-direct {p0, p6, p7, p8}, Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;->setPayload([BII)V

    .line 115
    return-void

    :cond_0
    move v0, v2

    .line 105
    goto :goto_0

    :cond_1
    move v0, v2

    .line 106
    goto :goto_1

    :cond_2
    move v1, v2

    .line 107
    goto :goto_2
.end method

.method public mutateToFirstPacket(JJJ)V
    .locals 3
    .param p1, "clientConnectionId"    # J
    .param p3, "serverConnectionId"    # J
    .param p5, "transferSize"    # J

    .prologue
    .line 151
    const-wide/16 v0, -0x1

    cmp-long v0, p5, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Invalid transferSize"

    invoke-static {v0, v1}, Lcom/google/android/wearable/datatransfer/internal/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    .line 152
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;->proto:Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;

    invoke-virtual {v0}, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;->clear()Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;

    .line 153
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;->proto:Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;

    iput-wide p3, v0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;->serverConnectionId:J

    .line 154
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;->proto:Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;

    iput-wide p1, v0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;->clientConnectionId:J

    .line 155
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;->proto:Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;

    new-instance v1, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket$FirstPacketParameters;

    invoke-direct {v1}, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket$FirstPacketParameters;-><init>()V

    iput-object v1, v0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;->firstPacketParameters:Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket$FirstPacketParameters;

    .line 157
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;->proto:Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;

    iget-object v0, v0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;->firstPacketParameters:Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket$FirstPacketParameters;

    iput-wide p5, v0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket$FirstPacketParameters;->transferSize:J

    .line 158
    return-void

    .line 151
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toBytes([B)[B
    .locals 7
    .param p1, "arrayToReuse"    # [B

    .prologue
    .line 174
    iget-object v4, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;->proto:Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;

    invoke-virtual {v4}, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;->getSerializedSize()I

    move-result v1

    .line 175
    .local v1, "headerSize":I
    add-int/lit8 v4, v1, 0x4

    iget v5, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;->mPayloadLength:I

    add-int v3, v4, v5

    .line 177
    .local v3, "serializedSize":I
    if-eqz p1, :cond_1

    array-length v4, p1

    if-ne v4, v3, :cond_1

    move-object v2, p1

    .line 180
    .local v2, "outputArray":[B
    :goto_0
    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 181
    .local v0, "buffer":Ljava/nio/ByteBuffer;
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 182
    iget-object v4, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;->proto:Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result v5

    invoke-static {v4, v2, v5, v1}, Lcom/google/protobuf/nano/MessageNano;->toByteArray(Lcom/google/protobuf/nano/MessageNano;[BII)V

    .line 183
    iget v4, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;->mPayloadLength:I

    if-lez v4, :cond_0

    .line 184
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result v4

    add-int/2addr v4, v1

    invoke-virtual {v0, v4}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 185
    iget-object v4, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;->mPayload:[B

    const/4 v5, 0x0

    iget v6, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;->mPayloadLength:I

    invoke-virtual {v0, v4, v5, v6}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    .line 187
    :cond_0
    return-object v2

    .line 177
    .end local v0    # "buffer":Ljava/nio/ByteBuffer;
    .end local v2    # "outputArray":[B
    :cond_1
    new-array v2, v3, [B

    goto :goto_0
.end method

.method public writePayload(Ljava/io/OutputStream;)V
    .locals 3
    .param p1, "outputStream"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 196
    iget v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;->mPayloadLength:I

    if-lez v0, :cond_0

    .line 197
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;->mPayload:[B

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;->mPayloadLength:I

    invoke-virtual {p1, v0, v1, v2}, Ljava/io/OutputStream;->write([BII)V

    .line 199
    :cond_0
    return-void
.end method
