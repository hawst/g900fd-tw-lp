.class public final Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;
.super Ljava/lang/Object;
.source "DataTransferClient.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/wearable/datatransfer/internal/DataTransferClient$1;,
        Lcom/google/android/wearable/datatransfer/internal/DataTransferClient$ReceiverCancelToken;,
        Lcom/google/android/wearable/datatransfer/internal/DataTransferClient$CallbackFutureTask;,
        Lcom/google/android/wearable/datatransfer/internal/DataTransferClient$MyDataReceiverCallbacks;,
        Lcom/google/android/wearable/datatransfer/internal/DataTransferClient$Callback;
    }
.end annotation


# instance fields
.field private final mCallback:Lcom/google/android/wearable/datatransfer/internal/DataTransferClient$Callback;

.field private final mCancelTokens:Landroid/util/LongSparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LongSparseArray",
            "<",
            "Lcom/google/android/wearable/datatransfer/internal/DataTransferClient$ReceiverCancelToken;",
            ">;"
        }
    .end annotation
.end field

.field private final mClient:Lcom/google/android/gms/common/api/GoogleApiClient;

.field private final mDataReceiverCallbacks:Lcom/google/android/wearable/datatransfer/internal/DataTransferClient$MyDataReceiverCallbacks;

.field private final mDownloadsThreadPool:Ljava/util/concurrent/ExecutorService;

.field private final mLock:Ljava/lang/Object;

.field private final mNextConnectionIdGenerator:Ljava/util/Random;

.field private final mPendingOpenConnectionResults:Landroid/util/LongSparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LongSparseArray",
            "<",
            "Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult",
            "<",
            "Lcom/google/android/wearable/datatransfer/DataTransferApi$OpenConnectionResult;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mReceivers:Landroid/util/LongSparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LongSparseArray",
            "<",
            "Lcom/google/android/wearable/datatransfer/internal/DataReceiver;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/api/GoogleApiClient;Lcom/google/android/wearable/datatransfer/internal/DataTransferClient$Callback;)V
    .locals 3
    .param p1, "client"    # Lcom/google/android/gms/common/api/GoogleApiClient;
    .param p2, "callback"    # Lcom/google/android/wearable/datatransfer/internal/DataTransferClient$Callback;

    .prologue
    const/4 v2, 0x2

    .line 106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    new-instance v0, Ljava/security/SecureRandom;

    invoke-direct {v0}, Ljava/security/SecureRandom;-><init>()V

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;->mNextConnectionIdGenerator:Ljava/util/Random;

    .line 63
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;->mLock:Ljava/lang/Object;

    .line 69
    new-instance v0, Landroid/util/LongSparseArray;

    invoke-direct {v0}, Landroid/util/LongSparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;->mPendingOpenConnectionResults:Landroid/util/LongSparseArray;

    .line 79
    new-instance v0, Landroid/util/LongSparseArray;

    invoke-direct {v0, v2}, Landroid/util/LongSparseArray;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;->mReceivers:Landroid/util/LongSparseArray;

    .line 86
    new-instance v0, Landroid/util/LongSparseArray;

    invoke-direct {v0}, Landroid/util/LongSparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;->mCancelTokens:Landroid/util/LongSparseArray;

    .line 95
    new-instance v0, Lcom/google/android/wearable/datatransfer/internal/CustomThreadFactory;

    const-string v1, "DataTransferClient"

    invoke-direct {v0, v1}, Lcom/google/android/wearable/datatransfer/internal/CustomThreadFactory;-><init>(Ljava/lang/String;)V

    invoke-static {v2, v0}, Ljava/util/concurrent/Executors;->newFixedThreadPool(ILjava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;->mDownloadsThreadPool:Ljava/util/concurrent/ExecutorService;

    .line 104
    new-instance v0, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient$MyDataReceiverCallbacks;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient$MyDataReceiverCallbacks;-><init>(Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;Lcom/google/android/wearable/datatransfer/internal/DataTransferClient$1;)V

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;->mDataReceiverCallbacks:Lcom/google/android/wearable/datatransfer/internal/DataTransferClient$MyDataReceiverCallbacks;

    .line 107
    const-string v0, "client"

    invoke-static {p1, v0}, Lcom/google/android/wearable/datatransfer/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/GoogleApiClient;

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;->mClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    .line 108
    iput-object p2, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;->mCallback:Lcom/google/android/wearable/datatransfer/internal/DataTransferClient$Callback;

    .line 109
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;->mLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;)Landroid/util/LongSparseArray;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;->mReceivers:Landroid/util/LongSparseArray;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;)Landroid/util/LongSparseArray;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;->mPendingOpenConnectionResults:Landroid/util/LongSparseArray;

    return-object v0
.end method

.method static synthetic access$400(Landroid/util/LongSparseArray;J)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Landroid/util/LongSparseArray;
    .param p1, "x1"    # J

    .prologue
    .line 35
    invoke-static {p0, p1, p2}, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;->getAndRemove(Landroid/util/LongSparseArray;J)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;)Landroid/util/LongSparseArray;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;->mCancelTokens:Landroid/util/LongSparseArray;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;J)Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$ICancelToken;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;
    .param p1, "x1"    # J

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;->getCancelToken(J)Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$ICancelToken;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;JII)V
    .locals 1
    .param p0, "x0"    # Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;
    .param p1, "x1"    # J
    .param p3, "x2"    # I
    .param p4, "x3"    # I

    .prologue
    .line 35
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;->terminateConnection(JII)V

    return-void
.end method

.method private cancelAllTokens()V
    .locals 5

    .prologue
    .line 243
    :goto_0
    iget-object v3, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;->mLock:Ljava/lang/Object;

    monitor-enter v3

    .line 244
    :try_start_0
    iget-object v2, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;->mCancelTokens:Landroid/util/LongSparseArray;

    invoke-virtual {v2}, Landroid/util/LongSparseArray;->size()I

    move-result v0

    .line 245
    .local v0, "remainingTokens":I
    if-nez v0, :cond_0

    .line 246
    monitor-exit v3

    return-void

    .line 250
    :cond_0
    iget-object v2, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;->mCancelTokens:Landroid/util/LongSparseArray;

    add-int/lit8 v4, v0, -0x1

    invoke-virtual {v2, v4}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient$ReceiverCancelToken;

    .line 251
    .local v1, "token":Lcom/google/android/wearable/datatransfer/internal/DataTransferClient$ReceiverCancelToken;
    iget-object v2, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;->mCancelTokens:Landroid/util/LongSparseArray;

    add-int/lit8 v4, v0, -0x1

    invoke-virtual {v2, v4}, Landroid/util/LongSparseArray;->removeAt(I)V

    .line 252
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 253
    invoke-virtual {v1}, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient$ReceiverCancelToken;->cancel()V

    goto :goto_0

    .line 252
    .end local v0    # "remainingTokens":I
    .end local v1    # "token":Lcom/google/android/wearable/datatransfer/internal/DataTransferClient$ReceiverCancelToken;
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method

.method private static getAndRemove(Landroid/util/LongSparseArray;J)Ljava/lang/Object;
    .locals 1
    .param p1, "key"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Landroid/util/LongSparseArray",
            "<+TT;>;J)TT;"
        }
    .end annotation

    .prologue
    .line 304
    .local p0, "sparseArray":Landroid/util/LongSparseArray;, "Landroid/util/LongSparseArray<+TT;>;"
    invoke-virtual {p0, p1, p2}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v0

    .line 305
    .local v0, "result":Ljava/lang/Object;, "TT;"
    invoke-virtual {p0, p1, p2}, Landroid/util/LongSparseArray;->remove(J)V

    .line 306
    return-object v0
.end method

.method private getCancelToken(J)Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$ICancelToken;
    .locals 3
    .param p1, "connectionId"    # J

    .prologue
    .line 293
    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 294
    :try_start_0
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;->mCancelTokens:Landroid/util/LongSparseArray;

    invoke-virtual {v0, p1, p2}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$ICancelToken;

    monitor-exit v1

    return-object v0

    .line 295
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private handleDataTransferPacket(Lcom/google/android/gms/wearable/MessageEvent;)V
    .locals 9
    .param p1, "messageEvent"    # Lcom/google/android/gms/wearable/MessageEvent;

    .prologue
    const/4 v8, 0x0

    .line 185
    const-string v4, "DataTransferClient"

    invoke-static {v4}, Lcom/google/android/wearable/datatransfer/internal/DebugLog;->isLoggable(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 186
    const-string v4, "DataTransferClient"

    const-string v5, "handleDataTransferPacket"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 188
    :cond_0
    invoke-static {}, Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;->newPacket()Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;

    move-result-object v1

    .line 191
    .local v1, "packet":Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;
    :try_start_0
    invoke-interface {p1}, Lcom/google/android/gms/wearable/MessageEvent;->getData()[B

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;->mutateHeaderOnlyFromBytes([B)V
    :try_end_0
    .catch Lcom/google/android/wearable/datatransfer/internal/InvalidRequestException; {:try_start_0 .. :try_end_0} :catch_0

    .line 198
    iget-object v5, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;->mLock:Ljava/lang/Object;

    monitor-enter v5

    .line 199
    :try_start_1
    iget-object v4, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;->mReceivers:Landroid/util/LongSparseArray;

    iget-object v6, v1, Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;->proto:Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;

    iget-wide v6, v6, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;->clientConnectionId:J

    invoke-virtual {v4, v6, v7}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/wearable/datatransfer/internal/DataReceiver;

    .line 200
    .local v3, "receiver":Lcom/google/android/wearable/datatransfer/internal/DataReceiver;
    iget-object v4, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;->mPendingOpenConnectionResults:Landroid/util/LongSparseArray;

    iget-object v6, v1, Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;->proto:Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;

    iget-wide v6, v6, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;->clientConnectionId:J

    invoke-virtual {v4, v6, v7}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;

    .line 201
    .local v2, "pendingResult":Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;, "Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult<Lcom/google/android/wearable/datatransfer/DataTransferApi$OpenConnectionResult;>;"
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 202
    if-nez v3, :cond_2

    .line 203
    const-string v4, "DataTransferClient"

    const-string v5, "DATA_TRANSFER for connection which is not open."

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 204
    if-eqz v2, :cond_1

    .line 207
    new-instance v4, Lcom/google/android/wearable/datatransfer/internal/OpenConnectionResultImpl;

    const/4 v5, 0x0

    const/16 v6, 0x8

    invoke-direct {v4, v5, v6, v8, v8}, Lcom/google/android/wearable/datatransfer/internal/OpenConnectionResultImpl;-><init>(Lcom/google/android/wearable/datatransfer/Connection;III)V

    invoke-virtual {v2, v4}, Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;->setResult(Ljava/lang/Object;)V

    .line 221
    .end local v2    # "pendingResult":Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;, "Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult<Lcom/google/android/wearable/datatransfer/DataTransferApi$OpenConnectionResult;>;"
    .end local v3    # "receiver":Lcom/google/android/wearable/datatransfer/internal/DataReceiver;
    :cond_1
    :goto_0
    return-void

    .line 192
    :catch_0
    move-exception v0

    .line 193
    .local v0, "e":Lcom/google/android/wearable/datatransfer/internal/InvalidRequestException;
    const-string v4, "DataTransferClient"

    const-string v5, "Invalid DATA_TRANSFER packet received"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 201
    .end local v0    # "e":Lcom/google/android/wearable/datatransfer/internal/InvalidRequestException;
    :catchall_0
    move-exception v4

    :try_start_2
    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v4

    .line 212
    .restart local v2    # "pendingResult":Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;, "Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult<Lcom/google/android/wearable/datatransfer/DataTransferApi$OpenConnectionResult;>;"
    .restart local v3    # "receiver":Lcom/google/android/wearable/datatransfer/internal/DataReceiver;
    :cond_2
    const-string v4, "DataTransferClient"

    invoke-static {v4}, Lcom/google/android/wearable/datatransfer/internal/DebugLog;->isLoggable(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 213
    const-string v4, "DataTransferClient"

    const-string v5, "pass to receiver"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 215
    :cond_3
    invoke-virtual {v3}, Lcom/google/android/wearable/datatransfer/internal/DataReceiver;->getRemoteNodeId()Ljava/lang/String;

    move-result-object v4

    invoke-interface {p1}, Lcom/google/android/gms/wearable/MessageEvent;->getSourceNodeId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 216
    const-string v4, "DataTransferClient"

    const-string v5, "received message for connection from wrong node"

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 220
    :cond_4
    invoke-interface {p1}, Lcom/google/android/gms/wearable/MessageEvent;->getData()[B

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/wearable/datatransfer/internal/DataReceiver;->onPacketReceived([B)V

    goto :goto_0
.end method

.method public static isHandledPath(Ljava/lang/String;)Z
    .locals 1
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 116
    const-string v0, "com.google.android.wearable.datatransfer.DATA_TRANSFER"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private terminateConnection(JII)V
    .locals 7
    .param p1, "connectionId"    # J
    .param p3, "serverStatusCode"    # I
    .param p4, "appErrorCode"    # I

    .prologue
    .line 267
    iget-object v4, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;->mLock:Ljava/lang/Object;

    monitor-enter v4

    .line 268
    :try_start_0
    iget-object v3, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;->mPendingOpenConnectionResults:Landroid/util/LongSparseArray;

    invoke-static {v3, p1, p2}, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;->getAndRemove(Landroid/util/LongSparseArray;J)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;

    .line 269
    .local v2, "pendingResult":Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;, "Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult<Lcom/google/android/wearable/datatransfer/DataTransferApi$OpenConnectionResult;>;"
    iget-object v3, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;->mReceivers:Landroid/util/LongSparseArray;

    invoke-virtual {v3, p1, p2}, Landroid/util/LongSparseArray;->remove(J)V

    .line 270
    iget-object v3, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;->mCancelTokens:Landroid/util/LongSparseArray;

    invoke-static {v3, p1, p2}, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;->getAndRemove(Landroid/util/LongSparseArray;J)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient$ReceiverCancelToken;

    .line 271
    .local v1, "cancelToken":Lcom/google/android/wearable/datatransfer/internal/DataTransferClient$ReceiverCancelToken;
    invoke-virtual {p0}, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;->hasOpenConnections()Z

    move-result v3

    if-nez v3, :cond_3

    const/4 v0, 0x1

    .line 272
    .local v0, "allConnectionsTerminated":Z
    :goto_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 273
    if-eqz v2, :cond_0

    .line 280
    new-instance v3, Lcom/google/android/wearable/datatransfer/internal/OpenConnectionResultImpl;

    const/4 v4, 0x0

    const/16 v5, 0xd

    invoke-direct {v3, v4, v5, p3, p4}, Lcom/google/android/wearable/datatransfer/internal/OpenConnectionResultImpl;-><init>(Lcom/google/android/wearable/datatransfer/Connection;III)V

    invoke-virtual {v2, v3}, Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;->setResult(Ljava/lang/Object;)V

    .line 283
    :cond_0
    if-eqz v1, :cond_1

    .line 284
    invoke-virtual {v1}, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient$ReceiverCancelToken;->futureDone()V

    .line 286
    :cond_1
    if-eqz v0, :cond_2

    iget-object v3, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;->mCallback:Lcom/google/android/wearable/datatransfer/internal/DataTransferClient$Callback;

    if-eqz v3, :cond_2

    .line 287
    iget-object v3, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;->mCallback:Lcom/google/android/wearable/datatransfer/internal/DataTransferClient$Callback;

    invoke-interface {v3}, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient$Callback;->onAllConnectionsTerminated()V

    .line 289
    :cond_2
    return-void

    .line 271
    .end local v0    # "allConnectionsTerminated":Z
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 272
    .end local v1    # "cancelToken":Lcom/google/android/wearable/datatransfer/internal/DataTransferClient$ReceiverCancelToken;
    .end local v2    # "pendingResult":Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;, "Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult<Lcom/google/android/wearable/datatransfer/DataTransferApi$OpenConnectionResult;>;"
    :catchall_0
    move-exception v3

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3
.end method


# virtual methods
.method public hasOpenConnections()Z
    .locals 2

    .prologue
    .line 175
    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 176
    :try_start_0
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;->mPendingOpenConnectionResults:Landroid/util/LongSparseArray;

    invoke-virtual {v0}, Landroid/util/LongSparseArray;->size()I

    move-result v0

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;->mReceivers:Landroid/util/LongSparseArray;

    invoke-virtual {v0}, Landroid/util/LongSparseArray;->size()I

    move-result v0

    if-lez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    monitor-exit v1

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 178
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onMessageReceived(Lcom/google/android/gms/wearable/MessageEvent;)Z
    .locals 2
    .param p1, "messageEvent"    # Lcom/google/android/gms/wearable/MessageEvent;

    .prologue
    .line 163
    const-string v0, "com.google.android.wearable.datatransfer.DATA_TRANSFER"

    invoke-interface {p1}, Lcom/google/android/gms/wearable/MessageEvent;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 164
    invoke-direct {p0, p1}, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;->handleDataTransferPacket(Lcom/google/android/gms/wearable/MessageEvent;)V

    .line 165
    const/4 v0, 0x1

    .line 167
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public openConnection(Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 12
    .param p2, "nodeId"    # Ljava/lang/String;
    .param p3, "path"    # Ljava/lang/String;
    .param p4, "offset"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult",
            "<",
            "Lcom/google/android/wearable/datatransfer/DataTransferApi$OpenConnectionResult;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "J)V"
        }
    .end annotation

    .prologue
    .line 137
    .local p1, "pendingResult":Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;, "Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult<Lcom/google/android/wearable/datatransfer/DataTransferApi$OpenConnectionResult;>;"
    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;->mNextConnectionIdGenerator:Ljava/util/Random;

    invoke-virtual {v1}, Ljava/util/Random;->nextLong()J

    move-result-wide v6

    .line 139
    .local v6, "connectionId":J
    iget-object v2, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;->mLock:Ljava/lang/Object;

    monitor-enter v2

    .line 140
    :try_start_0
    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;->mPendingOpenConnectionResults:Landroid/util/LongSparseArray;

    invoke-virtual {v1, v6, v7, p1}, Landroid/util/LongSparseArray;->append(JLjava/lang/Object;)V

    .line 141
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 142
    new-instance v0, Lcom/google/android/wearable/datatransfer/internal/ReceiveDataCallable;

    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;->mClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    iget-object v8, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;->mDataReceiverCallbacks:Lcom/google/android/wearable/datatransfer/internal/DataTransferClient$MyDataReceiverCallbacks;

    move-object v2, p2

    move-object v3, p3

    move-wide/from16 v4, p4

    invoke-direct/range {v0 .. v8}, Lcom/google/android/wearable/datatransfer/internal/ReceiveDataCallable;-><init>(Lcom/google/android/gms/common/api/GoogleApiClient;Ljava/lang/String;Ljava/lang/String;JJLcom/google/android/wearable/datatransfer/internal/DataReceiver$Callbacks;)V

    .line 144
    .local v0, "callable":Lcom/google/android/wearable/datatransfer/internal/ReceiveDataCallable;
    new-instance v10, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient$CallbackFutureTask;

    invoke-direct {v10, p0, v0, v6, v7}, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient$CallbackFutureTask;-><init>(Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;Lcom/google/android/wearable/datatransfer/internal/ReceiveDataCallable;J)V

    .line 145
    .local v10, "futureTask":Lcom/google/android/wearable/datatransfer/internal/DataTransferClient$CallbackFutureTask;
    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;->mDownloadsThreadPool:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v1, v10}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 149
    new-instance v9, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient$ReceiverCancelToken;

    invoke-direct {v9, p0, v10, v6, v7}, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient$ReceiverCancelToken;-><init>(Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;Ljava/util/concurrent/FutureTask;J)V

    .line 150
    .local v9, "cancelToken":Lcom/google/android/wearable/datatransfer/internal/DataTransferClient$ReceiverCancelToken;
    invoke-virtual {p1, v9}, Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;->setCancelToken(Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$ICancelToken;)V

    .line 151
    iget-object v2, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;->mLock:Ljava/lang/Object;

    monitor-enter v2

    .line 152
    :try_start_1
    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;->mCancelTokens:Landroid/util/LongSparseArray;

    invoke-virtual {v1, v6, v7, v9}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 153
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 154
    return-void

    .line 141
    .end local v0    # "callable":Lcom/google/android/wearable/datatransfer/internal/ReceiveDataCallable;
    .end local v9    # "cancelToken":Lcom/google/android/wearable/datatransfer/internal/DataTransferClient$ReceiverCancelToken;
    .end local v10    # "futureTask":Lcom/google/android/wearable/datatransfer/internal/DataTransferClient$CallbackFutureTask;
    :catchall_0
    move-exception v1

    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 153
    .restart local v0    # "callable":Lcom/google/android/wearable/datatransfer/internal/ReceiveDataCallable;
    .restart local v9    # "cancelToken":Lcom/google/android/wearable/datatransfer/internal/DataTransferClient$ReceiverCancelToken;
    .restart local v10    # "futureTask":Lcom/google/android/wearable/datatransfer/internal/DataTransferClient$CallbackFutureTask;
    :catchall_1
    move-exception v1

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v1
.end method

.method public shutdown()V
    .locals 2

    .prologue
    .line 227
    invoke-direct {p0}, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;->cancelAllTokens()V

    .line 228
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;->mDownloadsThreadPool:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdownNow()Ljava/util/List;

    .line 229
    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 230
    :try_start_0
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;->mReceivers:Landroid/util/LongSparseArray;

    invoke-virtual {v0}, Landroid/util/LongSparseArray;->clear()V

    .line 231
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;->mPendingOpenConnectionResults:Landroid/util/LongSparseArray;

    invoke-virtual {v0}, Landroid/util/LongSparseArray;->clear()V

    .line 232
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;->mCancelTokens:Landroid/util/LongSparseArray;

    invoke-virtual {v0}, Landroid/util/LongSparseArray;->clear()V

    .line 233
    monitor-exit v1

    .line 234
    return-void

    .line 233
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
