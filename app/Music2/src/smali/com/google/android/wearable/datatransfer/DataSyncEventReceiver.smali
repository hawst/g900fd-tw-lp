.class public final Lcom/google/android/wearable/datatransfer/DataSyncEventReceiver;
.super Landroid/content/BroadcastReceiver;
.source "DataSyncEventReceiver.java"


# static fields
.field private static final SUPPORTED_ACTIONS:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 40
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/google/android/wearable/datatransfer/DataSyncEventReceiver;->SUPPORTED_ACTIONS:Ljava/util/Set;

    .line 42
    sget-object v0, Lcom/google/android/wearable/datatransfer/DataSyncEventReceiver;->SUPPORTED_ACTIONS:Ljava/util/Set;

    const-string v1, "android.intent.action.ACTION_POWER_CONNECTED"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 43
    sget-object v0, Lcom/google/android/wearable/datatransfer/DataSyncEventReceiver;->SUPPORTED_ACTIONS:Ljava/util/Set;

    const-string v1, "android.intent.action.ACTION_POWER_DISCONNECTED"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 44
    sget-object v0, Lcom/google/android/wearable/datatransfer/DataSyncEventReceiver;->SUPPORTED_ACTIONS:Ljava/util/Set;

    const-string v1, "android.intent.action.BATTERY_LOW"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 45
    sget-object v0, Lcom/google/android/wearable/datatransfer/DataSyncEventReceiver;->SUPPORTED_ACTIONS:Ljava/util/Set;

    const-string v1, "android.intent.action.BATTERY_OKAY"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 46
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 59
    if-eqz p2, :cond_1

    sget-object v0, Lcom/google/android/wearable/datatransfer/DataSyncEventReceiver;->SUPPORTED_ACTIONS:Ljava/util/Set;

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 62
    invoke-static {p1}, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->isServiceRegistered(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 63
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->startWithBatteryUpdate(Landroid/content/Context;Ljava/lang/String;)V

    .line 72
    :cond_0
    :goto_0
    return-void

    .line 70
    :cond_1
    const-string v0, "DataSyncEventReceiver"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unexpected intent: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
