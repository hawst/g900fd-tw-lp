.class public Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;
.super Ljava/lang/Object;
.source "DataSyncServiceHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper$CallbackFutureTask;,
        Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper$IntentProcessor;,
        Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper$CompletionListener;
    }
.end annotation


# static fields
.field private static final BATTERY_STATUS_EXPIRATION_TIME_MS:J

.field private static final MIN_DELAY_BEFORE_ALARM:J


# instance fields
.field private final mAlarmScheduler:Lcom/google/android/wearable/datatransfer/internal/DataSyncAlarmScheduler;

.field private final mApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

.field private final mBatteryPolicy:Lcom/google/android/wearable/datatransfer/internal/BatteryPolicy;

.field private final mClient:Lcom/google/android/wearable/datatransfer/WearableDataApiClient;

.field private final mClock:Lcom/google/android/wearable/datatransfer/internal/Clock;

.field private final mDataSyncers:Ljava/util/concurrent/ConcurrentMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentMap",
            "<",
            "Ljava/io/File;",
            "Lcom/google/android/wearable/datatransfer/internal/DataSyncer;",
            ">;"
        }
    .end annotation
.end field

.field private final mDataSyncersThreadPool:Ljava/util/concurrent/ExecutorService;

.field private final mLock:Ljava/lang/Object;

.field private final mPeerProvider:Lcom/google/android/wearable/datatransfer/internal/PeerProvider;

.field private final mPreferences:Landroid/content/SharedPreferences;

.field private final mProcessingExecutor:Ljava/util/concurrent/ExecutorService;

.field private final mRetryPolicy:Lcom/google/android/wearable/datatransfer/internal/RetryPolicy;

.field private final mSharedBatteryStatusUpdater:Lcom/google/android/wearable/datatransfer/internal/SharedBatteryStatusUpdater;

.field private mSyncStateFlags:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0xa

    .line 91
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->MIN_DELAY_BEFORE_ALARM:J

    .line 98
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->BATTERY_STATUS_EXPIRATION_TIME_MS:J

    return-void
.end method

.method constructor <init>(Lcom/google/android/gms/common/api/GoogleApiClient;Lcom/google/android/wearable/datatransfer/WearableDataApiClient;Lcom/google/android/wearable/datatransfer/internal/PeerProvider;Lcom/google/android/wearable/datatransfer/internal/SharedBatteryStatusUpdater;Lcom/google/android/wearable/datatransfer/internal/Clock;Lcom/google/android/wearable/datatransfer/internal/BatteryPolicy;Lcom/google/android/wearable/datatransfer/internal/RetryPolicy;Lcom/google/android/wearable/datatransfer/internal/DataSyncAlarmScheduler;Landroid/content/SharedPreferences;)V
    .locals 3
    .param p1, "apiClient"    # Lcom/google/android/gms/common/api/GoogleApiClient;
    .param p2, "client"    # Lcom/google/android/wearable/datatransfer/WearableDataApiClient;
    .param p3, "peerProvider"    # Lcom/google/android/wearable/datatransfer/internal/PeerProvider;
    .param p4, "sharedBatteryStatusUpdater"    # Lcom/google/android/wearable/datatransfer/internal/SharedBatteryStatusUpdater;
    .param p5, "clock"    # Lcom/google/android/wearable/datatransfer/internal/Clock;
    .param p6, "batteryPolicy"    # Lcom/google/android/wearable/datatransfer/internal/BatteryPolicy;
    .param p7, "retryPolicy"    # Lcom/google/android/wearable/datatransfer/internal/RetryPolicy;
    .param p8, "alarmScheduler"    # Lcom/google/android/wearable/datatransfer/internal/DataSyncAlarmScheduler;
    .param p9, "preferences"    # Landroid/content/SharedPreferences;

    .prologue
    .line 249
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 101
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->mLock:Ljava/lang/Object;

    .line 110
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->mSyncStateFlags:Ljava/lang/Integer;

    .line 211
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->mDataSyncers:Ljava/util/concurrent/ConcurrentMap;

    .line 219
    const/4 v0, 0x2

    new-instance v1, Lcom/google/android/wearable/datatransfer/internal/CustomThreadFactory;

    const-string v2, "DataSyncer"

    invoke-direct {v1, v2}, Lcom/google/android/wearable/datatransfer/internal/CustomThreadFactory;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Ljava/util/concurrent/Executors;->newFixedThreadPool(ILjava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->mDataSyncersThreadPool:Ljava/util/concurrent/ExecutorService;

    .line 227
    new-instance v0, Lcom/google/android/wearable/datatransfer/internal/CustomThreadFactory;

    const-string v1, "DataSyncServiceHelper"

    invoke-direct {v0, v1}, Lcom/google/android/wearable/datatransfer/internal/CustomThreadFactory;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor(Ljava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->mProcessingExecutor:Ljava/util/concurrent/ExecutorService;

    .line 250
    const-string v0, "apiClient"

    invoke-static {p1, v0}, Lcom/google/android/wearable/datatransfer/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/GoogleApiClient;

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->mApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    .line 251
    const-string v0, "client"

    invoke-static {p2, v0}, Lcom/google/android/wearable/datatransfer/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/wearable/datatransfer/WearableDataApiClient;

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->mClient:Lcom/google/android/wearable/datatransfer/WearableDataApiClient;

    .line 252
    const-string v0, "peerProvider"

    invoke-static {p3, v0}, Lcom/google/android/wearable/datatransfer/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/wearable/datatransfer/internal/PeerProvider;

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->mPeerProvider:Lcom/google/android/wearable/datatransfer/internal/PeerProvider;

    .line 253
    const-string v0, "sharedBatteryStatusUpdater"

    invoke-static {p4, v0}, Lcom/google/android/wearable/datatransfer/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/wearable/datatransfer/internal/SharedBatteryStatusUpdater;

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->mSharedBatteryStatusUpdater:Lcom/google/android/wearable/datatransfer/internal/SharedBatteryStatusUpdater;

    .line 255
    const-string v0, "batteryChecker"

    invoke-static {p6, v0}, Lcom/google/android/wearable/datatransfer/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/wearable/datatransfer/internal/BatteryPolicy;

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->mBatteryPolicy:Lcom/google/android/wearable/datatransfer/internal/BatteryPolicy;

    .line 256
    const-string v0, "clock"

    invoke-static {p5, v0}, Lcom/google/android/wearable/datatransfer/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/wearable/datatransfer/internal/Clock;

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->mClock:Lcom/google/android/wearable/datatransfer/internal/Clock;

    .line 257
    const-string v0, "retryPolicy"

    invoke-static {p7, v0}, Lcom/google/android/wearable/datatransfer/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/wearable/datatransfer/internal/RetryPolicy;

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->mRetryPolicy:Lcom/google/android/wearable/datatransfer/internal/RetryPolicy;

    .line 258
    const-string v0, "alarmScheduler"

    invoke-static {p8, v0}, Lcom/google/android/wearable/datatransfer/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/wearable/datatransfer/internal/DataSyncAlarmScheduler;

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->mAlarmScheduler:Lcom/google/android/wearable/datatransfer/internal/DataSyncAlarmScheduler;

    .line 259
    const-string v0, "preferences"

    invoke-static {p9, v0}, Lcom/google/android/wearable/datatransfer/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->mPreferences:Landroid/content/SharedPreferences;

    .line 261
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->mApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/GoogleApiClient;->connect()V

    .line 262
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->mClient:Lcom/google/android/wearable/datatransfer/WearableDataApiClient;

    invoke-virtual {v0}, Lcom/google/android/wearable/datatransfer/WearableDataApiClient;->connect()V

    .line 263
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;Landroid/content/Intent;Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper$CompletionListener;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;
    .param p1, "x1"    # Landroid/content/Intent;
    .param p2, "x2"    # Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper$CompletionListener;

    .prologue
    .line 50
    invoke-direct {p0, p1, p2}, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->processIntent(Landroid/content/Intent;Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper$CompletionListener;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;)Ljava/util/concurrent/ConcurrentMap;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->mDataSyncers:Ljava/util/concurrent/ConcurrentMap;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;)Lcom/google/android/wearable/datatransfer/WearableDataApiClient;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->mClient:Lcom/google/android/wearable/datatransfer/WearableDataApiClient;

    return-object v0
.end method

.method private computeSyncStateFlags(Lcom/google/android/wearable/datatransfer/internal/BatteryPolicy$Decision;Ljava/util/List;Ljava/util/Set;Ljava/util/Set;)I
    .locals 3
    .param p1, "batteryPolicyDecision"    # Lcom/google/android/wearable/datatransfer/internal/BatteryPolicy$Decision;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/wearable/datatransfer/internal/BatteryPolicy$Decision;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 611
    .local p2, "schedulableRequests":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;>;"
    .local p3, "connectedNodes":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .local p4, "connectedNodesWithLowBattery":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const/4 v1, 0x0

    .line 613
    .local v1, "syncStateFlags":I
    iget-object v2, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->mClient:Lcom/google/android/wearable/datatransfer/WearableDataApiClient;

    invoke-virtual {v2}, Lcom/google/android/wearable/datatransfer/WearableDataApiClient;->getDataSyncDatabase()Lcom/google/android/wearable/datatransfer/internal/DataSyncDatabase;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/wearable/datatransfer/internal/DataSyncDatabase;->getQueueSize()I

    move-result v0

    .line 614
    .local v0, "queueSize":I
    if-nez v0, :cond_0

    .line 615
    or-int/lit8 v1, v1, 0x1

    .line 618
    :cond_0
    iget-boolean v2, p1, Lcom/google/android/wearable/datatransfer/internal/BatteryPolicy$Decision;->shouldSync:Z

    if-nez v2, :cond_1

    .line 619
    or-int/lit8 v1, v1, 0x2

    .line 622
    :cond_1
    const/4 v2, 0x0

    invoke-direct {p0, p2, p3, p4, v2}, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->computeSyncStateFlagsForRequests(Ljava/util/List;Ljava/util/Set;Ljava/util/Set;Z)I

    move-result v2

    or-int/2addr v1, v2

    .line 628
    return v1
.end method

.method private computeSyncStateFlagsForRequests(Ljava/util/List;Ljava/util/Set;Ljava/util/Set;Z)I
    .locals 7
    .param p4, "filterRequests"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;Z)I"
        }
    .end annotation

    .prologue
    .line 566
    .local p1, "requests":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;>;"
    .local p2, "connectedNodes":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .local p3, "connectedNodesWithLowBattery":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const/4 v5, 0x0

    .line 567
    .local v5, "syncStateFlags":I
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_5

    .line 569
    const/4 v0, 0x0

    .line 571
    .local v0, "anyRemoteNodeDisconnected":Z
    const/4 v1, 0x0

    .line 573
    .local v1, "anyRemoteNodeLowBattery":Z
    const/4 v2, 0x0

    .line 574
    .local v2, "anyRequestLeft":Z
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 575
    .local v4, "requestIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;>;"
    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 576
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;

    .line 577
    .local v3, "request":Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;
    iget-object v6, v3, Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;->remoteNodeId:Ljava/lang/String;

    invoke-interface {p2, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 578
    const/4 v0, 0x1

    .line 579
    if-eqz p4, :cond_0

    .line 580
    invoke-interface {v4}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 582
    :cond_1
    iget-object v6, v3, Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;->remoteNodeId:Ljava/lang/String;

    invoke-interface {p3, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 583
    const/4 v1, 0x1

    .line 584
    if-eqz p4, :cond_0

    .line 585
    invoke-interface {v4}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 589
    :cond_2
    const/4 v2, 0x1

    goto :goto_0

    .line 592
    .end local v3    # "request":Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;
    :cond_3
    if-nez v2, :cond_5

    .line 595
    if-eqz v0, :cond_4

    .line 596
    or-int/lit8 v5, v5, 0x4

    .line 598
    :cond_4
    if-eqz v1, :cond_5

    .line 599
    or-int/lit8 v5, v5, 0x8

    .line 603
    .end local v0    # "anyRemoteNodeDisconnected":Z
    .end local v1    # "anyRemoteNodeLowBattery":Z
    .end local v2    # "anyRequestLeft":Z
    .end local v4    # "requestIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;>;"
    :cond_5
    return v5
.end method

.method private static getDataSyncPrefs(Landroid/content/Context;)Landroid/content/SharedPreferences;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 704
    const-string v0, "DataSyncApiPrefs"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method

.method private getSchedulableRequests(J)Ljava/util/List;
    .locals 5
    .param p1, "currentTimeMillis"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;",
            ">;"
        }
    .end annotation

    .prologue
    .line 537
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->mClient:Lcom/google/android/wearable/datatransfer/WearableDataApiClient;

    invoke-virtual {v0}, Lcom/google/android/wearable/datatransfer/WearableDataApiClient;->getDataSyncDatabase()Lcom/google/android/wearable/datatransfer/internal/DataSyncDatabase;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->mRetryPolicy:Lcom/google/android/wearable/datatransfer/internal/RetryPolicy;

    iget v1, v1, Lcom/google/android/wearable/datatransfer/internal/RetryPolicy;->retryMaxDelayMillis:I

    mul-int/lit8 v1, v1, 0x2

    int-to-long v2, v1

    invoke-virtual {v0, p1, p2, v2, v3}, Lcom/google/android/wearable/datatransfer/internal/DataSyncDatabase;->getSchedulableRequests(JJ)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static getStartIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 184
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.wearable.datatransfer.UPDATE"

    const/4 v2, 0x0

    const-class v3, Lcom/google/android/wearable/datatransfer/DataSyncService;

    invoke-direct {v0, v1, v2, p0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    return-object v0
.end method

.method private getSyncStateFlagsPref()Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 679
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->mPreferences:Landroid/content/SharedPreferences;

    const-string v1, "sync_state_flags"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 681
    const/4 v0, 0x0

    .line 683
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->mPreferences:Landroid/content/SharedPreferences;

    const-string v1, "sync_state_flags"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method

.method public static isHandledDataItemPath(Ljava/lang/String;)Z
    .locals 1
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 335
    const-string v0, "/com.google.android.wearable.datatransfer.BATTERY_STATUS"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static isHandledMessagePath(Ljava/lang/String;)Z
    .locals 1
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 328
    const-string v0, "com.google.android.wearable.datatransfer.BATTERY_STATUS_POKE"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static isServiceRegistered(Landroid/content/Context;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 188
    const-class v0, Lcom/google/android/wearable/datatransfer/DataSyncService;

    invoke-static {p0, v0}, Lcom/google/android/wearable/datatransfer/internal/Utils;->isServiceRegistered(Landroid/content/Context;Ljava/lang/Class;)Z

    move-result v0

    return v0
.end method

.method private static logNodesNotAcceptingConnections(Ljava/util/Collection;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 692
    .local p0, "nodesNotAcceptingConnections":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    invoke-interface {p0}, Ljava/util/Collection;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 701
    :goto_0
    return-void

    .line 695
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 696
    .local v1, "logMessage":Ljava/lang/StringBuilder;
    const-string v3, "The following nodes are not accepting connections:\n"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 697
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 698
    .local v2, "nodeId":Ljava/lang/String;
    const-string v3, "    "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 700
    .end local v2    # "nodeId":Ljava/lang/String;
    :cond_1
    const-string v3, "DataSyncServiceHelper"

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static newInstance(Landroid/content/Context;Lcom/google/android/wearable/datatransfer/internal/RetryPolicy;)Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "retryPolicy"    # Lcom/google/android/wearable/datatransfer/internal/RetryPolicy;

    .prologue
    .line 192
    new-instance v0, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    invoke-direct {v0, p0}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;-><init>(Landroid/content/Context;)V

    sget-object v2, Lcom/google/android/gms/wearable/Wearable;->API:Lcom/google/android/gms/common/api/Api;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;->addApi(Lcom/google/android/gms/common/api/Api;)Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;->build()Lcom/google/android/gms/common/api/GoogleApiClient;

    move-result-object v1

    .line 195
    .local v1, "apiClient":Lcom/google/android/gms/common/api/GoogleApiClient;
    new-instance v5, Lcom/google/android/wearable/datatransfer/internal/ClockImpl;

    invoke-direct {v5}, Lcom/google/android/wearable/datatransfer/internal/ClockImpl;-><init>()V

    .line 196
    .local v5, "clock":Lcom/google/android/wearable/datatransfer/internal/Clock;
    new-instance v0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;

    new-instance v2, Lcom/google/android/wearable/datatransfer/WearableDataApiClient;

    invoke-direct {v2, p0}, Lcom/google/android/wearable/datatransfer/WearableDataApiClient;-><init>(Landroid/content/Context;)V

    new-instance v3, Lcom/google/android/wearable/datatransfer/internal/DefaultPeerProvider;

    invoke-direct {v3}, Lcom/google/android/wearable/datatransfer/internal/DefaultPeerProvider;-><init>()V

    new-instance v4, Lcom/google/android/wearable/datatransfer/internal/SharedBatteryStatusUpdaterImpl;

    invoke-direct {v4, p0, v5}, Lcom/google/android/wearable/datatransfer/internal/SharedBatteryStatusUpdaterImpl;-><init>(Landroid/content/Context;Lcom/google/android/wearable/datatransfer/internal/Clock;)V

    new-instance v6, Lcom/google/android/wearable/datatransfer/internal/BatteryPolicyImpl;

    invoke-direct {v6, p0}, Lcom/google/android/wearable/datatransfer/internal/BatteryPolicyImpl;-><init>(Landroid/content/Context;)V

    new-instance v8, Lcom/google/android/wearable/datatransfer/internal/DataSyncAlarmSchedulerImpl;

    invoke-direct {v8, p0}, Lcom/google/android/wearable/datatransfer/internal/DataSyncAlarmSchedulerImpl;-><init>(Landroid/content/Context;)V

    invoke-static {p0}, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->getDataSyncPrefs(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v9

    move-object v7, p1

    invoke-direct/range {v0 .. v9}, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;-><init>(Lcom/google/android/gms/common/api/GoogleApiClient;Lcom/google/android/wearable/datatransfer/WearableDataApiClient;Lcom/google/android/wearable/datatransfer/internal/PeerProvider;Lcom/google/android/wearable/datatransfer/internal/SharedBatteryStatusUpdater;Lcom/google/android/wearable/datatransfer/internal/Clock;Lcom/google/android/wearable/datatransfer/internal/BatteryPolicy;Lcom/google/android/wearable/datatransfer/internal/RetryPolicy;Lcom/google/android/wearable/datatransfer/internal/DataSyncAlarmScheduler;Landroid/content/SharedPreferences;)V

    return-object v0
.end method

.method private pokeNodesWithOldBatteryStatus(Ljava/util/Map;Ljava/util/Collection;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/wearable/datatransfer/internal/SharedBatteryStatus;",
            ">;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 665
    .local p1, "batteryStatuses":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/wearable/datatransfer/internal/SharedBatteryStatus;>;"
    .local p2, "nodesNotAcceptingConnections":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    invoke-static {p2}, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->logNodesNotAcceptingConnections(Ljava/util/Collection;)V

    .line 666
    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 667
    .local v2, "nodeId":Ljava/lang/String;
    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/wearable/datatransfer/internal/SharedBatteryStatus;

    .line 671
    .local v0, "batteryStatus":Lcom/google/android/wearable/datatransfer/internal/SharedBatteryStatus;
    iget-wide v4, v0, Lcom/google/android/wearable/datatransfer/internal/SharedBatteryStatus;->timestamp:J

    iget-object v3, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->mClock:Lcom/google/android/wearable/datatransfer/internal/Clock;

    invoke-interface {v3}, Lcom/google/android/wearable/datatransfer/internal/Clock;->getCurrentTimeMillis()J

    move-result-wide v6

    sget-wide v8, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->BATTERY_STATUS_EXPIRATION_TIME_MS:J

    sub-long/2addr v6, v8

    cmp-long v3, v4, v6

    if-gez v3, :cond_0

    .line 673
    iget-object v3, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->mSharedBatteryStatusUpdater:Lcom/google/android/wearable/datatransfer/internal/SharedBatteryStatusUpdater;

    iget-object v4, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->mApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-interface {v3, v4, v2}, Lcom/google/android/wearable/datatransfer/internal/SharedBatteryStatusUpdater;->sendBatteryStatusPoke(Lcom/google/android/gms/common/api/GoogleApiClient;Ljava/lang/String;)V

    goto :goto_0

    .line 676
    .end local v0    # "batteryStatus":Lcom/google/android/wearable/datatransfer/internal/SharedBatteryStatus;
    .end local v2    # "nodeId":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method private processIntent(Landroid/content/Intent;Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper$CompletionListener;)V
    .locals 38
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "listener"    # Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper$CompletionListener;

    .prologue
    .line 346
    const/16 v30, 0x2

    .line 357
    .local v30, "targetOngoingRequestsCount":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->mClock:Lcom/google/android/wearable/datatransfer/internal/Clock;

    move-object/from16 v31, v0

    invoke-interface/range {v31 .. v31}, Lcom/google/android/wearable/datatransfer/internal/Clock;->getCurrentTimeMillis()J

    move-result-wide v22

    .line 360
    .local v22, "now":J
    move-object/from16 v0, p0

    move-wide/from16 v1, v22

    invoke-direct {v0, v1, v2}, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->getSchedulableRequests(J)Ljava/util/List;

    move-result-object v26

    .line 362
    .local v26, "schedulableRequests":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;>;"
    invoke-interface/range {v26 .. v26}, Ljava/util/List;->isEmpty()Z

    move-result v31

    if-nez v31, :cond_1

    const/4 v6, 0x1

    .line 366
    .local v6, "anythingToSchedule":Z
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->mBatteryPolicy:Lcom/google/android/wearable/datatransfer/internal/BatteryPolicy;

    move-object/from16 v31, v0

    invoke-interface/range {v31 .. v31}, Lcom/google/android/wearable/datatransfer/internal/BatteryPolicy;->apply()Lcom/google/android/wearable/datatransfer/internal/BatteryPolicy$Decision;

    move-result-object v7

    .line 367
    .local v7, "batteryPolicyDecision":Lcom/google/android/wearable/datatransfer/internal/BatteryPolicy$Decision;
    iget-boolean v0, v7, Lcom/google/android/wearable/datatransfer/internal/BatteryPolicy$Decision;->shouldSync:Z

    move/from16 v31, v0

    if-eqz v31, :cond_2

    .line 369
    new-instance v27, Ljava/util/ArrayList;

    move-object/from16 v0, v27

    move-object/from16 v1, v26

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 378
    .local v27, "schedulableRequestsToSchedule":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;>;"
    :goto_1
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->mPeerProvider:Lcom/google/android/wearable/datatransfer/internal/PeerProvider;

    move-object/from16 v31, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->mApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    move-object/from16 v32, v0

    invoke-interface/range {v31 .. v32}, Lcom/google/android/wearable/datatransfer/internal/PeerProvider;->getConnectedPeers(Lcom/google/android/gms/common/api/GoogleApiClient;)Ljava/util/Set;
    :try_end_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v13

    .line 391
    .local v13, "connectedNodes":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->mSharedBatteryStatusUpdater:Lcom/google/android/wearable/datatransfer/internal/SharedBatteryStatusUpdater;

    move-object/from16 v31, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->mApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    move-object/from16 v32, v0

    invoke-interface/range {v31 .. v32}, Lcom/google/android/wearable/datatransfer/internal/SharedBatteryStatusUpdater;->getAllBatteryStatuses(Lcom/google/android/gms/common/api/GoogleApiClient;)Ljava/util/Map;
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v10

    .line 399
    .local v10, "batteryStatuses":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/wearable/datatransfer/internal/SharedBatteryStatus;>;"
    new-instance v14, Ljava/util/HashSet;

    invoke-direct {v14}, Ljava/util/HashSet;-><init>()V

    .line 400
    .local v14, "connectedNodesWithLowBattery":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface/range {v27 .. v27}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v16

    .local v16, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_2
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v31

    if-eqz v31, :cond_3

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;

    .line 401
    .local v24, "request":Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;
    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;->remoteNodeId:Ljava/lang/String;

    move-object/from16 v20, v0

    .line 402
    .local v20, "nodeId":Ljava/lang/String;
    move-object/from16 v0, v20

    invoke-interface {v10, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/google/android/wearable/datatransfer/internal/SharedBatteryStatus;

    .line 403
    .local v9, "batteryStatus":Lcom/google/android/wearable/datatransfer/internal/SharedBatteryStatus;
    move-object/from16 v0, v20

    invoke-interface {v13, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_0

    if-eqz v9, :cond_0

    iget-boolean v0, v9, Lcom/google/android/wearable/datatransfer/internal/SharedBatteryStatus;->batteryLow:Z

    move/from16 v31, v0

    if-eqz v31, :cond_0

    .line 405
    move-object/from16 v0, v20

    invoke-interface {v14, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 362
    .end local v6    # "anythingToSchedule":Z
    .end local v7    # "batteryPolicyDecision":Lcom/google/android/wearable/datatransfer/internal/BatteryPolicy$Decision;
    .end local v9    # "batteryStatus":Lcom/google/android/wearable/datatransfer/internal/SharedBatteryStatus;
    .end local v10    # "batteryStatuses":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/wearable/datatransfer/internal/SharedBatteryStatus;>;"
    .end local v13    # "connectedNodes":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v14    # "connectedNodesWithLowBattery":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v16    # "i$":Ljava/util/Iterator;
    .end local v20    # "nodeId":Ljava/lang/String;
    .end local v24    # "request":Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;
    .end local v27    # "schedulableRequestsToSchedule":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;>;"
    :cond_1
    const/4 v6, 0x0

    goto :goto_0

    .line 371
    .restart local v6    # "anythingToSchedule":Z
    .restart local v7    # "batteryPolicyDecision":Lcom/google/android/wearable/datatransfer/internal/BatteryPolicy$Decision;
    :cond_2
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v27

    .restart local v27    # "schedulableRequestsToSchedule":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;>;"
    goto :goto_1

    .line 379
    :catch_0
    move-exception v15

    .line 380
    .local v15, "e":Ljava/util/concurrent/TimeoutException;
    const-string v31, "DataSyncServiceHelper"

    const-string v32, "Timed out while getting connected peers"

    invoke-static/range {v31 .. v32}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 498
    .end local v15    # "e":Ljava/util/concurrent/TimeoutException;
    :goto_3
    return-void

    .line 382
    :catch_1
    move-exception v15

    .line 383
    .local v15, "e":Ljava/lang/InterruptedException;
    const-string v31, "DataSyncServiceHelper"

    const-string v32, "Interrupted while waiting for connected peers"

    invoke-static/range {v31 .. v32}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 384
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/Thread;->interrupt()V

    goto :goto_3

    .line 392
    .end local v15    # "e":Ljava/lang/InterruptedException;
    .restart local v13    # "connectedNodes":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :catch_2
    move-exception v15

    .line 393
    .restart local v15    # "e":Ljava/lang/InterruptedException;
    const-string v31, "DataSyncServiceHelper"

    const-string v32, "Interrupted while waiting for battery statuses"

    invoke-static/range {v31 .. v32}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 394
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/Thread;->interrupt()V

    goto :goto_3

    .line 410
    .end local v15    # "e":Ljava/lang/InterruptedException;
    .restart local v10    # "batteryStatuses":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/wearable/datatransfer/internal/SharedBatteryStatus;>;"
    .restart local v14    # "connectedNodesWithLowBattery":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .restart local v16    # "i$":Ljava/util/Iterator;
    :cond_3
    const/16 v31, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v27

    move/from16 v2, v31

    invoke-direct {v0, v1, v13, v14, v2}, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->computeSyncStateFlagsForRequests(Ljava/util/List;Ljava/util/Set;Ljava/util/Set;Z)I

    .line 416
    new-instance v21, Ljava/util/LinkedList;

    invoke-direct/range {v21 .. v21}, Ljava/util/LinkedList;-><init>()V

    .line 417
    .local v21, "ongoingFileRequests":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/io/File;>;"
    invoke-interface/range {v27 .. v27}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v16

    :cond_4
    :goto_4
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v31

    if-eqz v31, :cond_5

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;

    .line 418
    .restart local v24    # "request":Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->mDataSyncers:Ljava/util/concurrent/ConcurrentMap;

    move-object/from16 v31, v0

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;->targetFile:Ljava/io/File;

    move-object/from16 v32, v0

    invoke-interface/range {v31 .. v32}, Ljava/util/concurrent/ConcurrentMap;->containsKey(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_4

    .line 419
    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;->targetFile:Ljava/io/File;

    move-object/from16 v31, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 423
    .end local v24    # "request":Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;
    :cond_5
    invoke-virtual/range {v21 .. v21}, Ljava/util/LinkedList;->size()I

    move-result v31

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->mDataSyncers:Ljava/util/concurrent/ConcurrentMap;

    move-object/from16 v32, v0

    invoke-interface/range {v32 .. v32}, Ljava/util/concurrent/ConcurrentMap;->size()I

    move-result v32

    move/from16 v0, v31

    move/from16 v1, v32

    if-eq v0, v1, :cond_7

    .line 425
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->mDataSyncers:Ljava/util/concurrent/ConcurrentMap;

    move-object/from16 v31, v0

    invoke-interface/range {v31 .. v31}, Ljava/util/concurrent/ConcurrentMap;->keySet()Ljava/util/Set;

    move-result-object v31

    invoke-interface/range {v31 .. v31}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v16

    :cond_6
    :goto_5
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v31

    if-eqz v31, :cond_7

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Ljava/io/File;

    .line 426
    .local v29, "targetFile":Ljava/io/File;
    move-object/from16 v0, v21

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    move-result v31

    if-nez v31, :cond_6

    .line 427
    move-object/from16 v0, p0

    move-object/from16 v1, v29

    invoke-direct {v0, v1}, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->stopDataSyncer(Ljava/io/File;)V

    goto :goto_5

    .line 432
    .end local v29    # "targetFile":Ljava/io/File;
    :cond_7
    :goto_6
    invoke-virtual/range {v21 .. v21}, Ljava/util/LinkedList;->size()I

    move-result v31

    move/from16 v0, v31

    move/from16 v1, v30

    if-le v0, v1, :cond_8

    .line 433
    invoke-virtual/range {v21 .. v21}, Ljava/util/LinkedList;->pollLast()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/io/File;

    .line 434
    .local v17, "lastRequest":Ljava/io/File;
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->stopDataSyncer(Ljava/io/File;)V

    goto :goto_6

    .line 438
    .end local v17    # "lastRequest":Ljava/io/File;
    :cond_8
    const/16 v25, 0x0

    .line 440
    .local v25, "retryTime":Ljava/lang/Long;
    :try_start_2
    iget-boolean v0, v7, Lcom/google/android/wearable/datatransfer/internal/BatteryPolicy$Decision;->shouldSync:Z

    move/from16 v31, v0

    if-eqz v31, :cond_d

    .line 442
    invoke-interface/range {v27 .. v27}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v19

    .line 444
    .local v19, "nextToStartIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;>;"
    :cond_9
    :goto_7
    invoke-virtual/range {v21 .. v21}, Ljava/util/LinkedList;->size()I

    move-result v31

    move/from16 v0, v31

    move/from16 v1, v30

    if-ge v0, v1, :cond_c

    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v31

    if-eqz v31, :cond_c

    .line 445
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;

    .line 446
    .restart local v24    # "request":Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;
    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;->targetFile:Ljava/io/File;

    move-object/from16 v31, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    move-result v31

    if-nez v31, :cond_9

    .line 450
    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-direct {v0, v1}, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->startDataSyncer(Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;)V

    .line 451
    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;->targetFile:Ljava/io/File;

    move-object/from16 v31, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_7

    .line 481
    .end local v19    # "nextToStartIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;>;"
    .end local v24    # "request":Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;
    :catchall_0
    move-exception v31

    move-object/from16 v32, v31

    const-string v31, "com.google.android.wearable.datatransfer.BATTERY_UPDATE"

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 482
    .local v11, "batteryUpdate":Ljava/lang/String;
    const-string v31, "com.google.android.wearable.datatransfer.MESSAGE_PATH"

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 483
    .local v18, "messagePath":Ljava/lang/String;
    if-nez v11, :cond_a

    const-string v31, "com.google.android.wearable.datatransfer.BATTERY_STATUS_POKE"

    move-object/from16 v0, v31

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_b

    .line 484
    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->mSharedBatteryStatusUpdater:Lcom/google/android/wearable/datatransfer/internal/SharedBatteryStatusUpdater;

    move-object/from16 v31, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->mApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    move-object/from16 v33, v0

    move-object/from16 v0, v31

    move-object/from16 v1, v33

    invoke-interface {v0, v1, v11}, Lcom/google/android/wearable/datatransfer/internal/SharedBatteryStatusUpdater;->publishBatteryStatus(Lcom/google/android/gms/common/api/GoogleApiClient;Ljava/lang/String;)V

    .line 487
    :cond_b
    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-direct {v0, v7, v1, v13, v14}, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->computeSyncStateFlags(Lcom/google/android/wearable/datatransfer/internal/BatteryPolicy$Decision;Ljava/util/List;Ljava/util/Set;Ljava/util/Set;)I

    move-result v28

    .line 492
    .local v28, "syncStateFlags":I
    move-object/from16 v0, p0

    move/from16 v1, v28

    invoke-direct {v0, v1}, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->updateSyncStateFlags(I)V

    .line 496
    invoke-virtual/range {v21 .. v21}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v31

    if-nez v31, :cond_13

    const/16 v31, 0x1

    :goto_8
    move-object/from16 v0, p2

    move/from16 v1, v31

    invoke-interface {v0, v1}, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper$CompletionListener;->onIntentProcessed(Z)V

    .line 497
    throw v32

    .line 454
    .end local v11    # "batteryUpdate":Ljava/lang/String;
    .end local v18    # "messagePath":Ljava/lang/String;
    .end local v28    # "syncStateFlags":I
    .restart local v19    # "nextToStartIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;>;"
    :cond_c
    :try_start_3
    move-object/from16 v0, p0

    invoke-direct {v0, v10, v14}, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->pokeNodesWithOldBatteryStatus(Ljava/util/Map;Ljava/util/Collection;)V

    .line 456
    invoke-virtual/range {v21 .. v21}, Ljava/util/LinkedList;->size()I

    move-result v31

    move/from16 v0, v31

    move/from16 v1, v30

    if-ge v0, v1, :cond_d

    .line 460
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->mClient:Lcom/google/android/wearable/datatransfer/WearableDataApiClient;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Lcom/google/android/wearable/datatransfer/WearableDataApiClient;->getDataSyncDatabase()Lcom/google/android/wearable/datatransfer/internal/DataSyncDatabase;

    move-result-object v31

    move-object/from16 v0, v31

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2, v13}, Lcom/google/android/wearable/datatransfer/internal/DataSyncDatabase;->getNextRetryTimestamp(JLjava/util/Collection;)Ljava/lang/Long;

    move-result-object v25

    .line 466
    .end local v19    # "nextToStartIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;>;"
    :cond_d
    if-eqz v6, :cond_10

    iget-object v0, v7, Lcom/google/android/wearable/datatransfer/internal/BatteryPolicy$Decision;->pollDelayMillis:Ljava/lang/Long;

    move-object/from16 v31, v0

    if-eqz v31, :cond_10

    iget-object v0, v7, Lcom/google/android/wearable/datatransfer/internal/BatteryPolicy$Decision;->pollDelayMillis:Ljava/lang/Long;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Ljava/lang/Long;->longValue()J

    move-result-wide v32

    add-long v32, v32, v22

    invoke-static/range {v32 .. v33}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    .line 470
    .local v8, "batteryPollTime":Ljava/lang/Long;
    :goto_9
    move-object/from16 v0, v25

    invoke-static {v0, v8}, Lcom/google/android/wearable/datatransfer/internal/Utils;->minOrNull(Ljava/lang/Long;Ljava/lang/Long;)Ljava/lang/Long;

    move-result-object v12

    .line 471
    .local v12, "candidateAlarmTime":Ljava/lang/Long;
    if-eqz v12, :cond_11

    .line 473
    invoke-virtual {v12}, Ljava/lang/Long;->longValue()J

    move-result-wide v32

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->mClock:Lcom/google/android/wearable/datatransfer/internal/Clock;

    move-object/from16 v31, v0

    invoke-interface/range {v31 .. v31}, Lcom/google/android/wearable/datatransfer/internal/Clock;->getCurrentTimeMillis()J

    move-result-wide v34

    sget-wide v36, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->MIN_DELAY_BEFORE_ALARM:J

    add-long v34, v34, v36

    invoke-static/range {v32 .. v35}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    .line 476
    .local v4, "alarmTime":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->mAlarmScheduler:Lcom/google/android/wearable/datatransfer/internal/DataSyncAlarmScheduler;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    invoke-interface {v0, v4, v5}, Lcom/google/android/wearable/datatransfer/internal/DataSyncAlarmScheduler;->setAlarm(J)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 481
    .end local v4    # "alarmTime":J
    :goto_a
    const-string v31, "com.google.android.wearable.datatransfer.BATTERY_UPDATE"

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 482
    .restart local v11    # "batteryUpdate":Ljava/lang/String;
    const-string v31, "com.google.android.wearable.datatransfer.MESSAGE_PATH"

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 483
    .restart local v18    # "messagePath":Ljava/lang/String;
    if-nez v11, :cond_e

    const-string v31, "com.google.android.wearable.datatransfer.BATTERY_STATUS_POKE"

    move-object/from16 v0, v31

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_f

    .line 484
    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->mSharedBatteryStatusUpdater:Lcom/google/android/wearable/datatransfer/internal/SharedBatteryStatusUpdater;

    move-object/from16 v31, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->mApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    move-object/from16 v32, v0

    move-object/from16 v0, v31

    move-object/from16 v1, v32

    invoke-interface {v0, v1, v11}, Lcom/google/android/wearable/datatransfer/internal/SharedBatteryStatusUpdater;->publishBatteryStatus(Lcom/google/android/gms/common/api/GoogleApiClient;Ljava/lang/String;)V

    .line 487
    :cond_f
    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-direct {v0, v7, v1, v13, v14}, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->computeSyncStateFlags(Lcom/google/android/wearable/datatransfer/internal/BatteryPolicy$Decision;Ljava/util/List;Ljava/util/Set;Ljava/util/Set;)I

    move-result v28

    .line 492
    .restart local v28    # "syncStateFlags":I
    move-object/from16 v0, p0

    move/from16 v1, v28

    invoke-direct {v0, v1}, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->updateSyncStateFlags(I)V

    .line 496
    invoke-virtual/range {v21 .. v21}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v31

    if-nez v31, :cond_12

    const/16 v31, 0x1

    :goto_b
    move-object/from16 v0, p2

    move/from16 v1, v31

    invoke-interface {v0, v1}, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper$CompletionListener;->onIntentProcessed(Z)V

    goto/16 :goto_3

    .line 466
    .end local v8    # "batteryPollTime":Ljava/lang/Long;
    .end local v11    # "batteryUpdate":Ljava/lang/String;
    .end local v12    # "candidateAlarmTime":Ljava/lang/Long;
    .end local v18    # "messagePath":Ljava/lang/String;
    .end local v28    # "syncStateFlags":I
    :cond_10
    const/4 v8, 0x0

    goto :goto_9

    .line 478
    .restart local v8    # "batteryPollTime":Ljava/lang/Long;
    .restart local v12    # "candidateAlarmTime":Ljava/lang/Long;
    :cond_11
    :try_start_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->mAlarmScheduler:Lcom/google/android/wearable/datatransfer/internal/DataSyncAlarmScheduler;

    move-object/from16 v31, v0

    invoke-interface/range {v31 .. v31}, Lcom/google/android/wearable/datatransfer/internal/DataSyncAlarmScheduler;->removeAlarm()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_a

    .line 496
    .restart local v11    # "batteryUpdate":Ljava/lang/String;
    .restart local v18    # "messagePath":Ljava/lang/String;
    .restart local v28    # "syncStateFlags":I
    :cond_12
    const/16 v31, 0x0

    goto :goto_b

    .end local v8    # "batteryPollTime":Ljava/lang/Long;
    .end local v12    # "candidateAlarmTime":Ljava/lang/Long;
    :cond_13
    const/16 v31, 0x0

    goto/16 :goto_8
.end method

.method private setSyncStateFlagsPref(I)V
    .locals 2
    .param p1, "syncStateFlags"    # I

    .prologue
    .line 687
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->mPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "sync_state_flags"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 688
    return-void
.end method

.method private startDataSyncer(Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;)V
    .locals 8
    .param p1, "dataSyncRequest"    # Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;

    .prologue
    .line 632
    const-string v1, "DataSyncServiceHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "starting syncing of "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;->targetFile:Ljava/io/File;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 633
    new-instance v7, Lcom/google/android/wearable/datatransfer/internal/ServiceDataSyncListener;

    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->mClient:Lcom/google/android/wearable/datatransfer/WearableDataApiClient;

    invoke-direct {v7, v1}, Lcom/google/android/wearable/datatransfer/internal/ServiceDataSyncListener;-><init>(Lcom/google/android/wearable/datatransfer/WearableDataApiClient;)V

    .line 634
    .local v7, "listener":Lcom/google/android/wearable/datatransfer/internal/DataSyncListener;
    new-instance v0, Lcom/google/android/wearable/datatransfer/internal/DataSyncer;

    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->mApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    iget-object v2, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->mClient:Lcom/google/android/wearable/datatransfer/WearableDataApiClient;

    iget-object v4, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->mPeerProvider:Lcom/google/android/wearable/datatransfer/internal/PeerProvider;

    iget-object v5, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->mRetryPolicy:Lcom/google/android/wearable/datatransfer/internal/RetryPolicy;

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/wearable/datatransfer/internal/DataSyncer;-><init>(Lcom/google/android/gms/common/api/GoogleApiClient;Lcom/google/android/wearable/datatransfer/WearableDataApiClient;Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;Lcom/google/android/wearable/datatransfer/internal/PeerProvider;Lcom/google/android/wearable/datatransfer/internal/RetryPolicy;)V

    .line 636
    .local v0, "dataSyncer":Lcom/google/android/wearable/datatransfer/internal/DataSyncer;
    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->mDataSyncers:Ljava/util/concurrent/ConcurrentMap;

    iget-object v2, p1, Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;->targetFile:Ljava/io/File;

    invoke-interface {v1, v2, v0}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 637
    const-string v1, "DataSyncServiceHelper"

    const-string v2, "operation should not be already be started"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 643
    :goto_0
    return-void

    .line 640
    :cond_0
    new-instance v6, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper$CallbackFutureTask;

    invoke-direct {v6, p0, v0, p1, v7}, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper$CallbackFutureTask;-><init>(Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;Lcom/google/android/wearable/datatransfer/internal/DataSyncer;Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;Lcom/google/android/wearable/datatransfer/internal/DataSyncListener;)V

    .line 642
    .local v6, "futureTask":Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper$CallbackFutureTask;
    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->mDataSyncersThreadPool:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v1, v6}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public static startService(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 133
    invoke-static {p0}, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->getStartIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    move-result-object v0

    if-nez v0, :cond_0

    .line 136
    const-string v0, "DataSyncServiceHelper"

    const-string v1, "Failed to start DataSyncService, which is needed for DataSyncApi"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 138
    :cond_0
    return-void
.end method

.method public static startWithBatteryUpdate(Landroid/content/Context;Ljava/lang/String;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "batteryUpdateAction"    # Ljava/lang/String;

    .prologue
    .line 164
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 165
    .local v0, "extras":Landroid/os/Bundle;
    const-string v2, "com.google.android.wearable.datatransfer.BATTERY_UPDATE"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    invoke-static {p0}, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->getStartIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v1

    .line 168
    .local v1, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    move-result-object v2

    if-nez v2, :cond_0

    .line 171
    const-string v2, "DataSyncServiceHelper"

    const-string v3, "Failed to start DataSyncService, which is needed for DataSyncApi"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 173
    :cond_0
    return-void
.end method

.method public static startWithMessage(Landroid/content/Context;Lcom/google/android/gms/wearable/MessageEvent;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "message"    # Lcom/google/android/gms/wearable/MessageEvent;

    .prologue
    .line 146
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 147
    .local v0, "extras":Landroid/os/Bundle;
    const-string v2, "com.google.android.wearable.datatransfer.MESSAGE_PATH"

    invoke-interface {p1}, Lcom/google/android/gms/wearable/MessageEvent;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    invoke-static {p0}, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->getStartIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v1

    .line 150
    .local v1, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    move-result-object v2

    if-nez v2, :cond_0

    .line 153
    const-string v2, "DataSyncServiceHelper"

    const-string v3, "Failed to start DataSyncService, which is needed for DataSyncApi"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 155
    :cond_0
    return-void
.end method

.method private stopDataSyncer(Ljava/io/File;)V
    .locals 4
    .param p1, "localTargetFile"    # Ljava/io/File;

    .prologue
    .line 646
    const-string v1, "DataSyncServiceHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "stopping syncing of "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 647
    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->mDataSyncers:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v1, p1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/wearable/datatransfer/internal/DataSyncer;

    .line 648
    .local v0, "dataSyncer":Lcom/google/android/wearable/datatransfer/internal/DataSyncer;
    if-nez v0, :cond_0

    .line 650
    const-string v1, "DataSyncServiceHelper"

    const-string v2, "attempted to stop transfer that is not ongoing"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 655
    :goto_0
    return-void

    .line 653
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/wearable/datatransfer/internal/DataSyncer;->cancel()V

    goto :goto_0
.end method

.method private updateSyncStateFlags(I)V
    .locals 5
    .param p1, "syncStateFlags"    # I

    .prologue
    .line 504
    const-string v2, "DataSyncServiceHelper"

    invoke-static {v2}, Lcom/google/android/wearable/datatransfer/internal/DebugLog;->isLoggable(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 505
    const-string v2, "DataSyncServiceHelper"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Updating syncStateFlags to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 508
    :cond_0
    iget-object v3, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->mLock:Ljava/lang/Object;

    monitor-enter v3

    .line 509
    :try_start_0
    iget-object v2, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->mSyncStateFlags:Ljava/lang/Integer;

    if-nez v2, :cond_1

    .line 512
    invoke-direct {p0}, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->getSyncStateFlagsPref()Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->mSyncStateFlags:Ljava/lang/Integer;

    .line 514
    :cond_1
    iget-object v2, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->mSyncStateFlags:Ljava/lang/Integer;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->mSyncStateFlags:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-eq v2, p1, :cond_5

    :cond_2
    const/4 v1, 0x1

    .line 516
    .local v1, "syncStateFlagsChanged":Z
    :goto_0
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->mSyncStateFlags:Ljava/lang/Integer;

    .line 517
    if-eqz v1, :cond_3

    .line 518
    invoke-direct {p0, p1}, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->setSyncStateFlagsPref(I)V

    .line 520
    :cond_3
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 521
    if-eqz v1, :cond_4

    .line 522
    new-instance v0, Lcom/google/android/wearable/datatransfer/internal/ServiceDataSyncListener;

    iget-object v2, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->mClient:Lcom/google/android/wearable/datatransfer/WearableDataApiClient;

    invoke-direct {v0, v2}, Lcom/google/android/wearable/datatransfer/internal/ServiceDataSyncListener;-><init>(Lcom/google/android/wearable/datatransfer/WearableDataApiClient;)V

    .line 523
    .local v0, "serviceListener":Lcom/google/android/wearable/datatransfer/internal/ServiceDataSyncListener;
    invoke-static {p1}, Lcom/google/android/wearable/datatransfer/DataSyncApi$SyncStateFlags;->isSyncing(I)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 524
    invoke-virtual {v0}, Lcom/google/android/wearable/datatransfer/internal/ServiceDataSyncListener;->onSyncingStarted()V

    .line 529
    .end local v0    # "serviceListener":Lcom/google/android/wearable/datatransfer/internal/ServiceDataSyncListener;
    :cond_4
    :goto_1
    return-void

    .line 514
    .end local v1    # "syncStateFlagsChanged":Z
    :cond_5
    const/4 v1, 0x0

    goto :goto_0

    .line 520
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    .line 526
    .restart local v0    # "serviceListener":Lcom/google/android/wearable/datatransfer/internal/ServiceDataSyncListener;
    .restart local v1    # "syncStateFlagsChanged":Z
    :cond_6
    invoke-virtual {v0, p1}, Lcom/google/android/wearable/datatransfer/internal/ServiceDataSyncListener;->onSyncingStopped(I)V

    goto :goto_1
.end method


# virtual methods
.method public onIntentReceived(Landroid/content/Intent;Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper$CompletionListener;)V
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "listener"    # Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper$CompletionListener;

    .prologue
    .line 266
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->mProcessingExecutor:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper$IntentProcessor;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper$IntentProcessor;-><init>(Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;Landroid/content/Intent;Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper$CompletionListener;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 267
    return-void
.end method

.method public onStopped()V
    .locals 1

    .prologue
    .line 270
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->mClient:Lcom/google/android/wearable/datatransfer/WearableDataApiClient;

    invoke-virtual {v0}, Lcom/google/android/wearable/datatransfer/WearableDataApiClient;->disconnect()V

    .line 271
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->mApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/GoogleApiClient;->disconnect()V

    .line 272
    return-void
.end method
