.class interface abstract Lcom/google/android/wearable/datatransfer/internal/DataSyncListener;
.super Ljava/lang/Object;
.source "DataSyncListener.java"


# virtual methods
.method public abstract onDataSyncCompleted(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)V
.end method

.method public abstract onDataSyncFailed(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;II)V
.end method

.method public abstract onSyncingStarted()V
.end method

.method public abstract onSyncingStopped(I)V
.end method
