.class public final Lcom/google/android/wearable/datatransfer/ClientOnlyConnectionService;
.super Lcom/google/android/wearable/datatransfer/ConnectionService;
.source "ClientOnlyConnectionService.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/google/android/wearable/datatransfer/ConnectionService;-><init>()V

    return-void
.end method


# virtual methods
.method public openFileDescriptor(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/wearable/datatransfer/ConnectionService$OpenFileDescriptorResult;
    .locals 1
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "sourceNodeId"    # Ljava/lang/String;

    .prologue
    .line 14
    invoke-static {}, Lcom/google/android/wearable/datatransfer/ConnectionService$OpenFileDescriptorResult;->forClientOnlyConnectionService()Lcom/google/android/wearable/datatransfer/ConnectionService$OpenFileDescriptorResult;

    move-result-object v0

    return-object v0
.end method
