.class final Lcom/google/android/wearable/datatransfer/internal/DataSender;
.super Ljava/lang/Object;
.source "DataSender.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/wearable/datatransfer/internal/DataSender$1;,
        Lcom/google/android/wearable/datatransfer/internal/DataSender$MutableState;,
        Lcom/google/android/wearable/datatransfer/internal/DataSender$Callbacks;
    }
.end annotation


# instance fields
.field private final mDataHandler:Lcom/google/android/wearable/datatransfer/internal/DataHandler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/wearable/datatransfer/internal/DataHandler",
            "<",
            "Lcom/google/android/wearable/datatransfer/internal/DataTransferAckPacket;",
            ">;"
        }
    .end annotation
.end field

.field private final mInitialClientConnectionId:J

.field private final mInputStream:Ljava/io/InputStream;

.field private final mMutableState:Lcom/google/android/wearable/datatransfer/internal/DataSender$MutableState;

.field private final mOffset:J

.field private final mServerConnectionId:J

.field private final mTransferSize:J


# direct methods
.method private constructor <init>(Lcom/google/android/gms/common/api/GoogleApiClient;Ljava/lang/String;Ljava/io/InputStream;JJJJ)V
    .locals 2
    .param p1, "client"    # Lcom/google/android/gms/common/api/GoogleApiClient;
    .param p2, "targetNodeId"    # Ljava/lang/String;
    .param p3, "inputStream"    # Ljava/io/InputStream;
    .param p4, "offset"    # J
    .param p6, "transferSize"    # J
    .param p8, "serverConnectionId"    # J
    .param p10, "initialClientConnectionId"    # J

    .prologue
    .line 134
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 135
    new-instance v0, Lcom/google/android/wearable/datatransfer/internal/DataHandler;

    invoke-direct {v0, p1, p2}, Lcom/google/android/wearable/datatransfer/internal/DataHandler;-><init>(Lcom/google/android/gms/common/api/GoogleApiClient;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataSender;->mDataHandler:Lcom/google/android/wearable/datatransfer/internal/DataHandler;

    .line 136
    const-string v0, "inputStream"

    invoke-static {p3, v0}, Lcom/google/android/wearable/datatransfer/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/InputStream;

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataSender;->mInputStream:Ljava/io/InputStream;

    .line 137
    iput-wide p4, p0, Lcom/google/android/wearable/datatransfer/internal/DataSender;->mOffset:J

    .line 138
    iput-wide p6, p0, Lcom/google/android/wearable/datatransfer/internal/DataSender;->mTransferSize:J

    .line 139
    iput-wide p8, p0, Lcom/google/android/wearable/datatransfer/internal/DataSender;->mServerConnectionId:J

    .line 140
    iput-wide p10, p0, Lcom/google/android/wearable/datatransfer/internal/DataSender;->mInitialClientConnectionId:J

    .line 141
    new-instance v0, Lcom/google/android/wearable/datatransfer/internal/DataSender$MutableState;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/wearable/datatransfer/internal/DataSender$MutableState;-><init>(Lcom/google/android/wearable/datatransfer/internal/DataSender$1;)V

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataSender;->mMutableState:Lcom/google/android/wearable/datatransfer/internal/DataSender$MutableState;

    .line 142
    return-void
.end method

.method private fastForward()Z
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 235
    iget-wide v8, p0, Lcom/google/android/wearable/datatransfer/internal/DataSender;->mTransferSize:J

    const-wide/16 v10, -0x1

    cmp-long v7, v8, v10

    if-eqz v7, :cond_1

    iget-wide v8, p0, Lcom/google/android/wearable/datatransfer/internal/DataSender;->mOffset:J

    iget-wide v10, p0, Lcom/google/android/wearable/datatransfer/internal/DataSender;->mTransferSize:J

    cmp-long v7, v8, v10

    if-ltz v7, :cond_1

    .line 251
    :cond_0
    :goto_0
    return v6

    .line 239
    :cond_1
    iget-object v7, p0, Lcom/google/android/wearable/datatransfer/internal/DataSender;->mInputStream:Ljava/io/InputStream;

    iget-wide v8, p0, Lcom/google/android/wearable/datatransfer/internal/DataSender;->mOffset:J

    invoke-virtual {v7, v8, v9}, Ljava/io/InputStream;->skip(J)J

    move-result-wide v4

    .line 240
    .local v4, "skipped":J
    iget-wide v8, p0, Lcom/google/android/wearable/datatransfer/internal/DataSender;->mOffset:J

    sub-long v2, v8, v4

    .line 242
    .local v2, "bytesToSkip":J
    :goto_1
    const-wide/16 v8, 0x0

    cmp-long v7, v2, v8

    if-lez v7, :cond_2

    .line 243
    iget-object v7, p0, Lcom/google/android/wearable/datatransfer/internal/DataSender;->mMutableState:Lcom/google/android/wearable/datatransfer/internal/DataSender$MutableState;

    iget-object v7, v7, Lcom/google/android/wearable/datatransfer/internal/DataSender$MutableState;->buffer:[B

    array-length v7, v7

    int-to-long v8, v7

    invoke-static {v2, v3, v8, v9}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v8

    long-to-int v1, v8

    .line 244
    .local v1, "len":I
    iget-object v7, p0, Lcom/google/android/wearable/datatransfer/internal/DataSender;->mInputStream:Ljava/io/InputStream;

    iget-object v8, p0, Lcom/google/android/wearable/datatransfer/internal/DataSender;->mMutableState:Lcom/google/android/wearable/datatransfer/internal/DataSender$MutableState;

    iget-object v8, v8, Lcom/google/android/wearable/datatransfer/internal/DataSender$MutableState;->buffer:[B

    invoke-virtual {v7, v8, v6, v1}, Ljava/io/InputStream;->read([BII)I

    move-result v0

    .line 245
    .local v0, "bytesDiscarded":I
    if-lez v0, :cond_0

    .line 248
    int-to-long v8, v0

    sub-long/2addr v2, v8

    .line 249
    goto :goto_1

    .line 251
    .end local v0    # "bytesDiscarded":I
    .end local v1    # "len":I
    :cond_2
    const/4 v6, 0x1

    goto :goto_0
.end method

.method public static forOpenConnectionRequest(Lcom/google/android/gms/common/api/GoogleApiClient;Lcom/google/android/gms/wearable/MessageEvent;Landroid/os/ParcelFileDescriptor;JJJJ)Lcom/google/android/wearable/datatransfer/internal/DataSender;
    .locals 13
    .param p0, "client"    # Lcom/google/android/gms/common/api/GoogleApiClient;
    .param p1, "messageEvent"    # Lcom/google/android/gms/wearable/MessageEvent;
    .param p2, "fileDescriptor"    # Landroid/os/ParcelFileDescriptor;
    .param p3, "offset"    # J
    .param p5, "transferSize"    # J
    .param p7, "serverConnectionId"    # J
    .param p9, "clientConnectionId"    # J

    .prologue
    .line 87
    new-instance v0, Lcom/google/android/wearable/datatransfer/internal/DataSender;

    invoke-interface {p1}, Lcom/google/android/gms/wearable/MessageEvent;->getSourceNodeId()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/io/FileInputStream;

    invoke-virtual {p2}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v1

    invoke-direct {v3, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/FileDescriptor;)V

    move-object v1, p0

    move-wide/from16 v4, p3

    move-wide/from16 v6, p5

    move-wide/from16 v8, p7

    move-wide/from16 v10, p9

    invoke-direct/range {v0 .. v11}, Lcom/google/android/wearable/datatransfer/internal/DataSender;-><init>(Lcom/google/android/gms/common/api/GoogleApiClient;Ljava/lang/String;Ljava/io/InputStream;JJJJ)V

    return-object v0
.end method

.method private getNextSegmentPacket(Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;J)V
    .locals 10
    .param p1, "destinationPacket"    # Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;
    .param p2, "clientConnectionId"    # J

    .prologue
    .line 279
    iget-wide v4, p0, Lcom/google/android/wearable/datatransfer/internal/DataSender;->mServerConnectionId:J

    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataSender;->mMutableState:Lcom/google/android/wearable/datatransfer/internal/DataSender$MutableState;

    iget-boolean v6, v0, Lcom/google/android/wearable/datatransfer/internal/DataSender$MutableState;->finalPacket:Z

    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataSender;->mMutableState:Lcom/google/android/wearable/datatransfer/internal/DataSender$MutableState;

    iget-object v7, v0, Lcom/google/android/wearable/datatransfer/internal/DataSender$MutableState;->buffer:[B

    const/4 v8, 0x0

    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataSender;->mMutableState:Lcom/google/android/wearable/datatransfer/internal/DataSender$MutableState;

    iget v9, v0, Lcom/google/android/wearable/datatransfer/internal/DataSender$MutableState;->bytesRead:I

    move-object v1, p1

    move-wide v2, p2

    invoke-virtual/range {v1 .. v9}, Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;->mutateToDataPacket(JJZ[BII)V

    .line 286
    return-void
.end method

.method private loadNextSegment()Z
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 260
    iget-object v3, p0, Lcom/google/android/wearable/datatransfer/internal/DataSender;->mMutableState:Lcom/google/android/wearable/datatransfer/internal/DataSender$MutableState;

    iget-boolean v3, v3, Lcom/google/android/wearable/datatransfer/internal/DataSender$MutableState;->finalPacket:Z

    if-eqz v3, :cond_0

    .line 268
    :goto_0
    return v1

    .line 264
    :cond_0
    iget-object v3, p0, Lcom/google/android/wearable/datatransfer/internal/DataSender;->mInputStream:Ljava/io/InputStream;

    iget-object v4, p0, Lcom/google/android/wearable/datatransfer/internal/DataSender;->mMutableState:Lcom/google/android/wearable/datatransfer/internal/DataSender$MutableState;

    iget-object v4, v4, Lcom/google/android/wearable/datatransfer/internal/DataSender$MutableState;->buffer:[B

    invoke-virtual {v3, v4}, Ljava/io/InputStream;->read([B)I

    move-result v0

    .line 265
    .local v0, "bytesRead":I
    iget-object v3, p0, Lcom/google/android/wearable/datatransfer/internal/DataSender;->mMutableState:Lcom/google/android/wearable/datatransfer/internal/DataSender$MutableState;

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v4

    iput v4, v3, Lcom/google/android/wearable/datatransfer/internal/DataSender$MutableState;->bytesRead:I

    .line 266
    iget-object v3, p0, Lcom/google/android/wearable/datatransfer/internal/DataSender;->mMutableState:Lcom/google/android/wearable/datatransfer/internal/DataSender$MutableState;

    iget-object v4, p0, Lcom/google/android/wearable/datatransfer/internal/DataSender;->mMutableState:Lcom/google/android/wearable/datatransfer/internal/DataSender$MutableState;

    iget v4, v4, Lcom/google/android/wearable/datatransfer/internal/DataSender$MutableState;->bytesRead:I

    if-nez v4, :cond_1

    move v1, v2

    :cond_1
    iput-boolean v1, v3, Lcom/google/android/wearable/datatransfer/internal/DataSender$MutableState;->finalPacket:Z

    move v1, v2

    .line 268
    goto :goto_0
.end method


# virtual methods
.method cancel()V
    .locals 1

    .prologue
    .line 224
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataSender;->mInputStream:Ljava/io/InputStream;

    invoke-static {v0}, Lcom/google/android/wearable/datatransfer/internal/Utils;->closeQuietly(Ljava/io/Closeable;)V

    .line 225
    return-void
.end method

.method onAckReceived(Lcom/google/android/wearable/datatransfer/internal/DataTransferAckPacket;)V
    .locals 3
    .param p1, "packet"    # Lcom/google/android/wearable/datatransfer/internal/DataTransferAckPacket;

    .prologue
    .line 212
    :try_start_0
    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/internal/DataSender;->mDataHandler:Lcom/google/android/wearable/datatransfer/internal/DataHandler;

    invoke-virtual {v1, p1}, Lcom/google/android/wearable/datatransfer/internal/DataHandler;->onPacketReceived(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 216
    :goto_0
    return-void

    .line 213
    :catch_0
    move-exception v0

    .line 214
    .local v0, "e":Ljava/io/IOException;
    const-string v1, "DataSender"

    const-string v2, "error delivering packet"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method send()V
    .locals 12

    .prologue
    .line 146
    iget-wide v2, p0, Lcom/google/android/wearable/datatransfer/internal/DataSender;->mInitialClientConnectionId:J

    .line 149
    .local v2, "clientConnectionId":J
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/wearable/datatransfer/internal/DataSender;->fastForward()Z
    :try_end_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v4

    if-nez v4, :cond_0

    .line 201
    iget-object v4, p0, Lcom/google/android/wearable/datatransfer/internal/DataSender;->mInputStream:Ljava/io/InputStream;

    invoke-static {v4}, Lcom/google/android/wearable/datatransfer/internal/Utils;->closeQuietly(Ljava/io/Closeable;)V

    .line 203
    :goto_0
    return-void

    .line 153
    :cond_0
    :try_start_1
    invoke-static {}, Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;->newPacket()Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;

    move-result-object v1

    .line 154
    .local v1, "packet":Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;
    iget-wide v4, p0, Lcom/google/android/wearable/datatransfer/internal/DataSender;->mServerConnectionId:J

    iget-wide v6, p0, Lcom/google/android/wearable/datatransfer/internal/DataSender;->mTransferSize:J

    invoke-virtual/range {v1 .. v7}, Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;->mutateToFirstPacket(JJJ)V

    .line 162
    const/4 v11, 0x0

    .line 166
    .local v11, "packetBytes":[B
    :goto_1
    invoke-virtual {v1, v11}, Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;->toBytes([B)[B

    move-result-object v11

    .line 167
    iget-object v4, p0, Lcom/google/android/wearable/datatransfer/internal/DataSender;->mDataHandler:Lcom/google/android/wearable/datatransfer/internal/DataHandler;

    const-string v5, "com.google.android.wearable.datatransfer.DATA_TRANSFER"

    invoke-virtual {v4, v5, v11}, Lcom/google/android/wearable/datatransfer/internal/DataHandler;->sendMessage(Ljava/lang/String;[B)Ljava/util/concurrent/Future;

    move-result-object v9

    .line 169
    .local v9, "futureAck":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<Lcom/google/android/wearable/datatransfer/internal/DataTransferAckPacket;>;"
    invoke-direct {p0}, Lcom/google/android/wearable/datatransfer/internal/DataSender;->loadNextSegment()Z

    move-result v10

    .line 170
    .local v10, "moreSegmentsToSend":Z
    iget-object v4, p0, Lcom/google/android/wearable/datatransfer/internal/DataSender;->mDataHandler:Lcom/google/android/wearable/datatransfer/internal/DataHandler;

    const-wide/16 v6, 0x4e20

    sget-object v5, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v4, v9, v6, v7, v5}, Lcom/google/android/wearable/datatransfer/internal/DataHandler;->waitForPacket(Ljava/util/concurrent/Future;JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/wearable/datatransfer/internal/DataTransferAckPacket;

    .line 173
    .local v0, "ack":Lcom/google/android/wearable/datatransfer/internal/DataTransferAckPacket;
    iget-object v4, v0, Lcom/google/android/wearable/datatransfer/internal/DataTransferAckPacket;->proto:Lcom/google/android/wearable/datatransfer/proto/PacketProtos$AckPacket;

    iget-wide v2, v4, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$AckPacket;->clientConnectionId:J
    :try_end_1
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 174
    if-nez v10, :cond_1

    .line 201
    iget-object v4, p0, Lcom/google/android/wearable/datatransfer/internal/DataSender;->mInputStream:Ljava/io/InputStream;

    invoke-static {v4}, Lcom/google/android/wearable/datatransfer/internal/Utils;->closeQuietly(Ljava/io/Closeable;)V

    goto :goto_0

    .line 177
    :cond_1
    :try_start_2
    invoke-direct {p0, v1, v2, v3}, Lcom/google/android/wearable/datatransfer/internal/DataSender;->getNextSegmentPacket(Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;J)V
    :try_end_2
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 180
    .end local v0    # "ack":Lcom/google/android/wearable/datatransfer/internal/DataTransferAckPacket;
    .end local v1    # "packet":Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;
    .end local v9    # "futureAck":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<Lcom/google/android/wearable/datatransfer/internal/DataTransferAckPacket;>;"
    .end local v10    # "moreSegmentsToSend":Z
    .end local v11    # "packetBytes":[B
    :catch_0
    move-exception v8

    .line 181
    .local v8, "e":Ljava/util/concurrent/TimeoutException;
    :try_start_3
    const-string v4, "DataSender"

    const-string v5, "timed out while waiting for ACK"

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 182
    const-string v4, "DataSender"

    invoke-static {v4}, Lcom/google/android/wearable/datatransfer/internal/DebugLog;->isLoggable(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 183
    const-string v4, "DataSender"

    const-string v5, "Timeout stack trace"

    invoke-static {v4, v5, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 201
    :cond_2
    iget-object v4, p0, Lcom/google/android/wearable/datatransfer/internal/DataSender;->mInputStream:Ljava/io/InputStream;

    invoke-static {v4}, Lcom/google/android/wearable/datatransfer/internal/Utils;->closeQuietly(Ljava/io/Closeable;)V

    goto :goto_0

    .line 190
    .end local v8    # "e":Ljava/util/concurrent/TimeoutException;
    :catch_1
    move-exception v8

    .line 191
    .local v8, "e":Ljava/lang/InterruptedException;
    :try_start_4
    const-string v4, "DataSender"

    invoke-static {v4}, Lcom/google/android/wearable/datatransfer/internal/DebugLog;->isLoggable(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 192
    const-string v4, "DataSender"

    const-string v5, "transfer interrupted"

    invoke-static {v4, v5, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 194
    :cond_3
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Thread;->interrupt()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 201
    iget-object v4, p0, Lcom/google/android/wearable/datatransfer/internal/DataSender;->mInputStream:Ljava/io/InputStream;

    invoke-static {v4}, Lcom/google/android/wearable/datatransfer/internal/Utils;->closeQuietly(Ljava/io/Closeable;)V

    goto :goto_0

    .line 196
    .end local v8    # "e":Ljava/lang/InterruptedException;
    :catch_2
    move-exception v8

    .line 197
    .local v8, "e":Ljava/io/IOException;
    :try_start_5
    const-string v4, "DataSender"

    const-string v5, "error during transfer"

    invoke-static {v4, v5, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 201
    iget-object v4, p0, Lcom/google/android/wearable/datatransfer/internal/DataSender;->mInputStream:Ljava/io/InputStream;

    invoke-static {v4}, Lcom/google/android/wearable/datatransfer/internal/Utils;->closeQuietly(Ljava/io/Closeable;)V

    goto :goto_0

    .end local v8    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    iget-object v5, p0, Lcom/google/android/wearable/datatransfer/internal/DataSender;->mInputStream:Ljava/io/InputStream;

    invoke-static {v5}, Lcom/google/android/wearable/datatransfer/internal/Utils;->closeQuietly(Ljava/io/Closeable;)V

    throw v4
.end method
