.class final Lcom/google/android/wearable/datatransfer/internal/DataHandler;
.super Ljava/lang/Object;
.source "DataHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/wearable/datatransfer/internal/DataHandler$FuturePacket;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final mClient:Lcom/google/android/gms/common/api/GoogleApiClient;

.field private final mExpectedPacketLatch:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "Ljava/util/concurrent/CountDownLatch;",
            ">;"
        }
    .end annotation
.end field

.field private final mLatestPacket:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final mRemoteNodeId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/gms/common/api/GoogleApiClient;Ljava/lang/String;)V
    .locals 1
    .param p1, "client"    # Lcom/google/android/gms/common/api/GoogleApiClient;
    .param p2, "remoteNodeId"    # Ljava/lang/String;

    .prologue
    .line 68
    .local p0, "this":Lcom/google/android/wearable/datatransfer/internal/DataHandler;, "Lcom/google/android/wearable/datatransfer/internal/DataHandler<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataHandler;->mExpectedPacketLatch:Ljava/util/concurrent/atomic/AtomicReference;

    .line 60
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataHandler;->mLatestPacket:Ljava/util/concurrent/atomic/AtomicReference;

    .line 69
    const-string v0, "client"

    invoke-static {p1, v0}, Lcom/google/android/wearable/datatransfer/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/GoogleApiClient;

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataHandler;->mClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    .line 70
    const-string v0, "remoteNodeId"

    invoke-static {p2, v0}, Lcom/google/android/wearable/datatransfer/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataHandler;->mRemoteNodeId:Ljava/lang/String;

    .line 71
    return-void
.end method

.method private newExpectedPacketLatch()Ljava/util/concurrent/CountDownLatch;
    .locals 3

    .prologue
    .line 193
    .local p0, "this":Lcom/google/android/wearable/datatransfer/internal/DataHandler;, "Lcom/google/android/wearable/datatransfer/internal/DataHandler<TT;>;"
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    .line 194
    .local v0, "expectedPacketLatch":Ljava/util/concurrent/CountDownLatch;
    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/internal/DataHandler;->mExpectedPacketLatch:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 195
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "There should be no expected packet"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 197
    :cond_0
    return-object v0
.end method


# virtual methods
.method clearPending()V
    .locals 2

    .prologue
    .local p0, "this":Lcom/google/android/wearable/datatransfer/internal/DataHandler;, "Lcom/google/android/wearable/datatransfer/internal/DataHandler<TT;>;"
    const/4 v1, 0x0

    .line 181
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataHandler;->mExpectedPacketLatch:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 182
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataHandler;->mLatestPacket:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 183
    return-void
.end method

.method getRemoteNodeId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    .local p0, "this":Lcom/google/android/wearable/datatransfer/internal/DataHandler;, "Lcom/google/android/wearable/datatransfer/internal/DataHandler<TT;>;"
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataHandler;->mRemoteNodeId:Ljava/lang/String;

    return-object v0
.end method

.method onPacketReceived(Ljava/lang/Object;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 166
    .local p0, "this":Lcom/google/android/wearable/datatransfer/internal/DataHandler;, "Lcom/google/android/wearable/datatransfer/internal/DataHandler<TT;>;"
    .local p1, "packet":Ljava/lang/Object;, "TT;"
    const-string v1, "DataHandler"

    invoke-static {v1}, Lcom/google/android/wearable/datatransfer/internal/DebugLog;->isLoggable(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 167
    const-string v1, "DataHandler"

    const-string v2, "received packet"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 169
    :cond_0
    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/internal/DataHandler;->mExpectedPacketLatch:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicReference;->getAndSet(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/CountDownLatch;

    .line 170
    .local v0, "latch":Ljava/util/concurrent/CountDownLatch;
    const-string v1, "DataHandler"

    invoke-static {v1}, Lcom/google/android/wearable/datatransfer/internal/DebugLog;->isLoggable(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 171
    const-string v1, "DataHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "notifying latch: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 173
    :cond_1
    if-nez v0, :cond_2

    .line 174
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Received unexpected packet"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 176
    :cond_2
    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/internal/DataHandler;->mLatestPacket:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v1, p1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 177
    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 178
    return-void
.end method

.method sendMessage(Ljava/lang/String;[B)Ljava/util/concurrent/Future;
    .locals 6
    .param p1, "messagePath"    # Ljava/lang/String;
    .param p2, "bytes"    # [B
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "[B)",
            "Ljava/util/concurrent/Future",
            "<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 117
    .local p0, "this":Lcom/google/android/wearable/datatransfer/internal/DataHandler;, "Lcom/google/android/wearable/datatransfer/internal/DataHandler<TT;>;"
    const-string v2, "DataHandler"

    invoke-static {v2}, Lcom/google/android/wearable/datatransfer/internal/DebugLog;->isLoggable(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 118
    const-string v2, "DataHandler"

    const-string v3, "sendMessage"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    :cond_0
    invoke-direct {p0}, Lcom/google/android/wearable/datatransfer/internal/DataHandler;->newExpectedPacketLatch()Ljava/util/concurrent/CountDownLatch;

    move-result-object v0

    .line 121
    .local v0, "expectedPacketLatch":Ljava/util/concurrent/CountDownLatch;
    sget-object v2, Lcom/google/android/gms/wearable/Wearable;->MessageApi:Lcom/google/android/gms/wearable/MessageApi;

    iget-object v3, p0, Lcom/google/android/wearable/datatransfer/internal/DataHandler;->mClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    iget-object v4, p0, Lcom/google/android/wearable/datatransfer/internal/DataHandler;->mRemoteNodeId:Ljava/lang/String;

    invoke-interface {v2, v3, v4, p1, p2}, Lcom/google/android/gms/wearable/MessageApi;->sendMessage(Lcom/google/android/gms/common/api/GoogleApiClient;Ljava/lang/String;Ljava/lang/String;[B)Lcom/google/android/gms/common/api/PendingResult;

    move-result-object v2

    const-wide/16 v4, 0x2710

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v2, v4, v5, v3}, Lcom/google/android/gms/common/api/PendingResult;->await(JLjava/util/concurrent/TimeUnit;)Lcom/google/android/gms/common/api/Result;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/wearable/MessageApi$SendMessageResult;

    .line 124
    .local v1, "result":Lcom/google/android/gms/wearable/MessageApi$SendMessageResult;
    invoke-interface {v1}, Lcom/google/android/gms/wearable/MessageApi$SendMessageResult;->getStatus()Lcom/google/android/gms/common/api/Status;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/common/api/Status;->isInterrupted()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 125
    :cond_1
    new-instance v2, Ljava/lang/InterruptedException;

    const-string v3, "sendMessage interrupted."

    invoke-direct {v2, v3}, Ljava/lang/InterruptedException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 127
    :cond_2
    invoke-interface {v1}, Lcom/google/android/gms/wearable/MessageApi$SendMessageResult;->getStatus()Lcom/google/android/gms/common/api/Status;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/common/api/Status;->isSuccess()Z

    move-result v2

    if-nez v2, :cond_3

    .line 128
    new-instance v2, Ljava/io/IOException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to send message: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v1}, Lcom/google/android/gms/wearable/MessageApi$SendMessageResult;->getStatus()Lcom/google/android/gms/common/api/Status;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 130
    :cond_3
    new-instance v2, Lcom/google/android/wearable/datatransfer/internal/DataHandler$FuturePacket;

    iget-object v3, p0, Lcom/google/android/wearable/datatransfer/internal/DataHandler;->mLatestPacket:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v2, v0, v3}, Lcom/google/android/wearable/datatransfer/internal/DataHandler$FuturePacket;-><init>(Ljava/util/concurrent/CountDownLatch;Ljava/util/concurrent/atomic/AtomicReference;)V

    return-object v2
.end method

.method sendMessageAndWaitForPacket(Ljava/lang/String;[BJLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    .locals 3
    .param p1, "messagePath"    # Ljava/lang/String;
    .param p2, "bytes"    # [B
    .param p3, "timeout"    # J
    .param p5, "timeoutUnit"    # Ljava/util/concurrent/TimeUnit;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "[BJ",
            "Ljava/util/concurrent/TimeUnit;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;,
            Ljava/util/concurrent/TimeoutException;
        }
    .end annotation

    .prologue
    .line 95
    .local p0, "this":Lcom/google/android/wearable/datatransfer/internal/DataHandler;, "Lcom/google/android/wearable/datatransfer/internal/DataHandler<TT;>;"
    const-string v1, "DataHandler"

    invoke-static {v1}, Lcom/google/android/wearable/datatransfer/internal/DebugLog;->isLoggable(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 96
    const-string v1, "DataHandler"

    const-string v2, "sendMessageAndWaitForPacket"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    :cond_0
    :try_start_0
    invoke-virtual {p0, p1, p2}, Lcom/google/android/wearable/datatransfer/internal/DataHandler;->sendMessage(Ljava/lang/String;[B)Ljava/util/concurrent/Future;

    move-result-object v0

    .line 100
    .local v0, "futurePacket":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<TT;>;"
    invoke-virtual {p0, v0, p3, p4, p5}, Lcom/google/android/wearable/datatransfer/internal/DataHandler;->waitForPacket(Ljava/util/concurrent/Future;JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 103
    invoke-virtual {p0}, Lcom/google/android/wearable/datatransfer/internal/DataHandler;->clearPending()V

    return-object v1

    .end local v0    # "futurePacket":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<TT;>;"
    :catchall_0
    move-exception v1

    invoke-virtual {p0}, Lcom/google/android/wearable/datatransfer/internal/DataHandler;->clearPending()V

    throw v1
.end method

.method waitForPacket(Ljava/util/concurrent/Future;JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    .locals 4
    .param p2, "timeout"    # J
    .param p4, "timeoutUnit"    # Ljava/util/concurrent/TimeUnit;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Future",
            "<TT;>;J",
            "Ljava/util/concurrent/TimeUnit;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;,
            Ljava/util/concurrent/TimeoutException;
        }
    .end annotation

    .prologue
    .line 147
    .local p0, "this":Lcom/google/android/wearable/datatransfer/internal/DataHandler;, "Lcom/google/android/wearable/datatransfer/internal/DataHandler<TT;>;"
    .local p1, "futurePacket":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<TT;>;"
    :try_start_0
    const-string v1, "DataHandler"

    invoke-static {v1}, Lcom/google/android/wearable/datatransfer/internal/DebugLog;->isLoggable(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 148
    const-string v1, "DataHandler"

    const-string v2, "waitForPacket"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    :cond_0
    invoke-interface {p1, p2, p3, p4}, Ljava/util/concurrent/Future;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    .line 151
    :catch_0
    move-exception v0

    .line 152
    .local v0, "e":Ljava/util/concurrent/ExecutionException;
    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    instance-of v1, v1, Ljava/io/IOException;

    if-eqz v1, :cond_1

    .line 153
    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    check-cast v1, Ljava/io/IOException;

    throw v1

    .line 155
    :cond_1
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "unexpected error"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method
