.class final Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;
.super Ljava/lang/Object;
.source "DataSyncRequest.java"


# instance fields
.field public final path:Ljava/lang/String;

.field public final remoteNodeId:Ljava/lang/String;

.field public final retryCount:I

.field public final targetFile:Ljava/io/File;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;I)V
    .locals 1
    .param p1, "remoteNodeId"    # Ljava/lang/String;
    .param p2, "path"    # Ljava/lang/String;
    .param p3, "targetFile"    # Ljava/io/File;
    .param p4, "retryCount"    # I

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    const-string v0, "remoteNodeId"

    invoke-static {p1, v0}, Lcom/google/android/wearable/datatransfer/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;->remoteNodeId:Ljava/lang/String;

    .line 36
    const-string v0, "path"

    invoke-static {p2, v0}, Lcom/google/android/wearable/datatransfer/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;->path:Ljava/lang/String;

    .line 37
    const-string v0, "targetFile"

    invoke-static {p3, v0}, Lcom/google/android/wearable/datatransfer/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;->targetFile:Ljava/io/File;

    .line 38
    iput p4, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;->retryCount:I

    .line 39
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 53
    if-ne p0, p1, :cond_1

    .line 63
    :cond_0
    :goto_0
    return v1

    .line 57
    :cond_1
    instance-of v3, p1, Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;

    if-nez v3, :cond_2

    move v1, v2

    .line 58
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 61
    check-cast v0, Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;

    .line 63
    .local v0, "that":Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;
    iget-object v3, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;->path:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;->path:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;->remoteNodeId:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;->remoteNodeId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;->targetFile:Ljava/io/File;

    iget-object v4, v0, Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;->targetFile:Ljava/io/File;

    invoke-virtual {v3, v4}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget v3, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;->retryCount:I

    iget v4, v0, Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;->retryCount:I

    if-eq v3, v4, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 71
    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;->remoteNodeId:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 72
    .local v0, "result":I
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;->path:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int v0, v1, v2

    .line 73
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;->targetFile:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->hashCode()I

    move-result v2

    add-int v0, v1, v2

    .line 74
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;->retryCount:I

    add-int v0, v1, v2

    .line 75
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    const/16 v2, 0x27

    .line 43
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DataSyncRequest{remoteNodeId=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;->remoteNodeId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", path=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;->path:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", targetFile="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;->targetFile:Ljava/io/File;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", retryCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;->retryCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
