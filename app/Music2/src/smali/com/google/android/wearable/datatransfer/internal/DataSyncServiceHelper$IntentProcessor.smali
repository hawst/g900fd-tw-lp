.class final Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper$IntentProcessor;
.super Ljava/lang/Object;
.source "DataSyncServiceHelper.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "IntentProcessor"
.end annotation


# instance fields
.field private final mIntent:Landroid/content/Intent;

.field private final mListener:Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper$CompletionListener;

.field final synthetic this$0:Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;


# direct methods
.method public constructor <init>(Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;Landroid/content/Intent;Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper$CompletionListener;)V
    .locals 1
    .param p2, "intent"    # Landroid/content/Intent;
    .param p3, "listener"    # Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper$CompletionListener;

    .prologue
    .line 712
    iput-object p1, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper$IntentProcessor;->this$0:Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 713
    const-string v0, "intent"

    invoke-static {p2, v0}, Lcom/google/android/wearable/datatransfer/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper$IntentProcessor;->mIntent:Landroid/content/Intent;

    .line 714
    const-string v0, "listener"

    invoke-static {p3, v0}, Lcom/google/android/wearable/datatransfer/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper$CompletionListener;

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper$IntentProcessor;->mListener:Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper$CompletionListener;

    .line 715
    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 719
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper$IntentProcessor;->this$0:Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;

    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper$IntentProcessor;->mIntent:Landroid/content/Intent;

    iget-object v2, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper$IntentProcessor;->mListener:Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper$CompletionListener;

    # invokes: Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->processIntent(Landroid/content/Intent;Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper$CompletionListener;)V
    invoke-static {v0, v1, v2}, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->access$000(Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;Landroid/content/Intent;Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper$CompletionListener;)V

    .line 720
    return-void
.end method
