.class final Lcom/google/android/wearable/datatransfer/internal/DataTransferApiImpl$1;
.super Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;
.source "DataTransferApiImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/wearable/datatransfer/internal/DataTransferApiImpl;->newOpenConnectionPendingResult()Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult",
        "<",
        "Lcom/google/android/wearable/datatransfer/DataTransferApi$OpenConnectionResult;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(Landroid/os/Looper;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Looper;

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method protected createFailedResult(I)Lcom/google/android/wearable/datatransfer/DataTransferApi$OpenConnectionResult;
    .locals 1
    .param p1, "commonStatusCode"    # I

    .prologue
    .line 42
    # invokes: Lcom/google/android/wearable/datatransfer/internal/DataTransferApiImpl;->failedOpenConnectionResult(I)Lcom/google/android/wearable/datatransfer/DataTransferApi$OpenConnectionResult;
    invoke-static {p1}, Lcom/google/android/wearable/datatransfer/internal/DataTransferApiImpl;->access$000(I)Lcom/google/android/wearable/datatransfer/DataTransferApi$OpenConnectionResult;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic createFailedResult(I)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 39
    invoke-virtual {p0, p1}, Lcom/google/android/wearable/datatransfer/internal/DataTransferApiImpl$1;->createFailedResult(I)Lcom/google/android/wearable/datatransfer/DataTransferApi$OpenConnectionResult;

    move-result-object v0

    return-object v0
.end method
