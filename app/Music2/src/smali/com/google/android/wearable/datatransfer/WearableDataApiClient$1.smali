.class Lcom/google/android/wearable/datatransfer/WearableDataApiClient$1;
.super Ljava/lang/Object;
.source "WearableDataApiClient.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/wearable/datatransfer/WearableDataApiClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/wearable/datatransfer/WearableDataApiClient;


# direct methods
.method constructor <init>(Lcom/google/android/wearable/datatransfer/WearableDataApiClient;)V
    .locals 0

    .prologue
    .line 72
    iput-object p1, p0, Lcom/google/android/wearable/datatransfer/WearableDataApiClient$1;->this$0:Lcom/google/android/wearable/datatransfer/WearableDataApiClient;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 7
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "binder"    # Landroid/os/IBinder;

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/WearableDataApiClient$1;->this$0:Lcom/google/android/wearable/datatransfer/WearableDataApiClient;

    check-cast p2, Lcom/google/android/wearable/datatransfer/ConnectionService$ConnectionServiceBinder;

    .end local p2    # "binder":Landroid/os/IBinder;
    # setter for: Lcom/google/android/wearable/datatransfer/WearableDataApiClient;->mService:Lcom/google/android/wearable/datatransfer/ConnectionService$ConnectionServiceBinder;
    invoke-static {v0, p2}, Lcom/google/android/wearable/datatransfer/WearableDataApiClient;->access$002(Lcom/google/android/wearable/datatransfer/WearableDataApiClient;Lcom/google/android/wearable/datatransfer/ConnectionService$ConnectionServiceBinder;)Lcom/google/android/wearable/datatransfer/ConnectionService$ConnectionServiceBinder;

    .line 80
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/WearableDataApiClient$1;->this$0:Lcom/google/android/wearable/datatransfer/WearableDataApiClient;

    # getter for: Lcom/google/android/wearable/datatransfer/WearableDataApiClient;->mPendingEvents:Ljava/util/Queue;
    invoke-static {v0}, Lcom/google/android/wearable/datatransfer/WearableDataApiClient;->access$100(Lcom/google/android/wearable/datatransfer/WearableDataApiClient;)Ljava/util/Queue;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/wearable/datatransfer/WearableDataApiClient$Event;

    .local v6, "event":Lcom/google/android/wearable/datatransfer/WearableDataApiClient$Event;
    if-eqz v6, :cond_4

    .line 81
    iget-object v0, v6, Lcom/google/android/wearable/datatransfer/WearableDataApiClient$Event;->messageEvent:Lcom/google/android/gms/wearable/MessageEvent;

    if-eqz v0, :cond_1

    .line 82
    iget-object v0, v6, Lcom/google/android/wearable/datatransfer/WearableDataApiClient$Event;->messageEvent:Lcom/google/android/gms/wearable/MessageEvent;

    invoke-interface {v0}, Lcom/google/android/gms/wearable/MessageEvent;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->isHandledMessagePath(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 83
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/WearableDataApiClient$1;->this$0:Lcom/google/android/wearable/datatransfer/WearableDataApiClient;

    iget-object v1, v6, Lcom/google/android/wearable/datatransfer/WearableDataApiClient$Event;->messageEvent:Lcom/google/android/gms/wearable/MessageEvent;

    # invokes: Lcom/google/android/wearable/datatransfer/WearableDataApiClient;->startDataSyncServiceWithMessage(Lcom/google/android/gms/wearable/MessageEvent;)V
    invoke-static {v0, v1}, Lcom/google/android/wearable/datatransfer/WearableDataApiClient;->access$200(Lcom/google/android/wearable/datatransfer/WearableDataApiClient;Lcom/google/android/gms/wearable/MessageEvent;)V

    .line 88
    :cond_1
    :goto_1
    iget-object v0, v6, Lcom/google/android/wearable/datatransfer/WearableDataApiClient$Event;->openConnectionRequest:Lcom/google/android/wearable/datatransfer/WearableDataApiClient$OpenConnectionRequest;

    if-eqz v0, :cond_2

    .line 89
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/WearableDataApiClient$1;->this$0:Lcom/google/android/wearable/datatransfer/WearableDataApiClient;

    # getter for: Lcom/google/android/wearable/datatransfer/WearableDataApiClient;->mService:Lcom/google/android/wearable/datatransfer/ConnectionService$ConnectionServiceBinder;
    invoke-static {v0}, Lcom/google/android/wearable/datatransfer/WearableDataApiClient;->access$000(Lcom/google/android/wearable/datatransfer/WearableDataApiClient;)Lcom/google/android/wearable/datatransfer/ConnectionService$ConnectionServiceBinder;

    move-result-object v0

    iget-object v1, v6, Lcom/google/android/wearable/datatransfer/WearableDataApiClient$Event;->openConnectionRequest:Lcom/google/android/wearable/datatransfer/WearableDataApiClient$OpenConnectionRequest;

    iget-object v1, v1, Lcom/google/android/wearable/datatransfer/WearableDataApiClient$OpenConnectionRequest;->pendingResult:Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;

    iget-object v2, v6, Lcom/google/android/wearable/datatransfer/WearableDataApiClient$Event;->openConnectionRequest:Lcom/google/android/wearable/datatransfer/WearableDataApiClient$OpenConnectionRequest;

    iget-object v2, v2, Lcom/google/android/wearable/datatransfer/WearableDataApiClient$OpenConnectionRequest;->nodeId:Ljava/lang/String;

    iget-object v3, v6, Lcom/google/android/wearable/datatransfer/WearableDataApiClient$Event;->openConnectionRequest:Lcom/google/android/wearable/datatransfer/WearableDataApiClient$OpenConnectionRequest;

    iget-object v3, v3, Lcom/google/android/wearable/datatransfer/WearableDataApiClient$OpenConnectionRequest;->path:Ljava/lang/String;

    iget-object v4, v6, Lcom/google/android/wearable/datatransfer/WearableDataApiClient$Event;->openConnectionRequest:Lcom/google/android/wearable/datatransfer/WearableDataApiClient$OpenConnectionRequest;

    iget-wide v4, v4, Lcom/google/android/wearable/datatransfer/WearableDataApiClient$OpenConnectionRequest;->offset:J

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/wearable/datatransfer/ConnectionService$ConnectionServiceBinder;->openConnection(Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;Ljava/lang/String;Ljava/lang/String;J)V

    .line 95
    :cond_2
    iget-object v0, v6, Lcom/google/android/wearable/datatransfer/WearableDataApiClient$Event;->stopAllServerConnectionsEvent:Lcom/google/android/wearable/datatransfer/WearableDataApiClient$StopAllServerConnectionsEvent;

    if-eqz v0, :cond_0

    .line 96
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/WearableDataApiClient$1;->this$0:Lcom/google/android/wearable/datatransfer/WearableDataApiClient;

    # getter for: Lcom/google/android/wearable/datatransfer/WearableDataApiClient;->mService:Lcom/google/android/wearable/datatransfer/ConnectionService$ConnectionServiceBinder;
    invoke-static {v0}, Lcom/google/android/wearable/datatransfer/WearableDataApiClient;->access$000(Lcom/google/android/wearable/datatransfer/WearableDataApiClient;)Lcom/google/android/wearable/datatransfer/ConnectionService$ConnectionServiceBinder;

    move-result-object v0

    iget-object v1, v6, Lcom/google/android/wearable/datatransfer/WearableDataApiClient$Event;->stopAllServerConnectionsEvent:Lcom/google/android/wearable/datatransfer/WearableDataApiClient$StopAllServerConnectionsEvent;

    iget-object v1, v1, Lcom/google/android/wearable/datatransfer/WearableDataApiClient$StopAllServerConnectionsEvent;->pendingResult:Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;

    invoke-virtual {v0, v1}, Lcom/google/android/wearable/datatransfer/ConnectionService$ConnectionServiceBinder;->stopAllServerConnections(Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;)V

    goto :goto_0

    .line 85
    :cond_3
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/WearableDataApiClient$1;->this$0:Lcom/google/android/wearable/datatransfer/WearableDataApiClient;

    # getter for: Lcom/google/android/wearable/datatransfer/WearableDataApiClient;->mService:Lcom/google/android/wearable/datatransfer/ConnectionService$ConnectionServiceBinder;
    invoke-static {v0}, Lcom/google/android/wearable/datatransfer/WearableDataApiClient;->access$000(Lcom/google/android/wearable/datatransfer/WearableDataApiClient;)Lcom/google/android/wearable/datatransfer/ConnectionService$ConnectionServiceBinder;

    move-result-object v0

    iget-object v1, v6, Lcom/google/android/wearable/datatransfer/WearableDataApiClient$Event;->messageEvent:Lcom/google/android/gms/wearable/MessageEvent;

    invoke-virtual {v0, v1}, Lcom/google/android/wearable/datatransfer/ConnectionService$ConnectionServiceBinder;->onMessageReceived(Lcom/google/android/gms/wearable/MessageEvent;)Z

    goto :goto_1

    .line 100
    :cond_4
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/WearableDataApiClient$1;->this$0:Lcom/google/android/wearable/datatransfer/WearableDataApiClient;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/wearable/datatransfer/WearableDataApiClient;->mService:Lcom/google/android/wearable/datatransfer/ConnectionService$ConnectionServiceBinder;
    invoke-static {v0, v1}, Lcom/google/android/wearable/datatransfer/WearableDataApiClient;->access$002(Lcom/google/android/wearable/datatransfer/WearableDataApiClient;Lcom/google/android/wearable/datatransfer/ConnectionService$ConnectionServiceBinder;)Lcom/google/android/wearable/datatransfer/ConnectionService$ConnectionServiceBinder;

    .line 105
    return-void
.end method
