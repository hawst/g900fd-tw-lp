.class public Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$CallbackHandler;
.super Landroid/os/Handler;
.source "WearableDataCompatImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CallbackHandler"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R:",
        "Ljava/lang/Object;",
        ">",
        "Landroid/os/Handler;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 275
    .local p0, "this":Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$CallbackHandler;, "Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$CallbackHandler<TR;>;"
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$CallbackHandler;-><init>(Landroid/os/Looper;)V

    .line 276
    return-void
.end method

.method public constructor <init>(Landroid/os/Looper;)V
    .locals 0
    .param p1, "looper"    # Landroid/os/Looper;

    .prologue
    .line 279
    .local p0, "this":Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$CallbackHandler;, "Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$CallbackHandler<TR;>;"
    invoke-direct {p0, p1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 280
    return-void
.end method

.method private maybeReleaseResult(Ljava/lang/Object;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;)V"
        }
    .end annotation

    .prologue
    .line 328
    .local p0, "this":Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$CallbackHandler;, "Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$CallbackHandler<TR;>;"
    .local p1, "result":Ljava/lang/Object;, "TR;"
    instance-of v2, p1, Lcom/google/android/gms/common/api/Releasable;

    if-eqz v2, :cond_0

    .line 330
    :try_start_0
    move-object v0, p1

    check-cast v0, Lcom/google/android/gms/common/api/Releasable;

    move-object v2, v0

    invoke-interface {v2}, Lcom/google/android/gms/common/api/Releasable;->release()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 335
    :cond_0
    :goto_0
    return-void

    .line 331
    :catch_0
    move-exception v1

    .line 332
    .local v1, "e":Ljava/lang/RuntimeException;
    const-string v2, "CallbackHandler"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unable to release "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method


# virtual methods
.method deliverResultCallback(Lcom/google/android/wearable/datatransfer/WearableDataCompat$ResultCallback;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/wearable/datatransfer/WearableDataCompat$ResultCallback",
            "<-TR;>;TR;)V"
        }
    .end annotation

    .prologue
    .line 320
    .local p0, "this":Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$CallbackHandler;, "Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$CallbackHandler<TR;>;"
    .local p1, "callback":Lcom/google/android/wearable/datatransfer/WearableDataCompat$ResultCallback;, "Lcom/google/android/wearable/datatransfer/WearableDataCompat$ResultCallback<-TR;>;"
    .local p2, "result":Ljava/lang/Object;, "TR;"
    :try_start_0
    invoke-interface {p1, p2}, Lcom/google/android/wearable/datatransfer/WearableDataCompat$ResultCallback;->onResult(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 325
    return-void

    .line 321
    :catch_0
    move-exception v0

    .line 322
    .local v0, "e":Ljava/lang/RuntimeException;
    invoke-direct {p0, p2}, Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$CallbackHandler;->maybeReleaseResult(Ljava/lang/Object;)V

    .line 323
    throw v0
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 299
    .local p0, "this":Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$CallbackHandler;, "Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$CallbackHandler<TR;>;"
    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    .line 314
    const-string v2, "CallbackHandler"

    const-string v3, "Don\'t know how to handle this message."

    invoke-static {v2, v3}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 315
    :goto_0
    return-void

    .line 302
    :pswitch_0
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Landroid/util/Pair;

    .line 304
    .local v1, "resultCall":Landroid/util/Pair;, "Landroid/util/Pair<Lcom/google/android/wearable/datatransfer/WearableDataCompat$ResultCallback<-TR;>;TR;>;"
    iget-object v2, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Lcom/google/android/wearable/datatransfer/WearableDataCompat$ResultCallback;

    iget-object v3, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-virtual {p0, v2, v3}, Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$CallbackHandler;->deliverResultCallback(Lcom/google/android/wearable/datatransfer/WearableDataCompat$ResultCallback;Ljava/lang/Object;)V

    goto :goto_0

    .line 308
    .end local v1    # "resultCall":Landroid/util/Pair;, "Landroid/util/Pair<Lcom/google/android/wearable/datatransfer/WearableDataCompat$ResultCallback<-TR;>;TR;>;"
    :pswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;

    .line 309
    .local v0, "pendingResult":Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;, "Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult<TR;>;"
    invoke-virtual {v0}, Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;->setTimeoutResult()V

    goto :goto_0

    .line 299
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public removeTimeoutMessages()V
    .locals 1

    .prologue
    .line 294
    .local p0, "this":Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$CallbackHandler;, "Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$CallbackHandler<TR;>;"
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$CallbackHandler;->removeMessages(I)V

    .line 295
    return-void
.end method

.method public sendResultCallback(Lcom/google/android/wearable/datatransfer/WearableDataCompat$ResultCallback;Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/wearable/datatransfer/WearableDataCompat$ResultCallback",
            "<-TR;>;TR;)V"
        }
    .end annotation

    .prologue
    .line 284
    .local p0, "this":Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$CallbackHandler;, "Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$CallbackHandler<TR;>;"
    .local p1, "callback":Lcom/google/android/wearable/datatransfer/WearableDataCompat$ResultCallback;, "Lcom/google/android/wearable/datatransfer/WearableDataCompat$ResultCallback<-TR;>;"
    .local p2, "result":Ljava/lang/Object;, "TR;"
    const/4 v0, 0x1

    new-instance v1, Landroid/util/Pair;

    invoke-direct {v1, p1, p2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$CallbackHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$CallbackHandler;->sendMessage(Landroid/os/Message;)Z

    .line 287
    return-void
.end method
