.class final Lcom/google/android/wearable/datatransfer/internal/RetryIntervalCalculator;
.super Ljava/lang/Object;
.source "RetryIntervalCalculator.java"


# instance fields
.field private final mInitialDelayMillis:I

.field private final mMaxDelayMillis:I

.field private final mRandom:Ljava/util/Random;


# direct methods
.method constructor <init>(II)V
    .locals 1
    .param p1, "initialDelayMillis"    # I
    .param p2, "maxDelayMillis"    # I

    .prologue
    .line 21
    new-instance v0, Ljava/security/SecureRandom;

    invoke-direct {v0}, Ljava/security/SecureRandom;-><init>()V

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/wearable/datatransfer/internal/RetryIntervalCalculator;-><init>(IILjava/util/Random;)V

    .line 22
    return-void
.end method

.method constructor <init>(IILjava/util/Random;)V
    .locals 1
    .param p1, "initialDelayMillis"    # I
    .param p2, "maxDelayMillis"    # I
    .param p3, "random"    # Ljava/util/Random;

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput p1, p0, Lcom/google/android/wearable/datatransfer/internal/RetryIntervalCalculator;->mInitialDelayMillis:I

    .line 26
    iput p2, p0, Lcom/google/android/wearable/datatransfer/internal/RetryIntervalCalculator;->mMaxDelayMillis:I

    .line 27
    const-string v0, "random"

    invoke-static {p3, v0}, Lcom/google/android/wearable/datatransfer/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Random;

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/RetryIntervalCalculator;->mRandom:Ljava/util/Random;

    .line 28
    return-void
.end method

.method private static clamp(DDD)D
    .locals 2
    .param p0, "min"    # D
    .param p2, "value"    # D
    .param p4, "max"    # D

    .prologue
    .line 42
    invoke-static {p2, p3, p4, p5}, Ljava/lang/Math;->min(DD)D

    move-result-wide v0

    invoke-static {p0, p1, v0, v1}, Ljava/lang/Math;->max(DD)D

    move-result-wide v0

    return-wide v0
.end method


# virtual methods
.method getNextRetryDelay(I)I
    .locals 10
    .param p1, "retryCount"    # I

    .prologue
    .line 33
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    iget v2, p0, Lcom/google/android/wearable/datatransfer/internal/RetryIntervalCalculator;->mInitialDelayMillis:I

    int-to-double v2, v2

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    int-to-double v8, p1

    invoke-static {v4, v5, v8, v9}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v4

    mul-double/2addr v2, v4

    iget v4, p0, Lcom/google/android/wearable/datatransfer/internal/RetryIntervalCalculator;->mMaxDelayMillis:I

    int-to-double v4, v4

    invoke-static/range {v0 .. v5}, Lcom/google/android/wearable/datatransfer/internal/RetryIntervalCalculator;->clamp(DDD)D

    move-result-wide v0

    double-to-int v6, v0

    .line 36
    .local v6, "lengthMillis":I
    or-int/lit8 v7, v6, 0x1

    .line 38
    .local v7, "rangeMillis":I
    div-int/lit8 v0, v6, 0x2

    sub-int v0, v6, v0

    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/internal/RetryIntervalCalculator;->mRandom:Ljava/util/Random;

    invoke-virtual {v1, v7}, Ljava/util/Random;->nextInt(I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method
