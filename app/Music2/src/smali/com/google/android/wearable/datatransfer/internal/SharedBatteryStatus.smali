.class final Lcom/google/android/wearable/datatransfer/internal/SharedBatteryStatus;
.super Ljava/lang/Object;
.source "SharedBatteryStatus.java"


# instance fields
.field final batteryLow:Z

.field final timestamp:J


# direct methods
.method constructor <init>(JZ)V
    .locals 1
    .param p1, "timestamp"    # J
    .param p3, "batteryLow"    # Z

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-wide p1, p0, Lcom/google/android/wearable/datatransfer/internal/SharedBatteryStatus;->timestamp:J

    .line 24
    iput-boolean p3, p0, Lcom/google/android/wearable/datatransfer/internal/SharedBatteryStatus;->batteryLow:Z

    .line 25
    return-void
.end method

.method static fromDataMap(Lcom/google/android/gms/wearable/DataMap;)Lcom/google/android/wearable/datatransfer/internal/SharedBatteryStatus;
    .locals 5
    .param p0, "map"    # Lcom/google/android/gms/wearable/DataMap;

    .prologue
    .line 29
    new-instance v0, Lcom/google/android/wearable/datatransfer/internal/SharedBatteryStatus;

    const-string v1, "timestamp"

    const-wide/16 v2, 0x0

    invoke-virtual {p0, v1, v2, v3}, Lcom/google/android/gms/wearable/DataMap;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    const-string v1, "batteryLow"

    const/4 v4, 0x0

    invoke-virtual {p0, v1, v4}, Lcom/google/android/gms/wearable/DataMap;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    invoke-direct {v0, v2, v3, v1}, Lcom/google/android/wearable/datatransfer/internal/SharedBatteryStatus;-><init>(JZ)V

    return-object v0
.end method


# virtual methods
.method toDataMap(Lcom/google/android/gms/wearable/DataMap;)V
    .locals 4
    .param p1, "mapToMutate"    # Lcom/google/android/gms/wearable/DataMap;

    .prologue
    .line 36
    const-string v0, "timestamp"

    iget-wide v2, p0, Lcom/google/android/wearable/datatransfer/internal/SharedBatteryStatus;->timestamp:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/android/gms/wearable/DataMap;->putLong(Ljava/lang/String;J)V

    .line 37
    const-string v0, "batteryLow"

    iget-boolean v1, p0, Lcom/google/android/wearable/datatransfer/internal/SharedBatteryStatus;->batteryLow:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/android/gms/wearable/DataMap;->putBoolean(Ljava/lang/String;Z)V

    .line 38
    return-void
.end method
