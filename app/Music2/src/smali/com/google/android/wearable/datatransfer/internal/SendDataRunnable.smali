.class final Lcom/google/android/wearable/datatransfer/internal/SendDataRunnable;
.super Ljava/lang/Object;
.source "SendDataRunnable.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final mCallbacks:Lcom/google/android/wearable/datatransfer/internal/DataSender$Callbacks;

.field private final mClient:Lcom/google/android/gms/common/api/GoogleApiClient;

.field private mFileDescriptorOpener:Lcom/google/android/wearable/datatransfer/internal/FileDescriptorOpener;

.field private final mMessageEvent:Lcom/google/android/gms/wearable/MessageEvent;

.field private final mServerConnectionId:J


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/api/GoogleApiClient;Lcom/google/android/wearable/datatransfer/internal/FileDescriptorOpener;Lcom/google/android/gms/wearable/MessageEvent;JLcom/google/android/wearable/datatransfer/internal/DataSender$Callbacks;)V
    .locals 2
    .param p1, "client"    # Lcom/google/android/gms/common/api/GoogleApiClient;
    .param p2, "fileDescriptorOpener"    # Lcom/google/android/wearable/datatransfer/internal/FileDescriptorOpener;
    .param p3, "messageEvent"    # Lcom/google/android/gms/wearable/MessageEvent;
    .param p4, "serverConnectionId"    # J
    .param p6, "callbacks"    # Lcom/google/android/wearable/datatransfer/internal/DataSender$Callbacks;

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    const-string v0, "callbacks"

    invoke-static {p6, v0}, Lcom/google/android/wearable/datatransfer/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/wearable/datatransfer/internal/DataSender$Callbacks;

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/SendDataRunnable;->mCallbacks:Lcom/google/android/wearable/datatransfer/internal/DataSender$Callbacks;

    .line 62
    const-string v0, "client"

    invoke-static {p1, v0}, Lcom/google/android/wearable/datatransfer/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/GoogleApiClient;

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/SendDataRunnable;->mClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    .line 63
    const-string v0, "fileDescriptorOpener"

    invoke-static {p2, v0}, Lcom/google/android/wearable/datatransfer/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/wearable/datatransfer/internal/FileDescriptorOpener;

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/SendDataRunnable;->mFileDescriptorOpener:Lcom/google/android/wearable/datatransfer/internal/FileDescriptorOpener;

    .line 64
    const-string v0, "messageEvent"

    invoke-static {p3, v0}, Lcom/google/android/wearable/datatransfer/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wearable/MessageEvent;

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/SendDataRunnable;->mMessageEvent:Lcom/google/android/gms/wearable/MessageEvent;

    .line 65
    iput-wide p4, p0, Lcom/google/android/wearable/datatransfer/internal/SendDataRunnable;->mServerConnectionId:J

    .line 66
    return-void
.end method

.method private sendConnectionRefused(Lcom/google/android/wearable/datatransfer/internal/OpenConnectionPacket;II)V
    .locals 8
    .param p1, "openConnectionPacket"    # Lcom/google/android/wearable/datatransfer/internal/OpenConnectionPacket;
    .param p2, "statusCode"    # I
    .param p3, "appStatusCode"    # I

    .prologue
    .line 144
    invoke-static {}, Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;->newPacket()Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;

    move-result-object v1

    .line 145
    .local v1, "responsePacket":Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;
    iget-object v2, p1, Lcom/google/android/wearable/datatransfer/internal/OpenConnectionPacket;->proto:Lcom/google/android/wearable/datatransfer/proto/PacketProtos$OpenConnectionPacket;

    iget-wide v2, v2, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$OpenConnectionPacket;->clientConnectionId:J

    iget-wide v4, p0, Lcom/google/android/wearable/datatransfer/internal/SendDataRunnable;->mServerConnectionId:J

    move v6, p2

    move v7, p3

    invoke-virtual/range {v1 .. v7}, Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;->mutateToConnectionRefusedPacket(JJII)V

    .line 150
    sget-object v2, Lcom/google/android/gms/wearable/Wearable;->MessageApi:Lcom/google/android/gms/wearable/MessageApi;

    iget-object v3, p0, Lcom/google/android/wearable/datatransfer/internal/SendDataRunnable;->mClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    iget-object v4, p0, Lcom/google/android/wearable/datatransfer/internal/SendDataRunnable;->mMessageEvent:Lcom/google/android/gms/wearable/MessageEvent;

    invoke-interface {v4}, Lcom/google/android/gms/wearable/MessageEvent;->getSourceNodeId()Ljava/lang/String;

    move-result-object v4

    const-string v5, "com.google.android.wearable.datatransfer.DATA_TRANSFER"

    const/4 v6, 0x0

    invoke-virtual {v1, v6}, Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;->toBytes([B)[B

    move-result-object v6

    invoke-interface {v2, v3, v4, v5, v6}, Lcom/google/android/gms/wearable/MessageApi;->sendMessage(Lcom/google/android/gms/common/api/GoogleApiClient;Ljava/lang/String;Ljava/lang/String;[B)Lcom/google/android/gms/common/api/PendingResult;

    move-result-object v2

    const-wide/16 v4, 0x2710

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v2, v4, v5, v3}, Lcom/google/android/gms/common/api/PendingResult;->await(JLjava/util/concurrent/TimeUnit;)Lcom/google/android/gms/common/api/Result;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wearable/MessageApi$SendMessageResult;

    .line 156
    .local v0, "sendMessageResult":Lcom/google/android/gms/wearable/MessageApi$SendMessageResult;
    invoke-interface {v0}, Lcom/google/android/gms/wearable/MessageApi$SendMessageResult;->getStatus()Lcom/google/android/gms/common/api/Status;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/common/api/Status;->isSuccess()Z

    move-result v2

    if-nez v2, :cond_0

    .line 157
    const-string v2, "SendDataRunnable"

    const-string v3, "Failed to send CONNECTION_REFUSED packet"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 159
    :cond_0
    return-void
.end method


# virtual methods
.method public run()V
    .locals 15

    .prologue
    .line 70
    const/4 v14, 0x0

    .line 74
    .local v14, "sender":Lcom/google/android/wearable/datatransfer/internal/DataSender;
    :try_start_0
    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/internal/SendDataRunnable;->mMessageEvent:Lcom/google/android/gms/wearable/MessageEvent;

    invoke-interface {v1}, Lcom/google/android/gms/wearable/MessageEvent;->getData()[B

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/wearable/datatransfer/internal/OpenConnectionPacket;->fromBytes([B)Lcom/google/android/wearable/datatransfer/internal/OpenConnectionPacket;
    :try_end_0
    .catch Lcom/google/android/wearable/datatransfer/internal/InvalidRequestException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v13

    .line 82
    .local v13, "packet":Lcom/google/android/wearable/datatransfer/internal/OpenConnectionPacket;
    :try_start_1
    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/internal/SendDataRunnable;->mFileDescriptorOpener:Lcom/google/android/wearable/datatransfer/internal/FileDescriptorOpener;

    iget-object v2, v13, Lcom/google/android/wearable/datatransfer/internal/OpenConnectionPacket;->proto:Lcom/google/android/wearable/datatransfer/proto/PacketProtos$OpenConnectionPacket;

    iget-object v2, v2, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$OpenConnectionPacket;->path:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/wearable/datatransfer/internal/SendDataRunnable;->mMessageEvent:Lcom/google/android/gms/wearable/MessageEvent;

    invoke-interface {v4}, Lcom/google/android/gms/wearable/MessageEvent;->getSourceNodeId()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v2, v4}, Lcom/google/android/wearable/datatransfer/internal/FileDescriptorOpener;->openFileDescriptor(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/wearable/datatransfer/ConnectionService$OpenFileDescriptorResult;

    move-result-object v12

    .line 87
    .local v12, "openFileDescriptorResult":Lcom/google/android/wearable/datatransfer/ConnectionService$OpenFileDescriptorResult;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/wearable/datatransfer/internal/SendDataRunnable;->mFileDescriptorOpener:Lcom/google/android/wearable/datatransfer/internal/FileDescriptorOpener;

    .line 90
    if-nez v12, :cond_0

    .line 91
    const-string v1, "SendDataRunnable"

    const-string v2, "Implementation of openFileDescriptor returned null"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/google/android/wearable/datatransfer/ConnectionService$OpenFileDescriptorResult;->forFailure(I)Lcom/google/android/wearable/datatransfer/ConnectionService$OpenFileDescriptorResult;

    move-result-object v12

    .line 95
    :cond_0
    iget-object v3, v12, Lcom/google/android/wearable/datatransfer/ConnectionService$OpenFileDescriptorResult;->parcelFileDescriptor:Landroid/os/ParcelFileDescriptor;

    .line 96
    .local v3, "fd":Landroid/os/ParcelFileDescriptor;
    if-nez v3, :cond_2

    .line 97
    const-string v1, "SendDataRunnable"

    const-string v2, "openFileDescriptor returned error"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    iget v1, v12, Lcom/google/android/wearable/datatransfer/ConnectionService$OpenFileDescriptorResult;->statusCode:I

    iget v2, v12, Lcom/google/android/wearable/datatransfer/ConnectionService$OpenFileDescriptorResult;->appStatusCode:I

    invoke-direct {p0, v13, v1, v2}, Lcom/google/android/wearable/datatransfer/internal/SendDataRunnable;->sendConnectionRefused(Lcom/google/android/wearable/datatransfer/internal/OpenConnectionPacket;II)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 134
    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/internal/SendDataRunnable;->mCallbacks:Lcom/google/android/wearable/datatransfer/internal/DataSender$Callbacks;

    if-eqz v1, :cond_1

    .line 135
    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/internal/SendDataRunnable;->mCallbacks:Lcom/google/android/wearable/datatransfer/internal/DataSender$Callbacks;

    iget-wide v4, p0, Lcom/google/android/wearable/datatransfer/internal/SendDataRunnable;->mServerConnectionId:J

    invoke-interface {v1, v4, v5}, Lcom/google/android/wearable/datatransfer/internal/DataSender$Callbacks;->onConnectionTerminated(J)V

    .line 138
    .end local v3    # "fd":Landroid/os/ParcelFileDescriptor;
    .end local v12    # "openFileDescriptorResult":Lcom/google/android/wearable/datatransfer/ConnectionService$OpenFileDescriptorResult;
    .end local v13    # "packet":Lcom/google/android/wearable/datatransfer/internal/OpenConnectionPacket;
    :cond_1
    :goto_0
    return-void

    .line 75
    :catch_0
    move-exception v0

    .line 76
    .local v0, "e":Lcom/google/android/wearable/datatransfer/internal/InvalidRequestException;
    :try_start_2
    const-string v1, "SendDataRunnable"

    const-string v2, "Bad OPEN_CONNECTION packet received"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 134
    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/internal/SendDataRunnable;->mCallbacks:Lcom/google/android/wearable/datatransfer/internal/DataSender$Callbacks;

    if-eqz v1, :cond_1

    .line 135
    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/internal/SendDataRunnable;->mCallbacks:Lcom/google/android/wearable/datatransfer/internal/DataSender$Callbacks;

    iget-wide v4, p0, Lcom/google/android/wearable/datatransfer/internal/SendDataRunnable;->mServerConnectionId:J

    invoke-interface {v1, v4, v5}, Lcom/google/android/wearable/datatransfer/internal/DataSender$Callbacks;->onConnectionTerminated(J)V

    goto :goto_0

    .line 106
    .end local v0    # "e":Lcom/google/android/wearable/datatransfer/internal/InvalidRequestException;
    .restart local v3    # "fd":Landroid/os/ParcelFileDescriptor;
    .restart local v12    # "openFileDescriptorResult":Lcom/google/android/wearable/datatransfer/ConnectionService$OpenFileDescriptorResult;
    .restart local v13    # "packet":Lcom/google/android/wearable/datatransfer/internal/OpenConnectionPacket;
    :cond_2
    :try_start_3
    invoke-virtual {v3}, Landroid/os/ParcelFileDescriptor;->getStatSize()J
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-wide v6

    .line 109
    .local v6, "statSize":J
    :try_start_4
    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/internal/SendDataRunnable;->mClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    iget-object v2, p0, Lcom/google/android/wearable/datatransfer/internal/SendDataRunnable;->mMessageEvent:Lcom/google/android/gms/wearable/MessageEvent;

    iget-object v4, v13, Lcom/google/android/wearable/datatransfer/internal/OpenConnectionPacket;->proto:Lcom/google/android/wearable/datatransfer/proto/PacketProtos$OpenConnectionPacket;

    iget-wide v4, v4, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$OpenConnectionPacket;->offset:J

    iget-wide v8, p0, Lcom/google/android/wearable/datatransfer/internal/SendDataRunnable;->mServerConnectionId:J

    iget-object v10, v13, Lcom/google/android/wearable/datatransfer/internal/OpenConnectionPacket;->proto:Lcom/google/android/wearable/datatransfer/proto/PacketProtos$OpenConnectionPacket;

    iget-wide v10, v10, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$OpenConnectionPacket;->clientConnectionId:J

    invoke-static/range {v1 .. v11}, Lcom/google/android/wearable/datatransfer/internal/DataSender;->forOpenConnectionRequest(Lcom/google/android/gms/common/api/GoogleApiClient;Lcom/google/android/gms/wearable/MessageEvent;Landroid/os/ParcelFileDescriptor;JJJJ)Lcom/google/android/wearable/datatransfer/internal/DataSender;

    move-result-object v14

    .line 117
    const-string v1, "SendDataRunnable"

    invoke-static {v1}, Lcom/google/android/wearable/datatransfer/internal/DebugLog;->isLoggable(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 118
    const-string v1, "SendDataRunnable"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "sender created for: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p0, Lcom/google/android/wearable/datatransfer/internal/SendDataRunnable;->mServerConnectionId:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    :cond_3
    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/internal/SendDataRunnable;->mCallbacks:Lcom/google/android/wearable/datatransfer/internal/DataSender$Callbacks;

    if-eqz v1, :cond_4

    .line 121
    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/internal/SendDataRunnable;->mCallbacks:Lcom/google/android/wearable/datatransfer/internal/DataSender$Callbacks;

    iget-wide v4, p0, Lcom/google/android/wearable/datatransfer/internal/SendDataRunnable;->mServerConnectionId:J

    invoke-interface {v1, v4, v5, v14}, Lcom/google/android/wearable/datatransfer/internal/DataSender$Callbacks;->onSendStarted(JLcom/google/android/wearable/datatransfer/internal/DataSender;)V

    .line 123
    :cond_4
    invoke-virtual {v14}, Lcom/google/android/wearable/datatransfer/internal/DataSender;->send()V
    :try_end_4
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 128
    :try_start_5
    invoke-virtual {v3}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 134
    :goto_1
    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/internal/SendDataRunnable;->mCallbacks:Lcom/google/android/wearable/datatransfer/internal/DataSender$Callbacks;

    if-eqz v1, :cond_1

    .line 135
    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/internal/SendDataRunnable;->mCallbacks:Lcom/google/android/wearable/datatransfer/internal/DataSender$Callbacks;

    iget-wide v4, p0, Lcom/google/android/wearable/datatransfer/internal/SendDataRunnable;->mServerConnectionId:J

    invoke-interface {v1, v4, v5}, Lcom/google/android/wearable/datatransfer/internal/DataSender$Callbacks;->onConnectionTerminated(J)V

    goto :goto_0

    .line 129
    :catch_1
    move-exception v0

    .line 130
    .local v0, "e":Ljava/io/IOException;
    :try_start_6
    const-string v1, "SendDataRunnable"

    const-string v2, "Failed to close file descriptor"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_1

    .line 134
    .end local v0    # "e":Ljava/io/IOException;
    .end local v3    # "fd":Landroid/os/ParcelFileDescriptor;
    .end local v6    # "statSize":J
    .end local v12    # "openFileDescriptorResult":Lcom/google/android/wearable/datatransfer/ConnectionService$OpenFileDescriptorResult;
    .end local v13    # "packet":Lcom/google/android/wearable/datatransfer/internal/OpenConnectionPacket;
    :catchall_0
    move-exception v1

    iget-object v2, p0, Lcom/google/android/wearable/datatransfer/internal/SendDataRunnable;->mCallbacks:Lcom/google/android/wearable/datatransfer/internal/DataSender$Callbacks;

    if-eqz v2, :cond_5

    .line 135
    iget-object v2, p0, Lcom/google/android/wearable/datatransfer/internal/SendDataRunnable;->mCallbacks:Lcom/google/android/wearable/datatransfer/internal/DataSender$Callbacks;

    iget-wide v4, p0, Lcom/google/android/wearable/datatransfer/internal/SendDataRunnable;->mServerConnectionId:J

    invoke-interface {v2, v4, v5}, Lcom/google/android/wearable/datatransfer/internal/DataSender$Callbacks;->onConnectionTerminated(J)V

    :cond_5
    throw v1

    .line 124
    .restart local v3    # "fd":Landroid/os/ParcelFileDescriptor;
    .restart local v6    # "statSize":J
    .restart local v12    # "openFileDescriptorResult":Lcom/google/android/wearable/datatransfer/ConnectionService$OpenFileDescriptorResult;
    .restart local v13    # "packet":Lcom/google/android/wearable/datatransfer/internal/OpenConnectionPacket;
    :catch_2
    move-exception v0

    .line 125
    .local v0, "e":Ljava/lang/RuntimeException;
    :try_start_7
    const-string v1, "SendDataRunnable"

    const-string v2, "Sending file failed."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 128
    :try_start_8
    invoke-virtual {v3}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_1

    .line 129
    :catch_3
    move-exception v0

    .line 130
    .local v0, "e":Ljava/io/IOException;
    :try_start_9
    const-string v1, "SendDataRunnable"

    const-string v2, "Failed to close file descriptor"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto :goto_1

    .line 127
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_1
    move-exception v1

    .line 128
    :try_start_a
    invoke-virtual {v3}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_4
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 131
    :goto_2
    :try_start_b
    throw v1

    .line 129
    :catch_4
    move-exception v0

    .line 130
    .restart local v0    # "e":Ljava/io/IOException;
    const-string v2, "SendDataRunnable"

    const-string v4, "Failed to close file descriptor"

    invoke-static {v2, v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    goto :goto_2
.end method
