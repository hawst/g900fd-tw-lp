.class interface abstract Lcom/google/android/wearable/datatransfer/internal/SharedBatteryStatusUpdater;
.super Ljava/lang/Object;
.source "SharedBatteryStatusUpdater.java"


# virtual methods
.method public abstract getAllBatteryStatuses(Lcom/google/android/gms/common/api/GoogleApiClient;)Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/common/api/GoogleApiClient;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/wearable/datatransfer/internal/SharedBatteryStatus;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation
.end method

.method public abstract publishBatteryStatus(Lcom/google/android/gms/common/api/GoogleApiClient;Ljava/lang/String;)V
.end method

.method public abstract sendBatteryStatusPoke(Lcom/google/android/gms/common/api/GoogleApiClient;Ljava/lang/String;)V
.end method
