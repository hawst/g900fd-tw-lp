.class public final Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket$FirstPacketParameters;
.super Lcom/google/protobuf/nano/MessageNano;
.source "PacketProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "FirstPacketParameters"
.end annotation


# instance fields
.field public transferSize:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 145
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 146
    invoke-virtual {p0}, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket$FirstPacketParameters;->clear()Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket$FirstPacketParameters;

    .line 147
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket$FirstPacketParameters;
    .locals 2

    .prologue
    .line 150
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket$FirstPacketParameters;->transferSize:J

    .line 151
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket$FirstPacketParameters;->cachedSize:I

    .line 152
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 6

    .prologue
    .line 166
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 167
    .local v0, "size":I
    iget-wide v2, p0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket$FirstPacketParameters;->transferSize:J

    const-wide/16 v4, -0x1

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 168
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket$FirstPacketParameters;->transferSize:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 171
    :cond_0
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket$FirstPacketParameters;
    .locals 4
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 179
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 180
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 184
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 185
    :sswitch_0
    return-object p0

    .line 190
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket$FirstPacketParameters;->transferSize:J

    goto :goto_0

    .line 180
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 125
    invoke-virtual {p0, p1}, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket$FirstPacketParameters;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket$FirstPacketParameters;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 158
    iget-wide v0, p0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket$FirstPacketParameters;->transferSize:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 159
    const/4 v0, 0x1

    iget-wide v2, p0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket$FirstPacketParameters;->transferSize:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 161
    :cond_0
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 162
    return-void
.end method
