.class final Lcom/google/android/wearable/datatransfer/internal/DataSyncDatabase$QueryGetRequest;
.super Ljava/lang/Object;
.source "DataSyncDatabase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/wearable/datatransfer/internal/DataSyncDatabase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "QueryGetRequest"
.end annotation


# static fields
.field static final PROJECTION:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 412
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "remoteNodeId"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "remotePath"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "retryCount"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/wearable/datatransfer/internal/DataSyncDatabase$QueryGetRequest;->PROJECTION:[Ljava/lang/String;

    return-void
.end method
