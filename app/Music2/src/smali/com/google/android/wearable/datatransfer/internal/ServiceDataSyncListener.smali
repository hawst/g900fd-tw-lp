.class final Lcom/google/android/wearable/datatransfer/internal/ServiceDataSyncListener;
.super Ljava/lang/Object;
.source "ServiceDataSyncListener.java"

# interfaces
.implements Lcom/google/android/wearable/datatransfer/internal/DataSyncListener;


# instance fields
.field private final mClient:Lcom/google/android/wearable/datatransfer/WearableDataApiClient;


# direct methods
.method public constructor <init>(Lcom/google/android/wearable/datatransfer/WearableDataApiClient;)V
    .locals 1
    .param p1, "client"    # Lcom/google/android/wearable/datatransfer/WearableDataApiClient;

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    const-string v0, "client"

    invoke-static {p1, v0}, Lcom/google/android/wearable/datatransfer/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/wearable/datatransfer/WearableDataApiClient;

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/ServiceDataSyncListener;->mClient:Lcom/google/android/wearable/datatransfer/WearableDataApiClient;

    .line 43
    return-void
.end method

.method private getDataSyncListenerIntent()Landroid/content/Intent;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 170
    new-instance v1, Landroid/content/Intent;

    const-string v3, "com.google.android.wearable.datatransfer.DATASYNC_LISTENER"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 172
    .local v1, "intent":Landroid/content/Intent;
    :try_start_0
    iget-object v3, p0, Lcom/google/android/wearable/datatransfer/internal/ServiceDataSyncListener;->mClient:Lcom/google/android/wearable/datatransfer/WearableDataApiClient;

    invoke-virtual {v3}, Lcom/google/android/wearable/datatransfer/WearableDataApiClient;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v1}, Lcom/google/android/wearable/datatransfer/internal/Utils;->setServiceComponentName(Landroid/content/Context;Landroid/content/Intent;)V
    :try_end_0
    .catch Lcom/google/android/wearable/datatransfer/internal/ComponentNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/wearable/datatransfer/internal/AmbiguousComponentException; {:try_start_0 .. :try_end_0} :catch_1

    .line 179
    .end local v1    # "intent":Landroid/content/Intent;
    :goto_0
    return-object v1

    .line 174
    .restart local v1    # "intent":Landroid/content/Intent;
    :catch_0
    move-exception v0

    .local v0, "e":Lcom/google/android/wearable/datatransfer/internal/ComponentNotFoundException;
    move-object v1, v2

    .line 176
    goto :goto_0

    .line 177
    .end local v0    # "e":Lcom/google/android/wearable/datatransfer/internal/ComponentNotFoundException;
    :catch_1
    move-exception v0

    .local v0, "e":Lcom/google/android/wearable/datatransfer/internal/AmbiguousComponentException;
    move-object v1, v2

    .line 179
    goto :goto_0
.end method

.method private getListenerService(Lcom/google/android/wearable/datatransfer/internal/BlockingServiceConnection;)Lcom/google/android/wearable/datatransfer/internal/DataSyncListener;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/wearable/datatransfer/internal/BlockingServiceConnection",
            "<",
            "Lcom/google/android/wearable/datatransfer/internal/DataSyncListener;",
            ">;)",
            "Lcom/google/android/wearable/datatransfer/internal/DataSyncListener;"
        }
    .end annotation

    .prologue
    .local p1, "serviceConnection":Lcom/google/android/wearable/datatransfer/internal/BlockingServiceConnection;, "Lcom/google/android/wearable/datatransfer/internal/BlockingServiceConnection<Lcom/google/android/wearable/datatransfer/internal/DataSyncListener;>;"
    const/4 v2, 0x0

    .line 155
    const-wide/16 v4, 0x3e8

    :try_start_0
    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1, v4, v5, v1}, Lcom/google/android/wearable/datatransfer/internal/BlockingServiceConnection;->getBinder(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/wearable/datatransfer/internal/DataSyncListener;
    :try_end_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    .line 162
    :goto_0
    return-object v1

    .line 156
    :catch_0
    move-exception v0

    .line 157
    .local v0, "e":Ljava/util/concurrent/TimeoutException;
    const-string v1, "ServiceDataSyncListener"

    const-string v3, "timeout while waiting for service"

    invoke-static {v1, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v2

    .line 158
    goto :goto_0

    .line 159
    .end local v0    # "e":Ljava/util/concurrent/TimeoutException;
    :catch_1
    move-exception v0

    .line 160
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    .line 161
    const-string v1, "ServiceDataSyncListener"

    const-string v3, "interrupted while waiting for service"

    invoke-static {v1, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v2

    .line 162
    goto :goto_0
.end method

.method private getListenerServiceConnection()Lcom/google/android/wearable/datatransfer/internal/BlockingServiceConnection;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/wearable/datatransfer/internal/BlockingServiceConnection",
            "<",
            "Lcom/google/android/wearable/datatransfer/internal/DataSyncListener;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 131
    invoke-direct {p0}, Lcom/google/android/wearable/datatransfer/internal/ServiceDataSyncListener;->getDataSyncListenerIntent()Landroid/content/Intent;

    move-result-object v0

    .line 132
    .local v0, "intent":Landroid/content/Intent;
    if-nez v0, :cond_1

    move-object v1, v2

    .line 141
    :cond_0
    :goto_0
    return-object v1

    .line 135
    :cond_1
    new-instance v1, Lcom/google/android/wearable/datatransfer/internal/BlockingServiceConnection;

    invoke-direct {v1}, Lcom/google/android/wearable/datatransfer/internal/BlockingServiceConnection;-><init>()V

    .line 137
    .local v1, "serviceConnection":Lcom/google/android/wearable/datatransfer/internal/BlockingServiceConnection;, "Lcom/google/android/wearable/datatransfer/internal/BlockingServiceConnection<Lcom/google/android/wearable/datatransfer/internal/DataSyncListener;>;"
    iget-object v3, p0, Lcom/google/android/wearable/datatransfer/internal/ServiceDataSyncListener;->mClient:Lcom/google/android/wearable/datatransfer/WearableDataApiClient;

    invoke-virtual {v3}, Lcom/google/android/wearable/datatransfer/WearableDataApiClient;->getContext()Landroid/content/Context;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v0, v1, v4}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v3

    if-nez v3, :cond_0

    .line 140
    const-string v3, "ServiceDataSyncListener"

    const-string v4, "cannot connect to service"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v2

    .line 141
    goto :goto_0
.end method

.method private startDataSyncService()V
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/ServiceDataSyncListener;->mClient:Lcom/google/android/wearable/datatransfer/WearableDataApiClient;

    invoke-virtual {v0}, Lcom/google/android/wearable/datatransfer/WearableDataApiClient;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->startService(Landroid/content/Context;)V

    .line 121
    return-void
.end method


# virtual methods
.method public onDataSyncCompleted(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)V
    .locals 4
    .param p1, "nodeId"    # Ljava/lang/String;
    .param p2, "path"    # Ljava/lang/String;
    .param p3, "localTargetFile"    # Ljava/io/File;

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/google/android/wearable/datatransfer/internal/ServiceDataSyncListener;->getListenerServiceConnection()Lcom/google/android/wearable/datatransfer/internal/BlockingServiceConnection;

    move-result-object v1

    .line 49
    .local v1, "serviceConnection":Lcom/google/android/wearable/datatransfer/internal/BlockingServiceConnection;, "Lcom/google/android/wearable/datatransfer/internal/BlockingServiceConnection<Lcom/google/android/wearable/datatransfer/internal/DataSyncListener;>;"
    if-eqz v1, :cond_1

    .line 51
    :try_start_0
    invoke-direct {p0, v1}, Lcom/google/android/wearable/datatransfer/internal/ServiceDataSyncListener;->getListenerService(Lcom/google/android/wearable/datatransfer/internal/BlockingServiceConnection;)Lcom/google/android/wearable/datatransfer/internal/DataSyncListener;

    move-result-object v0

    .line 52
    .local v0, "listener":Lcom/google/android/wearable/datatransfer/internal/DataSyncListener;
    if-eqz v0, :cond_0

    .line 53
    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/wearable/datatransfer/internal/DataSyncListener;->onDataSyncCompleted(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 56
    :cond_0
    iget-object v2, p0, Lcom/google/android/wearable/datatransfer/internal/ServiceDataSyncListener;->mClient:Lcom/google/android/wearable/datatransfer/WearableDataApiClient;

    invoke-virtual {v2}, Lcom/google/android/wearable/datatransfer/WearableDataApiClient;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 59
    .end local v0    # "listener":Lcom/google/android/wearable/datatransfer/internal/DataSyncListener;
    :cond_1
    invoke-direct {p0}, Lcom/google/android/wearable/datatransfer/internal/ServiceDataSyncListener;->startDataSyncService()V

    .line 60
    return-void

    .line 56
    :catchall_0
    move-exception v2

    iget-object v3, p0, Lcom/google/android/wearable/datatransfer/internal/ServiceDataSyncListener;->mClient:Lcom/google/android/wearable/datatransfer/WearableDataApiClient;

    invoke-virtual {v3}, Lcom/google/android/wearable/datatransfer/WearableDataApiClient;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    throw v2
.end method

.method public onDataSyncFailed(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;II)V
    .locals 7
    .param p1, "nodeId"    # Ljava/lang/String;
    .param p2, "path"    # Ljava/lang/String;
    .param p3, "localTargetFile"    # Ljava/io/File;
    .param p4, "commonStatusCode"    # I
    .param p5, "appStatusCode"    # I

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/google/android/wearable/datatransfer/internal/ServiceDataSyncListener;->getListenerServiceConnection()Lcom/google/android/wearable/datatransfer/internal/BlockingServiceConnection;

    move-result-object v6

    .line 71
    .local v6, "serviceConnection":Lcom/google/android/wearable/datatransfer/internal/BlockingServiceConnection;, "Lcom/google/android/wearable/datatransfer/internal/BlockingServiceConnection<Lcom/google/android/wearable/datatransfer/internal/DataSyncListener;>;"
    if-eqz v6, :cond_1

    .line 73
    :try_start_0
    invoke-direct {p0, v6}, Lcom/google/android/wearable/datatransfer/internal/ServiceDataSyncListener;->getListenerService(Lcom/google/android/wearable/datatransfer/internal/BlockingServiceConnection;)Lcom/google/android/wearable/datatransfer/internal/DataSyncListener;

    move-result-object v0

    .line 74
    .local v0, "listener":Lcom/google/android/wearable/datatransfer/internal/DataSyncListener;
    if-eqz v0, :cond_0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    .line 75
    invoke-interface/range {v0 .. v5}, Lcom/google/android/wearable/datatransfer/internal/DataSyncListener;->onDataSyncFailed(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;II)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 79
    :cond_0
    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/internal/ServiceDataSyncListener;->mClient:Lcom/google/android/wearable/datatransfer/WearableDataApiClient;

    invoke-virtual {v1}, Lcom/google/android/wearable/datatransfer/WearableDataApiClient;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 82
    .end local v0    # "listener":Lcom/google/android/wearable/datatransfer/internal/DataSyncListener;
    :cond_1
    invoke-direct {p0}, Lcom/google/android/wearable/datatransfer/internal/ServiceDataSyncListener;->startDataSyncService()V

    .line 83
    return-void

    .line 79
    :catchall_0
    move-exception v1

    iget-object v2, p0, Lcom/google/android/wearable/datatransfer/internal/ServiceDataSyncListener;->mClient:Lcom/google/android/wearable/datatransfer/WearableDataApiClient;

    invoke-virtual {v2}, Lcom/google/android/wearable/datatransfer/WearableDataApiClient;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    throw v1
.end method

.method public onSyncingStarted()V
    .locals 4

    .prologue
    .line 87
    invoke-direct {p0}, Lcom/google/android/wearable/datatransfer/internal/ServiceDataSyncListener;->getListenerServiceConnection()Lcom/google/android/wearable/datatransfer/internal/BlockingServiceConnection;

    move-result-object v1

    .line 89
    .local v1, "serviceConnection":Lcom/google/android/wearable/datatransfer/internal/BlockingServiceConnection;, "Lcom/google/android/wearable/datatransfer/internal/BlockingServiceConnection<Lcom/google/android/wearable/datatransfer/internal/DataSyncListener;>;"
    if-eqz v1, :cond_1

    .line 91
    :try_start_0
    invoke-direct {p0, v1}, Lcom/google/android/wearable/datatransfer/internal/ServiceDataSyncListener;->getListenerService(Lcom/google/android/wearable/datatransfer/internal/BlockingServiceConnection;)Lcom/google/android/wearable/datatransfer/internal/DataSyncListener;

    move-result-object v0

    .line 92
    .local v0, "listener":Lcom/google/android/wearable/datatransfer/internal/DataSyncListener;
    if-eqz v0, :cond_0

    .line 93
    invoke-interface {v0}, Lcom/google/android/wearable/datatransfer/internal/DataSyncListener;->onSyncingStarted()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 96
    :cond_0
    iget-object v2, p0, Lcom/google/android/wearable/datatransfer/internal/ServiceDataSyncListener;->mClient:Lcom/google/android/wearable/datatransfer/WearableDataApiClient;

    invoke-virtual {v2}, Lcom/google/android/wearable/datatransfer/WearableDataApiClient;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 100
    .end local v0    # "listener":Lcom/google/android/wearable/datatransfer/internal/DataSyncListener;
    :cond_1
    return-void

    .line 96
    :catchall_0
    move-exception v2

    iget-object v3, p0, Lcom/google/android/wearable/datatransfer/internal/ServiceDataSyncListener;->mClient:Lcom/google/android/wearable/datatransfer/WearableDataApiClient;

    invoke-virtual {v3}, Lcom/google/android/wearable/datatransfer/WearableDataApiClient;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    throw v2
.end method

.method public onSyncingStopped(I)V
    .locals 4
    .param p1, "syncStateFlags"    # I

    .prologue
    .line 104
    invoke-direct {p0}, Lcom/google/android/wearable/datatransfer/internal/ServiceDataSyncListener;->getListenerServiceConnection()Lcom/google/android/wearable/datatransfer/internal/BlockingServiceConnection;

    move-result-object v1

    .line 106
    .local v1, "serviceConnection":Lcom/google/android/wearable/datatransfer/internal/BlockingServiceConnection;, "Lcom/google/android/wearable/datatransfer/internal/BlockingServiceConnection<Lcom/google/android/wearable/datatransfer/internal/DataSyncListener;>;"
    if-eqz v1, :cond_1

    .line 108
    :try_start_0
    invoke-direct {p0, v1}, Lcom/google/android/wearable/datatransfer/internal/ServiceDataSyncListener;->getListenerService(Lcom/google/android/wearable/datatransfer/internal/BlockingServiceConnection;)Lcom/google/android/wearable/datatransfer/internal/DataSyncListener;

    move-result-object v0

    .line 109
    .local v0, "listener":Lcom/google/android/wearable/datatransfer/internal/DataSyncListener;
    if-eqz v0, :cond_0

    .line 110
    invoke-interface {v0, p1}, Lcom/google/android/wearable/datatransfer/internal/DataSyncListener;->onSyncingStopped(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 113
    :cond_0
    iget-object v2, p0, Lcom/google/android/wearable/datatransfer/internal/ServiceDataSyncListener;->mClient:Lcom/google/android/wearable/datatransfer/WearableDataApiClient;

    invoke-virtual {v2}, Lcom/google/android/wearable/datatransfer/WearableDataApiClient;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 117
    .end local v0    # "listener":Lcom/google/android/wearable/datatransfer/internal/DataSyncListener;
    :cond_1
    return-void

    .line 113
    :catchall_0
    move-exception v2

    iget-object v3, p0, Lcom/google/android/wearable/datatransfer/internal/ServiceDataSyncListener;->mClient:Lcom/google/android/wearable/datatransfer/WearableDataApiClient;

    invoke-virtual {v3}, Lcom/google/android/wearable/datatransfer/WearableDataApiClient;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    throw v2
.end method
