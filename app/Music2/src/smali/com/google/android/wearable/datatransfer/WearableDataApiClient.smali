.class public final Lcom/google/android/wearable/datatransfer/WearableDataApiClient;
.super Ljava/lang/Object;
.source "WearableDataApiClient.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/wearable/datatransfer/WearableDataApiClient$Event;,
        Lcom/google/android/wearable/datatransfer/WearableDataApiClient$StopAllServerConnectionsEvent;,
        Lcom/google/android/wearable/datatransfer/WearableDataApiClient$OpenConnectionRequest;
    }
.end annotation


# instance fields
.field private volatile mConnecting:Z

.field private final mConnection:Landroid/content/ServiceConnection;

.field private final mContext:Landroid/content/Context;

.field private mDatabase:Lcom/google/android/wearable/datatransfer/internal/DataSyncDatabase;

.field private final mPendingEvents:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lcom/google/android/wearable/datatransfer/WearableDataApiClient$Event;",
            ">;"
        }
    .end annotation
.end field

.field private mService:Lcom/google/android/wearable/datatransfer/ConnectionService$ConnectionServiceBinder;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/WearableDataApiClient;->mPendingEvents:Ljava/util/Queue;

    .line 72
    new-instance v0, Lcom/google/android/wearable/datatransfer/WearableDataApiClient$1;

    invoke-direct {v0, p0}, Lcom/google/android/wearable/datatransfer/WearableDataApiClient$1;-><init>(Lcom/google/android/wearable/datatransfer/WearableDataApiClient;)V

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/WearableDataApiClient;->mConnection:Landroid/content/ServiceConnection;

    .line 120
    iput-object p1, p0, Lcom/google/android/wearable/datatransfer/WearableDataApiClient;->mContext:Landroid/content/Context;

    .line 121
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/wearable/datatransfer/WearableDataApiClient;)Lcom/google/android/wearable/datatransfer/ConnectionService$ConnectionServiceBinder;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/wearable/datatransfer/WearableDataApiClient;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/WearableDataApiClient;->mService:Lcom/google/android/wearable/datatransfer/ConnectionService$ConnectionServiceBinder;

    return-object v0
.end method

.method static synthetic access$002(Lcom/google/android/wearable/datatransfer/WearableDataApiClient;Lcom/google/android/wearable/datatransfer/ConnectionService$ConnectionServiceBinder;)Lcom/google/android/wearable/datatransfer/ConnectionService$ConnectionServiceBinder;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/wearable/datatransfer/WearableDataApiClient;
    .param p1, "x1"    # Lcom/google/android/wearable/datatransfer/ConnectionService$ConnectionServiceBinder;

    .prologue
    .line 65
    iput-object p1, p0, Lcom/google/android/wearable/datatransfer/WearableDataApiClient;->mService:Lcom/google/android/wearable/datatransfer/ConnectionService$ConnectionServiceBinder;

    return-object p1
.end method

.method static synthetic access$100(Lcom/google/android/wearable/datatransfer/WearableDataApiClient;)Ljava/util/Queue;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/wearable/datatransfer/WearableDataApiClient;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/WearableDataApiClient;->mPendingEvents:Ljava/util/Queue;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/wearable/datatransfer/WearableDataApiClient;Lcom/google/android/gms/wearable/MessageEvent;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/wearable/datatransfer/WearableDataApiClient;
    .param p1, "x1"    # Lcom/google/android/gms/wearable/MessageEvent;

    .prologue
    .line 65
    invoke-direct {p0, p1}, Lcom/google/android/wearable/datatransfer/WearableDataApiClient;->startDataSyncServiceWithMessage(Lcom/google/android/gms/wearable/MessageEvent;)V

    return-void
.end method

.method private startDataSyncServiceWithMessage(Lcom/google/android/gms/wearable/MessageEvent;)V
    .locals 1
    .param p1, "message"    # Lcom/google/android/gms/wearable/MessageEvent;

    .prologue
    .line 326
    invoke-virtual {p0}, Lcom/google/android/wearable/datatransfer/WearableDataApiClient;->isDataSyncServiceRegistered()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 327
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/WearableDataApiClient;->mContext:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->startWithMessage(Landroid/content/Context;Lcom/google/android/gms/wearable/MessageEvent;)V

    .line 329
    :cond_0
    return-void
.end method


# virtual methods
.method public connect()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 130
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.google.android.wearable.datatransfer.CONNECTION"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 132
    .local v1, "mConnectionServiceIntent":Landroid/content/Intent;
    :try_start_0
    iget-object v2, p0, Lcom/google/android/wearable/datatransfer/WearableDataApiClient;->mContext:Landroid/content/Context;

    invoke-static {v2, v1}, Lcom/google/android/wearable/datatransfer/internal/Utils;->setServiceComponentName(Landroid/content/Context;Landroid/content/Intent;)V
    :try_end_0
    .catch Lcom/google/android/wearable/datatransfer/internal/AmbiguousComponentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/wearable/datatransfer/internal/ComponentNotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    .line 140
    iget-object v2, p0, Lcom/google/android/wearable/datatransfer/WearableDataApiClient;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 141
    iget-object v2, p0, Lcom/google/android/wearable/datatransfer/WearableDataApiClient;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/wearable/datatransfer/WearableDataApiClient;->mConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v2, v1, v3, v4}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 142
    iput-boolean v4, p0, Lcom/google/android/wearable/datatransfer/WearableDataApiClient;->mConnecting:Z

    .line 146
    :goto_0
    return-void

    .line 133
    :catch_0
    move-exception v0

    .line 134
    .local v0, "e":Lcom/google/android/wearable/datatransfer/internal/AmbiguousComponentException;
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "multiple services for action: com.google.android.wearable.datatransfer.CONNECTION"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 136
    .end local v0    # "e":Lcom/google/android/wearable/datatransfer/internal/AmbiguousComponentException;
    :catch_1
    move-exception v0

    .line 137
    .local v0, "e":Lcom/google/android/wearable/datatransfer/internal/ComponentNotFoundException;
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "no services for action: com.google.android.wearable.datatransfer.CONNECTION"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 144
    .end local v0    # "e":Lcom/google/android/wearable/datatransfer/internal/ComponentNotFoundException;
    :cond_0
    const-string v2, "WearableDataApiClient"

    const-string v3, "Unable to bind to ConnectionService."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public disconnect()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/16 v4, 0xd

    const/4 v3, 0x0

    .line 155
    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/WearableDataApiClient;->mDatabase:Lcom/google/android/wearable/datatransfer/internal/DataSyncDatabase;

    if-eqz v1, :cond_0

    .line 156
    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/WearableDataApiClient;->mDatabase:Lcom/google/android/wearable/datatransfer/internal/DataSyncDatabase;

    invoke-virtual {v1}, Lcom/google/android/wearable/datatransfer/internal/DataSyncDatabase;->close()V

    .line 157
    iput-object v5, p0, Lcom/google/android/wearable/datatransfer/WearableDataApiClient;->mDatabase:Lcom/google/android/wearable/datatransfer/internal/DataSyncDatabase;

    .line 160
    :cond_0
    iput-boolean v3, p0, Lcom/google/android/wearable/datatransfer/WearableDataApiClient;->mConnecting:Z

    .line 162
    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/WearableDataApiClient;->mPendingEvents:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    .line 163
    const-string v1, "TAG"

    const-string v2, "Disconnecting WearableDataApiClient before all calls were processed"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 166
    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/WearableDataApiClient;->mPendingEvents:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/wearable/datatransfer/WearableDataApiClient$Event;

    .local v0, "event":Lcom/google/android/wearable/datatransfer/WearableDataApiClient$Event;
    if-eqz v0, :cond_3

    .line 167
    iget-object v1, v0, Lcom/google/android/wearable/datatransfer/WearableDataApiClient$Event;->openConnectionRequest:Lcom/google/android/wearable/datatransfer/WearableDataApiClient$OpenConnectionRequest;

    if-eqz v1, :cond_2

    .line 168
    iget-object v1, v0, Lcom/google/android/wearable/datatransfer/WearableDataApiClient$Event;->openConnectionRequest:Lcom/google/android/wearable/datatransfer/WearableDataApiClient$OpenConnectionRequest;

    iget-object v1, v1, Lcom/google/android/wearable/datatransfer/WearableDataApiClient$OpenConnectionRequest;->pendingResult:Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;

    new-instance v2, Lcom/google/android/wearable/datatransfer/internal/OpenConnectionResultImpl;

    invoke-direct {v2, v5, v4, v3, v3}, Lcom/google/android/wearable/datatransfer/internal/OpenConnectionResultImpl;-><init>(Lcom/google/android/wearable/datatransfer/Connection;III)V

    invoke-virtual {v1, v2}, Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;->setResult(Ljava/lang/Object;)V

    .line 171
    :cond_2
    iget-object v1, v0, Lcom/google/android/wearable/datatransfer/WearableDataApiClient$Event;->stopAllServerConnectionsEvent:Lcom/google/android/wearable/datatransfer/WearableDataApiClient$StopAllServerConnectionsEvent;

    if-eqz v1, :cond_1

    .line 172
    iget-object v1, v0, Lcom/google/android/wearable/datatransfer/WearableDataApiClient$Event;->stopAllServerConnectionsEvent:Lcom/google/android/wearable/datatransfer/WearableDataApiClient$StopAllServerConnectionsEvent;

    iget-object v1, v1, Lcom/google/android/wearable/datatransfer/WearableDataApiClient$StopAllServerConnectionsEvent;->pendingResult:Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;->setResult(Ljava/lang/Object;)V

    goto :goto_0

    .line 178
    .end local v0    # "event":Lcom/google/android/wearable/datatransfer/WearableDataApiClient$Event;
    :cond_3
    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/WearableDataApiClient;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/wearable/datatransfer/WearableDataApiClient;->mConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 179
    return-void
.end method

.method public getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 268
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/WearableDataApiClient;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public getDataSyncDatabase()Lcom/google/android/wearable/datatransfer/internal/DataSyncDatabase;
    .locals 2

    .prologue
    .line 319
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/WearableDataApiClient;->mDatabase:Lcom/google/android/wearable/datatransfer/internal/DataSyncDatabase;

    if-nez v0, :cond_0

    .line 320
    new-instance v0, Lcom/google/android/wearable/datatransfer/internal/DataSyncDatabase;

    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/WearableDataApiClient;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/android/wearable/datatransfer/internal/DataSyncDatabase;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/WearableDataApiClient;->mDatabase:Lcom/google/android/wearable/datatransfer/internal/DataSyncDatabase;

    .line 322
    :cond_0
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/WearableDataApiClient;->mDatabase:Lcom/google/android/wearable/datatransfer/internal/DataSyncDatabase;

    return-object v0
.end method

.method public isDataSyncServiceRegistered()Z
    .locals 1

    .prologue
    .line 277
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/WearableDataApiClient;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->isServiceRegistered(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method onDataChanged(Lcom/google/android/gms/wearable/DataEventBuffer;)V
    .locals 4
    .param p1, "dataEvents"    # Lcom/google/android/gms/wearable/DataEventBuffer;

    .prologue
    .line 219
    invoke-virtual {p1}, Lcom/google/android/gms/wearable/DataEventBuffer;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wearable/DataEvent;

    .line 220
    .local v0, "event":Lcom/google/android/gms/wearable/DataEvent;
    invoke-interface {v0}, Lcom/google/android/gms/wearable/DataEvent;->getDataItem()Lcom/google/android/gms/wearable/DataItem;

    move-result-object v2

    .line 222
    .local v2, "item":Lcom/google/android/gms/wearable/DataItem;
    invoke-interface {v2}, Lcom/google/android/gms/wearable/DataItem;->getUri()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->isHandledDataItemPath(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 223
    invoke-virtual {p0}, Lcom/google/android/wearable/datatransfer/WearableDataApiClient;->startDataSyncServiceIfPresent()V

    .line 227
    .end local v0    # "event":Lcom/google/android/gms/wearable/DataEvent;
    .end local v2    # "item":Lcom/google/android/gms/wearable/DataItem;
    :cond_1
    return-void
.end method

.method onMessageReceived(Lcom/google/android/gms/wearable/MessageEvent;)Z
    .locals 4
    .param p1, "messageEvent"    # Lcom/google/android/gms/wearable/MessageEvent;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 189
    iget-boolean v3, p0, Lcom/google/android/wearable/datatransfer/WearableDataApiClient;->mConnecting:Z

    if-nez v3, :cond_1

    .line 190
    const-string v2, "WearableDataApiClient"

    const-string v3, "onMessageReceived called on unbound WearableDataApiClient"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 209
    :cond_0
    :goto_0
    return v1

    .line 193
    :cond_1
    invoke-interface {p1}, Lcom/google/android/gms/wearable/MessageEvent;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 194
    .local v0, "messagePath":Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;->isHandledPath(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    invoke-static {v0}, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;->isHandledPath(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    invoke-static {v0}, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->isHandledMessagePath(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 200
    :cond_2
    invoke-static {v0}, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->isHandledMessagePath(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 201
    invoke-direct {p0, p1}, Lcom/google/android/wearable/datatransfer/WearableDataApiClient;->startDataSyncServiceWithMessage(Lcom/google/android/gms/wearable/MessageEvent;)V

    move v1, v2

    .line 202
    goto :goto_0

    .line 204
    :cond_3
    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/WearableDataApiClient;->mService:Lcom/google/android/wearable/datatransfer/ConnectionService$ConnectionServiceBinder;

    if-eqz v1, :cond_4

    .line 205
    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/WearableDataApiClient;->mService:Lcom/google/android/wearable/datatransfer/ConnectionService$ConnectionServiceBinder;

    invoke-virtual {v1, p1}, Lcom/google/android/wearable/datatransfer/ConnectionService$ConnectionServiceBinder;->onMessageReceived(Lcom/google/android/gms/wearable/MessageEvent;)Z

    move-result v1

    goto :goto_0

    .line 208
    :cond_4
    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/WearableDataApiClient;->mPendingEvents:Ljava/util/Queue;

    new-instance v3, Lcom/google/android/wearable/datatransfer/WearableDataApiClient$Event;

    invoke-direct {v3, p1}, Lcom/google/android/wearable/datatransfer/WearableDataApiClient$Event;-><init>(Lcom/google/android/gms/wearable/MessageEvent;)V

    invoke-interface {v1, v3}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    move v1, v2

    .line 209
    goto :goto_0
.end method

.method public openConnection(Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 8
    .param p2, "nodeId"    # Ljava/lang/String;
    .param p3, "path"    # Ljava/lang/String;
    .param p4, "offset"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult",
            "<",
            "Lcom/google/android/wearable/datatransfer/DataTransferApi$OpenConnectionResult;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "J)V"
        }
    .end annotation

    .prologue
    .local p1, "pendingResult":Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;, "Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult<Lcom/google/android/wearable/datatransfer/DataTransferApi$OpenConnectionResult;>;"
    const/4 v3, 0x0

    .line 236
    iget-boolean v0, p0, Lcom/google/android/wearable/datatransfer/WearableDataApiClient;->mConnecting:Z

    if-nez v0, :cond_0

    .line 237
    const-string v0, "WearableDataApiClient"

    const-string v1, "openConnection called on unbound WearableDataApiClient"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 238
    new-instance v0, Lcom/google/android/wearable/datatransfer/internal/OpenConnectionResultImpl;

    const/4 v1, 0x0

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2, v3, v3}, Lcom/google/android/wearable/datatransfer/internal/OpenConnectionResultImpl;-><init>(Lcom/google/android/wearable/datatransfer/Connection;III)V

    invoke-virtual {p1, v0}, Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;->setResult(Ljava/lang/Object;)V

    .line 248
    :goto_0
    return-void

    .line 242
    :cond_0
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/WearableDataApiClient;->mService:Lcom/google/android/wearable/datatransfer/ConnectionService$ConnectionServiceBinder;

    if-eqz v0, :cond_1

    .line 243
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/WearableDataApiClient;->mService:Lcom/google/android/wearable/datatransfer/ConnectionService$ConnectionServiceBinder;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-wide v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/wearable/datatransfer/ConnectionService$ConnectionServiceBinder;->openConnection(Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;Ljava/lang/String;Ljava/lang/String;J)V

    goto :goto_0

    .line 245
    :cond_1
    iget-object v6, p0, Lcom/google/android/wearable/datatransfer/WearableDataApiClient;->mPendingEvents:Ljava/util/Queue;

    new-instance v7, Lcom/google/android/wearable/datatransfer/WearableDataApiClient$Event;

    new-instance v0, Lcom/google/android/wearable/datatransfer/WearableDataApiClient$OpenConnectionRequest;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-wide v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/wearable/datatransfer/WearableDataApiClient$OpenConnectionRequest;-><init>(Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;Ljava/lang/String;Ljava/lang/String;J)V

    invoke-direct {v7, v0}, Lcom/google/android/wearable/datatransfer/WearableDataApiClient$Event;-><init>(Lcom/google/android/wearable/datatransfer/WearableDataApiClient$OpenConnectionRequest;)V

    invoke-interface {v6, v7}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public startDataSyncServiceIfPresent()V
    .locals 1

    .prologue
    .line 312
    invoke-virtual {p0}, Lcom/google/android/wearable/datatransfer/WearableDataApiClient;->isDataSyncServiceRegistered()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 313
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/WearableDataApiClient;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->startService(Landroid/content/Context;)V

    .line 315
    :cond_0
    return-void
.end method
