.class public final Lcom/google/android/wearable/datatransfer/internal/DataSyncDatabase;
.super Ljava/lang/Object;
.source "DataSyncDatabase.java"

# interfaces
.implements Ljava/io/Closeable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/wearable/datatransfer/internal/DataSyncDatabase$QueryGetNextRetryTimestamp;,
        Lcom/google/android/wearable/datatransfer/internal/DataSyncDatabase$QueryGetSchedulableRequests;,
        Lcom/google/android/wearable/datatransfer/internal/DataSyncDatabase$QueryGetQueueSize;,
        Lcom/google/android/wearable/datatransfer/internal/DataSyncDatabase$QueryGetRequest;,
        Lcom/google/android/wearable/datatransfer/internal/DataSyncDatabase$DatabaseHelper;
    }
.end annotation


# instance fields
.field private final mHelper:Lcom/google/android/wearable/datatransfer/internal/DataSyncDatabase$DatabaseHelper;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 38
    const-string v0, "WearableDataSync.db"

    invoke-direct {p0, p1, v0}, Lcom/google/android/wearable/datatransfer/internal/DataSyncDatabase;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 39
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "databaseName"    # Ljava/lang/String;

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    new-instance v0, Lcom/google/android/wearable/datatransfer/internal/DataSyncDatabase$DatabaseHelper;

    invoke-direct {v0, p1, p2}, Lcom/google/android/wearable/datatransfer/internal/DataSyncDatabase$DatabaseHelper;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncDatabase;->mHelper:Lcom/google/android/wearable/datatransfer/internal/DataSyncDatabase$DatabaseHelper;

    .line 55
    return-void
.end method

.method private static getRequest(Landroid/database/sqlite/SQLiteDatabase;Ljava/io/File;)Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;
    .locals 11
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "localTargetFile"    # Ljava/io/File;

    .prologue
    const/4 v10, 0x0

    const/4 v5, 0x0

    const/4 v9, 0x1

    .line 294
    const-string v1, "Requests"

    sget-object v2, Lcom/google/android/wearable/datatransfer/internal/DataSyncDatabase$QueryGetRequest;->PROJECTION:[Ljava/lang/String;

    const-string v3, "localTargetFile = ?"

    new-array v4, v9, [Ljava/lang/String;

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v10

    move-object v0, p0

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 300
    .local v8, "cursor":Landroid/database/Cursor;
    if-eqz v8, :cond_0

    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_2

    .line 311
    :cond_0
    if-eqz v8, :cond_1

    .line 312
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_1
    :goto_0
    return-object v5

    .line 304
    :cond_2
    :try_start_1
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-ne v0, v9, :cond_3

    move v0, v9

    :goto_1
    const-string v1, "there should be a single entry"

    invoke-static {v0, v1}, Lcom/google/android/wearable/datatransfer/internal/Preconditions;->checkState(ZLjava/lang/Object;)V

    .line 305
    new-instance v5, Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;

    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-direct {v5, v0, v1, p1, v2}, Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 311
    if-eqz v8, :cond_1

    .line 312
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_3
    move v0, v10

    .line 304
    goto :goto_1

    .line 311
    :catchall_0
    move-exception v0

    if-eqz v8, :cond_4

    .line 312
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0
.end method


# virtual methods
.method public close()V
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncDatabase;->mHelper:Lcom/google/android/wearable/datatransfer/internal/DataSyncDatabase$DatabaseHelper;

    invoke-virtual {v0}, Lcom/google/android/wearable/datatransfer/internal/DataSyncDatabase$DatabaseHelper;->close()V

    .line 69
    return-void
.end method

.method getNextRetryTimestamp(JLjava/util/Collection;)Ljava/lang/Long;
    .locals 5
    .param p1, "currentTimeMillis"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/Long;"
        }
    .end annotation

    .prologue
    .line 271
    .local p3, "connectedNodes":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    iget-object v2, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncDatabase;->mHelper:Lcom/google/android/wearable/datatransfer/internal/DataSyncDatabase$DatabaseHelper;

    invoke-virtual {v2}, Lcom/google/android/wearable/datatransfer/internal/DataSyncDatabase$DatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 272
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    invoke-static {v1, p1, p2, p3}, Lcom/google/android/wearable/datatransfer/internal/DataSyncDatabase$QueryGetNextRetryTimestamp;->query(Landroid/database/sqlite/SQLiteDatabase;JLjava/util/Collection;)Landroid/database/Cursor;

    move-result-object v0

    .line 274
    .local v0, "cursor":Landroid/database/Cursor;
    if-eqz v0, :cond_1

    :try_start_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 275
    const/4 v2, 0x0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 280
    if-eqz v0, :cond_0

    .line 281
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_0
    :goto_0
    return-object v2

    .line 278
    :cond_1
    const/4 v2, 0x0

    .line 280
    if-eqz v0, :cond_0

    .line 281
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 280
    :catchall_0
    move-exception v2

    if-eqz v0, :cond_2

    .line 281
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v2
.end method

.method getQueueSize()I
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v4, 0x0

    .line 135
    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncDatabase;->mHelper:Lcom/google/android/wearable/datatransfer/internal/DataSyncDatabase$DatabaseHelper;

    invoke-virtual {v1}, Lcom/google/android/wearable/datatransfer/internal/DataSyncDatabase$DatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 136
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const-string v1, "Requests"

    sget-object v2, Lcom/google/android/wearable/datatransfer/internal/DataSyncDatabase$QueryGetQueueSize;->PROJECTION:[Ljava/lang/String;

    sget-object v3, Lcom/google/android/wearable/datatransfer/internal/DataSyncDatabase$QueryGetQueueSize;->SELECTION:Ljava/lang/String;

    move-object v5, v4

    move-object v6, v4

    move-object v7, v4

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 141
    .local v8, "cursor":Landroid/database/Cursor;
    if-eqz v8, :cond_1

    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 142
    const/4 v1, 0x0

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    .line 148
    if-eqz v8, :cond_0

    .line 149
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_0
    :goto_0
    return v1

    .line 144
    :cond_1
    :try_start_1
    const-string v1, "DataSyncDatabase"

    const-string v2, "QueryGetQueueSize failed"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 148
    if-eqz v8, :cond_2

    .line 149
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_2
    move v1, v9

    goto :goto_0

    .line 148
    :catchall_0
    move-exception v1

    if-eqz v8, :cond_3

    .line 149
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v1
.end method

.method getRequest(Ljava/io/File;)Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;
    .locals 1
    .param p1, "localTargetFile"    # Ljava/io/File;

    .prologue
    .line 128
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncDatabase;->mHelper:Lcom/google/android/wearable/datatransfer/internal/DataSyncDatabase$DatabaseHelper;

    invoke-virtual {v0}, Lcom/google/android/wearable/datatransfer/internal/DataSyncDatabase$DatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/wearable/datatransfer/internal/DataSyncDatabase;->getRequest(Landroid/database/sqlite/SQLiteDatabase;Ljava/io/File;)Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;

    move-result-object v0

    return-object v0
.end method

.method getSchedulableRequests(JJ)Ljava/util/List;
    .locals 11
    .param p1, "currentTimeMillis"    # J
    .param p3, "maxDelayMillis"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x2

    const/4 v10, 0x1

    const/4 v7, 0x0

    .line 165
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 166
    .local v9, "requests":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;>;"
    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncDatabase;->mHelper:Lcom/google/android/wearable/datatransfer/internal/DataSyncDatabase$DatabaseHelper;

    invoke-virtual {v1}, Lcom/google/android/wearable/datatransfer/internal/DataSyncDatabase$DatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 167
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const-string v1, "Requests"

    sget-object v2, Lcom/google/android/wearable/datatransfer/internal/DataSyncDatabase$QueryGetSchedulableRequests;->PROJECTION:[Ljava/lang/String;

    const-string v3, "retryNextRequestTimestamp <= ? OR retryNextRequestTimestamp >= ?"

    new-array v4, v4, [Ljava/lang/String;

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v7

    add-long v6, p1, p3

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v10

    const-string v7, "requestTimestamp ASC,localTargetFile ASC"

    move-object v6, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 175
    .local v8, "cursor":Landroid/database/Cursor;
    :goto_0
    if-eqz v8, :cond_1

    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 176
    new-instance v1, Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;

    const/4 v2, 0x0

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/io/File;

    const/4 v5, 0x2

    invoke-interface {v8, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/4 v5, 0x3

    invoke-interface {v8, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;I)V

    invoke-interface {v9, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 185
    :catchall_0
    move-exception v1

    if-eqz v8, :cond_0

    .line 186
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_0
    throw v1

    .line 185
    :cond_1
    if-eqz v8, :cond_2

    .line 186
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_2
    return-object v9
.end method

.method removeRequest(Ljava/io/File;)Z
    .locals 7
    .param p1, "localTargetFile"    # Ljava/io/File;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 217
    iget-object v3, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncDatabase;->mHelper:Lcom/google/android/wearable/datatransfer/internal/DataSyncDatabase$DatabaseHelper;

    invoke-virtual {v3}, Lcom/google/android/wearable/datatransfer/internal/DataSyncDatabase$DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 218
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const-string v3, "Requests"

    const-string v4, "localTargetFile = ?"

    new-array v5, v1, [Ljava/lang/String;

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-virtual {v0, v3, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    if-eqz v3, :cond_0

    :goto_0
    return v1

    :cond_0
    move v1, v2

    goto :goto_0
.end method

.method scheduleRetry(Ljava/io/File;IJ)V
    .locals 7
    .param p1, "localTargetFile"    # Ljava/io/File;
    .param p2, "retryCount"    # I
    .param p3, "retryNextRequestTimestamp"    # J

    .prologue
    .line 200
    iget-object v2, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncDatabase;->mHelper:Lcom/google/android/wearable/datatransfer/internal/DataSyncDatabase$DatabaseHelper;

    invoke-virtual {v2}, Lcom/google/android/wearable/datatransfer/internal/DataSyncDatabase$DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 201
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 202
    .local v1, "values":Landroid/content/ContentValues;
    const-string v2, "retryCount"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 203
    const-string v2, "retryNextRequestTimestamp"

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 204
    const-string v2, "Requests"

    const-string v3, "localTargetFile = ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v0, v2, v1, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 209
    return-void
.end method
