.class final Lcom/google/android/wearable/datatransfer/internal/SharedBatteryStatusUpdaterImpl;
.super Ljava/lang/Object;
.source "SharedBatteryStatusUpdaterImpl.java"

# interfaces
.implements Lcom/google/android/wearable/datatransfer/internal/SharedBatteryStatusUpdater;


# static fields
.field private static final EMPTY_BYTE_ARRAY:[B


# instance fields
.field private final mClock:Lcom/google/android/wearable/datatransfer/internal/Clock;

.field private final mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x0

    new-array v0, v0, [B

    sput-object v0, Lcom/google/android/wearable/datatransfer/internal/SharedBatteryStatusUpdaterImpl;->EMPTY_BYTE_ARRAY:[B

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lcom/google/android/wearable/datatransfer/internal/Clock;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "clock"    # Lcom/google/android/wearable/datatransfer/internal/Clock;

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    const-string v0, "context"

    invoke-static {p1, v0}, Lcom/google/android/wearable/datatransfer/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/SharedBatteryStatusUpdaterImpl;->mContext:Landroid/content/Context;

    .line 50
    const-string v0, "clock"

    invoke-static {p2, v0}, Lcom/google/android/wearable/datatransfer/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/wearable/datatransfer/internal/Clock;

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/SharedBatteryStatusUpdaterImpl;->mClock:Lcom/google/android/wearable/datatransfer/internal/Clock;

    .line 51
    return-void
.end method


# virtual methods
.method public getAllBatteryStatuses(Lcom/google/android/gms/common/api/GoogleApiClient;)Ljava/util/Map;
    .locals 11
    .param p1, "client"    # Lcom/google/android/gms/common/api/GoogleApiClient;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/common/api/GoogleApiClient;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/wearable/datatransfer/internal/SharedBatteryStatus;",
            ">;"
        }
    .end annotation

    .prologue
    .line 90
    sget-object v7, Lcom/google/android/gms/wearable/Wearable;->DataApi:Lcom/google/android/gms/wearable/DataApi;

    const-string v8, "wear:/com.google.android.wearable.datatransfer.BATTERY_STATUS"

    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    invoke-interface {v7, p1, v8}, Lcom/google/android/gms/wearable/DataApi;->getDataItems(Lcom/google/android/gms/common/api/GoogleApiClient;Landroid/net/Uri;)Lcom/google/android/gms/common/api/PendingResult;

    move-result-object v7

    const-wide/16 v8, 0x2710

    sget-object v10, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v7, v8, v9, v10}, Lcom/google/android/gms/common/api/PendingResult;->await(JLjava/util/concurrent/TimeUnit;)Lcom/google/android/gms/common/api/Result;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wearable/DataItemBuffer;

    .line 95
    .local v0, "batteryStatusItems":Lcom/google/android/gms/wearable/DataItemBuffer;
    :try_start_0
    invoke-virtual {v0}, Lcom/google/android/gms/wearable/DataItemBuffer;->getStatus()Lcom/google/android/gms/common/api/Status;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/gms/common/api/Status;->isSuccess()Z

    move-result v7

    if-nez v7, :cond_0

    .line 96
    const-string v7, "SharedBatteryStatusUpdater"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Failed to get battery status data items: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Lcom/google/android/gms/wearable/DataItemBuffer;->getStatus()Lcom/google/android/gms/common/api/Status;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v5

    .line 110
    invoke-virtual {v0}, Lcom/google/android/gms/wearable/DataItemBuffer;->release()V

    :goto_0
    return-object v5

    .line 101
    :cond_0
    :try_start_1
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 102
    .local v5, "result":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/wearable/datatransfer/internal/SharedBatteryStatus;>;"
    invoke-virtual {v0}, Lcom/google/android/gms/wearable/DataItemBuffer;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/wearable/DataItem;

    .line 103
    .local v1, "dataItem":Lcom/google/android/gms/wearable/DataItem;
    invoke-interface {v1}, Lcom/google/android/gms/wearable/DataItem;->getUri()Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v7}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v4

    .line 104
    .local v4, "nodeId":Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/gms/wearable/DataMapItem;->fromDataItem(Lcom/google/android/gms/wearable/DataItem;)Lcom/google/android/gms/wearable/DataMapItem;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/gms/wearable/DataMapItem;->getDataMap()Lcom/google/android/gms/wearable/DataMap;

    move-result-object v3

    .line 105
    .local v3, "map":Lcom/google/android/gms/wearable/DataMap;
    invoke-static {v3}, Lcom/google/android/wearable/datatransfer/internal/SharedBatteryStatus;->fromDataMap(Lcom/google/android/gms/wearable/DataMap;)Lcom/google/android/wearable/datatransfer/internal/SharedBatteryStatus;

    move-result-object v6

    .line 106
    .local v6, "status":Lcom/google/android/wearable/datatransfer/internal/SharedBatteryStatus;
    invoke-interface {v5, v4, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 110
    .end local v1    # "dataItem":Lcom/google/android/gms/wearable/DataItem;
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "map":Lcom/google/android/gms/wearable/DataMap;
    .end local v4    # "nodeId":Ljava/lang/String;
    .end local v5    # "result":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/wearable/datatransfer/internal/SharedBatteryStatus;>;"
    .end local v6    # "status":Lcom/google/android/wearable/datatransfer/internal/SharedBatteryStatus;
    :catchall_0
    move-exception v7

    invoke-virtual {v0}, Lcom/google/android/gms/wearable/DataItemBuffer;->release()V

    throw v7

    .restart local v2    # "i$":Ljava/util/Iterator;
    .restart local v5    # "result":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/wearable/datatransfer/internal/SharedBatteryStatus;>;"
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/gms/wearable/DataItemBuffer;->release()V

    goto :goto_0
.end method

.method public publishBatteryStatus(Lcom/google/android/gms/common/api/GoogleApiClient;Ljava/lang/String;)V
    .locals 10
    .param p1, "client"    # Lcom/google/android/gms/common/api/GoogleApiClient;
    .param p2, "batteryUpdateAction"    # Ljava/lang/String;

    .prologue
    .line 56
    iget-object v6, p0, Lcom/google/android/wearable/datatransfer/internal/SharedBatteryStatusUpdaterImpl;->mContext:Landroid/content/Context;

    invoke-static {v6}, Lcom/google/android/wearable/datatransfer/internal/BatteryUtils;->getBatteryStatus(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v2

    .line 59
    .local v2, "batteryStatusIntent":Landroid/content/Intent;
    const-string v6, "android.intent.action.BATTERY_LOW"

    invoke-virtual {v6, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 60
    const/4 v0, 0x1

    .line 71
    .local v0, "batteryLow":Z
    :goto_0
    new-instance v5, Lcom/google/android/wearable/datatransfer/internal/SharedBatteryStatus;

    iget-object v6, p0, Lcom/google/android/wearable/datatransfer/internal/SharedBatteryStatusUpdaterImpl;->mClock:Lcom/google/android/wearable/datatransfer/internal/Clock;

    invoke-interface {v6}, Lcom/google/android/wearable/datatransfer/internal/Clock;->getCurrentTimeMillis()J

    move-result-wide v6

    invoke-direct {v5, v6, v7, v0}, Lcom/google/android/wearable/datatransfer/internal/SharedBatteryStatus;-><init>(JZ)V

    .line 74
    .local v5, "status":Lcom/google/android/wearable/datatransfer/internal/SharedBatteryStatus;
    const-string v6, "/com.google.android.wearable.datatransfer.BATTERY_STATUS"

    invoke-static {v6}, Lcom/google/android/gms/wearable/PutDataMapRequest;->create(Ljava/lang/String;)Lcom/google/android/gms/wearable/PutDataMapRequest;

    move-result-object v3

    .line 75
    .local v3, "request":Lcom/google/android/gms/wearable/PutDataMapRequest;
    invoke-virtual {v3}, Lcom/google/android/gms/wearable/PutDataMapRequest;->getDataMap()Lcom/google/android/gms/wearable/DataMap;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/wearable/datatransfer/internal/SharedBatteryStatus;->toDataMap(Lcom/google/android/gms/wearable/DataMap;)V

    .line 76
    sget-object v6, Lcom/google/android/gms/wearable/Wearable;->DataApi:Lcom/google/android/gms/wearable/DataApi;

    invoke-virtual {v3}, Lcom/google/android/gms/wearable/PutDataMapRequest;->asPutDataRequest()Lcom/google/android/gms/wearable/PutDataRequest;

    move-result-object v7

    invoke-interface {v6, p1, v7}, Lcom/google/android/gms/wearable/DataApi;->putDataItem(Lcom/google/android/gms/common/api/GoogleApiClient;Lcom/google/android/gms/wearable/PutDataRequest;)Lcom/google/android/gms/common/api/PendingResult;

    move-result-object v6

    const-wide/16 v8, 0x2710

    sget-object v7, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v6, v8, v9, v7}, Lcom/google/android/gms/common/api/PendingResult;->await(JLjava/util/concurrent/TimeUnit;)Lcom/google/android/gms/common/api/Result;

    move-result-object v4

    check-cast v4, Lcom/google/android/gms/wearable/DataApi$DataItemResult;

    .line 79
    .local v4, "result":Lcom/google/android/gms/wearable/DataApi$DataItemResult;
    invoke-interface {v4}, Lcom/google/android/gms/wearable/DataApi$DataItemResult;->getStatus()Lcom/google/android/gms/common/api/Status;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/gms/common/api/Status;->isSuccess()Z

    move-result v6

    if-nez v6, :cond_0

    .line 80
    const-string v6, "SharedBatteryStatusUpdater"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Failed to publish battery status: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-interface {v4}, Lcom/google/android/gms/wearable/DataApi$DataItemResult;->getStatus()Lcom/google/android/gms/common/api/Status;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    :cond_0
    return-void

    .line 61
    .end local v0    # "batteryLow":Z
    .end local v3    # "request":Lcom/google/android/gms/wearable/PutDataMapRequest;
    .end local v4    # "result":Lcom/google/android/gms/wearable/DataApi$DataItemResult;
    .end local v5    # "status":Lcom/google/android/wearable/datatransfer/internal/SharedBatteryStatus;
    :cond_1
    const-string v6, "android.intent.action.BATTERY_OKAY"

    invoke-virtual {v6, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 62
    const/4 v0, 0x0

    .restart local v0    # "batteryLow":Z
    goto :goto_0

    .line 68
    .end local v0    # "batteryLow":Z
    :cond_2
    invoke-static {v2}, Lcom/google/android/wearable/datatransfer/internal/BatteryUtils;->getBatteryChargePercentage(Landroid/content/Intent;)I

    move-result v1

    .line 69
    .local v1, "batteryPercentage":I
    const/16 v6, 0xf

    if-ge v1, v6, :cond_3

    const/4 v0, 0x1

    .restart local v0    # "batteryLow":Z
    :goto_1
    goto :goto_0

    .end local v0    # "batteryLow":Z
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public sendBatteryStatusPoke(Lcom/google/android/gms/common/api/GoogleApiClient;Ljava/lang/String;)V
    .locals 4
    .param p1, "client"    # Lcom/google/android/gms/common/api/GoogleApiClient;
    .param p2, "nodeId"    # Ljava/lang/String;

    .prologue
    .line 116
    sget-object v0, Lcom/google/android/gms/wearable/Wearable;->MessageApi:Lcom/google/android/gms/wearable/MessageApi;

    const-string v1, "com.google.android.wearable.datatransfer.BATTERY_STATUS_POKE"

    sget-object v2, Lcom/google/android/wearable/datatransfer/internal/SharedBatteryStatusUpdaterImpl;->EMPTY_BYTE_ARRAY:[B

    invoke-interface {v0, p1, p2, v1, v2}, Lcom/google/android/gms/wearable/MessageApi;->sendMessage(Lcom/google/android/gms/common/api/GoogleApiClient;Ljava/lang/String;Ljava/lang/String;[B)Lcom/google/android/gms/common/api/PendingResult;

    move-result-object v0

    const-wide/16 v2, 0x2710

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v2, v3, v1}, Lcom/google/android/gms/common/api/PendingResult;->await(JLjava/util/concurrent/TimeUnit;)Lcom/google/android/gms/common/api/Result;

    .line 119
    return-void
.end method
