.class final Lcom/google/android/wearable/datatransfer/internal/DataTransferClient$ReceiverCancelToken;
.super Ljava/lang/Object;
.source "DataTransferClient.java"

# interfaces
.implements Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$ICancelToken;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ReceiverCancelToken"
.end annotation


# instance fields
.field private mClient:Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;

.field private final mConnectionId:J

.field private mFutureTask:Ljava/util/concurrent/FutureTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/FutureTask",
            "<*>;"
        }
    .end annotation
.end field

.field private final mLock:Ljava/lang/Object;

.field private mPipeFds:[Landroid/os/ParcelFileDescriptor;


# direct methods
.method constructor <init>(Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;Ljava/util/concurrent/FutureTask;J)V
    .locals 1
    .param p1, "client"    # Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;
    .param p3, "connectionId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;",
            "Ljava/util/concurrent/FutureTask",
            "<*>;J)V"
        }
    .end annotation

    .prologue
    .line 419
    .local p2, "futureTask":Ljava/util/concurrent/FutureTask;, "Ljava/util/concurrent/FutureTask<*>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 395
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient$ReceiverCancelToken;->mLock:Ljava/lang/Object;

    .line 420
    iput-object p1, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient$ReceiverCancelToken;->mClient:Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;

    .line 421
    iput-object p2, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient$ReceiverCancelToken;->mFutureTask:Ljava/util/concurrent/FutureTask;

    .line 422
    iput-wide p3, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient$ReceiverCancelToken;->mConnectionId:J

    .line 423
    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 451
    const-string v4, "DataTransferClient"

    invoke-static {v4}, Lcom/google/android/wearable/datatransfer/internal/DebugLog;->isLoggable(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 452
    const-string v4, "DataTransferClient"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Canceling connection: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v6, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient$ReceiverCancelToken;->mConnectionId:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 457
    :cond_0
    iget-object v5, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient$ReceiverCancelToken;->mLock:Ljava/lang/Object;

    monitor-enter v5

    .line 458
    :try_start_0
    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient$ReceiverCancelToken;->mClient:Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;

    .line 459
    .local v1, "client":Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;
    iget-object v2, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient$ReceiverCancelToken;->mFutureTask:Ljava/util/concurrent/FutureTask;

    .line 460
    .local v2, "futureTask":Ljava/util/concurrent/FutureTask;, "Ljava/util/concurrent/FutureTask<*>;"
    iget-object v3, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient$ReceiverCancelToken;->mPipeFds:[Landroid/os/ParcelFileDescriptor;

    .line 461
    .local v3, "pipeFds":[Landroid/os/ParcelFileDescriptor;
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient$ReceiverCancelToken;->mClient:Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;

    .line 462
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient$ReceiverCancelToken;->mFutureTask:Ljava/util/concurrent/FutureTask;

    .line 463
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient$ReceiverCancelToken;->mPipeFds:[Landroid/os/ParcelFileDescriptor;

    .line 464
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 466
    if-eqz v3, :cond_1

    .line 469
    aget-object v4, v3, v9

    invoke-static {v4}, Lcom/google/android/wearable/datatransfer/internal/Utils;->closeQuietly(Ljava/io/Closeable;)V

    .line 472
    aget-object v4, v3, v8

    invoke-static {v4}, Lcom/google/android/wearable/datatransfer/internal/Utils;->closeQuietly(Ljava/io/Closeable;)V

    .line 477
    :cond_1
    if-eqz v2, :cond_3

    .line 478
    invoke-virtual {v2, v9}, Ljava/util/concurrent/FutureTask;->cancel(Z)Z

    move-result v0

    .line 479
    .local v0, "cancelled":Z
    if-nez v0, :cond_3

    .line 487
    .end local v0    # "cancelled":Z
    :cond_2
    :goto_0
    return-void

    .line 464
    .end local v1    # "client":Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;
    .end local v2    # "futureTask":Ljava/util/concurrent/FutureTask;, "Ljava/util/concurrent/FutureTask<*>;"
    .end local v3    # "pipeFds":[Landroid/os/ParcelFileDescriptor;
    :catchall_0
    move-exception v4

    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4

    .line 484
    .restart local v1    # "client":Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;
    .restart local v2    # "futureTask":Ljava/util/concurrent/FutureTask;, "Ljava/util/concurrent/FutureTask<*>;"
    .restart local v3    # "pipeFds":[Landroid/os/ParcelFileDescriptor;
    :cond_3
    if-eqz v1, :cond_2

    .line 485
    iget-wide v4, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient$ReceiverCancelToken;->mConnectionId:J

    # invokes: Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;->terminateConnection(JII)V
    invoke-static {v1, v4, v5, v8, v8}, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;->access$700(Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;JII)V

    goto :goto_0
.end method

.method futureDone()V
    .locals 2

    .prologue
    .line 442
    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient$ReceiverCancelToken;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 443
    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient$ReceiverCancelToken;->mClient:Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;

    .line 444
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient$ReceiverCancelToken;->mFutureTask:Ljava/util/concurrent/FutureTask;

    .line 445
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient$ReceiverCancelToken;->mPipeFds:[Landroid/os/ParcelFileDescriptor;

    .line 446
    monitor-exit v1

    .line 447
    return-void

    .line 446
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method onConnectionOpened([Landroid/os/ParcelFileDescriptor;)V
    .locals 0
    .param p1, "pipeFds"    # [Landroid/os/ParcelFileDescriptor;

    .prologue
    .line 432
    iput-object p1, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient$ReceiverCancelToken;->mPipeFds:[Landroid/os/ParcelFileDescriptor;

    .line 433
    return-void
.end method
