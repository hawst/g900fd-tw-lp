.class final Lcom/google/android/wearable/datatransfer/internal/DataHandler$FuturePacket;
.super Ljava/lang/Object;
.source "DataHandler.java"

# interfaces
.implements Ljava/util/concurrent/Future;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/wearable/datatransfer/internal/DataHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "FuturePacket"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Future",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final mPacketLatch:Ljava/util/concurrent/CountDownLatch;

.field private final mPacketReference:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/util/concurrent/CountDownLatch;Ljava/util/concurrent/atomic/AtomicReference;)V
    .locals 1
    .param p1, "packetLatch"    # Ljava/util/concurrent/CountDownLatch;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/CountDownLatch;",
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 217
    .local p0, "this":Lcom/google/android/wearable/datatransfer/internal/DataHandler$FuturePacket;, "Lcom/google/android/wearable/datatransfer/internal/DataHandler$FuturePacket<TT;>;"
    .local p2, "packetReference":Ljava/util/concurrent/atomic/AtomicReference;, "Ljava/util/concurrent/atomic/AtomicReference<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 218
    const-string v0, "packetLatch"

    invoke-static {p1, v0}, Lcom/google/android/wearable/datatransfer/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/CountDownLatch;

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataHandler$FuturePacket;->mPacketLatch:Ljava/util/concurrent/CountDownLatch;

    .line 219
    const-string v0, "packetReference"

    invoke-static {p2, v0}, Lcom/google/android/wearable/datatransfer/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/atomic/AtomicReference;

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataHandler$FuturePacket;->mPacketReference:Ljava/util/concurrent/atomic/AtomicReference;

    .line 220
    return-void
.end method


# virtual methods
.method public cancel(Z)Z
    .locals 1
    .param p1, "mayInterruptIfRunning"    # Z

    .prologue
    .line 225
    .local p0, "this":Lcom/google/android/wearable/datatransfer/internal/DataHandler$FuturePacket;, "Lcom/google/android/wearable/datatransfer/internal/DataHandler$FuturePacket<TT;>;"
    const/4 v0, 0x0

    return v0
.end method

.method public get()Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/util/concurrent/ExecutionException;
        }
    .end annotation

    .prologue
    .line 241
    .local p0, "this":Lcom/google/android/wearable/datatransfer/internal/DataHandler$FuturePacket;, "Lcom/google/android/wearable/datatransfer/internal/DataHandler$FuturePacket<TT;>;"
    const-string v1, "DataHandler"

    invoke-static {v1}, Lcom/google/android/wearable/datatransfer/internal/DebugLog;->isLoggable(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 242
    const-string v1, "DataHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "waiting for packet on "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/wearable/datatransfer/internal/DataHandler$FuturePacket;->mPacketLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 244
    :cond_0
    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/internal/DataHandler$FuturePacket;->mPacketLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v1}, Ljava/util/concurrent/CountDownLatch;->await()V

    .line 245
    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/internal/DataHandler$FuturePacket;->mPacketReference:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicReference;->getAndSet(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 246
    .local v0, "packet":Ljava/lang/Object;, "TT;"
    if-nez v0, :cond_2

    .line 247
    const-string v1, "DataHandler"

    invoke-static {v1}, Lcom/google/android/wearable/datatransfer/internal/DebugLog;->isLoggable(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 248
    const-string v1, "DataHandler"

    const-string v2, "missing packet"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 250
    :cond_1
    new-instance v1, Ljava/util/concurrent/ExecutionException;

    new-instance v2, Ljava/io/IOException;

    const-string v3, "Missing response packet"

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/util/concurrent/ExecutionException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 252
    :cond_2
    const-string v1, "DataHandler"

    invoke-static {v1}, Lcom/google/android/wearable/datatransfer/internal/DebugLog;->isLoggable(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 253
    const-string v1, "DataHandler"

    const-string v2, "packet received"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 255
    :cond_3
    return-object v0
.end method

.method public get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    .locals 7
    .param p1, "timeout"    # J
    .param p3, "unit"    # Ljava/util/concurrent/TimeUnit;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/concurrent/TimeUnit;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/util/concurrent/ExecutionException;,
            Ljava/util/concurrent/TimeoutException;
        }
    .end annotation

    .prologue
    .line 261
    .local p0, "this":Lcom/google/android/wearable/datatransfer/internal/DataHandler$FuturePacket;, "Lcom/google/android/wearable/datatransfer/internal/DataHandler$FuturePacket<TT;>;"
    const-string v1, "DataHandler"

    invoke-static {v1}, Lcom/google/android/wearable/datatransfer/internal/DebugLog;->isLoggable(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 262
    const-string v1, "DataHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "waiting for packet on "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/wearable/datatransfer/internal/DataHandler$FuturePacket;->mPacketLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p3, p1, p2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ms"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 265
    :cond_0
    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/internal/DataHandler$FuturePacket;->mPacketLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v1, p1, p2, p3}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 266
    const-string v1, "DataHandler"

    invoke-static {v1}, Lcom/google/android/wearable/datatransfer/internal/DebugLog;->isLoggable(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 267
    const-string v1, "DataHandler"

    const-string v2, "timeout"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 269
    :cond_1
    new-instance v1, Ljava/util/concurrent/TimeoutException;

    invoke-direct {v1}, Ljava/util/concurrent/TimeoutException;-><init>()V

    throw v1

    .line 271
    :cond_2
    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/internal/DataHandler$FuturePacket;->mPacketReference:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicReference;->getAndSet(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 272
    .local v0, "packet":Ljava/lang/Object;, "TT;"
    if-nez v0, :cond_4

    .line 273
    const-string v1, "DataHandler"

    invoke-static {v1}, Lcom/google/android/wearable/datatransfer/internal/DebugLog;->isLoggable(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 274
    const-string v1, "DataHandler"

    const-string v2, "missing packet"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 276
    :cond_3
    new-instance v1, Ljava/util/concurrent/ExecutionException;

    new-instance v2, Ljava/io/IOException;

    const-string v3, "Missing response packet"

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/util/concurrent/ExecutionException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 278
    :cond_4
    const-string v1, "DataHandler"

    invoke-static {v1}, Lcom/google/android/wearable/datatransfer/internal/DebugLog;->isLoggable(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 279
    const-string v1, "DataHandler"

    const-string v2, "packet received"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 281
    :cond_5
    return-object v0
.end method

.method public isCancelled()Z
    .locals 1

    .prologue
    .line 231
    .local p0, "this":Lcom/google/android/wearable/datatransfer/internal/DataHandler$FuturePacket;, "Lcom/google/android/wearable/datatransfer/internal/DataHandler$FuturePacket<TT;>;"
    const/4 v0, 0x0

    return v0
.end method

.method public isDone()Z
    .locals 4

    .prologue
    .line 236
    .local p0, "this":Lcom/google/android/wearable/datatransfer/internal/DataHandler$FuturePacket;, "Lcom/google/android/wearable/datatransfer/internal/DataHandler$FuturePacket<TT;>;"
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataHandler$FuturePacket;->mPacketLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->getCount()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
