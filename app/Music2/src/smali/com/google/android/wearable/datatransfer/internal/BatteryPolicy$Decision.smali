.class public final Lcom/google/android/wearable/datatransfer/internal/BatteryPolicy$Decision;
.super Ljava/lang/Object;
.source "BatteryPolicy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/wearable/datatransfer/internal/BatteryPolicy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Decision"
.end annotation


# instance fields
.field final pollDelayMillis:Ljava/lang/Long;

.field final shouldSync:Z


# direct methods
.method constructor <init>(ZLjava/lang/Long;)V
    .locals 0
    .param p1, "shouldSync"    # Z
    .param p2, "pollDelayMillis"    # Ljava/lang/Long;

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-boolean p1, p0, Lcom/google/android/wearable/datatransfer/internal/BatteryPolicy$Decision;->shouldSync:Z

    .line 34
    iput-object p2, p0, Lcom/google/android/wearable/datatransfer/internal/BatteryPolicy$Decision;->pollDelayMillis:Ljava/lang/Long;

    .line 35
    return-void
.end method
