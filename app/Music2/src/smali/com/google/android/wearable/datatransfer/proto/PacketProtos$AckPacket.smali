.class public final Lcom/google/android/wearable/datatransfer/proto/PacketProtos$AckPacket;
.super Lcom/google/protobuf/nano/MessageNano;
.source "PacketProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/wearable/datatransfer/proto/PacketProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AckPacket"
.end annotation


# instance fields
.field public clientConnectionId:J

.field public finalPacket:Z

.field public serverConnectionId:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 409
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 410
    invoke-virtual {p0}, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$AckPacket;->clear()Lcom/google/android/wearable/datatransfer/proto/PacketProtos$AckPacket;

    .line 411
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/wearable/datatransfer/proto/PacketProtos$AckPacket;
    .locals 2

    .prologue
    const-wide/16 v0, 0x0

    .line 414
    iput-wide v0, p0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$AckPacket;->clientConnectionId:J

    .line 415
    iput-wide v0, p0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$AckPacket;->serverConnectionId:J

    .line 416
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$AckPacket;->finalPacket:Z

    .line 417
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$AckPacket;->cachedSize:I

    .line 418
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 438
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 439
    .local v0, "size":I
    iget-wide v2, p0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$AckPacket;->clientConnectionId:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 440
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$AckPacket;->clientConnectionId:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeFixed64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 443
    :cond_0
    iget-wide v2, p0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$AckPacket;->serverConnectionId:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    .line 444
    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$AckPacket;->serverConnectionId:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeFixed64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 447
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$AckPacket;->finalPacket:Z

    if-eqz v1, :cond_2

    .line 448
    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$AckPacket;->finalPacket:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 451
    :cond_2
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/wearable/datatransfer/proto/PacketProtos$AckPacket;
    .locals 4
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 459
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 460
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 464
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 465
    :sswitch_0
    return-object p0

    .line 470
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readFixed64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$AckPacket;->clientConnectionId:J

    goto :goto_0

    .line 474
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readFixed64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$AckPacket;->serverConnectionId:J

    goto :goto_0

    .line 478
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$AckPacket;->finalPacket:Z

    goto :goto_0

    .line 460
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x9 -> :sswitch_1
        0x11 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 383
    invoke-virtual {p0, p1}, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$AckPacket;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/wearable/datatransfer/proto/PacketProtos$AckPacket;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 6
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v4, 0x0

    .line 424
    iget-wide v0, p0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$AckPacket;->clientConnectionId:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_0

    .line 425
    const/4 v0, 0x1

    iget-wide v2, p0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$AckPacket;->clientConnectionId:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeFixed64(IJ)V

    .line 427
    :cond_0
    iget-wide v0, p0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$AckPacket;->serverConnectionId:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_1

    .line 428
    const/4 v0, 0x2

    iget-wide v2, p0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$AckPacket;->serverConnectionId:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeFixed64(IJ)V

    .line 430
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$AckPacket;->finalPacket:Z

    if-eqz v0, :cond_2

    .line 431
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$AckPacket;->finalPacket:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 433
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 434
    return-void
.end method
