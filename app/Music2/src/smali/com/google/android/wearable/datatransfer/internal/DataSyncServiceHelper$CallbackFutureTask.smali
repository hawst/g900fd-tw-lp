.class Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper$CallbackFutureTask;
.super Ljava/util/concurrent/FutureTask;
.source "DataSyncServiceHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CallbackFutureTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/concurrent/FutureTask",
        "<",
        "Lcom/google/android/wearable/datatransfer/internal/DataSyncer$Result;",
        ">;"
    }
.end annotation


# instance fields
.field private final mListener:Lcom/google/android/wearable/datatransfer/internal/DataSyncListener;

.field private final mRequest:Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;

.field final synthetic this$0:Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;


# direct methods
.method constructor <init>(Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;Lcom/google/android/wearable/datatransfer/internal/DataSyncer;Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;Lcom/google/android/wearable/datatransfer/internal/DataSyncListener;)V
    .locals 0
    .param p2, "syncer"    # Lcom/google/android/wearable/datatransfer/internal/DataSyncer;
    .param p3, "request"    # Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;
    .param p4, "listener"    # Lcom/google/android/wearable/datatransfer/internal/DataSyncListener;

    .prologue
    .line 728
    iput-object p1, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper$CallbackFutureTask;->this$0:Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;

    .line 729
    invoke-direct {p0, p2}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    .line 730
    iput-object p3, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper$CallbackFutureTask;->mRequest:Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;

    .line 731
    iput-object p4, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper$CallbackFutureTask;->mListener:Lcom/google/android/wearable/datatransfer/internal/DataSyncListener;

    .line 732
    return-void
.end method

.method private notifyFailure(II)V
    .locals 6
    .param p1, "commonStatusCode"    # I
    .param p2, "appStatusCode"    # I

    .prologue
    .line 786
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper$CallbackFutureTask;->mListener:Lcom/google/android/wearable/datatransfer/internal/DataSyncListener;

    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper$CallbackFutureTask;->mRequest:Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;

    iget-object v1, v1, Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;->remoteNodeId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper$CallbackFutureTask;->mRequest:Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;

    iget-object v2, v2, Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;->path:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper$CallbackFutureTask;->mRequest:Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;

    iget-object v3, v3, Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;->targetFile:Ljava/io/File;

    move v4, p1

    move v5, p2

    invoke-interface/range {v0 .. v5}, Lcom/google/android/wearable/datatransfer/internal/DataSyncListener;->onDataSyncFailed(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;II)V

    .line 789
    return-void
.end method

.method private notifySuccess()V
    .locals 4

    .prologue
    .line 780
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper$CallbackFutureTask;->mListener:Lcom/google/android/wearable/datatransfer/internal/DataSyncListener;

    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper$CallbackFutureTask;->mRequest:Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;

    iget-object v1, v1, Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;->remoteNodeId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper$CallbackFutureTask;->mRequest:Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;

    iget-object v2, v2, Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;->path:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper$CallbackFutureTask;->mRequest:Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;

    iget-object v3, v3, Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;->targetFile:Ljava/io/File;

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/wearable/datatransfer/internal/DataSyncListener;->onDataSyncCompleted(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)V

    .line 782
    return-void
.end method


# virtual methods
.method protected done()V
    .locals 11

    .prologue
    const/16 v10, 0x8

    const/4 v9, 0x0

    .line 736
    const/4 v2, 0x0

    .line 738
    .local v2, "handled":Z
    const/4 v3, 0x0

    .line 740
    .local v3, "result":Lcom/google/android/wearable/datatransfer/internal/DataSyncer$Result;
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper$CallbackFutureTask;->get()Ljava/lang/Object;

    move-result-object v4

    move-object v0, v4

    check-cast v0, Lcom/google/android/wearable/datatransfer/internal/DataSyncer$Result;

    move-object v3, v0

    .line 741
    iget-object v4, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper$CallbackFutureTask;->this$0:Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;

    # getter for: Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->mDataSyncers:Ljava/util/concurrent/ConcurrentMap;
    invoke-static {v4}, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->access$100(Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;)Ljava/util/concurrent/ConcurrentMap;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper$CallbackFutureTask;->mRequest:Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;

    iget-object v5, v5, Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;->targetFile:Ljava/io/File;

    invoke-interface {v4, v5}, Ljava/util/concurrent/ConcurrentMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 742
    iget-boolean v4, v3, Lcom/google/android/wearable/datatransfer/internal/DataSyncer$Result;->success:Z

    if-eqz v4, :cond_2

    .line 743
    iget-object v4, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper$CallbackFutureTask;->this$0:Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;

    # getter for: Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->mClient:Lcom/google/android/wearable/datatransfer/WearableDataApiClient;
    invoke-static {v4}, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->access$200(Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;)Lcom/google/android/wearable/datatransfer/WearableDataApiClient;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/wearable/datatransfer/WearableDataApiClient;->getDataSyncDatabase()Lcom/google/android/wearable/datatransfer/internal/DataSyncDatabase;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper$CallbackFutureTask;->mRequest:Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;

    iget-object v5, v5, Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;->targetFile:Ljava/io/File;

    invoke-virtual {v4, v5}, Lcom/google/android/wearable/datatransfer/internal/DataSyncDatabase;->removeRequest(Ljava/io/File;)Z

    .line 744
    invoke-direct {p0}, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper$CallbackFutureTask;->notifySuccess()V
    :try_end_0
    .catch Ljava/util/concurrent/CancellationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 753
    :goto_0
    const/4 v2, 0x1

    .line 765
    :goto_1
    :try_start_1
    const-string v4, "DataSyncServiceHelper"

    invoke-static {v4}, Lcom/google/android/wearable/datatransfer/internal/DebugLog;->isLoggable(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 766
    const-string v4, "DataSyncServiceHelper"

    const-string v5, "DataSyncer with targetFile = %s is finished. result = %s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper$CallbackFutureTask;->mRequest:Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;

    iget-object v8, v8, Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;->targetFile:Ljava/io/File;

    aput-object v8, v6, v7

    const/4 v7, 0x1

    aput-object v3, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 771
    :cond_0
    if-nez v2, :cond_1

    .line 772
    const-string v4, "DataSyncServiceHelper"

    const-string v5, "Failed to handle syncer completion"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 773
    invoke-direct {p0, v10, v9}, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper$CallbackFutureTask;->notifyFailure(II)V

    .line 776
    :cond_1
    return-void

    .line 745
    :cond_2
    :try_start_2
    iget-boolean v4, v3, Lcom/google/android/wearable/datatransfer/internal/DataSyncer$Result;->permanentFailure:Z

    if-eqz v4, :cond_3

    .line 746
    iget-object v4, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper$CallbackFutureTask;->this$0:Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;

    # getter for: Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->mClient:Lcom/google/android/wearable/datatransfer/WearableDataApiClient;
    invoke-static {v4}, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->access$200(Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;)Lcom/google/android/wearable/datatransfer/WearableDataApiClient;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/wearable/datatransfer/WearableDataApiClient;->getDataSyncDatabase()Lcom/google/android/wearable/datatransfer/internal/DataSyncDatabase;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper$CallbackFutureTask;->mRequest:Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;

    iget-object v5, v5, Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;->targetFile:Ljava/io/File;

    invoke-virtual {v4, v5}, Lcom/google/android/wearable/datatransfer/internal/DataSyncDatabase;->removeRequest(Ljava/io/File;)Z

    .line 747
    iget v4, v3, Lcom/google/android/wearable/datatransfer/internal/DataSyncer$Result;->commonStatusCode:I

    iget v5, v3, Lcom/google/android/wearable/datatransfer/internal/DataSyncer$Result;->appStatusCode:I

    invoke-direct {p0, v4, v5}, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper$CallbackFutureTask;->notifyFailure(II)V

    goto :goto_0

    .line 754
    :catch_0
    move-exception v1

    .line 756
    .local v1, "e":Ljava/util/concurrent/CancellationException;
    const/4 v2, 0x1

    .line 764
    goto :goto_1

    .line 751
    .end local v1    # "e":Ljava/util/concurrent/CancellationException;
    :cond_3
    iget-object v4, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper$CallbackFutureTask;->this$0:Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;

    # getter for: Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->mClient:Lcom/google/android/wearable/datatransfer/WearableDataApiClient;
    invoke-static {v4}, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->access$200(Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;)Lcom/google/android/wearable/datatransfer/WearableDataApiClient;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/wearable/datatransfer/WearableDataApiClient;->startDataSyncServiceIfPresent()V
    :try_end_2
    .catch Ljava/util/concurrent/CancellationException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 757
    :catch_1
    move-exception v1

    .line 758
    .local v1, "e":Ljava/lang/InterruptedException;
    :try_start_3
    const-string v4, "DataSyncServiceHelper"

    const-string v5, "Getting syncer result was interrupted (?!)"

    invoke-static {v4, v5, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 759
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Thread;->interrupt()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 771
    .end local v1    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v4

    if-nez v2, :cond_4

    .line 772
    const-string v5, "DataSyncServiceHelper"

    const-string v6, "Failed to handle syncer completion"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 773
    invoke-direct {p0, v10, v9}, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper$CallbackFutureTask;->notifyFailure(II)V

    :cond_4
    throw v4

    .line 760
    :catch_2
    move-exception v1

    .line 761
    .local v1, "e":Ljava/util/concurrent/ExecutionException;
    :try_start_4
    const-string v4, "DataSyncServiceHelper"

    const-string v5, "Uncaught exception in DataSyncer"

    invoke-static {v4, v5, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 762
    const/16 v4, 0x8

    const/4 v5, 0x0

    invoke-direct {p0, v4, v5}, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper$CallbackFutureTask;->notifyFailure(II)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 763
    const/4 v2, 0x1

    goto/16 :goto_1
.end method
