.class Lcom/google/android/wearable/datatransfer/internal/DataTransferClient$CallbackFutureTask;
.super Ljava/util/concurrent/FutureTask;
.source "DataTransferClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CallbackFutureTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/concurrent/FutureTask",
        "<",
        "Lcom/google/android/wearable/datatransfer/internal/DataReceiver$Result;",
        ">;"
    }
.end annotation


# instance fields
.field private final mConnectionId:J

.field final synthetic this$0:Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;


# direct methods
.method constructor <init>(Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;Lcom/google/android/wearable/datatransfer/internal/ReceiveDataCallable;J)V
    .locals 1
    .param p2, "callable"    # Lcom/google/android/wearable/datatransfer/internal/ReceiveDataCallable;
    .param p3, "connectionId"    # J

    .prologue
    .line 361
    iput-object p1, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient$CallbackFutureTask;->this$0:Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;

    .line 362
    invoke-direct {p0, p2}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    .line 363
    iput-wide p3, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient$CallbackFutureTask;->mConnectionId:J

    .line 364
    return-void
.end method


# virtual methods
.method protected done()V
    .locals 12

    .prologue
    const/4 v2, 0x0

    .line 368
    const/4 v5, 0x0

    .line 369
    .local v5, "result":Lcom/google/android/wearable/datatransfer/internal/DataReceiver$Result;
    const/4 v3, 0x0

    .line 371
    .local v3, "cancelled":Z
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient$CallbackFutureTask;->get()Ljava/lang/Object;

    move-result-object v7

    move-object v0, v7

    check-cast v0, Lcom/google/android/wearable/datatransfer/internal/DataReceiver$Result;

    move-object v5, v0
    :try_end_0
    .catch Ljava/util/concurrent/CancellationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_2

    .line 380
    :goto_0
    const-string v7, "DataTransferClient"

    invoke-static {v7}, Lcom/google/android/wearable/datatransfer/internal/DebugLog;->isLoggable(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 381
    const-string v7, "DataTransferClient"

    const-string v8, "DataReceiver with connectionId = %s is finished. result = %s"

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    iget-wide v10, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient$CallbackFutureTask;->mConnectionId:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    aput-object v10, v9, v2

    const/4 v10, 0x1

    aput-object v5, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 386
    :cond_0
    if-nez v3, :cond_1

    .line 387
    if-nez v5, :cond_2

    move v6, v2

    .line 388
    .local v6, "serverStatusCode":I
    :goto_1
    if-nez v5, :cond_3

    .line 389
    .local v2, "appErrorCode":I
    :goto_2
    iget-object v7, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient$CallbackFutureTask;->this$0:Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;

    iget-wide v8, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient$CallbackFutureTask;->mConnectionId:J

    # invokes: Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;->terminateConnection(JII)V
    invoke-static {v7, v8, v9, v6, v2}, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;->access$700(Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;JII)V

    .line 391
    .end local v2    # "appErrorCode":I
    .end local v6    # "serverStatusCode":I
    :cond_1
    return-void

    .line 372
    :catch_0
    move-exception v4

    .line 373
    .local v4, "e":Ljava/util/concurrent/CancellationException;
    const/4 v3, 0x1

    .line 379
    goto :goto_0

    .line 374
    .end local v4    # "e":Ljava/util/concurrent/CancellationException;
    :catch_1
    move-exception v4

    .line 375
    .local v4, "e":Ljava/lang/InterruptedException;
    const-string v7, "DataTransferClient"

    const-string v8, "Getting receiver result was interrupted (?!)"

    invoke-static {v7, v8, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 376
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    .line 377
    .end local v4    # "e":Ljava/lang/InterruptedException;
    :catch_2
    move-exception v4

    .line 378
    .local v4, "e":Ljava/util/concurrent/ExecutionException;
    const-string v7, "DataTransferClient"

    const-string v8, "Uncaught exception in ReceiveDataCallable"

    invoke-static {v7, v8, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 387
    .end local v4    # "e":Ljava/util/concurrent/ExecutionException;
    :cond_2
    iget v6, v5, Lcom/google/android/wearable/datatransfer/internal/DataReceiver$Result;->serverStatusCode:I

    goto :goto_1

    .line 388
    .restart local v6    # "serverStatusCode":I
    :cond_3
    iget v2, v5, Lcom/google/android/wearable/datatransfer/internal/DataReceiver$Result;->appErrorCode:I

    goto :goto_2
.end method
