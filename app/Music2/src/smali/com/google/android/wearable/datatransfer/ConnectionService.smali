.class public abstract Lcom/google/android/wearable/datatransfer/ConnectionService;
.super Landroid/app/Service;
.source "ConnectionService.java"

# interfaces
.implements Lcom/google/android/wearable/datatransfer/internal/FileDescriptorOpener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/wearable/datatransfer/ConnectionService$ConnectionTerminatedHandler;,
        Lcom/google/android/wearable/datatransfer/ConnectionService$ConnectionServiceBinder;,
        Lcom/google/android/wearable/datatransfer/ConnectionService$OpenFileDescriptorResult;
    }
.end annotation


# instance fields
.field private mApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

.field private final mBinder:Landroid/os/IBinder;

.field private mBound:Z

.field private mClient:Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;

.field private final mClientCallback:Lcom/google/android/wearable/datatransfer/internal/DataTransferClient$Callback;

.field private mConnectionTerminatedHandler:Lcom/google/android/wearable/datatransfer/ConnectionService$ConnectionTerminatedHandler;

.field private mLatestStartId:I

.field private mServer:Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;

.field private final mServerCallback:Lcom/google/android/wearable/datatransfer/internal/DataTransferServer$Callback;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 123
    new-instance v0, Lcom/google/android/wearable/datatransfer/ConnectionService$ConnectionServiceBinder;

    invoke-direct {v0, p0}, Lcom/google/android/wearable/datatransfer/ConnectionService$ConnectionServiceBinder;-><init>(Lcom/google/android/wearable/datatransfer/ConnectionService;)V

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/ConnectionService;->mBinder:Landroid/os/IBinder;

    .line 125
    new-instance v0, Lcom/google/android/wearable/datatransfer/ConnectionService$1;

    invoke-direct {v0, p0}, Lcom/google/android/wearable/datatransfer/ConnectionService$1;-><init>(Lcom/google/android/wearable/datatransfer/ConnectionService;)V

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/ConnectionService;->mServerCallback:Lcom/google/android/wearable/datatransfer/internal/DataTransferServer$Callback;

    .line 132
    new-instance v0, Lcom/google/android/wearable/datatransfer/ConnectionService$2;

    invoke-direct {v0, p0}, Lcom/google/android/wearable/datatransfer/ConnectionService$2;-><init>(Lcom/google/android/wearable/datatransfer/ConnectionService;)V

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/ConnectionService;->mClientCallback:Lcom/google/android/wearable/datatransfer/internal/DataTransferClient$Callback;

    .line 329
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/wearable/datatransfer/ConnectionService;)Lcom/google/android/wearable/datatransfer/ConnectionService$ConnectionTerminatedHandler;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/wearable/datatransfer/ConnectionService;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/ConnectionService;->mConnectionTerminatedHandler:Lcom/google/android/wearable/datatransfer/ConnectionService$ConnectionTerminatedHandler;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/wearable/datatransfer/ConnectionService;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/wearable/datatransfer/ConnectionService;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/google/android/wearable/datatransfer/ConnectionService;->onConnectionTerminated()V

    return-void
.end method

.method private onConnectionTerminated()V
    .locals 1

    .prologue
    .line 281
    iget-boolean v0, p0, Lcom/google/android/wearable/datatransfer/ConnectionService;->mBound:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/ConnectionService;->mServer:Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;

    invoke-virtual {v0}, Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;->hasOpenConnections()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/ConnectionService;->mClient:Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;

    invoke-virtual {v0}, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;->hasOpenConnections()Z

    move-result v0

    if-nez v0, :cond_0

    .line 282
    iget v0, p0, Lcom/google/android/wearable/datatransfer/ConnectionService;->mLatestStartId:I

    invoke-virtual {p0, v0}, Lcom/google/android/wearable/datatransfer/ConnectionService;->stopSelfResult(I)Z

    .line 284
    :cond_0
    return-void
.end method


# virtual methods
.method public final onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 189
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/wearable/datatransfer/ConnectionService;->mBound:Z

    .line 190
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/ConnectionService;->mBinder:Landroid/os/IBinder;

    return-object v0
.end method

.method public onCreate()V
    .locals 3

    .prologue
    .line 204
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 205
    new-instance v0, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    invoke-direct {v0, p0}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;-><init>(Landroid/content/Context;)V

    sget-object v1, Lcom/google/android/gms/wearable/Wearable;->API:Lcom/google/android/gms/common/api/Api;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;->addApi(Lcom/google/android/gms/common/api/Api;)Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    move-result-object v0

    new-instance v1, Lcom/google/android/wearable/datatransfer/ConnectionService$3;

    invoke-direct {v1, p0}, Lcom/google/android/wearable/datatransfer/ConnectionService$3;-><init>(Lcom/google/android/wearable/datatransfer/ConnectionService;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;->addOnConnectionFailedListener(Lcom/google/android/gms/common/api/GoogleApiClient$OnConnectionFailedListener;)Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;->build()Lcom/google/android/gms/common/api/GoogleApiClient;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/ConnectionService;->mApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    .line 215
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/ConnectionService;->mApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/GoogleApiClient;->connect()V

    .line 216
    new-instance v0, Lcom/google/android/wearable/datatransfer/ConnectionService$ConnectionTerminatedHandler;

    invoke-direct {v0, p0}, Lcom/google/android/wearable/datatransfer/ConnectionService$ConnectionTerminatedHandler;-><init>(Lcom/google/android/wearable/datatransfer/ConnectionService;)V

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/ConnectionService;->mConnectionTerminatedHandler:Lcom/google/android/wearable/datatransfer/ConnectionService$ConnectionTerminatedHandler;

    .line 217
    new-instance v0, Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;

    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/ConnectionService;->mApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    iget-object v2, p0, Lcom/google/android/wearable/datatransfer/ConnectionService;->mServerCallback:Lcom/google/android/wearable/datatransfer/internal/DataTransferServer$Callback;

    invoke-direct {v0, v1, p0, v2}, Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;-><init>(Lcom/google/android/gms/common/api/GoogleApiClient;Lcom/google/android/wearable/datatransfer/internal/FileDescriptorOpener;Lcom/google/android/wearable/datatransfer/internal/DataTransferServer$Callback;)V

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/ConnectionService;->mServer:Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;

    .line 218
    new-instance v0, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;

    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/ConnectionService;->mApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    iget-object v2, p0, Lcom/google/android/wearable/datatransfer/ConnectionService;->mClientCallback:Lcom/google/android/wearable/datatransfer/internal/DataTransferClient$Callback;

    invoke-direct {v0, v1, v2}, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;-><init>(Lcom/google/android/gms/common/api/GoogleApiClient;Lcom/google/android/wearable/datatransfer/internal/DataTransferClient$Callback;)V

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/ConnectionService;->mClient:Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;

    .line 219
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 223
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/ConnectionService;->mApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/GoogleApiClient;->disconnect()V

    .line 224
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/ConnectionService;->mServer:Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;

    invoke-virtual {v0}, Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;->shutdown()V

    .line 225
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/ConnectionService;->mClient:Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;

    invoke-virtual {v0}, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;->shutdown()V

    .line 226
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/ConnectionService;->mConnectionTerminatedHandler:Lcom/google/android/wearable/datatransfer/ConnectionService$ConnectionTerminatedHandler;

    invoke-virtual {v0}, Lcom/google/android/wearable/datatransfer/ConnectionService$ConnectionTerminatedHandler;->onServiceDestroyed()V

    .line 227
    return-void
.end method

.method final onMessageReceived(Lcom/google/android/gms/wearable/MessageEvent;)Z
    .locals 1
    .param p1, "messageEvent"    # Lcom/google/android/gms/wearable/MessageEvent;

    .prologue
    .line 241
    invoke-interface {p1}, Lcom/google/android/gms/wearable/MessageEvent;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;->isHandledPath(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 242
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/ConnectionService;->mServer:Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;

    invoke-virtual {v0, p1}, Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;->onMessageReceived(Lcom/google/android/gms/wearable/MessageEvent;)Z

    move-result v0

    .line 247
    :goto_0
    return v0

    .line 244
    :cond_0
    invoke-interface {p1}, Lcom/google/android/gms/wearable/MessageEvent;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;->isHandledPath(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 245
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/ConnectionService;->mClient:Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;

    invoke-virtual {v0, p1}, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;->onMessageReceived(Lcom/google/android/gms/wearable/MessageEvent;)Z

    move-result v0

    goto :goto_0

    .line 247
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onStartCommand(Landroid/content/Intent;II)I
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 183
    iput p3, p0, Lcom/google/android/wearable/datatransfer/ConnectionService;->mLatestStartId:I

    .line 184
    const/4 v0, 0x1

    return v0
.end method

.method public final onUnbind(Landroid/content/Intent;)Z
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v1, 0x0

    .line 195
    iput-boolean v1, p0, Lcom/google/android/wearable/datatransfer/ConnectionService;->mBound:Z

    .line 196
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/ConnectionService;->mServer:Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;

    invoke-virtual {v0}, Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;->hasOpenConnections()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/ConnectionService;->mClient:Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;

    invoke-virtual {v0}, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;->hasOpenConnections()Z

    move-result v0

    if-nez v0, :cond_0

    .line 197
    iget v0, p0, Lcom/google/android/wearable/datatransfer/ConnectionService;->mLatestStartId:I

    invoke-virtual {p0, v0}, Lcom/google/android/wearable/datatransfer/ConnectionService;->stopSelfResult(I)Z

    .line 199
    :cond_0
    return v1
.end method

.method final openConnection(Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 6
    .param p2, "nodeId"    # Ljava/lang/String;
    .param p3, "path"    # Ljava/lang/String;
    .param p4, "offset"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult",
            "<",
            "Lcom/google/android/wearable/datatransfer/DataTransferApi$OpenConnectionResult;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "J)V"
        }
    .end annotation

    .prologue
    .line 268
    .local p1, "pendingResult":Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;, "Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult<Lcom/google/android/wearable/datatransfer/DataTransferApi$OpenConnectionResult;>;"
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/ConnectionService;->mClient:Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-wide v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;->openConnection(Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;Ljava/lang/String;Ljava/lang/String;J)V

    .line 269
    return-void
.end method

.method final stopAllServerConnections(Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 273
    .local p1, "pendingResult":Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;, "Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult<Ljava/lang/Integer;>;"
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/ConnectionService;->mServer:Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;

    invoke-virtual {v0}, Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;->shutdown()V

    .line 274
    new-instance v0, Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;

    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/ConnectionService;->mApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    iget-object v2, p0, Lcom/google/android/wearable/datatransfer/ConnectionService;->mServerCallback:Lcom/google/android/wearable/datatransfer/internal/DataTransferServer$Callback;

    invoke-direct {v0, v1, p0, v2}, Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;-><init>(Lcom/google/android/gms/common/api/GoogleApiClient;Lcom/google/android/wearable/datatransfer/internal/FileDescriptorOpener;Lcom/google/android/wearable/datatransfer/internal/DataTransferServer$Callback;)V

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/ConnectionService;->mServer:Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;

    .line 276
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/ConnectionService;->mConnectionTerminatedHandler:Lcom/google/android/wearable/datatransfer/ConnectionService$ConnectionTerminatedHandler;

    invoke-virtual {v0}, Lcom/google/android/wearable/datatransfer/ConnectionService$ConnectionTerminatedHandler;->notifyConnectionTerminated()V

    .line 277
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;->setResult(Ljava/lang/Object;)V

    .line 278
    return-void
.end method
