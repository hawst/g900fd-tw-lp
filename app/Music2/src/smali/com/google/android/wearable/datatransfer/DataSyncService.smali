.class public final Lcom/google/android/wearable/datatransfer/DataSyncService;
.super Landroid/app/Service;
.source "DataSyncService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/wearable/datatransfer/DataSyncService$MyCompletionListener;,
        Lcom/google/android/wearable/datatransfer/DataSyncService$DataSyncServiceBinder;
    }
.end annotation


# instance fields
.field private final mBinder:Landroid/os/IBinder;

.field private mHelper:Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 65
    new-instance v0, Lcom/google/android/wearable/datatransfer/DataSyncService$DataSyncServiceBinder;

    invoke-direct {v0, p0}, Lcom/google/android/wearable/datatransfer/DataSyncService$DataSyncServiceBinder;-><init>(Lcom/google/android/wearable/datatransfer/DataSyncService;)V

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/DataSyncService;->mBinder:Landroid/os/IBinder;

    .line 148
    return-void
.end method

.method private static getRetryPolicyFromMetadata(Landroid/os/Bundle;)Lcom/google/android/wearable/datatransfer/internal/RetryPolicy;
    .locals 5
    .param p0, "metadata"    # Landroid/os/Bundle;

    .prologue
    .line 123
    const-string v3, "RETRY_BASE_DELAY_MILLIS"

    sget-object v4, Lcom/google/android/wearable/datatransfer/internal/RetryPolicy;->DEFAULT:Lcom/google/android/wearable/datatransfer/internal/RetryPolicy;

    iget v4, v4, Lcom/google/android/wearable/datatransfer/internal/RetryPolicy;->retryBaseDelayMillis:I

    invoke-virtual {p0, v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 125
    .local v0, "retryBaseDelayMillis":I
    const-string v3, "RETRY_MAX_DELAY_MILLIS"

    sget-object v4, Lcom/google/android/wearable/datatransfer/internal/RetryPolicy;->DEFAULT:Lcom/google/android/wearable/datatransfer/internal/RetryPolicy;

    iget v4, v4, Lcom/google/android/wearable/datatransfer/internal/RetryPolicy;->retryMaxDelayMillis:I

    invoke-virtual {p0, v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 127
    .local v2, "retryMaxDelayMillis":I
    const-string v3, "RETRY_COUNT_MAX"

    sget-object v4, Lcom/google/android/wearable/datatransfer/internal/RetryPolicy;->DEFAULT:Lcom/google/android/wearable/datatransfer/internal/RetryPolicy;

    iget v4, v4, Lcom/google/android/wearable/datatransfer/internal/RetryPolicy;->retryCountMax:I

    invoke-virtual {p0, v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 129
    .local v1, "retryCountMax":I
    new-instance v3, Lcom/google/android/wearable/datatransfer/internal/RetryPolicy;

    invoke-direct {v3, v0, v2, v1}, Lcom/google/android/wearable/datatransfer/internal/RetryPolicy;-><init>(III)V

    return-object v3
.end method

.method private getServiceMetadata()Landroid/os/Bundle;
    .locals 6

    .prologue
    .line 106
    new-instance v0, Landroid/content/ComponentName;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-direct {v0, p0, v4}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 107
    .local v0, "componentName":Landroid/content/ComponentName;
    invoke-virtual {p0}, Lcom/google/android/wearable/datatransfer/DataSyncService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 108
    .local v2, "packageManager":Landroid/content/pm/PackageManager;
    if-eqz v2, :cond_0

    .line 110
    const/16 v4, 0x80

    :try_start_0
    invoke-virtual {v2, v0, v4}, Landroid/content/pm/PackageManager;->getServiceInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ServiceInfo;

    move-result-object v3

    .line 112
    .local v3, "serviceInfo":Landroid/content/pm/ServiceInfo;
    if-eqz v3, :cond_0

    .line 113
    iget-object v4, v3, Landroid/content/pm/ServiceInfo;->metaData:Landroid/os/Bundle;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 119
    .end local v3    # "serviceInfo":Landroid/content/pm/ServiceInfo;
    :goto_0
    return-object v4

    .line 115
    :catch_0
    move-exception v1

    .line 116
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v4, "DataSyncService"

    const-string v5, "Can\'t load meta-data tags."

    invoke-static {v4, v5, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 119
    .end local v1    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/DataSyncService;->mBinder:Landroid/os/IBinder;

    return-object v0
.end method

.method public onCreate()V
    .locals 5

    .prologue
    .line 77
    invoke-direct {p0}, Lcom/google/android/wearable/datatransfer/DataSyncService;->getServiceMetadata()Landroid/os/Bundle;

    move-result-object v0

    .line 78
    .local v0, "metadata":Landroid/os/Bundle;
    if-eqz v0, :cond_1

    invoke-static {v0}, Lcom/google/android/wearable/datatransfer/DataSyncService;->getRetryPolicyFromMetadata(Landroid/os/Bundle;)Lcom/google/android/wearable/datatransfer/internal/RetryPolicy;

    move-result-object v1

    .line 81
    .local v1, "policy":Lcom/google/android/wearable/datatransfer/internal/RetryPolicy;
    :goto_0
    const-string v2, "DataSyncService"

    invoke-static {v2}, Lcom/google/android/wearable/datatransfer/internal/DebugLog;->isLoggable(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 82
    const-string v2, "DataSyncService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Starting with retry policy: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 84
    :cond_0
    invoke-static {p0, v1}, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->newInstance(Landroid/content/Context;Lcom/google/android/wearable/datatransfer/internal/RetryPolicy;)Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/wearable/datatransfer/DataSyncService;->mHelper:Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;

    .line 85
    return-void

    .line 78
    .end local v1    # "policy":Lcom/google/android/wearable/datatransfer/internal/RetryPolicy;
    :cond_1
    sget-object v1, Lcom/google/android/wearable/datatransfer/internal/RetryPolicy;->DEFAULT:Lcom/google/android/wearable/datatransfer/internal/RetryPolicy;

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/DataSyncService;->mHelper:Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;

    invoke-virtual {v0}, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->onStopped()V

    .line 100
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/DataSyncService;->mHelper:Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;

    .line 101
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 102
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/DataSyncService;->mHelper:Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;

    new-instance v1, Lcom/google/android/wearable/datatransfer/DataSyncService$MyCompletionListener;

    invoke-direct {v1, p0, p3}, Lcom/google/android/wearable/datatransfer/DataSyncService$MyCompletionListener;-><init>(Lcom/google/android/wearable/datatransfer/DataSyncService;I)V

    invoke-virtual {v0, p1, v1}, Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper;->onIntentReceived(Landroid/content/Intent;Lcom/google/android/wearable/datatransfer/internal/DataSyncServiceHelper$CompletionListener;)V

    .line 92
    const/4 v0, 0x3

    return v0
.end method
