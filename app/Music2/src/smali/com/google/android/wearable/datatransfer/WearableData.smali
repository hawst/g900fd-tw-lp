.class public final Lcom/google/android/wearable/datatransfer/WearableData;
.super Ljava/lang/Object;
.source "WearableData.java"


# static fields
.field public static final DataSyncApi:Lcom/google/android/wearable/datatransfer/DataSyncApi;

.field public static final DataTransferApi:Lcom/google/android/wearable/datatransfer/DataTransferApi;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12
    new-instance v0, Lcom/google/android/wearable/datatransfer/internal/DataTransferApiImpl;

    invoke-direct {v0}, Lcom/google/android/wearable/datatransfer/internal/DataTransferApiImpl;-><init>()V

    sput-object v0, Lcom/google/android/wearable/datatransfer/WearableData;->DataTransferApi:Lcom/google/android/wearable/datatransfer/DataTransferApi;

    .line 13
    new-instance v0, Lcom/google/android/wearable/datatransfer/internal/DataSyncApiImpl;

    invoke-direct {v0}, Lcom/google/android/wearable/datatransfer/internal/DataSyncApiImpl;-><init>()V

    sput-object v0, Lcom/google/android/wearable/datatransfer/WearableData;->DataSyncApi:Lcom/google/android/wearable/datatransfer/DataSyncApi;

    return-void
.end method
