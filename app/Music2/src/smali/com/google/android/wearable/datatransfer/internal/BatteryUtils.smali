.class final Lcom/google/android/wearable/datatransfer/internal/BatteryUtils;
.super Ljava/lang/Object;
.source "BatteryUtils.java"


# direct methods
.method static getBatteryChargePercentage(Landroid/content/Intent;)I
    .locals 4
    .param p0, "batteryStatusIntent"    # Landroid/content/Intent;

    .prologue
    const/4 v3, -0x1

    .line 43
    const-string v2, "level"

    invoke-virtual {p0, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 44
    .local v0, "level":I
    const-string v2, "scale"

    invoke-virtual {p0, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 45
    .local v1, "scale":I
    if-eq v0, v3, :cond_0

    if-ne v1, v3, :cond_1

    .line 48
    :cond_0
    const/4 v2, 0x5

    .line 50
    :goto_0
    return v2

    :cond_1
    mul-int/lit8 v2, v0, 0x64

    div-int/2addr v2, v1

    goto :goto_0
.end method

.method static getBatteryStatus(Landroid/content/Context;)Landroid/content/Intent;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 19
    const/4 v0, 0x0

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method static isOnRealPowerSource(Landroid/content/Intent;)Z
    .locals 3
    .param p0, "batteryStatusIntent"    # Landroid/content/Intent;

    .prologue
    const/4 v1, 0x0

    .line 33
    const-string v2, "plugged"

    invoke-virtual {p0, v2, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 34
    .local v0, "plugged":I
    and-int/lit8 v2, v0, 0x3

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method
