.class final Lcom/google/android/wearable/datatransfer/ConnectionService$ConnectionTerminatedHandler;
.super Landroid/os/Handler;
.source "ConnectionService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/wearable/datatransfer/ConnectionService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ConnectionTerminatedHandler"
.end annotation


# instance fields
.field private mService:Lcom/google/android/wearable/datatransfer/ConnectionService;


# direct methods
.method public constructor <init>(Lcom/google/android/wearable/datatransfer/ConnectionService;)V
    .locals 1
    .param p1, "service"    # Lcom/google/android/wearable/datatransfer/ConnectionService;

    .prologue
    .line 336
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 337
    iput-object p1, p0, Lcom/google/android/wearable/datatransfer/ConnectionService$ConnectionTerminatedHandler;->mService:Lcom/google/android/wearable/datatransfer/ConnectionService;

    .line 338
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 364
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/ConnectionService$ConnectionTerminatedHandler;->mService:Lcom/google/android/wearable/datatransfer/ConnectionService;

    if-eqz v0, :cond_0

    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 365
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/ConnectionService$ConnectionTerminatedHandler;->mService:Lcom/google/android/wearable/datatransfer/ConnectionService;

    # invokes: Lcom/google/android/wearable/datatransfer/ConnectionService;->onConnectionTerminated()V
    invoke-static {v0}, Lcom/google/android/wearable/datatransfer/ConnectionService;->access$100(Lcom/google/android/wearable/datatransfer/ConnectionService;)V

    .line 367
    :cond_0
    return-void
.end method

.method notifyConnectionTerminated()V
    .locals 1

    .prologue
    .line 359
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/wearable/datatransfer/ConnectionService$ConnectionTerminatedHandler;->sendEmptyMessage(I)Z

    .line 360
    return-void
.end method

.method onServiceDestroyed()V
    .locals 1

    .prologue
    .line 349
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/ConnectionService$ConnectionTerminatedHandler;->mService:Lcom/google/android/wearable/datatransfer/ConnectionService;

    .line 350
    return-void
.end method
