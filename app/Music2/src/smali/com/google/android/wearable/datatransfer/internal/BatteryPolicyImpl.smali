.class final Lcom/google/android/wearable/datatransfer/internal/BatteryPolicyImpl;
.super Ljava/lang/Object;
.source "BatteryPolicyImpl.java"

# interfaces
.implements Lcom/google/android/wearable/datatransfer/internal/BatteryPolicy;


# static fields
.field private static final OFF_POWER_POLL_SYNCING_DELAY_MILLIS:J

.field private static final ON_POWER_POLL_NOT_SYNCING_DELAY_MILLIS:J

.field private static final ON_POWER_POLL_SYNCING_DELAY_MILLIS:J


# instance fields
.field private final mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x5

    .line 35
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/wearable/datatransfer/internal/BatteryPolicyImpl;->ON_POWER_POLL_SYNCING_DELAY_MILLIS:J

    .line 43
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/wearable/datatransfer/internal/BatteryPolicyImpl;->ON_POWER_POLL_NOT_SYNCING_DELAY_MILLIS:J

    .line 52
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xa

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/wearable/datatransfer/internal/BatteryPolicyImpl;->OFF_POWER_POLL_SYNCING_DELAY_MILLIS:J

    return-void
.end method

.method constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    iput-object p1, p0, Lcom/google/android/wearable/datatransfer/internal/BatteryPolicyImpl;->mContext:Landroid/content/Context;

    .line 60
    return-void
.end method

.method private getPollDelayMillis(Landroid/content/Intent;)Ljava/lang/Long;
    .locals 4
    .param p1, "batteryStatusIntent"    # Landroid/content/Intent;

    .prologue
    .line 78
    invoke-static {p1}, Lcom/google/android/wearable/datatransfer/internal/BatteryUtils;->isOnRealPowerSource(Landroid/content/Intent;)Z

    move-result v0

    .line 79
    .local v0, "onPower":Z
    invoke-static {p1}, Lcom/google/android/wearable/datatransfer/internal/BatteryPolicyImpl;->shouldSync(Landroid/content/Intent;)Z

    move-result v1

    .line 80
    .local v1, "shouldSync":Z
    if-eqz v1, :cond_1

    .line 81
    if-eqz v0, :cond_0

    .line 82
    sget-wide v2, Lcom/google/android/wearable/datatransfer/internal/BatteryPolicyImpl;->ON_POWER_POLL_SYNCING_DELAY_MILLIS:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 92
    :goto_0
    return-object v2

    .line 84
    :cond_0
    sget-wide v2, Lcom/google/android/wearable/datatransfer/internal/BatteryPolicyImpl;->OFF_POWER_POLL_SYNCING_DELAY_MILLIS:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    goto :goto_0

    .line 87
    :cond_1
    if-eqz v0, :cond_2

    .line 88
    sget-wide v2, Lcom/google/android/wearable/datatransfer/internal/BatteryPolicyImpl;->ON_POWER_POLL_NOT_SYNCING_DELAY_MILLIS:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    goto :goto_0

    .line 92
    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private static shouldSync(Landroid/content/Intent;)Z
    .locals 2
    .param p0, "batteryStatusIntent"    # Landroid/content/Intent;

    .prologue
    .line 103
    invoke-static {p0}, Lcom/google/android/wearable/datatransfer/internal/BatteryUtils;->isOnRealPowerSource(Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v0, 0xf

    .line 106
    .local v0, "lowBatteryPercentageThreshold":I
    :goto_0
    invoke-static {p0}, Lcom/google/android/wearable/datatransfer/internal/BatteryUtils;->getBatteryChargePercentage(Landroid/content/Intent;)I

    move-result v1

    if-le v1, v0, :cond_1

    const/4 v1, 0x1

    :goto_1
    return v1

    .line 103
    .end local v0    # "lowBatteryPercentageThreshold":I
    :cond_0
    const/16 v0, 0x32

    goto :goto_0

    .line 106
    .restart local v0    # "lowBatteryPercentageThreshold":I
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method


# virtual methods
.method public apply()Lcom/google/android/wearable/datatransfer/internal/BatteryPolicy$Decision;
    .locals 4

    .prologue
    .line 64
    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/internal/BatteryPolicyImpl;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/wearable/datatransfer/internal/BatteryUtils;->getBatteryStatus(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 65
    .local v0, "batteryStatusIntent":Landroid/content/Intent;
    new-instance v1, Lcom/google/android/wearable/datatransfer/internal/BatteryPolicy$Decision;

    invoke-static {v0}, Lcom/google/android/wearable/datatransfer/internal/BatteryPolicyImpl;->shouldSync(Landroid/content/Intent;)Z

    move-result v2

    invoke-direct {p0, v0}, Lcom/google/android/wearable/datatransfer/internal/BatteryPolicyImpl;->getPollDelayMillis(Landroid/content/Intent;)Ljava/lang/Long;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/google/android/wearable/datatransfer/internal/BatteryPolicy$Decision;-><init>(ZLjava/lang/Long;)V

    return-object v1
.end method
