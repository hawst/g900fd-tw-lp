.class public abstract Lcom/google/android/wearable/datatransfer/internal/BaseDataSyncListenerService;
.super Landroid/app/IntentService;
.source "BaseDataSyncListenerService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/wearable/datatransfer/internal/BaseDataSyncListenerService$1;,
        Lcom/google/android/wearable/datatransfer/internal/BaseDataSyncListenerService$OnSyncingStartedRequest;
    }
.end annotation


# static fields
.field private static final ON_SYNCING_STARTED_REQUEST:Lcom/google/android/wearable/datatransfer/internal/BaseDataSyncListenerService$OnSyncingStartedRequest;


# instance fields
.field private final mCallbackQueue:Ljava/util/Queue;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 43
    new-instance v0, Lcom/google/android/wearable/datatransfer/internal/BaseDataSyncListenerService$OnSyncingStartedRequest;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/wearable/datatransfer/internal/BaseDataSyncListenerService$OnSyncingStartedRequest;-><init>(Lcom/google/android/wearable/datatransfer/internal/BaseDataSyncListenerService$1;)V

    sput-object v0, Lcom/google/android/wearable/datatransfer/internal/BaseDataSyncListenerService;->ON_SYNCING_STARTED_REQUEST:Lcom/google/android/wearable/datatransfer/internal/BaseDataSyncListenerService$OnSyncingStartedRequest;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 60
    const-string v0, "DataSyncListenerService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 56
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/BaseDataSyncListenerService;->mCallbackQueue:Ljava/util/Queue;

    .line 61
    return-void
.end method
