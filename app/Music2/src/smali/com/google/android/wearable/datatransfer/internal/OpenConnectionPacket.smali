.class final Lcom/google/android/wearable/datatransfer/internal/OpenConnectionPacket;
.super Ljava/lang/Object;
.source "OpenConnectionPacket.java"


# instance fields
.field public final proto:Lcom/google/android/wearable/datatransfer/proto/PacketProtos$OpenConnectionPacket;


# direct methods
.method private constructor <init>(Lcom/google/android/wearable/datatransfer/proto/PacketProtos$OpenConnectionPacket;)V
    .locals 1
    .param p1, "proto"    # Lcom/google/android/wearable/datatransfer/proto/PacketProtos$OpenConnectionPacket;

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    const-string v0, "proto"

    invoke-static {p1, v0}, Lcom/google/android/wearable/datatransfer/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$OpenConnectionPacket;

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/OpenConnectionPacket;->proto:Lcom/google/android/wearable/datatransfer/proto/PacketProtos$OpenConnectionPacket;

    .line 63
    return-void
.end method

.method public static fromBytes([B)Lcom/google/android/wearable/datatransfer/internal/OpenConnectionPacket;
    .locals 7
    .param p0, "data"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/wearable/datatransfer/internal/InvalidRequestException;
        }
    .end annotation

    .prologue
    const/16 v6, 0x800

    .line 40
    new-instance v1, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$OpenConnectionPacket;

    invoke-direct {v1}, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$OpenConnectionPacket;-><init>()V

    .line 42
    .local v1, "packet":Lcom/google/android/wearable/datatransfer/proto/PacketProtos$OpenConnectionPacket;
    :try_start_0
    invoke-static {v1, p0}, Lcom/google/protobuf/nano/MessageNano;->mergeFrom(Lcom/google/protobuf/nano/MessageNano;[B)Lcom/google/protobuf/nano/MessageNano;
    :try_end_0
    .catch Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException; {:try_start_0 .. :try_end_0} :catch_0

    .line 47
    iget-wide v2, v1, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$OpenConnectionPacket;->offset:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-gez v2, :cond_0

    .line 48
    new-instance v2, Lcom/google/android/wearable/datatransfer/internal/InvalidRequestException;

    const-string v3, "Negative offset"

    invoke-direct {v2, v3}, Lcom/google/android/wearable/datatransfer/internal/InvalidRequestException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 43
    :catch_0
    move-exception v0

    .line 44
    .local v0, "e":Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;
    new-instance v2, Lcom/google/android/wearable/datatransfer/internal/InvalidRequestException;

    const-string v3, "Invalid OpenConnectionPacket proto"

    invoke-direct {v2, v3, v0}, Lcom/google/android/wearable/datatransfer/internal/InvalidRequestException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    .line 50
    .end local v0    # "e":Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;
    :cond_0
    iget-object v2, v1, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$OpenConnectionPacket;->path:Ljava/lang/String;

    if-nez v2, :cond_1

    .line 51
    new-instance v2, Lcom/google/android/wearable/datatransfer/internal/InvalidRequestException;

    const-string v3, "Missing path"

    invoke-direct {v2, v3}, Lcom/google/android/wearable/datatransfer/internal/InvalidRequestException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 53
    :cond_1
    iget-object v2, v1, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$OpenConnectionPacket;->path:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-le v2, v6, :cond_2

    .line 54
    new-instance v2, Lcom/google/android/wearable/datatransfer/internal/InvalidRequestException;

    const-string v3, "path length > MAX_PATH_LENGTH_BYTES(%s)"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/android/wearable/datatransfer/internal/InvalidRequestException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 58
    :cond_2
    new-instance v2, Lcom/google/android/wearable/datatransfer/internal/OpenConnectionPacket;

    invoke-direct {v2, v1}, Lcom/google/android/wearable/datatransfer/internal/OpenConnectionPacket;-><init>(Lcom/google/android/wearable/datatransfer/proto/PacketProtos$OpenConnectionPacket;)V

    return-object v2
.end method

.method public static newPacket(Ljava/lang/String;JJ)Lcom/google/android/wearable/datatransfer/internal/OpenConnectionPacket;
    .locals 9
    .param p0, "path"    # Ljava/lang/String;
    .param p1, "offset"    # J
    .param p3, "clientConnectionId"    # J

    .prologue
    const/16 v7, 0x800

    .line 24
    :try_start_0
    const-string v3, "UTF-8"

    invoke-virtual {p0, v3}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 28
    .local v2, "pathBytes":[B
    array-length v3, v2

    if-le v3, v7, :cond_0

    .line 29
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "path length > MAX_PATH_LENGTH_BYTES(%s)"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 25
    .end local v2    # "pathBytes":[B
    :catch_0
    move-exception v0

    .line 26
    .local v0, "e":Ljava/io/UnsupportedEncodingException;
    new-instance v3, Ljava/lang/RuntimeException;

    invoke-direct {v3, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v3

    .line 32
    .end local v0    # "e":Ljava/io/UnsupportedEncodingException;
    .restart local v2    # "pathBytes":[B
    :cond_0
    new-instance v1, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$OpenConnectionPacket;

    invoke-direct {v1}, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$OpenConnectionPacket;-><init>()V

    .line 33
    .local v1, "packet":Lcom/google/android/wearable/datatransfer/proto/PacketProtos$OpenConnectionPacket;
    iput-wide p3, v1, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$OpenConnectionPacket;->clientConnectionId:J

    .line 34
    iput-wide p1, v1, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$OpenConnectionPacket;->offset:J

    .line 35
    iput-object p0, v1, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$OpenConnectionPacket;->path:Ljava/lang/String;

    .line 36
    new-instance v3, Lcom/google/android/wearable/datatransfer/internal/OpenConnectionPacket;

    invoke-direct {v3, v1}, Lcom/google/android/wearable/datatransfer/internal/OpenConnectionPacket;-><init>(Lcom/google/android/wearable/datatransfer/proto/PacketProtos$OpenConnectionPacket;)V

    return-object v3
.end method


# virtual methods
.method public toBytes()[B
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/OpenConnectionPacket;->proto:Lcom/google/android/wearable/datatransfer/proto/PacketProtos$OpenConnectionPacket;

    invoke-static {v0}, Lcom/google/protobuf/nano/MessageNano;->toByteArray(Lcom/google/protobuf/nano/MessageNano;)[B

    move-result-object v0

    return-object v0
.end method
