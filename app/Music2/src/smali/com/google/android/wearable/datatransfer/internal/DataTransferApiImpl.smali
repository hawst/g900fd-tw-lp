.class public final Lcom/google/android/wearable/datatransfer/internal/DataTransferApiImpl;
.super Ljava/lang/Object;
.source "DataTransferApiImpl.java"

# interfaces
.implements Lcom/google/android/wearable/datatransfer/DataTransferApi;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000(I)Lcom/google/android/wearable/datatransfer/DataTransferApi$OpenConnectionResult;
    .locals 1
    .param p0, "x0"    # I

    .prologue
    .line 15
    invoke-static {p0}, Lcom/google/android/wearable/datatransfer/internal/DataTransferApiImpl;->failedOpenConnectionResult(I)Lcom/google/android/wearable/datatransfer/DataTransferApi$OpenConnectionResult;

    move-result-object v0

    return-object v0
.end method

.method private static failedOpenConnectionResult(I)Lcom/google/android/wearable/datatransfer/DataTransferApi$OpenConnectionResult;
    .locals 3
    .param p0, "commonStatusCode"    # I

    .prologue
    const/4 v2, 0x0

    .line 69
    new-instance v0, Lcom/google/android/wearable/datatransfer/internal/OpenConnectionResultImpl;

    const/4 v1, 0x0

    invoke-direct {v0, v1, p0, v2, v2}, Lcom/google/android/wearable/datatransfer/internal/OpenConnectionResultImpl;-><init>(Lcom/google/android/wearable/datatransfer/Connection;III)V

    return-object v0
.end method

.method private static newOpenConnectionPendingResult()Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult",
            "<",
            "Lcom/google/android/wearable/datatransfer/DataTransferApi$OpenConnectionResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 38
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    .line 39
    .local v0, "looper":Landroid/os/Looper;
    new-instance v1, Lcom/google/android/wearable/datatransfer/internal/DataTransferApiImpl$1;

    invoke-direct {v1, v0}, Lcom/google/android/wearable/datatransfer/internal/DataTransferApiImpl$1;-><init>(Landroid/os/Looper;)V

    return-object v1
.end method


# virtual methods
.method public openConnection(Lcom/google/android/wearable/datatransfer/WearableDataApiClient;Ljava/lang/String;Ljava/lang/String;J)Lcom/google/android/wearable/datatransfer/WearableDataCompat$PendingResult;
    .locals 6
    .param p1, "client"    # Lcom/google/android/wearable/datatransfer/WearableDataApiClient;
    .param p2, "nodeId"    # Ljava/lang/String;
    .param p3, "path"    # Ljava/lang/String;
    .param p4, "offset"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/wearable/datatransfer/WearableDataApiClient;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "J)",
            "Lcom/google/android/wearable/datatransfer/WearableDataCompat$PendingResult",
            "<",
            "Lcom/google/android/wearable/datatransfer/DataTransferApi$OpenConnectionResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 20
    invoke-static {}, Lcom/google/android/wearable/datatransfer/internal/DataTransferApiImpl;->newOpenConnectionPendingResult()Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;

    move-result-object v1

    .local v1, "pendingResult":Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;, "Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult<Lcom/google/android/wearable/datatransfer/DataTransferApi$OpenConnectionResult;>;"
    move-object v0, p1

    move-object v2, p2

    move-object v3, p3

    move-wide v4, p4

    .line 22
    invoke-virtual/range {v0 .. v5}, Lcom/google/android/wearable/datatransfer/WearableDataApiClient;->openConnection(Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;Ljava/lang/String;Ljava/lang/String;J)V

    .line 23
    return-object v1
.end method
