.class public interface abstract Lcom/google/android/wearable/datatransfer/DataTransferApi$OpenConnectionResult;
.super Ljava/lang/Object;
.source "DataTransferApi.java"

# interfaces
.implements Lcom/google/android/gms/common/api/Releasable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/wearable/datatransfer/DataTransferApi;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OpenConnectionResult"
.end annotation


# virtual methods
.method public abstract getAppStatusCode()I
.end method

.method public abstract getCommonStatusCode()I
.end method

.method public abstract getConnection()Lcom/google/android/wearable/datatransfer/Connection;
.end method
