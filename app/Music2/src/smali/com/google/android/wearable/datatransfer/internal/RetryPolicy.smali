.class public final Lcom/google/android/wearable/datatransfer/internal/RetryPolicy;
.super Ljava/lang/Object;
.source "RetryPolicy.java"


# static fields
.field public static final DEFAULT:Lcom/google/android/wearable/datatransfer/internal/RetryPolicy;


# instance fields
.field public final retryBaseDelayMillis:I

.field public final retryCountMax:I

.field public final retryIntervalCalculator:Lcom/google/android/wearable/datatransfer/internal/RetryIntervalCalculator;

.field public final retryMaxDelayMillis:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    .line 15
    new-instance v0, Lcom/google/android/wearable/datatransfer/internal/RetryPolicy;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x5

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    long-to-int v1, v2

    sget-object v2, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v4, 0x6

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    long-to-int v2, v2

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/wearable/datatransfer/internal/RetryPolicy;-><init>(III)V

    sput-object v0, Lcom/google/android/wearable/datatransfer/internal/RetryPolicy;->DEFAULT:Lcom/google/android/wearable/datatransfer/internal/RetryPolicy;

    return-void
.end method

.method public constructor <init>(III)V
    .locals 6
    .param p1, "retryBaseDelayMillis"    # I
    .param p2, "retryMaxDelayMillis"    # I
    .param p3, "retryCountMax"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    if-ltz p1, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "retryBaseDelayMillis negative: %s"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, Lcom/google/android/wearable/datatransfer/internal/Preconditions;->checkArgument(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 37
    if-lt p2, p1, :cond_1

    move v0, v1

    :goto_1
    const-string v3, "retryMaxDelayMillis less than retryBaseDelayMillis: %s < %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-static {v0, v3, v4}, Lcom/google/android/wearable/datatransfer/internal/Preconditions;->checkArgument(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 42
    if-ltz p3, :cond_2

    move v0, v1

    :goto_2
    const-string v3, "retryCountMax negative: %s"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-static {v0, v3, v1}, Lcom/google/android/wearable/datatransfer/internal/Preconditions;->checkArgument(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 46
    iput p1, p0, Lcom/google/android/wearable/datatransfer/internal/RetryPolicy;->retryBaseDelayMillis:I

    .line 47
    iput p2, p0, Lcom/google/android/wearable/datatransfer/internal/RetryPolicy;->retryMaxDelayMillis:I

    .line 48
    iput p3, p0, Lcom/google/android/wearable/datatransfer/internal/RetryPolicy;->retryCountMax:I

    .line 49
    new-instance v0, Lcom/google/android/wearable/datatransfer/internal/RetryIntervalCalculator;

    invoke-direct {v0, p1, p2}, Lcom/google/android/wearable/datatransfer/internal/RetryIntervalCalculator;-><init>(II)V

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/RetryPolicy;->retryIntervalCalculator:Lcom/google/android/wearable/datatransfer/internal/RetryIntervalCalculator;

    .line 51
    return-void

    :cond_0
    move v0, v2

    .line 34
    goto :goto_0

    :cond_1
    move v0, v2

    .line 37
    goto :goto_1

    :cond_2
    move v0, v2

    .line 42
    goto :goto_2
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 55
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "RetryPolicy{retryBaseDelayMillis="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/wearable/datatransfer/internal/RetryPolicy;->retryBaseDelayMillis:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", retryMaxDelayMillis="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/wearable/datatransfer/internal/RetryPolicy;->retryMaxDelayMillis:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", retryCountMax="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/wearable/datatransfer/internal/RetryPolicy;->retryCountMax:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
