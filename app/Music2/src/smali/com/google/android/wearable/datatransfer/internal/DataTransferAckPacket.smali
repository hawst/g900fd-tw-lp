.class final Lcom/google/android/wearable/datatransfer/internal/DataTransferAckPacket;
.super Ljava/lang/Object;
.source "DataTransferAckPacket.java"


# instance fields
.field public final proto:Lcom/google/android/wearable/datatransfer/proto/PacketProtos$AckPacket;


# direct methods
.method private constructor <init>(Lcom/google/android/wearable/datatransfer/proto/PacketProtos$AckPacket;)V
    .locals 1
    .param p1, "proto"    # Lcom/google/android/wearable/datatransfer/proto/PacketProtos$AckPacket;

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    const-string v0, "proto"

    invoke-static {p1, v0}, Lcom/google/android/wearable/datatransfer/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$AckPacket;

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferAckPacket;->proto:Lcom/google/android/wearable/datatransfer/proto/PacketProtos$AckPacket;

    .line 52
    return-void
.end method

.method public static fromBytes([B)Lcom/google/android/wearable/datatransfer/internal/DataTransferAckPacket;
    .locals 4
    .param p0, "data"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/wearable/datatransfer/internal/InvalidRequestException;
        }
    .end annotation

    .prologue
    .line 40
    new-instance v1, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$AckPacket;

    invoke-direct {v1}, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$AckPacket;-><init>()V

    .line 42
    .local v1, "packet":Lcom/google/android/wearable/datatransfer/proto/PacketProtos$AckPacket;
    :try_start_0
    invoke-static {v1, p0}, Lcom/google/protobuf/nano/MessageNano;->mergeFrom(Lcom/google/protobuf/nano/MessageNano;[B)Lcom/google/protobuf/nano/MessageNano;
    :try_end_0
    .catch Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException; {:try_start_0 .. :try_end_0} :catch_0

    .line 47
    new-instance v2, Lcom/google/android/wearable/datatransfer/internal/DataTransferAckPacket;

    invoke-direct {v2, v1}, Lcom/google/android/wearable/datatransfer/internal/DataTransferAckPacket;-><init>(Lcom/google/android/wearable/datatransfer/proto/PacketProtos$AckPacket;)V

    return-object v2

    .line 43
    :catch_0
    move-exception v0

    .line 44
    .local v0, "e":Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;
    new-instance v2, Lcom/google/android/wearable/datatransfer/internal/InvalidRequestException;

    const-string v3, "Invalid AckPacket proto"

    invoke-direct {v2, v3, v0}, Lcom/google/android/wearable/datatransfer/internal/InvalidRequestException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
.end method

.method public static newPacket(JJZ)Lcom/google/android/wearable/datatransfer/internal/DataTransferAckPacket;
    .locals 2
    .param p0, "clientConnectionId"    # J
    .param p2, "serverConnectionId"    # J
    .param p4, "fin"    # Z

    .prologue
    .line 25
    new-instance v0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$AckPacket;

    invoke-direct {v0}, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$AckPacket;-><init>()V

    .line 26
    .local v0, "packet":Lcom/google/android/wearable/datatransfer/proto/PacketProtos$AckPacket;
    iput-wide p0, v0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$AckPacket;->clientConnectionId:J

    .line 27
    iput-wide p2, v0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$AckPacket;->serverConnectionId:J

    .line 28
    iput-boolean p4, v0, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$AckPacket;->finalPacket:Z

    .line 29
    new-instance v1, Lcom/google/android/wearable/datatransfer/internal/DataTransferAckPacket;

    invoke-direct {v1, v0}, Lcom/google/android/wearable/datatransfer/internal/DataTransferAckPacket;-><init>(Lcom/google/android/wearable/datatransfer/proto/PacketProtos$AckPacket;)V

    return-object v1
.end method


# virtual methods
.method public toBytes()[B
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferAckPacket;->proto:Lcom/google/android/wearable/datatransfer/proto/PacketProtos$AckPacket;

    invoke-static {v0}, Lcom/google/protobuf/nano/MessageNano;->toByteArray(Lcom/google/protobuf/nano/MessageNano;)[B

    move-result-object v0

    return-object v0
.end method
