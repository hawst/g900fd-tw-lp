.class public Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;
.super Ljava/lang/Object;
.source "DataTransferServer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/wearable/datatransfer/internal/DataTransferServer$1;,
        Lcom/google/android/wearable/datatransfer/internal/DataTransferServer$MyDataSenderCallbacks;,
        Lcom/google/android/wearable/datatransfer/internal/DataTransferServer$Callback;
    }
.end annotation


# instance fields
.field private final mCallback:Lcom/google/android/wearable/datatransfer/internal/DataTransferServer$Callback;

.field private final mClient:Lcom/google/android/gms/common/api/GoogleApiClient;

.field private final mDataSenderCallbacks:Lcom/google/android/wearable/datatransfer/internal/DataSender$Callbacks;

.field private final mDataSenders:Landroid/util/LongSparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LongSparseArray",
            "<",
            "Lcom/google/android/wearable/datatransfer/internal/DataSender;",
            ">;"
        }
    .end annotation
.end field

.field private mFileDescriptorOpener:Lcom/google/android/wearable/datatransfer/internal/FileDescriptorOpener;

.field private final mLock:Ljava/lang/Object;

.field private final mNextConnectionIdGenerator:Ljava/util/Random;

.field private final mOpenConnections:Ljava/util/concurrent/atomic/AtomicInteger;

.field private volatile mShutdown:Z

.field private final mUploadsThreadPool:Ljava/util/concurrent/ExecutorService;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/api/GoogleApiClient;Lcom/google/android/wearable/datatransfer/internal/FileDescriptorOpener;Lcom/google/android/wearable/datatransfer/internal/DataTransferServer$Callback;)V
    .locals 3
    .param p1, "client"    # Lcom/google/android/gms/common/api/GoogleApiClient;
    .param p2, "fileDescriptorOpener"    # Lcom/google/android/wearable/datatransfer/internal/FileDescriptorOpener;
    .param p3, "callback"    # Lcom/google/android/wearable/datatransfer/internal/DataTransferServer$Callback;

    .prologue
    const/16 v2, 0x8

    .line 99
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    new-instance v0, Ljava/security/SecureRandom;

    invoke-direct {v0}, Ljava/security/SecureRandom;-><init>()V

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;->mNextConnectionIdGenerator:Ljava/util/Random;

    .line 60
    new-instance v0, Lcom/google/android/wearable/datatransfer/internal/DataTransferServer$MyDataSenderCallbacks;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/wearable/datatransfer/internal/DataTransferServer$MyDataSenderCallbacks;-><init>(Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;Lcom/google/android/wearable/datatransfer/internal/DataTransferServer$1;)V

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;->mDataSenderCallbacks:Lcom/google/android/wearable/datatransfer/internal/DataSender$Callbacks;

    .line 66
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;->mOpenConnections:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 81
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;->mLock:Ljava/lang/Object;

    .line 86
    new-instance v0, Landroid/util/LongSparseArray;

    invoke-direct {v0, v2}, Landroid/util/LongSparseArray;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;->mDataSenders:Landroid/util/LongSparseArray;

    .line 95
    new-instance v0, Lcom/google/android/wearable/datatransfer/internal/CustomThreadFactory;

    const-string v1, "DataTransferServer"

    invoke-direct {v0, v1}, Lcom/google/android/wearable/datatransfer/internal/CustomThreadFactory;-><init>(Ljava/lang/String;)V

    invoke-static {v2, v0}, Ljava/util/concurrent/Executors;->newFixedThreadPool(ILjava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;->mUploadsThreadPool:Ljava/util/concurrent/ExecutorService;

    .line 100
    const-string v0, "client"

    invoke-static {p1, v0}, Lcom/google/android/wearable/datatransfer/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/GoogleApiClient;

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;->mClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    .line 101
    const-string v0, "callback"

    invoke-static {p3, v0}, Lcom/google/android/wearable/datatransfer/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/wearable/datatransfer/internal/DataTransferServer$Callback;

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;->mCallback:Lcom/google/android/wearable/datatransfer/internal/DataTransferServer$Callback;

    .line 102
    const-string v0, "fileDescriptorOpener"

    invoke-static {p2, v0}, Lcom/google/android/wearable/datatransfer/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/wearable/datatransfer/internal/FileDescriptorOpener;

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;->mFileDescriptorOpener:Lcom/google/android/wearable/datatransfer/internal/FileDescriptorOpener;

    .line 104
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;

    .prologue
    .line 27
    iget-boolean v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;->mShutdown:Z

    return v0
.end method

.method static synthetic access$200(Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;->mLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;)Landroid/util/LongSparseArray;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;->mDataSenders:Landroid/util/LongSparseArray;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;)Ljava/util/concurrent/atomic/AtomicInteger;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;->mOpenConnections:Ljava/util/concurrent/atomic/AtomicInteger;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;)Lcom/google/android/wearable/datatransfer/internal/DataTransferServer$Callback;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;->mCallback:Lcom/google/android/wearable/datatransfer/internal/DataTransferServer$Callback;

    return-object v0
.end method

.method private handleAck(Lcom/google/android/gms/wearable/MessageEvent;)V
    .locals 7
    .param p1, "messageEvent"    # Lcom/google/android/gms/wearable/MessageEvent;

    .prologue
    .line 198
    const-string v5, "DataTransferServer"

    invoke-static {v5}, Lcom/google/android/wearable/datatransfer/internal/DebugLog;->isLoggable(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 199
    const-string v5, "DataTransferServer"

    const-string v6, "handleAck"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 201
    :cond_0
    iget-boolean v5, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;->mShutdown:Z

    if-eqz v5, :cond_2

    .line 202
    const-string v5, "DataTransferServer"

    invoke-static {v5}, Lcom/google/android/wearable/datatransfer/internal/DebugLog;->isLoggable(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 203
    const-string v5, "DataTransferServer"

    const-string v6, "already shutdown"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 227
    :cond_1
    :goto_0
    return-void

    .line 209
    :cond_2
    :try_start_0
    invoke-interface {p1}, Lcom/google/android/gms/wearable/MessageEvent;->getData()[B

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/wearable/datatransfer/internal/DataTransferAckPacket;->fromBytes([B)Lcom/google/android/wearable/datatransfer/internal/DataTransferAckPacket;
    :try_end_0
    .catch Lcom/google/android/wearable/datatransfer/internal/InvalidRequestException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 214
    .local v3, "packet":Lcom/google/android/wearable/datatransfer/internal/DataTransferAckPacket;
    iget-object v5, v3, Lcom/google/android/wearable/datatransfer/internal/DataTransferAckPacket;->proto:Lcom/google/android/wearable/datatransfer/proto/PacketProtos$AckPacket;

    iget-wide v0, v5, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$AckPacket;->serverConnectionId:J

    .line 216
    .local v0, "connectionId":J
    iget-object v6, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;->mLock:Ljava/lang/Object;

    monitor-enter v6

    .line 217
    :try_start_1
    iget-object v5, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;->mDataSenders:Landroid/util/LongSparseArray;

    invoke-virtual {v5, v0, v1}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/wearable/datatransfer/internal/DataSender;

    .line 218
    .local v4, "sender":Lcom/google/android/wearable/datatransfer/internal/DataSender;
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 219
    if-nez v4, :cond_3

    .line 220
    const-string v5, "DataTransferServer"

    const-string v6, "Received ACK for connection ID which isn\'t open"

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 210
    .end local v0    # "connectionId":J
    .end local v3    # "packet":Lcom/google/android/wearable/datatransfer/internal/DataTransferAckPacket;
    .end local v4    # "sender":Lcom/google/android/wearable/datatransfer/internal/DataSender;
    :catch_0
    move-exception v2

    .line 211
    .local v2, "e":Lcom/google/android/wearable/datatransfer/internal/InvalidRequestException;
    const-string v5, "DataTransferServer"

    const-string v6, "Bad DATA_TRANSFER_ACK received"

    invoke-static {v5, v6, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 218
    .end local v2    # "e":Lcom/google/android/wearable/datatransfer/internal/InvalidRequestException;
    .restart local v0    # "connectionId":J
    .restart local v3    # "packet":Lcom/google/android/wearable/datatransfer/internal/DataTransferAckPacket;
    :catchall_0
    move-exception v5

    :try_start_2
    monitor-exit v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v5

    .line 223
    .restart local v4    # "sender":Lcom/google/android/wearable/datatransfer/internal/DataSender;
    :cond_3
    const-string v5, "DataTransferServer"

    invoke-static {v5}, Lcom/google/android/wearable/datatransfer/internal/DebugLog;->isLoggable(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 224
    const-string v5, "DataTransferServer"

    const-string v6, "pass to sender"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 226
    :cond_4
    invoke-virtual {v4, v3}, Lcom/google/android/wearable/datatransfer/internal/DataSender;->onAckReceived(Lcom/google/android/wearable/datatransfer/internal/DataTransferAckPacket;)V

    goto :goto_0
.end method

.method private handleOpenConnection(Lcom/google/android/gms/wearable/MessageEvent;)V
    .locals 8
    .param p1, "messageEvent"    # Lcom/google/android/gms/wearable/MessageEvent;

    .prologue
    .line 165
    const-string v1, "DataTransferServer"

    invoke-static {v1}, Lcom/google/android/wearable/datatransfer/internal/DebugLog;->isLoggable(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 166
    const-string v1, "DataTransferServer"

    const-string v2, "handleOpenConnection"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 168
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;->mShutdown:Z

    if-eqz v1, :cond_2

    .line 169
    const-string v1, "DataTransferServer"

    invoke-static {v1}, Lcom/google/android/wearable/datatransfer/internal/DebugLog;->isLoggable(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 170
    const-string v1, "DataTransferServer"

    const-string v2, "already shutdown"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 188
    :cond_1
    :goto_0
    return-void

    .line 174
    :cond_2
    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;->mNextConnectionIdGenerator:Ljava/util/Random;

    invoke-virtual {v1}, Ljava/util/Random;->nextLong()J

    move-result-wide v4

    .line 175
    .local v4, "connectionId":J
    new-instance v0, Lcom/google/android/wearable/datatransfer/internal/SendDataRunnable;

    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;->mClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    iget-object v2, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;->mFileDescriptorOpener:Lcom/google/android/wearable/datatransfer/internal/FileDescriptorOpener;

    iget-object v6, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;->mDataSenderCallbacks:Lcom/google/android/wearable/datatransfer/internal/DataSender$Callbacks;

    move-object v3, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/wearable/datatransfer/internal/SendDataRunnable;-><init>(Lcom/google/android/gms/common/api/GoogleApiClient;Lcom/google/android/wearable/datatransfer/internal/FileDescriptorOpener;Lcom/google/android/gms/wearable/MessageEvent;JLcom/google/android/wearable/datatransfer/internal/DataSender$Callbacks;)V

    .line 177
    .local v0, "runnable":Lcom/google/android/wearable/datatransfer/internal/SendDataRunnable;
    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;->mOpenConnections:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    .line 178
    const/4 v7, 0x0

    .line 180
    .local v7, "submitted":Z
    :try_start_0
    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;->mUploadsThreadPool:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v1, v0}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 181
    const/4 v7, 0x1

    .line 183
    if-nez v7, :cond_1

    .line 185
    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;->mOpenConnections:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    goto :goto_0

    .line 183
    :catchall_0
    move-exception v1

    if-nez v7, :cond_3

    .line 185
    iget-object v2, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;->mOpenConnections:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    :cond_3
    throw v1
.end method

.method public static isHandledPath(Ljava/lang/String;)Z
    .locals 1
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 108
    const-string v0, "com.google.android.wearable.datatransfer.OPEN_CONNECTION"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "com.google.android.wearable.datatransfer.DATA_TRANSFER_ACK"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public hasOpenConnections()Z
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;->mOpenConnections:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onMessageReceived(Lcom/google/android/gms/wearable/MessageEvent;)Z
    .locals 3
    .param p1, "messageEvent"    # Lcom/google/android/gms/wearable/MessageEvent;

    .prologue
    const/4 v0, 0x1

    .line 119
    const-string v1, "com.google.android.wearable.datatransfer.OPEN_CONNECTION"

    invoke-interface {p1}, Lcom/google/android/gms/wearable/MessageEvent;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 120
    invoke-direct {p0, p1}, Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;->handleOpenConnection(Lcom/google/android/gms/wearable/MessageEvent;)V

    .line 127
    :goto_0
    return v0

    .line 123
    :cond_0
    const-string v1, "com.google.android.wearable.datatransfer.DATA_TRANSFER_ACK"

    invoke-interface {p1}, Lcom/google/android/gms/wearable/MessageEvent;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 124
    invoke-direct {p0, p1}, Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;->handleAck(Lcom/google/android/gms/wearable/MessageEvent;)V

    goto :goto_0

    .line 127
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public shutdown()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 135
    const-string v1, "DataTransferServer"

    invoke-static {v1}, Lcom/google/android/wearable/datatransfer/internal/DebugLog;->isLoggable(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 136
    const-string v1, "DataTransferServer"

    const-string v2, "shutdown"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 138
    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;->mShutdown:Z

    .line 139
    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;->mUploadsThreadPool:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v1}, Ljava/util/concurrent/ExecutorService;->shutdownNow()Ljava/util/List;

    .line 140
    iput-object v3, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;->mFileDescriptorOpener:Lcom/google/android/wearable/datatransfer/internal/FileDescriptorOpener;

    .line 141
    const/4 v0, 0x0

    .local v0, "index":I
    :goto_0
    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;->mDataSenders:Landroid/util/LongSparseArray;

    invoke-virtual {v1}, Landroid/util/LongSparseArray;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 142
    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;->mDataSenders:Landroid/util/LongSparseArray;

    invoke-virtual {v1, v0}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/wearable/datatransfer/internal/DataSender;

    invoke-virtual {v1}, Lcom/google/android/wearable/datatransfer/internal/DataSender;->cancel()V

    .line 141
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 144
    :cond_1
    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;->mDataSenders:Landroid/util/LongSparseArray;

    invoke-virtual {v1}, Landroid/util/LongSparseArray;->clear()V

    .line 145
    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;->mOpenConnections:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 147
    iput-object v3, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferServer;->mFileDescriptorOpener:Lcom/google/android/wearable/datatransfer/internal/FileDescriptorOpener;

    .line 148
    return-void
.end method
