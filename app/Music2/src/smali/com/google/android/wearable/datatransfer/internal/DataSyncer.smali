.class final Lcom/google/android/wearable/datatransfer/internal/DataSyncer;
.super Ljava/lang/Object;
.source "DataSyncer.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/wearable/datatransfer/internal/DataSyncer$Result;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/android/wearable/datatransfer/internal/DataSyncer$Result;",
        ">;"
    }
.end annotation


# instance fields
.field private final mApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

.field private mCancelled:Z

.field private final mClient:Lcom/google/android/wearable/datatransfer/WearableDataApiClient;

.field private final mLock:Ljava/lang/Object;

.field private final mPeerProvider:Lcom/google/android/wearable/datatransfer/internal/PeerProvider;

.field private mProcessingThread:Ljava/lang/Thread;

.field private final mRequest:Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;

.field private final mRetryPolicy:Lcom/google/android/wearable/datatransfer/internal/RetryPolicy;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/api/GoogleApiClient;Lcom/google/android/wearable/datatransfer/WearableDataApiClient;Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;Lcom/google/android/wearable/datatransfer/internal/PeerProvider;Lcom/google/android/wearable/datatransfer/internal/RetryPolicy;)V
    .locals 1
    .param p1, "apiClient"    # Lcom/google/android/gms/common/api/GoogleApiClient;
    .param p2, "client"    # Lcom/google/android/wearable/datatransfer/WearableDataApiClient;
    .param p3, "request"    # Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;
    .param p4, "peerProvider"    # Lcom/google/android/wearable/datatransfer/internal/PeerProvider;
    .param p5, "retryPolicy"    # Lcom/google/android/wearable/datatransfer/internal/RetryPolicy;

    .prologue
    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncer;->mLock:Ljava/lang/Object;

    .line 92
    const-string v0, "apiClient"

    invoke-static {p1, v0}, Lcom/google/android/wearable/datatransfer/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/GoogleApiClient;

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncer;->mApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    .line 93
    const-string v0, "client"

    invoke-static {p2, v0}, Lcom/google/android/wearable/datatransfer/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/wearable/datatransfer/WearableDataApiClient;

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncer;->mClient:Lcom/google/android/wearable/datatransfer/WearableDataApiClient;

    .line 94
    const-string v0, "request"

    invoke-static {p3, v0}, Lcom/google/android/wearable/datatransfer/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncer;->mRequest:Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;

    .line 95
    const-string v0, "peerProvider"

    invoke-static {p4, v0}, Lcom/google/android/wearable/datatransfer/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/wearable/datatransfer/internal/PeerProvider;

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncer;->mPeerProvider:Lcom/google/android/wearable/datatransfer/internal/PeerProvider;

    .line 96
    const-string v0, "retryPolicy"

    invoke-static {p5, v0}, Lcom/google/android/wearable/datatransfer/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/wearable/datatransfer/internal/RetryPolicy;

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncer;->mRetryPolicy:Lcom/google/android/wearable/datatransfer/internal/RetryPolicy;

    .line 97
    return-void
.end method

.method private checkAvailableSpaceInDirectory(Ljava/io/File;JJ)V
    .locals 10
    .param p1, "dir"    # Ljava/io/File;
    .param p2, "transferSize"    # J
    .param p4, "offset"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 284
    sub-long v4, p2, p4

    const-wide/16 v6, 0x0

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    const-wide/32 v6, 0x9600000

    add-long v0, v4, v6

    .line 286
    .local v0, "requiredSpace":J
    invoke-virtual {p1}, Ljava/io/File;->getUsableSpace()J

    move-result-wide v2

    .line 287
    .local v2, "usableSpace":J
    cmp-long v4, v2, v0

    if-gez v4, :cond_0

    .line 288
    new-instance v4, Ljava/io/IOException;

    const-string v5, "Not enough space on partition to sync file. Required: %d bytes, usable: %d bytes."

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 293
    :cond_0
    return-void
.end method

.method private download()Lcom/google/android/wearable/datatransfer/internal/DataSyncer$Result;
    .locals 14

    .prologue
    const/4 v4, 0x0

    .line 116
    const/16 v11, 0x400

    new-array v0, v11, [B

    .line 117
    .local v0, "buffer":[B
    const/4 v1, 0x0

    .line 118
    .local v1, "connection":Lcom/google/android/wearable/datatransfer/Connection;
    const/4 v3, 0x0

    .line 119
    .local v3, "inputStream":Ljava/io/InputStream;
    const/4 v9, 0x0

    .line 121
    .local v9, "outputStream":Ljava/io/OutputStream;
    :try_start_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v11

    if-eqz v11, :cond_0

    .line 122
    invoke-static {}, Lcom/google/android/wearable/datatransfer/internal/DataSyncer$Result;->forInterrupted()Lcom/google/android/wearable/datatransfer/internal/DataSyncer$Result;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v11

    .line 220
    invoke-static {v3}, Lcom/google/android/wearable/datatransfer/internal/Utils;->closeQuietly(Ljava/io/Closeable;)V

    .line 221
    invoke-static {v1}, Lcom/google/android/wearable/datatransfer/internal/Utils;->closeQuietly(Ljava/io/Closeable;)V

    .line 222
    invoke-static {v9}, Lcom/google/android/wearable/datatransfer/internal/Utils;->closeQuietly(Ljava/io/Closeable;)V

    :goto_0
    return-object v11

    .line 126
    :cond_0
    :try_start_1
    iget-object v11, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncer;->mRequest:Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;

    iget-object v11, v11, Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;->targetFile:Ljava/io/File;

    invoke-virtual {v11}, Ljava/io/File;->length()J

    move-result-wide v6

    .line 127
    .local v6, "offset":J
    invoke-direct {p0, v6, v7}, Lcom/google/android/wearable/datatransfer/internal/DataSyncer;->openRemoteConnection(J)Lcom/google/android/wearable/datatransfer/DataTransferApi$OpenConnectionResult;

    move-result-object v8

    .line 128
    .local v8, "openConnectionResult":Lcom/google/android/wearable/datatransfer/DataTransferApi$OpenConnectionResult;
    invoke-interface {v8}, Lcom/google/android/wearable/datatransfer/DataTransferApi$OpenConnectionResult;->getCommonStatusCode()I

    move-result v11

    const/16 v12, 0xe

    if-ne v11, v12, :cond_1

    .line 129
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Thread;->interrupt()V

    .line 130
    invoke-static {}, Lcom/google/android/wearable/datatransfer/internal/DataSyncer$Result;->forInterrupted()Lcom/google/android/wearable/datatransfer/internal/DataSyncer$Result;
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v11

    .line 220
    invoke-static {v3}, Lcom/google/android/wearable/datatransfer/internal/Utils;->closeQuietly(Ljava/io/Closeable;)V

    .line 221
    invoke-static {v1}, Lcom/google/android/wearable/datatransfer/internal/Utils;->closeQuietly(Ljava/io/Closeable;)V

    .line 222
    invoke-static {v9}, Lcom/google/android/wearable/datatransfer/internal/Utils;->closeQuietly(Ljava/io/Closeable;)V

    goto :goto_0

    .line 132
    :cond_1
    :try_start_2
    invoke-interface {v8}, Lcom/google/android/wearable/datatransfer/DataTransferApi$OpenConnectionResult;->getCommonStatusCode()I

    move-result v11

    if-eqz v11, :cond_6

    .line 133
    const-string v11, "DataSyncer"

    const-string v12, "Failed to open connection"

    invoke-static {v11, v12}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_4
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 139
    :try_start_3
    iget-object v11, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncer;->mPeerProvider:Lcom/google/android/wearable/datatransfer/internal/PeerProvider;

    iget-object v12, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncer;->mApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    iget-object v13, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncer;->mRequest:Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;

    iget-object v13, v13, Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;->remoteNodeId:Ljava/lang/String;

    invoke-interface {v11, v12, v13}, Lcom/google/android/wearable/datatransfer/internal/PeerProvider;->isPeerConnected(Lcom/google/android/gms/common/api/GoogleApiClient;Ljava/lang/String;)Z
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_4
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v11

    if-nez v11, :cond_2

    const/4 v4, 0x1

    .line 154
    .local v4, "isTransientFailure":Z
    :cond_2
    :goto_1
    if-eqz v4, :cond_5

    .line 155
    :try_start_4
    invoke-interface {v8}, Lcom/google/android/wearable/datatransfer/DataTransferApi$OpenConnectionResult;->getCommonStatusCode()I

    move-result v11

    invoke-interface {v8}, Lcom/google/android/wearable/datatransfer/DataTransferApi$OpenConnectionResult;->getAppStatusCode()I

    move-result v12

    invoke-static {v11, v12}, Lcom/google/android/wearable/datatransfer/internal/DataSyncer$Result;->forTransientFailure(II)Lcom/google/android/wearable/datatransfer/internal/DataSyncer$Result;
    :try_end_4
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v11

    .line 220
    invoke-static {v3}, Lcom/google/android/wearable/datatransfer/internal/Utils;->closeQuietly(Ljava/io/Closeable;)V

    .line 221
    invoke-static {v1}, Lcom/google/android/wearable/datatransfer/internal/Utils;->closeQuietly(Ljava/io/Closeable;)V

    .line 222
    invoke-static {v9}, Lcom/google/android/wearable/datatransfer/internal/Utils;->closeQuietly(Ljava/io/Closeable;)V

    goto :goto_0

    .line 141
    .end local v4    # "isTransientFailure":Z
    :catch_0
    move-exception v2

    .line 142
    .local v2, "e":Ljava/lang/InterruptedException;
    :try_start_5
    const-string v11, "DataSyncer"

    invoke-static {v11}, Lcom/google/android/wearable/datatransfer/internal/DebugLog;->isLoggable(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 143
    const-string v11, "DataSyncer"

    const-string v12, "interrupted while querying for connected peers"

    invoke-static {v11, v12, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 145
    :cond_3
    invoke-static {}, Lcom/google/android/wearable/datatransfer/internal/DataSyncer$Result;->forInterrupted()Lcom/google/android/wearable/datatransfer/internal/DataSyncer$Result;
    :try_end_5
    .catch Ljava/lang/RuntimeException; {:try_start_5 .. :try_end_5} :catch_4
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result-object v11

    .line 220
    invoke-static {v3}, Lcom/google/android/wearable/datatransfer/internal/Utils;->closeQuietly(Ljava/io/Closeable;)V

    .line 221
    invoke-static {v1}, Lcom/google/android/wearable/datatransfer/internal/Utils;->closeQuietly(Ljava/io/Closeable;)V

    .line 222
    invoke-static {v9}, Lcom/google/android/wearable/datatransfer/internal/Utils;->closeQuietly(Ljava/io/Closeable;)V

    goto :goto_0

    .line 146
    .end local v2    # "e":Ljava/lang/InterruptedException;
    :catch_1
    move-exception v2

    .line 147
    .local v2, "e":Ljava/util/concurrent/TimeoutException;
    :try_start_6
    const-string v11, "DataSyncer"

    const-string v12, "timed out while querying for connected peers"

    invoke-static {v11, v12}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 148
    const-string v11, "DataSyncer"

    invoke-static {v11}, Lcom/google/android/wearable/datatransfer/internal/DebugLog;->isLoggable(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 149
    const-string v11, "DataSyncer"

    const-string v12, "Timeout stack trace"

    invoke-static {v11, v12, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 152
    :cond_4
    const/4 v4, 0x1

    .restart local v4    # "isTransientFailure":Z
    goto :goto_1

    .line 159
    .end local v2    # "e":Ljava/util/concurrent/TimeoutException;
    :cond_5
    invoke-interface {v8}, Lcom/google/android/wearable/datatransfer/DataTransferApi$OpenConnectionResult;->getCommonStatusCode()I

    move-result v11

    invoke-interface {v8}, Lcom/google/android/wearable/datatransfer/DataTransferApi$OpenConnectionResult;->getAppStatusCode()I

    move-result v12

    invoke-static {v11, v12}, Lcom/google/android/wearable/datatransfer/internal/DataSyncer$Result;->forPermanentFailure(II)Lcom/google/android/wearable/datatransfer/internal/DataSyncer$Result;
    :try_end_6
    .catch Ljava/lang/RuntimeException; {:try_start_6 .. :try_end_6} :catch_4
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-result-object v11

    .line 220
    invoke-static {v3}, Lcom/google/android/wearable/datatransfer/internal/Utils;->closeQuietly(Ljava/io/Closeable;)V

    .line 221
    invoke-static {v1}, Lcom/google/android/wearable/datatransfer/internal/Utils;->closeQuietly(Ljava/io/Closeable;)V

    .line 222
    invoke-static {v9}, Lcom/google/android/wearable/datatransfer/internal/Utils;->closeQuietly(Ljava/io/Closeable;)V

    goto/16 :goto_0

    .line 165
    .end local v4    # "isTransientFailure":Z
    :cond_6
    :try_start_7
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v11

    if-eqz v11, :cond_7

    .line 166
    invoke-static {}, Lcom/google/android/wearable/datatransfer/internal/DataSyncer$Result;->forInterrupted()Lcom/google/android/wearable/datatransfer/internal/DataSyncer$Result;
    :try_end_7
    .catch Ljava/lang/RuntimeException; {:try_start_7 .. :try_end_7} :catch_4
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    move-result-object v11

    .line 220
    invoke-static {v3}, Lcom/google/android/wearable/datatransfer/internal/Utils;->closeQuietly(Ljava/io/Closeable;)V

    .line 221
    invoke-static {v1}, Lcom/google/android/wearable/datatransfer/internal/Utils;->closeQuietly(Ljava/io/Closeable;)V

    .line 222
    invoke-static {v9}, Lcom/google/android/wearable/datatransfer/internal/Utils;->closeQuietly(Ljava/io/Closeable;)V

    goto/16 :goto_0

    .line 168
    :cond_7
    :try_start_8
    invoke-interface {v8}, Lcom/google/android/wearable/datatransfer/DataTransferApi$OpenConnectionResult;->getConnection()Lcom/google/android/wearable/datatransfer/Connection;

    move-result-object v1

    .line 169
    if-nez v1, :cond_8

    .line 170
    const-string v11, "DataSyncer"

    const-string v12, "Successful open connection, but connection is null"

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 171
    const/16 v11, 0x8

    const/4 v12, 0x0

    invoke-static {v11, v12}, Lcom/google/android/wearable/datatransfer/internal/DataSyncer$Result;->forPermanentFailure(II)Lcom/google/android/wearable/datatransfer/internal/DataSyncer$Result;
    :try_end_8
    .catch Ljava/lang/RuntimeException; {:try_start_8 .. :try_end_8} :catch_4
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    move-result-object v11

    .line 220
    invoke-static {v3}, Lcom/google/android/wearable/datatransfer/internal/Utils;->closeQuietly(Ljava/io/Closeable;)V

    .line 221
    invoke-static {v1}, Lcom/google/android/wearable/datatransfer/internal/Utils;->closeQuietly(Ljava/io/Closeable;)V

    .line 222
    invoke-static {v9}, Lcom/google/android/wearable/datatransfer/internal/Utils;->closeQuietly(Ljava/io/Closeable;)V

    goto/16 :goto_0

    .line 174
    :cond_8
    :try_start_9
    invoke-interface {v1}, Lcom/google/android/wearable/datatransfer/Connection;->getInputStream()Ljava/io/InputStream;

    move-result-object v3

    .line 176
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v11

    if-eqz v11, :cond_9

    .line 177
    invoke-static {}, Lcom/google/android/wearable/datatransfer/internal/DataSyncer$Result;->forInterrupted()Lcom/google/android/wearable/datatransfer/internal/DataSyncer$Result;
    :try_end_9
    .catch Ljava/lang/RuntimeException; {:try_start_9 .. :try_end_9} :catch_4
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    move-result-object v11

    .line 220
    invoke-static {v3}, Lcom/google/android/wearable/datatransfer/internal/Utils;->closeQuietly(Ljava/io/Closeable;)V

    .line 221
    invoke-static {v1}, Lcom/google/android/wearable/datatransfer/internal/Utils;->closeQuietly(Ljava/io/Closeable;)V

    .line 222
    invoke-static {v9}, Lcom/google/android/wearable/datatransfer/internal/Utils;->closeQuietly(Ljava/io/Closeable;)V

    goto/16 :goto_0

    .line 180
    :cond_9
    :try_start_a
    iget-object v11, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncer;->mClient:Lcom/google/android/wearable/datatransfer/WearableDataApiClient;

    invoke-virtual {v11}, Lcom/google/android/wearable/datatransfer/WearableDataApiClient;->getDataSyncDatabase()Lcom/google/android/wearable/datatransfer/internal/DataSyncDatabase;

    move-result-object v11

    iget-object v12, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncer;->mRequest:Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;

    iget-object v12, v12, Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;->targetFile:Ljava/io/File;

    invoke-virtual {v11, v12}, Lcom/google/android/wearable/datatransfer/internal/DataSyncDatabase;->getRequest(Ljava/io/File;)Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;

    move-result-object v11

    if-nez v11, :cond_a

    .line 184
    invoke-static {}, Lcom/google/android/wearable/datatransfer/internal/DataSyncer$Result;->forInterrupted()Lcom/google/android/wearable/datatransfer/internal/DataSyncer$Result;
    :try_end_a
    .catch Ljava/lang/RuntimeException; {:try_start_a .. :try_end_a} :catch_4
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    move-result-object v11

    .line 220
    invoke-static {v3}, Lcom/google/android/wearable/datatransfer/internal/Utils;->closeQuietly(Ljava/io/Closeable;)V

    .line 221
    invoke-static {v1}, Lcom/google/android/wearable/datatransfer/internal/Utils;->closeQuietly(Ljava/io/Closeable;)V

    .line 222
    invoke-static {v9}, Lcom/google/android/wearable/datatransfer/internal/Utils;->closeQuietly(Ljava/io/Closeable;)V

    goto/16 :goto_0

    .line 187
    :cond_a
    :try_start_b
    invoke-interface {v1}, Lcom/google/android/wearable/datatransfer/Connection;->getSize()J

    move-result-wide v12

    invoke-direct {p0, v12, v13, v6, v7}, Lcom/google/android/wearable/datatransfer/internal/DataSyncer;->openLocalFile(JJ)Ljava/io/OutputStream;
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_2
    .catch Ljava/lang/RuntimeException; {:try_start_b .. :try_end_b} :catch_4
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    move-result-object v9

    .line 194
    :try_start_c
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v11

    if-eqz v11, :cond_b

    .line 195
    invoke-static {}, Lcom/google/android/wearable/datatransfer/internal/DataSyncer$Result;->forInterrupted()Lcom/google/android/wearable/datatransfer/internal/DataSyncer$Result;
    :try_end_c
    .catch Ljava/lang/RuntimeException; {:try_start_c .. :try_end_c} :catch_4
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    move-result-object v11

    .line 220
    invoke-static {v3}, Lcom/google/android/wearable/datatransfer/internal/Utils;->closeQuietly(Ljava/io/Closeable;)V

    .line 221
    invoke-static {v1}, Lcom/google/android/wearable/datatransfer/internal/Utils;->closeQuietly(Ljava/io/Closeable;)V

    .line 222
    invoke-static {v9}, Lcom/google/android/wearable/datatransfer/internal/Utils;->closeQuietly(Ljava/io/Closeable;)V

    goto/16 :goto_0

    .line 188
    :catch_2
    move-exception v2

    .line 190
    .local v2, "e":Ljava/io/IOException;
    :try_start_d
    const-string v11, "DataSyncer"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Failed to open target file: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 191
    const/16 v11, 0xd

    const/4 v12, 0x0

    invoke-static {v11, v12}, Lcom/google/android/wearable/datatransfer/internal/DataSyncer$Result;->forPermanentFailure(II)Lcom/google/android/wearable/datatransfer/internal/DataSyncer$Result;
    :try_end_d
    .catch Ljava/lang/RuntimeException; {:try_start_d .. :try_end_d} :catch_4
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    move-result-object v11

    .line 220
    invoke-static {v3}, Lcom/google/android/wearable/datatransfer/internal/Utils;->closeQuietly(Ljava/io/Closeable;)V

    .line 221
    invoke-static {v1}, Lcom/google/android/wearable/datatransfer/internal/Utils;->closeQuietly(Ljava/io/Closeable;)V

    .line 222
    invoke-static {v9}, Lcom/google/android/wearable/datatransfer/internal/Utils;->closeQuietly(Ljava/io/Closeable;)V

    goto/16 :goto_0

    .line 199
    .end local v2    # "e":Ljava/io/IOException;
    :cond_b
    const/4 v5, 0x0

    .line 202
    .local v5, "madeProgress":Z
    :cond_c
    :goto_2
    :try_start_e
    invoke-virtual {v3, v0}, Ljava/io/InputStream;->read([B)I

    move-result v10

    .local v10, "readCount":I
    const/4 v11, -0x1

    if-eq v10, v11, :cond_e

    .line 203
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v11

    if-eqz v11, :cond_d

    .line 204
    invoke-static {}, Lcom/google/android/wearable/datatransfer/internal/DataSyncer$Result;->forInterrupted()Lcom/google/android/wearable/datatransfer/internal/DataSyncer$Result;
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_3
    .catch Ljava/lang/RuntimeException; {:try_start_e .. :try_end_e} :catch_4
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    move-result-object v11

    .line 220
    invoke-static {v3}, Lcom/google/android/wearable/datatransfer/internal/Utils;->closeQuietly(Ljava/io/Closeable;)V

    .line 221
    invoke-static {v1}, Lcom/google/android/wearable/datatransfer/internal/Utils;->closeQuietly(Ljava/io/Closeable;)V

    .line 222
    invoke-static {v9}, Lcom/google/android/wearable/datatransfer/internal/Utils;->closeQuietly(Ljava/io/Closeable;)V

    goto/16 :goto_0

    .line 206
    :cond_d
    if-eqz v10, :cond_c

    .line 207
    const/4 v11, 0x0

    :try_start_f
    invoke-virtual {v9, v0, v11, v10}, Ljava/io/OutputStream;->write([BII)V
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_3
    .catch Ljava/lang/RuntimeException; {:try_start_f .. :try_end_f} :catch_4
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    .line 208
    const/4 v5, 0x1

    goto :goto_2

    .line 211
    .end local v10    # "readCount":I
    :catch_3
    move-exception v2

    .line 212
    .restart local v2    # "e":Ljava/io/IOException;
    :try_start_10
    invoke-direct {p0, v5, v2}, Lcom/google/android/wearable/datatransfer/internal/DataSyncer;->scheduleRetry(ZLjava/lang/Throwable;)Lcom/google/android/wearable/datatransfer/internal/DataSyncer$Result;
    :try_end_10
    .catch Ljava/lang/RuntimeException; {:try_start_10 .. :try_end_10} :catch_4
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    move-result-object v11

    .line 220
    invoke-static {v3}, Lcom/google/android/wearable/datatransfer/internal/Utils;->closeQuietly(Ljava/io/Closeable;)V

    .line 221
    invoke-static {v1}, Lcom/google/android/wearable/datatransfer/internal/Utils;->closeQuietly(Ljava/io/Closeable;)V

    .line 222
    invoke-static {v9}, Lcom/google/android/wearable/datatransfer/internal/Utils;->closeQuietly(Ljava/io/Closeable;)V

    goto/16 :goto_0

    .line 215
    .end local v2    # "e":Ljava/io/IOException;
    .restart local v10    # "readCount":I
    :cond_e
    :try_start_11
    invoke-static {}, Lcom/google/android/wearable/datatransfer/internal/DataSyncer$Result;->forSuccess()Lcom/google/android/wearable/datatransfer/internal/DataSyncer$Result;
    :try_end_11
    .catch Ljava/lang/RuntimeException; {:try_start_11 .. :try_end_11} :catch_4
    .catchall {:try_start_11 .. :try_end_11} :catchall_0

    move-result-object v11

    .line 220
    invoke-static {v3}, Lcom/google/android/wearable/datatransfer/internal/Utils;->closeQuietly(Ljava/io/Closeable;)V

    .line 221
    invoke-static {v1}, Lcom/google/android/wearable/datatransfer/internal/Utils;->closeQuietly(Ljava/io/Closeable;)V

    .line 222
    invoke-static {v9}, Lcom/google/android/wearable/datatransfer/internal/Utils;->closeQuietly(Ljava/io/Closeable;)V

    goto/16 :goto_0

    .line 216
    .end local v5    # "madeProgress":Z
    .end local v6    # "offset":J
    .end local v8    # "openConnectionResult":Lcom/google/android/wearable/datatransfer/DataTransferApi$OpenConnectionResult;
    .end local v10    # "readCount":I
    :catch_4
    move-exception v2

    .line 217
    .local v2, "e":Ljava/lang/RuntimeException;
    :try_start_12
    const-string v11, "DataSyncer"

    const-string v12, "Unexpected exception while downloading"

    invoke-static {v11, v12, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 218
    const/16 v11, 0x8

    const/4 v12, 0x0

    invoke-static {v11, v12}, Lcom/google/android/wearable/datatransfer/internal/DataSyncer$Result;->forPermanentFailure(II)Lcom/google/android/wearable/datatransfer/internal/DataSyncer$Result;
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_0

    move-result-object v11

    .line 220
    invoke-static {v3}, Lcom/google/android/wearable/datatransfer/internal/Utils;->closeQuietly(Ljava/io/Closeable;)V

    .line 221
    invoke-static {v1}, Lcom/google/android/wearable/datatransfer/internal/Utils;->closeQuietly(Ljava/io/Closeable;)V

    .line 222
    invoke-static {v9}, Lcom/google/android/wearable/datatransfer/internal/Utils;->closeQuietly(Ljava/io/Closeable;)V

    goto/16 :goto_0

    .line 220
    .end local v2    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v11

    invoke-static {v3}, Lcom/google/android/wearable/datatransfer/internal/Utils;->closeQuietly(Ljava/io/Closeable;)V

    .line 221
    invoke-static {v1}, Lcom/google/android/wearable/datatransfer/internal/Utils;->closeQuietly(Ljava/io/Closeable;)V

    .line 222
    invoke-static {v9}, Lcom/google/android/wearable/datatransfer/internal/Utils;->closeQuietly(Ljava/io/Closeable;)V

    throw v11
.end method

.method private getRetryDelayMillis(I)J
    .locals 2
    .param p1, "retryCount"    # I

    .prologue
    .line 337
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncer;->mRetryPolicy:Lcom/google/android/wearable/datatransfer/internal/RetryPolicy;

    iget-object v0, v0, Lcom/google/android/wearable/datatransfer/internal/RetryPolicy;->retryIntervalCalculator:Lcom/google/android/wearable/datatransfer/internal/RetryIntervalCalculator;

    invoke-virtual {v0, p1}, Lcom/google/android/wearable/datatransfer/internal/RetryIntervalCalculator;->getNextRetryDelay(I)I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method private openLocalFile(JJ)Ljava/io/OutputStream;
    .locals 7
    .param p1, "transferSize"    # J
    .param p3, "offset"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 262
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncer;->mRequest:Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;

    iget-object v0, v0, Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;->targetFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v1

    .line 263
    .local v1, "parentDir":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    move-result v0

    if-nez v0, :cond_0

    .line 264
    new-instance v0, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unable to create directory "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move-object v0, p0

    move-wide v2, p1

    move-wide v4, p3

    .line 267
    invoke-direct/range {v0 .. v5}, Lcom/google/android/wearable/datatransfer/internal/DataSyncer;->checkAvailableSpaceInDirectory(Ljava/io/File;JJ)V

    .line 268
    new-instance v0, Ljava/io/FileOutputStream;

    iget-object v2, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncer;->mRequest:Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;

    iget-object v2, v2, Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;->targetFile:Ljava/io/File;

    const/4 v3, 0x1

    invoke-direct {v0, v2, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V

    return-object v0
.end method

.method private openRemoteConnection(J)Lcom/google/android/wearable/datatransfer/DataTransferApi$OpenConnectionResult;
    .locals 7
    .param p1, "offset"    # J

    .prologue
    .line 244
    sget-object v0, Lcom/google/android/wearable/datatransfer/WearableData;->DataTransferApi:Lcom/google/android/wearable/datatransfer/DataTransferApi;

    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncer;->mClient:Lcom/google/android/wearable/datatransfer/WearableDataApiClient;

    iget-object v2, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncer;->mRequest:Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;

    iget-object v2, v2, Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;->remoteNodeId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncer;->mRequest:Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;

    iget-object v3, v3, Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;->path:Ljava/lang/String;

    move-wide v4, p1

    invoke-interface/range {v0 .. v5}, Lcom/google/android/wearable/datatransfer/DataTransferApi;->openConnection(Lcom/google/android/wearable/datatransfer/WearableDataApiClient;Ljava/lang/String;Ljava/lang/String;J)Lcom/google/android/wearable/datatransfer/WearableDataCompat$PendingResult;

    move-result-object v6

    .line 247
    .local v6, "openConnectionResult":Lcom/google/android/wearable/datatransfer/WearableDataCompat$PendingResult;, "Lcom/google/android/wearable/datatransfer/WearableDataCompat$PendingResult<Lcom/google/android/wearable/datatransfer/DataTransferApi$OpenConnectionResult;>;"
    const-wide/16 v0, 0x7530

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v6, v0, v1, v2}, Lcom/google/android/wearable/datatransfer/WearableDataCompat$PendingResult;->await(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/wearable/datatransfer/DataTransferApi$OpenConnectionResult;

    return-object v0
.end method

.method private scheduleRetry(ZLjava/lang/Throwable;)Lcom/google/android/wearable/datatransfer/internal/DataSyncer$Result;
    .locals 10
    .param p1, "madeProgress"    # Z
    .param p2, "cause"    # Ljava/lang/Throwable;

    .prologue
    const/16 v9, 0xd

    const/4 v3, 0x0

    .line 307
    if-eqz p1, :cond_1

    move v2, v3

    .line 310
    .local v2, "retryCount":I
    :goto_0
    iget-object v6, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncer;->mRetryPolicy:Lcom/google/android/wearable/datatransfer/internal/RetryPolicy;

    iget v6, v6, Lcom/google/android/wearable/datatransfer/internal/RetryPolicy;->retryCountMax:I

    if-lt v2, v6, :cond_2

    .line 311
    const-string v6, "DataSyncer"

    const-string v7, "error fetching data. aborting."

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 312
    if-eqz p2, :cond_0

    const-string v6, "DataSyncer"

    invoke-static {v6}, Lcom/google/android/wearable/datatransfer/internal/DebugLog;->isLoggable(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 313
    const-string v6, "DataSyncer"

    const-string v7, "Failure stack trace"

    invoke-static {v6, v7, p2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 316
    :cond_0
    invoke-static {v9, v3}, Lcom/google/android/wearable/datatransfer/internal/DataSyncer$Result;->forPermanentFailure(II)Lcom/google/android/wearable/datatransfer/internal/DataSyncer$Result;

    move-result-object v3

    .line 327
    :goto_1
    return-object v3

    .line 307
    .end local v2    # "retryCount":I
    :cond_1
    iget-object v6, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncer;->mRequest:Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;

    iget v2, v6, Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;->retryCount:I

    goto :goto_0

    .line 318
    .restart local v2    # "retryCount":I
    :cond_2
    iget-object v6, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncer;->mRequest:Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;

    iget v6, v6, Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;->retryCount:I

    invoke-direct {p0, v6}, Lcom/google/android/wearable/datatransfer/internal/DataSyncer;->getRetryDelayMillis(I)J

    move-result-wide v0

    .line 319
    .local v0, "delayMillis":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    add-long v4, v6, v0

    .line 321
    .local v4, "retryNextRequestTimestamp":J
    const-string v6, "DataSyncer"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "error fetching data. scheduling a retry in "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "ms"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 322
    if-eqz p2, :cond_3

    const-string v6, "DataSyncer"

    invoke-static {v6}, Lcom/google/android/wearable/datatransfer/internal/DebugLog;->isLoggable(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 323
    const-string v6, "DataSyncer"

    const-string v7, "Failure stack trace"

    invoke-static {v6, v7, p2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 325
    :cond_3
    iget-object v6, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncer;->mClient:Lcom/google/android/wearable/datatransfer/WearableDataApiClient;

    invoke-virtual {v6}, Lcom/google/android/wearable/datatransfer/WearableDataApiClient;->getDataSyncDatabase()Lcom/google/android/wearable/datatransfer/internal/DataSyncDatabase;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncer;->mRequest:Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;

    iget-object v7, v7, Lcom/google/android/wearable/datatransfer/internal/DataSyncRequest;->targetFile:Ljava/io/File;

    add-int/lit8 v8, v2, 0x1

    invoke-virtual {v6, v7, v8, v4, v5}, Lcom/google/android/wearable/datatransfer/internal/DataSyncDatabase;->scheduleRetry(Ljava/io/File;IJ)V

    .line 327
    invoke-static {v9, v3}, Lcom/google/android/wearable/datatransfer/internal/DataSyncer$Result;->forTransientFailure(II)Lcom/google/android/wearable/datatransfer/internal/DataSyncer$Result;

    move-result-object v3

    goto :goto_1
.end method


# virtual methods
.method public call()Lcom/google/android/wearable/datatransfer/internal/DataSyncer$Result;
    .locals 2

    .prologue
    .line 101
    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncer;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 102
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncer;->mCancelled:Z

    if-eqz v0, :cond_0

    .line 104
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 105
    invoke-static {}, Lcom/google/android/wearable/datatransfer/internal/DataSyncer$Result;->forInterrupted()Lcom/google/android/wearable/datatransfer/internal/DataSyncer$Result;

    move-result-object v0

    monitor-exit v1

    .line 112
    :goto_0
    return-object v0

    .line 109
    :cond_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncer;->mProcessingThread:Ljava/lang/Thread;

    .line 110
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 112
    invoke-direct {p0}, Lcom/google/android/wearable/datatransfer/internal/DataSyncer;->download()Lcom/google/android/wearable/datatransfer/internal/DataSyncer$Result;

    move-result-object v0

    goto :goto_0

    .line 110
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 36
    invoke-virtual {p0}, Lcom/google/android/wearable/datatransfer/internal/DataSyncer;->call()Lcom/google/android/wearable/datatransfer/internal/DataSyncer$Result;

    move-result-object v0

    return-object v0
.end method

.method public final cancel()V
    .locals 2

    .prologue
    .line 227
    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncer;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 228
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncer;->mCancelled:Z

    .line 229
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncer;->mProcessingThread:Ljava/lang/Thread;

    if-eqz v0, :cond_0

    .line 230
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncer;->mProcessingThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 232
    :cond_0
    monitor-exit v1

    .line 233
    return-void

    .line 232
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
