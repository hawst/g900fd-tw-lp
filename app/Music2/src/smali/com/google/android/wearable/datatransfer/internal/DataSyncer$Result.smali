.class final Lcom/google/android/wearable/datatransfer/internal/DataSyncer$Result;
.super Ljava/lang/Object;
.source "DataSyncer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/wearable/datatransfer/internal/DataSyncer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "Result"
.end annotation


# instance fields
.field public final appStatusCode:I

.field public final commonStatusCode:I

.field public final interrupted:Z

.field public final permanentFailure:Z

.field public final success:Z


# direct methods
.method private constructor <init>(ZZZII)V
    .locals 0
    .param p1, "success"    # Z
    .param p2, "interrupted"    # Z
    .param p3, "permanentFailure"    # Z
    .param p4, "commonStatusCode"    # I
    .param p5, "appStatusCode"    # I

    .prologue
    .line 365
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 366
    iput-boolean p1, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncer$Result;->success:Z

    .line 367
    iput-boolean p2, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncer$Result;->interrupted:Z

    .line 368
    iput-boolean p3, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncer$Result;->permanentFailure:Z

    .line 369
    iput p4, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncer$Result;->commonStatusCode:I

    .line 370
    iput p5, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncer$Result;->appStatusCode:I

    .line 371
    return-void
.end method

.method static forInterrupted()Lcom/google/android/wearable/datatransfer/internal/DataSyncer$Result;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 361
    new-instance v0, Lcom/google/android/wearable/datatransfer/internal/DataSyncer$Result;

    const/4 v2, 0x1

    move v3, v1

    move v4, v1

    move v5, v1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/wearable/datatransfer/internal/DataSyncer$Result;-><init>(ZZZII)V

    return-object v0
.end method

.method static forPermanentFailure(II)Lcom/google/android/wearable/datatransfer/internal/DataSyncer$Result;
    .locals 6
    .param p0, "commonStatusCode"    # I
    .param p1, "appStatusCode"    # I

    .prologue
    const/4 v1, 0x0

    .line 353
    new-instance v0, Lcom/google/android/wearable/datatransfer/internal/DataSyncer$Result;

    const/4 v3, 0x1

    move v2, v1

    move v4, p0

    move v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/wearable/datatransfer/internal/DataSyncer$Result;-><init>(ZZZII)V

    return-object v0
.end method

.method static forSuccess()Lcom/google/android/wearable/datatransfer/internal/DataSyncer$Result;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 349
    new-instance v0, Lcom/google/android/wearable/datatransfer/internal/DataSyncer$Result;

    const/4 v1, 0x1

    move v3, v2

    move v4, v2

    move v5, v2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/wearable/datatransfer/internal/DataSyncer$Result;-><init>(ZZZII)V

    return-object v0
.end method

.method static forTransientFailure(II)Lcom/google/android/wearable/datatransfer/internal/DataSyncer$Result;
    .locals 6
    .param p0, "commonStatusCode"    # I
    .param p1, "appStatusCode"    # I

    .prologue
    const/4 v1, 0x0

    .line 357
    new-instance v0, Lcom/google/android/wearable/datatransfer/internal/DataSyncer$Result;

    move v2, v1

    move v3, v1

    move v4, p0

    move v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/wearable/datatransfer/internal/DataSyncer$Result;-><init>(ZZZII)V

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 375
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Result{success="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncer$Result;->success:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", interrupted="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncer$Result;->interrupted:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", commonStatusCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncer$Result;->commonStatusCode:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", appStatusCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncer$Result;->appStatusCode:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
