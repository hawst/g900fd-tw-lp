.class final Lcom/google/android/wearable/datatransfer/internal/DataTransferClient$MyDataReceiverCallbacks;
.super Ljava/lang/Object;
.source "DataTransferClient.java"

# interfaces
.implements Lcom/google/android/wearable/datatransfer/internal/DataReceiver$Callbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "MyDataReceiverCallbacks"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;


# direct methods
.method private constructor <init>(Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;)V
    .locals 0

    .prologue
    .line 315
    iput-object p1, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient$MyDataReceiverCallbacks;->this$0:Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;Lcom/google/android/wearable/datatransfer/internal/DataTransferClient$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;
    .param p2, "x1"    # Lcom/google/android/wearable/datatransfer/internal/DataTransferClient$1;

    .prologue
    .line 315
    invoke-direct {p0, p1}, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient$MyDataReceiverCallbacks;-><init>(Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;)V

    return-void
.end method


# virtual methods
.method public onConnectionOpened(J[Landroid/os/ParcelFileDescriptor;Ljava/util/concurrent/atomic/AtomicReference;J)V
    .locals 7
    .param p1, "connectionId"    # J
    .param p3, "pipeFds"    # [Landroid/os/ParcelFileDescriptor;
    .param p5, "size"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J[",
            "Landroid/os/ParcelFileDescriptor;",
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "Ljava/lang/String;",
            ">;J)V"
        }
    .end annotation

    .prologue
    .local p4, "errorMessageRef":Ljava/util/concurrent/atomic/AtomicReference;, "Ljava/util/concurrent/atomic/AtomicReference<Ljava/lang/String;>;"
    const/4 v6, 0x0

    .line 333
    iget-object v2, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient$MyDataReceiverCallbacks;->this$0:Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;

    # getter for: Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;->mLock:Ljava/lang/Object;
    invoke-static {v2}, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;->access$100(Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;)Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3

    .line 334
    :try_start_0
    iget-object v2, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient$MyDataReceiverCallbacks;->this$0:Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;

    # getter for: Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;->mPendingOpenConnectionResults:Landroid/util/LongSparseArray;
    invoke-static {v2}, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;->access$300(Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;)Landroid/util/LongSparseArray;

    move-result-object v2

    # invokes: Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;->getAndRemove(Landroid/util/LongSparseArray;J)Ljava/lang/Object;
    invoke-static {v2, p1, p2}, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;->access$400(Landroid/util/LongSparseArray;J)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;

    .line 335
    .local v1, "pendingResult":Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;, "Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult<Lcom/google/android/wearable/datatransfer/DataTransferApi$OpenConnectionResult;>;"
    iget-object v2, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient$MyDataReceiverCallbacks;->this$0:Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;

    # getter for: Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;->mCancelTokens:Landroid/util/LongSparseArray;
    invoke-static {v2}, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;->access$500(Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;)Landroid/util/LongSparseArray;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient$ReceiverCancelToken;

    .line 336
    .local v0, "cancelToken":Lcom/google/android/wearable/datatransfer/internal/DataTransferClient$ReceiverCancelToken;
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 337
    if-nez v1, :cond_0

    .line 339
    const-string v2, "DataTransferClient"

    const-string v3, "connection opened without a pending result"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 355
    :goto_0
    return-void

    .line 336
    .end local v0    # "cancelToken":Lcom/google/android/wearable/datatransfer/internal/DataTransferClient$ReceiverCancelToken;
    .end local v1    # "pendingResult":Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;, "Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult<Lcom/google/android/wearable/datatransfer/DataTransferApi$OpenConnectionResult;>;"
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    .line 342
    .restart local v0    # "cancelToken":Lcom/google/android/wearable/datatransfer/internal/DataTransferClient$ReceiverCancelToken;
    .restart local v1    # "pendingResult":Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;, "Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult<Lcom/google/android/wearable/datatransfer/DataTransferApi$OpenConnectionResult;>;"
    :cond_0
    if-eqz v0, :cond_1

    .line 343
    invoke-virtual {v0, p3}, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient$ReceiverCancelToken;->onConnectionOpened([Landroid/os/ParcelFileDescriptor;)V

    .line 346
    :cond_1
    new-instance v2, Lcom/google/android/wearable/datatransfer/internal/OpenConnectionResultImpl;

    new-instance v3, Lcom/google/android/wearable/datatransfer/internal/ConnectionImpl;

    aget-object v4, p3, v6

    invoke-static {v4, p4}, Lcom/google/android/wearable/datatransfer/internal/ErrorCheckingInputStream;->forParcelFileDescriptor(Landroid/os/ParcelFileDescriptor;Ljava/util/concurrent/atomic/AtomicReference;)Lcom/google/android/wearable/datatransfer/internal/ErrorCheckingInputStream;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient$MyDataReceiverCallbacks;->this$0:Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;

    # invokes: Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;->getCancelToken(J)Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$ICancelToken;
    invoke-static {v5, p1, p2}, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;->access$600(Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;J)Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$ICancelToken;

    move-result-object v5

    invoke-direct {v3, v4, v5, p5, p6}, Lcom/google/android/wearable/datatransfer/internal/ConnectionImpl;-><init>(Ljava/io/InputStream;Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$ICancelToken;J)V

    invoke-direct {v2, v3, v6, v6, v6}, Lcom/google/android/wearable/datatransfer/internal/OpenConnectionResultImpl;-><init>(Lcom/google/android/wearable/datatransfer/Connection;III)V

    invoke-virtual {v1, v2}, Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;->setResult(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public onDownloadStarted(JLcom/google/android/wearable/datatransfer/internal/DataReceiver;)V
    .locals 3
    .param p1, "connectionId"    # J
    .param p3, "receiver"    # Lcom/google/android/wearable/datatransfer/internal/DataReceiver;

    .prologue
    .line 319
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient$MyDataReceiverCallbacks;->this$0:Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;

    # getter for: Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;->mLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;->access$100(Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 320
    :try_start_0
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient$MyDataReceiverCallbacks;->this$0:Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;

    # getter for: Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;->mReceivers:Landroid/util/LongSparseArray;
    invoke-static {v0}, Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;->access$200(Lcom/google/android/wearable/datatransfer/internal/DataTransferClient;)Landroid/util/LongSparseArray;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 321
    monitor-exit v1

    .line 322
    return-void

    .line 321
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
