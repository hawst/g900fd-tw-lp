.class final Lcom/google/android/wearable/datatransfer/internal/DataReceiver;
.super Ljava/lang/Object;
.source "DataReceiver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/wearable/datatransfer/internal/DataReceiver$Result;,
        Lcom/google/android/wearable/datatransfer/internal/DataReceiver$Callbacks;
    }
.end annotation


# instance fields
.field private final mCallbacks:Lcom/google/android/wearable/datatransfer/internal/DataReceiver$Callbacks;

.field private final mClientConnectionId:J

.field private final mDataHandler:Lcom/google/android/wearable/datatransfer/internal/DataHandler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/wearable/datatransfer/internal/DataHandler",
            "<[B>;"
        }
    .end annotation
.end field

.field private final mErrorMessageRef:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mOffset:J

.field private final mPath:Ljava/lang/String;

.field private mWriteFd:Landroid/os/ParcelFileDescriptor;


# direct methods
.method constructor <init>(Lcom/google/android/gms/common/api/GoogleApiClient;Ljava/lang/String;Ljava/lang/String;JJLcom/google/android/wearable/datatransfer/internal/DataReceiver$Callbacks;)V
    .locals 2
    .param p1, "client"    # Lcom/google/android/gms/common/api/GoogleApiClient;
    .param p2, "targetNodeId"    # Ljava/lang/String;
    .param p3, "path"    # Ljava/lang/String;
    .param p4, "offset"    # J
    .param p6, "connectionId"    # J
    .param p8, "callbacks"    # Lcom/google/android/wearable/datatransfer/internal/DataReceiver$Callbacks;

    .prologue
    .line 121
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 98
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataReceiver;->mErrorMessageRef:Ljava/util/concurrent/atomic/AtomicReference;

    .line 122
    new-instance v0, Lcom/google/android/wearable/datatransfer/internal/DataHandler;

    invoke-direct {v0, p1, p2}, Lcom/google/android/wearable/datatransfer/internal/DataHandler;-><init>(Lcom/google/android/gms/common/api/GoogleApiClient;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataReceiver;->mDataHandler:Lcom/google/android/wearable/datatransfer/internal/DataHandler;

    .line 123
    const-string v0, "path"

    invoke-static {p3, v0}, Lcom/google/android/wearable/datatransfer/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataReceiver;->mPath:Ljava/lang/String;

    .line 124
    iput-wide p4, p0, Lcom/google/android/wearable/datatransfer/internal/DataReceiver;->mOffset:J

    .line 125
    iput-wide p6, p0, Lcom/google/android/wearable/datatransfer/internal/DataReceiver;->mClientConnectionId:J

    .line 126
    iput-object p8, p0, Lcom/google/android/wearable/datatransfer/internal/DataReceiver;->mCallbacks:Lcom/google/android/wearable/datatransfer/internal/DataReceiver$Callbacks;

    .line 127
    return-void
.end method

.method private createNewConnection(J)Z
    .locals 9
    .param p1, "transferSize"    # J

    .prologue
    const/4 v8, 0x1

    .line 360
    :try_start_0
    invoke-static {}, Landroid/os/ParcelFileDescriptor;->createPipe()[Landroid/os/ParcelFileDescriptor;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 365
    .local v4, "descriptors":[Landroid/os/ParcelFileDescriptor;
    aget-object v1, v4, v8

    iput-object v1, p0, Lcom/google/android/wearable/datatransfer/internal/DataReceiver;->mWriteFd:Landroid/os/ParcelFileDescriptor;

    .line 367
    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/internal/DataReceiver;->mCallbacks:Lcom/google/android/wearable/datatransfer/internal/DataReceiver$Callbacks;

    if-eqz v1, :cond_0

    .line 368
    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/internal/DataReceiver;->mCallbacks:Lcom/google/android/wearable/datatransfer/internal/DataReceiver$Callbacks;

    iget-wide v2, p0, Lcom/google/android/wearable/datatransfer/internal/DataReceiver;->mClientConnectionId:J

    iget-object v5, p0, Lcom/google/android/wearable/datatransfer/internal/DataReceiver;->mErrorMessageRef:Ljava/util/concurrent/atomic/AtomicReference;

    move-wide v6, p1

    invoke-interface/range {v1 .. v7}, Lcom/google/android/wearable/datatransfer/internal/DataReceiver$Callbacks;->onConnectionOpened(J[Landroid/os/ParcelFileDescriptor;Ljava/util/concurrent/atomic/AtomicReference;J)V

    :cond_0
    move v1, v8

    .line 371
    .end local v4    # "descriptors":[Landroid/os/ParcelFileDescriptor;
    :goto_0
    return v1

    .line 361
    :catch_0
    move-exception v0

    .line 362
    .local v0, "e":Ljava/io/IOException;
    const-string v1, "DataReceiver"

    const-string v2, "Failed to create a pipe"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 363
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private downloadResource(J)V
    .locals 7
    .param p1, "serverConnectionId"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;,
            Ljava/util/concurrent/TimeoutException;
        }
    .end annotation

    .prologue
    .line 282
    const-string v3, "DataReceiver"

    invoke-static {v3}, Lcom/google/android/wearable/datatransfer/internal/DebugLog;->isLoggable(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 283
    const-string v3, "DataReceiver"

    const-string v4, "downloadResource"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 285
    :cond_0
    new-instance v2, Ljava/io/FileOutputStream;

    iget-object v3, p0, Lcom/google/android/wearable/datatransfer/internal/DataReceiver;->mWriteFd:Landroid/os/ParcelFileDescriptor;

    invoke-virtual {v3}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/FileDescriptor;)V

    .line 287
    .local v2, "out":Ljava/io/OutputStream;
    const/4 v0, 0x0

    .line 289
    .local v0, "gotFin":Z
    :try_start_0
    invoke-static {}, Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;->newPacket()Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;

    move-result-object v1

    .line 290
    .local v1, "latestPacket":Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;
    :cond_1
    :goto_0
    if-nez v0, :cond_5

    .line 291
    const-string v3, "DataReceiver"

    invoke-static {v3}, Lcom/google/android/wearable/datatransfer/internal/DebugLog;->isLoggable(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 292
    const-string v3, "DataReceiver"

    const-string v4, "sendAck"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 294
    :cond_2
    invoke-direct {p0, v1, p1, p2}, Lcom/google/android/wearable/datatransfer/internal/DataReceiver;->sendAckAndWaitForData(Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;J)V

    .line 295
    const-string v3, "DataReceiver"

    invoke-static {v3}, Lcom/google/android/wearable/datatransfer/internal/DebugLog;->isLoggable(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 296
    const-string v3, "DataReceiver"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "received data: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;->getPayloadLength()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 299
    :cond_3
    iget-object v3, v1, Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;->proto:Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;

    iget-wide p1, v3, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;->serverConnectionId:J

    .line 301
    invoke-virtual {v1, v2}, Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;->writePayload(Ljava/io/OutputStream;)V

    .line 302
    iget-object v3, v1, Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;->proto:Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;

    iget-boolean v0, v3, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;->finalPacket:Z

    .line 303
    const-string v3, "DataReceiver"

    invoke-static {v3}, Lcom/google/android/wearable/datatransfer/internal/DebugLog;->isLoggable(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 304
    const-string v3, "DataReceiver"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "gotFin: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 309
    .end local v1    # "latestPacket":Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;
    :catchall_0
    move-exception v3

    invoke-static {v2}, Lcom/google/android/wearable/datatransfer/internal/Utils;->closeQuietly(Ljava/io/Closeable;)V

    .line 310
    if-nez v0, :cond_4

    .line 312
    iget-object v4, p0, Lcom/google/android/wearable/datatransfer/internal/DataReceiver;->mErrorMessageRef:Ljava/util/concurrent/atomic/AtomicReference;

    const-string v5, "lost connection"

    invoke-virtual {v4, v5}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 314
    :cond_4
    iget-object v4, p0, Lcom/google/android/wearable/datatransfer/internal/DataReceiver;->mWriteFd:Landroid/os/ParcelFileDescriptor;

    invoke-static {v4}, Lcom/google/android/wearable/datatransfer/internal/Utils;->closeQuietly(Ljava/io/Closeable;)V

    throw v3

    .line 307
    .restart local v1    # "latestPacket":Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;
    :cond_5
    :try_start_1
    invoke-direct {p0, p1, p2}, Lcom/google/android/wearable/datatransfer/internal/DataReceiver;->sendFinalAck(J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 309
    invoke-static {v2}, Lcom/google/android/wearable/datatransfer/internal/Utils;->closeQuietly(Ljava/io/Closeable;)V

    .line 310
    if-nez v0, :cond_6

    .line 312
    iget-object v3, p0, Lcom/google/android/wearable/datatransfer/internal/DataReceiver;->mErrorMessageRef:Ljava/util/concurrent/atomic/AtomicReference;

    const-string v4, "lost connection"

    invoke-virtual {v3, v4}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 314
    :cond_6
    iget-object v3, p0, Lcom/google/android/wearable/datatransfer/internal/DataReceiver;->mWriteFd:Landroid/os/ParcelFileDescriptor;

    invoke-static {v3}, Lcom/google/android/wearable/datatransfer/internal/Utils;->closeQuietly(Ljava/io/Closeable;)V

    .line 316
    return-void
.end method

.method private openConnection()Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;,
            Ljava/util/concurrent/TimeoutException;
        }
    .end annotation

    .prologue
    .line 256
    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/internal/DataReceiver;->mPath:Ljava/lang/String;

    iget-wide v2, p0, Lcom/google/android/wearable/datatransfer/internal/DataReceiver;->mOffset:J

    iget-wide v4, p0, Lcom/google/android/wearable/datatransfer/internal/DataReceiver;->mClientConnectionId:J

    invoke-static {v1, v2, v3, v4, v5}, Lcom/google/android/wearable/datatransfer/internal/OpenConnectionPacket;->newPacket(Ljava/lang/String;JJ)Lcom/google/android/wearable/datatransfer/internal/OpenConnectionPacket;

    move-result-object v8

    .line 258
    .local v8, "packet":Lcom/google/android/wearable/datatransfer/internal/OpenConnectionPacket;
    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/internal/DataReceiver;->mDataHandler:Lcom/google/android/wearable/datatransfer/internal/DataHandler;

    const-string v2, "com.google.android.wearable.datatransfer.OPEN_CONNECTION"

    invoke-virtual {v8}, Lcom/google/android/wearable/datatransfer/internal/OpenConnectionPacket;->toBytes()[B

    move-result-object v3

    const-wide/16 v4, 0x4e20

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/wearable/datatransfer/internal/DataHandler;->sendMessageAndWaitForPacket(Ljava/lang/String;[BJLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 261
    .local v0, "dataTransferPacketBytes":[B
    invoke-static {}, Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;->newPacket()Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;

    move-result-object v9

    .line 263
    .local v9, "result":Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;
    :try_start_0
    invoke-virtual {v9, v0}, Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;->mutateFromBytes([B)V
    :try_end_0
    .catch Lcom/google/android/wearable/datatransfer/internal/InvalidRequestException; {:try_start_0 .. :try_end_0} :catch_0

    .line 269
    return-object v9

    .line 264
    :catch_0
    move-exception v7

    .line 267
    .local v7, "e":Lcom/google/android/wearable/datatransfer/internal/InvalidRequestException;
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Invalid data transfer packet"

    invoke-direct {v1, v2, v7}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method private sendAckAndWaitForData(Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;J)V
    .locals 10
    .param p1, "packetToMutate"    # Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;
    .param p2, "serverConnectionId"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;,
            Ljava/util/concurrent/TimeoutException;
        }
    .end annotation

    .prologue
    .line 336
    iget-wide v2, p0, Lcom/google/android/wearable/datatransfer/internal/DataReceiver;->mClientConnectionId:J

    const/4 v1, 0x0

    invoke-static {v2, v3, p2, p3, v1}, Lcom/google/android/wearable/datatransfer/internal/DataTransferAckPacket;->newPacket(JJZ)Lcom/google/android/wearable/datatransfer/internal/DataTransferAckPacket;

    move-result-object v0

    .line 339
    .local v0, "ackPacket":Lcom/google/android/wearable/datatransfer/internal/DataTransferAckPacket;
    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/internal/DataReceiver;->mDataHandler:Lcom/google/android/wearable/datatransfer/internal/DataHandler;

    const-string v2, "com.google.android.wearable.datatransfer.DATA_TRANSFER_ACK"

    invoke-virtual {v0}, Lcom/google/android/wearable/datatransfer/internal/DataTransferAckPacket;->toBytes()[B

    move-result-object v3

    const-wide/16 v4, 0x3a98

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/wearable/datatransfer/internal/DataHandler;->sendMessageAndWaitForPacket(Ljava/lang/String;[BJLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [B

    .line 343
    .local v7, "dataTransferPacketBytes":[B
    :try_start_0
    invoke-virtual {p1, v7}, Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;->mutateFromBytes([B)V
    :try_end_0
    .catch Lcom/google/android/wearable/datatransfer/internal/InvalidRequestException; {:try_start_0 .. :try_end_0} :catch_0

    .line 349
    return-void

    .line 344
    :catch_0
    move-exception v8

    .line 347
    .local v8, "e":Lcom/google/android/wearable/datatransfer/internal/InvalidRequestException;
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Invalid data transfer packet"

    invoke-direct {v1, v2, v8}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method private sendFinalAck(J)V
    .locals 5
    .param p1, "serverConnectionId"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 352
    iget-wide v2, p0, Lcom/google/android/wearable/datatransfer/internal/DataReceiver;->mClientConnectionId:J

    const/4 v1, 0x1

    invoke-static {v2, v3, p1, p2, v1}, Lcom/google/android/wearable/datatransfer/internal/DataTransferAckPacket;->newPacket(JJZ)Lcom/google/android/wearable/datatransfer/internal/DataTransferAckPacket;

    move-result-object v0

    .line 354
    .local v0, "ackPacket":Lcom/google/android/wearable/datatransfer/internal/DataTransferAckPacket;
    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/internal/DataReceiver;->mDataHandler:Lcom/google/android/wearable/datatransfer/internal/DataHandler;

    const-string v2, "com.google.android.wearable.datatransfer.DATA_TRANSFER_ACK"

    invoke-virtual {v0}, Lcom/google/android/wearable/datatransfer/internal/DataTransferAckPacket;->toBytes()[B

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/wearable/datatransfer/internal/DataHandler;->sendMessage(Ljava/lang/String;[B)Ljava/util/concurrent/Future;

    .line 355
    return-void
.end method


# virtual methods
.method download()Lcom/google/android/wearable/datatransfer/internal/DataReceiver$Result;
    .locals 10

    .prologue
    .line 140
    :try_start_0
    const-string v3, "DataReceiver"

    invoke-static {v3}, Lcom/google/android/wearable/datatransfer/internal/DebugLog;->isLoggable(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 141
    const-string v3, "DataReceiver"

    const-string v8, "download"

    invoke-static {v3, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    :cond_0
    iget-object v3, p0, Lcom/google/android/wearable/datatransfer/internal/DataReceiver;->mCallbacks:Lcom/google/android/wearable/datatransfer/internal/DataReceiver$Callbacks;

    if-eqz v3, :cond_1

    .line 144
    iget-object v3, p0, Lcom/google/android/wearable/datatransfer/internal/DataReceiver;->mCallbacks:Lcom/google/android/wearable/datatransfer/internal/DataReceiver$Callbacks;

    iget-wide v8, p0, Lcom/google/android/wearable/datatransfer/internal/DataReceiver;->mClientConnectionId:J

    invoke-interface {v3, v8, v9, p0}, Lcom/google/android/wearable/datatransfer/internal/DataReceiver$Callbacks;->onDownloadStarted(JLcom/google/android/wearable/datatransfer/internal/DataReceiver;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 150
    :cond_1
    :try_start_1
    const-string v3, "DataReceiver"

    invoke-static {v3}, Lcom/google/android/wearable/datatransfer/internal/DebugLog;->isLoggable(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 151
    const-string v3, "DataReceiver"

    const-string v8, "openConnection"

    invoke-static {v3, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 153
    :cond_2
    invoke-direct {p0}, Lcom/google/android/wearable/datatransfer/internal/DataReceiver;->openConnection()Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;

    move-result-object v2

    .line 154
    .local v2, "openConnectionResponsePacket":Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;
    const-string v3, "DataReceiver"

    invoke-static {v3}, Lcom/google/android/wearable/datatransfer/internal/DebugLog;->isLoggable(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 155
    const-string v3, "DataReceiver"

    const-string v8, "openConnection response received"

    invoke-static {v3, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 158
    :cond_3
    iget-object v3, v2, Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;->proto:Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;

    iget-wide v4, v3, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;->serverConnectionId:J

    .line 159
    .local v4, "serverConnectionId":J
    const-string v3, "DataReceiver"

    invoke-static {v3}, Lcom/google/android/wearable/datatransfer/internal/DebugLog;->isLoggable(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 160
    const-string v3, "DataReceiver"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "openConnection server connection "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v3, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 162
    :cond_4
    invoke-virtual {v2}, Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;->getPayloadLength()I

    move-result v3

    if-nez v3, :cond_5

    const/4 v3, 0x1

    :goto_0
    const-string v8, "there should no payload on open connection response"

    invoke-static {v3, v8}, Lcom/google/android/wearable/datatransfer/internal/Preconditions;->checkState(ZLjava/lang/Object;)V
    :try_end_1
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 184
    :try_start_2
    iget-object v3, v2, Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;->proto:Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;

    iget-boolean v3, v3, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;->connectionRefused:Z

    if-eqz v3, :cond_8

    .line 185
    iget-object v3, v2, Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;->proto:Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;

    iget v3, v3, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;->connectionRefusedStatusCode:I

    iget-object v8, v2, Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;->proto:Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;

    iget v8, v8, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;->connectionRefusedAppStatusCode:I

    invoke-static {v3, v8}, Lcom/google/android/wearable/datatransfer/internal/DataReceiver$Result;->forConnectionRefused(II)Lcom/google/android/wearable/datatransfer/internal/DataReceiver$Result;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v3

    .line 220
    iget-object v8, p0, Lcom/google/android/wearable/datatransfer/internal/DataReceiver;->mDataHandler:Lcom/google/android/wearable/datatransfer/internal/DataHandler;

    invoke-virtual {v8}, Lcom/google/android/wearable/datatransfer/internal/DataHandler;->clearPending()V

    .end local v2    # "openConnectionResponsePacket":Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;
    .end local v4    # "serverConnectionId":J
    :goto_1
    return-object v3

    .line 162
    .restart local v2    # "openConnectionResponsePacket":Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;
    .restart local v4    # "serverConnectionId":J
    :cond_5
    const/4 v3, 0x0

    goto :goto_0

    .line 164
    .end local v2    # "openConnectionResponsePacket":Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;
    .end local v4    # "serverConnectionId":J
    :catch_0
    move-exception v0

    .line 165
    .local v0, "e":Ljava/util/concurrent/TimeoutException;
    :try_start_3
    const-string v3, "DataReceiver"

    const-string v8, "Timed out while opening the connection"

    invoke-static {v3, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 166
    const-string v3, "DataReceiver"

    invoke-static {v3}, Lcom/google/android/wearable/datatransfer/internal/DebugLog;->isLoggable(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 167
    const-string v3, "DataReceiver"

    const-string v8, "Timeout stack trace"

    invoke-static {v3, v8, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 169
    :cond_6
    invoke-static {}, Lcom/google/android/wearable/datatransfer/internal/DataReceiver$Result;->forTimeout()Lcom/google/android/wearable/datatransfer/internal/DataReceiver$Result;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v3

    .line 220
    iget-object v8, p0, Lcom/google/android/wearable/datatransfer/internal/DataReceiver;->mDataHandler:Lcom/google/android/wearable/datatransfer/internal/DataHandler;

    invoke-virtual {v8}, Lcom/google/android/wearable/datatransfer/internal/DataHandler;->clearPending()V

    goto :goto_1

    .line 170
    .end local v0    # "e":Ljava/util/concurrent/TimeoutException;
    :catch_1
    move-exception v0

    .line 171
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_4
    const-string v3, "DataReceiver"

    invoke-static {v3}, Lcom/google/android/wearable/datatransfer/internal/DebugLog;->isLoggable(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 172
    const-string v3, "DataReceiver"

    const-string v8, "Cancelled while opening the connection"

    invoke-static {v3, v8, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 174
    :cond_7
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->interrupt()V

    .line 178
    invoke-static {}, Lcom/google/android/wearable/datatransfer/internal/DataReceiver$Result;->forError()Lcom/google/android/wearable/datatransfer/internal/DataReceiver$Result;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v3

    .line 220
    iget-object v8, p0, Lcom/google/android/wearable/datatransfer/internal/DataReceiver;->mDataHandler:Lcom/google/android/wearable/datatransfer/internal/DataHandler;

    invoke-virtual {v8}, Lcom/google/android/wearable/datatransfer/internal/DataHandler;->clearPending()V

    goto :goto_1

    .line 179
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catch_2
    move-exception v0

    .line 180
    .local v0, "e":Ljava/io/IOException;
    :try_start_5
    const-string v3, "DataReceiver"

    const-string v8, "Failed to open the connection"

    invoke-static {v3, v8, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 181
    invoke-static {}, Lcom/google/android/wearable/datatransfer/internal/DataReceiver$Result;->forError()Lcom/google/android/wearable/datatransfer/internal/DataReceiver$Result;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result-object v3

    .line 220
    iget-object v8, p0, Lcom/google/android/wearable/datatransfer/internal/DataReceiver;->mDataHandler:Lcom/google/android/wearable/datatransfer/internal/DataHandler;

    invoke-virtual {v8}, Lcom/google/android/wearable/datatransfer/internal/DataHandler;->clearPending()V

    goto :goto_1

    .line 190
    .end local v0    # "e":Ljava/io/IOException;
    .restart local v2    # "openConnectionResponsePacket":Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;
    .restart local v4    # "serverConnectionId":J
    :cond_8
    :try_start_6
    iget-object v3, v2, Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;->proto:Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;

    iget-object v1, v3, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket;->firstPacketParameters:Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket$FirstPacketParameters;

    .line 192
    .local v1, "firstPacketParameters":Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket$FirstPacketParameters;
    if-eqz v1, :cond_9

    iget-wide v6, v1, Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket$FirstPacketParameters;->transferSize:J

    .line 195
    .local v6, "transferSize":J
    :goto_2
    invoke-direct {p0, v6, v7}, Lcom/google/android/wearable/datatransfer/internal/DataReceiver;->createNewConnection(J)Z

    move-result v3

    if-nez v3, :cond_a

    .line 196
    invoke-static {}, Lcom/google/android/wearable/datatransfer/internal/DataReceiver$Result;->forError()Lcom/google/android/wearable/datatransfer/internal/DataReceiver$Result;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-result-object v3

    .line 220
    iget-object v8, p0, Lcom/google/android/wearable/datatransfer/internal/DataReceiver;->mDataHandler:Lcom/google/android/wearable/datatransfer/internal/DataHandler;

    invoke-virtual {v8}, Lcom/google/android/wearable/datatransfer/internal/DataHandler;->clearPending()V

    goto :goto_1

    .line 192
    .end local v6    # "transferSize":J
    :cond_9
    const-wide/16 v6, -0x1

    goto :goto_2

    .line 201
    .restart local v6    # "transferSize":J
    :cond_a
    :try_start_7
    invoke-direct {p0, v4, v5}, Lcom/google/android/wearable/datatransfer/internal/DataReceiver;->downloadResource(J)V
    :try_end_7
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_7 .. :try_end_7} :catch_3
    .catch Ljava/lang/InterruptedException; {:try_start_7 .. :try_end_7} :catch_4
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 218
    :try_start_8
    invoke-static {}, Lcom/google/android/wearable/datatransfer/internal/DataReceiver$Result;->forSuccess()Lcom/google/android/wearable/datatransfer/internal/DataReceiver$Result;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    move-result-object v3

    .line 220
    iget-object v8, p0, Lcom/google/android/wearable/datatransfer/internal/DataReceiver;->mDataHandler:Lcom/google/android/wearable/datatransfer/internal/DataHandler;

    invoke-virtual {v8}, Lcom/google/android/wearable/datatransfer/internal/DataHandler;->clearPending()V

    goto :goto_1

    .line 202
    :catch_3
    move-exception v0

    .line 203
    .local v0, "e":Ljava/util/concurrent/TimeoutException;
    :try_start_9
    const-string v3, "DataReceiver"

    const-string v8, "Timed out while downloading the resource"

    invoke-static {v3, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 204
    const-string v3, "DataReceiver"

    invoke-static {v3}, Lcom/google/android/wearable/datatransfer/internal/DebugLog;->isLoggable(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 205
    const-string v3, "DataReceiver"

    const-string v8, "Timeout stack trace"

    invoke-static {v3, v8, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 207
    :cond_b
    invoke-static {}, Lcom/google/android/wearable/datatransfer/internal/DataReceiver$Result;->forError()Lcom/google/android/wearable/datatransfer/internal/DataReceiver$Result;
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    move-result-object v3

    .line 220
    iget-object v8, p0, Lcom/google/android/wearable/datatransfer/internal/DataReceiver;->mDataHandler:Lcom/google/android/wearable/datatransfer/internal/DataHandler;

    invoke-virtual {v8}, Lcom/google/android/wearable/datatransfer/internal/DataHandler;->clearPending()V

    goto/16 :goto_1

    .line 208
    .end local v0    # "e":Ljava/util/concurrent/TimeoutException;
    :catch_4
    move-exception v0

    .line 209
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_a
    const-string v3, "DataReceiver"

    invoke-static {v3}, Lcom/google/android/wearable/datatransfer/internal/DebugLog;->isLoggable(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 210
    const-string v3, "DataReceiver"

    const-string v8, "Cancelled while downloading the resource"

    invoke-static {v3, v8, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 212
    :cond_c
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->interrupt()V

    .line 213
    invoke-static {}, Lcom/google/android/wearable/datatransfer/internal/DataReceiver$Result;->forError()Lcom/google/android/wearable/datatransfer/internal/DataReceiver$Result;
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    move-result-object v3

    .line 220
    iget-object v8, p0, Lcom/google/android/wearable/datatransfer/internal/DataReceiver;->mDataHandler:Lcom/google/android/wearable/datatransfer/internal/DataHandler;

    invoke-virtual {v8}, Lcom/google/android/wearable/datatransfer/internal/DataHandler;->clearPending()V

    goto/16 :goto_1

    .line 214
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catch_5
    move-exception v0

    .line 215
    .local v0, "e":Ljava/io/IOException;
    :try_start_b
    const-string v3, "DataReceiver"

    const-string v8, "Failed to download the resource"

    invoke-static {v3, v8, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 216
    invoke-static {}, Lcom/google/android/wearable/datatransfer/internal/DataReceiver$Result;->forError()Lcom/google/android/wearable/datatransfer/internal/DataReceiver$Result;
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    move-result-object v3

    .line 220
    iget-object v8, p0, Lcom/google/android/wearable/datatransfer/internal/DataReceiver;->mDataHandler:Lcom/google/android/wearable/datatransfer/internal/DataHandler;

    invoke-virtual {v8}, Lcom/google/android/wearable/datatransfer/internal/DataHandler;->clearPending()V

    goto/16 :goto_1

    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "firstPacketParameters":Lcom/google/android/wearable/datatransfer/proto/PacketProtos$DataTransferPacket$FirstPacketParameters;
    .end local v2    # "openConnectionResponsePacket":Lcom/google/android/wearable/datatransfer/internal/DataTransferPacket;
    .end local v4    # "serverConnectionId":J
    .end local v6    # "transferSize":J
    :catchall_0
    move-exception v3

    iget-object v8, p0, Lcom/google/android/wearable/datatransfer/internal/DataReceiver;->mDataHandler:Lcom/google/android/wearable/datatransfer/internal/DataHandler;

    invoke-virtual {v8}, Lcom/google/android/wearable/datatransfer/internal/DataHandler;->clearPending()V

    throw v3
.end method

.method getRemoteNodeId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 228
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataReceiver;->mDataHandler:Lcom/google/android/wearable/datatransfer/internal/DataHandler;

    invoke-virtual {v0}, Lcom/google/android/wearable/datatransfer/internal/DataHandler;->getRemoteNodeId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method onPacketReceived([B)V
    .locals 3
    .param p1, "packetData"    # [B

    .prologue
    .line 238
    :try_start_0
    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/internal/DataReceiver;->mDataHandler:Lcom/google/android/wearable/datatransfer/internal/DataHandler;

    invoke-virtual {v1, p1}, Lcom/google/android/wearable/datatransfer/internal/DataHandler;->onPacketReceived(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 242
    :goto_0
    return-void

    .line 239
    :catch_0
    move-exception v0

    .line 240
    .local v0, "e":Ljava/io/IOException;
    const-string v1, "DataReceiver"

    const-string v2, "error delivering packet"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
