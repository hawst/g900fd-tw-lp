.class public final Lcom/google/android/wearable/datatransfer/internal/ErrorCheckingInputStream;
.super Ljava/io/InputStream;
.source "ErrorCheckingInputStream.java"


# instance fields
.field private final mDelegate:Ljava/io/FileInputStream;

.field private final mErrorMessageRef:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Landroid/os/ParcelFileDescriptor;Ljava/util/concurrent/atomic/AtomicReference;)V
    .locals 2
    .param p1, "fileDescriptor"    # Landroid/os/ParcelFileDescriptor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/ParcelFileDescriptor;",
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 34
    .local p2, "errorMessageRef":Ljava/util/concurrent/atomic/AtomicReference;, "Ljava/util/concurrent/atomic/AtomicReference<Ljava/lang/String;>;"
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 35
    const-string v0, "fileDescriptor"

    invoke-static {p1, v0}, Lcom/google/android/wearable/datatransfer/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 36
    new-instance v0, Ljava/io/FileInputStream;

    invoke-virtual {p1}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/FileDescriptor;)V

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/ErrorCheckingInputStream;->mDelegate:Ljava/io/FileInputStream;

    .line 37
    const-string v0, "errorMessageRef"

    invoke-static {p2, v0}, Lcom/google/android/wearable/datatransfer/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/atomic/AtomicReference;

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/ErrorCheckingInputStream;->mErrorMessageRef:Ljava/util/concurrent/atomic/AtomicReference;

    .line 38
    return-void
.end method

.method private checkError()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 121
    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/internal/ErrorCheckingInputStream;->mErrorMessageRef:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 122
    .local v0, "errorMessage":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 123
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Stream closed with error: \'%s\'"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 126
    :cond_0
    return-void
.end method

.method public static forParcelFileDescriptor(Landroid/os/ParcelFileDescriptor;Ljava/util/concurrent/atomic/AtomicReference;)Lcom/google/android/wearable/datatransfer/internal/ErrorCheckingInputStream;
    .locals 1
    .param p0, "fileDescriptor"    # Landroid/os/ParcelFileDescriptor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/ParcelFileDescriptor;",
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/android/wearable/datatransfer/internal/ErrorCheckingInputStream;"
        }
    .end annotation

    .prologue
    .line 29
    .local p1, "errorMessageRef":Ljava/util/concurrent/atomic/AtomicReference;, "Ljava/util/concurrent/atomic/AtomicReference<Ljava/lang/String;>;"
    new-instance v0, Lcom/google/android/wearable/datatransfer/internal/ErrorCheckingInputStream;

    invoke-direct {v0, p0, p1}, Lcom/google/android/wearable/datatransfer/internal/ErrorCheckingInputStream;-><init>(Landroid/os/ParcelFileDescriptor;Ljava/util/concurrent/atomic/AtomicReference;)V

    return-object v0
.end method


# virtual methods
.method public available()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/ErrorCheckingInputStream;->mDelegate:Ljava/io/FileInputStream;

    invoke-virtual {v0}, Ljava/io/FileInputStream;->available()I

    move-result v0

    return v0
.end method

.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/ErrorCheckingInputStream;->mDelegate:Ljava/io/FileInputStream;

    invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V

    .line 50
    return-void
.end method

.method public mark(I)V
    .locals 1
    .param p1, "readlimit"    # I

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/ErrorCheckingInputStream;->mDelegate:Ljava/io/FileInputStream;

    invoke-virtual {v0, p1}, Ljava/io/FileInputStream;->mark(I)V

    .line 56
    return-void
.end method

.method public markSupported()Z
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/ErrorCheckingInputStream;->mDelegate:Ljava/io/FileInputStream;

    invoke-virtual {v0}, Ljava/io/FileInputStream;->markSupported()Z

    move-result v0

    return v0
.end method

.method public read()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 71
    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/internal/ErrorCheckingInputStream;->mDelegate:Ljava/io/FileInputStream;

    invoke-virtual {v1}, Ljava/io/FileInputStream;->read()I

    move-result v0

    .line 72
    .local v0, "readResult":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 73
    invoke-direct {p0}, Lcom/google/android/wearable/datatransfer/internal/ErrorCheckingInputStream;->checkError()V

    .line 75
    :cond_0
    return v0
.end method

.method public read([B)I
    .locals 2
    .param p1, "buffer"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 86
    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/internal/ErrorCheckingInputStream;->mDelegate:Ljava/io/FileInputStream;

    invoke-virtual {v1, p1}, Ljava/io/FileInputStream;->read([B)I

    move-result v0

    .line 87
    .local v0, "readResult":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 88
    invoke-direct {p0}, Lcom/google/android/wearable/datatransfer/internal/ErrorCheckingInputStream;->checkError()V

    .line 90
    :cond_0
    return v0
.end method

.method public read([BII)I
    .locals 2
    .param p1, "buffer"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 101
    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/internal/ErrorCheckingInputStream;->mDelegate:Ljava/io/FileInputStream;

    invoke-virtual {v1, p1, p2, p3}, Ljava/io/FileInputStream;->read([BII)I

    move-result v0

    .line 102
    .local v0, "readResult":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 103
    invoke-direct {p0}, Lcom/google/android/wearable/datatransfer/internal/ErrorCheckingInputStream;->checkError()V

    .line 105
    :cond_0
    return v0
.end method

.method public reset()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/ErrorCheckingInputStream;->mDelegate:Ljava/io/FileInputStream;

    invoke-virtual {v0}, Ljava/io/FileInputStream;->reset()V

    .line 112
    return-void
.end method

.method public skip(J)J
    .locals 3
    .param p1, "byteCount"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 117
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/ErrorCheckingInputStream;->mDelegate:Ljava/io/FileInputStream;

    invoke-virtual {v0, p1, p2}, Ljava/io/FileInputStream;->skip(J)J

    move-result-wide v0

    return-wide v0
.end method
