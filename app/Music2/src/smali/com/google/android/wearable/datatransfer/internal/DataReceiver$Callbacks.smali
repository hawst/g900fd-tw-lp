.class interface abstract Lcom/google/android/wearable/datatransfer/internal/DataReceiver$Callbacks;
.super Ljava/lang/Object;
.source "DataReceiver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/wearable/datatransfer/internal/DataReceiver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x608
    name = "Callbacks"
.end annotation


# virtual methods
.method public abstract onConnectionOpened(J[Landroid/os/ParcelFileDescriptor;Ljava/util/concurrent/atomic/AtomicReference;J)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J[",
            "Landroid/os/ParcelFileDescriptor;",
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "Ljava/lang/String;",
            ">;J)V"
        }
    .end annotation
.end method

.method public abstract onDownloadStarted(JLcom/google/android/wearable/datatransfer/internal/DataReceiver;)V
.end method
