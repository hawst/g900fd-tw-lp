.class public final Lcom/google/android/wearable/datatransfer/internal/DebugLog;
.super Ljava/lang/Object;
.source "DebugLog.java"


# direct methods
.method public static isLoggable(Ljava/lang/String;)Z
    .locals 4
    .param p0, "tag"    # Ljava/lang/String;

    .prologue
    const/16 v3, 0x17

    const/4 v2, 0x3

    const/4 v0, 0x0

    .line 48
    const-string v1, "WearableDataTransfer"

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-gt v1, v3, :cond_2

    .end local p0    # "tag":Ljava/lang/String;
    :goto_0
    invoke-static {p0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0

    .restart local p0    # "tag":Ljava/lang/String;
    :cond_2
    invoke-virtual {p0, v0, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method
