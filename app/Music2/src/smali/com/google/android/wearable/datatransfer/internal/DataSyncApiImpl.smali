.class public final Lcom/google/android/wearable/datatransfer/internal/DataSyncApiImpl;
.super Ljava/lang/Object;
.source "DataSyncApiImpl.java"

# interfaces
.implements Lcom/google/android/wearable/datatransfer/DataSyncApi;


# instance fields
.field private final mRequestProcessorExecutor:Ljava/util/concurrent/ExecutorService;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    new-instance v0, Lcom/google/android/wearable/datatransfer/internal/CustomThreadFactory;

    const-string v1, "RequestProcessor"

    invoke-direct {v0, v1}, Lcom/google/android/wearable/datatransfer/internal/CustomThreadFactory;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor(Ljava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/DataSyncApiImpl;->mRequestProcessorExecutor:Ljava/util/concurrent/ExecutorService;

    .line 538
    return-void
.end method
