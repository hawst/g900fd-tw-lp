.class final Lcom/google/android/wearable/datatransfer/internal/DataSyncDatabase$QueryGetNextRetryTimestamp;
.super Ljava/lang/Object;
.source "DataSyncDatabase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/wearable/datatransfer/internal/DataSyncDatabase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "QueryGetNextRetryTimestamp"
.end annotation


# static fields
.field private static final PROJECTION:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 533
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "retryNextRequestTimestamp"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/wearable/datatransfer/internal/DataSyncDatabase$QueryGetNextRetryTimestamp;->PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method private static makeSelectionArgs(JLjava/util/Collection;)[Ljava/lang/String;
    .locals 6
    .param p0, "currentTimeMillis"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)[",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 589
    .local p2, "connectedNodes":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    invoke-interface {p2}, Ljava/util/Collection;->size()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    new-array v3, v4, [Ljava/lang/String;

    .line 590
    .local v3, "result":[Ljava/lang/String;
    const/4 v4, 0x0

    invoke-static {p0, p1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 591
    const/4 v0, 0x1

    .line 592
    .local v0, "i":I
    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 593
    .local v2, "nodeId":Ljava/lang/String;
    aput-object v2, v3, v0

    .line 594
    add-int/lit8 v0, v0, 0x1

    .line 595
    goto :goto_0

    .line 596
    .end local v2    # "nodeId":Ljava/lang/String;
    :cond_0
    return-object v3
.end method

.method private static makeSelectionString(I)Ljava/lang/String;
    .locals 4
    .param p0, "numberOfConnectedNodes"    # I

    .prologue
    .line 570
    if-lez p0, :cond_0

    const/4 v2, 0x1

    :goto_0
    const-string v3, "non-positive numberOfConnectedNodes"

    invoke-static {v2, v3}, Lcom/google/android/wearable/datatransfer/internal/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    .line 571
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "retryNextRequestTimestamp"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " > ? AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "remoteNodeId"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " IN("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 576
    .local v0, "builder":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    add-int/lit8 v2, p0, -0x1

    if-ge v1, v2, :cond_1

    .line 577
    const-string v2, "?,"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 576
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 570
    .end local v0    # "builder":Ljava/lang/StringBuilder;
    .end local v1    # "i":I
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 579
    .restart local v0    # "builder":Ljava/lang/StringBuilder;
    .restart local v1    # "i":I
    :cond_1
    const-string v2, "?)"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 580
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method static query(Landroid/database/sqlite/SQLiteDatabase;JLjava/util/Collection;)Landroid/database/Cursor;
    .locals 9
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "currentTimeMillis"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "J",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Landroid/database/Cursor;"
        }
    .end annotation

    .prologue
    .local p3, "connectedNodes":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    const/4 v5, 0x0

    .line 553
    invoke-interface {p3}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 556
    :goto_0
    return-object v5

    :cond_0
    const-string v1, "Requests"

    sget-object v2, Lcom/google/android/wearable/datatransfer/internal/DataSyncDatabase$QueryGetNextRetryTimestamp;->PROJECTION:[Ljava/lang/String;

    invoke-interface {p3}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/wearable/datatransfer/internal/DataSyncDatabase$QueryGetNextRetryTimestamp;->makeSelectionString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, p2, p3}, Lcom/google/android/wearable/datatransfer/internal/DataSyncDatabase$QueryGetNextRetryTimestamp;->makeSelectionArgs(JLjava/util/Collection;)[Ljava/lang/String;

    move-result-object v4

    const-string v7, "retryNextRequestTimestamp ASC"

    const-string v8, "1"

    move-object v0, p0

    move-object v6, v5

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    goto :goto_0
.end method
