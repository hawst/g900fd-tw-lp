.class public final Lcom/google/android/wearable/datatransfer/WearableDataListenerDelegate;
.super Ljava/lang/Object;
.source "WearableDataListenerDelegate.java"

# interfaces
.implements Lcom/google/android/gms/common/api/Releasable;


# instance fields
.field private final mClient:Lcom/google/android/wearable/datatransfer/WearableDataApiClient;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 86
    new-instance v0, Lcom/google/android/wearable/datatransfer/WearableDataApiClient;

    invoke-direct {v0, p1}, Lcom/google/android/wearable/datatransfer/WearableDataApiClient;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/WearableDataListenerDelegate;->mClient:Lcom/google/android/wearable/datatransfer/WearableDataApiClient;

    .line 87
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/WearableDataListenerDelegate;->mClient:Lcom/google/android/wearable/datatransfer/WearableDataApiClient;

    invoke-virtual {v0}, Lcom/google/android/wearable/datatransfer/WearableDataApiClient;->connect()V

    .line 88
    return-void
.end method

.method private static appendDataEventBufferString(Ljava/lang/StringBuilder;Lcom/google/android/gms/wearable/DataEventBuffer;)V
    .locals 3
    .param p0, "messageBuilder"    # Ljava/lang/StringBuilder;
    .param p1, "dataEvents"    # Lcom/google/android/gms/wearable/DataEventBuffer;

    .prologue
    .line 142
    invoke-virtual {p1}, Lcom/google/android/gms/wearable/DataEventBuffer;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wearable/DataEvent;

    .line 144
    .local v0, "event":Lcom/google/android/gms/wearable/DataEvent;
    invoke-static {p0, v0}, Lcom/google/android/wearable/datatransfer/WearableDataListenerDelegate;->appendDataEventString(Ljava/lang/StringBuilder;Lcom/google/android/gms/wearable/DataEvent;)V

    .line 145
    const-string v2, ", "

    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 147
    .end local v0    # "event":Lcom/google/android/gms/wearable/DataEvent;
    :cond_0
    return-void
.end method

.method private static appendDataEventString(Ljava/lang/StringBuilder;Lcom/google/android/gms/wearable/DataEvent;)V
    .locals 2
    .param p0, "builder"    # Ljava/lang/StringBuilder;
    .param p1, "event"    # Lcom/google/android/gms/wearable/DataEvent;

    .prologue
    .line 150
    const-string v0, "DataEvent["

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "URI: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/gms/wearable/DataEvent;->getDataItem()Lcom/google/android/gms/wearable/DataItem;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/wearable/DataItem;->getUri()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", type: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/gms/wearable/DataEvent;->getType()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 156
    return-void
.end method


# virtual methods
.method public onDataChanged(Lcom/google/android/gms/wearable/DataEventBuffer;)V
    .locals 3
    .param p1, "dataEvents"    # Lcom/google/android/gms/wearable/DataEventBuffer;

    .prologue
    .line 99
    const-string v1, "WDListenerDelegate"

    invoke-static {v1}, Lcom/google/android/wearable/datatransfer/internal/DebugLog;->isLoggable(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 100
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 101
    .local v0, "messageBuilder":Ljava/lang/StringBuilder;
    const-string v1, "onDataChanged("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 103
    invoke-static {v0, p1}, Lcom/google/android/wearable/datatransfer/WearableDataListenerDelegate;->appendDataEventBufferString(Ljava/lang/StringBuilder;Lcom/google/android/gms/wearable/DataEventBuffer;)V

    .line 104
    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 105
    const-string v1, "WDListenerDelegate"

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    .end local v0    # "messageBuilder":Ljava/lang/StringBuilder;
    :cond_0
    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/WearableDataListenerDelegate;->mClient:Lcom/google/android/wearable/datatransfer/WearableDataApiClient;

    invoke-virtual {v1, p1}, Lcom/google/android/wearable/datatransfer/WearableDataApiClient;->onDataChanged(Lcom/google/android/gms/wearable/DataEventBuffer;)V

    .line 108
    return-void
.end method

.method public onMessageReceived(Lcom/google/android/gms/wearable/MessageEvent;)V
    .locals 3
    .param p1, "messageEvent"    # Lcom/google/android/gms/wearable/MessageEvent;

    .prologue
    .line 114
    const-string v0, "WDListenerDelegate"

    invoke-static {v0}, Lcom/google/android/wearable/datatransfer/internal/DebugLog;->isLoggable(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 115
    const-string v0, "WDListenerDelegate"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onMessageReceived("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 117
    :cond_0
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/WearableDataListenerDelegate;->mClient:Lcom/google/android/wearable/datatransfer/WearableDataApiClient;

    invoke-virtual {v0, p1}, Lcom/google/android/wearable/datatransfer/WearableDataApiClient;->onMessageReceived(Lcom/google/android/gms/wearable/MessageEvent;)Z

    .line 118
    return-void
.end method

.method public onPeerConnected(Lcom/google/android/gms/wearable/Node;)V
    .locals 3
    .param p1, "peer"    # Lcom/google/android/gms/wearable/Node;

    .prologue
    .line 124
    const-string v0, "WDListenerDelegate"

    invoke-static {v0}, Lcom/google/android/wearable/datatransfer/internal/DebugLog;->isLoggable(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 125
    const-string v0, "WDListenerDelegate"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onPeerConnected("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 127
    :cond_0
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/WearableDataListenerDelegate;->mClient:Lcom/google/android/wearable/datatransfer/WearableDataApiClient;

    invoke-virtual {v0}, Lcom/google/android/wearable/datatransfer/WearableDataApiClient;->startDataSyncServiceIfPresent()V

    .line 128
    return-void
.end method

.method public onPeerDisconnected(Lcom/google/android/gms/wearable/Node;)V
    .locals 3
    .param p1, "peer"    # Lcom/google/android/gms/wearable/Node;

    .prologue
    .line 134
    const-string v0, "WDListenerDelegate"

    invoke-static {v0}, Lcom/google/android/wearable/datatransfer/internal/DebugLog;->isLoggable(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 135
    const-string v0, "WDListenerDelegate"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onPeerDisconnected("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 138
    :cond_0
    return-void
.end method

.method public release()V
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/WearableDataListenerDelegate;->mClient:Lcom/google/android/wearable/datatransfer/WearableDataApiClient;

    invoke-virtual {v0}, Lcom/google/android/wearable/datatransfer/WearableDataApiClient;->disconnect()V

    .line 93
    return-void
.end method
