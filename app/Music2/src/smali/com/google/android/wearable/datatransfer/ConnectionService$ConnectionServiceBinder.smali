.class final Lcom/google/android/wearable/datatransfer/ConnectionService$ConnectionServiceBinder;
.super Landroid/os/Binder;
.source "ConnectionService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/wearable/datatransfer/ConnectionService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "ConnectionServiceBinder"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/wearable/datatransfer/ConnectionService;


# direct methods
.method constructor <init>(Lcom/google/android/wearable/datatransfer/ConnectionService;)V
    .locals 0

    .prologue
    .line 291
    iput-object p1, p0, Lcom/google/android/wearable/datatransfer/ConnectionService$ConnectionServiceBinder;->this$0:Lcom/google/android/wearable/datatransfer/ConnectionService;

    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    return-void
.end method


# virtual methods
.method onMessageReceived(Lcom/google/android/gms/wearable/MessageEvent;)Z
    .locals 1
    .param p1, "messageEvent"    # Lcom/google/android/gms/wearable/MessageEvent;

    .prologue
    .line 300
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/ConnectionService$ConnectionServiceBinder;->this$0:Lcom/google/android/wearable/datatransfer/ConnectionService;

    invoke-virtual {v0, p1}, Lcom/google/android/wearable/datatransfer/ConnectionService;->onMessageReceived(Lcom/google/android/gms/wearable/MessageEvent;)Z

    move-result v0

    return v0
.end method

.method openConnection(Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 6
    .param p2, "nodeId"    # Ljava/lang/String;
    .param p3, "path"    # Ljava/lang/String;
    .param p4, "offset"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult",
            "<",
            "Lcom/google/android/wearable/datatransfer/DataTransferApi$OpenConnectionResult;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "J)V"
        }
    .end annotation

    .prologue
    .line 318
    .local p1, "pendingResult":Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;, "Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult<Lcom/google/android/wearable/datatransfer/DataTransferApi$OpenConnectionResult;>;"
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/ConnectionService$ConnectionServiceBinder;->this$0:Lcom/google/android/wearable/datatransfer/ConnectionService;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-wide v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/wearable/datatransfer/ConnectionService;->openConnection(Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;Ljava/lang/String;Ljava/lang/String;J)V

    .line 319
    return-void
.end method

.method public stopAllServerConnections(Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 322
    .local p1, "pendingResult":Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;, "Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult<Ljava/lang/Integer;>;"
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/ConnectionService$ConnectionServiceBinder;->this$0:Lcom/google/android/wearable/datatransfer/ConnectionService;

    invoke-virtual {v0, p1}, Lcom/google/android/wearable/datatransfer/ConnectionService;->stopAllServerConnections(Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;)V

    .line 323
    return-void
.end method
