.class public final Lcom/google/android/wearable/datatransfer/internal/OpenConnectionResultImpl;
.super Ljava/lang/Object;
.source "OpenConnectionResultImpl.java"

# interfaces
.implements Lcom/google/android/wearable/datatransfer/DataTransferApi$OpenConnectionResult;


# instance fields
.field private final mAppStatusCode:I

.field private final mCommonStatusCode:I

.field private final mConnection:Lcom/google/android/wearable/datatransfer/Connection;

.field private final mStatusCode:I


# direct methods
.method public constructor <init>(Lcom/google/android/wearable/datatransfer/Connection;III)V
    .locals 0
    .param p1, "connection"    # Lcom/google/android/wearable/datatransfer/Connection;
    .param p2, "commonStatusCode"    # I
    .param p3, "statusCode"    # I
    .param p4, "appStatusCode"    # I

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/google/android/wearable/datatransfer/internal/OpenConnectionResultImpl;->mConnection:Lcom/google/android/wearable/datatransfer/Connection;

    .line 26
    iput p2, p0, Lcom/google/android/wearable/datatransfer/internal/OpenConnectionResultImpl;->mCommonStatusCode:I

    .line 27
    iput p3, p0, Lcom/google/android/wearable/datatransfer/internal/OpenConnectionResultImpl;->mStatusCode:I

    .line 28
    iput p4, p0, Lcom/google/android/wearable/datatransfer/internal/OpenConnectionResultImpl;->mAppStatusCode:I

    .line 29
    return-void
.end method


# virtual methods
.method public getAppStatusCode()I
    .locals 1

    .prologue
    .line 43
    iget v0, p0, Lcom/google/android/wearable/datatransfer/internal/OpenConnectionResultImpl;->mAppStatusCode:I

    return v0
.end method

.method public getCommonStatusCode()I
    .locals 1

    .prologue
    .line 33
    iget v0, p0, Lcom/google/android/wearable/datatransfer/internal/OpenConnectionResultImpl;->mCommonStatusCode:I

    return v0
.end method

.method public getConnection()Lcom/google/android/wearable/datatransfer/Connection;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/OpenConnectionResultImpl;->mConnection:Lcom/google/android/wearable/datatransfer/Connection;

    return-object v0
.end method

.method public release()V
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/OpenConnectionResultImpl;->mConnection:Lcom/google/android/wearable/datatransfer/Connection;

    if-eqz v0, :cond_0

    .line 65
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/OpenConnectionResultImpl;->mConnection:Lcom/google/android/wearable/datatransfer/Connection;

    invoke-static {v0}, Lcom/google/android/wearable/datatransfer/internal/Utils;->closeQuietly(Ljava/io/Closeable;)V

    .line 67
    :cond_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 54
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "OpenConnectionResultImpl{commonStatusCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/wearable/datatransfer/internal/OpenConnectionResultImpl;->mCommonStatusCode:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", statusCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/wearable/datatransfer/internal/OpenConnectionResultImpl;->mStatusCode:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", appStatusCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/wearable/datatransfer/internal/OpenConnectionResultImpl;->mAppStatusCode:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", connection="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/internal/OpenConnectionResultImpl;->mConnection:Lcom/google/android/wearable/datatransfer/Connection;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
