.class interface abstract Lcom/google/android/wearable/datatransfer/internal/PeerProvider;
.super Ljava/lang/Object;
.source "PeerProvider.java"


# virtual methods
.method public abstract getConnectedPeers(Lcom/google/android/gms/common/api/GoogleApiClient;)Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/common/api/GoogleApiClient;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/util/concurrent/TimeoutException;
        }
    .end annotation
.end method

.method public abstract isPeerConnected(Lcom/google/android/gms/common/api/GoogleApiClient;Ljava/lang/String;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/util/concurrent/TimeoutException;
        }
    .end annotation
.end method
