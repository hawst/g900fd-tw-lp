.class public interface abstract Lcom/google/android/wearable/datatransfer/DataTransferApi;
.super Ljava/lang/Object;
.source "DataTransferApi.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/wearable/datatransfer/DataTransferApi$OpenConnectionResult;
    }
.end annotation


# virtual methods
.method public abstract openConnection(Lcom/google/android/wearable/datatransfer/WearableDataApiClient;Ljava/lang/String;Ljava/lang/String;J)Lcom/google/android/wearable/datatransfer/WearableDataCompat$PendingResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/wearable/datatransfer/WearableDataApiClient;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "J)",
            "Lcom/google/android/wearable/datatransfer/WearableDataCompat$PendingResult",
            "<",
            "Lcom/google/android/wearable/datatransfer/DataTransferApi$OpenConnectionResult;",
            ">;"
        }
    .end annotation
.end method
