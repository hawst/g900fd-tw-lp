.class final Lcom/google/android/wearable/datatransfer/WearableDataApiClient$OpenConnectionRequest;
.super Ljava/lang/Object;
.source "WearableDataApiClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/wearable/datatransfer/WearableDataApiClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "OpenConnectionRequest"
.end annotation


# instance fields
.field final nodeId:Ljava/lang/String;

.field final offset:J

.field final path:Ljava/lang/String;

.field final pendingResult:Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult",
            "<",
            "Lcom/google/android/wearable/datatransfer/DataTransferApi$OpenConnectionResult;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 0
    .param p2, "nodeId"    # Ljava/lang/String;
    .param p3, "path"    # Ljava/lang/String;
    .param p4, "offset"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult",
            "<",
            "Lcom/google/android/wearable/datatransfer/DataTransferApi$OpenConnectionResult;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "J)V"
        }
    .end annotation

    .prologue
    .line 344
    .local p1, "pendingResult":Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;, "Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult<Lcom/google/android/wearable/datatransfer/DataTransferApi$OpenConnectionResult;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 345
    iput-object p1, p0, Lcom/google/android/wearable/datatransfer/WearableDataApiClient$OpenConnectionRequest;->pendingResult:Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;

    .line 346
    iput-object p2, p0, Lcom/google/android/wearable/datatransfer/WearableDataApiClient$OpenConnectionRequest;->nodeId:Ljava/lang/String;

    .line 347
    iput-object p3, p0, Lcom/google/android/wearable/datatransfer/WearableDataApiClient$OpenConnectionRequest;->path:Ljava/lang/String;

    .line 348
    iput-wide p4, p0, Lcom/google/android/wearable/datatransfer/WearableDataApiClient$OpenConnectionRequest;->offset:J

    .line 349
    return-void
.end method
