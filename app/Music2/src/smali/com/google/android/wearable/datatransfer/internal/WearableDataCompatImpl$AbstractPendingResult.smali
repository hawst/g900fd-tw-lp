.class public abstract Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;
.super Ljava/lang/Object;
.source "WearableDataCompatImpl.java"

# interfaces
.implements Lcom/google/android/wearable/datatransfer/WearableDataCompat$PendingResult;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "AbstractPendingResult"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/wearable/datatransfer/WearableDataCompat$PendingResult",
        "<TR;>;"
    }
.end annotation


# instance fields
.field private mCallback:Lcom/google/android/wearable/datatransfer/WearableDataCompat$ResultCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/wearable/datatransfer/WearableDataCompat$ResultCallback",
            "<-TR;>;"
        }
    .end annotation
.end field

.field private mCancelToken:Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$ICancelToken;

.field private mCanceled:Z

.field private volatile mConsumed:Z

.field private final mHandler:Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$CallbackHandler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$CallbackHandler",
            "<TR;>;"
        }
    .end annotation
.end field

.field private final mLatch:Ljava/util/concurrent/CountDownLatch;

.field private volatile mResult:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TR;"
        }
    .end annotation
.end field

.field private final mSyncToken:Ljava/lang/Object;

.field private mTimedOutOrInterrupted:Z


# direct methods
.method public constructor <init>(Landroid/os/Looper;)V
    .locals 2
    .param p1, "looper"    # Landroid/os/Looper;

    .prologue
    .line 57
    .local p0, "this":Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;, "Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult<TR;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;->mSyncToken:Ljava/lang/Object;

    .line 44
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;->mLatch:Ljava/util/concurrent/CountDownLatch;

    .line 58
    new-instance v0, Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$CallbackHandler;

    invoke-direct {v0, p1}, Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$CallbackHandler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;->mHandler:Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$CallbackHandler;

    .line 59
    return-void
.end method

.method private get()Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TR;"
        }
    .end annotation

    .prologue
    .line 81
    .local p0, "this":Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;, "Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult<TR;>;"
    iget-object v2, p0, Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;->mSyncToken:Ljava/lang/Object;

    monitor-enter v2

    .line 82
    :try_start_0
    iget-boolean v1, p0, Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;->mConsumed:Z

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    const-string v3, "Result has already been consumed."

    invoke-static {v1, v3}, Lcom/google/android/wearable/datatransfer/internal/Preconditions;->checkState(ZLjava/lang/Object;)V

    .line 83
    invoke-virtual {p0}, Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;->isReady()Z

    move-result v1

    const-string v3, "Result is not ready."

    invoke-static {v1, v3}, Lcom/google/android/wearable/datatransfer/internal/Preconditions;->checkState(ZLjava/lang/Object;)V

    .line 84
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;->mResult:Ljava/lang/Object;

    .line 85
    .local v0, "result":Ljava/lang/Object;, "TR;"
    invoke-virtual {p0}, Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;->consumeResult()V

    .line 86
    monitor-exit v2

    return-object v0

    .line 82
    .end local v0    # "result":Ljava/lang/Object;, "TR;"
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 87
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private maybeReleaseResult(Ljava/lang/Object;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;)V"
        }
    .end annotation

    .prologue
    .line 251
    .local p0, "this":Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;, "Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult<TR;>;"
    .local p1, "result":Ljava/lang/Object;, "TR;"
    instance-of v2, p1, Lcom/google/android/gms/common/api/Releasable;

    if-eqz v2, :cond_0

    .line 253
    :try_start_0
    move-object v0, p1

    check-cast v0, Lcom/google/android/gms/common/api/Releasable;

    move-object v2, v0

    invoke-interface {v2}, Lcom/google/android/gms/common/api/Releasable;->release()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 258
    :cond_0
    :goto_0
    return-void

    .line 254
    :catch_0
    move-exception v1

    .line 255
    .local v1, "e":Ljava/lang/RuntimeException;
    const-string v2, "AbstractPendingResult"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unable to release "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private setInterruptedResult()V
    .locals 2

    .prologue
    .line 218
    .local p0, "this":Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;, "Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult<TR;>;"
    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;->mSyncToken:Ljava/lang/Object;

    monitor-enter v1

    .line 219
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;->isReady()Z

    move-result v0

    if-nez v0, :cond_0

    .line 220
    const/16 v0, 0xe

    invoke-virtual {p0, v0}, Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;->createFailedResult(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;->setResult(Ljava/lang/Object;)V

    .line 221
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;->mTimedOutOrInterrupted:Z

    .line 223
    :cond_0
    monitor-exit v1

    .line 224
    return-void

    .line 223
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private setResultAndNotifyListeners(Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;)V"
        }
    .end annotation

    .prologue
    .line 236
    .local p0, "this":Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;, "Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult<TR;>;"
    .local p1, "result":Ljava/lang/Object;, "TR;"
    iput-object p1, p0, Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;->mResult:Ljava/lang/Object;

    .line 237
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;->mCancelToken:Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$ICancelToken;

    .line 238
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;->mLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 241
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;->mCallback:Lcom/google/android/wearable/datatransfer/WearableDataCompat$ResultCallback;

    if-eqz v0, :cond_0

    .line 242
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;->mHandler:Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$CallbackHandler;

    invoke-virtual {v0}, Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$CallbackHandler;->removeTimeoutMessages()V

    .line 243
    iget-boolean v0, p0, Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;->mCanceled:Z

    if-nez v0, :cond_0

    .line 244
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;->mHandler:Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$CallbackHandler;

    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;->mCallback:Lcom/google/android/wearable/datatransfer/WearableDataCompat$ResultCallback;

    invoke-direct {p0}, Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;->get()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$CallbackHandler;->sendResultCallback(Lcom/google/android/wearable/datatransfer/WearableDataCompat$ResultCallback;Ljava/lang/Object;)V

    .line 247
    :cond_0
    return-void
.end method


# virtual methods
.method public final await(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    .locals 9
    .param p1, "time"    # J
    .param p3, "units"    # Ljava/util/concurrent/TimeUnit;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/concurrent/TimeUnit;",
            ")TR;"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;, "Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult<TR;>;"
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 106
    const-wide/16 v6, 0x0

    cmp-long v2, p1, v6

    if-lez v2, :cond_0

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v5

    if-eq v2, v5, :cond_2

    :cond_0
    move v2, v4

    :goto_0
    const-string v5, "await must not be called on the UI thread when time is greater than zero."

    invoke-static {v2, v5}, Lcom/google/android/wearable/datatransfer/internal/Preconditions;->checkState(ZLjava/lang/Object;)V

    .line 108
    iget-boolean v2, p0, Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;->mConsumed:Z

    if-nez v2, :cond_3

    :goto_1
    const-string v2, "Result has already been consumed."

    invoke-static {v4, v2}, Lcom/google/android/wearable/datatransfer/internal/Preconditions;->checkState(ZLjava/lang/Object;)V

    .line 110
    :try_start_0
    iget-object v2, p0, Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;->mLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v2, p1, p2, p3}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z

    move-result v1

    .line 111
    .local v1, "isSignalled":Z
    if-nez v1, :cond_1

    .line 112
    invoke-virtual {p0}, Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;->setTimeoutResult()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 117
    .end local v1    # "isSignalled":Z
    :cond_1
    :goto_2
    invoke-virtual {p0}, Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;->isReady()Z

    move-result v2

    const-string v3, "Result is not ready."

    invoke-static {v2, v3}, Lcom/google/android/wearable/datatransfer/internal/Preconditions;->checkState(ZLjava/lang/Object;)V

    .line 118
    invoke-direct {p0}, Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;->get()Ljava/lang/Object;

    move-result-object v2

    return-object v2

    :cond_2
    move v2, v3

    .line 106
    goto :goto_0

    :cond_3
    move v4, v3

    .line 108
    goto :goto_1

    .line 114
    :catch_0
    move-exception v0

    .line 115
    .local v0, "ex":Ljava/lang/InterruptedException;
    invoke-direct {p0}, Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;->setInterruptedResult()V

    goto :goto_2
.end method

.method protected consumeResult()V
    .locals 2

    .prologue
    .local p0, "this":Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;, "Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult<TR;>;"
    const/4 v1, 0x0

    .line 212
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;->mConsumed:Z

    .line 213
    iput-object v1, p0, Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;->mResult:Ljava/lang/Object;

    .line 214
    iput-object v1, p0, Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;->mCallback:Lcom/google/android/wearable/datatransfer/WearableDataCompat$ResultCallback;

    .line 215
    return-void
.end method

.method protected abstract createFailedResult(I)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TR;"
        }
    .end annotation
.end method

.method public final isReady()Z
    .locals 4

    .prologue
    .line 77
    .local p0, "this":Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;, "Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult<TR;>;"
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;->mLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->getCount()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setCancelToken(Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$ICancelToken;)V
    .locals 2
    .param p1, "cancelToken"    # Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$ICancelToken;

    .prologue
    .line 156
    .local p0, "this":Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;, "Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult<TR;>;"
    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;->mSyncToken:Ljava/lang/Object;

    monitor-enter v1

    .line 157
    :try_start_0
    iput-object p1, p0, Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;->mCancelToken:Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$ICancelToken;

    .line 158
    monitor-exit v1

    .line 159
    return-void

    .line 158
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final setResult(Ljava/lang/Object;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;)V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;, "Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult<TR;>;"
    .local p1, "result":Ljava/lang/Object;, "TR;"
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 196
    iget-object v3, p0, Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;->mSyncToken:Ljava/lang/Object;

    monitor-enter v3

    .line 197
    :try_start_0
    iget-boolean v2, p0, Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;->mTimedOutOrInterrupted:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;->mCanceled:Z

    if-eqz v2, :cond_1

    .line 198
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;->maybeReleaseResult(Ljava/lang/Object;)V

    .line 199
    monitor-exit v3

    .line 206
    :goto_0
    return-void

    .line 201
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;->isReady()Z

    move-result v2

    if-nez v2, :cond_2

    move v2, v0

    :goto_1
    const-string v4, "Results have already been set"

    invoke-static {v2, v4}, Lcom/google/android/wearable/datatransfer/internal/Preconditions;->checkState(ZLjava/lang/Object;)V

    .line 202
    iget-boolean v2, p0, Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;->mConsumed:Z

    if-nez v2, :cond_3

    :goto_2
    const-string v1, "Result has already been consumed"

    invoke-static {v0, v1}, Lcom/google/android/wearable/datatransfer/internal/Preconditions;->checkState(ZLjava/lang/Object;)V

    .line 204
    invoke-direct {p0, p1}, Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;->setResultAndNotifyListeners(Ljava/lang/Object;)V

    .line 205
    monitor-exit v3

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    move v2, v1

    .line 201
    goto :goto_1

    :cond_3
    move v0, v1

    .line 202
    goto :goto_2
.end method

.method setTimeoutResult()V
    .locals 2

    .prologue
    .line 227
    .local p0, "this":Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;, "Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult<TR;>;"
    iget-object v1, p0, Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;->mSyncToken:Ljava/lang/Object;

    monitor-enter v1

    .line 228
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;->isReady()Z

    move-result v0

    if-nez v0, :cond_0

    .line 229
    const/16 v0, 0xf

    invoke-virtual {p0, v0}, Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;->createFailedResult(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;->setResult(Ljava/lang/Object;)V

    .line 230
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/wearable/datatransfer/internal/WearableDataCompatImpl$AbstractPendingResult;->mTimedOutOrInterrupted:Z

    .line 232
    :cond_0
    monitor-exit v1

    .line 233
    return-void

    .line 232
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
