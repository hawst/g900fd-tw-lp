.class public final Lcom/google/android/wearable/datatransfer/ConnectionService$OpenFileDescriptorResult;
.super Ljava/lang/Object;
.source "ConnectionService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/wearable/datatransfer/ConnectionService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "OpenFileDescriptorResult"
.end annotation


# instance fields
.field public final appStatusCode:I

.field public final parcelFileDescriptor:Landroid/os/ParcelFileDescriptor;

.field public final statusCode:I


# direct methods
.method private constructor <init>(IILandroid/os/ParcelFileDescriptor;)V
    .locals 0
    .param p1, "statusCode"    # I
    .param p2, "appStatusCode"    # I
    .param p3, "parcelFileDescriptor"    # Landroid/os/ParcelFileDescriptor;

    .prologue
    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    iput p1, p0, Lcom/google/android/wearable/datatransfer/ConnectionService$OpenFileDescriptorResult;->statusCode:I

    .line 83
    iput p2, p0, Lcom/google/android/wearable/datatransfer/ConnectionService$OpenFileDescriptorResult;->appStatusCode:I

    .line 84
    iput-object p3, p0, Lcom/google/android/wearable/datatransfer/ConnectionService$OpenFileDescriptorResult;->parcelFileDescriptor:Landroid/os/ParcelFileDescriptor;

    .line 85
    return-void
.end method

.method public static forClientOnlyConnectionService()Lcom/google/android/wearable/datatransfer/ConnectionService$OpenFileDescriptorResult;
    .locals 4

    .prologue
    .line 115
    new-instance v0, Lcom/google/android/wearable/datatransfer/ConnectionService$OpenFileDescriptorResult;

    const/4 v1, -0x2

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/wearable/datatransfer/ConnectionService$OpenFileDescriptorResult;-><init>(IILandroid/os/ParcelFileDescriptor;)V

    return-object v0
.end method

.method public static forFailure(I)Lcom/google/android/wearable/datatransfer/ConnectionService$OpenFileDescriptorResult;
    .locals 3
    .param p0, "appErrorCode"    # I

    .prologue
    .line 107
    new-instance v0, Lcom/google/android/wearable/datatransfer/ConnectionService$OpenFileDescriptorResult;

    const/4 v1, -0x1

    const/4 v2, 0x0

    invoke-direct {v0, v1, p0, v2}, Lcom/google/android/wearable/datatransfer/ConnectionService$OpenFileDescriptorResult;-><init>(IILandroid/os/ParcelFileDescriptor;)V

    return-object v0
.end method

.method public static forSuccess(Landroid/os/ParcelFileDescriptor;)Lcom/google/android/wearable/datatransfer/ConnectionService$OpenFileDescriptorResult;
    .locals 2
    .param p0, "parcelFileDescriptor"    # Landroid/os/ParcelFileDescriptor;

    .prologue
    const/4 v1, 0x0

    .line 94
    if-nez p0, :cond_0

    .line 95
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "parcelFileDescriptor can\'t be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 97
    :cond_0
    new-instance v0, Lcom/google/android/wearable/datatransfer/ConnectionService$OpenFileDescriptorResult;

    invoke-direct {v0, v1, v1, p0}, Lcom/google/android/wearable/datatransfer/ConnectionService$OpenFileDescriptorResult;-><init>(IILandroid/os/ParcelFileDescriptor;)V

    return-object v0
.end method
