.class public Lcom/google/android/wearable/datatransfer/WearableDataListenerService;
.super Lcom/google/android/gms/wearable/WearableListenerService;
.source "WearableDataListenerService.java"


# instance fields
.field private mDelegate:Lcom/google/android/wearable/datatransfer/WearableDataListenerDelegate;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/google/android/gms/wearable/WearableListenerService;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate()V
    .locals 1

    .prologue
    .line 38
    invoke-super {p0}, Lcom/google/android/gms/wearable/WearableListenerService;->onCreate()V

    .line 39
    new-instance v0, Lcom/google/android/wearable/datatransfer/WearableDataListenerDelegate;

    invoke-direct {v0, p0}, Lcom/google/android/wearable/datatransfer/WearableDataListenerDelegate;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/WearableDataListenerService;->mDelegate:Lcom/google/android/wearable/datatransfer/WearableDataListenerDelegate;

    .line 40
    return-void
.end method

.method public onDataChanged(Lcom/google/android/gms/wearable/DataEventBuffer;)V
    .locals 1
    .param p1, "dataEvents"    # Lcom/google/android/gms/wearable/DataEventBuffer;

    .prologue
    .line 50
    invoke-super {p0, p1}, Lcom/google/android/gms/wearable/WearableListenerService;->onDataChanged(Lcom/google/android/gms/wearable/DataEventBuffer;)V

    .line 51
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/WearableDataListenerService;->mDelegate:Lcom/google/android/wearable/datatransfer/WearableDataListenerDelegate;

    invoke-virtual {v0, p1}, Lcom/google/android/wearable/datatransfer/WearableDataListenerDelegate;->onDataChanged(Lcom/google/android/gms/wearable/DataEventBuffer;)V

    .line 52
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/WearableDataListenerService;->mDelegate:Lcom/google/android/wearable/datatransfer/WearableDataListenerDelegate;

    invoke-virtual {v0}, Lcom/google/android/wearable/datatransfer/WearableDataListenerDelegate;->release()V

    .line 45
    invoke-super {p0}, Lcom/google/android/gms/wearable/WearableListenerService;->onDestroy()V

    .line 46
    return-void
.end method

.method public onMessageReceived(Lcom/google/android/gms/wearable/MessageEvent;)V
    .locals 1
    .param p1, "messageEvent"    # Lcom/google/android/gms/wearable/MessageEvent;

    .prologue
    .line 56
    invoke-super {p0, p1}, Lcom/google/android/gms/wearable/WearableListenerService;->onMessageReceived(Lcom/google/android/gms/wearable/MessageEvent;)V

    .line 57
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/WearableDataListenerService;->mDelegate:Lcom/google/android/wearable/datatransfer/WearableDataListenerDelegate;

    invoke-virtual {v0, p1}, Lcom/google/android/wearable/datatransfer/WearableDataListenerDelegate;->onMessageReceived(Lcom/google/android/gms/wearable/MessageEvent;)V

    .line 58
    return-void
.end method

.method public onPeerConnected(Lcom/google/android/gms/wearable/Node;)V
    .locals 1
    .param p1, "peer"    # Lcom/google/android/gms/wearable/Node;

    .prologue
    .line 62
    invoke-super {p0, p1}, Lcom/google/android/gms/wearable/WearableListenerService;->onPeerConnected(Lcom/google/android/gms/wearable/Node;)V

    .line 63
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/WearableDataListenerService;->mDelegate:Lcom/google/android/wearable/datatransfer/WearableDataListenerDelegate;

    invoke-virtual {v0, p1}, Lcom/google/android/wearable/datatransfer/WearableDataListenerDelegate;->onPeerConnected(Lcom/google/android/gms/wearable/Node;)V

    .line 64
    return-void
.end method

.method public onPeerDisconnected(Lcom/google/android/gms/wearable/Node;)V
    .locals 1
    .param p1, "peer"    # Lcom/google/android/gms/wearable/Node;

    .prologue
    .line 68
    invoke-super {p0, p1}, Lcom/google/android/gms/wearable/WearableListenerService;->onPeerDisconnected(Lcom/google/android/gms/wearable/Node;)V

    .line 69
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/WearableDataListenerService;->mDelegate:Lcom/google/android/wearable/datatransfer/WearableDataListenerDelegate;

    invoke-virtual {v0, p1}, Lcom/google/android/wearable/datatransfer/WearableDataListenerDelegate;->onPeerDisconnected(Lcom/google/android/gms/wearable/Node;)V

    .line 70
    return-void
.end method
