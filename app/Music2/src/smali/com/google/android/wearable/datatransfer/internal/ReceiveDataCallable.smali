.class final Lcom/google/android/wearable/datatransfer/internal/ReceiveDataCallable;
.super Ljava/lang/Object;
.source "ReceiveDataCallable.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/android/wearable/datatransfer/internal/DataReceiver$Result;",
        ">;"
    }
.end annotation


# instance fields
.field private final mDataReceiver:Lcom/google/android/wearable/datatransfer/internal/DataReceiver;


# direct methods
.method constructor <init>(Lcom/google/android/gms/common/api/GoogleApiClient;Ljava/lang/String;Ljava/lang/String;JJLcom/google/android/wearable/datatransfer/internal/DataReceiver$Callbacks;)V
    .locals 10
    .param p1, "client"    # Lcom/google/android/gms/common/api/GoogleApiClient;
    .param p2, "targetNodeId"    # Ljava/lang/String;
    .param p3, "path"    # Ljava/lang/String;
    .param p4, "offset"    # J
    .param p6, "connectionId"    # J
    .param p8, "callbacks"    # Lcom/google/android/wearable/datatransfer/internal/DataReceiver$Callbacks;

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    new-instance v0, Lcom/google/android/wearable/datatransfer/internal/DataReceiver;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-wide v4, p4

    move-wide/from16 v6, p6

    move-object/from16 v8, p8

    invoke-direct/range {v0 .. v8}, Lcom/google/android/wearable/datatransfer/internal/DataReceiver;-><init>(Lcom/google/android/gms/common/api/GoogleApiClient;Ljava/lang/String;Ljava/lang/String;JJLcom/google/android/wearable/datatransfer/internal/DataReceiver$Callbacks;)V

    iput-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/ReceiveDataCallable;->mDataReceiver:Lcom/google/android/wearable/datatransfer/internal/DataReceiver;

    .line 30
    return-void
.end method


# virtual methods
.method public call()Lcom/google/android/wearable/datatransfer/internal/DataReceiver$Result;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/wearable/datatransfer/internal/ReceiveDataCallable;->mDataReceiver:Lcom/google/android/wearable/datatransfer/internal/DataReceiver;

    invoke-virtual {v0}, Lcom/google/android/wearable/datatransfer/internal/DataReceiver;->download()Lcom/google/android/wearable/datatransfer/internal/DataReceiver$Result;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 16
    invoke-virtual {p0}, Lcom/google/android/wearable/datatransfer/internal/ReceiveDataCallable;->call()Lcom/google/android/wearable/datatransfer/internal/DataReceiver$Result;

    move-result-object v0

    return-object v0
.end method
