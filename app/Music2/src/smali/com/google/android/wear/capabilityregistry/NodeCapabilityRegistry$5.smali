.class Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry$5;
.super Ljava/lang/Object;
.source "NodeCapabilityRegistry.java"

# interfaces
.implements Lcom/google/android/gms/common/api/ResultCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry;->getNodesForCapabilityWithConnectedClient(Lcom/google/android/gms/common/api/GoogleApiClient;Ljava/lang/String;Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry$NodeListCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/gms/common/api/ResultCallback",
        "<",
        "Lcom/google/android/gms/wearable/DataItemBuffer;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry;

.field final synthetic val$callback:Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry$NodeListCallback;

.field final synthetic val$capabilityPath:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry;Ljava/lang/String;Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry$NodeListCallback;)V
    .locals 0

    .prologue
    .line 180
    iput-object p1, p0, Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry$5;->this$0:Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry;

    iput-object p2, p0, Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry$5;->val$capabilityPath:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry$5;->val$callback:Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry$NodeListCallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic onResult(Lcom/google/android/gms/common/api/Result;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/gms/common/api/Result;

    .prologue
    .line 180
    check-cast p1, Lcom/google/android/gms/wearable/DataItemBuffer;

    .end local p1    # "x0":Lcom/google/android/gms/common/api/Result;
    invoke-virtual {p0, p1}, Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry$5;->onResult(Lcom/google/android/gms/wearable/DataItemBuffer;)V

    return-void
.end method

.method public onResult(Lcom/google/android/gms/wearable/DataItemBuffer;)V
    .locals 5
    .param p1, "buffer"    # Lcom/google/android/gms/wearable/DataItemBuffer;

    .prologue
    const/4 v3, 0x1

    .line 186
    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/gms/wearable/DataItemBuffer;->getCount()I

    move-result v2

    if-ge v2, v3, :cond_1

    .line 187
    const-string v2, "CAPABILITY_REGISTRY"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "No intent receiver node registered for: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry$5;->val$capabilityPath:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 188
    iget-object v2, p0, Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry$5;->val$callback:Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry$NodeListCallback;

    if-eqz v2, :cond_0

    .line 189
    iget-object v2, p0, Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry$5;->val$callback:Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry$NodeListCallback;

    const/4 v3, 0x0

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v2, v3, v4}, Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry$NodeListCallback;->onComplete(ZLjava/util/List;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 202
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/wearable/DataItemBuffer;->release()V

    .line 204
    :goto_0
    return-void

    .line 194
    :cond_1
    :try_start_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 195
    .local v1, "nodeIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    invoke-virtual {p1}, Lcom/google/android/gms/wearable/DataItemBuffer;->getCount()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 196
    invoke-virtual {p1, v0}, Lcom/google/android/gms/wearable/DataItemBuffer;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/wearable/DataItem;

    invoke-interface {v2}, Lcom/google/android/gms/wearable/DataItem;->getUri()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 195
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 198
    :cond_2
    iget-object v2, p0, Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry$5;->val$callback:Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry$NodeListCallback;

    if-eqz v2, :cond_3

    .line 199
    iget-object v2, p0, Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry$5;->val$callback:Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry$NodeListCallback;

    const/4 v3, 0x1

    invoke-interface {v2, v3, v1}, Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry$NodeListCallback;->onComplete(ZLjava/util/List;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 202
    :cond_3
    invoke-virtual {p1}, Lcom/google/android/gms/wearable/DataItemBuffer;->release()V

    goto :goto_0

    .end local v0    # "i":I
    .end local v1    # "nodeIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :catchall_0
    move-exception v2

    invoke-virtual {p1}, Lcom/google/android/gms/wearable/DataItemBuffer;->release()V

    throw v2
.end method
