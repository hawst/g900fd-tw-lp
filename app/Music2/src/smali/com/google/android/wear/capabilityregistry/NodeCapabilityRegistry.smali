.class public Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry;
.super Ljava/lang/Object;
.source "NodeCapabilityRegistry.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry$NodeListCallback;
    }
.end annotation


# instance fields
.field private final mApplicationContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry;->mApplicationContext:Landroid/content/Context;

    .line 53
    return-void
.end method

.method private static getCapabilityUri(Ljava/lang/String;)Landroid/net/Uri;
    .locals 3
    .param p0, "capabilityPath"    # Ljava/lang/String;

    .prologue
    .line 209
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string v1, "wear"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "/com.google.android.wear.capabilityregistry/capabilities/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static uriIsForCapability(Landroid/net/Uri;Ljava/lang/String;)Z
    .locals 2
    .param p0, "dataItemUri"    # Landroid/net/Uri;
    .param p1, "capabilityPath"    # Ljava/lang/String;

    .prologue
    .line 219
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/com.google.android.wear.capabilityregistry/capabilities/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public getNodesForCapabilityWithConnectedClient(Lcom/google/android/gms/common/api/GoogleApiClient;Ljava/lang/String;Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry$NodeListCallback;)V
    .locals 5
    .param p1, "client"    # Lcom/google/android/gms/common/api/GoogleApiClient;
    .param p2, "capabilityPath"    # Ljava/lang/String;
    .param p3, "callback"    # Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry$NodeListCallback;

    .prologue
    .line 173
    invoke-interface {p1}, Lcom/google/android/gms/common/api/GoogleApiClient;->isConnected()Z

    move-result v1

    if-nez v1, :cond_0

    .line 174
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Client must be connected before sending"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 177
    :cond_0
    sget-object v1, Lcom/google/android/gms/wearable/Wearable;->DataApi:Lcom/google/android/gms/wearable/DataApi;

    invoke-static {p2}, Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry;->getCapabilityUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-interface {v1, p1, v2}, Lcom/google/android/gms/wearable/DataApi;->getDataItems(Lcom/google/android/gms/common/api/GoogleApiClient;Landroid/net/Uri;)Lcom/google/android/gms/common/api/PendingResult;

    move-result-object v0

    .line 180
    .local v0, "pendingDataItems":Lcom/google/android/gms/common/api/PendingResult;, "Lcom/google/android/gms/common/api/PendingResult<Lcom/google/android/gms/wearable/DataItemBuffer;>;"
    new-instance v1, Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry$5;

    invoke-direct {v1, p0, p2, p3}, Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry$5;-><init>(Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry;Ljava/lang/String;Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry$NodeListCallback;)V

    const-wide/16 v2, 0x1e

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/google/android/gms/common/api/PendingResult;->setResultCallback(Lcom/google/android/gms/common/api/ResultCallback;JLjava/util/concurrent/TimeUnit;)V

    .line 206
    return-void
.end method

.method public registerCapabilityPath(Ljava/lang/String;)V
    .locals 3
    .param p1, "capabilityPath"    # Ljava/lang/String;

    .prologue
    .line 65
    new-instance v1, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    iget-object v2, p0, Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry;->mApplicationContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;-><init>(Landroid/content/Context;)V

    sget-object v2, Lcom/google/android/gms/wearable/Wearable;->API:Lcom/google/android/gms/common/api/Api;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;->addApi(Lcom/google/android/gms/common/api/Api;)Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;->build()Lcom/google/android/gms/common/api/GoogleApiClient;

    move-result-object v0

    .line 69
    .local v0, "client":Lcom/google/android/gms/common/api/GoogleApiClient;
    new-instance v1, Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry$1;

    invoke-direct {v1, p0, p1, v0}, Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry$1;-><init>(Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry;Ljava/lang/String;Lcom/google/android/gms/common/api/GoogleApiClient;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/GoogleApiClient;->registerConnectionCallbacks(Lcom/google/android/gms/common/api/GoogleApiClient$ConnectionCallbacks;)V

    .line 93
    invoke-interface {v0}, Lcom/google/android/gms/common/api/GoogleApiClient;->connect()V

    .line 94
    return-void
.end method
