.class public Lcom/google/android/wear/intentstarter/SendableIntent;
.super Ljava/lang/Object;
.source "SendableIntent.java"


# direct methods
.method static intentFromByteArray([B)Landroid/content/Intent;
    .locals 7
    .param p0, "array"    # [B

    .prologue
    .line 236
    invoke-static {p0}, Lcom/google/android/gms/wearable/DataMap;->fromByteArray([B)Lcom/google/android/gms/wearable/DataMap;

    move-result-object v2

    .line 237
    .local v2, "dataMap":Lcom/google/android/gms/wearable/DataMap;
    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    .line 238
    .local v4, "intent":Landroid/content/Intent;
    const-string v6, "action"

    invoke-virtual {v2, v6}, Lcom/google/android/gms/wearable/DataMap;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 239
    const-string v6, "action"

    invoke-virtual {v2, v6}, Lcom/google/android/gms/wearable/DataMap;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 241
    :cond_0
    const-string v6, "package"

    invoke-virtual {v2, v6}, Lcom/google/android/gms/wearable/DataMap;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 242
    const-string v6, "package"

    invoke-virtual {v2, v6}, Lcom/google/android/gms/wearable/DataMap;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 244
    :cond_1
    const-string v6, "categories"

    invoke-virtual {v2, v6}, Lcom/google/android/gms/wearable/DataMap;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 245
    const-string v6, "categories"

    invoke-virtual {v2, v6}, Lcom/google/android/gms/wearable/DataMap;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .local v0, "arr$":[Ljava/lang/String;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v5, :cond_2

    aget-object v1, v0, v3

    .line 246
    .local v1, "category":Ljava/lang/String;
    invoke-virtual {v4, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 245
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 249
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "category":Ljava/lang/String;
    .end local v3    # "i$":I
    .end local v5    # "len$":I
    :cond_2
    const-string v6, "data"

    invoke-virtual {v2, v6}, Lcom/google/android/gms/wearable/DataMap;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 250
    const-string v6, "data"

    invoke-virtual {v2, v6}, Lcom/google/android/gms/wearable/DataMap;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 252
    :cond_3
    const-string v6, "type"

    invoke-virtual {v2, v6}, Lcom/google/android/gms/wearable/DataMap;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 253
    const-string v6, "type"

    invoke-virtual {v2, v6}, Lcom/google/android/gms/wearable/DataMap;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 255
    :cond_4
    const-string v6, "extras"

    invoke-virtual {v2, v6}, Lcom/google/android/gms/wearable/DataMap;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 256
    const-string v6, "extras"

    invoke-virtual {v2, v6}, Lcom/google/android/gms/wearable/DataMap;->getDataMap(Ljava/lang/String;)Lcom/google/android/gms/wearable/DataMap;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/gms/wearable/DataMap;->toBundle()Landroid/os/Bundle;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 258
    :cond_5
    const-string v6, "flags"

    invoke-virtual {v2, v6}, Lcom/google/android/gms/wearable/DataMap;->getInt(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v4, v6}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 259
    return-object v4
.end method

.method static isForActivityStart([B)Z
    .locals 3
    .param p0, "array"    # [B

    .prologue
    .line 263
    invoke-static {p0}, Lcom/google/android/gms/wearable/DataMap;->fromByteArray([B)Lcom/google/android/gms/wearable/DataMap;

    move-result-object v0

    .line 264
    .local v0, "dataMap":Lcom/google/android/gms/wearable/DataMap;
    const-string v1, "intent_type"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wearable/DataMap;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 265
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Supplied data map does not contain type key"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 268
    :cond_0
    const-string v1, "activity"

    const-string v2, "intent_type"

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wearable/DataMap;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    return v1
.end method

.method static isForBroadcastSend([B)Z
    .locals 3
    .param p0, "array"    # [B

    .prologue
    .line 281
    invoke-static {p0}, Lcom/google/android/gms/wearable/DataMap;->fromByteArray([B)Lcom/google/android/gms/wearable/DataMap;

    move-result-object v0

    .line 282
    .local v0, "dataMap":Lcom/google/android/gms/wearable/DataMap;
    const-string v1, "intent_type"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wearable/DataMap;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 283
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Supplied data map does not contain type key"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 286
    :cond_0
    const-string v1, "broadcast"

    const-string v2, "intent_type"

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wearable/DataMap;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    return v1
.end method

.method static isForServiceStart([B)Z
    .locals 3
    .param p0, "array"    # [B

    .prologue
    .line 272
    invoke-static {p0}, Lcom/google/android/gms/wearable/DataMap;->fromByteArray([B)Lcom/google/android/gms/wearable/DataMap;

    move-result-object v0

    .line 273
    .local v0, "dataMap":Lcom/google/android/gms/wearable/DataMap;
    const-string v1, "intent_type"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wearable/DataMap;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 274
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Supplied data map does not contain type key"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 277
    :cond_0
    const-string v1, "service"

    const-string v2, "intent_type"

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wearable/DataMap;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    return v1
.end method
