.class Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry$1;
.super Ljava/lang/Object;
.source "NodeCapabilityRegistry.java"

# interfaces
.implements Lcom/google/android/gms/common/api/GoogleApiClient$ConnectionCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry;->registerCapabilityPath(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry;

.field final synthetic val$capabilityPath:Ljava/lang/String;

.field final synthetic val$client:Lcom/google/android/gms/common/api/GoogleApiClient;


# direct methods
.method constructor <init>(Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry;Ljava/lang/String;Lcom/google/android/gms/common/api/GoogleApiClient;)V
    .locals 0

    .prologue
    .line 69
    iput-object p1, p0, Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry$1;->this$0:Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry;

    iput-object p2, p0, Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry$1;->val$capabilityPath:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry$1;->val$client:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onConnected(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 72
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "/com.google.android.wear.capabilityregistry/capabilities/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry$1;->val$capabilityPath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/wearable/PutDataRequest;->create(Ljava/lang/String;)Lcom/google/android/gms/wearable/PutDataRequest;

    move-result-object v0

    .line 74
    .local v0, "dataRequest":Lcom/google/android/gms/wearable/PutDataRequest;
    sget-object v1, Lcom/google/android/gms/wearable/Wearable;->DataApi:Lcom/google/android/gms/wearable/DataApi;

    iget-object v2, p0, Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry$1;->val$client:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-interface {v1, v2, v0}, Lcom/google/android/gms/wearable/DataApi;->putDataItem(Lcom/google/android/gms/common/api/GoogleApiClient;Lcom/google/android/gms/wearable/PutDataRequest;)Lcom/google/android/gms/common/api/PendingResult;

    move-result-object v1

    new-instance v2, Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry$1$1;

    invoke-direct {v2, p0}, Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry$1$1;-><init>(Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry$1;)V

    const-wide/16 v4, 0x1e

    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v1, v2, v4, v5, v3}, Lcom/google/android/gms/common/api/PendingResult;->setResultCallback(Lcom/google/android/gms/common/api/ResultCallback;JLjava/util/concurrent/TimeUnit;)V

    .line 87
    return-void
.end method

.method public onConnectionSuspended(I)V
    .locals 0
    .param p1, "i"    # I

    .prologue
    .line 91
    return-void
.end method
