.class public Lcom/google/android/wear/intentstarter/IntentReceiver;
.super Ljava/lang/Object;
.source "IntentReceiver.java"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mNodeCapabilityRegistry:Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/google/android/wear/intentstarter/IntentReceiver;->mContext:Landroid/content/Context;

    .line 41
    new-instance v0, Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry;

    invoke-direct {v0, p1}, Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/wear/intentstarter/IntentReceiver;->mNodeCapabilityRegistry:Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry;

    .line 42
    return-void
.end method


# virtual methods
.method public handleIncomingIntentRequest(Lcom/google/android/gms/wearable/MessageEvent;)Z
    .locals 6
    .param p1, "messageEvent"    # Lcom/google/android/gms/wearable/MessageEvent;

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 62
    const-string v4, "/com.google.android.wear.intentstarter/start"

    invoke-interface {p1}, Lcom/google/android/gms/wearable/MessageEvent;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 78
    :cond_0
    :goto_0
    return v2

    .line 66
    :cond_1
    invoke-interface {p1}, Lcom/google/android/gms/wearable/MessageEvent;->getData()[B

    move-result-object v0

    .line 67
    .local v0, "data":[B
    invoke-static {v0}, Lcom/google/android/wear/intentstarter/SendableIntent;->intentFromByteArray([B)Landroid/content/Intent;

    move-result-object v1

    .line 68
    .local v1, "intent":Landroid/content/Intent;
    invoke-static {v0}, Lcom/google/android/wear/intentstarter/SendableIntent;->isForActivityStart([B)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 69
    iget-object v2, p0, Lcom/google/android/wear/intentstarter/IntentReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    move v2, v3

    .line 70
    goto :goto_0

    .line 71
    :cond_2
    invoke-static {v0}, Lcom/google/android/wear/intentstarter/SendableIntent;->isForBroadcastSend([B)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 72
    iget-object v2, p0, Lcom/google/android/wear/intentstarter/IntentReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    move v2, v3

    .line 73
    goto :goto_0

    .line 74
    :cond_3
    invoke-static {v0}, Lcom/google/android/wear/intentstarter/SendableIntent;->isForServiceStart([B)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 75
    iget-object v2, p0, Lcom/google/android/wear/intentstarter/IntentReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    move v2, v3

    .line 76
    goto :goto_0
.end method

.method public registerAsReceiverInWearNetwork()V
    .locals 2

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/wear/intentstarter/IntentReceiver;->mNodeCapabilityRegistry:Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry;

    const-string v1, "/com.google.android.wear.intentstarter"

    invoke-virtual {v0, v1}, Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry;->registerCapabilityPath(Ljava/lang/String;)V

    .line 52
    return-void
.end method
