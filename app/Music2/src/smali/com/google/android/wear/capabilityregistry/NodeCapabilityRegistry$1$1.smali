.class Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry$1$1;
.super Ljava/lang/Object;
.source "NodeCapabilityRegistry.java"

# interfaces
.implements Lcom/google/android/gms/common/api/ResultCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry$1;->onConnected(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/gms/common/api/ResultCallback",
        "<",
        "Lcom/google/android/gms/wearable/DataApi$DataItemResult;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry$1;


# direct methods
.method constructor <init>(Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry$1;)V
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry$1$1;->this$1:Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic onResult(Lcom/google/android/gms/common/api/Result;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/gms/common/api/Result;

    .prologue
    .line 75
    check-cast p1, Lcom/google/android/gms/wearable/DataApi$DataItemResult;

    .end local p1    # "x0":Lcom/google/android/gms/common/api/Result;
    invoke-virtual {p0, p1}, Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry$1$1;->onResult(Lcom/google/android/gms/wearable/DataApi$DataItemResult;)V

    return-void
.end method

.method public onResult(Lcom/google/android/gms/wearable/DataApi$DataItemResult;)V
    .locals 3
    .param p1, "dataItemResult"    # Lcom/google/android/gms/wearable/DataApi$DataItemResult;

    .prologue
    .line 78
    invoke-interface {p1}, Lcom/google/android/gms/wearable/DataApi$DataItemResult;->getStatus()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->isSuccess()Z

    move-result v0

    if-nez v0, :cond_0

    .line 79
    const-string v0, "CAPABILITY_REGISTRY"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to register path:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry$1$1;->this$1:Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry$1;

    iget-object v2, v2, Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry$1;->val$capabilityPath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, Lcom/google/android/gms/wearable/DataApi$DataItemResult;->getStatus()Lcom/google/android/gms/common/api/Status;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/common/api/Status;->getStatusMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 84
    :cond_0
    iget-object v0, p0, Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry$1$1;->this$1:Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry$1;

    iget-object v0, v0, Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry$1;->val$client:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/GoogleApiClient;->disconnect()V

    .line 85
    return-void
.end method
