.class Lcom/google/android/music/activitymanagement/KeepOnManager$1;
.super Ljava/lang/Object;
.source "KeepOnManager.java"

# interfaces
.implements Lcom/google/android/music/utils/async/AsyncRunner;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/activitymanagement/KeepOnManager;->showWillDownloadLaterDialogIfNecessary(Landroid/content/Context;Lcom/google/android/music/medialist/SongList;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field mSongListName:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/music/activitymanagement/KeepOnManager;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$isDLWifiOnly:Z

.field final synthetic val$songList:Lcom/google/android/music/medialist/SongList;


# direct methods
.method constructor <init>(Lcom/google/android/music/activitymanagement/KeepOnManager;Lcom/google/android/music/medialist/SongList;Landroid/content/Context;Z)V
    .locals 0

    .prologue
    .line 498
    iput-object p1, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$1;->this$0:Lcom/google/android/music/activitymanagement/KeepOnManager;

    iput-object p2, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$1;->val$songList:Lcom/google/android/music/medialist/SongList;

    iput-object p3, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$1;->val$context:Landroid/content/Context;

    iput-boolean p4, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$1;->val$isDLWifiOnly:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public backgroundTask()V
    .locals 2

    .prologue
    .line 503
    iget-object v0, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$1;->val$songList:Lcom/google/android/music/medialist/SongList;

    iget-object v1, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$1;->val$context:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/google/android/music/medialist/SongList;->getName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$1;->mSongListName:Ljava/lang/String;

    .line 504
    return-void
.end method

.method public taskCompleted()V
    .locals 12

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x1

    .line 508
    iget-object v5, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$1;->val$context:Landroid/content/Context;

    instance-of v5, v5, Lcom/google/android/music/ui/BaseActivity;

    if-eqz v5, :cond_1

    .line 509
    iget-object v0, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$1;->val$context:Landroid/content/Context;

    check-cast v0, Lcom/google/android/music/ui/BaseActivity;

    .line 510
    .local v0, "activity":Lcom/google/android/music/ui/BaseActivity;
    invoke-virtual {v0}, Lcom/google/android/music/ui/BaseActivity;->isFinishing()Z

    move-result v5

    if-nez v5, :cond_0

    invoke-virtual {v0}, Lcom/google/android/music/ui/BaseActivity;->isActivityDestroyed()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 529
    .end local v0    # "activity":Lcom/google/android/music/ui/BaseActivity;
    :cond_0
    :goto_0
    return-void

    .line 517
    :cond_1
    new-instance v2, Landroid/app/AlertDialog$Builder;

    iget-object v5, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$1;->val$context:Landroid/content/Context;

    invoke-direct {v2, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 518
    .local v2, "builder":Landroid/app/AlertDialog$Builder;
    iget-object v5, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$1;->val$context:Landroid/content/Context;

    invoke-static {v5}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    .line 519
    .local v4, "inflater":Landroid/view/LayoutInflater;
    const v5, 0x7f040087

    invoke-virtual {v4, v5, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    .line 521
    .local v3, "customView":Landroid/view/ViewGroup;
    const v5, 0x7f0e01ca

    invoke-virtual {v3, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 522
    .local v1, "body":Landroid/widget/TextView;
    iget-object v6, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$1;->val$context:Landroid/content/Context;

    iget-boolean v5, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$1;->val$isDLWifiOnly:Z

    if-eqz v5, :cond_2

    const v5, 0x7f0b0162

    :goto_1
    new-array v7, v10, [Ljava/lang/Object;

    const/4 v8, 0x0

    iget-object v9, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$1;->mSongListName:Ljava/lang/String;

    aput-object v9, v7, v8

    invoke-virtual {v6, v5, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 525
    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 526
    invoke-virtual {v2, v10}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 527
    const v5, 0x7f0b004e

    invoke-virtual {v2, v5, v11}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 528
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/AlertDialog;->show()V

    goto :goto_0

    .line 522
    :cond_2
    const v5, 0x7f0b0161

    goto :goto_1
.end method
