.class Lcom/google/android/music/cast/CastSessionManager$2;
.super Landroid/support/v7/media/MediaRouter$ControlRequestCallback;
.source "CastSessionManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/cast/CastSessionManager;->handleStartSession(Landroid/support/v7/media/MediaRouter$RouteInfo;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/cast/CastSessionManager;


# direct methods
.method constructor <init>(Lcom/google/android/music/cast/CastSessionManager;)V
    .locals 0

    .prologue
    .line 162
    iput-object p1, p0, Lcom/google/android/music/cast/CastSessionManager$2;->this$0:Lcom/google/android/music/cast/CastSessionManager;

    invoke-direct {p0}, Landroid/support/v7/media/MediaRouter$ControlRequestCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 3
    .param p1, "error"    # Ljava/lang/String;
    .param p2, "data"    # Landroid/os/Bundle;

    .prologue
    .line 185
    const-string v0, "MusicCastSession"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error encountered requesting remote start session: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p2}, Lcom/google/android/music/utils/DebugUtils;->bundleToString(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 187
    return-void
.end method

.method public onResult(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "data"    # Landroid/os/Bundle;

    .prologue
    .line 165
    iget-object v0, p0, Lcom/google/android/music/cast/CastSessionManager$2;->this$0:Lcom/google/android/music/cast/CastSessionManager;

    # invokes: Lcom/google/android/music/cast/CastSessionManager;->isLogVerbose()Z
    invoke-static {v0}, Lcom/google/android/music/cast/CastSessionManager;->access$200(Lcom/google/android/music/cast/CastSessionManager;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 166
    const-string v0, "MusicCastSession"

    const-string v1, "onResult data=%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Lcom/google/android/music/utils/DebugUtils;->bundleToString(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 170
    :cond_0
    if-eqz p1, :cond_2

    .line 171
    iget-object v0, p0, Lcom/google/android/music/cast/CastSessionManager$2;->this$0:Lcom/google/android/music/cast/CastSessionManager;

    const-string v1, "android.media.intent.extra.SESSION_ID"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/google/android/music/cast/CastSessionManager;->mSessionId:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/google/android/music/cast/CastSessionManager;->access$302(Lcom/google/android/music/cast/CastSessionManager;Ljava/lang/String;)Ljava/lang/String;

    .line 172
    iget-object v0, p0, Lcom/google/android/music/cast/CastSessionManager$2;->this$0:Lcom/google/android/music/cast/CastSessionManager;

    # getter for: Lcom/google/android/music/cast/CastSessionManager;->mSessionId:Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/music/cast/CastSessionManager;->access$300(Lcom/google/android/music/cast/CastSessionManager;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 173
    iget-object v0, p0, Lcom/google/android/music/cast/CastSessionManager$2;->this$0:Lcom/google/android/music/cast/CastSessionManager;

    # getter for: Lcom/google/android/music/cast/CastSessionManager;->mSessionManagerCallback:Lcom/google/android/music/cast/CastSessionManager$SessionManagerCallback;
    invoke-static {v0}, Lcom/google/android/music/cast/CastSessionManager;->access$500(Lcom/google/android/music/cast/CastSessionManager;)Lcom/google/android/music/cast/CastSessionManager$SessionManagerCallback;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 174
    iget-object v0, p0, Lcom/google/android/music/cast/CastSessionManager$2;->this$0:Lcom/google/android/music/cast/CastSessionManager;

    # getter for: Lcom/google/android/music/cast/CastSessionManager;->mSessionManagerCallback:Lcom/google/android/music/cast/CastSessionManager$SessionManagerCallback;
    invoke-static {v0}, Lcom/google/android/music/cast/CastSessionManager;->access$500(Lcom/google/android/music/cast/CastSessionManager;)Lcom/google/android/music/cast/CastSessionManager$SessionManagerCallback;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/cast/CastSessionManager$2;->this$0:Lcom/google/android/music/cast/CastSessionManager;

    # getter for: Lcom/google/android/music/cast/CastSessionManager;->mSessionId:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/music/cast/CastSessionManager;->access$300(Lcom/google/android/music/cast/CastSessionManager;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/music/cast/CastSessionManager$SessionManagerCallback;->onSessionCreated(Ljava/lang/String;)V

    .line 179
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/google/android/music/cast/CastSessionManager$2;->this$0:Lcom/google/android/music/cast/CastSessionManager;

    # invokes: Lcom/google/android/music/cast/CastSessionManager;->processControlRequestResultBundle(Landroid/os/Bundle;)V
    invoke-static {v0, p1}, Lcom/google/android/music/cast/CastSessionManager;->access$600(Lcom/google/android/music/cast/CastSessionManager;Landroid/os/Bundle;)V

    .line 181
    :cond_2
    return-void

    .line 177
    :cond_3
    const-string v0, "MusicCastSession"

    const-string v1, "Session started with empty session id"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
