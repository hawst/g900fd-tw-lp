.class Lcom/google/android/music/ui/AllTracksFragment$1;
.super Landroid/database/DataSetObserver;
.source "AllTracksFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/AllTracksFragment;->newAsyncCursorAdapter(Landroid/database/Cursor;)Lcom/google/android/music/ui/MediaListCursorAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/AllTracksFragment;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/AllTracksFragment;)V
    .locals 0

    .prologue
    .line 63
    iput-object p1, p0, Lcom/google/android/music/ui/AllTracksFragment$1;->this$0:Lcom/google/android/music/ui/AllTracksFragment;

    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public onChanged()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 66
    iget-object v0, p0, Lcom/google/android/music/ui/AllTracksFragment$1;->this$0:Lcom/google/android/music/ui/AllTracksFragment;

    # getter for: Lcom/google/android/music/ui/AllTracksFragment;->mAdapter:Lcom/google/android/music/ui/MediaListCursorAdapter;
    invoke-static {v0}, Lcom/google/android/music/ui/AllTracksFragment;->access$000(Lcom/google/android/music/ui/AllTracksFragment;)Lcom/google/android/music/ui/MediaListCursorAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/ui/MediaListCursorAdapter;->getCount()I

    move-result v0

    if-le v0, v2, :cond_0

    .line 68
    iget-object v0, p0, Lcom/google/android/music/ui/AllTracksFragment$1;->this$0:Lcom/google/android/music/ui/AllTracksFragment;

    # getter for: Lcom/google/android/music/ui/AllTracksFragment;->mShuffleRow:Landroid/view/View;
    invoke-static {v0}, Lcom/google/android/music/ui/AllTracksFragment;->access$100(Lcom/google/android/music/ui/AllTracksFragment;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 69
    iget-object v0, p0, Lcom/google/android/music/ui/AllTracksFragment$1;->this$0:Lcom/google/android/music/ui/AllTracksFragment;

    invoke-virtual {v0}, Lcom/google/android/music/ui/AllTracksFragment;->getTrackListView()Lcom/google/android/music/ui/BaseTrackListView;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/music/ui/BaseTrackListView;->setHeaderDividersEnabled(Z)V

    .line 71
    :cond_0
    return-void
.end method
