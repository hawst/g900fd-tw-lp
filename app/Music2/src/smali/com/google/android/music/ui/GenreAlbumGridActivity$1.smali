.class Lcom/google/android/music/ui/GenreAlbumGridActivity$1;
.super Ljava/lang/Object;
.source "GenreAlbumGridActivity.java"

# interfaces
.implements Lcom/google/android/music/utils/async/AsyncRunner;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/GenreAlbumGridActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mGenreName:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/music/ui/GenreAlbumGridActivity;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/GenreAlbumGridActivity;)V
    .locals 0

    .prologue
    .line 30
    iput-object p1, p0, Lcom/google/android/music/ui/GenreAlbumGridActivity$1;->this$0:Lcom/google/android/music/ui/GenreAlbumGridActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public backgroundTask()V
    .locals 2

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/music/ui/GenreAlbumGridActivity$1;->this$0:Lcom/google/android/music/ui/GenreAlbumGridActivity;

    # getter for: Lcom/google/android/music/ui/GenreAlbumGridActivity;->mGenreAlbumList:Lcom/google/android/music/medialist/GenreAlbumList;
    invoke-static {v0}, Lcom/google/android/music/ui/GenreAlbumGridActivity;->access$000(Lcom/google/android/music/ui/GenreAlbumGridActivity;)Lcom/google/android/music/medialist/GenreAlbumList;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/ui/GenreAlbumGridActivity$1;->this$0:Lcom/google/android/music/ui/GenreAlbumGridActivity;

    invoke-virtual {v0, v1}, Lcom/google/android/music/medialist/GenreAlbumList;->getName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/GenreAlbumGridActivity$1;->mGenreName:Ljava/lang/String;

    .line 36
    return-void
.end method

.method public taskCompleted()V
    .locals 2

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/music/ui/GenreAlbumGridActivity$1;->this$0:Lcom/google/android/music/ui/GenreAlbumGridActivity;

    iget-object v1, p0, Lcom/google/android/music/ui/GenreAlbumGridActivity$1;->mGenreName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/GenreAlbumGridActivity;->setActionBarTitle(Ljava/lang/String;)V

    .line 41
    return-void
.end method
