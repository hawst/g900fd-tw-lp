.class public Lcom/google/android/music/art/DocumentArtView;
.super Lcom/google/android/music/art/SimpleArtView;
.source "DocumentArtView.java"


# static fields
.field private static final LOGV:Z


# instance fields
.field private mArtBucket:I

.field private mArtType:Lcom/google/android/music/art/ArtType;

.field protected mDocument:Lcom/google/android/music/ui/cardlib/model/Document;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->ART_RESOLVER:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/art/DocumentArtView;->LOGV:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/google/android/music/art/SimpleArtView;-><init>(Landroid/content/Context;)V

    .line 20
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/music/art/DocumentArtView;->mArtBucket:I

    .line 24
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/art/SimpleArtView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 20
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/music/art/DocumentArtView;->mArtBucket:I

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 27
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/music/art/SimpleArtView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 20
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/music/art/DocumentArtView;->mArtBucket:I

    .line 28
    return-void
.end method

.method private updateDescriptorAndRequest()V
    .locals 5

    .prologue
    .line 54
    iget-object v1, p0, Lcom/google/android/music/art/DocumentArtView;->mDocument:Lcom/google/android/music/ui/cardlib/model/Document;

    if-nez v1, :cond_1

    .line 55
    sget-boolean v1, Lcom/google/android/music/art/DocumentArtView;->LOGV:Z

    if-eqz v1, :cond_0

    .line 56
    const-string v1, "DocumentArtView"

    const-string v2, "Skipping art request, document is null"

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    :cond_0
    :goto_0
    return-void

    .line 60
    :cond_1
    sget-boolean v1, Lcom/google/android/music/art/DocumentArtView;->LOGV:Z

    if-eqz v1, :cond_2

    .line 61
    const-string v1, "DocumentArtView"

    const-string v2, "starting art request"

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/google/android/music/art/DocumentArtView;->reset(Z)V

    .line 65
    new-instance v0, Lcom/google/android/music/art/DocumentArtDescriptor;

    iget-object v1, p0, Lcom/google/android/music/art/DocumentArtView;->mArtType:Lcom/google/android/music/art/ArtType;

    iget v2, p0, Lcom/google/android/music/art/DocumentArtView;->mArtBucket:I

    iget v3, p0, Lcom/google/android/music/art/DocumentArtView;->mAspectRatio:F

    iget-object v4, p0, Lcom/google/android/music/art/DocumentArtView;->mDocument:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/music/art/DocumentArtDescriptor;-><init>(Lcom/google/android/music/art/ArtType;IFLcom/google/android/music/ui/cardlib/model/Document;)V

    .line 67
    .local v0, "descriptor":Lcom/google/android/music/art/ArtDescriptor;
    invoke-virtual {p0, v0}, Lcom/google/android/music/art/DocumentArtView;->loadImage(Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public bind(Lcom/google/android/music/ui/cardlib/model/Document;Lcom/google/android/music/art/ArtType;)V
    .locals 3
    .param p1, "document"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p2, "artType"    # Lcom/google/android/music/art/ArtType;

    .prologue
    .line 35
    sget-boolean v0, Lcom/google/android/music/art/DocumentArtView;->LOGV:Z

    if-eqz v0, :cond_0

    .line 36
    const-string v0, "DocumentArtView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "bind document="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    :cond_0
    if-nez p2, :cond_1

    .line 43
    sget-object p2, Lcom/google/android/music/art/ArtType;->MAINSTAGE_CARD:Lcom/google/android/music/art/ArtType;

    .line 44
    const-string v0, "DocumentArtView"

    const-string v1, "bind called with null artType"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/art/DocumentArtView;->mDocument:Lcom/google/android/music/ui/cardlib/model/Document;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/music/art/DocumentArtView;->mDocument:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v0, p1}, Lcom/google/android/music/ui/cardlib/model/Document;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 47
    :cond_2
    iput-object p1, p0, Lcom/google/android/music/art/DocumentArtView;->mDocument:Lcom/google/android/music/ui/cardlib/model/Document;

    .line 48
    iput-object p2, p0, Lcom/google/android/music/art/DocumentArtView;->mArtType:Lcom/google/android/music/art/ArtType;

    .line 49
    invoke-direct {p0}, Lcom/google/android/music/art/DocumentArtView;->updateDescriptorAndRequest()V

    .line 51
    :cond_3
    return-void
.end method

.method protected onAspectRatioChanged()V
    .locals 0

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/google/android/music/art/DocumentArtView;->updateDescriptorAndRequest()V

    .line 73
    return-void
.end method

.method public setArtBucket(I)V
    .locals 0
    .param p1, "artBucket"    # I

    .prologue
    .line 80
    iput p1, p0, Lcom/google/android/music/art/DocumentArtView;->mArtBucket:I

    .line 81
    invoke-direct {p0}, Lcom/google/android/music/art/DocumentArtView;->updateDescriptorAndRequest()V

    .line 82
    return-void
.end method
