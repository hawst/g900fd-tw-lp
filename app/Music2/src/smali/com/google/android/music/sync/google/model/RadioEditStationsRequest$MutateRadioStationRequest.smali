.class public Lcom/google/android/music/sync/google/model/RadioEditStationsRequest$MutateRadioStationRequest;
.super Lcom/google/api/client/json/GenericJson;
.source "RadioEditStationsRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/sync/google/model/RadioEditStationsRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MutateRadioStationRequest"
.end annotation


# instance fields
.field public mCreateRadioStation:Lcom/google/android/music/sync/google/model/SyncableRadioStation;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "create"
    .end annotation
.end field

.field public mDeleteId:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "delete"
    .end annotation
.end field

.field public mIncludeFeed:Z
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "includeFeed"
    .end annotation
.end field

.field public mNumEntries:I
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "numEntries"
    .end annotation
.end field

.field public mUpdateRadioStation:Lcom/google/android/music/sync/google/model/SyncableRadioStation;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "update"
    .end annotation
.end field

.field public mUserFeedParams:Lcom/google/android/music/sync/google/model/RadioEditStationsRequest$UserFeedParams;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "params"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    return-void
.end method
