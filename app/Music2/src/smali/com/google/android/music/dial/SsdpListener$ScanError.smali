.class public final enum Lcom/google/android/music/dial/SsdpListener$ScanError;
.super Ljava/lang/Enum;
.source "SsdpListener.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/dial/SsdpListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ScanError"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/music/dial/SsdpListener$ScanError;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/music/dial/SsdpListener$ScanError;

.field public static final enum ERROR_RECEIVING_MESSAGE:Lcom/google/android/music/dial/SsdpListener$ScanError;

.field public static final enum ERROR_SENDING_MSEARCH:Lcom/google/android/music/dial/SsdpListener$ScanError;

.field public static final enum ERROR_SET_SO_TIMEOUT:Lcom/google/android/music/dial/SsdpListener$ScanError;

.field public static final enum RECEIVE_INTERRUPTED:Lcom/google/android/music/dial/SsdpListener$ScanError;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 12
    new-instance v0, Lcom/google/android/music/dial/SsdpListener$ScanError;

    const-string v1, "ERROR_SENDING_MSEARCH"

    invoke-direct {v0, v1, v2}, Lcom/google/android/music/dial/SsdpListener$ScanError;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/dial/SsdpListener$ScanError;->ERROR_SENDING_MSEARCH:Lcom/google/android/music/dial/SsdpListener$ScanError;

    .line 13
    new-instance v0, Lcom/google/android/music/dial/SsdpListener$ScanError;

    const-string v1, "ERROR_RECEIVING_MESSAGE"

    invoke-direct {v0, v1, v3}, Lcom/google/android/music/dial/SsdpListener$ScanError;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/dial/SsdpListener$ScanError;->ERROR_RECEIVING_MESSAGE:Lcom/google/android/music/dial/SsdpListener$ScanError;

    .line 14
    new-instance v0, Lcom/google/android/music/dial/SsdpListener$ScanError;

    const-string v1, "RECEIVE_INTERRUPTED"

    invoke-direct {v0, v1, v4}, Lcom/google/android/music/dial/SsdpListener$ScanError;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/dial/SsdpListener$ScanError;->RECEIVE_INTERRUPTED:Lcom/google/android/music/dial/SsdpListener$ScanError;

    .line 15
    new-instance v0, Lcom/google/android/music/dial/SsdpListener$ScanError;

    const-string v1, "ERROR_SET_SO_TIMEOUT"

    invoke-direct {v0, v1, v5}, Lcom/google/android/music/dial/SsdpListener$ScanError;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/dial/SsdpListener$ScanError;->ERROR_SET_SO_TIMEOUT:Lcom/google/android/music/dial/SsdpListener$ScanError;

    .line 11
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/music/dial/SsdpListener$ScanError;

    sget-object v1, Lcom/google/android/music/dial/SsdpListener$ScanError;->ERROR_SENDING_MSEARCH:Lcom/google/android/music/dial/SsdpListener$ScanError;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/music/dial/SsdpListener$ScanError;->ERROR_RECEIVING_MESSAGE:Lcom/google/android/music/dial/SsdpListener$ScanError;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/music/dial/SsdpListener$ScanError;->RECEIVE_INTERRUPTED:Lcom/google/android/music/dial/SsdpListener$ScanError;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/music/dial/SsdpListener$ScanError;->ERROR_SET_SO_TIMEOUT:Lcom/google/android/music/dial/SsdpListener$ScanError;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/music/dial/SsdpListener$ScanError;->$VALUES:[Lcom/google/android/music/dial/SsdpListener$ScanError;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 11
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/music/dial/SsdpListener$ScanError;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 11
    const-class v0, Lcom/google/android/music/dial/SsdpListener$ScanError;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/dial/SsdpListener$ScanError;

    return-object v0
.end method

.method public static values()[Lcom/google/android/music/dial/SsdpListener$ScanError;
    .locals 1

    .prologue
    .line 11
    sget-object v0, Lcom/google/android/music/dial/SsdpListener$ScanError;->$VALUES:[Lcom/google/android/music/dial/SsdpListener$ScanError;

    invoke-virtual {v0}, [Lcom/google/android/music/dial/SsdpListener$ScanError;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/music/dial/SsdpListener$ScanError;

    return-object v0
.end method
