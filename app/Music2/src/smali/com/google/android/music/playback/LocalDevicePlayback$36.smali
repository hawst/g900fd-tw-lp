.class Lcom/google/android/music/playback/LocalDevicePlayback$36;
.super Landroid/support/v7/media/MediaRouter$Callback;
.source "LocalDevicePlayback.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/playback/LocalDevicePlayback;->setMediaRouteImpl(ZLjava/lang/String;ZJ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

.field final synthetic val$autoPlay:Z

.field final synthetic val$isPrequeueEnabled:Z

.field final synthetic val$localRoute:Z

.field final synthetic val$position:J

.field final synthetic val$routeId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/music/playback/LocalDevicePlayback;Ljava/lang/String;ZZZJ)V
    .locals 0

    .prologue
    .line 5238
    iput-object p1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$36;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    iput-object p2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$36;->val$routeId:Ljava/lang/String;

    iput-boolean p3, p0, Lcom/google/android/music/playback/LocalDevicePlayback$36;->val$localRoute:Z

    iput-boolean p4, p0, Lcom/google/android/music/playback/LocalDevicePlayback$36;->val$isPrequeueEnabled:Z

    iput-boolean p5, p0, Lcom/google/android/music/playback/LocalDevicePlayback$36;->val$autoPlay:Z

    iput-wide p6, p0, Lcom/google/android/music/playback/LocalDevicePlayback$36;->val$position:J

    invoke-direct {p0}, Landroid/support/v7/media/MediaRouter$Callback;-><init>()V

    return-void
.end method


# virtual methods
.method public onRouteAdded(Landroid/support/v7/media/MediaRouter;Landroid/support/v7/media/MediaRouter$RouteInfo;)V
    .locals 6
    .param p1, "router"    # Landroid/support/v7/media/MediaRouter;
    .param p2, "route"    # Landroid/support/v7/media/MediaRouter$RouteInfo;

    .prologue
    .line 5241
    iget-object v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback$36;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->LOGV:Z
    invoke-static {v3}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$000(Lcom/google/android/music/playback/LocalDevicePlayback;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 5242
    const-string v3, "LocalDevicePlayback"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onRouteAdded: route="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 5245
    :cond_0
    invoke-virtual {p2}, Landroid/support/v7/media/MediaRouter$RouteInfo;->getId()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback$36;->val$routeId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 5246
    iget-object v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback$36;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaRouter:Landroid/support/v7/media/MediaRouter;
    invoke-static {v3}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$10400(Lcom/google/android/music/playback/LocalDevicePlayback;)Landroid/support/v7/media/MediaRouter;

    move-result-object v3

    invoke-virtual {v3, p2}, Landroid/support/v7/media/MediaRouter;->selectRoute(Landroid/support/v7/media/MediaRouter$RouteInfo;)V

    .line 5247
    iget-object v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback$36;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # setter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mConnectedMediaRoute:Landroid/support/v7/media/MediaRouter$RouteInfo;
    invoke-static {v3, p2}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$10502(Lcom/google/android/music/playback/LocalDevicePlayback;Landroid/support/v7/media/MediaRouter$RouteInfo;)Landroid/support/v7/media/MediaRouter$RouteInfo;

    .line 5248
    iget-boolean v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback$36;->val$localRoute:Z

    if-nez v3, :cond_2

    .line 5249
    iget-object v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback$36;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->addRemotePlaybackClient(Landroid/support/v7/media/MediaRouter$RouteInfo;)Z
    invoke-static {v3, p2}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$10600(Lcom/google/android/music/playback/LocalDevicePlayback;Landroid/support/v7/media/MediaRouter$RouteInfo;)Z

    .line 5250
    iget-object v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback$36;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    iget-object v3, v3, Lcom/google/android/music/playback/LocalDevicePlayback;->mService:Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v3}, Lcom/google/android/music/playback/MusicPlaybackService;->getMediaSession()Lcom/google/android/music/playback/session/MediaSessionCompat;

    move-result-object v2

    .line 5251
    .local v2, "session":Lcom/google/android/music/playback/session/MediaSessionCompat;
    if-eqz v2, :cond_1

    .line 5252
    invoke-virtual {v2}, Lcom/google/android/music/playback/session/MediaSessionCompat;->getMediaSession()Ljava/lang/Object;

    move-result-object v0

    .line 5253
    .local v0, "actualMediaSession":Ljava/lang/Object;
    invoke-virtual {v2}, Lcom/google/android/music/playback/session/MediaSessionCompat;->getRemoteControlClient()Ljava/lang/Object;

    move-result-object v1

    .line 5254
    .local v1, "actualRcc":Ljava/lang/Object;
    if-eqz v0, :cond_5

    .line 5255
    iget-object v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback$36;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaRouter:Landroid/support/v7/media/MediaRouter;
    invoke-static {v3}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$10400(Lcom/google/android/music/playback/LocalDevicePlayback;)Landroid/support/v7/media/MediaRouter;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/support/v7/media/MediaRouter;->setMediaSession(Ljava/lang/Object;)V

    .line 5260
    .end local v0    # "actualMediaSession":Ljava/lang/Object;
    .end local v1    # "actualRcc":Ljava/lang/Object;
    :cond_1
    :goto_0
    iget-object v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback$36;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->useRemotePlaybackClient()Z
    invoke-static {v3}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$100(Lcom/google/android/music/playback/LocalDevicePlayback;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 5262
    iget-object v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback$36;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mCastSessionManager:Lcom/google/android/music/cast/CastSessionManager;
    invoke-static {v3}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$10700(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/cast/CastSessionManager;

    move-result-object v3

    invoke-virtual {v3, p2}, Lcom/google/android/music/cast/CastSessionManager;->startSession(Landroid/support/v7/media/MediaRouter$RouteInfo;)V

    .line 5263
    iget-boolean v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback$36;->val$isPrequeueEnabled:Z

    if-eqz v3, :cond_6

    .line 5264
    iget-object v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback$36;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    invoke-static {p2}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->isActionEnqueueSupported(Landroid/support/v7/media/MediaRouter$RouteInfo;)Z

    move-result v4

    # setter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mPrequeueItems:Z
    invoke-static {v3, v4}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$10802(Lcom/google/android/music/playback/LocalDevicePlayback;Z)Z

    .line 5272
    .end local v2    # "session":Lcom/google/android/music/playback/session/MediaSessionCompat;
    :cond_2
    :goto_1
    iget-object v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback$36;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaRouter:Landroid/support/v7/media/MediaRouter;
    invoke-static {v3}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$10400(Lcom/google/android/music/playback/LocalDevicePlayback;)Landroid/support/v7/media/MediaRouter;

    move-result-object v3

    invoke-virtual {v3, p0}, Landroid/support/v7/media/MediaRouter;->removeCallback(Landroid/support/v7/media/MediaRouter$Callback;)V

    .line 5274
    iget-boolean v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback$36;->val$autoPlay:Z

    if-eqz v3, :cond_3

    .line 5275
    iget-object v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback$36;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    iget-wide v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback$36;->val$position:J

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->resumePlaybackAsync(J)V
    invoke-static {v3, v4, v5}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$10900(Lcom/google/android/music/playback/LocalDevicePlayback;J)V

    .line 5278
    :cond_3
    iget-object v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback$36;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->LOGV:Z
    invoke-static {v3}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$000(Lcom/google/android/music/playback/LocalDevicePlayback;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 5279
    const-string v3, "LocalDevicePlayback"

    const-string v4, "onRouteAdded: selected route"

    invoke-static {v3, v4}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 5282
    :cond_4
    return-void

    .line 5256
    .restart local v0    # "actualMediaSession":Ljava/lang/Object;
    .restart local v1    # "actualRcc":Ljava/lang/Object;
    .restart local v2    # "session":Lcom/google/android/music/playback/session/MediaSessionCompat;
    :cond_5
    if-eqz v1, :cond_1

    .line 5257
    iget-object v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback$36;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaRouter:Landroid/support/v7/media/MediaRouter;
    invoke-static {v3}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$10400(Lcom/google/android/music/playback/LocalDevicePlayback;)Landroid/support/v7/media/MediaRouter;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/support/v7/media/MediaRouter;->addRemoteControlClient(Ljava/lang/Object;)V

    goto :goto_0

    .line 5267
    .end local v0    # "actualMediaSession":Ljava/lang/Object;
    .end local v1    # "actualRcc":Ljava/lang/Object;
    :cond_6
    iget-object v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback$36;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    const/4 v4, 0x0

    # setter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mPrequeueItems:Z
    invoke-static {v3, v4}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$10802(Lcom/google/android/music/playback/LocalDevicePlayback;Z)Z

    goto :goto_1
.end method
