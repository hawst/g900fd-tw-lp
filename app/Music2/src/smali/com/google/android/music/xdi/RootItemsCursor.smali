.class Lcom/google/android/music/xdi/RootItemsCursor;
.super Lcom/google/android/music/xdi/XdiCursor;
.source "RootItemsCursor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/xdi/RootItemsCursor$1;,
        Lcom/google/android/music/xdi/RootItemsCursor$ExploreCursor;,
        Lcom/google/android/music/xdi/RootItemsCursor$InstantMixesCursor;,
        Lcom/google/android/music/xdi/RootItemsCursor$RadioCursor;,
        Lcom/google/android/music/xdi/RootItemsCursor$SearchCursor;,
        Lcom/google/android/music/xdi/RootItemsCursor$MyMusicCursor;
    }
.end annotation


# static fields
.field private static final PLAY_STORE_MUSIC_ALBUMS_BROWSE_URI:Landroid/net/Uri;

.field private static final PROJECTION_MAINSTAGE:[Ljava/lang/String;

.field private static final PROJECTION_PLAYLISTS:[Ljava/lang/String;


# instance fields
.field private final mHeaderId:J

.field private final mImageHeight:I

.field private final mImageWidth:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 27
    const/16 v0, 0xd

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "album_id"

    aput-object v1, v0, v4

    const-string v1, "album_name"

    aput-object v1, v0, v5

    const/4 v1, 0x3

    const-string v2, "playlist_id"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "playlist_name"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "playlist_type"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "album_artist_id"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "album_artist"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "radio_id"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "radio_name"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "radio_art"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "StoreAlbumId"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "artworkUrl"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/xdi/RootItemsCursor;->PROJECTION_MAINSTAGE:[Ljava/lang/String;

    .line 57
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "playlist_name"

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/music/xdi/RootItemsCursor;->PROJECTION_PLAYLISTS:[Ljava/lang/String;

    .line 66
    const-string v0, "content://com.google.android.canvas.play/music/albums/browse"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/xdi/RootItemsCursor;->PLAY_STORE_MUSIC_ALBUMS_BROWSE_URI:Landroid/net/Uri;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;[Ljava/lang/String;J)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "headerId"    # J

    .prologue
    .line 322
    invoke-static {p1, p3, p4}, Lcom/google/android/music/xdi/RootItemsCursor;->getCursorForHeader(Landroid/content/Context;J)Landroid/database/Cursor;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/music/xdi/XdiCursor;-><init>(Landroid/content/Context;[Ljava/lang/String;Landroid/database/Cursor;)V

    .line 323
    iput-wide p3, p0, Lcom/google/android/music/xdi/RootItemsCursor;->mHeaderId:J

    .line 324
    invoke-static {p1}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultItemWidthPx(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/xdi/RootItemsCursor;->mImageWidth:I

    .line 325
    invoke-static {p1}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultItemHeightPx(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/xdi/RootItemsCursor;->mImageHeight:I

    .line 326
    return-void
.end method

.method private extractDataForMainstage([Ljava/lang/Object;)V
    .locals 25
    .param p1, "values"    # [Ljava/lang/Object;

    .prologue
    .line 391
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/xdi/RootItemsCursor;->getWrappedCursor()Landroid/database/Cursor;

    move-result-object v8

    .line 392
    .local v8, "cursor":Landroid/database/Cursor;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/xdi/RootItemsCursor;->getContext()Landroid/content/Context;

    move-result-object v7

    .line 396
    .local v7, "context":Landroid/content/Context;
    invoke-interface {v8}, Landroid/database/Cursor;->getPosition()I

    move-result v19

    const/16 v22, 0x2

    move/from16 v0, v19

    move/from16 v1, v22

    if-ge v0, v1, :cond_0

    .line 397
    move-object/from16 v0, p1

    invoke-static {v8, v0}, Lcom/google/android/music/xdi/XdiUtils;->extractDataFromItemCursor(Landroid/database/Cursor;[Ljava/lang/Object;)V

    .line 501
    :goto_0
    return-void

    .line 401
    :cond_0
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 402
    .local v10, "id":J
    const/4 v6, 0x0

    .line 403
    .local v6, "artUri":Ljava/lang/String;
    const/16 v17, 0x0

    .line 404
    .local v17, "name":Ljava/lang/String;
    const/4 v9, 0x0

    .line 406
    .local v9, "description":Ljava/lang/String;
    const/4 v12, 0x0

    .line 408
    .local v12, "intent":Landroid/content/Intent;
    const/16 v19, 0x1

    move/from16 v0, v19

    invoke-interface {v8, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v19

    if-nez v19, :cond_2

    .line 409
    const/16 v19, 0x1

    move/from16 v0, v19

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 410
    .local v4, "albumId":J
    const/16 v19, 0x2

    move/from16 v0, v19

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 411
    const/16 v19, 0x7

    move/from16 v0, v19

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 412
    const/16 v19, 0x1

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/music/xdi/RootItemsCursor;->mImageWidth:I

    move/from16 v22, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/music/xdi/RootItemsCursor;->mImageHeight:I

    move/from16 v23, v0

    move/from16 v0, v19

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-static {v4, v5, v0, v1, v2}, Lcom/google/android/music/store/MusicContent$AlbumArt;->getAlbumArtUri(JZII)Landroid/net/Uri;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    .line 414
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v19

    if-eqz v19, :cond_1

    .line 415
    invoke-static {v7}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultAlbumArtUri(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    .line 417
    :cond_1
    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Lcom/google/android/music/xdi/XdiUtils;->getAlbumDetailsIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v12

    .line 494
    .end local v4    # "albumId":J
    :goto_1
    const-string v19, "_id"

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v22

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v19

    move-object/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/music/xdi/RootItemsCursor;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 495
    const-string v19, "display_name"

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v19

    move-object/from16 v3, v17

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/music/xdi/RootItemsCursor;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 496
    const-string v19, "display_description"

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2, v9}, Lcom/google/android/music/xdi/RootItemsCursor;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 497
    const-string v19, "image_uri"

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2, v6}, Lcom/google/android/music/xdi/RootItemsCursor;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 498
    const-string v19, "width"

    const/16 v22, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v19

    move-object/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/music/xdi/RootItemsCursor;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 499
    const-string v19, "height"

    const/16 v22, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v19

    move-object/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/music/xdi/RootItemsCursor;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 500
    const-string v19, "intent_uri"

    const/16 v22, 0x1

    move/from16 v0, v22

    invoke-virtual {v12, v0}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v19

    move-object/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/music/xdi/RootItemsCursor;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 418
    :cond_2
    const/16 v19, 0x3

    move/from16 v0, v19

    invoke-interface {v8, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v19

    if-nez v19, :cond_4

    .line 419
    const/16 v19, 0x3

    move/from16 v0, v19

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 420
    .local v14, "listId":J
    const/16 v19, 0x4

    move/from16 v0, v19

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 421
    .local v13, "listName":Ljava/lang/String;
    const/16 v19, 0x5

    move/from16 v0, v19

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v16

    .line 422
    .local v16, "listType":I
    move/from16 v0, v16

    invoke-static {v7, v13, v0}, Lcom/google/android/music/utils/LabelUtils;->getPlaylistPrimaryLabel(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v17

    .line 423
    move-object/from16 v0, p0

    invoke-static {v7, v0}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v18

    .line 426
    .local v18, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    :try_start_0
    move-object/from16 v0, v18

    move/from16 v1, v16

    invoke-static {v7, v0, v1}, Lcom/google/android/music/utils/LabelUtils;->getPlaylistSecondaryLabel(Landroid/content/Context;Lcom/google/android/music/preferences/MusicPreferences;I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v9

    .line 428
    invoke-static/range {p0 .. p0}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    .line 431
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/music/xdi/RootItemsCursor;->mImageWidth:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/music/xdi/RootItemsCursor;->mImageHeight:I

    move/from16 v22, v0

    move/from16 v0, v19

    move/from16 v1, v22

    invoke-static {v14, v15, v0, v1}, Lcom/google/android/music/store/MusicContent$PlaylistArt;->getPlaylistArtUri(JII)Landroid/net/Uri;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    .line 433
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v19

    if-eqz v19, :cond_3

    .line 434
    invoke-static {v7}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultPlaylistArtUri(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    .line 437
    :cond_3
    sparse-switch v16, :sswitch_data_0

    .line 455
    new-instance v12, Landroid/content/Intent;

    .end local v12    # "intent":Landroid/content/Intent;
    const-string v19, "com.google.android.xdi.action.DETAIL"

    sget-object v22, Lcom/google/android/music/xdi/XdiContentProvider;->BASE_URI:Landroid/net/Uri;

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "details/playlists/"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v22

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-direct {v12, v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .restart local v12    # "intent":Landroid/content/Intent;
    goto/16 :goto_1

    .line 428
    :catchall_0
    move-exception v19

    invoke-static/range {p0 .. p0}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    throw v19

    .line 439
    :sswitch_0
    invoke-static {}, Lcom/google/android/music/xdi/XdiUtils;->newPanoPlayIntent()Landroid/content/Intent;

    move-result-object v19

    const-string v22, "container"

    const/16 v23, 0x4

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v19

    const-string v22, "name"

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    move-object/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v19

    const-string v22, "id"

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-virtual {v0, v1, v14, v15}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v12

    .line 444
    goto/16 :goto_1

    .line 447
    :sswitch_1
    invoke-static {}, Lcom/google/android/music/xdi/XdiUtils;->newPanoPlayIntent()Landroid/content/Intent;

    move-result-object v19

    const-string v22, "container"

    const/16 v23, 0x3

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v19

    const-string v22, "name"

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    move-object/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v19

    const-string v22, "id"

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-virtual {v0, v1, v14, v15}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v12

    .line 452
    goto/16 :goto_1

    .line 459
    .end local v13    # "listName":Ljava/lang/String;
    .end local v14    # "listId":J
    .end local v16    # "listType":I
    .end local v18    # "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    :cond_4
    const/16 v19, 0x8

    move/from16 v0, v19

    invoke-interface {v8, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v19

    if-nez v19, :cond_6

    .line 460
    const/16 v19, 0x8

    move/from16 v0, v19

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v20

    .line 461
    .local v20, "radioId":J
    const/16 v19, 0x9

    move/from16 v0, v19

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 462
    const v19, 0x7f0b0264

    move/from16 v0, v19

    invoke-virtual {v7, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 463
    const/16 v19, 0xa

    move/from16 v0, v19

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 464
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v19

    if-eqz v19, :cond_5

    .line 465
    invoke-static {v7}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultRadioArtUri(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    .line 468
    :cond_5
    invoke-static {}, Lcom/google/android/music/xdi/XdiUtils;->newPanoPlayIntent()Landroid/content/Intent;

    move-result-object v19

    const-string v22, "container"

    const/16 v23, 0x4

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v19

    const-string v22, "id"

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    move-wide/from16 v2, v20

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v19

    const-string v22, "seed_type"

    sget-object v23, Lcom/google/android/music/mix/MixDescriptor$Type;->RADIO:Lcom/google/android/music/mix/MixDescriptor$Type;

    invoke-virtual/range {v23 .. v23}, Lcom/google/android/music/mix/MixDescriptor$Type;->ordinal()I

    move-result v23

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v19

    const-string v22, "art_uri"

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v19

    const-string v22, "name"

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    move-object/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v12

    .line 474
    goto/16 :goto_1

    .end local v20    # "radioId":J
    :cond_6
    const/16 v19, 0xb

    move/from16 v0, v19

    invoke-interface {v8, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v19

    if-nez v19, :cond_9

    .line 477
    const/16 v19, 0xb

    move/from16 v0, v19

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 478
    .local v4, "albumId":Ljava/lang/String;
    const/16 v19, 0x2

    move/from16 v0, v19

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 479
    const/16 v19, 0x7

    move/from16 v0, v19

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 480
    const/16 v19, 0xc

    move/from16 v0, v19

    invoke-interface {v8, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v19

    if-nez v19, :cond_7

    .line 481
    const/16 v19, 0xc

    move/from16 v0, v19

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 483
    :cond_7
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v19

    if-eqz v19, :cond_8

    .line 484
    invoke-static {v7}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultAlbumArtUri(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    .line 487
    :cond_8
    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Lcom/google/android/music/xdi/XdiUtils;->getAlbumDetailsIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v12

    .line 488
    goto/16 :goto_1

    .line 489
    .end local v4    # "albumId":Ljava/lang/String;
    :cond_9
    const-string v19, "MusicXdi"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "Unexpected mainstage item: "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-static {v8}, Landroid/database/DatabaseUtils;->dumpCursorToString(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 437
    :sswitch_data_0
    .sparse-switch
        0x32 -> :sswitch_1
        0x3c -> :sswitch_0
    .end sparse-switch
.end method

.method private extractDataForMyMusic([Ljava/lang/Object;)V
    .locals 4
    .param p1, "values"    # [Ljava/lang/Object;

    .prologue
    .line 504
    invoke-virtual {p0}, Lcom/google/android/music/xdi/RootItemsCursor;->getWrappedCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 506
    .local v0, "cursor":Landroid/database/Cursor;
    const-string v1, "_id"

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {p0, p1, v1, v2}, Lcom/google/android/music/xdi/RootItemsCursor;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 508
    const-string v1, "display_name"

    const/4 v2, 0x2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, p1, v1, v2}, Lcom/google/android/music/xdi/RootItemsCursor;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 510
    const-string v1, "display_description"

    const/4 v2, 0x3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, p1, v1, v2}, Lcom/google/android/music/xdi/RootItemsCursor;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 512
    const-string v1, "image_uri"

    const/4 v2, 0x4

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, p1, v1, v2}, Lcom/google/android/music/xdi/RootItemsCursor;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 514
    const-string v1, "width"

    const/4 v2, 0x5

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p0, p1, v1, v2}, Lcom/google/android/music/xdi/RootItemsCursor;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 516
    const-string v1, "height"

    const/4 v2, 0x6

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p0, p1, v1, v2}, Lcom/google/android/music/xdi/RootItemsCursor;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 518
    const-string v1, "intent_uri"

    const/4 v2, 0x7

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, p1, v1, v2}, Lcom/google/android/music/xdi/RootItemsCursor;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 520
    return-void
.end method

.method private extractDataForPlaylists([Ljava/lang/Object;)V
    .locals 10
    .param p1, "values"    # [Ljava/lang/Object;

    .prologue
    const/4 v9, 0x1

    .line 523
    invoke-virtual {p0}, Lcom/google/android/music/xdi/RootItemsCursor;->getWrappedCursor()Landroid/database/Cursor;

    move-result-object v1

    .line 525
    .local v1, "cursor":Landroid/database/Cursor;
    const/4 v6, 0x0

    invoke-interface {v1, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 526
    .local v2, "id":J
    invoke-interface {v1, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 527
    .local v5, "name":Ljava/lang/String;
    iget v6, p0, Lcom/google/android/music/xdi/RootItemsCursor;->mImageWidth:I

    iget v7, p0, Lcom/google/android/music/xdi/RootItemsCursor;->mImageHeight:I

    invoke-static {v2, v3, v6, v7}, Lcom/google/android/music/store/MusicContent$PlaylistArt;->getPlaylistArtUri(JII)Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v6}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 530
    .local v0, "artUri":Ljava/lang/String;
    invoke-static {v2, v3}, Lcom/google/android/music/xdi/XdiUtils;->getPlaylistDetailsIntent(J)Landroid/content/Intent;

    move-result-object v4

    .line 532
    .local v4, "intent":Landroid/content/Intent;
    const-string v6, "_id"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {p0, p1, v6, v7}, Lcom/google/android/music/xdi/RootItemsCursor;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 533
    const-string v6, "display_name"

    invoke-virtual {p0, p1, v6, v5}, Lcom/google/android/music/xdi/RootItemsCursor;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 534
    const-string v6, "display_description"

    invoke-virtual {p0}, Lcom/google/android/music/xdi/RootItemsCursor;->getContext()Landroid/content/Context;

    move-result-object v7

    const v8, 0x7f0b00fd

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, p1, v6, v7}, Lcom/google/android/music/xdi/RootItemsCursor;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 536
    const-string v6, "image_uri"

    invoke-virtual {p0, p1, v6, v0}, Lcom/google/android/music/xdi/RootItemsCursor;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 537
    const-string v6, "intent_uri"

    invoke-virtual {v4, v9}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, p1, v6, v7}, Lcom/google/android/music/xdi/RootItemsCursor;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 538
    return-void
.end method

.method private static getCursorForHeader(Landroid/content/Context;J)Landroid/database/Cursor;
    .locals 11
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "headerId"    # J

    .prologue
    const/4 v3, 0x0

    .line 329
    long-to-int v0, p1

    packed-switch v0, :pswitch_data_0

    .line 353
    :pswitch_0
    const-string v0, "MusicXdi"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected header id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    .line 354
    new-instance v0, Landroid/database/MatrixCursor;

    invoke-direct {v0, v3}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    :goto_0
    return-object v0

    .line 331
    :pswitch_1
    new-instance v9, Lcom/google/android/music/xdi/NowPlayingCursor;

    invoke-direct {v9, p0}, Lcom/google/android/music/xdi/NowPlayingCursor;-><init>(Landroid/content/Context;)V

    .line 332
    .local v9, "nowPlayingCursor":Landroid/database/Cursor;
    new-instance v7, Lcom/google/android/music/xdi/IAmFeelingLuckyRadioCursor;

    invoke-direct {v7, p0}, Lcom/google/android/music/xdi/IAmFeelingLuckyRadioCursor;-><init>(Landroid/content/Context;)V

    .line 333
    .local v7, "iflCursor":Landroid/database/Cursor;
    sget-object v1, Lcom/google/android/music/store/MusicContent$Mainstage;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/google/android/music/xdi/RootItemsCursor;->PROJECTION_MAINSTAGE:[Ljava/lang/String;

    move-object v0, p0

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 336
    .local v8, "mainstageCursor":Landroid/database/Cursor;
    const/4 v0, 0x3

    new-array v6, v0, [Landroid/database/Cursor;

    const/4 v0, 0x0

    aput-object v9, v6, v0

    const/4 v0, 0x1

    aput-object v7, v6, v0

    const/4 v0, 0x2

    aput-object v8, v6, v0

    .line 337
    .local v6, "cursors":[Landroid/database/Cursor;
    new-instance v0, Landroid/database/MergeCursor;

    invoke-direct {v0, v6}, Landroid/database/MergeCursor;-><init>([Landroid/database/Cursor;)V

    goto :goto_0

    .line 339
    .end local v6    # "cursors":[Landroid/database/Cursor;
    .end local v7    # "iflCursor":Landroid/database/Cursor;
    .end local v8    # "mainstageCursor":Landroid/database/Cursor;
    .end local v9    # "nowPlayingCursor":Landroid/database/Cursor;
    :pswitch_2
    new-instance v0, Lcom/google/android/music/xdi/RootItemsCursor$MyMusicCursor;

    invoke-direct {v0, p0, v3}, Lcom/google/android/music/xdi/RootItemsCursor$MyMusicCursor;-><init>(Landroid/content/Context;Lcom/google/android/music/xdi/RootItemsCursor$1;)V

    goto :goto_0

    .line 341
    :pswitch_3
    sget-object v1, Lcom/google/android/music/store/MusicContent$Playlists;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/google/android/music/xdi/RootItemsCursor;->PROJECTION_PLAYLISTS:[Ljava/lang/String;

    move-object v0, p0

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0

    .line 345
    :pswitch_4
    new-instance v0, Lcom/google/android/music/xdi/RootItemsCursor$RadioCursor;

    invoke-direct {v0, p0, v3}, Lcom/google/android/music/xdi/RootItemsCursor$RadioCursor;-><init>(Landroid/content/Context;Lcom/google/android/music/xdi/RootItemsCursor$1;)V

    goto :goto_0

    .line 347
    :pswitch_5
    new-instance v0, Lcom/google/android/music/xdi/RootItemsCursor$InstantMixesCursor;

    invoke-direct {v0, p0, v3}, Lcom/google/android/music/xdi/RootItemsCursor$InstantMixesCursor;-><init>(Landroid/content/Context;Lcom/google/android/music/xdi/RootItemsCursor$1;)V

    goto :goto_0

    .line 349
    :pswitch_6
    new-instance v0, Lcom/google/android/music/xdi/RootItemsCursor$SearchCursor;

    invoke-direct {v0, p0, v3}, Lcom/google/android/music/xdi/RootItemsCursor$SearchCursor;-><init>(Landroid/content/Context;Lcom/google/android/music/xdi/RootItemsCursor$1;)V

    goto :goto_0

    .line 351
    :pswitch_7
    new-instance v0, Lcom/google/android/music/xdi/RootItemsCursor$ExploreCursor;

    invoke-direct {v0, p0, v3}, Lcom/google/android/music/xdi/RootItemsCursor$ExploreCursor;-><init>(Landroid/content/Context;Lcom/google/android/music/xdi/RootItemsCursor$1;)V

    goto :goto_0

    .line 329
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_6
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_7
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method


# virtual methods
.method protected extractDataForCurrentRow([Ljava/lang/Object;)Z
    .locals 4
    .param p1, "values"    # [Ljava/lang/Object;

    .prologue
    .line 360
    iget-wide v0, p0, Lcom/google/android/music/xdi/RootItemsCursor;->mHeaderId:J

    long-to-int v0, v0

    packed-switch v0, :pswitch_data_0

    .line 383
    :pswitch_0
    const-string v0, "MusicXdi"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected header id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/music/xdi/RootItemsCursor;->mHeaderId:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    .line 384
    const/4 v0, 0x0

    .line 387
    :goto_0
    return v0

    .line 362
    :pswitch_1
    invoke-direct {p0, p1}, Lcom/google/android/music/xdi/RootItemsCursor;->extractDataForMainstage([Ljava/lang/Object;)V

    .line 387
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 365
    :pswitch_2
    invoke-direct {p0, p1}, Lcom/google/android/music/xdi/RootItemsCursor;->extractDataForMyMusic([Ljava/lang/Object;)V

    goto :goto_1

    .line 368
    :pswitch_3
    invoke-direct {p0, p1}, Lcom/google/android/music/xdi/RootItemsCursor;->extractDataForPlaylists([Ljava/lang/Object;)V

    goto :goto_1

    .line 371
    :pswitch_4
    invoke-virtual {p0}, Lcom/google/android/music/xdi/RootItemsCursor;->getWrappedCursor()Landroid/database/Cursor;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/music/xdi/XdiUtils;->extractDataFromItemCursor(Landroid/database/Cursor;[Ljava/lang/Object;)V

    goto :goto_1

    .line 374
    :pswitch_5
    invoke-virtual {p0}, Lcom/google/android/music/xdi/RootItemsCursor;->getWrappedCursor()Landroid/database/Cursor;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/music/xdi/XdiUtils;->extractDataFromItemCursor(Landroid/database/Cursor;[Ljava/lang/Object;)V

    goto :goto_1

    .line 377
    :pswitch_6
    invoke-virtual {p0}, Lcom/google/android/music/xdi/RootItemsCursor;->getWrappedCursor()Landroid/database/Cursor;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/music/xdi/XdiUtils;->extractDataFromItemCursor(Landroid/database/Cursor;[Ljava/lang/Object;)V

    goto :goto_1

    .line 380
    :pswitch_7
    invoke-virtual {p0}, Lcom/google/android/music/xdi/RootItemsCursor;->getWrappedCursor()Landroid/database/Cursor;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/music/xdi/XdiUtils;->extractDataFromItemCursor(Landroid/database/Cursor;[Ljava/lang/Object;)V

    goto :goto_1

    .line 360
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_6
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_7
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method
