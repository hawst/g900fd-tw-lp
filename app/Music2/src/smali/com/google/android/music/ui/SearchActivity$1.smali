.class Lcom/google/android/music/ui/SearchActivity$1;
.super Ljava/lang/Object;
.source "SearchActivity.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/SearchActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/SearchActivity;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/SearchActivity;)V
    .locals 0

    .prologue
    .line 105
    iput-object p1, p0, Lcom/google/android/music/ui/SearchActivity$1;->this$0:Lcom/google/android/music/ui/SearchActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 105
    invoke-virtual {p0}, Lcom/google/android/music/ui/SearchActivity$1;->call()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public call()Ljava/lang/Void;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 109
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 110
    # getter for: Lcom/google/android/music/ui/SearchActivity;->LOGV:Z
    invoke-static {}, Lcom/google/android/music/ui/SearchActivity;->access$000()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 111
    const-string v0, "SearchActivity"

    const-string v1, "Search was interrupted early. Return."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 117
    :cond_0
    :goto_0
    return-object v2

    .line 115
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/ui/SearchActivity$1;->this$0:Lcom/google/android/music/ui/SearchActivity;

    # invokes: Lcom/google/android/music/ui/SearchActivity;->handleSearchResult()V
    invoke-static {v0}, Lcom/google/android/music/ui/SearchActivity;->access$100(Lcom/google/android/music/ui/SearchActivity;)V

    goto :goto_0
.end method
