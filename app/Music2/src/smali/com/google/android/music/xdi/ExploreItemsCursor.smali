.class Lcom/google/android/music/xdi/ExploreItemsCursor;
.super Lcom/google/android/music/xdi/XdiCursor;
.source "ExploreItemsCursor.java"


# instance fields
.field private final mGenreId:Ljava/lang/String;

.field private final mHeaderId:J


# direct methods
.method constructor <init>(Landroid/content/Context;[Ljava/lang/String;JLjava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "headerId"    # J
    .param p5, "genreId"    # Ljava/lang/String;

    .prologue
    .line 32
    invoke-static {p1, p3, p4, p5}, Lcom/google/android/music/xdi/ExploreItemsCursor;->getCursorForHeader(Landroid/content/Context;JLjava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/music/xdi/XdiCursor;-><init>(Landroid/content/Context;[Ljava/lang/String;Landroid/database/Cursor;)V

    .line 33
    iput-wide p3, p0, Lcom/google/android/music/xdi/ExploreItemsCursor;->mHeaderId:J

    .line 34
    iput-object p5, p0, Lcom/google/android/music/xdi/ExploreItemsCursor;->mGenreId:Ljava/lang/String;

    .line 35
    return-void
.end method

.method private extractDataForFeaturedContent([Ljava/lang/Object;)V
    .locals 14
    .param p1, "values"    # [Ljava/lang/Object;

    .prologue
    const/4 v13, 0x0

    const/4 v12, 0x1

    .line 88
    invoke-virtual {p0}, Lcom/google/android/music/xdi/ExploreItemsCursor;->getWrappedCursor()Landroid/database/Cursor;

    move-result-object v1

    .line 89
    .local v1, "cursor":Landroid/database/Cursor;
    invoke-virtual {p0}, Lcom/google/android/music/xdi/ExploreItemsCursor;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 90
    .local v0, "context":Landroid/content/Context;
    invoke-interface {v1}, Landroid/database/Cursor;->getPosition()I

    move-result v7

    .line 91
    .local v7, "position":I
    int-to-long v4, v7

    .line 92
    .local v4, "id":J
    invoke-interface {v1, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 93
    .local v8, "title":Ljava/lang/String;
    const/4 v9, 0x2

    invoke-interface {v1, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 95
    .local v2, "description":Ljava/lang/String;
    const-string v6, "explore/featured"

    .line 96
    .local v6, "path":Ljava/lang/String;
    iget-object v9, p0, Lcom/google/android/music/xdi/ExploreItemsCursor;->mGenreId:Ljava/lang/String;

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_0

    .line 97
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "/"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/google/android/music/xdi/ExploreItemsCursor;->mGenreId:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 99
    :cond_0
    sget-object v9, Lcom/google/android/music/xdi/XdiContentProvider;->BASE_URI:Landroid/net/Uri;

    invoke-virtual {v9}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v9

    invoke-virtual {v9, v6}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v9

    invoke-virtual {v9}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v9

    invoke-static {v9, v7}, Lcom/google/android/music/xdi/XdiContract;->getBrowseIntent(Landroid/net/Uri;I)Landroid/content/Intent;

    move-result-object v3

    .line 101
    .local v3, "intent":Landroid/content/Intent;
    const-string v9, "meta_uri"

    const-wide/16 v10, 0x4

    invoke-static {v10, v11}, Lcom/google/android/music/xdi/XdiUtils;->getMetaUri(J)Landroid/net/Uri;

    move-result-object v10

    invoke-virtual {v10}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v3, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 104
    const-string v9, "_id"

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {p0, p1, v9, v10}, Lcom/google/android/music/xdi/ExploreItemsCursor;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 105
    const-string v9, "display_name"

    invoke-virtual {p0, p1, v9, v8}, Lcom/google/android/music/xdi/ExploreItemsCursor;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 106
    const-string v9, "display_description"

    invoke-virtual {p0, p1, v9, v2}, Lcom/google/android/music/xdi/ExploreItemsCursor;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 108
    const-string v9, "image_uri"

    invoke-virtual {p0}, Lcom/google/android/music/xdi/ExploreItemsCursor;->getContext()Landroid/content/Context;

    move-result-object v10

    invoke-static {v10}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultArtUri(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, p1, v9, v10}, Lcom/google/android/music/xdi/ExploreItemsCursor;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 109
    const-string v9, "width"

    invoke-virtual {p0, p1, v9, v13}, Lcom/google/android/music/xdi/ExploreItemsCursor;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 110
    const-string v9, "height"

    invoke-virtual {p0, p1, v9, v13}, Lcom/google/android/music/xdi/ExploreItemsCursor;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 111
    const-string v9, "intent_uri"

    invoke-virtual {v3, v12}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, p1, v9, v10}, Lcom/google/android/music/xdi/ExploreItemsCursor;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 112
    return-void
.end method

.method private extractDataForGenres([Ljava/lang/Object;)V
    .locals 14
    .param p1, "values"    # [Ljava/lang/Object;

    .prologue
    const/4 v13, 0x1

    const/4 v12, 0x0

    .line 167
    invoke-virtual {p0}, Lcom/google/android/music/xdi/ExploreItemsCursor;->getWrappedCursor()Landroid/database/Cursor;

    move-result-object v1

    .line 168
    .local v1, "cursor":Landroid/database/Cursor;
    invoke-virtual {p0}, Lcom/google/android/music/xdi/ExploreItemsCursor;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 169
    .local v0, "context":Landroid/content/Context;
    invoke-interface {v1}, Landroid/database/Cursor;->getPosition()I

    move-result v7

    .line 170
    .local v7, "position":I
    invoke-interface {v1}, Landroid/database/Cursor;->getPosition()I

    move-result v8

    int-to-long v4, v8

    .line 171
    .local v4, "id":J
    invoke-interface {v1, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 172
    .local v2, "genreId":Ljava/lang/String;
    const/4 v8, 0x2

    invoke-interface {v1, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 174
    .local v6, "name":Ljava/lang/String;
    sget-object v8, Lcom/google/android/music/xdi/XdiContentProvider;->BASE_URI:Landroid/net/Uri;

    invoke-virtual {v8}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v8

    const-string v9, "explore/genres"

    invoke-virtual {v8, v9}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v8

    invoke-virtual {v8, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v8

    invoke-virtual {v8, v6}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v8

    invoke-virtual {v8}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v8

    invoke-static {v8, v7}, Lcom/google/android/music/xdi/XdiContract;->getBrowseIntent(Landroid/net/Uri;I)Landroid/content/Intent;

    move-result-object v3

    .line 178
    .local v3, "intent":Landroid/content/Intent;
    const-string v8, "meta_uri"

    const-wide/16 v10, 0x7

    invoke-static {v10, v11}, Lcom/google/android/music/xdi/XdiUtils;->getMetaUri(J)Landroid/net/Uri;

    move-result-object v9

    invoke-virtual {v9}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 181
    const-string v8, "_id"

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {p0, p1, v8, v9}, Lcom/google/android/music/xdi/ExploreItemsCursor;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 182
    const-string v8, "display_name"

    invoke-virtual {p0, p1, v8, v6}, Lcom/google/android/music/xdi/ExploreItemsCursor;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 183
    const-string v8, "display_description"

    invoke-virtual {p0, p1, v8, v12}, Lcom/google/android/music/xdi/ExploreItemsCursor;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 184
    const-string v8, "image_uri"

    invoke-static {v0, v2, v12}, Lcom/google/android/music/xdi/XdiUtils;->getGenreArtUri(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, p1, v8, v9}, Lcom/google/android/music/xdi/ExploreItemsCursor;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 186
    const-string v8, "width"

    invoke-virtual {p0, p1, v8, v12}, Lcom/google/android/music/xdi/ExploreItemsCursor;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 187
    const-string v8, "height"

    invoke-virtual {p0, p1, v8, v12}, Lcom/google/android/music/xdi/ExploreItemsCursor;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 188
    const-string v8, "intent_uri"

    invoke-virtual {v3, v13}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, p1, v8, v9}, Lcom/google/android/music/xdi/ExploreItemsCursor;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 189
    return-void
.end method

.method private extractDataForNewReleases([Ljava/lang/Object;)V
    .locals 14
    .param p1, "values"    # [Ljava/lang/Object;

    .prologue
    const/4 v13, 0x0

    const/4 v12, 0x1

    .line 140
    invoke-virtual {p0}, Lcom/google/android/music/xdi/ExploreItemsCursor;->getWrappedCursor()Landroid/database/Cursor;

    move-result-object v1

    .line 141
    .local v1, "cursor":Landroid/database/Cursor;
    invoke-virtual {p0}, Lcom/google/android/music/xdi/ExploreItemsCursor;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 142
    .local v0, "context":Landroid/content/Context;
    invoke-interface {v1}, Landroid/database/Cursor;->getPosition()I

    move-result v7

    .line 143
    .local v7, "position":I
    int-to-long v4, v7

    .line 144
    .local v4, "id":J
    invoke-interface {v1, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 145
    .local v8, "title":Ljava/lang/String;
    const/4 v9, 0x2

    invoke-interface {v1, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 147
    .local v2, "description":Ljava/lang/String;
    const-string v6, "explore/newreleases"

    .line 148
    .local v6, "path":Ljava/lang/String;
    iget-object v9, p0, Lcom/google/android/music/xdi/ExploreItemsCursor;->mGenreId:Ljava/lang/String;

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_0

    .line 149
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "/"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/google/android/music/xdi/ExploreItemsCursor;->mGenreId:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 151
    :cond_0
    sget-object v9, Lcom/google/android/music/xdi/XdiContentProvider;->BASE_URI:Landroid/net/Uri;

    invoke-virtual {v9}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v9

    invoke-virtual {v9, v6}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v9

    invoke-virtual {v9}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v9

    invoke-static {v9, v7}, Lcom/google/android/music/xdi/XdiContract;->getBrowseIntent(Landroid/net/Uri;I)Landroid/content/Intent;

    move-result-object v3

    .line 153
    .local v3, "intent":Landroid/content/Intent;
    const-string v9, "meta_uri"

    const-wide/16 v10, 0x6

    invoke-static {v10, v11}, Lcom/google/android/music/xdi/XdiUtils;->getMetaUri(J)Landroid/net/Uri;

    move-result-object v10

    invoke-virtual {v10}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v3, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 156
    const-string v9, "_id"

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {p0, p1, v9, v10}, Lcom/google/android/music/xdi/ExploreItemsCursor;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 157
    const-string v9, "display_name"

    invoke-virtual {p0, p1, v9, v8}, Lcom/google/android/music/xdi/ExploreItemsCursor;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 158
    const-string v9, "display_description"

    invoke-virtual {p0, p1, v9, v2}, Lcom/google/android/music/xdi/ExploreItemsCursor;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 160
    const-string v9, "image_uri"

    invoke-virtual {p0}, Lcom/google/android/music/xdi/ExploreItemsCursor;->getContext()Landroid/content/Context;

    move-result-object v10

    invoke-static {v10}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultArtUri(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, p1, v9, v10}, Lcom/google/android/music/xdi/ExploreItemsCursor;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 161
    const-string v9, "width"

    invoke-virtual {p0, p1, v9, v13}, Lcom/google/android/music/xdi/ExploreItemsCursor;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 162
    const-string v9, "height"

    invoke-virtual {p0, p1, v9, v13}, Lcom/google/android/music/xdi/ExploreItemsCursor;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 163
    const-string v9, "intent_uri"

    invoke-virtual {v3, v12}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, p1, v9, v10}, Lcom/google/android/music/xdi/ExploreItemsCursor;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 164
    return-void
.end method

.method private extractDataForRecommendations([Ljava/lang/Object;)V
    .locals 14
    .param p1, "values"    # [Ljava/lang/Object;

    .prologue
    const/4 v13, 0x0

    const/4 v12, 0x1

    .line 115
    invoke-virtual {p0}, Lcom/google/android/music/xdi/ExploreItemsCursor;->getWrappedCursor()Landroid/database/Cursor;

    move-result-object v1

    .line 116
    .local v1, "cursor":Landroid/database/Cursor;
    invoke-virtual {p0}, Lcom/google/android/music/xdi/ExploreItemsCursor;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 117
    .local v0, "context":Landroid/content/Context;
    invoke-interface {v1}, Landroid/database/Cursor;->getPosition()I

    move-result v6

    .line 118
    .local v6, "position":I
    int-to-long v4, v6

    .line 119
    .local v4, "id":J
    invoke-interface {v1, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 120
    .local v7, "title":Ljava/lang/String;
    const/4 v8, 0x2

    invoke-interface {v1, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 122
    .local v2, "description":Ljava/lang/String;
    sget-object v8, Lcom/google/android/music/xdi/XdiContentProvider;->BASE_URI:Landroid/net/Uri;

    invoke-virtual {v8}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v8

    const-string v9, "explore/recommendations"

    invoke-virtual {v8, v9}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v8

    invoke-virtual {v8}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v8

    invoke-static {v8, v6}, Lcom/google/android/music/xdi/XdiContract;->getBrowseIntent(Landroid/net/Uri;I)Landroid/content/Intent;

    move-result-object v3

    .line 126
    .local v3, "intent":Landroid/content/Intent;
    const-string v8, "meta_uri"

    const-wide/16 v10, 0x5

    invoke-static {v10, v11}, Lcom/google/android/music/xdi/XdiUtils;->getMetaUri(J)Landroid/net/Uri;

    move-result-object v9

    invoke-virtual {v9}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 129
    const-string v8, "_id"

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {p0, p1, v8, v9}, Lcom/google/android/music/xdi/ExploreItemsCursor;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 130
    const-string v8, "display_name"

    invoke-virtual {p0, p1, v8, v7}, Lcom/google/android/music/xdi/ExploreItemsCursor;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 131
    const-string v8, "display_description"

    invoke-virtual {p0, p1, v8, v2}, Lcom/google/android/music/xdi/ExploreItemsCursor;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 133
    const-string v8, "image_uri"

    invoke-virtual {p0}, Lcom/google/android/music/xdi/ExploreItemsCursor;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-static {v9}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultArtUri(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, p1, v8, v9}, Lcom/google/android/music/xdi/ExploreItemsCursor;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 134
    const-string v8, "width"

    invoke-virtual {p0, p1, v8, v13}, Lcom/google/android/music/xdi/ExploreItemsCursor;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 135
    const-string v8, "height"

    invoke-virtual {p0, p1, v8, v13}, Lcom/google/android/music/xdi/ExploreItemsCursor;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 136
    const-string v8, "intent_uri"

    invoke-virtual {v3, v12}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, p1, v8, v9}, Lcom/google/android/music/xdi/ExploreItemsCursor;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 137
    return-void
.end method

.method private static getCursorForHeader(Landroid/content/Context;JLjava/lang/String;)Landroid/database/Cursor;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "headerId"    # J
    .param p3, "genreId"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 38
    long-to-int v0, p1

    packed-switch v0, :pswitch_data_0

    .line 57
    const-string v0, "MusicXdi"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "BrowseExploreHeaderCursor: Unexpected header id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    new-instance v0, Landroid/database/MatrixCursor;

    invoke-direct {v0, v3}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    :goto_0
    return-object v0

    .line 40
    :pswitch_0
    invoke-static {p3}, Lcom/google/android/music/store/MusicContent$Explore;->getTopChartGroupsUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/google/android/music/xdi/MusicProjections;->PROJECTION_ENTITY_GROUPS:[Ljava/lang/String;

    move-object v0, p0

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0

    .line 44
    :pswitch_1
    invoke-static {}, Lcom/google/android/music/store/MusicContent$Explore;->getRecommendedGroupsUri()Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/google/android/music/xdi/MusicProjections;->PROJECTION_ENTITY_GROUPS:[Ljava/lang/String;

    move-object v0, p0

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0

    .line 48
    :pswitch_2
    invoke-static {p3}, Lcom/google/android/music/store/MusicContent$Explore;->getNewReleaseGroupsUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/google/android/music/xdi/MusicProjections;->PROJECTION_ENTITY_GROUPS:[Ljava/lang/String;

    move-object v0, p0

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0

    .line 53
    :pswitch_3
    invoke-static {p3}, Lcom/google/android/music/store/MusicContent$Explore;->getGenresUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/google/android/music/xdi/MusicProjections;->PROJECTION_GENRES:[Ljava/lang/String;

    move-object v0, p0

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0

    .line 38
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method protected extractDataForCurrentRow([Ljava/lang/Object;)Z
    .locals 4
    .param p1, "values"    # [Ljava/lang/Object;

    .prologue
    .line 65
    iget-wide v0, p0, Lcom/google/android/music/xdi/ExploreItemsCursor;->mHeaderId:J

    long-to-int v0, v0

    packed-switch v0, :pswitch_data_0

    .line 79
    const-string v0, "MusicXdi"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "BrowseExploreHeaderCursor: Unexpected header id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/music/xdi/ExploreItemsCursor;->mHeaderId:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    const/4 v0, 0x0

    .line 84
    :goto_0
    return v0

    .line 67
    :pswitch_0
    invoke-direct {p0, p1}, Lcom/google/android/music/xdi/ExploreItemsCursor;->extractDataForFeaturedContent([Ljava/lang/Object;)V

    .line 84
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 70
    :pswitch_1
    invoke-direct {p0, p1}, Lcom/google/android/music/xdi/ExploreItemsCursor;->extractDataForRecommendations([Ljava/lang/Object;)V

    goto :goto_1

    .line 73
    :pswitch_2
    invoke-direct {p0, p1}, Lcom/google/android/music/xdi/ExploreItemsCursor;->extractDataForNewReleases([Ljava/lang/Object;)V

    goto :goto_1

    .line 76
    :pswitch_3
    invoke-direct {p0, p1}, Lcom/google/android/music/xdi/ExploreItemsCursor;->extractDataForGenres([Ljava/lang/Object;)V

    goto :goto_1

    .line 65
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
