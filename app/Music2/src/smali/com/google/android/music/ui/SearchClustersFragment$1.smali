.class Lcom/google/android/music/ui/SearchClustersFragment$1;
.super Ljava/lang/Object;
.source "SearchClustersFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/SearchClustersFragment;->onActivityCreated(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/SearchClustersFragment;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/SearchClustersFragment;)V
    .locals 0

    .prologue
    .line 99
    iput-object p1, p0, Lcom/google/android/music/ui/SearchClustersFragment$1;->this$0:Lcom/google/android/music/ui/SearchClustersFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/android/music/ui/SearchClustersFragment$1;->this$0:Lcom/google/android/music/ui/SearchClustersFragment;

    # getter for: Lcom/google/android/music/ui/SearchClustersFragment;->mSearchView:Landroid/support/v7/widget/SearchView;
    invoke-static {v0}, Lcom/google/android/music/ui/SearchClustersFragment;->access$000(Lcom/google/android/music/ui/SearchClustersFragment;)Landroid/support/v7/widget/SearchView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 103
    iget-object v0, p0, Lcom/google/android/music/ui/SearchClustersFragment$1;->this$0:Lcom/google/android/music/ui/SearchClustersFragment;

    # getter for: Lcom/google/android/music/ui/SearchClustersFragment;->mSearchView:Landroid/support/v7/widget/SearchView;
    invoke-static {v0}, Lcom/google/android/music/ui/SearchClustersFragment;->access$000(Lcom/google/android/music/ui/SearchClustersFragment;)Landroid/support/v7/widget/SearchView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/ui/SearchClustersFragment$1;->this$0:Lcom/google/android/music/ui/SearchClustersFragment;

    # getter for: Lcom/google/android/music/ui/SearchClustersFragment;->mSuggestedQuery:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/music/ui/SearchClustersFragment;->access$100(Lcom/google/android/music/ui/SearchClustersFragment;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/widget/SearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    .line 107
    :goto_0
    iget-object v0, p0, Lcom/google/android/music/ui/SearchClustersFragment$1;->this$0:Lcom/google/android/music/ui/SearchClustersFragment;

    # getter for: Lcom/google/android/music/ui/SearchClustersFragment;->mSuggestedQueryTextWrapperView:Landroid/view/View;
    invoke-static {v0}, Lcom/google/android/music/ui/SearchClustersFragment;->access$200(Lcom/google/android/music/ui/SearchClustersFragment;)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 108
    return-void

    .line 105
    :cond_0
    const-string v0, "SearchClustersFragment"

    const-string v1, "No searchview, can\'t handle suggested query touch."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
