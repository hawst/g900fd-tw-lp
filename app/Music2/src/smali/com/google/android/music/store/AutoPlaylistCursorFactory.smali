.class public Lcom/google/android/music/store/AutoPlaylistCursorFactory;
.super Ljava/lang/Object;
.source "AutoPlaylistCursorFactory.java"


# static fields
.field private static final LOGV:Z


# instance fields
.field private final mAutoPlaylistIds:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->STORE:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/store/AutoPlaylistCursorFactory;->LOGV:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;[J)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "autoPlaylistIds"    # [J

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/google/android/music/store/AutoPlaylistCursorFactory;->mContext:Landroid/content/Context;

    .line 40
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    iput-object v5, p0, Lcom/google/android/music/store/AutoPlaylistCursorFactory;->mAutoPlaylistIds:Ljava/util/Set;

    .line 41
    move-object v0, p2

    .local v0, "arr$":[J
    array-length v4, v0

    .local v4, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v4, :cond_0

    aget-wide v2, v0, v1

    .line 42
    .local v2, "id":J
    iget-object v5, p0, Lcom/google/android/music/store/AutoPlaylistCursorFactory;->mAutoPlaylistIds:Ljava/util/Set;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 41
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 44
    .end local v2    # "id":J
    :cond_0
    return-void
.end method

.method private createRow(Lcom/google/android/music/store/Store;[Ljava/lang/String;J)Ljava/util/ArrayList;
    .locals 23
    .param p1, "store"    # Lcom/google/android/music/store/Store;
    .param p2, "columnNames"    # [Ljava/lang/String;
    .param p3, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/music/store/Store;",
            "[",
            "Ljava/lang/String;",
            "J)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 172
    new-instance v16, Ljava/util/ArrayList;

    move-object/from16 v0, p2

    array-length v0, v0

    move/from16 v19, v0

    move-object/from16 v0, v16

    move/from16 v1, v19

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 174
    .local v16, "row":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    const/4 v4, 0x0

    .line 175
    .local v4, "c":Landroid/database/Cursor;
    const/4 v14, 0x0

    .line 176
    .local v14, "keeponId":Ljava/lang/Long;
    const/16 v17, 0x0

    .line 177
    .local v17, "songCount":I
    const/4 v11, 0x0

    .line 178
    .local v11, "downloadedSongCount":I
    const/4 v9, 0x0

    .line 179
    .local v9, "dateAdded":I
    const-wide/16 v6, 0x0

    .line 181
    .local v6, "containerSize":J
    const/4 v15, 0x0

    .line 182
    .local v15, "keeponIdIdx":I
    const/16 v18, 0x1

    .line 183
    .local v18, "songCountIdx":I
    const/4 v12, 0x2

    .line 184
    .local v12, "downloadedSongCountIdx":I
    const/4 v10, 0x3

    .line 185
    .local v10, "dateAddedIdx":I
    const/4 v8, 0x4

    .line 187
    .local v8, "containerSizeIdx":I
    :try_start_0
    move-object/from16 v0, p1

    move-wide/from16 v1, p3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/Store;->getKeeponAutoListInfo(J)Landroid/database/Cursor;

    move-result-object v4

    .line 188
    if-eqz v4, :cond_0

    invoke-interface {v4}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v19

    if-eqz v19, :cond_0

    .line 189
    invoke-interface {v4, v15}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v20

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    .line 190
    move/from16 v0, v18

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v17

    .line 191
    invoke-interface {v4, v12}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    .line 192
    invoke-interface {v4, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    .line 193
    invoke-interface {v4, v8}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v6

    .line 196
    :cond_0
    invoke-static {v4}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 199
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_0
    move-object/from16 v0, p2

    array-length v0, v0

    move/from16 v19, v0

    move/from16 v0, v19

    if-ge v13, v0, :cond_15

    .line 200
    aget-object v5, p2, v13

    .line 201
    .local v5, "col":Ljava/lang/String;
    const-string v19, "_id"

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_1

    .line 202
    invoke-static/range {p3 .. p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v19

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 199
    :goto_1
    add-int/lit8 v13, v13, 0x1

    goto :goto_0

    .line 196
    .end local v5    # "col":Ljava/lang/String;
    .end local v13    # "i":I
    :catchall_0
    move-exception v19

    invoke-static {v4}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v19

    .line 203
    .restart local v5    # "col":Ljava/lang/String;
    .restart local v13    # "i":I
    :cond_1
    const-string v19, "playlist_id"

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_2

    .line 204
    invoke-static/range {p3 .. p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v19

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 205
    :cond_2
    const-string v19, "playlist_name"

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_3

    .line 206
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/store/AutoPlaylistCursorFactory;->mContext:Landroid/content/Context;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move-wide/from16 v1, p3

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/AutoPlaylistCursorFactory;->getAutoPlaylistName(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 207
    :cond_3
    const-string v19, "playlist_description"

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_4

    .line 208
    const/16 v19, 0x0

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 209
    :cond_4
    const-string v19, "playlist_owner_name"

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_5

    .line 210
    const/16 v19, 0x0

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 211
    :cond_5
    const-string v19, "playlist_share_token"

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_6

    .line 212
    const/16 v19, 0x0

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 213
    :cond_6
    const-string v19, "playlist_art_url"

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_7

    .line 214
    const/16 v19, 0x0

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 215
    :cond_7
    const-string v19, "playlist_owner_profile_photo_url"

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_8

    .line 216
    const/16 v19, 0x0

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 217
    :cond_8
    const-string v19, "playlist_type"

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_9

    .line 218
    const/16 v19, 0x64

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 219
    :cond_9
    const-string v19, "KeepOnId"

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_a

    .line 220
    move-object/from16 v0, v16

    invoke-virtual {v0, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 221
    :cond_a
    const-string v19, "hasRemote"

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_c

    .line 222
    sget-object v19, Lcom/google/android/music/store/Store$ItemType;->REMOTE:Lcom/google/android/music/store/Store$ItemType;

    move-object/from16 v0, p1

    move-wide/from16 v1, p3

    move-object/from16 v3, v19

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/music/store/Store;->getAutoPlaylistContains(JLcom/google/android/music/store/Store$ItemType;)Z

    move-result v19

    if-eqz v19, :cond_b

    const/16 v19, 0x1

    :goto_2
    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :cond_b
    const/16 v19, 0x0

    goto :goto_2

    .line 223
    :cond_c
    const-string v19, "hasLocal"

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_e

    .line 224
    sget-object v19, Lcom/google/android/music/store/Store$ItemType;->LOCAL:Lcom/google/android/music/store/Store$ItemType;

    move-object/from16 v0, p1

    move-wide/from16 v1, p3

    move-object/from16 v3, v19

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/music/store/Store;->getAutoPlaylistContains(JLcom/google/android/music/store/Store$ItemType;)Z

    move-result v19

    if-eqz v19, :cond_d

    const/16 v19, 0x1

    :goto_3
    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :cond_d
    const/16 v19, 0x0

    goto :goto_3

    .line 225
    :cond_e
    const-string v19, "isAllLocal"

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_f

    .line 226
    move-object/from16 v0, p1

    move-wide/from16 v1, p3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/Store;->getAutoPlayIsAllLocal(J)Z

    move-result v19

    invoke-static/range {v19 .. v19}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v19

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 227
    :cond_f
    const-string v19, "keeponSongCount"

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_10

    .line 228
    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 229
    :cond_10
    const-string v19, "keeponDownloadedSongCount"

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_11

    .line 230
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 231
    :cond_11
    const-string v19, "DateAdded"

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_12

    .line 232
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 233
    :cond_12
    const-string v19, "SourceAccount"

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_13

    .line 234
    const/16 v19, 0x0

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 235
    :cond_13
    const-string v19, "ContainerSizeBytes"

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_14

    .line 236
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v19

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 238
    :cond_14
    const-string v19, "AutoPlaylistFactory"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "Ignoring projection: "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 241
    .end local v5    # "col":Ljava/lang/String;
    :cond_15
    return-object v16
.end method

.method private createSoundSearchPlaylistRow([Ljava/lang/String;Landroid/database/Cursor;)Ljava/util/ArrayList;
    .locals 13
    .param p1, "columnNames"    # [Ljava/lang/String;
    .param p2, "c"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            "Landroid/database/Cursor;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 114
    new-instance v8, Ljava/util/ArrayList;

    array-length v10, p1

    invoke-direct {v8, v10}, Ljava/util/ArrayList;-><init>(I)V

    .line 116
    .local v8, "row":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    array-length v10, p1

    if-ge v5, v10, :cond_11

    .line 117
    aget-object v1, p1, v5

    .line 118
    .local v1, "col":Ljava/lang/String;
    const-string v10, "_id"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 119
    const-string v10, "Id"

    invoke-interface {p2, v10}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v6

    .line 120
    .local v6, "idIdx":I
    invoke-interface {p2, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 116
    .end local v6    # "idIdx":I
    :goto_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 121
    :cond_0
    const-string v10, "playlist_id"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 122
    const-string v10, "Id"

    invoke-interface {p2, v10}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v6

    .line 123
    .restart local v6    # "idIdx":I
    invoke-interface {p2, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 124
    .end local v6    # "idIdx":I
    :cond_1
    const-string v10, "playlist_name"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 125
    iget-object v10, p0, Lcom/google/android/music/store/AutoPlaylistCursorFactory;->mContext:Landroid/content/Context;

    const v11, 0x7f0b00c3

    invoke-virtual {v10, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 126
    :cond_2
    const-string v10, "playlist_description"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 127
    const-string v10, "Description"

    invoke-interface {p2, v10}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    .line 128
    .local v3, "descriptionIdx":I
    invoke-interface {p2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 129
    .end local v3    # "descriptionIdx":I
    :cond_3
    const-string v10, "playlist_owner_name"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 130
    const/4 v10, 0x0

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 131
    :cond_4
    const-string v10, "playlist_share_token"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 132
    const/4 v10, 0x0

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 133
    :cond_5
    const-string v10, "playlist_art_url"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 134
    const-string v10, "ListArtworkLocation"

    invoke-interface {p2, v10}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    .line 135
    .local v0, "artUrlIdx":I
    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 136
    .end local v0    # "artUrlIdx":I
    :cond_6
    const-string v10, "playlist_owner_profile_photo_url"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 137
    const/4 v10, 0x0

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 138
    :cond_7
    const-string v10, "playlist_type"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_8

    .line 139
    const/16 v10, 0x50

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 140
    :cond_8
    const-string v10, "KeepOnId"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_9

    .line 141
    const-string v10, "KeepOnId"

    invoke-interface {p2, v10}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v7

    .line 142
    .local v7, "keeponIdIdx":I
    invoke-interface {p2, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 143
    .end local v7    # "keeponIdIdx":I
    :cond_9
    const-string v10, "downloadedSongCount"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_a

    .line 144
    const-string v10, "DownloadedSongCount"

    invoke-interface {p2, v10}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    .line 145
    .local v4, "downloadedIdx":I
    invoke-interface {p2, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 146
    .end local v4    # "downloadedIdx":I
    :cond_a
    const-string v10, "songCount"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_b

    .line 147
    const-string v10, "SongCount"

    invoke-interface {p2, v10}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v9

    .line 148
    .local v9, "songCountIdx":I
    invoke-interface {p2, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 149
    .end local v9    # "songCountIdx":I
    :cond_b
    const-string v10, "DateAdded"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_c

    .line 150
    const-string v10, "DateAdded"

    invoke-interface {p2, v10}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    .line 151
    .local v2, "dateAddedIdx":I
    invoke-interface {p2, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 152
    .end local v2    # "dateAddedIdx":I
    :cond_c
    const-string v10, "hasRemote"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_d

    .line 154
    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 155
    :cond_d
    const-string v10, "hasLocal"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_e

    .line 157
    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 158
    :cond_e
    const-string v10, "SourceAccount"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_f

    .line 159
    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 160
    :cond_f
    const-string v10, "ContainerSizeBytes"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_10

    .line 162
    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 164
    :cond_10
    const-string v10, "AutoPlaylistFactory"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Ignoring projection: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 168
    .end local v1    # "col":Ljava/lang/String;
    :cond_11
    return-object v8
.end method

.method static getAutoPlaylistName(Landroid/content/Context;J)Ljava/lang/String;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "autoPlaylistId"    # J

    .prologue
    .line 266
    const-wide/16 v0, -0x4

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    .line 267
    const v0, 0x7f0b00c1

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 275
    :goto_0
    return-object v0

    .line 268
    :cond_0
    const-wide/16 v0, -0x1

    cmp-long v0, p1, v0

    if-nez v0, :cond_1

    .line 269
    const v0, 0x7f0b00b9

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 270
    :cond_1
    const-wide/16 v0, -0x3

    cmp-long v0, p1, v0

    if-nez v0, :cond_2

    .line 271
    const v0, 0x7f0b00bd

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 272
    :cond_2
    const-wide/16 v0, -0x2

    cmp-long v0, p1, v0

    if-nez v0, :cond_3

    .line 273
    const v0, 0x7f0b00bb

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 275
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private showLastAddedSongs()Z
    .locals 3

    .prologue
    .line 261
    iget-object v0, p0, Lcom/google/android/music/store/AutoPlaylistCursorFactory;->mContext:Landroid/content/Context;

    sget-object v1, Lcom/google/android/music/store/MusicContent$XAudio;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContent;->existsContent(Landroid/content/Context;Landroid/net/Uri;Z)Z

    move-result v0

    return v0
.end method

.method private showStoreSongs()Z
    .locals 4

    .prologue
    .line 245
    new-instance v0, Lcom/google/android/music/medialist/StoreSongList;

    const/4 v1, -0x1

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/google/android/music/medialist/StoreSongList;-><init>(IZ)V

    .line 246
    .local v0, "list":Lcom/google/android/music/medialist/StoreSongList;
    iget-object v1, p0, Lcom/google/android/music/store/AutoPlaylistCursorFactory;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/music/store/AutoPlaylistCursorFactory;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v2}, Lcom/google/android/music/medialist/StoreSongList;->getContentUri(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/music/medialist/StoreSongList;->mayIncludeExternalContent()Z

    move-result v3

    invoke-static {v1, v2, v3}, Lcom/google/android/music/store/MusicContent;->existsContent(Landroid/content/Context;Landroid/net/Uri;Z)Z

    move-result v1

    return v1
.end method

.method private showThumbsSongs()Z
    .locals 4

    .prologue
    .line 253
    new-instance v0, Lcom/google/android/music/medialist/ThumbsUpSongList;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Lcom/google/android/music/medialist/ThumbsUpSongList;-><init>(I)V

    .line 254
    .local v0, "list":Lcom/google/android/music/medialist/ThumbsUpSongList;
    iget-object v1, p0, Lcom/google/android/music/store/AutoPlaylistCursorFactory;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/music/store/AutoPlaylistCursorFactory;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v2}, Lcom/google/android/music/medialist/ThumbsUpSongList;->getContentUri(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/music/medialist/ThumbsUpSongList;->mayIncludeExternalContent()Z

    move-result v3

    invoke-static {v1, v2, v3}, Lcom/google/android/music/store/MusicContent;->existsContent(Landroid/content/Context;Landroid/net/Uri;Z)Z

    move-result v1

    return v1
.end method


# virtual methods
.method public buildCursor([Ljava/lang/String;)Landroid/database/MatrixCursor;
    .locals 12
    .param p1, "columnNames"    # [Ljava/lang/String;

    .prologue
    .line 53
    iget-object v9, p0, Lcom/google/android/music/store/AutoPlaylistCursorFactory;->mAutoPlaylistIds:Ljava/util/Set;

    const-wide/16 v10, -0x1

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-interface {v9, v10}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    invoke-direct {p0}, Lcom/google/android/music/store/AutoPlaylistCursorFactory;->showLastAddedSongs()Z

    move-result v9

    if-eqz v9, :cond_6

    const/4 v2, 0x1

    .line 57
    .local v2, "includeLastAddedSongs":Z
    :goto_0
    iget-object v9, p0, Lcom/google/android/music/store/AutoPlaylistCursorFactory;->mAutoPlaylistIds:Ljava/util/Set;

    const-wide/16 v10, -0x3

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-interface {v9, v10}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_7

    invoke-direct {p0}, Lcom/google/android/music/store/AutoPlaylistCursorFactory;->showStoreSongs()Z

    move-result v9

    if-eqz v9, :cond_7

    const/4 v3, 0x1

    .line 60
    .local v3, "includeStoreSongs":Z
    :goto_1
    iget-object v9, p0, Lcom/google/android/music/store/AutoPlaylistCursorFactory;->mAutoPlaylistIds:Ljava/util/Set;

    const-wide/16 v10, -0x4

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-interface {v9, v10}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_8

    invoke-direct {p0}, Lcom/google/android/music/store/AutoPlaylistCursorFactory;->showThumbsSongs()Z

    move-result v9

    if-eqz v9, :cond_8

    const/4 v4, 0x1

    .line 64
    .local v4, "includeThumbsUp":Z
    :goto_2
    iget-object v9, p0, Lcom/google/android/music/store/AutoPlaylistCursorFactory;->mAutoPlaylistIds:Ljava/util/Set;

    const-wide/16 v10, -0x2

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-interface {v9, v10}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    .line 67
    .local v1, "includeAllTracksList":Z
    new-instance v0, Landroid/database/MatrixCursor;

    invoke-direct {v0, p1}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 68
    .local v0, "c":Landroid/database/MatrixCursor;
    iget-object v9, p0, Lcom/google/android/music/store/AutoPlaylistCursorFactory;->mContext:Landroid/content/Context;

    invoke-static {v9}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v8

    .line 69
    .local v8, "store":Lcom/google/android/music/store/Store;
    if-eqz v4, :cond_0

    .line 70
    const-wide/16 v10, -0x4

    invoke-direct {p0, v8, p1, v10, v11}, Lcom/google/android/music/store/AutoPlaylistCursorFactory;->createRow(Lcom/google/android/music/store/Store;[Ljava/lang/String;J)Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v0, v9}, Landroid/database/MatrixCursor;->addRow(Ljava/lang/Iterable;)V

    .line 72
    :cond_0
    if-eqz v2, :cond_1

    .line 73
    const-wide/16 v10, -0x1

    invoke-direct {p0, v8, p1, v10, v11}, Lcom/google/android/music/store/AutoPlaylistCursorFactory;->createRow(Lcom/google/android/music/store/Store;[Ljava/lang/String;J)Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v0, v9}, Landroid/database/MatrixCursor;->addRow(Ljava/lang/Iterable;)V

    .line 76
    :cond_1
    if-eqz v3, :cond_2

    .line 77
    const-wide/16 v10, -0x3

    invoke-direct {p0, v8, p1, v10, v11}, Lcom/google/android/music/store/AutoPlaylistCursorFactory;->createRow(Lcom/google/android/music/store/Store;[Ljava/lang/String;J)Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v0, v9}, Landroid/database/MatrixCursor;->addRow(Ljava/lang/Iterable;)V

    .line 79
    :cond_2
    if-eqz v1, :cond_3

    .line 80
    const-wide/16 v10, -0x2

    invoke-direct {p0, v8, p1, v10, v11}, Lcom/google/android/music/store/AutoPlaylistCursorFactory;->createRow(Lcom/google/android/music/store/Store;[Ljava/lang/String;J)Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v0, v9}, Landroid/database/MatrixCursor;->addRow(Ljava/lang/Iterable;)V

    .line 84
    :cond_3
    iget-object v9, p0, Lcom/google/android/music/store/AutoPlaylistCursorFactory;->mContext:Landroid/content/Context;

    invoke-static {v9, p0}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v6

    .line 86
    .local v6, "musicPreferences":Lcom/google/android/music/preferences/MusicPreferences;
    :try_start_0
    invoke-virtual {v6}, Lcom/google/android/music/preferences/MusicPreferences;->isNautilusEnabled()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v5

    .line 88
    .local v5, "isNautilusEnabled":Z
    invoke-static {p0}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    .line 97
    iget-object v9, p0, Lcom/google/android/music/store/AutoPlaylistCursorFactory;->mAutoPlaylistIds:Ljava/util/Set;

    invoke-interface {v9}, Ljava/util/Set;->size()I

    move-result v9

    const/4 v10, 0x1

    if-le v9, v10, :cond_5

    if-eqz v5, :cond_5

    .line 98
    const/4 v7, 0x0

    .line 100
    .local v7, "soundSearchCursor":Landroid/database/Cursor;
    :try_start_1
    invoke-virtual {v8, p1}, Lcom/google/android/music/store/Store;->getSoundSearchPlaylistCursor([Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 101
    if-eqz v7, :cond_4

    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v9

    if-eqz v9, :cond_4

    .line 102
    invoke-direct {p0, p1, v7}, Lcom/google/android/music/store/AutoPlaylistCursorFactory;->createSoundSearchPlaylistRow([Ljava/lang/String;Landroid/database/Cursor;)Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v0, v9}, Landroid/database/MatrixCursor;->addRow(Ljava/lang/Iterable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 105
    :cond_4
    invoke-static {v7}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 109
    .end local v7    # "soundSearchCursor":Landroid/database/Cursor;
    :cond_5
    return-object v0

    .line 53
    .end local v0    # "c":Landroid/database/MatrixCursor;
    .end local v1    # "includeAllTracksList":Z
    .end local v2    # "includeLastAddedSongs":Z
    .end local v3    # "includeStoreSongs":Z
    .end local v4    # "includeThumbsUp":Z
    .end local v5    # "isNautilusEnabled":Z
    .end local v6    # "musicPreferences":Lcom/google/android/music/preferences/MusicPreferences;
    .end local v8    # "store":Lcom/google/android/music/store/Store;
    :cond_6
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 57
    .restart local v2    # "includeLastAddedSongs":Z
    :cond_7
    const/4 v3, 0x0

    goto/16 :goto_1

    .line 60
    .restart local v3    # "includeStoreSongs":Z
    :cond_8
    const/4 v4, 0x0

    goto :goto_2

    .line 88
    .restart local v0    # "c":Landroid/database/MatrixCursor;
    .restart local v1    # "includeAllTracksList":Z
    .restart local v4    # "includeThumbsUp":Z
    .restart local v6    # "musicPreferences":Lcom/google/android/music/preferences/MusicPreferences;
    .restart local v8    # "store":Lcom/google/android/music/store/Store;
    :catchall_0
    move-exception v9

    invoke-static {p0}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    throw v9

    .line 105
    .restart local v5    # "isNautilusEnabled":Z
    .restart local v7    # "soundSearchCursor":Landroid/database/Cursor;
    :catchall_1
    move-exception v9

    invoke-static {v7}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v9
.end method
