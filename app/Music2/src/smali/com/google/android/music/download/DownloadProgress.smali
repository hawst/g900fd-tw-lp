.class public interface abstract Lcom/google/android/music/download/DownloadProgress;
.super Ljava/lang/Object;
.source "DownloadProgress.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/music/download/DownloadProgress;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    new-instance v0, Lcom/google/android/music/download/DownloadProgress$1;

    invoke-direct {v0}, Lcom/google/android/music/download/DownloadProgress$1;-><init>()V

    sput-object v0, Lcom/google/android/music/download/DownloadProgress;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method


# virtual methods
.method public abstract getCompletedBytes()J
.end method

.method public abstract getDownloadByteLength()J
.end method

.method public abstract getError()I
.end method

.method public abstract getId()Lcom/google/android/music/download/ContentIdentifier;
.end method

.method public abstract getOwner()Lcom/google/android/music/download/DownloadRequest$Owner;
.end method

.method public abstract getState()Lcom/google/android/music/download/DownloadState$State;
.end method
