.class public Lcom/google/android/music/SafePostProgressBar;
.super Landroid/widget/ProgressBar;
.source "SafePostProgressBar.java"


# instance fields
.field private mAttachedToWindow:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 20
    return-void
.end method


# virtual methods
.method public onAttachedToWindow()V
    .locals 1

    .prologue
    .line 35
    invoke-super {p0}, Landroid/widget/ProgressBar;->onAttachedToWindow()V

    .line 36
    monitor-enter p0

    .line 37
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/music/SafePostProgressBar;->mAttachedToWindow:Z

    .line 38
    monitor-exit p0

    .line 39
    return-void

    .line 38
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 43
    monitor-enter p0

    .line 44
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/music/SafePostProgressBar;->mAttachedToWindow:Z

    .line 45
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 46
    invoke-super {p0}, Landroid/widget/ProgressBar;->onDetachedFromWindow()V

    .line 47
    return-void

    .line 45
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public declared-synchronized post(Ljava/lang/Runnable;)Z
    .locals 1
    .param p1, "action"    # Ljava/lang/Runnable;

    .prologue
    .line 26
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/music/SafePostProgressBar;->mAttachedToWindow:Z

    if-eqz v0, :cond_0

    .line 27
    invoke-super {p0, p1}, Landroid/widget/ProgressBar;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 29
    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 26
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
