.class Lcom/google/android/music/leanback/Item$Builder;
.super Ljava/lang/Object;
.source "Item.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/leanback/Item;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Builder"
.end annotation


# instance fields
.field private mBitmapGettersGetter:Lcom/google/android/music/leanback/bitmap/BitmapGettersGetter;

.field private mDarkenBackground:Z

.field private mDescriptionGetter:Lcom/google/android/music/leanback/Item$StringGetter;

.field private mIconResourceId:I

.field private mIconUriGetter:Lcom/google/android/music/leanback/Item$StringGetter;

.field private mId:Ljava/lang/String;

.field private mIntentGetter:Lcom/google/android/music/leanback/Item$IntentGetter;

.field private mOverlayResourceId:I

.field private mSecondaryId:J

.field private mTitle:Ljava/lang/String;

.field private mWide:Z


# direct methods
.method constructor <init>()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    iput-object v2, p0, Lcom/google/android/music/leanback/Item$Builder;->mTitle:Ljava/lang/String;

    .line 62
    iput-object v2, p0, Lcom/google/android/music/leanback/Item$Builder;->mDescriptionGetter:Lcom/google/android/music/leanback/Item$StringGetter;

    .line 63
    iput-object v2, p0, Lcom/google/android/music/leanback/Item$Builder;->mIconUriGetter:Lcom/google/android/music/leanback/Item$StringGetter;

    .line 64
    iput v1, p0, Lcom/google/android/music/leanback/Item$Builder;->mIconResourceId:I

    .line 65
    iput-object v2, p0, Lcom/google/android/music/leanback/Item$Builder;->mIntentGetter:Lcom/google/android/music/leanback/Item$IntentGetter;

    .line 66
    iput-object v2, p0, Lcom/google/android/music/leanback/Item$Builder;->mId:Ljava/lang/String;

    .line 67
    iput-boolean v1, p0, Lcom/google/android/music/leanback/Item$Builder;->mWide:Z

    .line 68
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/leanback/Item$Builder;->mDarkenBackground:Z

    .line 69
    iput v1, p0, Lcom/google/android/music/leanback/Item$Builder;->mOverlayResourceId:I

    .line 70
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/music/leanback/Item$Builder;->mSecondaryId:J

    .line 71
    iput-object v2, p0, Lcom/google/android/music/leanback/Item$Builder;->mBitmapGettersGetter:Lcom/google/android/music/leanback/bitmap/BitmapGettersGetter;

    return-void
.end method

.method private toIntentGetter(Landroid/content/Intent;)Lcom/google/android/music/leanback/Item$IntentGetter;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 175
    if-eqz p1, :cond_0

    new-instance v0, Lcom/google/android/music/leanback/Item$ConstantIntentGetter;

    invoke-direct {v0, p1}, Lcom/google/android/music/leanback/Item$ConstantIntentGetter;-><init>(Landroid/content/Intent;)V

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private toStringGetter(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$StringGetter;
    .locals 1
    .param p1, "string"    # Ljava/lang/String;

    .prologue
    .line 171
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/music/leanback/Item$ConstantStringGetter;

    invoke-direct {v0, p1}, Lcom/google/android/music/leanback/Item$ConstantStringGetter;-><init>(Ljava/lang/String;)V

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method bitmapGettersGetter(Lcom/google/android/music/leanback/bitmap/BitmapGettersGetter;)Lcom/google/android/music/leanback/Item$Builder;
    .locals 0
    .param p1, "bitmapGettersGetter"    # Lcom/google/android/music/leanback/bitmap/BitmapGettersGetter;

    .prologue
    .line 139
    iput-object p1, p0, Lcom/google/android/music/leanback/Item$Builder;->mBitmapGettersGetter:Lcom/google/android/music/leanback/bitmap/BitmapGettersGetter;

    .line 140
    return-object p0
.end method

.method build()Lcom/google/android/music/leanback/Item;
    .locals 22

    .prologue
    .line 144
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/leanback/Item$Builder;->mBitmapGettersGetter:Lcom/google/android/music/leanback/bitmap/BitmapGettersGetter;

    if-nez v4, :cond_3

    .line 145
    const/16 v17, 0x0

    .line 147
    .local v17, "albumIdBitmapGetter":Lcom/google/android/music/leanback/bitmap/AlbumIdBitmapGetter;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/leanback/Item$Builder;->mId:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v18

    .line 148
    .local v18, "albumId":J
    const-wide/16 v4, 0x0

    cmp-long v4, v18, v4

    if-ltz v4, :cond_0

    .line 149
    new-instance v20, Lcom/google/android/music/leanback/bitmap/AlbumIdBitmapGetter;

    move-object/from16 v0, v20

    move-wide/from16 v1, v18

    invoke-direct {v0, v1, v2}, Lcom/google/android/music/leanback/bitmap/AlbumIdBitmapGetter;-><init>(J)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .end local v17    # "albumIdBitmapGetter":Lcom/google/android/music/leanback/bitmap/AlbumIdBitmapGetter;
    .local v20, "albumIdBitmapGetter":Lcom/google/android/music/leanback/bitmap/AlbumIdBitmapGetter;
    move-object/from16 v17, v20

    .line 156
    .end local v18    # "albumId":J
    .end local v20    # "albumIdBitmapGetter":Lcom/google/android/music/leanback/bitmap/AlbumIdBitmapGetter;
    .restart local v17    # "albumIdBitmapGetter":Lcom/google/android/music/leanback/bitmap/AlbumIdBitmapGetter;
    :cond_0
    :goto_0
    const/16 v21, 0x0

    .line 157
    .local v21, "stringGetterBitmapGetter":Lcom/google/android/music/leanback/bitmap/StringGetterBitmapGetter;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/leanback/Item$Builder;->mIconUriGetter:Lcom/google/android/music/leanback/Item$StringGetter;

    if-eqz v4, :cond_1

    .line 158
    new-instance v21, Lcom/google/android/music/leanback/bitmap/StringGetterBitmapGetter;

    .end local v21    # "stringGetterBitmapGetter":Lcom/google/android/music/leanback/bitmap/StringGetterBitmapGetter;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/leanback/Item$Builder;->mIconUriGetter:Lcom/google/android/music/leanback/Item$StringGetter;

    move-object/from16 v0, v21

    invoke-direct {v0, v4}, Lcom/google/android/music/leanback/bitmap/StringGetterBitmapGetter;-><init>(Lcom/google/android/music/leanback/Item$StringGetter;)V

    .line 160
    .restart local v21    # "stringGetterBitmapGetter":Lcom/google/android/music/leanback/bitmap/StringGetterBitmapGetter;
    :cond_1
    if-nez v17, :cond_2

    if-eqz v21, :cond_3

    .line 161
    :cond_2
    new-instance v4, Lcom/google/android/music/leanback/bitmap/ConstantBitmapGettersGetter;

    const/4 v5, 0x2

    new-array v5, v5, [Lcom/google/android/music/leanback/bitmap/BitmapGetter;

    const/4 v6, 0x0

    aput-object v17, v5, v6

    const/4 v6, 0x1

    aput-object v21, v5, v6

    invoke-direct {v4, v5}, Lcom/google/android/music/leanback/bitmap/ConstantBitmapGettersGetter;-><init>([Lcom/google/android/music/leanback/bitmap/BitmapGetter;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/music/leanback/Item$Builder;->mBitmapGettersGetter:Lcom/google/android/music/leanback/bitmap/BitmapGettersGetter;

    .line 165
    .end local v17    # "albumIdBitmapGetter":Lcom/google/android/music/leanback/bitmap/AlbumIdBitmapGetter;
    .end local v21    # "stringGetterBitmapGetter":Lcom/google/android/music/leanback/bitmap/StringGetterBitmapGetter;
    :cond_3
    new-instance v4, Lcom/google/android/music/leanback/Item;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/music/leanback/Item$Builder;->mTitle:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/music/leanback/Item$Builder;->mDescriptionGetter:Lcom/google/android/music/leanback/Item$StringGetter;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/music/leanback/Item$Builder;->mIconUriGetter:Lcom/google/android/music/leanback/Item$StringGetter;

    move-object/from16 v0, p0

    iget v8, v0, Lcom/google/android/music/leanback/Item$Builder;->mIconResourceId:I

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/music/leanback/Item$Builder;->mIntentGetter:Lcom/google/android/music/leanback/Item$IntentGetter;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/music/leanback/Item$Builder;->mId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-boolean v11, v0, Lcom/google/android/music/leanback/Item$Builder;->mWide:Z

    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/google/android/music/leanback/Item$Builder;->mDarkenBackground:Z

    move-object/from16 v0, p0

    iget v13, v0, Lcom/google/android/music/leanback/Item$Builder;->mOverlayResourceId:I

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/google/android/music/leanback/Item$Builder;->mSecondaryId:J

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/leanback/Item$Builder;->mBitmapGettersGetter:Lcom/google/android/music/leanback/bitmap/BitmapGettersGetter;

    move-object/from16 v16, v0

    invoke-direct/range {v4 .. v16}, Lcom/google/android/music/leanback/Item;-><init>(Ljava/lang/String;Lcom/google/android/music/leanback/Item$StringGetter;Lcom/google/android/music/leanback/Item$StringGetter;ILcom/google/android/music/leanback/Item$IntentGetter;Ljava/lang/String;ZZIJLcom/google/android/music/leanback/bitmap/BitmapGettersGetter;)V

    return-object v4

    .line 151
    .restart local v17    # "albumIdBitmapGetter":Lcom/google/android/music/leanback/bitmap/AlbumIdBitmapGetter;
    :catch_0
    move-exception v4

    goto :goto_0
.end method

.method darkenBackground(Z)Lcom/google/android/music/leanback/Item$Builder;
    .locals 0
    .param p1, "darkenBackground"    # Z

    .prologue
    .line 124
    iput-boolean p1, p0, Lcom/google/android/music/leanback/Item$Builder;->mDarkenBackground:Z

    .line 125
    return-object p0
.end method

.method description(Lcom/google/android/music/leanback/Item$StringGetter;)Lcom/google/android/music/leanback/Item$Builder;
    .locals 0
    .param p1, "descriptionGetter"    # Lcom/google/android/music/leanback/Item$StringGetter;

    .prologue
    .line 84
    iput-object p1, p0, Lcom/google/android/music/leanback/Item$Builder;->mDescriptionGetter:Lcom/google/android/music/leanback/Item$StringGetter;

    .line 85
    return-object p0
.end method

.method description(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;
    .locals 1
    .param p1, "description"    # Ljava/lang/String;

    .prologue
    .line 79
    invoke-direct {p0, p1}, Lcom/google/android/music/leanback/Item$Builder;->toStringGetter(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$StringGetter;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/leanback/Item$Builder;->mDescriptionGetter:Lcom/google/android/music/leanback/Item$StringGetter;

    .line 80
    return-object p0
.end method

.method iconResourceId(I)Lcom/google/android/music/leanback/Item$Builder;
    .locals 0
    .param p1, "iconResourceId"    # I

    .prologue
    .line 99
    iput p1, p0, Lcom/google/android/music/leanback/Item$Builder;->mIconResourceId:I

    .line 100
    return-object p0
.end method

.method iconUri(Lcom/google/android/music/leanback/Item$StringGetter;)Lcom/google/android/music/leanback/Item$Builder;
    .locals 0
    .param p1, "iconUriGetter"    # Lcom/google/android/music/leanback/Item$StringGetter;

    .prologue
    .line 94
    iput-object p1, p0, Lcom/google/android/music/leanback/Item$Builder;->mIconUriGetter:Lcom/google/android/music/leanback/Item$StringGetter;

    .line 95
    return-object p0
.end method

.method iconUri(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;
    .locals 1
    .param p1, "iconUri"    # Ljava/lang/String;

    .prologue
    .line 89
    invoke-direct {p0, p1}, Lcom/google/android/music/leanback/Item$Builder;->toStringGetter(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$StringGetter;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/leanback/Item$Builder;->mIconUriGetter:Lcom/google/android/music/leanback/Item$StringGetter;

    .line 90
    return-object p0
.end method

.method id(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;
    .locals 0
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 114
    iput-object p1, p0, Lcom/google/android/music/leanback/Item$Builder;->mId:Ljava/lang/String;

    .line 115
    return-object p0
.end method

.method intent(Landroid/content/Intent;)Lcom/google/android/music/leanback/Item$Builder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 104
    invoke-direct {p0, p1}, Lcom/google/android/music/leanback/Item$Builder;->toIntentGetter(Landroid/content/Intent;)Lcom/google/android/music/leanback/Item$IntentGetter;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/leanback/Item$Builder;->mIntentGetter:Lcom/google/android/music/leanback/Item$IntentGetter;

    .line 105
    return-object p0
.end method

.method intent(Lcom/google/android/music/leanback/Item$IntentGetter;)Lcom/google/android/music/leanback/Item$Builder;
    .locals 0
    .param p1, "intentGetter"    # Lcom/google/android/music/leanback/Item$IntentGetter;

    .prologue
    .line 109
    iput-object p1, p0, Lcom/google/android/music/leanback/Item$Builder;->mIntentGetter:Lcom/google/android/music/leanback/Item$IntentGetter;

    .line 110
    return-object p0
.end method

.method overlayResourceId(I)Lcom/google/android/music/leanback/Item$Builder;
    .locals 0
    .param p1, "overlayResourceId"    # I

    .prologue
    .line 129
    iput p1, p0, Lcom/google/android/music/leanback/Item$Builder;->mOverlayResourceId:I

    .line 130
    return-object p0
.end method

.method secondaryId(J)Lcom/google/android/music/leanback/Item$Builder;
    .locals 1
    .param p1, "secondaryId"    # J

    .prologue
    .line 134
    iput-wide p1, p0, Lcom/google/android/music/leanback/Item$Builder;->mSecondaryId:J

    .line 135
    return-object p0
.end method

.method title(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;
    .locals 0
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/google/android/music/leanback/Item$Builder;->mTitle:Ljava/lang/String;

    .line 75
    return-object p0
.end method

.method wide(Z)Lcom/google/android/music/leanback/Item$Builder;
    .locals 0
    .param p1, "wide"    # Z

    .prologue
    .line 119
    iput-boolean p1, p0, Lcom/google/android/music/leanback/Item$Builder;->mWide:Z

    .line 120
    return-object p0
.end method
