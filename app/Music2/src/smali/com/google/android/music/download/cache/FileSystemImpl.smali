.class public Lcom/google/android/music/download/cache/FileSystemImpl;
.super Ljava/lang/Object;
.source "FileSystemImpl.java"

# interfaces
.implements Lcom/google/android/music/download/cache/FileSystem;


# instance fields
.field private final mFilteredFileDeleter:Lcom/google/android/music/download/cache/FilteredFileDeleter;


# direct methods
.method public constructor <init>(Lcom/google/android/music/download/cache/FilteredFileDeleter;)V
    .locals 0
    .param p1, "filteredFileDeleter"    # Lcom/google/android/music/download/cache/FilteredFileDeleter;

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/google/android/music/download/cache/FileSystemImpl;->mFilteredFileDeleter:Lcom/google/android/music/download/cache/FilteredFileDeleter;

    .line 21
    return-void
.end method


# virtual methods
.method public delete(Ljava/io/File;)Z
    .locals 1
    .param p1, "f"    # Ljava/io/File;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/music/download/cache/FileSystemImpl;->mFilteredFileDeleter:Lcom/google/android/music/download/cache/FilteredFileDeleter;

    invoke-interface {v0, p1}, Lcom/google/android/music/download/cache/FilteredFileDeleter;->requestDeleteFile(Ljava/io/File;)Z

    move-result v0

    return v0
.end method

.method public exists(Ljava/io/File;)Z
    .locals 1
    .param p1, "f"    # Ljava/io/File;

    .prologue
    .line 35
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v0

    return v0
.end method

.method public getFreeSpace(Ljava/io/File;)J
    .locals 6
    .param p1, "location"    # Ljava/io/File;

    .prologue
    .line 40
    :try_start_0
    new-instance v1, Landroid/os/StatFs;

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 41
    .local v1, "stats":Landroid/os/StatFs;
    invoke-virtual {v1}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v1}, Landroid/os/StatFs;->getBlockSize()I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    int-to-long v4, v4

    mul-long/2addr v2, v4

    .line 45
    .end local v1    # "stats":Landroid/os/StatFs;
    :goto_0
    return-wide v2

    .line 42
    :catch_0
    move-exception v0

    .line 44
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string v2, "FileSystemImpl"

    const-string v3, "error in StatFs"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 45
    const-wide/16 v2, 0x0

    goto :goto_0
.end method

.method public getLength(Ljava/io/File;)J
    .locals 2
    .param p1, "f"    # Ljava/io/File;

    .prologue
    .line 24
    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v0

    return-wide v0
.end method

.method public getTotalSpace(Ljava/io/File;)J
    .locals 6
    .param p1, "location"    # Ljava/io/File;

    .prologue
    .line 51
    :try_start_0
    new-instance v1, Landroid/os/StatFs;

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 52
    .local v1, "stats":Landroid/os/StatFs;
    invoke-virtual {v1}, Landroid/os/StatFs;->getBlockCount()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v1}, Landroid/os/StatFs;->getBlockSize()I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    int-to-long v4, v4

    mul-long/2addr v2, v4

    .line 56
    .end local v1    # "stats":Landroid/os/StatFs;
    :goto_0
    return-wide v2

    .line 53
    :catch_0
    move-exception v0

    .line 55
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string v2, "FileSystemImpl"

    const-string v3, "error in StatFs"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 56
    const-wide/16 v2, 0x0

    goto :goto_0
.end method
