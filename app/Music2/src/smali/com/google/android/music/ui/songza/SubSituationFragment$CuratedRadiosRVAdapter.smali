.class public Lcom/google/android/music/ui/songza/SubSituationFragment$CuratedRadiosRVAdapter;
.super Landroid/support/v7/widget/RecyclerView$Adapter;
.source "SubSituationFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard$OnExpandListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/songza/SubSituationFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "CuratedRadiosRVAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v7/widget/RecyclerView$Adapter",
        "<",
        "Landroid/support/v7/widget/RecyclerView$ViewHolder;",
        ">;",
        "Landroid/view/View$OnClickListener;",
        "Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard$OnExpandListener;"
    }
.end annotation


# instance fields
.field private mCursor:Landroid/database/Cursor;

.field private mFragment:Lcom/google/android/music/ui/songza/SubSituationFragment;

.field final synthetic this$0:Lcom/google/android/music/ui/songza/SubSituationFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/music/ui/songza/SubSituationFragment;Lcom/google/android/music/ui/songza/SubSituationFragment;)V
    .locals 1
    .param p2, "fragment"    # Lcom/google/android/music/ui/songza/SubSituationFragment;

    .prologue
    .line 239
    iput-object p1, p0, Lcom/google/android/music/ui/songza/SubSituationFragment$CuratedRadiosRVAdapter;->this$0:Lcom/google/android/music/ui/songza/SubSituationFragment;

    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$Adapter;-><init>()V

    .line 240
    iput-object p2, p0, Lcom/google/android/music/ui/songza/SubSituationFragment$CuratedRadiosRVAdapter;->mFragment:Lcom/google/android/music/ui/songza/SubSituationFragment;

    .line 241
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/songza/SubSituationFragment$CuratedRadiosRVAdapter;->setHasStableIds(Z)V

    .line 242
    return-void
.end method

.method private bindDocument(Lcom/google/android/music/ui/PlayCardViewHolder;I)V
    .locals 10
    .param p1, "holder"    # Lcom/google/android/music/ui/PlayCardViewHolder;
    .param p2, "index"    # I

    .prologue
    .line 323
    new-instance v8, Lcom/google/android/music/ui/songza/SubSituationFragment$CuratedRadioDocument;

    invoke-direct {v8}, Lcom/google/android/music/ui/songza/SubSituationFragment$CuratedRadioDocument;-><init>()V

    iput-object v8, p1, Lcom/google/android/music/ui/PlayCardViewHolder;->document:Lcom/google/android/music/ui/cardlib/model/Document;

    .line 324
    iget-object v1, p1, Lcom/google/android/music/ui/PlayCardViewHolder;->document:Lcom/google/android/music/ui/cardlib/model/Document;

    .line 326
    .local v1, "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    iget-object v8, p0, Lcom/google/android/music/ui/songza/SubSituationFragment$CuratedRadiosRVAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v8, :cond_0

    iget-object v8, p0, Lcom/google/android/music/ui/songza/SubSituationFragment$CuratedRadiosRVAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v8, p2}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v8

    if-nez v8, :cond_1

    .line 358
    .end local v1    # "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    :cond_0
    :goto_0
    return-void

    .line 328
    .restart local v1    # "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    :cond_1
    iget-object v8, p0, Lcom/google/android/music/ui/songza/SubSituationFragment$CuratedRadiosRVAdapter;->mCursor:Landroid/database/Cursor;

    const/4 v9, 0x0

    invoke-interface {v8, v9}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 329
    .local v2, "id":J
    sget-object v8, Lcom/google/android/music/ui/cardlib/model/Document$Type;->RADIO:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    invoke-virtual {v1, v8}, Lcom/google/android/music/ui/cardlib/model/Document;->setType(Lcom/google/android/music/ui/cardlib/model/Document$Type;)V

    .line 330
    invoke-virtual {v1, v2, v3}, Lcom/google/android/music/ui/cardlib/model/Document;->setId(J)V

    .line 331
    invoke-virtual {v1, p2}, Lcom/google/android/music/ui/cardlib/model/Document;->setPosition(I)V

    .line 332
    const/4 v8, 0x7

    invoke-virtual {v1, v8}, Lcom/google/android/music/ui/cardlib/model/Document;->setMainstageReason(I)V

    .line 333
    iget-object v8, p0, Lcom/google/android/music/ui/songza/SubSituationFragment$CuratedRadiosRVAdapter;->mCursor:Landroid/database/Cursor;

    const/4 v9, 0x1

    invoke-interface {v8, v9}, Landroid/database/Cursor;->isNull(I)Z

    move-result v8

    if-nez v8, :cond_2

    .line 334
    iget-object v8, p0, Lcom/google/android/music/ui/songza/SubSituationFragment$CuratedRadiosRVAdapter;->mCursor:Landroid/database/Cursor;

    const/4 v9, 0x1

    invoke-interface {v8, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 335
    .local v7, "title":Ljava/lang/String;
    invoke-virtual {v1, v7}, Lcom/google/android/music/ui/cardlib/model/Document;->setTitle(Ljava/lang/String;)V

    .line 338
    .end local v7    # "title":Ljava/lang/String;
    :cond_2
    iget-object v8, p0, Lcom/google/android/music/ui/songza/SubSituationFragment$CuratedRadiosRVAdapter;->mCursor:Landroid/database/Cursor;

    const/4 v9, 0x3

    invoke-interface {v8, v9}, Landroid/database/Cursor;->isNull(I)Z

    move-result v8

    if-nez v8, :cond_3

    .line 339
    iget-object v8, p0, Lcom/google/android/music/ui/songza/SubSituationFragment$CuratedRadiosRVAdapter;->mCursor:Landroid/database/Cursor;

    const/4 v9, 0x3

    invoke-interface {v8, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 340
    .local v0, "artUrl":Ljava/lang/String;
    invoke-virtual {v1, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setArtUrl(Ljava/lang/String;)V

    .line 343
    .end local v0    # "artUrl":Ljava/lang/String;
    :cond_3
    iget-object v8, p0, Lcom/google/android/music/ui/songza/SubSituationFragment$CuratedRadiosRVAdapter;->mCursor:Landroid/database/Cursor;

    const/4 v9, 0x4

    invoke-interface {v8, v9}, Landroid/database/Cursor;->isNull(I)Z

    move-result v8

    if-eqz v8, :cond_5

    const/4 v5, 0x0

    .line 345
    .local v5, "radioSeedId":Ljava/lang/String;
    :goto_1
    invoke-virtual {v1, v5}, Lcom/google/android/music/ui/cardlib/model/Document;->setRadioSeedId(Ljava/lang/String;)V

    .line 346
    iget-object v8, p0, Lcom/google/android/music/ui/songza/SubSituationFragment$CuratedRadiosRVAdapter;->mCursor:Landroid/database/Cursor;

    const/4 v9, 0x5

    invoke-interface {v8, v9}, Landroid/database/Cursor;->isNull(I)Z

    move-result v8

    if-eqz v8, :cond_6

    const/4 v6, -0x1

    .line 348
    .local v6, "radioSeedType":I
    :goto_2
    invoke-virtual {v1, v6}, Lcom/google/android/music/ui/cardlib/model/Document;->setRadioSeedType(I)V

    .line 349
    iget-object v8, p0, Lcom/google/android/music/ui/songza/SubSituationFragment$CuratedRadiosRVAdapter;->mCursor:Landroid/database/Cursor;

    const/4 v9, 0x6

    invoke-interface {v8, v9}, Landroid/database/Cursor;->isNull(I)Z

    move-result v8

    if-eqz v8, :cond_7

    const/4 v4, 0x0

    .line 351
    .local v4, "radioRemoteId":Ljava/lang/String;
    :goto_3
    invoke-virtual {v1, v4}, Lcom/google/android/music/ui/cardlib/model/Document;->setRadioRemoteId(Ljava/lang/String;)V

    .line 353
    iget-object v8, p0, Lcom/google/android/music/ui/songza/SubSituationFragment$CuratedRadiosRVAdapter;->mCursor:Landroid/database/Cursor;

    const/4 v9, 0x2

    invoke-interface {v8, v9}, Landroid/database/Cursor;->isNull(I)Z

    move-result v8

    if-nez v8, :cond_4

    .line 354
    iget-object v8, p0, Lcom/google/android/music/ui/songza/SubSituationFragment$CuratedRadiosRVAdapter;->mCursor:Landroid/database/Cursor;

    const/4 v9, 0x2

    invoke-interface {v8, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v8}, Lcom/google/android/music/ui/cardlib/model/Document;->setDescription(Ljava/lang/String;)V

    .line 357
    :cond_4
    check-cast v1, Lcom/google/android/music/ui/songza/SubSituationFragment$CuratedRadioDocument;

    .end local v1    # "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    iget-object v8, p0, Lcom/google/android/music/ui/songza/SubSituationFragment$CuratedRadiosRVAdapter;->this$0:Lcom/google/android/music/ui/songza/SubSituationFragment;

    # getter for: Lcom/google/android/music/ui/songza/SubSituationFragment;->mExpandedRadioIndices:[Z
    invoke-static {v8}, Lcom/google/android/music/ui/songza/SubSituationFragment;->access$300(Lcom/google/android/music/ui/songza/SubSituationFragment;)[Z

    move-result-object v8

    aget-boolean v8, v8, p2

    iput-boolean v8, v1, Lcom/google/android/music/ui/songza/SubSituationFragment$CuratedRadioDocument;->mIsExpanded:Z

    goto/16 :goto_0

    .line 343
    .end local v4    # "radioRemoteId":Ljava/lang/String;
    .end local v5    # "radioSeedId":Ljava/lang/String;
    .end local v6    # "radioSeedType":I
    .restart local v1    # "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    :cond_5
    iget-object v8, p0, Lcom/google/android/music/ui/songza/SubSituationFragment$CuratedRadiosRVAdapter;->mCursor:Landroid/database/Cursor;

    const/4 v9, 0x4

    invoke-interface {v8, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    .line 346
    .restart local v5    # "radioSeedId":Ljava/lang/String;
    :cond_6
    iget-object v8, p0, Lcom/google/android/music/ui/songza/SubSituationFragment$CuratedRadiosRVAdapter;->mCursor:Landroid/database/Cursor;

    const/4 v9, 0x5

    invoke-interface {v8, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    goto :goto_2

    .line 349
    .restart local v6    # "radioSeedType":I
    :cond_7
    iget-object v8, p0, Lcom/google/android/music/ui/songza/SubSituationFragment$CuratedRadiosRVAdapter;->mCursor:Landroid/database/Cursor;

    const/4 v9, 0x6

    invoke-interface {v8, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_3
.end method


# virtual methods
.method public getItemCount()I
    .locals 1

    .prologue
    .line 251
    iget-object v0, p0, Lcom/google/android/music/ui/songza/SubSituationFragment$CuratedRadiosRVAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/ui/songza/SubSituationFragment$CuratedRadiosRVAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/music/ui/songza/SubSituationFragment$CuratedRadiosRVAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 246
    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 256
    if-nez p1, :cond_0

    .line 257
    const/4 v0, 0x0

    .line 259
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 4
    .param p1, "holder"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .param p2, "position"    # I

    .prologue
    .line 265
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView$ViewHolder;->getItemViewType()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 278
    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    move-object v0, p1

    .line 269
    check-cast v0, Lcom/google/android/music/ui/PlayCardViewHolder;

    .line 270
    .local v0, "playCardViewHolder":Lcom/google/android/music/ui/PlayCardViewHolder;
    add-int/lit8 v1, p2, -0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/ui/songza/SubSituationFragment$CuratedRadiosRVAdapter;->bindDocument(Lcom/google/android/music/ui/PlayCardViewHolder;I)V

    .line 271
    iget-object v1, v0, Lcom/google/android/music/ui/PlayCardViewHolder;->playCardView:Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    iget-object v2, v0, Lcom/google/android/music/ui/PlayCardViewHolder;->document:Lcom/google/android/music/ui/cardlib/model/Document;

    iget-object v3, p0, Lcom/google/android/music/ui/songza/SubSituationFragment$CuratedRadiosRVAdapter;->mFragment:Lcom/google/android/music/ui/songza/SubSituationFragment;

    # getter for: Lcom/google/android/music/ui/songza/SubSituationFragment;->mCardContextMenuDelegate:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$DocumentContextMenuDelegate;
    invoke-static {v3}, Lcom/google/android/music/ui/songza/SubSituationFragment;->access$000(Lcom/google/android/music/ui/songza/SubSituationFragment;)Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$DocumentContextMenuDelegate;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->bind(Lcom/google/android/music/ui/cardlib/model/Document;Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;)V

    .line 273
    iget-object v1, p0, Lcom/google/android/music/ui/songza/SubSituationFragment$CuratedRadiosRVAdapter;->mFragment:Lcom/google/android/music/ui/songza/SubSituationFragment;

    invoke-virtual {v1}, Lcom/google/android/music/ui/songza/SubSituationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/music/eventlog/MusicEventLogger;->getInstance(Landroid/content/Context;)Lcom/google/android/music/eventlog/MusicEventLogger;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/music/ui/PlayCardViewHolder;->document:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v1, v2}, Lcom/google/android/music/eventlog/MusicEventLogger;->logMainstageDocumentDisplayed(Lcom/google/android/music/ui/cardlib/model/Document;)V

    goto :goto_0

    .line 265
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 307
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/ui/PlayCardViewHolder;

    .line 308
    .local v0, "playCardViewHolder":Lcom/google/android/music/ui/PlayCardViewHolder;
    iget-object v1, p0, Lcom/google/android/music/ui/songza/SubSituationFragment$CuratedRadiosRVAdapter;->mFragment:Lcom/google/android/music/ui/songza/SubSituationFragment;

    invoke-virtual {v1}, Lcom/google/android/music/ui/songza/SubSituationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/music/ui/PlayCardViewHolder;->document:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-static {v1, v2, p1}, Lcom/google/android/music/ui/cardlib/model/DocumentClickHandler;->onDocumentClick(Landroid/content/Context;Lcom/google/android/music/ui/cardlib/model/Document;Landroid/view/View;)V

    .line 310
    return-void
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 7
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I

    .prologue
    const/4 v6, 0x0

    .line 282
    packed-switch p2, :pswitch_data_0

    .line 301
    const/4 v1, 0x0

    :goto_0
    return-object v1

    .line 284
    :pswitch_0
    iget-object v4, p0, Lcom/google/android/music/ui/songza/SubSituationFragment$CuratedRadiosRVAdapter;->mFragment:Lcom/google/android/music/ui/songza/SubSituationFragment;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    const v5, 0x7f0400e4

    invoke-virtual {v3, v5, p1, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    # setter for: Lcom/google/android/music/ui/songza/SubSituationFragment;->mSubSituationTitleView:Landroid/widget/TextView;
    invoke-static {v4, v3}, Lcom/google/android/music/ui/songza/SubSituationFragment;->access$102(Lcom/google/android/music/ui/songza/SubSituationFragment;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 287
    iget-object v3, p0, Lcom/google/android/music/ui/songza/SubSituationFragment$CuratedRadiosRVAdapter;->mFragment:Lcom/google/android/music/ui/songza/SubSituationFragment;

    # getter for: Lcom/google/android/music/ui/songza/SubSituationFragment;->mSubSituationTitleView:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/google/android/music/ui/songza/SubSituationFragment;->access$100(Lcom/google/android/music/ui/songza/SubSituationFragment;)Landroid/widget/TextView;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/music/ui/songza/SubSituationFragment$CuratedRadiosRVAdapter;->mFragment:Lcom/google/android/music/ui/songza/SubSituationFragment;

    # getter for: Lcom/google/android/music/ui/songza/SubSituationFragment;->mSubSituationTitle:Ljava/lang/String;
    invoke-static {v4}, Lcom/google/android/music/ui/songza/SubSituationFragment;->access$200(Lcom/google/android/music/ui/songza/SubSituationFragment;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 288
    new-instance v1, Lcom/google/android/music/ui/songza/SubSituationFragment$SubSituationTitleViewHolder;

    iget-object v3, p0, Lcom/google/android/music/ui/songza/SubSituationFragment$CuratedRadiosRVAdapter;->mFragment:Lcom/google/android/music/ui/songza/SubSituationFragment;

    # getter for: Lcom/google/android/music/ui/songza/SubSituationFragment;->mSubSituationTitleView:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/google/android/music/ui/songza/SubSituationFragment;->access$100(Lcom/google/android/music/ui/songza/SubSituationFragment;)Landroid/widget/TextView;

    move-result-object v3

    invoke-direct {v1, v3}, Lcom/google/android/music/ui/songza/SubSituationFragment$SubSituationTitleViewHolder;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 290
    :pswitch_1
    sget-object v2, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->CARD_CURATED_RADIO:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    .line 292
    .local v2, "metadata":Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    invoke-virtual {v2}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->getLayoutId()I

    move-result v4

    invoke-virtual {v3, v4, p1, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;

    .line 295
    .local v0, "card":Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;
    invoke-virtual {v0, p0}, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->setOnExpandListener(Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard$OnExpandListener;)V

    .line 296
    invoke-virtual {v0, p0}, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 297
    invoke-virtual {v2}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->getThumbnailAspectRatio()F

    move-result v3

    invoke-virtual {v0, v3}, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->setThumbnailAspectRatio(F)V

    .line 298
    new-instance v1, Lcom/google/android/music/ui/PlayCardViewHolder;

    invoke-direct {v1, v0}, Lcom/google/android/music/ui/PlayCardViewHolder;-><init>(Landroid/view/View;)V

    .line 299
    .local v1, "holder":Lcom/google/android/music/ui/PlayCardViewHolder;
    goto :goto_0

    .line 282
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onExpand(Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;)V
    .locals 4
    .param p1, "card"    # Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;

    .prologue
    .line 362
    invoke-virtual {p1}, Lcom/google/android/music/ui/cardlib/layout/CuratedRadioPlayCard;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/ui/PlayCardViewHolder;

    .line 365
    .local v0, "holder":Lcom/google/android/music/ui/PlayCardViewHolder;
    iget-object v1, p0, Lcom/google/android/music/ui/songza/SubSituationFragment$CuratedRadiosRVAdapter;->this$0:Lcom/google/android/music/ui/songza/SubSituationFragment;

    # getter for: Lcom/google/android/music/ui/songza/SubSituationFragment;->mExpandedRadioIndices:[Z
    invoke-static {v1}, Lcom/google/android/music/ui/songza/SubSituationFragment;->access$300(Lcom/google/android/music/ui/songza/SubSituationFragment;)[Z

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/music/ui/PlayCardViewHolder;->getPosition()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    const/4 v3, 0x1

    aput-boolean v3, v1, v2

    .line 366
    invoke-virtual {v0}, Lcom/google/android/music/ui/PlayCardViewHolder;->getPosition()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/music/ui/songza/SubSituationFragment$CuratedRadiosRVAdapter;->notifyItemChanged(I)V

    .line 367
    return-void
.end method

.method public swapCursor(Landroid/database/Cursor;)V
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 313
    iput-object p1, p0, Lcom/google/android/music/ui/songza/SubSituationFragment$CuratedRadiosRVAdapter;->mCursor:Landroid/database/Cursor;

    .line 314
    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 316
    iget-object v0, p0, Lcom/google/android/music/ui/songza/SubSituationFragment$CuratedRadiosRVAdapter;->mFragment:Lcom/google/android/music/ui/songza/SubSituationFragment;

    invoke-virtual {v0}, Lcom/google/android/music/ui/songza/SubSituationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/music/ui/AppNavigation;->goListenNow(Landroid/content/Context;)V

    .line 320
    :goto_0
    return-void

    .line 318
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/music/ui/songza/SubSituationFragment$CuratedRadiosRVAdapter;->notifyDataSetChanged()V

    goto :goto_0
.end method
