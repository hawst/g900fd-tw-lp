.class public Lcom/google/android/music/widgets/ExpandingScrollView;
.super Lcom/google/android/music/widgets/ScrollableViewGroup;
.source "ExpandingScrollView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/widgets/ExpandingScrollView$3;,
        Lcom/google/android/music/widgets/ExpandingScrollView$ConsistentTouchEventDispatcher;,
        Lcom/google/android/music/widgets/ExpandingScrollView$NoTwoThirdsExpandingStateTransition;,
        Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingStateTransition;,
        Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;,
        Lcom/google/android/music/widgets/ExpandingScrollView$SavedState;,
        Lcom/google/android/music/widgets/ExpandingScrollView$ScrollState;,
        Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingScrollViewListener;
    }
.end annotation


# static fields
.field public static final DEFAULT_EXPANDING_STATE_TRANSITION:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingStateTransition;

.field public static final NO_TWO_THIRDS_EXPANDING_STATE_TRANSITION:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingStateTransition;

.field private static final bounceInterpolator:Landroid/view/animation/Interpolator;

.field private static initialized:Z


# instance fields
.field private alwaysExpanded:Z

.field private animateInRunnable:Ljava/lang/Runnable;

.field private childDispatcher:Lcom/google/android/music/widgets/ExpandingScrollView$ConsistentTouchEventDispatcher;

.field private childHasScrolled:Z

.field private coercedState:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

.field private collapsedHeaderViewId:I

.field private contentView:Landroid/view/View;

.field private desiredState:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

.field private expandingState:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

.field private expandingStateTransition:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingStateTransition;

.field private final exposurePercentages:[F

.field private lastChildDownY:F

.field private lastDownX:F

.field private lastDownY:F

.field private lastDownYOffset:F

.field private lastY:F

.field private listeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingScrollViewListener;",
            ">;"
        }
    .end annotation
.end field

.field private maxExpandPixels:I

.field private minExposure:I

.field private pagingTouchSlop:I

.field private parentDispatcher:Lcom/google/android/music/widgets/ExpandingScrollView$ConsistentTouchEventDispatcher;

.field private portraitExpandingStateTransition:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingStateTransition;

.field private scrollState:Lcom/google/android/music/widgets/ExpandingScrollView$ScrollState;

.field private shadow:Landroid/graphics/drawable/Drawable;

.field private shadowHeight:I

.field private touchSlop:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    new-instance v0, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingStateTransition;

    invoke-direct {v0}, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingStateTransition;-><init>()V

    sput-object v0, Lcom/google/android/music/widgets/ExpandingScrollView;->DEFAULT_EXPANDING_STATE_TRANSITION:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingStateTransition;

    .line 43
    new-instance v0, Lcom/google/android/music/widgets/ExpandingScrollView$NoTwoThirdsExpandingStateTransition;

    invoke-direct {v0}, Lcom/google/android/music/widgets/ExpandingScrollView$NoTwoThirdsExpandingStateTransition;-><init>()V

    sput-object v0, Lcom/google/android/music/widgets/ExpandingScrollView;->NO_TWO_THIRDS_EXPANDING_STATE_TRANSITION:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingStateTransition;

    .line 149
    new-instance v0, Landroid/view/animation/OvershootInterpolator;

    invoke-direct {v0}, Landroid/view/animation/OvershootInterpolator;-><init>()V

    sput-object v0, Lcom/google/android/music/widgets/ExpandingScrollView;->bounceInterpolator:Landroid/view/animation/Interpolator;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 265
    invoke-direct {p0, p1}, Lcom/google/android/music/widgets/ScrollableViewGroup;-><init>(Landroid/content/Context;)V

    .line 173
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->alwaysExpanded:Z

    .line 179
    sget-object v2, Lcom/google/android/music/widgets/ExpandingScrollView$ScrollState;->NO_SCROLL:Lcom/google/android/music/widgets/ExpandingScrollView$ScrollState;

    iput-object v2, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->scrollState:Lcom/google/android/music/widgets/ExpandingScrollView$ScrollState;

    .line 226
    sget-object v2, Lcom/google/android/music/widgets/ExpandingScrollView;->DEFAULT_EXPANDING_STATE_TRANSITION:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingStateTransition;

    iput-object v2, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->expandingStateTransition:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingStateTransition;

    .line 229
    sget-object v2, Lcom/google/android/music/widgets/ExpandingScrollView;->DEFAULT_EXPANDING_STATE_TRANSITION:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingStateTransition;

    iput-object v2, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->portraitExpandingStateTransition:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingStateTransition;

    .line 233
    sget-object v2, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->HIDDEN:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    iput-object v2, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->expandingState:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    .line 249
    invoke-static {}, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->values()[Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    move-result-object v2

    array-length v2, v2

    new-array v2, v2, [F

    iput-object v2, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->exposurePercentages:[F

    .line 252
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->listeners:Ljava/util/List;

    .line 277
    invoke-virtual {p0}, Lcom/google/android/music/widgets/ExpandingScrollView;->getContext()Landroid/content/Context;

    move-result-object p1

    .line 278
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 279
    .local v1, "res":Landroid/content/res/Resources;
    sget-boolean v2, Lcom/google/android/music/widgets/ExpandingScrollView;->initialized:Z

    if-nez v2, :cond_0

    .line 280
    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/music/widgets/ExpandingScrollView;->updateStateTransition(Landroid/content/res/Configuration;)V

    .line 281
    const/4 v2, 0x1

    sput-boolean v2, Lcom/google/android/music/widgets/ExpandingScrollView;->initialized:Z

    .line 284
    :cond_0
    const v2, 0x7f02004b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->shadow:Landroid/graphics/drawable/Drawable;

    .line 285
    const v2, 0x7f0f00d1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->shadowHeight:I

    .line 287
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 288
    .local v0, "configuration":Landroid/view/ViewConfiguration;
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v2

    iput v2, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->touchSlop:I

    .line 289
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledPagingTouchSlop()I

    move-result v2

    iput v2, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->pagingTouchSlop:I

    .line 291
    new-instance v2, Lcom/google/android/music/widgets/ExpandingScrollView$ConsistentTouchEventDispatcher;

    new-instance v3, Lcom/google/android/music/widgets/ExpandingScrollView$1;

    invoke-direct {v3, p0}, Lcom/google/android/music/widgets/ExpandingScrollView$1;-><init>(Lcom/google/android/music/widgets/ExpandingScrollView;)V

    invoke-direct {v2, v3}, Lcom/google/android/music/widgets/ExpandingScrollView$ConsistentTouchEventDispatcher;-><init>(Lcom/google/android/music/widgets/ExpandingScrollView$ConsistentTouchEventDispatcher$TouchEventDispatcher;)V

    iput-object v2, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->parentDispatcher:Lcom/google/android/music/widgets/ExpandingScrollView$ConsistentTouchEventDispatcher;

    .line 299
    invoke-virtual {p0}, Lcom/google/android/music/widgets/ExpandingScrollView;->resetExposurePercentages()V

    .line 266
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 269
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/widgets/ScrollableViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 173
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->alwaysExpanded:Z

    .line 179
    sget-object v2, Lcom/google/android/music/widgets/ExpandingScrollView$ScrollState;->NO_SCROLL:Lcom/google/android/music/widgets/ExpandingScrollView$ScrollState;

    iput-object v2, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->scrollState:Lcom/google/android/music/widgets/ExpandingScrollView$ScrollState;

    .line 226
    sget-object v2, Lcom/google/android/music/widgets/ExpandingScrollView;->DEFAULT_EXPANDING_STATE_TRANSITION:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingStateTransition;

    iput-object v2, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->expandingStateTransition:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingStateTransition;

    .line 229
    sget-object v2, Lcom/google/android/music/widgets/ExpandingScrollView;->DEFAULT_EXPANDING_STATE_TRANSITION:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingStateTransition;

    iput-object v2, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->portraitExpandingStateTransition:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingStateTransition;

    .line 233
    sget-object v2, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->HIDDEN:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    iput-object v2, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->expandingState:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    .line 249
    invoke-static {}, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->values()[Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    move-result-object v2

    array-length v2, v2

    new-array v2, v2, [F

    iput-object v2, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->exposurePercentages:[F

    .line 252
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->listeners:Ljava/util/List;

    .line 277
    invoke-virtual {p0}, Lcom/google/android/music/widgets/ExpandingScrollView;->getContext()Landroid/content/Context;

    move-result-object p1

    .line 278
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 279
    .local v1, "res":Landroid/content/res/Resources;
    sget-boolean v2, Lcom/google/android/music/widgets/ExpandingScrollView;->initialized:Z

    if-nez v2, :cond_0

    .line 280
    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/music/widgets/ExpandingScrollView;->updateStateTransition(Landroid/content/res/Configuration;)V

    .line 281
    const/4 v2, 0x1

    sput-boolean v2, Lcom/google/android/music/widgets/ExpandingScrollView;->initialized:Z

    .line 284
    :cond_0
    const v2, 0x7f02004b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->shadow:Landroid/graphics/drawable/Drawable;

    .line 285
    const v2, 0x7f0f00d1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->shadowHeight:I

    .line 287
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 288
    .local v0, "configuration":Landroid/view/ViewConfiguration;
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v2

    iput v2, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->touchSlop:I

    .line 289
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledPagingTouchSlop()I

    move-result v2

    iput v2, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->pagingTouchSlop:I

    .line 291
    new-instance v2, Lcom/google/android/music/widgets/ExpandingScrollView$ConsistentTouchEventDispatcher;

    new-instance v3, Lcom/google/android/music/widgets/ExpandingScrollView$1;

    invoke-direct {v3, p0}, Lcom/google/android/music/widgets/ExpandingScrollView$1;-><init>(Lcom/google/android/music/widgets/ExpandingScrollView;)V

    invoke-direct {v2, v3}, Lcom/google/android/music/widgets/ExpandingScrollView$ConsistentTouchEventDispatcher;-><init>(Lcom/google/android/music/widgets/ExpandingScrollView$ConsistentTouchEventDispatcher$TouchEventDispatcher;)V

    iput-object v2, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->parentDispatcher:Lcom/google/android/music/widgets/ExpandingScrollView$ConsistentTouchEventDispatcher;

    .line 299
    invoke-virtual {p0}, Lcom/google/android/music/widgets/ExpandingScrollView;->resetExposurePercentages()V

    .line 270
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 273
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/music/widgets/ScrollableViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 173
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->alwaysExpanded:Z

    .line 179
    sget-object v2, Lcom/google/android/music/widgets/ExpandingScrollView$ScrollState;->NO_SCROLL:Lcom/google/android/music/widgets/ExpandingScrollView$ScrollState;

    iput-object v2, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->scrollState:Lcom/google/android/music/widgets/ExpandingScrollView$ScrollState;

    .line 226
    sget-object v2, Lcom/google/android/music/widgets/ExpandingScrollView;->DEFAULT_EXPANDING_STATE_TRANSITION:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingStateTransition;

    iput-object v2, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->expandingStateTransition:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingStateTransition;

    .line 229
    sget-object v2, Lcom/google/android/music/widgets/ExpandingScrollView;->DEFAULT_EXPANDING_STATE_TRANSITION:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingStateTransition;

    iput-object v2, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->portraitExpandingStateTransition:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingStateTransition;

    .line 233
    sget-object v2, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->HIDDEN:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    iput-object v2, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->expandingState:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    .line 249
    invoke-static {}, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->values()[Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    move-result-object v2

    array-length v2, v2

    new-array v2, v2, [F

    iput-object v2, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->exposurePercentages:[F

    .line 252
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->listeners:Ljava/util/List;

    .line 277
    invoke-virtual {p0}, Lcom/google/android/music/widgets/ExpandingScrollView;->getContext()Landroid/content/Context;

    move-result-object p1

    .line 278
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 279
    .local v1, "res":Landroid/content/res/Resources;
    sget-boolean v2, Lcom/google/android/music/widgets/ExpandingScrollView;->initialized:Z

    if-nez v2, :cond_0

    .line 280
    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/music/widgets/ExpandingScrollView;->updateStateTransition(Landroid/content/res/Configuration;)V

    .line 281
    const/4 v2, 0x1

    sput-boolean v2, Lcom/google/android/music/widgets/ExpandingScrollView;->initialized:Z

    .line 284
    :cond_0
    const v2, 0x7f02004b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->shadow:Landroid/graphics/drawable/Drawable;

    .line 285
    const v2, 0x7f0f00d1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->shadowHeight:I

    .line 287
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 288
    .local v0, "configuration":Landroid/view/ViewConfiguration;
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v2

    iput v2, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->touchSlop:I

    .line 289
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledPagingTouchSlop()I

    move-result v2

    iput v2, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->pagingTouchSlop:I

    .line 291
    new-instance v2, Lcom/google/android/music/widgets/ExpandingScrollView$ConsistentTouchEventDispatcher;

    new-instance v3, Lcom/google/android/music/widgets/ExpandingScrollView$1;

    invoke-direct {v3, p0}, Lcom/google/android/music/widgets/ExpandingScrollView$1;-><init>(Lcom/google/android/music/widgets/ExpandingScrollView;)V

    invoke-direct {v2, v3}, Lcom/google/android/music/widgets/ExpandingScrollView$ConsistentTouchEventDispatcher;-><init>(Lcom/google/android/music/widgets/ExpandingScrollView$ConsistentTouchEventDispatcher$TouchEventDispatcher;)V

    iput-object v2, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->parentDispatcher:Lcom/google/android/music/widgets/ExpandingScrollView$ConsistentTouchEventDispatcher;

    .line 299
    invoke-virtual {p0}, Lcom/google/android/music/widgets/ExpandingScrollView;->resetExposurePercentages()V

    .line 274
    return-void
.end method

.method static synthetic access$101(Lcom/google/android/music/widgets/ExpandingScrollView;Landroid/view/MotionEvent;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/widgets/ExpandingScrollView;
    .param p1, "x1"    # Landroid/view/MotionEvent;

    .prologue
    .line 37
    invoke-super {p0, p1}, Lcom/google/android/music/widgets/ScrollableViewGroup;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/google/android/music/widgets/ExpandingScrollView;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/widgets/ExpandingScrollView;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->contentView:Landroid/view/View;

    return-object v0
.end method

.method private getHeaderHeight()I
    .locals 2

    .prologue
    .line 467
    iget v1, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->collapsedHeaderViewId:I

    invoke-virtual {p0, v1}, Lcom/google/android/music/widgets/ExpandingScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 468
    .local v0, "contentHeader":Landroid/view/View;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v1

    goto :goto_0
.end method

.method private getPreviousExpandingState(Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;)Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;
    .locals 1
    .param p1, "state"    # Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    .prologue
    .line 805
    iget-object v0, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->expandingStateTransition:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingStateTransition;

    invoke-virtual {v0, p1}, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingStateTransition;->getPrevious(Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;)Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    move-result-object v0

    return-object v0
.end method

.method private nearestState(I)Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;
    .locals 6
    .param p1, "scroll"    # I

    .prologue
    .line 657
    const/4 v3, 0x0

    .line 658
    .local v3, "nearest":Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;
    const v2, 0x7fffffff

    .line 659
    .local v2, "minDelta":I
    iget-object v5, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->expandingStateTransition:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingStateTransition;

    invoke-virtual {v5}, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingStateTransition;->getScrollableStates()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    .line 660
    .local v4, "state":Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;
    invoke-virtual {p0, v4}, Lcom/google/android/music/widgets/ExpandingScrollView;->getExposurePixels(Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;)I

    move-result v5

    sub-int/2addr v5, p1

    invoke-static {v5}, Ljava/lang/Math;->abs(I)I

    move-result v0

    .line 661
    .local v0, "delta":I
    if-ge v0, v2, :cond_0

    .line 662
    move v2, v0

    .line 663
    move-object v3, v4

    goto :goto_0

    .line 666
    .end local v0    # "delta":I
    .end local v4    # "state":Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;
    :cond_1
    return-object v3
.end method

.method private notifyOnMoving(Ljava/lang/Iterable;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingScrollViewListener;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 883
    .local p1, "listeners":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingScrollViewListener;>;"
    invoke-virtual {p0}, Lcom/google/android/music/widgets/ExpandingScrollView;->getScroll()I

    move-result v9

    .line 884
    .local v9, "scrollPosition":I
    invoke-static {}, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->values()[Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    move-result-object v11

    const/4 v12, 0x0

    aget-object v1, v11, v12

    .line 885
    .local v1, "baseState":Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;
    invoke-static {}, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->values()[Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    move-result-object v0

    .local v0, "arr$":[Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_0

    aget-object v10, v0, v3

    .line 886
    .local v10, "state":Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;
    invoke-virtual {p0, v10}, Lcom/google/android/music/widgets/ExpandingScrollView;->getExposurePixels(Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;)I

    move-result v11

    if-ge v9, v11, :cond_1

    .line 893
    .end local v10    # "state":Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;
    :cond_0
    iget-object v11, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->exposurePercentages:[F

    invoke-virtual {v1}, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->ordinal()I

    move-result v12

    aget v11, v11, v12

    const/high16 v12, 0x42c80000    # 100.0f

    cmpl-float v11, v11, v12

    if-nez v11, :cond_2

    .line 894
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingScrollViewListener;

    .line 895
    .local v5, "listener":Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingScrollViewListener;
    const/4 v11, 0x0

    invoke-interface {v5, p0, v1, v11}, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingScrollViewListener;->onMoving(Lcom/google/android/music/widgets/ExpandingScrollView;Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;F)V

    goto :goto_1

    .line 889
    .end local v5    # "listener":Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingScrollViewListener;
    .local v3, "i$":I
    .restart local v10    # "state":Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;
    :cond_1
    move-object v1, v10

    .line 885
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 898
    .end local v10    # "state":Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;
    :cond_2
    invoke-virtual {p0, v1}, Lcom/google/android/music/widgets/ExpandingScrollView;->getExposurePixels(Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;)I

    move-result v2

    .line 899
    .local v2, "baseStatePosition":I
    sget-object v11, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->HIDDEN:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    if-ne v1, v11, :cond_3

    sget-object v6, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->COLLAPSED:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    .line 902
    .local v6, "nextState":Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;
    :goto_2
    invoke-virtual {p0, v6}, Lcom/google/android/music/widgets/ExpandingScrollView;->getExposurePixels(Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;)I

    move-result v7

    .line 903
    .local v7, "nextStatePosition":I
    int-to-float v11, v9

    int-to-float v12, v2

    sub-float/2addr v11, v12

    sub-int v12, v7, v2

    int-to-float v12, v12

    div-float v8, v11, v12

    .line 905
    .local v8, "ratio":F
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingScrollViewListener;

    .line 906
    .restart local v5    # "listener":Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingScrollViewListener;
    invoke-interface {v5, p0, v1, v8}, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingScrollViewListener;->onMoving(Lcom/google/android/music/widgets/ExpandingScrollView;Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;F)V

    goto :goto_3

    .line 899
    .end local v5    # "listener":Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingScrollViewListener;
    .end local v6    # "nextState":Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;
    .end local v7    # "nextStatePosition":I
    .end local v8    # "ratio":F
    .local v3, "i$":I
    :cond_3
    invoke-virtual {p0, v1}, Lcom/google/android/music/widgets/ExpandingScrollView;->getNextExpandingState(Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;)Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    move-result-object v6

    goto :goto_2

    .line 909
    .end local v2    # "baseStatePosition":I
    .local v3, "i$":Ljava/util/Iterator;
    :cond_4
    return-void
.end method

.method private scrollTo(IIZ)V
    .locals 1
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "notifyListeners"    # Z

    .prologue
    .line 851
    invoke-super {p0, p1, p2}, Lcom/google/android/music/widgets/ScrollableViewGroup;->scrollTo(II)V

    .line 852
    if-eqz p3, :cond_0

    iget-object v0, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->listeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 856
    :cond_0
    :goto_0
    return-void

    .line 855
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->listeners:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/google/android/music/widgets/ExpandingScrollView;->notifyOnMoving(Ljava/lang/Iterable;)V

    goto :goto_0
.end method

.method private setScrollLimits()V
    .locals 3

    .prologue
    .line 416
    iget-object v1, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->expandingState:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    sget-object v2, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->HIDDEN:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    if-ne v1, v2, :cond_0

    .line 417
    sget-object v1, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->HIDDEN:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    invoke-virtual {p0, v1}, Lcom/google/android/music/widgets/ExpandingScrollView;->getExposurePixels(Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;)I

    move-result v0

    .line 418
    .local v0, "hiddenExposurePixels":I
    invoke-virtual {p0, v0, v0}, Lcom/google/android/music/widgets/ExpandingScrollView;->setScrollLimits(II)V

    .line 423
    .end local v0    # "hiddenExposurePixels":I
    :goto_0
    return-void

    .line 420
    :cond_0
    sget-object v1, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->COLLAPSED:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    invoke-virtual {p0, v1}, Lcom/google/android/music/widgets/ExpandingScrollView;->getExposurePixels(Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;)I

    move-result v1

    sget-object v2, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->FULLY_EXPANDED:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    invoke-virtual {p0, v2}, Lcom/google/android/music/widgets/ExpandingScrollView;->getExposurePixels(Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;)I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/music/widgets/ExpandingScrollView;->setScrollLimits(II)V

    goto :goto_0
.end method

.method private updateStateTransition(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "config"    # Landroid/content/res/Configuration;

    .prologue
    .line 356
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/google/android/music/widgets/ExpandingScrollView;->NO_TWO_THIRDS_EXPANDING_STATE_TRANSITION:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingStateTransition;

    :goto_0
    iput-object v0, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->expandingStateTransition:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingStateTransition;

    .line 360
    iget-object v0, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->expandingState:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    invoke-virtual {p0, v0}, Lcom/google/android/music/widgets/ExpandingScrollView;->setExpandingState(Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;)V

    .line 361
    return-void

    .line 356
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->portraitExpandingStateTransition:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingStateTransition;

    goto :goto_0
.end method


# virtual methods
.method public addListener(Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingScrollViewListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingScrollViewListener;

    .prologue
    .line 836
    iget-object v0, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->listeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 837
    invoke-static {p1}, Lcom/google/common/collect/ImmutableList;->of(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/music/widgets/ExpandingScrollView;->notifyOnMoving(Ljava/lang/Iterable;)V

    .line 838
    iget-object v0, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->expandingState:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    iget-object v1, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->expandingState:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    invoke-interface {p1, p0, v0, v1}, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingScrollViewListener;->onExpandingStateChanged(Lcom/google/android/music/widgets/ExpandingScrollView;Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;)V

    .line 839
    return-void
.end method

.method public collapse()Z
    .locals 2

    .prologue
    .line 615
    sget-object v0, Lcom/google/android/music/widgets/ExpandingScrollView$3;->$SwitchMap$com$google$android$music$widgets$ExpandingScrollView$ExpandingState:[I

    iget-object v1, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->expandingState:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    invoke-virtual {v1}, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 620
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 617
    :pswitch_0
    sget-object v0, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->COLLAPSED:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    invoke-virtual {p0, v0}, Lcom/google/android/music/widgets/ExpandingScrollView;->moveToExpandingState(Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;)V

    .line 618
    const/4 v0, 0x1

    goto :goto_0

    .line 615
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 1
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 457
    iget-object v0, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->shadow:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 459
    invoke-super {p0, p1}, Lcom/google/android/music/widgets/ScrollableViewGroup;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 460
    return-void
.end method

.method dragEnded()V
    .locals 3

    .prologue
    .line 868
    invoke-super {p0}, Lcom/google/android/music/widgets/ScrollableViewGroup;->dragEnded()V

    .line 869
    iget-object v2, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->listeners:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingScrollViewListener;

    .line 870
    .local v1, "listener":Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingScrollViewListener;
    iget-object v2, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->expandingState:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    invoke-interface {v1, p0, v2}, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingScrollViewListener;->onDragEnded(Lcom/google/android/music/widgets/ExpandingScrollView;Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;)V

    goto :goto_0

    .line 872
    .end local v1    # "listener":Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingScrollViewListener;
    :cond_0
    return-void
.end method

.method dragStarted()V
    .locals 3

    .prologue
    .line 860
    invoke-super {p0}, Lcom/google/android/music/widgets/ScrollableViewGroup;->dragStarted()V

    .line 861
    iget-object v2, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->listeners:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingScrollViewListener;

    .line 862
    .local v1, "listener":Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingScrollViewListener;
    iget-object v2, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->expandingState:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    invoke-interface {v1, p0, v2}, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingScrollViewListener;->onDragStarted(Lcom/google/android/music/widgets/ExpandingScrollView;Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;)V

    goto :goto_0

    .line 864
    .end local v1    # "listener":Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingScrollViewListener;
    :cond_0
    return-void
.end method

.method public getContent()Landroid/view/View;
    .locals 1

    .prologue
    .line 303
    iget-object v0, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->contentView:Landroid/view/View;

    return-object v0
.end method

.method public getExpandingState()Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;
    .locals 1

    .prologue
    .line 878
    iget-object v0, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->expandingState:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    return-object v0
.end method

.method public getExposurePixels(Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;)I
    .locals 3
    .param p1, "state"    # Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    .prologue
    .line 728
    iget v0, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->maxExpandPixels:I

    int-to-float v0, v0

    iget-object v1, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->exposurePercentages:[F

    invoke-virtual {p1}, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->ordinal()I

    move-result v2

    aget v1, v1, v2

    mul-float/2addr v0, v1

    const/high16 v1, 0x42c80000    # 100.0f

    div-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method

.method getNextExpandingState(Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;)Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;
    .locals 1
    .param p1, "state"    # Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    .prologue
    .line 795
    iget-object v0, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->expandingStateTransition:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingStateTransition;

    invoke-virtual {v0, p1}, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingStateTransition;->getNext(Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;)Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    move-result-object v0

    return-object v0
.end method

.method public moveToExpandingState(Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;)V
    .locals 2
    .param p1, "newState"    # Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    .prologue
    .line 704
    invoke-virtual {p0, p1}, Lcom/google/android/music/widgets/ExpandingScrollView;->setExpandingState(Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;)V

    .line 705
    iget-object v0, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->expandingState:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    invoke-virtual {p0, v0}, Lcom/google/android/music/widgets/ExpandingScrollView;->getExposurePixels(Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;)I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/music/widgets/ExpandingScrollView;->smoothScrollTo(IZ)V

    .line 706
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 4
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const/4 v3, 0x0

    .line 365
    invoke-super {p0, p1}, Lcom/google/android/music/widgets/ScrollableViewGroup;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 369
    iget-object v0, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->expandingState:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    .line 371
    .local v0, "originalState":Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;
    invoke-direct {p0, p1}, Lcom/google/android/music/widgets/ExpandingScrollView;->updateStateTransition(Landroid/content/res/Configuration;)V

    .line 375
    iget-object v1, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->expandingState:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    if-eq v1, v0, :cond_1

    .line 376
    iput-object v0, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->desiredState:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    .line 377
    iget-object v1, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->expandingState:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    iput-object v1, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->coercedState:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    .line 386
    :cond_0
    :goto_0
    return-void

    .line 378
    :cond_1
    iget-object v1, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->desiredState:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->expandingStateTransition:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingStateTransition;

    invoke-virtual {v1}, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingStateTransition;->getScrollableStates()Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->desiredState:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 380
    iget-object v1, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->expandingState:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    iget-object v2, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->coercedState:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    if-ne v1, v2, :cond_2

    .line 381
    iget-object v1, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->desiredState:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    invoke-virtual {p0, v1}, Lcom/google/android/music/widgets/ExpandingScrollView;->setExpandingState(Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;)V

    .line 383
    :cond_2
    iput-object v3, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->desiredState:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    .line 384
    iput-object v3, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->coercedState:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 391
    iget-object v0, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->animateInRunnable:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 392
    iget-object v0, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->animateInRunnable:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/music/widgets/ExpandingScrollView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 393
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->animateInRunnable:Ljava/lang/Runnable;

    .line 395
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/music/widgets/ExpandingScrollView;->clearAnimation()V

    .line 397
    invoke-super {p0}, Lcom/google/android/music/widgets/ScrollableViewGroup;->onDetachedFromWindow()V

    .line 398
    return-void
.end method

.method public onHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 697
    const/4 v0, 0x0

    return v0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 475
    const/4 v0, 0x1

    return v0
.end method

.method public onLayout(ZIIII)V
    .locals 9
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    const/4 v4, 0x0

    .line 427
    iget-boolean v5, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->alwaysExpanded:Z

    if-eqz v5, :cond_0

    move p3, v4

    .line 429
    :goto_0
    iget-object v5, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->shadow:Landroid/graphics/drawable/Drawable;

    iget v6, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->shadowHeight:I

    add-int/2addr v6, p3

    invoke-virtual {v5, p2, p3, p4, v6}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 430
    iget v5, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->shadowHeight:I

    add-int/2addr p3, v5

    .line 432
    sub-int v3, p4, p2

    .line 433
    .local v3, "width":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/music/widgets/ExpandingScrollView;->getChildCount()I

    move-result v5

    if-ge v2, v5, :cond_1

    .line 434
    invoke-virtual {p0, v2}, Lcom/google/android/music/widgets/ExpandingScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 436
    .local v0, "child":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    add-int p5, p3, v5

    .line 437
    invoke-virtual {p0}, Lcom/google/android/music/widgets/ExpandingScrollView;->getPaddingRight()I

    move-result v5

    sub-int v1, v3, v5

    .line 438
    .local v1, "childRight":I
    invoke-virtual {p0}, Lcom/google/android/music/widgets/ExpandingScrollView;->getPaddingLeft()I

    move-result v5

    invoke-virtual {v0, v5, p3, v1, p5}, Landroid/view/View;->layout(IIII)V

    .line 439
    move p3, p5

    .line 433
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 427
    .end local v0    # "child":Landroid/view/View;
    .end local v1    # "childRight":I
    .end local v2    # "i":I
    .end local v3    # "width":I
    :cond_0
    iget p3, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->maxExpandPixels:I

    goto :goto_0

    .line 444
    .restart local v2    # "i":I
    .restart local v3    # "width":I
    :cond_1
    sget-object v5, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->COLLAPSED:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    invoke-direct {p0}, Lcom/google/android/music/widgets/ExpandingScrollView;->getHeaderHeight()I

    move-result v6

    invoke-virtual {p0}, Lcom/google/android/music/widgets/ExpandingScrollView;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0f00d1

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    add-int/2addr v6, v7

    invoke-virtual {p0, v5, v6}, Lcom/google/android/music/widgets/ExpandingScrollView;->setExposurePixels(Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;I)V

    .line 448
    invoke-direct {p0}, Lcom/google/android/music/widgets/ExpandingScrollView;->setScrollLimits()V

    .line 449
    iget-boolean v5, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->alwaysExpanded:Z

    if-eqz v5, :cond_2

    move v5, v4

    :goto_2
    invoke-direct {p0, v4, v5, v4}, Lcom/google/android/music/widgets/ExpandingScrollView;->scrollTo(IIZ)V

    .line 450
    return-void

    .line 449
    :cond_2
    iget-object v5, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->expandingState:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    invoke-virtual {p0, v5}, Lcom/google/android/music/widgets/ExpandingScrollView;->getExposurePixels(Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;)I

    move-result v5

    goto :goto_2
.end method

.method public onMeasure(II)V
    .locals 5
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 402
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 403
    .local v0, "height":I
    const/4 v4, 0x0

    iput v4, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->minExposure:I

    .line 404
    iget v4, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->minExposure:I

    sub-int v4, v0, v4

    iput v4, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->maxExpandPixels:I

    .line 405
    const/4 v3, 0x0

    .line 406
    .local v3, "myWidth":I
    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {v0, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 407
    .local v2, "maximumHeight":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/music/widgets/ExpandingScrollView;->getChildCount()I

    move-result v4

    if-ge v1, v4, :cond_0

    .line 408
    invoke-virtual {p0, v1}, Lcom/google/android/music/widgets/ExpandingScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, p1, v2}, Landroid/view/View;->measure(II)V

    .line 409
    invoke-virtual {p0, v1}, Lcom/google/android/music/widgets/ExpandingScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 407
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 412
    :cond_0
    invoke-virtual {p0, v3, v0}, Lcom/google/android/music/widgets/ExpandingScrollView;->setMeasuredDimension(II)V

    .line 413
    return-void
.end method

.method protected onMoveFinished(F)V
    .locals 8
    .param p1, "velocity"    # F

    .prologue
    .line 626
    iget-object v6, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->expandingState:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    sget-object v7, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->HIDDEN:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    if-ne v6, v7, :cond_0

    .line 654
    :goto_0
    return-void

    .line 632
    :cond_0
    const v6, 0x3e99999a    # 0.3f

    mul-float/2addr v6, p1

    invoke-virtual {p0}, Lcom/google/android/music/widgets/ExpandingScrollView;->getScroll()I

    move-result v7

    int-to-float v7, v7

    add-float/2addr v6, v7

    float-to-int v4, v6

    .line 633
    .local v4, "projectedScroll":I
    invoke-direct {p0, v4}, Lcom/google/android/music/widgets/ExpandingScrollView;->nearestState(I)Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    move-result-object v1

    .line 638
    .local v1, "destinationState":Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;
    iget-object v6, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->expandingState:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    if-ne v1, v6, :cond_1

    .line 639
    invoke-virtual {p0}, Lcom/google/android/music/widgets/ExpandingScrollView;->getScroll()I

    move-result v6

    iget-object v7, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->expandingState:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    invoke-virtual {p0, v7}, Lcom/google/android/music/widgets/ExpandingScrollView;->getExposurePixels(Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;)I

    move-result v7

    if-le v6, v7, :cond_2

    iget-object v6, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->expandingState:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    invoke-virtual {p0, v6}, Lcom/google/android/music/widgets/ExpandingScrollView;->getNextExpandingState(Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;)Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    move-result-object v2

    .line 642
    .local v2, "nextState":Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;
    :goto_1
    iget-object v6, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->expandingState:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    if-eq v2, v6, :cond_1

    .line 643
    iget-object v6, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->expandingState:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    invoke-virtual {p0, v6}, Lcom/google/android/music/widgets/ExpandingScrollView;->getExposurePixels(Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;)I

    move-result v0

    .line 644
    .local v0, "currentStateScroll":I
    invoke-virtual {p0, v2}, Lcom/google/android/music/widgets/ExpandingScrollView;->getExposurePixels(Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;)I

    move-result v3

    .line 645
    .local v3, "nextStateScroll":I
    invoke-virtual {p0}, Lcom/google/android/music/widgets/ExpandingScrollView;->getScroll()I

    move-result v6

    sub-int/2addr v6, v0

    int-to-float v6, v6

    sub-int v7, v3, v0

    int-to-float v7, v7

    div-float v5, v6, v7

    .line 647
    .local v5, "scrollFraction":F
    const v6, 0x3dcccccd    # 0.1f

    cmpl-float v6, v5, v6

    if-lez v6, :cond_1

    .line 648
    move-object v1, v2

    .line 653
    .end local v0    # "currentStateScroll":I
    .end local v2    # "nextState":Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;
    .end local v3    # "nextStateScroll":I
    .end local v5    # "scrollFraction":F
    :cond_1
    invoke-virtual {p0, v1}, Lcom/google/android/music/widgets/ExpandingScrollView;->moveToExpandingState(Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;)V

    goto :goto_0

    .line 639
    :cond_2
    iget-object v6, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->expandingState:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    invoke-direct {p0, v6}, Lcom/google/android/music/widgets/ExpandingScrollView;->getPreviousExpandingState(Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;)Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    move-result-object v2

    goto :goto_1
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 5
    .param p1, "state"    # Landroid/os/Parcelable;

    .prologue
    .line 677
    move-object v2, p1

    check-cast v2, Lcom/google/android/music/widgets/ExpandingScrollView$SavedState;

    .line 678
    .local v2, "ss":Lcom/google/android/music/widgets/ExpandingScrollView$SavedState;
    invoke-virtual {v2}, Lcom/google/android/music/widgets/ExpandingScrollView$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v3

    invoke-super {p0, v3}, Lcom/google/android/music/widgets/ScrollableViewGroup;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 679
    iget-object v3, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->listeners:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingScrollViewListener;

    .line 680
    .local v1, "listener":Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingScrollViewListener;
    iget-object v3, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->expandingState:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    # getter for: Lcom/google/android/music/widgets/ExpandingScrollView$SavedState;->expandingState:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;
    invoke-static {v2}, Lcom/google/android/music/widgets/ExpandingScrollView$SavedState;->access$300(Lcom/google/android/music/widgets/ExpandingScrollView$SavedState;)Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    move-result-object v4

    invoke-interface {v1, p0, v3, v4}, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingScrollViewListener;->onExpandingStateChanged(Lcom/google/android/music/widgets/ExpandingScrollView;Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;)V

    goto :goto_0

    .line 682
    .end local v1    # "listener":Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingScrollViewListener;
    :cond_0
    # getter for: Lcom/google/android/music/widgets/ExpandingScrollView$SavedState;->expandingState:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;
    invoke-static {v2}, Lcom/google/android/music/widgets/ExpandingScrollView$SavedState;->access$300(Lcom/google/android/music/widgets/ExpandingScrollView$SavedState;)Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->expandingState:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    .line 683
    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 4

    .prologue
    .line 671
    invoke-super {p0}, Lcom/google/android/music/widgets/ScrollableViewGroup;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 672
    .local v0, "state":Landroid/os/Parcelable;
    new-instance v1, Lcom/google/android/music/widgets/ExpandingScrollView$SavedState;

    iget-object v2, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->expandingState:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    iget-object v3, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->exposurePercentages:[F

    invoke-direct {v1, v0, v2, v3}, Lcom/google/android/music/widgets/ExpandingScrollView$SavedState;-><init>(Landroid/os/Parcelable;Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;[F)V

    return-object v1
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 13
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 480
    iget-object v8, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->expandingState:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    sget-object v9, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->HIDDEN:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    if-ne v8, v9, :cond_0

    .line 482
    const/4 v8, 0x0

    .line 569
    :goto_0
    return v8

    .line 485
    :cond_0
    iget v8, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->maxExpandPixels:I

    invoke-virtual {p0}, Lcom/google/android/music/widgets/ExpandingScrollView;->getScrollY()I

    move-result v9

    sub-int v5, v8, v9

    .line 486
    .local v5, "offsetY":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 487
    .local v0, "action":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    .line 488
    .local v6, "x":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v7

    .line 490
    .local v7, "y":F
    if-nez v0, :cond_1

    .line 492
    int-to-float v8, v5

    cmpl-float v8, v7, v8

    if-ltz v8, :cond_2

    sget-object v8, Lcom/google/android/music/widgets/ExpandingScrollView$ScrollState;->START_TOUCH:Lcom/google/android/music/widgets/ExpandingScrollView$ScrollState;

    :goto_1
    iput-object v8, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->scrollState:Lcom/google/android/music/widgets/ExpandingScrollView$ScrollState;

    .line 494
    iput v6, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->lastDownX:F

    .line 495
    iput v7, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->lastDownY:F

    .line 496
    int-to-float v8, v5

    sub-float v8, v7, v8

    iput v8, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->lastDownYOffset:F

    .line 497
    const/high16 v8, -0x40800000    # -1.0f

    iput v8, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->lastChildDownY:F

    .line 498
    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->childHasScrolled:Z

    .line 501
    :cond_1
    iget-object v8, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->scrollState:Lcom/google/android/music/widgets/ExpandingScrollView$ScrollState;

    sget-object v9, Lcom/google/android/music/widgets/ExpandingScrollView$ScrollState;->NO_SCROLL:Lcom/google/android/music/widgets/ExpandingScrollView$ScrollState;

    if-ne v8, v9, :cond_3

    .line 502
    const/4 v8, 0x0

    goto :goto_0

    .line 492
    :cond_2
    sget-object v8, Lcom/google/android/music/widgets/ExpandingScrollView$ScrollState;->NO_SCROLL:Lcom/google/android/music/widgets/ExpandingScrollView$ScrollState;

    goto :goto_1

    .line 506
    :cond_3
    iget-object v8, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->contentView:Landroid/view/View;

    if-nez v8, :cond_4

    .line 507
    invoke-super {p0, p1}, Lcom/google/android/music/widgets/ScrollableViewGroup;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v8

    goto :goto_0

    .line 510
    :cond_4
    iget-object v8, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->childDispatcher:Lcom/google/android/music/widgets/ExpandingScrollView$ConsistentTouchEventDispatcher;

    invoke-virtual {v8}, Lcom/google/android/music/widgets/ExpandingScrollView$ConsistentTouchEventDispatcher;->hasActiveEventStream()Z

    move-result v1

    .line 511
    .local v1, "currentlyDispatchingToChild":Z
    move v4, v1

    .line 512
    .local v4, "forwardToChild":Z
    if-nez v0, :cond_7

    .line 513
    const/4 v4, 0x1

    .line 548
    :cond_5
    :goto_2
    if-eqz v4, :cond_e

    .line 549
    iget-object v8, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->parentDispatcher:Lcom/google/android/music/widgets/ExpandingScrollView$ConsistentTouchEventDispatcher;

    invoke-virtual {v8}, Lcom/google/android/music/widgets/ExpandingScrollView$ConsistentTouchEventDispatcher;->cancelEventStream()V

    .line 552
    const/4 v8, 0x0

    neg-int v9, v5

    int-to-float v9, v9

    invoke-virtual {p1, v8, v9}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 555
    iget v8, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->lastChildDownY:F

    const/4 v9, 0x0

    cmpg-float v8, v8, v9

    if-gez v8, :cond_6

    .line 556
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v8

    iput v8, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->lastChildDownY:F

    .line 559
    :cond_6
    iget-object v8, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->childDispatcher:Lcom/google/android/music/widgets/ExpandingScrollView$ConsistentTouchEventDispatcher;

    invoke-virtual {v8, p1}, Lcom/google/android/music/widgets/ExpandingScrollView$ConsistentTouchEventDispatcher;->dispatchEvent(Landroid/view/MotionEvent;)Z

    .line 567
    :goto_3
    iput v7, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->lastY:F

    .line 569
    const/4 v8, 0x1

    goto :goto_0

    .line 514
    :cond_7
    const/4 v8, 0x2

    if-ne v0, v8, :cond_c

    .line 515
    iget-object v8, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->scrollState:Lcom/google/android/music/widgets/ExpandingScrollView$ScrollState;

    sget-object v9, Lcom/google/android/music/widgets/ExpandingScrollView$ScrollState;->START_TOUCH:Lcom/google/android/music/widgets/ExpandingScrollView$ScrollState;

    if-ne v8, v9, :cond_8

    .line 516
    iget v8, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->lastDownY:F

    sub-float v8, v7, v8

    invoke-static {v8}, Ljava/lang/Math;->abs(F)F

    move-result v8

    iget v9, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->touchSlop:I

    int-to-float v9, v9

    cmpl-float v8, v8, v9

    if-lez v8, :cond_9

    .line 517
    sget-object v8, Lcom/google/android/music/widgets/ExpandingScrollView$ScrollState;->VERTICAL_SCROLL:Lcom/google/android/music/widgets/ExpandingScrollView$ScrollState;

    iput-object v8, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->scrollState:Lcom/google/android/music/widgets/ExpandingScrollView$ScrollState;

    .line 523
    :cond_8
    :goto_4
    iget-object v8, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->scrollState:Lcom/google/android/music/widgets/ExpandingScrollView$ScrollState;

    sget-object v9, Lcom/google/android/music/widgets/ExpandingScrollView$ScrollState;->VERTICAL_SCROLL:Lcom/google/android/music/widgets/ExpandingScrollView$ScrollState;

    if-ne v8, v9, :cond_5

    .line 524
    iget v8, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->lastY:F

    sub-float v3, v7, v8

    .line 525
    .local v3, "deltaY":F
    invoke-virtual {p0}, Lcom/google/android/music/widgets/ExpandingScrollView;->getScroll()I

    move-result v8

    iget v9, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->maxExpandPixels:I

    if-ne v8, v9, :cond_a

    invoke-direct {p0}, Lcom/google/android/music/widgets/ExpandingScrollView;->getHeaderHeight()I

    move-result v8

    int-to-float v8, v8

    iget v9, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->lastDownYOffset:F

    cmpg-float v8, v8, v9

    if-gez v8, :cond_a

    iget-object v8, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->contentView:Landroid/view/View;

    const/4 v9, 0x1

    float-to-int v10, v3

    iget v11, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->lastDownX:F

    float-to-int v11, v11

    iget v12, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->lastDownYOffset:F

    float-to-int v12, v12

    invoke-static {v8, v9, v10, v11, v12}, Lcom/google/android/music/utils/ViewUtils;->canScrollVertically(Landroid/view/View;ZIII)Z

    move-result v8

    if-eqz v8, :cond_a

    const/4 v4, 0x1

    .line 530
    :goto_5
    int-to-float v8, v5

    sub-float v8, v7, v8

    iget v9, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->lastChildDownY:F

    sub-float v2, v8, v9

    .line 531
    .local v2, "deltaDownYOffset":F
    if-eqz v4, :cond_b

    if-eqz v1, :cond_b

    iget-boolean v8, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->childHasScrolled:Z

    if-nez v8, :cond_b

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v8

    iget v9, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->touchSlop:I

    int-to-float v9, v9

    cmpl-float v8, v8, v9

    if-lez v8, :cond_b

    .line 533
    const/4 v8, 0x1

    iput-boolean v8, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->childHasScrolled:Z

    goto/16 :goto_2

    .line 518
    .end local v2    # "deltaDownYOffset":F
    .end local v3    # "deltaY":F
    :cond_9
    iget v8, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->lastDownX:F

    sub-float v8, v6, v8

    invoke-static {v8}, Ljava/lang/Math;->abs(F)F

    move-result v8

    iget v9, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->touchSlop:I

    int-to-float v9, v9

    cmpl-float v8, v8, v9

    if-lez v8, :cond_8

    .line 519
    sget-object v8, Lcom/google/android/music/widgets/ExpandingScrollView$ScrollState;->HORIZONTAL_SCROLL:Lcom/google/android/music/widgets/ExpandingScrollView$ScrollState;

    iput-object v8, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->scrollState:Lcom/google/android/music/widgets/ExpandingScrollView$ScrollState;

    goto :goto_4

    .line 525
    .restart local v3    # "deltaY":F
    :cond_a
    const/4 v4, 0x0

    goto :goto_5

    .line 534
    .restart local v2    # "deltaDownYOffset":F
    :cond_b
    if-nez v4, :cond_5

    .line 535
    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->childHasScrolled:Z

    goto/16 :goto_2

    .line 538
    .end local v2    # "deltaDownYOffset":F
    .end local v3    # "deltaY":F
    :cond_c
    const/4 v8, 0x1

    if-ne v0, v8, :cond_5

    .line 539
    iget-object v8, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->scrollState:Lcom/google/android/music/widgets/ExpandingScrollView$ScrollState;

    sget-object v9, Lcom/google/android/music/widgets/ExpandingScrollView$ScrollState;->START_TOUCH:Lcom/google/android/music/widgets/ExpandingScrollView$ScrollState;

    if-ne v8, v9, :cond_d

    .line 540
    const/4 v4, 0x1

    goto/16 :goto_2

    .line 541
    :cond_d
    iget-object v8, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->scrollState:Lcom/google/android/music/widgets/ExpandingScrollView$ScrollState;

    sget-object v9, Lcom/google/android/music/widgets/ExpandingScrollView$ScrollState;->VERTICAL_SCROLL:Lcom/google/android/music/widgets/ExpandingScrollView$ScrollState;

    if-ne v8, v9, :cond_5

    iget-boolean v8, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->childHasScrolled:Z

    if-nez v8, :cond_5

    .line 544
    const/4 v4, 0x0

    goto/16 :goto_2

    .line 561
    :cond_e
    iget-object v8, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->childDispatcher:Lcom/google/android/music/widgets/ExpandingScrollView$ConsistentTouchEventDispatcher;

    invoke-virtual {v8}, Lcom/google/android/music/widgets/ExpandingScrollView$ConsistentTouchEventDispatcher;->cancelEventStream()V

    .line 562
    const/high16 v8, -0x40800000    # -1.0f

    iput v8, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->lastChildDownY:F

    .line 564
    iget-object v8, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->parentDispatcher:Lcom/google/android/music/widgets/ExpandingScrollView$ConsistentTouchEventDispatcher;

    invoke-virtual {v8, p1}, Lcom/google/android/music/widgets/ExpandingScrollView$ConsistentTouchEventDispatcher;->dispatchEvent(Landroid/view/MotionEvent;)Z

    goto/16 :goto_3
.end method

.method public removeListener(Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingScrollViewListener;)Z
    .locals 1
    .param p1, "listener"    # Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingScrollViewListener;

    .prologue
    .line 842
    iget-object v0, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->listeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public resetExposurePercentages()V
    .locals 7

    .prologue
    .line 328
    invoke-static {}, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->values()[Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    move-result-object v0

    .local v0, "arr$":[Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 329
    .local v3, "state":Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;
    iget-object v4, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->exposurePercentages:[F

    invoke-virtual {v3}, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->ordinal()I

    move-result v5

    iget v6, v3, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->defaultExposurePercentage:F

    aput v6, v4, v5

    .line 328
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 331
    .end local v3    # "state":Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;
    :cond_0
    return-void
.end method

.method public scrollTo(II)V
    .locals 1
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 847
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/music/widgets/ExpandingScrollView;->scrollTo(IIZ)V

    .line 848
    return-void
.end method

.method public setAlwaysExpanded(Z)V
    .locals 0
    .param p1, "alwaysExpanded"    # Z

    .prologue
    .line 337
    iput-boolean p1, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->alwaysExpanded:Z

    .line 338
    return-void
.end method

.method public setContent(Landroid/view/View;)V
    .locals 2
    .param p1, "contentView"    # Landroid/view/View;

    .prologue
    .line 307
    invoke-virtual {p0}, Lcom/google/android/music/widgets/ExpandingScrollView;->removeAllViews()V

    .line 309
    iput-object p1, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->contentView:Landroid/view/View;

    .line 310
    if-eqz p1, :cond_0

    .line 311
    invoke-virtual {p0, p1}, Lcom/google/android/music/widgets/ExpandingScrollView;->addView(Landroid/view/View;)V

    .line 312
    new-instance v0, Lcom/google/android/music/widgets/ExpandingScrollView$ConsistentTouchEventDispatcher;

    new-instance v1, Lcom/google/android/music/widgets/ExpandingScrollView$2;

    invoke-direct {v1, p0}, Lcom/google/android/music/widgets/ExpandingScrollView$2;-><init>(Lcom/google/android/music/widgets/ExpandingScrollView;)V

    invoke-direct {v0, v1}, Lcom/google/android/music/widgets/ExpandingScrollView$ConsistentTouchEventDispatcher;-><init>(Lcom/google/android/music/widgets/ExpandingScrollView$ConsistentTouchEventDispatcher$TouchEventDispatcher;)V

    iput-object v0, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->childDispatcher:Lcom/google/android/music/widgets/ExpandingScrollView$ConsistentTouchEventDispatcher;

    .line 322
    :goto_0
    return-void

    .line 320
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->childDispatcher:Lcom/google/android/music/widgets/ExpandingScrollView$ConsistentTouchEventDispatcher;

    goto :goto_0
.end method

.method public setExpandingState(Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;)V
    .locals 4
    .param p1, "newState"    # Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    .prologue
    .line 717
    iget-object v2, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->expandingState:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    .line 718
    .local v2, "oldState":Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;
    iget-object v3, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->expandingStateTransition:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingStateTransition;

    invoke-virtual {v3, p1}, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingStateTransition;->convertToAllowedState(Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;)Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->expandingState:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    .line 719
    invoke-direct {p0}, Lcom/google/android/music/widgets/ExpandingScrollView;->setScrollLimits()V

    .line 720
    iget-object v3, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->expandingState:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    if-eq v3, v2, :cond_0

    .line 721
    iget-object v3, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->listeners:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingScrollViewListener;

    .line 722
    .local v1, "listener":Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingScrollViewListener;
    iget-object v3, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->expandingState:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    invoke-interface {v1, p0, v2, v3}, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingScrollViewListener;->onExpandingStateChanged(Lcom/google/android/music/widgets/ExpandingScrollView;Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;)V

    goto :goto_0

    .line 725
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "listener":Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingScrollViewListener;
    :cond_0
    return-void
.end method

.method public setExpandingStateTransition(Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingStateTransition;)V
    .locals 2
    .param p1, "newTransition"    # Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingStateTransition;

    .prologue
    const/4 v1, 0x0

    .line 347
    iput-object p1, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->portraitExpandingStateTransition:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingStateTransition;

    .line 348
    invoke-virtual {p0}, Lcom/google/android/music/widgets/ExpandingScrollView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/music/widgets/ExpandingScrollView;->updateStateTransition(Landroid/content/res/Configuration;)V

    .line 349
    iput-object v1, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->desiredState:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    .line 350
    iput-object v1, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->coercedState:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    .line 351
    return-void
.end method

.method public setExposurePercentage(Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;F)V
    .locals 13
    .param p1, "state"    # Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;
    .param p2, "percentage"    # F

    .prologue
    const/4 v12, 0x3

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 746
    invoke-virtual {p1}, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->ordinal()I

    move-result v1

    .line 747
    .local v1, "ordinal":I
    iget-object v4, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->exposurePercentages:[F

    aget v4, v4, v1

    cmpl-float v4, v4, p2

    if-nez v4, :cond_1

    .line 785
    :cond_0
    :goto_0
    return-void

    .line 752
    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/music/widgets/ExpandingScrollView;->getPreviousExpandingState(Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;)Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    move-result-object v2

    .line 753
    .local v2, "previous":Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;
    if-eq p1, v2, :cond_2

    iget-object v4, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->exposurePercentages:[F

    invoke-virtual {v2}, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->ordinal()I

    move-result v5

    aget v4, v4, v5

    cmpg-float v4, p2, v4

    if-gez v4, :cond_2

    .line 754
    const-string v4, "ExpandingScrollView"

    const-string v5, "Exposure percentage less than previous state: state=%s, percentage=%f, prevStatePercentage=%f"

    new-array v6, v12, [Ljava/lang/Object;

    aput-object p1, v6, v9

    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    aput-object v7, v6, v10

    iget-object v7, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->exposurePercentages:[F

    invoke-virtual {v2}, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->ordinal()I

    move-result v8

    aget v7, v7, v8

    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    aput-object v7, v6, v11

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 758
    :cond_2
    invoke-virtual {p0, p1}, Lcom/google/android/music/widgets/ExpandingScrollView;->getNextExpandingState(Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;)Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    move-result-object v0

    .line 759
    .local v0, "next":Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;
    if-eq p1, v0, :cond_3

    iget-object v4, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->exposurePercentages:[F

    invoke-virtual {v0}, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->ordinal()I

    move-result v5

    aget v4, v4, v5

    cmpl-float v4, p2, v4

    if-lez v4, :cond_3

    .line 760
    const-string v4, "ExpandingScrollView"

    const-string v5, "Exposure percentage more than next state: state=%s, percentage=%f, nextStatePercentage=%f"

    new-array v6, v12, [Ljava/lang/Object;

    aput-object p1, v6, v9

    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    aput-object v7, v6, v10

    iget-object v7, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->exposurePercentages:[F

    invoke-virtual {v0}, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->ordinal()I

    move-result v8

    aget v7, v7, v8

    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    aput-object v7, v6, v11

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 765
    :cond_3
    iget-object v4, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->exposurePercentages:[F

    aput p2, v4, v1

    .line 766
    invoke-direct {p0}, Lcom/google/android/music/widgets/ExpandingScrollView;->setScrollLimits()V

    .line 767
    iget-boolean v4, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->isBeingDragged:Z

    if-eqz v4, :cond_5

    .line 772
    invoke-virtual {p0}, Lcom/google/android/music/widgets/ExpandingScrollView;->getScroll()I

    move-result v3

    .line 773
    .local v3, "scroll":I
    :goto_1
    iget-object v4, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->expandingState:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    invoke-direct {p0, v4}, Lcom/google/android/music/widgets/ExpandingScrollView;->getPreviousExpandingState(Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;)Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/android/music/widgets/ExpandingScrollView;->getExposurePixels(Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;)I

    move-result v4

    if-ge v3, v4, :cond_4

    .line 775
    iget-object v4, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->expandingState:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    invoke-direct {p0, v4}, Lcom/google/android/music/widgets/ExpandingScrollView;->getPreviousExpandingState(Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;)Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/android/music/widgets/ExpandingScrollView;->setExpandingState(Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;)V

    goto :goto_1

    .line 777
    :cond_4
    :goto_2
    iget-object v4, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->expandingState:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    invoke-virtual {p0, v4}, Lcom/google/android/music/widgets/ExpandingScrollView;->getNextExpandingState(Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;)Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/android/music/widgets/ExpandingScrollView;->getExposurePixels(Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;)I

    move-result v4

    if-le v3, v4, :cond_0

    .line 778
    iget-object v4, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->expandingState:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    invoke-virtual {p0, v4}, Lcom/google/android/music/widgets/ExpandingScrollView;->getNextExpandingState(Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;)Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/android/music/widgets/ExpandingScrollView;->setExpandingState(Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;)V

    goto :goto_2

    .line 781
    .end local v3    # "scroll":I
    :cond_5
    iget-object v4, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->expandingState:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    if-ne v4, p1, :cond_0

    .line 782
    invoke-virtual {p0, p1}, Lcom/google/android/music/widgets/ExpandingScrollView;->getExposurePixels(Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;)I

    move-result v4

    invoke-virtual {p0, v4}, Lcom/google/android/music/widgets/ExpandingScrollView;->smoothScrollTo(I)V

    goto/16 :goto_0
.end method

.method public setExposurePixels(Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;I)V
    .locals 3
    .param p1, "state"    # Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;
    .param p2, "pixels"    # I

    .prologue
    .line 738
    int-to-float v1, p2

    const/high16 v2, 0x42c80000    # 100.0f

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->maxExpandPixels:I

    int-to-float v2, v2

    div-float v0, v1, v2

    .line 739
    .local v0, "percentage":F
    invoke-virtual {p0, p1, v0}, Lcom/google/android/music/widgets/ExpandingScrollView;->setExposurePercentage(Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;F)V

    .line 740
    return-void
.end method

.method public setHidden(Z)V
    .locals 2
    .param p1, "hidden"    # Z

    .prologue
    .line 828
    if-eqz p1, :cond_1

    .line 829
    sget-object v0, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->HIDDEN:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    invoke-virtual {p0, v0}, Lcom/google/android/music/widgets/ExpandingScrollView;->moveToExpandingState(Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;)V

    .line 833
    :cond_0
    :goto_0
    return-void

    .line 830
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->expandingState:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    sget-object v1, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->HIDDEN:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    if-ne v0, v1, :cond_0

    .line 831
    sget-object v0, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->COLLAPSED:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    invoke-virtual {p0, v0}, Lcom/google/android/music/widgets/ExpandingScrollView;->moveToExpandingState(Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;)V

    goto :goto_0
.end method

.method public setViewIdForSizingCollapsedState(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 814
    iput p1, p0, Lcom/google/android/music/widgets/ExpandingScrollView;->collapsedHeaderViewId:I

    .line 815
    return-void
.end method
