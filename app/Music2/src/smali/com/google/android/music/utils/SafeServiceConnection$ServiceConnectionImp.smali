.class Lcom/google/android/music/utils/SafeServiceConnection$ServiceConnectionImp;
.super Ljava/lang/Object;
.source "SafeServiceConnection.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/utils/SafeServiceConnection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ServiceConnectionImp"
.end annotation


# instance fields
.field private mContextForDelayedUnbind:Landroid/content/Context;

.field private mFlags:I

.field private mService:Landroid/content/Intent;

.field private mState:I

.field final synthetic this$0:Lcom/google/android/music/utils/SafeServiceConnection;


# direct methods
.method public constructor <init>(Lcom/google/android/music/utils/SafeServiceConnection;)V
    .locals 1

    .prologue
    .line 74
    iput-object p1, p0, Lcom/google/android/music/utils/SafeServiceConnection$ServiceConnectionImp;->this$0:Lcom/google/android/music/utils/SafeServiceConnection;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/music/utils/SafeServiceConnection$ServiceConnectionImp;->mState:I

    .line 76
    return-void
.end method

.method private reportBadState()V
    .locals 3

    .prologue
    .line 220
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected method for state "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/music/utils/SafeServiceConnection$ServiceConnectionImp;->mState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public bindService(Landroid/content/Context;Landroid/content/Intent;I)Z
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "service"    # Landroid/content/Intent;
    .param p3, "flags"    # I

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 79
    monitor-enter p0

    .line 80
    :try_start_0
    iget v2, p0, Lcom/google/android/music/utils/SafeServiceConnection$ServiceConnectionImp;->mState:I

    packed-switch v2, :pswitch_data_0

    .line 110
    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/music/utils/SafeServiceConnection$ServiceConnectionImp;->reportBadState()V

    .line 111
    monitor-exit p0

    move v0, v1

    :goto_0
    return v0

    .line 82
    :pswitch_1
    iget-object v1, p0, Lcom/google/android/music/utils/SafeServiceConnection$ServiceConnectionImp;->mService:Landroid/content/Intent;

    if-nez v1, :cond_2

    .line 86
    iput-object p2, p0, Lcom/google/android/music/utils/SafeServiceConnection$ServiceConnectionImp;->mService:Landroid/content/Intent;

    .line 87
    iput p3, p0, Lcom/google/android/music/utils/SafeServiceConnection$ServiceConnectionImp;->mFlags:I

    .line 97
    :cond_0
    const/4 v1, 0x1

    iput v1, p0, Lcom/google/android/music/utils/SafeServiceConnection$ServiceConnectionImp;->mState:I

    .line 98
    invoke-virtual {p1, p2, p0, p3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    .line 99
    .local v0, "bound":Z
    if-nez v0, :cond_1

    .line 100
    const/4 v1, 0x0

    iput v1, p0, Lcom/google/android/music/utils/SafeServiceConnection$ServiceConnectionImp;->mState:I

    .line 102
    :cond_1
    monitor-exit p0

    goto :goto_0

    .line 113
    .end local v0    # "bound":Z
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 89
    :cond_2
    :try_start_1
    iget-object v1, p0, Lcom/google/android/music/utils/SafeServiceConnection$ServiceConnectionImp;->mService:Landroid/content/Intent;

    invoke-virtual {v1, p2}, Landroid/content/Intent;->filterEquals(Landroid/content/Intent;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 90
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "service must be equivalent for every call to bindService"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 92
    :cond_3
    iget v1, p0, Lcom/google/android/music/utils/SafeServiceConnection$ServiceConnectionImp;->mFlags:I

    if-eq v1, p3, :cond_0

    .line 93
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "flags must be equivalent for every call to bindService"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 106
    :pswitch_2
    const/4 v1, 0x1

    iput v1, p0, Lcom/google/android/music/utils/SafeServiceConnection$ServiceConnectionImp;->mState:I

    .line 107
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/music/utils/SafeServiceConnection$ServiceConnectionImp;->mContextForDelayedUnbind:Landroid/content/Context;

    .line 108
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 80
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 4
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 147
    const/4 v0, 0x0

    .line 151
    .local v0, "delayedUnbind":Z
    monitor-enter p0

    .line 152
    :try_start_0
    iget v2, p0, Lcom/google/android/music/utils/SafeServiceConnection$ServiceConnectionImp;->mState:I

    packed-switch v2, :pswitch_data_0

    .line 188
    invoke-direct {p0}, Lcom/google/android/music/utils/SafeServiceConnection$ServiceConnectionImp;->reportBadState()V

    .line 191
    :goto_0
    :pswitch_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 199
    iget-object v2, p0, Lcom/google/android/music/utils/SafeServiceConnection$ServiceConnectionImp;->this$0:Lcom/google/android/music/utils/SafeServiceConnection;

    invoke-virtual {v2, p1, p2}, Lcom/google/android/music/utils/SafeServiceConnection;->onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V

    .line 201
    return-void

    .line 154
    :pswitch_1
    const/4 v2, 0x2

    :try_start_1
    iput v2, p0, Lcom/google/android/music/utils/SafeServiceConnection$ServiceConnectionImp;->mState:I

    goto :goto_0

    .line 191
    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    .line 161
    :pswitch_2
    const/4 v0, 0x1

    .line 163
    :try_start_2
    iget-object v2, p0, Lcom/google/android/music/utils/SafeServiceConnection$ServiceConnectionImp;->mContextForDelayedUnbind:Landroid/content/Context;

    invoke-virtual {v2, p0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 184
    :goto_1
    const/4 v2, 0x0

    :try_start_3
    iput-object v2, p0, Lcom/google/android/music/utils/SafeServiceConnection$ServiceConnectionImp;->mContextForDelayedUnbind:Landroid/content/Context;

    .line 185
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/android/music/utils/SafeServiceConnection$ServiceConnectionImp;->mState:I

    goto :goto_0

    .line 164
    :catch_0
    move-exception v1

    .line 182
    .local v1, "e":Ljava/lang/RuntimeException;
    const-string v2, "SafeServiceConnection"

    const-string v3, "Unable to perform delayed unbind."

    invoke-static {v2, v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 152
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 1
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 216
    iget-object v0, p0, Lcom/google/android/music/utils/SafeServiceConnection$ServiceConnectionImp;->this$0:Lcom/google/android/music/utils/SafeServiceConnection;

    invoke-virtual {v0, p1}, Lcom/google/android/music/utils/SafeServiceConnection;->onServiceDisconnected(Landroid/content/ComponentName;)V

    .line 217
    return-void
.end method

.method public unbindService(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 117
    monitor-enter p0

    .line 118
    :try_start_0
    iget v0, p0, Lcom/google/android/music/utils/SafeServiceConnection$ServiceConnectionImp;->mState:I

    packed-switch v0, :pswitch_data_0

    .line 129
    invoke-direct {p0}, Lcom/google/android/music/utils/SafeServiceConnection$ServiceConnectionImp;->reportBadState()V

    .line 132
    :goto_0
    monitor-exit p0

    .line 133
    return-void

    .line 121
    :pswitch_0
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/android/music/utils/SafeServiceConnection$ServiceConnectionImp;->mState:I

    .line 122
    iput-object p1, p0, Lcom/google/android/music/utils/SafeServiceConnection$ServiceConnectionImp;->mContextForDelayedUnbind:Landroid/content/Context;

    goto :goto_0

    .line 132
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 125
    :pswitch_1
    const/4 v0, 0x0

    :try_start_1
    iput v0, p0, Lcom/google/android/music/utils/SafeServiceConnection$ServiceConnectionImp;->mState:I

    .line 126
    invoke-virtual {p1, p0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 118
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
