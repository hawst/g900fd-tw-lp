.class final Lcom/google/android/music/wear/GmsUtils$1;
.super Ljava/lang/Object;
.source "GmsUtils.java"

# interfaces
.implements Lcom/google/android/gms/common/api/GoogleApiClient$OnConnectionFailedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/wear/GmsUtils;->newClientBuilderWithErrorNotification(Landroid/content/Context;)Lcom/google/android/gms/common/api/GoogleApiClient$Builder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 25
    iput-object p1, p0, Lcom/google/android/music/wear/GmsUtils$1;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onConnectionFailed(Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 1
    .param p1, "result"    # Lcom/google/android/gms/common/ConnectionResult;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/music/wear/GmsUtils$1;->val$context:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/google/android/music/wear/GmsUtils;->handleConnectionFailureWithNotification(Landroid/content/Context;Lcom/google/android/gms/common/ConnectionResult;)V

    .line 29
    return-void
.end method
