.class public Lcom/google/android/music/download/cp/CpUtils;
.super Ljava/lang/Object;
.source "CpUtils.java"


# static fields
.field static final MAGIC_NUMBER:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x4

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/music/download/cp/CpUtils;->MAGIC_NUMBER:[B

    return-void

    nop

    :array_0
    .array-data 1
        0x12t
        -0x2dt
        0x15t
        0x27t
    .end array-data
.end method

.method static getCipher()Ljavax/crypto/Cipher;
    .locals 3

    .prologue
    .line 96
    :try_start_0
    const-string v1, "AES/CTR/NoPadding"

    invoke-static {v1}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    return-object v1

    .line 97
    :catch_0
    move-exception v0

    .line 98
    .local v0, "e":Ljava/security/NoSuchAlgorithmException;
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Required content protection algorithm is not present: AES/CTR/NoPadding"

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 101
    .end local v0    # "e":Ljava/security/NoSuchAlgorithmException;
    :catch_1
    move-exception v0

    .line 102
    .local v0, "e":Ljavax/crypto/NoSuchPaddingException;
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Required padding is not supported: AES/CTR/NoPadding"

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static getDecryptedSize(J)J
    .locals 12
    .param p0, "encryptedSize"    # J

    .prologue
    .line 57
    const-wide/16 v8, 0x4

    cmp-long v8, p0, v8

    if-gez v8, :cond_0

    .line 58
    const-wide/16 v8, 0x0

    .line 64
    :goto_0
    return-wide v8

    .line 60
    :cond_0
    const-wide/16 v8, 0x4

    sub-long v0, p0, v8

    .line 61
    .local v0, "contentSize":J
    const-wide/16 v8, 0x400

    div-long v6, v0, v8

    .line 62
    .local v6, "wholeBlocks":J
    const-wide/16 v8, 0x400

    mul-long/2addr v8, v6

    sub-long v2, v0, v8

    .line 63
    .local v2, "remainder":J
    const-wide/16 v8, 0x0

    const-wide/16 v10, 0x10

    sub-long v10, v2, v10

    invoke-static {v8, v9, v10, v11}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    .line 64
    .local v4, "remainderPayload":J
    const-wide/16 v8, 0x3f0

    mul-long/2addr v8, v6

    add-long/2addr v8, v4

    goto :goto_0
.end method

.method public static getEncryptedSize(J)J
    .locals 10
    .param p0, "decryptedSize"    # J

    .prologue
    const-wide/16 v6, 0x3f0

    .line 68
    div-long v4, p0, v6

    .line 69
    .local v4, "wholeBlocks":J
    mul-long/2addr v6, v4

    sub-long v2, p0, v6

    .line 70
    .local v2, "remainder":J
    const-wide/16 v6, 0x4

    const-wide/16 v8, 0x400

    mul-long/2addr v8, v4

    add-long v0, v6, v8

    .line 71
    .local v0, "fileSize":J
    const-wide/16 v6, 0x0

    cmp-long v6, v2, v6

    if-lez v6, :cond_0

    .line 72
    const-wide/16 v6, 0x10

    add-long/2addr v6, v2

    add-long/2addr v0, v6

    .line 74
    :cond_0
    return-wide v0
.end method

.method public static getRandom()Ljava/security/SecureRandom;
    .locals 4

    .prologue
    .line 111
    :try_start_0
    const-string v2, "SHA1PRNG"

    invoke-static {v2}, Ljava/security/SecureRandom;->getInstance(Ljava/lang/String;)Ljava/security/SecureRandom;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 116
    .local v1, "random":Ljava/security/SecureRandom;
    return-object v1

    .line 112
    .end local v1    # "random":Ljava/security/SecureRandom;
    :catch_0
    move-exception v0

    .line 113
    .local v0, "e":Ljava/security/NoSuchAlgorithmException;
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "Required SHA1PRNG algorithm is not present"

    invoke-direct {v2, v3, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
.end method

.method public static newCpData()[B
    .locals 6

    .prologue
    .line 83
    :try_start_0
    const-string v4, "AES"

    invoke-static {v4}, Ljavax/crypto/KeyGenerator;->getInstance(Ljava/lang/String;)Ljavax/crypto/KeyGenerator;

    move-result-object v1

    .line 84
    .local v1, "kgen":Ljavax/crypto/KeyGenerator;
    const-string v4, "SHA1PRNG"

    invoke-static {v4}, Ljava/security/SecureRandom;->getInstance(Ljava/lang/String;)Ljava/security/SecureRandom;

    move-result-object v2

    .line 85
    .local v2, "random":Ljava/security/SecureRandom;
    const/16 v4, 0x80

    invoke-virtual {v1, v4, v2}, Ljavax/crypto/KeyGenerator;->init(ILjava/security/SecureRandom;)V

    .line 86
    invoke-virtual {v1}, Ljavax/crypto/KeyGenerator;->generateKey()Ljavax/crypto/SecretKey;

    move-result-object v3

    .line 87
    .local v3, "skey":Ljavax/crypto/SecretKey;
    invoke-interface {v3}, Ljavax/crypto/SecretKey;->getEncoded()[B
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    return-object v4

    .line 88
    .end local v2    # "random":Ljava/security/SecureRandom;
    .end local v3    # "skey":Ljavax/crypto/SecretKey;
    :catch_0
    move-exception v0

    .line 89
    .local v0, "e":Ljava/security/NoSuchAlgorithmException;
    new-instance v4, Ljava/lang/IllegalStateException;

    const-string v5, "Required content protection algorithm is not present"

    invoke-direct {v4, v5, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4
.end method

.method static readAndCheckMagicNumber(Ljava/io/InputStream;)V
    .locals 5
    .param p0, "in"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/music/download/cp/UnrecognizedDataCpException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x0

    .line 138
    new-array v1, v4, [B

    .line 140
    .local v1, "magicNumber":[B
    :try_start_0
    invoke-static {p0, v1}, Lcom/google/common/io/ByteStreams;->readFully(Ljava/io/InputStream;[B)V
    :try_end_0
    .catch Ljava/io/EOFException; {:try_start_0 .. :try_end_0} :catch_0

    .line 144
    sget-object v2, Lcom/google/android/music/download/cp/CpUtils;->MAGIC_NUMBER:[B

    invoke-static {v1, v3, v2, v3, v4}, Lcom/google/android/music/utils/ByteUtils;->bytesEqual([BI[BII)Z

    move-result v2

    if-nez v2, :cond_0

    .line 145
    new-instance v2, Lcom/google/android/music/download/cp/UnrecognizedDataCpException;

    const-string v3, "Magic number is not found"

    invoke-direct {v2, v3}, Lcom/google/android/music/download/cp/UnrecognizedDataCpException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 141
    :catch_0
    move-exception v0

    .line 142
    .local v0, "e":Ljava/io/EOFException;
    new-instance v2, Lcom/google/android/music/download/cp/UnrecognizedDataCpException;

    const-string v3, "Magic number is not found. Input is too short"

    invoke-direct {v2, v3}, Lcom/google/android/music/download/cp/UnrecognizedDataCpException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 147
    .end local v0    # "e":Ljava/io/EOFException;
    :cond_0
    return-void
.end method

.method static writeMagicNumber(Ljava/io/OutputStream;)V
    .locals 1
    .param p0, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 126
    sget-object v0, Lcom/google/android/music/download/cp/CpUtils;->MAGIC_NUMBER:[B

    invoke-virtual {p0, v0}, Ljava/io/OutputStream;->write([B)V

    .line 127
    return-void
.end method
