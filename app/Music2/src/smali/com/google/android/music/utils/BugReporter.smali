.class public Lcom/google/android/music/utils/BugReporter;
.super Ljava/lang/Object;
.source "BugReporter.java"


# direct methods
.method static synthetic access$000(Landroid/app/Activity;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "x0"    # Landroid/app/Activity;

    .prologue
    .line 30
    invoke-static {p0}, Lcom/google/android/music/utils/BugReporter;->getCurrentScreenshot(Landroid/app/Activity;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method private static getBugReportIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 52
    const-string v0, "android.intent.action.BUG_REPORT"

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/google/android/music/utils/SystemUtils;->getExplicitServiceIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private static getCurrentScreenshot(Landroid/app/Activity;)Landroid/graphics/Bitmap;
    .locals 5
    .param p0, "activity"    # Landroid/app/Activity;

    .prologue
    .line 92
    :try_start_0
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v1

    .line 93
    .local v1, "currentView":Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/View;->isDrawingCacheEnabled()Z

    move-result v2

    .line 94
    .local v2, "drawingCacheWasEnabled":Z
    const/4 v4, 0x1

    invoke-virtual {v1, v4}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    .line 95
    invoke-virtual {v1}, Landroid/view/View;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 96
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_0

    .line 97
    invoke-static {v0}, Lcom/google/android/music/utils/BugReporter;->resizeBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 99
    :cond_0
    if-nez v2, :cond_1

    .line 100
    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    .line 101
    invoke-virtual {v1}, Landroid/view/View;->destroyDrawingCache()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 105
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v1    # "currentView":Landroid/view/View;
    .end local v2    # "drawingCacheWasEnabled":Z
    :cond_1
    :goto_0
    return-object v0

    .line 104
    :catch_0
    move-exception v3

    .line 105
    .local v3, "e":Ljava/lang/Exception;
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isGoogleFeedbackInstalled(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 42
    invoke-static {p0}, Lcom/google/android/music/utils/BugReporter;->getBugReportIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 43
    .local v0, "intent":Landroid/content/Intent;
    if-nez v0, :cond_0

    .line 44
    const-string v1, "BugReporter"

    const-string v2, "getBugReportIntent() returned null, assuming feedback is not available"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 45
    const/4 v1, 0x0

    .line 47
    :goto_0
    return v1

    :cond_0
    invoke-static {p0, v0}, Lcom/google/android/music/utils/BugReporter;->isSupportingServiceInstalled(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v1

    goto :goto_0
.end method

.method private static isSupportingServiceInstalled(Landroid/content/Context;Landroid/content/Intent;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 130
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 131
    .local v1, "manager":Landroid/content/pm/PackageManager;
    const/high16 v2, 0x10000

    invoke-virtual {v1, p1, v2}, Landroid/content/pm/PackageManager;->queryIntentServices(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 133
    .local v0, "list":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static launchGoogleFeedback(Landroid/app/Activity;)V
    .locals 4
    .param p0, "activity"    # Landroid/app/Activity;

    .prologue
    .line 56
    invoke-static {p0}, Lcom/google/android/music/utils/BugReporter;->isGoogleFeedbackInstalled(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 57
    const-string v2, "BugReporter"

    const-string v3, "GoogleFeedback is not installed"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    :goto_0
    return-void

    .line 60
    :cond_0
    new-instance v1, Lcom/google/android/music/utils/BugReporter$1;

    invoke-direct {v1, p0}, Lcom/google/android/music/utils/BugReporter$1;-><init>(Landroid/app/Activity;)V

    .line 82
    .local v1, "conn":Landroid/content/ServiceConnection;
    invoke-static {p0}, Lcom/google/android/music/utils/BugReporter;->getBugReportIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 83
    .local v0, "bugReportIntent":Landroid/content/Intent;
    if-nez v0, :cond_1

    .line 84
    const-string v2, "BugReporter"

    const-string v3, "Could not find bug reporter service!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 86
    :cond_1
    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Landroid/app/Activity;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    goto :goto_0
.end method

.method private static resizeBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 4
    .param p0, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 111
    sget-object v2, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object p0

    .line 113
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    .line 114
    .local v1, "width":I
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    .line 115
    .local v0, "height":I
    :goto_0
    mul-int v2, v1, v0

    mul-int/lit8 v2, v2, 0x2

    const/high16 v3, 0x40000

    if-le v2, v3, :cond_0

    .line 116
    div-int/lit8 v1, v1, 0x2

    .line 117
    div-int/lit8 v0, v0, 0x2

    goto :goto_0

    .line 119
    :cond_0
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    if-eq v1, v2, :cond_1

    .line 120
    const/4 v2, 0x1

    invoke-static {p0, v1, v0, v2}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object p0

    .line 122
    :cond_1
    return-object p0
.end method
