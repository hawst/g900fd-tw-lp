.class public Lcom/google/android/music/art/ArtRequest$Token;
.super Ljava/lang/Object;
.source "ArtRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/art/ArtRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Token"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final mArtType:Lcom/google/android/music/art/ArtType;

.field public final mDescriptor:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Object;Lcom/google/android/music/art/ArtType;)V
    .locals 0
    .param p2, "artType"    # Lcom/google/android/music/art/ArtType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Lcom/google/android/music/art/ArtType;",
            ")V"
        }
    .end annotation

    .prologue
    .line 358
    .local p0, "this":Lcom/google/android/music/art/ArtRequest$Token;, "Lcom/google/android/music/art/ArtRequest$Token<TT;>;"
    .local p1, "descriptor":Ljava/lang/Object;, "TT;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 359
    iput-object p1, p0, Lcom/google/android/music/art/ArtRequest$Token;->mDescriptor:Ljava/lang/Object;

    .line 360
    iput-object p2, p0, Lcom/google/android/music/art/ArtRequest$Token;->mArtType:Lcom/google/android/music/art/ArtType;

    .line 361
    return-void
.end method
