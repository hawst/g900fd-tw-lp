.class abstract Lcom/google/android/music/download/cache/BaseCacheManager;
.super Lcom/google/android/music/download/cache/ICacheManager$Stub;
.source "BaseCacheManager.java"


# static fields
.field private static final LOGV:Z

.field private static final sLogFile:Lcom/google/android/music/log/LogFile;


# instance fields
.field private volatile mAllowCaching:Z

.field private volatile mCacheStrategy:Lcom/google/android/music/download/cache/CacheStrategy;

.field private final mContext:Landroid/content/Context;

.field final mDeleteFilters:Landroid/os/RemoteCallbackList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/RemoteCallbackList",
            "<",
            "Lcom/google/android/music/download/cache/IDeleteFilter;",
            ">;"
        }
    .end annotation
.end field

.field private volatile mDeletionStrategy:Lcom/google/android/music/download/cache/DeletionStrategy;

.field private final mFileSystem:Lcom/google/android/music/download/cache/FileSystem;

.field private final mFilteredFileDeleter:Lcom/google/android/music/download/cache/FilteredFileDeleter;

.field private final mIsExternalSameAsInternal:Z

.field private volatile mLongTermCacheStrategy:Lcom/google/android/music/download/cache/CacheStrategy;

.field private final mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->DOWNLOAD:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->CACHE:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/music/download/cache/BaseCacheManager;->LOGV:Z

    .line 55
    const-string v0, "com.google.android.music.pin"

    invoke-static {v0}, Lcom/google/android/music/log/Log;->getLogFile(Ljava/lang/String;)Lcom/google/android/music/log/LogFile;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/download/cache/BaseCacheManager;->sLogFile:Lcom/google/android/music/log/LogFile;

    return-void

    .line 39
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/music/preferences/MusicPreferences;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "musicPreferences"    # Lcom/google/android/music/preferences/MusicPreferences;

    .prologue
    .line 80
    invoke-direct {p0}, Lcom/google/android/music/download/cache/ICacheManager$Stub;-><init>()V

    .line 54
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/download/cache/BaseCacheManager;->mAllowCaching:Z

    .line 64
    new-instance v0, Landroid/os/RemoteCallbackList;

    invoke-direct {v0}, Landroid/os/RemoteCallbackList;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/download/cache/BaseCacheManager;->mDeleteFilters:Landroid/os/RemoteCallbackList;

    .line 67
    new-instance v0, Lcom/google/android/music/download/cache/BaseCacheManager$1;

    invoke-direct {v0, p0}, Lcom/google/android/music/download/cache/BaseCacheManager$1;-><init>(Lcom/google/android/music/download/cache/BaseCacheManager;)V

    iput-object v0, p0, Lcom/google/android/music/download/cache/BaseCacheManager;->mFilteredFileDeleter:Lcom/google/android/music/download/cache/FilteredFileDeleter;

    .line 81
    invoke-static {}, Lcom/google/android/music/utils/PostFroyoUtils$EnvironmentCompat;->isExternalStorageEmulated()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/music/download/cache/BaseCacheManager;->mIsExternalSameAsInternal:Z

    .line 82
    iput-object p1, p0, Lcom/google/android/music/download/cache/BaseCacheManager;->mContext:Landroid/content/Context;

    .line 83
    iput-object p2, p0, Lcom/google/android/music/download/cache/BaseCacheManager;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    .line 84
    new-instance v0, Lcom/google/android/music/download/cache/FileSystemImpl;

    iget-object v1, p0, Lcom/google/android/music/download/cache/BaseCacheManager;->mFilteredFileDeleter:Lcom/google/android/music/download/cache/FilteredFileDeleter;

    invoke-direct {v0, v1}, Lcom/google/android/music/download/cache/FileSystemImpl;-><init>(Lcom/google/android/music/download/cache/FilteredFileDeleter;)V

    iput-object v0, p0, Lcom/google/android/music/download/cache/BaseCacheManager;->mFileSystem:Lcom/google/android/music/download/cache/FileSystem;

    .line 85
    invoke-direct {p0}, Lcom/google/android/music/download/cache/BaseCacheManager;->initCacheStrategies()V

    .line 86
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/download/cache/BaseCacheManager;Ljava/io/File;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/download/cache/BaseCacheManager;
    .param p1, "x1"    # Ljava/io/File;

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/google/android/music/download/cache/BaseCacheManager;->handleDeleteFile(Ljava/io/File;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/music/download/cache/BaseCacheManager;)Ljava/util/HashSet;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/download/cache/BaseCacheManager;

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/google/android/music/download/cache/BaseCacheManager;->handleGetFilteredIds()Ljava/util/HashSet;

    move-result-object v0

    return-object v0
.end method

.method private applyCacheStrategy(Lcom/google/android/music/download/cache/CacheLocation;JIZ)Z
    .locals 4
    .param p1, "location"    # Lcom/google/android/music/download/cache/CacheLocation;
    .param p2, "size"    # J
    .param p4, "cacheType"    # I
    .param p5, "auto"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/download/cache/OutOfSpaceException;
        }
    .end annotation

    .prologue
    .line 221
    invoke-virtual/range {p0 .. p5}, Lcom/google/android/music/download/cache/BaseCacheManager;->getAmountOfSpaceOverCapacity(Lcom/google/android/music/download/cache/CacheLocation;JIZ)J

    move-result-wide v0

    .line 223
    .local v0, "amountNeededToClear":J
    invoke-virtual {p0, p1, v0, v1}, Lcom/google/android/music/download/cache/BaseCacheManager;->tryFreeSpaceFromCache(Lcom/google/android/music/download/cache/CacheLocation;J)Z

    move-result v2

    return v2
.end method

.method private checkPossibleLocation(Ljava/io/File;Lcom/google/android/music/download/cache/CacheUtils$StorageType;J)Lcom/google/android/music/download/cache/CacheLocation;
    .locals 5
    .param p1, "path"    # Ljava/io/File;
    .param p2, "type"    # Lcom/google/android/music/download/cache/CacheUtils$StorageType;
    .param p3, "requestedSize"    # J

    .prologue
    .line 544
    const/4 v2, 0x0

    .line 545
    .local v2, "result":Lcom/google/android/music/download/cache/CacheLocation;
    if-eqz p1, :cond_0

    .line 546
    new-instance v0, Lcom/google/android/music/download/cache/CacheLocation;

    invoke-direct {v0, p1, p2}, Lcom/google/android/music/download/cache/CacheLocation;-><init>(Ljava/io/File;Lcom/google/android/music/download/cache/CacheUtils$StorageType;)V

    .line 547
    .local v0, "candidate":Lcom/google/android/music/download/cache/CacheLocation;
    invoke-direct {p0, v0}, Lcom/google/android/music/download/cache/BaseCacheManager;->prepareCacheLocation(Lcom/google/android/music/download/cache/CacheLocation;)Z

    move-result v1

    .line 548
    .local v1, "prepared":Z
    if-eqz v1, :cond_0

    invoke-virtual {p0, v0, p3, p4}, Lcom/google/android/music/download/cache/BaseCacheManager;->hasSpaceAtLocation(Lcom/google/android/music/download/cache/CacheLocation;J)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 549
    move-object v2, v0

    .line 552
    .end local v0    # "candidate":Lcom/google/android/music/download/cache/CacheLocation;
    .end local v1    # "prepared":Z
    :cond_0
    return-object v2
.end method

.method private getCacheLocationForDownload(Lcom/google/android/music/download/DownloadRequest;J)Lcom/google/android/music/download/cache/CacheLocation;
    .locals 18
    .param p1, "request"    # Lcom/google/android/music/download/DownloadRequest;
    .param p2, "downloadLength"    # J

    .prologue
    .line 280
    invoke-direct/range {p0 .. p0}, Lcom/google/android/music/download/cache/BaseCacheManager;->getPersistentCacheLocation()Lcom/google/android/music/download/cache/CacheLocation;

    move-result-object v3

    .line 281
    .local v3, "cacheLocation":Lcom/google/android/music/download/cache/CacheLocation;
    move-wide/from16 v4, p2

    .line 282
    .local v4, "spaceNeeded":J
    if-eqz v3, :cond_0

    invoke-interface/range {p1 .. p1}, Lcom/google/android/music/download/DownloadRequest;->getFileLocation()Lcom/google/android/music/download/cache/FileLocation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/music/download/cache/FileLocation;->getFullPath()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v3}, Lcom/google/android/music/download/cache/CacheLocation;->getPath()Ljava/io/File;

    move-result-object v7

    invoke-static {v2, v7}, Lcom/google/android/music/utils/IOUtils;->isFileInsideDirectory(Ljava/io/File;Ljava/io/File;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 286
    const-wide/16 v4, 0x0

    .line 288
    :cond_0
    const/4 v15, 0x0

    .line 289
    .local v15, "haveSpace":Z
    invoke-interface/range {p1 .. p1}, Lcom/google/android/music/download/DownloadRequest;->getFileLocation()Lcom/google/android/music/download/cache/FileLocation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/music/download/cache/FileLocation;->getCacheType()I

    move-result v6

    .line 290
    .local v6, "cacheType":I
    invoke-interface/range {p1 .. p1}, Lcom/google/android/music/download/DownloadRequest;->getOwner()Lcom/google/android/music/download/DownloadRequest$Owner;

    move-result-object v17

    .line 291
    .local v17, "owner":Lcom/google/android/music/download/DownloadRequest$Owner;
    if-eqz v3, :cond_1

    .line 293
    :try_start_0
    invoke-interface/range {v17 .. v17}, Lcom/google/android/music/download/DownloadRequest$Owner;->isAuto()Z

    move-result v7

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v7}, Lcom/google/android/music/download/cache/BaseCacheManager;->applyCacheStrategy(Lcom/google/android/music/download/cache/CacheLocation;JIZ)Z
    :try_end_0
    .catch Lcom/google/android/music/download/cache/OutOfSpaceException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v15

    .line 300
    :cond_1
    if-nez v15, :cond_7

    .line 304
    const/4 v2, 0x3

    if-ne v6, v2, :cond_2

    .line 305
    const/4 v2, 0x0

    .line 343
    :goto_0
    return-object v2

    .line 295
    :catch_0
    move-exception v14

    .line 296
    .local v14, "e":Lcom/google/android/music/download/cache/OutOfSpaceException;
    const-string v2, "CacheManagerImpl"

    invoke-virtual {v14}, Lcom/google/android/music/download/cache/OutOfSpaceException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-static {v2, v7}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 297
    const/4 v2, 0x0

    goto :goto_0

    .line 307
    .end local v14    # "e":Lcom/google/android/music/download/cache/OutOfSpaceException;
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/download/cache/BaseCacheManager;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/music/download/cache/CacheLocationManager;->getInstance(Landroid/content/Context;)Lcom/google/android/music/download/cache/CacheLocationManager;

    move-result-object v16

    .line 308
    .local v16, "manager":Lcom/google/android/music/download/cache/CacheLocationManager;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/download/cache/BaseCacheManager;->mContext:Landroid/content/Context;

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Lcom/google/android/music/download/cache/CacheLocationManager;->getInternal(Landroid/content/Context;)Lcom/google/android/music/download/cache/CacheLocation;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Lcom/google/android/music/download/cache/CacheLocationManager;->asMusicCacheLocation(Lcom/google/android/music/download/cache/CacheLocation;)Lcom/google/android/music/download/cache/CacheLocation;

    move-result-object v9

    .line 311
    .local v9, "internal":Lcom/google/android/music/download/cache/CacheLocation;
    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/google/android/music/download/cache/BaseCacheManager;->prepareCacheLocation(Lcom/google/android/music/download/cache/CacheLocation;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 312
    const/4 v2, 0x0

    goto :goto_0

    .line 315
    :cond_3
    invoke-virtual {v9, v3}, Lcom/google/android/music/download/cache/CacheLocation;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 316
    const/4 v2, 0x0

    goto :goto_0

    .line 318
    :cond_4
    sget-boolean v2, Lcom/google/android/music/download/cache/BaseCacheManager;->LOGV:Z

    if-eqz v2, :cond_5

    .line 319
    const-string v2, "CacheManagerImpl"

    const-string v7, "Insufficient space on %s, trying on internal"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v3, v8, v10

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v2, v7}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 323
    :cond_5
    :try_start_1
    invoke-interface/range {v17 .. v17}, Lcom/google/android/music/download/DownloadRequest$Owner;->isAuto()Z

    move-result v13

    move-object/from16 v8, p0

    move-wide/from16 v10, p2

    move v12, v6

    invoke-direct/range {v8 .. v13}, Lcom/google/android/music/download/cache/BaseCacheManager;->applyCacheStrategy(Lcom/google/android/music/download/cache/CacheLocation;JIZ)Z
    :try_end_1
    .catch Lcom/google/android/music/download/cache/OutOfSpaceException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v15

    .line 329
    if-nez v15, :cond_6

    .line 330
    const-string v2, "CacheManagerImpl"

    const-string v7, "Insufficient space on both internal and %s, unable to store download %s into persistent cache."

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v3, v8, v10

    const/4 v10, 0x1

    aput-object p1, v8, v10

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v2, v7}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 333
    const/4 v2, 0x0

    goto :goto_0

    .line 325
    :catch_1
    move-exception v14

    .line 326
    .restart local v14    # "e":Lcom/google/android/music/download/cache/OutOfSpaceException;
    const-string v2, "CacheManagerImpl"

    invoke-virtual {v14}, Lcom/google/android/music/download/cache/OutOfSpaceException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-static {v2, v7}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 327
    const/4 v2, 0x0

    goto :goto_0

    .line 336
    .end local v14    # "e":Lcom/google/android/music/download/cache/OutOfSpaceException;
    :cond_6
    move-object v3, v9

    .line 340
    .end local v9    # "internal":Lcom/google/android/music/download/cache/CacheLocation;
    .end local v16    # "manager":Lcom/google/android/music/download/cache/CacheLocationManager;
    :cond_7
    sget-boolean v2, Lcom/google/android/music/download/cache/BaseCacheManager;->LOGV:Z

    if-eqz v2, :cond_8

    .line 341
    const-string v2, "CacheManagerImpl"

    const-string v7, "Storage cache location found: %s"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v3, v8, v10

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v2, v7}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    :cond_8
    move-object v2, v3

    .line 343
    goto/16 :goto_0
.end method

.method protected static getLogFile()Lcom/google/android/music/log/LogFile;
    .locals 1

    .prologue
    .line 788
    sget-object v0, Lcom/google/android/music/download/cache/BaseCacheManager;->sLogFile:Lcom/google/android/music/log/LogFile;

    return-object v0
.end method

.method private getPersistentCacheLocation()Lcom/google/android/music/download/cache/CacheLocation;
    .locals 2

    .prologue
    .line 492
    const-wide/16 v0, -0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/music/download/cache/BaseCacheManager;->getCacheLocation(J)Lcom/google/android/music/download/cache/CacheLocation;

    move-result-object v0

    return-object v0
.end method

.method private getTempCacheLocation(J)Lcom/google/android/music/download/cache/CacheLocation;
    .locals 1
    .param p1, "requestedSize"    # J

    .prologue
    .line 483
    invoke-virtual {p0, p1, p2}, Lcom/google/android/music/download/cache/BaseCacheManager;->getCacheLocation(J)Lcom/google/android/music/download/cache/CacheLocation;

    move-result-object v0

    return-object v0
.end method

.method private handleDeleteFile(Ljava/io/File;)Z
    .locals 8
    .param p1, "file"    # Ljava/io/File;

    .prologue
    .line 421
    const/4 v2, 0x0

    .line 422
    .local v2, "shouldFilter":Z
    iget-object v5, p0, Lcom/google/android/music/download/cache/BaseCacheManager;->mDeleteFilters:Landroid/os/RemoteCallbackList;

    monitor-enter v5

    .line 423
    :try_start_0
    iget-object v4, p0, Lcom/google/android/music/download/cache/BaseCacheManager;->mDeleteFilters:Landroid/os/RemoteCallbackList;

    invoke-virtual {v4}, Landroid/os/RemoteCallbackList;->beginBroadcast()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v3

    .line 425
    .local v3, "size":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v3, :cond_0

    .line 427
    :try_start_1
    iget-object v4, p0, Lcom/google/android/music/download/cache/BaseCacheManager;->mDeleteFilters:Landroid/os/RemoteCallbackList;

    invoke-virtual {v4, v1}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v4

    check-cast v4, Lcom/google/android/music/download/cache/IDeleteFilter;

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v6}, Lcom/google/android/music/download/cache/IDeleteFilter;->shouldFilter(Ljava/lang/String;)Z
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v4

    if-eqz v4, :cond_1

    .line 428
    const/4 v2, 0x1

    .line 436
    :cond_0
    :try_start_2
    iget-object v4, p0, Lcom/google/android/music/download/cache/BaseCacheManager;->mDeleteFilters:Landroid/os/RemoteCallbackList;

    invoke-virtual {v4}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 438
    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 439
    if-nez v2, :cond_2

    .line 440
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    move-result v4

    .line 442
    :goto_1
    return v4

    .line 431
    :catch_0
    move-exception v0

    .line 432
    .local v0, "e":Landroid/os/RemoteException;
    :try_start_3
    const-string v4, "CacheManagerImpl"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Failed to call delete filter for "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6, v0}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 425
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 436
    :catchall_0
    move-exception v4

    :try_start_4
    iget-object v6, p0, Lcom/google/android/music/download/cache/BaseCacheManager;->mDeleteFilters:Landroid/os/RemoteCallbackList;

    invoke-virtual {v6}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    throw v4

    .line 438
    .end local v1    # "i":I
    .end local v3    # "size":I
    :catchall_1
    move-exception v4

    monitor-exit v5
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v4

    .line 442
    .restart local v1    # "i":I
    .restart local v3    # "size":I
    :cond_2
    const/4 v4, 0x0

    goto :goto_1
.end method

.method private handleGetFilteredIds()Ljava/util/HashSet;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashSet",
            "<",
            "Lcom/google/android/music/download/ContentIdentifier;",
            ">;"
        }
    .end annotation

    .prologue
    .line 446
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 447
    .local v0, "allFilteredIds":Ljava/util/HashSet;, "Ljava/util/HashSet<Lcom/google/android/music/download/ContentIdentifier;>;"
    iget-object v10, p0, Lcom/google/android/music/download/cache/BaseCacheManager;->mDeleteFilters:Landroid/os/RemoteCallbackList;

    monitor-enter v10

    .line 448
    :try_start_0
    iget-object v9, p0, Lcom/google/android/music/download/cache/BaseCacheManager;->mDeleteFilters:Landroid/os/RemoteCallbackList;

    invoke-virtual {v9}, Landroid/os/RemoteCallbackList;->beginBroadcast()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v8

    .line 450
    .local v8, "size":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v8, :cond_1

    .line 452
    :try_start_1
    iget-object v9, p0, Lcom/google/android/music/download/cache/BaseCacheManager;->mDeleteFilters:Landroid/os/RemoteCallbackList;

    invoke-virtual {v9, v3}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v9

    check-cast v9, Lcom/google/android/music/download/cache/IDeleteFilter;

    invoke-interface {v9}, Lcom/google/android/music/download/cache/IDeleteFilter;->getFilteredIds()[Lcom/google/android/music/download/ContentIdentifier;

    move-result-object v6

    .line 454
    .local v6, "ids":[Lcom/google/android/music/download/ContentIdentifier;
    if-eqz v6, :cond_0

    .line 455
    move-object v1, v6

    .local v1, "arr$":[Lcom/google/android/music/download/ContentIdentifier;
    array-length v7, v1

    .local v7, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_1
    if-ge v4, v7, :cond_0

    aget-object v5, v1, v4

    .line 456
    .local v5, "id":Lcom/google/android/music/download/ContentIdentifier;
    invoke-virtual {v0, v5}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 455
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 459
    .end local v1    # "arr$":[Lcom/google/android/music/download/ContentIdentifier;
    .end local v4    # "i$":I
    .end local v5    # "id":Lcom/google/android/music/download/ContentIdentifier;
    .end local v6    # "ids":[Lcom/google/android/music/download/ContentIdentifier;
    .end local v7    # "len$":I
    :catch_0
    move-exception v2

    .line 460
    .local v2, "e":Landroid/os/RemoteException;
    :try_start_2
    const-string v9, "CacheManagerImpl"

    const-string v11, "Failed to get deelte filter ids"

    invoke-static {v9, v11, v2}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 450
    .end local v2    # "e":Landroid/os/RemoteException;
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 464
    :cond_1
    :try_start_3
    iget-object v9, p0, Lcom/google/android/music/download/cache/BaseCacheManager;->mDeleteFilters:Landroid/os/RemoteCallbackList;

    invoke-virtual {v9}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 466
    monitor-exit v10

    .line 467
    return-object v0

    .line 464
    :catchall_0
    move-exception v9

    iget-object v11, p0, Lcom/google/android/music/download/cache/BaseCacheManager;->mDeleteFilters:Landroid/os/RemoteCallbackList;

    invoke-virtual {v11}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    throw v9

    .line 466
    .end local v3    # "i":I
    .end local v8    # "size":I
    :catchall_1
    move-exception v9

    monitor-exit v10
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v9
.end method

.method private initCacheStrategies()V
    .locals 12

    .prologue
    const-wide/16 v10, 0x400

    .line 624
    invoke-virtual {p0}, Lcom/google/android/music/download/cache/BaseCacheManager;->shouldAllowCaching()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/music/download/cache/BaseCacheManager;->mAllowCaching:Z

    .line 625
    iget-object v0, p0, Lcom/google/android/music/download/cache/BaseCacheManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v2

    .line 627
    .local v2, "store":Lcom/google/android/music/store/Store;
    invoke-virtual {p0}, Lcom/google/android/music/download/cache/BaseCacheManager;->getMinFreeSpaceMB()J

    move-result-wide v8

    .line 628
    .local v8, "minFreeSpaceMB":J
    iget-boolean v0, p0, Lcom/google/android/music/download/cache/BaseCacheManager;->mAllowCaching:Z

    if-eqz v0, :cond_0

    .line 629
    new-instance v0, Lcom/google/android/music/download/cache/FillUpToLimitCacheStrategy;

    iget-object v1, p0, Lcom/google/android/music/download/cache/BaseCacheManager;->mFileSystem:Lcom/google/android/music/download/cache/FileSystem;

    invoke-virtual {p0}, Lcom/google/android/music/download/cache/BaseCacheManager;->getMaxPercentageSpaceToUse()F

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/music/download/cache/BaseCacheManager;->getMaxBytesSpaceToUse()J

    move-result-wide v4

    mul-long v6, v8, v10

    mul-long/2addr v6, v10

    invoke-direct/range {v0 .. v7}, Lcom/google/android/music/download/cache/FillUpToLimitCacheStrategy;-><init>(Lcom/google/android/music/download/cache/FileSystem;Lcom/google/android/music/store/Store;FJJ)V

    iput-object v0, p0, Lcom/google/android/music/download/cache/BaseCacheManager;->mCacheStrategy:Lcom/google/android/music/download/cache/CacheStrategy;

    .line 633
    new-instance v0, Lcom/google/android/music/download/cache/LastUsedDeleteStrategy;

    iget-object v1, p0, Lcom/google/android/music/download/cache/BaseCacheManager;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/music/download/cache/BaseCacheManager;->mFileSystem:Lcom/google/android/music/download/cache/FileSystem;

    invoke-direct {v0, v1, v3, v2}, Lcom/google/android/music/download/cache/LastUsedDeleteStrategy;-><init>(Landroid/content/Context;Lcom/google/android/music/download/cache/FileSystem;Lcom/google/android/music/store/Store;)V

    iput-object v0, p0, Lcom/google/android/music/download/cache/BaseCacheManager;->mDeletionStrategy:Lcom/google/android/music/download/cache/DeletionStrategy;

    .line 639
    :goto_0
    iget-object v0, p0, Lcom/google/android/music/download/cache/BaseCacheManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "music_long_term_cache_min_free_space"

    const-wide/16 v4, 0x1f4

    invoke-static {v0, v1, v4, v5}, Lcom/google/android/gsf/Gservices;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v8

    .line 643
    new-instance v0, Lcom/google/android/music/download/cache/FillUpToLimitCacheStrategy;

    iget-object v1, p0, Lcom/google/android/music/download/cache/BaseCacheManager;->mFileSystem:Lcom/google/android/music/download/cache/FileSystem;

    const/high16 v3, -0x40800000    # -1.0f

    const-wide/16 v4, -0x1

    mul-long v6, v8, v10

    mul-long/2addr v6, v10

    invoke-direct/range {v0 .. v7}, Lcom/google/android/music/download/cache/FillUpToLimitCacheStrategy;-><init>(Lcom/google/android/music/download/cache/FileSystem;Lcom/google/android/music/store/Store;FJJ)V

    iput-object v0, p0, Lcom/google/android/music/download/cache/BaseCacheManager;->mLongTermCacheStrategy:Lcom/google/android/music/download/cache/CacheStrategy;

    .line 647
    return-void

    .line 635
    :cond_0
    new-instance v0, Lcom/google/android/music/download/cache/DeleteAllCacheStrategy;

    invoke-direct {v0}, Lcom/google/android/music/download/cache/DeleteAllCacheStrategy;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/download/cache/BaseCacheManager;->mCacheStrategy:Lcom/google/android/music/download/cache/CacheStrategy;

    .line 636
    new-instance v0, Lcom/google/android/music/download/cache/MaxFreeSpaceDeletionStrategy;

    iget-object v1, p0, Lcom/google/android/music/download/cache/BaseCacheManager;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/music/download/cache/BaseCacheManager;->mFileSystem:Lcom/google/android/music/download/cache/FileSystem;

    invoke-direct {v0, v1, v3, v2}, Lcom/google/android/music/download/cache/MaxFreeSpaceDeletionStrategy;-><init>(Landroid/content/Context;Lcom/google/android/music/download/cache/FileSystem;Lcom/google/android/music/store/Store;)V

    iput-object v0, p0, Lcom/google/android/music/download/cache/BaseCacheManager;->mDeletionStrategy:Lcom/google/android/music/download/cache/DeletionStrategy;

    goto :goto_0
.end method

.method private prepareCacheLocation(Lcom/google/android/music/download/cache/CacheLocation;)Z
    .locals 8
    .param p1, "location"    # Lcom/google/android/music/download/cache/CacheLocation;

    .prologue
    const/4 v4, 0x0

    .line 585
    if-nez p1, :cond_0

    .line 610
    :goto_0
    return v4

    .line 588
    :cond_0
    iget-object v5, p0, Lcom/google/android/music/download/cache/BaseCacheManager;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/google/android/music/download/cache/CacheLocationManager;->getInstance(Landroid/content/Context;)Lcom/google/android/music/download/cache/CacheLocationManager;

    move-result-object v5

    invoke-virtual {v5, p1}, Lcom/google/android/music/download/cache/CacheLocationManager;->asMusicCacheLocation(Lcom/google/android/music/download/cache/CacheLocation;)Lcom/google/android/music/download/cache/CacheLocation;

    move-result-object v2

    .line 590
    .local v2, "musicFiles":Lcom/google/android/music/download/cache/CacheLocation;
    if-nez v2, :cond_1

    .line 591
    move-object v2, p1

    .line 593
    :cond_1
    invoke-virtual {v2}, Lcom/google/android/music/download/cache/CacheLocation;->getPath()Ljava/io/File;

    move-result-object v0

    .line 594
    .local v0, "directory":Ljava/io/File;
    iget-object v5, p0, Lcom/google/android/music/download/cache/BaseCacheManager;->mFileSystem:Lcom/google/android/music/download/cache/FileSystem;

    invoke-interface {v5, v0}, Lcom/google/android/music/download/cache/FileSystem;->exists(Ljava/io/File;)Z

    move-result v5

    if-nez v5, :cond_2

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    move-result v5

    if-nez v5, :cond_2

    .line 595
    const-string v5, "CacheManagerImpl"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Could not create directory: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 598
    :cond_2
    new-instance v3, Ljava/io/File;

    const-string v5, ".nomedia"

    invoke-direct {v3, v0, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 599
    .local v3, "noMediaFile":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_3

    .line 601
    :try_start_0
    invoke-virtual {v3}, Ljava/io/File;->createNewFile()Z

    move-result v5

    if-nez v5, :cond_3

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_3

    .line 602
    const-string v5, "CacheManagerImpl"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Could not create: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 605
    :catch_0
    move-exception v1

    .line 606
    .local v1, "e":Ljava/io/IOException;
    const-string v5, "CacheManagerImpl"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Error while trying to create ("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "): "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 610
    .end local v1    # "e":Ljava/io/IOException;
    :cond_3
    const/4 v4, 0x1

    goto/16 :goto_0
.end method


# virtual methods
.method protected cleanUpDirectoryIfExists(Ljava/io/File;)V
    .locals 3
    .param p1, "dir"    # Ljava/io/File;

    .prologue
    .line 687
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 688
    const-string v0, "CacheManagerImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cleaning up an old dir: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 689
    invoke-static {p1}, Lcom/google/android/music/download/cache/CacheUtils;->cleanUpDirectory(Ljava/io/File;)Z

    .line 691
    :cond_0
    return-void
.end method

.method clearCachedFiles()V
    .locals 0

    .prologue
    .line 619
    invoke-direct {p0}, Lcom/google/android/music/download/cache/BaseCacheManager;->initCacheStrategies()V

    .line 620
    invoke-virtual {p0}, Lcom/google/android/music/download/cache/BaseCacheManager;->handleClearCache()V

    .line 621
    return-void
.end method

.method protected abstract clearOrphanedFiles()V
.end method

.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 1
    .param p1, "fd"    # Ljava/io/FileDescriptor;
    .param p2, "fout"    # Ljava/io/PrintWriter;
    .param p3, "args"    # [Ljava/lang/String;

    .prologue
    .line 775
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/music/download/cache/ICacheManager$Stub;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 776
    iget-object v0, p0, Lcom/google/android/music/download/cache/BaseCacheManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/music/download/cache/CacheLocationManager;->getInstance(Landroid/content/Context;)Lcom/google/android/music/download/cache/CacheLocationManager;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/music/download/cache/CacheLocationManager;->dump(Ljava/io/PrintWriter;)V

    .line 777
    return-void
.end method

.method protected getAmountOfSpaceOverCapacity(Lcom/google/android/music/download/cache/CacheLocation;JIZ)J
    .locals 4
    .param p1, "location"    # Lcom/google/android/music/download/cache/CacheLocation;
    .param p2, "size"    # J
    .param p4, "cacheType"    # I
    .param p5, "auto"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/download/cache/OutOfSpaceException;
        }
    .end annotation

    .prologue
    .line 241
    const/4 v2, 0x3

    if-eq p4, v2, :cond_0

    .line 242
    iget-object v2, p0, Lcom/google/android/music/download/cache/BaseCacheManager;->mCacheStrategy:Lcom/google/android/music/download/cache/CacheStrategy;

    invoke-virtual {v2, p2, p3, p1, p5}, Lcom/google/android/music/download/cache/CacheStrategy;->checkRequiredSpace(JLcom/google/android/music/download/cache/CacheLocation;Z)J

    move-result-wide v0

    .line 246
    .local v0, "spaceToDelete":J
    :goto_0
    return-wide v0

    .line 244
    .end local v0    # "spaceToDelete":J
    :cond_0
    iget-object v2, p0, Lcom/google/android/music/download/cache/BaseCacheManager;->mLongTermCacheStrategy:Lcom/google/android/music/download/cache/CacheStrategy;

    invoke-virtual {v2, p2, p3, p1, p5}, Lcom/google/android/music/download/cache/CacheStrategy;->checkRequiredSpace(JLcom/google/android/music/download/cache/CacheLocation;Z)J

    move-result-wide v0

    .restart local v0    # "spaceToDelete":J
    goto :goto_0
.end method

.method public getCacheLocation(J)Lcom/google/android/music/download/cache/CacheLocation;
    .locals 7
    .param p1, "requestedSize"    # J

    .prologue
    .line 505
    iget-object v4, p0, Lcom/google/android/music/download/cache/BaseCacheManager;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/google/android/music/download/cache/CacheLocationManager;->getInstance(Landroid/content/Context;)Lcom/google/android/music/download/cache/CacheLocationManager;

    move-result-object v1

    .line 506
    .local v1, "manager":Lcom/google/android/music/download/cache/CacheLocationManager;
    iget-object v4, p0, Lcom/google/android/music/download/cache/BaseCacheManager;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    invoke-virtual {v4}, Lcom/google/android/music/preferences/MusicPreferences;->getSelectedStorageVolumeId()Ljava/util/UUID;

    move-result-object v3

    .line 508
    .local v3, "selectedVolume":Ljava/util/UUID;
    if-nez v3, :cond_0

    .line 509
    invoke-virtual {v1}, Lcom/google/android/music/download/cache/CacheLocationManager;->getDefaultLocation()Lcom/google/android/music/download/cache/CacheLocation;

    move-result-object v0

    .line 513
    .local v0, "candidate":Lcom/google/android/music/download/cache/CacheLocation;
    :goto_0
    if-eqz v0, :cond_1

    .line 515
    invoke-virtual {v1, v0}, Lcom/google/android/music/download/cache/CacheLocationManager;->asMusicCacheLocation(Lcom/google/android/music/download/cache/CacheLocation;)Lcom/google/android/music/download/cache/CacheLocation;

    move-result-object v2

    .line 516
    .local v2, "musicLocation":Lcom/google/android/music/download/cache/CacheLocation;
    if-eqz v2, :cond_1

    .line 518
    invoke-virtual {v2}, Lcom/google/android/music/download/cache/CacheLocation;->getPath()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v2}, Lcom/google/android/music/download/cache/CacheLocation;->getStorageType()Lcom/google/android/music/download/cache/CacheUtils$StorageType;

    move-result-object v5

    invoke-direct {p0, v4, v5, p1, p2}, Lcom/google/android/music/download/cache/BaseCacheManager;->checkPossibleLocation(Ljava/io/File;Lcom/google/android/music/download/cache/CacheUtils$StorageType;J)Lcom/google/android/music/download/cache/CacheLocation;

    move-result-object v0

    .line 520
    if-eqz v0, :cond_1

    move-object v4, v0

    .line 540
    .end local v2    # "musicLocation":Lcom/google/android/music/download/cache/CacheLocation;
    :goto_1
    return-object v4

    .line 511
    .end local v0    # "candidate":Lcom/google/android/music/download/cache/CacheLocation;
    :cond_0
    invoke-virtual {v1, v3}, Lcom/google/android/music/download/cache/CacheLocationManager;->getKnownLocationByID(Ljava/util/UUID;)Lcom/google/android/music/download/cache/CacheLocation;

    move-result-object v0

    .restart local v0    # "candidate":Lcom/google/android/music/download/cache/CacheLocation;
    goto :goto_0

    .line 527
    :cond_1
    if-eqz v3, :cond_2

    const-wide/16 v4, -0x1

    cmp-long v4, p1, v4

    if-nez v4, :cond_3

    :cond_2
    const/4 v4, 0x0

    goto :goto_1

    .line 532
    :cond_3
    iget-boolean v4, p0, Lcom/google/android/music/download/cache/BaseCacheManager;->mIsExternalSameAsInternal:Z

    if-nez v4, :cond_4

    .line 533
    iget-object v4, p0, Lcom/google/android/music/download/cache/BaseCacheManager;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/google/android/music/download/cache/CacheUtils;->getExternalMusicCacheDirectory(Landroid/content/Context;)Ljava/io/File;

    move-result-object v4

    sget-object v5, Lcom/google/android/music/download/cache/CacheUtils$StorageType;->EXTERNAL:Lcom/google/android/music/download/cache/CacheUtils$StorageType;

    invoke-direct {p0, v4, v5, p1, p2}, Lcom/google/android/music/download/cache/BaseCacheManager;->checkPossibleLocation(Ljava/io/File;Lcom/google/android/music/download/cache/CacheUtils$StorageType;J)Lcom/google/android/music/download/cache/CacheLocation;

    move-result-object v0

    .line 535
    if-eqz v0, :cond_4

    move-object v4, v0

    goto :goto_1

    .line 538
    :cond_4
    iget-object v4, p0, Lcom/google/android/music/download/cache/BaseCacheManager;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/google/android/music/download/cache/CacheUtils;->getInternalMusicCacheDirectory(Landroid/content/Context;)Ljava/io/File;

    move-result-object v4

    sget-object v5, Lcom/google/android/music/download/cache/CacheUtils$StorageType;->INTERNAL:Lcom/google/android/music/download/cache/CacheUtils$StorageType;

    invoke-direct {p0, v4, v5, p1, p2}, Lcom/google/android/music/download/cache/BaseCacheManager;->checkPossibleLocation(Ljava/io/File;Lcom/google/android/music/download/cache/CacheUtils$StorageType;J)Lcom/google/android/music/download/cache/CacheLocation;

    move-result-object v0

    move-object v4, v0

    .line 540
    goto :goto_1
.end method

.method protected getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 780
    iget-object v0, p0, Lcom/google/android/music/download/cache/BaseCacheManager;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method protected getFilteredFileDeleter()Lcom/google/android/music/download/cache/FilteredFileDeleter;
    .locals 1

    .prologue
    .line 792
    iget-object v0, p0, Lcom/google/android/music/download/cache/BaseCacheManager;->mFilteredFileDeleter:Lcom/google/android/music/download/cache/FilteredFileDeleter;

    return-object v0
.end method

.method public getFreePersistentStorageSpaceInBytes()J
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 90
    invoke-direct {p0}, Lcom/google/android/music/download/cache/BaseCacheManager;->getPersistentCacheLocation()Lcom/google/android/music/download/cache/CacheLocation;

    move-result-object v2

    .line 91
    .local v2, "cacheLocation":Lcom/google/android/music/download/cache/CacheLocation;
    if-nez v2, :cond_0

    .line 92
    const-wide/16 v0, 0x0

    .line 105
    :goto_0
    return-wide v0

    .line 95
    :cond_0
    iget-object v3, p0, Lcom/google/android/music/download/cache/BaseCacheManager;->mFileSystem:Lcom/google/android/music/download/cache/FileSystem;

    invoke-virtual {v2}, Lcom/google/android/music/download/cache/CacheLocation;->getPath()Ljava/io/File;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/google/android/music/download/cache/FileSystem;->getFreeSpace(Ljava/io/File;)J

    move-result-wide v0

    .line 99
    .local v0, "availableFreeSpace":J
    iget-object v3, p0, Lcom/google/android/music/download/cache/BaseCacheManager;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v3

    const/16 v4, 0x64

    invoke-virtual {v3, v4}, Lcom/google/android/music/store/Store;->getTotalCachedSize(I)J

    move-result-wide v4

    add-long/2addr v0, v4

    .line 103
    iget-object v3, p0, Lcom/google/android/music/download/cache/BaseCacheManager;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/music/store/Store;->getSizeOfUndownloadedKeepOnFiles()J

    move-result-wide v4

    sub-long/2addr v0, v4

    .line 105
    goto :goto_0
.end method

.method protected abstract getMaxBytesSpaceToUse()J
.end method

.method protected abstract getMaxPercentageSpaceToUse()F
.end method

.method protected abstract getMinFreeSpaceMB()J
.end method

.method protected getMusicPreferences()Lcom/google/android/music/preferences/MusicPreferences;
    .locals 1

    .prologue
    .line 784
    iget-object v0, p0, Lcom/google/android/music/download/cache/BaseCacheManager;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    return-object v0
.end method

.method protected abstract getOwnerFromInt(I)Lcom/google/android/music/download/DownloadRequest$Owner;
.end method

.method public getStorageSizes()Lcom/google/android/music/download/cache/StorageSizes;
    .locals 23

    .prologue
    .line 120
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/music/download/cache/BaseCacheManager;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/music/store/Store;->getStoredMusicSize()J

    move-result-wide v6

    .line 121
    .local v6, "musicSize":J
    const-wide/16 v4, 0x0

    .line 122
    .local v4, "total":J
    const-wide/16 v8, 0x0

    .line 123
    .local v8, "free":J
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/music/download/cache/BaseCacheManager;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/google/android/music/download/cache/CacheLocationManager;->getInstance(Landroid/content/Context;)Lcom/google/android/music/download/cache/CacheLocationManager;

    move-result-object v18

    .line 124
    .local v18, "manager":Lcom/google/android/music/download/cache/CacheLocationManager;
    invoke-virtual/range {v18 .. v18}, Lcom/google/android/music/download/cache/CacheLocationManager;->getKnownUsableLocations()Ljava/util/Collection;

    move-result-object v2

    .line 125
    .local v2, "cacheDirs":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/music/download/cache/CacheLocation;>;"
    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v13

    .local v13, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/google/android/music/download/cache/CacheLocation;

    .line 126
    .local v12, "cacheLocation":Lcom/google/android/music/download/cache/CacheLocation;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/music/download/cache/BaseCacheManager;->mFileSystem:Lcom/google/android/music/download/cache/FileSystem;

    invoke-virtual {v12}, Lcom/google/android/music/download/cache/CacheLocation;->getPath()Ljava/io/File;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-interface {v3, v0}, Lcom/google/android/music/download/cache/FileSystem;->getTotalSpace(Ljava/io/File;)J

    move-result-wide v16

    .line 127
    .local v16, "locationTotal":J
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/music/download/cache/BaseCacheManager;->mFileSystem:Lcom/google/android/music/download/cache/FileSystem;

    invoke-virtual {v12}, Lcom/google/android/music/download/cache/CacheLocation;->getPath()Ljava/io/File;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-interface {v3, v0}, Lcom/google/android/music/download/cache/FileSystem;->getFreeSpace(Ljava/io/File;)J

    move-result-wide v14

    .line 128
    .local v14, "locationFree":J
    sget-boolean v3, Lcom/google/android/music/download/cache/BaseCacheManager;->LOGV:Z

    if-eqz v3, :cond_0

    .line 129
    const-string v3, "CacheManagerImpl"

    const-string v19, "getStorageSizes: location=%s total=%s free=%s "

    const/16 v20, 0x3

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    aput-object v12, v20, v21

    const/16 v21, 0x1

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v22

    aput-object v22, v20, v21

    const/16 v21, 0x2

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v22

    aput-object v22, v20, v21

    invoke-static/range {v19 .. v20}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-static {v3, v0}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    :cond_0
    add-long v4, v4, v16

    .line 133
    add-long/2addr v8, v14

    .line 134
    goto :goto_0

    .line 136
    .end local v12    # "cacheLocation":Lcom/google/android/music/download/cache/CacheLocation;
    .end local v14    # "locationFree":J
    .end local v16    # "locationTotal":J
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/music/download/cache/BaseCacheManager;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/music/store/Store;->getSizeOfUndownloadedKeepOnFiles()J

    move-result-wide v10

    .line 137
    .local v10, "notDownloaded":J
    sget-boolean v3, Lcom/google/android/music/download/cache/BaseCacheManager;->LOGV:Z

    if-eqz v3, :cond_2

    .line 138
    const-string v3, "CacheManagerImpl"

    const-string v19, "getStorageSizes: total=%s free=%s notDownloaded=%s"

    const/16 v20, 0x3

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v22

    aput-object v22, v20, v21

    const/16 v21, 0x1

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v22

    aput-object v22, v20, v21

    const/16 v21, 0x2

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v22

    aput-object v22, v20, v21

    invoke-static/range {v19 .. v20}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-static {v3, v0}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    :cond_2
    new-instance v3, Lcom/google/android/music/download/cache/StorageSizes;

    invoke-direct/range {v3 .. v11}, Lcom/google/android/music/download/cache/StorageSizes;-><init>(JJJJ)V

    return-object v3
.end method

.method public getTempFileLocation(Lcom/google/android/music/download/ContentIdentifier;IJI)Lcom/google/android/music/download/cache/FileLocation;
    .locals 13
    .param p1, "id"    # Lcom/google/android/music/download/ContentIdentifier;
    .param p2, "owner"    # I
    .param p3, "size"    # J
    .param p5, "cacheType"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 165
    invoke-virtual {p0, p2}, Lcom/google/android/music/download/cache/BaseCacheManager;->getOwnerFromInt(I)Lcom/google/android/music/download/DownloadRequest$Owner;

    move-result-object v11

    .line 167
    .local v11, "requestOwner":Lcom/google/android/music/download/DownloadRequest$Owner;
    sget-boolean v2, Lcom/google/android/music/download/cache/BaseCacheManager;->LOGV:Z

    if-eqz v2, :cond_0

    .line 168
    const-string v2, "CacheManagerImpl"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getTempFileLocation: id="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " cacheType="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, p5

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    :cond_0
    invoke-interface {v11}, Lcom/google/android/music/download/DownloadRequest$Owner;->isAuto()Z

    move-result v7

    .line 173
    .local v7, "auto":Z
    const/4 v2, 0x3

    move/from16 v0, p5

    if-eq v0, v2, :cond_1

    if-eqz v7, :cond_3

    .line 174
    :cond_1
    invoke-direct {p0}, Lcom/google/android/music/download/cache/BaseCacheManager;->getPersistentCacheLocation()Lcom/google/android/music/download/cache/CacheLocation;

    move-result-object v3

    .line 176
    .local v3, "cacheLocation":Lcom/google/android/music/download/cache/CacheLocation;
    if-eqz v3, :cond_2

    move-object v2, p0

    move-wide/from16 v4, p3

    move/from16 v6, p5

    :try_start_0
    invoke-direct/range {v2 .. v7}, Lcom/google/android/music/download/cache/BaseCacheManager;->applyCacheStrategy(Lcom/google/android/music/download/cache/CacheLocation;JIZ)Z
    :try_end_0
    .catch Lcom/google/android/music/download/cache/OutOfSpaceException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-nez v2, :cond_4

    .line 178
    :cond_2
    const/4 v2, 0x0

    .line 194
    :goto_0
    return-object v2

    .line 180
    :catch_0
    move-exception v8

    .line 181
    .local v8, "e":Lcom/google/android/music/download/cache/OutOfSpaceException;
    const-string v2, "CacheManagerImpl"

    invoke-virtual {v8}, Lcom/google/android/music/download/cache/OutOfSpaceException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    const/4 v2, 0x0

    goto :goto_0

    .line 185
    .end local v3    # "cacheLocation":Lcom/google/android/music/download/cache/CacheLocation;
    .end local v8    # "e":Lcom/google/android/music/download/cache/OutOfSpaceException;
    :cond_3
    move-wide/from16 v0, p3

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/download/cache/BaseCacheManager;->getTempCacheLocation(J)Lcom/google/android/music/download/cache/CacheLocation;

    move-result-object v3

    .line 187
    .restart local v3    # "cacheLocation":Lcom/google/android/music/download/cache/CacheLocation;
    :cond_4
    if-nez v3, :cond_5

    .line 188
    const/4 v2, 0x0

    goto :goto_0

    .line 191
    :cond_5
    const-string v2, "%s_%s.tmp"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-interface {v11}, Lcom/google/android/music/download/DownloadRequest$Owner;->toFileSystemString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-virtual {p1}, Lcom/google/android/music/download/ContentIdentifier;->toFileSystemString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    .line 193
    .local v10, "relativePath":Ljava/lang/String;
    invoke-virtual {v3, v10}, Lcom/google/android/music/download/cache/CacheLocation;->getCacheFile(Ljava/lang/String;)Ljava/io/File;

    move-result-object v9

    .line 194
    .local v9, "fullPath":Ljava/io/File;
    new-instance v2, Lcom/google/android/music/download/cache/FileLocation;

    invoke-virtual {v3}, Lcom/google/android/music/download/cache/CacheLocation;->getStorageType()Lcom/google/android/music/download/cache/CacheUtils$StorageType;

    move-result-object v4

    move/from16 v0, p5

    invoke-direct {v2, v9, v4, v0}, Lcom/google/android/music/download/cache/FileLocation;-><init>(Ljava/io/File;Lcom/google/android/music/download/cache/CacheUtils$StorageType;I)V

    goto :goto_0
.end method

.method public getTotalPersistentStorageSpaceInBytes()J
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 110
    invoke-direct {p0}, Lcom/google/android/music/download/cache/BaseCacheManager;->getPersistentCacheLocation()Lcom/google/android/music/download/cache/CacheLocation;

    move-result-object v0

    .line 111
    .local v0, "cacheLocation":Lcom/google/android/music/download/cache/CacheLocation;
    if-nez v0, :cond_0

    .line 112
    const-wide/16 v2, 0x0

    .line 115
    :goto_0
    return-wide v2

    :cond_0
    iget-object v1, p0, Lcom/google/android/music/download/cache/BaseCacheManager;->mFileSystem:Lcom/google/android/music/download/cache/FileSystem;

    invoke-virtual {v0}, Lcom/google/android/music/download/cache/CacheLocation;->getPath()Ljava/io/File;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/music/download/cache/FileSystem;->getTotalSpace(Ljava/io/File;)J

    move-result-wide v2

    goto :goto_0
.end method

.method protected abstract handleClearCache()V
.end method

.method protected abstract handleLowStorage()V
.end method

.method public hasSpaceAtLocation(Lcom/google/android/music/download/cache/CacheLocation;J)Z
    .locals 6
    .param p1, "cacheLocation"    # Lcom/google/android/music/download/cache/CacheLocation;
    .param p2, "requestedSize"    # J

    .prologue
    const/4 v1, 0x1

    .line 566
    if-eqz p1, :cond_3

    .line 567
    invoke-virtual {p1}, Lcom/google/android/music/download/cache/CacheLocation;->getPath()Ljava/io/File;

    move-result-object v0

    .line 568
    .local v0, "path":Ljava/io/File;
    invoke-virtual {p1}, Lcom/google/android/music/download/cache/CacheLocation;->getStorageType()Lcom/google/android/music/download/cache/CacheUtils$StorageType;

    move-result-object v4

    sget-object v5, Lcom/google/android/music/download/cache/CacheUtils$StorageType;->INTERNAL:Lcom/google/android/music/download/cache/CacheUtils$StorageType;

    if-eq v4, v5, :cond_0

    .line 571
    :cond_0
    const-wide/16 v4, 0x0

    cmp-long v4, p2, v4

    if-gtz v4, :cond_2

    .line 577
    .end local v0    # "path":Ljava/io/File;
    :cond_1
    :goto_0
    return v1

    .line 572
    .restart local v0    # "path":Ljava/io/File;
    :cond_2
    iget-object v4, p0, Lcom/google/android/music/download/cache/BaseCacheManager;->mFileSystem:Lcom/google/android/music/download/cache/FileSystem;

    invoke-interface {v4, v0}, Lcom/google/android/music/download/cache/FileSystem;->getFreeSpace(Ljava/io/File;)J

    move-result-wide v2

    .line 573
    .local v2, "size":J
    cmp-long v4, v2, p2

    if-gtz v4, :cond_1

    .line 577
    .end local v0    # "path":Ljava/io/File;
    .end local v2    # "size":J
    :cond_3
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public registerDeleteFilter(Lcom/google/android/music/download/cache/IDeleteFilter;)V
    .locals 1
    .param p1, "filter"    # Lcom/google/android/music/download/cache/IDeleteFilter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 148
    if-eqz p1, :cond_0

    .line 149
    iget-object v0, p0, Lcom/google/android/music/download/cache/BaseCacheManager;->mDeleteFilters:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0, p1}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;)Z

    .line 151
    :cond_0
    return-void
.end method

.method public requestDelete(Lcom/google/android/music/download/DownloadRequest;)V
    .locals 1
    .param p1, "request"    # Lcom/google/android/music/download/DownloadRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 472
    invoke-interface {p1}, Lcom/google/android/music/download/DownloadRequest;->getFileLocation()Lcom/google/android/music/download/cache/FileLocation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/download/cache/FileLocation;->getFullPath()Ljava/io/File;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/music/download/cache/BaseCacheManager;->handleDeleteFile(Ljava/io/File;)Z

    .line 473
    return-void
.end method

.method setAllowCaching(Z)V
    .locals 0
    .param p1, "allow"    # Z

    .prologue
    .line 614
    iput-boolean p1, p0, Lcom/google/android/music/download/cache/BaseCacheManager;->mAllowCaching:Z

    .line 615
    invoke-direct {p0}, Lcom/google/android/music/download/cache/BaseCacheManager;->initCacheStrategies()V

    .line 616
    return-void
.end method

.method protected abstract shouldAllowCaching()Z
.end method

.method public storeInCache(Lcom/google/android/music/download/DownloadRequest;Ljava/lang/String;JI)Ljava/lang/String;
    .locals 25
    .param p1, "request"    # Lcom/google/android/music/download/DownloadRequest;
    .param p2, "httpContentType"    # Ljava/lang/String;
    .param p3, "downloadLength"    # J
    .param p5, "streamFidelity"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 350
    sget-boolean v6, Lcom/google/android/music/download/cache/BaseCacheManager;->LOGV:Z

    if-eqz v6, :cond_0

    .line 351
    const-string v6, "CacheManagerImpl"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "storeInCache: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 354
    :cond_0
    invoke-interface/range {p1 .. p1}, Lcom/google/android/music/download/DownloadRequest;->getId()Lcom/google/android/music/download/ContentIdentifier;

    move-result-object v21

    .line 355
    .local v21, "id":Lcom/google/android/music/download/ContentIdentifier;
    invoke-virtual/range {v21 .. v21}, Lcom/google/android/music/download/ContentIdentifier;->isCacheable()Z

    move-result v6

    if-nez v6, :cond_1

    .line 356
    const-string v6, "CacheManagerImpl"

    const-string v7, "Trying to cache track from non cacheable domain"

    invoke-static {v6, v7}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 357
    const/4 v6, 0x0

    .line 417
    :goto_0
    return-object v6

    .line 359
    :cond_1
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/music/download/cache/BaseCacheManager;->mContext:Landroid/content/Context;

    invoke-static {v6}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v5

    .line 361
    .local v5, "store":Lcom/google/android/music/store/Store;
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-wide/from16 v2, p3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/music/download/cache/BaseCacheManager;->getCacheLocationForDownload(Lcom/google/android/music/download/DownloadRequest;J)Lcom/google/android/music/download/cache/CacheLocation;

    move-result-object v4

    .line 362
    .local v4, "cacheLocation":Lcom/google/android/music/download/cache/CacheLocation;
    if-nez v4, :cond_2

    .line 363
    const-string v6, "CacheManagerImpl"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Failed to store the requested download in cache:  "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v21

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 364
    const/4 v6, 0x0

    goto :goto_0

    .line 367
    :cond_2
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    .line 368
    .local v18, "finalFilename":Ljava/lang/StringBuilder;
    invoke-virtual/range {v21 .. v21}, Lcom/google/android/music/download/ContentIdentifier;->toFileSystemString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v18

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 369
    sget-object v6, Lcom/google/android/music/download/DownloadUtils;->MimeToExtensionMap:Lcom/google/common/collect/ImmutableMap;

    move-object/from16 v0, p2

    invoke-virtual {v6, v0}, Lcom/google/common/collect/ImmutableMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/String;

    .line 370
    .local v17, "extension":Ljava/lang/String;
    if-nez v17, :cond_3

    .line 371
    const-string v6, "CacheManagerImpl"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Missing file extension for download request: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 372
    const/4 v6, 0x0

    goto :goto_0

    .line 374
    :cond_3
    const/16 v6, 0x2e

    move-object/from16 v0, v18

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 375
    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 378
    invoke-interface/range {p1 .. p1}, Lcom/google/android/music/download/DownloadRequest;->getFileLocation()Lcom/google/android/music/download/cache/FileLocation;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/music/download/cache/FileLocation;->getFullPath()Ljava/io/File;

    move-result-object v20

    .line 379
    .local v20, "fullPath":Ljava/io/File;
    invoke-virtual/range {v20 .. v20}, Ljava/io/File;->length()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-nez v6, :cond_4

    .line 380
    const-string v6, "CacheManagerImpl"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Failed to store empty file in cache: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual/range {v20 .. v20}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 381
    const/4 v6, 0x0

    goto/16 :goto_0

    .line 383
    :cond_4
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/google/android/music/download/cache/CacheLocation;->getCacheFile(Ljava/lang/String;)Ljava/io/File;

    move-result-object v22

    .line 385
    .local v22, "newPath":Ljava/io/File;
    invoke-virtual/range {v22 .. v22}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_5

    invoke-virtual/range {v22 .. v22}, Ljava/io/File;->delete()Z

    move-result v6

    if-eqz v6, :cond_8

    :cond_5
    const/4 v15, 0x1

    .line 386
    .local v15, "deleteSuccess":Z
    :goto_1
    if-nez v15, :cond_6

    .line 387
    const-string v6, "CacheManagerImpl"

    const-string v7, "Unable to delete file at %s"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v22, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 389
    :cond_6
    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v23

    .line 390
    .local v23, "renameSuccess":Z
    if-nez v23, :cond_7

    .line 392
    :try_start_0
    const-string v6, "CacheManagerImpl"

    const-string v7, "Unable to rename file from %s to %s, falling back to copy."

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v20, v8, v9

    const/4 v9, 0x1

    aput-object v22, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 395
    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Lcom/google/common/io/Files;->copy(Ljava/io/File;Ljava/io/File;)V

    .line 396
    invoke-virtual/range {v20 .. v20}, Ljava/io/File;->delete()Z

    move-result v6

    if-nez v6, :cond_7

    .line 397
    const-string v6, "CacheManagerImpl"

    const-string v7, "Unable to remove old temporary file at %s"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v20, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 411
    :cond_7
    new-instance v19, Lcom/google/android/music/download/cache/FileLocation;

    invoke-virtual {v4}, Lcom/google/android/music/download/cache/CacheLocation;->getStorageType()Lcom/google/android/music/download/cache/CacheUtils$StorageType;

    move-result-object v6

    invoke-interface/range {p1 .. p1}, Lcom/google/android/music/download/DownloadRequest;->getFileLocation()Lcom/google/android/music/download/cache/FileLocation;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/music/download/cache/FileLocation;->getCacheType()I

    move-result v7

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-direct {v0, v1, v6, v7}, Lcom/google/android/music/download/cache/FileLocation;-><init>(Ljava/io/File;Lcom/google/android/music/download/cache/CacheUtils$StorageType;I)V

    .line 413
    .local v19, "finalLocation":Lcom/google/android/music/download/cache/FileLocation;
    invoke-virtual/range {v21 .. v21}, Lcom/google/android/music/download/ContentIdentifier;->getId()J

    move-result-wide v6

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/music/download/cache/FileLocation;->getSchemaValueForCacheType()I

    move-result v9

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/music/download/cache/FileLocation;->getSchemaValueForStorageType()I

    move-result v12

    invoke-virtual {v4}, Lcom/google/android/music/download/cache/CacheLocation;->getVolumeId()Ljava/util/UUID;

    move-result-object v13

    move-wide/from16 v10, p3

    move/from16 v14, p5

    invoke-virtual/range {v5 .. v14}, Lcom/google/android/music/store/Store;->updateCachedFileLocation(JLjava/lang/String;IJILjava/util/UUID;I)V

    .line 417
    invoke-virtual/range {v22 .. v22}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_0

    .line 385
    .end local v15    # "deleteSuccess":Z
    .end local v19    # "finalLocation":Lcom/google/android/music/download/cache/FileLocation;
    .end local v23    # "renameSuccess":Z
    :cond_8
    const/4 v15, 0x0

    goto/16 :goto_1

    .line 400
    .restart local v15    # "deleteSuccess":Z
    .restart local v23    # "renameSuccess":Z
    :catch_0
    move-exception v16

    .line 401
    .local v16, "e":Ljava/io/IOException;
    const-string v6, "CacheManagerImpl"

    const-string v7, "Failed to rename or copy file %s to file %s."

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-virtual/range {v20 .. v20}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    invoke-virtual/range {v22 .. v22}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 403
    const/4 v6, 0x0

    goto/16 :goto_0
.end method

.method protected tryFreeSpaceFromCache(Lcom/google/android/music/download/cache/CacheLocation;J)Z
    .locals 4
    .param p1, "cacheLocation"    # Lcom/google/android/music/download/cache/CacheLocation;
    .param p2, "size"    # J

    .prologue
    .line 257
    const-wide/16 v2, 0x0

    cmp-long v1, p2, v2

    if-lez v1, :cond_1

    .line 258
    iget-object v1, p0, Lcom/google/android/music/download/cache/BaseCacheManager;->mDeletionStrategy:Lcom/google/android/music/download/cache/DeletionStrategy;

    iget-object v2, p0, Lcom/google/android/music/download/cache/BaseCacheManager;->mFilteredFileDeleter:Lcom/google/android/music/download/cache/FilteredFileDeleter;

    invoke-interface {v1, p2, p3, p1, v2}, Lcom/google/android/music/download/cache/DeletionStrategy;->createSpace(JLcom/google/android/music/download/cache/CacheLocation;Lcom/google/android/music/download/cache/FilteredFileDeleter;)Z

    move-result v0

    .line 260
    .local v0, "success":Z
    sget-boolean v1, Lcom/google/android/music/download/cache/BaseCacheManager;->LOGV:Z

    if-eqz v1, :cond_0

    if-nez v0, :cond_0

    .line 261
    const-string v1, "CacheManagerImpl"

    const-string v2, "Failed to find storage cache space"

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 265
    .end local v0    # "success":Z
    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public unregisterDeleteFilter(Lcom/google/android/music/download/cache/IDeleteFilter;)V
    .locals 1
    .param p1, "filter"    # Lcom/google/android/music/download/cache/IDeleteFilter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 155
    if-eqz p1, :cond_0

    .line 156
    iget-object v0, p0, Lcom/google/android/music/download/cache/BaseCacheManager;->mDeleteFilters:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0, p1}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z

    .line 158
    :cond_0
    return-void
.end method

.method protected validateLocalFiles(Ljava/util/Set;Ljava/io/File;Z)V
    .locals 10
    .param p2, "fileOrDir"    # Ljava/io/File;
    .param p3, "isExternal"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/io/File;",
            "Z)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 718
    .local p1, "knownFiles":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    if-nez p2, :cond_2

    .line 719
    sget-boolean v6, Lcom/google/android/music/download/cache/BaseCacheManager;->LOGV:Z

    if-eqz v6, :cond_0

    .line 720
    const-string v6, "CacheManagerImpl"

    const-string v7, "Cached file or directory is null"

    invoke-static {v6, v7}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 722
    :cond_0
    sget-object v6, Lcom/google/android/music/download/cache/BaseCacheManager;->sLogFile:Lcom/google/android/music/log/LogFile;

    if-eqz v6, :cond_1

    .line 723
    sget-object v6, Lcom/google/android/music/download/cache/BaseCacheManager;->sLogFile:Lcom/google/android/music/log/LogFile;

    const-string v7, "CacheManagerImpl"

    const-string v8, "Cached file or directory is null"

    invoke-virtual {v6, v7, v8}, Lcom/google/android/music/log/LogFile;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 771
    :cond_1
    :goto_0
    return-void

    .line 727
    :cond_2
    invoke-virtual {p2}, Ljava/io/File;->exists()Z

    move-result v6

    if-nez v6, :cond_6

    .line 728
    sget-boolean v6, Lcom/google/android/music/download/cache/BaseCacheManager;->LOGV:Z

    if-eqz v6, :cond_3

    .line 729
    const-string v6, "CacheManagerImpl"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Cached file or directory \""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\" does not exist."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 732
    :cond_3
    sget-object v6, Lcom/google/android/music/download/cache/BaseCacheManager;->sLogFile:Lcom/google/android/music/log/LogFile;

    if-eqz v6, :cond_4

    .line 733
    sget-object v6, Lcom/google/android/music/download/cache/BaseCacheManager;->sLogFile:Lcom/google/android/music/log/LogFile;

    const-string v7, "CacheManagerImpl"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Cached file or directory \""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "\" does not exist."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lcom/google/android/music/log/LogFile;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 736
    :cond_4
    if-eqz p3, :cond_1

    invoke-static {}, Lcom/google/android/music/download/cache/CacheUtils;->isExternalStorageMounted()Z

    move-result v6

    if-nez v6, :cond_1

    .line 737
    sget-object v6, Lcom/google/android/music/download/cache/BaseCacheManager;->sLogFile:Lcom/google/android/music/log/LogFile;

    if-eqz v6, :cond_5

    .line 738
    sget-object v6, Lcom/google/android/music/download/cache/BaseCacheManager;->sLogFile:Lcom/google/android/music/log/LogFile;

    const-string v7, "CacheManagerImpl"

    const-string v8, "External storage not mounted"

    invoke-virtual {v6, v7, v8}, Lcom/google/android/music/log/LogFile;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 740
    :cond_5
    new-instance v6, Ljava/io/IOException;

    const-string v7, "External storage not mounted"

    invoke-direct {v6, v7}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 744
    :cond_6
    invoke-virtual {p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    .line 745
    .local v0, "absolutePath":Ljava/lang/String;
    invoke-virtual {p2}, Ljava/io/File;->isFile()Z

    move-result v6

    if-eqz v6, :cond_8

    .line 746
    const-string v6, ".nomedia"

    invoke-virtual {p2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 747
    invoke-interface {p1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 748
    iget-object v6, p0, Lcom/google/android/music/download/cache/BaseCacheManager;->mFileSystem:Lcom/google/android/music/download/cache/FileSystem;

    invoke-interface {v6, p2}, Lcom/google/android/music/download/cache/FileSystem;->delete(Ljava/io/File;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 749
    sget-boolean v6, Lcom/google/android/music/download/cache/BaseCacheManager;->LOGV:Z

    if-eqz v6, :cond_7

    .line 750
    const-string v6, "CacheManagerImpl"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Deleted orphaned file: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 752
    :cond_7
    sget-object v6, Lcom/google/android/music/download/cache/BaseCacheManager;->sLogFile:Lcom/google/android/music/log/LogFile;

    if-eqz v6, :cond_1

    .line 753
    sget-object v6, Lcom/google/android/music/download/cache/BaseCacheManager;->sLogFile:Lcom/google/android/music/log/LogFile;

    const-string v7, "CacheManagerImpl"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Deleted orphaned file: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lcom/google/android/music/log/LogFile;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 759
    :cond_8
    invoke-virtual {p2}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v3

    .line 760
    .local v3, "files":[Ljava/io/File;
    if-nez v3, :cond_9

    .line 761
    sget-boolean v6, Lcom/google/android/music/download/cache/BaseCacheManager;->LOGV:Z

    if-eqz v6, :cond_1

    const-string v6, "CacheManagerImpl"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Neither file nor directory: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 763
    :cond_9
    sget-object v6, Lcom/google/android/music/download/cache/BaseCacheManager;->sLogFile:Lcom/google/android/music/log/LogFile;

    if-eqz v6, :cond_a

    .line 764
    sget-object v6, Lcom/google/android/music/download/cache/BaseCacheManager;->sLogFile:Lcom/google/android/music/log/LogFile;

    const-string v7, "CacheManagerImpl"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "File.listFiles(): "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    array-length v9, v3

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lcom/google/android/music/log/LogFile;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 766
    :cond_a
    move-object v1, v3

    .local v1, "arr$":[Ljava/io/File;
    array-length v5, v1

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_1
    if-ge v4, v5, :cond_1

    aget-object v2, v1, v4

    .line 767
    .local v2, "file":Ljava/io/File;
    invoke-virtual {p0, p1, v2, p3}, Lcom/google/android/music/download/cache/BaseCacheManager;->validateLocalFiles(Ljava/util/Set;Ljava/io/File;Z)V

    .line 766
    add-int/lit8 v4, v4, 0x1

    goto :goto_1
.end method
