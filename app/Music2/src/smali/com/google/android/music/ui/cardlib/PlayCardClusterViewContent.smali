.class public Lcom/google/android/music/ui/cardlib/PlayCardClusterViewContent;
.super Landroid/view/ViewGroup;
.source "PlayCardClusterViewContent.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private mCardsContextMenuDelegate:Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;

.field private mClickListenerOverride:Landroid/view/View$OnClickListener;

.field private mDocs:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/ui/cardlib/model/Document;",
            ">;"
        }
    .end annotation
.end field

.field private mMetadata:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 33
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewContent;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 38
    return-void
.end method


# virtual methods
.method public clearThumbnails()V
    .locals 4

    .prologue
    .line 96
    iget-object v3, p0, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewContent;->mMetadata:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;

    invoke-virtual {v3}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->getTileCount()I

    move-result v1

    .line 97
    .local v1, "count":I
    const/4 v2, 0x0

    .local v2, "tileIndex":I
    :goto_0
    if-ge v2, v1, :cond_0

    .line 98
    invoke-virtual {p0, v2}, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewContent;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    .line 99
    .local v0, "card":Lcom/google/android/music/ui/cardlib/layout/PlayCardView;
    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->clearThumbnail()V

    .line 97
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 101
    .end local v0    # "card":Lcom/google/android/music/ui/cardlib/layout/PlayCardView;
    :cond_0
    return-void
.end method

.method public createContent()V
    .locals 5

    .prologue
    .line 77
    iget-object v4, p0, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewContent;->mMetadata:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;

    invoke-virtual {v4}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->getTileCount()I

    move-result v1

    .line 78
    .local v1, "count":I
    const/4 v3, 0x0

    .local v3, "tileIndex":I
    :goto_0
    if-ge v3, v1, :cond_4

    .line 79
    invoke-virtual {p0, v3}, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewContent;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    .line 80
    .local v0, "card":Lcom/google/android/music/ui/cardlib/layout/PlayCardView;
    iget-object v4, p0, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewContent;->mDocs:Ljava/util/List;

    if-eqz v4, :cond_0

    .line 81
    iget-object v4, p0, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewContent;->mDocs:Ljava/util/List;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewContent;->mDocs:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v3, v4, :cond_1

    iget-object v4, p0, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewContent;->mDocs:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/music/ui/cardlib/model/Document;

    move-object v2, v4

    .line 83
    .local v2, "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    :goto_1
    invoke-virtual {v0, v2}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->setTag(Ljava/lang/Object;)V

    .line 84
    if-nez v2, :cond_2

    .line 85
    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->bindNoDocument()V

    .line 78
    .end local v2    # "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    :cond_0
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 81
    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    .line 87
    .restart local v2    # "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    :cond_2
    iget-object v4, p0, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewContent;->mCardsContextMenuDelegate:Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;

    invoke-virtual {v0, v2, v4}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->bind(Lcom/google/android/music/ui/cardlib/model/Document;Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;)V

    .line 88
    iget-object v4, p0, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewContent;->mClickListenerOverride:Landroid/view/View$OnClickListener;

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewContent;->mClickListenerOverride:Landroid/view/View$OnClickListener;

    :goto_3
    invoke-virtual {v0, v4}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_2

    :cond_3
    move-object v4, p0

    goto :goto_3

    .line 93
    .end local v0    # "card":Lcom/google/android/music/ui/cardlib/layout/PlayCardView;
    .end local v2    # "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    :cond_4
    return-void
.end method

.method public getMetadata()Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewContent;->mMetadata:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;

    return-object v0
.end method

.method public inflateContent(Lcom/google/android/music/ui/cardlib/layout/PlayCardHeap;)V
    .locals 5
    .param p1, "cardHeap"    # Lcom/google/android/music/ui/cardlib/layout/PlayCardHeap;

    .prologue
    .line 62
    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewContent;->getChildCount()I

    move-result v4

    if-lez v4, :cond_0

    .line 64
    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewContent;->removeAllViews()V

    .line 67
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewContent;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 68
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const/4 v2, 0x0

    .local v2, "tileIndex":I
    :goto_0
    iget-object v4, p0, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewContent;->mMetadata:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;

    invoke-virtual {v4}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->getTileCount()I

    move-result v4

    if-ge v2, v4, :cond_1

    .line 69
    iget-object v4, p0, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewContent;->mMetadata:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;

    invoke-virtual {v4, v2}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->getTileMetadata(I)Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$ClusterTileMetadata;

    move-result-object v3

    .line 70
    .local v3, "tileMetadata":Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$ClusterTileMetadata;
    invoke-virtual {v3}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$ClusterTileMetadata;->getCardMetadata()Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    move-result-object v4

    invoke-virtual {p1, v4, v1}, Lcom/google/android/music/ui/cardlib/layout/PlayCardHeap;->getCard(Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;Landroid/view/LayoutInflater;)Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    move-result-object v0

    .line 71
    .local v0, "card":Lcom/google/android/music/ui/cardlib/layout/PlayCardView;
    invoke-virtual {v3}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$ClusterTileMetadata;->getCardMetadata()Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->getThumbnailAspectRatio()F

    move-result v4

    invoke-virtual {v0, v4}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->setThumbnailAspectRatio(F)V

    .line 72
    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewContent;->addView(Landroid/view/View;)V

    .line 68
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 74
    .end local v0    # "card":Lcom/google/android/music/ui/cardlib/layout/PlayCardView;
    .end local v3    # "tileMetadata":Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$ClusterTileMetadata;
    :cond_1
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 160
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/music/ui/cardlib/model/Document;

    if-eqz v0, :cond_0

    .line 161
    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewContent;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-static {v1, v0, p1}, Lcom/google/android/music/ui/cardlib/model/DocumentClickHandler;->onDocumentClick(Landroid/content/Context;Lcom/google/android/music/ui/cardlib/model/Document;Landroid/view/View;)V

    .line 163
    :cond_0
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 17
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 135
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewContent;->getWidth()I

    move-result v1

    .line 136
    .local v1, "availableWidth":I
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewContent;->mMetadata:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;

    invoke-virtual {v15}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->getWidth()I

    move-result v9

    .line 137
    .local v9, "columns":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewContent;->getPaddingLeft()I

    move-result v15

    sub-int v15, v1, v15

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewContent;->getPaddingRight()I

    move-result v16

    sub-int v15, v15, v16

    div-int v8, v15, v9

    .line 138
    .local v8, "cellSize":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewContent;->getPaddingTop()I

    move-result v11

    .line 139
    .local v11, "paddingTop":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewContent;->getPaddingLeft()I

    move-result v10

    .line 141
    .local v10, "paddingLeft":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewContent;->getChildCount()I

    move-result v15

    if-lez v15, :cond_0

    .line 142
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewContent;->mMetadata:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;

    invoke-virtual {v15}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->getTileCount()I

    move-result v12

    .line 143
    .local v12, "tileCount":I
    const/4 v13, 0x0

    .local v13, "tileIndex":I
    :goto_0
    if-ge v13, v12, :cond_0

    .line 144
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewContent;->mMetadata:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;

    invoke-virtual {v15, v13}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->getTileMetadata(I)Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$ClusterTileMetadata;

    move-result-object v14

    .line 145
    .local v14, "tileMetadata":Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$ClusterTileMetadata;
    invoke-virtual {v14}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$ClusterTileMetadata;->getXStart()I

    move-result v6

    .line 146
    .local v6, "cardXStart":I
    invoke-virtual {v14}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$ClusterTileMetadata;->getYStart()I

    move-result v7

    .line 147
    .local v7, "cardYStart":I
    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewContent;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 148
    .local v2, "card":Landroid/view/View;
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    .line 149
    .local v3, "cardHeight":I
    mul-int v15, v8, v6

    add-int v4, v10, v15

    .line 150
    .local v4, "cardLeft":I
    mul-int v15, v3, v7

    add-int v5, v11, v15

    .line 152
    .local v5, "cardTop":I
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v15

    add-int/2addr v15, v4

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v16

    add-int v16, v16, v5

    move/from16 v0, v16

    invoke-virtual {v2, v4, v5, v15, v0}, Landroid/view/View;->layout(IIII)V

    .line 143
    add-int/lit8 v13, v13, 0x1

    goto :goto_0

    .line 156
    .end local v2    # "card":Landroid/view/View;
    .end local v3    # "cardHeight":I
    .end local v4    # "cardLeft":I
    .end local v5    # "cardTop":I
    .end local v6    # "cardXStart":I
    .end local v7    # "cardYStart":I
    .end local v12    # "tileCount":I
    .end local v13    # "tileIndex":I
    .end local v14    # "tileMetadata":Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$ClusterTileMetadata;
    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 15
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 106
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 110
    .local v0, "availableWidth":I
    iget-object v13, p0, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewContent;->mMetadata:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;

    invoke-virtual {v13}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->getWidth()I

    move-result v6

    .line 111
    .local v6, "columns":I
    iget-object v13, p0, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewContent;->mMetadata:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;

    invoke-virtual {v13}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->getHeight()I

    move-result v8

    .line 112
    .local v8, "rows":I
    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewContent;->getPaddingLeft()I

    move-result v13

    sub-int v13, v0, v13

    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewContent;->getPaddingRight()I

    move-result v14

    sub-int/2addr v13, v14

    div-int v5, v13, v6

    .line 113
    .local v5, "cellSize":I
    const/4 v9, 0x0

    .line 115
    .local v9, "targetHeight":I
    iget-object v13, p0, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewContent;->mMetadata:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;

    invoke-virtual {v13}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->getTileCount()I

    move-result v10

    .line 116
    .local v10, "tileCount":I
    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewContent;->getChildCount()I

    move-result v13

    if-lez v13, :cond_0

    .line 117
    const/4 v11, 0x0

    .local v11, "tileIndex":I
    :goto_0
    if-ge v11, v10, :cond_0

    .line 118
    iget-object v13, p0, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewContent;->mMetadata:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;

    invoke-virtual {v13, v11}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->getTileMetadata(I)Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$ClusterTileMetadata;

    move-result-object v12

    .line 119
    .local v12, "tileMetadata":Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$ClusterTileMetadata;
    invoke-virtual {v12}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$ClusterTileMetadata;->getCardMetadata()Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    move-result-object v13

    invoke-virtual {v13}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->getHSpan()I

    move-result v2

    .line 120
    .local v2, "cardHSpan":I
    invoke-virtual {v12}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$ClusterTileMetadata;->getCardMetadata()Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    move-result-object v13

    invoke-virtual {v13}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->getVSpan()I

    move-result v3

    .line 121
    .local v3, "cardVSpan":I
    invoke-virtual {p0, v11}, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewContent;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 122
    .local v1, "card":Landroid/view/View;
    mul-int v4, v5, v2

    .line 123
    .local v4, "cardWidth":I
    const/high16 v13, 0x40000000    # 2.0f

    invoke-static {v4, v13}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v13

    const/4 v14, 0x0

    invoke-virtual {v1, v13, v14}, Landroid/view/View;->measure(II)V

    .line 125
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    .line 126
    .local v7, "measuredHeight":I
    div-int v13, v7, v3

    invoke-static {v13, v9}, Ljava/lang/Math;->max(II)I

    move-result v9

    .line 117
    add-int/lit8 v11, v11, 0x1

    goto :goto_0

    .line 130
    .end local v1    # "card":Landroid/view/View;
    .end local v2    # "cardHSpan":I
    .end local v3    # "cardVSpan":I
    .end local v4    # "cardWidth":I
    .end local v7    # "measuredHeight":I
    .end local v11    # "tileIndex":I
    .end local v12    # "tileMetadata":Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$ClusterTileMetadata;
    :cond_0
    mul-int v13, v9, v8

    invoke-virtual {p0, v0, v13}, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewContent;->setMeasuredDimension(II)V

    .line 131
    return-void
.end method

.method public setMetadata(Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;Ljava/util/List;Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;Landroid/view/View$OnClickListener;)V
    .locals 0
    .param p1, "metadata"    # Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;
    .param p3, "cardsContextMenuDelegate"    # Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;
    .param p4, "clickListener"    # Landroid/view/View$OnClickListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/ui/cardlib/model/Document;",
            ">;",
            "Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;",
            "Landroid/view/View$OnClickListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 51
    .local p2, "clusterDocs":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/ui/cardlib/model/Document;>;"
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewContent;->mMetadata:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;

    .line 52
    iput-object p2, p0, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewContent;->mDocs:Ljava/util/List;

    .line 53
    iput-object p3, p0, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewContent;->mCardsContextMenuDelegate:Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;

    .line 54
    iput-object p4, p0, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewContent;->mClickListenerOverride:Landroid/view/View$OnClickListener;

    .line 55
    return-void
.end method
