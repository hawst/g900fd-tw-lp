.class Lcom/google/android/music/ui/ContainerHeaderView$4;
.super Ljava/lang/Object;
.source "ContainerHeaderView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/ContainerHeaderView;->setupArtistArtSlideShow(Landroid/database/Cursor;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/ContainerHeaderView;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/ContainerHeaderView;)V
    .locals 0

    .prologue
    .line 693
    iput-object p1, p0, Lcom/google/android/music/ui/ContainerHeaderView$4;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 697
    iget-object v1, p0, Lcom/google/android/music/ui/ContainerHeaderView$4;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/ContainerHeaderView;->mSlideShowHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/google/android/music/ui/ContainerHeaderView;->access$1800(Lcom/google/android/music/ui/ContainerHeaderView;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/ContainerHeaderView$4;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/ContainerHeaderView;->mSlideSwitchRunnable:Ljava/lang/Runnable;
    invoke-static {v2}, Lcom/google/android/music/ui/ContainerHeaderView;->access$1700(Lcom/google/android/music/ui/ContainerHeaderView;)Ljava/lang/Runnable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 698
    iget-object v1, p0, Lcom/google/android/music/ui/ContainerHeaderView$4;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/ContainerHeaderView;->mSlideShowHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/google/android/music/ui/ContainerHeaderView;->access$1800(Lcom/google/android/music/ui/ContainerHeaderView;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/ContainerHeaderView$4;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/ContainerHeaderView;->mSlideRefreshRunnable:Ljava/lang/Runnable;
    invoke-static {v2}, Lcom/google/android/music/ui/ContainerHeaderView;->access$1900(Lcom/google/android/music/ui/ContainerHeaderView;)Ljava/lang/Runnable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 700
    iget-object v1, p0, Lcom/google/android/music/ui/ContainerHeaderView$4;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/ContainerHeaderView;->mShowingFirstSlide:Z
    invoke-static {v1}, Lcom/google/android/music/ui/ContainerHeaderView;->access$2000(Lcom/google/android/music/ui/ContainerHeaderView;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/music/ui/ContainerHeaderView$4;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/ContainerHeaderView;->mArtistArtHelper2:Lcom/google/android/music/ui/ContainerHeaderView$ArtHelper;
    invoke-static {v1}, Lcom/google/android/music/ui/ContainerHeaderView;->access$2100(Lcom/google/android/music/ui/ContainerHeaderView;)Lcom/google/android/music/ui/ContainerHeaderView$ArtHelper;

    move-result-object v0

    .line 703
    .local v0, "nextHelper":Lcom/google/android/music/ui/ContainerHeaderView$ArtHelper;
    :goto_0
    # getter for: Lcom/google/android/music/ui/ContainerHeaderView;->LOGV:Z
    invoke-static {}, Lcom/google/android/music/ui/ContainerHeaderView;->access$2200()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 704
    const-string v1, "ContainerHeader"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Slide switch. Switching to: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/music/ui/ContainerHeaderView$ArtHelper;->getViewName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " is ready: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/music/ui/ContainerHeaderView$ArtHelper;->isViewReady()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 709
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/ui/ContainerHeaderView$4;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/ContainerHeaderView;->mViewFlipper:Landroid/widget/ViewFlipper;
    invoke-static {v1}, Lcom/google/android/music/ui/ContainerHeaderView;->access$2300(Lcom/google/android/music/ui/ContainerHeaderView;)Landroid/widget/ViewFlipper;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ViewFlipper;->showNext()V

    .line 710
    iget-object v2, p0, Lcom/google/android/music/ui/ContainerHeaderView$4;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    iget-object v1, p0, Lcom/google/android/music/ui/ContainerHeaderView$4;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/ContainerHeaderView;->mShowingFirstSlide:Z
    invoke-static {v1}, Lcom/google/android/music/ui/ContainerHeaderView;->access$2000(Lcom/google/android/music/ui/ContainerHeaderView;)Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v1, 0x1

    :goto_1
    # setter for: Lcom/google/android/music/ui/ContainerHeaderView;->mShowingFirstSlide:Z
    invoke-static {v2, v1}, Lcom/google/android/music/ui/ContainerHeaderView;->access$2002(Lcom/google/android/music/ui/ContainerHeaderView;Z)Z

    .line 714
    iget-object v1, p0, Lcom/google/android/music/ui/ContainerHeaderView$4;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/ContainerHeaderView;->mSlideShowHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/google/android/music/ui/ContainerHeaderView;->access$1800(Lcom/google/android/music/ui/ContainerHeaderView;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/ContainerHeaderView$4;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/ContainerHeaderView;->mSlideRefreshRunnable:Ljava/lang/Runnable;
    invoke-static {v2}, Lcom/google/android/music/ui/ContainerHeaderView;->access$1900(Lcom/google/android/music/ui/ContainerHeaderView;)Ljava/lang/Runnable;

    move-result-object v2

    const-wide/16 v4, 0x640

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 718
    iget-object v1, p0, Lcom/google/android/music/ui/ContainerHeaderView$4;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/ContainerHeaderView;->mSlideShowHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/google/android/music/ui/ContainerHeaderView;->access$1800(Lcom/google/android/music/ui/ContainerHeaderView;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/ContainerHeaderView$4;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/ContainerHeaderView;->mSlideSwitchRunnable:Ljava/lang/Runnable;
    invoke-static {v2}, Lcom/google/android/music/ui/ContainerHeaderView;->access$1700(Lcom/google/android/music/ui/ContainerHeaderView;)Ljava/lang/Runnable;

    move-result-object v2

    const-wide/16 v4, 0x1388

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 720
    return-void

    .line 700
    .end local v0    # "nextHelper":Lcom/google/android/music/ui/ContainerHeaderView$ArtHelper;
    :cond_1
    iget-object v1, p0, Lcom/google/android/music/ui/ContainerHeaderView$4;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/ContainerHeaderView;->mArtistArtHelper1:Lcom/google/android/music/ui/ContainerHeaderView$ArtHelper;
    invoke-static {v1}, Lcom/google/android/music/ui/ContainerHeaderView;->access$900(Lcom/google/android/music/ui/ContainerHeaderView;)Lcom/google/android/music/ui/ContainerHeaderView$ArtHelper;

    move-result-object v0

    goto :goto_0

    .line 710
    .restart local v0    # "nextHelper":Lcom/google/android/music/ui/ContainerHeaderView$ArtHelper;
    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method
