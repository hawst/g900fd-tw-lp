.class Lcom/google/android/music/art/ArtResolver$2;
.super Ljava/lang/Object;
.source "ArtResolver.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/art/ArtResolver;->getArt(Lcom/google/android/music/cloudclient/QuizGenreJson;Lcom/google/android/music/art/ArtType;Z)Lcom/google/android/music/art/ArtRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/art/ArtResolver;

.field final synthetic val$artUrl:Ljava/lang/String;

.field final synthetic val$request:Lcom/google/android/music/art/ArtRequest;


# direct methods
.method constructor <init>(Lcom/google/android/music/art/ArtResolver;Ljava/lang/String;Lcom/google/android/music/art/ArtRequest;)V
    .locals 0

    .prologue
    .line 459
    iput-object p1, p0, Lcom/google/android/music/art/ArtResolver$2;->this$0:Lcom/google/android/music/art/ArtResolver;

    iput-object p2, p0, Lcom/google/android/music/art/ArtResolver$2;->val$artUrl:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/music/art/ArtResolver$2;->val$request:Lcom/google/android/music/art/ArtRequest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 462
    iget-object v0, p0, Lcom/google/android/music/art/ArtResolver$2;->val$artUrl:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 463
    iget-object v0, p0, Lcom/google/android/music/art/ArtResolver$2;->this$0:Lcom/google/android/music/art/ArtResolver;

    iget-object v1, p0, Lcom/google/android/music/art/ArtResolver$2;->val$artUrl:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/music/art/ArtResolver$2;->val$request:Lcom/google/android/music/art/ArtRequest;

    # invokes: Lcom/google/android/music/art/ArtResolver;->getGenericArtForUrl(Ljava/lang/String;Lcom/google/android/music/art/ArtRequest;)Z
    invoke-static {v0, v1, v2}, Lcom/google/android/music/art/ArtResolver;->access$000(Lcom/google/android/music/art/ArtResolver;Ljava/lang/String;Lcom/google/android/music/art/ArtRequest;)Z

    .line 467
    :goto_0
    return-void

    .line 465
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/art/ArtResolver$2;->val$request:Lcom/google/android/music/art/ArtRequest;

    iget-object v1, p0, Lcom/google/android/music/art/ArtResolver$2;->this$0:Lcom/google/android/music/art/ArtResolver;

    # getter for: Lcom/google/android/music/art/ArtResolver;->mAppContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/google/android/music/art/ArtResolver;->access$200(Lcom/google/android/music/art/ArtResolver;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/music/art/ArtRequest;->notifyIfDone(Landroid/content/Context;)Z

    goto :goto_0
.end method
