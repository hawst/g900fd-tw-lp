.class public Lcom/google/android/music/ui/AddToLibraryButton;
.super Lcom/google/android/music/ui/BaseActionButton;
.source "AddToLibraryButton.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 20
    const v0, 0x7f0b0231

    const v1, 0x7f0200c7

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/android/music/ui/BaseActionButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 22
    return-void
.end method

.method public static addToLibrary(Landroid/content/Context;Lcom/google/android/music/medialist/MediaList;Z)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "medialist"    # Lcom/google/android/music/medialist/MediaList;
    .param p2, "showToast"    # Z

    .prologue
    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 30
    if-eqz p1, :cond_4

    .line 31
    instance-of v2, p1, Lcom/google/android/music/medialist/ExternalSongList;

    if-eqz v2, :cond_3

    .line 32
    check-cast p1, Lcom/google/android/music/medialist/ExternalSongList;

    .end local p1    # "medialist":Lcom/google/android/music/medialist/MediaList;
    invoke-virtual {p1, p0, v4}, Lcom/google/android/music/medialist/ExternalSongList;->addToStore(Landroid/content/Context;Z)[J

    move-result-object v1

    .line 33
    .local v1, "localIds":[J
    if-eqz v1, :cond_0

    array-length v2, v1

    if-nez v2, :cond_2

    .line 34
    :cond_0
    if-eqz p2, :cond_1

    .line 35
    const v2, 0x7f0b00d6

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 54
    .end local v1    # "localIds":[J
    :cond_1
    :goto_0
    return-void

    .line 40
    .restart local v1    # "localIds":[J
    :cond_2
    array-length v0, v1

    .line 41
    .local v0, "length":I
    if-eqz p2, :cond_1

    .line 42
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f120006

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v0, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 49
    .end local v0    # "length":I
    .end local v1    # "localIds":[J
    .restart local p1    # "medialist":Lcom/google/android/music/medialist/MediaList;
    :cond_3
    const-string v2, "ActionButton"

    const-string v3, "The MediaList was not ExternalSongList"

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 52
    :cond_4
    const-string v2, "ActionButton"

    const-string v3, "MediaList is null"

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method protected handleAction(Landroid/content/Context;Lcom/google/android/music/medialist/MediaList;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "medialist"    # Lcom/google/android/music/medialist/MediaList;

    .prologue
    .line 26
    const/4 v0, 0x1

    invoke-static {p1, p2, v0}, Lcom/google/android/music/ui/AddToLibraryButton;->addToLibrary(Landroid/content/Context;Lcom/google/android/music/medialist/MediaList;Z)V

    .line 27
    return-void
.end method
