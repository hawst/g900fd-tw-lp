.class public abstract Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;
.super Ljava/lang/Object;
.source "KeepOnManager.java"

# interfaces
.implements Lcom/google/android/music/utils/async/AsyncRunner;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/activitymanagement/KeepOnManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "UpdateKeepOnStatusTask"
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mDownloadedSongCount:I

.field private mHasRemote:Ljava/lang/Boolean;

.field private mIsAllLocal:Ljava/lang/Boolean;

.field private mIsKeptInDb:Ljava/lang/Boolean;

.field private mKeepOnSongCount:I

.field private mSavedLocalId:J

.field private final mSavedSongList:Lcom/google/android/music/medialist/SongList;


# direct methods
.method protected constructor <init>(Landroid/content/Context;Lcom/google/android/music/medialist/SongList;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "songList"    # Lcom/google/android/music/medialist/SongList;

    .prologue
    const/4 v1, -0x1

    const/4 v0, 0x0

    .line 575
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 568
    iput-object v0, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mIsKeptInDb:Ljava/lang/Boolean;

    .line 569
    iput-object v0, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mHasRemote:Ljava/lang/Boolean;

    .line 570
    iput-object v0, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mIsAllLocal:Ljava/lang/Boolean;

    .line 571
    iput v1, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mDownloadedSongCount:I

    .line 572
    iput v1, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mKeepOnSongCount:I

    .line 573
    const-wide/32 v0, -0x80000000

    iput-wide v0, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mSavedLocalId:J

    .line 576
    iput-object p1, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mContext:Landroid/content/Context;

    .line 577
    iput-object p2, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mSavedSongList:Lcom/google/android/music/medialist/SongList;

    .line 578
    return-void
.end method


# virtual methods
.method public backgroundTask()V
    .locals 6

    .prologue
    .line 588
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/ui/UIStateManager;->getStoreService()Lcom/google/android/music/store/IStoreService;

    move-result-object v0

    .line 589
    .local v0, "storeService":Lcom/google/android/music/store/IStoreService;
    if-eqz v0, :cond_1

    .line 590
    iget-object v1, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mSavedSongList:Lcom/google/android/music/medialist/SongList;

    instance-of v1, v1, Lcom/google/android/music/medialist/AlbumSongList;

    if-eqz v1, :cond_2

    .line 591
    iget-object v1, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mSavedSongList:Lcom/google/android/music/medialist/SongList;

    iget-object v2, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lcom/google/android/music/medialist/SongList;->getAlbumId(Landroid/content/Context;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mSavedLocalId:J

    .line 607
    :cond_0
    :goto_0
    iget-wide v2, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mSavedLocalId:J

    const-wide/32 v4, -0x80000000

    cmp-long v1, v2, v4

    if-nez v1, :cond_7

    .line 616
    :cond_1
    :goto_1
    return-void

    .line 592
    :cond_2
    iget-object v1, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mSavedSongList:Lcom/google/android/music/medialist/SongList;

    instance-of v1, v1, Lcom/google/android/music/medialist/PlaylistSongList;

    if-eqz v1, :cond_3

    .line 593
    iget-object v1, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mSavedSongList:Lcom/google/android/music/medialist/SongList;

    check-cast v1, Lcom/google/android/music/medialist/PlaylistSongList;

    invoke-virtual {v1}, Lcom/google/android/music/medialist/PlaylistSongList;->getPlaylistId()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mSavedLocalId:J

    goto :goto_0

    .line 595
    :cond_3
    iget-object v1, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mSavedSongList:Lcom/google/android/music/medialist/SongList;

    instance-of v1, v1, Lcom/google/android/music/medialist/AutoPlaylistSongList;

    if-eqz v1, :cond_4

    .line 596
    iget-object v1, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mSavedSongList:Lcom/google/android/music/medialist/SongList;

    check-cast v1, Lcom/google/android/music/medialist/AutoPlaylistSongList;

    invoke-virtual {v1}, Lcom/google/android/music/medialist/AutoPlaylistSongList;->getId()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mSavedLocalId:J

    goto :goto_0

    .line 598
    :cond_4
    iget-object v1, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mSavedSongList:Lcom/google/android/music/medialist/SongList;

    instance-of v1, v1, Lcom/google/android/music/medialist/NautilusAlbumSongList;

    if-eqz v1, :cond_5

    .line 599
    iget-object v1, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mSavedSongList:Lcom/google/android/music/medialist/SongList;

    iget-object v2, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lcom/google/android/music/medialist/SongList;->getAlbumId(Landroid/content/Context;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mSavedLocalId:J

    goto :goto_0

    .line 600
    :cond_5
    iget-object v1, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mSavedSongList:Lcom/google/android/music/medialist/SongList;

    instance-of v1, v1, Lcom/google/android/music/medialist/RadioStationSongList;

    if-eqz v1, :cond_6

    .line 601
    iget-object v1, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mSavedSongList:Lcom/google/android/music/medialist/SongList;

    check-cast v1, Lcom/google/android/music/medialist/RadioStationSongList;

    invoke-virtual {v1}, Lcom/google/android/music/medialist/RadioStationSongList;->getRadioStationId()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mSavedLocalId:J

    goto :goto_0

    .line 602
    :cond_6
    iget-object v1, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mSavedSongList:Lcom/google/android/music/medialist/SongList;

    instance-of v1, v1, Lcom/google/android/music/medialist/SharedWithMeSongList;

    if-eqz v1, :cond_0

    .line 603
    iget-object v1, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mSavedSongList:Lcom/google/android/music/medialist/SongList;

    check-cast v1, Lcom/google/android/music/medialist/SharedWithMeSongList;

    iget-object v2, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lcom/google/android/music/medialist/SharedWithMeSongList;->getPlaylistId(Landroid/content/Context;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mSavedLocalId:J

    goto :goto_0

    .line 609
    :cond_7
    iget-object v1, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mSavedSongList:Lcom/google/android/music/medialist/SongList;

    iget-object v2, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2, v0}, Lcom/google/android/music/medialist/SongList;->isSelectedForOfflineCaching(Landroid/content/Context;Lcom/google/android/music/store/IStoreService;)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mIsKeptInDb:Ljava/lang/Boolean;

    .line 611
    iget-object v1, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mSavedSongList:Lcom/google/android/music/medialist/SongList;

    iget-object v2, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lcom/google/android/music/medialist/SongList;->containsRemoteItems(Landroid/content/Context;)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mHasRemote:Ljava/lang/Boolean;

    .line 612
    iget-object v1, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mSavedSongList:Lcom/google/android/music/medialist/SongList;

    iget-object v2, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lcom/google/android/music/medialist/SongList;->isAllLocal(Landroid/content/Context;)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mIsAllLocal:Ljava/lang/Boolean;

    .line 613
    iget-object v1, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mSavedSongList:Lcom/google/android/music/medialist/SongList;

    iget-object v2, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lcom/google/android/music/medialist/SongList;->getDownloadedSongCount(Landroid/content/Context;)I

    move-result v1

    iput v1, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mDownloadedSongCount:I

    .line 614
    iget-object v1, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mSavedSongList:Lcom/google/android/music/medialist/SongList;

    iget-object v2, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lcom/google/android/music/medialist/SongList;->getKeepOnSongCount(Landroid/content/Context;)I

    move-result v1

    iput v1, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mKeepOnSongCount:I

    goto/16 :goto_1
.end method

.method protected abstract getAnimatorUpdateListener()Lcom/google/android/music/animator/AnimatorUpdateListener;
.end method

.method protected abstract getCurrentSongList()Lcom/google/android/music/medialist/SongList;
.end method

.method protected abstract getKeepOnState()Lcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;
.end method

.method public taskCompleted()V
    .locals 13

    .prologue
    const-wide/16 v11, -0x1

    .line 620
    iget-object v2, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mSavedSongList:Lcom/google/android/music/medialist/SongList;

    invoke-virtual {p0}, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->getCurrentSongList()Lcom/google/android/music/medialist/SongList;

    move-result-object v3

    if-ne v2, v3, :cond_0

    iget-wide v3, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mSavedLocalId:J

    const-wide/32 v5, -0x80000000

    cmp-long v2, v3, v5

    if-nez v2, :cond_1

    .line 653
    :cond_0
    :goto_0
    return-void

    .line 624
    :cond_1
    invoke-static {}, Lcom/google/android/music/activitymanagement/KeepOnManager;->getInstance()Lcom/google/android/music/activitymanagement/KeepOnManager;

    move-result-object v1

    .line 625
    .local v1, "keepOnManager":Lcom/google/android/music/activitymanagement/KeepOnManager;
    iget-object v2, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mIsKeptInDb:Ljava/lang/Boolean;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mIsAllLocal:Ljava/lang/Boolean;

    if-eqz v2, :cond_0

    .line 626
    iget-object v2, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mSavedSongList:Lcom/google/android/music/medialist/SongList;

    instance-of v2, v2, Lcom/google/android/music/medialist/AlbumSongList;

    if-eqz v2, :cond_2

    .line 627
    iget-wide v2, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mSavedLocalId:J

    iget-object v4, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mHasRemote:Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    iget-object v5, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mIsKeptInDb:Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    iget-object v6, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mIsAllLocal:Ljava/lang/Boolean;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    iget v7, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mDownloadedSongCount:I

    iget v8, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mKeepOnSongCount:I

    invoke-virtual {p0}, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->getKeepOnState()Lcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;

    move-result-object v9

    invoke-virtual {p0}, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->getAnimatorUpdateListener()Lcom/google/android/music/animator/AnimatorUpdateListener;

    move-result-object v10

    invoke-virtual/range {v1 .. v12}, Lcom/google/android/music/activitymanagement/KeepOnManager;->initAlbumKeepOnViewStatus(JZZZIILcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;Lcom/google/android/music/animator/AnimatorUpdateListener;J)V

    goto :goto_0

    .line 630
    :cond_2
    iget-object v2, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mSavedSongList:Lcom/google/android/music/medialist/SongList;

    instance-of v2, v2, Lcom/google/android/music/medialist/PlaylistSongList;

    if-eqz v2, :cond_3

    .line 631
    iget-wide v2, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mSavedLocalId:J

    iget-object v4, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mHasRemote:Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    iget-object v5, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mIsKeptInDb:Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    iget-object v6, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mIsAllLocal:Ljava/lang/Boolean;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    iget v7, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mDownloadedSongCount:I

    iget v8, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mKeepOnSongCount:I

    invoke-virtual {p0}, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->getKeepOnState()Lcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;

    move-result-object v9

    invoke-virtual {p0}, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->getAnimatorUpdateListener()Lcom/google/android/music/animator/AnimatorUpdateListener;

    move-result-object v10

    invoke-virtual/range {v1 .. v12}, Lcom/google/android/music/activitymanagement/KeepOnManager;->initPlaylistKeepOnViewStatus(JZZZIILcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;Lcom/google/android/music/animator/AnimatorUpdateListener;J)V

    goto :goto_0

    .line 634
    :cond_3
    iget-object v2, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mSavedSongList:Lcom/google/android/music/medialist/SongList;

    instance-of v2, v2, Lcom/google/android/music/medialist/AutoPlaylistSongList;

    if-eqz v2, :cond_4

    .line 635
    iget-wide v2, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mSavedLocalId:J

    iget-object v4, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mHasRemote:Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    iget-object v5, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mIsKeptInDb:Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    iget-object v6, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mIsAllLocal:Ljava/lang/Boolean;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    iget v7, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mDownloadedSongCount:I

    iget v8, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mKeepOnSongCount:I

    invoke-virtual {p0}, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->getKeepOnState()Lcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;

    move-result-object v9

    invoke-virtual {p0}, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->getAnimatorUpdateListener()Lcom/google/android/music/animator/AnimatorUpdateListener;

    move-result-object v10

    invoke-virtual/range {v1 .. v12}, Lcom/google/android/music/activitymanagement/KeepOnManager;->initAutoPlaylistKeepOnViewStatus(JZZZIILcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;Lcom/google/android/music/animator/AnimatorUpdateListener;J)V

    goto/16 :goto_0

    .line 639
    :cond_4
    iget-object v2, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mSavedSongList:Lcom/google/android/music/medialist/SongList;

    instance-of v2, v2, Lcom/google/android/music/medialist/NautilusAlbumSongList;

    if-eqz v2, :cond_5

    .line 640
    iget-wide v2, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mSavedLocalId:J

    iget-object v4, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mHasRemote:Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    iget-object v5, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mIsKeptInDb:Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    iget-object v6, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mIsAllLocal:Ljava/lang/Boolean;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    iget v7, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mDownloadedSongCount:I

    iget v8, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mKeepOnSongCount:I

    invoke-virtual {p0}, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->getKeepOnState()Lcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;

    move-result-object v9

    invoke-virtual {p0}, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->getAnimatorUpdateListener()Lcom/google/android/music/animator/AnimatorUpdateListener;

    move-result-object v10

    invoke-virtual/range {v1 .. v12}, Lcom/google/android/music/activitymanagement/KeepOnManager;->initAlbumKeepOnViewStatus(JZZZIILcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;Lcom/google/android/music/animator/AnimatorUpdateListener;J)V

    goto/16 :goto_0

    .line 643
    :cond_5
    iget-object v2, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mSavedSongList:Lcom/google/android/music/medialist/SongList;

    instance-of v2, v2, Lcom/google/android/music/medialist/RadioStationSongList;

    if-eqz v2, :cond_6

    .line 644
    iget-wide v2, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mSavedLocalId:J

    iget-object v4, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mHasRemote:Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    iget-object v5, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mIsKeptInDb:Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    iget-object v6, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mIsAllLocal:Ljava/lang/Boolean;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    iget v7, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mDownloadedSongCount:I

    iget v8, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mKeepOnSongCount:I

    invoke-virtual {p0}, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->getKeepOnState()Lcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;

    move-result-object v9

    invoke-virtual {p0}, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->getAnimatorUpdateListener()Lcom/google/android/music/animator/AnimatorUpdateListener;

    move-result-object v10

    invoke-virtual/range {v1 .. v12}, Lcom/google/android/music/activitymanagement/KeepOnManager;->initRadioKeepOnViewStatus(JZZZIILcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;Lcom/google/android/music/animator/AnimatorUpdateListener;J)V

    goto/16 :goto_0

    .line 647
    :cond_6
    iget-object v2, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mSavedSongList:Lcom/google/android/music/medialist/SongList;

    instance-of v2, v2, Lcom/google/android/music/medialist/SharedWithMeSongList;

    if-eqz v2, :cond_0

    .line 648
    iget-wide v2, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mSavedLocalId:J

    iget-object v4, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mHasRemote:Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    iget-object v5, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mIsKeptInDb:Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    iget-object v6, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mIsAllLocal:Ljava/lang/Boolean;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    iget v7, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mDownloadedSongCount:I

    iget v8, p0, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->mKeepOnSongCount:I

    invoke-virtual {p0}, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->getKeepOnState()Lcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;

    move-result-object v9

    invoke-virtual {p0}, Lcom/google/android/music/activitymanagement/KeepOnManager$UpdateKeepOnStatusTask;->getAnimatorUpdateListener()Lcom/google/android/music/animator/AnimatorUpdateListener;

    move-result-object v10

    invoke-virtual/range {v1 .. v12}, Lcom/google/android/music/activitymanagement/KeepOnManager;->initPlaylistKeepOnViewStatus(JZZZIILcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;Lcom/google/android/music/animator/AnimatorUpdateListener;J)V

    goto/16 :goto_0
.end method
