.class public Lcom/google/android/music/store/MediaStoreImporter;
.super Ljava/lang/Object;
.source "MediaStoreImporter.java"


# static fields
.field private static final AUDIO_COLUMNS:[Ljava/lang/String;

.field private static final AUDIO_COLUMNS_WITH_ALBUM_ARTIST:[Ljava/lang/String;

.field private static final AUDIO_ID_COLUMNS:[Ljava/lang/String;

.field private static final COUNT_COLUMNS:[Ljava/lang/String;

.field private static final GENRE_COLUMNS:[Ljava/lang/String;

.field private static final GENRE_MEMBER_COLUMNS:[Ljava/lang/String;

.field private static final LOGV:Z

.field private static final MEDIA_STORE_FS_ID_COLUMNS:[Ljava/lang/String;

.field private static MEDIA_STORE_FS_ID_URI:Landroid/net/Uri;

.field private static final MEDIA_STORE_PLAYLIST_MEMBER_COLUMNS:[Ljava/lang/String;

.field private static final MUSIC_STORE_PLAYLIST_ITEM_COLUMNS:[Ljava/lang/String;

.field private static final PLAYLIST_COLUMNS:[Ljava/lang/String;

.field private static final PLAYLIST_NAME_CHECK_COLUMNS:[Ljava/lang/String;

.field private static final UPDATE_POST_SYNC_MESSAGE_TYPE:I

.field private static final sHasAlbumArtist:Ljava/util/concurrent/atomic/AtomicBoolean;


# instance fields
.field private mAddedMusicCount:I

.field private mAudioFilesInMediaStoreCount:I

.field private mContext:Landroid/content/Context;

.field private mCreatedPlaylistCount:I

.field private mDeletedMusicCount:I

.field private mDeletedPlaylistCount:I

.field private mDeletedPlaylistItemCount:I

.field private mImportChangesSinceDate:J

.field private mInsertedPlaylistItemCount:I

.field private mResolver:Landroid/content/ContentResolver;

.field private mStore:Lcom/google/android/music/store/Store;

.field private mUpdatedMusicCount:I

.field private mUpdatedPlaylistCount:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 41
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->STORE_IMPORTER:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/store/MediaStoreImporter;->LOGV:Z

    .line 43
    const-string v0, "content://media/external/fs_id"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/store/MediaStoreImporter;->MEDIA_STORE_FS_ID_URI:Landroid/net/Uri;

    .line 45
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "fsid"

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/music/store/MediaStoreImporter;->MEDIA_STORE_FS_ID_COLUMNS:[Ljava/lang/String;

    .line 68
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "count(*)"

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/music/store/MediaStoreImporter;->COUNT_COLUMNS:[Ljava/lang/String;

    .line 70
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/music/store/MediaStoreImporter;->AUDIO_ID_COLUMNS:[Ljava/lang/String;

    .line 72
    const/16 v0, 0xc

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v4

    const-string v1, "album"

    aput-object v1, v0, v3

    const-string v1, "artist"

    aput-object v1, v0, v5

    const-string v1, "composer"

    aput-object v1, v0, v6

    const-string v1, "date_added"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "duration"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "mime_type"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "_size"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "title"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "track"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "year"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "album_id"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/store/MediaStoreImporter;->AUDIO_COLUMNS:[Ljava/lang/String;

    .line 89
    const/16 v0, 0xd

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v4

    const-string v1, "album"

    aput-object v1, v0, v3

    const-string v1, "artist"

    aput-object v1, v0, v5

    const-string v1, "composer"

    aput-object v1, v0, v6

    const-string v1, "date_added"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "duration"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "mime_type"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "_size"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "title"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "track"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "year"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "album_id"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "album_artist"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/store/MediaStoreImporter;->AUDIO_COLUMNS_WITH_ALBUM_ARTIST:[Ljava/lang/String;

    .line 120
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v4

    const-string v1, "name"

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/music/store/MediaStoreImporter;->GENRE_COLUMNS:[Ljava/lang/String;

    .line 126
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/music/store/MediaStoreImporter;->GENRE_MEMBER_COLUMNS:[Ljava/lang/String;

    .line 129
    new-array v0, v7, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v4

    const-string v1, "name"

    aput-object v1, v0, v3

    const-string v1, "date_added"

    aput-object v1, v0, v5

    const-string v1, "date_modified"

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/android/music/store/MediaStoreImporter;->PLAYLIST_COLUMNS:[Ljava/lang/String;

    .line 140
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "audio_id"

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/music/store/MediaStoreImporter;->MEDIA_STORE_PLAYLIST_MEMBER_COLUMNS:[Ljava/lang/String;

    .line 145
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "LISTITEMS.Id"

    aput-object v1, v0, v4

    const-string v1, "MUSIC.SourceId"

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/music/store/MediaStoreImporter;->MUSIC_STORE_PLAYLIST_ITEM_COLUMNS:[Ljava/lang/String;

    .line 152
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "Id"

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/music/store/MediaStoreImporter;->PLAYLIST_NAME_CHECK_COLUMNS:[Ljava/lang/String;

    .line 160
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    sput-object v0, Lcom/google/android/music/store/MediaStoreImporter;->sHasAlbumArtist:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 162
    sget-object v0, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    invoke-static {v0}, Lcom/google/android/music/utils/async/AsyncWorkers;->getUniqueMessageType(Lcom/google/android/music/utils/LoggableHandler;)I

    move-result v0

    sput v0, Lcom/google/android/music/store/MediaStoreImporter;->UPDATE_POST_SYNC_MESSAGE_TYPE:I

    return-void
.end method

.method constructor <init>()V
    .locals 2

    .prologue
    .line 185
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 171
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/music/store/MediaStoreImporter;->mImportChangesSinceDate:J

    .line 187
    return-void
.end method

.method private collectMediaStoreAlbumArtPresence(Ljava/util/HashMap;Landroid/database/Cursor;I)V
    .locals 12
    .param p2, "mediaStoreCursor"    # Landroid/database/Cursor;
    .param p3, "howMany"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Boolean;",
            ">;",
            "Landroid/database/Cursor;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 732
    .local p1, "albumArtPresence":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Ljava/lang/Boolean;>;"
    if-nez p2, :cond_1

    .line 775
    :cond_0
    :goto_0
    return-void

    .line 736
    :cond_1
    invoke-interface {p2}, Landroid/database/Cursor;->getPosition()I

    move-result v6

    .line 737
    .local v6, "savedPosition":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    if-ge v3, p3, :cond_6

    .line 738
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v8

    if-eqz v8, :cond_6

    .line 739
    const/16 v8, 0xb

    const-wide/16 v10, 0x0

    invoke-static {p2, v8, v10, v11}, Lcom/google/android/music/utils/DbUtils;->getNullableLong(Landroid/database/Cursor;IJ)J

    move-result-wide v4

    .line 741
    .local v4, "mediaStoreAlbumId":J
    const-wide/16 v8, 0x0

    cmp-long v8, v4, v8

    if-eqz v8, :cond_3

    .line 742
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {p1, v8}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_3

    .line 743
    const/4 v2, 0x0

    .line 744
    .local v2, "hasAlbumArt":Z
    const/4 v1, 0x0

    .line 746
    .local v1, "fd":Landroid/os/ParcelFileDescriptor;
    :try_start_0
    iget-object v8, p0, Lcom/google/android/music/store/MediaStoreImporter;->mResolver:Landroid/content/ContentResolver;

    invoke-static {v4, v5}, Lcom/google/android/music/store/MusicContent$AlbumArt;->getMediaStoreAlbumArt(J)Landroid/net/Uri;

    move-result-object v9

    const-string v10, "r"

    invoke-virtual {v8, v9, v10}, Landroid/content/ContentResolver;->openFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 749
    if-eqz v1, :cond_4

    const/4 v2, 0x1

    .line 754
    :goto_2
    if-eqz v1, :cond_2

    .line 755
    :try_start_1
    invoke-virtual {v1}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 761
    :cond_2
    :goto_3
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    invoke-virtual {p1, v8, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 737
    .end local v1    # "fd":Landroid/os/ParcelFileDescriptor;
    .end local v2    # "hasAlbumArt":Z
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 749
    .restart local v1    # "fd":Landroid/os/ParcelFileDescriptor;
    .restart local v2    # "hasAlbumArt":Z
    :cond_4
    const/4 v2, 0x0

    goto :goto_2

    .line 757
    :catch_0
    move-exception v0

    .line 758
    .local v0, "e":Ljava/lang/Exception;
    const-string v8, "MediaStoreImporter"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_3

    .line 750
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v8

    .line 754
    if-eqz v1, :cond_2

    .line 755
    :try_start_2
    invoke-virtual {v1}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_3

    .line 757
    :catch_2
    move-exception v0

    .line 758
    .restart local v0    # "e":Ljava/lang/Exception;
    const-string v8, "MediaStoreImporter"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_3

    .line 753
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v8

    .line 754
    if-eqz v1, :cond_5

    .line 755
    :try_start_3
    invoke-virtual {v1}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    .line 759
    :cond_5
    :goto_4
    throw v8

    .line 757
    :catch_3
    move-exception v0

    .line 758
    .restart local v0    # "e":Ljava/lang/Exception;
    const-string v9, "MediaStoreImporter"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_4

    .line 770
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v1    # "fd":Landroid/os/ParcelFileDescriptor;
    .end local v2    # "hasAlbumArt":Z
    .end local v4    # "mediaStoreAlbumId":J
    :cond_6
    invoke-interface {p2, v6}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v7

    .line 771
    .local v7, "success":Z
    if-nez v7, :cond_0

    const/4 v8, -0x1

    if-eq v6, v8, :cond_0

    .line 772
    const-string v8, "MediaStoreImporter"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Failed to restore saved position "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ". Current position: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-interface {p2}, Landroid/database/Cursor;->getPosition()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    new-instance v10, Ljava/lang/Exception;

    invoke-direct {v10}, Ljava/lang/Exception;-><init>()V

    invoke-static {v8, v9, v10}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0
.end method

.method private finishAudioBlockImport(Landroid/database/Cursor;Ljava/util/HashMap;Ljava/util/HashSet;Lcom/google/android/music/store/TagNormalizer;Z)Z
    .locals 3
    .param p1, "mediaStoreCursor"    # Landroid/database/Cursor;
    .param p4, "normalizer"    # Lcom/google/android/music/store/TagNormalizer;
    .param p5, "updateRecentItems"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/google/android/music/store/TagNormalizer;",
            "Z)Z"
        }
    .end annotation

    .prologue
    .line 691
    .local p2, "albumArtPresence":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Ljava/lang/Boolean;>;"
    .local p3, "importedAudioIds":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Long;>;"
    if-eqz p5, :cond_0

    .line 692
    :try_start_0
    iget-object v1, p0, Lcom/google/android/music/store/MediaStoreImporter;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/music/store/RecentItemsManager;->countRecentItems(Landroid/content/Context;)I

    move-result v1

    const/16 v2, 0x32

    if-ge v1, v2, :cond_1

    .line 694
    iget-object v1, p0, Lcom/google/android/music/store/MediaStoreImporter;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/music/store/RecentItemsManager;->updateRecentItems(Landroid/content/Context;)V

    .line 700
    :cond_0
    :goto_0
    invoke-direct {p0, p4, p3}, Lcom/google/android/music/store/MediaStoreImporter;->importMediaStoreGenre(Lcom/google/android/music/store/TagNormalizer;Ljava/util/HashSet;)V

    .line 701
    invoke-virtual {p3}, Ljava/util/HashSet;->clear()V

    .line 703
    const/16 v1, 0x200

    invoke-direct {p0, p2, p1, v1}, Lcom/google/android/music/store/MediaStoreImporter;->collectMediaStoreAlbumArtPresence(Ljava/util/HashMap;Landroid/database/Cursor;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 709
    :goto_1
    return p5

    .line 696
    :cond_1
    const/4 p5, 0x0

    goto :goto_0

    .line 706
    :catch_0
    move-exception v0

    .line 707
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "MediaStoreImporter"

    const-string v2, "Failure in finishAudioBlockImport"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method private getAllAudioCount()I
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 847
    const/4 v6, 0x0

    .line 848
    .local v6, "count":I
    iget-object v0, p0, Lcom/google/android/music/store/MediaStoreImporter;->mContext:Landroid/content/Context;

    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/google/android/music/store/MediaStoreImporter;->COUNT_COLUMNS:[Ljava/lang/String;

    const-string v3, "(is_music=1 OR is_podcast=1) AND title IS NOT NULL"

    move-object v5, v4

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 853
    .local v7, "cursor":Landroid/database/Cursor;
    if-eqz v7, :cond_0

    :try_start_0
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 854
    const/4 v0, 0x0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v6

    .line 857
    :cond_0
    invoke-static {v7}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 860
    return v6

    .line 857
    :catchall_0
    move-exception v0

    invoke-static {v7}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method private getLocalPlaylistsFromMusicStore(Ljava/util/Map;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1372
    .local p1, "ids":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Long;Ljava/lang/Long;>;"
    const/4 v8, 0x0

    .line 1373
    .local v8, "c":Landroid/database/Cursor;
    iget-object v1, p0, Lcom/google/android/music/store/MediaStoreImporter;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v1}, Lcom/google/android/music/store/Store;->beginRead()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1375
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    const-string v1, "LISTS"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "Id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "MediaStoreId"

    aput-object v4, v2, v3

    const-string v3, "MediaStoreId NOT NULL"

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 1378
    if-eqz v8, :cond_0

    .line 1379
    :goto_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1380
    const/4 v1, 0x1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {p1, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1384
    :catchall_0
    move-exception v1

    invoke-static {v8}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 1385
    iget-object v2, p0, Lcom/google/android/music/store/MediaStoreImporter;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v2, v0}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    throw v1

    .line 1384
    :cond_0
    invoke-static {v8}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 1385
    iget-object v1, p0, Lcom/google/android/music/store/MediaStoreImporter;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v1, v0}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1387
    return-void
.end method

.method private getMediaDbVersion()Ljava/lang/String;
    .locals 4
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 371
    const/4 v1, 0x0

    .line 372
    .local v1, "version":Ljava/lang/String;
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xc

    if-lt v2, v3, :cond_0

    .line 373
    iget-object v2, p0, Lcom/google/android/music/store/MediaStoreImporter;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/provider/MediaStore;->getVersion(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 374
    .local v0, "mediaStoreVersion":Ljava/lang/String;
    if-nez v0, :cond_2

    .line 375
    const-string v2, "MediaStoreImporter"

    const-string v3, "MediaStore.getVersion() returned null. Using build number"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 380
    .end local v0    # "mediaStoreVersion":Ljava/lang/String;
    :cond_0
    :goto_0
    if-nez v1, :cond_1

    .line 384
    sget-object v1, Landroid/os/Build$VERSION;->INCREMENTAL:Ljava/lang/String;

    .line 386
    :cond_1
    return-object v1

    .line 377
    .restart local v0    # "mediaStoreVersion":Ljava/lang/String;
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "MediaStore."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private getMediaStoreAudioCursor(Z)Landroid/database/Cursor;
    .locals 14
    .param p1, "getAlbumArtist"    # Z

    .prologue
    const-wide/16 v12, 0x0

    .line 447
    const/4 v4, 0x0

    .line 448
    .local v4, "selectionParams":[Ljava/lang/String;
    iget-wide v0, p0, Lcom/google/android/music/store/MediaStoreImporter;->mImportChangesSinceDate:J

    cmp-long v0, v0, v12

    if-lez v0, :cond_1

    .line 449
    iget-wide v0, p0, Lcom/google/android/music/store/MediaStoreImporter;->mImportChangesSinceDate:J

    const-wide/16 v10, 0x3e8

    div-long/2addr v0, v10

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v9

    .line 450
    .local v9, "time":Ljava/lang/String;
    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    .end local v4    # "selectionParams":[Ljava/lang/String;
    const/4 v0, 0x0

    aput-object v9, v4, v0

    const/4 v0, 0x1

    aput-object v9, v4, v0

    .line 451
    .restart local v4    # "selectionParams":[Ljava/lang/String;
    const-string v3, "((is_music=1 OR is_podcast=1) AND title IS NOT NULL)  AND (date_added >=?  OR date_modified >=?  )"

    .line 461
    .end local v9    # "time":Ljava/lang/String;
    .local v3, "audioSelection":Ljava/lang/String;
    :goto_0
    const/4 v6, 0x0

    .line 462
    .local v6, "count":I
    const/4 v7, 0x0

    .line 464
    .local v7, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/store/MediaStoreImporter;->mContext:Landroid/content/Context;

    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    if-eqz p1, :cond_2

    sget-object v2, Lcom/google/android/music/store/MediaStoreImporter;->AUDIO_COLUMNS_WITH_ALBUM_ARTIST:[Ljava/lang/String;

    :goto_1
    const-string v5, "date_added"

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 471
    if-nez v7, :cond_3

    .line 472
    const-string v0, "MediaStoreImporter"

    const-string v1, "Failed to get the cursor"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 493
    :cond_0
    :goto_2
    return-object v7

    .line 458
    .end local v3    # "audioSelection":Ljava/lang/String;
    .end local v6    # "count":I
    .end local v7    # "cursor":Landroid/database/Cursor;
    :cond_1
    const-string v3, "(is_music=1 OR is_podcast=1) AND title IS NOT NULL"

    .restart local v3    # "audioSelection":Ljava/lang/String;
    goto :goto_0

    .line 464
    .restart local v6    # "count":I
    .restart local v7    # "cursor":Landroid/database/Cursor;
    :cond_2
    :try_start_1
    sget-object v2, Lcom/google/android/music/store/MediaStoreImporter;->AUDIO_COLUMNS:[Ljava/lang/String;

    goto :goto_1

    .line 476
    :cond_3
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v6

    .line 477
    sget-boolean v0, Lcom/google/android/music/store/MediaStoreImporter;->LOGV:Z

    if-eqz v0, :cond_0

    .line 478
    iget-wide v0, p0, Lcom/google/android/music/store/MediaStoreImporter;->mImportChangesSinceDate:J

    cmp-long v0, v0, v12

    if-lez v0, :cond_5

    .line 479
    const-string v0, "MediaStoreImporter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Number of new/modified songs in MediaStore: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    .line 485
    :catch_0
    move-exception v8

    .line 486
    .local v8, "e":Ljava/lang/RuntimeException;
    if-nez p1, :cond_4

    .line 487
    const-string v0, "MediaStoreImporter"

    const-string v1, "Exception while executing media store query"

    invoke-static {v0, v1, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 489
    :cond_4
    invoke-static {v7}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 490
    const/4 v7, 0x0

    goto :goto_2

    .line 481
    .end local v8    # "e":Ljava/lang/RuntimeException;
    :cond_5
    :try_start_2
    const-string v0, "MediaStoreImporter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Number of songs in MediaStore: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_2
.end method

.method private static getMediaStoreFsId(Landroid/content/Context;)J
    .locals 10
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    .line 422
    const-wide/16 v8, 0x0

    .line 423
    .local v8, "fsId":J
    sget-object v1, Lcom/google/android/music/store/MediaStoreImporter;->MEDIA_STORE_FS_ID_URI:Landroid/net/Uri;

    sget-object v2, Lcom/google/android/music/store/MediaStoreImporter;->MEDIA_STORE_FS_ID_COLUMNS:[Ljava/lang/String;

    move-object v0, p0

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 426
    .local v6, "c":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 427
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v8

    .line 430
    :cond_0
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 432
    return-wide v8

    .line 430
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method private haveNonMediaStorePlaylist(Ljava/lang/String;)Z
    .locals 12
    .param p1, "playlistName"    # Ljava/lang/String;

    .prologue
    const/4 v10, 0x1

    const/4 v11, 0x0

    .line 1119
    const/4 v9, 0x0

    .line 1120
    .local v9, "c":Landroid/database/Cursor;
    iget-object v1, p0, Lcom/google/android/music/store/MediaStoreImporter;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v1}, Lcom/google/android/music/store/Store;->beginRead()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1122
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    const-string v1, "LISTS"

    sget-object v2, Lcom/google/android/music/store/MediaStoreImporter;->PLAYLIST_NAME_CHECK_COLUMNS:[Ljava/lang/String;

    const-string v3, "Name=? AND MediaStoreId IS NULL"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const-string v8, "1"

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 1127
    if-eqz v9, :cond_0

    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_0

    .line 1133
    invoke-static {v9}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 1134
    iget-object v1, p0, Lcom/google/android/music/store/MediaStoreImporter;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v1, v0}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    move v1, v10

    :goto_0
    return v1

    .line 1133
    :cond_0
    invoke-static {v9}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 1134
    iget-object v1, p0, Lcom/google/android/music/store/MediaStoreImporter;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v1, v0}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    move v1, v11

    goto :goto_0

    .line 1133
    :catchall_0
    move-exception v1

    invoke-static {v9}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 1134
    iget-object v2, p0, Lcom/google/android/music/store/MediaStoreImporter;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v2, v0}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    throw v1
.end method

.method private importAudioFiles(Landroid/database/Cursor;)V
    .locals 30
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 498
    const/16 v24, 0x0

    .line 499
    .local v24, "success":Z
    const/16 v18, 0x0

    .line 500
    .local v18, "musicInsert":Landroid/database/sqlite/SQLiteStatement;
    const/16 v19, 0x0

    .line 501
    .local v19, "musicUpdate":Landroid/database/sqlite/SQLiteStatement;
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 502
    .local v6, "albumArtPresence":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Ljava/lang/Boolean;>;"
    const/16 v4, 0x200

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v6, v1, v4}, Lcom/google/android/music/store/MediaStoreImporter;->collectMediaStoreAlbumArtPresence(Ljava/util/HashMap;Landroid/database/Cursor;I)V

    .line 503
    const/16 v20, 0x0

    .line 504
    .local v20, "openTransaction":Z
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/store/MediaStoreImporter;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v4}, Lcom/google/android/music/store/Store;->beginWriteTxn()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v11

    .line 506
    .local v11, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/16 v20, 0x1

    .line 507
    :try_start_0
    new-instance v7, Ljava/util/HashSet;

    invoke-direct {v7}, Ljava/util/HashSet;-><init>()V

    .line 508
    .local v7, "importedAudioIds":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Long;>;"
    invoke-static {v11}, Lcom/google/android/music/store/MusicFile;->compileMusicInsertStatement(Landroid/database/sqlite/SQLiteDatabase;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v18

    .line 509
    invoke-static {v11}, Lcom/google/android/music/store/MusicFile;->compileFullUpdateStatement(Landroid/database/sqlite/SQLiteDatabase;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v19

    .line 511
    new-instance v8, Lcom/google/android/music/store/TagNormalizer;

    invoke-direct {v8}, Lcom/google/android/music/store/TagNormalizer;-><init>()V

    .line 512
    .local v8, "normalizer":Lcom/google/android/music/store/TagNormalizer;
    new-instance v14, Lcom/google/android/music/store/MusicFile;

    invoke-direct {v14}, Lcom/google/android/music/store/MusicFile;-><init>()V

    .line 513
    .local v14, "file":Lcom/google/android/music/store/MusicFile;
    invoke-virtual {v14, v8}, Lcom/google/android/music/store/MusicFile;->setTagNormalizer(Lcom/google/android/music/store/TagNormalizer;)V

    .line 515
    const/4 v9, 0x1

    .line 516
    .local v9, "updateRecentItems":Z
    const/16 v26, 0x0

    .line 519
    .local v26, "uncommittedChanges":Z
    :cond_0
    :goto_0
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_16

    .line 521
    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v22

    .line 522
    .local v22, "sourceId":J
    invoke-static/range {v22 .. v23}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v21

    .line 523
    .local v21, "sourceIdStr":Ljava/lang/String;
    invoke-static/range {v22 .. v23}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v7, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 526
    sget-object v4, Lcom/google/android/music/store/MusicFile;->MEDIA_STORE_SOURCE_ACCOUNT_AS_STRING:Ljava/lang/String;

    move-object/from16 v0, v21

    invoke-static {v11, v4, v0, v14}, Lcom/google/android/music/store/MusicFile;->readAndResetPreservingReferences(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/music/store/MusicFile;)Lcom/google/android/music/store/MusicFile;

    .line 530
    const/4 v4, 0x0

    invoke-virtual {v14, v4}, Lcom/google/android/music/store/MusicFile;->setSourceAccount(I)V

    .line 531
    invoke-static/range {v22 .. v23}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v14, v4}, Lcom/google/android/music/store/MusicFile;->setSourceId(Ljava/lang/String;)V

    .line 533
    const/4 v4, 0x1

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v4

    if-nez v4, :cond_1

    .line 534
    const/4 v4, 0x1

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v14, v4}, Lcom/google/android/music/store/MusicFile;->setAlbumName(Ljava/lang/String;)V

    .line 536
    :cond_1
    const/4 v4, 0x2

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v4

    if-nez v4, :cond_2

    .line 537
    const/4 v4, 0x2

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v14, v4}, Lcom/google/android/music/store/MusicFile;->setTrackArtist(Ljava/lang/String;)V

    .line 539
    :cond_2
    const/4 v4, 0x3

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v4

    if-nez v4, :cond_3

    .line 540
    const/4 v4, 0x3

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v14, v4}, Lcom/google/android/music/store/MusicFile;->setComposer(Ljava/lang/String;)V

    .line 543
    :cond_3
    const/4 v4, 0x4

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v4

    if-nez v4, :cond_4

    .line 545
    const/4 v4, 0x4

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    const-wide/16 v28, 0x3e8

    mul-long v4, v4, v28

    invoke-virtual {v14, v4, v5}, Lcom/google/android/music/store/MusicFile;->setAddedTime(J)V

    .line 547
    :cond_4
    const/4 v4, 0x5

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v4

    if-nez v4, :cond_5

    .line 548
    const/4 v4, 0x5

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-virtual {v14, v4, v5}, Lcom/google/android/music/store/MusicFile;->setDurationInMilliSec(J)V

    .line 550
    :cond_5
    const/4 v4, 0x6

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v4

    if-nez v4, :cond_6

    .line 551
    const/4 v4, 0x6

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v14, v4}, Lcom/google/android/music/store/MusicFile;->setMimeType(Ljava/lang/String;)V

    .line 553
    :cond_6
    const/4 v4, 0x7

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v4

    if-eqz v4, :cond_11

    .line 554
    const-wide/16 v4, 0x0

    invoke-virtual {v14, v4, v5}, Lcom/google/android/music/store/MusicFile;->setSize(J)V

    .line 558
    :goto_1
    const/16 v4, 0x8

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v4

    if-nez v4, :cond_7

    .line 559
    const/16 v4, 0x8

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v14, v4}, Lcom/google/android/music/store/MusicFile;->setTitle(Ljava/lang/String;)V

    .line 562
    :cond_7
    const/4 v12, 0x0

    .line 564
    .local v12, "discNumber":S
    const/16 v4, 0x9

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v4

    if-eqz v4, :cond_12

    .line 565
    const/16 v25, 0x0

    .line 570
    .local v25, "track":S
    :goto_2
    const/16 v4, 0x3e8

    move/from16 v0, v25

    if-lt v0, v4, :cond_8

    .line 571
    move/from16 v0, v25

    div-int/lit16 v4, v0, 0x3e8

    int-to-short v12, v4

    .line 572
    move/from16 v0, v25

    rem-int/lit16 v4, v0, 0x3e8

    int-to-short v0, v4

    move/from16 v25, v0

    .line 574
    :cond_8
    invoke-virtual {v14, v12}, Lcom/google/android/music/store/MusicFile;->setDiscPosition(S)V

    .line 575
    move/from16 v0, v25

    invoke-virtual {v14, v0}, Lcom/google/android/music/store/MusicFile;->setTrackPositionInAlbum(S)V

    .line 576
    const/16 v4, 0xa

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v4

    if-nez v4, :cond_9

    .line 577
    const/16 v4, 0xa

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getShort(I)S

    move-result v4

    invoke-virtual {v14, v4}, Lcom/google/android/music/store/MusicFile;->setYear(S)V

    .line 579
    :cond_9
    sget-object v4, Lcom/google/android/music/store/MediaStoreImporter;->sHasAlbumArtist:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v4

    if-eqz v4, :cond_a

    const/16 v4, 0xc

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v4

    if-nez v4, :cond_a

    .line 580
    const/16 v4, 0xc

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v14, v4}, Lcom/google/android/music/store/MusicFile;->setAlbumArtist(Ljava/lang/String;)V

    .line 582
    :cond_a
    const/16 v4, 0xb

    const-wide/16 v28, 0x0

    move-object/from16 v0, p1

    move-wide/from16 v1, v28

    invoke-static {v0, v4, v1, v2}, Lcom/google/android/music/utils/DbUtils;->getNullableLong(Landroid/database/Cursor;IJ)J

    move-result-wide v16

    .line 583
    .local v16, "mediaStoreAlbumId":J
    const/4 v15, 0x0

    .line 584
    .local v15, "hasArt":Z
    const-wide/16 v4, 0x0

    cmp-long v4, v16, v4

    if-eqz v4, :cond_b

    .line 585
    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v6, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Boolean;

    .line 586
    .local v10, "artPresence":Ljava/lang/Boolean;
    if-nez v10, :cond_13

    .line 592
    const-string v4, "MediaStoreImporter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "Unexpected unknown album art presence for album id: "

    move-object/from16 v0, v27

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, v16

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v27, ", cursor pos: "

    move-object/from16 v0, v27

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->getPosition()I

    move-result v27

    move/from16 v0, v27

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    new-instance v27, Ljava/lang/Exception;

    invoke-direct/range {v27 .. v27}, Ljava/lang/Exception;-><init>()V

    move-object/from16 v0, v27

    invoke-static {v4, v5, v0}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 599
    .end local v10    # "artPresence":Ljava/lang/Boolean;
    :cond_b
    :goto_3
    if-eqz v15, :cond_14

    .line 600
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mediastore:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, v16

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v14, v4}, Lcom/google/android/music/store/MusicFile;->setAlbumArtLocation(Ljava/lang/String;)V

    .line 612
    :goto_4
    const/16 v4, 0x12c

    invoke-virtual {v14, v4}, Lcom/google/android/music/store/MusicFile;->setLocalCopyType(I)V

    .line 615
    const-string v4, "<unknown>"

    invoke-virtual {v14}, Lcom/google/android/music/store/MusicFile;->getAlbumName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 616
    const/4 v4, 0x0

    invoke-virtual {v14, v4}, Lcom/google/android/music/store/MusicFile;->setAlbumName(Ljava/lang/String;)V

    .line 618
    :cond_c
    const-string v4, "<unknown>"

    invoke-virtual {v14}, Lcom/google/android/music/store/MusicFile;->getTrackArtist()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_d

    .line 619
    const/4 v4, 0x0

    invoke-virtual {v14, v4}, Lcom/google/android/music/store/MusicFile;->setTrackArtist(Ljava/lang/String;)V

    .line 624
    :cond_d
    const-string v4, "<unknown>"

    invoke-virtual {v14}, Lcom/google/android/music/store/MusicFile;->getAlbumArtist()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_e

    .line 625
    const/4 v4, 0x0

    invoke-virtual {v14, v4}, Lcom/google/android/music/store/MusicFile;->setAlbumArtist(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 629
    :cond_e
    :try_start_1
    invoke-virtual {v14}, Lcom/google/android/music/store/MusicFile;->getLocalId()J

    move-result-wide v4

    const-wide/16 v28, 0x0

    cmp-long v4, v4, v28

    if-lez v4, :cond_15

    .line 630
    move-object/from16 v0, v19

    invoke-virtual {v14, v0, v11}, Lcom/google/android/music/store/MusicFile;->updateMusicFile(Landroid/database/sqlite/SQLiteStatement;Landroid/database/sqlite/SQLiteDatabase;)V

    .line 631
    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/music/store/MediaStoreImporter;->mUpdatedMusicCount:I

    add-int/lit8 v4, v4, 0x1

    move-object/from16 v0, p0

    iput v4, v0, Lcom/google/android/music/store/MediaStoreImporter;->mUpdatedMusicCount:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 636
    :goto_5
    const/16 v26, 0x1

    .line 641
    :goto_6
    if-eqz v26, :cond_0

    :try_start_2
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->isLast()Z

    move-result v4

    if-nez v4, :cond_f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/music/store/MediaStoreImporter;->mAddedMusicCount:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/music/store/MediaStoreImporter;->mUpdatedMusicCount:I

    add-int/2addr v4, v5

    rem-int/lit16 v4, v4, 0x200

    if-nez v4, :cond_0

    .line 646
    :cond_f
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/store/MediaStoreImporter;->mStore:Lcom/google/android/music/store/Store;

    const/4 v5, 0x1

    invoke-virtual {v4, v11, v5}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    .line 647
    const/16 v20, 0x0

    .line 648
    const/16 v26, 0x0

    .line 650
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/store/MediaStoreImporter;->mResolver:Landroid/content/ContentResolver;

    sget-object v5, Lcom/google/android/music/store/MusicContent;->CONTENT_URI:Landroid/net/Uri;

    const/16 v27, 0x0

    const/16 v28, 0x0

    move-object/from16 v0, v27

    move/from16 v1, v28

    invoke-virtual {v4, v5, v0, v1}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    .line 652
    invoke-direct/range {v4 .. v9}, Lcom/google/android/music/store/MediaStoreImporter;->finishAudioBlockImport(Landroid/database/Cursor;Ljava/util/HashMap;Ljava/util/HashSet;Lcom/google/android/music/store/TagNormalizer;Z)Z

    move-result v9

    .line 655
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/store/MediaStoreImporter;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v4}, Lcom/google/android/music/store/Store;->beginWriteTxn()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v11

    .line 656
    const/16 v20, 0x1

    .line 657
    const-wide/16 v4, 0xc8

    invoke-virtual {v11, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->yieldIfContendedSafely(J)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    .line 663
    .end local v7    # "importedAudioIds":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Long;>;"
    .end local v8    # "normalizer":Lcom/google/android/music/store/TagNormalizer;
    .end local v9    # "updateRecentItems":Z
    .end local v12    # "discNumber":S
    .end local v14    # "file":Lcom/google/android/music/store/MusicFile;
    .end local v15    # "hasArt":Z
    .end local v16    # "mediaStoreAlbumId":J
    .end local v21    # "sourceIdStr":Ljava/lang/String;
    .end local v22    # "sourceId":J
    .end local v25    # "track":S
    .end local v26    # "uncommittedChanges":Z
    :catchall_0
    move-exception v4

    invoke-static/range {v18 .. v18}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/sqlite/SQLiteProgram;)V

    .line 664
    invoke-static/range {v19 .. v19}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/sqlite/SQLiteProgram;)V

    .line 665
    if-eqz v20, :cond_10

    .line 666
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/music/store/MediaStoreImporter;->mStore:Lcom/google/android/music/store/Store;

    move/from16 v0, v24

    invoke-virtual {v5, v11, v0}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    :cond_10
    throw v4

    .line 556
    .restart local v7    # "importedAudioIds":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Long;>;"
    .restart local v8    # "normalizer":Lcom/google/android/music/store/TagNormalizer;
    .restart local v9    # "updateRecentItems":Z
    .restart local v14    # "file":Lcom/google/android/music/store/MusicFile;
    .restart local v21    # "sourceIdStr":Ljava/lang/String;
    .restart local v22    # "sourceId":J
    .restart local v26    # "uncommittedChanges":Z
    :cond_11
    const/4 v4, 0x7

    :try_start_3
    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-virtual {v14, v4, v5}, Lcom/google/android/music/store/MusicFile;->setSize(J)V

    goto/16 :goto_1

    .line 567
    .restart local v12    # "discNumber":S
    :cond_12
    const/16 v4, 0x9

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getShort(I)S

    move-result v25

    .restart local v25    # "track":S
    goto/16 :goto_2

    .line 596
    .restart local v10    # "artPresence":Ljava/lang/Boolean;
    .restart local v15    # "hasArt":Z
    .restart local v16    # "mediaStoreAlbumId":J
    :cond_13
    invoke-virtual {v10}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v15

    goto/16 :goto_3

    .line 603
    .end local v10    # "artPresence":Ljava/lang/Boolean;
    :cond_14
    const/4 v4, 0x0

    invoke-virtual {v14, v4}, Lcom/google/android/music/store/MusicFile;->setAlbumArtLocation(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_4

    .line 633
    :cond_15
    :try_start_4
    move-object/from16 v0, v18

    invoke-virtual {v14, v0}, Lcom/google/android/music/store/MusicFile;->insertMusicFile(Landroid/database/sqlite/SQLiteStatement;)J

    .line 634
    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/music/store/MediaStoreImporter;->mAddedMusicCount:I

    add-int/lit8 v4, v4, 0x1

    move-object/from16 v0, p0

    iput v4, v0, Lcom/google/android/music/store/MediaStoreImporter;->mAddedMusicCount:I
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_5

    .line 637
    :catch_0
    move-exception v13

    .line 638
    .local v13, "e":Ljava/lang/Exception;
    :try_start_5
    const-string v4, "MediaStoreImporter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "Failed to insert local file "

    move-object/from16 v0, v27

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v14}, Lcom/google/android/music/store/MusicFile;->toString()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, v27

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_6

    .line 660
    .end local v12    # "discNumber":S
    .end local v13    # "e":Ljava/lang/Exception;
    .end local v15    # "hasArt":Z
    .end local v16    # "mediaStoreAlbumId":J
    .end local v21    # "sourceIdStr":Ljava/lang/String;
    .end local v22    # "sourceId":J
    .end local v25    # "track":S
    :cond_16
    const/16 v24, 0x1

    .line 663
    invoke-static/range {v18 .. v18}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/sqlite/SQLiteProgram;)V

    .line 664
    invoke-static/range {v19 .. v19}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/sqlite/SQLiteProgram;)V

    .line 665
    if-eqz v20, :cond_17

    .line 666
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/store/MediaStoreImporter;->mStore:Lcom/google/android/music/store/Store;

    move/from16 v0, v24

    invoke-virtual {v4, v11, v0}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    .line 669
    :cond_17
    return-void
.end method

.method private importMediaStoreGenre(Lcom/google/android/music/store/TagNormalizer;Ljava/util/HashSet;)V
    .locals 23
    .param p1, "normalizer"    # Lcom/google/android/music/store/TagNormalizer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/music/store/TagNormalizer;",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 873
    .local p2, "audioIds":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Long;>;"
    if-eqz p2, :cond_0

    invoke-virtual/range {p2 .. p2}, Ljava/util/HashSet;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 972
    :goto_0
    return-void

    .line 878
    :cond_0
    const/16 v17, 0x0

    .line 879
    .local v17, "howManyGenres":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/store/MediaStoreImporter;->mContext:Landroid/content/Context;

    sget-object v3, Landroid/provider/MediaStore$Audio$Genres;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    sget-object v4, Lcom/google/android/music/store/MediaStoreImporter;->GENRE_COLUMNS:[Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    .line 882
    .local v13, "genreCursor":Landroid/database/Cursor;
    if-nez v13, :cond_1

    .line 883
    :try_start_0
    const-string v2, "MediaStoreImporter"

    const-string v4, "Failed to get the genre cursor"

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 970
    invoke-static {v13}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto :goto_0

    .line 886
    :cond_1
    :try_start_1
    invoke-interface {v13}, Landroid/database/Cursor;->getCount()I

    move-result v17

    .line 887
    sget-boolean v2, Lcom/google/android/music/store/MediaStoreImporter;->LOGV:Z

    if-eqz v2, :cond_2

    const-string v2, "MediaStoreImporter"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Number of generes in MediaStore: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 890
    :cond_2
    invoke-virtual/range {p2 .. p2}, Ljava/util/HashSet;->size()I

    move-result v2

    mul-int/lit8 v12, v2, 0x6

    .line 891
    .local v12, "bufferSize":I
    new-instance v20, Ljava/lang/StringBuffer;

    move-object/from16 v0, v20

    invoke-direct {v0, v12}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 892
    .local v20, "selectionBuffer":Ljava/lang/StringBuffer;
    new-instance v22, Ljava/lang/StringBuffer;

    move-object/from16 v0, v22

    invoke-direct {v0, v12}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 893
    .local v22, "updateBuffer":Ljava/lang/StringBuffer;
    :cond_3
    :goto_1
    invoke-interface {v13}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_e

    if-eqz p2, :cond_4

    invoke-virtual/range {p2 .. p2}, Ljava/util/HashSet;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_e

    .line 894
    :cond_4
    const/4 v2, 0x0

    invoke-interface {v13, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 895
    .local v14, "genreId":J
    const/4 v2, 0x1

    invoke-interface {v13, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-eqz v2, :cond_7

    const/16 v16, 0x0

    .line 898
    .local v16, "genreName":Ljava/lang/String;
    :goto_2
    if-eqz v16, :cond_3

    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_3

    const-string v2, "<unknown>"

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 904
    invoke-static/range {v16 .. v16}, Lcom/google/android/music/utils/MusicTagUtils;->getGenreName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 906
    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Lcom/google/android/music/store/Store;->canonicalizeAndGenerateId(Lcom/google/android/music/store/TagNormalizer;Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v18

    .line 909
    .local v18, "idAndCanonicalValue":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Long;Ljava/lang/String;>;"
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuffer;->length()I

    move-result v2

    if-nez v2, :cond_5

    .line 910
    const-string v2, "_id"

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 911
    move-object/from16 v0, v20

    move-object/from16 v1, p2

    invoke-static {v0, v1}, Lcom/google/android/music/utils/DbUtils;->appendIN(Ljava/lang/StringBuffer;Ljava/util/Collection;)Ljava/lang/StringBuffer;

    .line 915
    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/store/MediaStoreImporter;->mContext:Landroid/content/Context;

    const-string v4, "external"

    invoke-static {v4, v14, v15}, Landroid/provider/MediaStore$Audio$Genres$Members;->getContentUri(Ljava/lang/String;J)Landroid/net/Uri;

    move-result-object v3

    sget-object v4, Lcom/google/android/music/store/MediaStoreImporter;->GENRE_MEMBER_COLUMNS:[Ljava/lang/String;

    if-nez v20, :cond_8

    const/4 v5, 0x0

    :goto_3
    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v19

    .line 923
    .local v19, "memberCursor":Landroid/database/Cursor;
    if-eqz v19, :cond_6

    :try_start_2
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->getCount()I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result v2

    if-nez v2, :cond_9

    .line 966
    :cond_6
    :try_start_3
    invoke-static/range {v19 .. v19}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 970
    .end local v12    # "bufferSize":I
    .end local v14    # "genreId":J
    .end local v16    # "genreName":Ljava/lang/String;
    .end local v18    # "idAndCanonicalValue":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Long;Ljava/lang/String;>;"
    .end local v19    # "memberCursor":Landroid/database/Cursor;
    .end local v20    # "selectionBuffer":Ljava/lang/StringBuffer;
    .end local v22    # "updateBuffer":Ljava/lang/StringBuffer;
    :catchall_0
    move-exception v2

    invoke-static {v13}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v2

    .line 895
    .restart local v12    # "bufferSize":I
    .restart local v14    # "genreId":J
    .restart local v20    # "selectionBuffer":Ljava/lang/StringBuffer;
    .restart local v22    # "updateBuffer":Ljava/lang/StringBuffer;
    :cond_7
    const/4 v2, 0x1

    :try_start_4
    invoke-interface {v13, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    goto :goto_2

    .line 915
    .restart local v16    # "genreName":Ljava/lang/String;
    .restart local v18    # "idAndCanonicalValue":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Long;Ljava/lang/String;>;"
    :cond_8
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v5

    goto :goto_3

    .line 927
    .restart local v19    # "memberCursor":Landroid/database/Cursor;
    :cond_9
    const/4 v2, 0x0

    :try_start_5
    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 928
    :cond_a
    :goto_4
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 929
    const/4 v2, 0x0

    move-object/from16 v0, v19

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 930
    .local v10, "audioId":J
    const-wide/16 v4, 0x0

    cmp-long v2, v10, v4

    if-eqz v2, :cond_a

    .line 931
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ","

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 932
    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 935
    const/4 v2, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 936
    invoke-virtual/range {p2 .. p2}, Ljava/util/HashSet;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 948
    .end local v10    # "audioId":J
    :cond_b
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuffer;->length()I

    move-result v2

    if-lez v2, :cond_c

    .line 950
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuffer;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 951
    const/16 v21, 0x0

    .line 952
    .local v21, "success":Z
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/store/MediaStoreImporter;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v2}, Lcom/google/android/music/store/Store;->beginWriteTxn()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    move-result-object v3

    .line 954
    .local v3, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v4, 0x0

    :try_start_6
    move-object/from16 v0, v18

    iget-object v2, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    move-object/from16 v0, v18

    iget-object v8, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v8, Ljava/lang/String;

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v2, p0

    move-object/from16 v5, v16

    invoke-direct/range {v2 .. v9}, Lcom/google/android/music/store/MediaStoreImporter;->setGenre(Landroid/database/sqlite/SQLiteDatabase;ILjava/lang/String;JLjava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 957
    const/16 v21, 0x1

    .line 959
    :try_start_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/store/MediaStoreImporter;->mStore:Lcom/google/android/music/store/Store;

    move/from16 v0, v21

    invoke-virtual {v2, v3, v0}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    .line 961
    if-eqz v21, :cond_c

    .line 962
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/store/MediaStoreImporter;->mResolver:Landroid/content/ContentResolver;

    sget-object v4, Lcom/google/android/music/store/MusicContent$Genres;->CONTENT_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v2, v4, v5, v6}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 966
    .end local v3    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v21    # "success":Z
    :cond_c
    :try_start_8
    invoke-static/range {v19 .. v19}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto/16 :goto_1

    .line 944
    .restart local v10    # "audioId":J
    :cond_d
    :try_start_9
    const-string v2, "MediaStoreImporter"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " Unexpected absence of audio "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    goto/16 :goto_4

    .line 966
    .end local v10    # "audioId":J
    :catchall_1
    move-exception v2

    :try_start_a
    invoke-static/range {v19 .. v19}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v2
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 959
    .restart local v3    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .restart local v21    # "success":Z
    :catchall_2
    move-exception v2

    :try_start_b
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/store/MediaStoreImporter;->mStore:Lcom/google/android/music/store/Store;

    move/from16 v0, v21

    invoke-virtual {v4, v3, v0}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    throw v2
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    .line 970
    .end local v3    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v14    # "genreId":J
    .end local v16    # "genreName":Ljava/lang/String;
    .end local v18    # "idAndCanonicalValue":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Long;Ljava/lang/String;>;"
    .end local v19    # "memberCursor":Landroid/database/Cursor;
    .end local v21    # "success":Z
    :cond_e
    invoke-static {v13}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto/16 :goto_0
.end method

.method private importNow()Z
    .locals 28

    .prologue
    .line 234
    const/16 v20, 0x0

    .line 236
    .local v20, "success":Z
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 238
    .local v8, "importStartTime":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/store/MediaStoreImporter;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    const-string v22, "store.preferences"

    const/16 v23, 0x0

    invoke-virtual/range {v21 .. v23}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v12

    .line 240
    .local v12, "prefs":Landroid/content/SharedPreferences;
    const-string v21, "media.store.import.db.version"

    const-string v22, ""

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-interface {v12, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 242
    .local v16, "previouslyImportedMediaDbVersion":Ljava/lang/String;
    move-object/from16 v10, v16

    .line 243
    .local v10, "mediaDbVersion":Ljava/lang/String;
    const-string v21, "media.store.import.db.version.check"

    const/16 v22, 0x0

    move-object/from16 v0, v21

    move/from16 v1, v22

    invoke-interface {v12, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v11

    .line 245
    .local v11, "mediaDbVersionCheck":Z
    if-eqz v11, :cond_0

    .line 251
    invoke-direct/range {p0 .. p0}, Lcom/google/android/music/store/MediaStoreImporter;->getMediaDbVersion()Ljava/lang/String;

    move-result-object v10

    .line 254
    :cond_0
    const-string v21, "media.store.import.time"

    const-wide/16 v22, 0x0

    move-object/from16 v0, v21

    move-wide/from16 v1, v22

    invoke-interface {v12, v0, v1, v2}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v22

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/google/android/music/store/MediaStoreImporter;->mImportChangesSinceDate:J

    .line 255
    const-string v21, "media.store.import.version"

    const-wide/16 v22, 0x0

    move-object/from16 v0, v21

    move-wide/from16 v1, v22

    invoke-interface {v12, v0, v1, v2}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v18

    .line 256
    .local v18, "previouslyImportedVersion":J
    const-string v21, "media.store.import.fs_id"

    const-wide/16 v22, 0x0

    move-object/from16 v0, v21

    move-wide/from16 v1, v22

    invoke-interface {v12, v0, v1, v2}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v14

    .line 258
    .local v14, "previouslyImportedFsId":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/store/MediaStoreImporter;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    invoke-static/range {v21 .. v21}, Lcom/google/android/music/store/MediaStoreImporter;->getMediaStoreFsId(Landroid/content/Context;)J

    move-result-wide v4

    .line 260
    .local v4, "currentFsId":J
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/music/store/MediaStoreImporter;->mImportChangesSinceDate:J

    move-wide/from16 v22, v0

    const-wide/16 v24, 0x1

    cmp-long v21, v22, v24

    if-gez v21, :cond_4

    .line 261
    sget-boolean v21, Lcom/google/android/music/store/MediaStoreImporter;->LOGV:Z

    if-eqz v21, :cond_1

    const-string v21, "MediaStoreImporter"

    const-string v22, "No valid media store import"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 262
    :cond_1
    const-wide/16 v22, 0x0

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/google/android/music/store/MediaStoreImporter;->mImportChangesSinceDate:J

    .line 280
    :cond_2
    :goto_0
    sget-object v21, Lcom/google/android/music/store/MediaStoreImporter;->sHasAlbumArtist:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual/range {v21 .. v21}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v21

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-direct {v0, v1}, Lcom/google/android/music/store/MediaStoreImporter;->getMediaStoreAudioCursor(Z)Landroid/database/Cursor;

    move-result-object v6

    .line 282
    .local v6, "cursor":Landroid/database/Cursor;
    if-nez v6, :cond_3

    sget-object v21, Lcom/google/android/music/store/MediaStoreImporter;->sHasAlbumArtist:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual/range {v21 .. v21}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v21

    if-eqz v21, :cond_3

    .line 284
    const/16 v21, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-direct {v0, v1}, Lcom/google/android/music/store/MediaStoreImporter;->getMediaStoreAudioCursor(Z)Landroid/database/Cursor;

    move-result-object v6

    .line 285
    if-eqz v6, :cond_3

    .line 287
    const-string v21, "MediaStoreImporter"

    const-string v22, "Current system does not support album artist tag"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 288
    sget-object v21, Lcom/google/android/music/store/MediaStoreImporter;->sHasAlbumArtist:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/16 v22, 0x0

    invoke-virtual/range {v21 .. v22}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 292
    :cond_3
    if-nez v6, :cond_7

    .line 293
    const-string v21, "MediaStoreImporter"

    const-string v22, "Failed to get the audio cursor"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 294
    const/16 v21, 0x0

    .line 355
    :goto_1
    return v21

    .line 263
    .end local v6    # "cursor":Landroid/database/Cursor;
    :cond_4
    const-wide/16 v22, 0xa

    cmp-long v21, v18, v22

    if-eqz v21, :cond_5

    .line 264
    const-string v21, "MediaStoreImporter"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "MediaStore import needs an upgrade from version "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move-wide/from16 v1, v18

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " to "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-wide/16 v24, 0xa

    move-object/from16 v0, v22

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 266
    const-wide/16 v22, 0x0

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/google/android/music/store/MediaStoreImporter;->mImportChangesSinceDate:J

    goto :goto_0

    .line 267
    :cond_5
    cmp-long v21, v14, v4

    if-eqz v21, :cond_6

    .line 268
    const-string v21, "MediaStoreImporter"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "File system changed since previous import from "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " to "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 270
    const-wide/16 v22, 0x0

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/google/android/music/store/MediaStoreImporter;->mImportChangesSinceDate:J

    goto/16 :goto_0

    .line 271
    :cond_6
    if-eqz v11, :cond_2

    move-object/from16 v0, v16

    invoke-static {v0, v10}, Lcom/google/android/music/store/MediaStoreImporter;->shouldReimportForMediaStoreVersionChange(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_2

    .line 273
    const-string v21, "MediaStoreImporter"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "Media store database version has changed since previous import from \""

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "\" to \""

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "\""

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 277
    const-wide/16 v22, 0x0

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/google/android/music/store/MediaStoreImporter;->mImportChangesSinceDate:J

    goto/16 :goto_0

    .line 297
    .restart local v6    # "cursor":Landroid/database/Cursor;
    :cond_7
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v7

    .line 298
    .local v7, "howManyFilesToImport":I
    move-object/from16 v0, p0

    iput v7, v0, Lcom/google/android/music/store/MediaStoreImporter;->mAudioFilesInMediaStoreCount:I

    .line 299
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/music/store/MediaStoreImporter;->mImportChangesSinceDate:J

    move-wide/from16 v22, v0

    const-wide/16 v24, 0x0

    cmp-long v21, v22, v24

    if-lez v21, :cond_8

    .line 300
    invoke-direct/range {p0 .. p0}, Lcom/google/android/music/store/MediaStoreImporter;->getAllAudioCount()I

    move-result v21

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/music/store/MediaStoreImporter;->mAudioFilesInMediaStoreCount:I

    .line 303
    :cond_8
    const/16 v17, 0x0

    .line 307
    .local v17, "removedPlaylistItemCount":I
    :try_start_0
    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/google/android/music/store/MediaStoreImporter;->importAudioFiles(Landroid/database/Cursor;)V

    .line 308
    invoke-direct/range {p0 .. p0}, Lcom/google/android/music/store/MediaStoreImporter;->removeDeletedAudioFilesIfNeeded()V

    .line 310
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/music/store/MediaStoreImporter;->mImportChangesSinceDate:J

    move-wide/from16 v22, v0

    const-wide/16 v24, 0x0

    cmp-long v21, v22, v24

    if-eqz v21, :cond_9

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/music/store/MediaStoreImporter;->mAddedMusicCount:I

    move/from16 v21, v0

    if-gtz v21, :cond_9

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/music/store/MediaStoreImporter;->mUpdatedMusicCount:I

    move/from16 v21, v0

    if-gtz v21, :cond_9

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/music/store/MediaStoreImporter;->mDeletedMusicCount:I

    move/from16 v21, v0

    if-lez v21, :cond_a

    .line 312
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/store/MediaStoreImporter;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    invoke-static/range {v21 .. v21}, Lcom/google/android/music/store/MediaStoreImporter;->setLocalMusicAlbumArtistBasedOnRemoteDup(Landroid/content/Context;)I

    .line 313
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/store/MediaStoreImporter;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    invoke-static/range {v21 .. v21}, Lcom/google/android/music/store/RecentItemsManager;->updateRecentItems(Landroid/content/Context;)V

    .line 316
    :cond_a
    invoke-direct/range {p0 .. p0}, Lcom/google/android/music/store/MediaStoreImporter;->importPlaylists()V

    .line 318
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/music/store/MediaStoreImporter;->mImportChangesSinceDate:J

    move-wide/from16 v22, v0

    const-wide/16 v24, 0x0

    cmp-long v21, v22, v24

    if-eqz v21, :cond_b

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/music/store/MediaStoreImporter;->mDeletedMusicCount:I

    move/from16 v21, v0

    if-lez v21, :cond_c

    .line 319
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/store/MediaStoreImporter;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    invoke-static/range {v21 .. v21}, Lcom/google/android/music/store/PlayList;->removeLocalOrphanedItems(Landroid/content/Context;)I

    move-result v17

    .line 322
    :cond_c
    invoke-interface {v12}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v13

    .line 323
    .local v13, "prefsEdit":Landroid/content/SharedPreferences$Editor;
    const-string v21, "media.store.import.time"

    move-object/from16 v0, v21

    invoke-interface {v13, v0, v8, v9}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 324
    const-string v21, "media.store.import.version"

    const-wide/16 v22, 0xa

    move-object/from16 v0, v21

    move-wide/from16 v1, v22

    invoke-interface {v13, v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 325
    const-string v21, "media.store.import.fs_id"

    move-object/from16 v0, v21

    invoke-interface {v13, v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 327
    if-eqz v11, :cond_d

    .line 328
    const-string v21, "media.store.import.db.version"

    move-object/from16 v0, v21

    invoke-interface {v13, v0, v10}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 329
    const-string v21, "media.store.import.db.version.check"

    move-object/from16 v0, v21

    invoke-interface {v13, v0}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 332
    :cond_d
    invoke-interface {v13}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v20

    .line 334
    if-nez v20, :cond_e

    .line 335
    const-string v21, "MediaStoreImporter"

    const-string v22, "Failed to update preference file"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 339
    :cond_e
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 342
    if-eqz v20, :cond_f

    .line 343
    const-string v22, "MediaStoreImporter"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "Update: "

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/music/store/MediaStoreImporter;->mImportChangesSinceDate:J

    move-wide/from16 v24, v0

    const-wide/16 v26, 0x0

    cmp-long v21, v24, v26

    if-nez v21, :cond_10

    const-string v21, "full"

    :goto_2
    move-object/from16 v0, v23

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v23, " Added music: "

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/music/store/MediaStoreImporter;->mAddedMusicCount:I

    move/from16 v23, v0

    move-object/from16 v0, v21

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v23, " Updated music: "

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/music/store/MediaStoreImporter;->mUpdatedMusicCount:I

    move/from16 v23, v0

    move-object/from16 v0, v21

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v23, " Deleted music: "

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/music/store/MediaStoreImporter;->mDeletedMusicCount:I

    move/from16 v23, v0

    move-object/from16 v0, v21

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v23, " Created playlists: "

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/music/store/MediaStoreImporter;->mCreatedPlaylistCount:I

    move/from16 v23, v0

    move-object/from16 v0, v21

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v23, " Updated playlists: "

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/music/store/MediaStoreImporter;->mUpdatedPlaylistCount:I

    move/from16 v23, v0

    move-object/from16 v0, v21

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v23, " Deleted playlists: "

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/music/store/MediaStoreImporter;->mDeletedPlaylistCount:I

    move/from16 v23, v0

    move-object/from16 v0, v21

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v23, " Inserted playlist items: "

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/music/store/MediaStoreImporter;->mInsertedPlaylistItemCount:I

    move/from16 v23, v0

    move-object/from16 v0, v21

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v23, " Deleted playlist items: "

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/music/store/MediaStoreImporter;->mDeletedPlaylistItemCount:I

    move/from16 v23, v0

    move-object/from16 v0, v21

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v23, " Removed orphaned playlist items: "

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v22

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_f
    move/from16 v21, v20

    .line 355
    goto/16 :goto_1

    .line 339
    .end local v13    # "prefsEdit":Landroid/content/SharedPreferences$Editor;
    :catchall_0
    move-exception v21

    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v21

    .line 343
    .restart local v13    # "prefsEdit":Landroid/content/SharedPreferences$Editor;
    :cond_10
    const-string v21, "incremental"

    goto/16 :goto_2
.end method

.method private importPlaylist(JLjava/lang/String;Ljava/lang/Long;Landroid/database/Cursor;)V
    .locals 25
    .param p1, "mediaStorePlaylistId"    # J
    .param p3, "playlistName"    # Ljava/lang/String;
    .param p4, "musicStorePlaylistId"    # Ljava/lang/Long;
    .param p5, "mediaStoreItemCursor"    # Landroid/database/Cursor;

    .prologue
    .line 1151
    new-instance v13, Lcom/google/android/music/store/PlayList;

    invoke-direct {v13}, Lcom/google/android/music/store/PlayList;-><init>()V

    .line 1152
    .local v13, "list":Lcom/google/android/music/store/PlayList;
    invoke-static/range {p1 .. p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v15

    .line 1153
    .local v15, "playlistSourceId":Ljava/lang/String;
    const-wide/16 v16, 0x0

    .line 1155
    .local v16, "startItemDeletionId":J
    const/16 v20, 0x0

    .line 1157
    .local v20, "updated":Z
    if-eqz p4, :cond_0

    .line 1158
    invoke-virtual/range {p4 .. p4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    move-object/from16 v0, p0

    move-object/from16 v1, p5

    invoke-direct {v0, v1, v4, v5}, Lcom/google/android/music/store/MediaStoreImporter;->matchPreviouslyImportedPlaylistItems(Landroid/database/Cursor;J)J

    move-result-wide v16

    .line 1162
    :cond_0
    const/16 v19, 0x0

    .line 1163
    .local v19, "success":Z
    const/16 v18, 0x0

    .line 1164
    .local v18, "statement":Landroid/database/sqlite/SQLiteStatement;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/store/MediaStoreImporter;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v4}, Lcom/google/android/music/store/Store;->beginWriteTxn()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 1166
    .local v3, "db":Landroid/database/sqlite/SQLiteDatabase;
    if-nez p4, :cond_5

    .line 1168
    :try_start_0
    move-object/from16 v0, p3

    invoke-virtual {v13, v0}, Lcom/google/android/music/store/PlayList;->setName(Ljava/lang/String;)V

    .line 1169
    move-wide/from16 v0, p1

    invoke-virtual {v13, v0, v1}, Lcom/google/android/music/store/PlayList;->setMediaStoreId(J)V

    .line 1170
    const/4 v4, 0x0

    invoke-virtual {v13, v4}, Lcom/google/android/music/store/PlayList;->setSourceAccount(I)V

    .line 1171
    invoke-virtual {v13, v15}, Lcom/google/android/music/store/PlayList;->setSourceId(Ljava/lang/String;)V

    .line 1173
    invoke-static {v3}, Lcom/google/android/music/store/PlayList;->compilePlayListInsertStatement(Landroid/database/sqlite/SQLiteDatabase;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v18

    .line 1174
    move-object/from16 v0, v18

    invoke-virtual {v13, v0}, Lcom/google/android/music/store/PlayList;->insertList(Landroid/database/sqlite/SQLiteStatement;)J

    move-result-wide v4

    const-wide/16 v8, 0x0

    cmp-long v4, v4, v8

    if-lez v4, :cond_1

    .line 1175
    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/music/store/MediaStoreImporter;->mCreatedPlaylistCount:I

    add-int/lit8 v4, v4, 0x1

    move-object/from16 v0, p0

    iput v4, v0, Lcom/google/android/music/store/MediaStoreImporter;->mCreatedPlaylistCount:I

    .line 1178
    :cond_1
    sget-boolean v4, Lcom/google/android/music/store/MediaStoreImporter;->LOGV:Z

    if-eqz v4, :cond_2

    .line 1179
    const-string v4, "MediaStoreImporter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Importing playlist "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p3

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " with "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface/range {p5 .. p5}, Landroid/database/Cursor;->getCount()I

    move-result v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " songs"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1232
    :cond_2
    :goto_0
    invoke-interface/range {p5 .. p5}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v4

    if-eqz v4, :cond_a

    .line 1233
    sget-boolean v4, Lcom/google/android/music/store/MediaStoreImporter;->LOGV:Z

    if-eqz v4, :cond_3

    .line 1234
    const-string v4, "MediaStoreImporter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "All items of playlist "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p3

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " already present."

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1267
    :cond_3
    :goto_1
    const-wide/16 v4, 0xc8

    invoke-virtual {v3, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->yieldIfContendedSafely(J)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1268
    const/16 v19, 0x1

    .line 1271
    invoke-static/range {v18 .. v18}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/sqlite/SQLiteProgram;)V

    .line 1272
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/store/MediaStoreImporter;->mStore:Lcom/google/android/music/store/Store;

    move/from16 v0, v19

    invoke-virtual {v4, v3, v0}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    .line 1275
    if-eqz v19, :cond_4

    .line 1276
    if-nez p4, :cond_e

    sget-object v14, Lcom/google/android/music/store/MusicContent$Playlists;->CONTENT_URI:Landroid/net/Uri;

    .line 1278
    .local v14, "notifyUri":Landroid/net/Uri;
    :goto_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/store/MediaStoreImporter;->mResolver:Landroid/content/ContentResolver;

    const/4 v5, 0x0

    const/4 v7, 0x0

    invoke-virtual {v4, v14, v5, v7}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    .line 1280
    .end local v14    # "notifyUri":Landroid/net/Uri;
    :cond_4
    :goto_3
    return-void

    .line 1186
    :cond_5
    :try_start_1
    sget-object v4, Lcom/google/android/music/store/MusicFile;->MEDIA_STORE_SOURCE_ACCOUNT_AS_STRING:Ljava/lang/String;

    invoke-static {v3, v4, v15, v13}, Lcom/google/android/music/store/PlayList;->readPlayList(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/music/store/PlayList;)Lcom/google/android/music/store/PlayList;

    move-result-object v4

    if-nez v4, :cond_6

    .line 1189
    const-string v4, "MediaStoreImporter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Playlist \""

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p3

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "\" ("

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, ") "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "dissapeared while being imported"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1271
    invoke-static/range {v18 .. v18}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/sqlite/SQLiteProgram;)V

    .line 1272
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/store/MediaStoreImporter;->mStore:Lcom/google/android/music/store/Store;

    move/from16 v0, v19

    invoke-virtual {v4, v3, v0}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    goto :goto_3

    .line 1193
    :cond_6
    :try_start_2
    invoke-virtual {v13}, Lcom/google/android/music/store/PlayList;->getMediaStoreId()J

    move-result-wide v4

    const-wide/16 v8, 0x0

    cmp-long v4, v4, v8

    if-nez v4, :cond_7

    .line 1194
    const-string v4, "MediaStoreImporter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Playlist \""

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p3

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "\" ("

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, ") "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "was modified while being imported"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1271
    invoke-static/range {v18 .. v18}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/sqlite/SQLiteProgram;)V

    .line 1272
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/store/MediaStoreImporter;->mStore:Lcom/google/android/music/store/Store;

    move/from16 v0, v19

    invoke-virtual {v4, v3, v0}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    goto/16 :goto_3

    .line 1198
    :cond_7
    :try_start_3
    invoke-virtual {v13}, Lcom/google/android/music/store/PlayList;->getName()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_8

    .line 1199
    const-string v4, "MediaStoreImporter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Playlist "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v13}, Lcom/google/android/music/store/PlayList;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " was renamed to "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p3

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1201
    move-object/from16 v0, p3

    invoke-virtual {v13, v0}, Lcom/google/android/music/store/PlayList;->setName(Ljava/lang/String;)V

    .line 1202
    invoke-static {v3}, Lcom/google/android/music/store/PlayList;->compilePlayListUpdateStatement(Landroid/database/sqlite/SQLiteDatabase;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v18

    .line 1203
    move-object/from16 v0, v18

    invoke-virtual {v13, v0}, Lcom/google/android/music/store/PlayList;->update(Landroid/database/sqlite/SQLiteStatement;)V

    .line 1204
    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/music/store/MediaStoreImporter;->mUpdatedPlaylistCount:I

    add-int/lit8 v4, v4, 0x1

    move-object/from16 v0, p0

    iput v4, v0, Lcom/google/android/music/store/MediaStoreImporter;->mUpdatedPlaylistCount:I

    .line 1205
    const/16 v20, 0x1

    .line 1208
    :cond_8
    const-wide/16 v4, 0x0

    cmp-long v4, v16, v4

    if-eqz v4, :cond_2

    .line 1209
    const/4 v4, 0x0

    move-wide/from16 v0, v16

    invoke-static {v3, v0, v1, v4}, Lcom/google/android/music/store/PlayList$Item;->readItem(Landroid/database/sqlite/SQLiteDatabase;JLcom/google/android/music/store/PlayList$Item;)Lcom/google/android/music/store/PlayList$Item;

    move-result-object v12

    .line 1210
    .local v12, "item":Lcom/google/android/music/store/PlayList$Item;
    if-nez v12, :cond_9

    .line 1211
    const-string v4, "MediaStoreImporter"

    const-string v5, "Unexpected disappearance of the item from the playlist."

    invoke-static {v4, v5}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1271
    invoke-static/range {v18 .. v18}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/sqlite/SQLiteProgram;)V

    .line 1272
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/store/MediaStoreImporter;->mStore:Lcom/google/android/music/store/Store;

    move/from16 v0, v19

    invoke-virtual {v4, v3, v0}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    goto/16 :goto_3

    .line 1216
    :cond_9
    :try_start_4
    const-string v4, "LISTITEMS"

    const-string v5, "ListId=? AND ClientPosition>=?"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    invoke-virtual {v13}, Lcom/google/android/music/store/PlayList;->getId()J

    move-result-wide v22

    invoke-static/range {v22 .. v23}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    invoke-virtual {v12}, Lcom/google/android/music/store/PlayList$Item;->getClientPosition()J

    move-result-wide v22

    invoke-static/range {v22 .. v23}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-virtual {v3, v4, v5, v7}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v11

    .line 1223
    .local v11, "deletedItemsCount":I
    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/music/store/MediaStoreImporter;->mDeletedPlaylistItemCount:I

    add-int/2addr v4, v11

    move-object/from16 v0, p0

    iput v4, v0, Lcom/google/android/music/store/MediaStoreImporter;->mDeletedPlaylistItemCount:I

    .line 1224
    if-lez v11, :cond_2

    if-nez v20, :cond_2

    .line 1225
    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/music/store/MediaStoreImporter;->mUpdatedPlaylistCount:I

    add-int/lit8 v4, v4, 0x1

    move-object/from16 v0, p0

    iput v4, v0, Lcom/google/android/music/store/MediaStoreImporter;->mUpdatedPlaylistCount:I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1226
    const/16 v20, 0x1

    goto/16 :goto_0

    .line 1237
    .end local v11    # "deletedItemsCount":I
    .end local v12    # "item":Lcom/google/android/music/store/PlayList$Item;
    :cond_a
    const/4 v6, 0x0

    .line 1239
    .local v6, "musicIds":Landroid/database/Cursor;
    const/4 v4, 0x0

    :try_start_5
    move-object/from16 v0, p5

    invoke-static {v3, v0, v4}, Lcom/google/android/music/store/Store;->getMusicIdsForSourceIds(Landroid/database/sqlite/SQLiteDatabase;Landroid/database/Cursor;I)Landroid/database/Cursor;

    move-result-object v6

    .line 1241
    if-nez v6, :cond_b

    .line 1242
    const-string v4, "MediaStoreImporter"

    const-string v5, "Failed to lookup music ids"

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 1258
    :try_start_6
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 1271
    invoke-static/range {v18 .. v18}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/sqlite/SQLiteProgram;)V

    .line 1272
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/store/MediaStoreImporter;->mStore:Lcom/google/android/music/store/Store;

    move/from16 v0, v19

    invoke-virtual {v4, v3, v0}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    goto/16 :goto_3

    .line 1245
    :cond_b
    :try_start_7
    invoke-virtual {v13}, Lcom/google/android/music/store/PlayList;->getId()J

    move-result-wide v4

    const/4 v7, 0x1

    sget-object v8, Lcom/google/android/music/store/MusicFile;->MEDIA_STORE_SOURCE_ACCOUNT_AS_INTEGER:Ljava/lang/Integer;

    const/4 v9, 0x1

    const/4 v10, 0x0

    invoke-static/range {v3 .. v10}, Lcom/google/android/music/store/PlayList;->appendItems(Landroid/database/sqlite/SQLiteDatabase;JLandroid/database/Cursor;ZLjava/lang/Integer;ZZ)I

    move-result v2

    .line 1252
    .local v2, "appended":I
    if-lez v2, :cond_d

    .line 1253
    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/music/store/MediaStoreImporter;->mInsertedPlaylistItemCount:I

    add-int/2addr v4, v2

    move-object/from16 v0, p0

    iput v4, v0, Lcom/google/android/music/store/MediaStoreImporter;->mInsertedPlaylistItemCount:I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 1258
    :cond_c
    :goto_4
    :try_start_8
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 1261
    if-eqz p4, :cond_3

    if-nez v20, :cond_3

    .line 1262
    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/music/store/MediaStoreImporter;->mUpdatedPlaylistCount:I

    add-int/lit8 v4, v4, 0x1

    move-object/from16 v0, p0

    iput v4, v0, Lcom/google/android/music/store/MediaStoreImporter;->mUpdatedPlaylistCount:I
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 1263
    const/16 v20, 0x1

    goto/16 :goto_1

    .line 1254
    :cond_d
    if-gez v2, :cond_c

    .line 1255
    :try_start_9
    const-string v4, "MediaStoreImporter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Reached size limit in playlist \""

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p3

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "\""

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto :goto_4

    .line 1258
    .end local v2    # "appended":I
    :catchall_0
    move-exception v4

    :try_start_a
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v4
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    .line 1271
    .end local v6    # "musicIds":Landroid/database/Cursor;
    :catchall_1
    move-exception v4

    invoke-static/range {v18 .. v18}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/sqlite/SQLiteProgram;)V

    .line 1272
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/music/store/MediaStoreImporter;->mStore:Lcom/google/android/music/store/Store;

    move/from16 v0, v19

    invoke-virtual {v5, v3, v0}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    throw v4

    .line 1276
    :cond_e
    invoke-virtual/range {p4 .. p4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/google/android/music/store/MusicContent$Playlists;->getPlaylistUri(J)Landroid/net/Uri;

    move-result-object v14

    goto/16 :goto_2
.end method

.method private importPlaylists()V
    .locals 21

    .prologue
    .line 995
    new-instance v20, Ljava/util/HashMap;

    invoke-direct/range {v20 .. v20}, Ljava/util/HashMap;-><init>()V

    .line 997
    .local v20, "previouslyImported":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Ljava/lang/Long;>;"
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/google/android/music/store/MediaStoreImporter;->mImportChangesSinceDate:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_1

    .line 998
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Lcom/google/android/music/store/MediaStoreImporter;->getLocalPlaylistsFromMusicStore(Ljava/util/Map;)V

    .line 1004
    :goto_0
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/google/android/music/store/MediaStoreImporter;->mImportChangesSinceDate:J

    const-wide/16 v4, 0x3e8

    div-long v12, v2, v4

    .line 1005
    .local v12, "importDateInSeconds":J
    const/4 v9, 0x0

    .line 1006
    .local v9, "howManyLists":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/store/MediaStoreImporter;->mContext:Landroid/content/Context;

    sget-object v3, Landroid/provider/MediaStore$Audio$Playlists;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    sget-object v4, Lcom/google/android/music/store/MediaStoreImporter;->PLAYLIST_COLUMNS:[Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v14

    .line 1012
    .local v14, "listCursor":Landroid/database/Cursor;
    if-nez v14, :cond_2

    .line 1013
    :try_start_0
    const-string v2, "MediaStoreImporter"

    const-string v3, "Failed to get the playlist cursor"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1107
    invoke-static {v14}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 1116
    :cond_0
    :goto_1
    return-void

    .line 1001
    .end local v9    # "howManyLists":I
    .end local v12    # "importDateInSeconds":J
    .end local v14    # "listCursor":Landroid/database/Cursor;
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/store/MediaStoreImporter;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/music/store/PlayList;->deleteAllMediaStorePlaylists(Landroid/content/Context;)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/music/store/MediaStoreImporter;->mDeletedPlaylistCount:I

    goto :goto_0

    .line 1016
    .restart local v9    # "howManyLists":I
    .restart local v12    # "importDateInSeconds":J
    .restart local v14    # "listCursor":Landroid/database/Cursor;
    :cond_2
    :try_start_1
    invoke-interface {v14}, Landroid/database/Cursor;->getCount()I

    move-result v9

    .line 1017
    sget-boolean v2, Lcom/google/android/music/store/MediaStoreImporter;->LOGV:Z

    if-eqz v2, :cond_3

    const-string v2, "MediaStoreImporter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Number of playlists in MediaStore: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1020
    :cond_3
    const/4 v2, 0x1

    if-ge v9, v2, :cond_5

    .line 1021
    invoke-interface {v14}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1107
    :cond_4
    invoke-static {v14}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 1110
    invoke-virtual/range {v20 .. v20}, Ljava/util/HashMap;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1113
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/store/MediaStoreImporter;->mContext:Landroid/content/Context;

    invoke-virtual/range {v20 .. v20}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/music/store/PlayList;->deletePlaylistsAndItems(Landroid/content/Context;Ljava/util/Collection;)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/music/store/MediaStoreImporter;->mDeletedPlaylistCount:I

    goto :goto_1

    .line 1023
    :cond_5
    :goto_2
    :try_start_2
    invoke-interface {v14}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1025
    const/4 v2, 0x0

    invoke-interface {v14, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v16

    .line 1026
    .local v16, "mediaStorePlaylistId":J
    const/4 v2, 0x1

    invoke-interface {v14, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 1027
    .local v15, "playlistName":Ljava/lang/String;
    const-wide/16 v10, 0x0

    .line 1028
    .local v10, "addedDate":J
    const-wide/16 v18, 0x0

    .line 1030
    .local v18, "modifyDate":J
    const/4 v2, 0x2

    invoke-interface {v14, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-nez v2, :cond_6

    .line 1031
    const/4 v2, 0x2

    invoke-interface {v14, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 1033
    :cond_6
    const/4 v2, 0x3

    invoke-interface {v14, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-nez v2, :cond_7

    .line 1034
    const/4 v2, 0x3

    invoke-interface {v14, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v18

    .line 1051
    :cond_7
    if-eqz v15, :cond_8

    invoke-virtual {v15}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_9

    .line 1052
    :cond_8
    const-string v2, "MediaStoreImporter"

    const-string v3, "Skipping import of playlist with empty name"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 1107
    .end local v10    # "addedDate":J
    .end local v15    # "playlistName":Ljava/lang/String;
    .end local v16    # "mediaStorePlaylistId":J
    .end local v18    # "modifyDate":J
    :catchall_0
    move-exception v2

    invoke-static {v14}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v2

    .line 1058
    .restart local v10    # "addedDate":J
    .restart local v15    # "playlistName":Ljava/lang/String;
    .restart local v16    # "mediaStorePlaylistId":J
    .restart local v18    # "modifyDate":J
    :cond_9
    :try_start_3
    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/google/android/music/store/MediaStoreImporter;->haveNonMediaStorePlaylist(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 1059
    const-string v2, "MediaStoreImporter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Skipping import of playlist "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " because non-media store playlist with same name "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "already exits"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2

    .line 1066
    :cond_a
    const/4 v8, 0x0

    .line 1080
    .local v8, "mediaStoreItemCursor":Landroid/database/Cursor;
    :try_start_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/store/MediaStoreImporter;->mContext:Landroid/content/Context;

    const-string v3, "external"

    move-wide/from16 v0, v16

    invoke-static {v3, v0, v1}, Landroid/provider/MediaStore$Audio$Playlists$Members;->getContentUri(Ljava/lang/String;J)Landroid/net/Uri;

    move-result-object v3

    sget-object v4, Lcom/google/android/music/store/MediaStoreImporter;->MEDIA_STORE_PLAYLIST_MEMBER_COLUMNS:[Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string v7, "play_order"

    invoke-static/range {v2 .. v7}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 1089
    if-eqz v8, :cond_c

    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_c

    .line 1090
    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Long;

    .local v7, "musicStorePlaylistId":Ljava/lang/Long;
    move-object/from16 v3, p0

    move-wide/from16 v4, v16

    move-object v6, v15

    .line 1092
    invoke-direct/range {v3 .. v8}, Lcom/google/android/music/store/MediaStoreImporter;->importPlaylist(JLjava/lang/String;Ljava/lang/Long;Landroid/database/Cursor;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1101
    .end local v7    # "musicStorePlaylistId":Ljava/lang/Long;
    :cond_b
    :goto_3
    :try_start_5
    invoke-static {v8}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_2

    .line 1095
    :cond_c
    :try_start_6
    sget-boolean v2, Lcom/google/android/music/store/MediaStoreImporter;->LOGV:Z

    if-eqz v2, :cond_b

    .line 1096
    const-string v2, "MediaStoreImporter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Skipping import of empty playlist "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto :goto_3

    .line 1101
    :catchall_1
    move-exception v2

    :try_start_7
    invoke-static {v8}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v2
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0
.end method

.method private matchPreviouslyImportedPlaylistItems(Landroid/database/Cursor;J)J
    .locals 18
    .param p1, "mediaCursor"    # Landroid/database/Cursor;
    .param p2, "musicStorePlaylistId"    # J

    .prologue
    .line 1297
    const-wide/16 v14, 0x0

    .line 1298
    .local v14, "musicItemIdToDelete":J
    const/4 v12, 0x0

    .line 1299
    .local v12, "musicCursor":Landroid/database/Cursor;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/music/store/MediaStoreImporter;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v3}, Lcom/google/android/music/store/Store;->beginRead()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 1302
    .local v2, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    const-string v3, "LISTITEMS JOIN MUSIC ON (LISTITEMS.MusicId=MUSIC.Id) "

    sget-object v4, Lcom/google/android/music/store/MediaStoreImporter;->MUSIC_STORE_PLAYLIST_ITEM_COLUMNS:[Ljava/lang/String;

    const-string v5, "ListId=?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    invoke-static/range {p2 .. p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x0

    const/4 v8, 0x0

    const-string v9, "ServerOrder, ClientPosition"

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 1309
    if-eqz v12, :cond_2

    .line 1310
    :cond_0
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1311
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1312
    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 1314
    .local v10, "audioId":J
    const/4 v3, 0x1

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v16

    .line 1316
    .local v16, "musicSourceId":J
    cmp-long v3, v10, v16

    if-eqz v3, :cond_0

    .line 1319
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToPrevious()Z

    .line 1320
    invoke-interface {v12}, Landroid/database/Cursor;->moveToPrevious()Z

    .line 1331
    .end local v10    # "audioId":J
    .end local v16    # "musicSourceId":J
    :cond_1
    :goto_0
    invoke-interface {v12}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v3

    if-nez v3, :cond_2

    .line 1332
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1333
    const/4 v3, 0x0

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v14

    .line 1339
    :cond_2
    invoke-static {v12}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 1340
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/music/store/MediaStoreImporter;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v3, v2}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1343
    return-wide v14

    .line 1326
    :cond_3
    :try_start_1
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToPrevious()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1339
    :catchall_0
    move-exception v3

    invoke-static {v12}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 1340
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/store/MediaStoreImporter;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v4, v2}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    throw v3
.end method

.method private removeDeletedAudioFiles()V
    .locals 12

    .prologue
    const/4 v11, 0x0

    const/4 v4, 0x0

    .line 802
    new-instance v9, Ljava/lang/StringBuffer;

    iget v0, p0, Lcom/google/android/music/store/MediaStoreImporter;->mAudioFilesInMediaStoreCount:I

    mul-int/lit8 v0, v0, 0x5

    add-int/lit8 v0, v0, 0x40

    invoke-direct {v9, v0}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 803
    .local v9, "selectionBuffer":Ljava/lang/StringBuffer;
    const-string v0, "SourceAccount"

    invoke-virtual {v9, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const/16 v1, 0x3d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v0

    sget-object v1, Lcom/google/android/music/store/MusicFile;->MEDIA_STORE_SOURCE_ACCOUNT_AS_STRING:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 805
    iget-object v0, p0, Lcom/google/android/music/store/MediaStoreImporter;->mContext:Landroid/content/Context;

    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/google/android/music/store/MediaStoreImporter;->AUDIO_ID_COLUMNS:[Ljava/lang/String;

    const-string v3, "(is_music=1 OR is_podcast=1) AND title IS NOT NULL"

    move-object v5, v4

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 808
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_2

    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 816
    const-string v0, " AND "

    invoke-virtual {v9, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, "SourceId"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, " NOT IN ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 817
    const/4 v0, -0x1

    invoke-interface {v6, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 818
    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 819
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const/16 v1, 0x2c

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 828
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0

    .line 822
    :cond_0
    :try_start_1
    invoke-virtual {v9}, Ljava/lang/StringBuffer;->length()I

    move-result v8

    .line 823
    .local v8, "length":I
    add-int/lit8 v0, v8, -0x1

    const-string v1, ")"

    invoke-virtual {v9, v0, v8, v1}, Ljava/lang/StringBuffer;->replace(IILjava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 828
    .end local v8    # "length":I
    :goto_1
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 832
    const/4 v10, 0x0

    .line 833
    .local v10, "success":Z
    iget-object v0, p0, Lcom/google/android/music/store/MediaStoreImporter;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v0}, Lcom/google/android/music/store/Store;->beginWriteTxn()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v7

    .line 835
    .local v7, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_2
    const-string v0, "MUSIC"

    invoke-virtual {v9}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v7, v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/store/MediaStoreImporter;->mDeletedMusicCount:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 836
    const/4 v10, 0x1

    .line 838
    iget-object v0, p0, Lcom/google/android/music/store/MediaStoreImporter;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v0, v7, v10}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    .line 841
    iget v0, p0, Lcom/google/android/music/store/MediaStoreImporter;->mDeletedMusicCount:I

    if-lez v0, :cond_1

    .line 842
    iget-object v0, p0, Lcom/google/android/music/store/MediaStoreImporter;->mResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/google/android/music/store/MusicContent;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1, v4, v11}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    .line 844
    :cond_1
    return-void

    .line 825
    .end local v7    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v10    # "success":Z
    :cond_2
    :try_start_3
    const-string v0, "MediaStoreImporter"

    const-string v1, "Deleting all local music"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 838
    .restart local v7    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .restart local v10    # "success":Z
    :catchall_1
    move-exception v0

    iget-object v1, p0, Lcom/google/android/music/store/MediaStoreImporter;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v1, v7, v10}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    throw v0
.end method

.method private removeDeletedAudioFilesIfNeeded()V
    .locals 5

    .prologue
    .line 781
    iget-object v2, p0, Lcom/google/android/music/store/MediaStoreImporter;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v2}, Lcom/google/android/music/store/Store;->countLocalMusic()I

    move-result v1

    .line 782
    .local v1, "localFilesInMusicStore":I
    iget v2, p0, Lcom/google/android/music/store/MediaStoreImporter;->mAudioFilesInMediaStoreCount:I

    if-ne v1, v2, :cond_1

    .line 783
    sget-boolean v2, Lcom/google/android/music/store/MediaStoreImporter;->LOGV:Z

    if-eqz v2, :cond_0

    const-string v2, "MediaStoreImporter"

    const-string v3, "No need to delete local files"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 799
    :cond_0
    :goto_0
    return-void

    .line 784
    :cond_1
    iget v2, p0, Lcom/google/android/music/store/MediaStoreImporter;->mAudioFilesInMediaStoreCount:I

    if-le v1, v2, :cond_2

    .line 785
    iget v2, p0, Lcom/google/android/music/store/MediaStoreImporter;->mAudioFilesInMediaStoreCount:I

    sub-int v0, v1, v2

    .line 787
    .local v0, "expectedDeletions":I
    const-string v2, "MediaStoreImporter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Need to delete "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " local files"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 788
    invoke-direct {p0}, Lcom/google/android/music/store/MediaStoreImporter;->removeDeletedAudioFiles()V

    .line 789
    iget v2, p0, Lcom/google/android/music/store/MediaStoreImporter;->mDeletedMusicCount:I

    if-eq v0, v2, :cond_0

    .line 790
    const-string v2, "MediaStoreImporter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Expected to delete "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " files but actually deleted "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/google/android/music/store/MediaStoreImporter;->mDeletedMusicCount:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 795
    .end local v0    # "expectedDeletions":I
    :cond_2
    const-string v2, "MediaStoreImporter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Music store has "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/google/android/music/store/MediaStoreImporter;->mAudioFilesInMediaStoreCount:I

    sub-int/2addr v4, v1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " less files than media store after import"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method static requestMediaStoreVersionCheck(Landroid/content/Context;)V
    .locals 4
    .param p0, "c"    # Landroid/content/Context;

    .prologue
    .line 390
    const-string v2, "store.preferences"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 391
    .local v1, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 392
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "media.store.import.db.version.check"

    const/4 v3, 0x1

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 393
    invoke-static {v0}, Lcom/google/android/music/SharedPreferencesCompat;->apply(Landroid/content/SharedPreferences$Editor;)V

    .line 394
    return-void
.end method

.method private setGenre(Landroid/database/sqlite/SQLiteDatabase;ILjava/lang/String;JLjava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "sourceAccount"    # I
    .param p3, "genreName"    # Ljava/lang/String;
    .param p4, "genreId"    # J
    .param p6, "canonicalGenreName"    # Ljava/lang/String;
    .param p7, "ids"    # Ljava/lang/String;

    .prologue
    .line 977
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 978
    .local v0, "values":Landroid/content/ContentValues;
    const-string v2, "Genre"

    invoke-virtual {v0, v2, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 979
    const-string v2, "GenreId"

    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 980
    const-string v2, "CanonicalGenre"

    invoke-virtual {v0, v2, p6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 982
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "GenreId=0 AND SourceAccount="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "SourceId"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " IN ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 986
    .local v1, "where":Ljava/lang/String;
    const-string v2, "MUSIC"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v0, v1, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 987
    return-void
.end method

.method static setLocalMusicAlbumArtistBasedOnRemoteDup(Landroid/content/Context;)I
    .locals 15
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 1406
    sget-object v12, Lcom/google/android/music/store/MediaStoreImporter;->sHasAlbumArtist:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v12}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v12

    if-eqz v12, :cond_0

    .line 1509
    :goto_0
    return v2

    .line 1416
    :cond_0
    invoke-static {p0}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v9

    .line 1417
    .local v9, "store":Lcom/google/android/music/store/Store;
    const/4 v2, 0x0

    .line 1418
    .local v2, "count":I
    const/4 v1, 0x0

    .line 1419
    .local v1, "c":Landroid/database/Cursor;
    invoke-virtual {v9}, Lcom/google/android/music/store/Store;->beginRead()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 1422
    .local v3, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "SELECT DISTINCT local.Id, remote.AlbumArtist FROM MUSIC as local, MUSIC as remote  WHERE local.CanonicalName=remote.CanonicalName AND local.CanonicalAlbum=remote.CanonicalAlbum AND local.DiscNumber=remote.DiscNumber AND local.TrackNumber=remote.TrackNumber AND abs(local.Duration - remote.Duration) < 5000 AND local.SourceAccount="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    sget-object v13, Lcom/google/android/music/store/MusicFile;->MEDIA_STORE_SOURCE_ACCOUNT_AS_STRING:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " AND ("

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "local."

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "AlbumArtistOrigin"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const/4 v13, 0x1

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " OR "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "local."

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "AlbumArtistId"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "=0"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ") AND "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "remote."

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "SourceAccount"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "<>"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    sget-object v13, Lcom/google/android/music/store/MusicFile;->MEDIA_STORE_SOURCE_ACCOUNT_AS_STRING:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " AND "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "remote."

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "AlbumArtistOrigin"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x0

    invoke-virtual {v3, v12, v13}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 1463
    if-eqz v1, :cond_1

    .line 1464
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v2

    .line 1467
    :cond_1
    invoke-virtual {v9, v3}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1470
    const/4 v3, 0x0

    .line 1471
    const/4 v11, 0x0

    .line 1472
    .local v11, "update":Landroid/database/sqlite/SQLiteStatement;
    const/4 v10, 0x0

    .line 1474
    .local v10, "success":Z
    if-lez v2, :cond_2

    .line 1475
    :try_start_1
    invoke-virtual {v9}, Lcom/google/android/music/store/Store;->beginWriteTxn()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 1476
    invoke-static {v3}, Lcom/google/android/music/store/MusicFile;->compileFullUpdateStatement(Landroid/database/sqlite/SQLiteDatabase;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v11

    .line 1478
    :cond_2
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    .line 1479
    .local v8, "resolver":Landroid/content/ContentResolver;
    new-instance v5, Lcom/google/android/music/store/MusicFile;

    invoke-direct {v5}, Lcom/google/android/music/store/MusicFile;-><init>()V

    .line 1480
    .local v5, "file":Lcom/google/android/music/store/MusicFile;
    const/4 v2, 0x0

    .line 1481
    :cond_3
    :goto_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v12

    if-eqz v12, :cond_6

    .line 1482
    const/4 v12, 0x0

    invoke-interface {v1, v12}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 1483
    .local v6, "id":J
    const/4 v12, 0x1

    invoke-interface {v1, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 1485
    .local v0, "albumArtist":Ljava/lang/String;
    :try_start_2
    invoke-virtual {v5, v3, v6, v7}, Lcom/google/android/music/store/MusicFile;->load(Landroid/database/sqlite/SQLiteDatabase;J)V

    .line 1486
    invoke-virtual {v5, v0}, Lcom/google/android/music/store/MusicFile;->forceAlbumArtist(Ljava/lang/String;)V

    .line 1487
    invoke-virtual {v5, v11, v3}, Lcom/google/android/music/store/MusicFile;->updateMusicFile(Landroid/database/sqlite/SQLiteStatement;Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1488
    add-int/lit8 v2, v2, 0x1

    .line 1489
    invoke-interface {v1}, Landroid/database/Cursor;->isLast()Z

    move-result v12

    if-nez v12, :cond_4

    rem-int/lit16 v12, v2, 0x200

    if-nez v12, :cond_3

    .line 1490
    :cond_4
    const/4 v12, 0x1

    invoke-virtual {v9, v3, v12}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    .line 1491
    const/4 v3, 0x0

    .line 1492
    sget-object v12, Lcom/google/android/music/store/MusicContent;->CONTENT_URI:Landroid/net/Uri;

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-virtual {v8, v12, v13, v14}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    .line 1493
    invoke-virtual {v9}, Lcom/google/android/music/store/Store;->beginWriteTxn()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 1494
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->yieldIfContendedSafely()Z
    :try_end_2
    .catch Lcom/google/android/music/store/DataNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 1496
    :catch_0
    move-exception v4

    .line 1497
    .local v4, "e":Lcom/google/android/music/store/DataNotFoundException;
    :try_start_3
    const-string v12, "MediaStoreImporter"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Local music file with id "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " disappeared"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 1502
    .end local v0    # "albumArtist":Ljava/lang/String;
    .end local v4    # "e":Lcom/google/android/music/store/DataNotFoundException;
    .end local v5    # "file":Lcom/google/android/music/store/MusicFile;
    .end local v6    # "id":J
    .end local v8    # "resolver":Landroid/content/ContentResolver;
    :catchall_0
    move-exception v12

    if-eqz v3, :cond_5

    .line 1503
    invoke-virtual {v9, v3, v10}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    .line 1505
    :cond_5
    invoke-static {v11}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/sqlite/SQLiteProgram;)V

    .line 1506
    invoke-static {v1}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v12

    .line 1467
    .end local v10    # "success":Z
    .end local v11    # "update":Landroid/database/sqlite/SQLiteStatement;
    :catchall_1
    move-exception v12

    invoke-virtual {v9, v3}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    throw v12

    .line 1500
    .restart local v5    # "file":Lcom/google/android/music/store/MusicFile;
    .restart local v8    # "resolver":Landroid/content/ContentResolver;
    .restart local v10    # "success":Z
    .restart local v11    # "update":Landroid/database/sqlite/SQLiteStatement;
    :cond_6
    const/4 v10, 0x1

    .line 1502
    if-eqz v3, :cond_7

    .line 1503
    invoke-virtual {v9, v3, v10}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    .line 1505
    :cond_7
    invoke-static {v11}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/sqlite/SQLiteProgram;)V

    .line 1506
    invoke-static {v1}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto/16 :goto_0
.end method

.method private static shouldReimportForMediaStoreVersionChange(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2
    .param p0, "oldVersion"    # Ljava/lang/String;
    .param p1, "newVersion"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 407
    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 418
    :cond_0
    :goto_0
    return v0

    .line 413
    :cond_1
    const-string v1, "MediaStore.601"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "MediaStore.700"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 418
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static updateLocalMusicBasedOnRemoteContentAsync(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1516
    sget-object v0, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    .line 1518
    .local v0, "bgWorker":Landroid/os/Handler;
    sget v2, Lcom/google/android/music/store/MediaStoreImporter;->UPDATE_POST_SYNC_MESSAGE_TYPE:I

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 1520
    new-instance v2, Lcom/google/android/music/store/MediaStoreImporter$1;

    invoke-direct {v2, p0}, Lcom/google/android/music/store/MediaStoreImporter$1;-><init>(Landroid/content/Context;)V

    invoke-static {v0, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;Ljava/lang/Runnable;)Landroid/os/Message;

    move-result-object v1

    .line 1525
    .local v1, "msg":Landroid/os/Message;
    sget v2, Lcom/google/android/music/store/MediaStoreImporter;->UPDATE_POST_SYNC_MESSAGE_TYPE:I

    iput v2, v1, Landroid/os/Message;->what:I

    .line 1526
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1527
    return-void
.end method


# virtual methods
.method declared-synchronized doImport(Landroid/content/Context;)Z
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 201
    monitor-enter p0

    const-wide/16 v0, 0x0

    :try_start_0
    iput-wide v0, p0, Lcom/google/android/music/store/MediaStoreImporter;->mImportChangesSinceDate:J

    .line 203
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/music/store/MediaStoreImporter;->mAddedMusicCount:I

    .line 204
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/music/store/MediaStoreImporter;->mUpdatedMusicCount:I

    .line 205
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/music/store/MediaStoreImporter;->mDeletedMusicCount:I

    .line 206
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/music/store/MediaStoreImporter;->mAudioFilesInMediaStoreCount:I

    .line 207
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/music/store/MediaStoreImporter;->mCreatedPlaylistCount:I

    .line 208
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/music/store/MediaStoreImporter;->mUpdatedPlaylistCount:I

    .line 209
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/music/store/MediaStoreImporter;->mDeletedPlaylistCount:I

    .line 210
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/music/store/MediaStoreImporter;->mInsertedPlaylistItemCount:I

    .line 211
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/music/store/MediaStoreImporter;->mDeletedPlaylistItemCount:I

    .line 213
    iput-object p1, p0, Lcom/google/android/music/store/MediaStoreImporter;->mContext:Landroid/content/Context;

    .line 214
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/store/MediaStoreImporter;->mResolver:Landroid/content/ContentResolver;

    .line 215
    invoke-static {p1}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/store/MediaStoreImporter;->mStore:Lcom/google/android/music/store/Store;

    .line 220
    iget-object v0, p0, Lcom/google/android/music/store/MediaStoreImporter;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v0}, Lcom/google/android/music/store/Store;->createDatabase()V

    .line 222
    invoke-direct {p0}, Lcom/google/android/music/store/MediaStoreImporter;->importNow()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 225
    const/4 v1, 0x0

    :try_start_1
    iput-object v1, p0, Lcom/google/android/music/store/MediaStoreImporter;->mContext:Landroid/content/Context;

    .line 226
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/music/store/MediaStoreImporter;->mResolver:Landroid/content/ContentResolver;

    .line 227
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/music/store/MediaStoreImporter;->mStore:Lcom/google/android/music/store/Store;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    monitor-exit p0

    return v0

    .line 225
    :catchall_0
    move-exception v0

    const/4 v1, 0x0

    :try_start_2
    iput-object v1, p0, Lcom/google/android/music/store/MediaStoreImporter;->mContext:Landroid/content/Context;

    .line 226
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/music/store/MediaStoreImporter;->mResolver:Landroid/content/ContentResolver;

    .line 227
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/music/store/MediaStoreImporter;->mStore:Lcom/google/android/music/store/Store;

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 201
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method invalidateMediaStoreImport(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 1391
    const-string v1, "store.preferences"

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1392
    .local v0, "prefEditor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "media.store.import.time"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1393
    const-string v1, "media.store.import.version"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1394
    const-string v1, "media.store.import.fs_id"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1395
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1396
    return-void
.end method
