.class Lcom/google/android/music/dial/DialRemotePlaybackClient$2;
.super Ljava/lang/Object;
.source "DialRemotePlaybackClient.java"

# interfaces
.implements Lcom/google/android/music/utils/async/AsyncRunner;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/dial/DialRemotePlaybackClient;->handleNewSession()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/dial/DialRemotePlaybackClient;


# direct methods
.method constructor <init>(Lcom/google/android/music/dial/DialRemotePlaybackClient;)V
    .locals 0

    .prologue
    .line 370
    iput-object p1, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient$2;->this$0:Lcom/google/android/music/dial/DialRemotePlaybackClient;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public backgroundTask()V
    .locals 2

    .prologue
    .line 373
    # getter for: Lcom/google/android/music/dial/DialRemotePlaybackClient;->LOGV:Z
    invoke-static {}, Lcom/google/android/music/dial/DialRemotePlaybackClient;->access$200()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 374
    const-string v0, "DialRemotePlaybackClient"

    const-string v1, "Setting cloud queue"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 376
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient$2;->this$0:Lcom/google/android/music/dial/DialRemotePlaybackClient;

    # getter for: Lcom/google/android/music/dial/DialRemotePlaybackClient;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/music/dial/DialRemotePlaybackClient;->access$300(Lcom/google/android/music/dial/DialRemotePlaybackClient;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/music/cloudclient/CloudQueueManager;->copyLocalQueueToCloudQueue(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 377
    iget-object v0, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient$2;->this$0:Lcom/google/android/music/dial/DialRemotePlaybackClient;

    # getter for: Lcom/google/android/music/dial/DialRemotePlaybackClient;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/music/dial/DialRemotePlaybackClient;->access$300(Lcom/google/android/music/dial/DialRemotePlaybackClient;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/music/cloudclient/CloudQueueManager;->setCloudQueue(Landroid/content/Context;)V

    .line 379
    :cond_1
    return-void
.end method

.method public taskCompleted()V
    .locals 1

    .prologue
    .line 383
    iget-object v0, p0, Lcom/google/android/music/dial/DialRemotePlaybackClient$2;->this$0:Lcom/google/android/music/dial/DialRemotePlaybackClient;

    # getter for: Lcom/google/android/music/dial/DialRemotePlaybackClient;->mServiceHooks:Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;
    invoke-static {v0}, Lcom/google/android/music/dial/DialRemotePlaybackClient;->access$400(Lcom/google/android/music/dial/DialRemotePlaybackClient;)Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;->onCloudQueueFirstSenderConnected()V

    .line 384
    return-void
.end method
