.class Lcom/google/android/music/net/NetworkMonitor$NotifyListenersDownloadabilityChangedRunnable;
.super Ljava/lang/Object;
.source "NetworkMonitor.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/net/NetworkMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "NotifyListenersDownloadabilityChangedRunnable"
.end annotation


# instance fields
.field private final mIsDownloadable:Z

.field final synthetic this$0:Lcom/google/android/music/net/NetworkMonitor;


# direct methods
.method public constructor <init>(Lcom/google/android/music/net/NetworkMonitor;Z)V
    .locals 0
    .param p2, "isDownloadable"    # Z

    .prologue
    .line 423
    iput-object p1, p0, Lcom/google/android/music/net/NetworkMonitor$NotifyListenersDownloadabilityChangedRunnable;->this$0:Lcom/google/android/music/net/NetworkMonitor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 424
    iput-boolean p2, p0, Lcom/google/android/music/net/NetworkMonitor$NotifyListenersDownloadabilityChangedRunnable;->mIsDownloadable:Z

    .line 425
    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 429
    iget-object v3, p0, Lcom/google/android/music/net/NetworkMonitor$NotifyListenersDownloadabilityChangedRunnable;->this$0:Lcom/google/android/music/net/NetworkMonitor;

    # getter for: Lcom/google/android/music/net/NetworkMonitor;->mDownloadabilityListeners:Landroid/os/RemoteCallbackList;
    invoke-static {v3}, Lcom/google/android/music/net/NetworkMonitor;->access$1900(Lcom/google/android/music/net/NetworkMonitor;)Landroid/os/RemoteCallbackList;

    move-result-object v4

    monitor-enter v4

    .line 431
    :try_start_0
    iget-object v3, p0, Lcom/google/android/music/net/NetworkMonitor$NotifyListenersDownloadabilityChangedRunnable;->this$0:Lcom/google/android/music/net/NetworkMonitor;

    # getter for: Lcom/google/android/music/net/NetworkMonitor;->mDownloadabilityListeners:Landroid/os/RemoteCallbackList;
    invoke-static {v3}, Lcom/google/android/music/net/NetworkMonitor;->access$1900(Lcom/google/android/music/net/NetworkMonitor;)Landroid/os/RemoteCallbackList;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v1

    .line 432
    .local v1, "i":I
    :goto_0
    if-lez v1, :cond_0

    .line 433
    add-int/lit8 v1, v1, -0x1

    .line 434
    iget-object v3, p0, Lcom/google/android/music/net/NetworkMonitor$NotifyListenersDownloadabilityChangedRunnable;->this$0:Lcom/google/android/music/net/NetworkMonitor;

    # getter for: Lcom/google/android/music/net/NetworkMonitor;->mDownloadabilityListeners:Landroid/os/RemoteCallbackList;
    invoke-static {v3}, Lcom/google/android/music/net/NetworkMonitor;->access$1900(Lcom/google/android/music/net/NetworkMonitor;)Landroid/os/RemoteCallbackList;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/net/IDownloadabilityChangeListener;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 437
    .local v2, "listener":Lcom/google/android/music/net/IDownloadabilityChangeListener;
    :try_start_1
    iget-boolean v3, p0, Lcom/google/android/music/net/NetworkMonitor$NotifyListenersDownloadabilityChangedRunnable;->mIsDownloadable:Z

    invoke-interface {v2, v3}, Lcom/google/android/music/net/IDownloadabilityChangeListener;->onDownloadabilityChanged(Z)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 438
    :catch_0
    move-exception v0

    .line 439
    .local v0, "e":Landroid/os/RemoteException;
    :try_start_2
    const-string v3, "NetworkMonitor"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Error trying to notify DownloadabilityListener: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5, v0}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 444
    .end local v0    # "e":Landroid/os/RemoteException;
    .end local v1    # "i":I
    .end local v2    # "listener":Lcom/google/android/music/net/IDownloadabilityChangeListener;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v3

    .line 443
    .restart local v1    # "i":I
    :cond_0
    :try_start_3
    iget-object v3, p0, Lcom/google/android/music/net/NetworkMonitor$NotifyListenersDownloadabilityChangedRunnable;->this$0:Lcom/google/android/music/net/NetworkMonitor;

    # getter for: Lcom/google/android/music/net/NetworkMonitor;->mDownloadabilityListeners:Landroid/os/RemoteCallbackList;
    invoke-static {v3}, Lcom/google/android/music/net/NetworkMonitor;->access$1900(Lcom/google/android/music/net/NetworkMonitor;)Landroid/os/RemoteCallbackList;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 444
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 445
    return-void
.end method
