.class Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;
.super Ljava/lang/Object;
.source "ManageDownloadsFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/ManageDownloadsFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ListItemEntry"
.end annotation


# instance fields
.field final artUrls:Ljava/lang/String;

.field final containerType:Lcom/google/android/music/ui/ManageDownloadsFragment$ContainerType;

.field downloadedSongCount:I

.field final id:J

.field isRemoving:Z

.field keeponSongCount:I

.field final listType:I

.field final remoteId:Ljava/lang/String;

.field final size:Ljava/lang/String;

.field final songList:Lcom/google/android/music/medialist/SongList;

.field final subtitle:Ljava/lang/String;

.field final title:Ljava/lang/String;


# direct methods
.method constructor <init>(JILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/music/ui/ManageDownloadsFragment$ContainerType;Ljava/lang/String;Ljava/lang/String;)V
    .locals 15
    .param p1, "id"    # J
    .param p3, "listType"    # I
    .param p4, "title"    # Ljava/lang/String;
    .param p5, "subtitle"    # Ljava/lang/String;
    .param p6, "size"    # Ljava/lang/String;
    .param p7, "containerType"    # Lcom/google/android/music/ui/ManageDownloadsFragment$ContainerType;
    .param p8, "artUrls"    # Ljava/lang/String;
    .param p9, "remoteId"    # Ljava/lang/String;

    .prologue
    .line 689
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 690
    move-wide/from16 v0, p1

    iput-wide v0, p0, Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;->id:J

    .line 691
    move/from16 v0, p3

    iput v0, p0, Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;->listType:I

    .line 692
    move-object/from16 v0, p4

    iput-object v0, p0, Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;->title:Ljava/lang/String;

    .line 693
    move-object/from16 v0, p5

    iput-object v0, p0, Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;->subtitle:Ljava/lang/String;

    .line 694
    move-object/from16 v0, p6

    iput-object v0, p0, Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;->size:Ljava/lang/String;

    .line 695
    move-object/from16 v0, p7

    iput-object v0, p0, Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;->containerType:Lcom/google/android/music/ui/ManageDownloadsFragment$ContainerType;

    .line 696
    move-object/from16 v0, p8

    iput-object v0, p0, Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;->artUrls:Ljava/lang/String;

    .line 697
    move-object/from16 v0, p9

    iput-object v0, p0, Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;->remoteId:Ljava/lang/String;

    .line 699
    sget-object v2, Lcom/google/android/music/ui/ManageDownloadsFragment$5;->$SwitchMap$com$google$android$music$ui$ManageDownloadsFragment$ContainerType:[I

    invoke-virtual/range {p7 .. p7}, Lcom/google/android/music/ui/ManageDownloadsFragment$ContainerType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 716
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unrecognized containerType="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p7

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 701
    :pswitch_0
    new-instance v3, Lcom/google/android/music/medialist/AlbumSongList;

    const/4 v8, 0x1

    move-wide/from16 v4, p1

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    invoke-direct/range {v3 .. v8}, Lcom/google/android/music/medialist/AlbumSongList;-><init>(JLjava/lang/String;Ljava/lang/String;Z)V

    iput-object v3, p0, Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;->songList:Lcom/google/android/music/medialist/SongList;

    .line 719
    :goto_0
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;->isRemoving:Z

    .line 720
    return-void

    .line 704
    :pswitch_1
    new-instance v3, Lcom/google/android/music/medialist/RadioStationSongList;

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-wide/from16 v4, p1

    move-object/from16 v6, p9

    move-object/from16 v7, p4

    invoke-direct/range {v3 .. v9}, Lcom/google/android/music/medialist/RadioStationSongList;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v3, p0, Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;->songList:Lcom/google/android/music/medialist/SongList;

    goto :goto_0

    .line 708
    :pswitch_2
    new-instance v3, Lcom/google/android/music/medialist/PlaylistSongList;

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x1

    move-wide/from16 v4, p1

    move-object/from16 v6, p4

    move/from16 v7, p3

    invoke-direct/range {v3 .. v13}, Lcom/google/android/music/medialist/PlaylistSongList;-><init>(JLjava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    iput-object v3, p0, Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;->songList:Lcom/google/android/music/medialist/SongList;

    goto :goto_0

    .line 712
    :pswitch_3
    const/4 v2, 0x0

    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v3

    move-wide/from16 v0, p1

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/music/medialist/AutoPlaylistSongList;->getAutoPlaylist(JZLcom/google/android/music/preferences/MusicPreferences;)Lcom/google/android/music/medialist/AutoPlaylistSongList;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;->songList:Lcom/google/android/music/medialist/SongList;

    goto :goto_0

    .line 699
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 728
    if-ne p0, p1, :cond_1

    .line 756
    :cond_0
    :goto_0
    return v1

    .line 731
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    :cond_2
    move v1, v2

    .line 732
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 735
    check-cast v0, Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;

    .line 737
    .local v0, "that":Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;
    iget-wide v4, p0, Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;->id:J

    iget-wide v6, v0, Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;->id:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_4

    move v1, v2

    .line 738
    goto :goto_0

    .line 740
    :cond_4
    iget v3, p0, Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;->listType:I

    iget v4, v0, Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;->listType:I

    if-eq v3, v4, :cond_5

    move v1, v2

    .line 741
    goto :goto_0

    .line 743
    :cond_5
    iget-object v3, p0, Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;->containerType:Lcom/google/android/music/ui/ManageDownloadsFragment$ContainerType;

    iget-object v4, v0, Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;->containerType:Lcom/google/android/music/ui/ManageDownloadsFragment$ContainerType;

    if-eq v3, v4, :cond_6

    move v1, v2

    .line 744
    goto :goto_0

    .line 746
    :cond_6
    iget-object v3, p0, Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;->subtitle:Ljava/lang/String;

    if-eqz v3, :cond_8

    iget-object v3, p0, Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;->subtitle:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;->subtitle:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_9

    :cond_7
    move v1, v2

    .line 747
    goto :goto_0

    .line 746
    :cond_8
    iget-object v3, v0, Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;->subtitle:Ljava/lang/String;

    if-nez v3, :cond_7

    .line 749
    :cond_9
    iget-object v3, p0, Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;->title:Ljava/lang/String;

    if-eqz v3, :cond_b

    iget-object v3, p0, Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;->title:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;->title:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_c

    :cond_a
    move v1, v2

    .line 750
    goto :goto_0

    .line 749
    :cond_b
    iget-object v3, v0, Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;->title:Ljava/lang/String;

    if-nez v3, :cond_a

    .line 752
    :cond_c
    iget-object v3, p0, Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;->remoteId:Ljava/lang/String;

    if-eqz v3, :cond_d

    iget-object v3, p0, Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;->remoteId:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;->remoteId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :goto_1
    move v1, v2

    .line 753
    goto :goto_0

    .line 752
    :cond_d
    iget-object v3, v0, Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;->remoteId:Ljava/lang/String;

    if-eqz v3, :cond_0

    goto :goto_1
.end method

.method public hashCode()I
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 761
    iget-wide v4, p0, Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;->id:J

    iget-wide v6, p0, Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;->id:J

    const/16 v1, 0x20

    ushr-long/2addr v6, v1

    xor-long/2addr v4, v6

    long-to-int v0, v4

    .line 762
    .local v0, "result":I
    mul-int/lit8 v1, v0, 0x1f

    iget v3, p0, Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;->listType:I

    add-int v0, v1, v3

    .line 763
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;->containerType:Lcom/google/android/music/ui/ManageDownloadsFragment$ContainerType;

    invoke-virtual {v3}, Lcom/google/android/music/ui/ManageDownloadsFragment$ContainerType;->ordinal()I

    move-result v3

    add-int v0, v1, v3

    .line 764
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;->title:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;->title:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :goto_0
    add-int v0, v3, v1

    .line 765
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;->subtitle:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;->subtitle:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :goto_1
    add-int v0, v3, v1

    .line 766
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;->remoteId:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v2, p0, Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;->remoteId:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_0
    add-int v0, v1, v2

    .line 767
    return v0

    :cond_1
    move v1, v2

    .line 764
    goto :goto_0

    :cond_2
    move v1, v2

    .line 765
    goto :goto_1
.end method

.method setRemoving(Z)V
    .locals 0
    .param p1, "removing"    # Z

    .prologue
    .line 723
    iput-boolean p1, p0, Lcom/google/android/music/ui/ManageDownloadsFragment$ListItemEntry;->isRemoving:Z

    .line 724
    return-void
.end method
