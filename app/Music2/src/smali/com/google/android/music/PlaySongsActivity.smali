.class public Lcom/google/android/music/PlaySongsActivity;
.super Landroid/app/Activity;
.source "PlaySongsActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/PlaySongsActivity$4;,
        Lcom/google/android/music/PlaySongsActivity$AsyncCheckupWorker;,
        Lcom/google/android/music/PlaySongsActivity$CheckState;
    }
.end annotation


# instance fields
.field private mAsyncWorker:Lcom/google/android/music/PlaySongsActivity$AsyncCheckupWorker;

.field private mContentObserver:Landroid/database/ContentObserver;

.field private mCurrentState:Lcom/google/android/music/PlaySongsActivity$CheckState;

.field private mMarketAccount:Ljava/lang/String;

.field private mMusicAccount:Ljava/lang/String;

.field private mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

.field private mPositiveButton:Landroid/widget/Button;

.field private mProgressBar:Landroid/widget/ProgressBar;

.field private mSecondaryButton:Landroid/widget/Button;

.field private mStoreId:Ljava/lang/String;

.field private mSyncObserver:Landroid/content/SyncStatusObserver;

.field private mSyncObserverHandle:Ljava/lang/Object;

.field private mTextView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 45
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 64
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/PlaySongsActivity;->mCurrentState:Lcom/google/android/music/PlaySongsActivity$CheckState;

    .line 361
    new-instance v0, Lcom/google/android/music/PlaySongsActivity$2;

    invoke-direct {v0, p0}, Lcom/google/android/music/PlaySongsActivity$2;-><init>(Lcom/google/android/music/PlaySongsActivity;)V

    iput-object v0, p0, Lcom/google/android/music/PlaySongsActivity;->mSyncObserver:Landroid/content/SyncStatusObserver;

    .line 410
    new-instance v0, Lcom/google/android/music/PlaySongsActivity$3;

    sget-object v1, Lcom/google/android/music/utils/async/AsyncWorkers;->sUIBackgroundWorker:Lcom/google/android/music/utils/LoggableHandler;

    invoke-direct {v0, p0, v1}, Lcom/google/android/music/PlaySongsActivity$3;-><init>(Lcom/google/android/music/PlaySongsActivity;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/google/android/music/PlaySongsActivity;->mContentObserver:Landroid/database/ContentObserver;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/PlaySongsActivity;)Lcom/google/android/music/preferences/MusicPreferences;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/PlaySongsActivity;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/music/PlaySongsActivity;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/music/PlaySongsActivity;)Lcom/google/android/music/PlaySongsActivity$AsyncCheckupWorker;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/PlaySongsActivity;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/music/PlaySongsActivity;->mAsyncWorker:Lcom/google/android/music/PlaySongsActivity$AsyncCheckupWorker;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/music/PlaySongsActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/PlaySongsActivity;

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/google/android/music/PlaySongsActivity;->doesMarketAccountMatchMusicAccount()Z

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/music/PlaySongsActivity;Lcom/google/android/music/PlaySongsActivity$CheckState;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/PlaySongsActivity;
    .param p1, "x1"    # Lcom/google/android/music/PlaySongsActivity$CheckState;

    .prologue
    .line 45
    invoke-direct {p0, p1}, Lcom/google/android/music/PlaySongsActivity;->startState(Lcom/google/android/music/PlaySongsActivity$CheckState;)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/music/PlaySongsActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/PlaySongsActivity;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/music/PlaySongsActivity;->mStoreId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/music/PlaySongsActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/PlaySongsActivity;

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/google/android/music/PlaySongsActivity;->isSyncActive()Z

    move-result v0

    return v0
.end method

.method private doesMarketAccountMatchMusicAccount()Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 138
    invoke-virtual {p0}, Lcom/google/android/music/PlaySongsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 139
    .local v0, "i":Landroid/content/Intent;
    const-string v3, "authAccount"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/music/PlaySongsActivity;->mMarketAccount:Ljava/lang/String;

    .line 141
    iget-object v3, p0, Lcom/google/android/music/PlaySongsActivity;->mMarketAccount:Ljava/lang/String;

    if-eqz v3, :cond_2

    .line 142
    iget-object v3, p0, Lcom/google/android/music/PlaySongsActivity;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    invoke-virtual {v3}, Lcom/google/android/music/preferences/MusicPreferences;->getSyncAccount()Landroid/accounts/Account;

    move-result-object v1

    .line 143
    .local v1, "musicAccount":Landroid/accounts/Account;
    if-nez v1, :cond_1

    .line 147
    sget-object v3, Lcom/google/android/music/tutorial/TutorialUtils$EntryPoint;->UNKNOWN:Lcom/google/android/music/tutorial/TutorialUtils$EntryPoint;

    invoke-static {p0, v3}, Lcom/google/android/music/tutorial/TutorialUtils;->launchTutorialOnDemand(Landroid/app/Activity;Lcom/google/android/music/tutorial/TutorialUtils$EntryPoint;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 149
    invoke-virtual {p0}, Lcom/google/android/music/PlaySongsActivity;->finish()V

    .line 159
    .end local v1    # "musicAccount":Landroid/accounts/Account;
    :cond_0
    :goto_0
    return v2

    .line 152
    .restart local v1    # "musicAccount":Landroid/accounts/Account;
    :cond_1
    iget-object v3, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/music/PlaySongsActivity;->mMarketAccount:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 153
    iget-object v3, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    iput-object v3, p0, Lcom/google/android/music/PlaySongsActivity;->mMusicAccount:Ljava/lang/String;

    goto :goto_0

    .line 157
    .end local v1    # "musicAccount":Landroid/accounts/Account;
    :cond_2
    const-string v2, "PlaySongsAct"

    const-string v3, "Market did not provide the account name"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 159
    :cond_3
    const/4 v2, 0x1

    goto :goto_0
.end method

.method private isSyncActive()Z
    .locals 2

    .prologue
    .line 372
    iget-object v1, p0, Lcom/google/android/music/PlaySongsActivity;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    invoke-virtual {v1}, Lcom/google/android/music/preferences/MusicPreferences;->getStreamingAccount()Landroid/accounts/Account;

    move-result-object v0

    .line 373
    .local v0, "account":Landroid/accounts/Account;
    if-nez v0, :cond_0

    .line 374
    const/4 v1, 0x0

    .line 376
    :goto_0
    return v1

    :cond_0
    const-string v1, "com.google.android.music.MusicContent"

    invoke-static {v0, v1}, Landroid/content/ContentResolver;->isSyncActive(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v1

    goto :goto_0
.end method

.method private declared-synchronized startState(Lcom/google/android/music/PlaySongsActivity$CheckState;)V
    .locals 6
    .param p1, "state"    # Lcom/google/android/music/PlaySongsActivity$CheckState;

    .prologue
    .line 163
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/android/music/PlaySongsActivity;->mCurrentState:Lcom/google/android/music/PlaySongsActivity$CheckState;

    .line 165
    sget-object v1, Lcom/google/android/music/PlaySongsActivity$4;->$SwitchMap$com$google$android$music$PlaySongsActivity$CheckState:[I

    invoke-virtual {p1}, Lcom/google/android/music/PlaySongsActivity$CheckState;->ordinal()I

    move-result v2

    aget v1, v1, v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    packed-switch v1, :pswitch_data_0

    .line 196
    :goto_0
    monitor-exit p0

    return-void

    .line 167
    :pswitch_0
    :try_start_1
    iget-object v1, p0, Lcom/google/android/music/PlaySongsActivity;->mProgressBar:Landroid/widget/ProgressBar;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 168
    iget-object v1, p0, Lcom/google/android/music/PlaySongsActivity;->mTextView:Landroid/widget/TextView;

    const/16 v2, 0x30

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setGravity(I)V

    .line 169
    iget-object v1, p0, Lcom/google/android/music/PlaySongsActivity;->mTextView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/music/PlaySongsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0213

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 170
    iget-object v1, p0, Lcom/google/android/music/PlaySongsActivity;->mSecondaryButton:Landroid/widget/Button;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 172
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 173
    .local v0, "syncExtras":Landroid/os/Bundle;
    const-string v1, "expedited"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 174
    const-string v1, "force"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 175
    iget-object v1, p0, Lcom/google/android/music/PlaySongsActivity;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    invoke-virtual {v1}, Lcom/google/android/music/preferences/MusicPreferences;->getSyncAccount()Landroid/accounts/Account;

    move-result-object v1

    const-string v2, "com.google.android.music.MusicContent"

    invoke-static {v1, v2, v0}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 163
    .end local v0    # "syncExtras":Landroid/os/Bundle;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 179
    :pswitch_1
    :try_start_2
    iget-object v1, p0, Lcom/google/android/music/PlaySongsActivity;->mTextView:Landroid/widget/TextView;

    const v2, 0x7f0b020b

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/music/PlaySongsActivity;->mMarketAccount:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/google/android/music/PlaySongsActivity;->mMusicAccount:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {p0, v2, v3}, Lcom/google/android/music/PlaySongsActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 181
    iget-object v1, p0, Lcom/google/android/music/PlaySongsActivity;->mSecondaryButton:Landroid/widget/Button;

    const v2, 0x7f0b020d

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(I)V

    .line 182
    iget-object v1, p0, Lcom/google/android/music/PlaySongsActivity;->mSecondaryButton:Landroid/widget/Button;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 183
    iget-object v1, p0, Lcom/google/android/music/PlaySongsActivity;->mTextView:Landroid/widget/TextView;

    const/16 v2, 0x13

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setGravity(I)V

    .line 184
    iget-object v1, p0, Lcom/google/android/music/PlaySongsActivity;->mProgressBar:Landroid/widget/ProgressBar;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0

    .line 188
    :pswitch_2
    iget-object v1, p0, Lcom/google/android/music/PlaySongsActivity;->mSecondaryButton:Landroid/widget/Button;

    const v2, 0x7f0b0212

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(I)V

    .line 189
    iget-object v1, p0, Lcom/google/android/music/PlaySongsActivity;->mSecondaryButton:Landroid/widget/Button;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 190
    iget-object v1, p0, Lcom/google/android/music/PlaySongsActivity;->mTextView:Landroid/widget/TextView;

    const/16 v2, 0x11

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setGravity(I)V

    .line 191
    iget-object v1, p0, Lcom/google/android/music/PlaySongsActivity;->mTextView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/music/PlaySongsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0214

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 193
    iget-object v1, p0, Lcom/google/android/music/PlaySongsActivity;->mProgressBar:Landroid/widget/ProgressBar;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    .line 165
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v1, 0x1

    .line 399
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    .line 400
    if-ne p1, v1, :cond_0

    .line 401
    invoke-direct {p0}, Lcom/google/android/music/PlaySongsActivity;->doesMarketAccountMatchMusicAccount()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 402
    sget-object v0, Lcom/google/android/music/PlaySongsActivity$CheckState;->WAITING_FOR_SYNC:Lcom/google/android/music/PlaySongsActivity$CheckState;

    invoke-direct {p0, v0}, Lcom/google/android/music/PlaySongsActivity;->startState(Lcom/google/android/music/PlaySongsActivity$CheckState;)V

    .line 403
    iget-object v0, p0, Lcom/google/android/music/PlaySongsActivity;->mAsyncWorker:Lcom/google/android/music/PlaySongsActivity$AsyncCheckupWorker;

    invoke-virtual {v0, v1}, Lcom/google/android/music/PlaySongsActivity$AsyncCheckupWorker;->sendEmptyMessage(I)Z

    .line 408
    :cond_0
    :goto_0
    return-void

    .line 404
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/music/PlaySongsActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 405
    sget-object v0, Lcom/google/android/music/PlaySongsActivity$CheckState;->ACCOUNT_MATCH_FAILURE:Lcom/google/android/music/PlaySongsActivity$CheckState;

    invoke-direct {p0, v0}, Lcom/google/android/music/PlaySongsActivity;->startState(Lcom/google/android/music/PlaySongsActivity$CheckState;)V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x1

    .line 380
    iget-object v0, p0, Lcom/google/android/music/PlaySongsActivity;->mPositiveButton:Landroid/widget/Button;

    if-ne p1, v0, :cond_1

    .line 381
    invoke-virtual {p0}, Lcom/google/android/music/PlaySongsActivity;->finish()V

    .line 393
    :cond_0
    :goto_0
    return-void

    .line 382
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/PlaySongsActivity;->mSecondaryButton:Landroid/widget/Button;

    if-ne p1, v0, :cond_0

    .line 383
    sget-object v0, Lcom/google/android/music/PlaySongsActivity$4;->$SwitchMap$com$google$android$music$PlaySongsActivity$CheckState:[I

    iget-object v1, p0, Lcom/google/android/music/PlaySongsActivity;->mCurrentState:Lcom/google/android/music/PlaySongsActivity$CheckState;

    invoke-virtual {v1}, Lcom/google/android/music/PlaySongsActivity$CheckState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 385
    :pswitch_0
    invoke-static {p0, v2, v2}, Lcom/google/android/music/tutorial/TutorialUtils;->launchTutorialToChooseAccountForResult(Landroid/app/Activity;ZI)Z

    goto :goto_0

    .line 389
    :pswitch_1
    sget-object v0, Lcom/google/android/music/PlaySongsActivity$CheckState;->WAITING_FOR_SYNC:Lcom/google/android/music/PlaySongsActivity$CheckState;

    invoke-direct {p0, v0}, Lcom/google/android/music/PlaySongsActivity;->startState(Lcom/google/android/music/PlaySongsActivity$CheckState;)V

    goto :goto_0

    .line 383
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 77
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 78
    invoke-static {p0, p0}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/PlaySongsActivity;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    .line 80
    new-instance v1, Lcom/google/android/music/PlaySongsActivity$AsyncCheckupWorker;

    invoke-direct {v1, p0}, Lcom/google/android/music/PlaySongsActivity$AsyncCheckupWorker;-><init>(Lcom/google/android/music/PlaySongsActivity;)V

    iput-object v1, p0, Lcom/google/android/music/PlaySongsActivity;->mAsyncWorker:Lcom/google/android/music/PlaySongsActivity$AsyncCheckupWorker;

    .line 82
    const v1, 0x7f040109

    invoke-virtual {p0, v1}, Lcom/google/android/music/PlaySongsActivity;->setContentView(I)V

    .line 83
    const v1, 0x7f0e00df

    invoke-virtual {p0, v1}, Lcom/google/android/music/PlaySongsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    iput-object v1, p0, Lcom/google/android/music/PlaySongsActivity;->mProgressBar:Landroid/widget/ProgressBar;

    .line 84
    const v1, 0x7f0e007d

    invoke-virtual {p0, v1}, Lcom/google/android/music/PlaySongsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/music/PlaySongsActivity;->mTextView:Landroid/widget/TextView;

    .line 85
    const v1, 0x7f0e0290

    invoke-virtual {p0, v1}, Lcom/google/android/music/PlaySongsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/google/android/music/PlaySongsActivity;->mPositiveButton:Landroid/widget/Button;

    .line 86
    iget-object v1, p0, Lcom/google/android/music/PlaySongsActivity;->mPositiveButton:Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 87
    const v1, 0x7f0e0291

    invoke-virtual {p0, v1}, Lcom/google/android/music/PlaySongsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/google/android/music/PlaySongsActivity;->mSecondaryButton:Landroid/widget/Button;

    .line 88
    iget-object v1, p0, Lcom/google/android/music/PlaySongsActivity;->mSecondaryButton:Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 90
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/music/PlaySongsActivity;->mSyncObserver:Landroid/content/SyncStatusObserver;

    invoke-static {v1, v2}, Landroid/content/ContentResolver;->addStatusChangeListener(ILandroid/content/SyncStatusObserver;)Ljava/lang/Object;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/PlaySongsActivity;->mSyncObserverHandle:Ljava/lang/Object;

    .line 92
    invoke-virtual {p0}, Lcom/google/android/music/PlaySongsActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/google/android/music/store/MusicContent;->CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/music/PlaySongsActivity;->mContentObserver:Landroid/database/ContentObserver;

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 95
    invoke-virtual {p0}, Lcom/google/android/music/PlaySongsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 96
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "storeId"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/PlaySongsActivity;->mStoreId:Ljava/lang/String;

    .line 97
    iget-object v1, p0, Lcom/google/android/music/PlaySongsActivity;->mStoreId:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 98
    const-string v1, "PlaySongsAct"

    const-string v2, "storeId extra was not supplied"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    invoke-virtual {p0}, Lcom/google/android/music/PlaySongsActivity;->finish()V

    .line 124
    :goto_0
    return-void

    .line 103
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/PlaySongsActivity;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    new-instance v2, Lcom/google/android/music/PlaySongsActivity$1;

    invoke-direct {v2, p0}, Lcom/google/android/music/PlaySongsActivity$1;-><init>(Lcom/google/android/music/PlaySongsActivity;)V

    invoke-virtual {v1, v2}, Lcom/google/android/music/preferences/MusicPreferences;->runWithPreferenceService(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 205
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 206
    iget-object v0, p0, Lcom/google/android/music/PlaySongsActivity;->mAsyncWorker:Lcom/google/android/music/PlaySongsActivity$AsyncCheckupWorker;

    invoke-virtual {v0}, Lcom/google/android/music/PlaySongsActivity$AsyncCheckupWorker;->quit()V

    .line 207
    iget-object v0, p0, Lcom/google/android/music/PlaySongsActivity;->mSyncObserverHandle:Ljava/lang/Object;

    invoke-static {v0}, Landroid/content/ContentResolver;->removeStatusChangeListener(Ljava/lang/Object;)V

    .line 208
    invoke-virtual {p0}, Lcom/google/android/music/PlaySongsActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/PlaySongsActivity;->mContentObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 209
    invoke-static {p0}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    .line 210
    return-void
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 200
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 201
    return-void
.end method
