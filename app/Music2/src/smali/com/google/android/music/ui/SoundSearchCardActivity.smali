.class public Lcom/google/android/music/ui/SoundSearchCardActivity;
.super Lcom/google/android/music/ui/BaseActivity;
.source "SoundSearchCardActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/android/music/ui/BaseActivity;-><init>()V

    return-void
.end method

.method public static showSoundSearchTutorialCard(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 30
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/music/ui/SoundSearchCardActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 31
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 32
    return-void
.end method


# virtual methods
.method protected doesSupportDownloadOnlyBanner()Z
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x0

    return v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedState"    # Landroid/os/Bundle;

    .prologue
    .line 15
    invoke-super {p0, p1}, Lcom/google/android/music/ui/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 16
    invoke-virtual {p0}, Lcom/google/android/music/ui/SoundSearchCardActivity;->getContent()Landroid/support/v4/app/Fragment;

    move-result-object v1

    if-nez v1, :cond_0

    .line 17
    new-instance v0, Lcom/google/android/music/ui/SoundSearchCardFragment;

    invoke-direct {v0}, Lcom/google/android/music/ui/SoundSearchCardFragment;-><init>()V

    .line 18
    .local v0, "fragment":Landroid/support/v4/app/Fragment;
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/music/ui/SoundSearchCardActivity;->replaceContent(Landroid/support/v4/app/Fragment;Z)V

    .line 21
    .end local v0    # "fragment":Landroid/support/v4/app/Fragment;
    :cond_0
    const v1, 0x7f0b00c3

    invoke-virtual {p0, v1}, Lcom/google/android/music/ui/SoundSearchCardActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/music/ui/SoundSearchCardActivity;->setActionBarTitle(Ljava/lang/String;)V

    .line 22
    return-void
.end method
