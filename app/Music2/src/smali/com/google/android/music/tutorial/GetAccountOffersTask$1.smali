.class Lcom/google/android/music/tutorial/GetAccountOffersTask$1;
.super Ljava/lang/Object;
.source "GetAccountOffersTask.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/tutorial/GetAccountOffersTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/tutorial/GetAccountOffersTask;


# direct methods
.method constructor <init>(Lcom/google/android/music/tutorial/GetAccountOffersTask;)V
    .locals 0

    .prologue
    .line 46
    iput-object p1, p0, Lcom/google/android/music/tutorial/GetAccountOffersTask$1;->this$0:Lcom/google/android/music/tutorial/GetAccountOffersTask;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    .line 49
    iget-object v5, p0, Lcom/google/android/music/tutorial/GetAccountOffersTask$1;->this$0:Lcom/google/android/music/tutorial/GetAccountOffersTask;

    # getter for: Lcom/google/android/music/tutorial/GetAccountOffersTask;->mIsCanceled:Z
    invoke-static {v5}, Lcom/google/android/music/tutorial/GetAccountOffersTask;->access$000(Lcom/google/android/music/tutorial/GetAccountOffersTask;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 119
    :cond_0
    :goto_0
    return-void

    .line 51
    :cond_1
    iget-object v5, p0, Lcom/google/android/music/tutorial/GetAccountOffersTask$1;->this$0:Lcom/google/android/music/tutorial/GetAccountOffersTask;

    # getter for: Lcom/google/android/music/tutorial/GetAccountOffersTask;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/google/android/music/tutorial/GetAccountOffersTask;->access$100(Lcom/google/android/music/tutorial/GetAccountOffersTask;)Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/music/utils/GoogleEduUtils;->isEduDevice(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 52
    iget-object v5, p0, Lcom/google/android/music/tutorial/GetAccountOffersTask$1;->this$0:Lcom/google/android/music/tutorial/GetAccountOffersTask;

    # getter for: Lcom/google/android/music/tutorial/GetAccountOffersTask;->mCallbackHandler:Landroid/os/Handler;
    invoke-static {v5}, Lcom/google/android/music/tutorial/GetAccountOffersTask;->access$300(Lcom/google/android/music/tutorial/GetAccountOffersTask;)Landroid/os/Handler;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/music/tutorial/GetAccountOffersTask$1;->this$0:Lcom/google/android/music/tutorial/GetAccountOffersTask;

    # getter for: Lcom/google/android/music/tutorial/GetAccountOffersTask;->mAccountDisabledCallbackRunnable:Ljava/lang/Runnable;
    invoke-static {v6}, Lcom/google/android/music/tutorial/GetAccountOffersTask;->access$200(Lcom/google/android/music/tutorial/GetAccountOffersTask;)Ljava/lang/Runnable;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 56
    :cond_2
    iget-object v5, p0, Lcom/google/android/music/tutorial/GetAccountOffersTask$1;->this$0:Lcom/google/android/music/tutorial/GetAccountOffersTask;

    const/4 v6, 0x0

    # setter for: Lcom/google/android/music/tutorial/GetAccountOffersTask;->mResponse:Lcom/google/android/music/cloudclient/OffersResponseJson;
    invoke-static {v5, v6}, Lcom/google/android/music/tutorial/GetAccountOffersTask;->access$402(Lcom/google/android/music/tutorial/GetAccountOffersTask;Lcom/google/android/music/cloudclient/OffersResponseJson;)Lcom/google/android/music/cloudclient/OffersResponseJson;

    .line 57
    const-string v4, ""

    .line 58
    .local v4, "error":Ljava/lang/String;
    new-instance v0, Lcom/google/android/music/cloudclient/MusicCloudImpl;

    iget-object v5, p0, Lcom/google/android/music/tutorial/GetAccountOffersTask$1;->this$0:Lcom/google/android/music/tutorial/GetAccountOffersTask;

    # getter for: Lcom/google/android/music/tutorial/GetAccountOffersTask;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/google/android/music/tutorial/GetAccountOffersTask;->access$100(Lcom/google/android/music/tutorial/GetAccountOffersTask;)Landroid/content/Context;

    move-result-object v5

    invoke-direct {v0, v5}, Lcom/google/android/music/cloudclient/MusicCloudImpl;-><init>(Landroid/content/Context;)V

    .line 60
    .local v0, "cloudClient":Lcom/google/android/music/cloudclient/MusicCloud;
    :try_start_0
    iget-object v5, p0, Lcom/google/android/music/tutorial/GetAccountOffersTask$1;->this$0:Lcom/google/android/music/tutorial/GetAccountOffersTask;

    # getter for: Lcom/google/android/music/tutorial/GetAccountOffersTask;->mCoupon:Ljava/lang/String;
    invoke-static {v5}, Lcom/google/android/music/tutorial/GetAccountOffersTask;->access$500(Lcom/google/android/music/tutorial/GetAccountOffersTask;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 61
    iget-object v5, p0, Lcom/google/android/music/tutorial/GetAccountOffersTask$1;->this$0:Lcom/google/android/music/tutorial/GetAccountOffersTask;

    iget-object v6, p0, Lcom/google/android/music/tutorial/GetAccountOffersTask$1;->this$0:Lcom/google/android/music/tutorial/GetAccountOffersTask;

    # getter for: Lcom/google/android/music/tutorial/GetAccountOffersTask;->mAccount:Landroid/accounts/Account;
    invoke-static {v6}, Lcom/google/android/music/tutorial/GetAccountOffersTask;->access$600(Lcom/google/android/music/tutorial/GetAccountOffersTask;)Landroid/accounts/Account;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/music/tutorial/GetAccountOffersTask$1;->this$0:Lcom/google/android/music/tutorial/GetAccountOffersTask;

    # getter for: Lcom/google/android/music/tutorial/GetAccountOffersTask;->mCoupon:Ljava/lang/String;
    invoke-static {v7}, Lcom/google/android/music/tutorial/GetAccountOffersTask;->access$500(Lcom/google/android/music/tutorial/GetAccountOffersTask;)Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/music/tutorial/GetAccountOffersTask$1;->this$0:Lcom/google/android/music/tutorial/GetAccountOffersTask;

    # getter for: Lcom/google/android/music/tutorial/GetAccountOffersTask;->mCouponType:Ljava/lang/String;
    invoke-static {v8}, Lcom/google/android/music/tutorial/GetAccountOffersTask;->access$700(Lcom/google/android/music/tutorial/GetAccountOffersTask;)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v0, v6, v7, v8}, Lcom/google/android/music/cloudclient/MusicCloud;->getOffersForAccountAndRedeemCoupon(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/music/cloudclient/OffersResponseJson;

    move-result-object v6

    # setter for: Lcom/google/android/music/tutorial/GetAccountOffersTask;->mResponse:Lcom/google/android/music/cloudclient/OffersResponseJson;
    invoke-static {v5, v6}, Lcom/google/android/music/tutorial/GetAccountOffersTask;->access$402(Lcom/google/android/music/tutorial/GetAccountOffersTask;Lcom/google/android/music/cloudclient/OffersResponseJson;)Lcom/google/android/music/cloudclient/OffersResponseJson;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 74
    :goto_1
    iget-object v5, p0, Lcom/google/android/music/tutorial/GetAccountOffersTask$1;->this$0:Lcom/google/android/music/tutorial/GetAccountOffersTask;

    # getter for: Lcom/google/android/music/tutorial/GetAccountOffersTask;->mResponse:Lcom/google/android/music/cloudclient/OffersResponseJson;
    invoke-static {v5}, Lcom/google/android/music/tutorial/GetAccountOffersTask;->access$400(Lcom/google/android/music/tutorial/GetAccountOffersTask;)Lcom/google/android/music/cloudclient/OffersResponseJson;

    move-result-object v5

    if-eqz v5, :cond_3

    .line 77
    iget-object v5, p0, Lcom/google/android/music/tutorial/GetAccountOffersTask$1;->this$0:Lcom/google/android/music/tutorial/GetAccountOffersTask;

    # getter for: Lcom/google/android/music/tutorial/GetAccountOffersTask;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/google/android/music/tutorial/GetAccountOffersTask;->access$100(Lcom/google/android/music/tutorial/GetAccountOffersTask;)Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "music_show_coupon_status"

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v5

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/google/android/music/tutorial/GetAccountOffersTask$1;->this$0:Lcom/google/android/music/tutorial/GetAccountOffersTask;

    # getter for: Lcom/google/android/music/tutorial/GetAccountOffersTask;->mCoupon:Ljava/lang/String;
    invoke-static {v5}, Lcom/google/android/music/tutorial/GetAccountOffersTask;->access$500(Lcom/google/android/music/tutorial/GetAccountOffersTask;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 81
    const-string v1, ""

    .line 84
    .local v1, "couponStatusText":Ljava/lang/String;
    iget-object v5, p0, Lcom/google/android/music/tutorial/GetAccountOffersTask$1;->this$0:Lcom/google/android/music/tutorial/GetAccountOffersTask;

    # getter for: Lcom/google/android/music/tutorial/GetAccountOffersTask;->mResponse:Lcom/google/android/music/cloudclient/OffersResponseJson;
    invoke-static {v5}, Lcom/google/android/music/tutorial/GetAccountOffersTask;->access$400(Lcom/google/android/music/tutorial/GetAccountOffersTask;)Lcom/google/android/music/cloudclient/OffersResponseJson;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/music/cloudclient/OffersResponseJson;->getCouponStatus()I

    move-result v5

    packed-switch v5, :pswitch_data_0

    .line 102
    const-string v5, "AccountOffers"

    const-string v6, "Unknown coupon status"

    invoke-static {v5, v6}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    :goto_2
    move-object v2, v1

    .line 105
    .local v2, "couponStatusTextFinal":Ljava/lang/String;
    new-instance v5, Lcom/google/android/music/tutorial/GetAccountOffersTask$1$1;

    invoke-direct {v5, p0, v2}, Lcom/google/android/music/tutorial/GetAccountOffersTask$1$1;-><init>(Lcom/google/android/music/tutorial/GetAccountOffersTask$1;Ljava/lang/String;)V

    iget-object v6, p0, Lcom/google/android/music/tutorial/GetAccountOffersTask$1;->this$0:Lcom/google/android/music/tutorial/GetAccountOffersTask;

    # getter for: Lcom/google/android/music/tutorial/GetAccountOffersTask;->mContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/google/android/music/tutorial/GetAccountOffersTask;->access$100(Lcom/google/android/music/tutorial/GetAccountOffersTask;)Landroid/content/Context;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/android/music/utils/MusicUtils;->runOnUiThread(Ljava/lang/Runnable;Landroid/content/Context;)V

    .line 116
    .end local v1    # "couponStatusText":Ljava/lang/String;
    .end local v2    # "couponStatusTextFinal":Ljava/lang/String;
    :cond_3
    iget-object v5, p0, Lcom/google/android/music/tutorial/GetAccountOffersTask$1;->this$0:Lcom/google/android/music/tutorial/GetAccountOffersTask;

    # getter for: Lcom/google/android/music/tutorial/GetAccountOffersTask;->mIsCanceled:Z
    invoke-static {v5}, Lcom/google/android/music/tutorial/GetAccountOffersTask;->access$000(Lcom/google/android/music/tutorial/GetAccountOffersTask;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 118
    iget-object v5, p0, Lcom/google/android/music/tutorial/GetAccountOffersTask$1;->this$0:Lcom/google/android/music/tutorial/GetAccountOffersTask;

    # getter for: Lcom/google/android/music/tutorial/GetAccountOffersTask;->mCallbackHandler:Landroid/os/Handler;
    invoke-static {v5}, Lcom/google/android/music/tutorial/GetAccountOffersTask;->access$300(Lcom/google/android/music/tutorial/GetAccountOffersTask;)Landroid/os/Handler;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/music/tutorial/GetAccountOffersTask$1;->this$0:Lcom/google/android/music/tutorial/GetAccountOffersTask;

    # getter for: Lcom/google/android/music/tutorial/GetAccountOffersTask;->mCallbackRunnable:Ljava/lang/Runnable;
    invoke-static {v6}, Lcom/google/android/music/tutorial/GetAccountOffersTask;->access$800(Lcom/google/android/music/tutorial/GetAccountOffersTask;)Ljava/lang/Runnable;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_0

    .line 64
    :cond_4
    :try_start_1
    iget-object v5, p0, Lcom/google/android/music/tutorial/GetAccountOffersTask$1;->this$0:Lcom/google/android/music/tutorial/GetAccountOffersTask;

    iget-object v6, p0, Lcom/google/android/music/tutorial/GetAccountOffersTask$1;->this$0:Lcom/google/android/music/tutorial/GetAccountOffersTask;

    # getter for: Lcom/google/android/music/tutorial/GetAccountOffersTask;->mAccount:Landroid/accounts/Account;
    invoke-static {v6}, Lcom/google/android/music/tutorial/GetAccountOffersTask;->access$600(Lcom/google/android/music/tutorial/GetAccountOffersTask;)Landroid/accounts/Account;

    move-result-object v6

    invoke-interface {v0, v6}, Lcom/google/android/music/cloudclient/MusicCloud;->getOffersForAccount(Landroid/accounts/Account;)Lcom/google/android/music/cloudclient/OffersResponseJson;

    move-result-object v6

    # setter for: Lcom/google/android/music/tutorial/GetAccountOffersTask;->mResponse:Lcom/google/android/music/cloudclient/OffersResponseJson;
    invoke-static {v5, v6}, Lcom/google/android/music/tutorial/GetAccountOffersTask;->access$402(Lcom/google/android/music/tutorial/GetAccountOffersTask;Lcom/google/android/music/cloudclient/OffersResponseJson;)Lcom/google/android/music/cloudclient/OffersResponseJson;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 66
    :catch_0
    move-exception v3

    .line 67
    .local v3, "e":Ljava/io/IOException;
    const-string v5, "AccountOffers"

    const-string v6, "getOffersFailed"

    invoke-static {v5, v6, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 68
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "IOException: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v3}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 72
    goto/16 :goto_1

    .line 69
    .end local v3    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v3

    .line 70
    .local v3, "e":Ljava/lang/Exception;
    const-string v5, "AccountOffers"

    const-string v6, "getOffersFailed"

    invoke-static {v5, v6, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 71
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Exception: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v3}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_1

    .line 86
    .end local v3    # "e":Ljava/lang/Exception;
    .restart local v1    # "couponStatusText":Ljava/lang/String;
    :pswitch_0
    iget-object v5, p0, Lcom/google/android/music/tutorial/GetAccountOffersTask$1;->this$0:Lcom/google/android/music/tutorial/GetAccountOffersTask;

    # getter for: Lcom/google/android/music/tutorial/GetAccountOffersTask;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/google/android/music/tutorial/GetAccountOffersTask;->access$100(Lcom/google/android/music/tutorial/GetAccountOffersTask;)Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b0315

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 88
    goto/16 :goto_2

    .line 90
    :pswitch_1
    iget-object v5, p0, Lcom/google/android/music/tutorial/GetAccountOffersTask$1;->this$0:Lcom/google/android/music/tutorial/GetAccountOffersTask;

    # getter for: Lcom/google/android/music/tutorial/GetAccountOffersTask;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/google/android/music/tutorial/GetAccountOffersTask;->access$100(Lcom/google/android/music/tutorial/GetAccountOffersTask;)Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b0316

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 92
    goto/16 :goto_2

    .line 94
    :pswitch_2
    iget-object v5, p0, Lcom/google/android/music/tutorial/GetAccountOffersTask$1;->this$0:Lcom/google/android/music/tutorial/GetAccountOffersTask;

    # getter for: Lcom/google/android/music/tutorial/GetAccountOffersTask;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/google/android/music/tutorial/GetAccountOffersTask;->access$100(Lcom/google/android/music/tutorial/GetAccountOffersTask;)Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b0317

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 96
    goto/16 :goto_2

    .line 98
    :pswitch_3
    iget-object v5, p0, Lcom/google/android/music/tutorial/GetAccountOffersTask$1;->this$0:Lcom/google/android/music/tutorial/GetAccountOffersTask;

    # getter for: Lcom/google/android/music/tutorial/GetAccountOffersTask;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/google/android/music/tutorial/GetAccountOffersTask;->access$100(Lcom/google/android/music/tutorial/GetAccountOffersTask;)Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b0318

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 100
    goto/16 :goto_2

    .line 84
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method
