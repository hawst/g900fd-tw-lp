.class public abstract Lcom/google/android/music/download/BaseDownloadProgress;
.super Ljava/lang/Object;
.source "BaseDownloadProgress.java"

# interfaces
.implements Lcom/google/android/music/download/DownloadProgress;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<DownloadRequestType::",
        "Lcom/google/android/music/download/DownloadRequest;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/music/download/DownloadProgress;"
    }
.end annotation


# instance fields
.field private final mCompletedBytes:J

.field private final mDownloadByteLength:J

.field private final mError:I

.field private final mHttpContentType:Ljava/lang/String;

.field private final mId:Lcom/google/android/music/download/ContentIdentifier;

.field private final mIsFullCopy:Z

.field private final mIsSavable:Z

.field private final mOwner:Lcom/google/android/music/download/DownloadRequest$Owner;

.field private final mPriority:I

.field private final mState:Lcom/google/android/music/download/DownloadState$State;


# direct methods
.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 6
    .param p1, "p"    # Landroid/os/Parcel;

    .prologue
    .local p0, "this":Lcom/google/android/music/download/BaseDownloadProgress;, "Lcom/google/android/music/download/BaseDownloadProgress<TDownloadRequestType;>;"
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    const-class v0, Lcom/google/android/music/download/ContentIdentifier;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/download/ContentIdentifier;

    iput-object v0, p0, Lcom/google/android/music/download/BaseDownloadProgress;->mId:Lcom/google/android/music/download/ContentIdentifier;

    .line 48
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/music/download/BaseDownloadProgress;->mPriority:I

    .line 49
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/music/download/BaseDownloadProgress;->getOwnerFromInt(I)Lcom/google/android/music/download/DownloadRequest$Owner;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/download/BaseDownloadProgress;->mOwner:Lcom/google/android/music/download/DownloadRequest$Owner;

    .line 50
    invoke-static {}, Lcom/google/android/music/download/DownloadState$State;->values()[Lcom/google/android/music/download/DownloadState$State;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    aget-object v0, v0, v3

    iput-object v0, p0, Lcom/google/android/music/download/BaseDownloadProgress;->mState:Lcom/google/android/music/download/DownloadState$State;

    .line 51
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/music/download/BaseDownloadProgress;->mCompletedBytes:J

    .line 52
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/music/download/BaseDownloadProgress;->mDownloadByteLength:J

    .line 53
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/music/download/BaseDownloadProgress;->mError:I

    .line 54
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/download/BaseDownloadProgress;->mHttpContentType:Ljava/lang/String;

    .line 55
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/music/download/BaseDownloadProgress;->mIsSavable:Z

    .line 56
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_1

    :goto_1
    iput-boolean v1, p0, Lcom/google/android/music/download/BaseDownloadProgress;->mIsFullCopy:Z

    .line 57
    return-void

    :cond_0
    move v0, v2

    .line 55
    goto :goto_0

    :cond_1
    move v1, v2

    .line 56
    goto :goto_1
.end method

.method protected constructor <init>(Lcom/google/android/music/download/DownloadRequest;Lcom/google/android/music/download/DownloadState;)V
    .locals 2
    .param p2, "downloadState"    # Lcom/google/android/music/download/DownloadState;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TDownloadRequestType;",
            "Lcom/google/android/music/download/DownloadState;",
            ")V"
        }
    .end annotation

    .prologue
    .line 33
    .local p0, "this":Lcom/google/android/music/download/BaseDownloadProgress;, "Lcom/google/android/music/download/BaseDownloadProgress<TDownloadRequestType;>;"
    .local p1, "downloadRequest":Lcom/google/android/music/download/DownloadRequest;, "TDownloadRequestType;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    invoke-interface {p1}, Lcom/google/android/music/download/DownloadRequest;->getId()Lcom/google/android/music/download/ContentIdentifier;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/download/BaseDownloadProgress;->mId:Lcom/google/android/music/download/ContentIdentifier;

    .line 35
    invoke-interface {p1}, Lcom/google/android/music/download/DownloadRequest;->getPriority()I

    move-result v0

    iput v0, p0, Lcom/google/android/music/download/BaseDownloadProgress;->mPriority:I

    .line 36
    invoke-interface {p1}, Lcom/google/android/music/download/DownloadRequest;->getOwner()Lcom/google/android/music/download/DownloadRequest$Owner;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/download/BaseDownloadProgress;->mOwner:Lcom/google/android/music/download/DownloadRequest$Owner;

    .line 37
    invoke-virtual {p2}, Lcom/google/android/music/download/DownloadState;->getState()Lcom/google/android/music/download/DownloadState$State;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/download/BaseDownloadProgress;->mState:Lcom/google/android/music/download/DownloadState$State;

    .line 38
    invoke-virtual {p2}, Lcom/google/android/music/download/DownloadState;->getCompletedBytes()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/music/download/BaseDownloadProgress;->mCompletedBytes:J

    .line 39
    invoke-virtual {p2}, Lcom/google/android/music/download/DownloadState;->getDownloadByteLength()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/music/download/BaseDownloadProgress;->mDownloadByteLength:J

    .line 40
    invoke-virtual {p2}, Lcom/google/android/music/download/DownloadState;->getError()I

    move-result v0

    iput v0, p0, Lcom/google/android/music/download/BaseDownloadProgress;->mError:I

    .line 41
    invoke-virtual {p2}, Lcom/google/android/music/download/DownloadState;->getHttpContentType()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/download/BaseDownloadProgress;->mHttpContentType:Ljava/lang/String;

    .line 42
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/download/BaseDownloadProgress;->checkSavable(Lcom/google/android/music/download/DownloadRequest;Lcom/google/android/music/download/DownloadState;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/music/download/BaseDownloadProgress;->mIsSavable:Z

    .line 43
    invoke-virtual {p0, p1, p2}, Lcom/google/android/music/download/BaseDownloadProgress;->checkFullCopy(Lcom/google/android/music/download/DownloadRequest;Lcom/google/android/music/download/DownloadState;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/music/download/BaseDownloadProgress;->mIsFullCopy:Z

    .line 44
    return-void
.end method

.method private checkSavable(Lcom/google/android/music/download/DownloadRequest;Lcom/google/android/music/download/DownloadState;)Z
    .locals 2
    .param p2, "downloadState"    # Lcom/google/android/music/download/DownloadState;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TDownloadRequestType;",
            "Lcom/google/android/music/download/DownloadState;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 127
    .local p0, "this":Lcom/google/android/music/download/BaseDownloadProgress;, "Lcom/google/android/music/download/BaseDownloadProgress<TDownloadRequestType;>;"
    .local p1, "downloadRequest":Lcom/google/android/music/download/DownloadRequest;, "TDownloadRequestType;"
    invoke-interface {p1}, Lcom/google/android/music/download/DownloadRequest;->getFileLocation()Lcom/google/android/music/download/cache/FileLocation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/download/cache/FileLocation;->getCacheType()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 128
    const/4 v0, 0x0

    .line 130
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/google/android/music/download/BaseDownloadProgress;->checkFullCopy(Lcom/google/android/music/download/DownloadRequest;Lcom/google/android/music/download/DownloadState;)Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method protected checkFullCopy(Lcom/google/android/music/download/DownloadRequest;Lcom/google/android/music/download/DownloadState;)Z
    .locals 8
    .param p2, "downloadState"    # Lcom/google/android/music/download/DownloadState;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TDownloadRequestType;",
            "Lcom/google/android/music/download/DownloadState;",
            ")Z"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/google/android/music/download/BaseDownloadProgress;, "Lcom/google/android/music/download/BaseDownloadProgress<TDownloadRequestType;>;"
    .local p1, "downloadRequest":Lcom/google/android/music/download/DownloadRequest;, "TDownloadRequestType;"
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 141
    invoke-virtual {p2}, Lcom/google/android/music/download/DownloadState;->getState()Lcom/google/android/music/download/DownloadState$State;

    move-result-object v3

    sget-object v4, Lcom/google/android/music/download/DownloadState$State;->COMPLETED:Lcom/google/android/music/download/DownloadState$State;

    if-ne v3, v4, :cond_1

    invoke-virtual {p2}, Lcom/google/android/music/download/DownloadState;->isExperiencedGlitch()Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {p2}, Lcom/google/android/music/download/DownloadState;->getCompletedBytes()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-eqz v3, :cond_1

    invoke-interface {p1}, Lcom/google/android/music/download/DownloadRequest;->getFileLocation()Lcom/google/android/music/download/cache/FileLocation;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/music/download/cache/FileLocation;->getFullPath()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v4

    invoke-virtual {p2}, Lcom/google/android/music/download/DownloadState;->getCompletedFileSize()J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-nez v3, :cond_1

    move v0, v1

    .line 150
    .local v0, "res":Z
    :goto_0
    if-eqz v0, :cond_0

    .line 151
    invoke-virtual {p2}, Lcom/google/android/music/download/DownloadState;->isDownloadByteLengthEstimated()Z

    move-result v3

    if-nez v3, :cond_0

    .line 152
    invoke-virtual {p2}, Lcom/google/android/music/download/DownloadState;->getDownloadByteLength()J

    move-result-wide v4

    invoke-virtual {p2}, Lcom/google/android/music/download/DownloadState;->getCompletedBytes()J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-nez v3, :cond_2

    move v0, v1

    .line 155
    :cond_0
    :goto_1
    return v0

    .end local v0    # "res":Z
    :cond_1
    move v0, v2

    .line 141
    goto :goto_0

    .restart local v0    # "res":Z
    :cond_2
    move v0, v2

    .line 152
    goto :goto_1
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 177
    .local p0, "this":Lcom/google/android/music/download/BaseDownloadProgress;, "Lcom/google/android/music/download/BaseDownloadProgress<TDownloadRequestType;>;"
    const/4 v0, 0x0

    return v0
.end method

.method public getCompletedBytes()J
    .locals 2

    .prologue
    .line 94
    .local p0, "this":Lcom/google/android/music/download/BaseDownloadProgress;, "Lcom/google/android/music/download/BaseDownloadProgress<TDownloadRequestType;>;"
    iget-wide v0, p0, Lcom/google/android/music/download/BaseDownloadProgress;->mCompletedBytes:J

    return-wide v0
.end method

.method public getDownloadByteLength()J
    .locals 2

    .prologue
    .line 101
    .local p0, "this":Lcom/google/android/music/download/BaseDownloadProgress;, "Lcom/google/android/music/download/BaseDownloadProgress<TDownloadRequestType;>;"
    iget-wide v0, p0, Lcom/google/android/music/download/BaseDownloadProgress;->mDownloadByteLength:J

    return-wide v0
.end method

.method public getError()I
    .locals 1

    .prologue
    .line 108
    .local p0, "this":Lcom/google/android/music/download/BaseDownloadProgress;, "Lcom/google/android/music/download/BaseDownloadProgress<TDownloadRequestType;>;"
    iget v0, p0, Lcom/google/android/music/download/BaseDownloadProgress;->mError:I

    return v0
.end method

.method public getHttpContentType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 115
    .local p0, "this":Lcom/google/android/music/download/BaseDownloadProgress;, "Lcom/google/android/music/download/BaseDownloadProgress<TDownloadRequestType;>;"
    iget-object v0, p0, Lcom/google/android/music/download/BaseDownloadProgress;->mHttpContentType:Ljava/lang/String;

    return-object v0
.end method

.method public getId()Lcom/google/android/music/download/ContentIdentifier;
    .locals 1

    .prologue
    .line 65
    .local p0, "this":Lcom/google/android/music/download/BaseDownloadProgress;, "Lcom/google/android/music/download/BaseDownloadProgress<TDownloadRequestType;>;"
    iget-object v0, p0, Lcom/google/android/music/download/BaseDownloadProgress;->mId:Lcom/google/android/music/download/ContentIdentifier;

    return-object v0
.end method

.method public getOwner()Lcom/google/android/music/download/DownloadRequest$Owner;
    .locals 1

    .prologue
    .line 80
    .local p0, "this":Lcom/google/android/music/download/BaseDownloadProgress;, "Lcom/google/android/music/download/BaseDownloadProgress<TDownloadRequestType;>;"
    iget-object v0, p0, Lcom/google/android/music/download/BaseDownloadProgress;->mOwner:Lcom/google/android/music/download/DownloadRequest$Owner;

    return-object v0
.end method

.method protected abstract getOwnerFromInt(I)Lcom/google/android/music/download/DownloadRequest$Owner;
.end method

.method public getState()Lcom/google/android/music/download/DownloadState$State;
    .locals 1

    .prologue
    .line 87
    .local p0, "this":Lcom/google/android/music/download/BaseDownloadProgress;, "Lcom/google/android/music/download/BaseDownloadProgress<TDownloadRequestType;>;"
    iget-object v0, p0, Lcom/google/android/music/download/BaseDownloadProgress;->mState:Lcom/google/android/music/download/DownloadState$State;

    return-object v0
.end method

.method public isFullCopy()Z
    .locals 1

    .prologue
    .line 137
    .local p0, "this":Lcom/google/android/music/download/BaseDownloadProgress;, "Lcom/google/android/music/download/BaseDownloadProgress<TDownloadRequestType;>;"
    iget-boolean v0, p0, Lcom/google/android/music/download/BaseDownloadProgress;->mIsFullCopy:Z

    return v0
.end method

.method public isSavable()Z
    .locals 1

    .prologue
    .line 122
    .local p0, "this":Lcom/google/android/music/download/BaseDownloadProgress;, "Lcom/google/android/music/download/BaseDownloadProgress<TDownloadRequestType;>;"
    iget-boolean v0, p0, Lcom/google/android/music/download/BaseDownloadProgress;->mIsSavable:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 160
    .local p0, "this":Lcom/google/android/music/download/BaseDownloadProgress;, "Lcom/google/android/music/download/BaseDownloadProgress<TDownloadRequestType;>;"
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 161
    .local v0, "builder":Ljava/lang/StringBuilder;
    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 162
    const-string v1, "mId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/download/BaseDownloadProgress;->mId:Lcom/google/android/music/download/ContentIdentifier;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 163
    const-string v1, " mPriority="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/music/download/BaseDownloadProgress;->mPriority:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 164
    const-string v1, " mOwner="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/download/BaseDownloadProgress;->mOwner:Lcom/google/android/music/download/DownloadRequest$Owner;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 165
    const-string v1, " mState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/download/BaseDownloadProgress;->mState:Lcom/google/android/music/download/DownloadState$State;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 166
    const-string v1, " mCompletedBytes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/music/download/BaseDownloadProgress;->mCompletedBytes:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 167
    const-string v1, " mDownloadByteLength="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/music/download/BaseDownloadProgress;->mDownloadByteLength:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 168
    const-string v1, " mError="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/music/download/BaseDownloadProgress;->mError:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 169
    const-string v1, " mHttpContentType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/download/BaseDownloadProgress;->mHttpContentType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 170
    const-string v1, " mIsSavable="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/music/download/BaseDownloadProgress;->mIsSavable:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 171
    const-string v1, " mIsFullCopy="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/music/download/BaseDownloadProgress;->mIsFullCopy:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 172
    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 173
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 6
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .local p0, "this":Lcom/google/android/music/download/BaseDownloadProgress;, "Lcom/google/android/music/download/BaseDownloadProgress<TDownloadRequestType;>;"
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 181
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/android/music/utils/ParcelUtils;->prepareParcel(Landroid/os/Parcel;Ljava/lang/Class;)V

    .line 183
    iget-object v0, p0, Lcom/google/android/music/download/BaseDownloadProgress;->mId:Lcom/google/android/music/download/ContentIdentifier;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 184
    iget v0, p0, Lcom/google/android/music/download/BaseDownloadProgress;->mPriority:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 185
    iget-object v0, p0, Lcom/google/android/music/download/BaseDownloadProgress;->mOwner:Lcom/google/android/music/download/DownloadRequest$Owner;

    invoke-interface {v0}, Lcom/google/android/music/download/DownloadRequest$Owner;->toInt()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 186
    iget-object v0, p0, Lcom/google/android/music/download/BaseDownloadProgress;->mState:Lcom/google/android/music/download/DownloadState$State;

    invoke-virtual {v0}, Lcom/google/android/music/download/DownloadState$State;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 187
    iget-wide v4, p0, Lcom/google/android/music/download/BaseDownloadProgress;->mCompletedBytes:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 188
    iget-wide v4, p0, Lcom/google/android/music/download/BaseDownloadProgress;->mDownloadByteLength:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 189
    iget v0, p0, Lcom/google/android/music/download/BaseDownloadProgress;->mError:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 190
    iget-object v0, p0, Lcom/google/android/music/download/BaseDownloadProgress;->mHttpContentType:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 191
    iget-boolean v0, p0, Lcom/google/android/music/download/BaseDownloadProgress;->mIsSavable:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 192
    iget-boolean v0, p0, Lcom/google/android/music/download/BaseDownloadProgress;->mIsFullCopy:Z

    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 193
    return-void

    :cond_0
    move v0, v2

    .line 191
    goto :goto_0

    :cond_1
    move v1, v2

    .line 192
    goto :goto_1
.end method
