.class public Lcom/google/android/music/ui/RecommendedExploreFragment;
.super Lcom/google/android/music/ui/ExploreClusterListFragment;
.source "RecommendedExploreFragment.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 11
    invoke-static {}, Lcom/google/android/music/store/MusicContent$Explore;->getRecommendedGroupsUri()Landroid/net/Uri;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/music/ui/ExploreClusterListFragment;-><init>(Landroid/net/Uri;)V

    .line 12
    return-void
.end method


# virtual methods
.method protected getGroupQueryUri(J)Landroid/net/Uri;
    .locals 1
    .param p1, "groupId"    # J

    .prologue
    .line 16
    invoke-static {p1, p2}, Lcom/google/android/music/store/MusicContent$Explore;->getRecommendedUri(J)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method
