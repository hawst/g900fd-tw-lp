.class public Lcom/google/android/music/cloudclient/CloudQueueRadioReferenceJson;
.super Lcom/google/api/client/json/GenericJson;
.source "CloudQueueRadioReferenceJson.java"


# instance fields
.field public mId:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "id"
    .end annotation
.end field

.field public mSeeds:Ljava/util/List;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "seeds"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/cloudclient/CloudQueueRadioSeedJson;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    return-void
.end method
