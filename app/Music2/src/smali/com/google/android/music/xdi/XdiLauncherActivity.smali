.class public Lcom/google/android/music/xdi/XdiLauncherActivity;
.super Landroid/app/Activity;
.source "XdiLauncherActivity.java"


# static fields
.field private static final LOGV:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->XDI:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/xdi/XdiLauncherActivity;->LOGV:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 20
    sget-boolean v3, Lcom/google/android/music/xdi/XdiLauncherActivity;->LOGV:Z

    if-eqz v3, :cond_0

    .line 21
    const-string v3, "MusicXdi"

    const-string v4, "onCreate"

    invoke-static {v3, v4}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 24
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 32
    invoke-static {}, Lcom/google/android/music/utils/MusicUtils;->isPlaying()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 33
    const v1, 0x7f0b0095

    .line 37
    .local v1, "nameResIdToSelect":I
    :goto_0
    invoke-virtual {p0, v1}, Lcom/google/android/music/xdi/XdiLauncherActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 38
    .local v2, "nameToSelect":Ljava/lang/String;
    sget-boolean v3, Lcom/google/android/music/xdi/XdiLauncherActivity;->LOGV:Z

    if-eqz v3, :cond_1

    .line 39
    const-string v3, "MusicXdi"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Selecting header "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    :cond_1
    sget-object v3, Lcom/google/android/music/xdi/XdiContentProvider;->BASE_URI:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "browse"

    invoke-virtual {v3, v4}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    invoke-static {v3, v2}, Lcom/google/android/music/xdi/XdiContract;->getBrowseIntentByName(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 46
    .local v0, "intent":Landroid/content/Intent;
    const-string v3, "meta_uri"

    const-wide/16 v4, 0x0

    invoke-static {v4, v5}, Lcom/google/android/music/xdi/XdiUtils;->getMetaUri(J)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 48
    const/high16 v3, 0x10000000

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 50
    invoke-virtual {p0, v0}, Lcom/google/android/music/xdi/XdiLauncherActivity;->startActivity(Landroid/content/Intent;)V

    .line 51
    invoke-virtual {p0}, Lcom/google/android/music/xdi/XdiLauncherActivity;->finish()V

    .line 52
    return-void

    .line 35
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v1    # "nameResIdToSelect":I
    .end local v2    # "nameToSelect":Ljava/lang/String;
    :cond_2
    const v1, 0x7f0b0096

    .restart local v1    # "nameResIdToSelect":I
    goto :goto_0
.end method
