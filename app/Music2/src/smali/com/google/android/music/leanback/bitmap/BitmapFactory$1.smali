.class final Lcom/google/android/music/leanback/bitmap/BitmapFactory$1;
.super Ljava/lang/Object;
.source "BitmapFactory.java"

# interfaces
.implements Lcom/google/android/music/utils/async/AsyncRunner;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/leanback/bitmap/BitmapFactory;->getBitmap(Landroid/content/Context;Lcom/google/android/music/leanback/bitmap/BitmapGettersGetter;IIZZLcom/google/android/music/leanback/bitmap/BitmapListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field mBitmap:Landroid/graphics/Bitmap;

.field mRecoverableError:Z

.field final synthetic val$bitmapGettersGetter:Lcom/google/android/music/leanback/bitmap/BitmapGettersGetter;

.field final synthetic val$bitmapListener:Lcom/google/android/music/leanback/bitmap/BitmapListener;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$cropToSquare:Z

.field final synthetic val$retryBitmapListener:Lcom/google/android/music/leanback/bitmap/RetryBitmapListener;

.field final synthetic val$targetHeight:I

.field final synthetic val$targetWidth:I

.field final synthetic val$withFiltering:Z


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/google/android/music/leanback/bitmap/BitmapGettersGetter;IIZZLcom/google/android/music/leanback/bitmap/BitmapListener;Lcom/google/android/music/leanback/bitmap/RetryBitmapListener;)V
    .locals 1

    .prologue
    .line 91
    iput-object p1, p0, Lcom/google/android/music/leanback/bitmap/BitmapFactory$1;->val$context:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/music/leanback/bitmap/BitmapFactory$1;->val$bitmapGettersGetter:Lcom/google/android/music/leanback/bitmap/BitmapGettersGetter;

    iput p3, p0, Lcom/google/android/music/leanback/bitmap/BitmapFactory$1;->val$targetWidth:I

    iput p4, p0, Lcom/google/android/music/leanback/bitmap/BitmapFactory$1;->val$targetHeight:I

    iput-boolean p5, p0, Lcom/google/android/music/leanback/bitmap/BitmapFactory$1;->val$cropToSquare:Z

    iput-boolean p6, p0, Lcom/google/android/music/leanback/bitmap/BitmapFactory$1;->val$withFiltering:Z

    iput-object p7, p0, Lcom/google/android/music/leanback/bitmap/BitmapFactory$1;->val$bitmapListener:Lcom/google/android/music/leanback/bitmap/BitmapListener;

    iput-object p8, p0, Lcom/google/android/music/leanback/bitmap/BitmapFactory$1;->val$retryBitmapListener:Lcom/google/android/music/leanback/bitmap/RetryBitmapListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 92
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/leanback/bitmap/BitmapFactory$1;->mRecoverableError:Z

    return-void
.end method


# virtual methods
.method public backgroundTask()V
    .locals 7

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/android/music/leanback/bitmap/BitmapFactory$1;->val$context:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/music/leanback/bitmap/BitmapFactory$1;->val$bitmapGettersGetter:Lcom/google/android/music/leanback/bitmap/BitmapGettersGetter;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/music/leanback/bitmap/BitmapFactory$1;->val$bitmapGettersGetter:Lcom/google/android/music/leanback/bitmap/BitmapGettersGetter;

    invoke-interface {v1}, Lcom/google/android/music/leanback/bitmap/BitmapGettersGetter;->getBitmapGetters()[Lcom/google/android/music/leanback/bitmap/BitmapGetter;

    move-result-object v1

    :goto_0
    iget v2, p0, Lcom/google/android/music/leanback/bitmap/BitmapFactory$1;->val$targetWidth:I

    iget v3, p0, Lcom/google/android/music/leanback/bitmap/BitmapFactory$1;->val$targetHeight:I

    iget-boolean v4, p0, Lcom/google/android/music/leanback/bitmap/BitmapFactory$1;->val$cropToSquare:Z

    iget-boolean v5, p0, Lcom/google/android/music/leanback/bitmap/BitmapFactory$1;->val$withFiltering:Z

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/leanback/bitmap/BitmapFactory;->getBitmap(Landroid/content/Context;[Lcom/google/android/music/leanback/bitmap/BitmapGetter;IIZZ)Landroid/util/Pair;

    move-result-object v6

    .line 100
    .local v6, "results":Landroid/util/Pair;, "Landroid/util/Pair<Landroid/graphics/Bitmap;Ljava/lang/Boolean;>;"
    iget-object v0, v6, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/google/android/music/leanback/bitmap/BitmapFactory$1;->mBitmap:Landroid/graphics/Bitmap;

    .line 101
    iget-object v0, v6, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/music/leanback/bitmap/BitmapFactory$1;->mRecoverableError:Z

    .line 102
    return-void

    .line 97
    .end local v6    # "results":Landroid/util/Pair;, "Landroid/util/Pair<Landroid/graphics/Bitmap;Ljava/lang/Boolean;>;"
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public taskCompleted()V
    .locals 2

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/music/leanback/bitmap/BitmapFactory$1;->mBitmap:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/music/leanback/bitmap/BitmapFactory$1;->mRecoverableError:Z

    if-nez v0, :cond_0

    .line 107
    iget-object v0, p0, Lcom/google/android/music/leanback/bitmap/BitmapFactory$1;->val$bitmapListener:Lcom/google/android/music/leanback/bitmap/BitmapListener;

    iget-object v1, p0, Lcom/google/android/music/leanback/bitmap/BitmapFactory$1;->mBitmap:Landroid/graphics/Bitmap;

    invoke-interface {v0, v1}, Lcom/google/android/music/leanback/bitmap/BitmapListener;->onBitmapLoaded(Landroid/graphics/Bitmap;)V

    .line 111
    :goto_0
    return-void

    .line 109
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/leanback/bitmap/BitmapFactory$1;->val$retryBitmapListener:Lcom/google/android/music/leanback/bitmap/RetryBitmapListener;

    iget-object v1, p0, Lcom/google/android/music/leanback/bitmap/BitmapFactory$1;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Lcom/google/android/music/leanback/bitmap/RetryBitmapListener;->onBitmapLoaded(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method
