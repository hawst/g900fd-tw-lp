.class Lcom/google/android/music/ui/BaseActivity$RouteSelectionCallback;
.super Landroid/support/v7/media/MediaRouter$Callback;
.source "BaseActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/BaseActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RouteSelectionCallback"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/BaseActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/music/ui/BaseActivity;)V
    .locals 0

    .prologue
    .line 278
    iput-object p1, p0, Lcom/google/android/music/ui/BaseActivity$RouteSelectionCallback;->this$0:Lcom/google/android/music/ui/BaseActivity;

    invoke-direct {p0}, Landroid/support/v7/media/MediaRouter$Callback;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/music/ui/BaseActivity;Lcom/google/android/music/ui/BaseActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/music/ui/BaseActivity;
    .param p2, "x1"    # Lcom/google/android/music/ui/BaseActivity$1;

    .prologue
    .line 278
    invoke-direct {p0, p1}, Lcom/google/android/music/ui/BaseActivity$RouteSelectionCallback;-><init>(Lcom/google/android/music/ui/BaseActivity;)V

    return-void
.end method


# virtual methods
.method public onRouteSelected(Landroid/support/v7/media/MediaRouter;Landroid/support/v7/media/MediaRouter$RouteInfo;)V
    .locals 2
    .param p1, "router"    # Landroid/support/v7/media/MediaRouter;
    .param p2, "routeInfo"    # Landroid/support/v7/media/MediaRouter$RouteInfo;

    .prologue
    .line 282
    iget-object v0, p0, Lcom/google/android/music/ui/BaseActivity$RouteSelectionCallback;->this$0:Lcom/google/android/music/ui/BaseActivity;

    invoke-static {v0, p2}, Lcom/google/android/music/cast/CastUtils;->isRemoteControlIntentRoute(Landroid/content/Context;Landroid/support/v7/media/MediaRouter$RouteInfo;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 283
    iget-object v0, p0, Lcom/google/android/music/ui/BaseActivity$RouteSelectionCallback;->this$0:Lcom/google/android/music/ui/BaseActivity;

    const/high16 v1, -0x80000000

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/BaseActivity;->setVolumeControlStream(I)V

    .line 287
    :goto_0
    return-void

    .line 285
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/ui/BaseActivity$RouteSelectionCallback;->this$0:Lcom/google/android/music/ui/BaseActivity;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/BaseActivity;->setVolumeControlStream(I)V

    goto :goto_0
.end method
