.class Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail$1;
.super Ljava/lang/Object;
.source "PlayCardThumbnail.java"

# interfaces
.implements Lcom/google/android/music/utils/async/AsyncRunner;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;->setRadioArt(Lcom/google/android/music/ui/cardlib/model/Document;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field mPlaylistId:J

.field final synthetic this$0:Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;

.field final synthetic val$doc:Lcom/google/android/music/ui/cardlib/model/Document;

.field final synthetic val$seedSourceId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;Ljava/lang/String;Lcom/google/android/music/ui/cardlib/model/Document;)V
    .locals 2

    .prologue
    .line 165
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail$1;->this$0:Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;

    iput-object p2, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail$1;->val$seedSourceId:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail$1;->val$doc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 166
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail$1;->mPlaylistId:J

    return-void
.end method


# virtual methods
.method public backgroundTask()V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 169
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "playlist_id"

    aput-object v0, v2, v3

    .line 173
    .local v2, "listCols":[Ljava/lang/String;
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail$1;->val$seedSourceId:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/music/store/MusicContent$Playlists;->getPlaylistUriByRemoteSourceId(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 175
    .local v1, "listUri":Landroid/net/Uri;
    const/4 v6, 0x0

    .line 177
    .local v6, "c":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail$1;->this$0:Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;

    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 178
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 179
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail$1;->mPlaylistId:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 182
    :cond_0
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 184
    return-void

    .line 182
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method public taskCompleted()V
    .locals 5

    .prologue
    .line 191
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail$1;->this$0:Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;

    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;->getContext()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail$1;->this$0:Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;

    # getter for: Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;->mThumbnail:Lcom/google/android/music/AsyncAlbumArtImageView;
    invoke-static {v0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;->access$000(Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;)Lcom/google/android/music/AsyncAlbumArtImageView;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail$1;->mPlaylistId:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail$1;->val$seedSourceId:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail$1;->val$doc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v1}, Lcom/google/android/music/ui/cardlib/model/Document;->getRadioSeedId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 193
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail$1;->this$0:Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;

    # getter for: Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;->mThumbnail:Lcom/google/android/music/AsyncAlbumArtImageView;
    invoke-static {v0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;->access$000(Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;)Lcom/google/android/music/AsyncAlbumArtImageView;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail$1;->mPlaylistId:J

    iget-object v1, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail$1;->val$doc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v1}, Lcom/google/android/music/ui/cardlib/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v1

    const/16 v4, 0x3c

    invoke-virtual {v0, v2, v3, v1, v4}, Lcom/google/android/music/AsyncAlbumArtImageView;->setPlaylistAlbumArt(JLjava/lang/String;I)V

    .line 196
    :cond_0
    return-void
.end method
