.class Lcom/google/android/music/ui/HomeMenuScreens$PlayDrawerScreenShimRunnable;
.super Ljava/lang/Object;
.source "HomeMenuScreens.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/HomeMenuScreens;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "PlayDrawerScreenShimRunnable"
.end annotation


# instance fields
.field private final mBaseActivity:Lcom/google/android/music/ui/BaseActivity;

.field private final mTargetScreen:Lcom/google/android/music/ui/HomeActivity$Screen;


# direct methods
.method private constructor <init>(Lcom/google/android/music/ui/HomeActivity$Screen;Lcom/google/android/music/ui/BaseActivity;)V
    .locals 0
    .param p1, "targetScreen"    # Lcom/google/android/music/ui/HomeActivity$Screen;
    .param p2, "baseActivity"    # Lcom/google/android/music/ui/BaseActivity;

    .prologue
    .line 145
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 146
    iput-object p1, p0, Lcom/google/android/music/ui/HomeMenuScreens$PlayDrawerScreenShimRunnable;->mTargetScreen:Lcom/google/android/music/ui/HomeActivity$Screen;

    .line 147
    iput-object p2, p0, Lcom/google/android/music/ui/HomeMenuScreens$PlayDrawerScreenShimRunnable;->mBaseActivity:Lcom/google/android/music/ui/BaseActivity;

    .line 148
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/music/ui/HomeActivity$Screen;Lcom/google/android/music/ui/BaseActivity;Lcom/google/android/music/ui/HomeMenuScreens$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/music/ui/HomeActivity$Screen;
    .param p2, "x1"    # Lcom/google/android/music/ui/BaseActivity;
    .param p3, "x2"    # Lcom/google/android/music/ui/HomeMenuScreens$1;

    .prologue
    .line 140
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/ui/HomeMenuScreens$PlayDrawerScreenShimRunnable;-><init>(Lcom/google/android/music/ui/HomeActivity$Screen;Lcom/google/android/music/ui/BaseActivity;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 152
    iget-object v3, p0, Lcom/google/android/music/ui/HomeMenuScreens$PlayDrawerScreenShimRunnable;->mBaseActivity:Lcom/google/android/music/ui/BaseActivity;

    invoke-virtual {v3}, Lcom/google/android/music/ui/BaseActivity;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/music/preferences/MusicPreferences;->getDisplayOptions()I

    move-result v3

    if-eqz v3, :cond_1

    const/4 v0, 0x1

    .line 155
    .local v0, "downloadOnly":Z
    :goto_0
    if-eqz v0, :cond_0

    iget-object v3, p0, Lcom/google/android/music/ui/HomeMenuScreens$PlayDrawerScreenShimRunnable;->mTargetScreen:Lcom/google/android/music/ui/HomeActivity$Screen;

    invoke-virtual {v3}, Lcom/google/android/music/ui/HomeActivity$Screen;->isInDownloadOnly()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 156
    :cond_0
    iget-object v2, p0, Lcom/google/android/music/ui/HomeMenuScreens$PlayDrawerScreenShimRunnable;->mBaseActivity:Lcom/google/android/music/ui/BaseActivity;

    invoke-static {v2}, Lcom/google/android/music/eventlog/MusicEventLogger;->getInstance(Landroid/content/Context;)Lcom/google/android/music/eventlog/MusicEventLogger;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/music/ui/HomeMenuScreens$PlayDrawerScreenShimRunnable;->mTargetScreen:Lcom/google/android/music/ui/HomeActivity$Screen;

    invoke-virtual {v2, v3}, Lcom/google/android/music/eventlog/MusicEventLogger;->logNavBarItemClicked(Lcom/google/android/music/ui/HomeActivity$Screen;)V

    .line 157
    iget-object v2, p0, Lcom/google/android/music/ui/HomeMenuScreens$PlayDrawerScreenShimRunnable;->mBaseActivity:Lcom/google/android/music/ui/BaseActivity;

    iget-object v3, p0, Lcom/google/android/music/ui/HomeMenuScreens$PlayDrawerScreenShimRunnable;->mTargetScreen:Lcom/google/android/music/ui/HomeActivity$Screen;

    invoke-static {v2, v3}, Lcom/google/android/music/ui/AppNavigation;->showHomeScreen(Lcom/google/android/music/ui/BaseActivity;Lcom/google/android/music/ui/HomeActivity$Screen;)V

    .line 164
    :goto_1
    return-void

    .end local v0    # "downloadOnly":Z
    :cond_1
    move v0, v2

    .line 152
    goto :goto_0

    .line 160
    .restart local v0    # "downloadOnly":Z
    :cond_2
    iget-object v3, p0, Lcom/google/android/music/ui/HomeMenuScreens$PlayDrawerScreenShimRunnable;->mBaseActivity:Lcom/google/android/music/ui/BaseActivity;

    const v4, 0x7f0b0358

    invoke-static {v3, v4, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    .line 162
    .local v1, "toast":Landroid/widget/Toast;
    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_1
.end method
