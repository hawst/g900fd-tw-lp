.class public Lcom/google/android/music/utils/LabelUtils;
.super Ljava/lang/Object;
.source "LabelUtils.java"


# direct methods
.method public static getPlaylistPrimaryLabel(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/String;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "playlistName"    # Ljava/lang/String;
    .param p2, "playlistType"    # I

    .prologue
    .line 14
    const/16 v0, 0x32

    if-ne p2, v0, :cond_0

    .line 15
    const v0, 0x7f0b0108

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 18
    .end local p1    # "playlistName":Ljava/lang/String;
    :cond_0
    return-object p1
.end method

.method public static getPlaylistSecondaryLabel(Landroid/content/Context;Lcom/google/android/music/preferences/MusicPreferences;I)Ljava/lang/String;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "prefs"    # Lcom/google/android/music/preferences/MusicPreferences;
    .param p2, "playlistType"    # I

    .prologue
    .line 23
    const/16 v0, 0x32

    if-ne p2, v0, :cond_1

    .line 24
    invoke-virtual {p1}, Lcom/google/android/music/preferences/MusicPreferences;->isNautilusEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 25
    const v0, 0x7f0b0109

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 39
    :goto_0
    return-object v0

    .line 27
    :cond_0
    const v0, 0x7f0b010a

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 29
    :cond_1
    if-eqz p2, :cond_2

    const/4 v0, 0x1

    if-ne p2, v0, :cond_3

    .line 31
    :cond_2
    const v0, 0x7f0b00fe

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 32
    :cond_3
    const/16 v0, 0x47

    if-ne p2, v0, :cond_4

    .line 33
    const v0, 0x7f0b0100

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 34
    :cond_4
    const/16 v0, 0x46

    if-ne p2, v0, :cond_5

    .line 35
    const v0, 0x7f0b0101

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 36
    :cond_5
    const/16 v0, 0x64

    if-ne p2, v0, :cond_6

    .line 37
    const v0, 0x7f0b00ff

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 39
    :cond_6
    const v0, 0x7f0b00fd

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
