.class Lcom/google/android/music/MusicUserContentNotifier$8;
.super Ljava/lang/Object;
.source "MusicUserContentNotifier.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/MusicUserContentNotifier;->notifyContentChanged(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/MusicUserContentNotifier;

.field final synthetic val$notifyType:I


# direct methods
.method constructor <init>(Lcom/google/android/music/MusicUserContentNotifier;I)V
    .locals 0

    .prologue
    .line 225
    iput-object p1, p0, Lcom/google/android/music/MusicUserContentNotifier$8;->this$0:Lcom/google/android/music/MusicUserContentNotifier;

    iput p2, p0, Lcom/google/android/music/MusicUserContentNotifier$8;->val$notifyType:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 229
    iget-object v2, p0, Lcom/google/android/music/MusicUserContentNotifier$8;->this$0:Lcom/google/android/music/MusicUserContentNotifier;

    # getter for: Lcom/google/android/music/MusicUserContentNotifier;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/google/android/music/MusicUserContentNotifier;->access$200(Lcom/google/android/music/MusicUserContentNotifier;)Landroid/content/Context;

    move-result-object v2

    if-nez v2, :cond_1

    .line 244
    :cond_0
    :goto_0
    return-void

    .line 232
    :cond_1
    iget-object v2, p0, Lcom/google/android/music/MusicUserContentNotifier$8;->this$0:Lcom/google/android/music/MusicUserContentNotifier;

    # getter for: Lcom/google/android/music/MusicUserContentNotifier;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/google/android/music/MusicUserContentNotifier;->access$200(Lcom/google/android/music/MusicUserContentNotifier;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/music/MusicUserContentBinder;->shouldPromoteNautilus(Landroid/content/Context;)Z

    move-result v0

    .line 233
    .local v0, "shouldPromoteNautilus":Z
    iget-object v2, p0, Lcom/google/android/music/MusicUserContentNotifier$8;->this$0:Lcom/google/android/music/MusicUserContentNotifier;

    # getter for: Lcom/google/android/music/MusicUserContentNotifier;->mPreviousShouldPromoteNautilus:Z
    invoke-static {v2}, Lcom/google/android/music/MusicUserContentNotifier;->access$700(Lcom/google/android/music/MusicUserContentNotifier;)Z

    move-result v2

    xor-int v1, v0, v2

    .line 238
    .local v1, "shouldPromoteNautilusChanged":Z
    iget v2, p0, Lcom/google/android/music/MusicUserContentNotifier$8;->val$notifyType:I

    if-eqz v2, :cond_2

    if-eqz v1, :cond_0

    .line 239
    :cond_2
    iget-object v2, p0, Lcom/google/android/music/MusicUserContentNotifier$8;->this$0:Lcom/google/android/music/MusicUserContentNotifier;

    # getter for: Lcom/google/android/music/MusicUserContentNotifier;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/google/android/music/MusicUserContentNotifier;->access$200(Lcom/google/android/music/MusicUserContentNotifier;)Landroid/content/Context;

    move-result-object v2

    # invokes: Lcom/google/android/music/MusicUserContentNotifier;->sendUserContentBroadcast(Landroid/content/Context;)V
    invoke-static {v2}, Lcom/google/android/music/MusicUserContentNotifier;->access$1100(Landroid/content/Context;)V

    .line 240
    if-eqz v1, :cond_0

    .line 241
    iget-object v2, p0, Lcom/google/android/music/MusicUserContentNotifier$8;->this$0:Lcom/google/android/music/MusicUserContentNotifier;

    # setter for: Lcom/google/android/music/MusicUserContentNotifier;->mPreviousShouldPromoteNautilus:Z
    invoke-static {v2, v0}, Lcom/google/android/music/MusicUserContentNotifier;->access$702(Lcom/google/android/music/MusicUserContentNotifier;Z)Z

    goto :goto_0
.end method
