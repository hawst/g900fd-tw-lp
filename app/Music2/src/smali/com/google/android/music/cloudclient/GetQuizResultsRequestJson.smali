.class public Lcom/google/android/music/cloudclient/GetQuizResultsRequestJson;
.super Lcom/google/api/client/json/GenericJson;
.source "GetQuizResultsRequestJson.java"


# instance fields
.field mRequest:Lcom/google/android/music/cloudclient/GetRecommendedArtistsRequestJson;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "quiz_results"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    return-void
.end method

.method public static serialize(Ljava/util/Collection;Ljava/util/Collection;Ljava/util/Collection;)[B
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)[B"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 25
    .local p0, "selectedGenresIds":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    .local p1, "selectedArtistIds":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    .local p2, "artistBlackListIds":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    new-instance v5, Lcom/google/android/music/cloudclient/GetRecommendedArtistsRequestJson;

    invoke-direct {v5}, Lcom/google/android/music/cloudclient/GetRecommendedArtistsRequestJson;-><init>()V

    .line 27
    .local v5, "result":Lcom/google/android/music/cloudclient/GetRecommendedArtistsRequestJson;
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 28
    .local v7, "selectedGenres":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/cloudclient/QuizGenreJson;>;"
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 29
    .local v8, "type":Ljava/lang/String;
    new-instance v3, Lcom/google/android/music/cloudclient/QuizGenreJson;

    invoke-direct {v3}, Lcom/google/android/music/cloudclient/QuizGenreJson;-><init>()V

    .line 30
    .local v3, "quizGenreJson":Lcom/google/android/music/cloudclient/QuizGenreJson;
    iput-object v8, v3, Lcom/google/android/music/cloudclient/QuizGenreJson;->mType:Ljava/lang/String;

    .line 31
    invoke-interface {v7, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 33
    .end local v3    # "quizGenreJson":Lcom/google/android/music/cloudclient/QuizGenreJson;
    .end local v8    # "type":Ljava/lang/String;
    :cond_0
    iput-object v7, v5, Lcom/google/android/music/cloudclient/GetRecommendedArtistsRequestJson;->mSelectedGenres:Ljava/util/List;

    .line 35
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 36
    .local v6, "selectedArtists":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 37
    .local v1, "artistId":Ljava/lang/String;
    invoke-interface {v6, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 39
    .end local v1    # "artistId":Ljava/lang/String;
    :cond_1
    iput-object v6, v5, Lcom/google/android/music/cloudclient/GetRecommendedArtistsRequestJson;->mSelectedArtistIds:Ljava/util/List;

    .line 41
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 42
    .local v0, "artistBlackLists":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 43
    .restart local v1    # "artistId":Ljava/lang/String;
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 45
    .end local v1    # "artistId":Ljava/lang/String;
    :cond_2
    iput-object v0, v5, Lcom/google/android/music/cloudclient/GetRecommendedArtistsRequestJson;->mArtistBlackListIds:Ljava/util/List;

    .line 47
    new-instance v4, Lcom/google/android/music/cloudclient/GetQuizResultsRequestJson;

    invoke-direct {v4}, Lcom/google/android/music/cloudclient/GetQuizResultsRequestJson;-><init>()V

    .line 48
    .local v4, "request":Lcom/google/android/music/cloudclient/GetQuizResultsRequestJson;
    iput-object v5, v4, Lcom/google/android/music/cloudclient/GetQuizResultsRequestJson;->mRequest:Lcom/google/android/music/cloudclient/GetRecommendedArtistsRequestJson;

    .line 49
    invoke-static {v4}, Lcom/google/android/music/cloudclient/JsonUtils;->toJsonByteArray(Lcom/google/api/client/json/GenericJson;)[B

    move-result-object v9

    return-object v9
.end method
