.class Lcom/google/android/music/playback/LocalDevicePlayback$7;
.super Ljava/lang/Object;
.source "LocalDevicePlayback.java"

# interfaces
.implements Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/playback/LocalDevicePlayback;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/playback/LocalDevicePlayback;


# direct methods
.method constructor <init>(Lcom/google/android/music/playback/LocalDevicePlayback;)V
    .locals 0

    .prologue
    .line 618
    iput-object p1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public cancelAllStreamingTracks()V
    .locals 1

    .prologue
    .line 679
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mStreamingClient:Lcom/google/android/music/download/stream/StreamingClient;
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$800(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/download/stream/StreamingClient;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 680
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mStreamingClient:Lcom/google/android/music/download/stream/StreamingClient;
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$800(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/download/stream/StreamingClient;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/download/stream/StreamingClient;->cancelAndPurgeAllStreamingTracks()V

    .line 682
    :cond_0
    return-void
.end method

.method public cancelTryNext()V
    .locals 2

    .prologue
    .line 729
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->LOGV:Z
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$000(Lcom/google/android/music/playback/LocalDevicePlayback;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 730
    const-string v0, "LocalDevicePlayback"

    const-string v1, "cancelTryNext"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 732
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaplayerHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$2000(Lcom/google/android/music/playback/LocalDevicePlayback;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 733
    return-void
.end method

.method public markSongPlayed(Lcom/google/android/music/download/ContentIdentifier;)V
    .locals 4
    .param p1, "songId"    # Lcom/google/android/music/download/ContentIdentifier;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 792
    invoke-virtual {p1}, Lcom/google/android/music/download/ContentIdentifier;->isDefaultDomain()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/music/download/ContentIdentifier;->isNautilusDomain()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 793
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    invoke-virtual {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/music/download/ContentIdentifier;->getId()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/music/store/Store;->markSongPlayed(J)V

    .line 795
    :cond_1
    return-void
.end method

.method public notifyMediaRouteInvalidated()V
    .locals 2

    .prologue
    .line 651
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->LOGV:Z
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$000(Lcom/google/android/music/playback/LocalDevicePlayback;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 652
    const-string v0, "LocalDevicePlayback"

    const-string v1, "notifyMediaRouteInvalidated"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 654
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    invoke-virtual {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->onMediaRouteInvalidated()V

    .line 655
    return-void
.end method

.method public notifyOpenComplete()V
    .locals 1

    .prologue
    .line 636
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    invoke-virtual {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->onOpenComplete()V

    .line 637
    return-void
.end method

.method public notifyOpenStarted()V
    .locals 1

    .prologue
    .line 631
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->onOpenStarted()V
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$1700(Lcom/google/android/music/playback/LocalDevicePlayback;)V

    .line 632
    return-void
.end method

.method public notifyPlayStateChanged()V
    .locals 1

    .prologue
    .line 641
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    invoke-virtual {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->onPlayStateChanged()V

    .line 642
    return-void
.end method

.method public notifyPlaybackComplete()V
    .locals 1

    .prologue
    .line 646
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    invoke-virtual {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->onPlaybackComplete()V

    .line 647
    return-void
.end method

.method public notifyQueueChanged()V
    .locals 1

    .prologue
    .line 621
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->onQueueChanged()V
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$1600(Lcom/google/android/music/playback/LocalDevicePlayback;)V

    .line 622
    return-void
.end method

.method public onCloudQueueAdditionalSenderConnected(Ljava/lang/String;)V
    .locals 2
    .param p1, "itemId"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    .line 861
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # setter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mIsSwappingToCloudQueue:Z
    invoke-static {v0, v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$3602(Lcom/google/android/music/playback/LocalDevicePlayback;Z)Z

    .line 862
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    invoke-virtual {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/music/store/QueueUtils;->notifyChange(Landroid/content/Context;)V

    .line 863
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback$7;->notifyQueueChanged()V

    .line 864
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 866
    invoke-virtual {p0, p1}, Lcom/google/android/music/playback/LocalDevicePlayback$7;->onCloudQueueTrackChanged(Ljava/lang/String;)V

    .line 868
    :cond_0
    invoke-virtual {p0, v1}, Lcom/google/android/music/playback/LocalDevicePlayback$7;->onCloudQueueSessionInitialized(Z)V

    .line 869
    return-void
.end method

.method public onCloudQueueFirstSenderConnected()V
    .locals 2

    .prologue
    .line 854
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mIsSwappingToCloudQueue:Z
    invoke-static {v0, v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$3602(Lcom/google/android/music/playback/LocalDevicePlayback;Z)Z

    .line 855
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    invoke-virtual {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/music/store/QueueUtils;->notifyChange(Landroid/content/Context;)V

    .line 856
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/music/playback/LocalDevicePlayback$7;->onCloudQueueSessionInitialized(Z)V

    .line 857
    return-void
.end method

.method public onCloudQueueSessionInitialized(Z)V
    .locals 2
    .param p1, "shouldSuppressPlaybackRequests"    # Z

    .prologue
    .line 822
    sget-object v0, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    new-instance v1, Lcom/google/android/music/playback/LocalDevicePlayback$7$4;

    invoke-direct {v1, p0, p1}, Lcom/google/android/music/playback/LocalDevicePlayback$7$4;-><init>(Lcom/google/android/music/playback/LocalDevicePlayback$7;Z)V

    invoke-static {v0, v1}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsync(Lcom/google/android/music/utils/LoggableHandler;Ljava/lang/Runnable;)V

    .line 850
    return-void
.end method

.method public onCloudQueueTrackChanged(Ljava/lang/String;)V
    .locals 2
    .param p1, "itemId"    # Ljava/lang/String;

    .prologue
    .line 799
    sget-object v0, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    new-instance v1, Lcom/google/android/music/playback/LocalDevicePlayback$7$3;

    invoke-direct {v1, p0, p1}, Lcom/google/android/music/playback/LocalDevicePlayback$7$3;-><init>(Lcom/google/android/music/playback/LocalDevicePlayback$7;Ljava/lang/String;)V

    invoke-static {v0, v1}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsync(Lcom/google/android/music/utils/LoggableHandler;Ljava/lang/Runnable;)V

    .line 818
    return-void
.end method

.method public onDownloadProgress(Lcom/google/android/music/download/TrackDownloadProgress;)V
    .locals 2
    .param p1, "downloadProgress"    # Lcom/google/android/music/download/TrackDownloadProgress;

    .prologue
    .line 686
    sget-object v0, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    new-instance v1, Lcom/google/android/music/playback/LocalDevicePlayback$7$1;

    invoke-direct {v1, p0, p1}, Lcom/google/android/music/playback/LocalDevicePlayback$7$1;-><init>(Lcom/google/android/music/playback/LocalDevicePlayback$7;Lcom/google/android/music/download/TrackDownloadProgress;)V

    invoke-static {v0, v1}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsync(Lcom/google/android/music/utils/LoggableHandler;Ljava/lang/Runnable;)V

    .line 707
    return-void
.end method

.method public onVolumeChanged(I)V
    .locals 3
    .param p1, "volume"    # I

    .prologue
    .line 711
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->LOGV:Z
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$000(Lcom/google/android/music/playback/LocalDevicePlayback;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 712
    const-string v0, "LocalDevicePlayback"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onVolumeChanged: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 714
    :cond_0
    new-instance v0, Lcom/google/android/music/playback/LocalDevicePlayback$7$2;

    invoke-direct {v0, p0, p1}, Lcom/google/android/music/playback/LocalDevicePlayback$7$2;-><init>(Lcom/google/android/music/playback/LocalDevicePlayback$7;I)V

    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    invoke-virtual {v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/utils/MusicUtils;->runOnUiThread(Ljava/lang/Runnable;Landroid/content/Context;)V

    .line 725
    return-void
.end method

.method public reportTrackEnded()V
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    const/4 v7, 0x0

    .line 753
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mStreamingClient:Lcom/google/android/music/download/stream/StreamingClient;
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$800(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/download/stream/StreamingClient;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 754
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mStreamingClient:Lcom/google/android/music/download/stream/StreamingClient;
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$800(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/download/stream/StreamingClient;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/download/stream/StreamingClient;->currenStreamingPlayEnded()V

    .line 756
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mNextPlayer:Lcom/google/android/music/playback/AsyncMediaPlayer;
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$600(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/playback/AsyncMediaPlayer;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 757
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayer:Lcom/google/android/music/playback/AsyncMediaPlayer;
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$400(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/playback/AsyncMediaPlayer;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/music/playback/AsyncMediaPlayer;->release()V

    .line 758
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mNextPlayer:Lcom/google/android/music/playback/AsyncMediaPlayer;
    invoke-static {v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$600(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/playback/AsyncMediaPlayer;

    move-result-object v1

    # setter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayer:Lcom/google/android/music/playback/AsyncMediaPlayer;
    invoke-static {v0, v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$402(Lcom/google/android/music/playback/LocalDevicePlayback;Lcom/google/android/music/playback/AsyncMediaPlayer;)Lcom/google/android/music/playback/AsyncMediaPlayer;

    .line 759
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayer:Lcom/google/android/music/playback/AsyncMediaPlayer;
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$400(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/playback/AsyncMediaPlayer;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/music/playback/AsyncMediaPlayer;->setAsCurrentPlayer()V

    .line 760
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # setter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mNextPlayer:Lcom/google/android/music/playback/AsyncMediaPlayer;
    invoke-static {v0, v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$602(Lcom/google/android/music/playback/LocalDevicePlayback;Lcom/google/android/music/playback/AsyncMediaPlayer;)Lcom/google/android/music/playback/AsyncMediaPlayer;

    .line 761
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # setter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mNextAudioId:Lcom/google/android/music/download/ContentIdentifier;
    invoke-static {v0, v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$2202(Lcom/google/android/music/playback/LocalDevicePlayback;Lcom/google/android/music/download/ContentIdentifier;)Lcom/google/android/music/download/ContentIdentifier;

    .line 762
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaplayerHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$2000(Lcom/google/android/music/playback/LocalDevicePlayback;)Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 783
    :cond_1
    :goto_0
    return-void

    .line 769
    :cond_2
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mWakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$2300(Lcom/google/android/music/playback/LocalDevicePlayback;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    const-wide/16 v2, 0x7530

    invoke-virtual {v0, v2, v3}, Landroid/os/PowerManager$WakeLock;->acquire(J)V

    .line 770
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaplayerHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$2000(Lcom/google/android/music/playback/LocalDevicePlayback;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 771
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaplayerHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$2000(Lcom/google/android/music/playback/LocalDevicePlayback;)Landroid/os/Handler;

    move-result-object v0

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 773
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mAudioId:Lcom/google/android/music/download/ContentIdentifier;
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$2400(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/download/ContentIdentifier;

    move-result-object v8

    .line 774
    .local v8, "audioId":Lcom/google/android/music/download/ContentIdentifier;
    if-eqz v8, :cond_1

    .line 775
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    invoke-virtual {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/music/eventlog/MusicEventLogger;->getInstance(Landroid/content/Context;)Lcom/google/android/music/eventlog/MusicEventLogger;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mUseLocalPlayer:Z
    invoke-static {v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$2500(Lcom/google/android/music/playback/LocalDevicePlayback;)Z

    move-result v2

    if-nez v2, :cond_3

    :goto_1
    invoke-virtual {v8}, Lcom/google/android/music/download/ContentIdentifier;->getId()J

    move-result-wide v2

    iget-object v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;
    invoke-static {v4}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$2600(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mLastUserExplicitPlayTime:J
    invoke-static {v5}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$2700(Lcom/google/android/music/playback/LocalDevicePlayback;)J

    move-result-wide v5

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/music/eventlog/MusicEventLogger;->logStopEventAsync(ZJLcom/google/android/music/store/ContainerDescriptor;JZ)V

    goto :goto_0

    :cond_3
    move v1, v7

    goto :goto_1
.end method

.method public reportTrackPaused()V
    .locals 2

    .prologue
    .line 745
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->LOGV:Z
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$000(Lcom/google/android/music/playback/LocalDevicePlayback;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 746
    const-string v0, "LocalDevicePlayback"

    const-string v1, "reportTrackPaused"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 748
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mIsSupposedToBePlaying:Z
    invoke-static {v0, v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$2102(Lcom/google/android/music/playback/LocalDevicePlayback;Z)Z

    .line 749
    return-void
.end method

.method public reportTrackPlaying()V
    .locals 2

    .prologue
    .line 737
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->LOGV:Z
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$000(Lcom/google/android/music/playback/LocalDevicePlayback;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 738
    const-string v0, "LocalDevicePlayback"

    const-string v1, "reportTrackPlaying"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 740
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mIsSupposedToBePlaying:Z
    invoke-static {v0, v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$2102(Lcom/google/android/music/playback/LocalDevicePlayback;Z)Z

    .line 741
    return-void
.end method

.method public streamTrack(Lcom/google/android/music/download/ContentIdentifier;JLcom/google/android/music/download/IDownloadProgressListener;ZZ)Ljava/lang/String;
    .locals 10
    .param p1, "trackId"    # Lcom/google/android/music/download/ContentIdentifier;
    .param p2, "seekMs"    # J
    .param p4, "progressListener"    # Lcom/google/android/music/download/IDownloadProgressListener;
    .param p5, "isCurrentPlayer"    # Z
    .param p6, "fromUserAction"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/download/stream/DownloadRequestException;,
            Lcom/google/android/music/download/cache/OutOfSpaceException;
        }
    .end annotation

    .prologue
    .line 662
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaList:Lcom/google/android/music/medialist/SongList;
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$1800(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/medialist/SongList;

    move-result-object v7

    .line 663
    .local v7, "songList":Lcom/google/android/music/medialist/SongList;
    if-nez v7, :cond_0

    .line 664
    const-string v0, "LocalDevicePlayback"

    const-string v1, "MediaList is null"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 665
    const/4 v0, 0x0

    .line 673
    :goto_0
    return-object v0

    .line 667
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mStreamingClient:Lcom/google/android/music/download/stream/StreamingClient;
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$800(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/download/stream/StreamingClient;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 668
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->getNextSongs()[Lcom/google/android/music/download/ContentIdentifier;
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$1900(Lcom/google/android/music/playback/LocalDevicePlayback;)[Lcom/google/android/music/download/ContentIdentifier;

    move-result-object v9

    .line 669
    .local v9, "prefetchList":[Lcom/google/android/music/download/ContentIdentifier;
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mStreamingClient:Lcom/google/android/music/download/stream/StreamingClient;
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$800(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/download/stream/StreamingClient;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    iget-object v8, v1, Lcom/google/android/music/playback/LocalDevicePlayback;->mCursorCols:[Ljava/lang/String;

    move-object v1, p1

    move-wide v2, p2

    move-object v4, p4

    move v5, p5

    move/from16 v6, p6

    invoke-virtual/range {v0 .. v9}, Lcom/google/android/music/download/stream/StreamingClient;->streamTrack(Lcom/google/android/music/download/ContentIdentifier;JLcom/google/android/music/download/IDownloadProgressListener;ZZLcom/google/android/music/medialist/SongList;[Ljava/lang/String;[Lcom/google/android/music/download/ContentIdentifier;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 672
    .end local v9    # "prefetchList":[Lcom/google/android/music/download/ContentIdentifier;
    :cond_1
    const-string v0, "LocalDevicePlayback"

    const-string v1, "Streaming client is null"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 673
    const/4 v0, 0x0

    goto :goto_0
.end method
