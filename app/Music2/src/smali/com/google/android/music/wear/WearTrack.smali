.class public final Lcom/google/android/music/wear/WearTrack;
.super Ljava/lang/Object;
.source "WearTrack.java"


# instance fields
.field public final artistName:Ljava/lang/String;

.field public final artworkId:J

.field public final id:J

.field public final sizeBytes:J

.field public final title:Ljava/lang/String;


# direct methods
.method public constructor <init>(JLjava/lang/String;Ljava/lang/String;JJ)V
    .locals 3
    .param p1, "id"    # J
    .param p3, "title"    # Ljava/lang/String;
    .param p4, "artistName"    # Ljava/lang/String;
    .param p5, "artworkId"    # J
    .param p7, "sizeBytes"    # J

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "id must be non-negative"

    invoke-static {v0, v1}, Lcom/google/android/common/base/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    .line 29
    iput-wide p1, p0, Lcom/google/android/music/wear/WearTrack;->id:J

    .line 30
    const-string v0, "title cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/music/wear/WearTrack;->title:Ljava/lang/String;

    .line 31
    iput-object p4, p0, Lcom/google/android/music/wear/WearTrack;->artistName:Ljava/lang/String;

    .line 32
    const-wide/16 v0, -0x1

    cmp-long v0, p5, v0

    if-ltz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, Lcom/google/android/common/base/Preconditions;->checkArgument(Z)V

    .line 33
    iput-wide p5, p0, Lcom/google/android/music/wear/WearTrack;->artworkId:J

    .line 34
    const-wide/16 v0, 0x0

    cmp-long v0, p7, v0

    if-ltz v0, :cond_2

    const/4 v0, 0x1

    :goto_2
    const-string v1, "sizeBytes must be non-negative"

    invoke-static {v0, v1}, Lcom/google/android/common/base/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    .line 35
    iput-wide p7, p0, Lcom/google/android/music/wear/WearTrack;->sizeBytes:J

    .line 36
    return-void

    .line 28
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 32
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 34
    :cond_2
    const/4 v0, 0x0

    goto :goto_2
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 51
    instance-of v3, p1, Lcom/google/android/music/wear/WearTrack;

    if-nez v3, :cond_1

    move v1, v2

    .line 58
    :cond_0
    :goto_0
    return v1

    .line 54
    :cond_1
    if-eq p1, p0, :cond_0

    move-object v0, p1

    .line 57
    check-cast v0, Lcom/google/android/music/wear/WearTrack;

    .line 58
    .local v0, "other":Lcom/google/android/music/wear/WearTrack;
    iget-wide v4, p0, Lcom/google/android/music/wear/WearTrack;->id:J

    iget-wide v6, v0, Lcom/google/android/music/wear/WearTrack;->id:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/google/android/music/wear/WearTrack;->title:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/music/wear/WearTrack;->title:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/music/wear/WearTrack;->artistName:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/music/wear/WearTrack;->artistName:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-wide v4, p0, Lcom/google/android/music/wear/WearTrack;->artworkId:J

    iget-wide v6, v0, Lcom/google/android/music/wear/WearTrack;->artworkId:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_2

    iget-wide v4, p0, Lcom/google/android/music/wear/WearTrack;->sizeBytes:J

    iget-wide v6, v0, Lcom/google/android/music/wear/WearTrack;->sizeBytes:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 67
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-wide v2, p0, Lcom/google/android/music/wear/WearTrack;->id:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/music/wear/WearTrack;->title:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/music/wear/WearTrack;->artistName:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/google/android/music/wear/WearTrack;->artworkId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-wide v2, p0, Lcom/google/android/music/wear/WearTrack;->sizeBytes:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/common/base/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 40
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/base/Objects;->toStringHelper(Ljava/lang/Class;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "id"

    iget-wide v2, p0, Lcom/google/android/music/wear/WearTrack;->id:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;J)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "title"

    iget-object v2, p0, Lcom/google/android/music/wear/WearTrack;->title:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "artistName"

    iget-object v2, p0, Lcom/google/android/music/wear/WearTrack;->artistName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "artworkId"

    iget-wide v2, p0, Lcom/google/android/music/wear/WearTrack;->artworkId:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;J)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "sizeBytes"

    iget-wide v2, p0, Lcom/google/android/music/wear/WearTrack;->sizeBytes:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;J)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/base/Objects$ToStringHelper;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
