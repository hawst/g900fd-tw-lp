.class public Lcom/google/android/music/cloudclient/SyncCloudQueueResponseJson;
.super Lcom/google/api/client/json/GenericJson;
.source "SyncCloudQueueResponseJson.java"


# instance fields
.field public mContainers:Ljava/util/List;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "containers"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/cloudclient/CloudQueueContainerJson;",
            ">;"
        }
    .end annotation
.end field

.field public mIncremental:Z
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "incremental"
    .end annotation
.end field

.field public mItemIdsToDelete:Ljava/util/List;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "itemIdsToDelete"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public mItems:Ljava/util/List;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "items"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/cloudclient/CloudQueueItemJson;",
            ">;"
        }
    .end annotation
.end field

.field public mSettings:Lcom/google/android/music/cloudclient/CloudQueuePlaySettingsJson;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "settings"
    .end annotation
.end field

.field public mShuffledItemOrderings:Ljava/util/List;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "shuffledItemOrderings"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/cloudclient/CloudQueueItemOrderingJson;",
            ">;"
        }
    .end annotation
.end field

.field public mTracks:Ljava/util/List;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "tracks"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/cloudclient/TrackJson;",
            ">;"
        }
    .end annotation
.end field

.field public mUnshuffledItemOrderings:Ljava/util/List;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "unshuffledItemOrderings"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/cloudclient/CloudQueueItemOrderingJson;",
            ">;"
        }
    .end annotation
.end field

.field public mVersion:Ljava/lang/Long;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "version"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 8

    .prologue
    .line 44
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 45
    .local v4, "s":Ljava/lang/StringBuilder;
    const-string v6, "version: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/music/cloudclient/SyncCloudQueueResponseJson;->mVersion:Ljava/lang/Long;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 46
    iget-object v6, p0, Lcom/google/android/music/cloudclient/SyncCloudQueueResponseJson;->mItems:Ljava/util/List;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/google/android/music/cloudclient/SyncCloudQueueResponseJson;->mItems:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_8

    .line 47
    :cond_0
    const-string v6, "; no items"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 53
    :cond_1
    iget-object v6, p0, Lcom/google/android/music/cloudclient/SyncCloudQueueResponseJson;->mItemIdsToDelete:Ljava/util/List;

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/google/android/music/cloudclient/SyncCloudQueueResponseJson;->mItemIdsToDelete:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_9

    .line 54
    :cond_2
    const-string v6, "; no item ids to delete"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 60
    :cond_3
    iget-object v6, p0, Lcom/google/android/music/cloudclient/SyncCloudQueueResponseJson;->mTracks:Ljava/util/List;

    if-eqz v6, :cond_4

    iget-object v6, p0, Lcom/google/android/music/cloudclient/SyncCloudQueueResponseJson;->mTracks:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_a

    .line 61
    :cond_4
    const-string v6, "; no tracks"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 67
    :cond_5
    iget-object v6, p0, Lcom/google/android/music/cloudclient/SyncCloudQueueResponseJson;->mContainers:Ljava/util/List;

    if-eqz v6, :cond_6

    iget-object v6, p0, Lcom/google/android/music/cloudclient/SyncCloudQueueResponseJson;->mContainers:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_b

    .line 68
    :cond_6
    const-string v6, "; no containers"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 74
    :cond_7
    const-string v6, "; settings: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/music/cloudclient/SyncCloudQueueResponseJson;->mSettings:Lcom/google/android/music/cloudclient/CloudQueuePlaySettingsJson;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 75
    const-string v6, "; incremental: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-boolean v7, p0, Lcom/google/android/music/cloudclient/SyncCloudQueueResponseJson;->mIncremental:Z

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 76
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    return-object v6

    .line 49
    :cond_8
    iget-object v6, p0, Lcom/google/android/music/cloudclient/SyncCloudQueueResponseJson;->mItems:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/cloudclient/CloudQueueItemJson;

    .line 50
    .local v2, "item":Lcom/google/android/music/cloudclient/CloudQueueItemJson;
    const-string v6, "; item: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 56
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "item":Lcom/google/android/music/cloudclient/CloudQueueItemJson;
    :cond_9
    iget-object v6, p0, Lcom/google/android/music/cloudclient/SyncCloudQueueResponseJson;->mItemIdsToDelete:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .restart local v1    # "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 57
    .local v3, "itemIdToDelete":Ljava/lang/String;
    const-string v6, "; itemIdToDelete: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 63
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v3    # "itemIdToDelete":Ljava/lang/String;
    :cond_a
    iget-object v6, p0, Lcom/google/android/music/cloudclient/SyncCloudQueueResponseJson;->mTracks:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .restart local v1    # "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/music/cloudclient/TrackJson;

    .line 64
    .local v5, "track":Lcom/google/android/music/cloudclient/TrackJson;
    const-string v6, "; track: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 70
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v5    # "track":Lcom/google/android/music/cloudclient/TrackJson;
    :cond_b
    iget-object v6, p0, Lcom/google/android/music/cloudclient/SyncCloudQueueResponseJson;->mContainers:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .restart local v1    # "i$":Ljava/util/Iterator;
    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/cloudclient/CloudQueueContainerJson;

    .line 71
    .local v0, "container":Lcom/google/android/music/cloudclient/CloudQueueContainerJson;
    const-string v6, "; container: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_3
.end method
