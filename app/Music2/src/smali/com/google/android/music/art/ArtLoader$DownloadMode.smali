.class public final enum Lcom/google/android/music/art/ArtLoader$DownloadMode;
.super Ljava/lang/Enum;
.source "ArtLoader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/art/ArtLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DownloadMode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/music/art/ArtLoader$DownloadMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/music/art/ArtLoader$DownloadMode;

.field public static final enum ASYNC_DOWNLOAD:Lcom/google/android/music/art/ArtLoader$DownloadMode;

.field public static final enum NO_DOWNLOAD:Lcom/google/android/music/art/ArtLoader$DownloadMode;

.field public static final enum SYNC_DOWNLOAD:Lcom/google/android/music/art/ArtLoader$DownloadMode;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 21
    new-instance v0, Lcom/google/android/music/art/ArtLoader$DownloadMode;

    const-string v1, "NO_DOWNLOAD"

    invoke-direct {v0, v1, v2}, Lcom/google/android/music/art/ArtLoader$DownloadMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/art/ArtLoader$DownloadMode;->NO_DOWNLOAD:Lcom/google/android/music/art/ArtLoader$DownloadMode;

    .line 29
    new-instance v0, Lcom/google/android/music/art/ArtLoader$DownloadMode;

    const-string v1, "ASYNC_DOWNLOAD"

    invoke-direct {v0, v1, v3}, Lcom/google/android/music/art/ArtLoader$DownloadMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/art/ArtLoader$DownloadMode;->ASYNC_DOWNLOAD:Lcom/google/android/music/art/ArtLoader$DownloadMode;

    .line 36
    new-instance v0, Lcom/google/android/music/art/ArtLoader$DownloadMode;

    const-string v1, "SYNC_DOWNLOAD"

    invoke-direct {v0, v1, v4}, Lcom/google/android/music/art/ArtLoader$DownloadMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/art/ArtLoader$DownloadMode;->SYNC_DOWNLOAD:Lcom/google/android/music/art/ArtLoader$DownloadMode;

    .line 17
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/music/art/ArtLoader$DownloadMode;

    sget-object v1, Lcom/google/android/music/art/ArtLoader$DownloadMode;->NO_DOWNLOAD:Lcom/google/android/music/art/ArtLoader$DownloadMode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/music/art/ArtLoader$DownloadMode;->ASYNC_DOWNLOAD:Lcom/google/android/music/art/ArtLoader$DownloadMode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/music/art/ArtLoader$DownloadMode;->SYNC_DOWNLOAD:Lcom/google/android/music/art/ArtLoader$DownloadMode;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/music/art/ArtLoader$DownloadMode;->$VALUES:[Lcom/google/android/music/art/ArtLoader$DownloadMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 17
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/music/art/ArtLoader$DownloadMode;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 17
    const-class v0, Lcom/google/android/music/art/ArtLoader$DownloadMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/art/ArtLoader$DownloadMode;

    return-object v0
.end method

.method public static values()[Lcom/google/android/music/art/ArtLoader$DownloadMode;
    .locals 1

    .prologue
    .line 17
    sget-object v0, Lcom/google/android/music/art/ArtLoader$DownloadMode;->$VALUES:[Lcom/google/android/music/art/ArtLoader$DownloadMode;

    invoke-virtual {v0}, [Lcom/google/android/music/art/ArtLoader$DownloadMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/music/art/ArtLoader$DownloadMode;

    return-object v0
.end method
