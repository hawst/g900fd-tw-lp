.class Lcom/google/android/music/ui/MaterialContainerHeaderView$4;
.super Ljava/lang/Object;
.source "MaterialContainerHeaderView.java"

# interfaces
.implements Lcom/google/android/music/utils/async/AsyncRunner;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/MaterialContainerHeaderView;->asyncSetContainerArt()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final mSavedContext:Landroid/content/Context;

.field private final mSavedSongList:Lcom/google/android/music/medialist/SongList;

.field final synthetic this$0:Lcom/google/android/music/ui/MaterialContainerHeaderView;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/MaterialContainerHeaderView;)V
    .locals 1

    .prologue
    .line 224
    iput-object p1, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView$4;->this$0:Lcom/google/android/music/ui/MaterialContainerHeaderView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 225
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView$4;->this$0:Lcom/google/android/music/ui/MaterialContainerHeaderView;

    invoke-virtual {v0}, Lcom/google/android/music/ui/MaterialContainerHeaderView;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView$4;->mSavedContext:Landroid/content/Context;

    .line 226
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView$4;->this$0:Lcom/google/android/music/ui/MaterialContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/MaterialContainerHeaderView;->mSongList:Lcom/google/android/music/medialist/SongList;
    invoke-static {v0}, Lcom/google/android/music/ui/MaterialContainerHeaderView;->access$000(Lcom/google/android/music/ui/MaterialContainerHeaderView;)Lcom/google/android/music/medialist/SongList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView$4;->mSavedSongList:Lcom/google/android/music/medialist/SongList;

    return-void
.end method


# virtual methods
.method public backgroundTask()V
    .locals 2

    .prologue
    .line 233
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView$4;->mSavedSongList:Lcom/google/android/music/medialist/SongList;

    instance-of v0, v0, Lcom/google/android/music/medialist/ExternalSongList;

    if-eqz v0, :cond_0

    .line 234
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView$4;->mSavedSongList:Lcom/google/android/music/medialist/SongList;

    check-cast v0, Lcom/google/android/music/medialist/ExternalSongList;

    iget-object v1, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView$4;->mSavedContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/google/android/music/medialist/ExternalSongList;->getAlbumArtUrl(Landroid/content/Context;)Ljava/lang/String;

    .line 236
    :cond_0
    return-void
.end method

.method public taskCompleted()V
    .locals 2

    .prologue
    .line 241
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView$4;->this$0:Lcom/google/android/music/ui/MaterialContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/MaterialContainerHeaderView;->mSongList:Lcom/google/android/music/medialist/SongList;
    invoke-static {v0}, Lcom/google/android/music/ui/MaterialContainerHeaderView;->access$000(Lcom/google/android/music/ui/MaterialContainerHeaderView;)Lcom/google/android/music/medialist/SongList;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView$4;->mSavedSongList:Lcom/google/android/music/medialist/SongList;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView$4;->this$0:Lcom/google/android/music/ui/MaterialContainerHeaderView;

    invoke-virtual {v0}, Lcom/google/android/music/ui/MaterialContainerHeaderView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView$4;->mSavedContext:Landroid/content/Context;

    if-eq v0, v1, :cond_1

    .line 251
    :cond_0
    :goto_0
    return-void

    .line 246
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView$4;->this$0:Lcom/google/android/music/ui/MaterialContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/MaterialContainerHeaderView;->mSongList:Lcom/google/android/music/medialist/SongList;
    invoke-static {v0}, Lcom/google/android/music/ui/MaterialContainerHeaderView;->access$000(Lcom/google/android/music/ui/MaterialContainerHeaderView;)Lcom/google/android/music/medialist/SongList;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/music/medialist/SharedWithMeSongList;

    if-eqz v0, :cond_2

    .line 247
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView$4;->this$0:Lcom/google/android/music/ui/MaterialContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/MaterialContainerHeaderView;->mContainerArt:Lcom/google/android/music/AsyncAlbumArtImageView;
    invoke-static {v0}, Lcom/google/android/music/ui/MaterialContainerHeaderView;->access$400(Lcom/google/android/music/ui/MaterialContainerHeaderView;)Lcom/google/android/music/AsyncAlbumArtImageView;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView$4;->this$0:Lcom/google/android/music/ui/MaterialContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/MaterialContainerHeaderView;->mSongList:Lcom/google/android/music/medialist/SongList;
    invoke-static {v0}, Lcom/google/android/music/ui/MaterialContainerHeaderView;->access$000(Lcom/google/android/music/ui/MaterialContainerHeaderView;)Lcom/google/android/music/medialist/SongList;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/medialist/SharedWithMeSongList;

    invoke-virtual {v0}, Lcom/google/android/music/medialist/SharedWithMeSongList;->getArtUrl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/music/AsyncAlbumArtImageView;->setSharedPlaylistArt(Ljava/lang/String;)V

    goto :goto_0

    .line 249
    :cond_2
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView$4;->this$0:Lcom/google/android/music/ui/MaterialContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/MaterialContainerHeaderView;->mContainerArt:Lcom/google/android/music/AsyncAlbumArtImageView;
    invoke-static {v0}, Lcom/google/android/music/ui/MaterialContainerHeaderView;->access$400(Lcom/google/android/music/ui/MaterialContainerHeaderView;)Lcom/google/android/music/AsyncAlbumArtImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView$4;->this$0:Lcom/google/android/music/ui/MaterialContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/MaterialContainerHeaderView;->mSongList:Lcom/google/android/music/medialist/SongList;
    invoke-static {v1}, Lcom/google/android/music/ui/MaterialContainerHeaderView;->access$000(Lcom/google/android/music/ui/MaterialContainerHeaderView;)Lcom/google/android/music/medialist/SongList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/music/AsyncAlbumArtImageView;->setArtForSonglist(Lcom/google/android/music/medialist/SongList;)V

    goto :goto_0
.end method
