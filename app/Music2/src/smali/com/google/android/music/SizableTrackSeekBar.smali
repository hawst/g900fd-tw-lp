.class public final Lcom/google/android/music/SizableTrackSeekBar;
.super Lcom/google/android/music/SafePostSeekBar;
.source "SizableTrackSeekBar.java"


# instance fields
.field private final mAnimatorListener:Lcom/google/android/music/animator/AnimatorUpdateListener;

.field private mCurrentThumbSizeRatio:F

.field private mEnableAccessibility:Z

.field private final mInternalSeekListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field private final mMaxThumbSize:I

.field private final mMaxThumbSizeRatio:Ljava/lang/Float;

.field private mPendingThumb:Landroid/graphics/drawable/Drawable;

.field private mSeekListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field private mThumb:Landroid/graphics/drawable/Drawable;

.field private mThumbAlpha:F

.field private mThumbGrowAnimator:Lcom/google/android/music/animator/Animator;

.field private mThumbShrinkAnimator:Lcom/google/android/music/animator/Animator;

.field private mTrackHeight:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v6, 0x0

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v4, -0x1

    .line 99
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/SafePostSeekBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 47
    iput v5, p0, Lcom/google/android/music/SizableTrackSeekBar;->mThumbAlpha:F

    .line 48
    iput-boolean v6, p0, Lcom/google/android/music/SizableTrackSeekBar;->mEnableAccessibility:Z

    .line 54
    iput v5, p0, Lcom/google/android/music/SizableTrackSeekBar;->mCurrentThumbSizeRatio:F

    .line 60
    new-instance v3, Lcom/google/android/music/SizableTrackSeekBar$1;

    invoke-direct {v3, p0}, Lcom/google/android/music/SizableTrackSeekBar$1;-><init>(Lcom/google/android/music/SizableTrackSeekBar;)V

    iput-object v3, p0, Lcom/google/android/music/SizableTrackSeekBar;->mAnimatorListener:Lcom/google/android/music/animator/AnimatorUpdateListener;

    .line 73
    new-instance v3, Lcom/google/android/music/SizableTrackSeekBar$2;

    invoke-direct {v3, p0}, Lcom/google/android/music/SizableTrackSeekBar$2;-><init>(Lcom/google/android/music/SizableTrackSeekBar;)V

    iput-object v3, p0, Lcom/google/android/music/SizableTrackSeekBar;->mInternalSeekListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 100
    sget-object v3, Lcom/google/android/music/R$styleable;->SizableSeekbar:[I

    invoke-virtual {p1, p2, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 101
    .local v0, "a":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v6, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    .line 102
    .local v2, "trackHeight":I
    if-ne v2, v4, :cond_2

    .line 103
    const/high16 v3, 0x40400000    # 3.0f

    invoke-virtual {p0, v3}, Lcom/google/android/music/SizableTrackSeekBar;->setTrackHeightDip(F)V

    .line 108
    :goto_0
    const/4 v3, 0x1

    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    .line 110
    .local v1, "minThumbSize":I
    const/4 v3, 0x2

    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v3

    iput v3, p0, Lcom/google/android/music/SizableTrackSeekBar;->mMaxThumbSize:I

    .line 113
    if-ltz v1, :cond_0

    iget v3, p0, Lcom/google/android/music/SizableTrackSeekBar;->mMaxThumbSize:I

    if-ltz v3, :cond_1

    :cond_0
    iget v3, p0, Lcom/google/android/music/SizableTrackSeekBar;->mMaxThumbSize:I

    if-ltz v3, :cond_3

    if-gez v1, :cond_3

    .line 115
    :cond_1
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "both or neither of defaultThumbSize and selectedThumbSize must be specified"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 105
    .end local v1    # "minThumbSize":I
    :cond_2
    invoke-virtual {p0, v2}, Lcom/google/android/music/SizableTrackSeekBar;->setTrackHeight(I)V

    goto :goto_0

    .line 119
    .restart local v1    # "minThumbSize":I
    :cond_3
    iget v3, p0, Lcom/google/android/music/SizableTrackSeekBar;->mMaxThumbSize:I

    if-le v1, v3, :cond_4

    .line 120
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "defaultThumbSize cannot be larger than selectedThumbSize"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 124
    :cond_4
    if-ltz v1, :cond_5

    iget v3, p0, Lcom/google/android/music/SizableTrackSeekBar;->mMaxThumbSize:I

    if-ltz v3, :cond_5

    .line 125
    iget v3, p0, Lcom/google/android/music/SizableTrackSeekBar;->mMaxThumbSize:I

    int-to-float v3, v3

    int-to-float v4, v1

    div-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/music/SizableTrackSeekBar;->mMaxThumbSizeRatio:Ljava/lang/Float;

    .line 130
    :goto_1
    iget-object v3, p0, Lcom/google/android/music/SizableTrackSeekBar;->mInternalSeekListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-super {p0, v3}, Lcom/google/android/music/SafePostSeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 132
    iget-object v3, p0, Lcom/google/android/music/SizableTrackSeekBar;->mPendingThumb:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v3}, Lcom/google/android/music/SizableTrackSeekBar;->setThumb(Landroid/graphics/drawable/Drawable;)V

    .line 133
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/google/android/music/SizableTrackSeekBar;->mPendingThumb:Landroid/graphics/drawable/Drawable;

    .line 135
    invoke-direct {p0}, Lcom/google/android/music/SizableTrackSeekBar;->configureThumbPadding()V

    .line 136
    return-void

    .line 127
    :cond_5
    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/music/SizableTrackSeekBar;->mMaxThumbSizeRatio:Ljava/lang/Float;

    goto :goto_1
.end method

.method static synthetic access$000(Lcom/google/android/music/SizableTrackSeekBar;)F
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/SizableTrackSeekBar;

    .prologue
    .line 33
    iget v0, p0, Lcom/google/android/music/SizableTrackSeekBar;->mCurrentThumbSizeRatio:F

    return v0
.end method

.method static synthetic access$002(Lcom/google/android/music/SizableTrackSeekBar;F)F
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/SizableTrackSeekBar;
    .param p1, "x1"    # F

    .prologue
    .line 33
    iput p1, p0, Lcom/google/android/music/SizableTrackSeekBar;->mCurrentThumbSizeRatio:F

    return p1
.end method

.method static synthetic access$100(Lcom/google/android/music/SizableTrackSeekBar;)Ljava/lang/Float;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/SizableTrackSeekBar;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/music/SizableTrackSeekBar;->mMaxThumbSizeRatio:Ljava/lang/Float;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/music/SizableTrackSeekBar;)Landroid/graphics/drawable/Drawable;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/SizableTrackSeekBar;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/music/SizableTrackSeekBar;->mThumb:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/music/SizableTrackSeekBar;)Landroid/widget/SeekBar$OnSeekBarChangeListener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/SizableTrackSeekBar;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/music/SizableTrackSeekBar;->mSeekListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/music/SizableTrackSeekBar;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/SizableTrackSeekBar;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/google/android/music/SizableTrackSeekBar;->startThumbGrowAnimation()V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/music/SizableTrackSeekBar;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/SizableTrackSeekBar;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/google/android/music/SizableTrackSeekBar;->startThumbShrinkAnimation()V

    return-void
.end method

.method private configureThumbPadding()V
    .locals 9

    .prologue
    .line 195
    iget-object v6, p0, Lcom/google/android/music/SizableTrackSeekBar;->mThumb:Landroid/graphics/drawable/Drawable;

    if-eqz v6, :cond_0

    .line 199
    iget-object v6, p0, Lcom/google/android/music/SizableTrackSeekBar;->mThumb:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    int-to-float v6, v6

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v6

    add-int/lit8 v5, v6, -0x1

    .line 204
    .local v5, "trackPadding":I
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/music/SizableTrackSeekBar;->getPaddingRight()I

    move-result v3

    .line 205
    .local v3, "paddingRight":I
    invoke-virtual {p0}, Lcom/google/android/music/SizableTrackSeekBar;->getPaddingLeft()I

    move-result v2

    .line 206
    .local v2, "paddingLeft":I
    invoke-virtual {p0}, Lcom/google/android/music/SizableTrackSeekBar;->getPaddingTop()I

    move-result v4

    .line 207
    .local v4, "paddingTop":I
    invoke-virtual {p0}, Lcom/google/android/music/SizableTrackSeekBar;->getPaddingBottom()I

    move-result v1

    .line 209
    .local v1, "paddingBottom":I
    invoke-virtual {p0}, Lcom/google/android/music/SizableTrackSeekBar;->getHeight()I

    move-result v6

    iget v7, p0, Lcom/google/android/music/SizableTrackSeekBar;->mTrackHeight:I

    sub-int/2addr v6, v7

    div-int/lit8 v0, v6, 0x2

    .line 211
    .local v0, "gapForCenteringTrack":I
    invoke-virtual {p0}, Lcom/google/android/music/SizableTrackSeekBar;->getProgressDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-virtual {p0}, Lcom/google/android/music/SizableTrackSeekBar;->getWidth()I

    move-result v7

    sub-int/2addr v7, v3

    sub-int/2addr v7, v2

    sub-int/2addr v7, v5

    invoke-virtual {p0}, Lcom/google/android/music/SizableTrackSeekBar;->getHeight()I

    move-result v8

    sub-int/2addr v8, v1

    sub-int/2addr v8, v0

    sub-int/2addr v8, v4

    invoke-virtual {v6, v5, v0, v7, v8}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 214
    const/4 v6, 0x0

    invoke-virtual {p0, v6}, Lcom/google/android/music/SizableTrackSeekBar;->setThumbOffset(I)V

    .line 215
    return-void

    .line 201
    .end local v0    # "gapForCenteringTrack":I
    .end local v1    # "paddingBottom":I
    .end local v2    # "paddingLeft":I
    .end local v3    # "paddingRight":I
    .end local v4    # "paddingTop":I
    .end local v5    # "trackPadding":I
    :cond_0
    const/4 v5, 0x0

    .restart local v5    # "trackPadding":I
    goto :goto_0
.end method

.method private startThumbGrowAnimation()V
    .locals 4

    .prologue
    .line 235
    iget-object v0, p0, Lcom/google/android/music/SizableTrackSeekBar;->mThumbShrinkAnimator:Lcom/google/android/music/animator/Animator;

    if-eqz v0, :cond_0

    .line 236
    iget-object v0, p0, Lcom/google/android/music/SizableTrackSeekBar;->mThumbShrinkAnimator:Lcom/google/android/music/animator/Animator;

    invoke-virtual {v0}, Lcom/google/android/music/animator/Animator;->cancel()V

    .line 237
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/SizableTrackSeekBar;->mThumbShrinkAnimator:Lcom/google/android/music/animator/Animator;

    .line 240
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/SizableTrackSeekBar;->mMaxThumbSizeRatio:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/google/android/music/SizableTrackSeekBar;->mCurrentThumbSizeRatio:F

    iget-object v1, p0, Lcom/google/android/music/SizableTrackSeekBar;->mMaxThumbSizeRatio:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_2

    .line 249
    :cond_1
    :goto_0
    return-void

    .line 245
    :cond_2
    new-instance v0, Lcom/google/android/music/animator/Animator;

    const/16 v1, 0x12c

    iget v2, p0, Lcom/google/android/music/SizableTrackSeekBar;->mCurrentThumbSizeRatio:F

    iget-object v3, p0, Lcom/google/android/music/SizableTrackSeekBar;->mMaxThumbSizeRatio:Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/music/animator/Animator;-><init>(IFF)V

    iput-object v0, p0, Lcom/google/android/music/SizableTrackSeekBar;->mThumbGrowAnimator:Lcom/google/android/music/animator/Animator;

    .line 247
    iget-object v0, p0, Lcom/google/android/music/SizableTrackSeekBar;->mThumbGrowAnimator:Lcom/google/android/music/animator/Animator;

    iget-object v1, p0, Lcom/google/android/music/SizableTrackSeekBar;->mAnimatorListener:Lcom/google/android/music/animator/AnimatorUpdateListener;

    invoke-virtual {v0, v1}, Lcom/google/android/music/animator/Animator;->addUpdateListener(Lcom/google/android/music/animator/AnimatorUpdateListener;)V

    .line 248
    iget-object v0, p0, Lcom/google/android/music/SizableTrackSeekBar;->mThumbGrowAnimator:Lcom/google/android/music/animator/Animator;

    invoke-virtual {v0}, Lcom/google/android/music/animator/Animator;->start()V

    goto :goto_0
.end method

.method private startThumbShrinkAnimation()V
    .locals 4

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    .line 218
    iget-object v0, p0, Lcom/google/android/music/SizableTrackSeekBar;->mThumbGrowAnimator:Lcom/google/android/music/animator/Animator;

    if-eqz v0, :cond_0

    .line 219
    iget-object v0, p0, Lcom/google/android/music/SizableTrackSeekBar;->mThumbGrowAnimator:Lcom/google/android/music/animator/Animator;

    invoke-virtual {v0}, Lcom/google/android/music/animator/Animator;->cancel()V

    .line 220
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/SizableTrackSeekBar;->mThumbGrowAnimator:Lcom/google/android/music/animator/Animator;

    .line 223
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/SizableTrackSeekBar;->mMaxThumbSizeRatio:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    cmpl-float v0, v0, v3

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/google/android/music/SizableTrackSeekBar;->mCurrentThumbSizeRatio:F

    cmpg-float v0, v0, v3

    if-gtz v0, :cond_2

    .line 232
    :cond_1
    :goto_0
    return-void

    .line 228
    :cond_2
    new-instance v0, Lcom/google/android/music/animator/Animator;

    const/16 v1, 0x12c

    iget v2, p0, Lcom/google/android/music/SizableTrackSeekBar;->mCurrentThumbSizeRatio:F

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/music/animator/Animator;-><init>(IFF)V

    iput-object v0, p0, Lcom/google/android/music/SizableTrackSeekBar;->mThumbShrinkAnimator:Lcom/google/android/music/animator/Animator;

    .line 230
    iget-object v0, p0, Lcom/google/android/music/SizableTrackSeekBar;->mThumbShrinkAnimator:Lcom/google/android/music/animator/Animator;

    iget-object v1, p0, Lcom/google/android/music/SizableTrackSeekBar;->mAnimatorListener:Lcom/google/android/music/animator/AnimatorUpdateListener;

    invoke-virtual {v0, v1}, Lcom/google/android/music/animator/Animator;->addUpdateListener(Lcom/google/android/music/animator/AnimatorUpdateListener;)V

    .line 231
    iget-object v0, p0, Lcom/google/android/music/SizableTrackSeekBar;->mThumbShrinkAnimator:Lcom/google/android/music/animator/Animator;

    invoke-virtual {v0}, Lcom/google/android/music/animator/Animator;->start()V

    goto :goto_0
.end method


# virtual methods
.method protected onSizeChanged(IIII)V
    .locals 0
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    .line 145
    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/android/music/SafePostSeekBar;->onSizeChanged(IIII)V

    .line 146
    invoke-direct {p0}, Lcom/google/android/music/SizableTrackSeekBar;->configureThumbPadding()V

    .line 147
    return-void
.end method

.method public setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V
    .locals 0
    .param p1, "listener"    # Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .prologue
    .line 140
    iput-object p1, p0, Lcom/google/android/music/SizableTrackSeekBar;->mSeekListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 141
    return-void
.end method

.method public setThumb(Landroid/graphics/drawable/Drawable;)V
    .locals 7
    .param p1, "thumb"    # Landroid/graphics/drawable/Drawable;

    .prologue
    const/high16 v6, 0x3f800000    # 1.0f

    .line 151
    iget-object v3, p0, Lcom/google/android/music/SizableTrackSeekBar;->mMaxThumbSizeRatio:Ljava/lang/Float;

    if-nez v3, :cond_0

    .line 154
    iput-object p1, p0, Lcom/google/android/music/SizableTrackSeekBar;->mPendingThumb:Landroid/graphics/drawable/Drawable;

    .line 186
    :goto_0
    return-void

    .line 158
    :cond_0
    if-eqz p1, :cond_3

    .line 159
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    if-eq v3, v4, :cond_1

    .line 160
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "SizableTrackSeekBar only supports square thumbs"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 163
    :cond_1
    iget v3, p0, Lcom/google/android/music/SizableTrackSeekBar;->mMaxThumbSize:I

    if-ltz v3, :cond_2

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    if-ltz v3, :cond_2

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    if-ltz v3, :cond_2

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    iget v4, p0, Lcom/google/android/music/SizableTrackSeekBar;->mMaxThumbSize:I

    if-eq v3, v4, :cond_2

    .line 168
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v4, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 170
    .local v1, "orig":Landroid/graphics/Bitmap;
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 171
    .local v0, "canvas":Landroid/graphics/Canvas;
    invoke-virtual {p1, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 172
    iget v3, p0, Lcom/google/android/music/SizableTrackSeekBar;->mMaxThumbSize:I

    iget v4, p0, Lcom/google/android/music/SizableTrackSeekBar;->mMaxThumbSize:I

    const/4 v5, 0x0

    invoke-static {v1, v3, v4, v5}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 174
    .local v2, "resized":Landroid/graphics/Bitmap;
    new-instance p1, Landroid/graphics/drawable/BitmapDrawable;

    .end local p1    # "thumb":Landroid/graphics/drawable/Drawable;
    invoke-virtual {p0}, Lcom/google/android/music/SizableTrackSeekBar;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-direct {p1, v3, v2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 175
    .restart local p1    # "thumb":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 177
    .end local v0    # "canvas":Landroid/graphics/Canvas;
    .end local v1    # "orig":Landroid/graphics/Bitmap;
    .end local v2    # "resized":Landroid/graphics/Bitmap;
    :cond_2
    new-instance v3, Landroid/graphics/drawable/ScaleDrawable;

    const/16 v4, 0x11

    invoke-direct {v3, p1, v4, v6, v6}, Landroid/graphics/drawable/ScaleDrawable;-><init>(Landroid/graphics/drawable/Drawable;IFF)V

    iput-object v3, p0, Lcom/google/android/music/SizableTrackSeekBar;->mThumb:Landroid/graphics/drawable/Drawable;

    .line 179
    iget-object v3, p0, Lcom/google/android/music/SizableTrackSeekBar;->mThumb:Landroid/graphics/drawable/Drawable;

    iget-object v4, p0, Lcom/google/android/music/SizableTrackSeekBar;->mMaxThumbSizeRatio:Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    div-float v4, v6, v4

    const v5, 0x461c4000    # 10000.0f

    mul-float/2addr v4, v5

    float-to-int v4, v4

    invoke-virtual {v3, v4}, Landroid/graphics/drawable/Drawable;->setLevel(I)Z

    .line 184
    :goto_1
    invoke-direct {p0}, Lcom/google/android/music/SizableTrackSeekBar;->configureThumbPadding()V

    .line 185
    iget-object v3, p0, Lcom/google/android/music/SizableTrackSeekBar;->mThumb:Landroid/graphics/drawable/Drawable;

    invoke-super {p0, v3}, Lcom/google/android/music/SafePostSeekBar;->setThumb(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 181
    :cond_3
    iput-object p1, p0, Lcom/google/android/music/SizableTrackSeekBar;->mThumb:Landroid/graphics/drawable/Drawable;

    goto :goto_1
.end method

.method public setThumbAlpha(I)V
    .locals 2
    .param p1, "alpha"    # I

    .prologue
    .line 265
    int-to-float v0, p1

    const/high16 v1, 0x437f0000    # 255.0f

    div-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/music/SizableTrackSeekBar;->mThumbAlpha:F

    .line 266
    iget-object v0, p0, Lcom/google/android/music/SizableTrackSeekBar;->mThumb:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 267
    iget-object v0, p0, Lcom/google/android/music/SizableTrackSeekBar;->mThumb:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 269
    :cond_0
    invoke-direct {p0}, Lcom/google/android/music/SizableTrackSeekBar;->configureThumbPadding()V

    .line 270
    return-void
.end method

.method public setTrackHeight(I)V
    .locals 0
    .param p1, "size"    # I

    .prologue
    .line 256
    iput p1, p0, Lcom/google/android/music/SizableTrackSeekBar;->mTrackHeight:I

    .line 257
    invoke-virtual {p0}, Lcom/google/android/music/SizableTrackSeekBar;->requestLayout()V

    .line 258
    return-void
.end method

.method public setTrackHeightDip(F)V
    .locals 1
    .param p1, "size"    # F

    .prologue
    .line 252
    invoke-virtual {p0}, Lcom/google/android/music/SizableTrackSeekBar;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v0, p1

    float-to-int v0, v0

    invoke-virtual {p0, v0}, Lcom/google/android/music/SizableTrackSeekBar;->setTrackHeight(I)V

    .line 253
    return-void
.end method
