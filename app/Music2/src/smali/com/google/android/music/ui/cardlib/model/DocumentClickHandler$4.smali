.class final Lcom/google/android/music/ui/cardlib/model/DocumentClickHandler$4;
.super Ljava/lang/Object;
.source "DocumentClickHandler.java"

# interfaces
.implements Lcom/google/android/music/utils/async/AsyncRunner;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/cardlib/model/DocumentClickHandler;->openSoundSearch(Landroid/content/Context;Lcom/google/android/music/ui/cardlib/model/Document;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field private mNumOfSongs:I

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$doc:Lcom/google/android/music/ui/cardlib/model/Document;

.field final synthetic val$songList:Lcom/google/android/music/medialist/PlaylistSongList;


# direct methods
.method constructor <init>(Lcom/google/android/music/medialist/PlaylistSongList;Landroid/content/Context;Lcom/google/android/music/ui/cardlib/model/Document;)V
    .locals 1

    .prologue
    .line 211
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/model/DocumentClickHandler$4;->val$songList:Lcom/google/android/music/medialist/PlaylistSongList;

    iput-object p2, p0, Lcom/google/android/music/ui/cardlib/model/DocumentClickHandler$4;->val$context:Landroid/content/Context;

    iput-object p3, p0, Lcom/google/android/music/ui/cardlib/model/DocumentClickHandler$4;->val$doc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 212
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/music/ui/cardlib/model/DocumentClickHandler$4;->mNumOfSongs:I

    return-void
.end method


# virtual methods
.method public backgroundTask()V
    .locals 4

    .prologue
    .line 216
    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/DocumentClickHandler$4;->val$songList:Lcom/google/android/music/medialist/PlaylistSongList;

    invoke-virtual {v2}, Lcom/google/android/music/medialist/PlaylistSongList;->getPlaylistId()J

    move-result-wide v0

    .line 217
    .local v0, "playListId":J
    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/DocumentClickHandler$4;->val$context:Landroid/content/Context;

    const/4 v3, 0x0

    invoke-static {v2, v0, v1, v3}, Lcom/google/android/music/store/MusicContent$Playlists$Members;->getPlaylistItemsCount(Landroid/content/Context;JZ)I

    move-result v2

    iput v2, p0, Lcom/google/android/music/ui/cardlib/model/DocumentClickHandler$4;->mNumOfSongs:I

    .line 219
    return-void
.end method

.method public taskCompleted()V
    .locals 2

    .prologue
    .line 223
    iget v0, p0, Lcom/google/android/music/ui/cardlib/model/DocumentClickHandler$4;->mNumOfSongs:I

    if-nez v0, :cond_0

    .line 224
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/DocumentClickHandler$4;->val$context:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/music/ui/SoundSearchCardActivity;->showSoundSearchTutorialCard(Landroid/content/Context;)V

    .line 228
    :goto_0
    return-void

    .line 226
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/DocumentClickHandler$4;->val$context:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/music/ui/cardlib/model/DocumentClickHandler$4;->val$doc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-static {v0, v1}, Lcom/google/android/music/ui/TrackContainerActivity;->showPlaylist(Landroid/content/Context;Lcom/google/android/music/ui/cardlib/model/Document;)V

    goto :goto_0
.end method
