.class public Lcom/google/android/music/art/ArtResolver;
.super Ljava/lang/Object;
.source "ArtResolver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/art/ArtResolver$13;
    }
.end annotation


# static fields
.field static final LOGV:Z

.field private static final PREFERRED_CONFIG:Landroid/graphics/Bitmap$Config;

.field private static final sBackgroundBitmapWorker:Lcom/google/android/music/utils/LoggableHandler;

.field private static final sBackgroundWorker:Lcom/google/android/music/utils/LoggableHandler;

.field private static volatile sInstance:Lcom/google/android/music/art/ArtResolver;


# instance fields
.field private final mAppContext:Landroid/content/Context;

.field private final mArtLoader:Lcom/google/android/music/art/ArtLoader;

.field private mMemoryCache:Lcom/google/android/music/art/BitmapLruCache;

.field private final mMemoryCacheLock:Ljava/lang/Object;

.field private mMemoryCacheUsers:I

.field private final mRequestMap:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Lcom/google/android/music/art/ArtKey",
            "<*>;",
            "Lcom/google/android/music/art/ArtRequest",
            "<*>;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 59
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->ART_RESOLVER:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/art/ArtResolver;->LOGV:Z

    .line 60
    sget-object v0, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    sput-object v0, Lcom/google/android/music/art/ArtResolver;->PREFERRED_CONFIG:Landroid/graphics/Bitmap$Config;

    .line 70
    new-instance v0, Lcom/google/android/music/utils/LoggableHandler;

    const-string v1, "ArtResolverWorker"

    invoke-direct {v0, v1}, Lcom/google/android/music/utils/LoggableHandler;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/music/art/ArtResolver;->sBackgroundWorker:Lcom/google/android/music/utils/LoggableHandler;

    .line 82
    new-instance v0, Lcom/google/android/music/utils/LoggableHandler;

    const-string v1, "ArtBitmapWorker"

    invoke-direct {v0, v1}, Lcom/google/android/music/utils/LoggableHandler;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/music/art/ArtResolver;->sBackgroundBitmapWorker:Lcom/google/android/music/utils/LoggableHandler;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 156
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/art/ArtResolver;->mRequestMap:Ljava/util/concurrent/ConcurrentHashMap;

    .line 96
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/art/ArtResolver;->mMemoryCacheLock:Ljava/lang/Object;

    .line 97
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/music/art/ArtResolver;->mMemoryCacheUsers:I

    .line 157
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/art/ArtResolver;->mAppContext:Landroid/content/Context;

    .line 158
    invoke-static {p1}, Lcom/google/android/music/art/ArtLoaderFactory;->getArtLoader(Landroid/content/Context;)Lcom/google/android/music/art/ArtLoader;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/art/ArtResolver;->mArtLoader:Lcom/google/android/music/art/ArtLoader;

    .line 159
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/art/ArtResolver;Ljava/lang/String;Lcom/google/android/music/art/ArtRequest;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/art/ArtResolver;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Lcom/google/android/music/art/ArtRequest;

    .prologue
    .line 52
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/art/ArtResolver;->getGenericArtForUrl(Ljava/lang/String;Lcom/google/android/music/art/ArtRequest;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/music/art/ArtResolver;Lcom/google/android/music/art/ArtRequest;JLjava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/art/ArtResolver;
    .param p1, "x1"    # Lcom/google/android/music/art/ArtRequest;
    .param p2, "x2"    # J
    .param p4, "x3"    # Ljava/lang/String;

    .prologue
    .line 52
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/music/art/ArtResolver;->processArtistArtCore(Lcom/google/android/music/art/ArtRequest;JLjava/lang/String;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/music/art/ArtResolver;Lcom/google/android/music/art/ContainerArtRequest;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/art/ArtResolver;
    .param p1, "x1"    # Lcom/google/android/music/art/ContainerArtRequest;

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lcom/google/android/music/art/ArtResolver;->startContainerTopSongsLookup(Lcom/google/android/music/art/ContainerArtRequest;)V

    return-void
.end method

.method static synthetic access$1100(Lcom/google/android/music/art/ArtResolver;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/art/ArtResolver;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/music/art/ArtResolver;->mMemoryCacheLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/android/music/art/ArtResolver;)Lcom/google/android/music/art/BitmapLruCache;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/art/ArtResolver;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/music/art/ArtResolver;->mMemoryCache:Lcom/google/android/music/art/BitmapLruCache;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/music/art/ArtResolver;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/art/ArtResolver;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/music/art/ArtResolver;->mAppContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/music/art/ArtResolver;Lcom/google/android/music/art/ArtRequest;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/art/ArtResolver;
    .param p1, "x1"    # Lcom/google/android/music/art/ArtRequest;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 52
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/art/ArtResolver;->startGenericUrlLookup(Lcom/google/android/music/art/ArtRequest;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/music/art/ArtResolver;Lcom/google/android/music/art/ContainerArtRequest;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/art/ArtResolver;
    .param p1, "x1"    # Lcom/google/android/music/art/ContainerArtRequest;

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lcom/google/android/music/art/ArtResolver;->startContainerAlbumLookup(Lcom/google/android/music/art/ContainerArtRequest;)V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/music/art/ArtResolver;Lcom/google/android/music/art/ContainerArtRequest;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/art/ArtResolver;
    .param p1, "x1"    # Lcom/google/android/music/art/ContainerArtRequest;

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lcom/google/android/music/art/ArtResolver;->startContainerLocalRadioLookup(Lcom/google/android/music/art/ContainerArtRequest;)V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/music/art/ArtResolver;Lcom/google/android/music/art/ContainerArtRequest;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/art/ArtResolver;
    .param p1, "x1"    # Lcom/google/android/music/art/ContainerArtRequest;

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lcom/google/android/music/art/ArtResolver;->startContainerArtistLookup(Lcom/google/android/music/art/ContainerArtRequest;)V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/music/art/ArtResolver;Lcom/google/android/music/art/ContainerArtRequest;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/art/ArtResolver;
    .param p1, "x1"    # Lcom/google/android/music/art/ContainerArtRequest;

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lcom/google/android/music/art/ArtResolver;->startContainerPlaylistLookup(Lcom/google/android/music/art/ContainerArtRequest;)V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/music/art/ArtResolver;Lcom/google/android/music/art/ContainerArtRequest;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/art/ArtResolver;
    .param p1, "x1"    # Lcom/google/android/music/art/ContainerArtRequest;

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lcom/google/android/music/art/ArtResolver;->startContainerGenreLookup(Lcom/google/android/music/art/ContainerArtRequest;)V

    return-void
.end method

.method static synthetic access$900(Lcom/google/android/music/art/ArtResolver;Lcom/google/android/music/art/ContainerArtRequest;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/art/ArtResolver;
    .param p1, "x1"    # Lcom/google/android/music/art/ContainerArtRequest;

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lcom/google/android/music/art/ArtResolver;->startContainerSingleSongLookup(Lcom/google/android/music/art/ContainerArtRequest;)V

    return-void
.end method

.method private assertOnBackgroundWorker()V
    .locals 5

    .prologue
    .line 980
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    .line 981
    .local v2, "mainThreadLooper":Landroid/os/Looper;
    sget-object v3, Lcom/google/android/music/utils/async/AsyncWorkers;->sUIBackgroundWorker:Lcom/google/android/music/utils/LoggableHandler;

    invoke-virtual {v3}, Lcom/google/android/music/utils/LoggableHandler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    .line 982
    .local v0, "appBackgroundLooper":Landroid/os/Looper;
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    .line 983
    .local v1, "looper":Landroid/os/Looper;
    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 984
    :cond_0
    new-instance v3, Ljava/lang/RuntimeException;

    const-string v4, "Not on background worker"

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 986
    :cond_1
    return-void
.end method

.method private decodeAndCapMultiUrlString(Ljava/lang/String;)[Ljava/lang/String;
    .locals 6
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 967
    invoke-static {p1}, Lcom/google/android/music/utils/MusicUtils;->decodeStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 968
    .local v1, "arts":[Ljava/lang/String;
    const/4 v0, 0x0

    .line 969
    .local v0, "artCount":I
    array-length v4, v1

    if-lez v4, :cond_0

    .line 970
    array-length v4, v1

    const/4 v5, 0x4

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 972
    :cond_0
    new-array v3, v0, [Ljava/lang/String;

    .line 973
    .local v3, "result":[Ljava/lang/String;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v4, v3

    if-ge v2, v4, :cond_1

    .line 974
    aget-object v4, v1, v2

    aput-object v4, v3, v2

    .line 973
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 976
    :cond_1
    return-object v3
.end method

.method private extractAlbumIdsFromUri(Landroid/net/Uri;Z)Ljava/util/LinkedHashSet;
    .locals 12
    .param p1, "itemsUri"    # Landroid/net/Uri;
    .param p2, "includeExternal"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Z)",
            "Ljava/util/LinkedHashSet",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 890
    const/4 v11, 0x0

    .line 891
    .local v11, "c":Landroid/database/Cursor;
    new-instance v10, Ljava/util/LinkedHashSet;

    invoke-direct {v10}, Ljava/util/LinkedHashSet;-><init>()V

    .line 892
    .local v10, "albumIds":Ljava/util/LinkedHashSet;, "Ljava/util/LinkedHashSet<Ljava/lang/Long;>;"
    if-nez p1, :cond_1

    .line 911
    :cond_0
    :goto_0
    return-object v10

    .line 894
    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/art/ArtResolver;->mAppContext:Landroid/content/Context;

    sget-object v2, Lcom/google/android/music/art/ArtTypeHandler;->ALBUM_ID_COLUMNS:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v1, p1

    move v7, p2

    invoke-static/range {v0 .. v7}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZZ)Landroid/database/Cursor;

    move-result-object v11

    .line 896
    if-eqz v11, :cond_3

    .line 897
    :cond_2
    :goto_1
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 898
    const/4 v0, 0x0

    invoke-interface {v11, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 899
    const/4 v0, 0x0

    invoke-interface {v11, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 900
    .local v8, "albumId":J
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v10, v0}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 905
    .end local v8    # "albumId":J
    :catchall_0
    move-exception v0

    invoke-static {v11}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0

    :cond_3
    invoke-static {v11}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 907
    sget-boolean v0, Lcom/google/android/music/art/ArtResolver;->LOGV:Z

    if-eqz v0, :cond_0

    .line 908
    const-string v0, "ArtResolver"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "extractAlbumIdsFromUri: uri: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", cursor mined: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v10}, Ljava/util/LinkedHashSet;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " album covers found"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private extractSharedWithMePlaylistArt(Ljava/lang/String;)Ljava/util/LinkedHashSet;
    .locals 4
    .param p1, "shareToken"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/LinkedHashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 859
    invoke-static {p1}, Lcom/google/android/music/store/MusicContent$SharedWithMePlaylist$Members;->getUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 860
    .local v0, "itemsUri":Landroid/net/Uri;
    sget-object v1, Lcom/google/android/music/art/ArtTypeHandler;->XAUDIO_ALBUM_ART_COLUMNS:[Ljava/lang/String;

    const/4 v2, 0x1

    const v3, 0x7fffffff

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/android/music/art/ArtResolver;->extractStringIdsFromUri(Landroid/net/Uri;[Ljava/lang/String;ZI)Ljava/util/LinkedHashSet;

    move-result-object v1

    return-object v1
.end method

.method private extractStringIdsFromUri(Landroid/net/Uri;[Ljava/lang/String;ZI)Ljava/util/LinkedHashSet;
    .locals 11
    .param p1, "itemsUri"    # Landroid/net/Uri;
    .param p2, "columns"    # [Ljava/lang/String;
    .param p3, "includeExternal"    # Z
    .param p4, "limit"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "[",
            "Ljava/lang/String;",
            "ZI)",
            "Ljava/util/LinkedHashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 865
    const/4 v9, 0x0

    .line 866
    .local v9, "c":Landroid/database/Cursor;
    new-instance v8, Ljava/util/LinkedHashSet;

    invoke-direct {v8}, Ljava/util/LinkedHashSet;-><init>()V

    .line 868
    .local v8, "albumIds":Ljava/util/LinkedHashSet;, "Ljava/util/LinkedHashSet<Ljava/lang/String;>;"
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/art/ArtResolver;->mAppContext:Landroid/content/Context;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v1, p1

    move-object v2, p2

    move v7, p3

    invoke-static/range {v0 .. v7}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZZ)Landroid/database/Cursor;

    move-result-object v9

    .line 870
    if-eqz v9, :cond_1

    .line 871
    const/4 v10, 0x0

    .line 872
    .local v10, "found":I
    :cond_0
    :goto_0
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    if-ge v10, p4, :cond_1

    .line 873
    const/4 v0, 0x0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 874
    const/4 v0, 0x0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 875
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 880
    .end local v10    # "found":I
    :cond_1
    invoke-static {v9}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 882
    sget-boolean v0, Lcom/google/android/music/art/ArtResolver;->LOGV:Z

    if-eqz v0, :cond_2

    .line 883
    const-string v0, "ArtResolver"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "extractStringIdsFromUri: uri: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", cursor mined: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v8}, Ljava/util/LinkedHashSet;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " album covers found"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 886
    :cond_2
    return-object v8

    .line 880
    :catchall_0
    move-exception v0

    invoke-static {v9}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method private getAlbumArtForId(JLcom/google/android/music/art/ArtRequest;)Z
    .locals 3
    .param p1, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/google/android/music/art/ArtRequest",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 999
    .local p3, "request":Lcom/google/android/music/art/ArtRequest;, "Lcom/google/android/music/art/ArtRequest<*>;"
    invoke-direct {p0}, Lcom/google/android/music/art/ArtResolver;->assertOnBackgroundWorker()V

    .line 1001
    iget-object v1, p0, Lcom/google/android/music/art/ArtResolver;->mAppContext:Landroid/content/Context;

    invoke-static {v1, p1, p2}, Lcom/google/android/music/store/MusicContent$Albums;->getAlbumArtLocation(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    .line 1002
    .local v0, "remoteUrl":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1003
    invoke-direct {p0, v0, p3}, Lcom/google/android/music/art/ArtResolver;->getAlbumArtForUrl(Ljava/lang/String;Lcom/google/android/music/art/ArtRequest;)Z

    move-result v1

    .line 1005
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private getAlbumArtForUrl(Ljava/lang/String;Lcom/google/android/music/art/ArtRequest;)Z
    .locals 5
    .param p1, "url"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/music/art/ArtRequest",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .local p2, "request":Lcom/google/android/music/art/ArtRequest;, "Lcom/google/android/music/art/ArtRequest<*>;"
    const/4 v4, 0x1

    .line 1021
    iget-object v2, p0, Lcom/google/android/music/art/ArtResolver;->mAppContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/music/utils/ViewUtils;->getShortestEdge(Landroid/content/Context;)I

    move-result v1

    .line 1022
    .local v1, "edge":I
    invoke-virtual {p2, p1}, Lcom/google/android/music/art/ArtRequest;->addNeededItem(Ljava/lang/Object;)V

    .line 1025
    :try_start_0
    iget-object v2, p0, Lcom/google/android/music/art/ArtResolver;->mArtLoader:Lcom/google/android/music/art/ArtLoader;

    sget-object v3, Lcom/google/android/music/art/ArtLoader$DownloadMode;->SYNC_DOWNLOAD:Lcom/google/android/music/art/ArtLoader$DownloadMode;

    invoke-interface {v2, p1, v3}, Lcom/google/android/music/art/ArtLoader;->getArtFileDescriptorSync(Ljava/lang/String;Lcom/google/android/music/art/ArtLoader$DownloadMode;)Landroid/os/ParcelFileDescriptor;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1032
    :cond_0
    :goto_0
    invoke-virtual {p2}, Lcom/google/android/music/art/ArtRequest;->isPrefetchOnlyRequest()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-direct {p0, p1}, Lcom/google/android/music/art/ArtResolver;->requestStillNeedsBitmap(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1045
    :cond_1
    :goto_1
    return v4

    .line 1026
    :catch_0
    move-exception v0

    .line 1027
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-direct {p0, p1}, Lcom/google/android/music/art/ArtResolver;->requestStillNeedsBitmap(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1028
    iget-object v2, p0, Lcom/google/android/music/art/ArtResolver;->mAppContext:Landroid/content/Context;

    invoke-virtual {p2, v2, p1}, Lcom/google/android/music/art/ArtRequest;->notifyError(Landroid/content/Context;Ljava/lang/Object;)Z

    goto :goto_0

    .line 1036
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_2
    sget-object v2, Lcom/google/android/music/art/ArtResolver;->sBackgroundBitmapWorker:Lcom/google/android/music/utils/LoggableHandler;

    new-instance v3, Lcom/google/android/music/art/ArtResolver$11;

    invoke-direct {v3, p0, p1, v1, p2}, Lcom/google/android/music/art/ArtResolver$11;-><init>(Lcom/google/android/music/art/ArtResolver;Ljava/lang/String;ILcom/google/android/music/art/ArtRequest;)V

    invoke-static {v2, v3}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsync(Lcom/google/android/music/utils/LoggableHandler;Ljava/lang/Runnable;)V

    goto :goto_1
.end method

.method private getAlbumArtUrlForMetajamId(Ljava/lang/String;)Ljava/lang/String;
    .locals 11
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    const/4 v10, 0x0

    .line 1056
    const/4 v9, 0x0

    .line 1057
    .local v9, "c":Landroid/database/Cursor;
    invoke-static {p1}, Lcom/google/android/music/store/MusicContent$XAudio;->getNautilusAudioUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 1058
    .local v1, "uri":Landroid/net/Uri;
    if-nez v1, :cond_1

    .line 1072
    :cond_0
    :goto_0
    return-object v10

    .line 1059
    :cond_1
    const/4 v8, 0x0

    .line 1061
    .local v8, "artUrl":Ljava/lang/String;
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/art/ArtResolver;->mAppContext:Landroid/content/Context;

    sget-object v2, Lcom/google/android/music/art/ArtTypeHandler;->XAUDIO_ALBUM_ART_COLUMNS:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x1

    invoke-static/range {v0 .. v7}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZZ)Landroid/database/Cursor;

    move-result-object v9

    .line 1063
    if-eqz v9, :cond_2

    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1064
    const/4 v0, 0x0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->isNull(I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_3

    move-object v8, v10

    .line 1067
    :cond_2
    :goto_1
    invoke-static {v9}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 1069
    if-eqz v8, :cond_0

    move-object v10, v8

    .line 1072
    goto :goto_0

    .line 1064
    :cond_3
    const/4 v0, 0x0

    :try_start_1
    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v8

    goto :goto_1

    .line 1067
    :catchall_0
    move-exception v0

    invoke-static {v9}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method private getArt(Lcom/google/android/music/cloudclient/QuizArtistJson;Lcom/google/android/music/art/ArtType;Z)Lcom/google/android/music/art/ArtRequest;
    .locals 8
    .param p1, "artist"    # Lcom/google/android/music/cloudclient/QuizArtistJson;
    .param p2, "type"    # Lcom/google/android/music/art/ArtType;
    .param p3, "isPrefetch"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/music/cloudclient/QuizArtistJson;",
            "Lcom/google/android/music/art/ArtType;",
            "Z)",
            "Lcom/google/android/music/art/ArtRequest",
            "<",
            "Lcom/google/android/music/cloudclient/QuizArtistJson;",
            ">;"
        }
    .end annotation

    .prologue
    .line 370
    sget-boolean v0, Lcom/google/android/music/art/ArtResolver;->LOGV:Z

    if-eqz v0, :cond_0

    .line 371
    const-string v0, "ArtResolver"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getArt(QuizArtistJson): artist="

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, ", type="

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 373
    :cond_0
    new-instance v3, Lcom/google/android/music/art/ArtRequest;

    invoke-direct {v3, p1, p2}, Lcom/google/android/music/art/ArtRequest;-><init>(Ljava/lang/Object;Lcom/google/android/music/art/ArtType;)V

    .line 374
    .local v3, "request":Lcom/google/android/music/art/ArtRequest;, "Lcom/google/android/music/art/ArtRequest<Lcom/google/android/music/cloudclient/QuizArtistJson;>;"
    invoke-virtual {v3, p3}, Lcom/google/android/music/art/ArtRequest;->setPrefetchOnlyRequest(Z)V

    .line 375
    invoke-virtual {v3}, Lcom/google/android/music/art/ArtRequest;->getArtKey()Lcom/google/android/music/art/ArtKey;

    move-result-object v6

    .line 376
    .local v6, "possibleKey":Lcom/google/android/music/art/ArtKey;, "Lcom/google/android/music/art/ArtKey<Lcom/google/android/music/cloudclient/QuizArtistJson;>;"
    iget-object v0, p0, Lcom/google/android/music/art/ArtResolver;->mRequestMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, v6}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 377
    iget-object v0, p0, Lcom/google/android/music/art/ArtResolver;->mRequestMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, v6}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/art/ArtRequest;

    move-object v3, v0

    .line 407
    .end local v3    # "request":Lcom/google/android/music/art/ArtRequest;, "Lcom/google/android/music/art/ArtRequest<Lcom/google/android/music/cloudclient/QuizArtistJson;>;"
    :cond_1
    :goto_0
    return-object v3

    .line 379
    .restart local v3    # "request":Lcom/google/android/music/art/ArtRequest;, "Lcom/google/android/music/art/ArtRequest<Lcom/google/android/music/cloudclient/QuizArtistJson;>;"
    :cond_2
    if-nez p3, :cond_3

    .line 380
    iget-object v0, p0, Lcom/google/android/music/art/ArtResolver;->mRequestMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, v6, v3}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 384
    :cond_3
    iget-object v4, p1, Lcom/google/android/music/cloudclient/QuizArtistJson;->mArtistId:Ljava/lang/String;

    .line 385
    .local v4, "artistMetajamId":Ljava/lang/String;
    iget-object v2, p1, Lcom/google/android/music/cloudclient/QuizArtistJson;->mArtistArtUrl:Ljava/lang/String;

    .line 386
    .local v2, "artUrl":Ljava/lang/String;
    sget-object v0, Lcom/google/android/music/art/ArtResolver$13;->$SwitchMap$com$google$android$music$art$ArtType:[I

    invoke-virtual {p2}, Lcom/google/android/music/art/ArtType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 388
    :pswitch_0
    invoke-direct {p0, v2, v3}, Lcom/google/android/music/art/ArtResolver;->getArtFromCache(Ljava/lang/Object;Lcom/google/android/music/art/ArtRequest;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 391
    sget-object v7, Lcom/google/android/music/art/ArtResolver;->sBackgroundWorker:Lcom/google/android/music/utils/LoggableHandler;

    new-instance v0, Lcom/google/android/music/art/ArtResolver$1;

    move-object v1, p0

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/music/art/ArtResolver$1;-><init>(Lcom/google/android/music/art/ArtResolver;Ljava/lang/String;Lcom/google/android/music/art/ArtRequest;Ljava/lang/String;Lcom/google/android/music/cloudclient/QuizArtistJson;)V

    invoke-static {v7, v0}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsync(Lcom/google/android/music/utils/LoggableHandler;Ljava/lang/Runnable;)V

    goto :goto_0

    .line 386
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method

.method private getArt(Lcom/google/android/music/cloudclient/QuizGenreJson;Lcom/google/android/music/art/ArtType;Z)Lcom/google/android/music/art/ArtRequest;
    .locals 6
    .param p1, "genre"    # Lcom/google/android/music/cloudclient/QuizGenreJson;
    .param p2, "type"    # Lcom/google/android/music/art/ArtType;
    .param p3, "isPrefetch"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/music/cloudclient/QuizGenreJson;",
            "Lcom/google/android/music/art/ArtType;",
            "Z)",
            "Lcom/google/android/music/art/ArtRequest",
            "<",
            "Lcom/google/android/music/cloudclient/QuizGenreJson;",
            ">;"
        }
    .end annotation

    .prologue
    .line 441
    sget-boolean v3, Lcom/google/android/music/art/ArtResolver;->LOGV:Z

    if-eqz v3, :cond_0

    .line 442
    const-string v3, "ArtResolver"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getArt(QuizGenreJson): genre="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", type="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 444
    :cond_0
    new-instance v2, Lcom/google/android/music/art/ArtRequest;

    invoke-direct {v2, p1, p2}, Lcom/google/android/music/art/ArtRequest;-><init>(Ljava/lang/Object;Lcom/google/android/music/art/ArtType;)V

    .line 445
    .local v2, "request":Lcom/google/android/music/art/ArtRequest;, "Lcom/google/android/music/art/ArtRequest<Lcom/google/android/music/cloudclient/QuizGenreJson;>;"
    invoke-virtual {v2, p3}, Lcom/google/android/music/art/ArtRequest;->setPrefetchOnlyRequest(Z)V

    .line 446
    invoke-virtual {v2}, Lcom/google/android/music/art/ArtRequest;->getArtKey()Lcom/google/android/music/art/ArtKey;

    move-result-object v1

    .line 447
    .local v1, "possibleKey":Lcom/google/android/music/art/ArtKey;, "Lcom/google/android/music/art/ArtKey<Lcom/google/android/music/cloudclient/QuizGenreJson;>;"
    iget-object v3, p0, Lcom/google/android/music/art/ArtResolver;->mRequestMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v3, v1}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 448
    iget-object v3, p0, Lcom/google/android/music/art/ArtResolver;->mRequestMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v3, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/music/art/ArtRequest;

    move-object v2, v3

    .line 471
    .end local v2    # "request":Lcom/google/android/music/art/ArtRequest;, "Lcom/google/android/music/art/ArtRequest<Lcom/google/android/music/cloudclient/QuizGenreJson;>;"
    :cond_1
    :goto_0
    return-object v2

    .line 450
    .restart local v2    # "request":Lcom/google/android/music/art/ArtRequest;, "Lcom/google/android/music/art/ArtRequest<Lcom/google/android/music/cloudclient/QuizGenreJson;>;"
    :cond_2
    if-nez p3, :cond_3

    .line 451
    iget-object v3, p0, Lcom/google/android/music/art/ArtResolver;->mRequestMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v3, v1, v2}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 453
    :cond_3
    iget-object v0, p1, Lcom/google/android/music/cloudclient/QuizGenreJson;->mImageUrl:Ljava/lang/String;

    .line 454
    .local v0, "artUrl":Ljava/lang/String;
    sget-object v3, Lcom/google/android/music/art/ArtResolver$13;->$SwitchMap$com$google$android$music$art$ArtType:[I

    invoke-virtual {p2}, Lcom/google/android/music/art/ArtType;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    goto :goto_0

    .line 456
    :pswitch_0
    invoke-direct {p0, v0, v2}, Lcom/google/android/music/art/ArtResolver;->getArtFromCache(Ljava/lang/Object;Lcom/google/android/music/art/ArtRequest;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 459
    sget-object v3, Lcom/google/android/music/art/ArtResolver;->sBackgroundWorker:Lcom/google/android/music/utils/LoggableHandler;

    new-instance v4, Lcom/google/android/music/art/ArtResolver$2;

    invoke-direct {v4, p0, v0, v2}, Lcom/google/android/music/art/ArtResolver$2;-><init>(Lcom/google/android/music/art/ArtResolver;Ljava/lang/String;Lcom/google/android/music/art/ArtRequest;)V

    invoke-static {v3, v4}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsync(Lcom/google/android/music/utils/LoggableHandler;Ljava/lang/Runnable;)V

    goto :goto_0

    .line 454
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method

.method private getArt(Lcom/google/android/music/store/ContainerDescriptor;Lcom/google/android/music/mix/MixDescriptor;Lcom/google/android/music/art/ArtType;Z)Lcom/google/android/music/art/ContainerArtRequest;
    .locals 4
    .param p1, "containerDescriptor"    # Lcom/google/android/music/store/ContainerDescriptor;
    .param p2, "mixDescriptor"    # Lcom/google/android/music/mix/MixDescriptor;
    .param p3, "type"    # Lcom/google/android/music/art/ArtType;
    .param p4, "isPrefetch"    # Z

    .prologue
    .line 308
    sget-boolean v1, Lcom/google/android/music/art/ArtResolver;->LOGV:Z

    if-eqz v1, :cond_0

    .line 309
    const-string v1, "ArtResolver"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getArt containerDescriptor="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "mixDescriptor="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", type="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "isPrefetch="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 313
    :cond_0
    new-instance v0, Lcom/google/android/music/art/ContainerMixDescriptorPair;

    invoke-direct {v0, p1, p2}, Lcom/google/android/music/art/ContainerMixDescriptorPair;-><init>(Lcom/google/android/music/store/ContainerDescriptor;Lcom/google/android/music/mix/MixDescriptor;)V

    .line 315
    .local v0, "artDescriptor":Lcom/google/android/music/art/ContainerMixDescriptorPair;
    invoke-direct {p0, v0, p3, p4}, Lcom/google/android/music/art/ArtResolver;->getContainerArt(Lcom/google/android/music/art/ContainerMixDescriptorPair;Lcom/google/android/music/art/ArtType;Z)Lcom/google/android/music/art/ContainerArtRequest;

    move-result-object v1

    return-object v1
.end method

.method private getArtFromCache(Ljava/lang/Object;Lcom/google/android/music/art/ArtRequest;)Z
    .locals 6
    .param p1, "key"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Lcom/google/android/music/art/ArtRequest",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .local p2, "request":Lcom/google/android/music/art/ArtRequest;, "Lcom/google/android/music/art/ArtRequest<*>;"
    const/4 v3, 0x0

    .line 215
    if-nez p1, :cond_0

    move v2, v3

    .line 240
    :goto_0
    return v2

    .line 216
    :cond_0
    const/4 v1, 0x0

    .line 217
    .local v1, "cached":Landroid/graphics/Bitmap;
    iget-object v4, p0, Lcom/google/android/music/art/ArtResolver;->mMemoryCacheLock:Ljava/lang/Object;

    monitor-enter v4

    .line 218
    :try_start_0
    iget-object v2, p0, Lcom/google/android/music/art/ArtResolver;->mMemoryCache:Lcom/google/android/music/art/BitmapLruCache;

    if-eqz v2, :cond_1

    .line 219
    iget-object v2, p0, Lcom/google/android/music/art/ArtResolver;->mMemoryCache:Lcom/google/android/music/art/BitmapLruCache;

    invoke-virtual {v2, p1}, Lcom/google/android/music/art/BitmapLruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Landroid/graphics/Bitmap;

    move-object v1, v0

    .line 222
    if-eqz v1, :cond_1

    .line 223
    sget-object v2, Lcom/google/android/music/art/ArtResolver;->PREFERRED_CONFIG:Landroid/graphics/Bitmap$Config;

    const/4 v5, 0x0

    invoke-virtual {v1, v2, v5}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 226
    :cond_1
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 227
    if-eqz v1, :cond_3

    .line 228
    sget-boolean v2, Lcom/google/android/music/art/ArtResolver;->LOGV:Z

    if-eqz v2, :cond_2

    .line 229
    const-string v2, "ArtResolver"

    const-string v3, "getArtFromCache: cache hit"

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    :cond_2
    invoke-virtual {p2, p1}, Lcom/google/android/music/art/ArtRequest;->addNeededItem(Ljava/lang/Object;)V

    .line 233
    iget-object v2, p0, Lcom/google/android/music/art/ArtResolver;->mAppContext:Landroid/content/Context;

    invoke-virtual {p2, v2, p1, v1}, Lcom/google/android/music/art/ArtRequest;->notifyImageLoaded(Landroid/content/Context;Ljava/lang/Object;Landroid/graphics/Bitmap;)Z

    .line 234
    const/4 v2, 0x1

    goto :goto_0

    .line 226
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    .line 236
    :cond_3
    sget-boolean v2, Lcom/google/android/music/art/ArtResolver;->LOGV:Z

    if-eqz v2, :cond_4

    .line 237
    const-string v2, "ArtResolver"

    const-string v4, "getArtFromCache): cache miss"

    invoke-static {v2, v4}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    move v2, v3

    .line 240
    goto :goto_0
.end method

.method private getAutoPlaylistForContainer(Lcom/google/android/music/store/ContainerDescriptor;)Lcom/google/android/music/medialist/AutoPlaylistSongList;
    .locals 6
    .param p1, "containerDescriptor"    # Lcom/google/android/music/store/ContainerDescriptor;

    .prologue
    .line 917
    const-wide/16 v2, -0x1

    .line 918
    .local v2, "playlistId":J
    sget-object v4, Lcom/google/android/music/art/ArtResolver$13;->$SwitchMap$com$google$android$music$store$ContainerDescriptor$Type:[I

    invoke-virtual {p1}, Lcom/google/android/music/store/ContainerDescriptor;->getType()Lcom/google/android/music/store/ContainerDescriptor$Type;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/music/store/ContainerDescriptor$Type;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 932
    :goto_0
    :pswitch_0
    iget-object v4, p0, Lcom/google/android/music/art/ArtResolver;->mAppContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/google/android/music/ui/UIStateManager;->getInstance(Landroid/content/Context;)Lcom/google/android/music/ui/UIStateManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v1

    .line 933
    .local v1, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    const/4 v4, 0x0

    invoke-static {v2, v3, v4, v1}, Lcom/google/android/music/medialist/AutoPlaylistSongList;->getAutoPlaylist(JZLcom/google/android/music/preferences/MusicPreferences;)Lcom/google/android/music/medialist/AutoPlaylistSongList;

    move-result-object v0

    .line 935
    .local v0, "autoPlaylist":Lcom/google/android/music/medialist/AutoPlaylistSongList;
    return-object v0

    .line 920
    .end local v0    # "autoPlaylist":Lcom/google/android/music/medialist/AutoPlaylistSongList;
    .end local v1    # "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    :pswitch_1
    const-wide/16 v2, -0x1

    .line 921
    goto :goto_0

    .line 923
    :pswitch_2
    const-wide/16 v2, -0x3

    .line 924
    goto :goto_0

    .line 926
    :pswitch_3
    const-wide/16 v2, -0x4

    .line 927
    goto :goto_0

    .line 929
    :pswitch_4
    const-wide/16 v2, -0x2

    goto :goto_0

    .line 918
    nop

    :pswitch_data_0
    .packed-switch 0x9
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method static getBackgroundWorker()Lcom/google/android/music/utils/LoggableHandler;
    .locals 1

    .prologue
    .line 75
    sget-object v0, Lcom/google/android/music/art/ArtResolver;->sBackgroundWorker:Lcom/google/android/music/utils/LoggableHandler;

    return-object v0
.end method

.method private getContainerArt(Lcom/google/android/music/art/ContainerMixDescriptorPair;Lcom/google/android/music/art/ArtType;Z)Lcom/google/android/music/art/ContainerArtRequest;
    .locals 4
    .param p1, "pair"    # Lcom/google/android/music/art/ContainerMixDescriptorPair;
    .param p2, "type"    # Lcom/google/android/music/art/ArtType;
    .param p3, "isPrefetch"    # Z

    .prologue
    .line 321
    new-instance v0, Lcom/google/android/music/art/ArtKey;

    invoke-direct {v0, p2, p1}, Lcom/google/android/music/art/ArtKey;-><init>(Lcom/google/android/music/art/ArtType;Ljava/lang/Object;)V

    .line 323
    .local v0, "possibleKey":Lcom/google/android/music/art/ArtKey;, "Lcom/google/android/music/art/ArtKey<Lcom/google/android/music/art/ContainerMixDescriptorPair;>;"
    iget-object v2, p0, Lcom/google/android/music/art/ArtResolver;->mRequestMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2, v0}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 324
    iget-object v2, p0, Lcom/google/android/music/art/ArtResolver;->mRequestMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2, v0}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/art/ContainerArtRequest;

    .line 336
    :goto_0
    return-object v2

    .line 326
    :cond_0
    sget-object v2, Lcom/google/android/music/art/ArtResolver$13;->$SwitchMap$com$google$android$music$art$ArtType:[I

    invoke-virtual {p2}, Lcom/google/android/music/art/ArtType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 333
    new-instance v2, Ljava/lang/UnsupportedOperationException;

    const-string v3, "Support for this art type not yet implemented"

    invoke-direct {v2, v3}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 329
    :pswitch_0
    iget-object v2, p1, Lcom/google/android/music/art/ContainerMixDescriptorPair;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    iget-object v3, p1, Lcom/google/android/music/art/ContainerMixDescriptorPair;->mMixDescriptor:Lcom/google/android/music/mix/MixDescriptor;

    invoke-direct {p0, v2, v3, p3}, Lcom/google/android/music/art/ArtResolver;->getQueueHeaderArt(Lcom/google/android/music/store/ContainerDescriptor;Lcom/google/android/music/mix/MixDescriptor;Z)Lcom/google/android/music/art/ContainerArtRequest;

    move-result-object v1

    .local v1, "result":Lcom/google/android/music/art/ContainerArtRequest;
    move-object v2, v1

    .line 336
    goto :goto_0

    .line 326
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private getGenericArtForUrl(Ljava/lang/String;Lcom/google/android/music/art/ArtRequest;)Z
    .locals 4
    .param p1, "url"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/music/art/ArtRequest",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .local p2, "request":Lcom/google/android/music/art/ArtRequest;, "Lcom/google/android/music/art/ArtRequest<*>;"
    const/4 v3, 0x1

    .line 1084
    invoke-direct {p0}, Lcom/google/android/music/art/ArtResolver;->assertOnBackgroundWorker()V

    .line 1085
    invoke-virtual {p2, p1}, Lcom/google/android/music/art/ArtRequest;->addNeededItem(Ljava/lang/Object;)V

    .line 1088
    :try_start_0
    iget-object v1, p0, Lcom/google/android/music/art/ArtResolver;->mArtLoader:Lcom/google/android/music/art/ArtLoader;

    sget-object v2, Lcom/google/android/music/art/ArtLoader$DownloadMode;->SYNC_DOWNLOAD:Lcom/google/android/music/art/ArtLoader$DownloadMode;

    invoke-interface {v1, p1, v2}, Lcom/google/android/music/art/ArtLoader;->getArtFileDescriptorSync(Ljava/lang/String;Lcom/google/android/music/art/ArtLoader$DownloadMode;)Landroid/os/ParcelFileDescriptor;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1095
    :cond_0
    :goto_0
    invoke-virtual {p2}, Lcom/google/android/music/art/ArtRequest;->isPrefetchOnlyRequest()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-direct {p0, p1}, Lcom/google/android/music/art/ArtResolver;->requestStillNeedsBitmap(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1117
    :cond_1
    :goto_1
    return v3

    .line 1089
    :catch_0
    move-exception v0

    .line 1090
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-direct {p0, p1}, Lcom/google/android/music/art/ArtResolver;->requestStillNeedsBitmap(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1091
    iget-object v1, p0, Lcom/google/android/music/art/ArtResolver;->mAppContext:Landroid/content/Context;

    invoke-virtual {p2, v1, p1}, Lcom/google/android/music/art/ArtRequest;->notifyError(Landroid/content/Context;Ljava/lang/Object;)Z

    goto :goto_0

    .line 1099
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_2
    sget-object v1, Lcom/google/android/music/art/ArtResolver;->sBackgroundBitmapWorker:Lcom/google/android/music/utils/LoggableHandler;

    new-instance v2, Lcom/google/android/music/art/ArtResolver$12;

    invoke-direct {v2, p0, p1, p2}, Lcom/google/android/music/art/ArtResolver$12;-><init>(Lcom/google/android/music/art/ArtResolver;Ljava/lang/String;Lcom/google/android/music/art/ArtRequest;)V

    invoke-static {v1, v2}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsync(Lcom/google/android/music/utils/LoggableHandler;Ljava/lang/Runnable;)V

    goto :goto_1
.end method

.method public static getInstance()Lcom/google/android/music/art/ArtResolver;
    .locals 2

    .prologue
    .line 187
    sget-object v0, Lcom/google/android/music/art/ArtResolver;->sInstance:Lcom/google/android/music/art/ArtResolver;

    if-nez v0, :cond_0

    .line 188
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "sInstance not yet constructed"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 190
    :cond_0
    sget-object v0, Lcom/google/android/music/art/ArtResolver;->sInstance:Lcom/google/android/music/art/ArtResolver;

    return-object v0
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/google/android/music/art/ArtResolver;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 169
    sget-object v0, Lcom/google/android/music/art/ArtResolver;->sInstance:Lcom/google/android/music/art/ArtResolver;

    if-nez v0, :cond_1

    .line 170
    const-class v1, Lcom/google/android/music/art/ArtResolver;

    monitor-enter v1

    .line 171
    :try_start_0
    sget-object v0, Lcom/google/android/music/art/ArtResolver;->sInstance:Lcom/google/android/music/art/ArtResolver;

    if-nez v0, :cond_0

    .line 172
    new-instance v0, Lcom/google/android/music/art/ArtResolver;

    invoke-direct {v0, p0}, Lcom/google/android/music/art/ArtResolver;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/music/art/ArtResolver;->sInstance:Lcom/google/android/music/art/ArtResolver;

    .line 174
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 176
    :cond_1
    sget-object v0, Lcom/google/android/music/art/ArtResolver;->sInstance:Lcom/google/android/music/art/ArtResolver;

    return-object v0

    .line 174
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private getQueueHeaderArt(Lcom/google/android/music/store/ContainerDescriptor;Lcom/google/android/music/mix/MixDescriptor;Z)Lcom/google/android/music/art/ContainerArtRequest;
    .locals 3
    .param p1, "containerDescriptor"    # Lcom/google/android/music/store/ContainerDescriptor;
    .param p2, "mixDescriptor"    # Lcom/google/android/music/mix/MixDescriptor;
    .param p3, "isPrefetch"    # Z

    .prologue
    .line 502
    new-instance v0, Lcom/google/android/music/art/ContainerArtRequest;

    sget-object v1, Lcom/google/android/music/art/ArtType;->QUEUE_HEADER:Lcom/google/android/music/art/ArtType;

    invoke-direct {v0, p1, p2, v1}, Lcom/google/android/music/art/ContainerArtRequest;-><init>(Lcom/google/android/music/store/ContainerDescriptor;Lcom/google/android/music/mix/MixDescriptor;Lcom/google/android/music/art/ArtType;)V

    .line 504
    .local v0, "request":Lcom/google/android/music/art/ContainerArtRequest;
    invoke-virtual {v0, p3}, Lcom/google/android/music/art/ContainerArtRequest;->setPrefetchOnlyRequest(Z)V

    .line 505
    if-nez p3, :cond_0

    .line 506
    iget-object v1, p0, Lcom/google/android/music/art/ArtResolver;->mRequestMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Lcom/google/android/music/art/ContainerArtRequest;->getArtKey()Lcom/google/android/music/art/ArtKey;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 508
    :cond_0
    sget-object v1, Lcom/google/android/music/art/ArtResolver$13;->$SwitchMap$com$google$android$music$store$ContainerDescriptor$Type:[I

    invoke-virtual {p1}, Lcom/google/android/music/store/ContainerDescriptor;->getType()Lcom/google/android/music/store/ContainerDescriptor$Type;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/music/store/ContainerDescriptor$Type;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 583
    :goto_0
    return-object v0

    .line 511
    :pswitch_0
    sget-object v1, Lcom/google/android/music/art/ArtResolver;->sBackgroundWorker:Lcom/google/android/music/utils/LoggableHandler;

    new-instance v2, Lcom/google/android/music/art/ArtResolver$4;

    invoke-direct {v2, p0, v0}, Lcom/google/android/music/art/ArtResolver$4;-><init>(Lcom/google/android/music/art/ArtResolver;Lcom/google/android/music/art/ContainerArtRequest;)V

    invoke-static {v1, v2}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsync(Lcom/google/android/music/utils/LoggableHandler;Ljava/lang/Runnable;)V

    goto :goto_0

    .line 519
    :pswitch_1
    iget-object v1, p0, Lcom/google/android/music/art/ArtResolver;->mAppContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/google/android/music/art/ContainerArtRequest;->notifyIfDone(Landroid/content/Context;)Z

    goto :goto_0

    .line 522
    :pswitch_2
    sget-object v1, Lcom/google/android/music/art/ArtResolver;->sBackgroundWorker:Lcom/google/android/music/utils/LoggableHandler;

    new-instance v2, Lcom/google/android/music/art/ArtResolver$5;

    invoke-direct {v2, p0, v0}, Lcom/google/android/music/art/ArtResolver$5;-><init>(Lcom/google/android/music/art/ArtResolver;Lcom/google/android/music/art/ContainerArtRequest;)V

    invoke-static {v1, v2}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsync(Lcom/google/android/music/utils/LoggableHandler;Ljava/lang/Runnable;)V

    goto :goto_0

    .line 533
    :pswitch_3
    sget-object v1, Lcom/google/android/music/art/ArtResolver;->sBackgroundWorker:Lcom/google/android/music/utils/LoggableHandler;

    new-instance v2, Lcom/google/android/music/art/ArtResolver$6;

    invoke-direct {v2, p0, v0}, Lcom/google/android/music/art/ArtResolver$6;-><init>(Lcom/google/android/music/art/ArtResolver;Lcom/google/android/music/art/ContainerArtRequest;)V

    invoke-static {v1, v2}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsync(Lcom/google/android/music/utils/LoggableHandler;Ljava/lang/Runnable;)V

    goto :goto_0

    .line 546
    :pswitch_4
    sget-object v1, Lcom/google/android/music/art/ArtResolver;->sBackgroundWorker:Lcom/google/android/music/utils/LoggableHandler;

    new-instance v2, Lcom/google/android/music/art/ArtResolver$7;

    invoke-direct {v2, p0, v0}, Lcom/google/android/music/art/ArtResolver$7;-><init>(Lcom/google/android/music/art/ArtResolver;Lcom/google/android/music/art/ContainerArtRequest;)V

    invoke-static {v1, v2}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsync(Lcom/google/android/music/utils/LoggableHandler;Ljava/lang/Runnable;)V

    goto :goto_0

    .line 554
    :pswitch_5
    sget-object v1, Lcom/google/android/music/art/ArtResolver;->sBackgroundWorker:Lcom/google/android/music/utils/LoggableHandler;

    new-instance v2, Lcom/google/android/music/art/ArtResolver$8;

    invoke-direct {v2, p0, v0}, Lcom/google/android/music/art/ArtResolver$8;-><init>(Lcom/google/android/music/art/ArtResolver;Lcom/google/android/music/art/ContainerArtRequest;)V

    invoke-static {v1, v2}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsync(Lcom/google/android/music/utils/LoggableHandler;Ljava/lang/Runnable;)V

    goto :goto_0

    .line 564
    :pswitch_6
    sget-object v1, Lcom/google/android/music/art/ArtResolver;->sBackgroundWorker:Lcom/google/android/music/utils/LoggableHandler;

    new-instance v2, Lcom/google/android/music/art/ArtResolver$9;

    invoke-direct {v2, p0, v0}, Lcom/google/android/music/art/ArtResolver$9;-><init>(Lcom/google/android/music/art/ArtResolver;Lcom/google/android/music/art/ContainerArtRequest;)V

    invoke-static {v1, v2}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsync(Lcom/google/android/music/utils/LoggableHandler;Ljava/lang/Runnable;)V

    goto :goto_0

    .line 573
    :pswitch_7
    sget-object v1, Lcom/google/android/music/art/ArtResolver;->sBackgroundWorker:Lcom/google/android/music/utils/LoggableHandler;

    new-instance v2, Lcom/google/android/music/art/ArtResolver$10;

    invoke-direct {v2, p0, v0}, Lcom/google/android/music/art/ArtResolver$10;-><init>(Lcom/google/android/music/art/ArtResolver;Lcom/google/android/music/art/ContainerArtRequest;)V

    invoke-static {v1, v2}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsync(Lcom/google/android/music/utils/LoggableHandler;Ljava/lang/Runnable;)V

    goto :goto_0

    .line 508
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_7
        :pswitch_7
    .end packed-switch
.end method

.method static getsBackgroundBitmapWorker()Lcom/google/android/music/utils/LoggableHandler;
    .locals 1

    .prologue
    .line 87
    sget-object v0, Lcom/google/android/music/art/ArtResolver;->sBackgroundBitmapWorker:Lcom/google/android/music/utils/LoggableHandler;

    return-object v0
.end method

.method private processArtistArtCore(Lcom/google/android/music/art/ArtRequest;JLjava/lang/String;)V
    .locals 10
    .param p2, "artistId"    # J
    .param p4, "artistMetajamId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/music/art/ArtRequest",
            "<*>;J",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 608
    .local p1, "request":Lcom/google/android/music/art/ArtRequest;, "Lcom/google/android/music/art/ArtRequest<*>;"
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-wide/16 v2, 0x0

    cmp-long v0, p2, v2

    if-gez v0, :cond_0

    .line 610
    const-string v0, "ArtResolver"

    const-string v2, "processArtistArtCore: artistId and metajamId are both empty. Unable to look up artist art."

    invoke-static {v0, v2}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 612
    iget-object v0, p0, Lcom/google/android/music/art/ArtResolver;->mAppContext:Landroid/content/Context;

    invoke-virtual {p1, v0}, Lcom/google/android/music/art/ArtRequest;->notifyIfDone(Landroid/content/Context;)Z

    .line 613
    iget-object v0, p0, Lcom/google/android/music/art/ArtResolver;->mRequestMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {p1}, Lcom/google/android/music/art/ArtRequest;->getArtKey()Lcom/google/android/music/art/ArtKey;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 656
    :goto_0
    return-void

    .line 616
    :cond_0
    sget-object v0, Lcom/google/android/music/art/ArtResolver$13;->$SwitchMap$com$google$android$music$art$ArtType:[I

    iget-object v2, p1, Lcom/google/android/music/art/ArtRequest;->mArtType:Lcom/google/android/music/art/ArtType;

    invoke-virtual {v2}, Lcom/google/android/music/art/ArtType;->ordinal()I

    move-result v2

    aget v0, v0, v2

    .line 619
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 620
    invoke-static {p4}, Lcom/google/android/music/store/MusicContent$Artists;->getNautilusArtistsUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 624
    .local v1, "artistUri":Landroid/net/Uri;
    :goto_1
    const/4 v7, 0x0

    .line 625
    .local v7, "c":Landroid/database/Cursor;
    const/4 v6, 0x0

    .line 627
    .local v6, "artUri":Ljava/lang/String;
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/art/ArtResolver;->mAppContext:Landroid/content/Context;

    sget-object v2, Lcom/google/android/music/art/ArtTypeHandler;->NAUTILUS_ART_COLUMNS:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 629
    if-eqz v7, :cond_1

    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 630
    const/4 v0, 0x0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->isNull(I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_4

    const/4 v6, 0x0

    .line 633
    :cond_1
    :goto_2
    invoke-static {v7}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 635
    if-eqz v6, :cond_5

    .line 636
    sget-boolean v0, Lcom/google/android/music/art/ArtResolver;->LOGV:Z

    if-eqz v0, :cond_2

    .line 637
    const-string v0, "ArtResolver"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "processArtistArtCore URI method, uri="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 639
    :cond_2
    invoke-direct {p0, v6, p1}, Lcom/google/android/music/art/ArtResolver;->getGenericArtForUrl(Ljava/lang/String;Lcom/google/android/music/art/ArtRequest;)Z

    goto :goto_0

    .line 622
    .end local v1    # "artistUri":Landroid/net/Uri;
    .end local v6    # "artUri":Ljava/lang/String;
    .end local v7    # "c":Landroid/database/Cursor;
    :cond_3
    invoke-static {p2, p3}, Lcom/google/android/music/store/MusicContent$Artists;->getArtistsUri(J)Landroid/net/Uri;

    move-result-object v1

    .restart local v1    # "artistUri":Landroid/net/Uri;
    goto :goto_1

    .line 630
    .restart local v6    # "artUri":Ljava/lang/String;
    .restart local v7    # "c":Landroid/database/Cursor;
    :cond_4
    const/4 v0, 0x0

    :try_start_1
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v6

    goto :goto_2

    .line 633
    :catchall_0
    move-exception v0

    invoke-static {v7}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0

    .line 641
    :cond_5
    sget-boolean v0, Lcom/google/android/music/art/ArtResolver;->LOGV:Z

    if-eqz v0, :cond_6

    .line 642
    const-string v0, "ArtResolver"

    const-string v2, "processArtistArtCore 4-up Album method"

    invoke-static {v0, v2}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 644
    :cond_6
    const-wide/16 v2, 0x0

    cmp-long v0, p2, v2

    if-gez v0, :cond_7

    .line 647
    iget-object v0, p0, Lcom/google/android/music/art/ArtResolver;->mAppContext:Landroid/content/Context;

    invoke-virtual {p1, v0}, Lcom/google/android/music/art/ArtRequest;->notifyIfDone(Landroid/content/Context;)Z

    .line 648
    iget-object v0, p0, Lcom/google/android/music/art/ArtResolver;->mRequestMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {p1}, Lcom/google/android/music/art/ArtRequest;->getArtKey()Lcom/google/android/music/art/ArtKey;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 650
    :cond_7
    invoke-static {p2, p3}, Lcom/google/android/music/store/MusicContent$Artists;->getAlbumsByArtistsUri(J)Landroid/net/Uri;

    move-result-object v1

    .line 651
    const/4 v0, 0x1

    invoke-direct {p0, v1, v0}, Lcom/google/android/music/art/ArtResolver;->extractAlbumIdsFromUri(Landroid/net/Uri;Z)Ljava/util/LinkedHashSet;

    move-result-object v8

    .line 652
    .local v8, "results":Ljava/util/LinkedHashSet;, "Ljava/util/LinkedHashSet<Ljava/lang/Long;>;"
    invoke-direct {p0, p1, v8}, Lcom/google/android/music/art/ArtResolver;->processMultiAlbumLookupResult(Lcom/google/android/music/art/ArtRequest;Ljava/util/Set;)V

    goto/16 :goto_0
.end method

.method private processMultiAlbumLookupResult(Lcom/google/android/music/art/ArtRequest;Ljava/util/Set;)V
    .locals 8
    .param p1, "request"    # Lcom/google/android/music/art/ArtRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/music/art/ArtRequest;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 939
    .local p2, "albumIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    const/4 v1, 0x0

    .line 940
    .local v1, "found":I
    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 941
    .local v4, "id":J
    invoke-direct {p0, v4, v5, p1}, Lcom/google/android/music/art/ArtResolver;->getAlbumArtForId(JLcom/google/android/music/art/ArtRequest;)Z

    move-result v3

    .line 942
    .local v3, "loadedImmediately":Z
    if-eqz v3, :cond_1

    add-int/lit8 v1, v1, 0x1

    .line 943
    :cond_1
    const/4 v6, 0x4

    if-ne v1, v6, :cond_0

    .line 945
    .end local v3    # "loadedImmediately":Z
    .end local v4    # "id":J
    :cond_2
    iget-object v6, p0, Lcom/google/android/music/art/ArtResolver;->mAppContext:Landroid/content/Context;

    invoke-virtual {p1, v6}, Lcom/google/android/music/art/ArtRequest;->notifyIfDone(Landroid/content/Context;)Z

    move-result v0

    .line 946
    .local v0, "done":Z
    if-eqz v0, :cond_3

    .line 947
    iget-object v6, p0, Lcom/google/android/music/art/ArtResolver;->mRequestMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {p1}, Lcom/google/android/music/art/ArtRequest;->getArtKey()Lcom/google/android/music/art/ArtKey;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 949
    :cond_3
    return-void
.end method

.method private processMultiAlbumUrlLookupRequest(Lcom/google/android/music/art/ArtRequest;Ljava/util/Set;)V
    .locals 7
    .param p1, "request"    # Lcom/google/android/music/art/ArtRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/music/art/ArtRequest;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 952
    .local p2, "albumUrls":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const/4 v1, 0x0

    .line 953
    .local v1, "found":I
    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 954
    .local v4, "url":Ljava/lang/String;
    invoke-direct {p0, v4, p1}, Lcom/google/android/music/art/ArtResolver;->getAlbumArtForUrl(Ljava/lang/String;Lcom/google/android/music/art/ArtRequest;)Z

    move-result v3

    .line 955
    .local v3, "loadedImmediately":Z
    if-eqz v3, :cond_1

    .line 956
    add-int/lit8 v1, v1, 0x1

    .line 958
    :cond_1
    const/4 v5, 0x4

    if-ne v1, v5, :cond_0

    .line 960
    .end local v3    # "loadedImmediately":Z
    .end local v4    # "url":Ljava/lang/String;
    :cond_2
    iget-object v5, p0, Lcom/google/android/music/art/ArtResolver;->mAppContext:Landroid/content/Context;

    invoke-virtual {p1, v5}, Lcom/google/android/music/art/ArtRequest;->notifyIfDone(Landroid/content/Context;)Z

    move-result v0

    .line 961
    .local v0, "done":Z
    if-eqz v0, :cond_3

    .line 962
    iget-object v5, p0, Lcom/google/android/music/art/ArtResolver;->mRequestMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {p1}, Lcom/google/android/music/art/ArtRequest;->getArtKey()Lcom/google/android/music/art/ArtKey;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 964
    :cond_3
    return-void
.end method

.method private requestStillNeedsBitmap(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 102
    iget-object v2, p0, Lcom/google/android/music/art/ArtResolver;->mRequestMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/art/ArtRequest;

    .line 103
    .local v1, "request":Lcom/google/android/music/art/ArtRequest;
    invoke-virtual {v1, p1}, Lcom/google/android/music/art/ArtRequest;->hasNeededItem(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    .line 105
    .end local v1    # "request":Lcom/google/android/music/art/ArtRequest;
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private startContainerAlbumLookup(Lcom/google/android/music/art/ContainerArtRequest;)V
    .locals 6
    .param p1, "request"    # Lcom/google/android/music/art/ContainerArtRequest;

    .prologue
    .line 669
    sget-boolean v3, Lcom/google/android/music/art/ArtResolver;->LOGV:Z

    if-eqz v3, :cond_0

    .line 670
    const-string v3, "ArtResolver"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "startContainerAlbumLookup request="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 672
    :cond_0
    invoke-direct {p0}, Lcom/google/android/music/art/ArtResolver;->assertOnBackgroundWorker()V

    .line 673
    invoke-virtual {p1}, Lcom/google/android/music/art/ContainerArtRequest;->container()Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/music/store/ContainerDescriptor;->getId()J

    move-result-wide v0

    .line 674
    .local v0, "id":J
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 675
    .local v2, "ids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 676
    invoke-direct {p0, p1, v2}, Lcom/google/android/music/art/ArtResolver;->processMultiAlbumLookupResult(Lcom/google/android/music/art/ArtRequest;Ljava/util/Set;)V

    .line 677
    return-void
.end method

.method private startContainerArtistLookup(Lcom/google/android/music/art/ContainerArtRequest;)V
    .locals 6
    .param p1, "request"    # Lcom/google/android/music/art/ContainerArtRequest;

    .prologue
    .line 594
    invoke-direct {p0}, Lcom/google/android/music/art/ArtResolver;->assertOnBackgroundWorker()V

    .line 595
    sget-boolean v3, Lcom/google/android/music/art/ArtResolver;->LOGV:Z

    if-eqz v3, :cond_0

    .line 596
    const-string v3, "ArtResolver"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "startContainerArtistLookup request="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 598
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/music/art/ContainerArtRequest;->container()Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/music/store/ContainerDescriptor;->getId()J

    move-result-wide v0

    .line 599
    .local v0, "artistId":J
    invoke-virtual {p1}, Lcom/google/android/music/art/ContainerArtRequest;->container()Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/music/store/ContainerDescriptor;->getExtId()Ljava/lang/String;

    move-result-object v2

    .line 600
    .local v2, "metajamId":Ljava/lang/String;
    invoke-direct {p0, p1, v0, v1, v2}, Lcom/google/android/music/art/ArtResolver;->processArtistArtCore(Lcom/google/android/music/art/ArtRequest;JLjava/lang/String;)V

    .line 601
    return-void
.end method

.method private startContainerGenreLookup(Lcom/google/android/music/art/ContainerArtRequest;)V
    .locals 12
    .param p1, "request"    # Lcom/google/android/music/art/ContainerArtRequest;

    .prologue
    .line 732
    invoke-direct {p0}, Lcom/google/android/music/art/ArtResolver;->assertOnBackgroundWorker()V

    .line 733
    sget-boolean v0, Lcom/google/android/music/art/ArtResolver;->LOGV:Z

    if-eqz v0, :cond_0

    .line 734
    const-string v0, "ArtResolver"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "startContainerGenreLookup: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 736
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/music/art/ContainerArtRequest;->container()Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/store/ContainerDescriptor;->getId()J

    move-result-wide v10

    .line 737
    .local v10, "genreId":J
    invoke-static {v10, v11}, Lcom/google/android/music/store/MusicContent$Genres;->getAlbumsOfGenreUri(J)Landroid/net/Uri;

    move-result-object v1

    .line 738
    .local v1, "lookupUri":Landroid/net/Uri;
    new-instance v8, Ljava/util/LinkedHashSet;

    invoke-direct {v8}, Ljava/util/LinkedHashSet;-><init>()V

    .line 739
    .local v8, "albumIds":Ljava/util/LinkedHashSet;, "Ljava/util/LinkedHashSet<Ljava/lang/Long;>;"
    const/4 v9, 0x0

    .line 741
    .local v9, "c":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/art/ArtResolver;->mAppContext:Landroid/content/Context;

    sget-object v2, Lcom/google/android/music/art/ArtTypeHandler;->GENRE_ALBUM_COLUMNS:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x1

    invoke-static/range {v0 .. v7}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZZ)Landroid/database/Cursor;

    move-result-object v9

    .line 743
    if-eqz v9, :cond_2

    .line 744
    :cond_1
    :goto_0
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 745
    const/4 v0, 0x0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 746
    const/4 v0, 0x0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 751
    :catchall_0
    move-exception v0

    invoke-static {v9}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0

    :cond_2
    invoke-static {v9}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 753
    sget-boolean v0, Lcom/google/android/music/art/ArtResolver;->LOGV:Z

    if-eqz v0, :cond_3

    .line 754
    const-string v0, "ArtResolver"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "cursor consumed, "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v8}, Ljava/util/LinkedHashSet;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " album ids found"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 756
    :cond_3
    invoke-direct {p0, p1, v8}, Lcom/google/android/music/art/ArtResolver;->processMultiAlbumLookupResult(Lcom/google/android/music/art/ArtRequest;Ljava/util/Set;)V

    .line 757
    return-void
.end method

.method private startContainerLocalRadioLookup(Lcom/google/android/music/art/ContainerArtRequest;)V
    .locals 12
    .param p1, "request"    # Lcom/google/android/music/art/ContainerArtRequest;

    .prologue
    .line 680
    invoke-direct {p0}, Lcom/google/android/music/art/ArtResolver;->assertOnBackgroundWorker()V

    .line 681
    invoke-virtual {p1}, Lcom/google/android/music/art/ContainerArtRequest;->container()Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/store/ContainerDescriptor;->getId()J

    move-result-wide v8

    .line 682
    .local v8, "id":J
    invoke-static {v8, v9}, Lcom/google/android/music/store/MusicContent$RadioStations;->getRadioStationUri(J)Landroid/net/Uri;

    move-result-object v1

    .line 683
    .local v1, "uri":Landroid/net/Uri;
    const/4 v6, 0x0

    .line 684
    .local v6, "c":Landroid/database/Cursor;
    new-instance v11, Ljava/util/HashSet;

    invoke-direct {v11}, Ljava/util/HashSet;-><init>()V

    .line 686
    .local v11, "urls":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/art/ArtResolver;->mAppContext:Landroid/content/Context;

    sget-object v2, Lcom/google/android/music/art/ArtTypeHandler;->RADIO_COLUMNS:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 687
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 688
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v7, ""

    .line 689
    .local v7, "multiUrls":Ljava/lang/String;
    :goto_0
    invoke-direct {p0, v7}, Lcom/google/android/music/art/ArtResolver;->decodeAndCapMultiUrlString(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v10

    .line 690
    .local v10, "urlArray":[Ljava/lang/String;
    invoke-static {v11, v10}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 693
    .end local v7    # "multiUrls":Ljava/lang/String;
    .end local v10    # "urlArray":[Ljava/lang/String;
    :cond_0
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 695
    invoke-direct {p0, p1, v11}, Lcom/google/android/music/art/ArtResolver;->processMultiAlbumUrlLookupRequest(Lcom/google/android/music/art/ArtRequest;Ljava/util/Set;)V

    .line 696
    return-void

    .line 688
    :cond_1
    const/4 v0, 0x0

    :try_start_1
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v7

    goto :goto_0

    .line 693
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method private startContainerPlaylistLookup(Lcom/google/android/music/art/ContainerArtRequest;)V
    .locals 10
    .param p1, "request"    # Lcom/google/android/music/art/ContainerArtRequest;

    .prologue
    .line 699
    invoke-direct {p0}, Lcom/google/android/music/art/ArtResolver;->assertOnBackgroundWorker()V

    .line 700
    invoke-virtual {p1}, Lcom/google/android/music/art/ContainerArtRequest;->container()Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/music/store/ContainerDescriptor;->getId()J

    move-result-wide v4

    .line 703
    .local v4, "playlistId":J
    sget-object v7, Lcom/google/android/music/art/ArtResolver$13;->$SwitchMap$com$google$android$music$store$ContainerDescriptor$Type:[I

    invoke-virtual {p1}, Lcom/google/android/music/art/ContainerArtRequest;->container()Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/music/store/ContainerDescriptor;->getType()Lcom/google/android/music/store/ContainerDescriptor$Type;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/music/store/ContainerDescriptor$Type;->ordinal()I

    move-result v8

    aget v7, v7, v8

    packed-switch v7, :pswitch_data_0

    .line 718
    :pswitch_0
    const/4 v2, 0x1

    .line 719
    .local v2, "includeExternal":Z
    invoke-static {v4, v5}, Lcom/google/android/music/store/MusicContent$Playlists$Members;->getPlaylistItemsUri(J)Landroid/net/Uri;

    move-result-object v3

    .line 723
    .local v3, "itemsUri":Landroid/net/Uri;
    :goto_0
    sget-boolean v7, Lcom/google/android/music/art/ArtResolver;->LOGV:Z

    if-eqz v7, :cond_0

    .line 724
    const-string v7, "ArtResolver"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "startContainerPlaylistLookup: request="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "itemsUri="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", includeExternal="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 727
    :cond_0
    invoke-direct {p0, v3, v2}, Lcom/google/android/music/art/ArtResolver;->extractAlbumIdsFromUri(Landroid/net/Uri;Z)Ljava/util/LinkedHashSet;

    move-result-object v0

    .line 728
    .local v0, "albumIds":Ljava/util/LinkedHashSet;, "Ljava/util/LinkedHashSet<Ljava/lang/Long;>;"
    invoke-direct {p0, p1, v0}, Lcom/google/android/music/art/ArtResolver;->processMultiAlbumLookupResult(Lcom/google/android/music/art/ArtRequest;Ljava/util/Set;)V

    .line 729
    .end local v0    # "albumIds":Ljava/util/LinkedHashSet;, "Ljava/util/LinkedHashSet<Ljava/lang/Long;>;"
    .end local v2    # "includeExternal":Z
    .end local v3    # "itemsUri":Landroid/net/Uri;
    :goto_1
    return-void

    .line 705
    :pswitch_1
    invoke-virtual {p1}, Lcom/google/android/music/art/ContainerArtRequest;->container()Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/music/store/ContainerDescriptor;->getExtId()Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/google/android/music/art/ArtResolver;->extractSharedWithMePlaylistArt(Ljava/lang/String;)Ljava/util/LinkedHashSet;

    move-result-object v1

    .line 707
    .local v1, "albumUrls":Ljava/util/LinkedHashSet;, "Ljava/util/LinkedHashSet<Ljava/lang/String;>;"
    invoke-direct {p0, p1, v1}, Lcom/google/android/music/art/ArtResolver;->processMultiAlbumUrlLookupRequest(Lcom/google/android/music/art/ArtRequest;Ljava/util/Set;)V

    goto :goto_1

    .line 713
    .end local v1    # "albumUrls":Ljava/util/LinkedHashSet;, "Ljava/util/LinkedHashSet<Ljava/lang/String;>;"
    :pswitch_2
    invoke-virtual {p1}, Lcom/google/android/music/art/ContainerArtRequest;->container()Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/google/android/music/art/ArtResolver;->getAutoPlaylistForContainer(Lcom/google/android/music/store/ContainerDescriptor;)Lcom/google/android/music/medialist/AutoPlaylistSongList;

    move-result-object v6

    .line 714
    .local v6, "songList":Lcom/google/android/music/medialist/AutoPlaylistSongList;
    iget-object v7, p0, Lcom/google/android/music/art/ArtResolver;->mAppContext:Landroid/content/Context;

    invoke-virtual {v6, v7}, Lcom/google/android/music/medialist/AutoPlaylistSongList;->getContentUri(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v3

    .line 715
    .restart local v3    # "itemsUri":Landroid/net/Uri;
    invoke-virtual {v6}, Lcom/google/android/music/medialist/AutoPlaylistSongList;->mayIncludeExternalContent()Z

    move-result v2

    .line 716
    .restart local v2    # "includeExternal":Z
    goto :goto_0

    .line 703
    :pswitch_data_0
    .packed-switch 0x9
        :pswitch_2
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private startContainerSingleSongLookup(Lcom/google/android/music/art/ContainerArtRequest;)V
    .locals 10
    .param p1, "request"    # Lcom/google/android/music/art/ContainerArtRequest;

    .prologue
    const/4 v9, 0x1

    .line 760
    invoke-direct {p0}, Lcom/google/android/music/art/ArtResolver;->assertOnBackgroundWorker()V

    .line 761
    sget-boolean v6, Lcom/google/android/music/art/ArtResolver;->LOGV:Z

    if-eqz v6, :cond_0

    .line 762
    const-string v6, "ArtResolver"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "startContainerSingleSongLookup: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 764
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/music/art/ContainerArtRequest;->container()Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/music/store/ContainerDescriptor;->getId()J

    move-result-wide v4

    .line 765
    .local v4, "songId":J
    invoke-virtual {p1}, Lcom/google/android/music/art/ContainerArtRequest;->container()Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/music/store/ContainerDescriptor;->getExtId()Ljava/lang/String;

    move-result-object v2

    .line 766
    .local v2, "extId":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/google/android/music/art/ContainerArtRequest;->container()Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/music/store/ContainerDescriptor;->getType()Lcom/google/android/music/store/ContainerDescriptor$Type;

    move-result-object v6

    sget-object v7, Lcom/google/android/music/store/ContainerDescriptor$Type;->NAUTILUS_SINGLE_SONG:Lcom/google/android/music/store/ContainerDescriptor$Type;

    if-eq v6, v7, :cond_1

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 769
    :cond_1
    invoke-static {v2}, Lcom/google/android/music/store/MusicContent$XAudio;->getNautilusAudioUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 770
    .local v3, "songUri":Landroid/net/Uri;
    sget-object v6, Lcom/google/android/music/art/ArtTypeHandler;->XAUDIO_ALBUM_ART_COLUMNS:[Ljava/lang/String;

    invoke-direct {p0, v3, v6, v9, v9}, Lcom/google/android/music/art/ArtResolver;->extractStringIdsFromUri(Landroid/net/Uri;[Ljava/lang/String;ZI)Ljava/util/LinkedHashSet;

    move-result-object v1

    .line 772
    .local v1, "artUrls":Ljava/util/LinkedHashSet;, "Ljava/util/LinkedHashSet<Ljava/lang/String;>;"
    invoke-direct {p0, p1, v1}, Lcom/google/android/music/art/ArtResolver;->processMultiAlbumUrlLookupRequest(Lcom/google/android/music/art/ArtRequest;Ljava/util/Set;)V

    .line 783
    .end local v1    # "artUrls":Ljava/util/LinkedHashSet;, "Ljava/util/LinkedHashSet<Ljava/lang/String;>;"
    :goto_0
    return-void

    .line 774
    .end local v3    # "songUri":Landroid/net/Uri;
    :cond_2
    invoke-static {v4, v5}, Lcom/google/android/music/store/MusicContent$XAudio;->getAudioUri(J)Landroid/net/Uri;

    move-result-object v3

    .line 775
    .restart local v3    # "songUri":Landroid/net/Uri;
    invoke-direct {p0, v3, v9}, Lcom/google/android/music/art/ArtResolver;->extractAlbumIdsFromUri(Landroid/net/Uri;Z)Ljava/util/LinkedHashSet;

    move-result-object v0

    .line 777
    .local v0, "albumIds":Ljava/util/LinkedHashSet;, "Ljava/util/LinkedHashSet<Ljava/lang/Long;>;"
    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->size()I

    move-result v6

    if-le v6, v9, :cond_3

    .line 778
    const-string v6, "ArtResolver"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "startContainerSingleSongLookup: got more than one album art for the song: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 781
    :cond_3
    invoke-direct {p0, p1, v0}, Lcom/google/android/music/art/ArtResolver;->processMultiAlbumLookupResult(Lcom/google/android/music/art/ArtRequest;Ljava/util/Set;)V

    goto :goto_0
.end method

.method private startContainerTopSongsLookup(Lcom/google/android/music/art/ContainerArtRequest;)V
    .locals 25
    .param p1, "request"    # Lcom/google/android/music/art/ContainerArtRequest;

    .prologue
    .line 786
    invoke-direct/range {p0 .. p0}, Lcom/google/android/music/art/ArtResolver;->assertOnBackgroundWorker()V

    .line 787
    sget-boolean v2, Lcom/google/android/music/art/ArtResolver;->LOGV:Z

    if-eqz v2, :cond_0

    .line 788
    const-string v2, "ArtResolver"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "startContainerTopSongsLookup: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 790
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/art/ContainerArtRequest;->container()Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/music/store/ContainerDescriptor;->getExtId()Ljava/lang/String;

    move-result-object v15

    .line 791
    .local v15, "extId":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/art/ContainerArtRequest;->container()Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/music/store/ContainerDescriptor;->getType()Lcom/google/android/music/store/ContainerDescriptor$Type;

    move-result-object v24

    .line 793
    .local v24, "type":Lcom/google/android/music/store/ContainerDescriptor$Type;
    sget-object v2, Lcom/google/android/music/store/ContainerDescriptor$Type;->TOP_SONGS_IN_ALL_ACCESS:Lcom/google/android/music/store/ContainerDescriptor$Type;

    move-object/from16 v0, v24

    if-ne v0, v2, :cond_1

    .line 794
    invoke-virtual/range {v24 .. v24}, Lcom/google/android/music/store/ContainerDescriptor$Type;->getDBValue()I

    move-result v2

    invoke-static {v2}, Lcom/google/android/music/store/MusicContent$Queue;->getContainerUri(I)Landroid/net/Uri;

    move-result-object v3

    .line 798
    .local v3, "queryUri":Landroid/net/Uri;
    :goto_0
    const/4 v2, 0x2

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v5, "album_id"

    aput-object v5, v4, v2

    const/4 v2, 0x1

    const-string v5, "StoreAlbumId"

    aput-object v5, v4, v2

    .line 800
    .local v4, "projection":[Ljava/lang/String;
    const/4 v13, 0x0

    .line 801
    .local v13, "c":Landroid/database/Cursor;
    new-instance v23, Ljava/util/LinkedHashSet;

    invoke-direct/range {v23 .. v23}, Ljava/util/LinkedHashSet;-><init>()V

    .line 803
    .local v23, "results":Ljava/util/LinkedHashSet;, "Ljava/util/LinkedHashSet<Landroid/util/Pair<Ljava/lang/Long;Ljava/lang/String;>;>;"
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/art/ArtResolver;->mAppContext:Landroid/content/Context;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x1

    invoke-static/range {v2 .. v9}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZZ)Landroid/database/Cursor;

    move-result-object v13

    .line 804
    if-eqz v13, :cond_4

    .line 805
    :goto_1
    invoke-interface {v13}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 806
    const/4 v2, 0x0

    invoke-interface {v13, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v10, 0x0

    .line 807
    .local v10, "albumId":Ljava/lang/Long;
    :goto_2
    const/4 v2, 0x1

    invoke-interface {v13, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v14, 0x0

    .line 808
    .local v14, "extAlbumId":Ljava/lang/String;
    :goto_3
    new-instance v22, Landroid/util/Pair;

    move-object/from16 v0, v22

    invoke-direct {v0, v10, v14}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 809
    .local v22, "pair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Long;Ljava/lang/String;>;"
    move-object/from16 v0, v23

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 813
    .end local v10    # "albumId":Ljava/lang/Long;
    .end local v14    # "extAlbumId":Ljava/lang/String;
    .end local v22    # "pair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Long;Ljava/lang/String;>;"
    :catchall_0
    move-exception v2

    invoke-static {v13}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v2

    .line 796
    .end local v3    # "queryUri":Landroid/net/Uri;
    .end local v4    # "projection":[Ljava/lang/String;
    .end local v13    # "c":Landroid/database/Cursor;
    .end local v23    # "results":Ljava/util/LinkedHashSet;, "Ljava/util/LinkedHashSet<Landroid/util/Pair<Ljava/lang/Long;Ljava/lang/String;>;>;"
    :cond_1
    invoke-virtual/range {v24 .. v24}, Lcom/google/android/music/store/ContainerDescriptor$Type;->getDBValue()I

    move-result v2

    invoke-static {v2, v15}, Lcom/google/android/music/store/MusicContent$Queue;->getContainerUri(ILjava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .restart local v3    # "queryUri":Landroid/net/Uri;
    goto :goto_0

    .line 806
    .restart local v4    # "projection":[Ljava/lang/String;
    .restart local v13    # "c":Landroid/database/Cursor;
    .restart local v23    # "results":Ljava/util/LinkedHashSet;, "Ljava/util/LinkedHashSet<Landroid/util/Pair<Ljava/lang/Long;Ljava/lang/String;>;>;"
    :cond_2
    const/4 v2, 0x0

    :try_start_1
    invoke-interface {v13, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    goto :goto_2

    .line 807
    .restart local v10    # "albumId":Ljava/lang/Long;
    :cond_3
    const/4 v2, 0x1

    invoke-interface {v13, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v14

    goto :goto_3

    .line 813
    .end local v10    # "albumId":Ljava/lang/Long;
    :cond_4
    invoke-static {v13}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 816
    const/16 v16, 0x0

    .line 817
    .local v16, "foundCount":I
    new-instance v11, Lcom/google/android/music/download/artwork/ArtIdRecorder$AlbumIdRecorder;

    invoke-direct {v11}, Lcom/google/android/music/download/artwork/ArtIdRecorder$AlbumIdRecorder;-><init>()V

    .line 818
    .local v11, "albumRecorder":Lcom/google/android/music/download/artwork/ArtIdRecorder$AlbumIdRecorder;
    new-instance v21, Ljava/util/LinkedHashSet;

    invoke-direct/range {v21 .. v21}, Ljava/util/LinkedHashSet;-><init>()V

    .line 819
    .local v21, "missingStringIDs":Ljava/util/LinkedHashSet;, "Ljava/util/LinkedHashSet<Ljava/lang/String;>;"
    invoke-virtual/range {v23 .. v23}, Ljava/util/LinkedHashSet;->iterator()Ljava/util/Iterator;

    move-result-object v17

    .local v17, "i$":Ljava/util/Iterator;
    :cond_5
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Landroid/util/Pair;

    .line 820
    .local v18, "ids":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Long;Ljava/lang/String;>;"
    move-object/from16 v0, v18

    iget-object v2, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    if-eqz v2, :cond_8

    move-object/from16 v0, v18

    iget-object v2, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {v6, v7}, Lcom/google/android/music/store/ProjectionUtils;->isFauxNautilusId(J)Z

    move-result v2

    if-nez v2, :cond_8

    .line 821
    move-object/from16 v0, v18

    iget-object v2, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v6, v7, v1}, Lcom/google/android/music/art/ArtResolver;->getAlbumArtForId(JLcom/google/android/music/art/ArtRequest;)Z

    move-result v19

    .line 822
    .local v19, "loaded":Z
    if-eqz v19, :cond_6

    .line 823
    add-int/lit8 v16, v16, 0x1

    .line 843
    .end local v19    # "loaded":Z
    :cond_6
    :goto_4
    const/4 v2, 0x4

    move/from16 v0, v16

    if-ne v0, v2, :cond_5

    .line 846
    .end local v18    # "ids":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Long;Ljava/lang/String;>;"
    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/art/ArtResolver;->mAppContext:Landroid/content/Context;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/google/android/music/art/ContainerArtRequest;->notifyIfDone(Landroid/content/Context;)Z

    .line 847
    return-void

    .line 825
    .restart local v18    # "ids":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Long;Ljava/lang/String;>;"
    :cond_8
    move-object/from16 v0, v18

    iget-object v2, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/CharSequence;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_a

    .line 826
    move-object/from16 v0, v18

    iget-object v2, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/music/art/ArtResolver;->getAlbumArtUrlForMetajamId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 827
    .local v12, "artUrl":Ljava/lang/String;
    if-nez v12, :cond_9

    .line 828
    sget-boolean v2, Lcom/google/android/music/art/ArtResolver;->LOGV:Z

    if-eqz v2, :cond_6

    .line 829
    const-string v5, "ArtResolver"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "startContainerTopSongsLookup: metajam ID "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v18

    iget-object v2, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, " has no art available"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v5, v2}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 833
    :cond_9
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v12, v1}, Lcom/google/android/music/art/ArtResolver;->getAlbumArtForUrl(Ljava/lang/String;Lcom/google/android/music/art/ArtRequest;)Z

    move-result v20

    .line 834
    .local v20, "loadedImmediately":Z
    if-eqz v20, :cond_6

    add-int/lit8 v16, v16, 0x1

    goto :goto_4

    .line 837
    .end local v12    # "artUrl":Ljava/lang/String;
    .end local v20    # "loadedImmediately":Z
    :cond_a
    sget-boolean v2, Lcom/google/android/music/art/ArtResolver;->LOGV:Z

    if-eqz v2, :cond_6

    .line 838
    const-string v2, "ArtResolver"

    const-string v5, "startContainerTopSongsLookup: unable to load image as both IDs are null"

    invoke-static {v2, v5}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4
.end method

.method private startGenericUrlLookup(Lcom/google/android/music/art/ArtRequest;Ljava/lang/String;)V
    .locals 2
    .param p1, "request"    # Lcom/google/android/music/art/ArtRequest;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    .line 850
    invoke-direct {p0}, Lcom/google/android/music/art/ArtResolver;->assertOnBackgroundWorker()V

    .line 851
    sget-boolean v0, Lcom/google/android/music/art/ArtResolver;->LOGV:Z

    if-eqz v0, :cond_0

    .line 852
    const-string v0, "ArtResolver"

    const-string v1, "startGenericUrlLookup"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 854
    :cond_0
    invoke-direct {p0, p2, p1}, Lcom/google/android/music/art/ArtResolver;->getGenericArtForUrl(Ljava/lang/String;Lcom/google/android/music/art/ArtRequest;)Z

    .line 855
    return-void
.end method


# virtual methods
.method executeOnBackgroundWorker(Ljava/lang/Runnable;)V
    .locals 1
    .param p1, "runnable"    # Ljava/lang/Runnable;

    .prologue
    .line 1121
    sget-object v0, Lcom/google/android/music/art/ArtResolver;->sBackgroundWorker:Lcom/google/android/music/utils/LoggableHandler;

    invoke-static {v0, p1}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsync(Lcom/google/android/music/utils/LoggableHandler;Ljava/lang/Runnable;)V

    .line 1122
    return-void
.end method

.method public getArt(Lcom/google/android/music/art/ArtRequest$Token;)Lcom/google/android/music/art/ArtRequest;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/music/art/ArtRequest$Token",
            "<*>;)",
            "Lcom/google/android/music/art/ArtRequest",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 253
    .local p1, "token":Lcom/google/android/music/art/ArtRequest$Token;, "Lcom/google/android/music/art/ArtRequest$Token<*>;"
    sget-boolean v0, Lcom/google/android/music/art/ArtResolver;->LOGV:Z

    if-eqz v0, :cond_0

    .line 254
    const-string v0, "ArtResolver"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getArt(Token): building new art for token="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    :cond_0
    iget-object v0, p1, Lcom/google/android/music/art/ArtRequest$Token;->mDescriptor:Ljava/lang/Object;

    instance-of v0, v0, Lcom/google/android/music/art/ContainerMixDescriptorPair;

    if-eqz v0, :cond_1

    .line 257
    iget-object v0, p1, Lcom/google/android/music/art/ArtRequest$Token;->mDescriptor:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/music/art/ContainerMixDescriptorPair;

    iget-object v1, p1, Lcom/google/android/music/art/ArtRequest$Token;->mArtType:Lcom/google/android/music/art/ArtType;

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/music/art/ArtResolver;->getContainerArt(Lcom/google/android/music/art/ContainerMixDescriptorPair;Lcom/google/android/music/art/ArtType;Z)Lcom/google/android/music/art/ContainerArtRequest;

    move-result-object v0

    .line 269
    :goto_0
    return-object v0

    .line 259
    :cond_1
    iget-object v0, p1, Lcom/google/android/music/art/ArtRequest$Token;->mDescriptor:Ljava/lang/Object;

    instance-of v0, v0, Lcom/google/android/music/cloudclient/QuizArtistJson;

    if-eqz v0, :cond_2

    .line 260
    iget-object v0, p1, Lcom/google/android/music/art/ArtRequest$Token;->mDescriptor:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/music/cloudclient/QuizArtistJson;

    iget-object v1, p1, Lcom/google/android/music/art/ArtRequest$Token;->mArtType:Lcom/google/android/music/art/ArtType;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/music/art/ArtResolver;->getArt(Lcom/google/android/music/cloudclient/QuizArtistJson;Lcom/google/android/music/art/ArtType;)Lcom/google/android/music/art/ArtRequest;

    move-result-object v0

    goto :goto_0

    .line 261
    :cond_2
    iget-object v0, p1, Lcom/google/android/music/art/ArtRequest$Token;->mDescriptor:Ljava/lang/Object;

    instance-of v0, v0, Lcom/google/android/music/cloudclient/QuizGenreJson;

    if-eqz v0, :cond_3

    .line 262
    iget-object v0, p1, Lcom/google/android/music/art/ArtRequest$Token;->mDescriptor:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/music/cloudclient/QuizGenreJson;

    iget-object v1, p1, Lcom/google/android/music/art/ArtRequest$Token;->mArtType:Lcom/google/android/music/art/ArtType;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/music/art/ArtResolver;->getArt(Lcom/google/android/music/cloudclient/QuizGenreJson;Lcom/google/android/music/art/ArtType;)Lcom/google/android/music/art/ArtRequest;

    move-result-object v0

    goto :goto_0

    .line 263
    :cond_3
    iget-object v0, p1, Lcom/google/android/music/art/ArtRequest$Token;->mDescriptor:Ljava/lang/Object;

    instance-of v0, v0, Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 264
    iget-object v0, p1, Lcom/google/android/music/art/ArtRequest$Token;->mDescriptor:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/music/art/ArtResolver;->getArtForUrl(Ljava/lang/String;)Lcom/google/android/music/art/ArtRequest;

    move-result-object v0

    goto :goto_0

    .line 266
    :cond_4
    const-string v0, "ArtResolver"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getArt(Token): Unknown descriptor type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/google/android/music/art/ArtRequest$Token;->mDescriptor:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", unable to rebuild art."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    .line 269
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getArt(Lcom/google/android/music/cloudclient/QuizArtistJson;Lcom/google/android/music/art/ArtType;)Lcom/google/android/music/art/ArtRequest;
    .locals 1
    .param p1, "artist"    # Lcom/google/android/music/cloudclient/QuizArtistJson;
    .param p2, "type"    # Lcom/google/android/music/art/ArtType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/music/cloudclient/QuizArtistJson;",
            "Lcom/google/android/music/art/ArtType;",
            ")",
            "Lcom/google/android/music/art/ArtRequest",
            "<",
            "Lcom/google/android/music/cloudclient/QuizArtistJson;",
            ">;"
        }
    .end annotation

    .prologue
    .line 347
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/music/art/ArtResolver;->getArt(Lcom/google/android/music/cloudclient/QuizArtistJson;Lcom/google/android/music/art/ArtType;Z)Lcom/google/android/music/art/ArtRequest;

    move-result-object v0

    return-object v0
.end method

.method public getArt(Lcom/google/android/music/cloudclient/QuizGenreJson;Lcom/google/android/music/art/ArtType;)Lcom/google/android/music/art/ArtRequest;
    .locals 1
    .param p1, "genre"    # Lcom/google/android/music/cloudclient/QuizGenreJson;
    .param p2, "type"    # Lcom/google/android/music/art/ArtType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/music/cloudclient/QuizGenreJson;",
            "Lcom/google/android/music/art/ArtType;",
            ")",
            "Lcom/google/android/music/art/ArtRequest",
            "<",
            "Lcom/google/android/music/cloudclient/QuizGenreJson;",
            ">;"
        }
    .end annotation

    .prologue
    .line 418
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/music/art/ArtResolver;->getArt(Lcom/google/android/music/cloudclient/QuizGenreJson;Lcom/google/android/music/art/ArtType;Z)Lcom/google/android/music/art/ArtRequest;

    move-result-object v0

    return-object v0
.end method

.method public getArt(Lcom/google/android/music/store/ContainerDescriptor;Lcom/google/android/music/mix/MixDescriptor;Lcom/google/android/music/art/ArtType;)Lcom/google/android/music/art/ContainerArtRequest;
    .locals 1
    .param p1, "containerDescriptor"    # Lcom/google/android/music/store/ContainerDescriptor;
    .param p2, "mixDescriptor"    # Lcom/google/android/music/mix/MixDescriptor;
    .param p3, "type"    # Lcom/google/android/music/art/ArtType;

    .prologue
    .line 282
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/music/art/ArtResolver;->getArt(Lcom/google/android/music/store/ContainerDescriptor;Lcom/google/android/music/mix/MixDescriptor;Lcom/google/android/music/art/ArtType;Z)Lcom/google/android/music/art/ContainerArtRequest;

    move-result-object v0

    return-object v0
.end method

.method public getArtForUrl(Ljava/lang/String;)Lcom/google/android/music/art/ArtRequest;
    .locals 3
    .param p1, "artUrl"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/android/music/art/ArtRequest",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 483
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    .line 493
    :goto_0
    return-object v0

    .line 484
    :cond_0
    new-instance v0, Lcom/google/android/music/art/ArtRequest;

    sget-object v1, Lcom/google/android/music/art/ArtType;->GENERIC_URL:Lcom/google/android/music/art/ArtType;

    invoke-direct {v0, p1, v1}, Lcom/google/android/music/art/ArtRequest;-><init>(Ljava/lang/Object;Lcom/google/android/music/art/ArtType;)V

    .line 485
    .local v0, "request":Lcom/google/android/music/art/ArtRequest;, "Lcom/google/android/music/art/ArtRequest<Ljava/lang/String;>;"
    iget-object v1, p0, Lcom/google/android/music/art/ArtResolver;->mRequestMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Lcom/google/android/music/art/ArtRequest;->getArtKey()Lcom/google/android/music/art/ArtKey;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 486
    sget-object v1, Lcom/google/android/music/art/ArtResolver;->sBackgroundWorker:Lcom/google/android/music/utils/LoggableHandler;

    new-instance v2, Lcom/google/android/music/art/ArtResolver$3;

    invoke-direct {v2, p0, v0, p1}, Lcom/google/android/music/art/ArtResolver$3;-><init>(Lcom/google/android/music/art/ArtResolver;Lcom/google/android/music/art/ArtRequest;Ljava/lang/String;)V

    invoke-static {v1, v2}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsync(Lcom/google/android/music/utils/LoggableHandler;Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public prefetchArt(Lcom/google/android/music/cloudclient/QuizArtistJson;Lcom/google/android/music/art/ArtType;)V
    .locals 1
    .param p1, "artist"    # Lcom/google/android/music/cloudclient/QuizArtistJson;
    .param p2, "type"    # Lcom/google/android/music/art/ArtType;

    .prologue
    .line 357
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/music/art/ArtResolver;->getArt(Lcom/google/android/music/cloudclient/QuizArtistJson;Lcom/google/android/music/art/ArtType;Z)Lcom/google/android/music/art/ArtRequest;

    .line 358
    return-void
.end method

.method recycleBitmap(Landroid/graphics/Bitmap;)V
    .locals 3
    .param p1, "bmp"    # Landroid/graphics/Bitmap;

    .prologue
    .line 1127
    iget-object v0, p0, Lcom/google/android/music/art/ArtResolver;->mMemoryCache:Lcom/google/android/music/art/BitmapLruCache;

    if-eqz v0, :cond_1

    .line 1136
    :cond_0
    :goto_0
    return-void

    .line 1129
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1130
    invoke-static {p1}, Lcom/google/android/music/utils/AlbumArtUtils;->recycleBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 1132
    :cond_2
    sget-boolean v0, Lcom/google/android/music/art/ArtResolver;->LOGV:Z

    if-eqz v0, :cond_0

    .line 1133
    const-string v0, "ArtResolver"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "recycleBitmap tried to recycle a recycled bitmap: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public releaseMemoryCaching()V
    .locals 3

    .prologue
    .line 204
    iget-object v1, p0, Lcom/google/android/music/art/ArtResolver;->mMemoryCacheLock:Ljava/lang/Object;

    monitor-enter v1

    .line 205
    :try_start_0
    iget v0, p0, Lcom/google/android/music/art/ArtResolver;->mMemoryCacheUsers:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/music/art/ArtResolver;->mMemoryCacheUsers:I

    .line 206
    iget-object v0, p0, Lcom/google/android/music/art/ArtResolver;->mMemoryCache:Lcom/google/android/music/art/BitmapLruCache;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/music/art/ArtResolver;->mMemoryCacheUsers:I

    if-nez v0, :cond_0

    .line 207
    const-string v0, "ArtResolver"

    const-string v2, "Disabling in-memory cache"

    invoke-static {v0, v2}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    iget-object v0, p0, Lcom/google/android/music/art/ArtResolver;->mMemoryCache:Lcom/google/android/music/art/BitmapLruCache;

    invoke-virtual {v0}, Lcom/google/android/music/art/BitmapLruCache;->evictAll()V

    .line 209
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/art/ArtResolver;->mMemoryCache:Lcom/google/android/music/art/BitmapLruCache;

    .line 211
    :cond_0
    monitor-exit v1

    .line 212
    return-void

    .line 211
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method removeRequest(Lcom/google/android/music/art/ArtRequest;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/music/art/ArtRequest",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 152
    .local p1, "request":Lcom/google/android/music/art/ArtRequest;, "Lcom/google/android/music/art/ArtRequest<*>;"
    iget-object v0, p0, Lcom/google/android/music/art/ArtResolver;->mRequestMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {p1}, Lcom/google/android/music/art/ArtRequest;->getArtKey()Lcom/google/android/music/art/ArtKey;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 153
    return-void
.end method

.method public requestMemoryCaching()V
    .locals 3

    .prologue
    .line 194
    iget-object v1, p0, Lcom/google/android/music/art/ArtResolver;->mMemoryCacheLock:Ljava/lang/Object;

    monitor-enter v1

    .line 195
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/art/ArtResolver;->mMemoryCache:Lcom/google/android/music/art/BitmapLruCache;

    if-nez v0, :cond_0

    .line 196
    const-string v0, "ArtResolver"

    const-string v2, "Enabling in-memory cache"

    invoke-static {v0, v2}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    new-instance v0, Lcom/google/android/music/art/BitmapLruCache;

    const/16 v2, 0x19

    invoke-direct {v0, v2}, Lcom/google/android/music/art/BitmapLruCache;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/music/art/ArtResolver;->mMemoryCache:Lcom/google/android/music/art/BitmapLruCache;

    .line 199
    :cond_0
    iget v0, p0, Lcom/google/android/music/art/ArtResolver;->mMemoryCacheUsers:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/music/art/ArtResolver;->mMemoryCacheUsers:I

    .line 200
    monitor-exit v1

    .line 201
    return-void

    .line 200
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
