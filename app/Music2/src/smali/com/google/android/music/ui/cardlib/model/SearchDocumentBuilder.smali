.class public Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;
.super Ljava/lang/Object;
.source "SearchDocumentBuilder.java"


# static fields
.field public static final CURSOR_COLUMNS:[Ljava/lang/String;

.field private static sAlbumArtistIdIdx:I

.field private static sAlbumArtistNameIdx:I

.field private static sAlbumIdIdx:I

.field private static sAlbumMetajamIdIdx:I

.field private static sAlbumNameIdx:I

.field private static sArtistMetajamIdIdx:I

.field private static sArtistNameIdx:I

.field private static sArtworkUrlIdx:I

.field private static sGenreArtUrlsIdx:I

.field private static sGenreIdIdx:I

.field private static sGenreNameIdx:I

.field private static sGenreParentIdIdx:I

.field private static sGenreSubgenreCountIdx:I

.field private static sIdIdx:I

.field private static sItemNameIdx:I

.field private static sItemTypeIdx:I

.field private static sPlaylistDescriptionIdx:I

.field private static sPlaylistNameIdx:I

.field private static sPlaylistOwnerNameIdx:I

.field private static sPlaylistOwnerProfilePhotoUrlIdx:I

.field private static sPlaylistShareTokenIdx:I

.field private static sPlaylistTypeIdx:I

.field private static sRadioDescriptionIdx:I

.field private static sRadioNameIdx:I

.field private static sRadioSeedSourceIdIdx:I

.field private static sRadioSeedSourceTypeIdx:I

.field private static sTrackMetajamIdIdx:I

.field private static sVideoIdIdx:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 16
    const/16 v0, 0x1c

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "ListType"

    aput-object v1, v0, v4

    const-string v1, "searchName"

    aput-object v1, v0, v5

    const-string v1, "Album"

    aput-object v1, v0, v6

    const-string v1, "AlbumArtist"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "AlbumArtistId"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "Artist"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "AlbumId"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "artworkUrl"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "Nid"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "StoreAlbumId"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "ArtistMetajamId"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "genreArtUris"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "genreServerId"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "name"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "parentGenreId"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "subgenreCount"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "searchType"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "Name"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "ShareToken"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "OwnerName"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "Description"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "OwnerProfilePhotoUrl"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "Vid"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "radio_name"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "radio_description"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "radio_seed_source_id"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "radio_seed_source_type"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->CURSOR_COLUMNS:[Ljava/lang/String;

    .line 47
    sput v3, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->sIdIdx:I

    .line 48
    sput v4, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->sPlaylistTypeIdx:I

    .line 49
    sput v5, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->sItemNameIdx:I

    .line 50
    sput v6, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->sAlbumNameIdx:I

    .line 51
    sput v7, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->sAlbumArtistNameIdx:I

    .line 52
    const/4 v0, 0x5

    sput v0, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->sAlbumArtistIdIdx:I

    .line 53
    const/4 v0, 0x6

    sput v0, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->sArtistNameIdx:I

    .line 54
    const/4 v0, 0x7

    sput v0, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->sAlbumIdIdx:I

    .line 55
    const/16 v0, 0x8

    sput v0, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->sArtworkUrlIdx:I

    .line 56
    const/16 v0, 0x9

    sput v0, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->sTrackMetajamIdIdx:I

    .line 57
    const/16 v0, 0xa

    sput v0, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->sAlbumMetajamIdIdx:I

    .line 58
    const/16 v0, 0xb

    sput v0, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->sArtistMetajamIdIdx:I

    .line 59
    const/16 v0, 0xc

    sput v0, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->sGenreArtUrlsIdx:I

    .line 60
    const/16 v0, 0xd

    sput v0, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->sGenreIdIdx:I

    .line 61
    const/16 v0, 0xe

    sput v0, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->sGenreNameIdx:I

    .line 62
    const/16 v0, 0xf

    sput v0, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->sGenreParentIdIdx:I

    .line 63
    const/16 v0, 0x10

    sput v0, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->sGenreSubgenreCountIdx:I

    .line 64
    const/16 v0, 0x11

    sput v0, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->sItemTypeIdx:I

    .line 65
    const/16 v0, 0x12

    sput v0, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->sPlaylistNameIdx:I

    .line 66
    const/16 v0, 0x13

    sput v0, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->sPlaylistShareTokenIdx:I

    .line 67
    const/16 v0, 0x14

    sput v0, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->sPlaylistOwnerNameIdx:I

    .line 68
    const/16 v0, 0x15

    sput v0, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->sPlaylistDescriptionIdx:I

    .line 69
    const/16 v0, 0x16

    sput v0, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->sPlaylistOwnerProfilePhotoUrlIdx:I

    .line 70
    const/16 v0, 0x17

    sput v0, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->sVideoIdIdx:I

    .line 71
    const/16 v0, 0x18

    sput v0, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->sRadioNameIdx:I

    .line 72
    const/16 v0, 0x19

    sput v0, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->sRadioDescriptionIdx:I

    .line 73
    const/16 v0, 0x1a

    sput v0, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->sRadioSeedSourceIdIdx:I

    .line 74
    const/16 v0, 0x1b

    sput v0, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->sRadioSeedSourceTypeIdx:I

    return-void
.end method

.method public static buildAlbumDocumentList(Landroid/database/Cursor;)Ljava/util/ArrayList;
    .locals 3
    .param p0, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/music/ui/cardlib/model/Document;",
            ">;"
        }
    .end annotation

    .prologue
    .line 78
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 79
    .local v1, "listDoc":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/music/ui/cardlib/model/Document;>;"
    const/4 v2, -0x1

    invoke-interface {p0, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 80
    :goto_0
    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 81
    new-instance v0, Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-direct {v0}, Lcom/google/android/music/ui/cardlib/model/Document;-><init>()V

    .line 82
    .local v0, "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    invoke-static {v0, p0}, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->getAlbumDocument(Lcom/google/android/music/ui/cardlib/model/Document;Landroid/database/Cursor;)Lcom/google/android/music/ui/cardlib/model/Document;

    move-result-object v0

    .line 83
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 85
    .end local v0    # "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    :cond_0
    return-object v1
.end method

.method public static buildArtistDocumentList(Landroid/database/Cursor;)Ljava/util/ArrayList;
    .locals 3
    .param p0, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/music/ui/cardlib/model/Document;",
            ">;"
        }
    .end annotation

    .prologue
    .line 89
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 90
    .local v1, "listDoc":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/music/ui/cardlib/model/Document;>;"
    const/4 v2, -0x1

    invoke-interface {p0, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 91
    :goto_0
    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 92
    new-instance v0, Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-direct {v0}, Lcom/google/android/music/ui/cardlib/model/Document;-><init>()V

    .line 93
    .local v0, "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    invoke-static {v0, p0}, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->getArtistDocument(Lcom/google/android/music/ui/cardlib/model/Document;Landroid/database/Cursor;)Lcom/google/android/music/ui/cardlib/model/Document;

    move-result-object v0

    .line 94
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 96
    .end local v0    # "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    :cond_0
    return-object v1
.end method

.method public static buildGenreDocumentList(Landroid/database/Cursor;Z)Ljava/util/ArrayList;
    .locals 3
    .param p0, "cursor"    # Landroid/database/Cursor;
    .param p1, "isRadio"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "Z)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/music/ui/cardlib/model/Document;",
            ">;"
        }
    .end annotation

    .prologue
    .line 135
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 136
    .local v1, "listDoc":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/music/ui/cardlib/model/Document;>;"
    const/4 v2, -0x1

    invoke-interface {p0, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 137
    :goto_0
    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 138
    new-instance v0, Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-direct {v0}, Lcom/google/android/music/ui/cardlib/model/Document;-><init>()V

    .line 139
    .local v0, "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    invoke-static {v0, p0, p1}, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->getGenreDocument(Lcom/google/android/music/ui/cardlib/model/Document;Landroid/database/Cursor;Z)Lcom/google/android/music/ui/cardlib/model/Document;

    .line 140
    invoke-virtual {v0, p1}, Lcom/google/android/music/ui/cardlib/model/Document;->setIsEmulatedRadio(Z)V

    .line 141
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 143
    .end local v0    # "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    :cond_0
    return-object v1
.end method

.method public static buildPlaylistDocumentList(Landroid/content/Context;Landroid/database/Cursor;)Ljava/util/ArrayList;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/database/Cursor;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/music/ui/cardlib/model/Document;",
            ">;"
        }
    .end annotation

    .prologue
    .line 100
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 101
    .local v1, "listDoc":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/music/ui/cardlib/model/Document;>;"
    const/4 v2, -0x1

    invoke-interface {p1, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 102
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 103
    new-instance v0, Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-direct {v0}, Lcom/google/android/music/ui/cardlib/model/Document;-><init>()V

    .line 104
    .local v0, "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    invoke-static {p0, v0, p1}, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->getPlaylistDocument(Landroid/content/Context;Lcom/google/android/music/ui/cardlib/model/Document;Landroid/database/Cursor;)Lcom/google/android/music/ui/cardlib/model/Document;

    move-result-object v0

    .line 105
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 108
    .end local v0    # "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    :cond_0
    return-object v1
.end method

.method public static buildRadioDocumentList(Landroid/content/Context;Landroid/database/Cursor;)Ljava/util/ArrayList;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/database/Cursor;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/music/ui/cardlib/model/Document;",
            ">;"
        }
    .end annotation

    .prologue
    .line 112
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 113
    .local v1, "listDoc":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/music/ui/cardlib/model/Document;>;"
    const/4 v2, -0x1

    invoke-interface {p1, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 114
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 115
    new-instance v0, Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-direct {v0}, Lcom/google/android/music/ui/cardlib/model/Document;-><init>()V

    .line 116
    .local v0, "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    invoke-static {p0, v0, p1}, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->getRadioDocument(Landroid/content/Context;Lcom/google/android/music/ui/cardlib/model/Document;Landroid/database/Cursor;)Lcom/google/android/music/ui/cardlib/model/Document;

    move-result-object v0

    .line 117
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 120
    .end local v0    # "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    :cond_0
    return-object v1
.end method

.method public static buildTrackDocumentList(Landroid/database/Cursor;)Ljava/util/ArrayList;
    .locals 3
    .param p0, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/music/ui/cardlib/model/Document;",
            ">;"
        }
    .end annotation

    .prologue
    .line 124
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 125
    .local v1, "listDoc":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/music/ui/cardlib/model/Document;>;"
    const/4 v2, -0x1

    invoke-interface {p0, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 126
    :goto_0
    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 127
    new-instance v0, Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-direct {v0}, Lcom/google/android/music/ui/cardlib/model/Document;-><init>()V

    .line 128
    .local v0, "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    invoke-static {v0, p0}, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->getTrackDocument(Lcom/google/android/music/ui/cardlib/model/Document;Landroid/database/Cursor;)Lcom/google/android/music/ui/cardlib/model/Document;

    move-result-object v0

    .line 129
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 131
    .end local v0    # "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    :cond_0
    return-object v1
.end method

.method public static getAlbumDocument(Lcom/google/android/music/ui/cardlib/model/Document;Landroid/database/Cursor;)Lcom/google/android/music/ui/cardlib/model/Document;
    .locals 1
    .param p0, "doc"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 148
    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/model/Document;->reset()V

    .line 149
    sget-object v0, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ALBUM:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setType(Lcom/google/android/music/ui/cardlib/model/Document$Type;)V

    .line 150
    invoke-static {p0, p1}, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->populateDocument(Lcom/google/android/music/ui/cardlib/model/Document;Landroid/database/Cursor;)Lcom/google/android/music/ui/cardlib/model/Document;

    move-result-object p0

    .line 151
    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/model/Document;->getAlbumName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setTitle(Ljava/lang/String;)V

    .line 152
    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/model/Document;->getArtistName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setSubTitle(Ljava/lang/String;)V

    .line 153
    return-object p0
.end method

.method public static getArtistDocument(Lcom/google/android/music/ui/cardlib/model/Document;Landroid/database/Cursor;)Lcom/google/android/music/ui/cardlib/model/Document;
    .locals 2
    .param p0, "doc"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 158
    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/model/Document;->reset()V

    .line 159
    sget-object v0, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ARTIST:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setType(Lcom/google/android/music/ui/cardlib/model/Document$Type;)V

    .line 160
    invoke-static {p0, p1}, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->populateDocument(Lcom/google/android/music/ui/cardlib/model/Document;Landroid/database/Cursor;)Lcom/google/android/music/ui/cardlib/model/Document;

    move-result-object p0

    .line 161
    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/model/Document;->getArtistName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 162
    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/model/Document;->getArtistName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setTitle(Ljava/lang/String;)V

    .line 164
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/model/Document;->getId()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/music/ui/cardlib/model/Document;->setArtistId(J)V

    .line 165
    return-object p0
.end method

.method public static getGenreDocument(Lcom/google/android/music/ui/cardlib/model/Document;Landroid/database/Cursor;Z)Lcom/google/android/music/ui/cardlib/model/Document;
    .locals 1
    .param p0, "doc"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "isRadio"    # Z

    .prologue
    .line 205
    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/model/Document;->reset()V

    .line 206
    sget-object v0, Lcom/google/android/music/ui/cardlib/model/Document$Type;->NAUTILUS_GENRE:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setType(Lcom/google/android/music/ui/cardlib/model/Document$Type;)V

    .line 207
    invoke-static {p0, p1}, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->populateDocument(Lcom/google/android/music/ui/cardlib/model/Document;Landroid/database/Cursor;)Lcom/google/android/music/ui/cardlib/model/Document;

    .line 208
    return-object p0
.end method

.method public static getPlaylistDocument(Landroid/content/Context;Lcom/google/android/music/ui/cardlib/model/Document;Landroid/database/Cursor;)Lcom/google/android/music/ui/cardlib/model/Document;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "doc"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p2, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 169
    invoke-virtual {p1}, Lcom/google/android/music/ui/cardlib/model/Document;->reset()V

    .line 170
    sget-object v2, Lcom/google/android/music/ui/cardlib/model/Document$Type;->PLAYLIST:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    invoke-virtual {p1, v2}, Lcom/google/android/music/ui/cardlib/model/Document;->setType(Lcom/google/android/music/ui/cardlib/model/Document$Type;)V

    .line 171
    invoke-static {p1, p2}, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->populateDocument(Lcom/google/android/music/ui/cardlib/model/Document;Landroid/database/Cursor;)Lcom/google/android/music/ui/cardlib/model/Document;

    move-result-object p1

    .line 174
    invoke-virtual {p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getPlaylistOwnerName()Ljava/lang/String;

    move-result-object v0

    .line 175
    .local v0, "ownerName":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 176
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0102

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 182
    .local v1, "subtitle":Ljava/lang/String;
    :goto_0
    invoke-virtual {p1}, Lcom/google/android/music/ui/cardlib/model/Document;->getPlaylistName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/google/android/music/ui/cardlib/model/Document;->setTitle(Ljava/lang/String;)V

    .line 183
    invoke-virtual {p1, v1}, Lcom/google/android/music/ui/cardlib/model/Document;->setSubTitle(Ljava/lang/String;)V

    .line 185
    return-object p1

    .line 179
    .end local v1    # "subtitle":Ljava/lang/String;
    :cond_0
    const-string v1, ""

    .restart local v1    # "subtitle":Ljava/lang/String;
    goto :goto_0
.end method

.method public static getRadioDocument(Landroid/content/Context;Lcom/google/android/music/ui/cardlib/model/Document;Landroid/database/Cursor;)Lcom/google/android/music/ui/cardlib/model/Document;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "doc"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p2, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 189
    invoke-virtual {p1}, Lcom/google/android/music/ui/cardlib/model/Document;->reset()V

    .line 190
    sget-object v0, Lcom/google/android/music/ui/cardlib/model/Document$Type;->RADIO:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    invoke-virtual {p1, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setType(Lcom/google/android/music/ui/cardlib/model/Document$Type;)V

    .line 191
    invoke-static {p1, p2}, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->populateDocument(Lcom/google/android/music/ui/cardlib/model/Document;Landroid/database/Cursor;)Lcom/google/android/music/ui/cardlib/model/Document;

    move-result-object p1

    .line 192
    return-object p1
.end method

.method public static getTrackDocument(Lcom/google/android/music/ui/cardlib/model/Document;Landroid/database/Cursor;)Lcom/google/android/music/ui/cardlib/model/Document;
    .locals 1
    .param p0, "doc"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 197
    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/model/Document;->reset()V

    .line 198
    sget-object v0, Lcom/google/android/music/ui/cardlib/model/Document$Type;->TRACK:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setType(Lcom/google/android/music/ui/cardlib/model/Document$Type;)V

    .line 199
    invoke-static {p0, p1}, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->populateDocument(Lcom/google/android/music/ui/cardlib/model/Document;Landroid/database/Cursor;)Lcom/google/android/music/ui/cardlib/model/Document;

    move-result-object p0

    .line 200
    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/model/Document;->getArtistName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setSubTitle(Ljava/lang/String;)V

    .line 201
    return-object p0
.end method

.method private static populateDocument(Lcom/google/android/music/ui/cardlib/model/Document;Landroid/database/Cursor;)Lcom/google/android/music/ui/cardlib/model/Document;
    .locals 2
    .param p0, "doc"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 213
    sget v0, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->sIdIdx:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 214
    sget v0, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->sIdIdx:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/music/ui/cardlib/model/Document;->setId(J)V

    .line 216
    :cond_0
    sget v0, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->sAlbumIdIdx:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 217
    sget v0, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->sAlbumIdIdx:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/music/ui/cardlib/model/Document;->setAlbumId(J)V

    .line 219
    :cond_1
    sget v0, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->sAlbumNameIdx:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 220
    sget v0, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->sAlbumNameIdx:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setAlbumName(Ljava/lang/String;)V

    .line 222
    :cond_2
    sget v0, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->sAlbumArtistIdIdx:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_3

    .line 223
    sget v0, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->sAlbumArtistIdIdx:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/music/ui/cardlib/model/Document;->setArtistId(J)V

    .line 225
    :cond_3
    sget v0, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->sAlbumArtistNameIdx:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_4

    .line 226
    sget v0, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->sAlbumArtistNameIdx:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setArtistName(Ljava/lang/String;)V

    .line 230
    :cond_4
    sget v0, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->sArtistNameIdx:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_5

    .line 231
    sget v0, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->sArtistNameIdx:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setArtistName(Ljava/lang/String;)V

    .line 233
    :cond_5
    sget v0, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->sArtworkUrlIdx:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_6

    .line 234
    sget v0, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->sArtworkUrlIdx:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setArtUrl(Ljava/lang/String;)V

    .line 236
    :cond_6
    sget v0, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->sPlaylistTypeIdx:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_7

    .line 237
    sget v0, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->sPlaylistTypeIdx:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setPlaylistType(I)V

    .line 239
    :cond_7
    sget v0, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->sItemNameIdx:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_8

    .line 240
    sget v0, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->sItemNameIdx:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setTitle(Ljava/lang/String;)V

    .line 242
    :cond_8
    sget v0, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->sTrackMetajamIdIdx:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_9

    .line 243
    sget v0, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->sTrackMetajamIdIdx:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setTrackMetajamId(Ljava/lang/String;)V

    .line 245
    :cond_9
    sget v0, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->sAlbumMetajamIdIdx:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_a

    .line 246
    sget v0, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->sAlbumMetajamIdIdx:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setAlbumMetajamId(Ljava/lang/String;)V

    .line 248
    :cond_a
    sget v0, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->sArtistMetajamIdIdx:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_b

    .line 249
    sget v0, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->sArtistMetajamIdIdx:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setArtistMetajamId(Ljava/lang/String;)V

    .line 251
    :cond_b
    sget v0, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->sGenreArtUrlsIdx:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_c

    .line 252
    sget v0, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->sGenreArtUrlsIdx:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setArtUrl(Ljava/lang/String;)V

    .line 254
    :cond_c
    sget v0, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->sGenreIdIdx:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_d

    .line 255
    sget v0, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->sGenreIdIdx:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setGenreId(Ljava/lang/String;)V

    .line 257
    :cond_d
    sget v0, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->sGenreNameIdx:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_e

    .line 258
    sget v0, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->sGenreNameIdx:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setTitle(Ljava/lang/String;)V

    .line 260
    :cond_e
    sget v0, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->sGenreParentIdIdx:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_f

    .line 261
    sget v0, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->sGenreParentIdIdx:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setParentGenreId(Ljava/lang/String;)V

    .line 263
    :cond_f
    sget v0, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->sGenreSubgenreCountIdx:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_10

    .line 264
    sget v0, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->sGenreSubgenreCountIdx:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setSubgenreCount(I)V

    .line 266
    :cond_10
    sget v0, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->sPlaylistNameIdx:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_11

    .line 267
    sget v0, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->sPlaylistNameIdx:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setPlaylistName(Ljava/lang/String;)V

    .line 269
    :cond_11
    sget v0, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->sPlaylistShareTokenIdx:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_12

    .line 270
    sget v0, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->sPlaylistShareTokenIdx:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setPlaylistShareToken(Ljava/lang/String;)V

    .line 272
    :cond_12
    sget v0, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->sPlaylistOwnerNameIdx:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_13

    .line 273
    sget v0, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->sPlaylistOwnerNameIdx:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setPlaylistOwnerName(Ljava/lang/String;)V

    .line 275
    :cond_13
    sget v0, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->sPlaylistDescriptionIdx:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_14

    .line 276
    sget v0, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->sPlaylistDescriptionIdx:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setDescription(Ljava/lang/String;)V

    .line 278
    :cond_14
    sget v0, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->sPlaylistOwnerProfilePhotoUrlIdx:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_15

    .line 279
    sget v0, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->sPlaylistOwnerProfilePhotoUrlIdx:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setProfilePhotoUrl(Ljava/lang/String;)V

    .line 281
    :cond_15
    sget v0, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->sVideoIdIdx:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_16

    .line 282
    sget v0, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->sVideoIdIdx:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setVideoId(Ljava/lang/String;)V

    .line 284
    :cond_16
    sget v0, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->sRadioNameIdx:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_17

    .line 285
    sget v0, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->sRadioNameIdx:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setTitle(Ljava/lang/String;)V

    .line 287
    :cond_17
    sget v0, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->sRadioDescriptionIdx:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_18

    .line 288
    sget v0, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->sRadioDescriptionIdx:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setSubTitle(Ljava/lang/String;)V

    .line 290
    :cond_18
    sget v0, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->sRadioSeedSourceIdIdx:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_19

    .line 291
    sget v0, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->sRadioSeedSourceIdIdx:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setRadioSeedId(Ljava/lang/String;)V

    .line 293
    :cond_19
    sget v0, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->sRadioSeedSourceTypeIdx:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_1a

    .line 294
    sget v0, Lcom/google/android/music/ui/cardlib/model/SearchDocumentBuilder;->sRadioSeedSourceTypeIdx:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setRadioSeedType(I)V

    .line 296
    :cond_1a
    return-object p0
.end method
