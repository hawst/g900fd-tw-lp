.class public interface abstract Lcom/google/android/music/cloudclient/MusicCloud;
.super Ljava/lang/Object;
.source "MusicCloud.java"


# static fields
.field public static final LOGV:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->CLOUD_CLIENT:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/cloudclient/MusicCloud;->LOGV:Z

    return-void
.end method


# virtual methods
.method public abstract createPlaylist(Lcom/google/android/music/sync/google/model/SyncablePlaylist;)Lcom/google/android/music/sync/google/model/SyncablePlaylist;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract createRadioStation(Lcom/google/android/music/sync/google/model/SyncableRadioStation;ZII)Lcom/google/android/music/cloudclient/RadioEditStationsResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract deleteUserDevice(Ljava/lang/String;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract enableNautilus(Ljava/lang/String;)Lcom/google/android/music/cloudclient/EnableNautilusResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getArtistShuffleFeed(Ljava/lang/String;II)Lcom/google/android/music/cloudclient/RadioFeedResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getCloudQueue()Lcom/google/android/music/cloudclient/GetCloudQueueResponseJson;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getEphemeralTopTracks(ILjava/lang/String;I)Lcom/google/android/music/sync/google/model/TrackFeed;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getGenres(Ljava/lang/String;)Lcom/google/android/music/cloudclient/MusicGenresResponseJson;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getListenNowRecommendations()Lcom/google/android/music/cloudclient/ListenNowRecommendationsJson;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getLuckyRadioFeed(ILjava/util/List;I)Lcom/google/android/music/cloudclient/RadioFeedResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/cloudclient/MixTrackId;",
            ">;I)",
            "Lcom/google/android/music/cloudclient/RadioFeedResponse;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getMixesFeed(III)Lcom/google/android/music/cloudclient/RadioFeedResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getNautilusAlbum(Ljava/lang/String;)Lcom/google/android/music/cloudclient/AlbumJson;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getNautilusArtist(Ljava/lang/String;IIZ)Lcom/google/android/music/cloudclient/ArtistJson;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getNautilusTrack(Ljava/lang/String;)Lcom/google/android/music/sync/google/model/Track;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getOffersForAccount(Landroid/accounts/Account;)Lcom/google/android/music/cloudclient/OffersResponseJson;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getOffersForAccountAndRedeemCoupon(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/music/cloudclient/OffersResponseJson;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getPlaylist(Ljava/lang/String;)Lcom/google/android/music/sync/google/model/SyncablePlaylist;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getRadioFeed(Ljava/lang/String;ILjava/util/List;I)Lcom/google/android/music/cloudclient/RadioFeedResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/cloudclient/MixTrackId;",
            ">;I)",
            "Lcom/google/android/music/cloudclient/RadioFeedResponse;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getSharedEntries(Ljava/lang/String;ILjava/lang/String;J)Lcom/google/android/music/cloudclient/GetSharedPlaylistEntriesResponseJson;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getSituations()Lcom/google/android/music/cloudclient/GetSituationsResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getSoundSearchEntries(Ljava/lang/String;I)Lcom/google/android/music/cloudclient/GetSoundSearchPlaylistResponseJson;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getTab(Lcom/google/android/music/sync/api/MusicUrl$ExploreTabType;ILjava/lang/String;)Lcom/google/android/music/cloudclient/TabJson;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getUserDevicesResponseJson()Lcom/google/android/music/cloudclient/UserDevicesListResponseJson;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getUserQuizGenresList()Lcom/google/android/music/cloudclient/QuizGenresResponseJson;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract isPlaylistShared(Ljava/lang/String;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract search(Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/music/cloudclient/SearchClientResponseJson;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract setCloudQueue(Lcom/google/android/music/cloudclient/SetCloudQueueRequestJson;)Lcom/google/android/music/cloudclient/SetCloudQueueResponseJson;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract syncCloudQueue(Lcom/google/android/music/cloudclient/SyncCloudQueueRequestJson;)Lcom/google/android/music/cloudclient/SyncCloudQueueResponseJson;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract updateCloudQueue(Lcom/google/android/music/cloudclient/UpdateCloudQueueRequestJson;)Lcom/google/android/music/cloudclient/UpdateCloudQueueResponseJson;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract updatePlaylist(Lcom/google/android/music/sync/google/model/SyncablePlaylist;)Lcom/google/android/music/sync/google/model/SyncablePlaylist;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation
.end method
