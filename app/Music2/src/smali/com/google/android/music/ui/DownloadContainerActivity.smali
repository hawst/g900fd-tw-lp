.class public Lcom/google/android/music/ui/DownloadContainerActivity;
.super Lcom/google/android/music/ui/BaseActivity;
.source "DownloadContainerActivity.java"


# instance fields
.field private final mKeeponSchedulingServiceConnection:Lcom/google/android/music/download/keepon/KeeponSchedulingServiceConnection;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/google/android/music/ui/BaseActivity;-><init>()V

    .line 16
    new-instance v0, Lcom/google/android/music/download/keepon/KeeponSchedulingServiceConnection;

    invoke-direct {v0}, Lcom/google/android/music/download/keepon/KeeponSchedulingServiceConnection;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/ui/DownloadContainerActivity;->mKeeponSchedulingServiceConnection:Lcom/google/android/music/download/keepon/KeeponSchedulingServiceConnection;

    return-void
.end method


# virtual methods
.method protected doesSupportDownloadOnlyBanner()Z
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x0

    return v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 21
    invoke-super {p0, p1}, Lcom/google/android/music/ui/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 23
    invoke-static {}, Lcom/google/android/music/utils/ConfigUtils;->isManageDownloadsEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 24
    invoke-virtual {p0}, Lcom/google/android/music/ui/DownloadContainerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0083

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/music/ui/DownloadContainerActivity;->setActionBarTitle(Ljava/lang/String;)V

    .line 28
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/music/ui/DownloadContainerActivity;->getContent()Landroid/support/v4/app/Fragment;

    move-result-object v1

    if-nez v1, :cond_0

    .line 30
    invoke-static {}, Lcom/google/android/music/utils/ConfigUtils;->isManageDownloadsEnabled()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 31
    new-instance v0, Lcom/google/android/music/ui/ManageDownloadsFragment;

    invoke-direct {v0}, Lcom/google/android/music/ui/ManageDownloadsFragment;-><init>()V

    .line 35
    .local v0, "fragment":Landroid/support/v4/app/Fragment;
    :goto_1
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/music/ui/DownloadContainerActivity;->replaceContent(Landroid/support/v4/app/Fragment;Z)V

    .line 38
    .end local v0    # "fragment":Landroid/support/v4/app/Fragment;
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/ui/DownloadContainerActivity;->mKeeponSchedulingServiceConnection:Lcom/google/android/music/download/keepon/KeeponSchedulingServiceConnection;

    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/google/android/music/download/keepon/KeeponSchedulingService;

    invoke-direct {v2, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v3, 0x1

    invoke-virtual {v1, p0, v2, v3}, Lcom/google/android/music/download/keepon/KeeponSchedulingServiceConnection;->bindService(Landroid/content/Context;Landroid/content/Intent;I)Z

    .line 41
    return-void

    .line 26
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/music/ui/DownloadContainerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b02c6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/music/ui/DownloadContainerActivity;->setActionBarTitle(Ljava/lang/String;)V

    goto :goto_0

    .line 33
    :cond_2
    new-instance v0, Lcom/google/android/music/ui/DownloadContainerFragment;

    invoke-direct {v0}, Lcom/google/android/music/ui/DownloadContainerFragment;-><init>()V

    .restart local v0    # "fragment":Landroid/support/v4/app/Fragment;
    goto :goto_1
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/music/ui/DownloadContainerActivity;->mKeeponSchedulingServiceConnection:Lcom/google/android/music/download/keepon/KeeponSchedulingServiceConnection;

    invoke-virtual {v0, p0}, Lcom/google/android/music/download/keepon/KeeponSchedulingServiceConnection;->unbindService(Landroid/content/Context;)V

    .line 51
    invoke-super {p0}, Lcom/google/android/music/ui/BaseActivity;->onDestroy()V

    .line 52
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 45
    invoke-super {p0}, Lcom/google/android/music/ui/BaseActivity;->onResume()V

    .line 46
    return-void
.end method
