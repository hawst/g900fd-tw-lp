.class public Lcom/google/android/music/widgets/ArtImageCheckbox;
.super Lcom/google/android/music/widgets/ArtImageView;
.source "ArtImageCheckbox.java"

# interfaces
.implements Landroid/widget/Checkable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/widgets/ArtImageCheckbox$OnCheckedChangeListener;
    }
.end annotation


# static fields
.field private static final LOGV:Z

.field private static sArtistOverlayColors:[I


# instance fields
.field private mCheckbox:Lcom/google/android/music/widgets/InternalImageView;

.field private mOverlayColor:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->UI:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/widgets/ArtImageCheckbox;->LOGV:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 46
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/music/widgets/ArtImageCheckbox;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 47
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 50
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/music/widgets/ArtImageCheckbox;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 51
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 54
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/music/widgets/ArtImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 55
    invoke-direct {p0, p2}, Lcom/google/android/music/widgets/ArtImageCheckbox;->init(Landroid/util/AttributeSet;)V

    .line 56
    sget-object v0, Lcom/google/android/music/art/ArtType;->USER_QUIZ:Lcom/google/android/music/art/ArtType;

    iput-object v0, p0, Lcom/google/android/music/widgets/ArtImageCheckbox;->mArtType:Lcom/google/android/music/art/ArtType;

    .line 57
    return-void
.end method

.method private static getArtistOverlayColor(Ljava/lang/String;)I
    .locals 6
    .param p0, "metajamId"    # Ljava/lang/String;

    .prologue
    .line 90
    sget-object v1, Lcom/google/android/music/widgets/ArtImageCheckbox;->sArtistOverlayColors:[I

    invoke-static {v1}, Lcom/google/android/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    invoke-static {p0}, Lcom/google/android/music/store/Store;->generateId(Ljava/lang/String;)J

    move-result-wide v2

    sget-object v1, Lcom/google/android/music/widgets/ArtImageCheckbox;->sArtistOverlayColors:[I

    array-length v1, v1

    int-to-long v4, v1

    rem-long/2addr v2, v4

    long-to-int v0, v2

    .line 92
    .local v0, "index":I
    sget-object v1, Lcom/google/android/music/widgets/ArtImageCheckbox;->sArtistOverlayColors:[I

    aget v1, v1, v0

    return v1
.end method

.method private init(Landroid/util/AttributeSet;)V
    .locals 8
    .param p1, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 60
    invoke-virtual {p0}, Lcom/google/android/music/widgets/ArtImageCheckbox;->getContext()Landroid/content/Context;

    move-result-object v5

    sget-object v6, Lcom/google/android/music/R$styleable;->ArtImageCheckbox:[I

    invoke-virtual {v5, p1, v6}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 61
    .local v0, "a":Landroid/content/res/TypedArray;
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->getIndexCount()I

    move-result v3

    .line 63
    .local v3, "count":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    if-ge v4, v3, :cond_1

    .line 64
    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getIndex(I)I

    move-result v1

    .line 65
    .local v1, "attr":I
    const v5, 0x1010106

    if-ne v1, v5, :cond_0

    .line 66
    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v2

    .line 67
    .local v2, "checked":Z
    invoke-virtual {p0, v2}, Lcom/google/android/music/widgets/ArtImageCheckbox;->setChecked(Z)V

    .line 63
    .end local v2    # "checked":Z
    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 70
    .end local v1    # "attr":I
    :cond_1
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 72
    sget-object v5, Lcom/google/android/music/widgets/ArtImageCheckbox;->sArtistOverlayColors:[I

    if-nez v5, :cond_3

    .line 73
    const-class v6, Lcom/google/android/music/art/ArtUtils;

    monitor-enter v6

    .line 74
    :try_start_0
    sget-object v5, Lcom/google/android/music/widgets/ArtImageCheckbox;->sArtistOverlayColors:[I

    if-nez v5, :cond_2

    .line 75
    invoke-virtual {p0}, Lcom/google/android/music/widgets/ArtImageCheckbox;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const/high16 v7, 0x7f110000

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v5

    sput-object v5, Lcom/google/android/music/widgets/ArtImageCheckbox;->sArtistOverlayColors:[I

    .line 78
    :cond_2
    monitor-exit v6

    .line 80
    :cond_3
    return-void

    .line 78
    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5
.end method


# virtual methods
.method public bind(Lcom/google/android/music/cloudclient/QuizArtistJson;)V
    .locals 1
    .param p1, "artistJson"    # Lcom/google/android/music/cloudclient/QuizArtistJson;

    .prologue
    .line 97
    invoke-super {p0, p1}, Lcom/google/android/music/widgets/ArtImageView;->bind(Lcom/google/android/music/cloudclient/QuizArtistJson;)V

    .line 98
    iget-object v0, p1, Lcom/google/android/music/cloudclient/QuizArtistJson;->mArtistId:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/music/widgets/ArtImageCheckbox;->getArtistOverlayColor(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/widgets/ArtImageCheckbox;->mOverlayColor:I

    .line 99
    return-void
.end method

.method public bind(Lcom/google/android/music/cloudclient/QuizGenreJson;)V
    .locals 6
    .param p1, "genreJson"    # Lcom/google/android/music/cloudclient/QuizGenreJson;

    .prologue
    const/high16 v5, -0x1000000

    .line 103
    invoke-super {p0, p1}, Lcom/google/android/music/widgets/ArtImageView;->bind(Lcom/google/android/music/cloudclient/QuizGenreJson;)V

    .line 104
    iget-object v0, p1, Lcom/google/android/music/cloudclient/QuizGenreJson;->mColor:Ljava/lang/String;

    .line 105
    .local v0, "color":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 107
    :try_start_0
    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/google/android/music/widgets/ArtImageCheckbox;->mOverlayColor:I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 115
    :goto_0
    return-void

    .line 108
    :catch_0
    move-exception v1

    .line 109
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    const-string v2, "MusicArtCheckbox"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Illegal color: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 110
    iput v5, p0, Lcom/google/android/music/widgets/ArtImageCheckbox;->mOverlayColor:I

    goto :goto_0

    .line 113
    .end local v1    # "e":Ljava/lang/IllegalArgumentException;
    :cond_0
    iput v5, p0, Lcom/google/android/music/widgets/ArtImageCheckbox;->mOverlayColor:I

    goto :goto_0
.end method

.method protected cleanUpArtRequest()V
    .locals 1

    .prologue
    .line 119
    invoke-super {p0}, Lcom/google/android/music/widgets/ArtImageView;->cleanUpArtRequest()V

    .line 120
    const/high16 v0, -0x1000000

    iput v0, p0, Lcom/google/android/music/widgets/ArtImageCheckbox;->mOverlayColor:I

    .line 121
    return-void
.end method

.method protected createForegroundUpdateTask()Lcom/google/android/music/art/UpdateForegroundTask;
    .locals 9

    .prologue
    .line 125
    new-instance v0, Lcom/google/android/music/art/UpdateCheckboxForegroundTask;

    invoke-virtual {p0}, Lcom/google/android/music/widgets/ArtImageCheckbox;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/music/widgets/ArtImageCheckbox;->mArtRequest:Lcom/google/android/music/art/ArtRequest;

    iget-boolean v4, p0, Lcom/google/android/music/widgets/ArtImageCheckbox;->mCropToCircle:Z

    iget v5, p0, Lcom/google/android/music/widgets/ArtImageCheckbox;->mOverlayColor:I

    iget v6, p0, Lcom/google/android/music/widgets/ArtImageCheckbox;->mDefaultArtId:I

    invoke-virtual {p0}, Lcom/google/android/music/widgets/ArtImageCheckbox;->getWidth()I

    move-result v7

    invoke-virtual {p0}, Lcom/google/android/music/widgets/ArtImageCheckbox;->getHeight()I

    move-result v8

    move-object v2, p0

    invoke-direct/range {v0 .. v8}, Lcom/google/android/music/art/UpdateCheckboxForegroundTask;-><init>(Landroid/content/Context;Lcom/google/android/music/art/UpdateForegroundTask$Callback;Lcom/google/android/music/art/ArtRequest;ZIIII)V

    return-object v0
.end method

.method protected createForegroundView()Lcom/google/android/music/widgets/InternalImageView;
    .locals 2

    .prologue
    .line 85
    new-instance v0, Lcom/google/android/music/widgets/InternalImageView;

    const/4 v1, 0x1

    invoke-direct {v0, p0, v1}, Lcom/google/android/music/widgets/InternalImageView;-><init>(Lcom/google/android/music/widgets/ArtImageView;Z)V

    iput-object v0, p0, Lcom/google/android/music/widgets/ArtImageCheckbox;->mCheckbox:Lcom/google/android/music/widgets/InternalImageView;

    .line 86
    iget-object v0, p0, Lcom/google/android/music/widgets/ArtImageCheckbox;->mCheckbox:Lcom/google/android/music/widgets/InternalImageView;

    return-object v0
.end method

.method public isChecked()Z
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/google/android/music/widgets/ArtImageCheckbox;->mCheckbox:Lcom/google/android/music/widgets/InternalImageView;

    invoke-virtual {v0}, Lcom/google/android/music/widgets/InternalImageView;->isChecked()Z

    move-result v0

    return v0
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 131
    invoke-super {p0, p1}, Lcom/google/android/music/widgets/ArtImageView;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 132
    const-class v0, Lcom/google/android/music/widgets/ArtImageCheckbox;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    .line 133
    invoke-virtual {p0}, Lcom/google/android/music/widgets/ArtImageCheckbox;->isChecked()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setChecked(Z)V

    .line 134
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 1
    .param p1, "info"    # Landroid/view/accessibility/AccessibilityNodeInfo;

    .prologue
    .line 138
    invoke-super {p0, p1}, Lcom/google/android/music/widgets/ArtImageView;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    .line 139
    invoke-static {}, Lcom/google/android/music/preferences/MusicPreferences;->isICSOrGreater()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 140
    const-class v0, Lcom/google/android/music/widgets/ArtImageCheckbox;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    .line 141
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setCheckable(Z)V

    .line 142
    invoke-virtual {p0}, Lcom/google/android/music/widgets/ArtImageCheckbox;->isChecked()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setChecked(Z)V

    .line 144
    :cond_0
    return-void
.end method

.method public setChecked(Z)V
    .locals 1
    .param p1, "checked"    # Z

    .prologue
    .line 148
    iget-object v0, p0, Lcom/google/android/music/widgets/ArtImageCheckbox;->mCheckbox:Lcom/google/android/music/widgets/InternalImageView;

    invoke-virtual {v0, p1}, Lcom/google/android/music/widgets/InternalImageView;->setChecked(Z)V

    .line 149
    return-void
.end method

.method public setOnCheckedChangeListener(Lcom/google/android/music/widgets/ArtImageCheckbox$OnCheckedChangeListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/google/android/music/widgets/ArtImageCheckbox$OnCheckedChangeListener;

    .prologue
    .line 162
    iget-object v0, p0, Lcom/google/android/music/widgets/ArtImageCheckbox;->mCheckbox:Lcom/google/android/music/widgets/InternalImageView;

    invoke-virtual {v0, p1}, Lcom/google/android/music/widgets/InternalImageView;->setOnCheckedChangeListener(Lcom/google/android/music/widgets/ArtImageCheckbox$OnCheckedChangeListener;)V

    .line 163
    return-void
.end method

.method public toggle()V
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lcom/google/android/music/widgets/ArtImageCheckbox;->mCheckbox:Lcom/google/android/music/widgets/InternalImageView;

    invoke-virtual {v0}, Lcom/google/android/music/widgets/InternalImageView;->toggle()V

    .line 159
    return-void
.end method
