.class Lcom/google/android/music/playback/MusicPlaybackService$6;
.super Lcom/google/android/music/playback/session/MediaSessionCompat$Callback;
.source "MusicPlaybackService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/playback/MusicPlaybackService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/playback/MusicPlaybackService;


# direct methods
.method constructor <init>(Lcom/google/android/music/playback/MusicPlaybackService;)V
    .locals 0

    .prologue
    .line 582
    iput-object p1, p0, Lcom/google/android/music/playback/MusicPlaybackService$6;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-direct {p0}, Lcom/google/android/music/playback/session/MediaSessionCompat$Callback;-><init>()V

    return-void
.end method


# virtual methods
.method public onCommand(Ljava/lang/String;Landroid/os/Bundle;Landroid/os/ResultReceiver;)V
    .locals 0
    .param p1, "command"    # Ljava/lang/String;
    .param p2, "extras"    # Landroid/os/Bundle;
    .param p3, "cb"    # Landroid/os/ResultReceiver;

    .prologue
    .line 585
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/music/playback/session/MediaSessionCompat$Callback;->onCommand(Ljava/lang/String;Landroid/os/Bundle;Landroid/os/ResultReceiver;)V

    .line 586
    return-void
.end method

.method public onCustomAction(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 1
    .param p1, "action"    # Ljava/lang/String;
    .param p2, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 648
    new-instance v0, Lcom/google/android/music/playback/MusicPlaybackService$6$2;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/music/playback/MusicPlaybackService$6$2;-><init>(Lcom/google/android/music/playback/MusicPlaybackService$6;Ljava/lang/String;Landroid/os/Bundle;)V

    invoke-static {v0}, Lcom/google/android/music/utils/MusicUtils;->runAsync(Ljava/lang/Runnable;)V

    .line 694
    return-void
.end method

.method public onGetPlaybackPosition()J
    .locals 2

    .prologue
    .line 630
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$6;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    # getter for: Lcom/google/android/music/playback/MusicPlaybackService;->mRemotePlaybackControlEnabled:Z
    invoke-static {v0}, Lcom/google/android/music/playback/MusicPlaybackService;->access$600(Lcom/google/android/music/playback/MusicPlaybackService;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 631
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$6;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v0}, Lcom/google/android/music/playback/MusicPlaybackService;->position()J

    move-result-wide v0

    .line 633
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public onMediaButtonEvent(Landroid/content/Intent;)Z
    .locals 3
    .param p1, "mediaButtonEvent"    # Landroid/content/Intent;

    .prologue
    .line 590
    const-string v0, "MusicPlaybackService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onMediaButtonEvent: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 591
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$6;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-static {v0, p1}, Lcom/google/android/music/playback/MediaButtonIntentReceiver$MediaButtonReceiverHelper;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V

    .line 593
    const/4 v0, 0x1

    return v0
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 603
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$6;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v0}, Lcom/google/android/music/playback/MusicPlaybackService;->pause()V

    .line 604
    return-void
.end method

.method public onPlay()V
    .locals 1

    .prologue
    .line 598
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$6;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v0}, Lcom/google/android/music/playback/MusicPlaybackService;->play()V

    .line 599
    return-void
.end method

.method public onPlayFromMediaId(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 3
    .param p1, "mediaId"    # Ljava/lang/String;
    .param p2, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 698
    # getter for: Lcom/google/android/music/playback/MusicPlaybackService;->LOGV:Z
    invoke-static {}, Lcom/google/android/music/playback/MusicPlaybackService;->access$100()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 699
    const-string v0, "MusicPlaybackService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "play from mediaId: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 702
    :cond_0
    sget-object v0, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    new-instance v1, Lcom/google/android/music/playback/MusicPlaybackService$6$3;

    invoke-direct {v1, p0, p1}, Lcom/google/android/music/playback/MusicPlaybackService$6$3;-><init>(Lcom/google/android/music/playback/MusicPlaybackService$6;Ljava/lang/String;)V

    invoke-static {v0, v1}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsync(Lcom/google/android/music/utils/LoggableHandler;Ljava/lang/Runnable;)V

    .line 768
    return-void
.end method

.method public onPlayFromSearch(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 5
    .param p1, "query"    # Ljava/lang/String;
    .param p2, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 772
    # getter for: Lcom/google/android/music/playback/MusicPlaybackService;->LOGV:Z
    invoke-static {}, Lcom/google/android/music/playback/MusicPlaybackService;->access$100()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 773
    const-string v1, "MusicPlaybackService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onPlayFromSearch "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 778
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/playback/MusicPlaybackService$6;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    # getter for: Lcom/google/android/music/playback/MusicPlaybackService;->mVoiceActionHelper:Lcom/google/android/music/utils/VoiceActionHelper;
    invoke-static {v1}, Lcom/google/android/music/playback/MusicPlaybackService;->access$1000(Lcom/google/android/music/playback/MusicPlaybackService;)Lcom/google/android/music/utils/VoiceActionHelper;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 779
    iget-object v1, p0, Lcom/google/android/music/playback/MusicPlaybackService$6;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    # getter for: Lcom/google/android/music/playback/MusicPlaybackService;->mVoiceActionHelper:Lcom/google/android/music/utils/VoiceActionHelper;
    invoke-static {v1}, Lcom/google/android/music/playback/MusicPlaybackService;->access$1000(Lcom/google/android/music/playback/MusicPlaybackService;)Lcom/google/android/music/utils/VoiceActionHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/utils/VoiceActionHelper;->cancelSearch()V

    .line 785
    :cond_1
    iget-object v1, p0, Lcom/google/android/music/playback/MusicPlaybackService$6;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    new-instance v2, Lcom/google/android/music/utils/VoiceActionHelper;

    iget-object v3, p0, Lcom/google/android/music/playback/MusicPlaybackService$6;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    iget-object v4, p0, Lcom/google/android/music/playback/MusicPlaybackService$6;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    # getter for: Lcom/google/android/music/playback/MusicPlaybackService;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;
    invoke-static {v4}, Lcom/google/android/music/playback/MusicPlaybackService;->access$900(Lcom/google/android/music/playback/MusicPlaybackService;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/google/android/music/utils/VoiceActionHelper;-><init>(Landroid/content/Context;Lcom/google/android/music/preferences/MusicPreferences;)V

    # setter for: Lcom/google/android/music/playback/MusicPlaybackService;->mVoiceActionHelper:Lcom/google/android/music/utils/VoiceActionHelper;
    invoke-static {v1, v2}, Lcom/google/android/music/playback/MusicPlaybackService;->access$1002(Lcom/google/android/music/playback/MusicPlaybackService;Lcom/google/android/music/utils/VoiceActionHelper;)Lcom/google/android/music/utils/VoiceActionHelper;

    .line 787
    new-instance v0, Lcom/google/android/music/playback/MusicPlaybackService$6$4;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/music/playback/MusicPlaybackService$6$4;-><init>(Lcom/google/android/music/playback/MusicPlaybackService$6;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 825
    .local v0, "voiceSearchRunnable":Ljava/lang/Runnable;
    sget-object v1, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    # getter for: Lcom/google/android/music/playback/MusicPlaybackService;->MSG_VOICE_SEARCH:I
    invoke-static {}, Lcom/google/android/music/playback/MusicPlaybackService;->access$1100()I

    move-result v2

    const/4 v3, 0x1

    invoke-static {v1, v2, v0, v3}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsync(Lcom/google/android/music/utils/LoggableHandler;ILjava/lang/Runnable;Z)V

    .line 827
    return-void
.end method

.method public onSeekTo(J)V
    .locals 1
    .param p1, "pos"    # J

    .prologue
    .line 623
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$6;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    # getter for: Lcom/google/android/music/playback/MusicPlaybackService;->mRemotePlaybackControlEnabled:Z
    invoke-static {v0}, Lcom/google/android/music/playback/MusicPlaybackService;->access$600(Lcom/google/android/music/playback/MusicPlaybackService;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 624
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$6;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/music/playback/MusicPlaybackService;->seek(J)J

    .line 626
    :cond_0
    return-void
.end method

.method public onSetRating(Lcom/google/android/music/playback/session/RatingCompat;)V
    .locals 3
    .param p1, "rating"    # Lcom/google/android/music/playback/session/RatingCompat;

    .prologue
    .line 831
    invoke-virtual {p1}, Lcom/google/android/music/playback/session/RatingCompat;->getRatingStyle()I

    move-result v1

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    .line 848
    :goto_0
    return-void

    .line 835
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/music/playback/session/RatingCompat;->isRated()Z

    move-result v1

    if-nez v1, :cond_1

    .line 836
    const/4 v0, 0x0

    .line 843
    .local v0, "ratingValue":I
    :goto_1
    sget-object v1, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    new-instance v2, Lcom/google/android/music/playback/MusicPlaybackService$6$5;

    invoke-direct {v2, p0, v0}, Lcom/google/android/music/playback/MusicPlaybackService$6$5;-><init>(Lcom/google/android/music/playback/MusicPlaybackService$6;I)V

    invoke-static {v1, v2}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsync(Lcom/google/android/music/utils/LoggableHandler;Ljava/lang/Runnable;)V

    goto :goto_0

    .line 837
    .end local v0    # "ratingValue":I
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/music/playback/session/RatingCompat;->isThumbUp()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 838
    const/4 v0, 0x5

    .restart local v0    # "ratingValue":I
    goto :goto_1

    .line 840
    .end local v0    # "ratingValue":I
    :cond_2
    const/4 v0, 0x1

    .restart local v0    # "ratingValue":I
    goto :goto_1
.end method

.method public onSkipToNext()V
    .locals 1

    .prologue
    .line 608
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$6;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v0}, Lcom/google/android/music/playback/MusicPlaybackService;->next()V

    .line 609
    return-void
.end method

.method public onSkipToPrevious()V
    .locals 1

    .prologue
    .line 613
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$6;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v0}, Lcom/google/android/music/playback/MusicPlaybackService;->prev()V

    .line 614
    return-void
.end method

.method public onSkipToQueueItem(J)V
    .locals 3
    .param p1, "id"    # J

    .prologue
    .line 638
    sget-object v0, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    new-instance v1, Lcom/google/android/music/playback/MusicPlaybackService$6$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/music/playback/MusicPlaybackService$6$1;-><init>(Lcom/google/android/music/playback/MusicPlaybackService$6;J)V

    invoke-static {v0, v1}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsync(Lcom/google/android/music/utils/LoggableHandler;Ljava/lang/Runnable;)V

    .line 644
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 618
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$6;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v0}, Lcom/google/android/music/playback/MusicPlaybackService;->stop()V

    .line 619
    return-void
.end method
