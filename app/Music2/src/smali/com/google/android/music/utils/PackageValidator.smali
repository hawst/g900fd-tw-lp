.class public Lcom/google/android/music/utils/PackageValidator;
.super Ljava/lang/Object;
.source "PackageValidator.java"


# static fields
.field static final VALID_GEARHEAD_SIGNATURES:[[B


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 65
    const/4 v0, 0x2

    new-array v0, v0, [[B

    const/4 v1, 0x0

    const-string v2, "0\u0082\u0003\u00bd0\u0082\u0002\u00a5\u00a0\u0003\u0002\u0001\u0002\u0002\t\u0000\u00c7\u008f\u009eK\u0093A0\u00060\r\u0006\t*\u0086H\u0086\u00f7\r\u0001\u0001\u0005\u0005\u00000u1\u000b0\t\u0006\u0003U\u0004\u0006\u0013\u0002US1\u00130\u0011\u0006\u0003U\u0004\u0008\u000c\nCalifornia1\u00160\u0014\u0006\u0003U\u0004\u0007\u000c\rMountain View1\u00140\u0012\u0006\u0003U\u0004\n\u000c\u000bGoogle Inc.1\u00100\u000e\u0006\u0003U\u0004\u000b\u000c\u0007Android1\u00110\u000f\u0006\u0003U\u0004\u0003\u000c\u0008gearhead0\u001e\u0017\r140527230534Z\u0017\r411012230534Z0u1\u000b0\t\u0006\u0003U\u0004\u0006\u0013\u0002US1\u00130\u0011\u0006\u0003U\u0004\u0008\u000c\nCalifornia1\u00160\u0014\u0006\u0003U\u0004\u0007\u000c\rMountain View1\u00140\u0012\u0006\u0003U\u0004\n\u000c\u000bGoogle Inc.1\u00100\u000e\u0006\u0003U\u0004\u000b\u000c\u0007Android1\u00110\u000f\u0006\u0003U\u0004\u0003\u000c\u0008gearhead0\u0082\u0001\"0\r\u0006\t*\u0086H\u0086\u00f7\r\u0001\u0001\u0001\u0005\u0000\u0003\u0082\u0001\u000f\u00000\u0082\u0001\n\u0002\u0082\u0001\u0001\u0000\u00d3\u009d\u0017\u000eCH\u00b1TL_l\u0013\u00bdZe\u00a4+\u00b8:\u00d9\u00f24\u00ad\u00af\u00e4\u001e\u00cfK\u00e0\u00e0\u0082a\u00f6\u00ca\u00e6b\u00c2\u0094\u00ee\u00ad\u00d2\u0083C\u00d4}S<G\u00f5N%\u00b0/\u00a6#\u0015\u00e4\u0088\u0016\n!cU\u0080\u00cbbN\u000cd\u0013.\u00dc\u0081k\u00dd`x\rb\u0091n\u00f0\u008cY)\u0080\u00f2]\u00eb>\u00d3j_\u00be\u009b\u00ba\u00dc\u00c2\u0001\u000f\u00f3\u00e7\u00bdQb\u00a6\u008dhR\u00b6\u00dfz\u00d8\u00fe\u009a\u00ba\u0004\u00a69\u00c0\u00efX\u0014KC\u00a4\u00f8~\u0097Yk&o\u00ccE\u001d\u0005L\u00a1\u0095\u0084#;\u0014\'i\u00e1\u009b\u00c1\u001c\u009c\u00f9\u0000=\u00f3Y\u0000o\u00be\\\u00b3\u00d1:\u0084P\t\u00ab0\u00c9\u008b\u001d\u00e3bn`\u0003\u00f7\u000b\u0006n\u00847\u0014l\u00c5\u00a6\u0093\u00ba\u00c1\u008b\u00d0UC\u00c8&\u0092\u00b6\u00f0\u00aa\u008fx\u0003\u00ba\u0092\u00b4\u00b5)\u00dc\u00dc\u0082\u009aR\u0092Xv\u0099\u00d3\u0094\u00ac\u00a4C\u00f0\u00b1\u00f7-\u0091\u00ad(\\n[\u0086\u0004\u00fa\u00eb\u00b1\u000c\u000b4>b\u00c1M\u00d6\u0082Q/\u00b4*\u00fac\u0010\u008cRl\u00df\u0002\u0003\u0001\u0000\u0001\u00a3P0N0\u001d\u0006\u0003U\u001d\u000e\u0004\u0016\u0004\u0014\u001a\u00f0_`\u00d7\u00ae\u00e8\u0094\u0089RrY\n&\u0081\u001a\u00c9\u00d7\u00ce\u00db0\u001f\u0006\u0003U\u001d#\u0004\u00180\u0016\u0080\u0014\u001a\u00f0_`\u00d7\u00ae\u00e8\u0094\u0089RrY\n&\u0081\u001a\u00c9\u00d7\u00ce\u00db0\u000c\u0006\u0003U\u001d\u0013\u0004\u00050\u0003\u0001\u0001\u00ff0\r\u0006\t*\u0086H\u0086\u00f7\r\u0001\u0001\u0005\u0005\u0000\u0003\u0082\u0001\u0001\u0000\u0094k\u0003cA\u000f\u00bbsAH~d\u00ea,?\u00c0\u0098}{|L\u00c1-{\u0012\u00b2\u0086\u0096\u001c\u0096\u00a2\u000cI32\u00e3\u0000\u00de\u00a0\u00d1\u00a0\u008f\u001f\u0010x\u00d0\u0084\u0002\u00fb\u00ca\u0080\u0097\u00e4K\u00edT1\u00ea\u008cm\u00b5\u00fd&\u00df\\\u0094\u0019\u0003\u00dc5\u0086\u00ed\u00d8,AL +\u00f3\u00ceh,\u00aem\u00d90\"\u00e6\u00d43\u0085\u00de\u0099\u0011\u0088\u00a1Y%\u0016Q\u00df\u00d7\u00f0\u0014\u0011\u00a2\u00ec[\u00a2\u00cb=A\u00b0@\u00fe\"1\u00d0\u00eaCk\u0018\u0080r\u00ae\u00c2o\u00ae\u00d3\u0085\u00e5\u00d9\u000f\u0011\u00aeC\u00c7\u00e6\u001d\u0086\u00cb\u00c7\u00ce)\u0012\u00f9\u00b7\r\u0003\u0081\u00fc\u00b2\u000c\u0092JPI\u00f1\u0002\u00d5\u00ff\u00a8?\\\u00c1\u00de\u00ea\u00cfS\u00f7R\u00bc@\u00ff,(\u000ev\u00baqg\u0097b\u00ed,\u0012\u00ca\u00e7\u00beV\u00af\u00d3e\u000c\u00b7\u00e2\u00d3\u00f2\u0080L\u00c3\u00d9\u00df!\u0016X\u007f\u00c9\u00f8V\u0090\u00c8\u00b39\u00e2\u0017q\u00ac\u0095\u0001\u0007M\u009f\u009c\u00e9\u0006K\u009a\u00cb[$\u0018\u00e8\u00d0C\u0099\u0013l7\u0003\u00ce(\u000e\u00d9\u001d\u00ab\u00aa~\u0087\t\u00dfe\u00e5\u009d\u0016!"

    invoke-static {v2}, Lcom/google/android/music/utils/PackageValidator;->extractKey(Ljava/lang/String;)[B

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "0\u0082\u0003\u00bd0\u0082\u0002\u00a5\u00a0\u0003\u0002\u0001\u0002\u0002\t\u0000\u00e7\u00e4\u0006\u00f0\u00d7\u00c3\u0096\u00f30\r\u0006\t*\u0086H\u0086\u00f7\r\u0001\u0001\u0005\u0005\u00000u1\u000b0\t\u0006\u0003U\u0004\u0006\u0013\u0002US1\u00130\u0011\u0006\u0003U\u0004\u0008\u000c\nCalifornia1\u00160\u0014\u0006\u0003U\u0004\u0007\u000c\rMountain View1\u00140\u0012\u0006\u0003U\u0004\n\u000c\u000bGoogle Inc.1\u00100\u000e\u0006\u0003U\u0004\u000b\u000c\u0007Android1\u00110\u000f\u0006\u0003U\u0004\u0003\u000c\u0008gearhead0\u001e\u0017\r140527230251Z\u0017\r411012230251Z0u1\u000b0\t\u0006\u0003U\u0004\u0006\u0013\u0002US1\u00130\u0011\u0006\u0003U\u0004\u0008\u000c\nCalifornia1\u00160\u0014\u0006\u0003U\u0004\u0007\u000c\rMountain View1\u00140\u0012\u0006\u0003U\u0004\n\u000c\u000bGoogle Inc.1\u00100\u000e\u0006\u0003U\u0004\u000b\u000c\u0007Android1\u00110\u000f\u0006\u0003U\u0004\u0003\u000c\u0008gearhead0\u0082\u0001\"0\r\u0006\t*\u0086H\u0086\u00f7\r\u0001\u0001\u0001\u0005\u0000\u0003\u0082\u0001\u000f\u00000\u0082\u0001\n\u0002\u0082\u0001\u0001\u0000\u00a2\u00ee\u00f0\u00c0\u0012\u0085\u00cb9\u00ea\u00a5\u001a\u00de\u00b4\u009d\u00c4V\u009ey\u00fd\u008a\u00f4\u00e3\u00d0 \u00e7\tF\u00be\u00b0\u00a7\u008c\u0083\u00fc\u000e\u00b3+S\u00eb$|\u00a7\u00b5\u000el)\u00b0\u00b3m\u009e\u0018b4\u007f\u0089\u00d3M\u000b\u00a2M\u00e1s\u00c8\u00ddX\u00a7\u008a:\u008as(`\u00cd\u00bc\u00bf\u00c7\u00bet\u00bb\u00d1\u009c\u00a4\u00db\u00a8#\u00f6;L0|\u00fd\u00d9\u00a6]\u00a6l\u0003\u00eb\u00b1M\u00999F\u00d8Q\u0011\u00af\u00e4\u00f00>Z\u0081\u00a3\u00e7\u00b0Tv\u00ceV\u00ba\u00ba\u0005/\u001cl\u00f3\u00eb\u0096\u0003\u00c6\u0090\u0099\u00b1\u000f\u00d3\u00a3\u000c\u0083.|`1\u00a8/\u0086\u00f4\u00be9\u00ecw\u00ca\u001d\u00857\u00baI\u007f\u0004\u00b4\u00dc\u00a7FvE\u008fl\u00ba\u009f\u00f4W\u00a6\u00d3\u00db9\u008e7\u0099[\u00f3\u00b7F\t\u00ca\u00a1\u0013\u00c8\'\u0084\u000b+\u00bd\u001e~0\u0019\u00a8\u009c\u0081\u0019\u00c0\u00d9\u00c9\u00030:\u00cf\u00bc\u001c\u0089\'\u00ad\u00a7\u00fc\u00f9\u00c4Y$<\u00ea;\u001e\u00eb\u00b6\u00d9|3r\u0086\u0007a\u0005\u00964\u00e9\u00eb\u00f1r\u00c4\u0092\u00e7\u0002\u008e\u0090\u0095y\u00fb\u001a\u00b6\u001a\u009524\u00c8\u00b5=u\u0002\u0003\u0001\u0000\u0001\u00a3P0N0\u001d\u0006\u0003U\u001d\u000e\u0004\u0016\u0004\u0014\u00f5\u0003\u00c9\u00e7\u0012D\u000c\u000f\u000c\r\u0003+\u008fHf\u00db\u00f06\u0005\u00190\u001f\u0006\u0003U\u001d#\u0004\u00180\u0016\u0080\u0014\u00f5\u0003\u00c9\u00e7\u0012D\u000c\u000f\u000c\r\u0003+\u008fHf\u00db\u00f06\u0005\u00190\u000c\u0006\u0003U\u001d\u0013\u0004\u00050\u0003\u0001\u0001\u00ff0\r\u0006\t*\u0086H\u0086\u00f7\r\u0001\u0001\u0005\u0005\u0000\u0003\u0082\u0001\u0001\u0000\r\u00ca\u00f9\u0087QQ\u00f0\u008af7\u0088R\u00b1@=Jp\u0090W%\u00da\u00d4d!\u00ce\u0094 E\u00b1~\u009e\u0099 :}\u008cy\u00ba|m\u00dd\u00bcV\u0097\u00e0\u00a2\u0080\u00f68\u0013P\\%\u001cfI\u00fb\u00a5h\u00fe\u00fa\u00eb}\u001e\u0013\u009b\u001dV\u0095\u00e4S`\u00d2\u0097C\u00a8\u00b9\u00da\u00f5\u0006}c\u008a\u0012\u00f9\u009a\u00e2\u008c\u00ae\u00f4]\u009f\u00c4\u008eV\u0014\u001e\u00f8n\u00d2\u0092#d\u0006\u00c3\u00f0)\u0082\u0016Z0I\u001ey\u00a8$\u00a33\u0098\u0092\u00df\u00b2\u00d9\u0007}\u00922\u00bdA\u0006&+4\u000b\u00e7p\u00a8\u00d8AR\u00bcr\u00d4\u00d1\u00ce\u001aMA\u0003\u00c1\u0081p@\u00f7\u00c5\u00e5\u00f9\u00ddC?-4%d.\u0017K,\u009a\u0012\u009c&\u00eb\u00dftI\u00c5\u0017\u00b1\u00efk\u001c\u00ff\u0080$=\u009f6\u00ab@\u008d\u00c2$\u001f\u001d9up\u0017\u00c9\u009c\u00c84A\u00cf\u0082Q\u00f9\u0080\u00e9^\u008e\u0081\u000f\u00e7\u00c6\u00b7^h\u00bf\u00ec\u00e6\u00a8/1i?O\u00d7\u00f2`\u00a05\u00e22\u001c\u00bf\u00ea\u00bc v/V\u00c4\u00f7\u00fc\u0099\u00be\u00d3\u009c\u0010\u00be\nK\u0017\u00d0"

    invoke-static {v2}, Lcom/google/android/music/utils/PackageValidator;->extractKey(Ljava/lang/String;)[B

    move-result-object v2

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/utils/PackageValidator;->VALID_GEARHEAD_SIGNATURES:[[B

    return-void
.end method

.method private static extractKey(Ljava/lang/String;)[B
    .locals 3
    .param p0, "keyString"    # Ljava/lang/String;

    .prologue
    .line 54
    :try_start_0
    const-string v1, "ISO-8859-1"

    invoke-virtual {p0, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 57
    :goto_0
    return-object v1

    .line 55
    :catch_0
    move-exception v0

    .line 56
    .local v0, "e":Ljava/io/UnsupportedEncodingException;
    const-string v1, "PackageValidator"

    const-string v2, "Failed to extract GearHead key!"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 57
    const/4 v1, 0x0

    new-array v1, v1, [B

    goto :goto_0
.end method

.method private static isGearHeadSignatureCorrupted()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 62
    sget-object v2, Lcom/google/android/music/utils/PackageValidator;->VALID_GEARHEAD_SIGNATURES:[[B

    aget-object v2, v2, v0

    array-length v2, v2

    if-eqz v2, :cond_0

    sget-object v2, Lcom/google/android/music/utils/PackageValidator;->VALID_GEARHEAD_SIGNATURES:[[B

    aget-object v2, v2, v1

    array-length v2, v2

    if-nez v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    return v0
.end method

.method public static isGoogleSignedGearHead(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z
    .locals 1
    .param p0, "packageManager"    # Landroid/content/pm/PackageManager;
    .param p1, "callingPackage"    # Ljava/lang/String;

    .prologue
    .line 16
    invoke-static {}, Lcom/google/android/music/utils/PackageValidator;->isGearHeadSignatureCorrupted()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/music/utils/PackageValidator;->VALID_GEARHEAD_SIGNATURES:[[B

    invoke-static {p0, p1, v0}, Lcom/google/android/music/utils/PackageValidator;->verifySignature(Landroid/content/pm/PackageManager;Ljava/lang/String;[[B)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static varargs verifySignature(Landroid/content/pm/PackageManager;Ljava/lang/String;[[B)Z
    .locals 9
    .param p0, "packageManager"    # Landroid/content/pm/PackageManager;
    .param p1, "callingPackage"    # Ljava/lang/String;
    .param p2, "allValidSignatures"    # [[B

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 24
    const/16 v7, 0x40

    :try_start_0
    invoke-virtual {p0, p1, v7}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 33
    .local v2, "packageInfo":Landroid/content/pm/PackageInfo;
    if-eqz v2, :cond_0

    iget-object v7, v2, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    if-eqz v7, :cond_0

    iget-object v7, v2, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    array-length v7, v7

    if-eq v7, v5, :cond_3

    :cond_0
    move v5, v6

    .line 44
    .end local v2    # "packageInfo":Landroid/content/pm/PackageInfo;
    :cond_1
    :goto_0
    return v5

    .line 26
    :catch_0
    move-exception v1

    .line 27
    .local v1, "ignored":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v5, "PackageValidator"

    const/4 v7, 0x3

    invoke-static {v5, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 28
    const-string v5, "PackageValidator"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Package manager can\'t find package "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", defaulting to false"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    move v5, v6

    .line 31
    goto :goto_0

    .line 37
    .end local v1    # "ignored":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v2    # "packageInfo":Landroid/content/pm/PackageInfo;
    :cond_3
    iget-object v7, v2, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    aget-object v7, v7, v6

    invoke-virtual {v7}, Landroid/content/pm/Signature;->toByteArray()[B

    move-result-object v3

    .line 38
    .local v3, "signature":[B
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    array-length v7, p2

    if-ge v0, v7, :cond_4

    .line 39
    aget-object v4, p2, v0

    .line 40
    .local v4, "validSignature":[B
    invoke-static {v4, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v7

    if-nez v7, :cond_1

    .line 38
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .end local v4    # "validSignature":[B
    :cond_4
    move v5, v6

    .line 44
    goto :goto_0
.end method
