.class Lcom/google/android/music/art/ArtLoaderImpl$SyncRequestRunnable$1;
.super Ljava/lang/Object;
.source "ArtLoaderImpl.java"

# interfaces
.implements Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/art/ArtLoaderImpl$SyncRequestRunnable;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/music/art/ArtLoaderImpl$SyncRequestRunnable;

.field final synthetic val$latch:Ljava/util/concurrent/CountDownLatch;

.field final synthetic val$pfdHolder:Ljava/util/concurrent/atomic/AtomicReference;


# direct methods
.method constructor <init>(Lcom/google/android/music/art/ArtLoaderImpl$SyncRequestRunnable;Ljava/util/concurrent/atomic/AtomicReference;Ljava/util/concurrent/CountDownLatch;)V
    .locals 0

    .prologue
    .line 252
    iput-object p1, p0, Lcom/google/android/music/art/ArtLoaderImpl$SyncRequestRunnable$1;->this$1:Lcom/google/android/music/art/ArtLoaderImpl$SyncRequestRunnable;

    iput-object p2, p0, Lcom/google/android/music/art/ArtLoaderImpl$SyncRequestRunnable$1;->val$pfdHolder:Ljava/util/concurrent/atomic/AtomicReference;

    iput-object p3, p0, Lcom/google/android/music/art/ArtLoaderImpl$SyncRequestRunnable$1;->val$latch:Ljava/util/concurrent/CountDownLatch;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onArtChanged(Ljava/lang/String;)V
    .locals 2
    .param p1, "remoteLocation"    # Ljava/lang/String;

    .prologue
    .line 255
    iget-object v0, p0, Lcom/google/android/music/art/ArtLoaderImpl$SyncRequestRunnable$1;->this$1:Lcom/google/android/music/art/ArtLoaderImpl$SyncRequestRunnable;

    iget-object v0, v0, Lcom/google/android/music/art/ArtLoaderImpl$SyncRequestRunnable;->this$0:Lcom/google/android/music/art/ArtLoaderImpl;

    # getter for: Lcom/google/android/music/art/ArtLoaderImpl;->mArtMonitor:Lcom/google/android/music/download/artwork/ArtMonitor;
    invoke-static {v0}, Lcom/google/android/music/art/ArtLoaderImpl;->access$400(Lcom/google/android/music/art/ArtLoaderImpl;)Lcom/google/android/music/download/artwork/ArtMonitor;

    move-result-object v0

    invoke-interface {v0, p1, p0}, Lcom/google/android/music/download/artwork/ArtMonitor;->unregisterArtChangeListener(Ljava/lang/String;Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;)V

    .line 256
    iget-object v0, p0, Lcom/google/android/music/art/ArtLoaderImpl$SyncRequestRunnable$1;->val$pfdHolder:Ljava/util/concurrent/atomic/AtomicReference;

    iget-object v1, p0, Lcom/google/android/music/art/ArtLoaderImpl$SyncRequestRunnable$1;->this$1:Lcom/google/android/music/art/ArtLoaderImpl$SyncRequestRunnable;

    invoke-virtual {v1}, Lcom/google/android/music/art/ArtLoaderImpl$SyncRequestRunnable;->getFileDescriptor()Landroid/os/ParcelFileDescriptor;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 257
    iget-object v0, p0, Lcom/google/android/music/art/ArtLoaderImpl$SyncRequestRunnable$1;->val$latch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 258
    return-void
.end method

.method public onError(Ljava/lang/String;)V
    .locals 1
    .param p1, "remoteLocation"    # Ljava/lang/String;

    .prologue
    .line 262
    iget-object v0, p0, Lcom/google/android/music/art/ArtLoaderImpl$SyncRequestRunnable$1;->this$1:Lcom/google/android/music/art/ArtLoaderImpl$SyncRequestRunnable;

    iget-object v0, v0, Lcom/google/android/music/art/ArtLoaderImpl$SyncRequestRunnable;->this$0:Lcom/google/android/music/art/ArtLoaderImpl;

    # getter for: Lcom/google/android/music/art/ArtLoaderImpl;->mArtMonitor:Lcom/google/android/music/download/artwork/ArtMonitor;
    invoke-static {v0}, Lcom/google/android/music/art/ArtLoaderImpl;->access$400(Lcom/google/android/music/art/ArtLoaderImpl;)Lcom/google/android/music/download/artwork/ArtMonitor;

    move-result-object v0

    invoke-interface {v0, p1, p0}, Lcom/google/android/music/download/artwork/ArtMonitor;->unregisterArtChangeListener(Ljava/lang/String;Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;)V

    .line 263
    iget-object v0, p0, Lcom/google/android/music/art/ArtLoaderImpl$SyncRequestRunnable$1;->val$latch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 264
    return-void
.end method
