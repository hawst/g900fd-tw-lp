.class public Lcom/google/android/music/ui/ArtistAlbumsGridFragment;
.super Lcom/google/android/music/ui/AlbumGridFragment;
.source "ArtistAlbumsGridFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/ui/ArtistAlbumsGridFragment$ArtistAlbumsAdapter;,
        Lcom/google/android/music/ui/ArtistAlbumsGridFragment$ClusterLoaderCallbacks;
    }
.end annotation


# instance fields
.field private mAllAlbumsCluster:Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

.field private mAllSongs:Lcom/google/android/music/ui/AllSongsView;

.field private mArtistHeader:Lcom/google/android/music/ui/ArtistHeaderView;

.field private mArtistId:J

.field private mArtistMetajamId:Ljava/lang/String;

.field private mArtistName:Ljava/lang/String;

.field private mCardHeap:Lcom/google/android/music/ui/cardlib/layout/PlayCardHeap;

.field private mClusterLoaderCallback:Lcom/google/android/music/ui/ArtistAlbumsGridFragment$ClusterLoaderCallbacks;

.field private mLocalAlbumList:Lcom/google/android/music/medialist/AlbumList;

.field private mMyLibraryHeader:Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;

.field private mRelatedArtistsCluster:Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

.field private mSpinner:Lcom/google/android/music/ui/SpinnerHeaderView;

.field private mTopSongsCluster:Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

.field private mVideoThumbnailCluster:Lcom/google/android/music/ui/cardlib/PlayCardClusterView;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 51
    invoke-direct {p0}, Lcom/google/android/music/ui/AlbumGridFragment;-><init>()V

    .line 75
    new-instance v0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$ClusterLoaderCallbacks;

    invoke-direct {v0, p0, v2}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$ClusterLoaderCallbacks;-><init>(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;Lcom/google/android/music/ui/ArtistAlbumsGridFragment$1;)V

    iput-object v0, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mClusterLoaderCallback:Lcom/google/android/music/ui/ArtistAlbumsGridFragment$ClusterLoaderCallbacks;

    .line 78
    iput-object v2, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mLocalAlbumList:Lcom/google/android/music/medialist/AlbumList;

    .line 79
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mArtistId:J

    .line 80
    iput-object v2, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mArtistMetajamId:Ljava/lang/String;

    .line 82
    new-instance v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardHeap;

    invoke-direct {v0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardHeap;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mCardHeap:Lcom/google/android/music/ui/cardlib/layout/PlayCardHeap;

    .line 502
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;)Lcom/google/android/music/ui/ArtistHeaderView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mArtistHeader:Lcom/google/android/music/ui/ArtistHeaderView;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;)Lcom/google/android/music/ui/cardlib/PlayCardClusterView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mRelatedArtistsCluster:Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;Lcom/google/android/music/ui/cardlib/PlayCardClusterView;)Lcom/google/android/music/ui/cardlib/PlayCardClusterView;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/ArtistAlbumsGridFragment;
    .param p1, "x1"    # Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    .prologue
    .line 51
    iput-object p1, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mRelatedArtistsCluster:Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;)Lcom/google/android/music/ui/cardlib/PlayCardClusterView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mVideoThumbnailCluster:Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    return-object v0
.end method

.method static synthetic access$1102(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;Lcom/google/android/music/ui/cardlib/PlayCardClusterView;)Lcom/google/android/music/ui/cardlib/PlayCardClusterView;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/ArtistAlbumsGridFragment;
    .param p1, "x1"    # Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    .prologue
    .line 51
    iput-object p1, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mVideoThumbnailCluster:Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    return-object p1
.end method

.method static synthetic access$1200(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;)Lcom/google/android/music/ui/SpinnerHeaderView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mSpinner:Lcom/google/android/music/ui/SpinnerHeaderView;

    return-object v0
.end method

.method static synthetic access$1202(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;Lcom/google/android/music/ui/SpinnerHeaderView;)Lcom/google/android/music/ui/SpinnerHeaderView;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/ArtistAlbumsGridFragment;
    .param p1, "x1"    # Lcom/google/android/music/ui/SpinnerHeaderView;

    .prologue
    .line 51
    iput-object p1, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mSpinner:Lcom/google/android/music/ui/SpinnerHeaderView;

    return-object p1
.end method

.method static synthetic access$1300(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;)Lcom/google/android/music/ui/ArtistAlbumsGridFragment$ClusterLoaderCallbacks;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mClusterLoaderCallback:Lcom/google/android/music/ui/ArtistAlbumsGridFragment$ClusterLoaderCallbacks;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;Lcom/google/android/music/ui/cardlib/PlayCardClusterView;ILandroid/database/Cursor;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/ArtistAlbumsGridFragment;
    .param p1, "x1"    # Lcom/google/android/music/ui/cardlib/PlayCardClusterView;
    .param p2, "x2"    # I
    .param p3, "x3"    # Landroid/database/Cursor;

    .prologue
    .line 51
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->populateCluster(Lcom/google/android/music/ui/cardlib/PlayCardClusterView;ILandroid/database/Cursor;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mArtistName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$202(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/ArtistAlbumsGridFragment;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 51
    iput-object p1, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mArtistName:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$300(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;)Lcom/google/android/music/medialist/AlbumList;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mLocalAlbumList:Lcom/google/android/music/medialist/AlbumList;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;Lcom/google/android/music/medialist/AlbumList;)Lcom/google/android/music/medialist/AlbumList;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/ArtistAlbumsGridFragment;
    .param p1, "x1"    # Lcom/google/android/music/medialist/AlbumList;

    .prologue
    .line 51
    iput-object p1, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mLocalAlbumList:Lcom/google/android/music/medialist/AlbumList;

    return-object p1
.end method

.method static synthetic access$400(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;)J
    .locals 2
    .param p0, "x0"    # Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    .prologue
    .line 51
    iget-wide v0, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mArtistId:J

    return-wide v0
.end method

.method static synthetic access$402(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;J)J
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/ArtistAlbumsGridFragment;
    .param p1, "x1"    # J

    .prologue
    .line 51
    iput-wide p1, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mArtistId:J

    return-wide p1
.end method

.method static synthetic access$500(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mArtistMetajamId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$502(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/ArtistAlbumsGridFragment;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 51
    iput-object p1, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mArtistMetajamId:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$600(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;)Lcom/google/android/music/ui/AllSongsView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mAllSongs:Lcom/google/android/music/ui/AllSongsView;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;)Lcom/google/android/music/ui/cardlib/PlayCardClusterView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mTopSongsCluster:Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    return-object v0
.end method

.method static synthetic access$702(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;Lcom/google/android/music/ui/cardlib/PlayCardClusterView;)Lcom/google/android/music/ui/cardlib/PlayCardClusterView;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/ArtistAlbumsGridFragment;
    .param p1, "x1"    # Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    .prologue
    .line 51
    iput-object p1, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mTopSongsCluster:Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    return-object p1
.end method

.method static synthetic access$800(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;Landroid/view/LayoutInflater;Landroid/widget/ListView;)Lcom/google/android/music/ui/cardlib/PlayCardClusterView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/ArtistAlbumsGridFragment;
    .param p1, "x1"    # Landroid/view/LayoutInflater;
    .param p2, "x2"    # Landroid/widget/ListView;

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->buildEmptyCluster(Landroid/view/LayoutInflater;Landroid/widget/ListView;)Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;)Lcom/google/android/music/ui/cardlib/PlayCardClusterView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mAllAlbumsCluster:Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    return-object v0
.end method

.method static synthetic access$902(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;Lcom/google/android/music/ui/cardlib/PlayCardClusterView;)Lcom/google/android/music/ui/cardlib/PlayCardClusterView;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/ArtistAlbumsGridFragment;
    .param p1, "x1"    # Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    .prologue
    .line 51
    iput-object p1, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mAllAlbumsCluster:Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    return-object p1
.end method

.method private buildEmptyCluster(Landroid/view/LayoutInflater;Landroid/widget/ListView;)Lcom/google/android/music/ui/cardlib/PlayCardClusterView;
    .locals 6
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "lv"    # Landroid/widget/ListView;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 310
    const v2, 0x7f040092

    const/4 v3, 0x0

    invoke-virtual {p1, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    .line 313
    .local v0, "cluster":Lcom/google/android/music/ui/cardlib/PlayCardClusterView;
    new-instance v1, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;

    invoke-direct {v1, v4, v4}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;-><init>(II)V

    .line 314
    .local v1, "clusterMetadata":Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;
    invoke-virtual {v0, v1, v5, v5}, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->setMetadata(Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;Ljava/util/List;Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;)V

    .line 315
    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->setVisibility(I)V

    .line 316
    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->hideHeader()V

    .line 317
    return-object v0
.end method

.method private getClusterInfo(ILandroid/database/Cursor;)Lcom/google/android/music/ui/Cluster;
    .locals 11
    .param p1, "groupType"    # I
    .param p2, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 361
    sget-object v2, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    .line 365
    .local v2, "cardType":Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;
    const/4 v9, 0x0

    .line 366
    .local v9, "clickListener":Landroid/view/View$OnClickListener;
    invoke-virtual {p0}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->getScreenColumns()I

    move-result v7

    .line 367
    .local v7, "nbColumns":I
    invoke-virtual {p0}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d000f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v8

    .line 368
    .local v8, "nbRows":I
    const/4 v10, 0x0

    .line 369
    .local v10, "descriptor":Lcom/google/android/music/store/ContainerDescriptor;
    packed-switch p1, :pswitch_data_0

    .line 403
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unexpected type value:"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 371
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00b0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 372
    .local v3, "title":Ljava/lang/String;
    sget-object v2, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->CARD_ROW:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    .line 373
    invoke-virtual {p0}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d000d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v7

    .line 374
    invoke-virtual {p0}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0010

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v8

    .line 375
    invoke-static {p2}, Lcom/google/android/music/ui/cardlib/model/ExploreDocumentClusterBuilder;->buildTrackDocumentList(Landroid/database/Cursor;)Ljava/util/ArrayList;

    move-result-object v5

    .line 376
    .local v5, "docList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/music/ui/cardlib/model/Document;>;"
    sget-object v6, Lcom/google/android/music/ui/cardlib/model/Document$Type;->TRACK:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    .line 377
    .local v6, "docType":Lcom/google/android/music/ui/cardlib/model/Document$Type;
    iget-object v0, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mArtistMetajamId:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mArtistName:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/music/store/ContainerDescriptor;->newTopSongsArtistDescriptor(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v10

    .line 381
    new-instance v9, Lcom/google/android/music/ui/PlayTrackDocumentsClickListener;

    .end local v9    # "clickListener":Landroid/view/View$OnClickListener;
    invoke-direct {v9, v5, v10}, Lcom/google/android/music/ui/PlayTrackDocumentsClickListener;-><init>(Ljava/util/List;Lcom/google/android/music/store/ContainerDescriptor;)V

    .line 405
    .restart local v9    # "clickListener":Landroid/view/View$OnClickListener;
    :goto_0
    new-instance v0, Lcom/google/android/music/ui/Cluster;

    invoke-virtual {p0}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v4, 0x0

    invoke-direct/range {v0 .. v10}, Lcom/google/android/music/ui/Cluster;-><init>(Landroid/app/Activity;Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Lcom/google/android/music/ui/cardlib/model/Document$Type;IILandroid/view/View$OnClickListener;Lcom/google/android/music/store/ContainerDescriptor;)V

    return-object v0

    .line 384
    .end local v3    # "title":Ljava/lang/String;
    .end local v5    # "docList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/music/ui/cardlib/model/Document;>;"
    .end local v6    # "docType":Lcom/google/android/music/ui/cardlib/model/Document$Type;
    :pswitch_1
    invoke-virtual {p0}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00af

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 385
    .restart local v3    # "title":Ljava/lang/String;
    sget-object v2, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->CARD_SMALL_WRAPPED:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    .line 386
    invoke-static {p2}, Lcom/google/android/music/ui/cardlib/model/ExploreDocumentClusterBuilder;->buildAlbumDocumentList(Landroid/database/Cursor;)Ljava/util/ArrayList;

    move-result-object v5

    .line 387
    .restart local v5    # "docList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/music/ui/cardlib/model/Document;>;"
    sget-object v6, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ALBUM:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    .line 388
    .restart local v6    # "docType":Lcom/google/android/music/ui/cardlib/model/Document$Type;
    goto :goto_0

    .line 390
    .end local v3    # "title":Ljava/lang/String;
    .end local v5    # "docList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/music/ui/cardlib/model/Document;>;"
    .end local v6    # "docType":Lcom/google/android/music/ui/cardlib/model/Document$Type;
    :pswitch_2
    invoke-virtual {p0}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00b1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 391
    .restart local v3    # "title":Ljava/lang/String;
    sget-object v2, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->CARD_SMALL_2x1_WRAPPED:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    .line 392
    invoke-static {p2}, Lcom/google/android/music/ui/cardlib/model/ExploreDocumentClusterBuilder;->buildArtistDocumentList(Landroid/database/Cursor;)Ljava/util/ArrayList;

    move-result-object v5

    .line 393
    .restart local v5    # "docList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/music/ui/cardlib/model/Document;>;"
    sget-object v6, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ARTIST:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    .line 394
    .restart local v6    # "docType":Lcom/google/android/music/ui/cardlib/model/Document$Type;
    goto :goto_0

    .line 396
    .end local v3    # "title":Ljava/lang/String;
    .end local v5    # "docList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/music/ui/cardlib/model/Document;>;"
    .end local v6    # "docType":Lcom/google/android/music/ui/cardlib/model/Document$Type;
    :pswitch_3
    const v0, 0x7f0b00b3

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 397
    .restart local v3    # "title":Ljava/lang/String;
    sget-object v2, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->CARD_VIDEO_THUMBNAIL:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    .line 398
    const/4 v0, 0x0

    invoke-static {p2, v0}, Lcom/google/android/music/ui/cardlib/model/VideoDocumentClusterBuilder;->buildVideoDocumentList(Landroid/database/Cursor;Z)Ljava/util/ArrayList;

    move-result-object v5

    .line 400
    .restart local v5    # "docList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/music/ui/cardlib/model/Document;>;"
    sget-object v6, Lcom/google/android/music/ui/cardlib/model/Document$Type;->VIDEO:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    .line 401
    .restart local v6    # "docType":Lcom/google/android/music/ui/cardlib/model/Document$Type;
    goto :goto_0

    .line 369
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static newInstance(Lcom/google/android/music/medialist/AlbumList;)Lcom/google/android/music/ui/ArtistAlbumsGridFragment;
    .locals 3
    .param p0, "albumlist"    # Lcom/google/android/music/medialist/AlbumList;

    .prologue
    .line 85
    new-instance v1, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    invoke-direct {v1}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;-><init>()V

    .line 88
    .local v1, "fragment":Lcom/google/android/music/ui/ArtistAlbumsGridFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 89
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "AlbumList"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 90
    invoke-virtual {v1, v0}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->setArguments(Landroid/os/Bundle;)V

    .line 91
    return-object v1
.end method

.method private populateCluster(Lcom/google/android/music/ui/cardlib/PlayCardClusterView;ILandroid/database/Cursor;)V
    .locals 19
    .param p1, "cluster"    # Lcom/google/android/music/ui/cardlib/PlayCardClusterView;
    .param p2, "type"    # I
    .param p3, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 321
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getCount()I

    move-result v14

    if-nez v14, :cond_0

    .line 322
    const/16 v14, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->setVisibility(I)V

    .line 358
    :goto_0
    return-void

    .line 326
    :cond_0
    move-object/from16 v0, p0

    move/from16 v1, p2

    move-object/from16 v2, p3

    invoke-direct {v0, v1, v2}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->getClusterInfo(ILandroid/database/Cursor;)Lcom/google/android/music/ui/Cluster;

    move-result-object v4

    .line 328
    .local v4, "clusterInfo":Lcom/google/android/music/ui/Cluster;
    invoke-virtual {v4}, Lcom/google/android/music/ui/Cluster;->getNbColumns()I

    move-result v10

    .line 329
    .local v10, "nbColumns":I
    invoke-virtual {v4}, Lcom/google/android/music/ui/Cluster;->getVisibleContent()Ljava/util/List;

    move-result-object v13

    .line 330
    .local v13, "visibleDocList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/ui/cardlib/model/Document;>;"
    invoke-virtual {v4}, Lcom/google/android/music/ui/Cluster;->getCardType()Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    move-result-object v3

    .line 332
    .local v3, "cardType":Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;
    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v14

    add-int/2addr v14, v10

    add-int/lit8 v14, v14, -0x1

    div-int v11, v14, v10

    .line 333
    .local v11, "nbRows":I
    new-instance v5, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;

    invoke-virtual {v3}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->getHSpan()I

    move-result v14

    mul-int/2addr v14, v10

    invoke-virtual {v3}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->getVSpan()I

    move-result v15

    mul-int/2addr v15, v11

    invoke-direct {v5, v14, v15}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;-><init>(II)V

    .line 336
    .local v5, "clusterMetadata":Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_1
    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v14

    if-ge v7, v14, :cond_1

    .line 337
    div-int v14, v7, v10

    invoke-virtual {v3}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->getVSpan()I

    move-result v15

    mul-int v12, v14, v15

    .line 338
    .local v12, "row":I
    rem-int v14, v7, v10

    invoke-virtual {v3}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->getHSpan()I

    move-result v15

    mul-int v6, v14, v15

    .line 339
    .local v6, "column":I
    invoke-virtual {v5, v3, v6, v12}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->addTile(Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;

    .line 336
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 342
    .end local v6    # "column":I
    .end local v12    # "row":I
    :cond_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mCardsContextMenuDelegate:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$DocumentContextMenuDelegate;

    invoke-virtual {v4}, Lcom/google/android/music/ui/Cluster;->getItemOnClickListener()Landroid/view/View$OnClickListener;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v13, v14, v15}, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->setMetadata(Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;Ljava/util/List;Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;Landroid/view/View$OnClickListener;)V

    .line 345
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->hasCards()Z

    move-result v14

    if-nez v14, :cond_2

    .line 346
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mCardHeap:Lcom/google/android/music/ui/cardlib/layout/PlayCardHeap;

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->inflateContent(Lcom/google/android/music/ui/cardlib/layout/PlayCardHeap;)V

    .line 349
    :cond_2
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->createContent()V

    .line 351
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getCount()I

    move-result v14

    invoke-virtual {v5}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->getTileCount()I

    move-result v15

    sub-int v9, v14, v15

    .line 352
    .local v9, "moreItems":I
    if-lez v9, :cond_3

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0x7f0b0260

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    aput-object v18, v16, v17

    invoke-virtual/range {v14 .. v16}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 354
    .local v8, "more":Ljava/lang/String;
    :goto_2
    invoke-virtual {v4}, Lcom/google/android/music/ui/Cluster;->getTitle()Ljava/lang/String;

    move-result-object v14

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v15, v8}, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->showHeader(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 355
    invoke-virtual {v4}, Lcom/google/android/music/ui/Cluster;->getMoreOnClickListener()Lcom/google/android/music/ui/cardlib/model/MoreClusterClickHandler;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->setMoreClickHandler(Landroid/view/View$OnClickListener;)V

    .line 357
    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->setVisibility(I)V

    goto/16 :goto_0

    .line 352
    .end local v8    # "more":Ljava/lang/String;
    :cond_3
    const/4 v8, 0x0

    goto :goto_2
.end method


# virtual methods
.method public getArtistId()J
    .locals 2

    .prologue
    .line 286
    iget-wide v0, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mArtistId:J

    return-wide v0
.end method

.method public getArtistMetajamId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 281
    iget-object v0, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mArtistMetajamId:Ljava/lang/String;

    return-object v0
.end method

.method protected initEmptyScreen()V
    .locals 0

    .prologue
    .line 413
    return-void
.end method

.method newAsyncCursorAdapter(Landroid/database/Cursor;)Lcom/google/android/music/ui/MediaListCursorAdapter;
    .locals 2
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    .line 499
    new-instance v0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$ArtistAlbumsAdapter;

    iget-object v1, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mCardsContextMenuDelegate:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$DocumentContextMenuDelegate;

    invoke-direct {v0, p0, p0, p1, v1}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$ArtistAlbumsAdapter;-><init>(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;Lcom/google/android/music/ui/AlbumGridFragment;Landroid/database/Cursor;Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;)V

    return-object v0
.end method

.method newLoaderCursorAdapter()Lcom/google/android/music/ui/MediaListCursorAdapter;
    .locals 2

    .prologue
    .line 494
    new-instance v0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$ArtistAlbumsAdapter;

    iget-object v1, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mCardsContextMenuDelegate:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$DocumentContextMenuDelegate;

    invoke-direct {v0, p0, p0, v1}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$ArtistAlbumsAdapter;-><init>(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;Lcom/google/android/music/ui/AlbumGridFragment;Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;)V

    return-object v0
.end method

.method public onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 2
    .param p2, "data"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 294
    .local p1, "loader":Landroid/support/v4/content/Loader;, "Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 295
    iget-object v0, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mMyLibraryHeader:Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;

    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;->show()V

    .line 301
    :goto_0
    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->isTabletMusicExperience()Z

    move-result v0

    if-nez v0, :cond_1

    .line 302
    iget-object v0, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mAllSongs:Lcom/google/android/music/ui/AllSongsView;

    invoke-virtual {v0}, Lcom/google/android/music/ui/AllSongsView;->show()V

    .line 306
    :goto_1
    invoke-super {p0, p1, p2}, Lcom/google/android/music/ui/AlbumGridFragment;->onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    .line 307
    return-void

    .line 297
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mMyLibraryHeader:Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;

    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;->hide()V

    goto :goto_0

    .line 304
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mAllSongs:Lcom/google/android/music/ui/AllSongsView;

    invoke-virtual {v0}, Lcom/google/android/music/ui/AllSongsView;->hide()V

    goto :goto_1
.end method

.method public bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Landroid/support/v4/content/Loader;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 51
    check-cast p2, Landroid/database/Cursor;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 255
    invoke-virtual {p0}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->isClearedFromOnStop()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 256
    iget-object v0, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mArtistHeader:Lcom/google/android/music/ui/ArtistHeaderView;

    invoke-virtual {v0}, Lcom/google/android/music/ui/ArtistHeaderView;->onStart()V

    .line 257
    iget-object v0, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mAllAlbumsCluster:Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    if-eqz v0, :cond_0

    .line 258
    iget-object v0, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mAllAlbumsCluster:Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->createContent()V

    .line 259
    iget-object v0, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mTopSongsCluster:Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->createContent()V

    .line 260
    iget-object v0, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mRelatedArtistsCluster:Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->createContent()V

    .line 261
    iget-object v0, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mVideoThumbnailCluster:Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->createContent()V

    .line 264
    :cond_0
    invoke-super {p0}, Lcom/google/android/music/ui/AlbumGridFragment;->onStart()V

    .line 265
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 269
    invoke-super {p0}, Lcom/google/android/music/ui/AlbumGridFragment;->onStop()V

    .line 270
    iget-object v0, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mArtistHeader:Lcom/google/android/music/ui/ArtistHeaderView;

    invoke-virtual {v0}, Lcom/google/android/music/ui/ArtistHeaderView;->onStop()V

    .line 271
    iget-object v0, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mAllAlbumsCluster:Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    if-eqz v0, :cond_0

    .line 272
    iget-object v0, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mAllAlbumsCluster:Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->clearThumbnails()V

    .line 273
    iget-object v0, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mTopSongsCluster:Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->clearThumbnails()V

    .line 274
    iget-object v0, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mRelatedArtistsCluster:Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->clearThumbnails()V

    .line 275
    iget-object v0, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mVideoThumbnailCluster:Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->clearThumbnails()V

    .line 277
    :cond_0
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 8
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 96
    invoke-virtual {p0}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    .line 97
    .local v1, "inflater":Landroid/view/LayoutInflater;
    invoke-virtual {p0}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "AlbumList"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/medialist/AlbumList;

    .line 98
    .local v0, "albumlist":Lcom/google/android/music/medialist/AlbumList;
    invoke-virtual {p0}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->getListView()Landroid/widget/ListView;

    move-result-object v2

    .line 100
    .local v2, "lv":Landroid/widget/ListView;
    invoke-virtual {v2, v6}, Landroid/widget/ListView;->setHeaderDividersEnabled(Z)V

    .line 101
    invoke-virtual {v2, v6}, Landroid/widget/ListView;->setFooterDividersEnabled(Z)V

    .line 104
    const v3, 0x7f04001b

    invoke-virtual {v1, v3, v2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/music/ui/ArtistHeaderView;

    iput-object v3, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mArtistHeader:Lcom/google/android/music/ui/ArtistHeaderView;

    .line 106
    iget-object v3, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mArtistHeader:Lcom/google/android/music/ui/ArtistHeaderView;

    invoke-virtual {v3, v0}, Lcom/google/android/music/ui/ArtistHeaderView;->setAlbumList(Lcom/google/android/music/medialist/AlbumList;)V

    .line 107
    iget-object v3, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mArtistHeader:Lcom/google/android/music/ui/ArtistHeaderView;

    invoke-virtual {v2, v3, v7, v6}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 110
    const v3, 0x7f040093

    invoke-virtual {v1, v3, v2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;

    iput-object v3, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mMyLibraryHeader:Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;

    .line 112
    iget-object v3, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mMyLibraryHeader:Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;

    invoke-virtual {p0}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b0097

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v7, v7}, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;->setContent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    iget-object v3, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mMyLibraryHeader:Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;

    invoke-virtual {v3}, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;->hide()V

    .line 115
    iget-object v3, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mMyLibraryHeader:Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;

    invoke-virtual {v2, v3, v7, v6}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 118
    const v3, 0x7f04001a

    invoke-virtual {v1, v3, v2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/music/ui/AllSongsView;

    iput-object v3, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mAllSongs:Lcom/google/android/music/ui/AllSongsView;

    .line 119
    iget-object v3, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mAllSongs:Lcom/google/android/music/ui/AllSongsView;

    invoke-virtual {v3}, Lcom/google/android/music/ui/AllSongsView;->hide()V

    .line 120
    iget-object v3, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mAllSongs:Lcom/google/android/music/ui/AllSongsView;

    invoke-virtual {v2, v3, v7, v6}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 123
    new-instance v3, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$1;

    invoke-direct {v3, p0}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$1;-><init>(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;)V

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 149
    new-instance v3, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$2;

    invoke-direct {v3, p0, v0, v1, v2}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$2;-><init>(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;Lcom/google/android/music/medialist/AlbumList;Landroid/view/LayoutInflater;Landroid/widget/ListView;)V

    invoke-static {v3}, Lcom/google/android/music/utils/MusicUtils;->runAsyncWithCallback(Lcom/google/android/music/utils/async/AsyncRunner;)V

    .line 251
    return-void
.end method
