.class Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;
.super Landroid/view/View$BaseSavedState;
.source "QueuePlayingFromHeaderView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/QueuePlayingFromHeaderView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "QueueHeaderViewState"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mBackground:Landroid/graphics/Bitmap;

.field private mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

.field private mIsRadio:Z

.field private mMixDescriptor:Lcom/google/android/music/mix/MixDescriptor;

.field private mPlayingFromBoldText:Ljava/lang/String;

.field private mPlayingFromDescription:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 939
    new-instance v0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState$1;

    invoke-direct {v0}, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState$1;-><init>()V

    sput-object v0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 911
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcel;)V

    .line 912
    invoke-static {}, Ljava/lang/ClassLoader;->getSystemClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;->mBackground:Landroid/graphics/Bitmap;

    .line 913
    const-class v0, Lcom/google/android/music/mix/MixDescriptor;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/mix/MixDescriptor;

    iput-object v0, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;->mMixDescriptor:Lcom/google/android/music/mix/MixDescriptor;

    .line 914
    const-class v0, Lcom/google/android/music/store/ContainerDescriptor;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/store/ContainerDescriptor;

    iput-object v0, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    .line 916
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;->mPlayingFromDescription:Ljava/lang/String;

    .line 917
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;->mPlayingFromBoldText:Ljava/lang/String;

    .line 918
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;->mIsRadio:Z

    .line 919
    return-void

    .line 918
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/os/Parcelable;)V
    .locals 0
    .param p1, "superState"    # Landroid/os/Parcelable;

    .prologue
    .line 922
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcelable;)V

    .line 923
    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;

    .prologue
    .line 902
    iget-object v0, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;->mBackground:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;
    .param p1, "x1"    # Landroid/graphics/Bitmap;

    .prologue
    .line 902
    iput-object p1, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;->mBackground:Landroid/graphics/Bitmap;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;)Lcom/google/android/music/mix/MixDescriptor;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;

    .prologue
    .line 902
    iget-object v0, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;->mMixDescriptor:Lcom/google/android/music/mix/MixDescriptor;

    return-object v0
.end method

.method static synthetic access$1102(Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;Lcom/google/android/music/mix/MixDescriptor;)Lcom/google/android/music/mix/MixDescriptor;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;
    .param p1, "x1"    # Lcom/google/android/music/mix/MixDescriptor;

    .prologue
    .line 902
    iput-object p1, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;->mMixDescriptor:Lcom/google/android/music/mix/MixDescriptor;

    return-object p1
.end method

.method static synthetic access$1200(Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;

    .prologue
    .line 902
    iget-object v0, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;->mPlayingFromDescription:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1202(Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 902
    iput-object p1, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;->mPlayingFromDescription:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1300(Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;)Lcom/google/android/music/store/ContainerDescriptor;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;

    .prologue
    .line 902
    iget-object v0, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    return-object v0
.end method

.method static synthetic access$1302(Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;Lcom/google/android/music/store/ContainerDescriptor;)Lcom/google/android/music/store/ContainerDescriptor;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;
    .param p1, "x1"    # Lcom/google/android/music/store/ContainerDescriptor;

    .prologue
    .line 902
    iput-object p1, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    return-object p1
.end method

.method static synthetic access$1400(Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;

    .prologue
    .line 902
    iget-object v0, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;->mPlayingFromBoldText:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1402(Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 902
    iput-object p1, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;->mPlayingFromBoldText:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1500(Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;

    .prologue
    .line 902
    iget-boolean v0, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;->mIsRadio:Z

    return v0
.end method

.method static synthetic access$1502(Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;
    .param p1, "x1"    # Z

    .prologue
    .line 902
    iput-boolean p1, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;->mIsRadio:Z

    return p1
.end method


# virtual methods
.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    const/4 v0, 0x0

    .line 927
    invoke-super {p0, p1, p2}, Landroid/view/View$BaseSavedState;->writeToParcel(Landroid/os/Parcel;I)V

    .line 928
    iget-object v1, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;->mBackground:Landroid/graphics/Bitmap;

    invoke-virtual {p1, v1, v0}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 929
    iget-object v1, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;->mBackground:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;->mBackground:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 932
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;->mMixDescriptor:Lcom/google/android/music/mix/MixDescriptor;

    invoke-virtual {p1, v1, v0}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 933
    iget-object v1, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    invoke-virtual {p1, v1, v0}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 934
    iget-object v1, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;->mPlayingFromDescription:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 935
    iget-object v1, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;->mPlayingFromBoldText:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 936
    iget-boolean v1, p0, Lcom/google/android/music/ui/QueuePlayingFromHeaderView$QueueHeaderViewState;->mIsRadio:Z

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 937
    return-void
.end method
