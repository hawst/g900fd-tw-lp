.class Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$1;
.super Ljava/lang/Object;
.source "CastRemotePlaybackClient.java"

# interfaces
.implements Lcom/google/android/gms/common/api/ResultCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->handleLeaveSession()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/gms/common/api/ResultCallback",
        "<",
        "Lcom/google/android/gms/common/api/Status;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;


# direct methods
.method constructor <init>(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)V
    .locals 0

    .prologue
    .line 344
    iput-object p1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$1;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic onResult(Lcom/google/android/gms/common/api/Result;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/gms/common/api/Result;

    .prologue
    .line 344
    check-cast p1, Lcom/google/android/gms/common/api/Status;

    .end local p1    # "x0":Lcom/google/android/gms/common/api/Result;
    invoke-virtual {p0, p1}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$1;->onResult(Lcom/google/android/gms/common/api/Status;)V

    return-void
.end method

.method public onResult(Lcom/google/android/gms/common/api/Status;)V
    .locals 2
    .param p1, "status"    # Lcom/google/android/gms/common/api/Status;

    .prologue
    .line 347
    iget-object v0, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$1;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    # getter for: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->LOGV:Z
    invoke-static {v0}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$400(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 348
    const-string v0, "MusicCastRemote"

    const-string v1, "session left"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 350
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$1;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mIsStreaming:Z
    invoke-static {v0, v1}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$502(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;Z)Z

    .line 352
    return-void
.end method
