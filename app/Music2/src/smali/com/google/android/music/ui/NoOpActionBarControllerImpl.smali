.class public Lcom/google/android/music/ui/NoOpActionBarControllerImpl;
.super Ljava/lang/Object;
.source "NoOpActionBarControllerImpl.java"

# interfaces
.implements Lcom/google/android/music/ui/ActionBarController;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public hideActionBar()V
    .locals 0

    .prologue
    .line 24
    return-void
.end method

.method public setActionBarAlpha(I)V
    .locals 0
    .param p1, "alpha"    # I

    .prologue
    .line 18
    return-void
.end method

.method public setActionBarTitle(Ljava/lang/String;)V
    .locals 0
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 12
    return-void
.end method

.method public showActionBar()V
    .locals 0

    .prologue
    .line 21
    return-void
.end method
