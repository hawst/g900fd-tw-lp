.class public Lcom/google/android/music/AsyncAlbumArtImageView;
.super Landroid/widget/ImageView;
.source "AsyncAlbumArtImageView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/AsyncAlbumArtImageView$3;,
        Lcom/google/android/music/AsyncAlbumArtImageView$AlbumArtHandler;,
        Lcom/google/android/music/AsyncAlbumArtImageView$ImFeelingLuckyArtworkMode;,
        Lcom/google/android/music/AsyncAlbumArtImageView$DefaultArtworkMode;,
        Lcom/google/android/music/AsyncAlbumArtImageView$GenreMode;,
        Lcom/google/android/music/AsyncAlbumArtImageView$ArtistMode;,
        Lcom/google/android/music/AsyncAlbumArtImageView$SongListMode;,
        Lcom/google/android/music/AsyncAlbumArtImageView$ServiceAlbumMode;,
        Lcom/google/android/music/AsyncAlbumArtImageView$MultiUrlCompositeMode;,
        Lcom/google/android/music/AsyncAlbumArtImageView$ExternalRadioMode;,
        Lcom/google/android/music/AsyncAlbumArtImageView$VideoThumbnailMode;,
        Lcom/google/android/music/AsyncAlbumArtImageView$ExternalAlbumMode;,
        Lcom/google/android/music/AsyncAlbumArtImageView$ShuffleAllMode;,
        Lcom/google/android/music/AsyncAlbumArtImageView$PlaylistMode;,
        Lcom/google/android/music/AsyncAlbumArtImageView$AllSongsMode;,
        Lcom/google/android/music/AsyncAlbumArtImageView$AlbumMode;,
        Lcom/google/android/music/AsyncAlbumArtImageView$Mode;,
        Lcom/google/android/music/AsyncAlbumArtImageView$LayerMode;
    }
.end annotation


# static fields
.field private static final LOGV:Z

.field private static final sHandler:Lcom/google/android/music/AsyncAlbumArtImageView$AlbumArtHandler;


# instance fields
.field private mActualArtworkSet:Z
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
    .end annotation
.end field

.field private mAllowAnimation:Z

.field private final mArtChangeListener:Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;

.field private mAttachedToWindow:Z

.field private mAvailable:Z
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
    .end annotation
.end field

.field private mBitmap:Landroid/graphics/Bitmap;

.field private mFadeInAnimation:Landroid/view/animation/Animation;

.field private mIsScrolling:Z

.field private mLayerMode:Lcom/google/android/music/AsyncAlbumArtImageView$LayerMode;

.field private mLayoutAsSquare:Z

.field private mLoadingArtworkSet:Z
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
    .end annotation
.end field

.field private mMissingArtHelper:Lcom/google/android/music/download/artwork/MissingArtHelper;

.field private mMode:Lcom/google/android/music/AsyncAlbumArtImageView$Mode;
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
    .end annotation
.end field

.field private final mNotAvailableImageOverlay:Landroid/graphics/drawable/ColorDrawable;

.field private mRequestedHeight:I

.field private mRequestedWidth:I

.field private mStretchToFill:Z

.field private mVirtualHeight:I

.field private mVirtualWidth:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 62
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->UI:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/AsyncAlbumArtImageView;->LOGV:Z

    .line 82
    new-instance v0, Lcom/google/android/music/AsyncAlbumArtImageView$AlbumArtHandler;

    invoke-direct {v0}, Lcom/google/android/music/AsyncAlbumArtImageView$AlbumArtHandler;-><init>()V

    sput-object v0, Lcom/google/android/music/AsyncAlbumArtImageView;->sHandler:Lcom/google/android/music/AsyncAlbumArtImageView$AlbumArtHandler;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 985
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {p0, v1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 937
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mMode:Lcom/google/android/music/AsyncAlbumArtImageView$Mode;

    .line 940
    sget-object v1, Lcom/google/android/music/AsyncAlbumArtImageView$LayerMode;->BACKGROUND:Lcom/google/android/music/AsyncAlbumArtImageView$LayerMode;

    iput-object v1, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mLayerMode:Lcom/google/android/music/AsyncAlbumArtImageView$LayerMode;

    .line 942
    iput-boolean v2, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mActualArtworkSet:Z

    .line 945
    iput-boolean v2, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mLoadingArtworkSet:Z

    .line 949
    iput v2, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mVirtualWidth:I

    .line 952
    iput v2, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mVirtualHeight:I

    .line 955
    iput v2, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mRequestedWidth:I

    .line 958
    iput v2, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mRequestedHeight:I

    .line 965
    iput-boolean v3, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mAllowAnimation:Z

    .line 968
    iput-boolean v3, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mAvailable:Z

    .line 1540
    new-instance v1, Lcom/google/android/music/AsyncAlbumArtImageView$2;

    invoke-direct {v1, p0}, Lcom/google/android/music/AsyncAlbumArtImageView$2;-><init>(Lcom/google/android/music/AsyncAlbumArtImageView;)V

    iput-object v1, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mArtChangeListener:Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;

    .line 986
    sget-object v1, Lcom/google/android/music/R$styleable;->AlbumArt:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 987
    .local v0, "a":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v2, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mLayoutAsSquare:Z

    .line 988
    invoke-virtual {v0, v3, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mStretchToFill:Z

    .line 989
    new-instance v1, Landroid/view/animation/AlphaAnimation;

    const/4 v2, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-direct {v1, v2, v3}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    iput-object v1, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mFadeInAnimation:Landroid/view/animation/Animation;

    .line 990
    iget-object v1, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mFadeInAnimation:Landroid/view/animation/Animation;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d0008

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 992
    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c006e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mNotAvailableImageOverlay:Landroid/graphics/drawable/ColorDrawable;

    .line 994
    return-void
.end method

.method static synthetic access$100(Landroid/content/Context;Ljava/lang/String;IILcom/google/android/music/download/artwork/RemoteUrlSink;Z)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "x0"    # Landroid/content/Context;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # I
    .param p3, "x3"    # I
    .param p4, "x4"    # Lcom/google/android/music/download/artwork/RemoteUrlSink;
    .param p5, "x5"    # Z

    .prologue
    .line 58
    invoke-static/range {p0 .. p5}, Lcom/google/android/music/AsyncAlbumArtImageView;->getBitmapForRemoteUrlOrDefault(Landroid/content/Context;Ljava/lang/String;IILcom/google/android/music/download/artwork/RemoteUrlSink;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/android/music/AsyncAlbumArtImageView;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/AsyncAlbumArtImageView;

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/google/android/music/AsyncAlbumArtImageView;->getAlbumWidth()I

    move-result v0

    return v0
.end method

.method static synthetic access$1100(Lcom/google/android/music/AsyncAlbumArtImageView;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/AsyncAlbumArtImageView;

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/google/android/music/AsyncAlbumArtImageView;->getAlbumHeight()I

    move-result v0

    return v0
.end method

.method static synthetic access$1200(Lcom/google/android/music/AsyncAlbumArtImageView;Lcom/google/android/music/download/artwork/ArtIdRecorder$RemoteArtUrlRecorder;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/AsyncAlbumArtImageView;
    .param p1, "x1"    # Lcom/google/android/music/download/artwork/ArtIdRecorder$RemoteArtUrlRecorder;

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/google/android/music/AsyncAlbumArtImageView;->requestMissingArt(Lcom/google/android/music/download/artwork/ArtIdRecorder$RemoteArtUrlRecorder;)V

    return-void
.end method

.method static synthetic access$1300(Lcom/google/android/music/AsyncAlbumArtImageView;Landroid/graphics/Bitmap;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/AsyncAlbumArtImageView;
    .param p1, "x1"    # Landroid/graphics/Bitmap;
    .param p2, "x2"    # Z

    .prologue
    .line 58
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/AsyncAlbumArtImageView;->setAlbumImage(Landroid/graphics/Bitmap;Z)V

    return-void
.end method

.method static synthetic access$200()Z
    .locals 1

    .prologue
    .line 58
    sget-boolean v0, Lcom/google/android/music/AsyncAlbumArtImageView;->LOGV:Z

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/music/AsyncAlbumArtImageView;)Lcom/google/android/music/AsyncAlbumArtImageView$LayerMode;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/AsyncAlbumArtImageView;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mLayerMode:Lcom/google/android/music/AsyncAlbumArtImageView$LayerMode;

    return-object v0
.end method

.method static synthetic access$402(Lcom/google/android/music/AsyncAlbumArtImageView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/AsyncAlbumArtImageView;
    .param p1, "x1"    # Z

    .prologue
    .line 58
    iput-boolean p1, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mActualArtworkSet:Z

    return p1
.end method

.method static synthetic access$502(Lcom/google/android/music/AsyncAlbumArtImageView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/AsyncAlbumArtImageView;
    .param p1, "x1"    # I

    .prologue
    .line 58
    iput p1, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mRequestedWidth:I

    return p1
.end method

.method static synthetic access$602(Lcom/google/android/music/AsyncAlbumArtImageView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/AsyncAlbumArtImageView;
    .param p1, "x1"    # I

    .prologue
    .line 58
    iput p1, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mRequestedHeight:I

    return p1
.end method

.method static synthetic access$700(Lcom/google/android/music/AsyncAlbumArtImageView;)Lcom/google/android/music/download/artwork/MissingArtHelper;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/AsyncAlbumArtImageView;

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/google/android/music/AsyncAlbumArtImageView;->getMissingArtHelper()Lcom/google/android/music/download/artwork/MissingArtHelper;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/music/AsyncAlbumArtImageView;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/AsyncAlbumArtImageView;

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/google/android/music/AsyncAlbumArtImageView;->makeDrawable()V

    return-void
.end method

.method static synthetic access$900(Lcom/google/android/music/AsyncAlbumArtImageView;)Lcom/google/android/music/AsyncAlbumArtImageView$Mode;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/AsyncAlbumArtImageView;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mMode:Lcom/google/android/music/AsyncAlbumArtImageView$Mode;

    return-object v0
.end method

.method private static createDrawable(Landroid/content/Context;Landroid/graphics/Bitmap;II)Landroid/graphics/drawable/Drawable;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "w"    # I
    .param p3, "h"    # I

    .prologue
    .line 1961
    const/4 v0, 0x0

    .line 1962
    .local v0, "drawable":Landroid/graphics/drawable/Drawable;
    if-eqz p1, :cond_0

    .line 1965
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    .end local v0    # "drawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 1967
    .restart local v0    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_0
    return-object v0
.end method

.method private getAlbumHeight()I
    .locals 1

    .prologue
    .line 1652
    iget v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mVirtualHeight:I

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/music/AsyncAlbumArtImageView;->getHeight()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mVirtualHeight:I

    goto :goto_0
.end method

.method private getAlbumWidth()I
    .locals 1

    .prologue
    .line 1648
    iget v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mVirtualWidth:I

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/music/AsyncAlbumArtImageView;->getWidth()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mVirtualWidth:I

    goto :goto_0
.end method

.method private static getBitmapForRemoteUrlOrDefault(Landroid/content/Context;Ljava/lang/String;IILcom/google/android/music/download/artwork/RemoteUrlSink;Z)Landroid/graphics/Bitmap;
    .locals 18
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "remoteUrl"    # Ljava/lang/String;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "missingUrlSink"    # Lcom/google/android/music/download/artwork/RemoteUrlSink;
    .param p5, "cropToSquare"    # Z

    .prologue
    .line 1627
    invoke-static/range {p1 .. p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1628
    const/16 v17, 0x0

    .line 1644
    :cond_0
    :goto_0
    return-object v17

    .line 1631
    :cond_1
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    move/from16 v4, p5

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/music/utils/AlbumArtUtils;->getRealNonAlbumArt(Landroid/content/Context;Ljava/lang/String;IIZ)Landroid/graphics/Bitmap;

    move-result-object v17

    .line 1634
    .local v17, "bitmap":Landroid/graphics/Bitmap;
    if-nez v17, :cond_0

    .line 1635
    move-object/from16 v0, p4

    move-object/from16 v1, p1

    invoke-interface {v0, v1}, Lcom/google/android/music/download/artwork/RemoteUrlSink;->report(Ljava/lang/String;)V

    .line 1636
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/music/ui/UIStateManager;->isNetworkConnected()Z

    move-result v6

    if-nez v6, :cond_0

    .line 1638
    invoke-static/range {p1 .. p1}, Lcom/google/android/music/utils/AlbumArtUtils;->makeDefaultArtId(Ljava/lang/String;)J

    move-result-wide v8

    .line 1639
    .local v8, "idForCache":J
    const/16 v16, 0x0

    .line 1640
    .local v16, "allowAlias":Z
    const/4 v7, 0x1

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    move-object/from16 v6, p0

    move/from16 v10, p2

    move/from16 v11, p3

    invoke-static/range {v6 .. v15}, Lcom/google/android/music/utils/AlbumArtUtils;->getDefaultArtwork(Landroid/content/Context;ZJIILjava/lang/String;Ljava/lang/String;Lcom/google/android/music/download/artwork/AlbumIdSink;Z)Landroid/graphics/Bitmap;

    move-result-object v17

    goto :goto_0
.end method

.method private getMissingArtHelper()Lcom/google/android/music/download/artwork/MissingArtHelper;
    .locals 5

    .prologue
    .line 1575
    iget-object v2, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mMissingArtHelper:Lcom/google/android/music/download/artwork/MissingArtHelper;

    if-nez v2, :cond_0

    .line 1576
    invoke-virtual {p0}, Lcom/google/android/music/AsyncAlbumArtImageView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/music/ui/UIStateManager;->getInstance(Landroid/content/Context;)Lcom/google/android/music/ui/UIStateManager;

    move-result-object v1

    .line 1577
    .local v1, "mgr":Lcom/google/android/music/ui/UIStateManager;
    invoke-virtual {v1}, Lcom/google/android/music/ui/UIStateManager;->getArtMonitor()Lcom/google/android/music/download/artwork/ArtMonitor;

    move-result-object v0

    .line 1580
    .local v0, "artConnection":Lcom/google/android/music/download/artwork/ArtMonitor;
    new-instance v2, Lcom/google/android/music/download/artwork/MissingArtHelper;

    invoke-virtual {p0}, Lcom/google/android/music/AsyncAlbumArtImageView;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mArtChangeListener:Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;

    invoke-direct {v2, v3, v4, v0}, Lcom/google/android/music/download/artwork/MissingArtHelper;-><init>(Landroid/content/Context;Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;Lcom/google/android/music/download/artwork/ArtMonitor;)V

    iput-object v2, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mMissingArtHelper:Lcom/google/android/music/download/artwork/MissingArtHelper;

    .line 1584
    .end local v0    # "artConnection":Lcom/google/android/music/download/artwork/ArtMonitor;
    .end local v1    # "mgr":Lcom/google/android/music/ui/UIStateManager;
    :cond_0
    iget-object v2, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mMissingArtHelper:Lcom/google/android/music/download/artwork/MissingArtHelper;

    return-object v2
.end method

.method private loadArtAsynch()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1796
    invoke-direct {p0}, Lcom/google/android/music/AsyncAlbumArtImageView;->setLoadingArtwork()V

    .line 1798
    iget-boolean v1, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mIsScrolling:Z

    if-eqz v1, :cond_0

    .line 1801
    iput v2, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mRequestedWidth:I

    .line 1802
    iput v2, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mRequestedHeight:I

    .line 1812
    :goto_0
    return-void

    .line 1806
    :cond_0
    invoke-direct {p0}, Lcom/google/android/music/AsyncAlbumArtImageView;->getMissingArtHelper()Lcom/google/android/music/download/artwork/MissingArtHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/download/artwork/MissingArtHelper;->clear()V

    .line 1808
    sget-object v1, Lcom/google/android/music/AsyncAlbumArtImageView;->sHandler:Lcom/google/android/music/AsyncAlbumArtImageView$AlbumArtHandler;

    invoke-virtual {v1, p0}, Lcom/google/android/music/AsyncAlbumArtImageView$AlbumArtHandler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 1809
    sget-object v1, Lcom/google/android/music/AsyncAlbumArtImageView;->sHandler:Lcom/google/android/music/AsyncAlbumArtImageView$AlbumArtHandler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/android/music/AsyncAlbumArtImageView$AlbumArtHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 1810
    .local v0, "msg":Landroid/os/Message;
    iput-object p0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1811
    sget-object v1, Lcom/google/android/music/AsyncAlbumArtImageView;->sHandler:Lcom/google/android/music/AsyncAlbumArtImageView$AlbumArtHandler;

    invoke-virtual {v1, v0}, Lcom/google/android/music/AsyncAlbumArtImageView$AlbumArtHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private declared-synchronized makeDrawable()V
    .locals 29

    .prologue
    .line 1656
    monitor-enter p0

    :try_start_0
    invoke-direct/range {p0 .. p0}, Lcom/google/android/music/AsyncAlbumArtImageView;->getAlbumWidth()I

    move-result v6

    .line 1657
    .local v6, "width":I
    invoke-direct/range {p0 .. p0}, Lcom/google/android/music/AsyncAlbumArtImageView;->getAlbumHeight()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v7

    .line 1658
    .local v7, "height":I
    if-lez v6, :cond_0

    if-gtz v7, :cond_1

    .line 1793
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 1664
    :cond_1
    :try_start_1
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/music/AsyncAlbumArtImageView;->mActualArtworkSet:Z

    if-nez v3, :cond_0

    .line 1665
    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/music/AsyncAlbumArtImageView;->mRequestedWidth:I

    if-ne v6, v3, :cond_2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/music/AsyncAlbumArtImageView;->mRequestedHeight:I

    if-eq v7, v3, :cond_0

    .line 1669
    :cond_2
    move-object/from16 v0, p0

    iput v6, v0, Lcom/google/android/music/AsyncAlbumArtImageView;->mRequestedWidth:I

    .line 1670
    move-object/from16 v0, p0

    iput v7, v0, Lcom/google/android/music/AsyncAlbumArtImageView;->mRequestedHeight:I

    .line 1671
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/AsyncAlbumArtImageView;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 1673
    .local v2, "context":Landroid/content/Context;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/music/AsyncAlbumArtImageView;->mMode:Lcom/google/android/music/AsyncAlbumArtImageView$Mode;

    if-nez v3, :cond_3

    .line 1676
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/google/android/music/AsyncAlbumArtImageView;->mActualArtworkSet:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1656
    .end local v2    # "context":Landroid/content/Context;
    .end local v6    # "width":I
    .end local v7    # "height":I
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 1677
    .restart local v2    # "context":Landroid/content/Context;
    .restart local v6    # "width":I
    .restart local v7    # "height":I
    :cond_3
    :try_start_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/music/AsyncAlbumArtImageView;->mMode:Lcom/google/android/music/AsyncAlbumArtImageView$Mode;

    instance-of v3, v3, Lcom/google/android/music/AsyncAlbumArtImageView$AllSongsMode;

    if-eqz v3, :cond_4

    .line 1678
    invoke-direct/range {p0 .. p0}, Lcom/google/android/music/AsyncAlbumArtImageView;->loadArtAsynch()V

    goto :goto_0

    .line 1679
    :cond_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/music/AsyncAlbumArtImageView;->mMode:Lcom/google/android/music/AsyncAlbumArtImageView$Mode;

    instance-of v3, v3, Lcom/google/android/music/AsyncAlbumArtImageView$ShuffleAllMode;

    if-eqz v3, :cond_5

    .line 1680
    invoke-direct/range {p0 .. p0}, Lcom/google/android/music/AsyncAlbumArtImageView;->loadArtAsynch()V

    goto :goto_0

    .line 1681
    :cond_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/music/AsyncAlbumArtImageView;->mMode:Lcom/google/android/music/AsyncAlbumArtImageView$Mode;

    instance-of v3, v3, Lcom/google/android/music/AsyncAlbumArtImageView$PlaylistMode;

    if-eqz v3, :cond_7

    .line 1682
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/AsyncAlbumArtImageView;->mMode:Lcom/google/android/music/AsyncAlbumArtImageView$Mode;

    move-object/from16 v27, v0

    check-cast v27, Lcom/google/android/music/AsyncAlbumArtImageView$PlaylistMode;

    .line 1686
    .local v27, "mode":Lcom/google/android/music/AsyncAlbumArtImageView$PlaylistMode;
    new-instance v8, Lcom/google/android/music/download/artwork/ArtIdRecorder$AlbumIdRecorder;

    invoke-direct {v8}, Lcom/google/android/music/download/artwork/ArtIdRecorder$AlbumIdRecorder;-><init>()V

    .line 1687
    .local v8, "missingAlbums":Lcom/google/android/music/download/artwork/ArtIdRecorder$AlbumIdRecorder;
    move-object/from16 v0, v27

    iget v3, v0, Lcom/google/android/music/AsyncAlbumArtImageView$PlaylistMode;->style:I

    move-object/from16 v0, v27

    iget-wide v4, v0, Lcom/google/android/music/AsyncAlbumArtImageView$PlaylistMode;->id:J

    const/4 v9, 0x0

    invoke-static/range {v2 .. v9}, Lcom/google/android/music/utils/AlbumArtUtils;->getCachedFauxAlbumArt(Landroid/content/Context;IJIILcom/google/android/music/download/artwork/AlbumIdSink;Z)Landroid/graphics/Bitmap;

    move-result-object v21

    .line 1689
    .local v21, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v21, :cond_6

    .line 1690
    const/4 v3, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1, v3}, Lcom/google/android/music/AsyncAlbumArtImageView;->setAlbumImage(Landroid/graphics/Bitmap;Z)V

    goto :goto_0

    .line 1692
    :cond_6
    invoke-direct/range {p0 .. p0}, Lcom/google/android/music/AsyncAlbumArtImageView;->loadArtAsynch()V

    goto :goto_0

    .line 1694
    .end local v8    # "missingAlbums":Lcom/google/android/music/download/artwork/ArtIdRecorder$AlbumIdRecorder;
    .end local v21    # "bitmap":Landroid/graphics/Bitmap;
    .end local v27    # "mode":Lcom/google/android/music/AsyncAlbumArtImageView$PlaylistMode;
    :cond_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/music/AsyncAlbumArtImageView;->mMode:Lcom/google/android/music/AsyncAlbumArtImageView$Mode;

    instance-of v3, v3, Lcom/google/android/music/AsyncAlbumArtImageView$AlbumMode;

    if-eqz v3, :cond_9

    .line 1695
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/AsyncAlbumArtImageView;->mMode:Lcom/google/android/music/AsyncAlbumArtImageView$Mode;

    move-object/from16 v27, v0

    check-cast v27, Lcom/google/android/music/AsyncAlbumArtImageView$AlbumMode;

    .line 1698
    .local v27, "mode":Lcom/google/android/music/AsyncAlbumArtImageView$AlbumMode;
    new-instance v8, Lcom/google/android/music/download/artwork/ArtIdRecorder$AlbumIdRecorder;

    invoke-direct {v8}, Lcom/google/android/music/download/artwork/ArtIdRecorder$AlbumIdRecorder;-><init>()V

    .line 1699
    .restart local v8    # "missingAlbums":Lcom/google/android/music/download/artwork/ArtIdRecorder$AlbumIdRecorder;
    move-object/from16 v0, v27

    iget-wide v10, v0, Lcom/google/android/music/AsyncAlbumArtImageView$AlbumMode;->albumId:J

    const/4 v12, 0x0

    move-object/from16 v0, v27

    iget-object v15, v0, Lcom/google/android/music/AsyncAlbumArtImageView$AlbumMode;->album:Ljava/lang/String;

    move-object/from16 v0, v27

    iget-object v0, v0, Lcom/google/android/music/AsyncAlbumArtImageView$AlbumMode;->artist:Ljava/lang/String;

    move-object/from16 v16, v0

    const/16 v18, 0x0

    const/16 v19, 0x1

    const/16 v20, 0x0

    move-object v9, v2

    move v13, v6

    move v14, v7

    move-object/from16 v17, v8

    invoke-static/range {v9 .. v20}, Lcom/google/android/music/utils/AlbumArtUtils;->getCachedBitmap(Landroid/content/Context;JLjava/lang/String;IILjava/lang/String;Ljava/lang/String;Lcom/google/android/music/download/artwork/AlbumIdSink;ZZZ)Landroid/graphics/Bitmap;

    move-result-object v21

    .line 1702
    .restart local v21    # "bitmap":Landroid/graphics/Bitmap;
    if-eqz v21, :cond_8

    .line 1703
    const/4 v3, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1, v3}, Lcom/google/android/music/AsyncAlbumArtImageView;->setAlbumImage(Landroid/graphics/Bitmap;Z)V

    goto/16 :goto_0

    .line 1705
    :cond_8
    invoke-direct/range {p0 .. p0}, Lcom/google/android/music/AsyncAlbumArtImageView;->loadArtAsynch()V

    goto/16 :goto_0

    .line 1707
    .end local v8    # "missingAlbums":Lcom/google/android/music/download/artwork/ArtIdRecorder$AlbumIdRecorder;
    .end local v21    # "bitmap":Landroid/graphics/Bitmap;
    .end local v27    # "mode":Lcom/google/android/music/AsyncAlbumArtImageView$AlbumMode;
    :cond_9
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/music/AsyncAlbumArtImageView;->mMode:Lcom/google/android/music/AsyncAlbumArtImageView$Mode;

    instance-of v3, v3, Lcom/google/android/music/AsyncAlbumArtImageView$ArtistMode;

    if-eqz v3, :cond_c

    .line 1708
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/AsyncAlbumArtImageView;->mMode:Lcom/google/android/music/AsyncAlbumArtImageView$Mode;

    move-object/from16 v27, v0

    check-cast v27, Lcom/google/android/music/AsyncAlbumArtImageView$ArtistMode;

    .line 1711
    .local v27, "mode":Lcom/google/android/music/AsyncAlbumArtImageView$ArtistMode;
    new-instance v8, Lcom/google/android/music/download/artwork/ArtIdRecorder$AlbumIdRecorder;

    invoke-direct {v8}, Lcom/google/android/music/download/artwork/ArtIdRecorder$AlbumIdRecorder;-><init>()V

    .line 1713
    .restart local v8    # "missingAlbums":Lcom/google/android/music/download/artwork/ArtIdRecorder$AlbumIdRecorder;
    move-object/from16 v0, v27

    iget-boolean v3, v0, Lcom/google/android/music/AsyncAlbumArtImageView$ArtistMode;->useFauxArt:Z

    if-eqz v3, :cond_a

    .line 1714
    const/4 v3, 0x7

    move-object/from16 v0, v27

    iget-wide v4, v0, Lcom/google/android/music/AsyncAlbumArtImageView$ArtistMode;->id:J

    const/4 v9, 0x0

    invoke-static/range {v2 .. v9}, Lcom/google/android/music/utils/AlbumArtUtils;->getCachedFauxAlbumArt(Landroid/content/Context;IJIILcom/google/android/music/download/artwork/AlbumIdSink;Z)Landroid/graphics/Bitmap;

    move-result-object v21

    .line 1725
    .restart local v21    # "bitmap":Landroid/graphics/Bitmap;
    :goto_1
    if-eqz v21, :cond_b

    .line 1726
    const/4 v3, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1, v3}, Lcom/google/android/music/AsyncAlbumArtImageView;->setAlbumImage(Landroid/graphics/Bitmap;Z)V

    goto/16 :goto_0

    .line 1720
    .end local v21    # "bitmap":Landroid/graphics/Bitmap;
    :cond_a
    move-object/from16 v0, v27

    iget-object v3, v0, Lcom/google/android/music/AsyncAlbumArtImageView$ArtistMode;->uri:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v28

    .line 1721
    .local v28, "remoteUrl":Ljava/lang/String;
    invoke-virtual/range {v27 .. v27}, Lcom/google/android/music/AsyncAlbumArtImageView$ArtistMode;->cropToSquare()Z

    move-result v3

    move-object/from16 v0, v28

    invoke-static {v2, v0, v6, v7, v3}, Lcom/google/android/music/utils/AlbumArtUtils;->getRealNonAlbumArtCopy(Landroid/content/Context;Ljava/lang/String;IIZ)Landroid/graphics/Bitmap;

    move-result-object v21

    .restart local v21    # "bitmap":Landroid/graphics/Bitmap;
    goto :goto_1

    .line 1728
    .end local v28    # "remoteUrl":Ljava/lang/String;
    :cond_b
    invoke-direct/range {p0 .. p0}, Lcom/google/android/music/AsyncAlbumArtImageView;->loadArtAsynch()V

    goto/16 :goto_0

    .line 1730
    .end local v8    # "missingAlbums":Lcom/google/android/music/download/artwork/ArtIdRecorder$AlbumIdRecorder;
    .end local v21    # "bitmap":Landroid/graphics/Bitmap;
    .end local v27    # "mode":Lcom/google/android/music/AsyncAlbumArtImageView$ArtistMode;
    :cond_c
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/music/AsyncAlbumArtImageView;->mMode:Lcom/google/android/music/AsyncAlbumArtImageView$Mode;

    instance-of v3, v3, Lcom/google/android/music/AsyncAlbumArtImageView$GenreMode;

    if-eqz v3, :cond_e

    .line 1731
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/AsyncAlbumArtImageView;->mMode:Lcom/google/android/music/AsyncAlbumArtImageView$Mode;

    move-object/from16 v27, v0

    check-cast v27, Lcom/google/android/music/AsyncAlbumArtImageView$GenreMode;

    .line 1735
    .local v27, "mode":Lcom/google/android/music/AsyncAlbumArtImageView$GenreMode;
    new-instance v8, Lcom/google/android/music/download/artwork/ArtIdRecorder$AlbumIdRecorder;

    invoke-direct {v8}, Lcom/google/android/music/download/artwork/ArtIdRecorder$AlbumIdRecorder;-><init>()V

    .line 1736
    .restart local v8    # "missingAlbums":Lcom/google/android/music/download/artwork/ArtIdRecorder$AlbumIdRecorder;
    const/16 v3, 0x8

    move-object/from16 v0, v27

    iget-wide v4, v0, Lcom/google/android/music/AsyncAlbumArtImageView$GenreMode;->id:J

    const/4 v9, 0x0

    invoke-static/range {v2 .. v9}, Lcom/google/android/music/utils/AlbumArtUtils;->getCachedFauxAlbumArt(Landroid/content/Context;IJIILcom/google/android/music/download/artwork/AlbumIdSink;Z)Landroid/graphics/Bitmap;

    move-result-object v21

    .line 1739
    .restart local v21    # "bitmap":Landroid/graphics/Bitmap;
    if-eqz v21, :cond_d

    .line 1740
    const/4 v3, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1, v3}, Lcom/google/android/music/AsyncAlbumArtImageView;->setAlbumImage(Landroid/graphics/Bitmap;Z)V

    goto/16 :goto_0

    .line 1742
    :cond_d
    invoke-direct/range {p0 .. p0}, Lcom/google/android/music/AsyncAlbumArtImageView;->loadArtAsynch()V

    goto/16 :goto_0

    .line 1744
    .end local v8    # "missingAlbums":Lcom/google/android/music/download/artwork/ArtIdRecorder$AlbumIdRecorder;
    .end local v21    # "bitmap":Landroid/graphics/Bitmap;
    .end local v27    # "mode":Lcom/google/android/music/AsyncAlbumArtImageView$GenreMode;
    :cond_e
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/music/AsyncAlbumArtImageView;->mMode:Lcom/google/android/music/AsyncAlbumArtImageView$Mode;

    instance-of v3, v3, Lcom/google/android/music/AsyncAlbumArtImageView$ExternalAlbumMode;

    if-nez v3, :cond_f

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/music/AsyncAlbumArtImageView;->mMode:Lcom/google/android/music/AsyncAlbumArtImageView$Mode;

    instance-of v3, v3, Lcom/google/android/music/AsyncAlbumArtImageView$SongListMode;

    if-nez v3, :cond_f

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/music/AsyncAlbumArtImageView;->mMode:Lcom/google/android/music/AsyncAlbumArtImageView$Mode;

    instance-of v3, v3, Lcom/google/android/music/AsyncAlbumArtImageView$VideoThumbnailMode;

    if-eqz v3, :cond_13

    .line 1748
    :cond_f
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/music/AsyncAlbumArtImageView;->mMode:Lcom/google/android/music/AsyncAlbumArtImageView$Mode;

    invoke-virtual {v3, v2}, Lcom/google/android/music/AsyncAlbumArtImageView$Mode;->getExternalUrl(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v28

    .line 1749
    .restart local v28    # "remoteUrl":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/music/AsyncAlbumArtImageView;->mMode:Lcom/google/android/music/AsyncAlbumArtImageView$Mode;

    invoke-virtual {v3}, Lcom/google/android/music/AsyncAlbumArtImageView$Mode;->cropToSquare()Z

    move-result v3

    move-object/from16 v0, v28

    invoke-static {v2, v0, v6, v7, v3}, Lcom/google/android/music/utils/AlbumArtUtils;->getRealNonAlbumArtCopy(Landroid/content/Context;Ljava/lang/String;IIZ)Landroid/graphics/Bitmap;

    move-result-object v21

    .line 1752
    .restart local v21    # "bitmap":Landroid/graphics/Bitmap;
    if-eqz v21, :cond_12

    .line 1753
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/music/AsyncAlbumArtImageView;->mMode:Lcom/google/android/music/AsyncAlbumArtImageView$Mode;

    instance-of v3, v3, Lcom/google/android/music/AsyncAlbumArtImageView$VideoThumbnailMode;

    if-eqz v3, :cond_11

    .line 1754
    invoke-virtual/range {v21 .. v21}, Landroid/graphics/Bitmap;->isMutable()Z

    move-result v3

    if-nez v3, :cond_10

    .line 1755
    invoke-static {}, Lcom/google/android/music/utils/AlbumArtUtils;->getPreferredConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v3

    const/4 v4, 0x1

    move-object/from16 v0, v21

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v21

    .line 1757
    :cond_10
    move-object/from16 v0, v21

    invoke-static {v2, v0, v6, v7}, Lcom/google/android/music/utils/AlbumArtUtils;->drawPlayYTVideoOverlay(Landroid/content/Context;Landroid/graphics/Bitmap;II)V

    .line 1759
    :cond_11
    const/4 v3, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1, v3}, Lcom/google/android/music/AsyncAlbumArtImageView;->setAlbumImage(Landroid/graphics/Bitmap;Z)V

    goto/16 :goto_0

    .line 1761
    :cond_12
    invoke-direct/range {p0 .. p0}, Lcom/google/android/music/AsyncAlbumArtImageView;->loadArtAsynch()V

    goto/16 :goto_0

    .line 1763
    .end local v21    # "bitmap":Landroid/graphics/Bitmap;
    .end local v28    # "remoteUrl":Ljava/lang/String;
    :cond_13
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/music/AsyncAlbumArtImageView;->mMode:Lcom/google/android/music/AsyncAlbumArtImageView$Mode;

    instance-of v3, v3, Lcom/google/android/music/AsyncAlbumArtImageView$MultiUrlCompositeMode;

    if-eqz v3, :cond_16

    .line 1764
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/AsyncAlbumArtImageView;->mMode:Lcom/google/android/music/AsyncAlbumArtImageView$Mode;

    move-object/from16 v25, v0

    check-cast v25, Lcom/google/android/music/AsyncAlbumArtImageView$MultiUrlCompositeMode;

    .line 1765
    .local v25, "compMode":Lcom/google/android/music/AsyncAlbumArtImageView$MultiUrlCompositeMode;
    move-object/from16 v0, v25

    instance-of v0, v0, Lcom/google/android/music/AsyncAlbumArtImageView$ExternalRadioMode;

    move/from16 v26, v0

    .line 1766
    .local v26, "isRadio":Z
    move-object/from16 v0, v25

    iget-object v3, v0, Lcom/google/android/music/AsyncAlbumArtImageView$MultiUrlCompositeMode;->mUrls:Ljava/lang/String;

    move/from16 v0, v26

    invoke-static {v2, v3, v6, v7, v0}, Lcom/google/android/music/utils/AlbumArtUtils;->getCachedMultiImageComposite(Landroid/content/Context;Ljava/lang/String;IIZ)Landroid/graphics/Bitmap;

    move-result-object v24

    .line 1768
    .local v24, "cached":Landroid/graphics/Bitmap;
    invoke-virtual/range {v25 .. v25}, Lcom/google/android/music/AsyncAlbumArtImageView$MultiUrlCompositeMode;->isCacheable()Z

    move-result v23

    .line 1769
    .local v23, "cacheable":Z
    if-eqz v23, :cond_14

    if-nez v24, :cond_15

    .line 1770
    :cond_14
    invoke-direct/range {p0 .. p0}, Lcom/google/android/music/AsyncAlbumArtImageView;->loadArtAsynch()V

    goto/16 :goto_0

    .line 1772
    :cond_15
    const/4 v3, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-direct {v0, v1, v3}, Lcom/google/android/music/AsyncAlbumArtImageView;->setAlbumImage(Landroid/graphics/Bitmap;Z)V

    goto/16 :goto_0

    .line 1774
    .end local v23    # "cacheable":Z
    .end local v24    # "cached":Landroid/graphics/Bitmap;
    .end local v25    # "compMode":Lcom/google/android/music/AsyncAlbumArtImageView$MultiUrlCompositeMode;
    .end local v26    # "isRadio":Z
    :cond_16
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/music/AsyncAlbumArtImageView;->mMode:Lcom/google/android/music/AsyncAlbumArtImageView$Mode;

    instance-of v3, v3, Lcom/google/android/music/AsyncAlbumArtImageView$DefaultArtworkMode;

    if-eqz v3, :cond_17

    .line 1775
    sget-object v3, Lcom/google/android/music/AsyncAlbumArtImageView$3;->$SwitchMap$com$google$android$music$AsyncAlbumArtImageView$LayerMode:[I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/AsyncAlbumArtImageView;->mLayerMode:Lcom/google/android/music/AsyncAlbumArtImageView$LayerMode;

    invoke-virtual {v4}, Lcom/google/android/music/AsyncAlbumArtImageView$LayerMode;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 1783
    :goto_2
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/google/android/music/AsyncAlbumArtImageView;->mActualArtworkSet:Z

    goto/16 :goto_0

    .line 1777
    :pswitch_0
    const v3, 0x7f020037

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/music/AsyncAlbumArtImageView;->setBackgroundResource(I)V

    goto :goto_2

    .line 1780
    :pswitch_1
    const v3, 0x7f020037

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/music/AsyncAlbumArtImageView;->setImageResource(I)V

    goto :goto_2

    .line 1784
    :cond_17
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/music/AsyncAlbumArtImageView;->mMode:Lcom/google/android/music/AsyncAlbumArtImageView$Mode;

    instance-of v3, v3, Lcom/google/android/music/AsyncAlbumArtImageView$ServiceAlbumMode;

    if-eqz v3, :cond_18

    .line 1785
    invoke-direct/range {p0 .. p0}, Lcom/google/android/music/AsyncAlbumArtImageView;->loadArtAsynch()V

    goto/16 :goto_0

    .line 1786
    :cond_18
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/music/AsyncAlbumArtImageView;->mMode:Lcom/google/android/music/AsyncAlbumArtImageView$Mode;

    instance-of v3, v3, Lcom/google/android/music/AsyncAlbumArtImageView$ImFeelingLuckyArtworkMode;

    if-eqz v3, :cond_19

    .line 1787
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/music/AsyncAlbumArtImageView;->mMode:Lcom/google/android/music/AsyncAlbumArtImageView$Mode;

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object v10, v2

    move v11, v6

    move v12, v7

    invoke-virtual/range {v9 .. v14}, Lcom/google/android/music/AsyncAlbumArtImageView$Mode;->createBitmap(Landroid/content/Context;IILcom/google/android/music/download/artwork/AlbumIdSink;Lcom/google/android/music/download/artwork/RemoteUrlSink;)Landroid/graphics/Bitmap;

    move-result-object v22

    .line 1788
    .local v22, "bmp":Landroid/graphics/Bitmap;
    const/4 v3, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-direct {v0, v1, v3}, Lcom/google/android/music/AsyncAlbumArtImageView;->setAlbumImage(Landroid/graphics/Bitmap;Z)V

    goto/16 :goto_0

    .line 1790
    .end local v22    # "bmp":Landroid/graphics/Bitmap;
    :cond_19
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unkown mode: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/music/AsyncAlbumArtImageView;->mMode:Lcom/google/android/music/AsyncAlbumArtImageView$Mode;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1775
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private declared-synchronized registerArtChangeListener()V
    .locals 1

    .prologue
    .line 1126
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mAttachedToWindow:Z

    if-eqz v0, :cond_0

    .line 1127
    invoke-direct {p0}, Lcom/google/android/music/AsyncAlbumArtImageView;->getMissingArtHelper()Lcom/google/android/music/download/artwork/MissingArtHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/download/artwork/MissingArtHelper;->register()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1129
    :cond_0
    monitor-exit p0

    return-void

    .line 1126
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private requestMissingArt(Lcom/google/android/music/download/artwork/ArtIdRecorder$RemoteArtUrlRecorder;)V
    .locals 3
    .param p1, "missingUrls"    # Lcom/google/android/music/download/artwork/ArtIdRecorder$RemoteArtUrlRecorder;

    .prologue
    .line 1861
    invoke-static {}, Lcom/google/android/music/utils/MusicUtils;->assertUiThread()V

    .line 1863
    if-eqz p1, :cond_0

    .line 1864
    invoke-direct {p0}, Lcom/google/android/music/AsyncAlbumArtImageView;->getMissingArtHelper()Lcom/google/android/music/download/artwork/MissingArtHelper;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/music/download/artwork/ArtIdRecorder$RemoteArtUrlRecorder;->extractIds()Ljava/util/Set;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mAttachedToWindow:Z

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/download/artwork/MissingArtHelper;->set(Ljava/util/Set;Z)V

    .line 1867
    :cond_0
    return-void
.end method

.method private setAlbumImage(Landroid/graphics/Bitmap;Z)V
    .locals 8
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "fromCache"    # Z

    .prologue
    const/4 v5, 0x0

    const/4 v7, 0x0

    .line 1881
    const-string v2, "Null bitmap"

    invoke-static {p1, v2}, Lcom/google/android/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1882
    iget-object v2, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mBitmap:Landroid/graphics/Bitmap;

    if-ne p1, v2, :cond_1

    .line 1919
    :cond_0
    :goto_0
    return-void

    .line 1883
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1884
    const-string v2, "AsyncAlbumArtImageView"

    const-string v3, "AAAIV tried to set album image to a recycled bitmap. Clearing drawables to prevent a crash, art will not be shown."

    new-instance v4, Ljava/lang/RuntimeException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "AAAIV createDrawable: bitmap "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " is recycled"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    invoke-static {v2, v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1888
    invoke-virtual {p0, v7}, Lcom/google/android/music/AsyncAlbumArtImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1889
    invoke-virtual {p0, v7}, Lcom/google/android/music/AsyncAlbumArtImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1890
    iget-object v2, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_2

    .line 1891
    iget-object v2, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-static {v2}, Lcom/google/android/music/utils/AlbumArtUtils;->recycleBitmap(Landroid/graphics/Bitmap;)V

    .line 1893
    :cond_2
    iput-object v7, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mBitmap:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 1897
    :cond_3
    iget-object v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mBitmap:Landroid/graphics/Bitmap;

    .line 1898
    .local v0, "old":Landroid/graphics/Bitmap;
    iput-object p1, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mBitmap:Landroid/graphics/Bitmap;

    .line 1900
    sget-object v2, Lcom/google/android/music/AsyncAlbumArtImageView$3;->$SwitchMap$com$google$android$music$AsyncAlbumArtImageView$LayerMode:[I

    iget-object v3, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mLayerMode:Lcom/google/android/music/AsyncAlbumArtImageView$LayerMode;

    invoke-virtual {v3}, Lcom/google/android/music/AsyncAlbumArtImageView$LayerMode;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 1910
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/music/AsyncAlbumArtImageView;->getVisibility()I

    move-result v2

    if-nez v2, :cond_4

    iget-boolean v2, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mAllowAnimation:Z

    if-eqz v2, :cond_4

    if-nez p2, :cond_4

    .line 1911
    iget-object v2, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mFadeInAnimation:Landroid/view/animation/Animation;

    invoke-virtual {p0, v2}, Lcom/google/android/music/AsyncAlbumArtImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1912
    iput-boolean v5, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mAllowAnimation:Z

    .line 1914
    :cond_4
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mActualArtworkSet:Z

    .line 1915
    iput-boolean v5, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mLoadingArtworkSet:Z

    .line 1916
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1917
    invoke-static {v0}, Lcom/google/android/music/utils/AlbumArtUtils;->recycleBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 1902
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/music/AsyncAlbumArtImageView;->getContext()Landroid/content/Context;

    move-result-object v2

    iget v3, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mRequestedHeight:I

    iget v4, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mRequestedHeight:I

    invoke-static {v2, p1, v3, v4}, Lcom/google/android/music/AsyncAlbumArtImageView;->createDrawable(Landroid/content/Context;Landroid/graphics/Bitmap;II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 1904
    .local v1, "value":Landroid/graphics/drawable/Drawable;
    invoke-virtual {p0, v1}, Lcom/google/android/music/AsyncAlbumArtImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    .line 1907
    .end local v1    # "value":Landroid/graphics/drawable/Drawable;
    :pswitch_1
    invoke-virtual {p0, p1}, Lcom/google/android/music/AsyncAlbumArtImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_1

    .line 1900
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private setExternalArtRadio(Ljava/lang/String;Z)V
    .locals 3
    .param p1, "artUrls"    # Ljava/lang/String;
    .param p2, "turnOffOverlay"    # Z

    .prologue
    const/4 v1, 0x0

    .line 1335
    monitor-enter p0

    .line 1338
    :try_start_0
    iget-object v2, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mMode:Lcom/google/android/music/AsyncAlbumArtImageView$Mode;

    instance-of v2, v2, Lcom/google/android/music/AsyncAlbumArtImageView$ExternalRadioMode;

    if-eqz v2, :cond_3

    .line 1339
    iget-object v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mMode:Lcom/google/android/music/AsyncAlbumArtImageView$Mode;

    check-cast v0, Lcom/google/android/music/AsyncAlbumArtImageView$ExternalRadioMode;

    .line 1340
    .local v0, "mode":Lcom/google/android/music/AsyncAlbumArtImageView$ExternalRadioMode;
    invoke-virtual {v0, p1}, Lcom/google/android/music/AsyncAlbumArtImageView$ExternalRadioMode;->equals(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v1, 0x1

    .line 1347
    .local v1, "needUpdate":Z
    :cond_0
    :goto_0
    if-eqz p2, :cond_1

    .line 1348
    invoke-virtual {v0}, Lcom/google/android/music/AsyncAlbumArtImageView$ExternalRadioMode;->turnOffRadioOverlay()V

    .line 1351
    :cond_1
    if-eqz v1, :cond_2

    .line 1353
    invoke-direct {p0}, Lcom/google/android/music/AsyncAlbumArtImageView;->unregisterArtChangeListener()V

    .line 1354
    iput-object p1, v0, Lcom/google/android/music/AsyncAlbumArtImageView$ExternalRadioMode;->mUrls:Ljava/lang/String;

    .line 1355
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mLoadingArtworkSet:Z

    .line 1356
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mActualArtworkSet:Z

    .line 1357
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mRequestedWidth:I

    .line 1358
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mRequestedHeight:I

    .line 1359
    invoke-direct {p0}, Lcom/google/android/music/AsyncAlbumArtImageView;->makeDrawable()V

    .line 1361
    :cond_2
    monitor-exit p0

    .line 1362
    return-void

    .line 1342
    .end local v0    # "mode":Lcom/google/android/music/AsyncAlbumArtImageView$ExternalRadioMode;
    .end local v1    # "needUpdate":Z
    :cond_3
    new-instance v0, Lcom/google/android/music/AsyncAlbumArtImageView$ExternalRadioMode;

    invoke-direct {v0}, Lcom/google/android/music/AsyncAlbumArtImageView$ExternalRadioMode;-><init>()V

    .line 1343
    .restart local v0    # "mode":Lcom/google/android/music/AsyncAlbumArtImageView$ExternalRadioMode;
    iput-object v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mMode:Lcom/google/android/music/AsyncAlbumArtImageView$Mode;

    .line 1344
    const/4 v1, 0x1

    .restart local v1    # "needUpdate":Z
    goto :goto_0

    .line 1361
    .end local v0    # "mode":Lcom/google/android/music/AsyncAlbumArtImageView$ExternalRadioMode;
    .end local v1    # "needUpdate":Z
    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method private setLoadingArtwork()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 1815
    iget-boolean v3, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mLoadingArtworkSet:Z

    if-nez v3, :cond_0

    .line 1816
    sget-object v3, Lcom/google/android/music/AsyncAlbumArtImageView$3;->$SwitchMap$com$google$android$music$AsyncAlbumArtImageView$LayerMode:[I

    iget-object v4, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mLayerMode:Lcom/google/android/music/AsyncAlbumArtImageView$LayerMode;

    invoke-virtual {v4}, Lcom/google/android/music/AsyncAlbumArtImageView$LayerMode;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 1853
    :cond_0
    :goto_0
    return-void

    .line 1820
    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/music/AsyncAlbumArtImageView;->getAlbumWidth()I

    move-result v2

    .line 1821
    .local v2, "w":I
    invoke-direct {p0}, Lcom/google/android/music/AsyncAlbumArtImageView;->getAlbumHeight()I

    move-result v1

    .line 1823
    .local v1, "h":I
    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    .line 1836
    const/4 v0, 0x0

    .line 1838
    .local v0, "drawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual {p0, v0}, Lcom/google/android/music/AsyncAlbumArtImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1839
    iput-boolean v5, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mLoadingArtworkSet:Z

    goto :goto_0

    .line 1846
    .end local v0    # "drawable":Landroid/graphics/drawable/Drawable;
    .end local v1    # "h":I
    .end local v2    # "w":I
    :pswitch_1
    const/4 v3, 0x0

    invoke-virtual {p0, v3}, Lcom/google/android/music/AsyncAlbumArtImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1848
    iput-boolean v5, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mLoadingArtworkSet:Z

    goto :goto_0

    .line 1816
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private declared-synchronized unregisterArtChangeListener()V
    .locals 1

    .prologue
    .line 1132
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/music/AsyncAlbumArtImageView;->getMissingArtHelper()Lcom/google/android/music/download/artwork/MissingArtHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/download/artwork/MissingArtHelper;->unregister()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1133
    monitor-exit p0

    return-void

    .line 1132
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public clearArtwork()V
    .locals 1

    .prologue
    .line 1169
    monitor-enter p0

    .line 1171
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/music/AsyncAlbumArtImageView;->unregisterArtChangeListener()V

    .line 1172
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mMode:Lcom/google/android/music/AsyncAlbumArtImageView$Mode;

    .line 1173
    invoke-direct {p0}, Lcom/google/android/music/AsyncAlbumArtImageView;->setLoadingArtwork()V

    .line 1174
    iget-object v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/google/android/music/utils/AlbumArtUtils;->recycleBitmap(Landroid/graphics/Bitmap;)V

    .line 1175
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mBitmap:Landroid/graphics/Bitmap;

    .line 1176
    monitor-exit p0

    .line 1177
    return-void

    .line 1176
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 1005
    invoke-super {p0}, Landroid/widget/ImageView;->onAttachedToWindow()V

    .line 1006
    iput-boolean v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mAttachedToWindow:Z

    .line 1007
    invoke-direct {p0}, Lcom/google/android/music/AsyncAlbumArtImageView;->registerArtChangeListener()V

    .line 1008
    iput-boolean v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mAllowAnimation:Z

    .line 1009
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 1013
    invoke-super {p0}, Landroid/widget/ImageView;->onDetachedFromWindow()V

    .line 1014
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mAttachedToWindow:Z

    .line 1015
    invoke-direct {p0}, Lcom/google/android/music/AsyncAlbumArtImageView;->unregisterArtChangeListener()V

    .line 1016
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mAllowAnimation:Z

    .line 1017
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 2
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    const/4 v0, 0x0

    .line 1923
    invoke-super/range {p0 .. p5}, Landroid/widget/ImageView;->onLayout(ZIIII)V

    .line 1925
    iget v1, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mVirtualWidth:I

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mVirtualHeight:I

    if-nez v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 1926
    .local v0, "isViewSizeRelevantForAlbumArt":Z
    :cond_1
    if-eqz p1, :cond_2

    if-eqz v0, :cond_2

    .line 1927
    monitor-enter p0

    .line 1928
    const/4 v1, 0x0

    :try_start_0
    iput-boolean v1, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mLoadingArtworkSet:Z

    .line 1929
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mActualArtworkSet:Z

    .line 1930
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1931
    invoke-direct {p0}, Lcom/google/android/music/AsyncAlbumArtImageView;->makeDrawable()V

    .line 1933
    :cond_2
    return-void

    .line 1930
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method protected onMeasure(II)V
    .locals 8
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/high16 v5, 0x40000000    # 2.0f

    const/high16 v6, -0x80000000

    .line 1029
    iget-boolean v7, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mLayoutAsSquare:Z

    if-eqz v7, :cond_0

    .line 1030
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v3

    .line 1031
    .local v3, "widthMode":I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    .line 1032
    .local v0, "heightMode":I
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v4

    .line 1033
    .local v4, "widthSize":I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 1035
    .local v1, "heightSize":I
    if-nez v3, :cond_1

    if-nez v0, :cond_1

    .line 1076
    .end local v0    # "heightMode":I
    .end local v1    # "heightSize":I
    .end local v3    # "widthMode":I
    .end local v4    # "widthSize":I
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Landroid/widget/ImageView;->onMeasure(II)V

    .line 1077
    return-void

    .line 1037
    .restart local v0    # "heightMode":I
    .restart local v1    # "heightSize":I
    .restart local v3    # "widthMode":I
    .restart local v4    # "widthSize":I
    :cond_1
    if-ne v3, v5, :cond_2

    if-ne v0, v5, :cond_2

    .line 1038
    invoke-static {v4, v1}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 1039
    .local v2, "size":I
    invoke-static {v2, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    move p1, p2

    .line 1041
    goto :goto_0

    .end local v2    # "size":I
    :cond_2
    if-eq v3, v5, :cond_3

    if-ne v0, v5, :cond_7

    .line 1042
    :cond_3
    if-ne v3, v5, :cond_5

    .line 1044
    if-ne v0, v6, :cond_4

    if-lt v1, v4, :cond_0

    .line 1049
    :cond_4
    move p2, p1

    goto :goto_0

    .line 1053
    :cond_5
    if-ne v3, v6, :cond_6

    if-lt v4, v1, :cond_0

    .line 1058
    :cond_6
    move p1, p2

    goto :goto_0

    .line 1061
    :cond_7
    if-ne v3, v6, :cond_9

    if-ne v0, v6, :cond_9

    .line 1062
    invoke-static {v4, v1}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 1063
    .restart local v2    # "size":I
    iget-boolean v7, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mStretchToFill:Z

    if-eqz v7, :cond_8

    :goto_1
    invoke-static {v2, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    move p1, p2

    .line 1066
    goto :goto_0

    :cond_8
    move v5, v6

    .line 1063
    goto :goto_1

    .line 1066
    .end local v2    # "size":I
    :cond_9
    if-eq v3, v6, :cond_a

    if-ne v0, v6, :cond_d

    .line 1067
    :cond_a
    if-ne v3, v6, :cond_b

    move v2, v4

    .line 1068
    .restart local v2    # "size":I
    :goto_2
    iget-boolean v7, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mStretchToFill:Z

    if-eqz v7, :cond_c

    :goto_3
    invoke-static {v2, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    move p1, p2

    .line 1071
    goto :goto_0

    .end local v2    # "size":I
    :cond_b
    move v2, v1

    .line 1067
    goto :goto_2

    .restart local v2    # "size":I
    :cond_c
    move v5, v6

    .line 1068
    goto :goto_3

    .line 1072
    .end local v2    # "size":I
    :cond_d
    new-instance v5, Ljava/lang/IllegalStateException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Unknown modes: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v5
.end method

.method protected onSizeChanged(IIII)V
    .locals 3
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    .line 1083
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ImageView;->onSizeChanged(IIII)V

    .line 1084
    iget-boolean v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mLayoutAsSquare:Z

    if-eqz v0, :cond_0

    if-eq p1, p2, :cond_0

    .line 1085
    const-string v0, "AsyncAlbumArtImageView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "told to layout as square, but provided sizes ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") were not square"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1088
    :cond_0
    return-void
.end method

.method public onStartTemporaryDetach()V
    .locals 1

    .prologue
    .line 1021
    invoke-super {p0}, Landroid/widget/ImageView;->onStartTemporaryDetach()V

    .line 1022
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mAllowAnimation:Z

    .line 1023
    return-void
.end method

.method public requestLayout()V
    .locals 1

    .prologue
    .line 1937
    invoke-static {}, Lcom/google/android/music/preferences/MusicPreferences;->isKitKatOrGreater()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1944
    invoke-super {p0}, Landroid/widget/ImageView;->requestLayout()V

    .line 1958
    :cond_0
    return-void
.end method

.method public setAlbumId(JLjava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "id"    # J
    .param p3, "album"    # Ljava/lang/String;
    .param p4, "artist"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 1141
    monitor-enter p0

    .line 1144
    :try_start_0
    iget-object v2, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mMode:Lcom/google/android/music/AsyncAlbumArtImageView$Mode;

    instance-of v2, v2, Lcom/google/android/music/AsyncAlbumArtImageView$AlbumMode;

    if-eqz v2, :cond_2

    .line 1145
    iget-object v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mMode:Lcom/google/android/music/AsyncAlbumArtImageView$Mode;

    check-cast v0, Lcom/google/android/music/AsyncAlbumArtImageView$AlbumMode;

    .line 1146
    .local v0, "mode":Lcom/google/android/music/AsyncAlbumArtImageView$AlbumMode;
    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/music/AsyncAlbumArtImageView$AlbumMode;->equals(JLjava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v1, 0x1

    .line 1152
    .local v1, "needUpdate":Z
    :cond_0
    :goto_0
    if-eqz v1, :cond_1

    .line 1154
    invoke-direct {p0}, Lcom/google/android/music/AsyncAlbumArtImageView;->unregisterArtChangeListener()V

    .line 1155
    iput-wide p1, v0, Lcom/google/android/music/AsyncAlbumArtImageView$AlbumMode;->albumId:J

    .line 1156
    iput-object p3, v0, Lcom/google/android/music/AsyncAlbumArtImageView$AlbumMode;->album:Ljava/lang/String;

    .line 1157
    iput-object p4, v0, Lcom/google/android/music/AsyncAlbumArtImageView$AlbumMode;->artist:Ljava/lang/String;

    .line 1158
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mLoadingArtworkSet:Z

    .line 1159
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mActualArtworkSet:Z

    .line 1160
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mRequestedWidth:I

    .line 1161
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mRequestedHeight:I

    .line 1162
    invoke-direct {p0}, Lcom/google/android/music/AsyncAlbumArtImageView;->getMissingArtHelper()Lcom/google/android/music/download/artwork/MissingArtHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/music/download/artwork/MissingArtHelper;->clear()V

    .line 1163
    invoke-direct {p0}, Lcom/google/android/music/AsyncAlbumArtImageView;->makeDrawable()V

    .line 1165
    :cond_1
    monitor-exit p0

    .line 1166
    return-void

    .line 1148
    .end local v0    # "mode":Lcom/google/android/music/AsyncAlbumArtImageView$AlbumMode;
    .end local v1    # "needUpdate":Z
    :cond_2
    new-instance v0, Lcom/google/android/music/AsyncAlbumArtImageView$AlbumMode;

    invoke-direct {v0}, Lcom/google/android/music/AsyncAlbumArtImageView$AlbumMode;-><init>()V

    .line 1149
    .restart local v0    # "mode":Lcom/google/android/music/AsyncAlbumArtImageView$AlbumMode;
    iput-object v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mMode:Lcom/google/android/music/AsyncAlbumArtImageView$Mode;

    .line 1150
    const/4 v1, 0x1

    .restart local v1    # "needUpdate":Z
    goto :goto_0

    .line 1165
    .end local v0    # "mode":Lcom/google/android/music/AsyncAlbumArtImageView$AlbumMode;
    .end local v1    # "needUpdate":Z
    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public setAllSongs(Ljava/lang/String;JLandroid/net/Uri;)V
    .locals 2
    .param p1, "parentName"    # Ljava/lang/String;
    .param p2, "id"    # J
    .param p4, "childUri"    # Landroid/net/Uri;

    .prologue
    .line 1199
    monitor-enter p0

    .line 1201
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/music/AsyncAlbumArtImageView;->unregisterArtChangeListener()V

    .line 1202
    iget-object v1, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mMode:Lcom/google/android/music/AsyncAlbumArtImageView$Mode;

    instance-of v1, v1, Lcom/google/android/music/AsyncAlbumArtImageView$AllSongsMode;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mMode:Lcom/google/android/music/AsyncAlbumArtImageView$Mode;

    check-cast v1, Lcom/google/android/music/AsyncAlbumArtImageView$AllSongsMode;

    move-object v0, v1

    .line 1204
    .local v0, "mode":Lcom/google/android/music/AsyncAlbumArtImageView$AllSongsMode;
    :goto_0
    iput-object v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mMode:Lcom/google/android/music/AsyncAlbumArtImageView$Mode;

    .line 1205
    iput-object p1, v0, Lcom/google/android/music/AsyncAlbumArtImageView$AllSongsMode;->parentName:Ljava/lang/String;

    .line 1206
    iput-wide p2, v0, Lcom/google/android/music/AsyncAlbumArtImageView$AllSongsMode;->id:J

    .line 1207
    iput-object p4, v0, Lcom/google/android/music/AsyncAlbumArtImageView$AllSongsMode;->uri:Landroid/net/Uri;

    .line 1208
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mLoadingArtworkSet:Z

    .line 1209
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mActualArtworkSet:Z

    .line 1210
    const/4 v1, 0x0

    iput v1, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mRequestedWidth:I

    .line 1211
    const/4 v1, 0x0

    iput v1, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mRequestedHeight:I

    .line 1212
    invoke-direct {p0}, Lcom/google/android/music/AsyncAlbumArtImageView;->makeDrawable()V

    .line 1213
    monitor-exit p0

    .line 1214
    return-void

    .line 1202
    .end local v0    # "mode":Lcom/google/android/music/AsyncAlbumArtImageView$AllSongsMode;
    :cond_0
    new-instance v0, Lcom/google/android/music/AsyncAlbumArtImageView$AllSongsMode;

    invoke-direct {v0}, Lcom/google/android/music/AsyncAlbumArtImageView$AllSongsMode;-><init>()V

    goto :goto_0

    .line 1213
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public setArtForSonglist(Lcom/google/android/music/medialist/SongList;)V
    .locals 2
    .param p1, "songlist"    # Lcom/google/android/music/medialist/SongList;

    .prologue
    .line 1180
    monitor-enter p0

    .line 1182
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/music/AsyncAlbumArtImageView;->unregisterArtChangeListener()V

    .line 1183
    iget-object v1, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mMode:Lcom/google/android/music/AsyncAlbumArtImageView$Mode;

    instance-of v1, v1, Lcom/google/android/music/AsyncAlbumArtImageView$SongListMode;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mMode:Lcom/google/android/music/AsyncAlbumArtImageView$Mode;

    check-cast v1, Lcom/google/android/music/AsyncAlbumArtImageView$SongListMode;

    move-object v0, v1

    .line 1185
    .local v0, "mode":Lcom/google/android/music/AsyncAlbumArtImageView$SongListMode;
    :goto_0
    iput-object v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mMode:Lcom/google/android/music/AsyncAlbumArtImageView$Mode;

    .line 1186
    iput-object p1, v0, Lcom/google/android/music/AsyncAlbumArtImageView$SongListMode;->songlist:Lcom/google/android/music/medialist/SongList;

    .line 1187
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mLoadingArtworkSet:Z

    .line 1188
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mActualArtworkSet:Z

    .line 1189
    const/4 v1, 0x0

    iput v1, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mRequestedWidth:I

    .line 1190
    const/4 v1, 0x0

    iput v1, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mRequestedHeight:I

    .line 1191
    invoke-direct {p0}, Lcom/google/android/music/AsyncAlbumArtImageView;->makeDrawable()V

    .line 1192
    monitor-exit p0

    .line 1193
    return-void

    .line 1183
    .end local v0    # "mode":Lcom/google/android/music/AsyncAlbumArtImageView$SongListMode;
    :cond_0
    new-instance v0, Lcom/google/android/music/AsyncAlbumArtImageView$SongListMode;

    invoke-direct {v0}, Lcom/google/android/music/AsyncAlbumArtImageView$SongListMode;-><init>()V

    goto :goto_0

    .line 1192
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public setArtistArt(Ljava/lang/String;JLandroid/net/Uri;Z)V
    .locals 8
    .param p1, "parentName"    # Ljava/lang/String;
    .param p2, "id"    # J
    .param p4, "childUri"    # Landroid/net/Uri;
    .param p5, "useFauxArt"    # Z

    .prologue
    const/4 v6, 0x0

    .line 1400
    monitor-enter p0

    .line 1403
    :try_start_0
    iget-object v1, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mMode:Lcom/google/android/music/AsyncAlbumArtImageView$Mode;

    instance-of v1, v1, Lcom/google/android/music/AsyncAlbumArtImageView$ArtistMode;

    if-eqz v1, :cond_2

    .line 1404
    iget-object v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mMode:Lcom/google/android/music/AsyncAlbumArtImageView$Mode;

    check-cast v0, Lcom/google/android/music/AsyncAlbumArtImageView$ArtistMode;

    .local v0, "mode":Lcom/google/android/music/AsyncAlbumArtImageView$ArtistMode;
    move-object v1, p1

    move-wide v2, p2

    move-object v4, p4

    move v5, p5

    .line 1405
    invoke-virtual/range {v0 .. v5}, Lcom/google/android/music/AsyncAlbumArtImageView$ArtistMode;->equals(Ljava/lang/String;JLandroid/net/Uri;Z)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v6, 0x1

    .line 1411
    .local v6, "needUpdate":Z
    :cond_0
    :goto_0
    if-eqz v6, :cond_1

    .line 1413
    invoke-direct {p0}, Lcom/google/android/music/AsyncAlbumArtImageView;->unregisterArtChangeListener()V

    .line 1414
    iput-object p1, v0, Lcom/google/android/music/AsyncAlbumArtImageView$ArtistMode;->parentName:Ljava/lang/String;

    .line 1415
    iput-wide p2, v0, Lcom/google/android/music/AsyncAlbumArtImageView$ArtistMode;->id:J

    .line 1416
    iput-object p4, v0, Lcom/google/android/music/AsyncAlbumArtImageView$ArtistMode;->uri:Landroid/net/Uri;

    .line 1417
    iput-boolean p5, v0, Lcom/google/android/music/AsyncAlbumArtImageView$ArtistMode;->useFauxArt:Z

    .line 1418
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mLoadingArtworkSet:Z

    .line 1419
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mActualArtworkSet:Z

    .line 1420
    const/4 v1, 0x0

    iput v1, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mRequestedWidth:I

    .line 1421
    const/4 v1, 0x0

    iput v1, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mRequestedHeight:I

    .line 1422
    invoke-direct {p0}, Lcom/google/android/music/AsyncAlbumArtImageView;->makeDrawable()V

    .line 1424
    :cond_1
    monitor-exit p0

    .line 1425
    return-void

    .line 1407
    .end local v0    # "mode":Lcom/google/android/music/AsyncAlbumArtImageView$ArtistMode;
    .end local v6    # "needUpdate":Z
    :cond_2
    new-instance v0, Lcom/google/android/music/AsyncAlbumArtImageView$ArtistMode;

    invoke-direct {v0}, Lcom/google/android/music/AsyncAlbumArtImageView$ArtistMode;-><init>()V

    .line 1408
    .restart local v0    # "mode":Lcom/google/android/music/AsyncAlbumArtImageView$ArtistMode;
    iput-object v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mMode:Lcom/google/android/music/AsyncAlbumArtImageView$Mode;

    .line 1409
    const/4 v6, 0x1

    .restart local v6    # "needUpdate":Z
    goto :goto_0

    .line 1424
    .end local v0    # "mode":Lcom/google/android/music/AsyncAlbumArtImageView$ArtistMode;
    .end local v6    # "needUpdate":Z
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public setAvailable(Z)V
    .locals 3
    .param p1, "available"    # Z

    .prologue
    .line 1519
    iget-object v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mLayerMode:Lcom/google/android/music/AsyncAlbumArtImageView$LayerMode;

    sget-object v1, Lcom/google/android/music/AsyncAlbumArtImageView$LayerMode;->FOREGROUND:Lcom/google/android/music/AsyncAlbumArtImageView$LayerMode;

    if-ne v0, v1, :cond_0

    .line 1520
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setAvailable only available in "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/google/android/music/AsyncAlbumArtImageView$LayerMode;->BACKGROUND:Lcom/google/android/music/AsyncAlbumArtImageView$LayerMode;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1523
    :cond_0
    iput-boolean p1, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mAvailable:Z

    .line 1524
    if-eqz p1, :cond_1

    .line 1525
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/music/AsyncAlbumArtImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1529
    :goto_0
    return-void

    .line 1527
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mNotAvailableImageOverlay:Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0, v0}, Lcom/google/android/music/AsyncAlbumArtImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public setExternalAlbumArt(Ljava/lang/String;)V
    .locals 3
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 1264
    monitor-enter p0

    .line 1267
    :try_start_0
    iget-object v2, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mMode:Lcom/google/android/music/AsyncAlbumArtImageView$Mode;

    instance-of v2, v2, Lcom/google/android/music/AsyncAlbumArtImageView$ExternalAlbumMode;

    if-eqz v2, :cond_2

    .line 1268
    iget-object v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mMode:Lcom/google/android/music/AsyncAlbumArtImageView$Mode;

    check-cast v0, Lcom/google/android/music/AsyncAlbumArtImageView$ExternalAlbumMode;

    .line 1269
    .local v0, "mode":Lcom/google/android/music/AsyncAlbumArtImageView$ExternalAlbumMode;
    invoke-virtual {v0, p1}, Lcom/google/android/music/AsyncAlbumArtImageView$ExternalAlbumMode;->equals(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v1, 0x1

    .line 1275
    .local v1, "needUpdate":Z
    :cond_0
    :goto_0
    if-eqz v1, :cond_1

    .line 1277
    invoke-direct {p0}, Lcom/google/android/music/AsyncAlbumArtImageView;->unregisterArtChangeListener()V

    .line 1278
    iput-object p1, v0, Lcom/google/android/music/AsyncAlbumArtImageView$ExternalAlbumMode;->mUrl:Ljava/lang/String;

    .line 1279
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mLoadingArtworkSet:Z

    .line 1280
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mActualArtworkSet:Z

    .line 1281
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mRequestedWidth:I

    .line 1282
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mRequestedHeight:I

    .line 1283
    invoke-direct {p0}, Lcom/google/android/music/AsyncAlbumArtImageView;->makeDrawable()V

    .line 1285
    :cond_1
    monitor-exit p0

    .line 1286
    return-void

    .line 1271
    .end local v0    # "mode":Lcom/google/android/music/AsyncAlbumArtImageView$ExternalAlbumMode;
    .end local v1    # "needUpdate":Z
    :cond_2
    new-instance v0, Lcom/google/android/music/AsyncAlbumArtImageView$ExternalAlbumMode;

    invoke-direct {v0}, Lcom/google/android/music/AsyncAlbumArtImageView$ExternalAlbumMode;-><init>()V

    .line 1272
    .restart local v0    # "mode":Lcom/google/android/music/AsyncAlbumArtImageView$ExternalAlbumMode;
    iput-object v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mMode:Lcom/google/android/music/AsyncAlbumArtImageView$Mode;

    .line 1273
    const/4 v1, 0x1

    .restart local v1    # "needUpdate":Z
    goto :goto_0

    .line 1285
    .end local v0    # "mode":Lcom/google/android/music/AsyncAlbumArtImageView$ExternalAlbumMode;
    .end local v1    # "needUpdate":Z
    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public setExternalArtRadio(Ljava/lang/String;)V
    .locals 1
    .param p1, "artUrls"    # Ljava/lang/String;

    .prologue
    .line 1318
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/music/AsyncAlbumArtImageView;->setExternalArtRadio(Ljava/lang/String;Z)V

    .line 1319
    return-void
.end method

.method public setExternalArtRadioWithoutOverlay(Ljava/lang/String;)V
    .locals 1
    .param p1, "artUrls"    # Ljava/lang/String;

    .prologue
    .line 1327
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/music/AsyncAlbumArtImageView;->setExternalArtRadio(Ljava/lang/String;Z)V

    .line 1328
    return-void
.end method

.method public setGenreArt(Ljava/lang/String;JLandroid/net/Uri;)V
    .locals 4
    .param p1, "parentName"    # Ljava/lang/String;
    .param p2, "id"    # J
    .param p4, "childUri"    # Landroid/net/Uri;

    .prologue
    const/4 v1, 0x0

    .line 1431
    monitor-enter p0

    .line 1434
    :try_start_0
    iget-object v2, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mMode:Lcom/google/android/music/AsyncAlbumArtImageView$Mode;

    instance-of v2, v2, Lcom/google/android/music/AsyncAlbumArtImageView$GenreMode;

    if-eqz v2, :cond_2

    .line 1435
    iget-object v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mMode:Lcom/google/android/music/AsyncAlbumArtImageView$Mode;

    check-cast v0, Lcom/google/android/music/AsyncAlbumArtImageView$GenreMode;

    .line 1436
    .local v0, "mode":Lcom/google/android/music/AsyncAlbumArtImageView$GenreMode;
    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/music/AsyncAlbumArtImageView$GenreMode;->equals(Ljava/lang/String;JLandroid/net/Uri;)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v1, 0x1

    .line 1442
    .local v1, "needUpdate":Z
    :cond_0
    :goto_0
    if-eqz v1, :cond_1

    .line 1444
    invoke-direct {p0}, Lcom/google/android/music/AsyncAlbumArtImageView;->unregisterArtChangeListener()V

    .line 1445
    iput-object p1, v0, Lcom/google/android/music/AsyncAlbumArtImageView$GenreMode;->parentName:Ljava/lang/String;

    .line 1446
    iput-wide p2, v0, Lcom/google/android/music/AsyncAlbumArtImageView$GenreMode;->id:J

    .line 1447
    iput-object p4, v0, Lcom/google/android/music/AsyncAlbumArtImageView$GenreMode;->uri:Landroid/net/Uri;

    .line 1448
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mLoadingArtworkSet:Z

    .line 1449
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mActualArtworkSet:Z

    .line 1450
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mRequestedWidth:I

    .line 1451
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mRequestedHeight:I

    .line 1452
    invoke-direct {p0}, Lcom/google/android/music/AsyncAlbumArtImageView;->makeDrawable()V

    .line 1454
    :cond_1
    monitor-exit p0

    .line 1455
    return-void

    .line 1438
    .end local v0    # "mode":Lcom/google/android/music/AsyncAlbumArtImageView$GenreMode;
    .end local v1    # "needUpdate":Z
    :cond_2
    new-instance v0, Lcom/google/android/music/AsyncAlbumArtImageView$GenreMode;

    invoke-direct {v0}, Lcom/google/android/music/AsyncAlbumArtImageView$GenreMode;-><init>()V

    .line 1439
    .restart local v0    # "mode":Lcom/google/android/music/AsyncAlbumArtImageView$GenreMode;
    iput-object v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mMode:Lcom/google/android/music/AsyncAlbumArtImageView$Mode;

    .line 1440
    const/4 v1, 0x1

    .restart local v1    # "needUpdate":Z
    goto :goto_0

    .line 1454
    .end local v0    # "mode":Lcom/google/android/music/AsyncAlbumArtImageView$GenreMode;
    .end local v1    # "needUpdate":Z
    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public setIsScrolling(Z)V
    .locals 2
    .param p1, "scrolling"    # Z

    .prologue
    .line 1532
    if-nez p1, :cond_1

    iget-boolean v1, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mIsScrolling:Z

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    .line 1533
    .local v0, "needsUpdate":Z
    :goto_0
    iput-boolean p1, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mIsScrolling:Z

    .line 1534
    if-eqz v0, :cond_0

    .line 1536
    invoke-direct {p0}, Lcom/google/android/music/AsyncAlbumArtImageView;->makeDrawable()V

    .line 1538
    :cond_0
    return-void

    .line 1532
    .end local v0    # "needsUpdate":Z
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setLayerMode(Lcom/google/android/music/AsyncAlbumArtImageView$LayerMode;)V
    .locals 0
    .param p1, "mode"    # Lcom/google/android/music/AsyncAlbumArtImageView$LayerMode;

    .prologue
    .line 1000
    iput-object p1, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mLayerMode:Lcom/google/android/music/AsyncAlbumArtImageView$LayerMode;

    .line 1001
    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1, "listener"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 1092
    invoke-super {p0, p1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1094
    new-instance v0, Lcom/google/android/music/AsyncAlbumArtImageView$1;

    invoke-direct {v0, p0}, Lcom/google/android/music/AsyncAlbumArtImageView$1;-><init>(Lcom/google/android/music/AsyncAlbumArtImageView;)V

    invoke-virtual {p0, v0}, Lcom/google/android/music/AsyncAlbumArtImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1123
    return-void
.end method

.method public setPlaylistAlbumArt(JLjava/lang/String;I)V
    .locals 3
    .param p1, "id"    # J
    .param p3, "mainLabel"    # Ljava/lang/String;
    .param p4, "listType"    # I

    .prologue
    const/4 v1, 0x0

    .line 1236
    monitor-enter p0

    .line 1239
    :try_start_0
    iget-object v2, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mMode:Lcom/google/android/music/AsyncAlbumArtImageView$Mode;

    instance-of v2, v2, Lcom/google/android/music/AsyncAlbumArtImageView$PlaylistMode;

    if-eqz v2, :cond_2

    .line 1240
    iget-object v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mMode:Lcom/google/android/music/AsyncAlbumArtImageView$Mode;

    check-cast v0, Lcom/google/android/music/AsyncAlbumArtImageView$PlaylistMode;

    .line 1241
    .local v0, "mode":Lcom/google/android/music/AsyncAlbumArtImageView$PlaylistMode;
    invoke-static {p4}, Lcom/google/android/music/utils/AlbumArtUtils;->playlistTypeToArtStyle(I)I

    move-result v2

    invoke-virtual {v0, p3, p1, p2, v2}, Lcom/google/android/music/AsyncAlbumArtImageView$PlaylistMode;->equals(Ljava/lang/String;JI)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v1, 0x1

    .line 1248
    .local v1, "needUpdate":Z
    :cond_0
    :goto_0
    if-eqz v1, :cond_1

    .line 1250
    invoke-direct {p0}, Lcom/google/android/music/AsyncAlbumArtImageView;->unregisterArtChangeListener()V

    .line 1251
    iput-object p3, v0, Lcom/google/android/music/AsyncAlbumArtImageView$PlaylistMode;->mainLabel:Ljava/lang/String;

    .line 1252
    iput-wide p1, v0, Lcom/google/android/music/AsyncAlbumArtImageView$PlaylistMode;->id:J

    .line 1253
    invoke-static {p4}, Lcom/google/android/music/utils/AlbumArtUtils;->playlistTypeToArtStyle(I)I

    move-result v2

    iput v2, v0, Lcom/google/android/music/AsyncAlbumArtImageView$PlaylistMode;->style:I

    .line 1254
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mLoadingArtworkSet:Z

    .line 1255
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mActualArtworkSet:Z

    .line 1256
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mRequestedWidth:I

    .line 1257
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mRequestedHeight:I

    .line 1258
    invoke-direct {p0}, Lcom/google/android/music/AsyncAlbumArtImageView;->makeDrawable()V

    .line 1260
    :cond_1
    monitor-exit p0

    .line 1261
    return-void

    .line 1244
    .end local v0    # "mode":Lcom/google/android/music/AsyncAlbumArtImageView$PlaylistMode;
    .end local v1    # "needUpdate":Z
    :cond_2
    new-instance v0, Lcom/google/android/music/AsyncAlbumArtImageView$PlaylistMode;

    invoke-direct {v0}, Lcom/google/android/music/AsyncAlbumArtImageView$PlaylistMode;-><init>()V

    .line 1245
    .restart local v0    # "mode":Lcom/google/android/music/AsyncAlbumArtImageView$PlaylistMode;
    iput-object v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mMode:Lcom/google/android/music/AsyncAlbumArtImageView$Mode;

    .line 1246
    const/4 v1, 0x1

    .restart local v1    # "needUpdate":Z
    goto :goto_0

    .line 1260
    .end local v0    # "mode":Lcom/google/android/music/AsyncAlbumArtImageView$PlaylistMode;
    .end local v1    # "needUpdate":Z
    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public setServiceAlbumArt(JLjava/lang/String;Ljava/lang/String;Lcom/google/android/music/playback/IMusicPlaybackService;)V
    .locals 3
    .param p1, "albumId"    # J
    .param p3, "artist"    # Ljava/lang/String;
    .param p4, "album"    # Ljava/lang/String;
    .param p5, "service"    # Lcom/google/android/music/playback/IMusicPlaybackService;

    .prologue
    .line 1499
    monitor-enter p0

    .line 1501
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/music/AsyncAlbumArtImageView;->unregisterArtChangeListener()V

    .line 1502
    iget-object v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mMode:Lcom/google/android/music/AsyncAlbumArtImageView$Mode;

    instance-of v0, v0, Lcom/google/android/music/AsyncAlbumArtImageView$ServiceAlbumMode;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mMode:Lcom/google/android/music/AsyncAlbumArtImageView$Mode;

    check-cast v0, Lcom/google/android/music/AsyncAlbumArtImageView$ServiceAlbumMode;

    iget-wide v0, v0, Lcom/google/android/music/AsyncAlbumArtImageView$ServiceAlbumMode;->albumId:J

    cmp-long v0, v0, p1

    if-eqz v0, :cond_1

    .line 1504
    :cond_0
    new-instance v0, Lcom/google/android/music/AsyncAlbumArtImageView$ServiceAlbumMode;

    invoke-direct {v0}, Lcom/google/android/music/AsyncAlbumArtImageView$ServiceAlbumMode;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mMode:Lcom/google/android/music/AsyncAlbumArtImageView$Mode;

    .line 1505
    iget-object v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mMode:Lcom/google/android/music/AsyncAlbumArtImageView$Mode;

    check-cast v0, Lcom/google/android/music/AsyncAlbumArtImageView$ServiceAlbumMode;

    iput-wide p1, v0, Lcom/google/android/music/AsyncAlbumArtImageView$ServiceAlbumMode;->albumId:J

    .line 1506
    iget-object v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mMode:Lcom/google/android/music/AsyncAlbumArtImageView$Mode;

    check-cast v0, Lcom/google/android/music/AsyncAlbumArtImageView$ServiceAlbumMode;

    iput-object p3, v0, Lcom/google/android/music/AsyncAlbumArtImageView$ServiceAlbumMode;->artist:Ljava/lang/String;

    .line 1507
    iget-object v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mMode:Lcom/google/android/music/AsyncAlbumArtImageView$Mode;

    check-cast v0, Lcom/google/android/music/AsyncAlbumArtImageView$ServiceAlbumMode;

    iput-object p4, v0, Lcom/google/android/music/AsyncAlbumArtImageView$ServiceAlbumMode;->album:Ljava/lang/String;

    .line 1508
    iget-object v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mMode:Lcom/google/android/music/AsyncAlbumArtImageView$Mode;

    check-cast v0, Lcom/google/android/music/AsyncAlbumArtImageView$ServiceAlbumMode;

    iput-object p5, v0, Lcom/google/android/music/AsyncAlbumArtImageView$ServiceAlbumMode;->service:Lcom/google/android/music/playback/IMusicPlaybackService;

    .line 1509
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mLoadingArtworkSet:Z

    .line 1510
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mActualArtworkSet:Z

    .line 1511
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mRequestedWidth:I

    .line 1512
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mRequestedHeight:I

    .line 1513
    invoke-direct {p0}, Lcom/google/android/music/AsyncAlbumArtImageView;->makeDrawable()V

    .line 1515
    :cond_1
    monitor-exit p0

    .line 1516
    return-void

    .line 1515
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setSharedPlaylistArt(Ljava/lang/String;)V
    .locals 3
    .param p1, "artUrls"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 1369
    monitor-enter p0

    .line 1374
    :try_start_0
    iget-object v2, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mMode:Lcom/google/android/music/AsyncAlbumArtImageView$Mode;

    instance-of v2, v2, Lcom/google/android/music/AsyncAlbumArtImageView$MultiUrlCompositeMode;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mMode:Lcom/google/android/music/AsyncAlbumArtImageView$Mode;

    instance-of v2, v2, Lcom/google/android/music/AsyncAlbumArtImageView$ExternalRadioMode;

    if-nez v2, :cond_2

    .line 1376
    iget-object v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mMode:Lcom/google/android/music/AsyncAlbumArtImageView$Mode;

    check-cast v0, Lcom/google/android/music/AsyncAlbumArtImageView$MultiUrlCompositeMode;

    .line 1377
    .local v0, "mode":Lcom/google/android/music/AsyncAlbumArtImageView$MultiUrlCompositeMode;
    invoke-virtual {v0, p1}, Lcom/google/android/music/AsyncAlbumArtImageView$MultiUrlCompositeMode;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v1, 0x1

    .line 1383
    .local v1, "needUpdate":Z
    :cond_0
    :goto_0
    if-eqz v1, :cond_1

    .line 1385
    invoke-direct {p0}, Lcom/google/android/music/AsyncAlbumArtImageView;->unregisterArtChangeListener()V

    .line 1386
    iput-object p1, v0, Lcom/google/android/music/AsyncAlbumArtImageView$MultiUrlCompositeMode;->mUrls:Ljava/lang/String;

    .line 1387
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mLoadingArtworkSet:Z

    .line 1388
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mActualArtworkSet:Z

    .line 1389
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mRequestedWidth:I

    .line 1390
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mRequestedHeight:I

    .line 1391
    invoke-direct {p0}, Lcom/google/android/music/AsyncAlbumArtImageView;->makeDrawable()V

    .line 1393
    :cond_1
    monitor-exit p0

    .line 1394
    return-void

    .line 1379
    .end local v0    # "mode":Lcom/google/android/music/AsyncAlbumArtImageView$MultiUrlCompositeMode;
    .end local v1    # "needUpdate":Z
    :cond_2
    new-instance v0, Lcom/google/android/music/AsyncAlbumArtImageView$MultiUrlCompositeMode;

    invoke-direct {v0}, Lcom/google/android/music/AsyncAlbumArtImageView$MultiUrlCompositeMode;-><init>()V

    .line 1380
    .restart local v0    # "mode":Lcom/google/android/music/AsyncAlbumArtImageView$MultiUrlCompositeMode;
    iput-object v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mMode:Lcom/google/android/music/AsyncAlbumArtImageView$Mode;

    .line 1381
    const/4 v1, 0x1

    .restart local v1    # "needUpdate":Z
    goto :goto_0

    .line 1393
    .end local v0    # "mode":Lcom/google/android/music/AsyncAlbumArtImageView$MultiUrlCompositeMode;
    .end local v1    # "needUpdate":Z
    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public setShuffleAllArt(Ljava/lang/String;)V
    .locals 2
    .param p1, "parentName"    # Ljava/lang/String;

    .prologue
    .line 1220
    monitor-enter p0

    .line 1222
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/music/AsyncAlbumArtImageView;->unregisterArtChangeListener()V

    .line 1223
    iget-object v1, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mMode:Lcom/google/android/music/AsyncAlbumArtImageView$Mode;

    instance-of v1, v1, Lcom/google/android/music/AsyncAlbumArtImageView$ShuffleAllMode;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mMode:Lcom/google/android/music/AsyncAlbumArtImageView$Mode;

    check-cast v1, Lcom/google/android/music/AsyncAlbumArtImageView$ShuffleAllMode;

    move-object v0, v1

    .line 1225
    .local v0, "mode":Lcom/google/android/music/AsyncAlbumArtImageView$ShuffleAllMode;
    :goto_0
    iput-object p1, v0, Lcom/google/android/music/AsyncAlbumArtImageView$ShuffleAllMode;->parentName:Ljava/lang/String;

    .line 1226
    iput-object v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mMode:Lcom/google/android/music/AsyncAlbumArtImageView$Mode;

    .line 1227
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mLoadingArtworkSet:Z

    .line 1228
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mActualArtworkSet:Z

    .line 1229
    const/4 v1, 0x0

    iput v1, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mRequestedWidth:I

    .line 1230
    const/4 v1, 0x0

    iput v1, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mRequestedHeight:I

    .line 1231
    invoke-direct {p0}, Lcom/google/android/music/AsyncAlbumArtImageView;->makeDrawable()V

    .line 1232
    monitor-exit p0

    .line 1233
    return-void

    .line 1223
    .end local v0    # "mode":Lcom/google/android/music/AsyncAlbumArtImageView$ShuffleAllMode;
    :cond_0
    new-instance v0, Lcom/google/android/music/AsyncAlbumArtImageView$ShuffleAllMode;

    invoke-direct {v0}, Lcom/google/android/music/AsyncAlbumArtImageView$ShuffleAllMode;-><init>()V

    goto :goto_0

    .line 1232
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public setVideoThumbnailArt(Ljava/lang/String;)V
    .locals 3
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 1289
    monitor-enter p0

    .line 1292
    :try_start_0
    iget-object v2, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mMode:Lcom/google/android/music/AsyncAlbumArtImageView$Mode;

    instance-of v2, v2, Lcom/google/android/music/AsyncAlbumArtImageView$VideoThumbnailMode;

    if-eqz v2, :cond_2

    .line 1293
    iget-object v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mMode:Lcom/google/android/music/AsyncAlbumArtImageView$Mode;

    check-cast v0, Lcom/google/android/music/AsyncAlbumArtImageView$VideoThumbnailMode;

    .line 1294
    .local v0, "mode":Lcom/google/android/music/AsyncAlbumArtImageView$VideoThumbnailMode;
    invoke-virtual {v0, p1}, Lcom/google/android/music/AsyncAlbumArtImageView$VideoThumbnailMode;->equals(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v1, 0x1

    .line 1300
    .local v1, "needUpdate":Z
    :cond_0
    :goto_0
    if-eqz v1, :cond_1

    .line 1302
    invoke-direct {p0}, Lcom/google/android/music/AsyncAlbumArtImageView;->unregisterArtChangeListener()V

    .line 1303
    iput-object p1, v0, Lcom/google/android/music/AsyncAlbumArtImageView$VideoThumbnailMode;->mUrl:Ljava/lang/String;

    .line 1304
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mLoadingArtworkSet:Z

    .line 1305
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mActualArtworkSet:Z

    .line 1306
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mRequestedWidth:I

    .line 1307
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mRequestedHeight:I

    .line 1308
    invoke-direct {p0}, Lcom/google/android/music/AsyncAlbumArtImageView;->makeDrawable()V

    .line 1310
    :cond_1
    monitor-exit p0

    .line 1311
    return-void

    .line 1296
    .end local v0    # "mode":Lcom/google/android/music/AsyncAlbumArtImageView$VideoThumbnailMode;
    .end local v1    # "needUpdate":Z
    :cond_2
    new-instance v0, Lcom/google/android/music/AsyncAlbumArtImageView$VideoThumbnailMode;

    invoke-direct {v0}, Lcom/google/android/music/AsyncAlbumArtImageView$VideoThumbnailMode;-><init>()V

    .line 1297
    .restart local v0    # "mode":Lcom/google/android/music/AsyncAlbumArtImageView$VideoThumbnailMode;
    iput-object v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mMode:Lcom/google/android/music/AsyncAlbumArtImageView$Mode;

    .line 1298
    const/4 v1, 0x1

    .restart local v1    # "needUpdate":Z
    goto :goto_0

    .line 1310
    .end local v0    # "mode":Lcom/google/android/music/AsyncAlbumArtImageView$VideoThumbnailMode;
    .end local v1    # "needUpdate":Z
    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public setVirtualSize(II)V
    .locals 2
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 1595
    if-nez p1, :cond_0

    if-eqz p2, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mLayerMode:Lcom/google/android/music/AsyncAlbumArtImageView$LayerMode;

    sget-object v1, Lcom/google/android/music/AsyncAlbumArtImageView$LayerMode;->FOREGROUND:Lcom/google/android/music/AsyncAlbumArtImageView$LayerMode;

    if-eq v0, v1, :cond_1

    .line 1596
    const-string v0, "AsyncAlbumArtImageView"

    const-string v1, "Can not set virtual size if Album-Art-Mode is not FOREGROUND"

    invoke-static {v0, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 1599
    :cond_1
    iget v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mVirtualWidth:I

    if-ne v0, p1, :cond_2

    iget v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mVirtualHeight:I

    if-eq v0, p2, :cond_3

    .line 1600
    :cond_2
    iput p1, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mVirtualWidth:I

    .line 1601
    iput p2, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mVirtualHeight:I

    .line 1602
    monitor-enter p0

    .line 1603
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mLoadingArtworkSet:Z

    .line 1604
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mActualArtworkSet:Z

    .line 1605
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1606
    invoke-direct {p0}, Lcom/google/android/music/AsyncAlbumArtImageView;->makeDrawable()V

    .line 1608
    :cond_3
    return-void

    .line 1605
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public showDefaultArtwork()V
    .locals 1

    .prologue
    .line 1458
    monitor-enter p0

    .line 1459
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mMode:Lcom/google/android/music/AsyncAlbumArtImageView$Mode;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mMode:Lcom/google/android/music/AsyncAlbumArtImageView$Mode;

    instance-of v0, v0, Lcom/google/android/music/AsyncAlbumArtImageView$DefaultArtworkMode;

    if-eqz v0, :cond_0

    .line 1461
    monitor-exit p0

    .line 1470
    :goto_0
    return-void

    .line 1463
    :cond_0
    new-instance v0, Lcom/google/android/music/AsyncAlbumArtImageView$DefaultArtworkMode;

    invoke-direct {v0}, Lcom/google/android/music/AsyncAlbumArtImageView$DefaultArtworkMode;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mMode:Lcom/google/android/music/AsyncAlbumArtImageView$Mode;

    .line 1464
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mLoadingArtworkSet:Z

    .line 1465
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mActualArtworkSet:Z

    .line 1466
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mRequestedWidth:I

    .line 1467
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mRequestedHeight:I

    .line 1468
    invoke-direct {p0}, Lcom/google/android/music/AsyncAlbumArtImageView;->makeDrawable()V

    .line 1469
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public showImFeelingLuckyArtwork(Z)V
    .locals 3
    .param p1, "canStartIFL"    # Z

    .prologue
    .line 1473
    monitor-enter p0

    .line 1476
    :try_start_0
    iget-object v2, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mMode:Lcom/google/android/music/AsyncAlbumArtImageView$Mode;

    instance-of v2, v2, Lcom/google/android/music/AsyncAlbumArtImageView$ImFeelingLuckyArtworkMode;

    if-eqz v2, :cond_1

    .line 1477
    iget-object v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mMode:Lcom/google/android/music/AsyncAlbumArtImageView$Mode;

    check-cast v0, Lcom/google/android/music/AsyncAlbumArtImageView$ImFeelingLuckyArtworkMode;

    .line 1478
    .local v0, "mode":Lcom/google/android/music/AsyncAlbumArtImageView$ImFeelingLuckyArtworkMode;
    invoke-virtual {v0, p1}, Lcom/google/android/music/AsyncAlbumArtImageView$ImFeelingLuckyArtworkMode;->hasIFLAvailabilityChanged(Z)Z

    move-result v1

    .line 1479
    .local v1, "needUpdate":Z
    invoke-virtual {v0, p1}, Lcom/google/android/music/AsyncAlbumArtImageView$ImFeelingLuckyArtworkMode;->setIFLAvailability(Z)V

    .line 1487
    :goto_0
    if-eqz v1, :cond_0

    .line 1488
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mLoadingArtworkSet:Z

    .line 1489
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mActualArtworkSet:Z

    .line 1490
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mRequestedHeight:I

    .line 1491
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mRequestedWidth:I

    .line 1492
    invoke-direct {p0}, Lcom/google/android/music/AsyncAlbumArtImageView;->makeDrawable()V

    .line 1494
    :cond_0
    monitor-exit p0

    .line 1495
    return-void

    .line 1481
    .end local v0    # "mode":Lcom/google/android/music/AsyncAlbumArtImageView$ImFeelingLuckyArtworkMode;
    .end local v1    # "needUpdate":Z
    :cond_1
    new-instance v2, Lcom/google/android/music/AsyncAlbumArtImageView$ImFeelingLuckyArtworkMode;

    invoke-direct {v2}, Lcom/google/android/music/AsyncAlbumArtImageView$ImFeelingLuckyArtworkMode;-><init>()V

    iput-object v2, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mMode:Lcom/google/android/music/AsyncAlbumArtImageView$Mode;

    .line 1482
    iget-object v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView;->mMode:Lcom/google/android/music/AsyncAlbumArtImageView$Mode;

    check-cast v0, Lcom/google/android/music/AsyncAlbumArtImageView$ImFeelingLuckyArtworkMode;

    .line 1483
    .restart local v0    # "mode":Lcom/google/android/music/AsyncAlbumArtImageView$ImFeelingLuckyArtworkMode;
    invoke-virtual {v0, p1}, Lcom/google/android/music/AsyncAlbumArtImageView$ImFeelingLuckyArtworkMode;->setIFLAvailability(Z)V

    .line 1484
    const/4 v1, 0x1

    .restart local v1    # "needUpdate":Z
    goto :goto_0

    .line 1494
    .end local v0    # "mode":Lcom/google/android/music/AsyncAlbumArtImageView$ImFeelingLuckyArtworkMode;
    .end local v1    # "needUpdate":Z
    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method
