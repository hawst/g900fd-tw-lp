.class public Lcom/google/android/music/store/ConfigItem;
.super Ljava/lang/Object;
.source "ConfigItem.java"


# static fields
.field private static final PROJECTION:[Ljava/lang/String;


# instance fields
.field private mId:J

.field private mName:Ljava/lang/String;

.field private mType:I

.field private mValue:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 28
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "CONFIG.id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "CONFIG.Name"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "CONFIG.Value"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "CONFIG.Type"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/store/ConfigItem;->PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    return-void
.end method

.method public static compileInsertStatement(Landroid/database/sqlite/SQLiteDatabase;)Landroid/database/sqlite/SQLiteStatement;
    .locals 1
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 101
    const-string v0, "insert into CONFIG ( Name, Value, Type) values (?,?,?)"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v0

    return-object v0
.end method

.method public static deleteAllServerSettings(Landroid/database/sqlite/SQLiteDatabase;)I
    .locals 6
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    const/4 v5, 0x1

    .line 161
    const-string v1, "Type=?"

    .line 163
    .local v1, "selection":Ljava/lang/String;
    const-string v2, "CONFIG"

    new-array v3, v5, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {p0, v2, v1, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 167
    .local v0, "deleted":I
    return v0
.end method

.method private prepareInsertOrFullUpdate(Landroid/database/sqlite/SQLiteStatement;)V
    .locals 4
    .param p1, "statement"    # Landroid/database/sqlite/SQLiteStatement;

    .prologue
    const/4 v2, 0x2

    .line 105
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    .line 107
    iget-object v0, p0, Lcom/google/android/music/store/ConfigItem;->mName:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 108
    new-instance v0, Lcom/google/android/music/store/InvalidDataException;

    const-string v1, "Name must be set before storing"

    invoke-direct {v0, v1}, Lcom/google/android/music/store/InvalidDataException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 110
    :cond_0
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/music/store/ConfigItem;->mName:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 112
    iget-object v0, p0, Lcom/google/android/music/store/ConfigItem;->mValue:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 113
    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteStatement;->bindNull(I)V

    .line 118
    :goto_0
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/android/music/store/ConfigItem;->mType:I

    int-to-long v2, v1

    invoke-virtual {p1, v0, v2, v3}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 119
    return-void

    .line 115
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/store/ConfigItem;->mValue:Ljava/lang/String;

    invoke-virtual {p1, v2, v0}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public insert(Landroid/database/sqlite/SQLiteStatement;)J
    .locals 6
    .param p1, "insertStatement"    # Landroid/database/sqlite/SQLiteStatement;

    .prologue
    .line 140
    iget-wide v2, p0, Lcom/google/android/music/store/ConfigItem;->mId:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    .line 141
    new-instance v2, Lcom/google/android/music/store/InvalidDataException;

    const-string v3, "The local id of a config entry must not be set for an insert."

    invoke-direct {v2, v3}, Lcom/google/android/music/store/InvalidDataException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 145
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/music/store/ConfigItem;->prepareInsertOrFullUpdate(Landroid/database/sqlite/SQLiteStatement;)V

    .line 146
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteStatement;->executeInsert()J

    move-result-wide v0

    .line 147
    .local v0, "insertedId":J
    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-nez v2, :cond_1

    .line 148
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "Failed to insert into config"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 150
    :cond_1
    iput-wide v0, p0, Lcom/google/android/music/store/ConfigItem;->mId:J

    .line 153
    iget-wide v2, p0, Lcom/google/android/music/store/ConfigItem;->mId:J

    return-wide v2
.end method

.method public reset()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 49
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/music/store/ConfigItem;->mId:J

    .line 50
    iput-object v2, p0, Lcom/google/android/music/store/ConfigItem;->mName:Ljava/lang/String;

    .line 51
    iput-object v2, p0, Lcom/google/android/music/store/ConfigItem;->mValue:Ljava/lang/String;

    .line 52
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/music/store/ConfigItem;->mType:I

    .line 53
    return-void
.end method

.method public final setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 81
    iput-object p1, p0, Lcom/google/android/music/store/ConfigItem;->mName:Ljava/lang/String;

    .line 82
    return-void
.end method

.method public setType(I)V
    .locals 0
    .param p1, "type"    # I

    .prologue
    .line 97
    iput p1, p0, Lcom/google/android/music/store/ConfigItem;->mType:I

    .line 98
    return-void
.end method

.method public final setValue(Ljava/lang/String;)V
    .locals 0
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 89
    iput-object p1, p0, Lcom/google/android/music/store/ConfigItem;->mValue:Ljava/lang/String;

    .line 90
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 57
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 58
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 59
    const-string v1, "mId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/music/store/ConfigItem;->mId:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 60
    const-string v1, "mName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/store/ConfigItem;->mName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 61
    const-string v1, "mValue="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/store/ConfigItem;->mValue:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 62
    const-string v1, "mType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/music/store/ConfigItem;->mType:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 63
    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 65
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
