.class Lcom/google/android/music/download/artwork/ArtMonitorImpl;
.super Ljava/lang/Object;
.source "ArtMonitorImpl.java"

# interfaces
.implements Lcom/google/android/music/download/artwork/ArtMonitor;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/download/artwork/ArtMonitorImpl$IncomingHandler;
    }
.end annotation


# static fields
.field private static final LOGV:Z


# instance fields
.field private final mArtworkListeners:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/LinkedList",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private mBound:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final mContext:Landroid/content/Context;

.field private final mIncomingMessenger:Landroid/os/Messenger;

.field private final mLock:Ljava/lang/Object;

.field private final mMainHandler:Landroid/os/Handler;

.field private final mReferenceQueue:Ljava/lang/ref/ReferenceQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/ReferenceQueue",
            "<",
            "Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;",
            ">;"
        }
    .end annotation
.end field

.field private final mReverseArtListenerCache:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mServiceConnection:Lcom/google/android/music/utils/SafeServiceConnection;

.field private final mServiceIdleListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/download/artwork/ArtMonitor$ArtServiceIdleListener;",
            ">;"
        }
    .end annotation
.end field

.field private final mUnbindServiceRunnable:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->DOWNLOAD:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/download/artwork/ArtMonitorImpl;->LOGV:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/download/artwork/ArtMonitorImpl;->mArtworkListeners:Ljava/util/Map;

    .line 44
    invoke-static {}, Lcom/google/android/music/ui/cardlib/utils/Lists;->newLinkedList()Ljava/util/LinkedList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/download/artwork/ArtMonitorImpl;->mServiceIdleListeners:Ljava/util/List;

    .line 46
    new-instance v0, Ljava/lang/ref/ReferenceQueue;

    invoke-direct {v0}, Ljava/lang/ref/ReferenceQueue;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/download/artwork/ArtMonitorImpl;->mReferenceQueue:Ljava/lang/ref/ReferenceQueue;

    .line 48
    new-instance v0, Landroid/os/Messenger;

    new-instance v1, Lcom/google/android/music/download/artwork/ArtMonitorImpl$IncomingHandler;

    invoke-direct {v1, p0}, Lcom/google/android/music/download/artwork/ArtMonitorImpl$IncomingHandler;-><init>(Lcom/google/android/music/download/artwork/ArtMonitorImpl;)V

    invoke-direct {v0, v1}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/google/android/music/download/artwork/ArtMonitorImpl;->mIncomingMessenger:Landroid/os/Messenger;

    .line 52
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/download/artwork/ArtMonitorImpl;->mReverseArtListenerCache:Ljava/util/Map;

    .line 57
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/download/artwork/ArtMonitorImpl;->mLock:Ljava/lang/Object;

    .line 59
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/music/download/artwork/ArtMonitorImpl;->mMainHandler:Landroid/os/Handler;

    .line 61
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/music/download/artwork/ArtMonitorImpl;->mBound:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 63
    new-instance v0, Lcom/google/android/music/download/artwork/ArtMonitorImpl$1;

    invoke-direct {v0, p0}, Lcom/google/android/music/download/artwork/ArtMonitorImpl$1;-><init>(Lcom/google/android/music/download/artwork/ArtMonitorImpl;)V

    iput-object v0, p0, Lcom/google/android/music/download/artwork/ArtMonitorImpl;->mServiceConnection:Lcom/google/android/music/utils/SafeServiceConnection;

    .line 89
    new-instance v0, Lcom/google/android/music/download/artwork/ArtMonitorImpl$2;

    invoke-direct {v0, p0}, Lcom/google/android/music/download/artwork/ArtMonitorImpl$2;-><init>(Lcom/google/android/music/download/artwork/ArtMonitorImpl;)V

    iput-object v0, p0, Lcom/google/android/music/download/artwork/ArtMonitorImpl;->mUnbindServiceRunnable:Ljava/lang/Runnable;

    .line 99
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/download/artwork/ArtMonitorImpl;->mContext:Landroid/content/Context;

    .line 100
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/download/artwork/ArtMonitorImpl;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/download/artwork/ArtMonitorImpl;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/music/download/artwork/ArtMonitorImpl;->mLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/music/download/artwork/ArtMonitorImpl;)Landroid/os/Messenger;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/download/artwork/ArtMonitorImpl;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/music/download/artwork/ArtMonitorImpl;->mIncomingMessenger:Landroid/os/Messenger;

    return-object v0
.end method

.method static synthetic access$200()Z
    .locals 1

    .prologue
    .line 37
    sget-boolean v0, Lcom/google/android/music/download/artwork/ArtMonitorImpl;->LOGV:Z

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/music/download/artwork/ArtMonitorImpl;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/download/artwork/ArtMonitorImpl;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/music/download/artwork/ArtMonitorImpl;->mBound:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/music/download/artwork/ArtMonitorImpl;)Lcom/google/android/music/utils/SafeServiceConnection;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/download/artwork/ArtMonitorImpl;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/music/download/artwork/ArtMonitorImpl;->mServiceConnection:Lcom/google/android/music/utils/SafeServiceConnection;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/music/download/artwork/ArtMonitorImpl;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/download/artwork/ArtMonitorImpl;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/music/download/artwork/ArtMonitorImpl;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/music/download/artwork/ArtMonitorImpl;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/download/artwork/ArtMonitorImpl;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/google/android/music/download/artwork/ArtMonitorImpl;->onArtChanged(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/music/download/artwork/ArtMonitorImpl;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/download/artwork/ArtMonitorImpl;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/google/android/music/download/artwork/ArtMonitorImpl;->onError(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/music/download/artwork/ArtMonitorImpl;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/download/artwork/ArtMonitorImpl;

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/google/android/music/download/artwork/ArtMonitorImpl;->handleServiceIdleLocked()V

    return-void
.end method

.method private addArtworkListenerLocked(Ljava/lang/String;Ljava/util/LinkedList;)V
    .locals 1
    .param p1, "remoteLocation"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/LinkedList",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 174
    .local p2, "list":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/lang/ref/WeakReference<Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;>;>;"
    iget-object v0, p0, Lcom/google/android/music/download/artwork/ArtMonitorImpl;->mArtworkListeners:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 175
    return-void
.end method

.method private cleanArtListenerCache()V
    .locals 8

    .prologue
    .line 183
    iget-object v7, p0, Lcom/google/android/music/download/artwork/ArtMonitorImpl;->mArtworkListeners:Ljava/util/Map;

    monitor-enter v7

    .line 185
    :cond_0
    :try_start_0
    iget-object v6, p0, Lcom/google/android/music/download/artwork/ArtMonitorImpl;->mReferenceQueue:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {v6}, Ljava/lang/ref/ReferenceQueue;->poll()Ljava/lang/ref/Reference;

    move-result-object v3

    .local v3, "ref":Ljava/lang/ref/Reference;, "Ljava/lang/ref/Reference<+Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;>;"
    if-eqz v3, :cond_3

    .line 186
    iget-object v6, p0, Lcom/google/android/music/download/artwork/ArtMonitorImpl;->mReverseArtListenerCache:Ljava/util/Map;

    invoke-interface {v6, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 187
    .local v0, "artIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    if-eqz v0, :cond_0

    .line 188
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 189
    .local v4, "remoteLocation":Ljava/lang/String;
    iget-object v6, p0, Lcom/google/android/music/download/artwork/ArtMonitorImpl;->mArtworkListeners:Ljava/util/Map;

    invoke-interface {v6, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/LinkedList;

    .line 191
    .local v2, "list":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/lang/ref/WeakReference<Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;>;>;"
    if-eqz v2, :cond_1

    .line 195
    :cond_2
    invoke-virtual {v2, v3}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    move-result v5

    .line 196
    .local v5, "removed":Z
    if-nez v5, :cond_2

    .line 198
    invoke-virtual {v2}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 199
    invoke-direct {p0, v4}, Lcom/google/android/music/download/artwork/ArtMonitorImpl;->removeArtworkListenerLocked(Ljava/lang/String;)V

    goto :goto_0

    .line 205
    .end local v0    # "artIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "list":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/lang/ref/WeakReference<Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;>;>;"
    .end local v3    # "ref":Ljava/lang/ref/Reference;, "Ljava/lang/ref/Reference<+Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;>;"
    .end local v4    # "remoteLocation":Ljava/lang/String;
    .end local v5    # "removed":Z
    :catchall_0
    move-exception v6

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v6

    .restart local v3    # "ref":Ljava/lang/ref/Reference;, "Ljava/lang/ref/Reference<+Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;>;"
    :cond_3
    :try_start_1
    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 206
    return-void
.end method

.method private getListenersToNotifyLocked(Ljava/lang/String;)Ljava/util/List;
    .locals 8
    .param p1, "remoteArtUrl"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;",
            ">;"
        }
    .end annotation

    .prologue
    .line 117
    const/4 v4, 0x0

    .line 118
    .local v4, "listenersToNotify":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;>;"
    iget-object v7, p0, Lcom/google/android/music/download/artwork/ArtMonitorImpl;->mArtworkListeners:Ljava/util/Map;

    invoke-interface {v7, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/LinkedList;

    .line 120
    .local v3, "list":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/lang/ref/WeakReference<Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;>;>;"
    if-eqz v3, :cond_4

    .line 121
    invoke-virtual {v3}, Ljava/util/LinkedList;->size()I

    move-result v6

    .line 122
    .local v6, "size":I
    invoke-virtual {v3}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 124
    .local v0, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/ref/WeakReference<Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;>;>;"
    const/4 v2, 0x0

    .line 125
    .local v2, "itemsToRemove":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/ref/WeakReference<Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;>;>;"
    new-instance v4, Ljava/util/ArrayList;

    .end local v4    # "listenersToNotify":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;>;"
    invoke-direct {v4, v6}, Ljava/util/ArrayList;-><init>(I)V

    .line 126
    .restart local v4    # "listenersToNotify":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;>;"
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 127
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/ref/WeakReference;

    .line 128
    .local v1, "item":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;>;"
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;

    .line 129
    .local v5, "possibleListener":Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;
    if-nez v5, :cond_1

    .line 130
    if-nez v2, :cond_0

    .line 131
    new-instance v2, Ljava/util/ArrayList;

    .end local v2    # "itemsToRemove":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/ref/WeakReference<Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;>;>;"
    invoke-direct {v2, v6}, Ljava/util/ArrayList;-><init>(I)V

    .line 134
    .restart local v2    # "itemsToRemove":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/ref/WeakReference<Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;>;>;"
    :cond_0
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 136
    :cond_1
    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 139
    .end local v1    # "item":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;>;"
    .end local v5    # "possibleListener":Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;
    :cond_2
    if-eqz v2, :cond_3

    .line 140
    invoke-virtual {v3, v2}, Ljava/util/LinkedList;->removeAll(Ljava/util/Collection;)Z

    .line 142
    :cond_3
    invoke-virtual {v3}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 143
    invoke-direct {p0, p1}, Lcom/google/android/music/download/artwork/ArtMonitorImpl;->removeArtworkListenerLocked(Ljava/lang/String;)V

    .line 147
    .end local v0    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/ref/WeakReference<Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;>;>;"
    .end local v2    # "itemsToRemove":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/ref/WeakReference<Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;>;>;"
    .end local v6    # "size":I
    :cond_4
    return-object v4
.end method

.method private handleServiceIdleLocked()V
    .locals 3

    .prologue
    .line 164
    iget-object v2, p0, Lcom/google/android/music/download/artwork/ArtMonitorImpl;->mServiceIdleListeners:Ljava/util/List;

    monitor-enter v2

    .line 165
    :try_start_0
    iget-object v1, p0, Lcom/google/android/music/download/artwork/ArtMonitorImpl;->mServiceIdleListeners:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v0, v1, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_0

    .line 166
    iget-object v1, p0, Lcom/google/android/music/download/artwork/ArtMonitorImpl;->mServiceIdleListeners:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/download/artwork/ArtMonitor$ArtServiceIdleListener;

    invoke-interface {v1}, Lcom/google/android/music/download/artwork/ArtMonitor$ArtServiceIdleListener;->onServiceIdle()V

    .line 165
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 168
    :cond_0
    monitor-exit v2

    .line 169
    return-void

    .line 168
    .end local v0    # "i":I
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private onArtChanged(Ljava/lang/String;)V
    .locals 5
    .param p1, "remoteArtUrl"    # Ljava/lang/String;

    .prologue
    .line 103
    iget-object v4, p0, Lcom/google/android/music/download/artwork/ArtMonitorImpl;->mArtworkListeners:Ljava/util/Map;

    monitor-enter v4

    .line 104
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/music/download/artwork/ArtMonitorImpl;->getListenersToNotifyLocked(Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    .line 105
    .local v2, "listenersToNotify":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;>;"
    if-eqz v2, :cond_0

    .line 107
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;

    .line 109
    .local v1, "listener":Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;
    invoke-interface {v1, p1}, Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;->onArtChanged(Ljava/lang/String;)V

    goto :goto_0

    .line 112
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "listener":Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;
    .end local v2    # "listenersToNotify":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;>;"
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .restart local v2    # "listenersToNotify":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;>;"
    :cond_0
    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 113
    return-void
.end method

.method private onError(Ljava/lang/String;)V
    .locals 5
    .param p1, "remoteArtUrl"    # Ljava/lang/String;

    .prologue
    .line 151
    iget-object v4, p0, Lcom/google/android/music/download/artwork/ArtMonitorImpl;->mArtworkListeners:Ljava/util/Map;

    monitor-enter v4

    .line 152
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/music/download/artwork/ArtMonitorImpl;->getListenersToNotifyLocked(Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    .line 153
    .local v2, "listenersToNotify":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;>;"
    if-eqz v2, :cond_0

    .line 155
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;

    .line 157
    .local v1, "listener":Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;
    invoke-interface {v1, p1}, Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;->onError(Ljava/lang/String;)V

    goto :goto_0

    .line 160
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "listener":Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;
    .end local v2    # "listenersToNotify":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;>;"
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .restart local v2    # "listenersToNotify":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;>;"
    :cond_0
    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 161
    return-void
.end method

.method private removeArtworkListenerLocked(Ljava/lang/String;)V
    .locals 1
    .param p1, "remoteLocation"    # Ljava/lang/String;

    .prologue
    .line 179
    iget-object v0, p0, Lcom/google/android/music/download/artwork/ArtMonitorImpl;->mArtworkListeners:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 180
    return-void
.end method


# virtual methods
.method public getWatchedArtUrls()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 297
    iget-object v5, p0, Lcom/google/android/music/download/artwork/ArtMonitorImpl;->mArtworkListeners:Ljava/util/Map;

    monitor-enter v5

    .line 298
    :try_start_0
    iget-object v4, p0, Lcom/google/android/music/download/artwork/ArtMonitorImpl;->mArtworkListeners:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->size()I

    move-result v3

    .line 299
    .local v3, "size":I
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 300
    .local v2, "result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v4, p0, Lcom/google/android/music/download/artwork/ArtMonitorImpl;->mArtworkListeners:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 301
    .local v1, "key":Ljava/lang/String;
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 304
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "key":Ljava/lang/String;
    .end local v2    # "result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v3    # "size":I
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    .line 303
    .restart local v0    # "i$":Ljava/util/Iterator;
    .restart local v2    # "result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v3    # "size":I
    :cond_0
    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v2
.end method

.method public registerArtChangeListener(Ljava/lang/String;Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;)V
    .locals 8
    .param p1, "remoteLocation"    # Ljava/lang/String;
    .param p2, "listener"    # Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;

    .prologue
    .line 211
    invoke-direct {p0}, Lcom/google/android/music/download/artwork/ArtMonitorImpl;->cleanArtListenerCache()V

    .line 212
    iget-object v4, p0, Lcom/google/android/music/download/artwork/ArtMonitorImpl;->mArtworkListeners:Ljava/util/Map;

    monitor-enter v4

    .line 213
    :try_start_0
    iget-object v3, p0, Lcom/google/android/music/download/artwork/ArtMonitorImpl;->mArtworkListeners:Ljava/util/Map;

    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/LinkedList;

    .line 214
    .local v1, "list":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/lang/ref/WeakReference<Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;>;>;"
    if-nez v1, :cond_0

    .line 215
    new-instance v1, Ljava/util/LinkedList;

    .end local v1    # "list":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/lang/ref/WeakReference<Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;>;>;"
    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 216
    .restart local v1    # "list":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/lang/ref/WeakReference<Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;>;>;"
    invoke-direct {p0, p1, v1}, Lcom/google/android/music/download/artwork/ArtMonitorImpl;->addArtworkListenerLocked(Ljava/lang/String;Ljava/util/LinkedList;)V

    .line 218
    :cond_0
    new-instance v2, Ljava/lang/ref/WeakReference;

    iget-object v3, p0, Lcom/google/android/music/download/artwork/ArtMonitorImpl;->mReferenceQueue:Ljava/lang/ref/ReferenceQueue;

    invoke-direct {v2, p2, v3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;Ljava/lang/ref/ReferenceQueue;)V

    .line 220
    .local v2, "ref":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;>;"
    invoke-virtual {v1, v2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 222
    iget-object v3, p0, Lcom/google/android/music/download/artwork/ArtMonitorImpl;->mReverseArtListenerCache:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 223
    .local v0, "allRegisteredArt":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    if-nez v0, :cond_1

    .line 224
    new-instance v0, Ljava/util/TreeSet;

    .end local v0    # "allRegisteredArt":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-direct {v0}, Ljava/util/TreeSet;-><init>()V

    .line 225
    .restart local v0    # "allRegisteredArt":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    iget-object v3, p0, Lcom/google/android/music/download/artwork/ArtMonitorImpl;->mReverseArtListenerCache:Ljava/util/Map;

    invoke-interface {v3, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 227
    :cond_1
    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 229
    iget-object v3, p0, Lcom/google/android/music/download/artwork/ArtMonitorImpl;->mMainHandler:Landroid/os/Handler;

    iget-object v5, p0, Lcom/google/android/music/download/artwork/ArtMonitorImpl;->mUnbindServiceRunnable:Ljava/lang/Runnable;

    invoke-virtual {v3, v5}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 230
    iget-object v3, p0, Lcom/google/android/music/download/artwork/ArtMonitorImpl;->mBound:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v5, 0x0

    const/4 v6, 0x1

    invoke-virtual {v3, v5, v6}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 231
    iget-object v3, p0, Lcom/google/android/music/download/artwork/ArtMonitorImpl;->mContext:Landroid/content/Context;

    new-instance v5, Landroid/content/Intent;

    iget-object v6, p0, Lcom/google/android/music/download/artwork/ArtMonitorImpl;->mContext:Landroid/content/Context;

    const-class v7, Lcom/google/android/music/download/artwork/ArtDownloadService;

    invoke-direct {v5, v6, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v6, p0, Lcom/google/android/music/download/artwork/ArtMonitorImpl;->mServiceConnection:Lcom/google/android/music/utils/SafeServiceConnection;

    const/4 v7, 0x1

    invoke-virtual {v3, v5, v6, v7}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 234
    :cond_2
    monitor-exit v4

    .line 235
    return-void

    .line 234
    .end local v0    # "allRegisteredArt":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v1    # "list":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/lang/ref/WeakReference<Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;>;>;"
    .end local v2    # "ref":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;>;"
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method

.method public registerArtServiceIdleListener(Lcom/google/android/music/download/artwork/ArtMonitor$ArtServiceIdleListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/google/android/music/download/artwork/ArtMonitor$ArtServiceIdleListener;

    .prologue
    .line 282
    iget-object v1, p0, Lcom/google/android/music/download/artwork/ArtMonitorImpl;->mServiceIdleListeners:Ljava/util/List;

    monitor-enter v1

    .line 283
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/download/artwork/ArtMonitorImpl;->mServiceIdleListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 284
    monitor-exit v1

    .line 285
    return-void

    .line 284
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public unregisterArtChangeListener(Ljava/lang/String;Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;)V
    .locals 12
    .param p1, "remoteLocation"    # Ljava/lang/String;
    .param p2, "listener"    # Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;

    .prologue
    .line 239
    iget-object v8, p0, Lcom/google/android/music/download/artwork/ArtMonitorImpl;->mArtworkListeners:Ljava/util/Map;

    monitor-enter v8

    .line 240
    :try_start_0
    iget-object v7, p0, Lcom/google/android/music/download/artwork/ArtMonitorImpl;->mArtworkListeners:Ljava/util/Map;

    invoke-interface {v7, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/LinkedList;

    .line 242
    .local v4, "list":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/lang/ref/WeakReference<Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;>;>;"
    if-eqz v4, :cond_5

    .line 243
    invoke-virtual {v4}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 245
    .local v1, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/ref/WeakReference<Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;>;>;"
    const/4 v3, 0x0

    .line 246
    .local v3, "itemsToRemove":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/ref/WeakReference<Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;>;>;"
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 247
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/ref/WeakReference;

    .line 248
    .local v2, "item":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;>;"
    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;

    .line 249
    .local v5, "possibleListener":Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;
    if-eqz v5, :cond_1

    if-ne v5, p2, :cond_0

    .line 250
    :cond_1
    if-nez v3, :cond_2

    .line 251
    invoke-virtual {v4}, Ljava/util/LinkedList;->size()I

    move-result v6

    .line 252
    .local v6, "size":I
    new-instance v3, Ljava/util/ArrayList;

    .end local v3    # "itemsToRemove":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/ref/WeakReference<Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;>;>;"
    invoke-direct {v3, v6}, Ljava/util/ArrayList;-><init>(I)V

    .line 255
    .end local v6    # "size":I
    .restart local v3    # "itemsToRemove":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/ref/WeakReference<Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;>;>;"
    :cond_2
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 257
    iget-object v7, p0, Lcom/google/android/music/download/artwork/ArtMonitorImpl;->mReverseArtListenerCache:Ljava/util/Map;

    invoke-interface {v7, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 258
    .local v0, "allArtForListener":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    if-eqz v0, :cond_0

    .line 259
    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 260
    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 261
    iget-object v7, p0, Lcom/google/android/music/download/artwork/ArtMonitorImpl;->mReverseArtListenerCache:Ljava/util/Map;

    invoke-interface {v7, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 277
    .end local v0    # "allArtForListener":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v1    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/ref/WeakReference<Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;>;>;"
    .end local v2    # "item":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;>;"
    .end local v3    # "itemsToRemove":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/ref/WeakReference<Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;>;>;"
    .end local v4    # "list":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/lang/ref/WeakReference<Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;>;>;"
    .end local v5    # "possibleListener":Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;
    :catchall_0
    move-exception v7

    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v7

    .line 266
    .restart local v1    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/ref/WeakReference<Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;>;>;"
    .restart local v3    # "itemsToRemove":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/ref/WeakReference<Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;>;>;"
    .restart local v4    # "list":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/lang/ref/WeakReference<Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;>;>;"
    :cond_3
    if-eqz v3, :cond_4

    .line 267
    :try_start_1
    invoke-virtual {v4, v3}, Ljava/util/LinkedList;->removeAll(Ljava/util/Collection;)Z

    .line 269
    :cond_4
    invoke-virtual {v4}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_5

    .line 270
    invoke-direct {p0, p1}, Lcom/google/android/music/download/artwork/ArtMonitorImpl;->removeArtworkListenerLocked(Ljava/lang/String;)V

    .line 274
    .end local v1    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/ref/WeakReference<Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;>;>;"
    .end local v3    # "itemsToRemove":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/ref/WeakReference<Lcom/google/android/music/download/artwork/ArtMonitor$ArtChangeListener;>;>;"
    :cond_5
    iget-object v7, p0, Lcom/google/android/music/download/artwork/ArtMonitorImpl;->mBound:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v7}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v7

    if-eqz v7, :cond_6

    iget-object v7, p0, Lcom/google/android/music/download/artwork/ArtMonitorImpl;->mArtworkListeners:Ljava/util/Map;

    invoke-interface {v7}, Ljava/util/Map;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_6

    .line 275
    iget-object v7, p0, Lcom/google/android/music/download/artwork/ArtMonitorImpl;->mMainHandler:Landroid/os/Handler;

    iget-object v9, p0, Lcom/google/android/music/download/artwork/ArtMonitorImpl;->mUnbindServiceRunnable:Ljava/lang/Runnable;

    const-wide/16 v10, 0x2710

    invoke-virtual {v7, v9, v10, v11}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 277
    :cond_6
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 278
    return-void
.end method

.method public unregisterArtServiceIdleListener(Lcom/google/android/music/download/artwork/ArtMonitor$ArtServiceIdleListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/google/android/music/download/artwork/ArtMonitor$ArtServiceIdleListener;

    .prologue
    .line 289
    iget-object v1, p0, Lcom/google/android/music/download/artwork/ArtMonitorImpl;->mServiceIdleListeners:Ljava/util/List;

    monitor-enter v1

    .line 290
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/download/artwork/ArtMonitorImpl;->mServiceIdleListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 291
    monitor-exit v1

    .line 292
    return-void

    .line 291
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
