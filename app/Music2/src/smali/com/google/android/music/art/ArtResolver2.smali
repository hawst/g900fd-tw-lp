.class public Lcom/google/android/music/art/ArtResolver2;
.super Ljava/lang/Object;
.source "ArtResolver2.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;,
        Lcom/google/android/music/art/ArtResolver2$BitmapOriginInfo;,
        Lcom/google/android/music/art/ArtResolver2$ArtUrl;,
        Lcom/google/android/music/art/ArtResolver2$LoadRenderTask;,
        Lcom/google/android/music/art/ArtResolver2$HandlerKey;,
        Lcom/google/android/music/art/ArtResolver2$RequestListener;
    }
.end annotation


# static fields
.field public static final DEFAULT_CONFIG:Landroid/graphics/Bitmap$Config;

.field static final LOGV:Z

.field private static final sInitializationLock:Ljava/lang/Object;

.field private static sInstance:Lcom/google/android/music/art/ArtResolver2;


# instance fields
.field private final mAppContext:Landroid/content/Context;

.field private final mArtLoader:Lcom/google/android/music/art/ArtLoader;

.field private final mAvailableHandlers:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/music/art/ArtResolver2$HandlerKey;",
            "Lcom/google/android/music/art/ArtTypeHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final mBitmaps:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/music/art/ArtResolver2$BitmapOriginInfo;",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mBitmapsLock:Ljava/lang/Object;

.field private final mFilesCache:Lcom/google/android/music/art/ArtFileDescriptorLruCache;

.field private final mItemToRequestMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Object;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/art/ArtRequest2;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mOldArtResolver:Lcom/google/android/music/art/ArtResolver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 98
    sget-object v0, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    sput-object v0, Lcom/google/android/music/art/ArtResolver2;->DEFAULT_CONFIG:Landroid/graphics/Bitmap$Config;

    .line 99
    sget-boolean v0, Lcom/google/android/music/art/ArtResolver;->LOGV:Z

    sput-boolean v0, Lcom/google/android/music/art/ArtResolver2;->LOGV:Z

    .line 106
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/music/art/ArtResolver2;->sInitializationLock:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "appContext"    # Landroid/content/Context;

    .prologue
    .line 123
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 102
    invoke-static {}, Lcom/google/android/music/ui/cardlib/utils/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/art/ArtResolver2;->mAvailableHandlers:Ljava/util/Map;

    .line 104
    invoke-static {}, Lcom/google/android/music/ui/cardlib/utils/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/art/ArtResolver2;->mItemToRequestMap:Ljava/util/Map;

    .line 112
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/google/android/music/art/ArtResolver2;->mBitmapsLock:Ljava/lang/Object;

    .line 113
    invoke-static {}, Lcom/google/android/music/ui/cardlib/utils/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/art/ArtResolver2;->mBitmaps:Ljava/util/Map;

    .line 124
    iput-object p1, p0, Lcom/google/android/music/art/ArtResolver2;->mAppContext:Landroid/content/Context;

    .line 125
    invoke-static {p1}, Lcom/google/android/music/art/ArtResolver;->getInstance(Landroid/content/Context;)Lcom/google/android/music/art/ArtResolver;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/art/ArtResolver2;->mOldArtResolver:Lcom/google/android/music/art/ArtResolver;

    .line 126
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "music_artresolver_max_file_descriptors"

    const/16 v3, 0x3c

    invoke-static {v1, v2, v3}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 129
    .local v0, "fileDescriptorsMax":I
    new-instance v1, Lcom/google/android/music/art/ArtFileDescriptorLruCache;

    invoke-direct {v1, v0}, Lcom/google/android/music/art/ArtFileDescriptorLruCache;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/music/art/ArtResolver2;->mFilesCache:Lcom/google/android/music/art/ArtFileDescriptorLruCache;

    .line 130
    iget-object v1, p0, Lcom/google/android/music/art/ArtResolver2;->mAppContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/music/art/ArtLoaderFactory;->getArtLoader(Landroid/content/Context;)Lcom/google/android/music/art/ArtLoader;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/art/ArtResolver2;->mArtLoader:Lcom/google/android/music/art/ArtLoader;

    .line 131
    invoke-direct {p0}, Lcom/google/android/music/art/ArtResolver2;->initHandlers()V

    .line 132
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/music/art/ArtResolver2;Lcom/google/android/music/art/ArtRequest2;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/art/ArtResolver2;
    .param p1, "x1"    # Lcom/google/android/music/art/ArtRequest2;

    .prologue
    .line 60
    invoke-direct {p0, p1}, Lcom/google/android/music/art/ArtResolver2;->processRequest(Lcom/google/android/music/art/ArtRequest2;)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/music/art/ArtResolver2;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/art/ArtResolver2;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/music/art/ArtResolver2;->mAppContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/music/art/ArtResolver2;Lcom/google/android/music/art/ArtResolver2$BitmapOriginInfo;)Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/art/ArtResolver2;
    .param p1, "x1"    # Lcom/google/android/music/art/ArtResolver2$BitmapOriginInfo;

    .prologue
    .line 60
    invoke-direct {p0, p1}, Lcom/google/android/music/art/ArtResolver2;->checkManagedBitmapMap(Lcom/google/android/music/art/ArtResolver2$BitmapOriginInfo;)Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/music/art/ArtResolver2;Lcom/google/android/music/art/ArtResolver2$BitmapOriginInfo;Landroid/graphics/Bitmap;)Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/art/ArtResolver2;
    .param p1, "x1"    # Lcom/google/android/music/art/ArtResolver2$BitmapOriginInfo;
    .param p2, "x2"    # Landroid/graphics/Bitmap;

    .prologue
    .line 60
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/art/ArtResolver2;->createManagedBitmap(Lcom/google/android/music/art/ArtResolver2$BitmapOriginInfo;Landroid/graphics/Bitmap;)Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/music/art/ArtResolver2;Lcom/google/android/music/art/ArtRequest2;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/art/ArtResolver2;
    .param p1, "x1"    # Lcom/google/android/music/art/ArtRequest2;

    .prologue
    .line 60
    invoke-direct {p0, p1}, Lcom/google/android/music/art/ArtResolver2;->notifyRequestListeners(Lcom/google/android/music/art/ArtRequest2;)V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/music/art/ArtResolver2;Ljava/lang/String;Z)Landroid/os/ParcelFileDescriptor;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/art/ArtResolver2;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Z

    .prologue
    .line 60
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/art/ArtResolver2;->loadArtUrl(Ljava/lang/String;Z)Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    return-object v0
.end method

.method private buildRequestAndHandler(Lcom/google/android/music/art/ArtDescriptor;Z)Landroid/util/Pair;
    .locals 9
    .param p1, "descriptor"    # Lcom/google/android/music/art/ArtDescriptor;
    .param p2, "isSync"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/music/art/ArtDescriptor;",
            "Z)",
            "Landroid/util/Pair",
            "<",
            "Lcom/google/android/music/art/ArtRequest2;",
            "Lcom/google/android/music/art/ArtTypeHandler;",
            ">;"
        }
    .end annotation

    .prologue
    .line 205
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    .line 206
    .local v2, "objClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/google/android/music/art/ArtDescriptor;>;"
    new-instance v1, Lcom/google/android/music/art/ArtResolver2$HandlerKey;

    iget-object v6, p1, Lcom/google/android/music/art/ArtDescriptor;->artType:Lcom/google/android/music/art/ArtType;

    const/4 v7, 0x0

    invoke-direct {v1, v2, v6, v7}, Lcom/google/android/music/art/ArtResolver2$HandlerKey;-><init>(Ljava/lang/Class;Lcom/google/android/music/art/ArtType;Lcom/google/android/music/art/ArtResolver2$1;)V

    .line 207
    .local v1, "key":Lcom/google/android/music/art/ArtResolver2$HandlerKey;
    iget-object v6, p0, Lcom/google/android/music/art/ArtResolver2;->mAvailableHandlers:Ljava/util/Map;

    invoke-interface {v6, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/art/ArtTypeHandler;

    .line 208
    .local v0, "handler":Lcom/google/android/music/art/ArtTypeHandler;
    if-nez v0, :cond_0

    .line 209
    const-string v6, "ArtResolver"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Unable to load art for object "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ". No handler available"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 211
    new-instance v6, Ljava/lang/IllegalArgumentException;

    const-string v7, "Unable to load art"

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 213
    :cond_0
    invoke-virtual {v0, p1}, Lcom/google/android/music/art/ArtTypeHandler;->getPostProcessor(Lcom/google/android/music/art/ArtDescriptor;)Lcom/google/android/music/art/ArtPostProcessor;

    move-result-object v3

    .line 214
    .local v3, "postProcessor":Lcom/google/android/music/art/ArtPostProcessor;
    new-instance v4, Lcom/google/android/music/art/ArtRequest2;

    invoke-direct {v4, v3, p1, p2}, Lcom/google/android/music/art/ArtRequest2;-><init>(Lcom/google/android/music/art/ArtPostProcessor;Lcom/google/android/music/art/ArtDescriptor;Z)V

    .line 215
    .local v4, "request":Lcom/google/android/music/art/ArtRequest2;
    new-instance v5, Landroid/util/Pair;

    invoke-direct {v5, v4, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 216
    .local v5, "result":Landroid/util/Pair;, "Landroid/util/Pair<Lcom/google/android/music/art/ArtRequest2;Lcom/google/android/music/art/ArtTypeHandler;>;"
    return-object v5
.end method

.method private checkManagedBitmapMap(Lcom/google/android/music/art/ArtResolver2$BitmapOriginInfo;)Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;
    .locals 5
    .param p1, "originInfo"    # Lcom/google/android/music/art/ArtResolver2$BitmapOriginInfo;

    .prologue
    .line 401
    iget-object v3, p0, Lcom/google/android/music/art/ArtResolver2;->mBitmapsLock:Ljava/lang/Object;

    monitor-enter v3

    .line 402
    :try_start_0
    iget-object v2, p0, Lcom/google/android/music/art/ArtResolver2;->mBitmaps:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/ref/WeakReference;

    .line 403
    .local v1, "reference":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;>;"
    if-nez v1, :cond_0

    const/4 v0, 0x0

    monitor-exit v3

    .line 411
    :goto_0
    return-object v0

    .line 404
    :cond_0
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;

    .line 405
    .local v0, "bitmap":Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;
    if-nez v0, :cond_2

    .line 406
    sget-boolean v2, Lcom/google/android/music/art/ArtResolver2;->LOGV:Z

    if-eqz v2, :cond_1

    .line 407
    const-string v2, "ArtResolver"

    const-string v4, "Managed bitmap map contains a disposed reference"

    invoke-static {v2, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 409
    :cond_1
    iget-object v2, p0, Lcom/google/android/music/art/ArtResolver2;->mBitmaps:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 411
    :cond_2
    monitor-exit v3

    goto :goto_0

    .line 412
    .end local v0    # "bitmap":Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;
    .end local v1    # "reference":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;>;"
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method private createManagedBitmap(Lcom/google/android/music/art/ArtResolver2$BitmapOriginInfo;Landroid/graphics/Bitmap;)Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;
    .locals 4
    .param p1, "originInfo"    # Lcom/google/android/music/art/ArtResolver2$BitmapOriginInfo;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 416
    iget-object v3, p0, Lcom/google/android/music/art/ArtResolver2;->mBitmapsLock:Ljava/lang/Object;

    monitor-enter v3

    .line 417
    :try_start_0
    new-instance v0, Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;

    invoke-direct {v0, p2, p1}, Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;-><init>(Landroid/graphics/Bitmap;Lcom/google/android/music/art/ArtResolver2$BitmapOriginInfo;)V

    .line 418
    .local v0, "managedBitmap":Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    .line 419
    .local v1, "reference":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;>;"
    iget-object v2, p0, Lcom/google/android/music/art/ArtResolver2;->mBitmaps:Ljava/util/Map;

    invoke-interface {v2, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 420
    monitor-exit v3

    return-object v0

    .line 421
    .end local v0    # "managedBitmap":Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;
    .end local v1    # "reference":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;>;"
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method private finalizeRequest(Lcom/google/android/music/art/ArtRequest2;)V
    .locals 2
    .param p1, "request"    # Lcom/google/android/music/art/ArtRequest2;

    .prologue
    .line 327
    invoke-virtual {p1}, Lcom/google/android/music/art/ArtRequest2;->finishedLoadingSuccessfully()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lcom/google/android/music/art/ArtRequest2;->isInFinalState()Z

    move-result v1

    if-nez v1, :cond_1

    .line 328
    new-instance v0, Lcom/google/android/music/art/ArtResolver2$LoadRenderTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lcom/google/android/music/art/ArtResolver2$LoadRenderTask;-><init>(Lcom/google/android/music/art/ArtResolver2;Lcom/google/android/music/art/ArtRequest2;Lcom/google/android/music/art/ArtResolver2$1;)V

    .line 329
    .local v0, "task":Lcom/google/android/music/art/ArtResolver2$LoadRenderTask;
    const/16 v1, 0x14

    invoke-virtual {p1, v1}, Lcom/google/android/music/art/ArtRequest2;->externalChangeState(I)V

    .line 330
    invoke-virtual {p1}, Lcom/google/android/music/art/ArtRequest2;->isSynchronous()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 331
    invoke-virtual {v0}, Lcom/google/android/music/art/ArtResolver2$LoadRenderTask;->run()V

    .line 340
    .end local v0    # "task":Lcom/google/android/music/art/ArtResolver2$LoadRenderTask;
    :goto_0
    return-void

    .line 335
    .restart local v0    # "task":Lcom/google/android/music/art/ArtResolver2$LoadRenderTask;
    :cond_0
    invoke-static {}, Lcom/google/android/music/art/ArtResolver;->getsBackgroundBitmapWorker()Lcom/google/android/music/utils/LoggableHandler;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/music/utils/LoggableHandler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 338
    .end local v0    # "task":Lcom/google/android/music/art/ArtResolver2$LoadRenderTask;
    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/music/art/ArtResolver2;->notifyRequestListeners(Lcom/google/android/music/art/ArtRequest2;)V

    goto :goto_0
.end method

.method public static getInstance()Lcom/google/android/music/art/ArtResolver2;
    .locals 1

    .prologue
    .line 151
    sget-object v0, Lcom/google/android/music/art/ArtResolver2;->sInstance:Lcom/google/android/music/art/ArtResolver2;

    return-object v0
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/google/android/music/art/ArtResolver2;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 135
    sget-object v1, Lcom/google/android/music/art/ArtResolver2;->sInstance:Lcom/google/android/music/art/ArtResolver2;

    if-nez v1, :cond_1

    .line 136
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 137
    .local v0, "appContext":Landroid/content/Context;
    sget-object v2, Lcom/google/android/music/art/ArtResolver2;->sInitializationLock:Ljava/lang/Object;

    monitor-enter v2

    .line 138
    :try_start_0
    sget-object v1, Lcom/google/android/music/art/ArtResolver2;->sInstance:Lcom/google/android/music/art/ArtResolver2;

    if-nez v1, :cond_0

    .line 139
    new-instance v1, Lcom/google/android/music/art/ArtResolver2;

    invoke-direct {v1, v0}, Lcom/google/android/music/art/ArtResolver2;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/google/android/music/art/ArtResolver2;->sInstance:Lcom/google/android/music/art/ArtResolver2;

    .line 141
    :cond_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 143
    .end local v0    # "appContext":Landroid/content/Context;
    :cond_1
    sget-object v1, Lcom/google/android/music/art/ArtResolver2;->sInstance:Lcom/google/android/music/art/ArtResolver2;

    return-object v1

    .line 141
    .restart local v0    # "appContext":Landroid/content/Context;
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method private initHandlers()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 68
    new-instance v1, Lcom/google/android/music/art/MainstageArtTypeHandler;

    iget-object v3, p0, Lcom/google/android/music/art/ArtResolver2;->mAppContext:Landroid/content/Context;

    invoke-direct {v1, p0, v3}, Lcom/google/android/music/art/MainstageArtTypeHandler;-><init>(Lcom/google/android/music/art/ArtResolver2;Landroid/content/Context;)V

    .line 69
    .local v1, "mainstageHandler":Lcom/google/android/music/art/MainstageArtTypeHandler;
    new-instance v0, Lcom/google/android/music/art/ArtResolver2$HandlerKey;

    const-class v3, Lcom/google/android/music/art/DocumentArtDescriptor;

    sget-object v4, Lcom/google/android/music/art/ArtType;->MAINSTAGE_CARD:Lcom/google/android/music/art/ArtType;

    invoke-direct {v0, v3, v4, v5}, Lcom/google/android/music/art/ArtResolver2$HandlerKey;-><init>(Ljava/lang/Class;Lcom/google/android/music/art/ArtType;Lcom/google/android/music/art/ArtResolver2$1;)V

    .line 70
    .local v0, "key":Lcom/google/android/music/art/ArtResolver2$HandlerKey;
    iget-object v3, p0, Lcom/google/android/music/art/ArtResolver2;->mAvailableHandlers:Ljava/util/Map;

    invoke-interface {v3, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    new-instance v2, Lcom/google/android/music/art/SingleUrlTypeHandler;

    iget-object v3, p0, Lcom/google/android/music/art/ArtResolver2;->mAppContext:Landroid/content/Context;

    invoke-direct {v2, p0, v3}, Lcom/google/android/music/art/SingleUrlTypeHandler;-><init>(Lcom/google/android/music/art/ArtResolver2;Landroid/content/Context;)V

    .line 72
    .local v2, "singleUrlTypeHandler":Lcom/google/android/music/art/SingleUrlTypeHandler;
    new-instance v0, Lcom/google/android/music/art/ArtResolver2$HandlerKey;

    .end local v0    # "key":Lcom/google/android/music/art/ArtResolver2$HandlerKey;
    const-class v3, Lcom/google/android/music/art/SingleUrlArtDescriptor;

    sget-object v4, Lcom/google/android/music/art/ArtType;->SONGZA_SITUATION:Lcom/google/android/music/art/ArtType;

    invoke-direct {v0, v3, v4, v5}, Lcom/google/android/music/art/ArtResolver2$HandlerKey;-><init>(Ljava/lang/Class;Lcom/google/android/music/art/ArtType;Lcom/google/android/music/art/ArtResolver2$1;)V

    .line 73
    .restart local v0    # "key":Lcom/google/android/music/art/ArtResolver2$HandlerKey;
    iget-object v3, p0, Lcom/google/android/music/art/ArtResolver2;->mAvailableHandlers:Ljava/util/Map;

    invoke-interface {v3, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    new-instance v0, Lcom/google/android/music/art/ArtResolver2$HandlerKey;

    .end local v0    # "key":Lcom/google/android/music/art/ArtResolver2$HandlerKey;
    const-class v3, Lcom/google/android/music/art/SingleUrlArtDescriptor;

    sget-object v4, Lcom/google/android/music/art/ArtType;->AVATAR:Lcom/google/android/music/art/ArtType;

    invoke-direct {v0, v3, v4, v5}, Lcom/google/android/music/art/ArtResolver2$HandlerKey;-><init>(Ljava/lang/Class;Lcom/google/android/music/art/ArtType;Lcom/google/android/music/art/ArtResolver2$1;)V

    .line 75
    .restart local v0    # "key":Lcom/google/android/music/art/ArtResolver2$HandlerKey;
    iget-object v3, p0, Lcom/google/android/music/art/ArtResolver2;->mAvailableHandlers:Ljava/util/Map;

    invoke-interface {v3, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    return-void
.end method

.method private loadArtUrl(Ljava/lang/String;Z)Landroid/os/ParcelFileDescriptor;
    .locals 5
    .param p1, "artUrl"    # Ljava/lang/String;
    .param p2, "loadIfMissing"    # Z

    .prologue
    .line 265
    iget-object v3, p0, Lcom/google/android/music/art/ArtResolver2;->mFilesCache:Lcom/google/android/music/art/ArtFileDescriptorLruCache;

    invoke-virtual {v3, p1}, Lcom/google/android/music/art/ArtFileDescriptorLruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/ParcelFileDescriptor;

    .line 268
    .local v1, "pfd":Landroid/os/ParcelFileDescriptor;
    if-nez v1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    move-object v2, v1

    .line 278
    .end local v1    # "pfd":Landroid/os/ParcelFileDescriptor;
    .local v2, "pfd":Landroid/os/ParcelFileDescriptor;
    :goto_0
    return-object v2

    .line 273
    .end local v2    # "pfd":Landroid/os/ParcelFileDescriptor;
    .restart local v1    # "pfd":Landroid/os/ParcelFileDescriptor;
    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/google/android/music/art/ArtResolver2;->mArtLoader:Lcom/google/android/music/art/ArtLoader;

    sget-object v4, Lcom/google/android/music/art/ArtLoader$DownloadMode;->SYNC_DOWNLOAD:Lcom/google/android/music/art/ArtLoader$DownloadMode;

    invoke-interface {v3, p1, v4}, Lcom/google/android/music/art/ArtLoader;->getArtFileDescriptorSync(Ljava/lang/String;Lcom/google/android/music/art/ArtLoader$DownloadMode;)Landroid/os/ParcelFileDescriptor;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_1
    move-object v2, v1

    .line 278
    .end local v1    # "pfd":Landroid/os/ParcelFileDescriptor;
    .restart local v2    # "pfd":Landroid/os/ParcelFileDescriptor;
    goto :goto_0

    .line 275
    .end local v2    # "pfd":Landroid/os/ParcelFileDescriptor;
    .restart local v1    # "pfd":Landroid/os/ParcelFileDescriptor;
    :catch_0
    move-exception v0

    .line 276
    .local v0, "e":Ljava/lang/InterruptedException;
    const-string v3, "ArtResolver"

    const-string v4, "Interrupted while getting art"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method private notifyRequestListeners(Lcom/google/android/music/art/ArtRequest2;)V
    .locals 4
    .param p1, "request"    # Lcom/google/android/music/art/ArtRequest2;

    .prologue
    .line 348
    invoke-virtual {p1}, Lcom/google/android/music/art/ArtRequest2;->isCancelled()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 376
    :cond_0
    :goto_0
    return-void

    .line 349
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/music/art/ArtRequest2;->retain()I

    .line 353
    invoke-virtual {p1}, Lcom/google/android/music/art/ArtRequest2;->getOriginatingHandler()Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/google/android/music/art/ArtResolver2$2;

    invoke-direct {v2, p0, p1}, Lcom/google/android/music/art/ArtResolver2$2;-><init>(Lcom/google/android/music/art/ArtResolver2;Lcom/google/android/music/art/ArtRequest2;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    move-result v0

    .line 372
    .local v0, "posted":Z
    if-nez v0, :cond_0

    .line 373
    const-string v1, "ArtResolver"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error posting callbacks to Handler for request="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 374
    invoke-virtual {p1}, Lcom/google/android/music/art/ArtRequest2;->release()I

    goto :goto_0
.end method

.method private processRequest(Lcom/google/android/music/art/ArtRequest2;)V
    .locals 7
    .param p1, "request"    # Lcom/google/android/music/art/ArtRequest2;

    .prologue
    .line 226
    sget-boolean v4, Lcom/google/android/music/art/ArtResolver2;->LOGV:Z

    if-eqz v4, :cond_0

    .line 227
    const-string v4, "ArtResolver"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "processRequest: request="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 229
    :cond_0
    const/16 v4, 0xc

    invoke-virtual {p1, v4}, Lcom/google/android/music/art/ArtRequest2;->externalChangeState(I)V

    .line 232
    iget-object v4, p0, Lcom/google/android/music/art/ArtResolver2;->mAppContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/google/android/music/utils/ViewUtils;->getShortestEdge(Landroid/content/Context;)I

    move-result v1

    .line 233
    .local v1, "edge":I
    invoke-virtual {p1}, Lcom/google/android/music/art/ArtRequest2;->getNeededItems()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/art/ArtResolver2$ArtUrl;

    .line 234
    .local v0, "artUrl":Lcom/google/android/music/art/ArtResolver2$ArtUrl;
    # getter for: Lcom/google/android/music/art/ArtResolver2$ArtUrl;->mUrl:Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/music/art/ArtResolver2$ArtUrl;->access$200(Lcom/google/android/music/art/ArtResolver2$ArtUrl;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    invoke-direct {p0, v4, v5}, Lcom/google/android/music/art/ArtResolver2;->loadArtUrl(Ljava/lang/String;Z)Landroid/os/ParcelFileDescriptor;

    move-result-object v3

    .line 235
    .local v3, "pfd":Landroid/os/ParcelFileDescriptor;
    if-eqz v3, :cond_3

    .line 236
    invoke-virtual {p1, v0}, Lcom/google/android/music/art/ArtRequest2;->notifyItemAvailable(Lcom/google/android/music/art/ArtResolver2$ArtUrl;)V

    .line 241
    :goto_0
    invoke-virtual {p1}, Lcom/google/android/music/art/ArtRequest2;->finishedLoadingSuccessfully()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 242
    invoke-direct {p0, p1}, Lcom/google/android/music/art/ArtResolver2;->finalizeRequest(Lcom/google/android/music/art/ArtRequest2;)V

    .line 246
    .end local v0    # "artUrl":Lcom/google/android/music/art/ArtResolver2$ArtUrl;
    .end local v3    # "pfd":Landroid/os/ParcelFileDescriptor;
    :cond_2
    return-void

    .line 238
    .restart local v0    # "artUrl":Lcom/google/android/music/art/ArtResolver2$ArtUrl;
    .restart local v3    # "pfd":Landroid/os/ParcelFileDescriptor;
    :cond_3
    # getter for: Lcom/google/android/music/art/ArtResolver2$ArtUrl;->mUrl:Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/music/art/ArtResolver2$ArtUrl;->access$200(Lcom/google/android/music/art/ArtResolver2$ArtUrl;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Lcom/google/android/music/art/ArtRequest2;->notifyItemUnavailable(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public getArt(Lcom/google/android/music/art/ArtDescriptor;Lcom/google/android/music/art/ArtResolver2$RequestListener;)Lcom/google/android/music/art/ArtRequest2;
    .locals 6
    .param p1, "descriptor"    # Lcom/google/android/music/art/ArtDescriptor;
    .param p2, "listener"    # Lcom/google/android/music/art/ArtResolver2$RequestListener;

    .prologue
    .line 162
    const/4 v3, 0x0

    invoke-direct {p0, p1, v3}, Lcom/google/android/music/art/ArtResolver2;->buildRequestAndHandler(Lcom/google/android/music/art/ArtDescriptor;Z)Landroid/util/Pair;

    move-result-object v2

    .line 164
    .local v2, "requestPair":Landroid/util/Pair;, "Landroid/util/Pair<Lcom/google/android/music/art/ArtRequest2;Lcom/google/android/music/art/ArtTypeHandler;>;"
    iget-object v1, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/music/art/ArtRequest2;

    .line 165
    .local v1, "request":Lcom/google/android/music/art/ArtRequest2;
    iget-object v0, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/music/art/ArtTypeHandler;

    .line 166
    .local v0, "handler":Lcom/google/android/music/art/ArtTypeHandler;
    sget-boolean v3, Lcom/google/android/music/art/ArtResolver2;->LOGV:Z

    if-eqz v3, :cond_0

    .line 167
    const-string v3, "ArtResolver"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ArtResolver2.getArt: resolved request for descriptor "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "to handler "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 170
    :cond_0
    invoke-virtual {v1, p2}, Lcom/google/android/music/art/ArtRequest2;->setListener(Lcom/google/android/music/art/ArtResolver2$RequestListener;)V

    .line 173
    invoke-static {}, Lcom/google/android/music/art/ArtResolver;->getBackgroundWorker()Lcom/google/android/music/utils/LoggableHandler;

    move-result-object v3

    new-instance v4, Lcom/google/android/music/art/ArtResolver2$1;

    invoke-direct {v4, p0, v0, v1}, Lcom/google/android/music/art/ArtResolver2$1;-><init>(Lcom/google/android/music/art/ArtResolver2;Lcom/google/android/music/art/ArtTypeHandler;Lcom/google/android/music/art/ArtRequest2;)V

    invoke-virtual {v3, v4}, Lcom/google/android/music/utils/LoggableHandler;->post(Ljava/lang/Runnable;)Z

    .line 181
    return-object v1
.end method

.method public getMutableBitmap(IFLandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    .locals 5
    .param p1, "width"    # I
    .param p2, "aspectRatio"    # F
    .param p3, "config"    # Landroid/graphics/Bitmap$Config;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 387
    if-lez p1, :cond_0

    move v1, v2

    :goto_0
    const-string v4, "width must not be 0"

    invoke-static {v1, v4}, Lcom/google/common/base/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    .line 390
    const/high16 v1, 0x3e800000    # 0.25f

    cmpl-float v1, p2, v1

    if-ltz v1, :cond_1

    const/high16 v1, 0x40800000    # 4.0f

    cmpg-float v1, p2, v1

    if-gtz v1, :cond_1

    :goto_1
    const-string v1, "aspect ratio must be between 4:1 and 1:4"

    invoke-static {v2, v1}, Lcom/google/common/base/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    .line 392
    int-to-float v1, p1

    mul-float/2addr v1, p2

    float-to-int v0, v1

    .line 393
    .local v0, "height":I
    invoke-static {p1, v0, p3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    return-object v1

    .end local v0    # "height":I
    :cond_0
    move v1, v3

    .line 387
    goto :goto_0

    :cond_1
    move v2, v3

    .line 390
    goto :goto_1
.end method

.method removeManagedBitmap(Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;)V
    .locals 3
    .param p1, "managedBitmap"    # Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;

    .prologue
    .line 430
    iget-object v1, p0, Lcom/google/android/music/art/ArtResolver2;->mBitmapsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 431
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/art/ArtResolver2;->mBitmaps:Ljava/util/Map;

    iget-object v2, p1, Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;->descriptor:Lcom/google/android/music/art/ArtResolver2$BitmapOriginInfo;

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 432
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 433
    iget-object v0, p1, Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;->bitmap:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/google/android/music/utils/AlbumArtUtils;->recycleBitmap(Landroid/graphics/Bitmap;)V

    .line 434
    sget-boolean v0, Lcom/google/android/music/art/ArtResolver2;->LOGV:Z

    if-eqz v0, :cond_0

    .line 435
    const-string v0, "ArtResolver"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "removeManagedBitmap: remaining bitmaps stored: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/art/ArtResolver2;->mBitmaps:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 437
    :cond_0
    return-void

    .line 432
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
