.class public abstract Lcom/google/android/music/ui/ExploreClusterListFragment;
.super Lcom/google/android/music/ui/BaseClusterListFragment;
.source "ExploreClusterListFragment.java"


# static fields
.field public static final GROUPS_COLUMNS:[Ljava/lang/String;


# instance fields
.field protected final mCardsContextMenuDelegate:Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;

.field private mGroupData:Landroid/database/Cursor;

.field private mRootUri:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 35
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "description"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "size"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "groupType"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/ui/ExploreClusterListFragment;->GROUPS_COLUMNS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/google/android/music/ui/BaseClusterListFragment;-><init>()V

    .line 51
    new-instance v0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$DocumentContextMenuDelegate;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$DocumentContextMenuDelegate;-><init>(Lcom/google/android/music/ui/MusicFragment;)V

    iput-object v0, p0, Lcom/google/android/music/ui/ExploreClusterListFragment;->mCardsContextMenuDelegate:Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;

    .line 64
    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;)V
    .locals 1
    .param p1, "rootUri"    # Landroid/net/Uri;

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/google/android/music/ui/BaseClusterListFragment;-><init>()V

    .line 51
    new-instance v0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$DocumentContextMenuDelegate;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$DocumentContextMenuDelegate;-><init>(Lcom/google/android/music/ui/MusicFragment;)V

    iput-object v0, p0, Lcom/google/android/music/ui/ExploreClusterListFragment;->mCardsContextMenuDelegate:Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;

    .line 55
    iput-object p1, p0, Lcom/google/android/music/ui/ExploreClusterListFragment;->mRootUri:Landroid/net/Uri;

    .line 56
    return-void
.end method

.method private moveToGroupOrThrow(I)V
    .locals 3
    .param p1, "clusterIndex"    # I

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/android/music/ui/ExploreClusterListFragment;->mGroupData:Landroid/database/Cursor;

    if-nez v0, :cond_0

    .line 143
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No group info"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 145
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/ui/ExploreClusterListFragment;->mGroupData:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 146
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid group requested. Index: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". Total: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/ExploreClusterListFragment;->mGroupData:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 149
    :cond_1
    return-void
.end method


# virtual methods
.method protected createCluster(ILandroid/database/Cursor;)Lcom/google/android/music/ui/Cluster;
    .locals 20
    .param p1, "clusterIndex"    # I
    .param p2, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 187
    invoke-direct/range {p0 .. p1}, Lcom/google/android/music/ui/ExploreClusterListFragment;->moveToGroupOrThrow(I)V

    .line 188
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/music/ui/ExploreClusterListFragment;->mGroupData:Landroid/database/Cursor;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 189
    .local v4, "title":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/music/ui/ExploreClusterListFragment;->mGroupData:Landroid/database/Cursor;

    const/4 v2, 0x4

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v15

    .line 191
    .local v15, "groupType":I
    sget-object v3, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    .line 194
    .local v3, "cardType":Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;
    const/4 v10, 0x0

    .line 195
    .local v10, "clickListener":Landroid/view/View$OnClickListener;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/ExploreClusterListFragment;->getScreenColumns()I

    move-result v8

    .line 196
    .local v8, "nbColumns":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/ExploreClusterListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d000f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v9

    .line 197
    .local v9, "nbRows":I
    const/4 v11, 0x0

    .line 198
    .local v11, "containerDescriptor":Lcom/google/android/music/store/ContainerDescriptor;
    packed-switch v15, :pswitch_data_0

    .line 251
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unexpected type value:"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 200
    :pswitch_0
    sget-object v3, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->CARD_ROW:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    .line 201
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/ExploreClusterListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d000d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v8

    .line 202
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/ExploreClusterListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0010

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v9

    .line 203
    invoke-static/range {p2 .. p2}, Lcom/google/android/music/ui/cardlib/model/ExploreDocumentClusterBuilder;->buildTrackDocumentList(Landroid/database/Cursor;)Ljava/util/ArrayList;

    move-result-object v6

    .line 204
    .local v6, "docList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/music/ui/cardlib/model/Document;>;"
    sget-object v7, Lcom/google/android/music/ui/cardlib/model/Document$Type;->TRACK:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    .line 206
    .local v7, "docType":Lcom/google/android/music/ui/cardlib/model/Document$Type;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/ExploreClusterListFragment;->getGenreId()Ljava/lang/String;

    move-result-object v13

    .line 207
    .local v13, "genreId":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/ExploreClusterListFragment;->getGenreName()Ljava/lang/String;

    move-result-object v14

    .line 209
    .local v14, "genreName":Ljava/lang/String;
    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 210
    invoke-static {v13, v14}, Lcom/google/android/music/store/ContainerDescriptor;->newTopSongsGenreDescriptor(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v11

    .line 215
    :goto_0
    new-instance v10, Lcom/google/android/music/ui/PlayTrackDocumentsClickListener;

    .end local v10    # "clickListener":Landroid/view/View$OnClickListener;
    invoke-direct {v10, v6, v11}, Lcom/google/android/music/ui/PlayTrackDocumentsClickListener;-><init>(Ljava/util/List;Lcom/google/android/music/store/ContainerDescriptor;)V

    .line 253
    .end local v13    # "genreId":Ljava/lang/String;
    .end local v14    # "genreName":Ljava/lang/String;
    .restart local v10    # "clickListener":Landroid/view/View$OnClickListener;
    :goto_1
    new-instance v1, Lcom/google/android/music/ui/Cluster;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/ExploreClusterListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const/4 v5, 0x0

    invoke-direct/range {v1 .. v11}, Lcom/google/android/music/ui/Cluster;-><init>(Landroid/app/Activity;Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Lcom/google/android/music/ui/cardlib/model/Document$Type;IILandroid/view/View$OnClickListener;Lcom/google/android/music/store/ContainerDescriptor;)V

    return-object v1

    .line 213
    .restart local v13    # "genreId":Ljava/lang/String;
    .restart local v14    # "genreName":Ljava/lang/String;
    :cond_0
    invoke-static {}, Lcom/google/android/music/store/ContainerDescriptor;->newTopSongsInAllAccessDescriptor()Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v11

    goto :goto_0

    .line 218
    .end local v6    # "docList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/music/ui/cardlib/model/Document;>;"
    .end local v7    # "docType":Lcom/google/android/music/ui/cardlib/model/Document$Type;
    .end local v13    # "genreId":Ljava/lang/String;
    .end local v14    # "genreName":Ljava/lang/String;
    :pswitch_1
    sget-object v3, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->CARD_SMALL_WRAPPED:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    .line 219
    invoke-static/range {p2 .. p2}, Lcom/google/android/music/ui/cardlib/model/ExploreDocumentClusterBuilder;->buildAlbumDocumentList(Landroid/database/Cursor;)Ljava/util/ArrayList;

    move-result-object v6

    .line 220
    .restart local v6    # "docList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/music/ui/cardlib/model/Document;>;"
    sget-object v7, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ALBUM:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    .line 221
    .restart local v7    # "docType":Lcom/google/android/music/ui/cardlib/model/Document$Type;
    goto :goto_1

    .line 223
    .end local v6    # "docList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/music/ui/cardlib/model/Document;>;"
    .end local v7    # "docType":Lcom/google/android/music/ui/cardlib/model/Document$Type;
    :pswitch_2
    sget-object v3, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->CARD_SMALL_2x1_WRAPPED:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    .line 224
    invoke-static/range {p2 .. p2}, Lcom/google/android/music/ui/cardlib/model/ExploreDocumentClusterBuilder;->buildArtistDocumentList(Landroid/database/Cursor;)Ljava/util/ArrayList;

    move-result-object v6

    .line 225
    .restart local v6    # "docList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/music/ui/cardlib/model/Document;>;"
    sget-object v7, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ARTIST:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    .line 226
    .restart local v7    # "docType":Lcom/google/android/music/ui/cardlib/model/Document$Type;
    goto :goto_1

    .line 228
    .end local v6    # "docList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/music/ui/cardlib/model/Document;>;"
    .end local v7    # "docType":Lcom/google/android/music/ui/cardlib/model/Document$Type;
    :pswitch_3
    sget-object v3, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->CARD_SMALL_WRAPPED:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    .line 229
    invoke-static/range {p2 .. p2}, Lcom/google/android/music/ui/cardlib/model/ExploreDocumentClusterBuilder;->buildPlaylistDocumentList(Landroid/database/Cursor;)Ljava/util/ArrayList;

    move-result-object v6

    .line 230
    .restart local v6    # "docList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/music/ui/cardlib/model/Document;>;"
    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v16

    .local v16, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/google/android/music/ui/cardlib/model/Document;

    .line 234
    .local v12, "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    invoke-virtual {v12}, Lcom/google/android/music/ui/cardlib/model/Document;->getPlaylistType()I

    move-result v17

    .line 235
    .local v17, "playlistType":I
    const/16 v1, 0x47

    move/from16 v0, v17

    if-eq v0, v1, :cond_1

    const/16 v1, 0x46

    move/from16 v0, v17

    if-ne v0, v1, :cond_2

    :cond_1
    invoke-virtual {v12}, Lcom/google/android/music/ui/cardlib/model/Document;->getPlaylistOwnerName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 238
    const v1, 0x7f0b0102

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v12}, Lcom/google/android/music/ui/cardlib/model/Document;->getPlaylistOwnerName()Ljava/lang/String;

    move-result-object v19

    aput-object v19, v2, v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/ui/ExploreClusterListFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    .line 244
    .local v18, "subtitle":Ljava/lang/String;
    :goto_3
    move-object/from16 v0, v18

    invoke-virtual {v12, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->setSubTitle(Ljava/lang/String;)V

    goto :goto_2

    .line 241
    .end local v18    # "subtitle":Ljava/lang/String;
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/ExploreClusterListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/ExploreClusterListFragment;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v2

    move/from16 v0, v17

    invoke-static {v1, v2, v0}, Lcom/google/android/music/utils/LabelUtils;->getPlaylistSecondaryLabel(Landroid/content/Context;Lcom/google/android/music/preferences/MusicPreferences;I)Ljava/lang/String;

    move-result-object v18

    .restart local v18    # "subtitle":Ljava/lang/String;
    goto :goto_3

    .line 246
    .end local v12    # "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    .end local v17    # "playlistType":I
    .end local v18    # "subtitle":Ljava/lang/String;
    :cond_3
    sget-object v7, Lcom/google/android/music/ui/cardlib/model/Document$Type;->PLAYLIST:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    .line 247
    .restart local v7    # "docType":Lcom/google/android/music/ui/cardlib/model/Document$Type;
    goto :goto_1

    .line 198
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected getCardsContextMenuDelegate()Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/music/ui/ExploreClusterListFragment;->mCardsContextMenuDelegate:Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;

    return-object v0
.end method

.method protected getClusterContentUri(I)Landroid/net/Uri;
    .locals 4
    .param p1, "clusterIndex"    # I

    .prologue
    .line 155
    invoke-direct {p0, p1}, Lcom/google/android/music/ui/ExploreClusterListFragment;->moveToGroupOrThrow(I)V

    .line 157
    iget-object v2, p0, Lcom/google/android/music/ui/ExploreClusterListFragment;->mGroupData:Landroid/database/Cursor;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 158
    .local v0, "groupId":J
    invoke-virtual {p0, v0, v1}, Lcom/google/android/music/ui/ExploreClusterListFragment;->getGroupQueryUri(J)Landroid/net/Uri;

    move-result-object v2

    return-object v2
.end method

.method protected getClusterProjection(I)[Ljava/lang/String;
    .locals 3
    .param p1, "clusterIndex"    # I

    .prologue
    .line 165
    invoke-direct {p0, p1}, Lcom/google/android/music/ui/ExploreClusterListFragment;->moveToGroupOrThrow(I)V

    .line 166
    iget-object v1, p0, Lcom/google/android/music/ui/ExploreClusterListFragment;->mGroupData:Landroid/database/Cursor;

    const/4 v2, 0x4

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 168
    .local v0, "type":I
    packed-switch v0, :pswitch_data_0

    .line 180
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Unexpected type value"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 170
    :pswitch_0
    sget-object v1, Lcom/google/android/music/ui/cardlib/model/ExploreDocumentClusterBuilder;->SONG_COLUMNS:[Ljava/lang/String;

    .line 176
    :goto_0
    return-object v1

    .line 172
    :pswitch_1
    sget-object v1, Lcom/google/android/music/ui/cardlib/model/ExploreDocumentClusterBuilder;->ALBUM_COLUMNS:[Ljava/lang/String;

    goto :goto_0

    .line 174
    :pswitch_2
    sget-object v1, Lcom/google/android/music/ui/cardlib/model/ExploreDocumentClusterBuilder;->ARTIST_COLUMNS:[Ljava/lang/String;

    goto :goto_0

    .line 176
    :pswitch_3
    sget-object v1, Lcom/google/android/music/ui/cardlib/model/ExploreDocumentClusterBuilder;->SHARED_WITH_ME_PLAYLIST_COLUMNS:[Ljava/lang/String;

    goto :goto_0

    .line 168
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected getGenreId()Ljava/lang/String;
    .locals 4

    .prologue
    .line 258
    invoke-virtual {p0}, Lcom/google/android/music/ui/ExploreClusterListFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 259
    .local v0, "bundle":Landroid/os/Bundle;
    if-nez v0, :cond_0

    .line 260
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Either genre level, genre ID or genre name is required."

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 263
    :cond_0
    const-string v2, "nautilusId"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 264
    .local v1, "genreId":Ljava/lang/String;
    return-object v1
.end method

.method protected getGenreName()Ljava/lang/String;
    .locals 4

    .prologue
    .line 268
    invoke-virtual {p0}, Lcom/google/android/music/ui/ExploreClusterListFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 269
    .local v0, "bundle":Landroid/os/Bundle;
    if-nez v0, :cond_0

    .line 270
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Either genre level, genre ID or genre name is required."

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 273
    :cond_0
    const-string v2, "name"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 274
    .local v1, "genreId":Ljava/lang/String;
    return-object v1
.end method

.method protected abstract getGroupQueryUri(J)Landroid/net/Uri;
.end method

.method protected getNumberOfClusters()I
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/google/android/music/ui/ExploreClusterListFragment;->mGroupData:Landroid/database/Cursor;

    if-nez v0, :cond_0

    .line 135
    const/4 v0, 0x0

    .line 137
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/music/ui/ExploreClusterListFragment;->mGroupData:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    goto :goto_0
.end method

.method public init(Landroid/net/Uri;)V
    .locals 0
    .param p1, "rootUri"    # Landroid/net/Uri;

    .prologue
    .line 67
    iput-object p1, p0, Lcom/google/android/music/ui/ExploreClusterListFragment;->mRootUri:Landroid/net/Uri;

    .line 68
    return-void
.end method

.method protected initEmptyScreen()V
    .locals 1

    .prologue
    .line 91
    invoke-super {p0}, Lcom/google/android/music/ui/BaseClusterListFragment;->initEmptyScreen()V

    .line 92
    const v0, 0x7f0b02a6

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/ExploreClusterListFragment;->setEmptyScreenText(I)V

    .line 93
    const v0, 0x7f0200f2

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/ExploreClusterListFragment;->setEmptyImageView(I)V

    .line 94
    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 80
    invoke-super {p0, p1}, Lcom/google/android/music/ui/BaseClusterListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 82
    iget-object v0, p0, Lcom/google/android/music/ui/ExploreClusterListFragment;->mRootUri:Landroid/net/Uri;

    if-nez v0, :cond_0

    .line 83
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Search string is not initialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 86
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/music/ui/ExploreClusterListFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/16 v1, 0x3e8

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 87
    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 7
    .param p1, "id"    # I
    .param p2, "args"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 99
    const/16 v0, 0x3e8

    if-eq p1, v0, :cond_0

    .line 100
    invoke-super {p0, p1, p2}, Lcom/google/android/music/ui/BaseClusterListFragment;->onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;

    move-result-object v0

    .line 102
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Landroid/support/v4/content/CursorLoader;

    invoke-virtual {p0}, Lcom/google/android/music/ui/ExploreClusterListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/ExploreClusterListFragment;->mRootUri:Landroid/net/Uri;

    sget-object v3, Lcom/google/android/music/ui/ExploreClusterListFragment;->GROUPS_COLUMNS:[Ljava/lang/String;

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 2
    .param p2, "data"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 111
    .local p1, "loader":Landroid/support/v4/content/Loader;, "Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    const/16 v1, 0x3e8

    if-eq v0, v1, :cond_0

    .line 112
    invoke-super {p0, p1, p2}, Lcom/google/android/music/ui/BaseClusterListFragment;->onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    .line 117
    :goto_0
    return-void

    .line 114
    :cond_0
    iput-object p2, p0, Lcom/google/android/music/ui/ExploreClusterListFragment;->mGroupData:Landroid/database/Cursor;

    .line 115
    invoke-virtual {p0}, Lcom/google/android/music/ui/ExploreClusterListFragment;->startLoading()V

    goto :goto_0
.end method

.method public bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Landroid/support/v4/content/Loader;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 28
    check-cast p2, Landroid/database/Cursor;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/music/ui/ExploreClusterListFragment;->onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 123
    .local p1, "loader":Landroid/support/v4/content/Loader;, "Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    const-string v0, "ExploreClusterListFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onLoaderReset is called for loader "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 124
    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    const/16 v1, 0x3e8

    if-eq v0, v1, :cond_0

    .line 125
    invoke-super {p0, p1}, Lcom/google/android/music/ui/BaseClusterListFragment;->onLoaderReset(Landroid/support/v4/content/Loader;)V

    .line 129
    :goto_0
    return-void

    .line 127
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/ui/ExploreClusterListFragment;->mGroupData:Landroid/database/Cursor;

    goto :goto_0
.end method
