.class public abstract Lcom/google/android/music/store/IStoreService$Stub;
.super Landroid/os/Binder;
.source "IStoreService.java"

# interfaces
.implements Lcom/google/android/music/store/IStoreService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/store/IStoreService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/store/IStoreService$Stub$Proxy;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 15
    const-string v0, "com.google.android.music.store.IStoreService"

    invoke-virtual {p0, p0, v0}, Lcom/google/android/music/store/IStoreService$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 16
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/google/android/music/store/IStoreService;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 23
    if-nez p0, :cond_0

    .line 24
    const/4 v0, 0x0

    .line 30
    :goto_0
    return-object v0

    .line 26
    :cond_0
    const-string v1, "com.google.android.music.store.IStoreService"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 27
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/google/android/music/store/IStoreService;

    if-eqz v1, :cond_1

    .line 28
    check-cast v0, Lcom/google/android/music/store/IStoreService;

    goto :goto_0

    .line 30
    :cond_1
    new-instance v0, Lcom/google/android/music/store/IStoreService$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/google/android/music/store/IStoreService$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 34
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 7
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x1

    .line 38
    sparse-switch p1, :sswitch_data_0

    .line 145
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v5

    :goto_0
    return v5

    .line 42
    :sswitch_0
    const-string v4, "com.google.android.music.store.IStoreService"

    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 47
    :sswitch_1
    const-string v4, "com.google.android.music.store.IStoreService"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 49
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    .line 50
    .local v0, "_arg0":J
    invoke-virtual {p0, v0, v1}, Lcom/google/android/music/store/IStoreService$Stub;->getSizeAlbum(J)J

    move-result-wide v2

    .line 51
    .local v2, "_result":J
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 52
    invoke-virtual {p3, v2, v3}, Landroid/os/Parcel;->writeLong(J)V

    goto :goto_0

    .line 57
    .end local v0    # "_arg0":J
    .end local v2    # "_result":J
    :sswitch_2
    const-string v4, "com.google.android.music.store.IStoreService"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 59
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    .line 60
    .restart local v0    # "_arg0":J
    invoke-virtual {p0, v0, v1}, Lcom/google/android/music/store/IStoreService$Stub;->getSizePlaylist(J)J

    move-result-wide v2

    .line 61
    .restart local v2    # "_result":J
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 62
    invoke-virtual {p3, v2, v3}, Landroid/os/Parcel;->writeLong(J)V

    goto :goto_0

    .line 67
    .end local v0    # "_arg0":J
    .end local v2    # "_result":J
    :sswitch_3
    const-string v4, "com.google.android.music.store.IStoreService"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 69
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    .line 70
    .restart local v0    # "_arg0":J
    invoke-virtual {p0, v0, v1}, Lcom/google/android/music/store/IStoreService$Stub;->getSizeAutoPlaylist(J)J

    move-result-wide v2

    .line 71
    .restart local v2    # "_result":J
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 72
    invoke-virtual {p3, v2, v3}, Landroid/os/Parcel;->writeLong(J)V

    goto :goto_0

    .line 77
    .end local v0    # "_arg0":J
    .end local v2    # "_result":J
    :sswitch_4
    const-string v4, "com.google.android.music.store.IStoreService"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 79
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    .line 80
    .restart local v0    # "_arg0":J
    invoke-virtual {p0, v0, v1}, Lcom/google/android/music/store/IStoreService$Stub;->getSizeKeeponRadioStation(J)J

    move-result-wide v2

    .line 81
    .restart local v2    # "_result":J
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 82
    invoke-virtual {p3, v2, v3}, Landroid/os/Parcel;->writeLong(J)V

    goto :goto_0

    .line 87
    .end local v0    # "_arg0":J
    .end local v2    # "_result":J
    :sswitch_5
    const-string v4, "com.google.android.music.store.IStoreService"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 89
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    .line 90
    .restart local v0    # "_arg0":J
    invoke-virtual {p0, v0, v1}, Lcom/google/android/music/store/IStoreService$Stub;->getArtistIdsForAlbum(J)[J

    move-result-object v2

    .line 91
    .local v2, "_result":[J
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 92
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeLongArray([J)V

    goto :goto_0

    .line 97
    .end local v0    # "_arg0":J
    .end local v2    # "_result":[J
    :sswitch_6
    const-string v6, "com.google.android.music.store.IStoreService"

    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 99
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    .line 100
    .restart local v0    # "_arg0":J
    invoke-virtual {p0, v0, v1}, Lcom/google/android/music/store/IStoreService$Stub;->isAlbumSelectedAsKeepOn(J)Z

    move-result v2

    .line 101
    .local v2, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 102
    if-eqz v2, :cond_0

    move v4, v5

    :cond_0
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 107
    .end local v0    # "_arg0":J
    .end local v2    # "_result":Z
    :sswitch_7
    const-string v6, "com.google.android.music.store.IStoreService"

    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 109
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    .line 110
    .restart local v0    # "_arg0":J
    invoke-virtual {p0, v0, v1}, Lcom/google/android/music/store/IStoreService$Stub;->isPlaylistSelectedAsKeepOn(J)Z

    move-result v2

    .line 111
    .restart local v2    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 112
    if-eqz v2, :cond_1

    move v4, v5

    :cond_1
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 117
    .end local v0    # "_arg0":J
    .end local v2    # "_result":Z
    :sswitch_8
    const-string v6, "com.google.android.music.store.IStoreService"

    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 119
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    .line 120
    .restart local v0    # "_arg0":J
    invoke-virtual {p0, v0, v1}, Lcom/google/android/music/store/IStoreService$Stub;->isAutoPlaylistSelectedAsKeepOn(J)Z

    move-result v2

    .line 121
    .restart local v2    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 122
    if-eqz v2, :cond_2

    move v4, v5

    :cond_2
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 127
    .end local v0    # "_arg0":J
    .end local v2    # "_result":Z
    :sswitch_9
    const-string v6, "com.google.android.music.store.IStoreService"

    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 129
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    .line 130
    .restart local v0    # "_arg0":J
    invoke-virtual {p0, v0, v1}, Lcom/google/android/music/store/IStoreService$Stub;->isRadioStationSelectedAsKeepOn(J)Z

    move-result v2

    .line 131
    .restart local v2    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 132
    if-eqz v2, :cond_3

    move v4, v5

    :cond_3
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 137
    .end local v0    # "_arg0":J
    .end local v2    # "_result":Z
    :sswitch_a
    const-string v4, "com.google.android.music.store.IStoreService"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 139
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 140
    .local v0, "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/google/android/music/store/IStoreService$Stub;->insertLockerRecommendations(Ljava/lang/String;)V

    .line 141
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 38
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
