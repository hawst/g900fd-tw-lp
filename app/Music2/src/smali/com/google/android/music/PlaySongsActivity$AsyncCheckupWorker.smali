.class Lcom/google/android/music/PlaySongsActivity$AsyncCheckupWorker;
.super Lcom/google/android/music/utils/LoggableHandler;
.source "PlaySongsActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/PlaySongsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AsyncCheckupWorker"
.end annotation


# instance fields
.field private mFinished:Z

.field private mFoundLocalId:J

.field private mFoundSingleSong:Z

.field private mTitle:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/music/PlaySongsActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/music/PlaySongsActivity;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 226
    iput-object p1, p0, Lcom/google/android/music/PlaySongsActivity$AsyncCheckupWorker;->this$0:Lcom/google/android/music/PlaySongsActivity;

    .line 227
    const-class v0, Lcom/google/android/music/PlaySongsActivity$AsyncCheckupWorker;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/music/utils/LoggableHandler;-><init>(Ljava/lang/String;)V

    .line 221
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/music/PlaySongsActivity$AsyncCheckupWorker;->mFoundLocalId:J

    .line 222
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/PlaySongsActivity$AsyncCheckupWorker;->mTitle:Ljava/lang/String;

    .line 223
    iput-boolean v2, p0, Lcom/google/android/music/PlaySongsActivity$AsyncCheckupWorker;->mFoundSingleSong:Z

    .line 224
    iput-boolean v2, p0, Lcom/google/android/music/PlaySongsActivity$AsyncCheckupWorker;->mFinished:Z

    .line 228
    return-void
.end method

.method private checkIfExists()Z
    .locals 9

    .prologue
    const/4 v3, 0x0

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 266
    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v8

    const-string v0, "title"

    aput-object v0, v2, v7

    .line 267
    .local v2, "queryCols":[Ljava/lang/String;
    const-wide/16 v4, -0x3

    iget-object v0, p0, Lcom/google/android/music/PlaySongsActivity$AsyncCheckupWorker;->this$0:Lcom/google/android/music/PlaySongsActivity;

    # getter for: Lcom/google/android/music/PlaySongsActivity;->mStoreId:Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/music/PlaySongsActivity;->access$400(Lcom/google/android/music/PlaySongsActivity;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v5, v0}, Lcom/google/android/music/store/MusicContent$AutoPlaylists$Members;->getAutoPlaylistItemUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 269
    .local v1, "uri":Landroid/net/Uri;
    iget-object v0, p0, Lcom/google/android/music/PlaySongsActivity$AsyncCheckupWorker;->this$0:Lcom/google/android/music/PlaySongsActivity;

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 271
    .local v6, "c":Landroid/database/Cursor;
    if-eqz v6, :cond_1

    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 272
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/music/PlaySongsActivity$AsyncCheckupWorker;->mFoundLocalId:J

    .line 273
    array-length v0, v2

    if-le v0, v7, :cond_0

    .line 274
    const/4 v0, 0x1

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/PlaySongsActivity$AsyncCheckupWorker;->mTitle:Ljava/lang/String;

    .line 276
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/PlaySongsActivity$AsyncCheckupWorker;->mFoundSingleSong:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 280
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    move v0, v7

    .line 297
    :goto_0
    return v0

    .line 280
    :cond_1
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 284
    new-array v2, v7, [Ljava/lang/String;

    .end local v2    # "queryCols":[Ljava/lang/String;
    const-string v0, "album_id"

    aput-object v0, v2, v8

    .line 285
    .restart local v2    # "queryCols":[Ljava/lang/String;
    iget-object v0, p0, Lcom/google/android/music/PlaySongsActivity$AsyncCheckupWorker;->this$0:Lcom/google/android/music/PlaySongsActivity;

    # getter for: Lcom/google/android/music/PlaySongsActivity;->mStoreId:Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/music/PlaySongsActivity;->access$400(Lcom/google/android/music/PlaySongsActivity;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/music/store/MusicContent$Albums;->getStoreAlbumUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 286
    iget-object v0, p0, Lcom/google/android/music/PlaySongsActivity$AsyncCheckupWorker;->this$0:Lcom/google/android/music/PlaySongsActivity;

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 288
    if-eqz v6, :cond_2

    :try_start_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 289
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/music/PlaySongsActivity$AsyncCheckupWorker;->mFoundLocalId:J

    .line 290
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/PlaySongsActivity$AsyncCheckupWorker;->mFoundSingleSong:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 294
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    move v0, v7

    goto :goto_0

    .line 280
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0

    .line 294
    :cond_2
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    move v0, v8

    .line 297
    goto :goto_0

    .line 294
    :catchall_1
    move-exception v0

    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method private playSonglistAsync(Lcom/google/android/music/medialist/SongList;)V
    .locals 2
    .param p1, "songList"    # Lcom/google/android/music/medialist/SongList;

    .prologue
    .line 351
    iget-object v0, p0, Lcom/google/android/music/PlaySongsActivity$AsyncCheckupWorker;->this$0:Lcom/google/android/music/PlaySongsActivity;

    new-instance v1, Lcom/google/android/music/PlaySongsActivity$AsyncCheckupWorker$2;

    invoke-direct {v1, p0, p1}, Lcom/google/android/music/PlaySongsActivity$AsyncCheckupWorker$2;-><init>(Lcom/google/android/music/PlaySongsActivity$AsyncCheckupWorker;Lcom/google/android/music/medialist/SongList;)V

    invoke-virtual {v0, v1}, Lcom/google/android/music/PlaySongsActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 358
    return-void
.end method

.method private postFailure()V
    .locals 1

    .prologue
    .line 307
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/google/android/music/PlaySongsActivity$AsyncCheckupWorker;->sendEmptyMessage(I)Z

    .line 308
    return-void
.end method

.method private postSuccess()V
    .locals 2

    .prologue
    .line 301
    invoke-virtual {p0}, Lcom/google/android/music/PlaySongsActivity$AsyncCheckupWorker;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 302
    .local v0, "msg":Landroid/os/Message;
    const/4 v1, 0x4

    iput v1, v0, Landroid/os/Message;->what:I

    .line 303
    invoke-virtual {p0, v0}, Lcom/google/android/music/PlaySongsActivity$AsyncCheckupWorker;->sendMessage(Landroid/os/Message;)Z

    .line 304
    return-void
.end method

.method private processNautilusSuccess()V
    .locals 4

    .prologue
    .line 327
    iget-boolean v2, p0, Lcom/google/android/music/PlaySongsActivity$AsyncCheckupWorker;->mFinished:Z

    if-eqz v2, :cond_0

    .line 348
    :goto_0
    return-void

    .line 330
    :cond_0
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/music/PlaySongsActivity$AsyncCheckupWorker;->mFinished:Z

    .line 331
    const/4 v1, 0x0

    .line 332
    .local v1, "songList":Lcom/google/android/music/medialist/SongList;
    iget-object v2, p0, Lcom/google/android/music/PlaySongsActivity$AsyncCheckupWorker;->this$0:Lcom/google/android/music/PlaySongsActivity;

    # getter for: Lcom/google/android/music/PlaySongsActivity;->mStoreId:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/music/PlaySongsActivity;->access$400(Lcom/google/android/music/PlaySongsActivity;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "T"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 333
    iget-object v2, p0, Lcom/google/android/music/PlaySongsActivity$AsyncCheckupWorker;->this$0:Lcom/google/android/music/PlaySongsActivity;

    # getter for: Lcom/google/android/music/PlaySongsActivity;->mStoreId:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/music/PlaySongsActivity;->access$400(Lcom/google/android/music/PlaySongsActivity;)Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    invoke-static {v2, v3}, Lcom/google/android/music/store/ContainerDescriptor;->newNautilusSingleSongDescriptor(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v0

    .line 336
    .local v0, "descriptor":Lcom/google/android/music/store/ContainerDescriptor;
    new-instance v1, Lcom/google/android/music/medialist/NautilusSingleSongList;

    .end local v1    # "songList":Lcom/google/android/music/medialist/SongList;
    iget-object v2, p0, Lcom/google/android/music/PlaySongsActivity$AsyncCheckupWorker;->this$0:Lcom/google/android/music/PlaySongsActivity;

    # getter for: Lcom/google/android/music/PlaySongsActivity;->mStoreId:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/music/PlaySongsActivity;->access$400(Lcom/google/android/music/PlaySongsActivity;)Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    invoke-direct {v1, v0, v2, v3}, Lcom/google/android/music/medialist/NautilusSingleSongList;-><init>(Lcom/google/android/music/store/ContainerDescriptor;Ljava/lang/String;Ljava/lang/String;)V

    .line 341
    .end local v0    # "descriptor":Lcom/google/android/music/store/ContainerDescriptor;
    .restart local v1    # "songList":Lcom/google/android/music/medialist/SongList;
    :cond_1
    :goto_1
    if-nez v1, :cond_3

    .line 342
    const/4 v2, 0x5

    invoke-virtual {p0, v2}, Lcom/google/android/music/PlaySongsActivity$AsyncCheckupWorker;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 337
    :cond_2
    iget-object v2, p0, Lcom/google/android/music/PlaySongsActivity$AsyncCheckupWorker;->this$0:Lcom/google/android/music/PlaySongsActivity;

    # getter for: Lcom/google/android/music/PlaySongsActivity;->mStoreId:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/music/PlaySongsActivity;->access$400(Lcom/google/android/music/PlaySongsActivity;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "B"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 338
    new-instance v1, Lcom/google/android/music/medialist/NautilusAlbumSongList;

    .end local v1    # "songList":Lcom/google/android/music/medialist/SongList;
    iget-object v2, p0, Lcom/google/android/music/PlaySongsActivity$AsyncCheckupWorker;->this$0:Lcom/google/android/music/PlaySongsActivity;

    # getter for: Lcom/google/android/music/PlaySongsActivity;->mStoreId:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/music/PlaySongsActivity;->access$400(Lcom/google/android/music/PlaySongsActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/music/medialist/NautilusAlbumSongList;-><init>(Ljava/lang/String;)V

    .restart local v1    # "songList":Lcom/google/android/music/medialist/SongList;
    goto :goto_1

    .line 347
    :cond_3
    invoke-direct {p0, v1}, Lcom/google/android/music/PlaySongsActivity$AsyncCheckupWorker;->playSonglistAsync(Lcom/google/android/music/medialist/SongList;)V

    goto :goto_0
.end method

.method private processSuccess()V
    .locals 5

    .prologue
    .line 311
    iget-boolean v2, p0, Lcom/google/android/music/PlaySongsActivity$AsyncCheckupWorker;->mFinished:Z

    if-eqz v2, :cond_0

    .line 324
    :goto_0
    return-void

    .line 314
    :cond_0
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/music/PlaySongsActivity$AsyncCheckupWorker;->mFinished:Z

    .line 316
    iget-boolean v2, p0, Lcom/google/android/music/PlaySongsActivity$AsyncCheckupWorker;->mFoundSingleSong:Z

    if-eqz v2, :cond_1

    .line 317
    iget-wide v2, p0, Lcom/google/android/music/PlaySongsActivity$AsyncCheckupWorker;->mFoundLocalId:J

    iget-object v4, p0, Lcom/google/android/music/PlaySongsActivity$AsyncCheckupWorker;->mTitle:Ljava/lang/String;

    invoke-static {v2, v3, v4}, Lcom/google/android/music/store/ContainerDescriptor;->newSingleSongDescriptor(JLjava/lang/String;)Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v0

    .line 319
    .local v0, "descriptor":Lcom/google/android/music/store/ContainerDescriptor;
    new-instance v1, Lcom/google/android/music/medialist/SingleSongList;

    iget-wide v2, p0, Lcom/google/android/music/PlaySongsActivity$AsyncCheckupWorker;->mFoundLocalId:J

    iget-object v4, p0, Lcom/google/android/music/PlaySongsActivity$AsyncCheckupWorker;->mTitle:Ljava/lang/String;

    invoke-direct {v1, v0, v2, v3, v4}, Lcom/google/android/music/medialist/SingleSongList;-><init>(Lcom/google/android/music/store/ContainerDescriptor;JLjava/lang/String;)V

    .line 323
    .end local v0    # "descriptor":Lcom/google/android/music/store/ContainerDescriptor;
    .local v1, "songlist":Lcom/google/android/music/medialist/SongList;
    :goto_1
    invoke-direct {p0, v1}, Lcom/google/android/music/PlaySongsActivity$AsyncCheckupWorker;->playSonglistAsync(Lcom/google/android/music/medialist/SongList;)V

    goto :goto_0

    .line 321
    .end local v1    # "songlist":Lcom/google/android/music/medialist/SongList;
    :cond_1
    new-instance v1, Lcom/google/android/music/medialist/AlbumSongList;

    iget-wide v2, p0, Lcom/google/android/music/PlaySongsActivity$AsyncCheckupWorker;->mFoundLocalId:J

    const/4 v4, 0x0

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/music/medialist/AlbumSongList;-><init>(JZ)V

    .restart local v1    # "songlist":Lcom/google/android/music/medialist/SongList;
    goto :goto_1
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v3, 0x3

    .line 232
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 258
    :cond_0
    :goto_0
    return-void

    .line 236
    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/music/PlaySongsActivity$AsyncCheckupWorker;->checkIfExists()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 237
    iget v1, p1, Landroid/os/Message;->what:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    const-string v0, "initial_check"

    .line 239
    .local v0, "trackerLabel":Ljava/lang/String;
    :goto_1
    invoke-direct {p0}, Lcom/google/android/music/PlaySongsActivity$AsyncCheckupWorker;->postSuccess()V

    goto :goto_0

    .line 237
    .end local v0    # "trackerLabel":Ljava/lang/String;
    :cond_1
    iget v1, p1, Landroid/os/Message;->what:I

    if-ne v1, v3, :cond_2

    const-string v0, "post_sync"

    goto :goto_1

    :cond_2
    const-string v0, "uri_update"

    goto :goto_1

    .line 240
    :cond_3
    iget v1, p1, Landroid/os/Message;->what:I

    if-ne v1, v3, :cond_0

    .line 241
    invoke-direct {p0}, Lcom/google/android/music/PlaySongsActivity$AsyncCheckupWorker;->postFailure()V

    goto :goto_0

    .line 245
    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/music/PlaySongsActivity$AsyncCheckupWorker;->processSuccess()V

    goto :goto_0

    .line 248
    :pswitch_2
    invoke-direct {p0}, Lcom/google/android/music/PlaySongsActivity$AsyncCheckupWorker;->processNautilusSuccess()V

    goto :goto_0

    .line 251
    :pswitch_3
    iget-object v1, p0, Lcom/google/android/music/PlaySongsActivity$AsyncCheckupWorker;->this$0:Lcom/google/android/music/PlaySongsActivity;

    new-instance v2, Lcom/google/android/music/PlaySongsActivity$AsyncCheckupWorker$1;

    invoke-direct {v2, p0}, Lcom/google/android/music/PlaySongsActivity$AsyncCheckupWorker$1;-><init>(Lcom/google/android/music/PlaySongsActivity$AsyncCheckupWorker;)V

    invoke-virtual {v1, v2}, Lcom/google/android/music/PlaySongsActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 232
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method
