.class public Lcom/google/android/music/utils/TypefaceUtil;
.super Ljava/lang/Object;
.source "TypefaceUtil.java"


# static fields
.field private static sIsRobotoBoldLoaded:Z

.field private static sIsRobotoCondensedItalicLoaded:Z

.field private static sIsRobotoLightLoaded:Z

.field private static sIsRobotoMediumLoaded:Z

.field private static sIsRobotoRegularLoaded:Z

.field private static sRobotoBold:Landroid/graphics/Typeface;

.field private static sRobotoCondensedItalic:Landroid/graphics/Typeface;

.field private static sRobotoLight:Landroid/graphics/Typeface;

.field private static sRobotoMedium:Landroid/graphics/Typeface;

.field private static sRobotoRegular:Landroid/graphics/Typeface;


# direct methods
.method public static getRobotoBoldTypeface()Landroid/graphics/Typeface;
    .locals 3

    .prologue
    .line 82
    :try_start_0
    sget-boolean v1, Lcom/google/android/music/utils/TypefaceUtil;->sIsRobotoBoldLoaded:Z

    if-nez v1, :cond_0

    .line 83
    const-string v1, "/system/fonts/Roboto-Bold.ttf"

    invoke-static {v1}, Landroid/graphics/Typeface;->createFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v1

    sput-object v1, Lcom/google/android/music/utils/TypefaceUtil;->sRobotoBold:Landroid/graphics/Typeface;

    .line 84
    const/4 v1, 0x1

    sput-boolean v1, Lcom/google/android/music/utils/TypefaceUtil;->sIsRobotoBoldLoaded:Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 90
    :cond_0
    sget-object v1, Lcom/google/android/music/utils/TypefaceUtil;->sRobotoBold:Landroid/graphics/Typeface;

    .local v0, "e":Ljava/lang/RuntimeException;
    :goto_0
    return-object v1

    .line 86
    .end local v0    # "e":Ljava/lang/RuntimeException;
    :catch_0
    move-exception v0

    .line 87
    .restart local v0    # "e":Ljava/lang/RuntimeException;
    const-string v1, "TypefaceUtil"

    const-string v2, "Failed to create /system/fonts/Roboto-Bold.ttf"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getRobotoCondensedItalicTypeface()Landroid/graphics/Typeface;
    .locals 3

    .prologue
    .line 100
    :try_start_0
    sget-boolean v1, Lcom/google/android/music/utils/TypefaceUtil;->sIsRobotoCondensedItalicLoaded:Z

    if-nez v1, :cond_0

    .line 101
    const-string v1, "/system/fonts/RobotoCondensed-Italic.ttf"

    invoke-static {v1}, Landroid/graphics/Typeface;->createFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v1

    sput-object v1, Lcom/google/android/music/utils/TypefaceUtil;->sRobotoCondensedItalic:Landroid/graphics/Typeface;

    .line 102
    const/4 v1, 0x1

    sput-boolean v1, Lcom/google/android/music/utils/TypefaceUtil;->sIsRobotoCondensedItalicLoaded:Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 108
    :cond_0
    sget-object v1, Lcom/google/android/music/utils/TypefaceUtil;->sRobotoCondensedItalic:Landroid/graphics/Typeface;

    .local v0, "e":Ljava/lang/RuntimeException;
    :goto_0
    return-object v1

    .line 104
    .end local v0    # "e":Ljava/lang/RuntimeException;
    :catch_0
    move-exception v0

    .line 105
    .restart local v0    # "e":Ljava/lang/RuntimeException;
    const-string v1, "TypefaceUtil"

    const-string v2, "Failed to create /system/fonts/RobotoCondensed-Italic.ttf"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getRobotoLightTypeface()Landroid/graphics/Typeface;
    .locals 3

    .prologue
    .line 65
    :try_start_0
    sget-boolean v1, Lcom/google/android/music/utils/TypefaceUtil;->sIsRobotoLightLoaded:Z

    if-nez v1, :cond_0

    .line 66
    const-string v1, "/system/fonts/Roboto-Light.ttf"

    invoke-static {v1}, Landroid/graphics/Typeface;->createFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v1

    sput-object v1, Lcom/google/android/music/utils/TypefaceUtil;->sRobotoLight:Landroid/graphics/Typeface;

    .line 67
    const/4 v1, 0x1

    sput-boolean v1, Lcom/google/android/music/utils/TypefaceUtil;->sIsRobotoLightLoaded:Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 73
    :cond_0
    sget-object v1, Lcom/google/android/music/utils/TypefaceUtil;->sRobotoLight:Landroid/graphics/Typeface;

    .local v0, "e":Ljava/lang/RuntimeException;
    :goto_0
    return-object v1

    .line 69
    .end local v0    # "e":Ljava/lang/RuntimeException;
    :catch_0
    move-exception v0

    .line 70
    .restart local v0    # "e":Ljava/lang/RuntimeException;
    const-string v1, "TypefaceUtil"

    const-string v2, "Failed to create /system/fonts/Roboto-Light.ttf"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 71
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getRobotoMediumTypeface()Landroid/graphics/Typeface;
    .locals 3

    .prologue
    .line 117
    :try_start_0
    sget-boolean v1, Lcom/google/android/music/utils/TypefaceUtil;->sIsRobotoMediumLoaded:Z

    if-nez v1, :cond_0

    .line 118
    const-string v1, "/system/fonts/Roboto-Medium.ttf"

    invoke-static {v1}, Landroid/graphics/Typeface;->createFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v1

    sput-object v1, Lcom/google/android/music/utils/TypefaceUtil;->sRobotoMedium:Landroid/graphics/Typeface;

    .line 119
    const/4 v1, 0x1

    sput-boolean v1, Lcom/google/android/music/utils/TypefaceUtil;->sIsRobotoMediumLoaded:Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 125
    :cond_0
    sget-object v1, Lcom/google/android/music/utils/TypefaceUtil;->sRobotoMedium:Landroid/graphics/Typeface;

    .local v0, "e":Ljava/lang/RuntimeException;
    :goto_0
    return-object v1

    .line 121
    .end local v0    # "e":Ljava/lang/RuntimeException;
    :catch_0
    move-exception v0

    .line 122
    .restart local v0    # "e":Ljava/lang/RuntimeException;
    const-string v1, "TypefaceUtil"

    const-string v2, "Failed to create /system/fonts/Roboto-Medium.ttf"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 123
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getRobotoRegularTypeface()Landroid/graphics/Typeface;
    .locals 3

    .prologue
    .line 48
    :try_start_0
    sget-boolean v1, Lcom/google/android/music/utils/TypefaceUtil;->sIsRobotoRegularLoaded:Z

    if-nez v1, :cond_0

    .line 49
    const-string v1, "/system/fonts/Roboto-Regular.ttf"

    invoke-static {v1}, Landroid/graphics/Typeface;->createFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v1

    sput-object v1, Lcom/google/android/music/utils/TypefaceUtil;->sRobotoRegular:Landroid/graphics/Typeface;

    .line 50
    const/4 v1, 0x1

    sput-boolean v1, Lcom/google/android/music/utils/TypefaceUtil;->sIsRobotoRegularLoaded:Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 56
    :cond_0
    sget-object v1, Lcom/google/android/music/utils/TypefaceUtil;->sRobotoRegular:Landroid/graphics/Typeface;

    .local v0, "e":Ljava/lang/RuntimeException;
    :goto_0
    return-object v1

    .line 52
    .end local v0    # "e":Ljava/lang/RuntimeException;
    :catch_0
    move-exception v0

    .line 53
    .restart local v0    # "e":Ljava/lang/RuntimeException;
    const-string v1, "TypefaceUtil"

    const-string v2, "Failed to create /system/fonts/Roboto-Regular.ttf"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 54
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static setTypeface(Landroid/widget/TextView;I)V
    .locals 1
    .param p0, "view"    # Landroid/widget/TextView;
    .param p1, "index"    # I

    .prologue
    .line 135
    packed-switch p1, :pswitch_data_0

    .line 162
    :cond_0
    :goto_0
    return-void

    .line 137
    :pswitch_0
    invoke-static {}, Lcom/google/android/music/utils/TypefaceUtil;->getRobotoRegularTypeface()Landroid/graphics/Typeface;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 138
    sget-object v0, Lcom/google/android/music/utils/TypefaceUtil;->sRobotoRegular:Landroid/graphics/Typeface;

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    goto :goto_0

    .line 142
    :pswitch_1
    invoke-static {}, Lcom/google/android/music/utils/TypefaceUtil;->getRobotoLightTypeface()Landroid/graphics/Typeface;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 143
    sget-object v0, Lcom/google/android/music/utils/TypefaceUtil;->sRobotoLight:Landroid/graphics/Typeface;

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    goto :goto_0

    .line 147
    :pswitch_2
    invoke-static {}, Lcom/google/android/music/utils/TypefaceUtil;->getRobotoBoldTypeface()Landroid/graphics/Typeface;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 148
    sget-object v0, Lcom/google/android/music/utils/TypefaceUtil;->sRobotoBold:Landroid/graphics/Typeface;

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    goto :goto_0

    .line 152
    :pswitch_3
    invoke-static {}, Lcom/google/android/music/utils/TypefaceUtil;->getRobotoCondensedItalicTypeface()Landroid/graphics/Typeface;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 153
    sget-object v0, Lcom/google/android/music/utils/TypefaceUtil;->sRobotoCondensedItalic:Landroid/graphics/Typeface;

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    goto :goto_0

    .line 157
    :pswitch_4
    invoke-static {}, Lcom/google/android/music/utils/TypefaceUtil;->getRobotoMediumTypeface()Landroid/graphics/Typeface;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 158
    sget-object v0, Lcom/google/android/music/utils/TypefaceUtil;->sRobotoMedium:Landroid/graphics/Typeface;

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    goto :goto_0

    .line 135
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
