.class Lcom/google/android/music/store/ConfigStore$DatabaseHelper;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "ConfigStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/store/ConfigStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DatabaseHelper"
.end annotation


# instance fields
.field private mDBPath:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/music/store/ConfigStore;


# direct methods
.method constructor <init>(Lcom/google/android/music/store/ConfigStore;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 108
    iput-object p1, p0, Lcom/google/android/music/store/ConfigStore$DatabaseHelper;->this$0:Lcom/google/android/music/store/ConfigStore;

    .line 109
    # getter for: Lcom/google/android/music/store/ConfigStore;->mContext:Landroid/content/Context;
    invoke-static {p1}, Lcom/google/android/music/store/ConfigStore;->access$100(Lcom/google/android/music/store/ConfigStore;)Landroid/content/Context;

    move-result-object v0

    const-string v1, "config.db"

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v3, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 106
    iput-object v3, p0, Lcom/google/android/music/store/ConfigStore$DatabaseHelper;->mDBPath:Ljava/lang/String;

    .line 110
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/store/ConfigStore$DatabaseHelper;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/store/ConfigStore$DatabaseHelper;

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/android/music/store/ConfigStore$DatabaseHelper;->mDBPath:Ljava/lang/String;

    return-object v0
.end method

.method private createConfigTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 145
    const-string v0, "CREATE TABLE CONFIG(id INTEGER PRIMARY KEY AUTOINCREMENT, Name TEXT NOT NULL, Value TEXT, Type INTEGER NOT NULL DEFAULT 0, UNIQUE(Name, Type) ON CONFLICT REPLACE);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 154
    const-string v0, "CREATE INDEX CONFIG_NAME_TYPE_INDEX on CONFIG(Name,Type)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 159
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 125
    sget-boolean v0, Lcom/google/android/music/store/ConfigStore;->LOGV:Z

    if-eqz v0, :cond_0

    .line 126
    const-string v0, "ConfigStore"

    const-string v1, "Config Database created"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    :cond_0
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->getPath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/store/ConfigStore$DatabaseHelper;->mDBPath:Ljava/lang/String;

    .line 129
    const/4 v0, -0x1

    const/4 v1, 0x1

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/android/music/store/ConfigStore$DatabaseHelper;->onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V

    .line 130
    return-void
.end method

.method public onDowngrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 133
    new-instance v0, Lcom/google/android/music/store/DowngradeException;

    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/music/store/DowngradeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onOpen(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 116
    invoke-static {p1}, Lcom/google/android/music/store/Store;->configureDatabaseConnection(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 117
    invoke-super {p0, p1}, Landroid/database/sqlite/SQLiteOpenHelper;->onOpen(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 118
    sget-boolean v0, Lcom/google/android/music/store/ConfigStore;->LOGV:Z

    if-eqz v0, :cond_0

    .line 119
    const-string v0, "ConfigStore"

    const-string v1, "Database opened"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    :cond_0
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 138
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->getPath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/store/ConfigStore$DatabaseHelper;->mDBPath:Ljava/lang/String;

    .line 139
    const/4 v0, 0x1

    if-ge p2, v0, :cond_0

    .line 140
    invoke-direct {p0, p1}, Lcom/google/android/music/store/ConfigStore$DatabaseHelper;->createConfigTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 142
    :cond_0
    return-void
.end method
