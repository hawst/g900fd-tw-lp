.class public Lcom/google/android/music/ui/ExpandedDescriptionFragment;
.super Landroid/support/v4/app/Fragment;
.source "ExpandedDescriptionFragment.java"


# instance fields
.field private mDescription:Landroid/widget/TextView;

.field private mDescriptionAttribution:Landroid/widget/TextView;

.field private mTitle:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method static createArguments(Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;)Landroid/os/Bundle;
    .locals 2
    .param p0, "metadata"    # Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;

    .prologue
    .line 128
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 129
    .local v0, "result":Landroid/os/Bundle;
    if-eqz p0, :cond_0

    .line 130
    const-string v1, "containerMetadata"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 132
    :cond_0
    return-object v0
.end method

.method private setDescriptionAttribution(Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;)V
    .locals 22
    .param p1, "metadata"    # Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;

    .prologue
    .line 63
    move-object/from16 v0, p1

    iget-object v15, v0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;->descriptionAttrSourceTitle:Ljava/lang/String;

    .line 64
    .local v15, "sourceTitle":Ljava/lang/String;
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;->descriptionAttrSourceUrl:Ljava/lang/String;

    move-object/from16 v16, v0

    .line 65
    .local v16, "sourceUrl":Ljava/lang/String;
    move-object/from16 v0, p1

    iget-object v12, v0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;->descriptionAttrLicenseTitle:Ljava/lang/String;

    .line 66
    .local v12, "licenseTitle":Ljava/lang/String;
    move-object/from16 v0, p1

    iget-object v13, v0, Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;->descriptionAttrLicenseUrl:Ljava/lang/String;

    .line 68
    .local v13, "licenseUrl":Ljava/lang/String;
    invoke-static {v15}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v18

    if-nez v18, :cond_0

    const/4 v8, 0x1

    .line 69
    .local v8, "hasSourceTitle":Z
    :goto_0
    invoke-static/range {v16 .. v16}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v18

    if-nez v18, :cond_1

    const/4 v9, 0x1

    .line 70
    .local v9, "hasSourceUrl":Z
    :goto_1
    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v18

    if-nez v18, :cond_2

    const/4 v6, 0x1

    .line 71
    .local v6, "hasLicenseTitle":Z
    :goto_2
    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v18

    if-nez v18, :cond_3

    const/4 v7, 0x1

    .line 73
    .local v7, "hasLicenseUrl":Z
    :goto_3
    if-nez v8, :cond_4

    if-nez v6, :cond_4

    .line 125
    :goto_4
    return-void

    .line 68
    .end local v6    # "hasLicenseTitle":Z
    .end local v7    # "hasLicenseUrl":Z
    .end local v8    # "hasSourceTitle":Z
    .end local v9    # "hasSourceUrl":Z
    :cond_0
    const/4 v8, 0x0

    goto :goto_0

    .line 69
    .restart local v8    # "hasSourceTitle":Z
    :cond_1
    const/4 v9, 0x0

    goto :goto_1

    .line 70
    .restart local v9    # "hasSourceUrl":Z
    :cond_2
    const/4 v6, 0x0

    goto :goto_2

    .line 71
    .restart local v6    # "hasLicenseTitle":Z
    :cond_3
    const/4 v7, 0x0

    goto :goto_3

    .line 75
    .restart local v7    # "hasLicenseUrl":Z
    :cond_4
    const-string v5, ""

    .line 77
    .local v5, "attributionHtml":Ljava/lang/String;
    if-nez v6, :cond_7

    if-eqz v8, :cond_7

    .line 79
    if-nez v9, :cond_6

    .line 80
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/ExpandedDescriptionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    const v19, 0x7f0b038d

    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    aput-object v15, v20, v21

    invoke-virtual/range {v18 .. v20}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 115
    :cond_5
    :goto_5
    invoke-static {v5}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v14

    check-cast v14, Landroid/text/Spannable;

    .line 116
    .local v14, "s":Landroid/text/Spannable;
    const/16 v18, 0x0

    invoke-interface {v14}, Landroid/text/Spannable;->length()I

    move-result v19

    const-class v20, Landroid/text/style/URLSpan;

    move/from16 v0, v18

    move/from16 v1, v19

    move-object/from16 v2, v20

    invoke-interface {v14, v0, v1, v2}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Landroid/text/style/URLSpan;

    .local v4, "arr$":[Landroid/text/style/URLSpan;
    array-length v11, v4

    .local v11, "len$":I
    const/4 v10, 0x0

    .local v10, "i$":I
    :goto_6
    if-ge v10, v11, :cond_d

    aget-object v17, v4, v10

    .line 117
    .local v17, "u":Landroid/text/style/URLSpan;
    new-instance v18, Lcom/google/android/music/ui/ExpandedDescriptionFragment$1;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/google/android/music/ui/ExpandedDescriptionFragment$1;-><init>(Lcom/google/android/music/ui/ExpandedDescriptionFragment;)V

    move-object/from16 v0, v17

    invoke-interface {v14, v0}, Landroid/text/Spannable;->getSpanStart(Ljava/lang/Object;)I

    move-result v19

    move-object/from16 v0, v17

    invoke-interface {v14, v0}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v20

    const/16 v21, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v19

    move/from16 v2, v20

    move/from16 v3, v21

    invoke-interface {v14, v0, v1, v2, v3}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 116
    add-int/lit8 v10, v10, 0x1

    goto :goto_6

    .line 83
    .end local v4    # "arr$":[Landroid/text/style/URLSpan;
    .end local v10    # "i$":I
    .end local v11    # "len$":I
    .end local v14    # "s":Landroid/text/Spannable;
    .end local v17    # "u":Landroid/text/style/URLSpan;
    :cond_6
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/ExpandedDescriptionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    const v19, 0x7f0b038c

    const/16 v20, 0x2

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    aput-object v16, v20, v21

    const/16 v21, 0x1

    aput-object v15, v20, v21

    invoke-virtual/range {v18 .. v20}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    goto :goto_5

    .line 86
    :cond_7
    if-eqz v6, :cond_9

    if-nez v8, :cond_9

    .line 88
    if-nez v7, :cond_8

    .line 89
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/ExpandedDescriptionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    const v19, 0x7f0b038f

    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    aput-object v12, v20, v21

    invoke-virtual/range {v18 .. v20}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_5

    .line 92
    :cond_8
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/ExpandedDescriptionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    const v19, 0x7f0b038e

    const/16 v20, 0x2

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    aput-object v13, v20, v21

    const/16 v21, 0x1

    aput-object v12, v20, v21

    invoke-virtual/range {v18 .. v20}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_5

    .line 95
    :cond_9
    if-eqz v6, :cond_5

    if-eqz v8, :cond_5

    .line 96
    if-eqz v9, :cond_a

    if-eqz v7, :cond_a

    .line 97
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/ExpandedDescriptionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    const v19, 0x7f0b0388

    const/16 v20, 0x4

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    aput-object v16, v20, v21

    const/16 v21, 0x1

    aput-object v15, v20, v21

    const/16 v21, 0x2

    aput-object v13, v20, v21

    const/16 v21, 0x3

    aput-object v12, v20, v21

    invoke-virtual/range {v18 .. v20}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_5

    .line 100
    :cond_a
    if-eqz v9, :cond_b

    if-nez v7, :cond_b

    .line 101
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/ExpandedDescriptionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    const v19, 0x7f0b0389

    const/16 v20, 0x3

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    aput-object v16, v20, v21

    const/16 v21, 0x1

    aput-object v15, v20, v21

    const/16 v21, 0x2

    aput-object v12, v20, v21

    invoke-virtual/range {v18 .. v20}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_5

    .line 104
    :cond_b
    if-nez v9, :cond_c

    if-eqz v7, :cond_c

    .line 105
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/ExpandedDescriptionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    const v19, 0x7f0b038a

    const/16 v20, 0x3

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    aput-object v15, v20, v21

    const/16 v21, 0x1

    aput-object v13, v20, v21

    const/16 v21, 0x2

    aput-object v12, v20, v21

    invoke-virtual/range {v18 .. v20}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_5

    .line 108
    :cond_c
    if-nez v9, :cond_5

    if-nez v7, :cond_5

    .line 109
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/ExpandedDescriptionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    const v19, 0x7f0b038b

    const/16 v20, 0x2

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    aput-object v15, v20, v21

    const/16 v21, 0x1

    aput-object v12, v20, v21

    invoke-virtual/range {v18 .. v20}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_5

    .line 124
    .restart local v4    # "arr$":[Landroid/text/style/URLSpan;
    .restart local v10    # "i$":I
    .restart local v11    # "len$":I
    .restart local v14    # "s":Landroid/text/Spannable;
    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/ExpandedDescriptionFragment;->mDescriptionAttribution:Landroid/widget/TextView;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_4
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 35
    invoke-virtual {p0}, Lcom/google/android/music/ui/ExpandedDescriptionFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 36
    .local v0, "args":Landroid/os/Bundle;
    const-string v3, "containerMetadata"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;

    .line 37
    .local v1, "metadata":Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;
    const v3, 0x7f04006f

    const/4 v4, 0x0

    invoke-virtual {p1, v3, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 40
    .local v2, "view":Landroid/view/View;
    if-eqz v1, :cond_0

    .line 41
    const v3, 0x7f0e00b8

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/google/android/music/ui/ExpandedDescriptionFragment;->mTitle:Landroid/widget/TextView;

    .line 42
    iget-object v3, p0, Lcom/google/android/music/ui/ExpandedDescriptionFragment;->mTitle:Landroid/widget/TextView;

    iget-object v4, v1, Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;->primaryTitle:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 44
    const v3, 0x7f0e0198

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/google/android/music/ui/ExpandedDescriptionFragment;->mDescription:Landroid/widget/TextView;

    .line 45
    iget-object v3, p0, Lcom/google/android/music/ui/ExpandedDescriptionFragment;->mDescription:Landroid/widget/TextView;

    iget-object v4, v1, Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;->description:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 47
    invoke-static {}, Lcom/google/android/music/utils/ConfigUtils;->isDescriptionAttributionEnabled()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 48
    const v3, 0x7f0e0199

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/google/android/music/ui/ExpandedDescriptionFragment;->mDescriptionAttribution:Landroid/widget/TextView;

    .line 50
    iget-object v3, p0, Lcom/google/android/music/ui/ExpandedDescriptionFragment;->mDescriptionAttribution:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 51
    invoke-direct {p0, v1}, Lcom/google/android/music/ui/ExpandedDescriptionFragment;->setDescriptionAttribution(Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;)V

    .line 55
    :cond_0
    return-object v2
.end method
