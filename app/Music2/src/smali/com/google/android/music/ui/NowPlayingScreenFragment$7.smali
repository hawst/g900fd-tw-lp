.class Lcom/google/android/music/ui/NowPlayingScreenFragment$7;
.super Ljava/lang/Object;
.source "NowPlayingScreenFragment.java"

# interfaces
.implements Lcom/google/android/music/animator/AnimatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/NowPlayingScreenFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mCancelled:Z

.field final synthetic this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/NowPlayingScreenFragment;)V
    .locals 1

    .prologue
    .line 786
    iput-object p1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$7;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 788
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$7;->mCancelled:Z

    return-void
.end method

.method private isToVisible(Lcom/google/android/music/animator/Animator;)Z
    .locals 2
    .param p1, "animation"    # Lcom/google/android/music/animator/Animator;

    .prologue
    .line 794
    invoke-virtual {p1}, Lcom/google/android/music/animator/Animator;->getValueTo()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/16 v1, 0xff

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onAnimationEnd(Lcom/google/android/music/animator/Animator;)V
    .locals 1
    .param p1, "animation"    # Lcom/google/android/music/animator/Animator;

    .prologue
    .line 807
    iget-boolean v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$7;->mCancelled:Z

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/music/ui/NowPlayingScreenFragment$7;->isToVisible(Lcom/google/android/music/animator/Animator;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 810
    :cond_0
    return-void
.end method

.method public onAnimationRepeat(Lcom/google/android/music/animator/Animator;)V
    .locals 0
    .param p1, "animation"    # Lcom/google/android/music/animator/Animator;

    .prologue
    .line 804
    return-void
.end method

.method public onAnimationStart(Lcom/google/android/music/animator/Animator;)V
    .locals 2
    .param p1, "animation"    # Lcom/google/android/music/animator/Animator;

    .prologue
    .line 798
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$7;->mCancelled:Z

    .line 799
    invoke-direct {p0, p1}, Lcom/google/android/music/ui/NowPlayingScreenFragment$7;->isToVisible(Lcom/google/android/music/animator/Animator;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 800
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$7;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # getter for: Lcom/google/android/music/ui/NowPlayingScreenFragment;->mProgress:Lcom/google/android/music/SizableTrackSeekBar;
    invoke-static {v0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$1200(Lcom/google/android/music/ui/NowPlayingScreenFragment;)Lcom/google/android/music/SizableTrackSeekBar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/music/SizableTrackSeekBar;->setEnabled(Z)V

    .line 802
    :cond_0
    return-void
.end method
