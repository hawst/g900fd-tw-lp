.class Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;
.super Ljava/lang/Object;
.source "LocalDevicePlayback.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/playback/LocalDevicePlayback;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "MediaListWrapper"
.end annotation


# instance fields
.field mColDomainIdx:I

.field mColListItemIdx:I

.field mColMusicIdx:I

.field mContext:Landroid/content/Context;

.field mCursor:Lcom/google/android/music/medialist/MediaList$MediaCursor;

.field private mCursorLock:Ljava/util/concurrent/locks/ReadWriteLock;

.field private mCursorSongList:Lcom/google/android/music/medialist/SongList;

.field private mPlaylistLoading:Z

.field private mRadiusToSearch:I

.field private final mRefreshContentObserver:Landroid/database/ContentObserver;

.field final synthetic this$0:Lcom/google/android/music/playback/LocalDevicePlayback;


# direct methods
.method public constructor <init>(Lcom/google/android/music/playback/LocalDevicePlayback;Landroid/content/Context;)V
    .locals 2
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, -0x1

    .line 1068
    iput-object p1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1052
    iput v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mColMusicIdx:I

    .line 1053
    iput v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mColListItemIdx:I

    .line 1054
    iput v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mColDomainIdx:I

    .line 1055
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mPlaylistLoading:Z

    .line 1056
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mCursorSongList:Lcom/google/android/music/medialist/SongList;

    .line 1058
    new-instance v0, Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mCursorLock:Ljava/util/concurrent/locks/ReadWriteLock;

    .line 1469
    new-instance v0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper$1;

    sget-object v1, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    invoke-direct {v0, p0, v1}, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper$1;-><init>(Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mRefreshContentObserver:Landroid/database/ContentObserver;

    .line 1069
    iput-object p2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mContext:Landroid/content/Context;

    .line 1070
    return-void
.end method

.method static synthetic access$5000(Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;

    .prologue
    .line 1039
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->processContentChange()V

    return-void
.end method

.method static synthetic access$6400(Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;

    .prologue
    .line 1039
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->refreshCurrentSongPosition()Z

    move-result v0

    return v0
.end method

.method private getDomainLocked()Lcom/google/android/music/download/ContentIdentifier$Domain;
    .locals 4

    .prologue
    .line 1154
    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mCursor:Lcom/google/android/music/medialist/MediaList$MediaCursor;

    iget v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mColDomainIdx:I

    invoke-virtual {v2, v3}, Lcom/google/android/music/medialist/MediaList$MediaCursor;->getInt(I)I

    move-result v1

    .line 1155
    .local v1, "domainInt":I
    invoke-static {v1}, Lcom/google/android/music/download/ContentIdentifier$Domain;->fromDBValue(I)Lcom/google/android/music/download/ContentIdentifier$Domain;

    move-result-object v0

    .line 1156
    .local v0, "domain":Lcom/google/android/music/download/ContentIdentifier$Domain;
    return-object v0
.end method

.method private moveToPositionLocked(ILjava/util/concurrent/locks/Lock;)Z
    .locals 6
    .param p1, "index"    # I
    .param p2, "lock"    # Ljava/util/concurrent/locks/Lock;

    .prologue
    const/4 v2, 0x0

    .line 1269
    iget-object v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mContext:Landroid/content/Context;

    const-string v4, "MediaListWrapper.moveToPositionLocked() on main thread"

    invoke-static {v3, v4}, Lcom/google/android/music/utils/MusicUtils;->checkMainThread(Landroid/content/Context;Ljava/lang/String;)V

    .line 1271
    if-gez p1, :cond_2

    .line 1272
    iget-object v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->LOGV:Z
    invoke-static {v3}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$000(Lcom/google/android/music/playback/LocalDevicePlayback;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1274
    const-string v3, "LocalDevicePlayback"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "invalid position requested: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/Exception;

    invoke-direct {v5}, Ljava/lang/Exception;-><init>()V

    invoke-static {v3, v4, v5}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1307
    :cond_0
    :goto_0
    return v2

    .line 1276
    :cond_1
    const-string v3, "LocalDevicePlayback"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "invalid position requested: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1281
    :cond_2
    :try_start_0
    iget-object v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mCursor:Lcom/google/android/music/medialist/MediaList$MediaCursor;

    if-eqz v3, :cond_0

    .line 1282
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->length()I

    move-result v1

    .line 1283
    .local v1, "songListSize":I
    if-lt p1, v1, :cond_3

    .line 1284
    const-string v3, "LocalDevicePlayback"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "invalid position requested: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ". List size: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1306
    .end local v1    # "songListSize":I
    :catch_0
    move-exception v0

    .line 1307
    .local v0, "ex":Landroid/database/CursorIndexOutOfBoundsException;
    goto :goto_0

    .line 1288
    .end local v0    # "ex":Landroid/database/CursorIndexOutOfBoundsException;
    .restart local v1    # "songListSize":I
    :cond_3
    iget-object v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mCursor:Lcom/google/android/music/medialist/MediaList$MediaCursor;

    invoke-virtual {v3, p1}, Lcom/google/android/music/medialist/MediaList$MediaCursor;->moveToPosition(I)Z

    move-result v3

    if-nez v3, :cond_4

    .line 1289
    const-string v3, "LocalDevicePlayback"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed to move cursor to position: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1293
    :cond_4
    :goto_1
    iget-object v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mCursor:Lcom/google/android/music/medialist/MediaList$MediaCursor;

    iget v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mColMusicIdx:I

    invoke-virtual {v3, v4}, Lcom/google/android/music/medialist/MediaList$MediaCursor;->isNull(I)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 1294
    invoke-interface {p2}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 1295
    const-wide/16 v4, 0xc8

    invoke-static {v4, v5}, Landroid/os/SystemClock;->sleep(J)V

    .line 1296
    invoke-interface {p2}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 1297
    iget-object v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mCursor:Lcom/google/android/music/medialist/MediaList$MediaCursor;

    if-eqz v3, :cond_0

    .line 1298
    iget-object v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mCursor:Lcom/google/android/music/medialist/MediaList$MediaCursor;

    invoke-virtual {v3}, Lcom/google/android/music/medialist/MediaList$MediaCursor;->requery()Z
    :try_end_0
    .catch Landroid/database/CursorIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 1303
    :cond_5
    const/4 v2, 0x1

    goto/16 :goto_0
.end method

.method private processContentChange()V
    .locals 10

    .prologue
    const-wide/16 v6, 0x0

    const/4 v8, 0x0

    const/4 v4, 0x2

    const/4 v2, 0x0

    .line 1426
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->refresh()I

    move-result v9

    .line 1427
    .local v9, "refreshResult":I
    if-ne v9, v4, :cond_5

    .line 1428
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->length()I

    move-result v0

    .line 1429
    .local v0, "listLength":I
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    invoke-virtual {v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->isPlaying()Z

    move-result v3

    .line 1430
    .local v3, "play":Z
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayPos:I
    invoke-static {v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$2800(Lcom/google/android/music/playback/LocalDevicePlayback;)I

    move-result v1

    if-ltz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayPos:I
    invoke-static {v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$2800(Lcom/google/android/music/playback/LocalDevicePlayback;)I

    move-result v1

    if-ge v1, v0, :cond_1

    .line 1445
    :goto_0
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->useRemotePlaybackClient()Z
    invoke-static {v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$100(Lcom/google/android/music/playback/LocalDevicePlayback;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1448
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mRemotePlaybackClient:Lcom/google/android/music/cast/remotemedia/RemotePlaybackClient;
    invoke-static {v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$200(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/cast/remotemedia/RemotePlaybackClient;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/music/cast/remotemedia/RemotePlaybackClient;->isQueueLoaded()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1449
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    move v4, v2

    move v5, v2

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->openCurrentAndNext(ZZZZJLcom/google/android/music/download/ContentIdentifier;)V
    invoke-static/range {v1 .. v8}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$3500(Lcom/google/android/music/playback/LocalDevicePlayback;ZZZZJLcom/google/android/music/download/ContentIdentifier;)V

    .line 1466
    .end local v0    # "listLength":I
    .end local v3    # "play":Z
    :cond_0
    :goto_1
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    sget-object v2, Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;->FEED:Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->feedQueueIfNeeded(Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;)Z
    invoke-static {v1, v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$4000(Lcom/google/android/music/playback/LocalDevicePlayback;Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;)Z

    .line 1467
    return-void

    .line 1432
    .restart local v0    # "listLength":I
    .restart local v3    # "play":Z
    :cond_1
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mRepeatMode:I
    invoke-static {v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$4100(Lcom/google/android/music/playback/LocalDevicePlayback;)I

    move-result v1

    if-ne v1, v4, :cond_2

    .line 1435
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # setter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayPos:I
    invoke-static {v1, v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$2802(Lcom/google/android/music/playback/LocalDevicePlayback;I)I

    goto :goto_0

    .line 1436
    :cond_2
    if-lez v0, :cond_3

    .line 1438
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    add-int/lit8 v4, v0, -0x1

    # setter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayPos:I
    invoke-static {v1, v4}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$2802(Lcom/google/android/music/playback/LocalDevicePlayback;I)I

    .line 1439
    const/4 v3, 0x0

    goto :goto_0

    .line 1441
    :cond_3
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # setter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayPos:I
    invoke-static {v1, v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$2802(Lcom/google/android/music/playback/LocalDevicePlayback;I)I

    .line 1442
    const/4 v3, 0x0

    goto :goto_0

    .line 1454
    :cond_4
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    move v4, v2

    move v5, v2

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->openCurrentAndNext(ZZZZJLcom/google/android/music/download/ContentIdentifier;)V
    invoke-static/range {v1 .. v8}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$3500(Lcom/google/android/music/playback/LocalDevicePlayback;ZZZZJLcom/google/android/music/download/ContentIdentifier;)V

    goto :goto_1

    .line 1458
    .end local v0    # "listLength":I
    .end local v3    # "play":Z
    :cond_5
    const/4 v1, 0x1

    if-ne v9, v1, :cond_0

    .line 1460
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mAudioId:Lcom/google/android/music/download/ContentIdentifier;
    invoke-static {v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$2400(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/download/ContentIdentifier;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1461
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mAudioId:Lcom/google/android/music/download/ContentIdentifier;
    invoke-static {v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$2400(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/download/ContentIdentifier;

    move-result-object v2

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->refreshCursor(Lcom/google/android/music/download/ContentIdentifier;)Z
    invoke-static {v1, v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$3000(Lcom/google/android/music/playback/LocalDevicePlayback;Lcom/google/android/music/download/ContentIdentifier;)Z

    .line 1462
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->setNextTrack()V
    invoke-static {v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$700(Lcom/google/android/music/playback/LocalDevicePlayback;)V

    goto :goto_1
.end method

.method private refresh()I
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1323
    iget-object v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mCursorLock:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v3}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 1325
    :try_start_0
    iget-object v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mCursorSongList:Lcom/google/android/music/medialist/SongList;

    invoke-direct {p0, v3}, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->resetCursorLocked(Lcom/google/android/music/medialist/SongList;)V

    .line 1326
    iget-object v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mCursor:Lcom/google/android/music/medialist/MediaList$MediaCursor;

    if-eqz v3, :cond_4

    .line 1327
    iget-object v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayPos:I
    invoke-static {v3}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$2800(Lcom/google/android/music/playback/LocalDevicePlayback;)I

    move-result v0

    .line 1328
    .local v0, "oldPosition":I
    iget-object v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mCursor:Lcom/google/android/music/medialist/MediaList$MediaCursor;

    invoke-virtual {v3}, Lcom/google/android/music/medialist/MediaList$MediaCursor;->getCountSync()I

    move-result v3

    if-nez v3, :cond_1

    .line 1329
    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mCursor:Lcom/google/android/music/medialist/MediaList$MediaCursor;

    invoke-virtual {v2}, Lcom/google/android/music/medialist/MediaList$MediaCursor;->close()V

    .line 1330
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mCursor:Lcom/google/android/music/medialist/MediaList$MediaCursor;

    .line 1331
    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    invoke-virtual {v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->isPlaying()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1332
    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    invoke-virtual {v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->stopSync()V

    .line 1334
    :cond_0
    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->onQueueChanged()V
    invoke-static {v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$1600(Lcom/google/android/music/playback/LocalDevicePlayback;)V

    .line 1335
    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->onSongChanged()V
    invoke-static {v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$1000(Lcom/google/android/music/playback/LocalDevicePlayback;)V

    .line 1336
    const-string v2, "LocalDevicePlayback"

    const-string v3, "New list returned an empty list"

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1355
    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mCursorLock:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v2}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .end local v0    # "oldPosition":I
    :goto_0
    return v1

    .line 1339
    .restart local v0    # "oldPosition":I
    :cond_1
    const/4 v1, -0x1

    if-eq v0, v1, :cond_3

    :try_start_1
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mAudioId:Lcom/google/android/music/download/ContentIdentifier;
    invoke-static {v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$2400(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/download/ContentIdentifier;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 1341
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->updateSongPositionLocked()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    if-eqz v1, :cond_2

    .line 1355
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mCursorLock:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    move v1, v2

    goto :goto_0

    .line 1344
    :cond_2
    :try_start_2
    const-string v1, "LocalDevicePlayback"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Could not find old file: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mAudioId:Lcom/google/android/music/download/ContentIdentifier;
    invoke-static {v3}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$2400(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/download/ContentIdentifier;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " in new list with search radius "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mRadiusToSearch:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1346
    const/4 v1, 0x2

    .line 1355
    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mCursorLock:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v2}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mCursorLock:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    move v1, v2

    goto :goto_0

    .line 1351
    .end local v0    # "oldPosition":I
    :cond_4
    :try_start_3
    const-string v2, "LocalDevicePlayback"

    const-string v3, "Could not find old position... mCursor was null"

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1355
    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mCursorLock:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v2}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_0

    :catchall_0
    move-exception v1

    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mCursorLock:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v2}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v1
.end method

.method private refreshCurrentSongPosition()Z
    .locals 2

    .prologue
    .line 1314
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mCursorLock:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 1316
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->updateSongPositionLocked()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 1318
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mCursorLock:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return v0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mCursorLock:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method private resetCursorLocked(Lcom/google/android/music/medialist/SongList;)V
    .locals 7
    .param p1, "medialist"    # Lcom/google/android/music/medialist/SongList;

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 1098
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mCursor:Lcom/google/android/music/medialist/MediaList$MediaCursor;

    if-eqz v1, :cond_0

    .line 1099
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mCursor:Lcom/google/android/music/medialist/MediaList$MediaCursor;

    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mRefreshContentObserver:Landroid/database/ContentObserver;

    invoke-virtual {v1, v2}, Lcom/google/android/music/medialist/MediaList$MediaCursor;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 1100
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mCursor:Lcom/google/android/music/medialist/MediaList$MediaCursor;

    invoke-virtual {v1}, Lcom/google/android/music/medialist/MediaList$MediaCursor;->close()V

    .line 1101
    iput-object v6, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mCursor:Lcom/google/android/music/medialist/MediaList$MediaCursor;

    .line 1103
    :cond_0
    if-nez p1, :cond_2

    .line 1123
    :cond_1
    :goto_0
    return-void

    .line 1108
    :cond_2
    invoke-virtual {p1}, Lcom/google/android/music/medialist/SongList;->hasUniqueAudioId()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1109
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v4

    const-string v1, "Domain"

    aput-object v1, v0, v3

    .line 1110
    .local v0, "cols":[Ljava/lang/String;
    const/4 v1, -0x1

    iput v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mColListItemIdx:I

    .line 1111
    iput v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mColDomainIdx:I

    .line 1118
    :goto_1
    iput v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mColMusicIdx:I

    .line 1119
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mContext:Landroid/content/Context;

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->getSyncMediaCursor(Lcom/google/android/music/medialist/SongList;Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/music/medialist/MediaList$MediaCursor;
    invoke-static {v1, p1, v2, v0, v6}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$4900(Lcom/google/android/music/playback/LocalDevicePlayback;Lcom/google/android/music/medialist/SongList;Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/music/medialist/MediaList$MediaCursor;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mCursor:Lcom/google/android/music/medialist/MediaList$MediaCursor;

    .line 1120
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mCursor:Lcom/google/android/music/medialist/MediaList$MediaCursor;

    if-eqz v1, :cond_1

    .line 1121
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mCursor:Lcom/google/android/music/medialist/MediaList$MediaCursor;

    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mRefreshContentObserver:Landroid/database/ContentObserver;

    invoke-virtual {v1, v2}, Lcom/google/android/music/medialist/MediaList$MediaCursor;->registerContentObserver(Landroid/database/ContentObserver;)V

    goto :goto_0

    .line 1113
    .end local v0    # "cols":[Ljava/lang/String;
    :cond_3
    const/4 v1, 0x3

    new-array v0, v1, [Ljava/lang/String;

    const-string v1, "audio_id"

    aput-object v1, v0, v4

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "Domain"

    aput-object v1, v0, v5

    .line 1115
    .restart local v0    # "cols":[Ljava/lang/String;
    iput v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mColListItemIdx:I

    .line 1116
    iput v5, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mColDomainIdx:I

    goto :goto_1
.end method

.method private updateSongPositionLocked()Z
    .locals 14

    .prologue
    const/4 v13, 0x1

    const/4 v12, 0x0

    .line 1365
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mAudioId:Lcom/google/android/music/download/ContentIdentifier;
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$2400(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/download/ContentIdentifier;

    move-result-object v0

    if-nez v0, :cond_0

    move v0, v12

    .line 1419
    :goto_0
    return v0

    .line 1369
    :cond_0
    iget v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mColListItemIdx:I

    if-gez v0, :cond_2

    .line 1370
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mAudioId:Lcom/google/android/music/download/ContentIdentifier;
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$2400(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/download/ContentIdentifier;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/download/ContentIdentifier;->getId()J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayPos:I
    invoke-static {v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$2800(Lcom/google/android/music/playback/LocalDevicePlayback;)I

    move-result v2

    iget-object v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mCursor:Lcom/google/android/music/medialist/MediaList$MediaCursor;

    iget v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mColMusicIdx:I

    iget v5, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mRadiusToSearch:I

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/DbUtils;->findItemInCursor(JILandroid/database/Cursor;II)I

    move-result v9

    .line 1372
    .local v9, "newPosition":I
    if-gez v9, :cond_1

    move v0, v12

    .line 1373
    goto :goto_0

    .line 1375
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # setter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayPos:I
    invoke-static {v0, v9}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$2802(Lcom/google/android/music/playback/LocalDevicePlayback;I)I

    move v0, v13

    .line 1376
    goto :goto_0

    .line 1378
    .end local v9    # "newPosition":I
    :cond_2
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaList:Lcom/google/android/music/medialist/SongList;
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$1800(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/medialist/SongList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/medialist/SongList;->hasStablePrimaryIds()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1380
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mListItemId:J
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$1100(Lcom/google/android/music/playback/LocalDevicePlayback;)J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayPos:I
    invoke-static {v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$2800(Lcom/google/android/music/playback/LocalDevicePlayback;)I

    move-result v2

    iget-object v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mCursor:Lcom/google/android/music/medialist/MediaList$MediaCursor;

    iget v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mColListItemIdx:I

    iget v5, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mRadiusToSearch:I

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/DbUtils;->findItemInCursor(JILandroid/database/Cursor;II)I

    move-result v9

    .line 1382
    .restart local v9    # "newPosition":I
    if-gez v9, :cond_3

    move v0, v12

    .line 1383
    goto :goto_0

    .line 1385
    :cond_3
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # setter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayPos:I
    invoke-static {v0, v9}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$2802(Lcom/google/android/music/playback/LocalDevicePlayback;I)I

    move v0, v13

    .line 1386
    goto :goto_0

    .line 1395
    .end local v9    # "newPosition":I
    :cond_4
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mListItemId:J
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$1100(Lcom/google/android/music/playback/LocalDevicePlayback;)J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mAudioId:Lcom/google/android/music/download/ContentIdentifier;
    invoke-static {v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$2400(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/download/ContentIdentifier;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/music/download/ContentIdentifier;->getId()J

    move-result-wide v2

    iget-object v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayPos:I
    invoke-static {v4}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$2800(Lcom/google/android/music/playback/LocalDevicePlayback;)I

    move-result v4

    iget-object v5, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mCursor:Lcom/google/android/music/medialist/MediaList$MediaCursor;

    iget v6, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mColListItemIdx:I

    iget v7, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mColMusicIdx:I

    iget v8, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mRadiusToSearch:I

    invoke-static/range {v0 .. v8}, Lcom/google/android/music/utils/DbUtils;->findIndirectlyReferencedItem(JJILandroid/database/Cursor;III)I

    move-result v9

    .line 1404
    .restart local v9    # "newPosition":I
    if-gez v9, :cond_5

    move v0, v12

    .line 1405
    goto :goto_0

    .line 1407
    :cond_5
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # setter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayPos:I
    invoke-static {v0, v9}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$2802(Lcom/google/android/music/playback/LocalDevicePlayback;I)I

    .line 1409
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mCursor:Lcom/google/android/music/medialist/MediaList$MediaCursor;

    invoke-virtual {v0, v9}, Lcom/google/android/music/medialist/MediaList$MediaCursor;->moveToPosition(I)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1410
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mCursor:Lcom/google/android/music/medialist/MediaList$MediaCursor;

    iget v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mColListItemIdx:I

    invoke-virtual {v0, v1}, Lcom/google/android/music/medialist/MediaList$MediaCursor;->getLong(I)J

    move-result-wide v10

    .line 1411
    .local v10, "newListItemId":J
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mListItemId:J
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$1100(Lcom/google/android/music/playback/LocalDevicePlayback;)J

    move-result-wide v0

    cmp-long v0, v10, v0

    if-eqz v0, :cond_6

    .line 1413
    const-string v0, "LocalDevicePlayback"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Now playing song ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mAudioId:Lcom/google/android/music/download/ContentIdentifier;
    invoke-static {v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$2400(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/download/ContentIdentifier;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/music/download/ContentIdentifier;->getId()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") list item id changed from "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mListItemId:J
    invoke-static {v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$1100(Lcom/google/android/music/playback/LocalDevicePlayback;)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 1416
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # setter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mListItemId:J
    invoke-static {v0, v10, v11}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$1102(Lcom/google/android/music/playback/LocalDevicePlayback;J)J

    .end local v10    # "newListItemId":J
    :cond_6
    move v0, v13

    .line 1419
    goto/16 :goto_0
.end method


# virtual methods
.method public get(I)Lcom/google/android/music/download/ContentIdentifier;
    .locals 7
    .param p1, "index"    # I

    .prologue
    const/4 v3, 0x0

    .line 1169
    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mCursorLock:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v2}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v1

    .line 1171
    .local v1, "lock":Ljava/util/concurrent/locks/Lock;
    :try_start_0
    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 1172
    invoke-direct {p0, p1, v1}, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->moveToPositionLocked(ILjava/util/concurrent/locks/Lock;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1174
    new-instance v2, Lcom/google/android/music/download/ContentIdentifier;

    iget-object v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mCursor:Lcom/google/android/music/medialist/MediaList$MediaCursor;

    iget v5, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mColMusicIdx:I

    invoke-virtual {v4, v5}, Lcom/google/android/music/medialist/MediaList$MediaCursor;->getLong(I)J

    move-result-wide v4

    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->getDomainLocked()Lcom/google/android/music/download/ContentIdentifier$Domain;

    move-result-object v6

    invoke-direct {v2, v4, v5, v6}, Lcom/google/android/music/download/ContentIdentifier;-><init>(JLcom/google/android/music/download/ContentIdentifier$Domain;)V
    :try_end_0
    .catch Landroid/database/CursorIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1181
    :try_start_1
    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 1186
    :goto_0
    return-object v2

    .line 1181
    :cond_0
    :try_start_2
    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    :goto_1
    move-object v2, v3

    .line 1186
    goto :goto_0

    .line 1177
    :catch_0
    move-exception v0

    .line 1181
    .local v0, "ex":Landroid/database/CursorIndexOutOfBoundsException;
    :try_start_3
    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    :goto_2
    move-object v2, v3

    .line 1186
    goto :goto_0

    .line 1180
    .end local v0    # "ex":Landroid/database/CursorIndexOutOfBoundsException;
    :catchall_0
    move-exception v2

    .line 1181
    :try_start_4
    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_4

    .line 1186
    :goto_3
    throw v2

    .line 1182
    :catch_1
    move-exception v3

    goto :goto_0

    :catch_2
    move-exception v2

    goto :goto_1

    .restart local v0    # "ex":Landroid/database/CursorIndexOutOfBoundsException;
    :catch_3
    move-exception v2

    goto :goto_2

    .end local v0    # "ex":Landroid/database/CursorIndexOutOfBoundsException;
    :catch_4
    move-exception v3

    goto :goto_3
.end method

.method public getAudioIdAndListItemId(I)Landroid/util/Pair;
    .locals 10
    .param p1, "index"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Landroid/util/Pair",
            "<",
            "Lcom/google/android/music/download/ContentIdentifier;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 1196
    iget-object v6, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mCursorLock:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v6}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v3

    .line 1198
    .local v3, "lock":Ljava/util/concurrent/locks/Lock;
    :try_start_0
    invoke-interface {v3}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 1199
    invoke-direct {p0, p1, v3}, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->moveToPositionLocked(ILjava/util/concurrent/locks/Lock;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1200
    const-wide/16 v4, 0x0

    .line 1201
    .local v4, "listItemId":J
    iget v6, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mColListItemIdx:I

    if-ltz v6, :cond_0

    .line 1202
    iget-object v6, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mCursor:Lcom/google/android/music/medialist/MediaList$MediaCursor;

    iget v8, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mColListItemIdx:I

    invoke-virtual {v6, v8}, Lcom/google/android/music/medialist/MediaList$MediaCursor;->getLong(I)J

    move-result-wide v4

    .line 1204
    :cond_0
    iget-object v6, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mCursor:Lcom/google/android/music/medialist/MediaList$MediaCursor;

    iget v8, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mColMusicIdx:I

    invoke-virtual {v6, v8}, Lcom/google/android/music/medialist/MediaList$MediaCursor;->getLong(I)J

    move-result-wide v0

    .line 1205
    .local v0, "audioId":J
    new-instance v6, Landroid/util/Pair;

    new-instance v8, Lcom/google/android/music/download/ContentIdentifier;

    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->getDomainLocked()Lcom/google/android/music/download/ContentIdentifier$Domain;

    move-result-object v9

    invoke-direct {v8, v0, v1, v9}, Lcom/google/android/music/download/ContentIdentifier;-><init>(JLcom/google/android/music/download/ContentIdentifier$Domain;)V

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-direct {v6, v8, v9}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Landroid/database/CursorIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1213
    :try_start_1
    invoke-interface {v3}, Ljava/util/concurrent/locks/Lock;->unlock()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 1218
    .end local v0    # "audioId":J
    .end local v4    # "listItemId":J
    :goto_0
    return-object v6

    .line 1213
    :cond_1
    :try_start_2
    invoke-interface {v3}, Ljava/util/concurrent/locks/Lock;->unlock()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    :goto_1
    move-object v6, v7

    .line 1218
    goto :goto_0

    .line 1209
    :catch_0
    move-exception v2

    .line 1213
    .local v2, "ex":Landroid/database/CursorIndexOutOfBoundsException;
    :try_start_3
    invoke-interface {v3}, Ljava/util/concurrent/locks/Lock;->unlock()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    :goto_2
    move-object v6, v7

    .line 1218
    goto :goto_0

    .line 1212
    .end local v2    # "ex":Landroid/database/CursorIndexOutOfBoundsException;
    :catchall_0
    move-exception v6

    .line 1213
    :try_start_4
    invoke-interface {v3}, Ljava/util/concurrent/locks/Lock;->unlock()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_4

    .line 1218
    :goto_3
    throw v6

    .line 1214
    .restart local v0    # "audioId":J
    .restart local v4    # "listItemId":J
    :catch_1
    move-exception v7

    goto :goto_0

    .end local v0    # "audioId":J
    .end local v4    # "listItemId":J
    :catch_2
    move-exception v6

    goto :goto_1

    .restart local v2    # "ex":Landroid/database/CursorIndexOutOfBoundsException;
    :catch_3
    move-exception v6

    goto :goto_2

    .end local v2    # "ex":Landroid/database/CursorIndexOutOfBoundsException;
    :catch_4
    move-exception v7

    goto :goto_3
.end method

.method public getTailTracks(I)Ljava/util/List;
    .locals 11
    .param p1, "tailSize"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/download/ContentIdentifier;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1228
    new-instance v7, Ljava/util/LinkedList;

    invoke-direct {v7}, Ljava/util/LinkedList;-><init>()V

    .line 1229
    .local v7, "trackList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/download/ContentIdentifier;>;"
    iget-object v8, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mCursorLock:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v8}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v5

    .line 1231
    .local v5, "lock":Ljava/util/concurrent/locks/Lock;
    :try_start_0
    invoke-interface {v5}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 1232
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->length()I

    move-result v4

    .line 1233
    .local v4, "length":I
    const/4 v6, 0x0

    .line 1234
    .local v6, "startPosition":I
    if-ge p1, v4, :cond_0

    .line 1235
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->length()I

    move-result v8

    sub-int v6, v8, p1

    .line 1237
    :cond_0
    move v3, v6

    .local v3, "i":I
    :goto_0
    if-ge v3, v4, :cond_2

    .line 1238
    invoke-direct {p0, v3, v5}, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->moveToPositionLocked(ILjava/util/concurrent/locks/Lock;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 1239
    iget-object v8, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mCursor:Lcom/google/android/music/medialist/MediaList$MediaCursor;

    iget v9, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mColMusicIdx:I

    invoke-virtual {v8, v9}, Lcom/google/android/music/medialist/MediaList$MediaCursor;->getLong(I)J

    move-result-wide v0

    .line 1240
    .local v0, "audioId":J
    new-instance v8, Lcom/google/android/music/download/ContentIdentifier;

    invoke-direct {p0}, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->getDomainLocked()Lcom/google/android/music/download/ContentIdentifier$Domain;

    move-result-object v9

    invoke-direct {v8, v0, v1, v9}, Lcom/google/android/music/download/ContentIdentifier;-><init>(JLcom/google/android/music/download/ContentIdentifier$Domain;)V

    invoke-interface {v7, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1237
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1242
    .end local v0    # "audioId":J
    :cond_1
    const-string v8, "LocalDevicePlayback"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Failed to move the playback cursor at position: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/database/CursorIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1252
    :try_start_1
    invoke-interface {v5}, Ljava/util/concurrent/locks/Lock;->unlock()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    .line 1257
    .end local v3    # "i":I
    .end local v4    # "length":I
    .end local v6    # "startPosition":I
    .end local v7    # "trackList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/download/ContentIdentifier;>;"
    :goto_1
    return-object v7

    .line 1252
    .restart local v3    # "i":I
    .restart local v4    # "length":I
    .restart local v6    # "startPosition":I
    .restart local v7    # "trackList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/download/ContentIdentifier;>;"
    :cond_2
    :try_start_2
    invoke-interface {v5}, Ljava/util/concurrent/locks/Lock;->unlock()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1

    .line 1253
    :catch_0
    move-exception v8

    goto :goto_1

    .line 1248
    .end local v3    # "i":I
    .end local v4    # "length":I
    .end local v6    # "startPosition":I
    :catch_1
    move-exception v2

    .line 1249
    .local v2, "ex":Landroid/database/CursorIndexOutOfBoundsException;
    const/4 v7, 0x0

    .line 1252
    .end local v7    # "trackList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/download/ContentIdentifier;>;"
    :try_start_3
    invoke-interface {v5}, Ljava/util/concurrent/locks/Lock;->unlock()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_1

    .line 1253
    :catch_2
    move-exception v8

    goto :goto_1

    .line 1251
    .end local v2    # "ex":Landroid/database/CursorIndexOutOfBoundsException;
    .restart local v7    # "trackList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/download/ContentIdentifier;>;"
    :catchall_0
    move-exception v8

    .line 1252
    :try_start_4
    invoke-interface {v5}, Ljava/util/concurrent/locks/Lock;->unlock()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_4

    .line 1257
    :goto_2
    throw v8

    .line 1253
    .restart local v3    # "i":I
    .restart local v4    # "length":I
    .restart local v6    # "startPosition":I
    :catch_3
    move-exception v8

    goto :goto_1

    .end local v3    # "i":I
    .end local v4    # "length":I
    .end local v6    # "startPosition":I
    :catch_4
    move-exception v9

    goto :goto_2
.end method

.method public isValid()Z
    .locals 1

    .prologue
    .line 1138
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mCursor:Lcom/google/android/music/medialist/MediaList$MediaCursor;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public length()I
    .locals 2

    .prologue
    .line 1127
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mCursorLock:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/ReadWriteLock;->readLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 1128
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mCursor:Lcom/google/android/music/medialist/MediaList$MediaCursor;

    if-eqz v0, :cond_0

    .line 1129
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mCursor:Lcom/google/android/music/medialist/MediaList$MediaCursor;

    invoke-virtual {v0}, Lcom/google/android/music/medialist/MediaList$MediaCursor;->getCountSync()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 1133
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mCursorLock:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/ReadWriteLock;->readLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    :goto_0
    return v0

    .line 1131
    :cond_0
    const/4 v0, 0x0

    .line 1133
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mCursorLock:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/ReadWriteLock;->readLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mCursorLock:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/ReadWriteLock;->readLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public playlistLoading()Z
    .locals 1

    .prologue
    .line 1142
    iget-boolean v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mPlaylistLoading:Z

    return v0
.end method

.method public setMediaList(Lcom/google/android/music/medialist/SongList;)V
    .locals 5
    .param p1, "medialist"    # Lcom/google/android/music/medialist/SongList;

    .prologue
    const/4 v4, 0x0

    .line 1074
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mPlaylistLoading:Z

    .line 1080
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mAudioId:Lcom/google/android/music/download/ContentIdentifier;
    invoke-static {v0, v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$2402(Lcom/google/android/music/playback/LocalDevicePlayback;Lcom/google/android/music/download/ContentIdentifier;)Lcom/google/android/music/download/ContentIdentifier;

    .line 1081
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    const-wide/16 v2, 0x0

    # setter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mListItemId:J
    invoke-static {v0, v2, v3}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$1102(Lcom/google/android/music/playback/LocalDevicePlayback;J)J

    .line 1083
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mCursorLock:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 1084
    iput-object p1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mCursorSongList:Lcom/google/android/music/medialist/SongList;

    .line 1085
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mCursorSongList:Lcom/google/android/music/medialist/SongList;

    invoke-direct {p0, v0}, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->resetCursorLocked(Lcom/google/android/music/medialist/SongList;)V

    .line 1086
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mCursorSongList:Lcom/google/android/music/medialist/SongList;

    if-eqz v0, :cond_0

    .line 1087
    invoke-virtual {p1}, Lcom/google/android/music/medialist/SongList;->getSuggestedPositionSearchRadius()I

    move-result v0

    iput v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mRadiusToSearch:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1091
    :cond_0
    iput-boolean v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mPlaylistLoading:Z

    .line 1092
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mCursorLock:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 1094
    return-void

    .line 1091
    :catchall_0
    move-exception v0

    iput-boolean v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mPlaylistLoading:Z

    .line 1092
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mCursorLock:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public setPlaylistLoading(Z)V
    .locals 0
    .param p1, "playlistLoading"    # Z

    .prologue
    .line 1146
    iput-boolean p1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->mPlaylistLoading:Z

    .line 1147
    return-void
.end method
