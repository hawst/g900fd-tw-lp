.class public Lcom/google/android/music/leanback/SongTitleRow;
.super Landroid/support/v17/leanback/widget/Row;
.source "SongTitleRow.java"


# instance fields
.field private final mTitle:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 15
    invoke-direct {p0}, Landroid/support/v17/leanback/widget/Row;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/google/android/music/leanback/SongTitleRow;->mTitle:Ljava/lang/String;

    .line 17
    return-void
.end method


# virtual methods
.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/google/android/music/leanback/SongTitleRow;->mTitle:Ljava/lang/String;

    return-object v0
.end method
