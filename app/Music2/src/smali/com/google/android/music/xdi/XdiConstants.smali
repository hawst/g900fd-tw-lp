.class public final Lcom/google/android/music/xdi/XdiConstants;
.super Ljava/lang/Object;
.source "XdiConstants.java"


# static fields
.field static final BROWSE_EXPLORE_HEADER_IDS:[I

.field static final BROWSE_EXPLORE_HEADER_NAMES:[I

.field static final BROWSE_EXPLORE_INDEX:[I

.field static final BROWSE_INSTANT_MIXES_HEADER_IDS:[I

.field static final BROWSE_INSTANT_MIXES_HEADER_NAMES:[I

.field static final BROWSE_MY_MUSIC_HEADER_IDS:[I

.field static final BROWSE_MY_MUSIC_HEADER_NAMES:[I

.field static final BROWSE_RADIO_HEADER_IDS:[I

.field static final BROWSE_RADIO_HEADER_NAMES:[I

.field static final REASONS:[I

.field public static final SEARCH_HEADER_NAMES:[I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x5

    const/4 v3, 0x2

    const/4 v2, 0x4

    const/4 v1, 0x3

    .line 53
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/music/xdi/XdiConstants;->BROWSE_MY_MUSIC_HEADER_NAMES:[I

    .line 59
    new-array v0, v1, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/google/android/music/xdi/XdiConstants;->BROWSE_MY_MUSIC_HEADER_IDS:[I

    .line 75
    new-array v0, v2, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/google/android/music/xdi/XdiConstants;->BROWSE_EXPLORE_INDEX:[I

    .line 82
    new-array v0, v2, [I

    fill-array-data v0, :array_3

    sput-object v0, Lcom/google/android/music/xdi/XdiConstants;->BROWSE_EXPLORE_HEADER_NAMES:[I

    .line 89
    new-array v0, v2, [I

    fill-array-data v0, :array_4

    sput-object v0, Lcom/google/android/music/xdi/XdiConstants;->BROWSE_EXPLORE_HEADER_IDS:[I

    .line 96
    new-array v0, v4, [I

    fill-array-data v0, :array_5

    sput-object v0, Lcom/google/android/music/xdi/XdiConstants;->REASONS:[I

    .line 104
    new-array v0, v4, [I

    fill-array-data v0, :array_6

    sput-object v0, Lcom/google/android/music/xdi/XdiConstants;->SEARCH_HEADER_NAMES:[I

    .line 116
    new-array v0, v1, [I

    fill-array-data v0, :array_7

    sput-object v0, Lcom/google/android/music/xdi/XdiConstants;->BROWSE_RADIO_HEADER_IDS:[I

    .line 122
    new-array v0, v1, [I

    fill-array-data v0, :array_8

    sput-object v0, Lcom/google/android/music/xdi/XdiConstants;->BROWSE_RADIO_HEADER_NAMES:[I

    .line 131
    new-array v0, v3, [I

    fill-array-data v0, :array_9

    sput-object v0, Lcom/google/android/music/xdi/XdiConstants;->BROWSE_INSTANT_MIXES_HEADER_IDS:[I

    .line 136
    new-array v0, v3, [I

    fill-array-data v0, :array_a

    sput-object v0, Lcom/google/android/music/xdi/XdiConstants;->BROWSE_INSTANT_MIXES_HEADER_NAMES:[I

    return-void

    .line 53
    :array_0
    .array-data 4
        0x7f0b009e
        0x7f0b00a0
        0x7f0b009f
    .end array-data

    .line 59
    :array_1
    .array-data 4
        0x0
        0x1
        0x2
    .end array-data

    .line 75
    :array_2
    .array-data 4
        0x0
        0x1
        0x2
        0x3
    .end array-data

    .line 82
    :array_3
    .array-data 4
        0x7f0b00a6
        0x7f0b00a4
        0x7f0b00a5
        0x7f0b00a7
    .end array-data

    .line 89
    :array_4
    .array-data 4
        0x0
        0x1
        0x2
        0x3
    .end array-data

    .line 96
    :array_5
    .array-data 4
        0x7f0b024e
        0x7f0b024f
        0x7f0b0250
        0x7f0b0251
        0x7f0b00a7
    .end array-data

    .line 104
    :array_6
    .array-data 4
        0x7f0b009f
        0x7f0b00a1
        0x7f0b009e
        0x7f0b00a2
        0x7f0b00a3
    .end array-data

    .line 116
    :array_7
    .array-data 4
        0x1
        0x2
        0x3
    .end array-data

    .line 122
    :array_8
    .array-data 4
        0x7f0b02ed
        0x7f0b00ab
        0x7f0b00aa
    .end array-data

    .line 131
    :array_9
    .array-data 4
        0x4
        0x5
    .end array-data

    .line 136
    :array_a
    .array-data 4
        0x7f0b00ae
        0x7f0b00ad
    .end array-data
.end method
