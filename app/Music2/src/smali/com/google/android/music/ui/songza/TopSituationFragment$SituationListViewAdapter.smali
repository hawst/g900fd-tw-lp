.class Lcom/google/android/music/ui/songza/TopSituationFragment$SituationListViewAdapter;
.super Landroid/widget/BaseAdapter;
.source "TopSituationFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/songza/TopSituationFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SituationListViewAdapter"
.end annotation


# instance fields
.field private mCursor:Landroid/database/Cursor;

.field private mFragment:Lcom/google/android/music/ui/songza/TopSituationFragment;

.field private final mShowDescriptionRow:Z


# direct methods
.method public constructor <init>(Lcom/google/android/music/ui/songza/TopSituationFragment;Z)V
    .locals 0
    .param p1, "fragment"    # Lcom/google/android/music/ui/songza/TopSituationFragment;
    .param p2, "showDescriptionRow"    # Z

    .prologue
    .line 238
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 239
    iput-object p1, p0, Lcom/google/android/music/ui/songza/TopSituationFragment$SituationListViewAdapter;->mFragment:Lcom/google/android/music/ui/songza/TopSituationFragment;

    .line 240
    iput-boolean p2, p0, Lcom/google/android/music/ui/songza/TopSituationFragment$SituationListViewAdapter;->mShowDescriptionRow:Z

    .line 241
    return-void
.end method

.method private getDescriptionView(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1, "convertView"    # Landroid/view/View;
    .param p2, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 294
    if-eqz p1, :cond_0

    move-object v0, p1

    .line 302
    .end local p1    # "convertView":Landroid/view/View;
    .local v0, "convertView":Landroid/view/View;
    :goto_0
    return-object v0

    .line 297
    .end local v0    # "convertView":Landroid/view/View;
    .restart local p1    # "convertView":Landroid/view/View;
    :cond_0
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f0400df

    const/4 v4, 0x0

    invoke-virtual {v2, v3, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    move-object v1, p1

    .line 299
    check-cast v1, Landroid/widget/TextView;

    .line 300
    .local v1, "descriptionView":Landroid/widget/TextView;
    iget-object v2, p0, Lcom/google/android/music/ui/songza/TopSituationFragment$SituationListViewAdapter;->mFragment:Lcom/google/android/music/ui/songza/TopSituationFragment;

    # getter for: Lcom/google/android/music/ui/songza/TopSituationFragment;->mTopSituationDescription:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/music/ui/songza/TopSituationFragment;->access$000(Lcom/google/android/music/ui/songza/TopSituationFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object v0, p1

    .line 302
    .end local p1    # "convertView":Landroid/view/View;
    .restart local v0    # "convertView":Landroid/view/View;
    goto :goto_0
.end method

.method private getPositionInCursor(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 290
    iget-boolean v0, p0, Lcom/google/android/music/ui/songza/TopSituationFragment$SituationListViewAdapter;->mShowDescriptionRow:Z

    if-eqz v0, :cond_0

    add-int/lit8 p1, p1, -0x1

    .end local p1    # "position":I
    :cond_0
    return p1
.end method

.method private getTitleView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v1, 0x1

    const/4 v7, 0x0

    .line 306
    if-nez p2, :cond_0

    .line 307
    new-instance v5, Lcom/google/android/music/ui/songza/TopSituationFragment$SituationViewHolder;

    const/4 v6, 0x0

    invoke-direct {v5, v6}, Lcom/google/android/music/ui/songza/TopSituationFragment$SituationViewHolder;-><init>(Lcom/google/android/music/ui/songza/TopSituationFragment$1;)V

    .line 308
    .local v5, "vh":Lcom/google/android/music/ui/songza/TopSituationFragment$SituationViewHolder;
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v6

    const v8, 0x7f0400ee

    invoke-virtual {v6, v8, p3, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 310
    const v6, 0x7f0e026d

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, v5, Lcom/google/android/music/ui/songza/TopSituationFragment$SituationViewHolder;->mTitleView:Landroid/widget/TextView;

    .line 311
    const v6, 0x7f0e00f9

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    iput-object v6, v5, Lcom/google/android/music/ui/songza/TopSituationFragment$SituationViewHolder;->mDividerLine:Landroid/view/View;

    .line 312
    invoke-virtual {p2, v5}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 315
    .end local v5    # "vh":Lcom/google/android/music/ui/songza/TopSituationFragment$SituationViewHolder;
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/music/ui/songza/TopSituationFragment$SituationViewHolder;

    .line 316
    .restart local v5    # "vh":Lcom/google/android/music/ui/songza/TopSituationFragment$SituationViewHolder;
    new-instance v6, Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-direct {v6}, Lcom/google/android/music/ui/cardlib/model/Document;-><init>()V

    iput-object v6, v5, Lcom/google/android/music/ui/songza/TopSituationFragment$SituationViewHolder;->mDocument:Lcom/google/android/music/ui/cardlib/model/Document;

    .line 317
    iget-object v6, p0, Lcom/google/android/music/ui/songza/TopSituationFragment$SituationListViewAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v6, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 318
    iget-object v6, v5, Lcom/google/android/music/ui/songza/TopSituationFragment$SituationViewHolder;->mDocument:Lcom/google/android/music/ui/cardlib/model/Document;

    sget-object v8, Lcom/google/android/music/ui/cardlib/model/Document$Type;->SUBSITUATION:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    invoke-virtual {v6, v8}, Lcom/google/android/music/ui/cardlib/model/Document;->setType(Lcom/google/android/music/ui/cardlib/model/Document$Type;)V

    .line 319
    iget-object v6, v5, Lcom/google/android/music/ui/songza/TopSituationFragment$SituationViewHolder;->mDocument:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v6, p1}, Lcom/google/android/music/ui/cardlib/model/Document;->setPosition(I)V

    .line 320
    iget-object v6, v5, Lcom/google/android/music/ui/songza/TopSituationFragment$SituationViewHolder;->mDocument:Lcom/google/android/music/ui/cardlib/model/Document;

    const/4 v8, 0x7

    invoke-virtual {v6, v8}, Lcom/google/android/music/ui/cardlib/model/Document;->setMainstageReason(I)V

    .line 322
    iget-object v6, p0, Lcom/google/android/music/ui/songza/TopSituationFragment$SituationListViewAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v6, v7}, Landroid/database/Cursor;->isNull(I)Z

    move-result v6

    if-nez v6, :cond_1

    .line 323
    iget-object v6, p0, Lcom/google/android/music/ui/songza/TopSituationFragment$SituationListViewAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/google/android/music/ui/songza/TopSituationFragment$SituationViewHolder;->mSubSituationId:Ljava/lang/String;

    .line 324
    iget-object v6, v5, Lcom/google/android/music/ui/songza/TopSituationFragment$SituationViewHolder;->mDocument:Lcom/google/android/music/ui/cardlib/model/Document;

    iget-object v8, p0, Lcom/google/android/music/ui/songza/TopSituationFragment$SituationListViewAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v8, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Lcom/google/android/music/ui/cardlib/model/Document;->setSituationId(Ljava/lang/String;)V

    .line 326
    :cond_1
    iget-object v6, p0, Lcom/google/android/music/ui/songza/TopSituationFragment$SituationListViewAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v6, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v6

    if-nez v6, :cond_2

    .line 327
    iget-object v6, p0, Lcom/google/android/music/ui/songza/TopSituationFragment$SituationListViewAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/google/android/music/ui/songza/TopSituationFragment$SituationViewHolder;->mSubSituationTitle:Ljava/lang/String;

    .line 328
    iget-object v6, v5, Lcom/google/android/music/ui/songza/TopSituationFragment$SituationViewHolder;->mTitleView:Landroid/widget/TextView;

    iget-object v8, v5, Lcom/google/android/music/ui/songza/TopSituationFragment$SituationViewHolder;->mSubSituationTitle:Ljava/lang/String;

    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 329
    iget-object v6, v5, Lcom/google/android/music/ui/songza/TopSituationFragment$SituationViewHolder;->mDocument:Lcom/google/android/music/ui/cardlib/model/Document;

    iget-object v8, v5, Lcom/google/android/music/ui/songza/TopSituationFragment$SituationViewHolder;->mSubSituationTitle:Ljava/lang/String;

    invoke-virtual {v6, v8}, Lcom/google/android/music/ui/cardlib/model/Document;->setTitle(Ljava/lang/String;)V

    .line 332
    :cond_2
    iget-object v6, v5, Lcom/google/android/music/ui/songza/TopSituationFragment$SituationViewHolder;->mDocument:Lcom/google/android/music/ui/cardlib/model/Document;

    iget-object v8, p0, Lcom/google/android/music/ui/songza/TopSituationFragment$SituationListViewAdapter;->mFragment:Lcom/google/android/music/ui/songza/TopSituationFragment;

    # getter for: Lcom/google/android/music/ui/songza/TopSituationFragment;->mTopSituationArtUrl:Ljava/lang/String;
    invoke-static {v8}, Lcom/google/android/music/ui/songza/TopSituationFragment;->access$200(Lcom/google/android/music/ui/songza/TopSituationFragment;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Lcom/google/android/music/ui/cardlib/model/Document;->setArtUrl(Ljava/lang/String;)V

    .line 334
    iget-object v6, p0, Lcom/google/android/music/ui/songza/TopSituationFragment$SituationListViewAdapter;->mFragment:Lcom/google/android/music/ui/songza/TopSituationFragment;

    invoke-virtual {v6}, Lcom/google/android/music/ui/songza/TopSituationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/music/eventlog/MusicEventLogger;->getInstance(Landroid/content/Context;)Lcom/google/android/music/eventlog/MusicEventLogger;

    move-result-object v6

    iget-object v8, v5, Lcom/google/android/music/ui/songza/TopSituationFragment$SituationViewHolder;->mDocument:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v6, v8}, Lcom/google/android/music/eventlog/MusicEventLogger;->logMainstageDocumentDisplayed(Lcom/google/android/music/ui/cardlib/model/Document;)V

    .line 338
    :cond_3
    iget-object v6, p0, Lcom/google/android/music/ui/songza/TopSituationFragment$SituationListViewAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    if-ne p1, v6, :cond_6

    .line 339
    .local v1, "lastPosition":Z
    :goto_0
    iget-object v6, v5, Lcom/google/android/music/ui/songza/TopSituationFragment$SituationViewHolder;->mDividerLine:Landroid/view/View;

    if-eqz v1, :cond_4

    const/16 v7, 0x8

    :cond_4
    invoke-virtual {v6, v7}, Landroid/view/View;->setVisibility(I)V

    .line 341
    if-eqz v1, :cond_5

    .line 343
    iget-object v6, v5, Lcom/google/android/music/ui/songza/TopSituationFragment$SituationViewHolder;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getPaddingLeft()I

    move-result v2

    .line 344
    .local v2, "lp":I
    iget-object v6, v5, Lcom/google/android/music/ui/songza/TopSituationFragment$SituationViewHolder;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getPaddingRight()I

    move-result v3

    .line 345
    .local v3, "rp":I
    iget-object v6, v5, Lcom/google/android/music/ui/songza/TopSituationFragment$SituationViewHolder;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getPaddingTop()I

    move-result v4

    .line 346
    .local v4, "tp":I
    iget-object v6, v5, Lcom/google/android/music/ui/songza/TopSituationFragment$SituationViewHolder;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getPaddingBottom()I

    move-result v0

    .line 347
    .local v0, "bp":I
    iget-object v6, p0, Lcom/google/android/music/ui/songza/TopSituationFragment$SituationListViewAdapter;->mFragment:Lcom/google/android/music/ui/songza/TopSituationFragment;

    invoke-virtual {v6}, Lcom/google/android/music/ui/songza/TopSituationFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0f01ad

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    add-int v0, v4, v6

    .line 349
    iget-object v6, v5, Lcom/google/android/music/ui/songza/TopSituationFragment$SituationViewHolder;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v6, v2, v4, v3, v0}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 352
    .end local v0    # "bp":I
    .end local v2    # "lp":I
    .end local v3    # "rp":I
    .end local v4    # "tp":I
    :cond_5
    invoke-virtual {p2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 353
    return-object p2

    .end local v1    # "lastPosition":Z
    :cond_6
    move v1, v7

    .line 338
    goto :goto_0
.end method


# virtual methods
.method public getCount()I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 249
    iget-object v1, p0, Lcom/google/android/music/ui/songza/TopSituationFragment$SituationListViewAdapter;->mCursor:Landroid/database/Cursor;

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/music/ui/songza/TopSituationFragment$SituationListViewAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    iget-boolean v2, p0, Lcom/google/android/music/ui/songza/TopSituationFragment$SituationListViewAdapter;->mShowDescriptionRow:Z

    if-eqz v2, :cond_1

    const/4 v0, 0x1

    :cond_1
    add-int/2addr v0, v1

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 255
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 260
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 270
    if-nez p1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/music/ui/songza/TopSituationFragment$SituationListViewAdapter;->mShowDescriptionRow:Z

    if-eqz v0, :cond_0

    .line 271
    const/4 v0, 0x0

    .line 273
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 279
    invoke-virtual {p0, p1}, Lcom/google/android/music/ui/songza/TopSituationFragment$SituationListViewAdapter;->getItemViewType(I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 285
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 281
    :pswitch_0
    invoke-direct {p0, p2, p3}, Lcom/google/android/music/ui/songza/TopSituationFragment$SituationListViewAdapter;->getDescriptionView(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 283
    :pswitch_1
    invoke-direct {p0, p1}, Lcom/google/android/music/ui/songza/TopSituationFragment$SituationListViewAdapter;->getPositionInCursor(I)I

    move-result v0

    invoke-direct {p0, v0, p2, p3}, Lcom/google/android/music/ui/songza/TopSituationFragment$SituationListViewAdapter;->getTitleView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 279
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 265
    const/4 v0, 0x2

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 369
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 370
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/ui/songza/TopSituationFragment$SituationViewHolder;

    .line 372
    .local v1, "vh":Lcom/google/android/music/ui/songza/TopSituationFragment$SituationViewHolder;
    iget-object v2, p0, Lcom/google/android/music/ui/songza/TopSituationFragment$SituationListViewAdapter;->mFragment:Lcom/google/android/music/ui/songza/TopSituationFragment;

    invoke-virtual {v2}, Lcom/google/android/music/ui/songza/TopSituationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/music/eventlog/MusicEventLogger;->getInstance(Landroid/content/Context;)Lcom/google/android/music/eventlog/MusicEventLogger;

    move-result-object v2

    iget-object v3, v1, Lcom/google/android/music/ui/songza/TopSituationFragment$SituationViewHolder;->mDocument:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v2, v3}, Lcom/google/android/music/eventlog/MusicEventLogger;->logMainstageCardClicked(Lcom/google/android/music/ui/cardlib/model/Document;)V

    .line 375
    new-instance v0, Landroid/content/Intent;

    iget-object v2, p0, Lcom/google/android/music/ui/songza/TopSituationFragment$SituationListViewAdapter;->mFragment:Lcom/google/android/music/ui/songza/TopSituationFragment;

    invoke-virtual {v2}, Lcom/google/android/music/ui/songza/TopSituationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-class v3, Lcom/google/android/music/ui/songza/SubSituationActivity;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 376
    .local v0, "i":Landroid/content/Intent;
    const-string v2, "subSituationId"

    iget-object v3, v1, Lcom/google/android/music/ui/songza/TopSituationFragment$SituationViewHolder;->mSubSituationId:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 377
    const-string v2, "subSituationTitle"

    iget-object v3, v1, Lcom/google/android/music/ui/songza/TopSituationFragment$SituationViewHolder;->mSubSituationTitle:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 378
    const-string v2, "subSituationArtUrl"

    iget-object v3, p0, Lcom/google/android/music/ui/songza/TopSituationFragment$SituationListViewAdapter;->mFragment:Lcom/google/android/music/ui/songza/TopSituationFragment;

    # getter for: Lcom/google/android/music/ui/songza/TopSituationFragment;->mTopSituationArtUrl:Ljava/lang/String;
    invoke-static {v3}, Lcom/google/android/music/ui/songza/TopSituationFragment;->access$200(Lcom/google/android/music/ui/songza/TopSituationFragment;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 380
    iget-object v2, p0, Lcom/google/android/music/ui/songza/TopSituationFragment$SituationListViewAdapter;->mFragment:Lcom/google/android/music/ui/songza/TopSituationFragment;

    invoke-virtual {v2, v0}, Lcom/google/android/music/ui/songza/TopSituationFragment;->startActivity(Landroid/content/Intent;)V

    .line 382
    .end local v0    # "i":Landroid/content/Intent;
    .end local v1    # "vh":Lcom/google/android/music/ui/songza/TopSituationFragment$SituationViewHolder;
    :cond_0
    return-void
.end method

.method public swapCursor(Landroid/database/Cursor;)V
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 357
    iput-object p1, p0, Lcom/google/android/music/ui/songza/TopSituationFragment$SituationListViewAdapter;->mCursor:Landroid/database/Cursor;

    .line 358
    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 360
    iget-object v0, p0, Lcom/google/android/music/ui/songza/TopSituationFragment$SituationListViewAdapter;->mFragment:Lcom/google/android/music/ui/songza/TopSituationFragment;

    invoke-virtual {v0}, Lcom/google/android/music/ui/songza/TopSituationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/music/ui/AppNavigation;->goListenNow(Landroid/content/Context;)V

    .line 364
    :goto_0
    return-void

    .line 362
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/music/ui/songza/TopSituationFragment$SituationListViewAdapter;->notifyDataSetChanged()V

    goto :goto_0
.end method
