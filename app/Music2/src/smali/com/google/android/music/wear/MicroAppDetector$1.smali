.class Lcom/google/android/music/wear/MicroAppDetector$1;
.super Ljava/lang/Object;
.source "MicroAppDetector.java"

# interfaces
.implements Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry$NodeListCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/wear/MicroAppDetector;->refreshMicroAppPresence()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/wear/MicroAppDetector;

.field final synthetic val$client:Lcom/google/android/gms/common/api/GoogleApiClient;


# direct methods
.method constructor <init>(Lcom/google/android/music/wear/MicroAppDetector;Lcom/google/android/gms/common/api/GoogleApiClient;)V
    .locals 0

    .prologue
    .line 90
    iput-object p1, p0, Lcom/google/android/music/wear/MicroAppDetector$1;->this$0:Lcom/google/android/music/wear/MicroAppDetector;

    iput-object p2, p0, Lcom/google/android/music/wear/MicroAppDetector$1;->val$client:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onComplete(ZLjava/util/List;)V
    .locals 1
    .param p1, "success"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 93
    .local p2, "nodeIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz p1, :cond_0

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 95
    iget-object v0, p0, Lcom/google/android/music/wear/MicroAppDetector$1;->this$0:Lcom/google/android/music/wear/MicroAppDetector;

    # getter for: Lcom/google/android/music/wear/MicroAppDetector;->mMusicDataProvider:Lcom/google/android/music/wear/WearMusicDataProvider;
    invoke-static {v0}, Lcom/google/android/music/wear/MicroAppDetector;->access$000(Lcom/google/android/music/wear/MicroAppDetector;)Lcom/google/android/music/wear/WearMusicDataProvider;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/music/wear/WearMusicDataProvider;->markWearSyncAvailable()V

    .line 96
    iget-object v0, p0, Lcom/google/android/music/wear/MicroAppDetector$1;->val$client:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/GoogleApiClient;->disconnect()V

    .line 101
    :goto_0
    return-void

    .line 99
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/wear/MicroAppDetector$1;->val$client:Lcom/google/android/gms/common/api/GoogleApiClient;

    # invokes: Lcom/google/android/music/wear/MicroAppDetector;->pingConnectedNodesAndDisconnect(Lcom/google/android/gms/common/api/GoogleApiClient;)V
    invoke-static {v0}, Lcom/google/android/music/wear/MicroAppDetector;->access$100(Lcom/google/android/gms/common/api/GoogleApiClient;)V

    goto :goto_0
.end method
