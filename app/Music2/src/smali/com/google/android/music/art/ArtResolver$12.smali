.class Lcom/google/android/music/art/ArtResolver$12;
.super Ljava/lang/Object;
.source "ArtResolver.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/art/ArtResolver;->getGenericArtForUrl(Ljava/lang/String;Lcom/google/android/music/art/ArtRequest;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/art/ArtResolver;

.field final synthetic val$request:Lcom/google/android/music/art/ArtRequest;

.field final synthetic val$url:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/music/art/ArtResolver;Ljava/lang/String;Lcom/google/android/music/art/ArtRequest;)V
    .locals 0

    .prologue
    .line 1099
    iput-object p1, p0, Lcom/google/android/music/art/ArtResolver$12;->this$0:Lcom/google/android/music/art/ArtResolver;

    iput-object p2, p0, Lcom/google/android/music/art/ArtResolver$12;->val$url:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/music/art/ArtResolver$12;->val$request:Lcom/google/android/music/art/ArtRequest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 1102
    iget-object v1, p0, Lcom/google/android/music/art/ArtResolver$12;->this$0:Lcom/google/android/music/art/ArtResolver;

    # getter for: Lcom/google/android/music/art/ArtResolver;->mAppContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/google/android/music/art/ArtResolver;->access$200(Lcom/google/android/music/art/ArtResolver;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/art/ArtResolver$12;->val$url:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/music/utils/AlbumArtUtils;->getBitmapFromDisk(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1104
    .local v0, "bmp":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_1

    .line 1105
    iget-object v1, p0, Lcom/google/android/music/art/ArtResolver$12;->this$0:Lcom/google/android/music/art/ArtResolver;

    # getter for: Lcom/google/android/music/art/ArtResolver;->mMemoryCacheLock:Ljava/lang/Object;
    invoke-static {v1}, Lcom/google/android/music/art/ArtResolver;->access$1100(Lcom/google/android/music/art/ArtResolver;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    .line 1106
    :try_start_0
    iget-object v1, p0, Lcom/google/android/music/art/ArtResolver$12;->this$0:Lcom/google/android/music/art/ArtResolver;

    # getter for: Lcom/google/android/music/art/ArtResolver;->mMemoryCache:Lcom/google/android/music/art/BitmapLruCache;
    invoke-static {v1}, Lcom/google/android/music/art/ArtResolver;->access$1200(Lcom/google/android/music/art/ArtResolver;)Lcom/google/android/music/art/BitmapLruCache;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1107
    iget-object v1, p0, Lcom/google/android/music/art/ArtResolver$12;->this$0:Lcom/google/android/music/art/ArtResolver;

    # getter for: Lcom/google/android/music/art/ArtResolver;->mMemoryCache:Lcom/google/android/music/art/BitmapLruCache;
    invoke-static {v1}, Lcom/google/android/music/art/ArtResolver;->access$1200(Lcom/google/android/music/art/ArtResolver;)Lcom/google/android/music/art/BitmapLruCache;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/music/art/ArtResolver$12;->val$url:Ljava/lang/String;

    invoke-virtual {v1, v3, v0}, Lcom/google/android/music/art/BitmapLruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1109
    :cond_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1110
    iget-object v1, p0, Lcom/google/android/music/art/ArtResolver$12;->val$request:Lcom/google/android/music/art/ArtRequest;

    iget-object v2, p0, Lcom/google/android/music/art/ArtResolver$12;->this$0:Lcom/google/android/music/art/ArtResolver;

    # getter for: Lcom/google/android/music/art/ArtResolver;->mAppContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/google/android/music/art/ArtResolver;->access$200(Lcom/google/android/music/art/ArtResolver;)Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/music/art/ArtResolver$12;->val$url:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v0}, Lcom/google/android/music/art/ArtRequest;->notifyImageLoaded(Landroid/content/Context;Ljava/lang/Object;Landroid/graphics/Bitmap;)Z

    .line 1114
    :goto_0
    return-void

    .line 1109
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    .line 1112
    :cond_1
    iget-object v1, p0, Lcom/google/android/music/art/ArtResolver$12;->val$request:Lcom/google/android/music/art/ArtRequest;

    iget-object v2, p0, Lcom/google/android/music/art/ArtResolver$12;->this$0:Lcom/google/android/music/art/ArtResolver;

    # getter for: Lcom/google/android/music/art/ArtResolver;->mAppContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/google/android/music/art/ArtResolver;->access$200(Lcom/google/android/music/art/ArtResolver;)Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/music/art/ArtResolver$12;->val$url:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/music/art/ArtRequest;->notifyError(Landroid/content/Context;Ljava/lang/Object;)Z

    goto :goto_0
.end method
