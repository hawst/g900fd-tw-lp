.class public Lcom/google/android/music/store/StoreService;
.super Lcom/google/android/music/store/IStoreService$Stub;
.source "StoreService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/store/StoreService$1;,
        Lcom/google/android/music/store/StoreService$StoreServiceBinder;
    }
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field private final mStore:Lcom/google/android/music/store/Store;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/google/android/music/store/IStoreService$Stub;-><init>()V

    .line 25
    invoke-static {p1}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/store/StoreService;->mStore:Lcom/google/android/music/store/Store;

    .line 26
    iput-object p1, p0, Lcom/google/android/music/store/StoreService;->mContext:Landroid/content/Context;

    .line 27
    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;Lcom/google/android/music/store/StoreService$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/content/Context;
    .param p2, "x1"    # Lcom/google/android/music/store/StoreService$1;

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lcom/google/android/music/store/StoreService;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public getArtistIdsForAlbum(J)[J
    .locals 7
    .param p1, "albumId"    # J

    .prologue
    .line 46
    iget-object v3, p0, Lcom/google/android/music/store/StoreService;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v3, p1, p2}, Lcom/google/android/music/store/Store;->getArtistIdsForAlbum(J)Landroid/database/Cursor;

    move-result-object v0

    .line 47
    .local v0, "c":Landroid/database/Cursor;
    if-nez v0, :cond_0

    .line 48
    const/4 v2, 0x0

    .line 58
    :goto_0
    return-object v2

    .line 51
    :cond_0
    :try_start_0
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v3

    new-array v2, v3, [J

    .line 52
    .local v2, "ret":[J
    const/4 v1, 0x0

    .line 53
    .local v1, "i":I
    :goto_1
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 54
    const/4 v3, 0x0

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    aput-wide v4, v2, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 58
    .end local v1    # "i":I
    .end local v2    # "ret":[J
    :catchall_0
    move-exception v3

    invoke-static {v0}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v3

    .restart local v1    # "i":I
    .restart local v2    # "ret":[J
    :cond_1
    invoke-static {v0}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto :goto_0
.end method

.method public getSizeAlbum(J)J
    .locals 3
    .param p1, "albumId"    # J

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/music/store/StoreService;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/music/store/Store;->getSizeAlbum(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public getSizeAutoPlaylist(J)J
    .locals 3
    .param p1, "autoPlaylistId"    # J

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/music/store/StoreService;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/music/store/Store;->getSizeAutoPlaylist(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public getSizeKeeponRadioStation(J)J
    .locals 3
    .param p1, "radioStationId"    # J

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/music/store/StoreService;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/music/store/Store;->getSizeKeeponRadioStation(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public getSizePlaylist(J)J
    .locals 3
    .param p1, "playlistId"    # J

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/music/store/StoreService;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/music/store/Store;->getSizePlaylist(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public insertLockerRecommendations(Ljava/lang/String;)V
    .locals 4
    .param p1, "responseJson"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 104
    :try_start_0
    const-class v2, Lcom/google/android/music/cloudclient/ListenNowRecommendationsJson;

    invoke-static {v2, p1}, Lcom/google/android/music/cloudclient/JsonUtils;->parseFromJsonString(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/cloudclient/ListenNowRecommendationsJson;

    .line 106
    .local v1, "response":Lcom/google/android/music/cloudclient/ListenNowRecommendationsJson;
    iget-object v2, p0, Lcom/google/android/music/store/StoreService;->mContext:Landroid/content/Context;

    invoke-static {v2, v1}, Lcom/google/android/music/store/RecentItemsManager;->insertLockerRecommendations(Landroid/content/Context;Lcom/google/android/music/cloudclient/ListenNowRecommendationsJson;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 110
    .end local v1    # "response":Lcom/google/android/music/cloudclient/ListenNowRecommendationsJson;
    :goto_0
    return-void

    .line 107
    :catch_0
    move-exception v0

    .line 108
    .local v0, "e":Ljava/io/IOException;
    const-string v2, "StoreService"

    const-string v3, "Error parsing NewUserQuizResultJson"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public isAlbumSelectedAsKeepOn(J)Z
    .locals 1
    .param p1, "albumId"    # J

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/music/store/StoreService;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/music/store/Store;->isAlbumSelectedAsKeepOn(J)Z

    move-result v0

    return v0
.end method

.method public isAutoPlaylistSelectedAsKeepOn(J)Z
    .locals 1
    .param p1, "playlistId"    # J

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/music/store/StoreService;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/music/store/Store;->isAutoPlaylistSelectedAsKeepOn(J)Z

    move-result v0

    return v0
.end method

.method public isPlaylistSelectedAsKeepOn(J)Z
    .locals 1
    .param p1, "playlistId"    # J

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/music/store/StoreService;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/music/store/Store;->isPlaylistSelectedAsKeepOn(J)Z

    move-result v0

    return v0
.end method

.method public isRadioStationSelectedAsKeepOn(J)Z
    .locals 1
    .param p1, "radioStationId"    # J

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/music/store/StoreService;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/music/store/Store;->isRadioStationSelectedAsKeepOn(J)Z

    move-result v0

    return v0
.end method
