.class public Lcom/google/android/music/art/DocumentArtDescriptor;
.super Lcom/google/android/music/art/ArtDescriptor;
.source "DocumentArtDescriptor.java"


# instance fields
.field public final document:Lcom/google/android/music/ui/cardlib/model/Document;


# direct methods
.method public constructor <init>(Lcom/google/android/music/art/ArtType;IFLcom/google/android/music/ui/cardlib/model/Document;)V
    .locals 1
    .param p1, "artType"    # Lcom/google/android/music/art/ArtType;
    .param p2, "sizeBucket"    # I
    .param p3, "aspectRatio"    # F
    .param p4, "document"    # Lcom/google/android/music/ui/cardlib/model/Document;

    .prologue
    .line 16
    sget-object v0, Lcom/google/android/music/art/ArtDescriptor$SizeHandling;->SLOPPY:Lcom/google/android/music/art/ArtDescriptor$SizeHandling;

    invoke-direct {p0, p1, v0, p2, p3}, Lcom/google/android/music/art/ArtDescriptor;-><init>(Lcom/google/android/music/art/ArtType;Lcom/google/android/music/art/ArtDescriptor$SizeHandling;IF)V

    .line 17
    invoke-static {p4}, Lcom/google/android/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 18
    iput-object p4, p0, Lcom/google/android/music/art/DocumentArtDescriptor;->document:Lcom/google/android/music/ui/cardlib/model/Document;

    .line 19
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 26
    if-ne p0, p1, :cond_1

    const/4 v1, 0x1

    .line 32
    :cond_0
    :goto_0
    return v1

    .line 27
    :cond_1
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-ne v2, v3, :cond_0

    move-object v0, p1

    .line 29
    check-cast v0, Lcom/google/android/music/art/DocumentArtDescriptor;

    .line 31
    .local v0, "that":Lcom/google/android/music/art/DocumentArtDescriptor;
    iget-object v2, p0, Lcom/google/android/music/art/DocumentArtDescriptor;->document:Lcom/google/android/music/ui/cardlib/model/Document;

    iget-object v3, v0, Lcom/google/android/music/art/DocumentArtDescriptor;->document:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v2, v3}, Lcom/google/android/music/ui/cardlib/model/Document;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 32
    invoke-super {p0, p1}, Lcom/google/android/music/art/ArtDescriptor;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 37
    invoke-super {p0}, Lcom/google/android/music/art/ArtDescriptor;->hashCode()I

    move-result v0

    .line 38
    .local v0, "result":I
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/music/art/DocumentArtDescriptor;->document:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v2}, Lcom/google/android/music/ui/cardlib/model/Document;->hashCode()I

    move-result v2

    add-int v0, v1, v2

    .line 39
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 44
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DocumentArtDescriptor{document.title="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/art/DocumentArtDescriptor;->document:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v1}, Lcom/google/android/music/ui/cardlib/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
