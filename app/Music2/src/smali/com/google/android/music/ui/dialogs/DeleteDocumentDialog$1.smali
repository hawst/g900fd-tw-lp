.class Lcom/google/android/music/ui/dialogs/DeleteDocumentDialog$1;
.super Ljava/lang/Object;
.source "DeleteDocumentDialog.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/dialogs/DeleteDocumentDialog;->onOkClicked()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/dialogs/DeleteDocumentDialog;

.field final synthetic val$docType:I

.field final synthetic val$itemId:J

.field final synthetic val$resolver:Landroid/content/ContentResolver;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/dialogs/DeleteDocumentDialog;IJLandroid/content/ContentResolver;)V
    .locals 1

    .prologue
    .line 53
    iput-object p1, p0, Lcom/google/android/music/ui/dialogs/DeleteDocumentDialog$1;->this$0:Lcom/google/android/music/ui/dialogs/DeleteDocumentDialog;

    iput p2, p0, Lcom/google/android/music/ui/dialogs/DeleteDocumentDialog$1;->val$docType:I

    iput-wide p3, p0, Lcom/google/android/music/ui/dialogs/DeleteDocumentDialog$1;->val$itemId:J

    iput-object p5, p0, Lcom/google/android/music/ui/dialogs/DeleteDocumentDialog$1;->val$resolver:Landroid/content/ContentResolver;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 56
    iget v2, p0, Lcom/google/android/music/ui/dialogs/DeleteDocumentDialog$1;->val$docType:I

    invoke-static {v2}, Lcom/google/android/music/ui/cardlib/model/Document$Type;->fromOrdinal(I)Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v1

    .line 58
    .local v1, "type":Lcom/google/android/music/ui/cardlib/model/Document$Type;
    sget-object v2, Lcom/google/android/music/ui/cardlib/model/Document$Type;->PLAYLIST:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-ne v1, v2, :cond_1

    .line 59
    iget-wide v2, p0, Lcom/google/android/music/ui/dialogs/DeleteDocumentDialog$1;->val$itemId:J

    invoke-static {v2, v3}, Lcom/google/android/music/store/MusicContent$Playlists;->getPlaylistUri(J)Landroid/net/Uri;

    move-result-object v0

    .line 60
    .local v0, "playListUri":Landroid/net/Uri;
    iget-object v2, p0, Lcom/google/android/music/ui/dialogs/DeleteDocumentDialog$1;->val$resolver:Landroid/content/ContentResolver;

    invoke-virtual {v2, v0, v4, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 65
    .end local v0    # "playListUri":Landroid/net/Uri;
    :cond_0
    :goto_0
    return-void

    .line 61
    :cond_1
    sget-object v2, Lcom/google/android/music/ui/cardlib/model/Document$Type;->RADIO:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-ne v1, v2, :cond_0

    .line 62
    iget-wide v2, p0, Lcom/google/android/music/ui/dialogs/DeleteDocumentDialog$1;->val$itemId:J

    invoke-static {v2, v3}, Lcom/google/android/music/store/MusicContent$RadioStations;->getRadioStationUri(J)Landroid/net/Uri;

    move-result-object v0

    .line 63
    .restart local v0    # "playListUri":Landroid/net/Uri;
    iget-object v2, p0, Lcom/google/android/music/ui/dialogs/DeleteDocumentDialog$1;->val$resolver:Landroid/content/ContentResolver;

    invoke-virtual {v2, v0, v4, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0
.end method
