.class Lcom/google/android/music/playback/MusicPlaybackService$7;
.super Ljava/lang/Object;
.source "MusicPlaybackService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/playback/MusicPlaybackService;->notifyChange(Ljava/lang/String;Lcom/google/android/music/playback/DevicePlayback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/playback/MusicPlaybackService;

.field final synthetic val$intent:Landroid/content/Intent;

.field final synthetic val$what:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/music/playback/MusicPlaybackService;Landroid/content/Intent;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 931
    iput-object p1, p0, Lcom/google/android/music/playback/MusicPlaybackService$7;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    iput-object p2, p0, Lcom/google/android/music/playback/MusicPlaybackService$7;->val$intent:Landroid/content/Intent;

    iput-object p3, p0, Lcom/google/android/music/playback/MusicPlaybackService$7;->val$what:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 942
    iget-object v1, p0, Lcom/google/android/music/playback/MusicPlaybackService$7;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    const/4 v2, 0x1

    # invokes: Lcom/google/android/music/playback/MusicPlaybackService;->updateMediaSessionMetadata(Z)V
    invoke-static {v1, v2}, Lcom/google/android/music/playback/MusicPlaybackService;->access$1200(Lcom/google/android/music/playback/MusicPlaybackService;Z)V

    .line 943
    iget-object v1, p0, Lcom/google/android/music/playback/MusicPlaybackService$7;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    iget-object v2, p0, Lcom/google/android/music/playback/MusicPlaybackService$7;->val$intent:Landroid/content/Intent;

    iget-object v3, p0, Lcom/google/android/music/playback/MusicPlaybackService$7;->val$what:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lcom/google/android/music/homewidgets/NowPlayingWidgetProvider;->notifyChange(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/String;)V

    .line 946
    iget-object v1, p0, Lcom/google/android/music/playback/MusicPlaybackService$7;->val$intent:Landroid/content/Intent;

    const-string v2, "inErrorState"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 948
    .local v0, "errorState":Z
    iget-object v1, p0, Lcom/google/android/music/playback/MusicPlaybackService$7;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    iget-object v2, p0, Lcom/google/android/music/playback/MusicPlaybackService$7;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    iget-object v3, p0, Lcom/google/android/music/playback/MusicPlaybackService$7;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v3}, Lcom/google/android/music/playback/MusicPlaybackService;->getErrorType()I

    move-result v3

    # invokes: Lcom/google/android/music/playback/MusicPlaybackService;->getPlaybackErrorString(I)Ljava/lang/String;
    invoke-static {v2, v3}, Lcom/google/android/music/playback/MusicPlaybackService;->access$1300(Lcom/google/android/music/playback/MusicPlaybackService;I)Ljava/lang/String;

    move-result-object v2

    # invokes: Lcom/google/android/music/playback/MusicPlaybackService;->updateMediaSessionPlaystate(ZLjava/lang/String;)V
    invoke-static {v1, v0, v2}, Lcom/google/android/music/playback/MusicPlaybackService;->access$800(Lcom/google/android/music/playback/MusicPlaybackService;ZLjava/lang/String;)V

    .line 950
    return-void
.end method
