.class public Lcom/google/android/music/medialist/RecentItemsList;
.super Lcom/google/android/music/medialist/MediaList;
.source "RecentItemsList.java"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/music/medialist/RecentItemsList;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    new-instance v0, Lcom/google/android/music/medialist/RecentItemsList$1;

    invoke-direct {v0}, Lcom/google/android/music/medialist/RecentItemsList$1;-><init>()V

    sput-object v0, Lcom/google/android/music/medialist/RecentItemsList;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 16
    sget-object v0, Lcom/google/android/music/download/ContentIdentifier$Domain;->DEFAULT:Lcom/google/android/music/download/ContentIdentifier$Domain;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/music/medialist/MediaList;-><init>(Lcom/google/android/music/download/ContentIdentifier$Domain;ZZ)V

    .line 18
    return-void
.end method


# virtual methods
.method public getContentUri(Landroid/content/Context;)Landroid/net/Uri;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 22
    sget-object v0, Lcom/google/android/music/store/MusicContent$Recent;->CONTENT_URI:Landroid/net/Uri;

    return-object v0
.end method
