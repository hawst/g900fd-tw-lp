.class public Lcom/google/android/music/store/BlacklistItem;
.super Lcom/google/android/music/store/Syncable;
.source "BlacklistItem.java"


# static fields
.field private static final PROJECTION:[Ljava/lang/String;


# instance fields
.field private mAlbumArtist:Ljava/lang/String;

.field private mAlbumLocalId:J

.field private mAlbumMetajamId:Ljava/lang/String;

.field private mAlbumTitle:Ljava/lang/String;

.field private mDismissDate:J

.field private mId:J

.field private mListShareToken:Ljava/lang/String;

.field private mRadioRemoteId:Ljava/lang/String;

.field private mRadioSeedId:Ljava/lang/String;

.field private mRadioSeedType:I

.field private mReasonType:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 69
    const/16 v0, 0xf

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "MAINSTAGE_BLACKLIST.Id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "MAINSTAGE_BLACKLIST.AlbumArtist"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "MAINSTAGE_BLACKLIST.AlbumTitle"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "MAINSTAGE_BLACKLIST.AlbumLocalId"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "MAINSTAGE_BLACKLIST.AlbumMetajamId"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "MAINSTAGE_BLACKLIST.ListShareToken"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "MAINSTAGE_BLACKLIST.RadioRemoteId"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "MAINSTAGE_BLACKLIST.RadioSeedId"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "MAINSTAGE_BLACKLIST.RadioSeedType"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "MAINSTAGE_BLACKLIST.ReasonType"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "MAINSTAGE_BLACKLIST.DismissDate"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "MAINSTAGE_BLACKLIST._sync_version"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "MAINSTAGE_BLACKLIST.SourceAccount"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "MAINSTAGE_BLACKLIST.SourceId"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "MAINSTAGE_BLACKLIST._sync_dirty"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/store/BlacklistItem;->PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/google/android/music/store/Syncable;-><init>()V

    return-void
.end method

.method private addNullAlbumBindings(Landroid/database/sqlite/SQLiteStatement;)V
    .locals 1
    .param p1, "statement"    # Landroid/database/sqlite/SQLiteStatement;

    .prologue
    .line 396
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteStatement;->bindNull(I)V

    .line 397
    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteStatement;->bindNull(I)V

    .line 398
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteStatement;->bindNull(I)V

    .line 399
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteStatement;->bindNull(I)V

    .line 400
    return-void
.end method

.method private addNullListBindings(Landroid/database/sqlite/SQLiteStatement;)V
    .locals 1
    .param p1, "statement"    # Landroid/database/sqlite/SQLiteStatement;

    .prologue
    .line 403
    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteStatement;->bindNull(I)V

    .line 404
    return-void
.end method

.method private addNullRadioBindings(Landroid/database/sqlite/SQLiteStatement;)V
    .locals 1
    .param p1, "statement"    # Landroid/database/sqlite/SQLiteStatement;

    .prologue
    .line 407
    const/4 v0, 0x6

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteStatement;->bindNull(I)V

    .line 408
    const/4 v0, 0x7

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteStatement;->bindNull(I)V

    .line 409
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteStatement;->bindNull(I)V

    .line 410
    return-void
.end method

.method public static compileInsertStatement(Landroid/database/sqlite/SQLiteDatabase;)Landroid/database/sqlite/SQLiteStatement;
    .locals 1
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 299
    const-string v0, "insert into MAINSTAGE_BLACKLIST ( AlbumArtist, AlbumTitle, AlbumLocalId, AlbumMetajamId, ListShareToken, RadioRemoteId, RadioSeedId, RadioSeedType, ReasonType, DismissDate, _sync_version, SourceAccount, SourceId, _sync_dirty) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v0

    return-object v0
.end method

.method public static getBlacklistItemsToSync(Landroid/database/sqlite/SQLiteDatabase;)Landroid/database/Cursor;
    .locals 8
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    const/4 v4, 0x0

    .line 199
    const-string v1, "MAINSTAGE_BLACKLIST"

    sget-object v2, Lcom/google/android/music/store/BlacklistItem;->PROJECTION:[Ljava/lang/String;

    const-string v3, "MAINSTAGE_BLACKLIST._sync_dirty=1"

    move-object v0, p0

    move-object v5, v4

    move-object v6, v4

    move-object v7, v4

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method private prepareInsertOrFullUpdate(Landroid/database/sqlite/SQLiteStatement;)V
    .locals 8
    .param p1, "statement"    # Landroid/database/sqlite/SQLiteStatement;

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const-wide/16 v0, 0x0

    .line 321
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    .line 323
    iget-wide v2, p0, Lcom/google/android/music/store/BlacklistItem;->mAlbumLocalId:J

    cmp-long v2, v2, v0

    if-gez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/music/store/BlacklistItem;->mAlbumMetajamId:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 324
    :cond_0
    iget-wide v2, p0, Lcom/google/android/music/store/BlacklistItem;->mAlbumLocalId:J

    cmp-long v2, v2, v0

    if-gez v2, :cond_2

    .line 325
    invoke-virtual {p1, v6}, Landroid/database/sqlite/SQLiteStatement;->bindNull(I)V

    .line 330
    :goto_0
    iget-object v2, p0, Lcom/google/android/music/store/BlacklistItem;->mAlbumMetajamId:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 331
    invoke-virtual {p1, v7}, Landroid/database/sqlite/SQLiteStatement;->bindNull(I)V

    .line 336
    :goto_1
    iget-object v2, p0, Lcom/google/android/music/store/BlacklistItem;->mAlbumArtist:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 337
    invoke-virtual {p1, v4}, Landroid/database/sqlite/SQLiteStatement;->bindNull(I)V

    .line 342
    :goto_2
    iget-object v2, p0, Lcom/google/android/music/store/BlacklistItem;->mAlbumTitle:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 343
    invoke-virtual {p1, v5}, Landroid/database/sqlite/SQLiteStatement;->bindNull(I)V

    .line 348
    :goto_3
    invoke-direct {p0, p1}, Lcom/google/android/music/store/BlacklistItem;->addNullListBindings(Landroid/database/sqlite/SQLiteStatement;)V

    .line 349
    invoke-direct {p0, p1}, Lcom/google/android/music/store/BlacklistItem;->addNullRadioBindings(Landroid/database/sqlite/SQLiteStatement;)V

    .line 375
    :goto_4
    const/16 v2, 0xa

    iget-wide v4, p0, Lcom/google/android/music/store/BlacklistItem;->mDismissDate:J

    invoke-virtual {p1, v2, v4, v5}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 376
    const/16 v2, 0x9

    iget v3, p0, Lcom/google/android/music/store/BlacklistItem;->mReasonType:I

    int-to-long v4, v3

    invoke-virtual {p1, v2, v4, v5}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 377
    const/16 v2, 0xe

    iget-boolean v3, p0, Lcom/google/android/music/store/BlacklistItem;->mNeedsSync:Z

    if-eqz v3, :cond_1

    const-wide/16 v0, 0x1

    :cond_1
    invoke-virtual {p1, v2, v0, v1}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 380
    iget-object v0, p0, Lcom/google/android/music/store/BlacklistItem;->mSourceVersion:Ljava/lang/String;

    if-nez v0, :cond_b

    .line 381
    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteStatement;->bindNull(I)V

    .line 386
    :goto_5
    const/16 v0, 0xc

    iget v1, p0, Lcom/google/android/music/store/BlacklistItem;->mSourceAccount:I

    int-to-long v2, v1

    invoke-virtual {p1, v0, v2, v3}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 388
    iget-object v0, p0, Lcom/google/android/music/store/BlacklistItem;->mSourceId:Ljava/lang/String;

    if-nez v0, :cond_c

    .line 389
    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteStatement;->bindNull(I)V

    .line 393
    :goto_6
    return-void

    .line 327
    :cond_2
    iget-wide v2, p0, Lcom/google/android/music/store/BlacklistItem;->mAlbumLocalId:J

    invoke-virtual {p1, v6, v2, v3}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    goto :goto_0

    .line 333
    :cond_3
    iget-object v2, p0, Lcom/google/android/music/store/BlacklistItem;->mAlbumMetajamId:Ljava/lang/String;

    invoke-virtual {p1, v7, v2}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    goto :goto_1

    .line 339
    :cond_4
    iget-object v2, p0, Lcom/google/android/music/store/BlacklistItem;->mAlbumArtist:Ljava/lang/String;

    invoke-virtual {p1, v4, v2}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    goto :goto_2

    .line 345
    :cond_5
    iget-object v2, p0, Lcom/google/android/music/store/BlacklistItem;->mAlbumTitle:Ljava/lang/String;

    invoke-virtual {p1, v5, v2}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    goto :goto_3

    .line 350
    :cond_6
    iget-object v2, p0, Lcom/google/android/music/store/BlacklistItem;->mListShareToken:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 351
    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/android/music/store/BlacklistItem;->mListShareToken:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 352
    invoke-direct {p0, p1}, Lcom/google/android/music/store/BlacklistItem;->addNullAlbumBindings(Landroid/database/sqlite/SQLiteStatement;)V

    .line 353
    invoke-direct {p0, p1}, Lcom/google/android/music/store/BlacklistItem;->addNullRadioBindings(Landroid/database/sqlite/SQLiteStatement;)V

    goto :goto_4

    .line 354
    :cond_7
    iget-object v2, p0, Lcom/google/android/music/store/BlacklistItem;->mRadioSeedId:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_a

    .line 355
    const/4 v2, 0x7

    iget-object v3, p0, Lcom/google/android/music/store/BlacklistItem;->mRadioSeedId:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 358
    iget-object v2, p0, Lcom/google/android/music/store/BlacklistItem;->mRadioRemoteId:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 359
    const/4 v2, 0x6

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteStatement;->bindNull(I)V

    .line 364
    :goto_7
    iget v2, p0, Lcom/google/android/music/store/BlacklistItem;->mRadioSeedType:I

    if-gez v2, :cond_9

    .line 365
    new-instance v0, Lcom/google/android/music/store/InvalidDataException;

    const-string v1, "Radio seed type must be set before storing"

    invoke-direct {v0, v1}, Lcom/google/android/music/store/InvalidDataException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 361
    :cond_8
    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/android/music/store/BlacklistItem;->mRadioRemoteId:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    goto :goto_7

    .line 367
    :cond_9
    const/16 v2, 0x8

    iget v3, p0, Lcom/google/android/music/store/BlacklistItem;->mRadioSeedType:I

    int-to-long v4, v3

    invoke-virtual {p1, v2, v4, v5}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 369
    invoke-direct {p0, p1}, Lcom/google/android/music/store/BlacklistItem;->addNullAlbumBindings(Landroid/database/sqlite/SQLiteStatement;)V

    .line 370
    invoke-direct {p0, p1}, Lcom/google/android/music/store/BlacklistItem;->addNullListBindings(Landroid/database/sqlite/SQLiteStatement;)V

    goto/16 :goto_4

    .line 372
    :cond_a
    new-instance v0, Lcom/google/android/music/store/InvalidDataException;

    const-string v1, "No identifier set before storing"

    invoke-direct {v0, v1}, Lcom/google/android/music/store/InvalidDataException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 383
    :cond_b
    const/16 v0, 0xb

    iget-object v1, p0, Lcom/google/android/music/store/BlacklistItem;->mSourceVersion:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    goto/16 :goto_5

    .line 391
    :cond_c
    const/16 v0, 0xd

    iget-object v1, p0, Lcom/google/android/music/store/BlacklistItem;->mSourceId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    goto/16 :goto_6
.end method

.method public static replaceStaleItems(Landroid/accounts/Account;Lcom/google/android/music/store/Store;Ljava/util/List;)J
    .locals 14
    .param p0, "account"    # Landroid/accounts/Account;
    .param p1, "store"    # Lcom/google/android/music/store/Store;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            "Lcom/google/android/music/store/Store;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/store/BlacklistItem;",
            ">;)J"
        }
    .end annotation

    .prologue
    .line 423
    .local p2, "items":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/store/BlacklistItem;>;"
    invoke-virtual {p1}, Lcom/google/android/music/store/Store;->beginWriteTxn()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 424
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v6, 0x0

    .line 425
    .local v6, "success":Z
    const-wide/16 v8, 0x0

    .line 427
    .local v8, "updateMin":J
    if-nez p2, :cond_0

    .line 428
    const-string v11, "BlacklistItem"

    const-string v12, "No list proivded to replace stale blacklist items"

    invoke-static {v11, v12}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 429
    const-wide/16 v12, 0x0

    .line 453
    :goto_0
    return-wide v12

    .line 433
    :cond_0
    :try_start_0
    const-string v7, "_sync_dirty=?"

    .line 435
    .local v7, "where":Ljava/lang/String;
    const/4 v11, 0x1

    new-array v10, v11, [Ljava/lang/String;

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-static {v12}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    .line 436
    .local v10, "whereArgs":[Ljava/lang/String;
    const-string v11, "MAINSTAGE_BLACKLIST"

    invoke-virtual {v0, v11, v7, v10}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 437
    .local v1, "deleted":I
    const-string v11, "BlacklistItem"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " blacklist items deleted."

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 438
    invoke-static {v0}, Lcom/google/android/music/store/BlacklistItem;->compileInsertStatement(Landroid/database/sqlite/SQLiteDatabase;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v4

    .line 439
    .local v4, "insertBlacklistItemStatement":Landroid/database/sqlite/SQLiteStatement;
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/music/store/BlacklistItem;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 441
    .local v5, "item":Lcom/google/android/music/store/BlacklistItem;
    :try_start_1
    invoke-virtual {v5, v4}, Lcom/google/android/music/store/BlacklistItem;->insert(Landroid/database/sqlite/SQLiteStatement;)J

    .line 442
    iget-wide v12, v5, Lcom/google/android/music/store/BlacklistItem;->mDismissDate:J

    cmp-long v11, v8, v12

    if-gez v11, :cond_1

    .line 443
    iget-wide v8, v5, Lcom/google/android/music/store/BlacklistItem;->mDismissDate:J
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 445
    :catch_0
    move-exception v2

    .line 446
    .local v2, "e":Ljava/lang/RuntimeException;
    :try_start_2
    const-string v11, "BlacklistItem"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Ignoring blacklist item: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 451
    .end local v1    # "deleted":I
    .end local v2    # "e":Ljava/lang/RuntimeException;
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "insertBlacklistItemStatement":Landroid/database/sqlite/SQLiteStatement;
    .end local v5    # "item":Lcom/google/android/music/store/BlacklistItem;
    .end local v7    # "where":Ljava/lang/String;
    .end local v10    # "whereArgs":[Ljava/lang/String;
    :catchall_0
    move-exception v11

    invoke-virtual {p1, v0, v6}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    throw v11

    .line 449
    .restart local v1    # "deleted":I
    .restart local v3    # "i$":Ljava/util/Iterator;
    .restart local v4    # "insertBlacklistItemStatement":Landroid/database/sqlite/SQLiteStatement;
    .restart local v7    # "where":Ljava/lang/String;
    .restart local v10    # "whereArgs":[Ljava/lang/String;
    :cond_2
    const/4 v6, 0x1

    .line 451
    invoke-virtual {p1, v0, v6}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    move-wide v12, v8

    .line 453
    goto :goto_0
.end method


# virtual methods
.method public getAlbumArtist()Ljava/lang/String;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/google/android/music/store/BlacklistItem;->mAlbumArtist:Ljava/lang/String;

    return-object v0
.end method

.method public getAlbumLocalId()J
    .locals 2

    .prologue
    .line 150
    iget-wide v0, p0, Lcom/google/android/music/store/BlacklistItem;->mAlbumLocalId:J

    return-wide v0
.end method

.method public getAlbumMetajamId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lcom/google/android/music/store/BlacklistItem;->mAlbumMetajamId:Ljava/lang/String;

    return-object v0
.end method

.method public getAlbumTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcom/google/android/music/store/BlacklistItem;->mAlbumTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getDismissDate()J
    .locals 2

    .prologue
    .line 192
    iget-wide v0, p0, Lcom/google/android/music/store/BlacklistItem;->mDismissDate:J

    return-wide v0
.end method

.method public getId()J
    .locals 2

    .prologue
    .line 132
    iget-wide v0, p0, Lcom/google/android/music/store/BlacklistItem;->mId:J

    return-wide v0
.end method

.method public getListShareToken()Ljava/lang/String;
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/google/android/music/store/BlacklistItem;->mListShareToken:Ljava/lang/String;

    return-object v0
.end method

.method public getRadioRemoteId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lcom/google/android/music/store/BlacklistItem;->mRadioRemoteId:Ljava/lang/String;

    return-object v0
.end method

.method public getRadioSeedId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/google/android/music/store/BlacklistItem;->mRadioSeedId:Ljava/lang/String;

    return-object v0
.end method

.method public getRadioSeedType()I
    .locals 1

    .prologue
    .line 180
    iget v0, p0, Lcom/google/android/music/store/BlacklistItem;->mRadioSeedType:I

    return v0
.end method

.method public getReasonType()I
    .locals 1

    .prologue
    .line 186
    iget v0, p0, Lcom/google/android/music/store/BlacklistItem;->mReasonType:I

    return v0
.end method

.method public insert(Landroid/database/sqlite/SQLiteStatement;)J
    .locals 6
    .param p1, "insertStatement"    # Landroid/database/sqlite/SQLiteStatement;

    .prologue
    const-wide/16 v4, -0x1

    .line 304
    iget-wide v2, p0, Lcom/google/android/music/store/BlacklistItem;->mId:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    .line 305
    new-instance v2, Lcom/google/android/music/store/InvalidDataException;

    const-string v3, "The local id of a blacklist item must not be set for an insert."

    invoke-direct {v2, v3}, Lcom/google/android/music/store/InvalidDataException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 309
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/music/store/BlacklistItem;->prepareInsertOrFullUpdate(Landroid/database/sqlite/SQLiteStatement;)V

    .line 310
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteStatement;->executeInsert()J

    move-result-wide v0

    .line 311
    .local v0, "insertedId":J
    cmp-long v2, v0, v4

    if-nez v2, :cond_1

    .line 312
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "Failed to insert into dismissed item"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 314
    :cond_1
    iput-wide v0, p0, Lcom/google/android/music/store/BlacklistItem;->mId:J

    .line 317
    iget-wide v2, p0, Lcom/google/android/music/store/BlacklistItem;->mId:J

    return-wide v2
.end method

.method public populateFromFullProjectionCursor(Landroid/database/Cursor;)V
    .locals 7
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    const/4 v6, 0x2

    const/16 v5, 0xb

    const/4 v1, 0x0

    const/4 v0, 0x1

    const/4 v4, 0x0

    .line 209
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/music/store/BlacklistItem;->mId:J

    .line 211
    invoke-interface {p1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 212
    iput-object v4, p0, Lcom/google/android/music/store/BlacklistItem;->mAlbumArtist:Ljava/lang/String;

    .line 217
    :goto_0
    invoke-interface {p1, v6}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 218
    iput-object v4, p0, Lcom/google/android/music/store/BlacklistItem;->mAlbumTitle:Ljava/lang/String;

    .line 223
    :goto_1
    const/4 v2, 0x3

    invoke-interface {p1, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 224
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/google/android/music/store/BlacklistItem;->mAlbumLocalId:J

    .line 229
    :goto_2
    const/4 v2, 0x4

    invoke-interface {p1, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 230
    iput-object v4, p0, Lcom/google/android/music/store/BlacklistItem;->mAlbumMetajamId:Ljava/lang/String;

    .line 235
    :goto_3
    const/4 v2, 0x5

    invoke-interface {p1, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 236
    iput-object v4, p0, Lcom/google/android/music/store/BlacklistItem;->mListShareToken:Ljava/lang/String;

    .line 241
    :goto_4
    const/4 v2, 0x6

    invoke-interface {p1, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 242
    iput-object v4, p0, Lcom/google/android/music/store/BlacklistItem;->mRadioRemoteId:Ljava/lang/String;

    .line 247
    :goto_5
    const/4 v2, 0x7

    invoke-interface {p1, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 248
    iput-object v4, p0, Lcom/google/android/music/store/BlacklistItem;->mRadioSeedId:Ljava/lang/String;

    .line 253
    :goto_6
    const/16 v2, 0x8

    invoke-interface {p1, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 254
    iput v1, p0, Lcom/google/android/music/store/BlacklistItem;->mRadioSeedType:I

    .line 259
    :goto_7
    const/16 v2, 0x9

    invoke-interface {p1, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 260
    sget v2, Lcom/google/android/music/store/Schema$MainstageBlacklist;->REASON_TYPE_VALUE_UNKNOWN:I

    iput v2, p0, Lcom/google/android/music/store/BlacklistItem;->mReasonType:I

    .line 265
    :goto_8
    const/16 v2, 0xa

    invoke-interface {p1, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 266
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/google/android/music/store/BlacklistItem;->mDismissDate:J

    .line 271
    :goto_9
    invoke-interface {p1, v5}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 272
    iput-object v4, p0, Lcom/google/android/music/store/BlacklistItem;->mSourceVersion:Ljava/lang/String;

    .line 277
    :goto_a
    const/16 v2, 0xe

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-ne v2, v0, :cond_b

    :goto_b
    iput-boolean v0, p0, Lcom/google/android/music/store/BlacklistItem;->mNeedsSync:Z

    .line 278
    const/16 v0, 0xc

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/store/BlacklistItem;->mSourceAccount:I

    .line 279
    invoke-interface {p1, v5}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 280
    iput-object v4, p0, Lcom/google/android/music/store/BlacklistItem;->mSourceId:Ljava/lang/String;

    .line 284
    :goto_c
    return-void

    .line 214
    :cond_0
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/store/BlacklistItem;->mAlbumArtist:Ljava/lang/String;

    goto/16 :goto_0

    .line 220
    :cond_1
    invoke-interface {p1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/store/BlacklistItem;->mAlbumTitle:Ljava/lang/String;

    goto/16 :goto_1

    .line 226
    :cond_2
    const/4 v2, 0x3

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/music/store/BlacklistItem;->mAlbumLocalId:J

    goto :goto_2

    .line 232
    :cond_3
    const/4 v2, 0x4

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/store/BlacklistItem;->mAlbumMetajamId:Ljava/lang/String;

    goto :goto_3

    .line 238
    :cond_4
    const/4 v2, 0x5

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/store/BlacklistItem;->mListShareToken:Ljava/lang/String;

    goto :goto_4

    .line 244
    :cond_5
    const/4 v2, 0x6

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/store/BlacklistItem;->mRadioRemoteId:Ljava/lang/String;

    goto :goto_5

    .line 250
    :cond_6
    const/4 v2, 0x7

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/store/BlacklistItem;->mRadioSeedId:Ljava/lang/String;

    goto :goto_6

    .line 256
    :cond_7
    const/16 v2, 0x8

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, p0, Lcom/google/android/music/store/BlacklistItem;->mRadioSeedType:I

    goto :goto_7

    .line 262
    :cond_8
    const/16 v2, 0x9

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, p0, Lcom/google/android/music/store/BlacklistItem;->mReasonType:I

    goto :goto_8

    .line 268
    :cond_9
    const/16 v2, 0xa

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/music/store/BlacklistItem;->mDismissDate:J

    goto :goto_9

    .line 274
    :cond_a
    invoke-interface {p1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/store/BlacklistItem;->mSourceVersion:Ljava/lang/String;

    goto :goto_a

    :cond_b
    move v0, v1

    .line 277
    goto :goto_b

    .line 282
    :cond_c
    const/16 v0, 0xd

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/store/BlacklistItem;->mSourceId:Ljava/lang/String;

    goto :goto_c
.end method

.method public reset()V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    const/4 v0, 0x0

    .line 117
    invoke-super {p0}, Lcom/google/android/music/store/Syncable;->reset()V

    .line 118
    iput-wide v2, p0, Lcom/google/android/music/store/BlacklistItem;->mId:J

    .line 119
    iput-object v0, p0, Lcom/google/android/music/store/BlacklistItem;->mAlbumArtist:Ljava/lang/String;

    .line 120
    iput-object v0, p0, Lcom/google/android/music/store/BlacklistItem;->mAlbumTitle:Ljava/lang/String;

    .line 121
    iput-wide v2, p0, Lcom/google/android/music/store/BlacklistItem;->mAlbumLocalId:J

    .line 122
    iput-object v0, p0, Lcom/google/android/music/store/BlacklistItem;->mAlbumMetajamId:Ljava/lang/String;

    .line 123
    iput-object v0, p0, Lcom/google/android/music/store/BlacklistItem;->mListShareToken:Ljava/lang/String;

    .line 124
    iput-object v0, p0, Lcom/google/android/music/store/BlacklistItem;->mRadioRemoteId:Ljava/lang/String;

    .line 125
    iput-object v0, p0, Lcom/google/android/music/store/BlacklistItem;->mRadioSeedId:Ljava/lang/String;

    .line 126
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/music/store/BlacklistItem;->mRadioSeedType:I

    .line 127
    sget v0, Lcom/google/android/music/store/Schema$MainstageBlacklist;->REASON_TYPE_VALUE_UNKNOWN:I

    iput v0, p0, Lcom/google/android/music/store/BlacklistItem;->mReasonType:I

    .line 128
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/music/store/BlacklistItem;->mDismissDate:J

    .line 129
    return-void
.end method

.method public setAlbumArtist(Ljava/lang/String;)V
    .locals 0
    .param p1, "albumArtist"    # Ljava/lang/String;

    .prologue
    .line 141
    iput-object p1, p0, Lcom/google/android/music/store/BlacklistItem;->mAlbumArtist:Ljava/lang/String;

    .line 142
    return-void
.end method

.method public setAlbumLocalId(J)V
    .locals 1
    .param p1, "albumLocalId"    # J

    .prologue
    .line 153
    iput-wide p1, p0, Lcom/google/android/music/store/BlacklistItem;->mAlbumLocalId:J

    .line 154
    return-void
.end method

.method public setAlbumMetajamId(Ljava/lang/String;)V
    .locals 0
    .param p1, "albumMetajamId"    # Ljava/lang/String;

    .prologue
    .line 159
    iput-object p1, p0, Lcom/google/android/music/store/BlacklistItem;->mAlbumMetajamId:Ljava/lang/String;

    .line 160
    return-void
.end method

.method public setAlbumTitle(Ljava/lang/String;)V
    .locals 0
    .param p1, "albumTitle"    # Ljava/lang/String;

    .prologue
    .line 147
    iput-object p1, p0, Lcom/google/android/music/store/BlacklistItem;->mAlbumTitle:Ljava/lang/String;

    .line 148
    return-void
.end method

.method public setDismissDate(J)V
    .locals 1
    .param p1, "dismissDate"    # J

    .prologue
    .line 195
    iput-wide p1, p0, Lcom/google/android/music/store/BlacklistItem;->mDismissDate:J

    .line 196
    return-void
.end method

.method public setListShareToken(Ljava/lang/String;)V
    .locals 0
    .param p1, "listShareToken"    # Ljava/lang/String;

    .prologue
    .line 165
    iput-object p1, p0, Lcom/google/android/music/store/BlacklistItem;->mListShareToken:Ljava/lang/String;

    .line 166
    return-void
.end method

.method public setRadioRemoteId(Ljava/lang/String;)V
    .locals 0
    .param p1, "radioRemoteId"    # Ljava/lang/String;

    .prologue
    .line 171
    iput-object p1, p0, Lcom/google/android/music/store/BlacklistItem;->mRadioRemoteId:Ljava/lang/String;

    .line 172
    return-void
.end method

.method public setRadioSeedId(Ljava/lang/String;)V
    .locals 0
    .param p1, "radioSeedId"    # Ljava/lang/String;

    .prologue
    .line 177
    iput-object p1, p0, Lcom/google/android/music/store/BlacklistItem;->mRadioSeedId:Ljava/lang/String;

    .line 178
    return-void
.end method

.method public setRadioSeedType(I)V
    .locals 0
    .param p1, "radioSeedType"    # I

    .prologue
    .line 183
    iput p1, p0, Lcom/google/android/music/store/BlacklistItem;->mRadioSeedType:I

    .line 184
    return-void
.end method

.method public setReasonType(I)V
    .locals 0
    .param p1, "reasonType"    # I

    .prologue
    .line 189
    iput p1, p0, Lcom/google/android/music/store/BlacklistItem;->mReasonType:I

    .line 190
    return-void
.end method
