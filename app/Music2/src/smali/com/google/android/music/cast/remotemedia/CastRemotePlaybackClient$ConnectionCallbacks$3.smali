.class Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks$3;
.super Ljava/lang/Object;
.source "CastRemotePlaybackClient.java"

# interfaces
.implements Lcom/google/android/gms/cast/RemoteMediaPlayer$OnMetadataUpdatedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;->createMessageCallbacks()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;


# direct methods
.method constructor <init>(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;)V
    .locals 0

    .prologue
    .line 798
    iput-object p1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks$3;->this$1:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMetadataUpdated()V
    .locals 5

    .prologue
    .line 802
    iget-object v2, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks$3;->this$1:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;

    iget-object v2, v2, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    # getter for: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->LOGV:Z
    invoke-static {v2}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$400(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 803
    const-string v2, "MusicCastRemote"

    const-string v3, "onMetadataUpdated"

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 805
    :cond_0
    iget-object v2, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks$3;->this$1:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;

    iget-object v2, v2, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    # getter for: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mRemoteMediaPlayer:Lcom/google/android/gms/cast/RemoteMediaPlayer;
    invoke-static {v2}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$1800(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)Lcom/google/android/gms/cast/RemoteMediaPlayer;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/cast/RemoteMediaPlayer;->getMediaInfo()Lcom/google/android/gms/cast/MediaInfo;

    move-result-object v0

    .line 806
    .local v0, "mediaInfo":Lcom/google/android/gms/cast/MediaInfo;
    if-eqz v0, :cond_1

    .line 807
    invoke-virtual {v0}, Lcom/google/android/gms/cast/MediaInfo;->getMetadata()Lcom/google/android/gms/cast/MediaMetadata;

    move-result-object v1

    .line 808
    .local v1, "metadata":Lcom/google/android/gms/cast/MediaMetadata;
    iget-object v2, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks$3;->this$1:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;

    iget-object v2, v2, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    # getter for: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->LOGV:Z
    invoke-static {v2}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$400(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 809
    const-string v2, "MusicCastRemote"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "media metadata: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "com.google.android.gms.cast.metadata.TITLE"

    invoke-virtual {v1, v4}, Lcom/google/android/gms/cast/MediaMetadata;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 815
    .end local v1    # "metadata":Lcom/google/android/gms/cast/MediaMetadata;
    :cond_1
    return-void
.end method
