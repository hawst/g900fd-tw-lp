.class public interface abstract Lcom/google/android/music/dial/RemoteDeviceManager$Listener;
.super Ljava/lang/Object;
.source "RemoteDeviceManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/dial/RemoteDeviceManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Listener"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/dial/RemoteDeviceManager$Listener$ScanError;
    }
.end annotation


# virtual methods
.method public abstract onDeviceOffline(Lcom/google/android/music/dial/RemoteDeviceInfo;)V
.end method

.method public abstract onDeviceOnline(Lcom/google/android/music/dial/RemoteDeviceInfo;)V
.end method

.method public abstract onScanError(Lcom/google/android/music/dial/RemoteDeviceManager$Listener$ScanError;)V
.end method
