.class Lcom/google/android/music/tutorial/TutorialTryNautilusActivity$TryNautilusFragmentPageAdapter;
.super Landroid/support/v4/app/FragmentPagerAdapter;
.source "TutorialTryNautilusActivity.java"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TryNautilusFragmentPageAdapter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;Landroid/support/v4/app/FragmentManager;)V
    .locals 1
    .param p2, "fm"    # Landroid/support/v4/app/FragmentManager;

    .prologue
    .line 349
    iput-object p1, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity$TryNautilusFragmentPageAdapter;->this$0:Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;

    .line 350
    invoke-direct {p0, p2}, Landroid/support/v4/app/FragmentPagerAdapter;-><init>(Landroid/support/v4/app/FragmentManager;)V

    .line 351
    # getter for: Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mViewPager:Landroid/support/v4/view/ViewPager;
    invoke-static {p1}, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->access$200(Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;)Landroid/support/v4/view/ViewPager;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 352
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 381
    iget-object v0, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity$TryNautilusFragmentPageAdapter;->this$0:Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;

    # getter for: Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mIsYTAvailable:Z
    invoke-static {v0}, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->access$800(Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x4

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x3

    goto :goto_0
.end method

.method public getItem(I)Landroid/support/v4/app/Fragment;
    .locals 8
    .param p1, "position"    # I

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 356
    packed-switch p1, :pswitch_data_0

    .line 373
    const-string v0, "MusicTutorial"

    const-string v1, "Unexpected item for Try Naulitus ViewPager."

    invoke-static {v0, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 376
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 358
    :pswitch_0
    sget-object v0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->TITLE_RESOURCE_ID:[I

    aget v0, v0, v3

    sget-object v1, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->SUMMARY_RESOURCE_ID:[I

    aget v1, v1, v3

    sget-object v2, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->ICON_RESOURCE_ID:[I

    aget v2, v2, v3

    invoke-static {v0, v1, v2}, Lcom/google/android/music/tutorial/TutorialTryNautilusFragment;->newInstance(III)Lcom/google/android/music/tutorial/TutorialTryNautilusFragment;

    move-result-object v0

    goto :goto_0

    .line 361
    :pswitch_1
    sget-object v0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->TITLE_RESOURCE_ID:[I

    aget v0, v0, v4

    sget-object v1, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->SUMMARY_RESOURCE_ID:[I

    aget v1, v1, v4

    sget-object v2, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->ICON_RESOURCE_ID:[I

    aget v2, v2, v4

    invoke-static {v0, v1, v2}, Lcom/google/android/music/tutorial/TutorialTryNautilusFragment;->newInstance(III)Lcom/google/android/music/tutorial/TutorialTryNautilusFragment;

    move-result-object v0

    goto :goto_0

    .line 364
    :pswitch_2
    sget-object v0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->TITLE_RESOURCE_ID:[I

    aget v0, v0, v5

    sget-object v1, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->SUMMARY_RESOURCE_ID:[I

    aget v1, v1, v5

    sget-object v2, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->ICON_RESOURCE_ID:[I

    aget v2, v2, v5

    invoke-static {v0, v1, v2}, Lcom/google/android/music/tutorial/TutorialTryNautilusFragment;->newInstance(III)Lcom/google/android/music/tutorial/TutorialTryNautilusFragment;

    move-result-object v0

    goto :goto_0

    .line 367
    :pswitch_3
    sget-object v0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->TITLE_RESOURCE_ID:[I

    aget v0, v0, v6

    sget-object v1, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->SUMMARY_RESOURCE_ID:[I

    aget v1, v1, v6

    sget-object v2, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->ICON_RESOURCE_ID:[I

    aget v2, v2, v6

    invoke-static {v0, v1, v2}, Lcom/google/android/music/tutorial/TutorialTryNautilusFragment;->newInstance(III)Lcom/google/android/music/tutorial/TutorialTryNautilusFragment;

    move-result-object v0

    goto :goto_0

    .line 370
    :pswitch_4
    sget-object v0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->TITLE_RESOURCE_ID:[I

    aget v0, v0, v7

    sget-object v1, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->SUMMARY_RESOURCE_ID:[I

    aget v1, v1, v7

    sget-object v2, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->ICON_RESOURCE_ID:[I

    aget v2, v2, v7

    invoke-static {v0, v1, v2}, Lcom/google/android/music/tutorial/TutorialTryNautilusFragment;->newInstance(III)Lcom/google/android/music/tutorial/TutorialTryNautilusFragment;

    move-result-object v0

    goto :goto_0

    .line 356
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public onPageScrollStateChanged(I)V
    .locals 0
    .param p1, "position"    # I

    .prologue
    .line 386
    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 0
    .param p1, "position"    # I
    .param p2, "positionOffset"    # F
    .param p3, "positionOffsetPixels"    # I

    .prologue
    .line 390
    return-void
.end method

.method public onPageSelected(I)V
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 394
    iget-object v0, p0, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity$TryNautilusFragmentPageAdapter;->this$0:Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;

    # getter for: Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->mPageIndicator:Lcom/google/android/music/widgets/PageIndicator;
    invoke-static {v0}, Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;->access$900(Lcom/google/android/music/tutorial/TutorialTryNautilusActivity;)Lcom/google/android/music/widgets/PageIndicator;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/music/widgets/PageIndicator;->updateSelectedDrawable(I)V

    .line 395
    return-void
.end method
