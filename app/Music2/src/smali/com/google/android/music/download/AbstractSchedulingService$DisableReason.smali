.class public final enum Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;
.super Ljava/lang/Enum;
.source "AbstractSchedulingService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/download/AbstractSchedulingService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DisableReason"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;

.field public static final enum CONNECTIVITY_LOST:Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;

.field public static final enum DEVICE_NOT_AUTHORIZED:Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;

.field public static final enum DOWNLOAD_PAUSED:Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;

.field public static final enum HIGH_SPEED_LOST:Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;

.field public static final enum OUT_OF_SPACE:Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;

.field public static final enum SUCCESSIVE_FAILURES:Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 73
    new-instance v0, Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;

    const-string v1, "HIGH_SPEED_LOST"

    invoke-direct {v0, v1, v3}, Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;->HIGH_SPEED_LOST:Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;

    .line 74
    new-instance v0, Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;

    const-string v1, "OUT_OF_SPACE"

    invoke-direct {v0, v1, v4}, Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;->OUT_OF_SPACE:Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;

    .line 75
    new-instance v0, Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;

    const-string v1, "SUCCESSIVE_FAILURES"

    invoke-direct {v0, v1, v5}, Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;->SUCCESSIVE_FAILURES:Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;

    .line 76
    new-instance v0, Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;

    const-string v1, "CONNECTIVITY_LOST"

    invoke-direct {v0, v1, v6}, Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;->CONNECTIVITY_LOST:Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;

    .line 77
    new-instance v0, Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;

    const-string v1, "DEVICE_NOT_AUTHORIZED"

    invoke-direct {v0, v1, v7}, Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;->DEVICE_NOT_AUTHORIZED:Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;

    .line 78
    new-instance v0, Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;

    const-string v1, "DOWNLOAD_PAUSED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;->DOWNLOAD_PAUSED:Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;

    .line 72
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;

    sget-object v1, Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;->HIGH_SPEED_LOST:Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;->OUT_OF_SPACE:Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;->SUCCESSIVE_FAILURES:Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;->CONNECTIVITY_LOST:Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;->DEVICE_NOT_AUTHORIZED:Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;->DOWNLOAD_PAUSED:Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;->$VALUES:[Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 72
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 72
    const-class v0, Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;

    return-object v0
.end method

.method public static values()[Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;
    .locals 1

    .prologue
    .line 72
    sget-object v0, Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;->$VALUES:[Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;

    invoke-virtual {v0}, [Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;

    return-object v0
.end method
