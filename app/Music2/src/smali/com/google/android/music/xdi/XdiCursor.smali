.class public abstract Lcom/google/android/music/xdi/XdiCursor;
.super Landroid/database/AbstractCursor;
.source "XdiCursor.java"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mCurrentRow:[Ljava/lang/Object;

.field private final mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

.field private final mWrappedCursor:Landroid/database/Cursor;


# direct methods
.method public constructor <init>(Landroid/content/Context;[Ljava/lang/String;Landroid/database/Cursor;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "cursorToWrap"    # Landroid/database/Cursor;

    .prologue
    .line 27
    invoke-direct {p0}, Landroid/database/AbstractCursor;-><init>()V

    .line 25
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/xdi/XdiCursor;->mCurrentRow:[Ljava/lang/Object;

    .line 28
    iput-object p1, p0, Lcom/google/android/music/xdi/XdiCursor;->mContext:Landroid/content/Context;

    .line 29
    new-instance v0, Lcom/google/android/music/xdi/ProjectionMap;

    invoke-direct {v0, p2}, Lcom/google/android/music/xdi/ProjectionMap;-><init>([Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/music/xdi/XdiCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    .line 30
    iput-object p3, p0, Lcom/google/android/music/xdi/XdiCursor;->mWrappedCursor:Landroid/database/Cursor;

    .line 31
    return-void
.end method

.method private clearCallingIdentityAndExtractDataForCurrentRow([Ljava/lang/Object;)Z
    .locals 3
    .param p1, "values"    # [Ljava/lang/Object;

    .prologue
    .line 58
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v0

    .line 60
    .local v0, "token":J
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/google/android/music/xdi/XdiCursor;->extractDataForCurrentRow([Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    .line 62
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    return v2

    :catchall_0
    move-exception v2

    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v2
.end method

.method private get(I)Ljava/lang/Object;
    .locals 2
    .param p1, "i"    # I

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/music/xdi/XdiCursor;->mCurrentRow:[Ljava/lang/Object;

    if-nez v0, :cond_0

    .line 112
    new-instance v0, Landroid/database/CursorIndexOutOfBoundsException;

    const-string v1, "Not on a row"

    invoke-direct {v0, v1}, Landroid/database/CursorIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 114
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/xdi/XdiCursor;->mCurrentRow:[Ljava/lang/Object;

    aget-object v0, v0, p1

    return-object v0
.end method


# virtual methods
.method public close()V
    .locals 1

    .prologue
    .line 76
    invoke-super {p0}, Landroid/database/AbstractCursor;->close()V

    .line 77
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/xdi/XdiCursor;->mCurrentRow:[Ljava/lang/Object;

    .line 78
    iget-object v0, p0, Lcom/google/android/music/xdi/XdiCursor;->mWrappedCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/xdi/XdiCursor;->mWrappedCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 79
    :cond_0
    return-void
.end method

.method protected createArrayForRow()[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/music/xdi/XdiCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    invoke-virtual {v0}, Lcom/google/android/music/xdi/ProjectionMap;->getColumnNames()[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [Ljava/lang/Object;

    return-object v0
.end method

.method public deactivate()V
    .locals 1

    .prologue
    .line 91
    invoke-super {p0}, Landroid/database/AbstractCursor;->deactivate()V

    .line 92
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/xdi/XdiCursor;->mCurrentRow:[Ljava/lang/Object;

    .line 93
    iget-object v0, p0, Lcom/google/android/music/xdi/XdiCursor;->mWrappedCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/xdi/XdiCursor;->mWrappedCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->deactivate()V

    .line 94
    :cond_0
    return-void
.end method

.method protected abstract extractDataForCurrentRow([Ljava/lang/Object;)Z
.end method

.method public getColumnNames()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/android/music/xdi/XdiCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    invoke-virtual {v0}, Lcom/google/android/music/xdi/ProjectionMap;->getColumnNames()[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/music/xdi/XdiCursor;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/android/music/xdi/XdiCursor;->mWrappedCursor:Landroid/database/Cursor;

    if-nez v0, :cond_0

    .line 99
    const/4 v0, 0x0

    .line 101
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/music/xdi/XdiCursor;->mWrappedCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    goto :goto_0
.end method

.method public getDouble(I)D
    .locals 4
    .param p1, "i"    # I

    .prologue
    .line 158
    invoke-direct {p0, p1}, Lcom/google/android/music/xdi/XdiCursor;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 159
    .local v0, "o":Ljava/lang/Object;
    if-nez v0, :cond_0

    const-wide/16 v2, 0x0

    .line 161
    .end local v0    # "o":Ljava/lang/Object;
    :goto_0
    return-wide v2

    .line 160
    .restart local v0    # "o":Ljava/lang/Object;
    :cond_0
    instance-of v1, v0, Ljava/lang/Number;

    if-eqz v1, :cond_1

    check-cast v0, Ljava/lang/Number;

    .end local v0    # "o":Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v2

    goto :goto_0

    .line 161
    .restart local v0    # "o":Ljava/lang/Object;
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v2

    goto :goto_0
.end method

.method public getFloat(I)F
    .locals 2
    .param p1, "i"    # I

    .prologue
    .line 150
    invoke-direct {p0, p1}, Lcom/google/android/music/xdi/XdiCursor;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 151
    .local v0, "o":Ljava/lang/Object;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    .line 153
    .end local v0    # "o":Ljava/lang/Object;
    :goto_0
    return v1

    .line 152
    .restart local v0    # "o":Ljava/lang/Object;
    :cond_0
    instance-of v1, v0, Ljava/lang/Number;

    if-eqz v1, :cond_1

    check-cast v0, Ljava/lang/Number;

    .end local v0    # "o":Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F

    move-result v1

    goto :goto_0

    .line 153
    .restart local v0    # "o":Ljava/lang/Object;
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    goto :goto_0
.end method

.method public getInt(I)I
    .locals 2
    .param p1, "i"    # I

    .prologue
    .line 134
    invoke-direct {p0, p1}, Lcom/google/android/music/xdi/XdiCursor;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 135
    .local v0, "o":Ljava/lang/Object;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    .line 137
    .end local v0    # "o":Ljava/lang/Object;
    :goto_0
    return v1

    .line 136
    .restart local v0    # "o":Ljava/lang/Object;
    :cond_0
    instance-of v1, v0, Ljava/lang/Number;

    if-eqz v1, :cond_1

    check-cast v0, Ljava/lang/Number;

    .end local v0    # "o":Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v1

    goto :goto_0

    .line 137
    .restart local v0    # "o":Ljava/lang/Object;
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    goto :goto_0
.end method

.method public getLong(I)J
    .locals 4
    .param p1, "i"    # I

    .prologue
    .line 142
    invoke-direct {p0, p1}, Lcom/google/android/music/xdi/XdiCursor;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 143
    .local v0, "o":Ljava/lang/Object;
    if-nez v0, :cond_0

    const-wide/16 v2, 0x0

    .line 145
    .end local v0    # "o":Ljava/lang/Object;
    :goto_0
    return-wide v2

    .line 144
    .restart local v0    # "o":Ljava/lang/Object;
    :cond_0
    instance-of v1, v0, Ljava/lang/Number;

    if-eqz v1, :cond_1

    check-cast v0, Ljava/lang/Number;

    .end local v0    # "o":Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/lang/Number;->longValue()J

    move-result-wide v2

    goto :goto_0

    .line 145
    .restart local v0    # "o":Ljava/lang/Object;
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    goto :goto_0
.end method

.method public getShort(I)S
    .locals 2
    .param p1, "i"    # I

    .prologue
    .line 126
    invoke-direct {p0, p1}, Lcom/google/android/music/xdi/XdiCursor;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 127
    .local v0, "o":Ljava/lang/Object;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    .line 129
    .end local v0    # "o":Ljava/lang/Object;
    :goto_0
    return v1

    .line 128
    .restart local v0    # "o":Ljava/lang/Object;
    :cond_0
    instance-of v1, v0, Ljava/lang/Number;

    if-eqz v1, :cond_1

    check-cast v0, Ljava/lang/Number;

    .end local v0    # "o":Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/lang/Number;->shortValue()S

    move-result v1

    goto :goto_0

    .line 129
    .restart local v0    # "o":Ljava/lang/Object;
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Short;->parseShort(Ljava/lang/String;)S

    move-result v1

    goto :goto_0
.end method

.method public getString(I)Ljava/lang/String;
    .locals 2
    .param p1, "i"    # I

    .prologue
    .line 119
    invoke-direct {p0, p1}, Lcom/google/android/music/xdi/XdiCursor;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 120
    .local v0, "o":Ljava/lang/Object;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    .line 121
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method protected getWrappedCursor()Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/music/xdi/XdiCursor;->mWrappedCursor:Landroid/database/Cursor;

    return-object v0
.end method

.method public isNull(I)Z
    .locals 1
    .param p1, "i"    # I

    .prologue
    .line 166
    invoke-direct {p0, p1}, Lcom/google/android/music/xdi/XdiCursor;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onMove(II)Z
    .locals 1
    .param p1, "oldPosition"    # I
    .param p2, "newPosition"    # I

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/music/xdi/XdiCursor;->mWrappedCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/xdi/XdiCursor;->mWrappedCursor:Landroid/database/Cursor;

    invoke-interface {v0, p2}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 86
    :goto_0
    return v0

    .line 84
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/music/xdi/XdiCursor;->createArrayForRow()[Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/xdi/XdiCursor;->mCurrentRow:[Ljava/lang/Object;

    .line 86
    iget-object v0, p0, Lcom/google/android/music/xdi/XdiCursor;->mCurrentRow:[Ljava/lang/Object;

    invoke-direct {p0, v0}, Lcom/google/android/music/xdi/XdiCursor;->clearCallingIdentityAndExtractDataForCurrentRow([Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method protected writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1
    .param p1, "out"    # [Ljava/lang/Object;
    .param p2, "columnName"    # Ljava/lang/String;
    .param p3, "value"    # Ljava/lang/Object;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/music/xdi/XdiCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 47
    return-void
.end method
