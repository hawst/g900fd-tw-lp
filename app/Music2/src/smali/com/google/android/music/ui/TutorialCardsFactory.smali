.class public Lcom/google/android/music/ui/TutorialCardsFactory;
.super Ljava/lang/Object;
.source "TutorialCardsFactory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;,
        Lcom/google/android/music/ui/TutorialCardsFactory$ImageSpanHack;
    }
.end annotation


# static fields
.field private static final ACTIONS_GOT_IT:[Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;

.field private static final ACTIONS_MAINSTAGE_TRY_NAUTILUS:[Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;

.field private static final ACTIONS_MY_LIBRARY:[Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;

.field private static final EXPLORE_BULLETS:[Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;

.field private static final INSTANT_MIX_BULLETS:[Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;

.field private static final MAINSTAGE_TRY_NAUTILUS_BULLETS:[Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;

.field private static final RADIO_BULLETS:[Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const v8, 0x7f0200d4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 69
    new-array v0, v7, [Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;

    new-instance v1, Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;

    const v2, 0x7f02015e

    const v3, 0x7f0b0280

    invoke-direct {v1, v2, v3}, Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;-><init>(II)V

    aput-object v1, v0, v4

    new-instance v1, Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;

    const v2, 0x7f02015c

    const v3, 0x7f0b0282

    invoke-direct {v1, v2, v3}, Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;-><init>(II)V

    aput-object v1, v0, v5

    new-instance v1, Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;

    const v2, 0x7f02015f

    const v3, 0x7f0b0283

    invoke-direct {v1, v2, v3}, Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;-><init>(II)V

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/android/music/ui/TutorialCardsFactory;->RADIO_BULLETS:[Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;

    .line 74
    new-array v0, v7, [Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;

    new-instance v1, Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;

    const v2, 0x7f02015a

    const v3, 0x7f0b0281

    invoke-direct {v1, v2, v3}, Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;-><init>(II)V

    aput-object v1, v0, v4

    new-instance v1, Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;

    const v2, 0x7f02015c

    const v3, 0x7f0b0282

    invoke-direct {v1, v2, v3}, Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;-><init>(II)V

    aput-object v1, v0, v5

    new-instance v1, Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;

    const v2, 0x7f02015f

    const v3, 0x7f0b0283

    invoke-direct {v1, v2, v3}, Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;-><init>(II)V

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/android/music/ui/TutorialCardsFactory;->INSTANT_MIX_BULLETS:[Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;

    .line 79
    new-array v0, v7, [Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;

    new-instance v1, Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;

    const v2, 0x7f020158

    const v3, 0x7f0b0285

    invoke-direct {v1, v2, v3}, Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;-><init>(II)V

    aput-object v1, v0, v4

    new-instance v1, Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;

    const v2, 0x7f02015d

    const v3, 0x7f0b0286

    invoke-direct {v1, v2, v3}, Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;-><init>(II)V

    aput-object v1, v0, v5

    new-instance v1, Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;

    const v2, 0x7f02015b

    const v3, 0x7f0b0287

    invoke-direct {v1, v2, v3}, Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;-><init>(II)V

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/android/music/ui/TutorialCardsFactory;->EXPLORE_BULLETS:[Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;

    .line 84
    new-array v0, v7, [Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;

    new-instance v1, Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;

    const v2, 0x7f020158

    const v3, 0x7f0b0123

    invoke-direct {v1, v2, v3}, Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;-><init>(II)V

    aput-object v1, v0, v4

    new-instance v1, Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;

    const v2, 0x7f02015e

    const v3, 0x7f0b0125

    invoke-direct {v1, v2, v3}, Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;-><init>(II)V

    aput-object v1, v0, v5

    new-instance v1, Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;

    const v2, 0x7f020159

    const v3, 0x7f0b0127

    invoke-direct {v1, v2, v3}, Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;-><init>(II)V

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/android/music/ui/TutorialCardsFactory;->MAINSTAGE_TRY_NAUTILUS_BULLETS:[Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;

    .line 93
    new-array v0, v6, [Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;

    new-instance v1, Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;

    const v2, 0x7f020119

    const v3, 0x7f0b028c

    invoke-direct {v1, v2, v3}, Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;-><init>(II)V

    aput-object v1, v0, v4

    new-instance v1, Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;

    const v2, 0x7f0b028b

    invoke-direct {v1, v8, v2}, Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;-><init>(II)V

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/music/ui/TutorialCardsFactory;->ACTIONS_MAINSTAGE_TRY_NAUTILUS:[Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;

    .line 98
    new-array v0, v6, [Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;

    new-instance v1, Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;

    const v2, 0x7f020150

    const v3, 0x7f0b028a

    invoke-direct {v1, v2, v3}, Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;-><init>(II)V

    aput-object v1, v0, v4

    new-instance v1, Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;

    const v2, 0x7f0b028b

    invoke-direct {v1, v8, v2}, Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;-><init>(II)V

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/music/ui/TutorialCardsFactory;->ACTIONS_MY_LIBRARY:[Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;

    .line 102
    new-array v0, v5, [Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;

    new-instance v1, Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;

    const v2, 0x7f0b028b

    invoke-direct {v1, v8, v2}, Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;-><init>(II)V

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/music/ui/TutorialCardsFactory;->ACTIONS_GOT_IT:[Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;

    return-void
.end method

.method static synthetic access$000()Z
    .locals 1

    .prologue
    .line 46
    invoke-static {}, Lcom/google/android/music/ui/TutorialCardsFactory;->hasSubscription()Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/music/ui/MusicFragment;Landroid/widget/LinearLayout;Ljava/lang/String;Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/MusicFragment;
    .param p1, "x1"    # Landroid/widget/LinearLayout;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;

    .prologue
    .line 46
    invoke-static {p0, p1, p2, p3}, Lcom/google/android/music/ui/TutorialCardsFactory;->dismissTutorialCard(Lcom/google/android/music/ui/MusicFragment;Landroid/widget/LinearLayout;Ljava/lang/String;Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;)V

    return-void
.end method

.method private static addCard(Landroid/view/ViewGroup;Landroid/widget/LinearLayout;)Landroid/view/ViewGroup;
    .locals 0
    .param p0, "container"    # Landroid/view/ViewGroup;
    .param p1, "card"    # Landroid/widget/LinearLayout;

    .prologue
    .line 635
    if-eqz p1, :cond_0

    .line 636
    invoke-virtual {p0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 638
    :cond_0
    return-object p0
.end method

.method private static buildMainStageWelcomeCardWithImage(Lcom/google/android/music/ui/MusicFragment;Landroid/content/Context;Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;)Landroid/widget/LinearLayout;
    .locals 8
    .param p0, "fragment"    # Lcom/google/android/music/ui/MusicFragment;
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "dismissHandler"    # Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;

    .prologue
    const/4 v7, 0x2

    .line 195
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    const v5, 0x7f040063

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 198
    .local v1, "card":Landroid/widget/LinearLayout;
    const v4, 0x7f0e017a

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 199
    .local v3, "title":Landroid/widget/TextView;
    invoke-static {v3, v7}, Lcom/google/android/music/utils/TypefaceUtil;->setTypeface(Landroid/widget/TextView;I)V

    .line 201
    const v4, 0x7f0e017b

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 202
    .local v2, "content":Landroid/widget/TextView;
    invoke-static {v2, v7}, Lcom/google/android/music/utils/TypefaceUtil;->setTypeface(Landroid/widget/TextView;I)V

    .line 204
    invoke-static {}, Lcom/google/android/music/ui/TutorialCardsFactory;->hasSubscription()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 205
    invoke-static {}, Lcom/google/android/music/utils/ConfigUtils;->isConciergeListenNowEnabled()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 206
    const v4, 0x7f0b037c

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 207
    const v4, 0x7f0b037d

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(I)V

    .line 218
    :goto_0
    const v4, 0x7f0e017c

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 219
    .local v0, "actionButton":Landroid/view/View;
    new-instance v4, Lcom/google/android/music/ui/TutorialCardsFactory$1;

    invoke-direct {v4, p0, v1, p2}, Lcom/google/android/music/ui/TutorialCardsFactory$1;-><init>(Lcom/google/android/music/ui/MusicFragment;Landroid/widget/LinearLayout;Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;)V

    invoke-virtual {v0, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 236
    return-object v1

    .line 209
    .end local v0    # "actionButton":Landroid/view/View;
    :cond_0
    const v4, 0x7f0b037e

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 210
    const v4, 0x7f0b037f

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 213
    :cond_1
    const v4, 0x7f0b0274

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 214
    const v4, 0x7f0b0275

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method

.method private static buildMainstageSituationsWelcomeCard(Lcom/google/android/music/ui/MusicFragment;Landroid/content/Context;Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;)Landroid/widget/LinearLayout;
    .locals 8
    .param p0, "fragment"    # Lcom/google/android/music/ui/MusicFragment;
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "dismissHandler"    # Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;

    .prologue
    const/4 v7, 0x2

    .line 275
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    const v5, 0x7f040063

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 278
    .local v1, "card":Landroid/widget/LinearLayout;
    const v4, 0x7f0e017a

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 279
    .local v3, "title":Landroid/widget/TextView;
    invoke-static {v3, v7}, Lcom/google/android/music/utils/TypefaceUtil;->setTypeface(Landroid/widget/TextView;I)V

    .line 281
    const v4, 0x7f0e017b

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 282
    .local v2, "content":Landroid/widget/TextView;
    invoke-static {v2, v7}, Lcom/google/android/music/utils/TypefaceUtil;->setTypeface(Landroid/widget/TextView;I)V

    .line 286
    const v4, 0x7f0b037c

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 287
    const v4, 0x7f0b037d

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(I)V

    .line 289
    const v4, 0x7f0e017c

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 290
    .local v0, "actionButton":Landroid/view/View;
    new-instance v4, Lcom/google/android/music/ui/TutorialCardsFactory$4;

    invoke-direct {v4, p0, v1, p2}, Lcom/google/android/music/ui/TutorialCardsFactory$4;-><init>(Lcom/google/android/music/ui/MusicFragment;Landroid/widget/LinearLayout;Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;)V

    invoke-virtual {v0, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 298
    return-object v1
.end method

.method private static buildMainstageSongzaWelcomeCard(Lcom/google/android/music/ui/MusicFragment;Landroid/content/Context;Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;)Landroid/widget/LinearLayout;
    .locals 9
    .param p0, "fragment"    # Lcom/google/android/music/ui/MusicFragment;
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "dismissHandler"    # Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;

    .prologue
    const/4 v8, 0x2

    .line 392
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v5

    const v6, 0x7f040063

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 396
    .local v1, "card":Landroid/widget/LinearLayout;
    const v5, 0x7f0e0179

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 397
    .local v3, "image":Landroid/widget/ImageView;
    const v5, 0x7f020182

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 399
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0c00cf

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    .line 403
    const v5, 0x7f0e017a

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 404
    .local v4, "title":Landroid/widget/TextView;
    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 405
    invoke-static {v4, v8}, Lcom/google/android/music/utils/TypefaceUtil;->setTypeface(Landroid/widget/TextView;I)V

    .line 406
    const v5, 0x7f0b027b

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 409
    const v5, 0x7f0e017b

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 410
    .local v2, "content":Landroid/widget/TextView;
    invoke-static {v2, v8}, Lcom/google/android/music/utils/TypefaceUtil;->setTypeface(Landroid/widget/TextView;I)V

    .line 411
    const v5, 0x7f0b027c

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(I)V

    .line 413
    const v5, 0x7f0e017c

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 414
    .local v0, "actionButton":Landroid/view/View;
    new-instance v5, Lcom/google/android/music/ui/TutorialCardsFactory$8;

    invoke-direct {v5, p0, v1, p2}, Lcom/google/android/music/ui/TutorialCardsFactory$8;-><init>(Lcom/google/android/music/ui/MusicFragment;Landroid/widget/LinearLayout;Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;)V

    invoke-virtual {v0, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 424
    return-object v1
.end method

.method public static buildMainstageTutorialCardToShow(Lcom/google/android/music/ui/MusicFragment;Landroid/content/Context;Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;)Landroid/view/View;
    .locals 8
    .param p0, "fragment"    # Lcom/google/android/music/ui/MusicFragment;
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "dismissHandler"    # Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;

    .prologue
    const/4 v0, 0x0

    .line 440
    invoke-static {p1}, Lcom/google/android/music/ui/TutorialCardsFactory;->getMainstageTutorialCardToShow(Landroid/content/Context;)I

    move-result v7

    .line 441
    .local v7, "cardType":I
    const/4 v1, -0x1

    if-ne v7, v1, :cond_0

    .line 465
    :goto_0
    return-object v0

    .line 445
    :cond_0
    packed-switch v7, :pswitch_data_0

    goto :goto_0

    .line 449
    :pswitch_0
    invoke-static {p0, p1, p2}, Lcom/google/android/music/ui/TutorialCardsFactory;->buildMainStageWelcomeCardWithImage(Lcom/google/android/music/ui/MusicFragment;Landroid/content/Context;Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;)Landroid/widget/LinearLayout;

    move-result-object v0

    goto :goto_0

    .line 447
    :pswitch_1
    invoke-static {p0, p1, p2}, Lcom/google/android/music/ui/TutorialCardsFactory;->buildResubscribeNautilusCardWithImage(Lcom/google/android/music/ui/MusicFragment;Landroid/content/Context;Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;)Landroid/widget/LinearLayout;

    move-result-object v0

    goto :goto_0

    .line 451
    :pswitch_2
    invoke-static {p0, p1, p2}, Lcom/google/android/music/ui/TutorialCardsFactory;->buildMainstageSituationsWelcomeCard(Lcom/google/android/music/ui/MusicFragment;Landroid/content/Context;Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;)Landroid/widget/LinearLayout;

    move-result-object v0

    goto :goto_0

    .line 453
    :pswitch_3
    new-instance v1, Landroid/widget/LinearLayout;

    invoke-direct {v1, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const-string v2, "MainstageBasicTryNautilusAgain5"

    const v3, 0x7f0b0276

    sget-object v4, Lcom/google/android/music/ui/TutorialCardsFactory;->MAINSTAGE_TRY_NAUTILUS_BULLETS:[Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;

    sget-object v5, Lcom/google/android/music/ui/TutorialCardsFactory;->ACTIONS_MAINSTAGE_TRY_NAUTILUS:[Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;

    move-object v0, p0

    move-object v6, p2

    invoke-static/range {v0 .. v6}, Lcom/google/android/music/ui/TutorialCardsFactory;->createBulletsTextTutorial(Lcom/google/android/music/ui/MusicFragment;Landroid/view/ViewGroup;Ljava/lang/String;I[Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;[Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;)Lcom/google/android/music/ui/cardlib/layout/PlayTutorialHeader;

    move-result-object v0

    goto :goto_0

    .line 459
    :pswitch_4
    invoke-static {p0, p1, p2}, Lcom/google/android/music/ui/TutorialCardsFactory;->buildMainstageWearCard(Lcom/google/android/music/ui/MusicFragment;Landroid/content/Context;Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;)Landroid/widget/LinearLayout;

    move-result-object v0

    goto :goto_0

    .line 461
    :pswitch_5
    invoke-static {p0, p1, p2}, Lcom/google/android/music/ui/TutorialCardsFactory;->buildMainstageYoutubeWelcomeCard(Lcom/google/android/music/ui/MusicFragment;Landroid/content/Context;Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;)Landroid/widget/LinearLayout;

    move-result-object v0

    goto :goto_0

    .line 463
    :pswitch_6
    invoke-static {p0, p1, p2}, Lcom/google/android/music/ui/TutorialCardsFactory;->buildMainstageSongzaWelcomeCard(Lcom/google/android/music/ui/MusicFragment;Landroid/content/Context;Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;)Landroid/widget/LinearLayout;

    move-result-object v0

    goto :goto_0

    .line 445
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_3
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private static buildMainstageWearCard(Lcom/google/android/music/ui/MusicFragment;Landroid/content/Context;Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;)Landroid/widget/LinearLayout;
    .locals 9
    .param p0, "fragment"    # Lcom/google/android/music/ui/MusicFragment;
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "dismissHandler"    # Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;

    .prologue
    const/4 v8, 0x2

    .line 242
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v5

    const v6, 0x7f04010a

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 245
    .local v0, "card":Landroid/widget/LinearLayout;
    const v5, 0x7f0e017a

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 246
    .local v3, "title":Landroid/widget/TextView;
    invoke-static {v3, v8}, Lcom/google/android/music/utils/TypefaceUtil;->setTypeface(Landroid/widget/TextView;I)V

    .line 248
    const v5, 0x7f0e017b

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 249
    .local v1, "content":Landroid/widget/TextView;
    invoke-static {v1, v8}, Lcom/google/android/music/utils/TypefaceUtil;->setTypeface(Landroid/widget/TextView;I)V

    .line 251
    const v5, 0x7f0e0292

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 252
    .local v2, "noThanksButton":Landroid/view/View;
    new-instance v5, Lcom/google/android/music/ui/TutorialCardsFactory$2;

    invoke-direct {v5, p0, v0, p2}, Lcom/google/android/music/ui/TutorialCardsFactory$2;-><init>(Lcom/google/android/music/ui/MusicFragment;Landroid/widget/LinearLayout;Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;)V

    invoke-virtual {v2, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 260
    const v5, 0x7f0e0293

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 261
    .local v4, "yesButton":Landroid/view/View;
    new-instance v5, Lcom/google/android/music/ui/TutorialCardsFactory$3;

    invoke-direct {v5, p0, v0, p2}, Lcom/google/android/music/ui/TutorialCardsFactory$3;-><init>(Lcom/google/android/music/ui/MusicFragment;Landroid/widget/LinearLayout;Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;)V

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 269
    return-object v0
.end method

.method private static buildMainstageYoutubeWelcomeCard(Lcom/google/android/music/ui/MusicFragment;Landroid/content/Context;Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;)Landroid/widget/LinearLayout;
    .locals 9
    .param p0, "fragment"    # Lcom/google/android/music/ui/MusicFragment;
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "dismissHandler"    # Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;

    .prologue
    const/4 v8, 0x2

    .line 358
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v5

    const v6, 0x7f040063

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 362
    .local v1, "card":Landroid/widget/LinearLayout;
    const v5, 0x7f0e0179

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 363
    .local v3, "image":Landroid/widget/ImageView;
    const v5, 0x7f020184

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 366
    const v5, 0x7f0e017a

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 367
    .local v4, "title":Landroid/widget/TextView;
    invoke-static {v4, v8}, Lcom/google/android/music/utils/TypefaceUtil;->setTypeface(Landroid/widget/TextView;I)V

    .line 368
    const v5, 0x7f0b0279

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 371
    const v5, 0x7f0e017b

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 372
    .local v2, "content":Landroid/widget/TextView;
    invoke-static {v2, v8}, Lcom/google/android/music/utils/TypefaceUtil;->setTypeface(Landroid/widget/TextView;I)V

    .line 373
    const v5, 0x7f0b027a

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(I)V

    .line 375
    const v5, 0x7f0e017c

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 376
    .local v0, "actionButton":Landroid/view/View;
    new-instance v5, Lcom/google/android/music/ui/TutorialCardsFactory$7;

    invoke-direct {v5, p0, v1, p2}, Lcom/google/android/music/ui/TutorialCardsFactory$7;-><init>(Lcom/google/android/music/ui/MusicFragment;Landroid/widget/LinearLayout;Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;)V

    invoke-virtual {v0, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 384
    return-object v1
.end method

.method private static buildResubscribeNautilusCardWithImage(Lcom/google/android/music/ui/MusicFragment;Landroid/content/Context;Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;)Landroid/widget/LinearLayout;
    .locals 13
    .param p0, "fragment"    # Lcom/google/android/music/ui/MusicFragment;
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "dismissHandler"    # Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;

    .prologue
    const v12, 0x7f0f0156

    const/4 v11, 0x2

    .line 305
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v7

    const v8, 0x7f0400d2

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 309
    .local v0, "card":Landroid/widget/LinearLayout;
    invoke-static {p1}, Lcom/google/android/music/utils/MusicUtils;->isLandscape(Landroid/content/Context;)Z

    move-result v7

    if-nez v7, :cond_0

    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/music/preferences/MusicPreferences;->isTabletMusicExperience()Z

    move-result v7

    if-nez v7, :cond_0

    .line 311
    const v7, 0x7f0e0179

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 312
    .local v3, "header":Landroid/widget/ImageView;
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f020181

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 313
    .local v2, "drawable":Landroid/graphics/drawable/Drawable;
    const/4 v7, 0x0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8, v12}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v8

    float-to-int v8, v8

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v9

    div-int/lit8 v9, v9, 0x2

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    invoke-virtual {v10, v12}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v10

    float-to-int v10, v10

    invoke-virtual {v3, v7, v8, v9, v10}, Landroid/widget/ImageView;->setPadding(IIII)V

    .line 322
    .end local v2    # "drawable":Landroid/graphics/drawable/Drawable;
    .end local v3    # "header":Landroid/widget/ImageView;
    :cond_0
    const v7, 0x7f0e017a

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 323
    .local v6, "title":Landroid/widget/TextView;
    invoke-static {v6, v11}, Lcom/google/android/music/utils/TypefaceUtil;->setTypeface(Landroid/widget/TextView;I)V

    .line 325
    const v7, 0x7f0e017b

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 326
    .local v1, "content":Landroid/widget/TextView;
    invoke-static {v1, v11}, Lcom/google/android/music/utils/TypefaceUtil;->setTypeface(Landroid/widget/TextView;I)V

    .line 328
    const v7, 0x7f0e024e

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 329
    .local v5, "resubscribeButton":Landroid/view/View;
    new-instance v7, Lcom/google/android/music/ui/TutorialCardsFactory$5;

    invoke-direct {v7, p0, v0, p2}, Lcom/google/android/music/ui/TutorialCardsFactory$5;-><init>(Lcom/google/android/music/ui/MusicFragment;Landroid/widget/LinearLayout;Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;)V

    invoke-virtual {v5, v7}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 341
    const v7, 0x7f0e024f

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 342
    .local v4, "noThanksButton":Landroid/view/View;
    new-instance v7, Lcom/google/android/music/ui/TutorialCardsFactory$6;

    invoke-direct {v7, p0, v0, p2}, Lcom/google/android/music/ui/TutorialCardsFactory$6;-><init>(Lcom/google/android/music/ui/MusicFragment;Landroid/widget/LinearLayout;Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;)V

    invoke-virtual {v4, v7}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 351
    return-object v0
.end method

.method private static createActionsClickListener(Lcom/google/android/music/ui/MusicFragment;Lcom/google/android/music/ui/cardlib/layout/PlayTutorialHeader;Ljava/lang/String;Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;)Landroid/widget/AdapterView$OnItemClickListener;
    .locals 1
    .param p0, "fragment"    # Lcom/google/android/music/ui/MusicFragment;
    .param p1, "header"    # Lcom/google/android/music/ui/cardlib/layout/PlayTutorialHeader;
    .param p2, "prefKey"    # Ljava/lang/String;
    .param p3, "dismissHandler"    # Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;

    .prologue
    .line 709
    new-instance v0, Lcom/google/android/music/ui/TutorialCardsFactory$9;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/music/ui/TutorialCardsFactory$9;-><init>(Lcom/google/android/music/ui/MusicFragment;Lcom/google/android/music/ui/cardlib/layout/PlayTutorialHeader;Ljava/lang/String;Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;)V

    return-object v0
.end method

.method private static createBasicTextTutorial(Lcom/google/android/music/ui/MusicFragment;Landroid/view/ViewGroup;Ljava/lang/String;III[Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;)Lcom/google/android/music/ui/cardlib/layout/PlayTutorialHeader;
    .locals 11
    .param p0, "fragment"    # Lcom/google/android/music/ui/MusicFragment;
    .param p1, "parentView"    # Landroid/view/ViewGroup;
    .param p2, "prefKey"    # Ljava/lang/String;
    .param p3, "titleResId"    # I
    .param p4, "bodyTextResId"    # I
    .param p5, "bodyIconResId"    # I
    .param p6, "actions"    # [Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;
    .param p7, "dismissHandler"    # Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;

    .prologue
    .line 650
    invoke-static {p2}, Lcom/google/android/music/ui/TutorialCardsFactory;->shouldShowCard(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_0

    const/4 v5, 0x0

    .line 678
    :goto_0
    return-object v5

    .line 652
    :cond_0
    invoke-interface {p0}, Lcom/google/android/music/ui/MusicFragment;->getFragment()Landroid/support/v4/app/Fragment;

    move-result-object v8

    invoke-virtual {v8}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    .line 653
    .local v3, "context":Landroid/content/Context;
    invoke-static {v3, p1}, Lcom/google/android/music/ui/cardlib/layout/PlayTutorialHeader;->create(Landroid/content/Context;Landroid/view/ViewGroup;)Lcom/google/android/music/ui/cardlib/layout/PlayTutorialHeader;

    move-result-object v5

    .line 654
    .local v5, "header":Lcom/google/android/music/ui/cardlib/layout/PlayTutorialHeader;
    invoke-virtual {v5, p3}, Lcom/google/android/music/ui/cardlib/layout/PlayTutorialHeader;->setTitleResId(I)V

    .line 656
    const v8, 0x7f0400c5

    invoke-virtual {v5, v8}, Lcom/google/android/music/ui/cardlib/layout/PlayTutorialHeader;->inflateBody(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 658
    .local v2, "bodyText":Landroid/widget/TextView;
    const/4 v8, 0x1

    invoke-static {v2, v8}, Lcom/google/android/music/utils/TypefaceUtil;->setTypeface(Landroid/widget/TextView;I)V

    .line 660
    if-nez p5, :cond_1

    .line 661
    invoke-virtual {v2, p4}, Landroid/widget/TextView;->setText(I)V

    .line 676
    :goto_1
    move-object/from16 v0, p7

    invoke-static {p0, v5, p2, v0}, Lcom/google/android/music/ui/TutorialCardsFactory;->createActionsClickListener(Lcom/google/android/music/ui/MusicFragment;Lcom/google/android/music/ui/cardlib/layout/PlayTutorialHeader;Ljava/lang/String;Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;)Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v8

    move-object/from16 v0, p6

    invoke-virtual {v5, v0, v8}, Lcom/google/android/music/ui/cardlib/layout/PlayTutorialHeader;->setupActionsList([Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;Landroid/widget/AdapterView$OnItemClickListener;)V

    goto :goto_0

    .line 663
    :cond_1
    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    const-string v10, "__#icn1#__"

    aput-object v10, v8, v9

    invoke-virtual {v3, p4, v8}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 664
    .local v1, "bodyString":Ljava/lang/String;
    new-instance v7, Landroid/text/SpannableStringBuilder;

    invoke-direct {v7, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 665
    .local v7, "ssb":Landroid/text/SpannableStringBuilder;
    const-string v8, "__#icn1#__"

    invoke-virtual {v1, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    .line 666
    .local v4, "hackStart":I
    new-instance v6, Lcom/google/android/music/ui/TutorialCardsFactory$ImageSpanHack;

    const/4 v8, 0x0

    move/from16 v0, p5

    invoke-direct {v6, v3, v0, v8}, Lcom/google/android/music/ui/TutorialCardsFactory$ImageSpanHack;-><init>(Landroid/content/Context;II)V

    .line 668
    .local v6, "is":Landroid/text/style/ImageSpan;
    const-string v8, "__#icn1#__"

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v8, v4

    const/16 v9, 0x21

    invoke-virtual {v7, v6, v4, v8, v9}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 670
    sget-object v8, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    invoke-virtual {v2, v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 673
    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    const-string v10, ""

    aput-object v10, v8, v9

    invoke-virtual {v3, p4, v8}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method private static createBulletsTextTutorial(Lcom/google/android/music/ui/MusicFragment;Landroid/view/ViewGroup;Ljava/lang/String;I[Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;[Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;)Lcom/google/android/music/ui/cardlib/layout/PlayTutorialHeader;
    .locals 3
    .param p0, "fragment"    # Lcom/google/android/music/ui/MusicFragment;
    .param p1, "parentView"    # Landroid/view/ViewGroup;
    .param p2, "prefKey"    # Ljava/lang/String;
    .param p3, "titleResId"    # I
    .param p4, "bullets"    # [Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;
    .param p5, "actions"    # [Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;
    .param p6, "dismissHandler"    # Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;

    .prologue
    .line 689
    invoke-static {p2}, Lcom/google/android/music/ui/TutorialCardsFactory;->shouldShowCard(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v1, 0x0

    .line 697
    :goto_0
    return-object v1

    .line 691
    :cond_0
    invoke-interface {p0}, Lcom/google/android/music/ui/MusicFragment;->getFragment()Landroid/support/v4/app/Fragment;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 692
    .local v0, "context":Landroid/content/Context;
    invoke-static {v0, p1}, Lcom/google/android/music/ui/cardlib/layout/PlayTutorialHeader;->create(Landroid/content/Context;Landroid/view/ViewGroup;)Lcom/google/android/music/ui/cardlib/layout/PlayTutorialHeader;

    move-result-object v1

    .line 693
    .local v1, "header":Lcom/google/android/music/ui/cardlib/layout/PlayTutorialHeader;
    invoke-virtual {v1, p3}, Lcom/google/android/music/ui/cardlib/layout/PlayTutorialHeader;->setTitleResId(I)V

    .line 694
    invoke-virtual {v1, p4}, Lcom/google/android/music/ui/cardlib/layout/PlayTutorialHeader;->inflateBody([Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;)V

    .line 695
    invoke-static {p0, v1, p2, p6}, Lcom/google/android/music/ui/TutorialCardsFactory;->createActionsClickListener(Lcom/google/android/music/ui/MusicFragment;Lcom/google/android/music/ui/cardlib/layout/PlayTutorialHeader;Ljava/lang/String;Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;)Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v2

    invoke-virtual {v1, p5, v2}, Lcom/google/android/music/ui/cardlib/layout/PlayTutorialHeader;->setupActionsList([Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;Landroid/widget/AdapterView$OnItemClickListener;)V

    goto :goto_0
.end method

.method private static dismissTutorialCard(Lcom/google/android/music/ui/MusicFragment;Landroid/widget/LinearLayout;Ljava/lang/String;Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;)V
    .locals 2
    .param p0, "fragment"    # Lcom/google/android/music/ui/MusicFragment;
    .param p1, "header"    # Landroid/widget/LinearLayout;
    .param p2, "prefKey"    # Ljava/lang/String;
    .param p3, "dismissHandler"    # Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;

    .prologue
    .line 730
    if-nez p3, :cond_0

    .line 731
    invoke-static {p1}, Lcom/google/android/music/utils/ViewUtils;->removeViewFromParent(Landroid/view/View;)V

    .line 735
    :goto_0
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    .line 736
    .local v0, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    const/4 v1, 0x0

    invoke-virtual {v0, p2, v1}, Lcom/google/android/music/preferences/MusicPreferences;->setShouldShowTutorialCard(Ljava/lang/String;Z)V

    .line 737
    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->setLastTimeTutorialCardDismissed()V

    .line 738
    invoke-interface {p0}, Lcom/google/android/music/ui/MusicFragment;->onTutorialCardClosed()V

    .line 739
    return-void

    .line 733
    .end local v0    # "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    :cond_0
    invoke-interface {p3, p1}, Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;->onDismissTutorialCard(Landroid/view/View;)V

    goto :goto_0
.end method

.method private static getMainstageTutorialCardToShow(Landroid/content/Context;)I
    .locals 14
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v8, -0x1

    const/4 v9, 0x1

    .line 536
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v1

    .line 538
    .local v1, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 539
    .local v2, "currentTime":J
    invoke-virtual {v1}, Lcom/google/android/music/preferences/MusicPreferences;->getLastTimeTutorialCardDismissed()J

    move-result-wide v4

    .line 540
    .local v4, "lastTimeTutorialCardDismissed":J
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    const-string v11, "music_show_tutorial_cards_threshold_sec"

    const-wide/32 v12, 0x15180

    invoke-static {v10, v11, v12, v13}, Lcom/google/android/gsf/Gservices;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v10

    const-wide/16 v12, 0x3e8

    mul-long v6, v10, v12

    .line 544
    .local v6, "tutorialCardThreshold":J
    sub-long v10, v2, v4

    cmp-long v10, v10, v6

    if-gez v10, :cond_1

    .line 590
    :cond_0
    :goto_0
    return v8

    .line 548
    :cond_1
    invoke-virtual {v1}, Lcom/google/android/music/preferences/MusicPreferences;->isNautilusEnabled()Z

    move-result v10

    if-nez v10, :cond_2

    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/android/music/ui/UIStateManager;->isNetworkConnected()Z

    move-result v10

    if-eqz v10, :cond_2

    move v0, v9

    .line 551
    .local v0, "canShowNautilusSignupCard":Z
    :goto_1
    if-eqz v0, :cond_3

    invoke-static {}, Lcom/google/android/music/utils/ConfigUtils;->wasEverInNautilus()Z

    move-result v10

    if-eqz v10, :cond_3

    const-string v10, "ResubscribeNautilus"

    invoke-static {v10}, Lcom/google/android/music/ui/TutorialCardsFactory;->shouldShowCard(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 553
    const/4 v8, 0x3

    goto :goto_0

    .line 548
    .end local v0    # "canShowNautilusSignupCard":Z
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 557
    .restart local v0    # "canShowNautilusSignupCard":Z
    :cond_3
    invoke-static {}, Lcom/google/android/music/ui/TutorialCardsFactory;->hasSubscription()Z

    move-result v10

    if-eqz v10, :cond_5

    const-string v10, "Mainstage"

    invoke-static {v10}, Lcom/google/android/music/ui/TutorialCardsFactory;->shouldShowCard(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 558
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/music/preferences/MusicPreferences;->getShowSongzaWelcomeCard()Z

    move-result v8

    if-eqz v8, :cond_4

    .line 559
    const/4 v8, 0x7

    goto :goto_0

    :cond_4
    move v8, v9

    .line 561
    goto :goto_0

    .line 566
    :cond_5
    invoke-static {}, Lcom/google/android/music/ui/TutorialCardsFactory;->hasSubscription()Z

    move-result v10

    if-eqz v10, :cond_6

    const-string v10, "MainstageYoutube"

    invoke-static {v10}, Lcom/google/android/music/ui/TutorialCardsFactory;->shouldShowCard(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_6

    invoke-static {}, Lcom/google/android/music/utils/ConfigUtils;->isVsAvailable()Z

    move-result v10

    if-eqz v10, :cond_6

    .line 568
    const/4 v8, 0x6

    goto :goto_0

    .line 571
    :cond_6
    invoke-static {}, Lcom/google/android/music/ui/TutorialCardsFactory;->hasSubscription()Z

    move-result v10

    if-eqz v10, :cond_7

    const-string v10, "MainstageSituations"

    invoke-static {v10}, Lcom/google/android/music/ui/TutorialCardsFactory;->shouldShowCard(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_7

    invoke-static {}, Lcom/google/android/music/utils/ConfigUtils;->isConciergeListenNowEnabled()Z

    move-result v10

    if-eqz v10, :cond_7

    .line 573
    const/4 v8, 0x4

    goto :goto_0

    .line 576
    :cond_7
    invoke-static {}, Lcom/google/android/music/ui/TutorialCardsFactory;->hasSubscription()Z

    move-result v10

    if-nez v10, :cond_8

    const-string v10, "MainstageBasic"

    invoke-static {v10}, Lcom/google/android/music/ui/TutorialCardsFactory;->shouldShowCard(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_8

    move v8, v9

    .line 577
    goto/16 :goto_0

    .line 580
    :cond_8
    if-eqz v0, :cond_9

    invoke-static {}, Lcom/google/android/music/utils/ConfigUtils;->wasEverInNautilus()Z

    move-result v9

    if-nez v9, :cond_9

    const-string v9, "MainstageBasicTryNautilusAgain5"

    invoke-static {v9}, Lcom/google/android/music/ui/TutorialCardsFactory;->shouldShowCard(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_9

    invoke-virtual {v1}, Lcom/google/android/music/preferences/MusicPreferences;->getNautilusStatus()Lcom/google/android/music/NautilusStatus;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/android/music/NautilusStatus;->isFreeTrialAvailable()Z

    move-result v9

    if-eqz v9, :cond_9

    .line 583
    const/4 v8, 0x2

    goto/16 :goto_0

    .line 586
    :cond_9
    invoke-static {p0}, Lcom/google/android/music/ui/TutorialCardsFactory;->shouldShowMainstageWearCard(Landroid/content/Context;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 587
    const/4 v8, 0x5

    goto/16 :goto_0
.end method

.method private static hasSubscription()Z
    .locals 1

    .prologue
    .line 517
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->isNautilusEnabled()Z

    move-result v0

    return v0
.end method

.method public static setupExploreTutorial(Lcom/google/android/music/ui/MusicFragment;Landroid/view/ViewGroup;Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;)V
    .locals 8
    .param p0, "fragment"    # Lcom/google/android/music/ui/MusicFragment;
    .param p1, "headerContainer"    # Landroid/view/ViewGroup;
    .param p2, "dismissHandler"    # Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;

    .prologue
    .line 509
    invoke-static {p1}, Lcom/google/android/music/ui/TutorialCardsFactory;->setupHeaderContainer(Landroid/view/ViewGroup;)Landroid/view/ViewGroup;

    move-result-object v1

    .line 511
    .local v1, "container":Landroid/view/ViewGroup;
    const-string v2, "Explore"

    const v3, 0x7f0b0284

    sget-object v4, Lcom/google/android/music/ui/TutorialCardsFactory;->EXPLORE_BULLETS:[Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;

    sget-object v5, Lcom/google/android/music/ui/TutorialCardsFactory;->ACTIONS_GOT_IT:[Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;

    move-object v0, p0

    move-object v6, p2

    invoke-static/range {v0 .. v6}, Lcom/google/android/music/ui/TutorialCardsFactory;->createBulletsTextTutorial(Lcom/google/android/music/ui/MusicFragment;Landroid/view/ViewGroup;Ljava/lang/String;I[Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;[Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;)Lcom/google/android/music/ui/cardlib/layout/PlayTutorialHeader;

    move-result-object v7

    .line 513
    .local v7, "card":Lcom/google/android/music/ui/cardlib/layout/PlayTutorialHeader;
    invoke-static {v1, v7}, Lcom/google/android/music/ui/TutorialCardsFactory;->addCard(Landroid/view/ViewGroup;Landroid/widget/LinearLayout;)Landroid/view/ViewGroup;

    .line 514
    return-void
.end method

.method private static setupHeaderContainer(Landroid/view/ViewGroup;)Landroid/view/ViewGroup;
    .locals 0
    .param p0, "headerContainer"    # Landroid/view/ViewGroup;

    .prologue
    .line 627
    invoke-virtual {p0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 628
    return-object p0
.end method

.method public static setupMyLibraryTutorial(Lcom/google/android/music/ui/MusicFragment;Landroid/view/ViewGroup;Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;)V
    .locals 9
    .param p0, "fragment"    # Lcom/google/android/music/ui/MusicFragment;
    .param p1, "headerContainer"    # Landroid/view/ViewGroup;
    .param p2, "dismissHandler"    # Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;

    .prologue
    .line 475
    invoke-static {p1}, Lcom/google/android/music/ui/TutorialCardsFactory;->setupHeaderContainer(Landroid/view/ViewGroup;)Landroid/view/ViewGroup;

    move-result-object v1

    .line 476
    .local v1, "container":Landroid/view/ViewGroup;
    invoke-static {}, Lcom/google/android/music/ui/TutorialCardsFactory;->hasSubscription()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 477
    const-string v2, "MyLibrary"

    const v3, 0x7f0b0277

    const v4, 0x7f0b0278

    const v5, 0x7f0200c7

    sget-object v6, Lcom/google/android/music/ui/TutorialCardsFactory;->ACTIONS_MY_LIBRARY:[Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;

    move-object v0, p0

    move-object v7, p2

    invoke-static/range {v0 .. v7}, Lcom/google/android/music/ui/TutorialCardsFactory;->createBasicTextTutorial(Lcom/google/android/music/ui/MusicFragment;Landroid/view/ViewGroup;Ljava/lang/String;III[Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;)Lcom/google/android/music/ui/cardlib/layout/PlayTutorialHeader;

    move-result-object v8

    .line 480
    .local v8, "card":Lcom/google/android/music/ui/cardlib/layout/PlayTutorialHeader;
    invoke-static {v1, v8}, Lcom/google/android/music/ui/TutorialCardsFactory;->addCard(Landroid/view/ViewGroup;Landroid/widget/LinearLayout;)Landroid/view/ViewGroup;

    .line 482
    .end local v8    # "card":Lcom/google/android/music/ui/cardlib/layout/PlayTutorialHeader;
    :cond_0
    return-void
.end method

.method public static setupRadioTutorial(Lcom/google/android/music/ui/MusicFragment;Landroid/view/ViewGroup;Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;)V
    .locals 8
    .param p0, "fragment"    # Lcom/google/android/music/ui/MusicFragment;
    .param p1, "headerContainer"    # Landroid/view/ViewGroup;
    .param p2, "dismissHandler"    # Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;

    .prologue
    .line 490
    invoke-static {p1}, Lcom/google/android/music/ui/TutorialCardsFactory;->setupHeaderContainer(Landroid/view/ViewGroup;)Landroid/view/ViewGroup;

    move-result-object v1

    .line 491
    .local v1, "container":Landroid/view/ViewGroup;
    const/4 v7, 0x0

    .line 492
    .local v7, "card":Lcom/google/android/music/ui/cardlib/layout/PlayTutorialHeader;
    invoke-static {}, Lcom/google/android/music/ui/TutorialCardsFactory;->hasSubscription()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 493
    const-string v2, "Radio"

    const v3, 0x7f0b027e

    sget-object v4, Lcom/google/android/music/ui/TutorialCardsFactory;->RADIO_BULLETS:[Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;

    sget-object v5, Lcom/google/android/music/ui/TutorialCardsFactory;->ACTIONS_GOT_IT:[Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;

    move-object v0, p0

    move-object v6, p2

    invoke-static/range {v0 .. v6}, Lcom/google/android/music/ui/TutorialCardsFactory;->createBulletsTextTutorial(Lcom/google/android/music/ui/MusicFragment;Landroid/view/ViewGroup;Ljava/lang/String;I[Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;[Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;)Lcom/google/android/music/ui/cardlib/layout/PlayTutorialHeader;

    move-result-object v7

    .line 501
    :goto_0
    invoke-static {v1, v7}, Lcom/google/android/music/ui/TutorialCardsFactory;->addCard(Landroid/view/ViewGroup;Landroid/widget/LinearLayout;)Landroid/view/ViewGroup;

    .line 502
    return-void

    .line 497
    :cond_0
    const-string v2, "RadioBasic"

    const v3, 0x7f0b027f

    sget-object v4, Lcom/google/android/music/ui/TutorialCardsFactory;->INSTANT_MIX_BULLETS:[Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;

    sget-object v5, Lcom/google/android/music/ui/TutorialCardsFactory;->ACTIONS_GOT_IT:[Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;

    move-object v0, p0

    move-object v6, p2

    invoke-static/range {v0 .. v6}, Lcom/google/android/music/ui/TutorialCardsFactory;->createBulletsTextTutorial(Lcom/google/android/music/ui/MusicFragment;Landroid/view/ViewGroup;Ljava/lang/String;I[Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;[Lcom/google/android/music/ui/cardlib/PlayIconAndTextListAdapter$IconAndTextListEntry;Lcom/google/android/music/ui/TutorialCardsFactory$TutorialDismissHandler;)Lcom/google/android/music/ui/cardlib/layout/PlayTutorialHeader;

    move-result-object v7

    goto :goto_0
.end method

.method private static shouldShowCard(Ljava/lang/String;)Z
    .locals 2
    .param p0, "prefKey"    # Ljava/lang/String;

    .prologue
    .line 622
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    .line 623
    .local v0, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    invoke-virtual {v0, p0}, Lcom/google/android/music/preferences/MusicPreferences;->shouldShowTutorialCard(Ljava/lang/String;)Z

    move-result v1

    return v1
.end method

.method public static shouldShowMainstageTutorialCard(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 526
    invoke-static {p0}, Lcom/google/android/music/ui/TutorialCardsFactory;->getMainstageTutorialCardToShow(Landroid/content/Context;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static shouldShowMainstageWearCard(Landroid/content/Context;)Z
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 594
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 595
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    const-string v6, "music_wear_sync_enabled"

    invoke-static {v0, v6, v4}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v2

    .line 599
    .local v2, "wearOverallGserviceEnabled":Z
    if-nez v2, :cond_1

    .line 617
    :cond_0
    :goto_0
    return v5

    .line 603
    :cond_1
    const-string v6, "music_show_wear_sync_promo"

    invoke-static {v0, v6, v4}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v3

    .line 607
    .local v3, "wearPromoGserviceEnabled":Z
    if-eqz v3, :cond_0

    .line 611
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v1

    .line 617
    .local v1, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    invoke-virtual {v1}, Lcom/google/android/music/preferences/MusicPreferences;->hasStreamingAccount()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {v1}, Lcom/google/android/music/preferences/MusicPreferences;->isWearSyncAvailable()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {v1}, Lcom/google/android/music/preferences/MusicPreferences;->isWearSyncEnabled()Z

    move-result v6

    if-nez v6, :cond_2

    const-string v6, "MainstageWear"

    invoke-static {v6}, Lcom/google/android/music/ui/TutorialCardsFactory;->shouldShowCard(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    :goto_1
    move v5, v4

    goto :goto_0

    :cond_2
    move v4, v5

    goto :goto_1
.end method
