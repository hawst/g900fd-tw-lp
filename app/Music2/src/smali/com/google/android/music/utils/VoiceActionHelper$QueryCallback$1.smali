.class Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;
.super Ljava/lang/Object;
.source "VoiceActionHelper.java"

# interfaces
.implements Lcom/google/android/music/utils/async/AsyncRunner;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback;->onQueryComplete(Landroid/database/Cursor;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mMixDescriptor:Lcom/google/android/music/mix/MixDescriptor;

.field mResultFound:Z

.field private mSonglist:Lcom/google/android/music/medialist/SongList;

.field final synthetic this$1:Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback;

.field final synthetic val$data:Landroid/database/Cursor;


# direct methods
.method constructor <init>(Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback;Landroid/database/Cursor;)V
    .locals 1

    .prologue
    .line 186
    iput-object p1, p0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->this$1:Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback;

    iput-object p2, p0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->val$data:Landroid/database/Cursor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 188
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->mResultFound:Z

    return-void
.end method


# virtual methods
.method public backgroundTask()V
    .locals 52

    .prologue
    .line 194
    :cond_0
    :goto_0
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->val$data:Landroid/database/Cursor;

    if-eqz v9, :cond_2

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->val$data:Landroid/database/Cursor;

    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v9

    if-eqz v9, :cond_2

    .line 195
    const/4 v8, 0x0

    .line 196
    .local v8, "name":Ljava/lang/String;
    const/16 v49, 0x0

    .line 197
    .local v49, "trackMetajamId":Ljava/lang/String;
    const/4 v4, 0x0

    .line 198
    .local v4, "albumMetajamId":Ljava/lang/String;
    const/16 v28, 0x0

    .line 199
    .local v28, "artistMetajamId":Ljava/lang/String;
    const/16 v18, 0x0

    .line 200
    .local v18, "artUrl":Ljava/lang/String;
    const/16 v22, 0x0

    .line 201
    .local v22, "genreName":Ljava/lang/String;
    const/16 v20, 0x0

    .line 202
    .local v20, "genreId":Ljava/lang/String;
    const/16 v38, 0x0

    .line 203
    .local v38, "genreArtUrl":Ljava/lang/String;
    const/16 v39, 0x0

    .line 204
    .local v39, "genreParentId":Ljava/lang/String;
    const/16 v29, 0x0

    .line 207
    .local v29, "descriptor":Lcom/google/android/music/store/ContainerDescriptor;
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->val$data:Landroid/database/Cursor;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->val$data:Landroid/database/Cursor;

    const-string v14, "searchType"

    invoke-interface {v12, v14}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v12

    invoke-interface {v9, v12}, Landroid/database/Cursor;->getInt(I)I

    move-result v47

    .line 216
    .local v47, "resultType":I
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->val$data:Landroid/database/Cursor;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->val$data:Landroid/database/Cursor;

    const-string v14, "searchName"

    invoke-interface {v12, v14}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v12

    invoke-interface {v9, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 218
    new-instance v46, Lcom/google/android/music/store/TagNormalizer;

    invoke-direct/range {v46 .. v46}, Lcom/google/android/music/store/TagNormalizer;-><init>()V

    .line 222
    .local v46, "normalizer":Lcom/google/android/music/store/TagNormalizer;
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->this$1:Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback;

    iget-object v9, v9, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback;->this$0:Lcom/google/android/music/utils/VoiceActionHelper;

    # getter for: Lcom/google/android/music/utils/VoiceActionHelper;->mSearchItemName:Ljava/lang/String;
    invoke-static {v9}, Lcom/google/android/music/utils/VoiceActionHelper;->access$100(Lcom/google/android/music/utils/VoiceActionHelper;)Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, v46

    invoke-virtual {v0, v9}, Lcom/google/android/music/store/TagNormalizer;->normalize(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v45

    .line 223
    .local v45, "normalizedSearchItemName":Ljava/lang/String;
    move-object/from16 v0, v46

    invoke-virtual {v0, v8}, Lcom/google/android/music/store/TagNormalizer;->normalize(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v44

    .line 224
    .local v44, "normalizedResultItemName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->this$1:Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback;

    iget-object v9, v9, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback;->this$0:Lcom/google/android/music/utils/VoiceActionHelper;

    # getter for: Lcom/google/android/music/utils/VoiceActionHelper;->mFallbackCategories:Ljava/util/List;
    invoke-static {v9}, Lcom/google/android/music/utils/VoiceActionHelper;->access$200(Lcom/google/android/music/utils/VoiceActionHelper;)Ljava/util/List;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->this$1:Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback;

    iget v12, v12, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback;->currentCategoryIndex:I

    invoke-interface {v9, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    const-string v12, "bestmatch"

    invoke-virtual {v9, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 230
    invoke-virtual/range {v44 .. v45}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 241
    :cond_1
    packed-switch v47, :pswitch_data_0

    .line 458
    sget-object v9, Lcom/google/android/music/utils/VoiceActionHelper;->TAG:Ljava/lang/String;

    const-string v12, "Unknown best match item type."

    invoke-static {v9, v12}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 460
    :goto_1
    move-object/from16 v0, p0

    iget-boolean v9, v0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->mResultFound:Z

    if-eqz v9, :cond_0

    .line 464
    .end local v4    # "albumMetajamId":Ljava/lang/String;
    .end local v8    # "name":Ljava/lang/String;
    .end local v18    # "artUrl":Ljava/lang/String;
    .end local v20    # "genreId":Ljava/lang/String;
    .end local v22    # "genreName":Ljava/lang/String;
    .end local v28    # "artistMetajamId":Ljava/lang/String;
    .end local v29    # "descriptor":Lcom/google/android/music/store/ContainerDescriptor;
    .end local v38    # "genreArtUrl":Ljava/lang/String;
    .end local v39    # "genreParentId":Ljava/lang/String;
    .end local v44    # "normalizedResultItemName":Ljava/lang/String;
    .end local v45    # "normalizedSearchItemName":Ljava/lang/String;
    .end local v46    # "normalizer":Lcom/google/android/music/store/TagNormalizer;
    .end local v47    # "resultType":I
    .end local v49    # "trackMetajamId":Ljava/lang/String;
    :cond_2
    return-void

    .line 235
    .restart local v4    # "albumMetajamId":Ljava/lang/String;
    .restart local v8    # "name":Ljava/lang/String;
    .restart local v18    # "artUrl":Ljava/lang/String;
    .restart local v20    # "genreId":Ljava/lang/String;
    .restart local v22    # "genreName":Ljava/lang/String;
    .restart local v28    # "artistMetajamId":Ljava/lang/String;
    .restart local v29    # "descriptor":Lcom/google/android/music/store/ContainerDescriptor;
    .restart local v38    # "genreArtUrl":Ljava/lang/String;
    .restart local v39    # "genreParentId":Ljava/lang/String;
    .restart local v44    # "normalizedResultItemName":Ljava/lang/String;
    .restart local v45    # "normalizedSearchItemName":Ljava/lang/String;
    .restart local v46    # "normalizer":Lcom/google/android/music/store/TagNormalizer;
    .restart local v47    # "resultType":I
    .restart local v49    # "trackMetajamId":Ljava/lang/String;
    :cond_3
    invoke-virtual/range {v44 .. v45}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_1

    goto/16 :goto_0

    .line 244
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->val$data:Landroid/database/Cursor;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->val$data:Landroid/database/Cursor;

    const-string v14, "_id"

    invoke-interface {v12, v14}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v12

    invoke-interface {v9, v12}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 246
    .local v6, "artistId":J
    new-instance v5, Lcom/google/android/music/medialist/ArtistSongList;

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-direct/range {v5 .. v10}, Lcom/google/android/music/medialist/ArtistSongList;-><init>(JLjava/lang/String;IZ)V

    .line 248
    .local v5, "artistSongList":Lcom/google/android/music/medialist/SongList;
    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->mSonglist:Lcom/google/android/music/medialist/SongList;

    .line 253
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->this$1:Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback;

    iget-boolean v9, v9, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback;->eyesFree:Z

    if-nez v9, :cond_4

    .line 254
    new-instance v9, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1$1;

    move-object/from16 v0, p0

    invoke-direct {v9, v0, v5}, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1$1;-><init>(Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;Lcom/google/android/music/medialist/SongList;)V

    invoke-static {v9}, Lcom/google/android/music/utils/MusicUtils;->runAsync(Ljava/lang/Runnable;)V

    .line 262
    :cond_4
    const/4 v9, 0x1

    move-object/from16 v0, p0

    iput-boolean v9, v0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->mResultFound:Z

    goto :goto_1

    .line 265
    .end local v5    # "artistSongList":Lcom/google/android/music/medialist/SongList;
    .end local v6    # "artistId":J
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->val$data:Landroid/database/Cursor;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->val$data:Landroid/database/Cursor;

    const-string v14, "ArtistMetajamId"

    invoke-interface {v12, v14}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v12

    invoke-interface {v9, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v28

    .line 268
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->this$1:Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback;

    iget-boolean v9, v9, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback;->eyesFree:Z

    if-nez v9, :cond_5

    .line 269
    new-instance v41, Lcom/google/android/music/medialist/NautilusArtistSongList;

    move-object/from16 v0, v41

    move-object/from16 v1, v28

    invoke-direct {v0, v1, v8}, Lcom/google/android/music/medialist/NautilusArtistSongList;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 271
    .local v41, "nautArtistSongList":Lcom/google/android/music/medialist/SongList;
    move-object/from16 v0, v41

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->mSonglist:Lcom/google/android/music/medialist/SongList;

    .line 272
    new-instance v9, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1$2;

    move-object/from16 v0, p0

    move-object/from16 v1, v41

    invoke-direct {v9, v0, v1}, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1$2;-><init>(Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;Lcom/google/android/music/medialist/SongList;)V

    invoke-static {v9}, Lcom/google/android/music/utils/MusicUtils;->runAsync(Ljava/lang/Runnable;)V

    .line 284
    .end local v41    # "nautArtistSongList":Lcom/google/android/music/medialist/SongList;
    :goto_2
    const/4 v9, 0x1

    move-object/from16 v0, p0

    iput-boolean v9, v0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->mResultFound:Z

    goto :goto_1

    .line 280
    :cond_5
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->this$1:Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback;

    iget-object v9, v9, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback;->this$0:Lcom/google/android/music/utils/VoiceActionHelper;

    # getter for: Lcom/google/android/music/utils/VoiceActionHelper;->mContext:Landroid/content/Context;
    invoke-static {v9}, Lcom/google/android/music/utils/VoiceActionHelper;->access$300(Lcom/google/android/music/utils/VoiceActionHelper;)Landroid/content/Context;

    move-result-object v9

    move-object/from16 v0, v28

    invoke-static {v9, v0, v8}, Lcom/google/android/music/ui/CreateArtistShuffleActivity;->getArtistShuffleSongList(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/music/medialist/SongList;

    move-result-object v9

    move-object/from16 v0, p0

    iput-object v9, v0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->mSonglist:Lcom/google/android/music/medialist/SongList;

    goto :goto_2

    .line 287
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->val$data:Landroid/database/Cursor;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->val$data:Landroid/database/Cursor;

    const-string v14, "_id"

    invoke-interface {v12, v14}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v12

    invoke-interface {v9, v12}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v26

    .line 289
    .local v26, "albumId":J
    new-instance v9, Lcom/google/android/music/medialist/AlbumSongList;

    const/4 v12, 0x0

    move-wide/from16 v0, v26

    invoke-direct {v9, v0, v1, v12}, Lcom/google/android/music/medialist/AlbumSongList;-><init>(JZ)V

    move-object/from16 v0, p0

    iput-object v9, v0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->mSonglist:Lcom/google/android/music/medialist/SongList;

    .line 294
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->this$1:Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback;

    iget-boolean v9, v9, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback;->eyesFree:Z

    if-nez v9, :cond_6

    .line 295
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->mSonglist:Lcom/google/android/music/medialist/SongList;

    move-object/from16 v30, v0

    .line 296
    .local v30, "finalAlbumSonglist":Lcom/google/android/music/medialist/SongList;
    new-instance v9, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1$3;

    move-object/from16 v0, p0

    move-object/from16 v1, v30

    invoke-direct {v9, v0, v1}, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1$3;-><init>(Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;Lcom/google/android/music/medialist/SongList;)V

    invoke-static {v9}, Lcom/google/android/music/utils/MusicUtils;->runAsync(Ljava/lang/Runnable;)V

    .line 304
    .end local v30    # "finalAlbumSonglist":Lcom/google/android/music/medialist/SongList;
    :cond_6
    const/4 v9, 0x1

    move-object/from16 v0, p0

    iput-boolean v9, v0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->mResultFound:Z

    goto/16 :goto_1

    .line 307
    .end local v26    # "albumId":J
    :pswitch_3
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->val$data:Landroid/database/Cursor;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->val$data:Landroid/database/Cursor;

    const-string v14, "StoreAlbumId"

    invoke-interface {v12, v14}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v12

    invoke-interface {v9, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 309
    new-instance v9, Lcom/google/android/music/medialist/NautilusAlbumSongList;

    invoke-direct {v9, v4}, Lcom/google/android/music/medialist/NautilusAlbumSongList;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v9, v0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->mSonglist:Lcom/google/android/music/medialist/SongList;

    .line 314
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->mSonglist:Lcom/google/android/music/medialist/SongList;

    move-object/from16 v34, v0

    .line 315
    .local v34, "finalNautAlbumSonglist":Lcom/google/android/music/medialist/SongList;
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->this$1:Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback;

    iget-boolean v9, v9, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback;->eyesFree:Z

    if-nez v9, :cond_7

    .line 316
    new-instance v9, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1$4;

    move-object/from16 v0, p0

    move-object/from16 v1, v34

    invoke-direct {v9, v0, v1}, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1$4;-><init>(Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;Lcom/google/android/music/medialist/SongList;)V

    invoke-static {v9}, Lcom/google/android/music/utils/MusicUtils;->runAsync(Ljava/lang/Runnable;)V

    .line 324
    :cond_7
    const/4 v9, 0x1

    move-object/from16 v0, p0

    iput-boolean v9, v0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->mResultFound:Z

    goto/16 :goto_1

    .line 327
    .end local v34    # "finalNautAlbumSonglist":Lcom/google/android/music/medialist/SongList;
    :pswitch_4
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->val$data:Landroid/database/Cursor;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->val$data:Landroid/database/Cursor;

    const-string v14, "_id"

    invoke-interface {v12, v14}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v12

    invoke-interface {v9, v12}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 329
    .local v10, "playlistId":J
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->val$data:Landroid/database/Cursor;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->val$data:Landroid/database/Cursor;

    const-string v14, "ListType"

    invoke-interface {v12, v14}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v12

    invoke-interface {v9, v12}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    .line 331
    .local v13, "playlistType":I
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->val$data:Landroid/database/Cursor;

    const-string v12, "artworkUrl"

    invoke-interface {v9, v12}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v25

    .line 333
    .local v25, "artUrlIndex":I
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->val$data:Landroid/database/Cursor;

    move/from16 v0, v25

    invoke-interface {v9, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v9

    if-nez v9, :cond_8

    .line 334
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->val$data:Landroid/database/Cursor;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->val$data:Landroid/database/Cursor;

    const-string v14, "artworkUrl"

    invoke-interface {v12, v14}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v12

    invoke-interface {v9, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v18

    .line 337
    :cond_8
    new-instance v9, Lcom/google/android/music/medialist/PlaylistSongList;

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v19, 0x0

    move-object v12, v8

    invoke-direct/range {v9 .. v19}, Lcom/google/android/music/medialist/PlaylistSongList;-><init>(JLjava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    move-object/from16 v0, p0

    iput-object v9, v0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->mSonglist:Lcom/google/android/music/medialist/SongList;

    .line 343
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->mSonglist:Lcom/google/android/music/medialist/SongList;

    move-object/from16 v36, v0

    .line 344
    .local v36, "finalPlaylistSonglist":Lcom/google/android/music/medialist/SongList;
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->this$1:Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback;

    iget-boolean v9, v9, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback;->eyesFree:Z

    if-nez v9, :cond_9

    .line 345
    new-instance v9, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1$5;

    move-object/from16 v0, p0

    move-object/from16 v1, v36

    invoke-direct {v9, v0, v1}, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1$5;-><init>(Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;Lcom/google/android/music/medialist/SongList;)V

    invoke-static {v9}, Lcom/google/android/music/utils/MusicUtils;->runAsync(Ljava/lang/Runnable;)V

    .line 353
    :cond_9
    const/4 v9, 0x1

    move-object/from16 v0, p0

    iput-boolean v9, v0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->mResultFound:Z

    goto/16 :goto_1

    .line 356
    .end local v10    # "playlistId":J
    .end local v13    # "playlistType":I
    .end local v25    # "artUrlIndex":I
    .end local v36    # "finalPlaylistSonglist":Lcom/google/android/music/medialist/SongList;
    :pswitch_5
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->val$data:Landroid/database/Cursor;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->val$data:Landroid/database/Cursor;

    const-string v14, "_id"

    invoke-interface {v12, v14}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v12

    invoke-interface {v9, v12}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v42

    .line 358
    .local v42, "musicId":J
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->val$data:Landroid/database/Cursor;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->val$data:Landroid/database/Cursor;

    const-string v14, "AlbumId"

    invoke-interface {v12, v14}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v12

    invoke-interface {v9, v12}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v26

    .line 361
    .restart local v26    # "albumId":J
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->this$1:Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback;

    iget-object v9, v9, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback;->this$0:Lcom/google/android/music/utils/VoiceActionHelper;

    # getter for: Lcom/google/android/music/utils/VoiceActionHelper;->mSearchActivityString:Ljava/lang/String;
    invoke-static {v9}, Lcom/google/android/music/utils/VoiceActionHelper;->access$500(Lcom/google/android/music/utils/VoiceActionHelper;)Ljava/lang/String;

    move-result-object v9

    move-wide/from16 v0, v42

    invoke-static {v0, v1, v9}, Lcom/google/android/music/store/ContainerDescriptor;->newSearchResultsDescriptor(JLjava/lang/String;)Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v29

    .line 363
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->val$data:Landroid/database/Cursor;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->val$data:Landroid/database/Cursor;

    const-string v14, "SongId"

    invoke-interface {v12, v14}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v12

    invoke-interface {v9, v12}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v50

    .line 369
    .local v50, "trackSongId":J
    new-instance v37, Lcom/google/android/music/medialist/SingleSongList;

    move-object/from16 v0, v37

    move-object/from16 v1, v29

    move-wide/from16 v2, v42

    invoke-direct {v0, v1, v2, v3, v8}, Lcom/google/android/music/medialist/SingleSongList;-><init>(Lcom/google/android/music/store/ContainerDescriptor;JLjava/lang/String;)V

    .line 371
    .local v37, "finalTrackSonglist":Lcom/google/android/music/medialist/SongList;
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->this$1:Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback;

    iget-object v9, v9, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback;->this$0:Lcom/google/android/music/utils/VoiceActionHelper;

    # getter for: Lcom/google/android/music/utils/VoiceActionHelper;->mContext:Landroid/content/Context;
    invoke-static {v9}, Lcom/google/android/music/utils/VoiceActionHelper;->access$300(Lcom/google/android/music/utils/VoiceActionHelper;)Landroid/content/Context;

    move-result-object v9

    move-object/from16 v0, v37

    invoke-static {v9, v0}, Lcom/google/android/music/utils/MusicUtils;->getSongListRadioMixDescriptor(Landroid/content/Context;Lcom/google/android/music/medialist/SongList;)Lcom/google/android/music/mix/MixDescriptor;

    move-result-object v9

    move-object/from16 v0, p0

    iput-object v9, v0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->mMixDescriptor:Lcom/google/android/music/mix/MixDescriptor;

    .line 373
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->this$1:Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback;

    iget-boolean v9, v9, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback;->eyesFree:Z

    if-nez v9, :cond_a

    .line 374
    new-instance v9, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1$6;

    move-object/from16 v0, p0

    move-object/from16 v1, v37

    invoke-direct {v9, v0, v1}, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1$6;-><init>(Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;Lcom/google/android/music/medialist/SongList;)V

    invoke-static {v9}, Lcom/google/android/music/utils/MusicUtils;->runAsync(Ljava/lang/Runnable;)V

    .line 382
    :cond_a
    const/4 v9, 0x1

    move-object/from16 v0, p0

    iput-boolean v9, v0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->mResultFound:Z

    goto/16 :goto_1

    .line 385
    .end local v26    # "albumId":J
    .end local v37    # "finalTrackSonglist":Lcom/google/android/music/medialist/SongList;
    .end local v42    # "musicId":J
    .end local v50    # "trackSongId":J
    :pswitch_6
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->val$data:Landroid/database/Cursor;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->val$data:Landroid/database/Cursor;

    const-string v14, "Nid"

    invoke-interface {v12, v14}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v12

    invoke-interface {v9, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v49

    .line 387
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->val$data:Landroid/database/Cursor;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->val$data:Landroid/database/Cursor;

    const-string v14, "StoreAlbumId"

    invoke-interface {v12, v14}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v12

    invoke-interface {v9, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 390
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->this$1:Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback;

    iget-object v9, v9, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback;->this$0:Lcom/google/android/music/utils/VoiceActionHelper;

    # getter for: Lcom/google/android/music/utils/VoiceActionHelper;->mSearchActivityString:Ljava/lang/String;
    invoke-static {v9}, Lcom/google/android/music/utils/VoiceActionHelper;->access$500(Lcom/google/android/music/utils/VoiceActionHelper;)Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, v49

    invoke-static {v0, v9}, Lcom/google/android/music/store/ContainerDescriptor;->newSearchResultsDescriptor(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v29

    .line 396
    new-instance v35, Lcom/google/android/music/medialist/NautilusSingleSongList;

    move-object/from16 v0, v35

    move-object/from16 v1, v29

    move-object/from16 v2, v49

    invoke-direct {v0, v1, v2, v8}, Lcom/google/android/music/medialist/NautilusSingleSongList;-><init>(Lcom/google/android/music/store/ContainerDescriptor;Ljava/lang/String;Ljava/lang/String;)V

    .line 398
    .local v35, "finalNautTrackSonglist":Lcom/google/android/music/medialist/SongList;
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->this$1:Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback;

    iget-object v9, v9, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback;->this$0:Lcom/google/android/music/utils/VoiceActionHelper;

    # getter for: Lcom/google/android/music/utils/VoiceActionHelper;->mContext:Landroid/content/Context;
    invoke-static {v9}, Lcom/google/android/music/utils/VoiceActionHelper;->access$300(Lcom/google/android/music/utils/VoiceActionHelper;)Landroid/content/Context;

    move-result-object v9

    move-object/from16 v0, v35

    invoke-static {v9, v0}, Lcom/google/android/music/utils/MusicUtils;->getSongListRadioMixDescriptor(Landroid/content/Context;Lcom/google/android/music/medialist/SongList;)Lcom/google/android/music/mix/MixDescriptor;

    move-result-object v9

    move-object/from16 v0, p0

    iput-object v9, v0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->mMixDescriptor:Lcom/google/android/music/mix/MixDescriptor;

    .line 400
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->this$1:Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback;

    iget-boolean v9, v9, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback;->eyesFree:Z

    if-nez v9, :cond_b

    .line 401
    new-instance v9, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1$7;

    move-object/from16 v0, p0

    move-object/from16 v1, v35

    invoke-direct {v9, v0, v1}, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1$7;-><init>(Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;Lcom/google/android/music/medialist/SongList;)V

    invoke-static {v9}, Lcom/google/android/music/utils/MusicUtils;->runAsync(Ljava/lang/Runnable;)V

    .line 409
    :cond_b
    const/4 v9, 0x1

    move-object/from16 v0, p0

    iput-boolean v9, v0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->mResultFound:Z

    goto/16 :goto_1

    .line 412
    .end local v35    # "finalNautTrackSonglist":Lcom/google/android/music/medialist/SongList;
    :pswitch_7
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->val$data:Landroid/database/Cursor;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->val$data:Landroid/database/Cursor;

    const-string v14, "name"

    invoke-interface {v12, v14}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v12

    invoke-interface {v9, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v22

    .line 414
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->val$data:Landroid/database/Cursor;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->val$data:Landroid/database/Cursor;

    const-string v14, "genreServerId"

    invoke-interface {v12, v14}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v12

    invoke-interface {v9, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v20

    .line 416
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->val$data:Landroid/database/Cursor;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->val$data:Landroid/database/Cursor;

    const-string v14, "parentGenreId"

    invoke-interface {v12, v14}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v12

    invoke-interface {v9, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v39

    .line 418
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->val$data:Landroid/database/Cursor;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->val$data:Landroid/database/Cursor;

    const-string v14, "subgenreCount"

    invoke-interface {v12, v14}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v12

    invoke-interface {v9, v12}, Landroid/database/Cursor;->getInt(I)I

    move-result v48

    .line 420
    .local v48, "subgenreCount":I
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->val$data:Landroid/database/Cursor;

    const-string v12, "genreArtUris"

    invoke-interface {v9, v12}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v25

    .line 423
    .restart local v25    # "artUrlIndex":I
    new-instance v19, Lcom/google/android/music/mix/MixDescriptor;

    sget-object v21, Lcom/google/android/music/mix/MixDescriptor$Type;->GENRE_SEED:Lcom/google/android/music/mix/MixDescriptor$Type;

    const/16 v23, 0x0

    const/16 v24, 0x0

    invoke-direct/range {v19 .. v24}, Lcom/google/android/music/mix/MixDescriptor;-><init>(Ljava/lang/String;Lcom/google/android/music/mix/MixDescriptor$Type;Ljava/lang/String;Ljava/lang/String;Z)V

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->mMixDescriptor:Lcom/google/android/music/mix/MixDescriptor;

    .line 426
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->val$data:Landroid/database/Cursor;

    move/from16 v0, v25

    invoke-interface {v9, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v9

    if-nez v9, :cond_c

    .line 427
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->val$data:Landroid/database/Cursor;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->val$data:Landroid/database/Cursor;

    const-string v14, "genreArtUris"

    invoke-interface {v12, v14}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v12

    invoke-interface {v9, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v38

    .line 431
    :cond_c
    invoke-static/range {v39 .. v39}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_e

    .line 432
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->this$1:Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback;

    iget-object v9, v9, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback;->this$0:Lcom/google/android/music/utils/VoiceActionHelper;

    # getter for: Lcom/google/android/music/utils/VoiceActionHelper;->mContext:Landroid/content/Context;
    invoke-static {v9}, Lcom/google/android/music/utils/VoiceActionHelper;->access$300(Lcom/google/android/music/utils/VoiceActionHelper;)Landroid/content/Context;

    move-result-object v9

    const/4 v12, 0x1

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    move/from16 v2, v48

    invoke-static {v9, v0, v1, v2, v12}, Lcom/google/android/music/ui/GenresExploreActivity;->buildStartIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;II)Landroid/content/Intent;

    move-result-object v40

    .line 443
    .local v40, "i":Landroid/content/Intent;
    :goto_3
    move-object/from16 v33, v22

    .line 444
    .local v33, "finalGenreName":Ljava/lang/String;
    move-object/from16 v32, v20

    .line 445
    .local v32, "finalGenreId":Ljava/lang/String;
    move-object/from16 v31, v38

    .line 446
    .local v31, "finalGenreArtUrl":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->this$1:Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback;

    iget-boolean v9, v9, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback;->eyesFree:Z

    if-nez v9, :cond_d

    .line 447
    new-instance v9, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1$8;

    move-object/from16 v0, p0

    move-object/from16 v1, v33

    move-object/from16 v2, v32

    move-object/from16 v3, v31

    invoke-direct {v9, v0, v1, v2, v3}, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1$8;-><init>(Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v9}, Lcom/google/android/music/utils/MusicUtils;->runAsync(Ljava/lang/Runnable;)V

    .line 455
    :cond_d
    const/4 v9, 0x1

    move-object/from16 v0, p0

    iput-boolean v9, v0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->mResultFound:Z

    goto/16 :goto_1

    .line 436
    .end local v31    # "finalGenreArtUrl":Ljava/lang/String;
    .end local v32    # "finalGenreId":Ljava/lang/String;
    .end local v33    # "finalGenreName":Ljava/lang/String;
    .end local v40    # "i":Landroid/content/Intent;
    :cond_e
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->this$1:Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback;

    iget-object v9, v9, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback;->this$0:Lcom/google/android/music/utils/VoiceActionHelper;

    # getter for: Lcom/google/android/music/utils/VoiceActionHelper;->mContext:Landroid/content/Context;
    invoke-static {v9}, Lcom/google/android/music/utils/VoiceActionHelper;->access$300(Lcom/google/android/music/utils/VoiceActionHelper;)Landroid/content/Context;

    move-result-object v9

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    move-object/from16 v2, v39

    invoke-static {v9, v0, v1, v2}, Lcom/google/android/music/ui/SubGenresExploreActivity;->buildStartIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v40

    .restart local v40    # "i":Landroid/content/Intent;
    goto :goto_3

    .line 241
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_4
        :pswitch_5
        :pswitch_1
        :pswitch_3
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public taskCompleted()V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 468
    iget-boolean v1, p0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->mResultFound:Z

    if-eqz v1, :cond_0

    .line 469
    iget-object v1, p0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->this$1:Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback;

    iget-object v1, v1, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback;->mCallback:Lcom/google/android/music/utils/VoiceActionHelper$SearchCallback;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->mSonglist:Lcom/google/android/music/medialist/SongList;

    iget-object v4, p0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->mMixDescriptor:Lcom/google/android/music/mix/MixDescriptor;

    invoke-interface {v1, v2, v3, v4}, Lcom/google/android/music/utils/VoiceActionHelper$SearchCallback;->onSearchComplete(ILcom/google/android/music/medialist/SongList;Lcom/google/android/music/mix/MixDescriptor;)V

    .line 482
    :goto_0
    return-void

    .line 473
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->this$1:Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback;

    iget v1, v1, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback;->currentCategoryIndex:I

    add-int/lit8 v0, v1, 0x1

    .line 474
    .local v0, "nextIndex":I
    iget-object v1, p0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->this$1:Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback;

    iget-object v1, v1, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback;->this$0:Lcom/google/android/music/utils/VoiceActionHelper;

    # getter for: Lcom/google/android/music/utils/VoiceActionHelper;->mFallbackCategories:Ljava/util/List;
    invoke-static {v1}, Lcom/google/android/music/utils/VoiceActionHelper;->access$200(Lcom/google/android/music/utils/VoiceActionHelper;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 476
    iget-object v1, p0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->this$1:Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback;

    iget-object v1, v1, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback;->mCallback:Lcom/google/android/music/utils/VoiceActionHelper$SearchCallback;

    const/4 v2, 0x0

    invoke-interface {v1, v2, v3, v3}, Lcom/google/android/music/utils/VoiceActionHelper$SearchCallback;->onSearchComplete(ILcom/google/android/music/medialist/SongList;Lcom/google/android/music/mix/MixDescriptor;)V

    goto :goto_0

    .line 479
    :cond_1
    iget-object v1, p0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->this$1:Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback;

    iget-object v1, v1, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback;->this$0:Lcom/google/android/music/utils/VoiceActionHelper;

    iget-object v2, p0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->this$1:Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback;

    iget-object v2, v2, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback;->query:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->this$1:Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback;

    iget-object v3, v3, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback;->mCallback:Lcom/google/android/music/utils/VoiceActionHelper$SearchCallback;

    iget-object v4, p0, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback$1;->this$1:Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback;

    iget-boolean v4, v4, Lcom/google/android/music/utils/VoiceActionHelper$QueryCallback;->eyesFree:Z

    # invokes: Lcom/google/android/music/utils/VoiceActionHelper;->lookupVoiceQuery(Ljava/lang/String;ILcom/google/android/music/utils/VoiceActionHelper$SearchCallback;Z)V
    invoke-static {v1, v2, v0, v3, v4}, Lcom/google/android/music/utils/VoiceActionHelper;->access$600(Lcom/google/android/music/utils/VoiceActionHelper;Ljava/lang/String;ILcom/google/android/music/utils/VoiceActionHelper$SearchCallback;Z)V

    goto :goto_0
.end method
