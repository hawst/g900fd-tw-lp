.class public Lcom/google/android/music/cloudclient/ListenNowAlbumJson;
.super Lcom/google/api/client/json/GenericJson;
.source "ListenNowAlbumJson.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/cloudclient/ListenNowAlbumJson$AlbumIdJson;
    }
.end annotation


# instance fields
.field public mArtistMetajamId:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "artist_metajam_id"
    .end annotation
.end field

.field public mArtistProfileImage:Lcom/google/android/music/cloudclient/ImageRefJson;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "artist_profile_image"
    .end annotation
.end field

.field public mDescription:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "description"
    .end annotation
.end field

.field public mId:Lcom/google/android/music/cloudclient/ListenNowAlbumJson$AlbumIdJson;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "id"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    .line 22
    return-void
.end method
