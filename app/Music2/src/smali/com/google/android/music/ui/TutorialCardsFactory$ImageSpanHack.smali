.class public Lcom/google/android/music/ui/TutorialCardsFactory$ImageSpanHack;
.super Landroid/text/style/ImageSpan;
.source "TutorialCardsFactory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/TutorialCardsFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ImageSpanHack"
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;II)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "resourceId"    # I
    .param p3, "verticalAlignment"    # I

    .prologue
    .line 748
    invoke-direct {p0, p1, p2, p3}, Landroid/text/style/ImageSpan;-><init>(Landroid/content/Context;II)V

    .line 749
    return-void
.end method


# virtual methods
.method public getSize(Landroid/graphics/Paint;Ljava/lang/CharSequence;IILandroid/graphics/Paint$FontMetricsInt;)I
    .locals 5
    .param p1, "paint"    # Landroid/graphics/Paint;
    .param p2, "text"    # Ljava/lang/CharSequence;
    .param p3, "start"    # I
    .param p4, "end"    # I
    .param p5, "fm"    # Landroid/graphics/Paint$FontMetricsInt;

    .prologue
    .line 754
    const/4 v0, 0x0

    .local v0, "ascent":I
    const/4 v2, 0x0

    .local v2, "descent":I
    const/4 v4, 0x0

    .local v4, "top":I
    const/4 v1, 0x0

    .line 755
    .local v1, "bottom":I
    if-eqz p5, :cond_0

    .line 756
    iget v0, p5, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    .line 757
    iget v2, p5, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    .line 758
    iget v4, p5, Landroid/graphics/Paint$FontMetricsInt;->top:I

    .line 759
    iget v1, p5, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    .line 762
    :cond_0
    invoke-super/range {p0 .. p5}, Landroid/text/style/ImageSpan;->getSize(Landroid/graphics/Paint;Ljava/lang/CharSequence;IILandroid/graphics/Paint$FontMetricsInt;)I

    move-result v3

    .line 764
    .local v3, "result":I
    if-eqz p5, :cond_1

    .line 765
    iput v0, p5, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    .line 766
    iput v2, p5, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    .line 767
    iput v4, p5, Landroid/graphics/Paint$FontMetricsInt;->top:I

    .line 768
    iput v1, p5, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    .line 771
    :cond_1
    return v3
.end method
