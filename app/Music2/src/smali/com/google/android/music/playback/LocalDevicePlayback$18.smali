.class Lcom/google/android/music/playback/LocalDevicePlayback$18;
.super Landroid/content/BroadcastReceiver;
.source "LocalDevicePlayback.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/playback/LocalDevicePlayback;->registerExternalStorageListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/playback/LocalDevicePlayback;


# direct methods
.method constructor <init>(Lcom/google/android/music/playback/LocalDevicePlayback;)V
    .locals 0

    .prologue
    .line 1822
    iput-object p1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$18;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 1825
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 1826
    .local v0, "action":Ljava/lang/String;
    const-string v1, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1827
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$18;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    const/4 v2, 0x1

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->saveQueue(Z)V
    invoke-static {v1, v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$5900(Lcom/google/android/music/playback/LocalDevicePlayback;Z)V

    .line 1828
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$18;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    const/4 v2, 0x0

    # setter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mQueueIsSaveable:Z
    invoke-static {v1, v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$6002(Lcom/google/android/music/playback/LocalDevicePlayback;Z)Z

    .line 1831
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$18;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->closeExternalStorageFiles(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$6100(Lcom/google/android/music/playback/LocalDevicePlayback;Ljava/lang/String;)V

    .line 1842
    :cond_0
    :goto_0
    return-void

    .line 1832
    :cond_1
    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1833
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$18;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # operator++ for: Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaMountedCount:I
    invoke-static {v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$6208(Lcom/google/android/music/playback/LocalDevicePlayback;)I

    .line 1834
    sget-object v1, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    new-instance v2, Lcom/google/android/music/playback/LocalDevicePlayback$18$1;

    invoke-direct {v2, p0}, Lcom/google/android/music/playback/LocalDevicePlayback$18$1;-><init>(Lcom/google/android/music/playback/LocalDevicePlayback$18;)V

    invoke-static {v1, v2}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsync(Lcom/google/android/music/utils/LoggableHandler;Ljava/lang/Runnable;)V

    goto :goto_0
.end method
