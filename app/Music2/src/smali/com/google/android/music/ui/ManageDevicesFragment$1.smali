.class Lcom/google/android/music/ui/ManageDevicesFragment$1;
.super Ljava/lang/Object;
.source "ManageDevicesFragment.java"

# interfaces
.implements Lcom/google/android/music/utils/async/AsyncRunner;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/ManageDevicesFragment;->getUserDevices()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field mResponse:Lcom/google/android/music/cloudclient/UserDevicesListResponseJson;

.field final synthetic this$0:Lcom/google/android/music/ui/ManageDevicesFragment;

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/ManageDevicesFragment;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 88
    iput-object p1, p0, Lcom/google/android/music/ui/ManageDevicesFragment$1;->this$0:Lcom/google/android/music/ui/ManageDevicesFragment;

    iput-object p2, p0, Lcom/google/android/music/ui/ManageDevicesFragment$1;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/ui/ManageDevicesFragment$1;->mResponse:Lcom/google/android/music/cloudclient/UserDevicesListResponseJson;

    return-void
.end method


# virtual methods
.method public backgroundTask()V
    .locals 4

    .prologue
    .line 95
    :try_start_0
    new-instance v0, Lcom/google/android/music/cloudclient/MusicCloudImpl;

    iget-object v2, p0, Lcom/google/android/music/ui/ManageDevicesFragment$1;->val$context:Landroid/content/Context;

    invoke-direct {v0, v2}, Lcom/google/android/music/cloudclient/MusicCloudImpl;-><init>(Landroid/content/Context;)V

    .line 96
    .local v0, "client":Lcom/google/android/music/cloudclient/MusicCloud;
    invoke-interface {v0}, Lcom/google/android/music/cloudclient/MusicCloud;->getUserDevicesResponseJson()Lcom/google/android/music/cloudclient/UserDevicesListResponseJson;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/ui/ManageDevicesFragment$1;->mResponse:Lcom/google/android/music/cloudclient/UserDevicesListResponseJson;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 102
    .end local v0    # "client":Lcom/google/android/music/cloudclient/MusicCloud;
    :goto_0
    return-void

    .line 97
    :catch_0
    move-exception v1

    .line 98
    .local v1, "e":Ljava/lang/InterruptedException;
    const-string v2, "MusicDevices"

    invoke-virtual {v1}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 99
    .end local v1    # "e":Ljava/lang/InterruptedException;
    :catch_1
    move-exception v1

    .line 100
    .local v1, "e":Ljava/io/IOException;
    const-string v2, "MusicDevices"

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public taskCompleted()V
    .locals 4

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/music/ui/ManageDevicesFragment$1;->this$0:Lcom/google/android/music/ui/ManageDevicesFragment;

    invoke-virtual {v0}, Lcom/google/android/music/ui/ManageDevicesFragment;->isRemoving()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/ui/ManageDevicesFragment$1;->this$0:Lcom/google/android/music/ui/ManageDevicesFragment;

    # getter for: Lcom/google/android/music/ui/ManageDevicesFragment;->mDestroyed:Z
    invoke-static {v0}, Lcom/google/android/music/ui/ManageDevicesFragment;->access$000(Lcom/google/android/music/ui/ManageDevicesFragment;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 125
    :cond_0
    :goto_0
    return-void

    .line 110
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/ui/ManageDevicesFragment$1;->this$0:Lcom/google/android/music/ui/ManageDevicesFragment;

    # getter for: Lcom/google/android/music/ui/ManageDevicesFragment;->mAdapter:Lcom/google/android/music/ui/ManageDevicesFragment$DevicesListAdapter;
    invoke-static {v0}, Lcom/google/android/music/ui/ManageDevicesFragment;->access$100(Lcom/google/android/music/ui/ManageDevicesFragment;)Lcom/google/android/music/ui/ManageDevicesFragment$DevicesListAdapter;

    move-result-object v0

    if-nez v0, :cond_2

    .line 111
    iget-object v0, p0, Lcom/google/android/music/ui/ManageDevicesFragment$1;->this$0:Lcom/google/android/music/ui/ManageDevicesFragment;

    new-instance v1, Lcom/google/android/music/ui/ManageDevicesFragment$DevicesListAdapter;

    iget-object v2, p0, Lcom/google/android/music/ui/ManageDevicesFragment$1;->this$0:Lcom/google/android/music/ui/ManageDevicesFragment;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/google/android/music/ui/ManageDevicesFragment$DevicesListAdapter;-><init>(Lcom/google/android/music/ui/ManageDevicesFragment;Lcom/google/android/music/ui/ManageDevicesFragment$1;)V

    # setter for: Lcom/google/android/music/ui/ManageDevicesFragment;->mAdapter:Lcom/google/android/music/ui/ManageDevicesFragment$DevicesListAdapter;
    invoke-static {v0, v1}, Lcom/google/android/music/ui/ManageDevicesFragment;->access$102(Lcom/google/android/music/ui/ManageDevicesFragment;Lcom/google/android/music/ui/ManageDevicesFragment$DevicesListAdapter;)Lcom/google/android/music/ui/ManageDevicesFragment$DevicesListAdapter;

    .line 115
    :cond_2
    iget-object v0, p0, Lcom/google/android/music/ui/ManageDevicesFragment$1;->this$0:Lcom/google/android/music/ui/ManageDevicesFragment;

    iget-object v1, p0, Lcom/google/android/music/ui/ManageDevicesFragment$1;->this$0:Lcom/google/android/music/ui/ManageDevicesFragment;

    # getter for: Lcom/google/android/music/ui/ManageDevicesFragment;->mAdapter:Lcom/google/android/music/ui/ManageDevicesFragment$DevicesListAdapter;
    invoke-static {v1}, Lcom/google/android/music/ui/ManageDevicesFragment;->access$100(Lcom/google/android/music/ui/ManageDevicesFragment;)Lcom/google/android/music/ui/ManageDevicesFragment$DevicesListAdapter;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/ManageDevicesFragment;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 117
    iget-object v0, p0, Lcom/google/android/music/ui/ManageDevicesFragment$1;->mResponse:Lcom/google/android/music/cloudclient/UserDevicesListResponseJson;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/music/ui/ManageDevicesFragment$1;->mResponse:Lcom/google/android/music/cloudclient/UserDevicesListResponseJson;

    iget-object v0, v0, Lcom/google/android/music/cloudclient/UserDevicesListResponseJson;->mUserDevicesData:Lcom/google/android/music/cloudclient/UserDevicesListResponseJson$UserDevicesListJson;

    if-eqz v0, :cond_3

    .line 118
    iget-object v0, p0, Lcom/google/android/music/ui/ManageDevicesFragment$1;->this$0:Lcom/google/android/music/ui/ManageDevicesFragment;

    iget-object v1, p0, Lcom/google/android/music/ui/ManageDevicesFragment$1;->mResponse:Lcom/google/android/music/cloudclient/UserDevicesListResponseJson;

    iget-object v1, v1, Lcom/google/android/music/cloudclient/UserDevicesListResponseJson;->mUserDevicesData:Lcom/google/android/music/cloudclient/UserDevicesListResponseJson$UserDevicesListJson;

    iget-object v1, v1, Lcom/google/android/music/cloudclient/UserDevicesListResponseJson$UserDevicesListJson;->mDevicesList:Ljava/util/List;

    # setter for: Lcom/google/android/music/ui/ManageDevicesFragment;->mUserDevices:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/android/music/ui/ManageDevicesFragment;->access$302(Lcom/google/android/music/ui/ManageDevicesFragment;Ljava/util/List;)Ljava/util/List;

    .line 123
    :goto_1
    iget-object v0, p0, Lcom/google/android/music/ui/ManageDevicesFragment$1;->this$0:Lcom/google/android/music/ui/ManageDevicesFragment;

    # invokes: Lcom/google/android/music/ui/ManageDevicesFragment;->groupUserDevices()V
    invoke-static {v0}, Lcom/google/android/music/ui/ManageDevicesFragment;->access$400(Lcom/google/android/music/ui/ManageDevicesFragment;)V

    goto :goto_0

    .line 120
    :cond_3
    iget-object v0, p0, Lcom/google/android/music/ui/ManageDevicesFragment$1;->this$0:Lcom/google/android/music/ui/ManageDevicesFragment;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/android/music/ui/ManageDevicesFragment;->mUserDevices:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/android/music/ui/ManageDevicesFragment;->access$302(Lcom/google/android/music/ui/ManageDevicesFragment;Ljava/util/List;)Ljava/util/List;

    goto :goto_1
.end method
