.class Lcom/google/android/music/download/cache/StorageMigrationWorker$FileCopyRunnable;
.super Ljava/lang/Object;
.source "StorageMigrationWorker.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/download/cache/StorageMigrationWorker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FileCopyRunnable"
.end annotation


# instance fields
.field mAbort:Z

.field final synthetic this$0:Lcom/google/android/music/download/cache/StorageMigrationWorker;


# direct methods
.method private constructor <init>(Lcom/google/android/music/download/cache/StorageMigrationWorker;)V
    .locals 1

    .prologue
    .line 359
    iput-object p1, p0, Lcom/google/android/music/download/cache/StorageMigrationWorker$FileCopyRunnable;->this$0:Lcom/google/android/music/download/cache/StorageMigrationWorker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 360
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/download/cache/StorageMigrationWorker$FileCopyRunnable;->mAbort:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/music/download/cache/StorageMigrationWorker;Lcom/google/android/music/download/cache/StorageMigrationWorker$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/music/download/cache/StorageMigrationWorker;
    .param p2, "x1"    # Lcom/google/android/music/download/cache/StorageMigrationWorker$1;

    .prologue
    .line 359
    invoke-direct {p0, p1}, Lcom/google/android/music/download/cache/StorageMigrationWorker$FileCopyRunnable;-><init>(Lcom/google/android/music/download/cache/StorageMigrationWorker;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 364
    iget-object v2, p0, Lcom/google/android/music/download/cache/StorageMigrationWorker$FileCopyRunnable;->this$0:Lcom/google/android/music/download/cache/StorageMigrationWorker;

    # getter for: Lcom/google/android/music/download/cache/StorageMigrationWorker;->mFileIdsToCopy:Ljava/util/LinkedList;
    invoke-static {v2}, Lcom/google/android/music/download/cache/StorageMigrationWorker;->access$100(Lcom/google/android/music/download/cache/StorageMigrationWorker;)Ljava/util/LinkedList;

    move-result-object v3

    monitor-enter v3

    .line 365
    :try_start_0
    iget-boolean v2, p0, Lcom/google/android/music/download/cache/StorageMigrationWorker$FileCopyRunnable;->mAbort:Z

    if-eqz v2, :cond_0

    monitor-exit v3

    .line 377
    :goto_0
    return-void

    .line 366
    :cond_0
    iget-object v2, p0, Lcom/google/android/music/download/cache/StorageMigrationWorker$FileCopyRunnable;->this$0:Lcom/google/android/music/download/cache/StorageMigrationWorker;

    # getter for: Lcom/google/android/music/download/cache/StorageMigrationWorker;->mFileIdsToCopy:Ljava/util/LinkedList;
    invoke-static {v2}, Lcom/google/android/music/download/cache/StorageMigrationWorker;->access$100(Lcom/google/android/music/download/cache/StorageMigrationWorker;)Ljava/util/LinkedList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/download/cache/StorageMigrationWorker$FileCopyDetails;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 369
    .local v1, "next":Lcom/google/android/music/download/cache/StorageMigrationWorker$FileCopyDetails;
    :try_start_1
    iget-object v2, p0, Lcom/google/android/music/download/cache/StorageMigrationWorker$FileCopyRunnable;->this$0:Lcom/google/android/music/download/cache/StorageMigrationWorker;

    # invokes: Lcom/google/android/music/download/cache/StorageMigrationWorker;->doCopy(Lcom/google/android/music/download/cache/StorageMigrationWorker$FileCopyDetails;)Z
    invoke-static {v2, v1}, Lcom/google/android/music/download/cache/StorageMigrationWorker;->access$700(Lcom/google/android/music/download/cache/StorageMigrationWorker;Lcom/google/android/music/download/cache/StorageMigrationWorker$FileCopyDetails;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 373
    :goto_1
    :try_start_2
    iget-object v2, p0, Lcom/google/android/music/download/cache/StorageMigrationWorker$FileCopyRunnable;->this$0:Lcom/google/android/music/download/cache/StorageMigrationWorker;

    # invokes: Lcom/google/android/music/download/cache/StorageMigrationWorker;->updateRemainingString()V
    invoke-static {v2}, Lcom/google/android/music/download/cache/StorageMigrationWorker;->access$800(Lcom/google/android/music/download/cache/StorageMigrationWorker;)V

    .line 374
    iget-object v2, p0, Lcom/google/android/music/download/cache/StorageMigrationWorker$FileCopyRunnable;->this$0:Lcom/google/android/music/download/cache/StorageMigrationWorker;

    # getter for: Lcom/google/android/music/download/cache/StorageMigrationWorker;->mNotificationManager:Landroid/app/NotificationManager;
    invoke-static {v2}, Lcom/google/android/music/download/cache/StorageMigrationWorker;->access$200(Lcom/google/android/music/download/cache/StorageMigrationWorker;)Landroid/app/NotificationManager;

    move-result-object v2

    const/16 v4, 0x425

    iget-object v5, p0, Lcom/google/android/music/download/cache/StorageMigrationWorker$FileCopyRunnable;->this$0:Lcom/google/android/music/download/cache/StorageMigrationWorker;

    # getter for: Lcom/google/android/music/download/cache/StorageMigrationWorker;->mNotificationBuilder:Landroid/support/v4/app/NotificationCompat$Builder;
    invoke-static {v5}, Lcom/google/android/music/download/cache/StorageMigrationWorker;->access$900(Lcom/google/android/music/download/cache/StorageMigrationWorker;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 375
    iget-object v2, p0, Lcom/google/android/music/download/cache/StorageMigrationWorker$FileCopyRunnable;->this$0:Lcom/google/android/music/download/cache/StorageMigrationWorker;

    # invokes: Lcom/google/android/music/download/cache/StorageMigrationWorker;->moveToNextFileOrEnd()V
    invoke-static {v2}, Lcom/google/android/music/download/cache/StorageMigrationWorker;->access$1000(Lcom/google/android/music/download/cache/StorageMigrationWorker;)V

    .line 376
    monitor-exit v3

    goto :goto_0

    .end local v1    # "next":Lcom/google/android/music/download/cache/StorageMigrationWorker$FileCopyDetails;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2

    .line 370
    .restart local v1    # "next":Lcom/google/android/music/download/cache/StorageMigrationWorker$FileCopyDetails;
    :catch_0
    move-exception v0

    .line 371
    .local v0, "e":Ljava/lang/Exception;
    :try_start_3
    const-string v2, "StorageMigrationWkr"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unknown error while copying file "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4, v0}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method
