.class Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Shuffle;
.super Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$UIThreadMenuEntry;
.source "DocumentMenuHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Shuffle"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;Landroid/content/res/Resources;)V
    .locals 2
    .param p2, "res"    # Landroid/content/res/Resources;

    .prologue
    .line 931
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Shuffle;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    .line 932
    const v0, 0x7f0e0031

    const v1, 0x7f0b0056

    invoke-direct {p0, p2, v0, v1}, Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$UIThreadMenuEntry;-><init>(Landroid/content/res/Resources;II)V

    .line 933
    return-void
.end method


# virtual methods
.method public onActionSelected()V
    .locals 2

    .prologue
    .line 937
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Shuffle;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    iget-object v1, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$Shuffle;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v1, v1, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mContext:Landroid/app/Activity;

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/cardlib/model/Document;->getSongList(Landroid/content/Context;)Lcom/google/android/music/medialist/SongList;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/music/utils/MusicUtils;->shuffleSongs(Lcom/google/android/music/medialist/SongList;)V

    .line 939
    return-void
.end method
