.class public abstract Lcom/google/android/music/leanback/LeanbackExploreGroupsActivity;
.super Lcom/google/android/music/leanback/LeanbackBrowseActivity;
.source "LeanbackExploreGroupsActivity.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->LEANBACK:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-virtual {v0}, Lcom/google/android/music/utils/DebugUtils$MusicTag;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/leanback/LeanbackExploreGroupsActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackBrowseActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/google/android/music/leanback/LeanbackExploreGroupsActivity;->TAG:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method protected getBrowseTitle()Ljava/lang/String;
    .locals 2

    .prologue
    .line 86
    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackExploreGroupsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "title"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected abstract getGroupItemsUri(J)Landroid/net/Uri;
.end method

.method protected abstract getGroupsUri()Landroid/net/Uri;
.end method

.method protected getListRowsAdapter()Landroid/support/v17/leanback/widget/ObjectAdapter;
    .locals 6

    .prologue
    .line 33
    new-instance v2, Lcom/google/android/music/leanback/LeanbackExploreGroupsActivity$1;

    new-instance v3, Landroid/support/v17/leanback/widget/ListRowPresenter;

    invoke-direct {v3}, Landroid/support/v17/leanback/widget/ListRowPresenter;-><init>()V

    invoke-direct {v2, p0, v3}, Lcom/google/android/music/leanback/LeanbackExploreGroupsActivity$1;-><init>(Lcom/google/android/music/leanback/LeanbackExploreGroupsActivity;Landroid/support/v17/leanback/widget/Presenter;)V

    .line 59
    .local v2, "topChartsGroupsAdapter":Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 60
    .local v0, "handler":Landroid/os/Handler;
    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackExploreGroupsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "selected_row"

    const/4 v5, -0x1

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 61
    .local v1, "rowToSelect":I
    if-lez v1, :cond_0

    .line 62
    new-instance v3, Lcom/google/android/music/leanback/LeanbackExploreGroupsActivity$2;

    invoke-direct {v3, p0, v2, v0, v1}, Lcom/google/android/music/leanback/LeanbackExploreGroupsActivity$2;-><init>(Lcom/google/android/music/leanback/LeanbackExploreGroupsActivity;Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;Landroid/os/Handler;I)V

    invoke-virtual {v2, v3}, Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;->registerObserver(Landroid/support/v17/leanback/widget/ObjectAdapter$DataObserver;)V

    .line 78
    :cond_0
    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackExploreGroupsActivity;->getGroupsUri()Landroid/net/Uri;

    move-result-object v4

    sget-object v5, Lcom/google/android/music/ui/ExploreClusterListFragment;->GROUPS_COLUMNS:[Ljava/lang/String;

    invoke-virtual {p0, v3, v2, v4, v5}, Lcom/google/android/music/leanback/LeanbackExploreGroupsActivity;->loadUri(ILandroid/support/v17/leanback/widget/CursorObjectAdapter;Landroid/net/Uri;[Ljava/lang/String;)V

    .line 81
    return-object v2
.end method
