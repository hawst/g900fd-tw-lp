.class Lcom/google/android/music/ui/ArtistAlbumsGridFragment$1;
.super Ljava/lang/Object;
.source "ArtistAlbumsGridFragment.java"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/ArtistAlbumsGridFragment;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;)V
    .locals 0

    .prologue
    .line 123
    iput-object p1, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$1;->this$0:Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 7
    .param p1, "view"    # Landroid/widget/AbsListView;
    .param p2, "firstVisibleItem"    # I
    .param p3, "visibleItemCount"    # I
    .param p4, "totalItemCount"    # I

    .prologue
    const/16 v6, 0xff

    .line 128
    iget-object v4, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$1;->this$0:Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    # getter for: Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mArtistHeader:Lcom/google/android/music/ui/ArtistHeaderView;
    invoke-static {v4}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->access$100(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;)Lcom/google/android/music/ui/ArtistHeaderView;

    move-result-object v4

    invoke-virtual {v4, p1, p2, p3, p4}, Lcom/google/android/music/ui/ArtistHeaderView;->onScroll(Landroid/widget/AbsListView;III)V

    .line 130
    const/4 v4, 0x0

    invoke-virtual {p1, v4}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 131
    .local v3, "v":Landroid/view/View;
    iget-object v4, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$1;->this$0:Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    # getter for: Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mArtistHeader:Lcom/google/android/music/ui/ArtistHeaderView;
    invoke-static {v4}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->access$100(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;)Lcom/google/android/music/ui/ArtistHeaderView;

    move-result-object v4

    if-ne v3, v4, :cond_2

    iget-object v4, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$1;->this$0:Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    invoke-virtual {v4}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/music/preferences/MusicPreferences;->isDownloadedOnlyMode()Z

    move-result v4

    if-nez v4, :cond_2

    .line 132
    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v4

    neg-int v4, v4

    int-to-float v2, v4

    .line 133
    .local v2, "offset":F
    iget-object v4, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$1;->this$0:Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    # getter for: Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mArtistHeader:Lcom/google/android/music/ui/ArtistHeaderView;
    invoke-static {v4}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->access$100(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;)Lcom/google/android/music/ui/ArtistHeaderView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/music/ui/ArtistHeaderView;->getHeight()I

    move-result v1

    .line 134
    .local v1, "height":I
    div-int/lit8 v4, v1, 0x2

    int-to-float v4, v4

    sub-float/2addr v2, v4

    .line 135
    div-int/lit8 v4, v1, 0x2

    int-to-float v4, v4

    div-float v4, v2, v4

    const/high16 v5, 0x437f0000    # 255.0f

    mul-float/2addr v4, v5

    float-to-int v0, v4

    .line 136
    .local v0, "alpha":I
    if-le v0, v6, :cond_0

    const/16 v0, 0xff

    .line 137
    :cond_0
    if-gez v0, :cond_1

    const/4 v0, 0x0

    .line 138
    :cond_1
    iget-object v4, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$1;->this$0:Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    invoke-virtual {v4}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->getActionBarController()Lcom/google/android/music/ui/ActionBarController;

    move-result-object v4

    invoke-interface {v4, v0}, Lcom/google/android/music/ui/ActionBarController;->setActionBarAlpha(I)V

    .line 143
    .end local v0    # "alpha":I
    .end local v1    # "height":I
    .end local v2    # "offset":F
    :goto_0
    return-void

    .line 141
    :cond_2
    iget-object v4, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$1;->this$0:Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    invoke-virtual {v4}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->getActionBarController()Lcom/google/android/music/ui/ActionBarController;

    move-result-object v4

    invoke-interface {v4, v6}, Lcom/google/android/music/ui/ActionBarController;->setActionBarAlpha(I)V

    goto :goto_0
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0
    .param p1, "view"    # Landroid/widget/AbsListView;
    .param p2, "scrollState"    # I

    .prologue
    .line 145
    return-void
.end method
