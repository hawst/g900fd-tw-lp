.class public Lcom/google/android/music/medialist/SharedAlbumSongList;
.super Lcom/google/android/music/medialist/SharedSongList;
.source "SharedAlbumSongList.java"


# static fields
.field private static final LOGV:Z

.field private static final LOG_PROJ:Z

.field private static final TAG:Ljava/lang/String; = "SharedAlbumSongList"


# instance fields
.field private mAlbumArtUrl:Ljava/lang/String;

.field private mSongs:Lcom/google/android/music/medialist/SongDataList;

.field private mStoreUrl:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->MEDIA_LIST:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/medialist/SharedAlbumSongList;->LOGV:Z

    .line 20
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->CONTENT_PROVIDER:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/medialist/SharedAlbumSongList;->LOG_PROJ:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/music/medialist/SongDataList;)V
    .locals 1
    .param p1, "albumArtUrl"    # Ljava/lang/String;
    .param p2, "storeUrl"    # Ljava/lang/String;
    .param p3, "songs"    # Lcom/google/android/music/medialist/SongDataList;

    .prologue
    .line 27
    sget-object v0, Lcom/google/android/music/download/ContentIdentifier$Domain;->SHARED:Lcom/google/android/music/download/ContentIdentifier$Domain;

    invoke-direct {p0, v0}, Lcom/google/android/music/medialist/SharedSongList;-><init>(Lcom/google/android/music/download/ContentIdentifier$Domain;)V

    .line 28
    iput-object p1, p0, Lcom/google/android/music/medialist/SharedAlbumSongList;->mAlbumArtUrl:Ljava/lang/String;

    .line 29
    iput-object p2, p0, Lcom/google/android/music/medialist/SharedAlbumSongList;->mStoreUrl:Ljava/lang/String;

    .line 30
    iput-object p3, p0, Lcom/google/android/music/medialist/SharedAlbumSongList;->mSongs:Lcom/google/android/music/medialist/SongDataList;

    .line 31
    return-void
.end method


# virtual methods
.method public createSyncCursor(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "proj"    # [Ljava/lang/String;
    .param p3, "filter"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 72
    iget-object v4, p0, Lcom/google/android/music/medialist/SharedAlbumSongList;->mSongs:Lcom/google/android/music/medialist/SongDataList;

    if-nez v4, :cond_1

    .line 100
    :cond_0
    return-object v0

    .line 76
    :cond_1
    if-eqz p2, :cond_0

    .line 80
    array-length v4, p2

    new-array v1, v4, [Ljava/lang/String;

    .line 81
    .local v1, "cursorCols":[Ljava/lang/String;
    sget-boolean v4, Lcom/google/android/music/medialist/SharedAlbumSongList;->LOG_PROJ:Z

    if-eqz v4, :cond_2

    .line 82
    invoke-virtual {p0, p2}, Lcom/google/android/music/medialist/SharedAlbumSongList;->logProjection([Ljava/lang/String;)V

    .line 85
    :cond_2
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v4, p2

    if-ge v2, v4, :cond_3

    .line 86
    aget-object v4, p2, v2

    aput-object v4, v1, v2

    .line 85
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 89
    :cond_3
    new-instance v0, Landroid/database/MatrixCursor;

    invoke-direct {v0, v1}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 91
    .local v0, "c":Landroid/database/MatrixCursor;
    const/4 v2, 0x0

    :goto_1
    iget-object v4, p0, Lcom/google/android/music/medialist/SharedAlbumSongList;->mSongs:Lcom/google/android/music/medialist/SongDataList;

    iget-object v4, v4, Lcom/google/android/music/medialist/SongDataList;->mList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v2, v4, :cond_0

    .line 92
    int-to-long v6, v2

    iget-object v4, p0, Lcom/google/android/music/medialist/SharedAlbumSongList;->mSongs:Lcom/google/android/music/medialist/SongDataList;

    iget-object v4, v4, Lcom/google/android/music/medialist/SongDataList;->mList:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/music/medialist/SongData;

    invoke-virtual {p0, v6, v7, v1, v4}, Lcom/google/android/music/medialist/SharedAlbumSongList;->createRow(J[Ljava/lang/String;Lcom/google/android/music/medialist/SongData;)Ljava/util/ArrayList;

    move-result-object v3

    .line 93
    .local v3, "row":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    if-eqz v3, :cond_4

    .line 94
    invoke-virtual {v0, v3}, Landroid/database/MatrixCursor;->addRow(Ljava/lang/Iterable;)V

    .line 91
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 96
    :cond_4
    const-string v4, "SharedAlbumSongList"

    const-string v5, "Failed to create row"

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method public getAlbumArtUrl(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/android/music/medialist/SharedAlbumSongList;->mAlbumArtUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getArgs()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 110
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/music/medialist/SharedAlbumSongList;->mAlbumArtUrl:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/music/medialist/SharedAlbumSongList;->mStoreUrl:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/music/medialist/SharedAlbumSongList;->mSongs:Lcom/google/android/music/medialist/SongDataList;

    invoke-static {v2}, Lcom/google/android/music/medialist/SongDataList;->toJson(Lcom/google/android/music/medialist/SongDataList;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    return-object v0
.end method

.method public getName(Landroid/content/Context;)Ljava/lang/String;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 123
    iget-object v0, p0, Lcom/google/android/music/medialist/SharedAlbumSongList;->mSongs:Lcom/google/android/music/medialist/SongDataList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/medialist/SharedAlbumSongList;->mSongs:Lcom/google/android/music/medialist/SongDataList;

    iget-object v0, v0, Lcom/google/android/music/medialist/SongDataList;->mList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/medialist/SharedAlbumSongList;->mSongs:Lcom/google/android/music/medialist/SongDataList;

    iget-object v0, v0, Lcom/google/android/music/medialist/SongDataList;->mList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 124
    :cond_0
    const/4 v0, 0x0

    .line 126
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/music/medialist/SharedAlbumSongList;->mSongs:Lcom/google/android/music/medialist/SongDataList;

    iget-object v0, v0, Lcom/google/android/music/medialist/SongDataList;->mList:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/medialist/SongData;

    iget-object v0, v0, Lcom/google/android/music/medialist/SongData;->mAlbum:Ljava/lang/String;

    goto :goto_0
.end method

.method public getScreenTitle(Landroid/content/Context;)Ljava/lang/String;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 115
    iget-object v0, p0, Lcom/google/android/music/medialist/SharedAlbumSongList;->mSongs:Lcom/google/android/music/medialist/SongDataList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/medialist/SharedAlbumSongList;->mSongs:Lcom/google/android/music/medialist/SongDataList;

    iget-object v0, v0, Lcom/google/android/music/medialist/SongDataList;->mList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/medialist/SharedAlbumSongList;->mSongs:Lcom/google/android/music/medialist/SongDataList;

    iget-object v0, v0, Lcom/google/android/music/medialist/SongDataList;->mList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 116
    :cond_0
    const/4 v0, 0x0

    .line 118
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/music/medialist/SharedAlbumSongList;->mSongs:Lcom/google/android/music/medialist/SongDataList;

    iget-object v0, v0, Lcom/google/android/music/medialist/SongDataList;->mList:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/medialist/SongData;

    iget-object v0, v0, Lcom/google/android/music/medialist/SongData;->mAlbum:Ljava/lang/String;

    goto :goto_0
.end method

.method public getSecondaryName(Landroid/content/Context;)Ljava/lang/String;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 131
    iget-object v0, p0, Lcom/google/android/music/medialist/SharedAlbumSongList;->mSongs:Lcom/google/android/music/medialist/SongDataList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/medialist/SharedAlbumSongList;->mSongs:Lcom/google/android/music/medialist/SongDataList;

    iget-object v0, v0, Lcom/google/android/music/medialist/SongDataList;->mList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/medialist/SharedAlbumSongList;->mSongs:Lcom/google/android/music/medialist/SongDataList;

    iget-object v0, v0, Lcom/google/android/music/medialist/SongDataList;->mList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 132
    :cond_0
    const/4 v0, 0x0

    .line 134
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/music/medialist/SharedAlbumSongList;->mSongs:Lcom/google/android/music/medialist/SongDataList;

    iget-object v0, v0, Lcom/google/android/music/medialist/SongDataList;->mList:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/medialist/SongData;

    iget-object v0, v0, Lcom/google/android/music/medialist/SongData;->mArtist:Ljava/lang/String;

    goto :goto_0
.end method

.method public getSongCursor(Landroid/content/Context;Lcom/google/android/music/download/ContentIdentifier;[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "songId"    # Lcom/google/android/music/download/ContentIdentifier;
    .param p3, "proj"    # [Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 35
    if-nez p2, :cond_1

    .line 66
    :cond_0
    :goto_0
    return-object v5

    .line 39
    :cond_1
    iget-object v6, p0, Lcom/google/android/music/medialist/SharedAlbumSongList;->mSongs:Lcom/google/android/music/medialist/SongDataList;

    if-eqz v6, :cond_0

    .line 43
    array-length v6, p3

    new-array v1, v6, [Ljava/lang/String;

    .line 45
    .local v1, "cursorCols":[Ljava/lang/String;
    sget-boolean v6, Lcom/google/android/music/medialist/SharedAlbumSongList;->LOG_PROJ:Z

    if-eqz v6, :cond_2

    .line 46
    invoke-virtual {p0, p3}, Lcom/google/android/music/medialist/SharedAlbumSongList;->logProjection([Ljava/lang/String;)V

    .line 49
    :cond_2
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    array-length v6, p3

    if-ge v2, v6, :cond_3

    .line 50
    aget-object v6, p3, v2

    aput-object v6, v1, v2

    .line 49
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 53
    :cond_3
    iget-object v6, p0, Lcom/google/android/music/medialist/SharedAlbumSongList;->mSongs:Lcom/google/android/music/medialist/SongDataList;

    iget-object v6, v6, Lcom/google/android/music/medialist/SongDataList;->mList:Ljava/util/ArrayList;

    invoke-virtual {p2}, Lcom/google/android/music/download/ContentIdentifier;->getId()J

    move-result-wide v8

    long-to-int v7, v8

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/music/medialist/SongData;

    .line 54
    .local v4, "song":Lcom/google/android/music/medialist/SongData;
    if-eqz v4, :cond_0

    .line 58
    invoke-virtual {p2}, Lcom/google/android/music/download/ContentIdentifier;->getId()J

    move-result-wide v6

    invoke-virtual {p0, v6, v7, p3, v4}, Lcom/google/android/music/medialist/SharedAlbumSongList;->createRow(J[Ljava/lang/String;Lcom/google/android/music/medialist/SongData;)Ljava/util/ArrayList;

    move-result-object v3

    .line 59
    .local v3, "row":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    if-eqz v3, :cond_0

    .line 63
    new-instance v0, Landroid/database/MatrixCursor;

    invoke-direct {v0, v1}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 64
    .local v0, "c":Landroid/database/MatrixCursor;
    invoke-virtual {v0, v3}, Landroid/database/MatrixCursor;->addRow(Ljava/lang/Iterable;)V

    .line 66
    invoke-virtual {p0, p1, v0}, Lcom/google/android/music/medialist/SharedAlbumSongList;->createMediaCursor(Landroid/content/Context;Landroid/database/Cursor;)Lcom/google/android/music/medialist/MediaList$MediaCursor;

    move-result-object v5

    goto :goto_0
.end method

.method public getStoreUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/google/android/music/medialist/SharedAlbumSongList;->mStoreUrl:Ljava/lang/String;

    return-object v0
.end method

.method public hasDifferentTrackArtists(Landroid/content/Context;)Z
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 139
    const/4 v2, 0x0

    .line 140
    .local v2, "prev":Ljava/lang/String;
    iget-object v4, p0, Lcom/google/android/music/medialist/SharedAlbumSongList;->mSongs:Lcom/google/android/music/medialist/SongDataList;

    iget-object v4, v4, Lcom/google/android/music/medialist/SongDataList;->mList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/music/medialist/SongData;

    .line 141
    .local v3, "song":Lcom/google/android/music/medialist/SongData;
    iget-object v0, v3, Lcom/google/android/music/medialist/SongData;->mArtist:Ljava/lang/String;

    .line 142
    .local v0, "artist":Ljava/lang/String;
    if-eqz v2, :cond_0

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 143
    const/4 v4, 0x1

    .line 147
    .end local v0    # "artist":Ljava/lang/String;
    .end local v3    # "song":Lcom/google/android/music/medialist/SongData;
    :goto_1
    return v4

    .line 145
    .restart local v0    # "artist":Ljava/lang/String;
    .restart local v3    # "song":Lcom/google/android/music/medialist/SongData;
    :cond_0
    move-object v2, v0

    .line 146
    goto :goto_0

    .line 147
    .end local v0    # "artist":Ljava/lang/String;
    .end local v3    # "song":Lcom/google/android/music/medialist/SongData;
    :cond_1
    const/4 v4, 0x0

    goto :goto_1
.end method
