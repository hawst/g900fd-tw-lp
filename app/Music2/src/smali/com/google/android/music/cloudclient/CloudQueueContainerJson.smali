.class public Lcom/google/android/music/cloudclient/CloudQueueContainerJson;
.super Lcom/google/api/client/json/GenericJson;
.source "CloudQueueContainerJson.java"


# static fields
.field public static final CONTAINER_TYPE_ALBUM:I = 0x9

.field public static final CONTAINER_TYPE_ALL_ACCESS_TOP_SONGS:I = 0x11

.field public static final CONTAINER_TYPE_ALL_SONGS_IN_MY_LIBRARY:I = 0xa

.field public static final CONTAINER_TYPE_ARTIST_ALL_SONGS:I = 0xb

.field public static final CONTAINER_TYPE_ARTIST_SHUFFLE:I = 0xd

.field public static final CONTAINER_TYPE_ARTIST_TOP_SONGS:I = 0xc

.field public static final CONTAINER_TYPE_GENRE_ALL_SONGS:I = 0xf

.field public static final CONTAINER_TYPE_GENRE_TOP_SONGS:I = 0x10

.field public static final CONTAINER_TYPE_PLAYLIST:I = 0x1

.field public static final CONTAINER_TYPE_PLAYLIST_GOOGLE_PLAY_RECOMMENDS:I = 0x7

.field public static final CONTAINER_TYPE_PLAYLIST_GOOGLE_PLUS_SHARED:I = 0x6

.field public static final CONTAINER_TYPE_PLAYLIST_RECENTLY_ADDED:I = 0x4

.field public static final CONTAINER_TYPE_PLAYLIST_SHARED:I = 0x2

.field public static final CONTAINER_TYPE_PLAYLIST_SOUND_SEARCH:I = 0x8

.field public static final CONTAINER_TYPE_PLAYLIST_STORE:I = 0x5

.field public static final CONTAINER_TYPE_PLAYLIST_THUMBS_UP:I = 0x3

.field public static final CONTAINER_TYPE_RADIO:I = 0xe

.field public static final CONTAINER_TYPE_SEARCH_RESULTS:I = 0x12

.field public static final CONTAINER_TYPE_SINGLE_SONG:I = 0x13

.field public static final CONTAINER_TYPE_UNKNOWN_CONTAINER:I


# instance fields
.field public mKey:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "key"
    .end annotation
.end field

.field public mName:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "name"
    .end annotation
.end field

.field public mReference:Lcom/google/android/music/cloudclient/CloudQueueContainerReferenceJson;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "reference"
    .end annotation
.end field

.field public mSevered:Z
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "severed"
    .end annotation
.end field

.field public mType:I
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "type"
    .end annotation
.end field

.field public mVersion:J
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "version"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    return-void
.end method
