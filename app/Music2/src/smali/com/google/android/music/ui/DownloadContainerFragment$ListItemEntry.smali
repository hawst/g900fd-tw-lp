.class Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;
.super Ljava/lang/Object;
.source "DownloadContainerFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/DownloadContainerFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ListItemEntry"
.end annotation


# instance fields
.field final artUrls:Ljava/lang/String;

.field final containerType:Lcom/google/android/music/ui/DownloadContainerFragment$ContainerType;

.field final id:J

.field isRemoving:Z

.field final listType:I

.field final remoteId:Ljava/lang/String;

.field final subtitle:Ljava/lang/String;

.field final title:Ljava/lang/String;


# direct methods
.method constructor <init>(JILjava/lang/String;Ljava/lang/String;Lcom/google/android/music/ui/DownloadContainerFragment$ContainerType;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "id"    # J
    .param p3, "listType"    # I
    .param p4, "title"    # Ljava/lang/String;
    .param p5, "subtitle"    # Ljava/lang/String;
    .param p6, "containerType"    # Lcom/google/android/music/ui/DownloadContainerFragment$ContainerType;
    .param p7, "artUrls"    # Ljava/lang/String;
    .param p8, "remoteId"    # Ljava/lang/String;

    .prologue
    .line 319
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 320
    iput-wide p1, p0, Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;->id:J

    .line 321
    iput p3, p0, Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;->listType:I

    .line 322
    iput-object p4, p0, Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;->title:Ljava/lang/String;

    .line 323
    iput-object p5, p0, Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;->subtitle:Ljava/lang/String;

    .line 324
    iput-object p6, p0, Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;->containerType:Lcom/google/android/music/ui/DownloadContainerFragment$ContainerType;

    .line 325
    iput-object p7, p0, Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;->artUrls:Ljava/lang/String;

    .line 326
    iput-object p8, p0, Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;->remoteId:Ljava/lang/String;

    .line 328
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;->isRemoving:Z

    .line 329
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 340
    if-ne p0, p1, :cond_1

    .line 353
    :cond_0
    :goto_0
    return v1

    .line 341
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    :cond_2
    move v1, v2

    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 343
    check-cast v0, Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;

    .line 345
    .local v0, "that":Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;
    iget-wide v4, p0, Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;->id:J

    iget-wide v6, v0, Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;->id:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_4

    move v1, v2

    goto :goto_0

    .line 346
    :cond_4
    iget v3, p0, Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;->listType:I

    iget v4, v0, Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;->listType:I

    if-eq v3, v4, :cond_5

    move v1, v2

    goto :goto_0

    .line 347
    :cond_5
    iget-object v3, p0, Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;->containerType:Lcom/google/android/music/ui/DownloadContainerFragment$ContainerType;

    iget-object v4, v0, Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;->containerType:Lcom/google/android/music/ui/DownloadContainerFragment$ContainerType;

    if-eq v3, v4, :cond_6

    move v1, v2

    goto :goto_0

    .line 348
    :cond_6
    iget-object v3, p0, Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;->subtitle:Ljava/lang/String;

    if-eqz v3, :cond_8

    iget-object v3, p0, Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;->subtitle:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;->subtitle:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_9

    :cond_7
    move v1, v2

    .line 349
    goto :goto_0

    .line 348
    :cond_8
    iget-object v3, v0, Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;->subtitle:Ljava/lang/String;

    if-nez v3, :cond_7

    .line 351
    :cond_9
    iget-object v3, p0, Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;->title:Ljava/lang/String;

    if-eqz v3, :cond_a

    iget-object v3, p0, Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;->title:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;->title:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :goto_1
    move v1, v2

    goto :goto_0

    :cond_a
    iget-object v3, v0, Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;->title:Ljava/lang/String;

    if-eqz v3, :cond_0

    goto :goto_1
.end method

.method public hashCode()I
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 358
    iget-wide v4, p0, Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;->id:J

    iget-wide v6, p0, Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;->id:J

    const/16 v1, 0x20

    ushr-long/2addr v6, v1

    xor-long/2addr v4, v6

    long-to-int v0, v4

    .line 359
    .local v0, "result":I
    mul-int/lit8 v1, v0, 0x1f

    iget v3, p0, Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;->listType:I

    add-int v0, v1, v3

    .line 360
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;->containerType:Lcom/google/android/music/ui/DownloadContainerFragment$ContainerType;

    invoke-virtual {v3}, Lcom/google/android/music/ui/DownloadContainerFragment$ContainerType;->ordinal()I

    move-result v3

    add-int v0, v1, v3

    .line 361
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;->title:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;->title:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :goto_0
    add-int v0, v3, v1

    .line 362
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;->subtitle:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v2, p0, Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;->subtitle:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_0
    add-int v0, v1, v2

    .line 363
    return v0

    :cond_1
    move v1, v2

    .line 361
    goto :goto_0
.end method

.method setRemoving(Z)V
    .locals 0
    .param p1, "removing"    # Z

    .prologue
    .line 332
    iput-boolean p1, p0, Lcom/google/android/music/ui/DownloadContainerFragment$ListItemEntry;->isRemoving:Z

    .line 333
    return-void
.end method
