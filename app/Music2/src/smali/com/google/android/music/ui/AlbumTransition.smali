.class public Lcom/google/android/music/ui/AlbumTransition;
.super Lcom/google/android/music/ui/SharedElementTransition;
.source "AlbumTransition.java"


# instance fields
.field private mActionBarId:I

.field private mBackgroundContainerId:I

.field private mBaseActivity:Lcom/google/android/music/ui/BaseActivity;

.field private mEndingSharedElementColor:I

.field private mFabId:I

.field private mImageId:I

.field private mMetadataParentViewId:I

.field private mRootSharedElementId:I

.field private mShadowId:I

.field private mSharedElementMaxWidth:I

.field private mSnapshotAlbum:Landroid/widget/ImageView;

.field private mSnapshotAlbumBitmap:Landroid/graphics/Bitmap;

.field private mSpacerId:I

.field private mStartingSharedElementColor:I

.field private mTransitionInfo:Lcom/google/android/music/ui/AlbumTransitionInfo;


# direct methods
.method public constructor <init>(Lcom/google/android/music/ui/TransitionInfo;Lcom/google/android/music/ui/BaseActivity;)V
    .locals 0
    .param p1, "info"    # Lcom/google/android/music/ui/TransitionInfo;
    .param p2, "activity"    # Lcom/google/android/music/ui/BaseActivity;

    .prologue
    .line 61
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/ui/SharedElementTransition;-><init>(Lcom/google/android/music/ui/TransitionInfo;Lcom/google/android/music/ui/BaseActivity;)V

    .line 63
    check-cast p1, Lcom/google/android/music/ui/AlbumTransitionInfo;

    .end local p1    # "info":Lcom/google/android/music/ui/TransitionInfo;
    iput-object p1, p0, Lcom/google/android/music/ui/AlbumTransition;->mTransitionInfo:Lcom/google/android/music/ui/AlbumTransitionInfo;

    .line 64
    iput-object p2, p0, Lcom/google/android/music/ui/AlbumTransition;->mBaseActivity:Lcom/google/android/music/ui/BaseActivity;

    .line 65
    invoke-direct {p0}, Lcom/google/android/music/ui/AlbumTransition;->initialize()V

    .line 66
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/ui/AlbumTransition;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/AlbumTransition;

    .prologue
    .line 38
    iget v0, p0, Lcom/google/android/music/ui/AlbumTransition;->mStartingSharedElementColor:I

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/music/ui/AlbumTransition;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/AlbumTransition;

    .prologue
    .line 38
    iget v0, p0, Lcom/google/android/music/ui/AlbumTransition;->mImageId:I

    return v0
.end method

.method static synthetic access$1000(Lcom/google/android/music/ui/AlbumTransition;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/AlbumTransition;

    .prologue
    .line 38
    iget v0, p0, Lcom/google/android/music/ui/AlbumTransition;->mMetadataParentViewId:I

    return v0
.end method

.method static synthetic access$1100(Lcom/google/android/music/ui/AlbumTransition;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/AlbumTransition;

    .prologue
    .line 38
    iget v0, p0, Lcom/google/android/music/ui/AlbumTransition;->mActionBarId:I

    return v0
.end method

.method static synthetic access$1200(Lcom/google/android/music/ui/AlbumTransition;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/AlbumTransition;

    .prologue
    .line 38
    iget v0, p0, Lcom/google/android/music/ui/AlbumTransition;->mShadowId:I

    return v0
.end method

.method static synthetic access$1300(Lcom/google/android/music/ui/AlbumTransition;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/AlbumTransition;

    .prologue
    .line 38
    iget v0, p0, Lcom/google/android/music/ui/AlbumTransition;->mSharedElementMaxWidth:I

    return v0
.end method

.method static synthetic access$1302(Lcom/google/android/music/ui/AlbumTransition;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/AlbumTransition;
    .param p1, "x1"    # I

    .prologue
    .line 38
    iput p1, p0, Lcom/google/android/music/ui/AlbumTransition;->mSharedElementMaxWidth:I

    return p1
.end method

.method static synthetic access$1400(Lcom/google/android/music/ui/AlbumTransition;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/AlbumTransition;

    .prologue
    .line 38
    iget v0, p0, Lcom/google/android/music/ui/AlbumTransition;->mEndingSharedElementColor:I

    return v0
.end method

.method static synthetic access$1500(Lcom/google/android/music/ui/AlbumTransition;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/AlbumTransition;

    .prologue
    .line 38
    iget v0, p0, Lcom/google/android/music/ui/AlbumTransition;->mRootSharedElementId:I

    return v0
.end method

.method static synthetic access$200(Lcom/google/android/music/ui/AlbumTransition;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/AlbumTransition;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/music/ui/AlbumTransition;->mSnapshotAlbum:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$202(Lcom/google/android/music/ui/AlbumTransition;Landroid/widget/ImageView;)Landroid/widget/ImageView;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/AlbumTransition;
    .param p1, "x1"    # Landroid/widget/ImageView;

    .prologue
    .line 38
    iput-object p1, p0, Lcom/google/android/music/ui/AlbumTransition;->mSnapshotAlbum:Landroid/widget/ImageView;

    return-object p1
.end method

.method static synthetic access$300(Lcom/google/android/music/ui/AlbumTransition;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/AlbumTransition;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/music/ui/AlbumTransition;->mSnapshotAlbumBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/android/music/ui/AlbumTransition;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/AlbumTransition;
    .param p1, "x1"    # Landroid/graphics/Bitmap;

    .prologue
    .line 38
    iput-object p1, p0, Lcom/google/android/music/ui/AlbumTransition;->mSnapshotAlbumBitmap:Landroid/graphics/Bitmap;

    return-object p1
.end method

.method static synthetic access$400(Lcom/google/android/music/ui/AlbumTransition;)Lcom/google/android/music/ui/BaseActivity;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/AlbumTransition;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/music/ui/AlbumTransition;->mBaseActivity:Lcom/google/android/music/ui/BaseActivity;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/music/ui/AlbumTransition;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/AlbumTransition;

    .prologue
    .line 38
    iget v0, p0, Lcom/google/android/music/ui/AlbumTransition;->mBackgroundContainerId:I

    return v0
.end method

.method static synthetic access$600(Lcom/google/android/music/ui/AlbumTransition;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/AlbumTransition;

    .prologue
    .line 38
    iget v0, p0, Lcom/google/android/music/ui/AlbumTransition;->mFabId:I

    return v0
.end method

.method static synthetic access$700(Lcom/google/android/music/ui/AlbumTransition;Landroid/view/View;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/AlbumTransition;
    .param p1, "x1"    # Landroid/view/View;
    .param p2, "x2"    # Z

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/ui/AlbumTransition;->toggleVisibility(Landroid/view/View;Z)V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/music/ui/AlbumTransition;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/AlbumTransition;

    .prologue
    .line 38
    iget v0, p0, Lcom/google/android/music/ui/AlbumTransition;->mSpacerId:I

    return v0
.end method

.method static synthetic access$900(Lcom/google/android/music/ui/AlbumTransition;I)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/AlbumTransition;
    .param p1, "x1"    # I

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/google/android/music/ui/AlbumTransition;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private customizeEnterTransitions()V
    .locals 6

    .prologue
    .line 336
    iget-object v5, p0, Lcom/google/android/music/ui/AlbumTransition;->mBaseActivity:Lcom/google/android/music/ui/BaseActivity;

    invoke-virtual {v5}, Lcom/google/android/music/ui/BaseActivity;->getWindow()Landroid/view/Window;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/Window;->getSharedElementEnterTransition()Landroid/transition/Transition;

    move-result-object v2

    .line 340
    .local v2, "enterTransition":Landroid/transition/Transition;
    if-nez v2, :cond_0

    .line 417
    :goto_0
    return-void

    .line 344
    :cond_0
    const-wide/16 v0, 0xc8

    .line 345
    .local v0, "duration":J
    invoke-virtual {v2, v0, v1}, Landroid/transition/Transition;->setDuration(J)Landroid/transition/Transition;

    .line 347
    new-instance v3, Landroid/transition/Fade;

    invoke-direct {v3}, Landroid/transition/Fade;-><init>()V

    .line 348
    .local v3, "fadeShadow":Landroid/transition/Fade;
    iget v5, p0, Lcom/google/android/music/ui/AlbumTransition;->mShadowId:I

    invoke-virtual {v3, v5}, Landroid/transition/Fade;->addTarget(I)Landroid/transition/Transition;

    .line 349
    invoke-virtual {v3, v0, v1}, Landroid/transition/Fade;->setDuration(J)Landroid/transition/Transition;

    .line 351
    new-instance v4, Landroid/transition/TransitionSet;

    invoke-direct {v4}, Landroid/transition/TransitionSet;-><init>()V

    .line 352
    .local v4, "in":Landroid/transition/TransitionSet;
    invoke-virtual {v4, v2}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    .line 353
    invoke-virtual {v4, v3}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    .line 355
    new-instance v5, Lcom/google/android/music/ui/AlbumTransition$2;

    invoke-direct {v5, p0}, Lcom/google/android/music/ui/AlbumTransition$2;-><init>(Lcom/google/android/music/ui/AlbumTransition;)V

    invoke-virtual {v4, v5}, Landroid/transition/TransitionSet;->addListener(Landroid/transition/Transition$TransitionListener;)Landroid/transition/TransitionSet;

    .line 416
    iget-object v5, p0, Lcom/google/android/music/ui/AlbumTransition;->mBaseActivity:Lcom/google/android/music/ui/BaseActivity;

    invoke-virtual {v5}, Lcom/google/android/music/ui/BaseActivity;->getWindow()Landroid/view/Window;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/view/Window;->setSharedElementEnterTransition(Landroid/transition/Transition;)V

    goto :goto_0
.end method

.method private customizeReturnTransitions()V
    .locals 18

    .prologue
    .line 280
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/AlbumTransition;->mBaseActivity:Lcom/google/android/music/ui/BaseActivity;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/google/android/music/ui/BaseActivity;->getWindow()Landroid/view/Window;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Landroid/view/Window;->getSharedElementEnterTransition()Landroid/transition/Transition;

    move-result-object v3

    .line 281
    .local v3, "enterTransition":Landroid/transition/Transition;
    if-eqz v3, :cond_1

    .line 282
    const-wide/16 v4, 0xc8

    .line 285
    .local v4, "duration":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/AlbumTransition;->mBaseActivity:Lcom/google/android/music/ui/BaseActivity;

    move-object/from16 v16, v0

    invoke-static/range {v16 .. v16}, Landroid/transition/TransitionInflater;->from(Landroid/content/Context;)Landroid/transition/TransitionInflater;

    move-result-object v10

    .line 286
    .local v10, "inflater":Landroid/transition/TransitionInflater;
    const v16, 0x10f0001

    move/from16 v0, v16

    invoke-virtual {v10, v0}, Landroid/transition/TransitionInflater;->inflateTransition(I)Landroid/transition/Transition;

    move-result-object v11

    .line 287
    .local v11, "returnTransition":Landroid/transition/Transition;
    invoke-virtual {v3}, Landroid/transition/Transition;->getTargets()Ljava/util/List;

    move-result-object v16

    invoke-interface/range {v16 .. v16}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .local v9, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v16

    if-eqz v16, :cond_0

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Landroid/view/View;

    .line 288
    .local v15, "shared":Landroid/view/View;
    invoke-virtual {v11, v15}, Landroid/transition/Transition;->addTarget(Landroid/view/View;)Landroid/transition/Transition;

    goto :goto_0

    .line 290
    .end local v15    # "shared":Landroid/view/View;
    :cond_0
    invoke-virtual {v11, v4, v5}, Landroid/transition/Transition;->setDuration(J)Landroid/transition/Transition;

    .line 293
    new-instance v6, Landroid/transition/Fade;

    invoke-direct {v6}, Landroid/transition/Fade;-><init>()V

    .line 294
    .local v6, "fadeOut":Landroid/transition/Fade;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/music/ui/AlbumTransition;->mMetadataParentViewId:I

    move/from16 v16, v0

    move/from16 v0, v16

    invoke-virtual {v6, v0}, Landroid/transition/Fade;->addTarget(I)Landroid/transition/Transition;

    .line 298
    new-instance v12, Landroid/transition/ChangeTransform;

    invoke-direct {v12}, Landroid/transition/ChangeTransform;-><init>()V

    .line 299
    .local v12, "scaleFab":Landroid/transition/ChangeTransform;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/music/ui/AlbumTransition;->mFabId:I

    move/from16 v16, v0

    move/from16 v0, v16

    invoke-virtual {v12, v0}, Landroid/transition/ChangeTransform;->addTarget(I)Landroid/transition/Transition;

    .line 301
    new-instance v2, Landroid/transition/TransitionSet;

    invoke-direct {v2}, Landroid/transition/TransitionSet;-><init>()V

    .line 302
    .local v2, "contentTransition":Landroid/transition/TransitionSet;
    invoke-virtual {v2, v6}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    .line 303
    invoke-virtual {v2, v12}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    .line 304
    const-wide/16 v16, 0x2

    div-long v16, v4, v16

    move-wide/from16 v0, v16

    invoke-virtual {v2, v0, v1}, Landroid/transition/TransitionSet;->setDuration(J)Landroid/transition/TransitionSet;

    .line 306
    new-instance v13, Lcom/google/android/music/ui/BaseTrackListScrollTransition;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/AlbumTransition;->mBaseActivity:Lcom/google/android/music/ui/BaseActivity;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-direct {v13, v0}, Lcom/google/android/music/ui/BaseTrackListScrollTransition;-><init>(Landroid/content/Context;)V

    .line 307
    .local v13, "scrollList":Lcom/google/android/music/ui/BaseTrackListScrollTransition;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/music/ui/AlbumTransition;->mMetadataParentViewId:I

    move/from16 v16, v0

    move/from16 v0, v16

    invoke-virtual {v13, v0}, Lcom/google/android/music/ui/BaseTrackListScrollTransition;->addTarget(I)Landroid/transition/Transition;

    .line 308
    invoke-virtual {v13, v4, v5}, Lcom/google/android/music/ui/BaseTrackListScrollTransition;->setDuration(J)Landroid/transition/Transition;

    .line 310
    new-instance v8, Landroid/transition/Fade;

    invoke-direct {v8}, Landroid/transition/Fade;-><init>()V

    .line 311
    .local v8, "headerFadeOut":Landroid/transition/Fade;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/music/ui/AlbumTransition;->mActionBarId:I

    move/from16 v16, v0

    move/from16 v0, v16

    invoke-virtual {v8, v0}, Landroid/transition/Fade;->addTarget(I)Landroid/transition/Transition;

    .line 312
    const-wide/16 v16, 0xa

    div-long v16, v4, v16

    move-wide/from16 v0, v16

    invoke-virtual {v8, v0, v1}, Landroid/transition/Fade;->setDuration(J)Landroid/transition/Transition;

    .line 314
    new-instance v14, Landroid/transition/Fade;

    invoke-direct {v14}, Landroid/transition/Fade;-><init>()V

    .line 315
    .local v14, "shadowFade":Landroid/transition/Fade;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/music/ui/AlbumTransition;->mShadowId:I

    move/from16 v16, v0

    move/from16 v0, v16

    invoke-virtual {v14, v0}, Landroid/transition/Fade;->addTarget(I)Landroid/transition/Transition;

    .line 316
    invoke-virtual {v14, v4, v5}, Landroid/transition/Fade;->setDuration(J)Landroid/transition/Transition;

    .line 319
    new-instance v7, Landroid/transition/TransitionSet;

    invoke-direct {v7}, Landroid/transition/TransitionSet;-><init>()V

    .line 320
    .local v7, "fullExitSet":Landroid/transition/TransitionSet;
    invoke-virtual {v7, v13}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    .line 321
    invoke-virtual {v7, v2}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    .line 322
    invoke-virtual {v7, v11}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    .line 323
    invoke-virtual {v7, v8}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    .line 324
    invoke-virtual {v7, v14}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    .line 325
    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v7, v0}, Landroid/transition/TransitionSet;->setOrdering(I)Landroid/transition/TransitionSet;

    .line 327
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/AlbumTransition;->mBaseActivity:Lcom/google/android/music/ui/BaseActivity;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/google/android/music/ui/BaseActivity;->getWindow()Landroid/view/Window;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v7}, Landroid/view/Window;->setSharedElementReturnTransition(Landroid/transition/Transition;)V

    .line 329
    .end local v2    # "contentTransition":Landroid/transition/TransitionSet;
    .end local v4    # "duration":J
    .end local v6    # "fadeOut":Landroid/transition/Fade;
    .end local v7    # "fullExitSet":Landroid/transition/TransitionSet;
    .end local v8    # "headerFadeOut":Landroid/transition/Fade;
    .end local v9    # "i$":Ljava/util/Iterator;
    .end local v10    # "inflater":Landroid/transition/TransitionInflater;
    .end local v11    # "returnTransition":Landroid/transition/Transition;
    .end local v12    # "scaleFab":Landroid/transition/ChangeTransform;
    .end local v13    # "scrollList":Lcom/google/android/music/ui/BaseTrackListScrollTransition;
    .end local v14    # "shadowFade":Landroid/transition/Fade;
    :cond_1
    return-void
.end method

.method private findViewById(I)Landroid/view/View;
    .locals 1
    .param p1, "id"    # I

    .prologue
    .line 441
    iget-object v0, p0, Lcom/google/android/music/ui/AlbumTransition;->mBaseActivity:Lcom/google/android/music/ui/BaseActivity;

    invoke-virtual {v0, p1}, Lcom/google/android/music/ui/BaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private initialize()V
    .locals 4

    .prologue
    .line 69
    const v2, 0x7f0e019d

    iput v2, p0, Lcom/google/android/music/ui/AlbumTransition;->mRootSharedElementId:I

    .line 70
    const v2, 0x7f0e019a

    iput v2, p0, Lcom/google/android/music/ui/AlbumTransition;->mFabId:I

    .line 71
    const v2, 0x7f0e00d1

    iput v2, p0, Lcom/google/android/music/ui/AlbumTransition;->mImageId:I

    .line 72
    const v2, 0x7f0e020e

    iput v2, p0, Lcom/google/android/music/ui/AlbumTransition;->mBackgroundContainerId:I

    .line 73
    const v2, 0x102000a

    iput v2, p0, Lcom/google/android/music/ui/AlbumTransition;->mMetadataParentViewId:I

    .line 74
    const v2, 0x7f0e00c1

    iput v2, p0, Lcom/google/android/music/ui/AlbumTransition;->mActionBarId:I

    .line 75
    const v2, 0x7f0e0014

    iput v2, p0, Lcom/google/android/music/ui/AlbumTransition;->mSpacerId:I

    .line 76
    const v2, 0x7f0e00ea

    iput v2, p0, Lcom/google/android/music/ui/AlbumTransition;->mShadowId:I

    .line 78
    iget-object v2, p0, Lcom/google/android/music/ui/AlbumTransition;->mTransitionInfo:Lcom/google/android/music/ui/AlbumTransitionInfo;

    invoke-virtual {v2}, Lcom/google/android/music/ui/AlbumTransitionInfo;->getBackgroundColor()I

    move-result v1

    .line 79
    .local v1, "startColor":I
    iget-object v2, p0, Lcom/google/android/music/ui/AlbumTransition;->mBaseActivity:Lcom/google/android/music/ui/BaseActivity;

    invoke-virtual {v2}, Lcom/google/android/music/ui/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c0082

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 81
    .local v0, "endColor":I
    iput v1, p0, Lcom/google/android/music/ui/AlbumTransition;->mStartingSharedElementColor:I

    .line 82
    iput v0, p0, Lcom/google/android/music/ui/AlbumTransition;->mEndingSharedElementColor:I

    .line 84
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/android/music/ui/AlbumTransition;->mSharedElementMaxWidth:I

    .line 85
    return-void
.end method

.method private toggleVisibility(Landroid/view/View;Z)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;
    .param p2, "makeVisible"    # Z

    .prologue
    const/4 v1, 0x4

    .line 426
    if-eqz p2, :cond_1

    .line 427
    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-ne v0, v1, :cond_0

    .line 428
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 435
    :cond_0
    :goto_0
    return-void

    .line 431
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 432
    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public destroy()V
    .locals 0

    .prologue
    .line 269
    invoke-virtual {p0}, Lcom/google/android/music/ui/AlbumTransition;->recycleBitmaps()V

    .line 271
    invoke-super {p0}, Lcom/google/android/music/ui/SharedElementTransition;->destroy()V

    .line 272
    return-void
.end method

.method public recycleBitmaps()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 448
    iget-object v1, p0, Lcom/google/android/music/ui/AlbumTransition;->mSnapshotAlbum:Landroid/widget/ImageView;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/music/ui/AlbumTransition;->mSnapshotAlbumBitmap:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_1

    .line 449
    iget-object v1, p0, Lcom/google/android/music/ui/AlbumTransition;->mSnapshotAlbum:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 450
    iget-object v1, p0, Lcom/google/android/music/ui/AlbumTransition;->mSnapshotAlbumBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 451
    iput-object v2, p0, Lcom/google/android/music/ui/AlbumTransition;->mSnapshotAlbumBitmap:Landroid/graphics/Bitmap;

    .line 453
    iget-object v1, p0, Lcom/google/android/music/ui/AlbumTransition;->mSnapshotAlbum:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    instance-of v1, v1, Landroid/view/ViewGroup;

    if-eqz v1, :cond_0

    .line 454
    iget-object v1, p0, Lcom/google/android/music/ui/AlbumTransition;->mSnapshotAlbum:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 455
    .local v0, "parent":Landroid/view/ViewGroup;
    iget-object v1, p0, Lcom/google/android/music/ui/AlbumTransition;->mSnapshotAlbum:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 458
    .end local v0    # "parent":Landroid/view/ViewGroup;
    :cond_0
    iput-object v2, p0, Lcom/google/android/music/ui/AlbumTransition;->mSnapshotAlbum:Landroid/widget/ImageView;

    .line 460
    :cond_1
    return-void
.end method

.method public setup()V
    .locals 2

    .prologue
    .line 93
    invoke-direct {p0}, Lcom/google/android/music/ui/AlbumTransition;->customizeEnterTransitions()V

    .line 94
    invoke-direct {p0}, Lcom/google/android/music/ui/AlbumTransition;->customizeReturnTransitions()V

    .line 95
    iget-object v0, p0, Lcom/google/android/music/ui/AlbumTransition;->mBaseActivity:Lcom/google/android/music/ui/BaseActivity;

    new-instance v1, Lcom/google/android/music/ui/AlbumTransition$1;

    invoke-direct {v1, p0}, Lcom/google/android/music/ui/AlbumTransition$1;-><init>(Lcom/google/android/music/ui/AlbumTransition;)V

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/BaseActivity;->setEnterSharedElementCallback(Landroid/support/v4/app/SharedElementCallback;)V

    .line 265
    return-void
.end method
