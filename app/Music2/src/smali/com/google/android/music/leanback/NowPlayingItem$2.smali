.class Lcom/google/android/music/leanback/NowPlayingItem$2;
.super Landroid/content/BroadcastReceiver;
.source "NowPlayingItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/leanback/NowPlayingItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/leanback/NowPlayingItem;


# direct methods
.method constructor <init>(Lcom/google/android/music/leanback/NowPlayingItem;)V
    .locals 0

    .prologue
    .line 153
    iput-object p1, p0, Lcom/google/android/music/leanback/NowPlayingItem$2;->this$0:Lcom/google/android/music/leanback/NowPlayingItem;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 156
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 157
    .local v0, "action":Ljava/lang/String;
    # getter for: Lcom/google/android/music/leanback/NowPlayingItem;->LOGV:Z
    invoke-static {}, Lcom/google/android/music/leanback/NowPlayingItem;->access$200()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 158
    # getter for: Lcom/google/android/music/leanback/NowPlayingItem;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/music/leanback/NowPlayingItem;->access$300()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onReceive: action="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    :cond_0
    const-string v1, "com.android.music.metachanged"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "com.android.music.asyncopencomplete"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "com.android.music.asyncopenstart"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "com.android.music.playbackfailed"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "com.android.music.playstatechanged"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 165
    :cond_1
    iget-object v1, p0, Lcom/google/android/music/leanback/NowPlayingItem$2;->this$0:Lcom/google/android/music/leanback/NowPlayingItem;

    # invokes: Lcom/google/android/music/leanback/NowPlayingItem;->updateTrackInfo(Landroid/content/Intent;)V
    invoke-static {v1, p2}, Lcom/google/android/music/leanback/NowPlayingItem;->access$400(Lcom/google/android/music/leanback/NowPlayingItem;Landroid/content/Intent;)V

    .line 167
    :cond_2
    return-void
.end method
