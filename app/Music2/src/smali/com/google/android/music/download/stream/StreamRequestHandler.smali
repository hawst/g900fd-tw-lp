.class public Lcom/google/android/music/download/stream/StreamRequestHandler;
.super Ljava/lang/Object;
.source "StreamRequestHandler.java"

# interfaces
.implements Lorg/apache/http/protocol/HttpRequestHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/download/stream/StreamRequestHandler$1;,
        Lcom/google/android/music/download/stream/StreamRequestHandler$InputStreamEntity;
    }
.end annotation


# static fields
.field private static final LOGV:Z

.field private static final RANGE_PATTERN:Ljava/util/regex/Pattern;


# instance fields
.field private final mAllowedStreams:Lcom/google/android/music/download/stream/AllowedStreams;

.field private volatile mStreamingContent:Lcom/google/android/music/download/stream/StreamingContent;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->DOWNLOAD:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/download/stream/StreamRequestHandler;->LOGV:Z

    .line 36
    const-string v0, "bytes=(\\d+)-?(\\d*)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/download/stream/StreamRequestHandler;->RANGE_PATTERN:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/music/download/stream/AllowedStreams;)V
    .locals 0
    .param p1, "allowedStreams"    # Lcom/google/android/music/download/stream/AllowedStreams;

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object p1, p0, Lcom/google/android/music/download/stream/StreamRequestHandler;->mAllowedStreams:Lcom/google/android/music/download/stream/AllowedStreams;

    .line 50
    return-void
.end method


# virtual methods
.method public getDownloadRequest()Lcom/google/android/music/download/TrackDownloadRequest;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/google/android/music/download/stream/StreamRequestHandler;->mStreamingContent:Lcom/google/android/music/download/stream/StreamingContent;

    if-nez v0, :cond_0

    .line 148
    const/4 v0, 0x0

    .line 150
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/music/download/stream/StreamRequestHandler;->mStreamingContent:Lcom/google/android/music/download/stream/StreamingContent;

    invoke-virtual {v0}, Lcom/google/android/music/download/stream/StreamingContent;->getDownloadRequest()Lcom/google/android/music/download/TrackDownloadRequest;

    move-result-object v0

    goto :goto_0
.end method

.method public handle(Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;)V
    .locals 26
    .param p1, "request"    # Lorg/apache/http/HttpRequest;
    .param p2, "response"    # Lorg/apache/http/HttpResponse;
    .param p3, "context"    # Lorg/apache/http/protocol/HttpContext;

    .prologue
    .line 53
    invoke-interface/range {p1 .. p1}, Lorg/apache/http/HttpRequest;->getRequestLine()Lorg/apache/http/RequestLine;

    move-result-object v14

    .line 57
    .local v14, "reqLine":Lorg/apache/http/RequestLine;
    invoke-interface {v14}, Lorg/apache/http/RequestLine;->getUri()Ljava/lang/String;

    move-result-object v13

    .line 58
    .local v13, "path":Ljava/lang/String;
    sget-boolean v21, Lcom/google/android/music/download/stream/StreamRequestHandler;->LOGV:Z

    if-eqz v21, :cond_0

    .line 59
    const-string v21, "StreamRequestHandler"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "handle: "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    invoke-interface/range {p1 .. p1}, Lorg/apache/http/HttpRequest;->getAllHeaders()[Lorg/apache/http/Header;

    move-result-object v4

    .local v4, "arr$":[Lorg/apache/http/Header;
    array-length v11, v4

    .local v11, "len$":I
    const/4 v9, 0x0

    .local v9, "i$":I
    :goto_0
    if-ge v9, v11, :cond_0

    aget-object v7, v4, v9

    .line 61
    .local v7, "header":Lorg/apache/http/Header;
    const-string v21, "StreamRequestHandler"

    const-string v22, "Header: %s: %s"

    const/16 v23, 0x2

    move/from16 v0, v23

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-interface {v7}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v25

    aput-object v25, v23, v24

    const/16 v24, 0x1

    invoke-interface {v7}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v25

    aput-object v25, v23, v24

    invoke-static/range {v22 .. v23}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 64
    .end local v4    # "arr$":[Lorg/apache/http/Header;
    .end local v7    # "header":Lorg/apache/http/Header;
    .end local v9    # "i$":I
    .end local v11    # "len$":I
    :cond_0
    const/16 v21, 0x2f

    move/from16 v0, v21

    invoke-virtual {v13, v0}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v10

    .line 65
    .local v10, "lastSlash":I
    const/16 v21, -0x1

    move/from16 v0, v21

    if-ne v10, v0, :cond_1

    .line 66
    new-instance v21, Ljava/lang/IllegalArgumentException;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "Unknown URL requested: "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-direct/range {v21 .. v22}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v21

    .line 70
    :cond_1
    add-int/lit8 v21, v10, 0x1

    :try_start_0
    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v22

    move/from16 v0, v21

    move/from16 v1, v22

    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v16

    .line 75
    .local v16, "secureId":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/stream/StreamRequestHandler;->mAllowedStreams:Lcom/google/android/music/download/stream/AllowedStreams;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move-wide/from16 v1, v16

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/download/stream/AllowedStreams;->findStreamBySecureId(J)Lcom/google/android/music/download/stream/StreamingContent;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/music/download/stream/StreamRequestHandler;->mStreamingContent:Lcom/google/android/music/download/stream/StreamingContent;

    .line 76
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/stream/StreamRequestHandler;->mStreamingContent:Lcom/google/android/music/download/stream/StreamingContent;

    move-object/from16 v21, v0

    if-nez v21, :cond_3

    .line 78
    sget-boolean v21, Lcom/google/android/music/download/stream/StreamRequestHandler;->LOGV:Z

    if-eqz v21, :cond_2

    .line 79
    const-string v21, "StreamRequestHandler"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "Requesting file which is not allowed to be streamed: "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/stream/StreamRequestHandler;->mAllowedStreams:Lcom/google/android/music/download/stream/AllowedStreams;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    :cond_2
    const/16 v21, 0x194

    move-object/from16 v0, p2

    move/from16 v1, v21

    invoke-interface {v0, v1}, Lorg/apache/http/HttpResponse;->setStatusCode(I)V

    .line 144
    :goto_1
    return-void

    .line 71
    .end local v16    # "secureId":J
    :catch_0
    move-exception v6

    .line 72
    .local v6, "e":Ljava/lang/NumberFormatException;
    new-instance v21, Ljava/lang/IllegalArgumentException;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "Unknown URL requested: "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-direct/range {v21 .. v22}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v21

    .line 87
    .end local v6    # "e":Ljava/lang/NumberFormatException;
    .restart local v16    # "secureId":J
    :cond_3
    const-string v21, "Range"

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, Lorg/apache/http/HttpRequest;->getLastHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v8

    .line 88
    .local v8, "headerRange":Lorg/apache/http/Header;
    const/16 v20, 0x0

    .line 89
    .local v20, "usingRangeHeader":Z
    const-wide/16 v18, 0x0

    .line 91
    .local v18, "startRangeByte":J
    if-eqz v8, :cond_5

    .line 94
    sget-object v21, Lcom/google/android/music/download/stream/StreamRequestHandler;->RANGE_PATTERN:Ljava/util/regex/Pattern;

    invoke-interface {v8}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v12

    .line 95
    .local v12, "matcher":Ljava/util/regex/Matcher;
    invoke-virtual {v12}, Ljava/util/regex/Matcher;->matches()Z

    move-result v21

    if-eqz v21, :cond_5

    .line 96
    const/16 v21, 0x1

    move/from16 v0, v21

    invoke-virtual {v12, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v18

    .line 97
    sget-boolean v21, Lcom/google/android/music/download/stream/StreamRequestHandler;->LOGV:Z

    if-eqz v21, :cond_4

    const-string v21, "StreamRequestHandler"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "Server requesting byte: "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move-wide/from16 v1, v18

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    :cond_4
    const/16 v20, 0x1

    .line 102
    .end local v12    # "matcher":Ljava/util/regex/Matcher;
    :cond_5
    const/4 v5, 0x0

    .line 103
    .local v5, "contentType":Ljava/lang/String;
    invoke-static {}, Lcom/google/android/music/preferences/MusicPreferences;->isGingerbreadOrGreater()Z

    move-result v21

    if-eqz v21, :cond_6

    .line 108
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/stream/StreamRequestHandler;->mStreamingContent:Lcom/google/android/music/download/stream/StreamingContent;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/google/android/music/download/stream/StreamingContent;->waitForContentType()Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v5

    .line 113
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/stream/StreamRequestHandler;->mStreamingContent:Lcom/google/android/music/download/stream/StreamingContent;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/google/android/music/download/stream/StreamingContent;->getStartReadPoint()J

    move-result-wide v22

    add-long v18, v18, v22

    .line 115
    if-nez v5, :cond_6

    .line 116
    const-string v21, "StreamRequestHandler"

    const-string v22, "Missing content type - exiting content=%s"

    const/16 v23, 0x1

    move/from16 v0, v23

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/stream/StreamRequestHandler;->mStreamingContent:Lcom/google/android/music/download/stream/StreamingContent;

    move-object/from16 v25, v0

    aput-object v25, v23, v24

    invoke-static/range {v22 .. v23}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    const/16 v21, 0x194

    move-object/from16 v0, p2

    move/from16 v1, v21

    invoke-interface {v0, v1}, Lorg/apache/http/HttpResponse;->setStatusCode(I)V

    goto/16 :goto_1

    .line 109
    :catch_1
    move-exception v6

    .line 110
    .local v6, "e":Ljava/lang/InterruptedException;
    const-string v21, "StreamRequestHandler"

    const-string v22, "Failed to retrieve content type"

    invoke-static/range {v21 .. v22}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 122
    .end local v6    # "e":Ljava/lang/InterruptedException;
    :cond_6
    sget-boolean v21, Lcom/google/android/music/download/stream/StreamRequestHandler;->LOGV:Z

    if-eqz v21, :cond_7

    .line 123
    const-string v21, "StreamRequestHandler"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "The content type is: "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    :cond_7
    if-eqz v5, :cond_8

    .line 126
    const-string v21, "Content-Type"

    move-object/from16 v0, p2

    move-object/from16 v1, v21

    invoke-interface {v0, v1, v5}, Lorg/apache/http/HttpResponse;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    :cond_8
    const-string v21, "X-SocketTimeout"

    const-string v22, "60"

    move-object/from16 v0, p2

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-interface {v0, v1, v2}, Lorg/apache/http/HttpResponse;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    const-string v21, "Pragma"

    const-string v22, "no-cache"

    move-object/from16 v0, p2

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-interface {v0, v1, v2}, Lorg/apache/http/HttpResponse;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    const-string v21, "Cache-Control"

    const-string v22, "no-cache"

    move-object/from16 v0, p2

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-interface {v0, v1, v2}, Lorg/apache/http/HttpResponse;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    new-instance v15, Lcom/google/android/music/download/stream/TailStream;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/stream/StreamRequestHandler;->mStreamingContent:Lcom/google/android/music/download/stream/StreamingContent;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/google/android/music/download/stream/StreamingContent;->getContext()Landroid/content/Context;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/stream/StreamRequestHandler;->mStreamingContent:Lcom/google/android/music/download/stream/StreamingContent;

    move-object/from16 v22, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    move-wide/from16 v2, v18

    invoke-direct {v15, v0, v1, v2, v3}, Lcom/google/android/music/download/stream/TailStream;-><init>(Landroid/content/Context;Lcom/google/android/music/download/stream/StreamingContent;J)V

    .line 138
    .local v15, "tailStream":Lcom/google/android/music/download/stream/TailStream;
    new-instance v21, Lcom/google/android/music/download/stream/StreamRequestHandler$InputStreamEntity;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/download/stream/StreamRequestHandler;->mStreamingContent:Lcom/google/android/music/download/stream/StreamingContent;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/google/android/music/download/stream/StreamingContent;->toString()Ljava/lang/String;

    move-result-object v22

    const/16 v23, 0x0

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-direct {v0, v1, v15, v2}, Lcom/google/android/music/download/stream/StreamRequestHandler$InputStreamEntity;-><init>(Ljava/lang/String;Ljava/io/InputStream;Lcom/google/android/music/download/stream/StreamRequestHandler$1;)V

    move-object/from16 v0, p2

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, Lorg/apache/http/HttpResponse;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 139
    if-eqz v20, :cond_9

    .line 140
    const/16 v21, 0xce

    move-object/from16 v0, p2

    move/from16 v1, v21

    invoke-interface {v0, v1}, Lorg/apache/http/HttpResponse;->setStatusCode(I)V

    goto/16 :goto_1

    .line 142
    :cond_9
    const/16 v21, 0xc8

    move-object/from16 v0, p2

    move/from16 v1, v21

    invoke-interface {v0, v1}, Lorg/apache/http/HttpResponse;->setStatusCode(I)V

    goto/16 :goto_1
.end method
