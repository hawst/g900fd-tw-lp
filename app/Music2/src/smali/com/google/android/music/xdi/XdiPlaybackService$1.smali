.class Lcom/google/android/music/xdi/XdiPlaybackService$1;
.super Ljava/lang/Object;
.source "XdiPlaybackService.java"

# interfaces
.implements Lcom/google/android/music/xdi/XdiPlayHelper$CompletionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/xdi/XdiPlaybackService;->onCreate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/xdi/XdiPlaybackService;


# direct methods
.method constructor <init>(Lcom/google/android/music/xdi/XdiPlaybackService;)V
    .locals 0

    .prologue
    .line 40
    iput-object p1, p0, Lcom/google/android/music/xdi/XdiPlaybackService$1;->this$0:Lcom/google/android/music/xdi/XdiPlaybackService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onComplete(Z)V
    .locals 3
    .param p1, "status"    # Z

    .prologue
    .line 43
    # getter for: Lcom/google/android/music/xdi/XdiPlaybackService;->LOGV:Z
    invoke-static {}, Lcom/google/android/music/xdi/XdiPlaybackService;->access$000()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 44
    const-string v0, "MusicXdi"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onComplete - status: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/xdi/XdiPlaybackService$1;->this$0:Lcom/google/android/music/xdi/XdiPlaybackService;

    invoke-virtual {v0}, Lcom/google/android/music/xdi/XdiPlaybackService;->stopSelf()V

    .line 47
    return-void
.end method
