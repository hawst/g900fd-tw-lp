.class public Lcom/google/android/music/cloudclient/SearchClientResponseJson;
.super Lcom/google/api/client/json/GenericJson;
.source "SearchClientResponseJson.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry;
    }
.end annotation


# instance fields
.field public mContinuationToken:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "continuation_token"
    .end annotation
.end field

.field public mEntries:Ljava/util/List;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "entries"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/cloudclient/SearchClientResponseJson$SearchClientResponseEntry;",
            ">;"
        }
    .end annotation
.end field

.field public mNumResults:I
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "num_results"
    .end annotation
.end field

.field public mSuggestedQuery:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "suggestedQuery"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    .line 65
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/cloudclient/SearchClientResponseJson;->mEntries:Ljava/util/List;

    return-void
.end method
