.class Lcom/google/android/music/leanback/BackgroundImageMessageHandler$1;
.super Ljava/lang/Object;
.source "BackgroundImageMessageHandler.java"

# interfaces
.implements Lcom/google/android/music/leanback/bitmap/LoadBitmapAsyncTask$OnIdleListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/leanback/BackgroundImageMessageHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/leanback/BackgroundImageMessageHandler;


# direct methods
.method constructor <init>(Lcom/google/android/music/leanback/BackgroundImageMessageHandler;)V
    .locals 0

    .prologue
    .line 68
    iput-object p1, p0, Lcom/google/android/music/leanback/BackgroundImageMessageHandler$1;->this$0:Lcom/google/android/music/leanback/BackgroundImageMessageHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onIdle()V
    .locals 4

    .prologue
    .line 71
    # getter for: Lcom/google/android/music/leanback/BackgroundImageMessageHandler;->DEBUG:Z
    invoke-static {}, Lcom/google/android/music/leanback/BackgroundImageMessageHandler;->access$000()Z

    move-result v0

    if-eqz v0, :cond_0

    # getter for: Lcom/google/android/music/leanback/BackgroundImageMessageHandler;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/music/leanback/BackgroundImageMessageHandler;->access$100()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onIdle "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/leanback/BackgroundImageMessageHandler$1;->this$0:Lcom/google/android/music/leanback/BackgroundImageMessageHandler;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/leanback/BackgroundImageMessageHandler$1;->this$0:Lcom/google/android/music/leanback/BackgroundImageMessageHandler;

    # getter for: Lcom/google/android/music/leanback/BackgroundImageMessageHandler;->mItem:Lcom/google/android/music/leanback/Item;
    invoke-static {v0}, Lcom/google/android/music/leanback/BackgroundImageMessageHandler;->access$200(Lcom/google/android/music/leanback/BackgroundImageMessageHandler;)Lcom/google/android/music/leanback/Item;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 73
    iget-object v0, p0, Lcom/google/android/music/leanback/BackgroundImageMessageHandler$1;->this$0:Lcom/google/android/music/leanback/BackgroundImageMessageHandler;

    iget-object v1, p0, Lcom/google/android/music/leanback/BackgroundImageMessageHandler$1;->this$0:Lcom/google/android/music/leanback/BackgroundImageMessageHandler;

    # getter for: Lcom/google/android/music/leanback/BackgroundImageMessageHandler;->mLastLoadingStartTime:J
    invoke-static {v1}, Lcom/google/android/music/leanback/BackgroundImageMessageHandler;->access$300(Lcom/google/android/music/leanback/BackgroundImageMessageHandler;)J

    move-result-wide v2

    iget-object v1, p0, Lcom/google/android/music/leanback/BackgroundImageMessageHandler$1;->this$0:Lcom/google/android/music/leanback/BackgroundImageMessageHandler;

    # getter for: Lcom/google/android/music/leanback/BackgroundImageMessageHandler;->mItem:Lcom/google/android/music/leanback/Item;
    invoke-static {v1}, Lcom/google/android/music/leanback/BackgroundImageMessageHandler;->access$200(Lcom/google/android/music/leanback/BackgroundImageMessageHandler;)Lcom/google/android/music/leanback/Item;

    move-result-object v1

    # invokes: Lcom/google/android/music/leanback/BackgroundImageMessageHandler;->loadItemBackground(JLcom/google/android/music/leanback/Item;)V
    invoke-static {v0, v2, v3, v1}, Lcom/google/android/music/leanback/BackgroundImageMessageHandler;->access$400(Lcom/google/android/music/leanback/BackgroundImageMessageHandler;JLcom/google/android/music/leanback/Item;)V

    .line 75
    :cond_1
    return-void
.end method
