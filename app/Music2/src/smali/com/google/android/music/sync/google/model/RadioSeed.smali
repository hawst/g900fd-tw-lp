.class public Lcom/google/android/music/sync/google/model/RadioSeed;
.super Lcom/google/api/client/json/GenericJson;
.source "RadioSeed.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/sync/google/model/RadioSeed$1;,
        Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;
    }
.end annotation


# instance fields
.field public mAlbumId:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "albumId"
    .end annotation
.end field

.field public mArtistId:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "artistId"
    .end annotation
.end field

.field public mCuratedStationId:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "curatedStationId"
    .end annotation
.end field

.field public mGenreId:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "genreId"
    .end annotation
.end field

.field public mPlaylistShareToken:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "playlistShareToken"
    .end annotation
.end field

.field public mSeedType:I
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "seedType"
    .end annotation
.end field

.field public mTrackId:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "trackId"
    .end annotation
.end field

.field public mTrackLockerId:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "trackLockerId"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    .line 20
    return-void
.end method

.method public static createRadioSeed(Ljava/lang/String;I)Lcom/google/android/music/sync/google/model/RadioSeed;
    .locals 5
    .param p0, "sourceId"    # Ljava/lang/String;
    .param p1, "schemaSeedType"    # I

    .prologue
    .line 121
    new-instance v0, Lcom/google/android/music/sync/google/model/RadioSeed;

    invoke-direct {v0}, Lcom/google/android/music/sync/google/model/RadioSeed;-><init>()V

    .line 122
    .local v0, "radioSeed":Lcom/google/android/music/sync/google/model/RadioSeed;
    invoke-static {p1}, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;->fromSchemaValue(I)Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;

    move-result-object v1

    .line 123
    .local v1, "rst":Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;
    invoke-virtual {v1}, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;->getRemoteValue()I

    move-result v2

    iput v2, v0, Lcom/google/android/music/sync/google/model/RadioSeed;->mSeedType:I

    .line 124
    sget-object v2, Lcom/google/android/music/sync/google/model/RadioSeed$1;->$SwitchMap$com$google$android$music$sync$google$model$RadioSeed$RemoteSeedType:[I

    invoke-virtual {v1}, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 150
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unsupported schemaSeedType: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 126
    :pswitch_0
    iput-object p0, v0, Lcom/google/android/music/sync/google/model/RadioSeed;->mTrackLockerId:Ljava/lang/String;

    .line 152
    :goto_0
    :pswitch_1
    return-object v0

    .line 129
    :pswitch_2
    iput-object p0, v0, Lcom/google/android/music/sync/google/model/RadioSeed;->mTrackId:Ljava/lang/String;

    goto :goto_0

    .line 132
    :pswitch_3
    iput-object p0, v0, Lcom/google/android/music/sync/google/model/RadioSeed;->mAlbumId:Ljava/lang/String;

    goto :goto_0

    .line 136
    :pswitch_4
    iput-object p0, v0, Lcom/google/android/music/sync/google/model/RadioSeed;->mArtistId:Ljava/lang/String;

    goto :goto_0

    .line 139
    :pswitch_5
    iput-object p0, v0, Lcom/google/android/music/sync/google/model/RadioSeed;->mGenreId:Ljava/lang/String;

    goto :goto_0

    .line 144
    :pswitch_6
    iput-object p0, v0, Lcom/google/android/music/sync/google/model/RadioSeed;->mPlaylistShareToken:Ljava/lang/String;

    goto :goto_0

    .line 147
    :pswitch_7
    iput-object p0, v0, Lcom/google/android/music/sync/google/model/RadioSeed;->mCuratedStationId:Ljava/lang/String;

    goto :goto_0

    .line 124
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_4
        :pswitch_3
        :pswitch_5
        :pswitch_1
        :pswitch_4
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method


# virtual methods
.method public getSourceIdAndType()Landroid/util/Pair;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 159
    const/4 v0, 0x0

    .line 160
    .local v0, "result":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Integer;>;"
    iget v3, p0, Lcom/google/android/music/sync/google/model/RadioSeed;->mSeedType:I

    invoke-static {v3}, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;->fromRemoteValue(I)Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;

    move-result-object v1

    .line 161
    .local v1, "rst":Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;
    invoke-virtual {v1}, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;->getSchemaValue()I

    move-result v2

    .line 162
    .local v2, "schemaValue":I
    sget-object v3, Lcom/google/android/music/sync/google/model/RadioSeed$1;->$SwitchMap$com$google$android$music$sync$google$model$RadioSeed$RemoteSeedType:[I

    invoke-virtual {v1}, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 193
    iget-object v3, p0, Lcom/google/android/music/sync/google/model/RadioSeed;->mTrackLockerId:Ljava/lang/String;

    if-eqz v3, :cond_1

    .line 194
    new-instance v0, Landroid/util/Pair;

    .end local v0    # "result":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Integer;>;"
    iget-object v3, p0, Lcom/google/android/music/sync/google/model/RadioSeed;->mTrackLockerId:Ljava/lang/String;

    sget-object v4, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;->LOCKER_TRACK:Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;

    invoke-virtual {v4}, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;->getSchemaValue()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v3, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 216
    .restart local v0    # "result":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Integer;>;"
    :cond_0
    :goto_0
    if-nez v0, :cond_7

    .line 217
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Seed is empty"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 164
    :pswitch_0
    new-instance v0, Landroid/util/Pair;

    .end local v0    # "result":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Integer;>;"
    iget-object v3, p0, Lcom/google/android/music/sync/google/model/RadioSeed;->mTrackLockerId:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v3, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 165
    .restart local v0    # "result":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Integer;>;"
    goto :goto_0

    .line 167
    :pswitch_1
    new-instance v0, Landroid/util/Pair;

    .end local v0    # "result":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Integer;>;"
    iget-object v3, p0, Lcom/google/android/music/sync/google/model/RadioSeed;->mTrackId:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v3, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 168
    .restart local v0    # "result":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Integer;>;"
    goto :goto_0

    .line 170
    :pswitch_2
    new-instance v0, Landroid/util/Pair;

    .end local v0    # "result":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Integer;>;"
    iget-object v3, p0, Lcom/google/android/music/sync/google/model/RadioSeed;->mAlbumId:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v3, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 171
    .restart local v0    # "result":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Integer;>;"
    goto :goto_0

    .line 174
    :pswitch_3
    new-instance v0, Landroid/util/Pair;

    .end local v0    # "result":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Integer;>;"
    iget-object v3, p0, Lcom/google/android/music/sync/google/model/RadioSeed;->mArtistId:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v3, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 175
    .restart local v0    # "result":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Integer;>;"
    goto :goto_0

    .line 177
    :pswitch_4
    new-instance v0, Landroid/util/Pair;

    .end local v0    # "result":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Integer;>;"
    iget-object v3, p0, Lcom/google/android/music/sync/google/model/RadioSeed;->mGenreId:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v3, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 178
    .restart local v0    # "result":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Integer;>;"
    goto :goto_0

    .line 180
    :pswitch_5
    new-instance v0, Landroid/util/Pair;

    .end local v0    # "result":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Integer;>;"
    const/4 v3, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v3, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 181
    .restart local v0    # "result":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Integer;>;"
    goto :goto_0

    .line 183
    :pswitch_6
    new-instance v0, Landroid/util/Pair;

    .end local v0    # "result":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Integer;>;"
    iget-object v3, p0, Lcom/google/android/music/sync/google/model/RadioSeed;->mPlaylistShareToken:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v3, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 184
    .restart local v0    # "result":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Integer;>;"
    goto :goto_0

    .line 186
    :pswitch_7
    new-instance v0, Landroid/util/Pair;

    .end local v0    # "result":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Integer;>;"
    iget-object v3, p0, Lcom/google/android/music/sync/google/model/RadioSeed;->mCuratedStationId:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v3, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 187
    .restart local v0    # "result":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Integer;>;"
    goto :goto_0

    .line 196
    :cond_1
    iget-object v3, p0, Lcom/google/android/music/sync/google/model/RadioSeed;->mTrackId:Ljava/lang/String;

    if-eqz v3, :cond_2

    .line 197
    new-instance v0, Landroid/util/Pair;

    .end local v0    # "result":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Integer;>;"
    iget-object v3, p0, Lcom/google/android/music/sync/google/model/RadioSeed;->mTrackId:Ljava/lang/String;

    sget-object v4, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;->METAJAM_TRACK:Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;

    invoke-virtual {v4}, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;->getSchemaValue()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v3, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .restart local v0    # "result":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Integer;>;"
    goto :goto_0

    .line 199
    :cond_2
    iget-object v3, p0, Lcom/google/android/music/sync/google/model/RadioSeed;->mAlbumId:Ljava/lang/String;

    if-eqz v3, :cond_3

    .line 200
    new-instance v0, Landroid/util/Pair;

    .end local v0    # "result":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Integer;>;"
    iget-object v3, p0, Lcom/google/android/music/sync/google/model/RadioSeed;->mAlbumId:Ljava/lang/String;

    sget-object v4, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;->ALBUM:Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;

    invoke-virtual {v4}, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;->getSchemaValue()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v3, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .restart local v0    # "result":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Integer;>;"
    goto/16 :goto_0

    .line 202
    :cond_3
    iget-object v3, p0, Lcom/google/android/music/sync/google/model/RadioSeed;->mArtistId:Ljava/lang/String;

    if-eqz v3, :cond_4

    .line 203
    new-instance v0, Landroid/util/Pair;

    .end local v0    # "result":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Integer;>;"
    iget-object v3, p0, Lcom/google/android/music/sync/google/model/RadioSeed;->mArtistId:Ljava/lang/String;

    sget-object v4, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;->ARTIST:Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;

    invoke-virtual {v4}, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;->getSchemaValue()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v3, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .restart local v0    # "result":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Integer;>;"
    goto/16 :goto_0

    .line 205
    :cond_4
    iget-object v3, p0, Lcom/google/android/music/sync/google/model/RadioSeed;->mGenreId:Ljava/lang/String;

    if-eqz v3, :cond_5

    .line 206
    new-instance v0, Landroid/util/Pair;

    .end local v0    # "result":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Integer;>;"
    iget-object v3, p0, Lcom/google/android/music/sync/google/model/RadioSeed;->mGenreId:Ljava/lang/String;

    sget-object v4, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;->GENRE:Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;

    invoke-virtual {v4}, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;->getSchemaValue()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v3, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .restart local v0    # "result":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Integer;>;"
    goto/16 :goto_0

    .line 208
    :cond_5
    iget-object v3, p0, Lcom/google/android/music/sync/google/model/RadioSeed;->mPlaylistShareToken:Ljava/lang/String;

    if-eqz v3, :cond_6

    .line 209
    new-instance v0, Landroid/util/Pair;

    .end local v0    # "result":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Integer;>;"
    iget-object v3, p0, Lcom/google/android/music/sync/google/model/RadioSeed;->mPlaylistShareToken:Ljava/lang/String;

    sget-object v4, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;->PLAYLIST:Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;

    invoke-virtual {v4}, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;->getSchemaValue()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v3, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .restart local v0    # "result":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Integer;>;"
    goto/16 :goto_0

    .line 211
    :cond_6
    iget-object v3, p0, Lcom/google/android/music/sync/google/model/RadioSeed;->mCuratedStationId:Ljava/lang/String;

    if-eqz v3, :cond_0

    .line 212
    new-instance v3, Landroid/util/Pair;

    iget-object v4, p0, Lcom/google/android/music/sync/google/model/RadioSeed;->mCuratedStationId:Ljava/lang/String;

    sget-object v5, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;->CURATED:Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;

    invoke-virtual {v5}, Lcom/google/android/music/sync/google/model/RadioSeed$RemoteSeedType;->getSchemaValue()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 219
    :goto_1
    return-object v3

    :cond_7
    move-object v3, v0

    goto :goto_1

    .line 162
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_5
        :pswitch_3
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method isValidForUpstreamInsert()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 223
    iget-object v0, p0, Lcom/google/android/music/sync/google/model/RadioSeed;->mTrackLockerId:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/sync/google/model/RadioSeed;->mTrackId:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/sync/google/model/RadioSeed;->mAlbumId:Ljava/lang/String;

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    iget-object v3, p0, Lcom/google/android/music/sync/google/model/RadioSeed;->mArtistId:Ljava/lang/String;

    if-eqz v3, :cond_3

    move v3, v1

    :goto_1
    or-int/2addr v0, v3

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/sync/google/model/RadioSeed;->mGenreId:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/sync/google/model/RadioSeed;->mPlaylistShareToken:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/sync/google/model/RadioSeed;->mCuratedStationId:Ljava/lang/String;

    if-eqz v0, :cond_1

    :cond_0
    move v2, v1

    :cond_1
    return v2

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    move v3, v2

    goto :goto_1
.end method
