.class Lcom/google/android/music/playback/LocalDevicePlayback$22;
.super Ljava/lang/Object;
.source "LocalDevicePlayback.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/playback/LocalDevicePlayback;->open(Lcom/google/android/music/medialist/SongList;ZIZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field shouldClearQueue:Z

.field final synthetic this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

.field final synthetic val$medialist:Lcom/google/android/music/medialist/SongList;

.field final synthetic val$play:Z

.field final synthetic val$position:I

.field final synthetic val$shuffleList:Z


# direct methods
.method constructor <init>(Lcom/google/android/music/playback/LocalDevicePlayback;Lcom/google/android/music/medialist/SongList;IZZ)V
    .locals 1

    .prologue
    .line 2449
    iput-object p1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$22;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    iput-object p2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$22;->val$medialist:Lcom/google/android/music/medialist/SongList;

    iput p3, p0, Lcom/google/android/music/playback/LocalDevicePlayback$22;->val$position:I

    iput-boolean p4, p0, Lcom/google/android/music/playback/LocalDevicePlayback$22;->val$shuffleList:Z

    iput-boolean p5, p0, Lcom/google/android/music/playback/LocalDevicePlayback$22;->val$play:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2451
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$22;->shouldClearQueue:Z

    return-void
.end method


# virtual methods
.method public run()V
    .locals 15

    .prologue
    const/4 v10, 0x1

    const/4 v0, 0x0

    .line 2458
    :try_start_0
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$22;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaList:Lcom/google/android/music/medialist/SongList;
    invoke-static {v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$1800(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/medialist/SongList;

    move-result-object v6

    .line 2459
    .local v6, "currentList":Lcom/google/android/music/medialist/SongList;
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$22;->val$medialist:Lcom/google/android/music/medialist/SongList;

    invoke-virtual {v1, v6}, Lcom/google/android/music/medialist/SongList;->equals(Ljava/lang/Object;)Z

    move-result v14

    .line 2460
    .local v14, "sameList":Z
    if-eqz v14, :cond_0

    iget v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$22;->val$position:I

    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$22;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayPos:I
    invoke-static {v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$2800(Lcom/google/android/music/playback/LocalDevicePlayback;)I

    move-result v2

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$22;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    invoke-virtual {v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->isPlaying()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v1

    if-eqz v1, :cond_0

    .line 2554
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$22;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mAsyncWakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$1400(Lcom/google/android/music/playback/LocalDevicePlayback;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 2556
    :goto_0
    return-void

    .line 2464
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$22;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->isPlayQueue(Lcom/google/android/music/medialist/SongList;)Z
    invoke-static {v1, v6}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$6700(Lcom/google/android/music/playback/LocalDevicePlayback;Lcom/google/android/music/medialist/SongList;)Z

    move-result v7

    .line 2465
    .local v7, "currentlyPlayingFromPlayQ":Z
    if-nez v14, :cond_1

    if-eqz v7, :cond_1

    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$22;->val$medialist:Lcom/google/android/music/medialist/SongList;

    invoke-virtual {v1}, Lcom/google/android/music/medialist/SongList;->isDefaultDomain()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2468
    const/4 v14, 0x1

    .line 2471
    :cond_1
    if-nez v14, :cond_d

    .line 2472
    .local v10, "listChanged":Z
    :goto_1
    if-eqz v10, :cond_2

    .line 2473
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$22;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->clearMediaList()V
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$6800(Lcom/google/android/music/playback/LocalDevicePlayback;)V

    .line 2479
    :cond_2
    iget-object v11, p0, Lcom/google/android/music/playback/LocalDevicePlayback$22;->val$medialist:Lcom/google/android/music/medialist/SongList;

    .line 2480
    .local v11, "listToPlay":Lcom/google/android/music/medialist/SongList;
    iget v12, p0, Lcom/google/android/music/playback/LocalDevicePlayback$22;->val$position:I

    .line 2482
    .local v12, "positionToPlay":I
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$22;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$22;->val$medialist:Lcom/google/android/music/medialist/SongList;

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->isSuggestedMix(Lcom/google/android/music/medialist/SongList;)Z
    invoke-static {v0, v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$6900(Lcom/google/android/music/playback/LocalDevicePlayback;Lcom/google/android/music/medialist/SongList;)Z

    move-result v9

    .line 2483
    .local v9, "isSuggestedMix":Z
    if-eqz v9, :cond_3

    .line 2484
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$22;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$22;->val$medialist:Lcom/google/android/music/medialist/SongList;

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->enableInfiniteMixModeForSuggestedMix(Lcom/google/android/music/medialist/SongList;)V
    invoke-static {v0, v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$7000(Lcom/google/android/music/playback/LocalDevicePlayback;Lcom/google/android/music/medialist/SongList;)V

    .line 2485
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$22;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->updateLastTimeSuggestedMixPlayed()V
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$7100(Lcom/google/android/music/playback/LocalDevicePlayback;)V

    .line 2487
    :cond_3
    if-nez v9, :cond_4

    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$22;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$22;->val$medialist:Lcom/google/android/music/medialist/SongList;

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->isPlayQueue(Lcom/google/android/music/medialist/SongList;)Z
    invoke-static {v0, v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$6700(Lcom/google/android/music/playback/LocalDevicePlayback;Lcom/google/android/music/medialist/SongList;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$22;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$22;->val$medialist:Lcom/google/android/music/medialist/SongList;

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->isRadio(Lcom/google/android/music/medialist/SongList;)Z
    invoke-static {v0, v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$7200(Lcom/google/android/music/playback/LocalDevicePlayback;Lcom/google/android/music/medialist/SongList;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 2488
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$22;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->disableInfiniteMixMode()V
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$7300(Lcom/google/android/music/playback/LocalDevicePlayback;)V

    .line 2491
    :cond_4
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$22;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->LOGV:Z
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$000(Lcom/google/android/music/playback/LocalDevicePlayback;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2492
    const-string v0, "LocalDevicePlayback"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mMixGenerationState="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$22;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mMixGenerationState:Lcom/google/android/music/mix/MixGenerationState;
    invoke-static {v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$1500(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/mix/MixGenerationState;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2495
    :cond_5
    const/4 v5, 0x0

    .line 2496
    .local v5, "audioIdWhenRadioRequested":Lcom/google/android/music/download/ContentIdentifier;
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$22;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mMixGenerationState:Lcom/google/android/music/mix/MixGenerationState;
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$1500(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/mix/MixGenerationState;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 2497
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$22;->shouldClearQueue:Z

    .line 2500
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$22;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mSavedAudioId:Lcom/google/android/music/download/ContentIdentifier;
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$7400(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/download/ContentIdentifier;

    move-result-object v5

    .line 2503
    :cond_6
    const/4 v8, 0x0

    .line 2504
    .local v8, "isQueueChanged":Z
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$22;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$22;->val$medialist:Lcom/google/android/music/medialist/SongList;

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->isPlayQueue(Lcom/google/android/music/medialist/SongList;)Z
    invoke-static {v0, v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$6700(Lcom/google/android/music/playback/LocalDevicePlayback;Lcom/google/android/music/medialist/SongList;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 2505
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$22;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$22;->val$medialist:Lcom/google/android/music/medialist/SongList;

    iget-boolean v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$22;->val$shuffleList:Z

    iget v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback$22;->val$position:I

    iget-boolean v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback$22;->shouldClearQueue:Z

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->caqPlay(Lcom/google/android/music/medialist/SongList;ZIZ)Lcom/google/android/music/store/PlayQueueAddResult;
    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$7500(Lcom/google/android/music/playback/LocalDevicePlayback;Lcom/google/android/music/medialist/SongList;ZIZ)Lcom/google/android/music/store/PlayQueueAddResult;

    move-result-object v13

    .line 2507
    .local v13, "result":Lcom/google/android/music/store/PlayQueueAddResult;
    if-eqz v13, :cond_7

    invoke-virtual {v13}, Lcom/google/android/music/store/PlayQueueAddResult;->getAddedSize()I

    move-result v0

    if-lez v0, :cond_7

    .line 2508
    const/4 v8, 0x1

    .line 2509
    if-eqz v7, :cond_e

    .line 2510
    move-object v11, v6

    .line 2514
    :goto_2
    invoke-virtual {v13}, Lcom/google/android/music/store/PlayQueueAddResult;->getPlayPosition()I

    move-result v12

    .line 2519
    .end local v13    # "result":Lcom/google/android/music/store/PlayQueueAddResult;
    :cond_7
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$22;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->setMediaList(Lcom/google/android/music/medialist/SongList;)V
    invoke-static {v0, v11}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$7700(Lcom/google/android/music/playback/LocalDevicePlayback;Lcom/google/android/music/medialist/SongList;)V

    .line 2520
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$22;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayList:Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$2900(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;

    move-result-object v0

    invoke-virtual {v0, v11}, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->setMediaList(Lcom/google/android/music/medialist/SongList;)V

    .line 2521
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$22;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # setter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayPos:I
    invoke-static {v0, v12}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$2802(Lcom/google/android/music/playback/LocalDevicePlayback;I)I

    .line 2522
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$22;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mOpenFailedCounter:I
    invoke-static {v0, v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$7802(Lcom/google/android/music/playback/LocalDevicePlayback;I)I

    .line 2523
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$22;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    const-wide/16 v2, -0x1

    # setter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mReloadedQueueSeekPos:J
    invoke-static {v0, v2, v3}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$7902(Lcom/google/android/music/playback/LocalDevicePlayback;J)J

    .line 2525
    if-eqz v8, :cond_8

    .line 2526
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$22;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    invoke-virtual {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/music/store/QueueUtils;->notifyChange(Landroid/content/Context;)V

    .line 2529
    :cond_8
    if-eqz v9, :cond_9

    .line 2530
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$22;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->requestCreateRadioStationSync()V
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$8000(Lcom/google/android/music/playback/LocalDevicePlayback;)V

    .line 2532
    :cond_9
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$22;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    sget-object v1, Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;->FEED:Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->feedQueueIfNeeded(Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;)Z
    invoke-static {v0, v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$4000(Lcom/google/android/music/playback/LocalDevicePlayback;Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;)Z

    .line 2534
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$22;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mFuture:Ljava/util/LinkedList;
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$8100(Lcom/google/android/music/playback/LocalDevicePlayback;)Ljava/util/LinkedList;

    move-result-object v1

    monitor-enter v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2535
    :try_start_2
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$22;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mHistory:Ljava/util/LinkedList;
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$8200(Lcom/google/android/music/playback/LocalDevicePlayback;)Ljava/util/LinkedList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 2536
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$22;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mFuture:Ljava/util/LinkedList;
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$8100(Lcom/google/android/music/playback/LocalDevicePlayback;)Ljava/util/LinkedList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 2537
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$22;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->shouldPlayInRandomOrder()Z
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$8300(Lcom/google/android/music/playback/LocalDevicePlayback;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 2538
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$22;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mRand:Lcom/google/android/music/StrictShuffler;
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$8400(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/StrictShuffler;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$22;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayList:Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;
    invoke-static {v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$2900(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/music/playback/LocalDevicePlayback$MediaListWrapper;->length()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/android/music/StrictShuffler;->setHistorySize(I)V

    .line 2539
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$22;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayPos:I
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$2800(Lcom/google/android/music/playback/LocalDevicePlayback;)I

    move-result v0

    if-ltz v0, :cond_a

    .line 2540
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$22;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mRand:Lcom/google/android/music/StrictShuffler;
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$8400(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/StrictShuffler;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$22;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayPos:I
    invoke-static {v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$2800(Lcom/google/android/music/playback/LocalDevicePlayback;)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/android/music/StrictShuffler;->injectHistoricalValue(I)V

    .line 2542
    :cond_a
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$22;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->fillShuffleList()V
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$8500(Lcom/google/android/music/playback/LocalDevicePlayback;)V

    .line 2543
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$22;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->dumpPastPresentAndFuture()V
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$8600(Lcom/google/android/music/playback/LocalDevicePlayback;)V

    .line 2545
    :cond_b
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2547
    :try_start_3
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$22;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->saveBookmarkIfNeeded()V
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$8700(Lcom/google/android/music/playback/LocalDevicePlayback;)V

    .line 2548
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$22;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-boolean v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback$22;->val$play:Z

    const/4 v4, 0x0

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->openCurrentAndPrepareToPlaySync(ZZZZLcom/google/android/music/download/ContentIdentifier;)V
    invoke-static/range {v0 .. v5}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$8800(Lcom/google/android/music/playback/LocalDevicePlayback;ZZZZLcom/google/android/music/download/ContentIdentifier;)V

    .line 2550
    if-eqz v10, :cond_c

    .line 2551
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$22;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->onQueueChanged()V
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$1600(Lcom/google/android/music/playback/LocalDevicePlayback;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 2554
    :cond_c
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$22;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mAsyncWakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$1400(Lcom/google/android/music/playback/LocalDevicePlayback;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    goto/16 :goto_0

    .end local v5    # "audioIdWhenRadioRequested":Lcom/google/android/music/download/ContentIdentifier;
    .end local v8    # "isQueueChanged":Z
    .end local v9    # "isSuggestedMix":Z
    .end local v10    # "listChanged":Z
    .end local v11    # "listToPlay":Lcom/google/android/music/medialist/SongList;
    .end local v12    # "positionToPlay":I
    :cond_d
    move v10, v0

    .line 2471
    goto/16 :goto_1

    .line 2512
    .restart local v5    # "audioIdWhenRadioRequested":Lcom/google/android/music/download/ContentIdentifier;
    .restart local v8    # "isQueueChanged":Z
    .restart local v9    # "isSuggestedMix":Z
    .restart local v10    # "listChanged":Z
    .restart local v11    # "listToPlay":Lcom/google/android/music/medialist/SongList;
    .restart local v12    # "positionToPlay":I
    .restart local v13    # "result":Lcom/google/android/music/store/PlayQueueAddResult;
    :cond_e
    :try_start_4
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$22;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->createPlayQueueSongList()Lcom/google/android/music/medialist/SongList;
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$7600(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/medialist/SongList;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result-object v11

    goto/16 :goto_2

    .line 2545
    .end local v13    # "result":Lcom/google/android/music/store/PlayQueueAddResult;
    :catchall_0
    move-exception v0

    :try_start_5
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2554
    .end local v5    # "audioIdWhenRadioRequested":Lcom/google/android/music/download/ContentIdentifier;
    .end local v6    # "currentList":Lcom/google/android/music/medialist/SongList;
    .end local v7    # "currentlyPlayingFromPlayQ":Z
    .end local v8    # "isQueueChanged":Z
    .end local v9    # "isSuggestedMix":Z
    .end local v10    # "listChanged":Z
    .end local v11    # "listToPlay":Lcom/google/android/music/medialist/SongList;
    .end local v12    # "positionToPlay":I
    .end local v14    # "sameList":Z
    :catchall_1
    move-exception v0

    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$22;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mAsyncWakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$1400(Lcom/google/android/music/playback/LocalDevicePlayback;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    throw v0
.end method
