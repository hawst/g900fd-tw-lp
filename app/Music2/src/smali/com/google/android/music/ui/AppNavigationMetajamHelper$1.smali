.class final Lcom/google/android/music/ui/AppNavigationMetajamHelper$1;
.super Ljava/lang/Object;
.source "AppNavigationMetajamHelper.java"

# interfaces
.implements Lcom/google/android/music/utils/MusicUtils$QueryCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/AppNavigationMetajamHelper;->checkForTrackInLocker(Lcom/google/android/music/ui/BaseActivity;Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$activity:Lcom/google/android/music/ui/BaseActivity;

.field final synthetic val$callback:Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemCallback;

.field final synthetic val$openMetajamItemInfo:Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/BaseActivity;Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemCallback;)V
    .locals 0

    .prologue
    .line 176
    iput-object p1, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$1;->val$activity:Lcom/google/android/music/ui/BaseActivity;

    iput-object p2, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$1;->val$openMetajamItemInfo:Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;

    iput-object p3, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$1;->val$callback:Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemCallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onQueryComplete(Landroid/database/Cursor;)V
    .locals 10
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 181
    :try_start_0
    iget-object v1, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$1;->val$activity:Lcom/google/android/music/ui/BaseActivity;

    invoke-virtual {v1}, Lcom/google/android/music/ui/BaseActivity;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$1;->val$activity:Lcom/google/android/music/ui/BaseActivity;

    invoke-virtual {v1}, Lcom/google/android/music/ui/BaseActivity;->isActivityDestroyed()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_1

    .line 206
    :cond_0
    invoke-static {p1}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 208
    :goto_0
    return-void

    .line 184
    :cond_1
    if-eqz p1, :cond_4

    :try_start_1
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 185
    # getter for: Lcom/google/android/music/ui/AppNavigationMetajamHelper;->LOGV:Z
    invoke-static {}, Lcom/google/android/music/ui/AppNavigationMetajamHelper;->access$000()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 186
    const-string v1, "MusicNavigationHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Found track in locker: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 189
    :cond_2
    iget-object v1, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$1;->val$activity:Lcom/google/android/music/ui/BaseActivity;

    const/4 v2, 0x1

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    const/4 v8, 0x0

    invoke-static/range {v1 .. v8}, Lcom/google/android/music/ui/TrackContainerActivity;->showAlbum(Landroid/content/Context;JLcom/google/android/music/ui/cardlib/model/Document;ZJLandroid/view/View;)V

    .line 192
    iget-object v1, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$1;->val$openMetajamItemInfo:Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;

    invoke-virtual {v1}, Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;->isAutoPlay()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 193
    iget-object v1, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$1;->val$openMetajamItemInfo:Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;

    invoke-virtual {v1}, Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;->getMetajamId()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/music/store/ContainerDescriptor;->newSearchResultsDescriptor(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v0

    .line 197
    .local v0, "descriptor":Lcom/google/android/music/store/ContainerDescriptor;
    new-instance v9, Lcom/google/android/music/medialist/SingleSongList;

    const/4 v1, 0x3

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    const/4 v1, 0x0

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v9, v0, v2, v3, v1}, Lcom/google/android/music/medialist/SingleSongList;-><init>(Lcom/google/android/music/store/ContainerDescriptor;JLjava/lang/String;)V

    .line 199
    .local v9, "songList":Lcom/google/android/music/medialist/SongList;
    iget-object v1, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$1;->val$activity:Lcom/google/android/music/ui/BaseActivity;

    invoke-static {v1, v9}, Lcom/google/android/music/utils/MusicUtils;->playRadio(Landroid/content/Context;Lcom/google/android/music/medialist/SongList;)V

    .line 201
    .end local v0    # "descriptor":Lcom/google/android/music/store/ContainerDescriptor;
    .end local v9    # "songList":Lcom/google/android/music/medialist/SongList;
    :cond_3
    iget-object v1, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$1;->val$activity:Lcom/google/android/music/ui/BaseActivity;

    invoke-virtual {v1}, Lcom/google/android/music/ui/BaseActivity;->finish()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 206
    :goto_1
    invoke-static {p1}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto :goto_0

    .line 203
    :cond_4
    :try_start_2
    iget-object v1, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$1;->val$activity:Lcom/google/android/music/ui/BaseActivity;

    iget-object v2, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$1;->val$openMetajamItemInfo:Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;

    iget-object v3, p0, Lcom/google/android/music/ui/AppNavigationMetajamHelper$1;->val$callback:Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemCallback;

    # invokes: Lcom/google/android/music/ui/AppNavigationMetajamHelper;->openMetajamItemFromNautilus(Lcom/google/android/music/ui/BaseActivity;Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemCallback;)V
    invoke-static {v1, v2, v3}, Lcom/google/android/music/ui/AppNavigationMetajamHelper;->access$100(Lcom/google/android/music/ui/BaseActivity;Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemInfo;Lcom/google/android/music/ui/AppNavigationMetajamHelper$OpenMetajamItemCallback;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 206
    :catchall_0
    move-exception v1

    invoke-static {p1}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v1
.end method
