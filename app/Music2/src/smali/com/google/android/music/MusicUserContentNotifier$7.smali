.class Lcom/google/android/music/MusicUserContentNotifier$7;
.super Ljava/lang/Object;
.source "MusicUserContentNotifier.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/MusicUserContentNotifier;->registerPromoteNautilusListeners()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/MusicUserContentNotifier;


# direct methods
.method constructor <init>(Lcom/google/android/music/MusicUserContentNotifier;)V
    .locals 0

    .prologue
    .line 179
    iput-object p1, p0, Lcom/google/android/music/MusicUserContentNotifier$7;->this$0:Lcom/google/android/music/MusicUserContentNotifier;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 183
    iget-object v2, p0, Lcom/google/android/music/MusicUserContentNotifier$7;->this$0:Lcom/google/android/music/MusicUserContentNotifier;

    # getter for: Lcom/google/android/music/MusicUserContentNotifier;->mPreferenceListenerRegistered:Z
    invoke-static {v2}, Lcom/google/android/music/MusicUserContentNotifier;->access$100(Lcom/google/android/music/MusicUserContentNotifier;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 184
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "mPreferenceChangeListener is already registered"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 188
    :cond_0
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    .line 189
    .local v1, "refObject":Ljava/lang/Object;
    iget-object v2, p0, Lcom/google/android/music/MusicUserContentNotifier$7;->this$0:Lcom/google/android/music/MusicUserContentNotifier;

    # getter for: Lcom/google/android/music/MusicUserContentNotifier;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/google/android/music/MusicUserContentNotifier;->access$200(Lcom/google/android/music/MusicUserContentNotifier;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    .line 191
    .local v0, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    :try_start_0
    iget-object v2, p0, Lcom/google/android/music/MusicUserContentNotifier$7;->this$0:Lcom/google/android/music/MusicUserContentNotifier;

    # getter for: Lcom/google/android/music/MusicUserContentNotifier;->mPreferenceChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;
    invoke-static {v2}, Lcom/google/android/music/MusicUserContentNotifier;->access$300(Lcom/google/android/music/MusicUserContentNotifier;)Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/music/preferences/MusicPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 192
    iget-object v2, p0, Lcom/google/android/music/MusicUserContentNotifier$7;->this$0:Lcom/google/android/music/MusicUserContentNotifier;

    const/4 v3, 0x1

    # setter for: Lcom/google/android/music/MusicUserContentNotifier;->mPreferenceListenerRegistered:Z
    invoke-static {v2, v3}, Lcom/google/android/music/MusicUserContentNotifier;->access$102(Lcom/google/android/music/MusicUserContentNotifier;Z)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 194
    invoke-static {v1}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    .line 197
    iget-object v2, p0, Lcom/google/android/music/MusicUserContentNotifier$7;->this$0:Lcom/google/android/music/MusicUserContentNotifier;

    # getter for: Lcom/google/android/music/MusicUserContentNotifier;->mConfigListenerRegistered:Z
    invoke-static {v2}, Lcom/google/android/music/MusicUserContentNotifier;->access$400(Lcom/google/android/music/MusicUserContentNotifier;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 198
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "mConfigObserver is already registered"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 194
    :catchall_0
    move-exception v2

    invoke-static {v1}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    throw v2

    .line 201
    :cond_1
    iget-object v2, p0, Lcom/google/android/music/MusicUserContentNotifier$7;->this$0:Lcom/google/android/music/MusicUserContentNotifier;

    # getter for: Lcom/google/android/music/MusicUserContentNotifier;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/google/android/music/MusicUserContentNotifier;->access$200(Lcom/google/android/music/MusicUserContentNotifier;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/google/android/music/store/ConfigContent;->SERVER_SETTINGS_URI:Landroid/net/Uri;

    iget-object v4, p0, Lcom/google/android/music/MusicUserContentNotifier$7;->this$0:Lcom/google/android/music/MusicUserContentNotifier;

    # getter for: Lcom/google/android/music/MusicUserContentNotifier;->mConfigObserver:Landroid/database/ContentObserver;
    invoke-static {v4}, Lcom/google/android/music/MusicUserContentNotifier;->access$500(Lcom/google/android/music/MusicUserContentNotifier;)Landroid/database/ContentObserver;

    move-result-object v4

    invoke-virtual {v2, v3, v5, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 203
    iget-object v2, p0, Lcom/google/android/music/MusicUserContentNotifier$7;->this$0:Lcom/google/android/music/MusicUserContentNotifier;

    # setter for: Lcom/google/android/music/MusicUserContentNotifier;->mConfigListenerRegistered:Z
    invoke-static {v2, v5}, Lcom/google/android/music/MusicUserContentNotifier;->access$402(Lcom/google/android/music/MusicUserContentNotifier;Z)Z

    .line 204
    return-void
.end method
