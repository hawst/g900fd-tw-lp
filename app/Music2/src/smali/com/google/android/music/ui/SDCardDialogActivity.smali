.class public Lcom/google/android/music/ui/SDCardDialogActivity;
.super Landroid/support/v4/app/FragmentActivity;
.source "SDCardDialogActivity.java"


# static fields
.field public static final DIALOG_PIN_FAILURE:Ljava/lang/String;

.field public static final DIALOG_SELECTION:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 16
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/google/android/music/ui/SDCardDialogActivity;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".SelectionDialog"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/ui/SDCardDialogActivity;->DIALOG_SELECTION:Ljava/lang/String;

    .line 19
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/google/android/music/ui/SDCardDialogActivity;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".PinFailureDialog"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/ui/SDCardDialogActivity;->DIALOG_PIN_FAILURE:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Landroid/support/v4/app/FragmentActivity;-><init>()V

    return-void
.end method

.method public static showPinFailureDialog(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 55
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/music/ui/SDCardDialogActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 56
    .local v0, "i":Landroid/content/Intent;
    sget-object v1, Lcom/google/android/music/ui/SDCardDialogActivity;->DIALOG_PIN_FAILURE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 58
    const/high16 v1, 0x10800000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 60
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 61
    return-void
.end method

.method public static showSelectionDialog(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 49
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/music/ui/SDCardDialogActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 50
    .local v0, "i":Landroid/content/Intent;
    sget-object v1, Lcom/google/android/music/ui/SDCardDialogActivity;->DIALOG_SELECTION:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 51
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 52
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedState"    # Landroid/os/Bundle;

    .prologue
    .line 24
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 25
    invoke-virtual {p0}, Lcom/google/android/music/ui/SDCardDialogActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "music_enable_secondary_sdcards"

    const/4 v6, 0x1

    invoke-static {v4, v5, v6}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v1

    .line 28
    .local v1, "enabled":Z
    if-nez v1, :cond_0

    .line 29
    invoke-virtual {p0}, Lcom/google/android/music/ui/SDCardDialogActivity;->finish()V

    .line 46
    :goto_0
    return-void

    .line 32
    :cond_0
    const/4 v3, 0x0

    .line 33
    .local v3, "fragment":Landroid/support/v4/app/DialogFragment;
    invoke-virtual {p0}, Lcom/google/android/music/ui/SDCardDialogActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 34
    .local v0, "action":Ljava/lang/String;
    sget-object v4, Lcom/google/android/music/ui/SDCardDialogActivity;->DIALOG_SELECTION:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 35
    new-instance v3, Lcom/google/android/music/ui/SDCardSelectorFragment;

    .end local v3    # "fragment":Landroid/support/v4/app/DialogFragment;
    invoke-direct {v3}, Lcom/google/android/music/ui/SDCardSelectorFragment;-><init>()V

    .line 39
    .restart local v3    # "fragment":Landroid/support/v4/app/DialogFragment;
    :cond_1
    :goto_1
    if-eqz v3, :cond_3

    .line 41
    invoke-virtual {p0}, Lcom/google/android/music/ui/SDCardDialogActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    .line 42
    .local v2, "fm":Landroid/support/v4/app/FragmentManager;
    const/4 v4, 0x0

    invoke-virtual {v3, v2, v4}, Landroid/support/v4/app/DialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 36
    .end local v2    # "fm":Landroid/support/v4/app/FragmentManager;
    :cond_2
    sget-object v4, Lcom/google/android/music/ui/SDCardDialogActivity;->DIALOG_PIN_FAILURE:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 37
    new-instance v3, Lcom/google/android/music/ui/SDCardPinNotAllowedDialogFragment;

    .end local v3    # "fragment":Landroid/support/v4/app/DialogFragment;
    invoke-direct {v3}, Lcom/google/android/music/ui/SDCardPinNotAllowedDialogFragment;-><init>()V

    .restart local v3    # "fragment":Landroid/support/v4/app/DialogFragment;
    goto :goto_1

    .line 44
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/music/ui/SDCardDialogActivity;->finish()V

    goto :goto_0
.end method
