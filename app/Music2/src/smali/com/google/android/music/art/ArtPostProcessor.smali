.class public interface abstract Lcom/google/android/music/art/ArtPostProcessor;
.super Ljava/lang/Object;
.source "ArtPostProcessor.java"


# virtual methods
.method public abstract aliasPostProcess(Lcom/google/android/music/art/ArtRequest2;[Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;)Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;
.end method

.method public abstract getConfig(Lcom/google/android/music/art/ArtDescriptor;)Landroid/graphics/Bitmap$Config;
.end method

.method public abstract getMaxNeededImageCount(Lcom/google/android/music/art/ArtDescriptor;)I
.end method

.method public abstract getMinNeededImageCount(Lcom/google/android/music/art/ArtDescriptor;)I
.end method

.method public abstract getPostProcessingMode(Lcom/google/android/music/art/ArtRequest2;[Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;)I
.end method

.method public abstract renderPostProcess(Lcom/google/android/music/art/ArtRequest2;[Lcom/google/android/music/art/ArtResolver2$ManagedBitmap;Landroid/graphics/Bitmap;)Z
.end method
