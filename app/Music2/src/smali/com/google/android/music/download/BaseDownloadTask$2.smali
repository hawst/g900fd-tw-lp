.class Lcom/google/android/music/download/BaseDownloadTask$2;
.super Lcom/google/android/music/net/IDownloadabilityChangeListener$Stub;
.source "BaseDownloadTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/download/BaseDownloadTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/download/BaseDownloadTask;


# direct methods
.method constructor <init>(Lcom/google/android/music/download/BaseDownloadTask;)V
    .locals 0

    .prologue
    .line 369
    .local p0, "this":Lcom/google/android/music/download/BaseDownloadTask$2;, "Lcom/google/android/music/download/BaseDownloadTask.2;"
    iput-object p1, p0, Lcom/google/android/music/download/BaseDownloadTask$2;->this$0:Lcom/google/android/music/download/BaseDownloadTask;

    invoke-direct {p0}, Lcom/google/android/music/net/IDownloadabilityChangeListener$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onDownloadabilityChanged(Z)V
    .locals 4
    .param p1, "isDownloadable"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 373
    .local p0, "this":Lcom/google/android/music/download/BaseDownloadTask$2;, "Lcom/google/android/music/download/BaseDownloadTask.2;"
    iget-object v0, p0, Lcom/google/android/music/download/BaseDownloadTask$2;->this$0:Lcom/google/android/music/download/BaseDownloadTask;

    # getter for: Lcom/google/android/music/download/BaseDownloadTask;->mDownloadingEnabled:Ljava/util/concurrent/atomic/AtomicBoolean;
    invoke-static {v0}, Lcom/google/android/music/download/BaseDownloadTask;->access$300(Lcom/google/android/music/download/BaseDownloadTask;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eq v0, p1, :cond_0

    .line 374
    iget-object v0, p0, Lcom/google/android/music/download/BaseDownloadTask$2;->this$0:Lcom/google/android/music/download/BaseDownloadTask;

    # getter for: Lcom/google/android/music/download/BaseDownloadTask;->mLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/google/android/music/download/BaseDownloadTask;->access$100(Lcom/google/android/music/download/BaseDownloadTask;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 375
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/download/BaseDownloadTask$2;->this$0:Lcom/google/android/music/download/BaseDownloadTask;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    # setter for: Lcom/google/android/music/download/BaseDownloadTask;->mDownloadingEnabledChangedTime:J
    invoke-static {v0, v2, v3}, Lcom/google/android/music/download/BaseDownloadTask;->access$402(Lcom/google/android/music/download/BaseDownloadTask;J)J

    .line 376
    iget-object v0, p0, Lcom/google/android/music/download/BaseDownloadTask$2;->this$0:Lcom/google/android/music/download/BaseDownloadTask;

    # getter for: Lcom/google/android/music/download/BaseDownloadTask;->mDownloadingEnabled:Ljava/util/concurrent/atomic/AtomicBoolean;
    invoke-static {v0}, Lcom/google/android/music/download/BaseDownloadTask;->access$300(Lcom/google/android/music/download/BaseDownloadTask;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 377
    iget-object v0, p0, Lcom/google/android/music/download/BaseDownloadTask$2;->this$0:Lcom/google/android/music/download/BaseDownloadTask;

    # getter for: Lcom/google/android/music/download/BaseDownloadTask;->mLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/google/android/music/download/BaseDownloadTask;->access$100(Lcom/google/android/music/download/BaseDownloadTask;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 378
    monitor-exit v1

    .line 380
    :cond_0
    return-void

    .line 378
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
