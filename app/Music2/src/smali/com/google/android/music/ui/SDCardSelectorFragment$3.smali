.class Lcom/google/android/music/ui/SDCardSelectorFragment$3;
.super Landroid/widget/BaseAdapter;
.source "SDCardSelectorFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/SDCardSelectorFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/SDCardSelectorFragment;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/SDCardSelectorFragment;)V
    .locals 0

    .prologue
    .line 179
    iput-object p1, p0, Lcom/google/android/music/ui/SDCardSelectorFragment$3;->this$0:Lcom/google/android/music/ui/SDCardSelectorFragment;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 2

    .prologue
    .line 182
    iget-object v1, p0, Lcom/google/android/music/ui/SDCardSelectorFragment$3;->this$0:Lcom/google/android/music/ui/SDCardSelectorFragment;

    # getter for: Lcom/google/android/music/ui/SDCardSelectorFragment;->mLocationList:Ljava/util/List;
    invoke-static {v1}, Lcom/google/android/music/ui/SDCardSelectorFragment;->access$200(Lcom/google/android/music/ui/SDCardSelectorFragment;)Ljava/util/List;

    move-result-object v1

    if-nez v1, :cond_1

    const/4 v0, 0x0

    .line 189
    :cond_0
    :goto_0
    return v0

    .line 185
    :cond_1
    iget-object v1, p0, Lcom/google/android/music/ui/SDCardSelectorFragment$3;->this$0:Lcom/google/android/music/ui/SDCardSelectorFragment;

    # getter for: Lcom/google/android/music/ui/SDCardSelectorFragment;->mLocationList:Ljava/util/List;
    invoke-static {v1}, Lcom/google/android/music/ui/SDCardSelectorFragment;->access$200(Lcom/google/android/music/ui/SDCardSelectorFragment;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    .line 186
    .local v0, "count":I
    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    iget-object v1, p0, Lcom/google/android/music/ui/SDCardSelectorFragment$3;->this$0:Lcom/google/android/music/ui/SDCardSelectorFragment;

    # getter for: Lcom/google/android/music/ui/SDCardSelectorFragment;->mShowCardCapability:Z
    invoke-static {v1}, Lcom/google/android/music/ui/SDCardSelectorFragment;->access$400(Lcom/google/android/music/ui/SDCardSelectorFragment;)Z

    move-result v1

    if-nez v1, :cond_3

    :cond_2
    iget-object v1, p0, Lcom/google/android/music/ui/SDCardSelectorFragment$3;->this$0:Lcom/google/android/music/ui/SDCardSelectorFragment;

    # getter for: Lcom/google/android/music/ui/SDCardSelectorFragment;->mShowMissingCard:Z
    invoke-static {v1}, Lcom/google/android/music/ui/SDCardSelectorFragment;->access$500(Lcom/google/android/music/ui/SDCardSelectorFragment;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 187
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "i"    # I

    .prologue
    .line 194
    iget-object v0, p0, Lcom/google/android/music/ui/SDCardSelectorFragment$3;->this$0:Lcom/google/android/music/ui/SDCardSelectorFragment;

    # getter for: Lcom/google/android/music/ui/SDCardSelectorFragment;->mLocationList:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/music/ui/SDCardSelectorFragment;->access$200(Lcom/google/android/music/ui/SDCardSelectorFragment;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 195
    iget-object v0, p0, Lcom/google/android/music/ui/SDCardSelectorFragment$3;->this$0:Lcom/google/android/music/ui/SDCardSelectorFragment;

    # getter for: Lcom/google/android/music/ui/SDCardSelectorFragment;->mLocationList:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/music/ui/SDCardSelectorFragment;->access$200(Lcom/google/android/music/ui/SDCardSelectorFragment;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 197
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/music/ui/SDCardSelectorFragment$3;->this$0:Lcom/google/android/music/ui/SDCardSelectorFragment;

    # getter for: Lcom/google/android/music/ui/SDCardSelectorFragment;->mFakeRowObject:Ljava/lang/Object;
    invoke-static {v0}, Lcom/google/android/music/ui/SDCardSelectorFragment;->access$600(Lcom/google/android/music/ui/SDCardSelectorFragment;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "i"    # I

    .prologue
    .line 204
    invoke-virtual {p0, p1}, Lcom/google/android/music/ui/SDCardSelectorFragment$3;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .param p1, "i"    # I
    .param p2, "view"    # Landroid/view/View;
    .param p3, "viewGroup"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v6, 0x0

    .line 209
    if-nez p2, :cond_0

    .line 210
    iget-object v5, p0, Lcom/google/android/music/ui/SDCardSelectorFragment$3;->this$0:Lcom/google/android/music/ui/SDCardSelectorFragment;

    invoke-virtual {v5}, Lcom/google/android/music/ui/SDCardSelectorFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-static {v5}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 212
    .local v1, "inflater":Landroid/view/LayoutInflater;
    iget-object v5, p0, Lcom/google/android/music/ui/SDCardSelectorFragment$3;->this$0:Lcom/google/android/music/ui/SDCardSelectorFragment;

    # getter for: Lcom/google/android/music/ui/SDCardSelectorFragment;->mEnableDebugUi:Z
    invoke-static {v5}, Lcom/google/android/music/ui/SDCardSelectorFragment;->access$700(Lcom/google/android/music/ui/SDCardSelectorFragment;)Z

    move-result v5

    if-eqz v5, :cond_1

    # getter for: Lcom/google/android/music/ui/SDCardSelectorFragment;->LOGV:Z
    invoke-static {}, Lcom/google/android/music/ui/SDCardSelectorFragment;->access$800()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 213
    new-instance v0, Lcom/google/android/music/ui/SDCardSelectorFragment$DebugViewHolder;

    iget-object v5, p0, Lcom/google/android/music/ui/SDCardSelectorFragment$3;->this$0:Lcom/google/android/music/ui/SDCardSelectorFragment;

    invoke-direct {v0, v5, v6}, Lcom/google/android/music/ui/SDCardSelectorFragment$DebugViewHolder;-><init>(Lcom/google/android/music/ui/SDCardSelectorFragment;Lcom/google/android/music/ui/SDCardSelectorFragment$1;)V

    .line 217
    .local v0, "holder":Lcom/google/android/music/ui/SDCardSelectorFragment$ViewHolder;
    :goto_0
    invoke-virtual {v0, v1, p3}, Lcom/google/android/music/ui/SDCardSelectorFragment$ViewHolder;->createView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 219
    .end local v0    # "holder":Lcom/google/android/music/ui/SDCardSelectorFragment$ViewHolder;
    .end local v1    # "inflater":Landroid/view/LayoutInflater;
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/music/ui/SDCardSelectorFragment$ViewHolder;

    .line 220
    .local v4, "vh":Lcom/google/android/music/ui/SDCardSelectorFragment$ViewHolder;
    invoke-virtual {p0, p1}, Lcom/google/android/music/ui/SDCardSelectorFragment$3;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    .line 221
    .local v2, "item":Ljava/lang/Object;
    iget-object v5, p0, Lcom/google/android/music/ui/SDCardSelectorFragment$3;->this$0:Lcom/google/android/music/ui/SDCardSelectorFragment;

    # getter for: Lcom/google/android/music/ui/SDCardSelectorFragment;->mFakeRowObject:Ljava/lang/Object;
    invoke-static {v5}, Lcom/google/android/music/ui/SDCardSelectorFragment;->access$600(Lcom/google/android/music/ui/SDCardSelectorFragment;)Ljava/lang/Object;

    move-result-object v5

    if-ne v2, v5, :cond_2

    .line 222
    invoke-virtual {v4}, Lcom/google/android/music/ui/SDCardSelectorFragment$ViewHolder;->showFake()V

    .line 227
    :goto_1
    return-object p2

    .line 215
    .end local v2    # "item":Ljava/lang/Object;
    .end local v4    # "vh":Lcom/google/android/music/ui/SDCardSelectorFragment$ViewHolder;
    .restart local v1    # "inflater":Landroid/view/LayoutInflater;
    :cond_1
    new-instance v0, Lcom/google/android/music/ui/SDCardSelectorFragment$ViewHolder;

    iget-object v5, p0, Lcom/google/android/music/ui/SDCardSelectorFragment$3;->this$0:Lcom/google/android/music/ui/SDCardSelectorFragment;

    invoke-direct {v0, v5, v6}, Lcom/google/android/music/ui/SDCardSelectorFragment$ViewHolder;-><init>(Lcom/google/android/music/ui/SDCardSelectorFragment;Lcom/google/android/music/ui/SDCardSelectorFragment$1;)V

    .restart local v0    # "holder":Lcom/google/android/music/ui/SDCardSelectorFragment$ViewHolder;
    goto :goto_0

    .line 224
    .end local v0    # "holder":Lcom/google/android/music/ui/SDCardSelectorFragment$ViewHolder;
    .end local v1    # "inflater":Landroid/view/LayoutInflater;
    .restart local v2    # "item":Ljava/lang/Object;
    .restart local v4    # "vh":Lcom/google/android/music/ui/SDCardSelectorFragment$ViewHolder;
    :cond_2
    invoke-virtual {p0, p1}, Lcom/google/android/music/ui/SDCardSelectorFragment$3;->getItem(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/music/download/cache/CacheLocation;

    .line 225
    .local v3, "loc":Lcom/google/android/music/download/cache/CacheLocation;
    invoke-virtual {v4, v3}, Lcom/google/android/music/ui/SDCardSelectorFragment$ViewHolder;->update(Lcom/google/android/music/download/cache/CacheLocation;)V

    goto :goto_1
.end method
