.class Lcom/google/android/music/download/stream/StreamRequestHandler$InputStreamEntity;
.super Lorg/apache/http/entity/AbstractHttpEntity;
.source "StreamRequestHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/download/stream/StreamRequestHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "InputStreamEntity"
.end annotation


# instance fields
.field private mConsumed:Z

.field private final mDebugPrefix:Ljava/lang/String;

.field private mReadStream:Ljava/io/InputStream;


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/io/InputStream;)V
    .locals 1
    .param p1, "debugprefix"    # Ljava/lang/String;
    .param p2, "is"    # Ljava/io/InputStream;

    .prologue
    .line 161
    invoke-direct {p0}, Lorg/apache/http/entity/AbstractHttpEntity;-><init>()V

    .line 156
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/download/stream/StreamRequestHandler$InputStreamEntity;->mConsumed:Z

    .line 162
    iput-object p1, p0, Lcom/google/android/music/download/stream/StreamRequestHandler$InputStreamEntity;->mDebugPrefix:Ljava/lang/String;

    .line 163
    iput-object p2, p0, Lcom/google/android/music/download/stream/StreamRequestHandler$InputStreamEntity;->mReadStream:Ljava/io/InputStream;

    .line 164
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Ljava/io/InputStream;Lcom/google/android/music/download/stream/StreamRequestHandler$1;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/String;
    .param p2, "x1"    # Ljava/io/InputStream;
    .param p3, "x2"    # Lcom/google/android/music/download/stream/StreamRequestHandler$1;

    .prologue
    .line 153
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/download/stream/StreamRequestHandler$InputStreamEntity;-><init>(Ljava/lang/String;Ljava/io/InputStream;)V

    return-void
.end method


# virtual methods
.method public consumeContent()V
    .locals 1

    .prologue
    .line 190
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public getContent()Ljava/io/InputStream;
    .locals 1

    .prologue
    .line 194
    iget-object v0, p0, Lcom/google/android/music/download/stream/StreamRequestHandler$InputStreamEntity;->mReadStream:Ljava/io/InputStream;

    return-object v0
.end method

.method public getContentLength()J
    .locals 2

    .prologue
    .line 210
    const-wide/16 v0, -0x1

    return-wide v0
.end method

.method public isRepeatable()Z
    .locals 1

    .prologue
    .line 198
    const/4 v0, 0x0

    return v0
.end method

.method public isStreaming()Z
    .locals 1

    .prologue
    .line 202
    iget-boolean v0, p0, Lcom/google/android/music/download/stream/StreamRequestHandler$InputStreamEntity;->mConsumed:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public writeTo(Ljava/io/OutputStream;)V
    .locals 10
    .param p1, "outstream"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v6, 0x0

    .line 167
    const-wide/16 v4, 0x0

    .line 168
    .local v4, "totalRead":J
    const/4 v2, 0x0

    .line 170
    .local v2, "success":Z
    :try_start_0
    invoke-virtual {p1}, Ljava/io/OutputStream;->flush()V

    .line 173
    const/16 v7, 0x800

    new-array v0, v7, [B

    .line 175
    .local v0, "buff":[B
    :goto_0
    iget-object v7, p0, Lcom/google/android/music/download/stream/StreamRequestHandler$InputStreamEntity;->mReadStream:Ljava/io/InputStream;

    invoke-virtual {v7, v0}, Ljava/io/InputStream;->read([B)I

    move-result v1

    .local v1, "len":I
    if-ltz v1, :cond_0

    .line 176
    const/4 v7, 0x0

    invoke-virtual {p1, v0, v7, v1}, Ljava/io/OutputStream;->write([BII)V

    .line 177
    int-to-long v8, v1

    add-long/2addr v4, v8

    goto :goto_0

    .line 179
    :cond_0
    invoke-virtual {p1}, Ljava/io/OutputStream;->flush()V

    .line 180
    const-string v7, "StreamRequestHandler"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Finished writeTo("

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/music/download/stream/StreamRequestHandler$InputStreamEntity;->mDebugPrefix:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ") "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    const/4 v7, 0x1

    iput-boolean v7, p0, Lcom/google/android/music/download/stream/StreamRequestHandler$InputStreamEntity;->mConsumed:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 182
    const/4 v2, 0x1

    .line 184
    iget-object v7, p0, Lcom/google/android/music/download/stream/StreamRequestHandler$InputStreamEntity;->mReadStream:Ljava/io/InputStream;

    if-nez v2, :cond_1

    :goto_1
    invoke-static {v7, v3}, Lcom/google/common/io/Closeables;->close(Ljava/io/Closeable;Z)V

    .line 186
    return-void

    :cond_1
    move v3, v6

    .line 184
    goto :goto_1

    .end local v0    # "buff":[B
    .end local v1    # "len":I
    :catchall_0
    move-exception v7

    iget-object v8, p0, Lcom/google/android/music/download/stream/StreamRequestHandler$InputStreamEntity;->mReadStream:Ljava/io/InputStream;

    if-nez v2, :cond_2

    :goto_2
    invoke-static {v8, v3}, Lcom/google/common/io/Closeables;->close(Ljava/io/Closeable;Z)V

    throw v7

    :cond_2
    move v3, v6

    goto :goto_2
.end method
