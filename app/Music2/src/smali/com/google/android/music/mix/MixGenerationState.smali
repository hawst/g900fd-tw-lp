.class public Lcom/google/android/music/mix/MixGenerationState;
.super Ljava/lang/Object;
.source "MixGenerationState.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/mix/MixGenerationState$State;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/music/mix/MixGenerationState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mMix:Lcom/google/android/music/mix/MixDescriptor;

.field private mOriginalMix:Lcom/google/android/music/mix/MixDescriptor;

.field private mState:Lcom/google/android/music/mix/MixGenerationState$State;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 102
    new-instance v0, Lcom/google/android/music/mix/MixGenerationState$1;

    invoke-direct {v0}, Lcom/google/android/music/mix/MixGenerationState$1;-><init>()V

    sput-object v0, Lcom/google/android/music/mix/MixGenerationState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "p"    # Landroid/os/Parcel;

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    sget-object v0, Lcom/google/android/music/mix/MixGenerationState$State;->NOT_STARTED:Lcom/google/android/music/mix/MixGenerationState$State;

    iput-object v0, p0, Lcom/google/android/music/mix/MixGenerationState;->mState:Lcom/google/android/music/mix/MixGenerationState$State;

    .line 34
    const-class v0, Lcom/google/android/music/mix/MixDescriptor;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/mix/MixDescriptor;

    iput-object v0, p0, Lcom/google/android/music/mix/MixGenerationState;->mMix:Lcom/google/android/music/mix/MixDescriptor;

    .line 35
    const-class v0, Lcom/google/android/music/mix/MixDescriptor;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/mix/MixDescriptor;

    iput-object v0, p0, Lcom/google/android/music/mix/MixGenerationState;->mOriginalMix:Lcom/google/android/music/mix/MixDescriptor;

    .line 36
    invoke-static {}, Lcom/google/android/music/mix/MixGenerationState$State;->values()[Lcom/google/android/music/mix/MixGenerationState$State;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/google/android/music/mix/MixGenerationState;->mState:Lcom/google/android/music/mix/MixGenerationState$State;

    .line 37
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/google/android/music/mix/MixGenerationState$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/google/android/music/mix/MixGenerationState$1;

    .prologue
    .line 13
    invoke-direct {p0, p1}, Lcom/google/android/music/mix/MixGenerationState;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/music/mix/MixDescriptor;)V
    .locals 1
    .param p1, "mix"    # Lcom/google/android/music/mix/MixDescriptor;

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    sget-object v0, Lcom/google/android/music/mix/MixGenerationState$State;->NOT_STARTED:Lcom/google/android/music/mix/MixGenerationState$State;

    iput-object v0, p0, Lcom/google/android/music/mix/MixGenerationState;->mState:Lcom/google/android/music/mix/MixGenerationState$State;

    .line 30
    iput-object p1, p0, Lcom/google/android/music/mix/MixGenerationState;->mMix:Lcom/google/android/music/mix/MixDescriptor;

    .line 31
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 92
    const/4 v0, 0x0

    return v0
.end method

.method public declared-synchronized getMix()Lcom/google/android/music/mix/MixDescriptor;
    .locals 1

    .prologue
    .line 43
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/mix/MixGenerationState;->mMix:Lcom/google/android/music/mix/MixDescriptor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getState()Lcom/google/android/music/mix/MixGenerationState$State;
    .locals 1

    .prologue
    .line 58
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/mix/MixGenerationState;->mState:Lcom/google/android/music/mix/MixGenerationState$State;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized isMyMix(Lcom/google/android/music/mix/MixDescriptor;)Z
    .locals 1
    .param p1, "mix"    # Lcom/google/android/music/mix/MixDescriptor;

    .prologue
    .line 50
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/mix/MixGenerationState;->mMix:Lcom/google/android/music/mix/MixDescriptor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/mix/MixGenerationState;->mMix:Lcom/google/android/music/mix/MixDescriptor;

    invoke-virtual {v0, p1}, Lcom/google/android/music/mix/MixDescriptor;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/music/mix/MixGenerationState;->mOriginalMix:Lcom/google/android/music/mix/MixDescriptor;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/music/mix/MixGenerationState;->mOriginalMix:Lcom/google/android/music/mix/MixDescriptor;

    invoke-virtual {v0, p1}, Lcom/google/android/music/mix/MixDescriptor;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setState(Lcom/google/android/music/mix/MixGenerationState$State;)V
    .locals 1
    .param p1, "state"    # Lcom/google/android/music/mix/MixGenerationState$State;

    .prologue
    .line 65
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/android/music/mix/MixGenerationState;->mState:Lcom/google/android/music/mix/MixGenerationState$State;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 66
    monitor-exit p0

    return-void

    .line 65
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 79
    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 80
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 81
    iget-object v1, p0, Lcom/google/android/music/mix/MixGenerationState;->mMix:Lcom/google/android/music/mix/MixDescriptor;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 82
    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 83
    iget-object v1, p0, Lcom/google/android/music/mix/MixGenerationState;->mOriginalMix:Lcom/google/android/music/mix/MixDescriptor;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 84
    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 85
    iget-object v1, p0, Lcom/google/android/music/mix/MixGenerationState;->mState:Lcom/google/android/music/mix/MixGenerationState$State;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 86
    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 87
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    monitor-exit p0

    return-object v1

    .line 79
    .end local v0    # "sb":Ljava/lang/StringBuilder;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized transitionToNewMix(Lcom/google/android/music/mix/MixDescriptor;)V
    .locals 1
    .param p1, "newMix"    # Lcom/google/android/music/mix/MixDescriptor;

    .prologue
    .line 72
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/google/android/music/mix/MixGenerationState$State;->FINISHED:Lcom/google/android/music/mix/MixGenerationState$State;

    iput-object v0, p0, Lcom/google/android/music/mix/MixGenerationState;->mState:Lcom/google/android/music/mix/MixGenerationState$State;

    .line 73
    iget-object v0, p0, Lcom/google/android/music/mix/MixGenerationState;->mMix:Lcom/google/android/music/mix/MixDescriptor;

    iput-object v0, p0, Lcom/google/android/music/mix/MixGenerationState;->mOriginalMix:Lcom/google/android/music/mix/MixDescriptor;

    .line 74
    iput-object p1, p0, Lcom/google/android/music/mix/MixGenerationState;->mMix:Lcom/google/android/music/mix/MixDescriptor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 75
    monitor-exit p0

    return-void

    .line 72
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 97
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/mix/MixGenerationState;->mMix:Lcom/google/android/music/mix/MixDescriptor;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 98
    iget-object v0, p0, Lcom/google/android/music/mix/MixGenerationState;->mOriginalMix:Lcom/google/android/music/mix/MixDescriptor;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 99
    iget-object v0, p0, Lcom/google/android/music/mix/MixGenerationState;->mState:Lcom/google/android/music/mix/MixGenerationState$State;

    invoke-virtual {v0}, Lcom/google/android/music/mix/MixGenerationState$State;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 100
    monitor-exit p0

    return-void

    .line 97
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
