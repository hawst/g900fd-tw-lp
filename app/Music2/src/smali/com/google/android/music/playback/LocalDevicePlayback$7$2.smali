.class Lcom/google/android/music/playback/LocalDevicePlayback$7$2;
.super Ljava/lang/Object;
.source "LocalDevicePlayback.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/playback/LocalDevicePlayback$7;->onVolumeChanged(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/music/playback/LocalDevicePlayback$7;

.field final synthetic val$volume:I


# direct methods
.method constructor <init>(Lcom/google/android/music/playback/LocalDevicePlayback$7;I)V
    .locals 0

    .prologue
    .line 714
    iput-object p1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7$2;->this$1:Lcom/google/android/music/playback/LocalDevicePlayback$7;

    iput p2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7$2;->val$volume:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 720
    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7$2;->this$1:Lcom/google/android/music/playback/LocalDevicePlayback$7;

    iget-object v1, v1, Lcom/google/android/music/playback/LocalDevicePlayback$7;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    invoke-virtual {v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/support/v7/media/MediaRouter;->getInstance(Landroid/content/Context;)Landroid/support/v7/media/MediaRouter;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v7/media/MediaRouter;->getSelectedRoute()Landroid/support/v7/media/MediaRouter$RouteInfo;

    move-result-object v0

    .line 722
    .local v0, "selectedRoute":Landroid/support/v7/media/MediaRouter$RouteInfo;
    iget v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$7$2;->val$volume:I

    invoke-virtual {v0, v1}, Landroid/support/v7/media/MediaRouter$RouteInfo;->requestSetVolume(I)V

    .line 723
    return-void
.end method
