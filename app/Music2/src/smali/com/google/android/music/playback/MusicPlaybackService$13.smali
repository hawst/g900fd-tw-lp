.class Lcom/google/android/music/playback/MusicPlaybackService$13;
.super Ljava/lang/Object;
.source "MusicPlaybackService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/playback/MusicPlaybackService;->setMediaSessionQueue()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/playback/MusicPlaybackService;


# direct methods
.method constructor <init>(Lcom/google/android/music/playback/MusicPlaybackService;)V
    .locals 0

    .prologue
    .line 1859
    iput-object p1, p0, Lcom/google/android/music/playback/MusicPlaybackService$13;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 1862
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$13;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    # getter for: Lcom/google/android/music/playback/MusicPlaybackService;->mMediaSession:Lcom/google/android/music/playback/session/MediaSessionCompat;
    invoke-static {v0}, Lcom/google/android/music/playback/MusicPlaybackService;->access$1600(Lcom/google/android/music/playback/MusicPlaybackService;)Lcom/google/android/music/playback/session/MediaSessionCompat;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1863
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$13;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    iget-object v1, p0, Lcom/google/android/music/playback/MusicPlaybackService$13;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    iget-object v2, p0, Lcom/google/android/music/playback/MusicPlaybackService$13;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v2}, Lcom/google/android/music/playback/MusicPlaybackService;->isInCloudQueueMode()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/android/music/playback/MediaSessionUtil;->getCurrentPlayQueue(Landroid/content/Context;Z)Ljava/util/List;

    move-result-object v1

    # setter for: Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentQueue:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/android/music/playback/MusicPlaybackService;->access$1702(Lcom/google/android/music/playback/MusicPlaybackService;Ljava/util/List;)Ljava/util/List;

    .line 1865
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$13;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    # getter for: Lcom/google/android/music/playback/MusicPlaybackService;->mMediaSession:Lcom/google/android/music/playback/session/MediaSessionCompat;
    invoke-static {v0}, Lcom/google/android/music/playback/MusicPlaybackService;->access$1600(Lcom/google/android/music/playback/MusicPlaybackService;)Lcom/google/android/music/playback/session/MediaSessionCompat;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/playback/MusicPlaybackService$13;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    # getter for: Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentQueue:Ljava/util/List;
    invoke-static {v1}, Lcom/google/android/music/playback/MusicPlaybackService;->access$1700(Lcom/google/android/music/playback/MusicPlaybackService;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/music/playback/session/MediaSessionCompat;->setQueue(Ljava/util/List;)V

    .line 1867
    :cond_0
    return-void
.end method
