.class public abstract Lcom/google/android/music/download/BaseDownloadRequest;
.super Ljava/lang/Object;
.source "BaseDownloadRequest.java"

# interfaces
.implements Lcom/google/android/music/download/DownloadRequest;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ProgressType::",
        "Lcom/google/android/music/download/DownloadProgress;",
        "OwnerType::",
        "Lcom/google/android/music/download/DownloadRequest$Owner;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/music/download/DownloadRequest",
        "<TProgressType;TOwnerType;>;"
    }
.end annotation


# instance fields
.field private volatile mExplicit:Z

.field private final mFileLocation:Lcom/google/android/music/download/cache/FileLocation;

.field private final mId:Lcom/google/android/music/download/ContentIdentifier;

.field private final mOwner:Lcom/google/android/music/download/DownloadRequest$Owner;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TOwnerType;"
        }
    .end annotation
.end field

.field private volatile mPriority:I

.field private final mRetryAllowed:Z


# direct methods
.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "p"    # Landroid/os/Parcel;

    .prologue
    .local p0, "this":Lcom/google/android/music/download/BaseDownloadRequest;, "Lcom/google/android/music/download/BaseDownloadRequest<TProgressType;TOwnerType;>;"
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    const-class v0, Lcom/google/android/music/download/ContentIdentifier;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/download/ContentIdentifier;

    iput-object v0, p0, Lcom/google/android/music/download/BaseDownloadRequest;->mId:Lcom/google/android/music/download/ContentIdentifier;

    .line 55
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/music/download/BaseDownloadRequest;->mPriority:I

    .line 56
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/music/download/BaseDownloadRequest;->getOwnerFromInt(I)Lcom/google/android/music/download/DownloadRequest$Owner;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/download/BaseDownloadRequest;->mOwner:Lcom/google/android/music/download/DownloadRequest$Owner;

    .line 57
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/music/download/BaseDownloadRequest;->mRetryAllowed:Z

    .line 58
    const-class v0, Lcom/google/android/music/download/cache/FileLocation;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/download/cache/FileLocation;

    iput-object v0, p0, Lcom/google/android/music/download/BaseDownloadRequest;->mFileLocation:Lcom/google/android/music/download/cache/FileLocation;

    .line 59
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_1

    :goto_1
    iput-boolean v1, p0, Lcom/google/android/music/download/BaseDownloadRequest;->mExplicit:Z

    .line 60
    return-void

    :cond_0
    move v0, v2

    .line 57
    goto :goto_0

    :cond_1
    move v1, v2

    .line 59
    goto :goto_1
.end method

.method public constructor <init>(Lcom/google/android/music/download/ContentIdentifier;ILcom/google/android/music/download/DownloadRequest$Owner;ZLcom/google/android/music/download/cache/FileLocation;Z)V
    .locals 3
    .param p1, "id"    # Lcom/google/android/music/download/ContentIdentifier;
    .param p2, "priority"    # I
    .param p4, "retryAllowed"    # Z
    .param p5, "fileLocation"    # Lcom/google/android/music/download/cache/FileLocation;
    .param p6, "explicit"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/music/download/ContentIdentifier;",
            "ITOwnerType;Z",
            "Lcom/google/android/music/download/cache/FileLocation;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 36
    .local p0, "this":Lcom/google/android/music/download/BaseDownloadRequest;, "Lcom/google/android/music/download/BaseDownloadRequest<TProgressType;TOwnerType;>;"
    .local p3, "owner":Lcom/google/android/music/download/DownloadRequest$Owner;, "TOwnerType;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    invoke-virtual {p0}, Lcom/google/android/music/download/BaseDownloadRequest;->getMinPriority()I

    move-result v0

    if-gt p2, v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/music/download/BaseDownloadRequest;->getMaxPriority()I

    move-result v0

    if-ge p2, v0, :cond_1

    .line 39
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Priority out of range: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 41
    :cond_1
    if-nez p5, :cond_2

    .line 42
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Missing file location"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 45
    :cond_2
    iput-object p1, p0, Lcom/google/android/music/download/BaseDownloadRequest;->mId:Lcom/google/android/music/download/ContentIdentifier;

    .line 46
    iput p2, p0, Lcom/google/android/music/download/BaseDownloadRequest;->mPriority:I

    .line 47
    iput-object p3, p0, Lcom/google/android/music/download/BaseDownloadRequest;->mOwner:Lcom/google/android/music/download/DownloadRequest$Owner;

    .line 48
    iput-boolean p4, p0, Lcom/google/android/music/download/BaseDownloadRequest;->mRetryAllowed:Z

    .line 49
    iput-object p5, p0, Lcom/google/android/music/download/BaseDownloadRequest;->mFileLocation:Lcom/google/android/music/download/cache/FileLocation;

    .line 50
    iput-boolean p6, p0, Lcom/google/android/music/download/BaseDownloadRequest;->mExplicit:Z

    .line 51
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 190
    .local p0, "this":Lcom/google/android/music/download/BaseDownloadRequest;, "Lcom/google/android/music/download/BaseDownloadRequest<TProgressType;TOwnerType;>;"
    const/4 v0, 0x0

    return v0
.end method

.method protected doUpgrade(Lcom/google/android/music/download/BaseDownloadRequest;)V
    .locals 1
    .param p1, "newRequest"    # Lcom/google/android/music/download/BaseDownloadRequest;

    .prologue
    .line 108
    .local p0, "this":Lcom/google/android/music/download/BaseDownloadRequest;, "Lcom/google/android/music/download/BaseDownloadRequest<TProgressType;TOwnerType;>;"
    invoke-virtual {p1}, Lcom/google/android/music/download/BaseDownloadRequest;->getPriority()I

    move-result v0

    iput v0, p0, Lcom/google/android/music/download/BaseDownloadRequest;->mPriority:I

    .line 109
    invoke-virtual {p1}, Lcom/google/android/music/download/BaseDownloadRequest;->getExplicit()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/music/download/BaseDownloadRequest;->mExplicit:Z

    .line 110
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .local p0, "this":Lcom/google/android/music/download/BaseDownloadRequest;, "Lcom/google/android/music/download/BaseDownloadRequest<TProgressType;TOwnerType;>;"
    const/4 v1, 0x0

    .line 164
    instance-of v2, p1, Lcom/google/android/music/download/BaseDownloadRequest;

    if-nez v2, :cond_1

    .line 169
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 168
    check-cast v0, Lcom/google/android/music/download/BaseDownloadRequest;

    .line 169
    .local v0, "downloadRequest":Lcom/google/android/music/download/BaseDownloadRequest;
    iget-object v2, p0, Lcom/google/android/music/download/BaseDownloadRequest;->mOwner:Lcom/google/android/music/download/DownloadRequest$Owner;

    invoke-virtual {v0}, Lcom/google/android/music/download/BaseDownloadRequest;->getOwner()Lcom/google/android/music/download/DownloadRequest$Owner;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/music/download/BaseDownloadRequest;->mId:Lcom/google/android/music/download/ContentIdentifier;

    invoke-virtual {v0}, Lcom/google/android/music/download/BaseDownloadRequest;->getId()Lcom/google/android/music/download/ContentIdentifier;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/music/download/ContentIdentifier;->equals(Lcom/google/android/music/download/ContentIdentifier;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method protected getExplicit()Z
    .locals 1

    .prologue
    .line 113
    .local p0, "this":Lcom/google/android/music/download/BaseDownloadRequest;, "Lcom/google/android/music/download/BaseDownloadRequest<TProgressType;TOwnerType;>;"
    iget-boolean v0, p0, Lcom/google/android/music/download/BaseDownloadRequest;->mExplicit:Z

    return v0
.end method

.method public getFileLocation()Lcom/google/android/music/download/cache/FileLocation;
    .locals 1

    .prologue
    .line 134
    .local p0, "this":Lcom/google/android/music/download/BaseDownloadRequest;, "Lcom/google/android/music/download/BaseDownloadRequest<TProgressType;TOwnerType;>;"
    iget-object v0, p0, Lcom/google/android/music/download/BaseDownloadRequest;->mFileLocation:Lcom/google/android/music/download/cache/FileLocation;

    return-object v0
.end method

.method public getId()Lcom/google/android/music/download/ContentIdentifier;
    .locals 1

    .prologue
    .line 69
    .local p0, "this":Lcom/google/android/music/download/BaseDownloadRequest;, "Lcom/google/android/music/download/BaseDownloadRequest<TProgressType;TOwnerType;>;"
    iget-object v0, p0, Lcom/google/android/music/download/BaseDownloadRequest;->mId:Lcom/google/android/music/download/ContentIdentifier;

    return-object v0
.end method

.method protected abstract getMaxPriority()I
.end method

.method protected abstract getMinPriority()I
.end method

.method public getOwner()Lcom/google/android/music/download/DownloadRequest$Owner;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TOwnerType;"
        }
    .end annotation

    .prologue
    .line 120
    .local p0, "this":Lcom/google/android/music/download/BaseDownloadRequest;, "Lcom/google/android/music/download/BaseDownloadRequest<TProgressType;TOwnerType;>;"
    iget-object v0, p0, Lcom/google/android/music/download/BaseDownloadRequest;->mOwner:Lcom/google/android/music/download/DownloadRequest$Owner;

    return-object v0
.end method

.method protected abstract getOwnerFromInt(I)Lcom/google/android/music/download/DownloadRequest$Owner;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TOwnerType;"
        }
    .end annotation
.end method

.method public getPriority()I
    .locals 1

    .prologue
    .line 78
    .local p0, "this":Lcom/google/android/music/download/BaseDownloadRequest;, "Lcom/google/android/music/download/BaseDownloadRequest<TProgressType;TOwnerType;>;"
    iget v0, p0, Lcom/google/android/music/download/BaseDownloadRequest;->mPriority:I

    return v0
.end method

.method public hashCode()I
    .locals 8

    .prologue
    .line 155
    .local p0, "this":Lcom/google/android/music/download/BaseDownloadRequest;, "Lcom/google/android/music/download/BaseDownloadRequest<TProgressType;TOwnerType;>;"
    const/4 v0, 0x0

    .line 156
    .local v0, "h":I
    int-to-long v2, v0

    const/4 v1, 0x0

    int-to-long v4, v1

    iget-object v1, p0, Lcom/google/android/music/download/BaseDownloadRequest;->mId:Lcom/google/android/music/download/ContentIdentifier;

    invoke-virtual {v1}, Lcom/google/android/music/download/ContentIdentifier;->getId()J

    move-result-wide v6

    add-long/2addr v4, v6

    add-long/2addr v2, v4

    long-to-int v0, v2

    .line 157
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/music/download/BaseDownloadRequest;->mOwner:Lcom/google/android/music/download/DownloadRequest$Owner;

    invoke-interface {v2}, Lcom/google/android/music/download/DownloadRequest$Owner;->toInt()I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 159
    return v0
.end method

.method public isExplicit()Z
    .locals 1

    .prologue
    .line 141
    .local p0, "this":Lcom/google/android/music/download/BaseDownloadRequest;, "Lcom/google/android/music/download/BaseDownloadRequest<TProgressType;TOwnerType;>;"
    iget-boolean v0, p0, Lcom/google/android/music/download/BaseDownloadRequest;->mExplicit:Z

    return v0
.end method

.method public isMyProgress(Lcom/google/android/music/download/DownloadProgress;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TProgressType;)Z"
        }
    .end annotation

    .prologue
    .line 150
    .local p0, "this":Lcom/google/android/music/download/BaseDownloadRequest;, "Lcom/google/android/music/download/BaseDownloadRequest<TProgressType;TOwnerType;>;"
    .local p1, "progress":Lcom/google/android/music/download/DownloadProgress;, "TProgressType;"
    iget-object v0, p0, Lcom/google/android/music/download/BaseDownloadRequest;->mOwner:Lcom/google/android/music/download/DownloadRequest$Owner;

    invoke-interface {p1}, Lcom/google/android/music/download/DownloadProgress;->getOwner()Lcom/google/android/music/download/DownloadRequest$Owner;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/download/BaseDownloadRequest;->mId:Lcom/google/android/music/download/ContentIdentifier;

    invoke-interface {p1}, Lcom/google/android/music/download/DownloadProgress;->getId()Lcom/google/android/music/download/ContentIdentifier;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/music/download/ContentIdentifier;->equals(Lcom/google/android/music/download/ContentIdentifier;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isRetryAllowed()Z
    .locals 1

    .prologue
    .line 127
    .local p0, "this":Lcom/google/android/music/download/BaseDownloadRequest;, "Lcom/google/android/music/download/BaseDownloadRequest<TProgressType;TOwnerType;>;"
    iget-boolean v0, p0, Lcom/google/android/music/download/BaseDownloadRequest;->mRetryAllowed:Z

    return v0
.end method

.method protected shouldUpgrade(Lcom/google/android/music/download/BaseDownloadRequest;)Z
    .locals 2
    .param p1, "newRequest"    # Lcom/google/android/music/download/BaseDownloadRequest;

    .prologue
    .line 102
    .local p0, "this":Lcom/google/android/music/download/BaseDownloadRequest;, "Lcom/google/android/music/download/BaseDownloadRequest<TProgressType;TOwnerType;>;"
    iget-object v0, p0, Lcom/google/android/music/download/BaseDownloadRequest;->mOwner:Lcom/google/android/music/download/DownloadRequest$Owner;

    invoke-virtual {p1}, Lcom/google/android/music/download/BaseDownloadRequest;->getOwner()Lcom/google/android/music/download/DownloadRequest$Owner;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/download/BaseDownloadRequest;->mId:Lcom/google/android/music/download/ContentIdentifier;

    invoke-virtual {p1}, Lcom/google/android/music/download/BaseDownloadRequest;->getId()Lcom/google/android/music/download/ContentIdentifier;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/music/download/ContentIdentifier;->equals(Lcom/google/android/music/download/ContentIdentifier;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/music/download/BaseDownloadRequest;->mPriority:I

    invoke-virtual {p1}, Lcom/google/android/music/download/BaseDownloadRequest;->getPriority()I

    move-result v1

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 174
    .local p0, "this":Lcom/google/android/music/download/BaseDownloadRequest;, "Lcom/google/android/music/download/BaseDownloadRequest<TProgressType;TOwnerType;>;"
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 175
    .local v0, "builder":Ljava/lang/StringBuilder;
    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 176
    const-string v1, "mId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/download/BaseDownloadRequest;->mId:Lcom/google/android/music/download/ContentIdentifier;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 177
    const-string v1, " mPriority="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/music/download/BaseDownloadRequest;->mPriority:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 178
    const-string v1, " mOwner="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/download/BaseDownloadRequest;->mOwner:Lcom/google/android/music/download/DownloadRequest$Owner;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 179
    const-string v1, " mRetryAllowed="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/music/download/BaseDownloadRequest;->mRetryAllowed:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 180
    const-string v1, " mFileLocation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/download/BaseDownloadRequest;->mFileLocation:Lcom/google/android/music/download/cache/FileLocation;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 182
    const-string v1, " mExplicit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/music/download/BaseDownloadRequest;->mExplicit:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 184
    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 185
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public upgrade(Lcom/google/android/music/download/DownloadRequest;)Z
    .locals 3
    .param p1, "newRequest"    # Lcom/google/android/music/download/DownloadRequest;

    .prologue
    .line 88
    .local p0, "this":Lcom/google/android/music/download/BaseDownloadRequest;, "Lcom/google/android/music/download/BaseDownloadRequest<TProgressType;TOwnerType;>;"
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 89
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Can only pass DownloadRequest of same class to upgrade()"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    move-object v0, p1

    .line 92
    check-cast v0, Lcom/google/android/music/download/BaseDownloadRequest;

    .line 93
    .local v0, "baseDownloadRequest":Lcom/google/android/music/download/BaseDownloadRequest;
    invoke-virtual {p0, v0}, Lcom/google/android/music/download/BaseDownloadRequest;->shouldUpgrade(Lcom/google/android/music/download/BaseDownloadRequest;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 94
    invoke-virtual {p0, v0}, Lcom/google/android/music/download/BaseDownloadRequest;->doUpgrade(Lcom/google/android/music/download/BaseDownloadRequest;)V

    .line 95
    const/4 v1, 0x1

    .line 98
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .local p0, "this":Lcom/google/android/music/download/BaseDownloadRequest;, "Lcom/google/android/music/download/BaseDownloadRequest<TProgressType;TOwnerType;>;"
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 195
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/android/music/utils/ParcelUtils;->prepareParcel(Landroid/os/Parcel;Ljava/lang/Class;)V

    .line 197
    iget-object v0, p0, Lcom/google/android/music/download/BaseDownloadRequest;->mId:Lcom/google/android/music/download/ContentIdentifier;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 198
    iget v0, p0, Lcom/google/android/music/download/BaseDownloadRequest;->mPriority:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 199
    iget-object v0, p0, Lcom/google/android/music/download/BaseDownloadRequest;->mOwner:Lcom/google/android/music/download/DownloadRequest$Owner;

    invoke-interface {v0}, Lcom/google/android/music/download/DownloadRequest$Owner;->toInt()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 200
    iget-boolean v0, p0, Lcom/google/android/music/download/BaseDownloadRequest;->mRetryAllowed:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 201
    iget-object v0, p0, Lcom/google/android/music/download/BaseDownloadRequest;->mFileLocation:Lcom/google/android/music/download/cache/FileLocation;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 202
    iget-boolean v0, p0, Lcom/google/android/music/download/BaseDownloadRequest;->mExplicit:Z

    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 203
    return-void

    :cond_0
    move v0, v2

    .line 200
    goto :goto_0

    :cond_1
    move v1, v2

    .line 202
    goto :goto_1
.end method
