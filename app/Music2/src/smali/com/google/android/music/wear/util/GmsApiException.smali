.class public Lcom/google/android/music/wear/util/GmsApiException;
.super Ljava/lang/Exception;
.source "GmsApiException.java"


# instance fields
.field private final mStatus:Lcom/google/android/gms/common/api/Status;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/api/Status;)V
    .locals 1
    .param p1, "status"    # Lcom/google/android/gms/common/api/Status;

    .prologue
    .line 16
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-direct {p0, v0}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 17
    iput-object p1, p0, Lcom/google/android/music/wear/util/GmsApiException;->mStatus:Lcom/google/android/gms/common/api/Status;

    .line 18
    return-void

    .line 16
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/common/api/Status;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
