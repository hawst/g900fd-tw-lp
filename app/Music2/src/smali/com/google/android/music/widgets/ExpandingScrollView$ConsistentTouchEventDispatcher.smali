.class Lcom/google/android/music/widgets/ExpandingScrollView$ConsistentTouchEventDispatcher;
.super Ljava/lang/Object;
.source "ExpandingScrollView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/widgets/ExpandingScrollView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ConsistentTouchEventDispatcher"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/widgets/ExpandingScrollView$ConsistentTouchEventDispatcher$TouchEventDispatcher;
    }
.end annotation


# instance fields
.field private dispatcher:Lcom/google/android/music/widgets/ExpandingScrollView$ConsistentTouchEventDispatcher$TouchEventDispatcher;

.field private lastSentEvent:Landroid/view/MotionEvent;


# direct methods
.method public constructor <init>(Lcom/google/android/music/widgets/ExpandingScrollView$ConsistentTouchEventDispatcher$TouchEventDispatcher;)V
    .locals 0
    .param p1, "dispatcher"    # Lcom/google/android/music/widgets/ExpandingScrollView$ConsistentTouchEventDispatcher$TouchEventDispatcher;

    .prologue
    .line 1006
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1007
    iput-object p1, p0, Lcom/google/android/music/widgets/ExpandingScrollView$ConsistentTouchEventDispatcher;->dispatcher:Lcom/google/android/music/widgets/ExpandingScrollView$ConsistentTouchEventDispatcher$TouchEventDispatcher;

    .line 1008
    return-void
.end method

.method private static getPointerCountAfterEvent(Landroid/view/MotionEvent;)I
    .locals 4
    .param p0, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x0

    .line 1100
    if-nez p0, :cond_1

    move v1, v2

    .line 1111
    :cond_0
    :goto_0
    return v1

    .line 1104
    :cond_1
    invoke-virtual {p0}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v1

    .line 1105
    .local v1, "pointerCount":I
    invoke-virtual {p0}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    .line 1106
    .local v0, "action":I
    const/4 v3, 0x3

    if-ne v0, v3, :cond_2

    move v1, v2

    .line 1107
    goto :goto_0

    .line 1108
    :cond_2
    const/4 v2, 0x6

    if-eq v0, v2, :cond_3

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    .line 1109
    :cond_3
    add-int/lit8 v1, v1, -0x1

    goto :goto_0
.end method


# virtual methods
.method public cancelEventStream()V
    .locals 2

    .prologue
    .line 1083
    iget-object v0, p0, Lcom/google/android/music/widgets/ExpandingScrollView$ConsistentTouchEventDispatcher;->lastSentEvent:Landroid/view/MotionEvent;

    if-eqz v0, :cond_0

    .line 1084
    iget-object v0, p0, Lcom/google/android/music/widgets/ExpandingScrollView$ConsistentTouchEventDispatcher;->lastSentEvent:Landroid/view/MotionEvent;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->setAction(I)V

    .line 1085
    iget-object v0, p0, Lcom/google/android/music/widgets/ExpandingScrollView$ConsistentTouchEventDispatcher;->dispatcher:Lcom/google/android/music/widgets/ExpandingScrollView$ConsistentTouchEventDispatcher$TouchEventDispatcher;

    iget-object v1, p0, Lcom/google/android/music/widgets/ExpandingScrollView$ConsistentTouchEventDispatcher;->lastSentEvent:Landroid/view/MotionEvent;

    invoke-interface {v0, v1}, Lcom/google/android/music/widgets/ExpandingScrollView$ConsistentTouchEventDispatcher$TouchEventDispatcher;->dispatchEvent(Landroid/view/MotionEvent;)Z

    .line 1086
    iget-object v0, p0, Lcom/google/android/music/widgets/ExpandingScrollView$ConsistentTouchEventDispatcher;->lastSentEvent:Landroid/view/MotionEvent;

    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    .line 1087
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/widgets/ExpandingScrollView$ConsistentTouchEventDispatcher;->lastSentEvent:Landroid/view/MotionEvent;

    .line 1089
    :cond_0
    return-void
.end method

.method public dispatchEvent(Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x1

    .line 1015
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    .line 1016
    .local v0, "action":I
    if-eq v0, v7, :cond_4

    .line 1018
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v4

    .line 1019
    .local v4, "targetPointerCount":I
    const/4 v5, 0x5

    if-eq v0, v5, :cond_0

    if-nez v0, :cond_1

    .line 1022
    :cond_0
    add-int/lit8 v4, v4, -0x1

    .line 1026
    :cond_1
    iget-object v5, p0, Lcom/google/android/music/widgets/ExpandingScrollView$ConsistentTouchEventDispatcher;->lastSentEvent:Landroid/view/MotionEvent;

    invoke-static {v5}, Lcom/google/android/music/widgets/ExpandingScrollView$ConsistentTouchEventDispatcher;->getPointerCountAfterEvent(Landroid/view/MotionEvent;)I

    move-result v1

    .line 1027
    .local v1, "currentPointerCount":I
    if-ge v1, v4, :cond_5

    .line 1028
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v2

    .line 1030
    .local v2, "fakeEvent":Landroid/view/MotionEvent;
    if-nez v1, :cond_2

    .line 1031
    const/4 v5, 0x0

    invoke-virtual {v2, v5}, Landroid/view/MotionEvent;->setAction(I)V

    .line 1032
    iget-object v5, p0, Lcom/google/android/music/widgets/ExpandingScrollView$ConsistentTouchEventDispatcher;->dispatcher:Lcom/google/android/music/widgets/ExpandingScrollView$ConsistentTouchEventDispatcher$TouchEventDispatcher;

    invoke-interface {v5, v2}, Lcom/google/android/music/widgets/ExpandingScrollView$ConsistentTouchEventDispatcher$TouchEventDispatcher;->dispatchEvent(Landroid/view/MotionEvent;)Z

    .line 1033
    add-int/lit8 v1, v1, 0x1

    .line 1037
    :cond_2
    :goto_0
    if-ge v1, v4, :cond_3

    .line 1038
    shl-int/lit8 v5, v1, 0x8

    or-int/lit8 v5, v5, 0x5

    invoke-virtual {v2, v5}, Landroid/view/MotionEvent;->setAction(I)V

    .line 1040
    iget-object v5, p0, Lcom/google/android/music/widgets/ExpandingScrollView$ConsistentTouchEventDispatcher;->dispatcher:Lcom/google/android/music/widgets/ExpandingScrollView$ConsistentTouchEventDispatcher$TouchEventDispatcher;

    invoke-interface {v5, v2}, Lcom/google/android/music/widgets/ExpandingScrollView$ConsistentTouchEventDispatcher$TouchEventDispatcher;->dispatchEvent(Landroid/view/MotionEvent;)Z

    .line 1041
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1043
    :cond_3
    invoke-virtual {v2}, Landroid/view/MotionEvent;->recycle()V

    .line 1069
    .end local v1    # "currentPointerCount":I
    .end local v2    # "fakeEvent":Landroid/view/MotionEvent;
    .end local v4    # "targetPointerCount":I
    :cond_4
    :goto_1
    if-eq v0, v6, :cond_8

    if-eq v0, v7, :cond_8

    .line 1070
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/music/widgets/ExpandingScrollView$ConsistentTouchEventDispatcher;->lastSentEvent:Landroid/view/MotionEvent;

    .line 1076
    :goto_2
    iget-object v5, p0, Lcom/google/android/music/widgets/ExpandingScrollView$ConsistentTouchEventDispatcher;->dispatcher:Lcom/google/android/music/widgets/ExpandingScrollView$ConsistentTouchEventDispatcher$TouchEventDispatcher;

    invoke-interface {v5, p1}, Lcom/google/android/music/widgets/ExpandingScrollView$ConsistentTouchEventDispatcher$TouchEventDispatcher;->dispatchEvent(Landroid/view/MotionEvent;)Z

    move-result v5

    return v5

    .line 1044
    .restart local v1    # "currentPointerCount":I
    .restart local v4    # "targetPointerCount":I
    :cond_5
    if-le v1, v4, :cond_4

    .line 1047
    iget-object v5, p0, Lcom/google/android/music/widgets/ExpandingScrollView$ConsistentTouchEventDispatcher;->lastSentEvent:Landroid/view/MotionEvent;

    invoke-static {v5}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v2

    .line 1050
    .restart local v2    # "fakeEvent":Landroid/view/MotionEvent;
    invoke-static {v4, v6}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 1051
    .local v3, "minPointerCount":I
    :goto_3
    if-le v1, v3, :cond_6

    .line 1052
    add-int/lit8 v1, v1, -0x1

    .line 1053
    shl-int/lit8 v5, v1, 0x8

    or-int/lit8 v5, v5, 0x6

    invoke-virtual {v2, v5}, Landroid/view/MotionEvent;->setAction(I)V

    .line 1055
    iget-object v5, p0, Lcom/google/android/music/widgets/ExpandingScrollView$ConsistentTouchEventDispatcher;->dispatcher:Lcom/google/android/music/widgets/ExpandingScrollView$ConsistentTouchEventDispatcher$TouchEventDispatcher;

    invoke-interface {v5, v2}, Lcom/google/android/music/widgets/ExpandingScrollView$ConsistentTouchEventDispatcher$TouchEventDispatcher;->dispatchEvent(Landroid/view/MotionEvent;)Z

    goto :goto_3

    .line 1059
    :cond_6
    if-nez v4, :cond_7

    .line 1060
    invoke-virtual {v2, v6}, Landroid/view/MotionEvent;->setAction(I)V

    .line 1061
    iget-object v5, p0, Lcom/google/android/music/widgets/ExpandingScrollView$ConsistentTouchEventDispatcher;->dispatcher:Lcom/google/android/music/widgets/ExpandingScrollView$ConsistentTouchEventDispatcher$TouchEventDispatcher;

    invoke-interface {v5, v2}, Lcom/google/android/music/widgets/ExpandingScrollView$ConsistentTouchEventDispatcher$TouchEventDispatcher;->dispatchEvent(Landroid/view/MotionEvent;)Z

    .line 1062
    add-int/lit8 v1, v1, -0x1

    .line 1064
    :cond_7
    invoke-virtual {v2}, Landroid/view/MotionEvent;->recycle()V

    goto :goto_1

    .line 1072
    .end local v1    # "currentPointerCount":I
    .end local v2    # "fakeEvent":Landroid/view/MotionEvent;
    .end local v3    # "minPointerCount":I
    .end local v4    # "targetPointerCount":I
    :cond_8
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/google/android/music/widgets/ExpandingScrollView$ConsistentTouchEventDispatcher;->lastSentEvent:Landroid/view/MotionEvent;

    goto :goto_2
.end method

.method public hasActiveEventStream()Z
    .locals 1

    .prologue
    .line 1095
    iget-object v0, p0, Lcom/google/android/music/widgets/ExpandingScrollView$ConsistentTouchEventDispatcher;->lastSentEvent:Landroid/view/MotionEvent;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
