.class Lcom/google/android/music/download/AbstractSchedulingService$2;
.super Lcom/google/android/music/utils/SafeServiceConnection;
.source "AbstractSchedulingService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/download/AbstractSchedulingService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/download/AbstractSchedulingService;


# direct methods
.method constructor <init>(Lcom/google/android/music/download/AbstractSchedulingService;)V
    .locals 0

    .prologue
    .line 128
    iput-object p1, p0, Lcom/google/android/music/download/AbstractSchedulingService$2;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    invoke-direct {p0}, Lcom/google/android/music/utils/SafeServiceConnection;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 131
    iget-object v1, p0, Lcom/google/android/music/download/AbstractSchedulingService$2;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    invoke-static {p2}, Lcom/google/android/music/download/cache/ICacheManager$Stub;->asInterface(Landroid/os/IBinder;)Lcom/google/android/music/download/cache/ICacheManager;

    move-result-object v2

    # setter for: Lcom/google/android/music/download/AbstractSchedulingService;->mCacheManager:Lcom/google/android/music/download/cache/ICacheManager;
    invoke-static {v1, v2}, Lcom/google/android/music/download/AbstractSchedulingService;->access$202(Lcom/google/android/music/download/AbstractSchedulingService;Lcom/google/android/music/download/cache/ICacheManager;)Lcom/google/android/music/download/cache/ICacheManager;

    .line 133
    :try_start_0
    iget-object v1, p0, Lcom/google/android/music/download/AbstractSchedulingService$2;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    # getter for: Lcom/google/android/music/download/AbstractSchedulingService;->mCacheManager:Lcom/google/android/music/download/cache/ICacheManager;
    invoke-static {v1}, Lcom/google/android/music/download/AbstractSchedulingService;->access$200(Lcom/google/android/music/download/AbstractSchedulingService;)Lcom/google/android/music/download/cache/ICacheManager;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/download/AbstractSchedulingService$2;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    # getter for: Lcom/google/android/music/download/AbstractSchedulingService;->mDeleteFilter:Lcom/google/android/music/download/cache/IDeleteFilter;
    invoke-static {v2}, Lcom/google/android/music/download/AbstractSchedulingService;->access$300(Lcom/google/android/music/download/AbstractSchedulingService;)Lcom/google/android/music/download/cache/IDeleteFilter;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/music/download/cache/ICacheManager;->registerDeleteFilter(Lcom/google/android/music/download/cache/IDeleteFilter;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 137
    :goto_0
    iget-object v1, p0, Lcom/google/android/music/download/AbstractSchedulingService$2;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    # invokes: Lcom/google/android/music/download/AbstractSchedulingService;->checkDependentServices()V
    invoke-static {v1}, Lcom/google/android/music/download/AbstractSchedulingService;->access$100(Lcom/google/android/music/download/AbstractSchedulingService;)V

    .line 138
    return-void

    .line 134
    :catch_0
    move-exception v0

    .line 135
    .local v0, "e":Landroid/os/RemoteException;
    iget-object v1, p0, Lcom/google/android/music/download/AbstractSchedulingService$2;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    # getter for: Lcom/google/android/music/download/AbstractSchedulingService;->TAG:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/music/download/AbstractSchedulingService;->access$400(Lcom/google/android/music/download/AbstractSchedulingService;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "Failed to register delete filter"

    invoke-static {v1, v2, v0}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 0
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 143
    return-void
.end method
