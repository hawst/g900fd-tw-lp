.class public Lcom/google/android/music/ui/SubGenresExploreActivity;
.super Lcom/google/android/music/ui/BaseActivity;
.source "SubGenresExploreActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/google/android/music/ui/BaseActivity;-><init>()V

    return-void
.end method

.method public static final buildStartIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "genreId"    # Ljava/lang/String;
    .param p2, "genreName"    # Ljava/lang/String;
    .param p3, "parentGenreId"    # Ljava/lang/String;

    .prologue
    .line 41
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 42
    :cond_0
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Genre name and its NautilusId are needed to display new releases and top charts for sub genres. "

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 46
    :cond_1
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/google/android/music/ui/SubGenresExploreActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 47
    .local v1, "intent":Landroid/content/Intent;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 48
    .local v0, "extra":Landroid/os/Bundle;
    const-string v2, "nautilusId"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    const-string v2, "name"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    const-string v2, "parentNautilusId"

    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 52
    return-object v1
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 18
    invoke-super {p0, p1}, Lcom/google/android/music/ui/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 20
    invoke-virtual {p0}, Lcom/google/android/music/ui/SubGenresExploreActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 21
    .local v0, "extra":Landroid/os/Bundle;
    const-string v3, "name"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 22
    .local v2, "genreName":Ljava/lang/String;
    invoke-virtual {p0, v2}, Lcom/google/android/music/ui/SubGenresExploreActivity;->setActionBarTitle(Ljava/lang/String;)V

    .line 24
    invoke-virtual {p0}, Lcom/google/android/music/ui/SubGenresExploreActivity;->getContent()Landroid/support/v4/app/Fragment;

    move-result-object v3

    if-nez v3, :cond_0

    .line 25
    new-instance v1, Lcom/google/android/music/ui/SubGenresExploreTabFragment;

    invoke-direct {v1}, Lcom/google/android/music/ui/SubGenresExploreTabFragment;-><init>()V

    .line 26
    .local v1, "fragment":Landroid/support/v4/app/Fragment;
    invoke-virtual {v1, v0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 27
    const/4 v3, 0x0

    invoke-virtual {p0, v1, v3}, Lcom/google/android/music/ui/SubGenresExploreActivity;->replaceContent(Landroid/support/v4/app/Fragment;Z)V

    .line 29
    .end local v1    # "fragment":Landroid/support/v4/app/Fragment;
    :cond_0
    return-void
.end method
