.class public Lcom/google/android/music/leanback/bitmap/MyAlbumsBitmapGettersGetter;
.super Lcom/google/android/music/leanback/bitmap/MediaListBitmapGettersGetter;
.source "MyAlbumsBitmapGettersGetter.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/support/v4/app/LoaderManager;IJ)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "loaderManager"    # Landroid/support/v4/app/LoaderManager;
    .param p3, "loaderId"    # I
    .param p4, "seed"    # J

    .prologue
    .line 20
    invoke-direct/range {p0 .. p5}, Lcom/google/android/music/leanback/bitmap/MediaListBitmapGettersGetter;-><init>(Landroid/content/Context;Landroid/support/v4/app/LoaderManager;IJ)V

    .line 21
    invoke-virtual {p0}, Lcom/google/android/music/leanback/bitmap/MyAlbumsBitmapGettersGetter;->startLoading()V

    .line 22
    return-void
.end method


# virtual methods
.method protected addBitmapGetters(Landroid/database/Cursor;Ljava/util/ArrayList;)V
    .locals 3
    .param p1, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/music/leanback/bitmap/BitmapGetter;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 36
    .local p2, "bitmapGettersList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/music/leanback/bitmap/BitmapGetter;>;"
    const/4 v2, 0x0

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 37
    .local v0, "id":J
    new-instance v2, Lcom/google/android/music/leanback/bitmap/AlbumIdBitmapGetter;

    invoke-direct {v2, v0, v1}, Lcom/google/android/music/leanback/bitmap/AlbumIdBitmapGetter;-><init>(J)V

    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 38
    return-void
.end method

.method public bridge synthetic getBitmapGetters()[Lcom/google/android/music/leanback/bitmap/BitmapGetter;
    .locals 1

    .prologue
    .line 16
    invoke-super {p0}, Lcom/google/android/music/leanback/bitmap/MediaListBitmapGettersGetter;->getBitmapGetters()[Lcom/google/android/music/leanback/bitmap/BitmapGetter;

    move-result-object v0

    return-object v0
.end method

.method protected getMediaList()Lcom/google/android/music/medialist/MediaList;
    .locals 1

    .prologue
    .line 26
    new-instance v0, Lcom/google/android/music/medialist/AllAlbumsList;

    invoke-direct {v0}, Lcom/google/android/music/medialist/AllAlbumsList;-><init>()V

    return-object v0
.end method

.method protected getProjection()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/google/android/music/ui/AlbumsAdapter;->PROJECTION:[Ljava/lang/String;

    return-object v0
.end method

.method public bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 0
    .param p1, "x0"    # Landroid/support/v4/content/Loader;
    .param p2, "x1"    # Landroid/database/Cursor;

    .prologue
    .line 16
    invoke-super {p0, p1, p2}, Lcom/google/android/music/leanback/bitmap/MediaListBitmapGettersGetter;->onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public bridge synthetic onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 0
    .param p1, "x0"    # Landroid/support/v4/content/Loader;

    .prologue
    .line 16
    invoke-super {p0, p1}, Lcom/google/android/music/leanback/bitmap/MediaListBitmapGettersGetter;->onLoaderReset(Landroid/support/v4/content/Loader;)V

    return-void
.end method

.method public bridge synthetic startLoading()V
    .locals 0

    .prologue
    .line 16
    invoke-super {p0}, Lcom/google/android/music/leanback/bitmap/MediaListBitmapGettersGetter;->startLoading()V

    return-void
.end method
