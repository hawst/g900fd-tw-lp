.class Lcom/google/android/music/download/cache/BaseCacheService$CacheWorker;
.super Lcom/google/android/music/utils/LoggableHandler;
.source "BaseCacheService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/download/cache/BaseCacheService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CacheWorker"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/download/cache/BaseCacheService;


# direct methods
.method public constructor <init>(Lcom/google/android/music/download/cache/BaseCacheService;Ljava/lang/String;)V
    .locals 0
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 64
    iput-object p1, p0, Lcom/google/android/music/download/cache/BaseCacheService$CacheWorker;->this$0:Lcom/google/android/music/download/cache/BaseCacheService;

    .line 65
    invoke-direct {p0, p2}, Lcom/google/android/music/utils/LoggableHandler;-><init>(Ljava/lang/String;)V

    .line 66
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 70
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 83
    :goto_0
    iget-object v0, p0, Lcom/google/android/music/download/cache/BaseCacheService$CacheWorker;->this$0:Lcom/google/android/music/download/cache/BaseCacheService;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v1}, Lcom/google/android/music/download/cache/BaseCacheService;->stopSelf(I)V

    .line 84
    return-void

    .line 72
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/music/download/cache/BaseCacheService$CacheWorker;->this$0:Lcom/google/android/music/download/cache/BaseCacheService;

    # invokes: Lcom/google/android/music/download/cache/BaseCacheService;->handleClearOrphaned(Landroid/os/Message;)V
    invoke-static {v0, p1}, Lcom/google/android/music/download/cache/BaseCacheService;->access$000(Lcom/google/android/music/download/cache/BaseCacheService;Landroid/os/Message;)V

    goto :goto_0

    .line 75
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/music/download/cache/BaseCacheService$CacheWorker;->this$0:Lcom/google/android/music/download/cache/BaseCacheService;

    # invokes: Lcom/google/android/music/download/cache/BaseCacheService;->handleClearCache(Landroid/os/Message;)V
    invoke-static {v0, p1}, Lcom/google/android/music/download/cache/BaseCacheService;->access$100(Lcom/google/android/music/download/cache/BaseCacheService;Landroid/os/Message;)V

    goto :goto_0

    .line 78
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/music/download/cache/BaseCacheService$CacheWorker;->this$0:Lcom/google/android/music/download/cache/BaseCacheService;

    # invokes: Lcom/google/android/music/download/cache/BaseCacheService;->handleLowStorage(Landroid/os/Message;)V
    invoke-static {v0, p1}, Lcom/google/android/music/download/cache/BaseCacheService;->access$200(Lcom/google/android/music/download/cache/BaseCacheService;Landroid/os/Message;)V

    goto :goto_0

    .line 70
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
