.class public Lcom/google/android/music/dial/model/CommandHeaderJson;
.super Lcom/google/api/client/json/GenericJson;
.source "CommandHeaderJson.java"


# instance fields
.field public mCmdId:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "cmdId"
    .end annotation
.end field

.field public mCommand:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "command"
    .end annotation
.end field

.field public mNamespace:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "namespace"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    return-void
.end method
