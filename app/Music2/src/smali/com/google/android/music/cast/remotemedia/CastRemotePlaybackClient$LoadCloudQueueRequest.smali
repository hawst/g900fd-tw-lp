.class Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$LoadCloudQueueRequest;
.super Ljava/lang/Object;
.source "CastRemotePlaybackClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadCloudQueueRequest"
.end annotation


# instance fields
.field private final mItemId:Ljava/lang/String;

.field private final mMusicFile:Lcom/google/android/music/store/MusicFile;

.field private final mPlayOnCompletion:Z

.field private final mPositionMillis:J

.field private final mQueueBaseUrl:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;


# direct methods
.method public constructor <init>(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;Ljava/lang/String;Lcom/google/android/music/store/MusicFile;Ljava/lang/String;JZ)V
    .locals 1
    .param p2, "queueBaseUrl"    # Ljava/lang/String;
    .param p3, "musicFile"    # Lcom/google/android/music/store/MusicFile;
    .param p4, "itemId"    # Ljava/lang/String;
    .param p5, "positionMillis"    # J
    .param p7, "playOnCompletion"    # Z

    .prologue
    .line 1103
    iput-object p1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$LoadCloudQueueRequest;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1104
    iput-object p2, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$LoadCloudQueueRequest;->mQueueBaseUrl:Ljava/lang/String;

    .line 1105
    iput-object p3, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$LoadCloudQueueRequest;->mMusicFile:Lcom/google/android/music/store/MusicFile;

    .line 1106
    iput-object p4, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$LoadCloudQueueRequest;->mItemId:Ljava/lang/String;

    .line 1107
    iput-wide p5, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$LoadCloudQueueRequest;->mPositionMillis:J

    .line 1108
    iput-boolean p7, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$LoadCloudQueueRequest;->mPlayOnCompletion:Z

    .line 1109
    return-void
.end method


# virtual methods
.method public getItemId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1120
    iget-object v0, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$LoadCloudQueueRequest;->mItemId:Ljava/lang/String;

    return-object v0
.end method

.method public getMusicFile()Lcom/google/android/music/store/MusicFile;
    .locals 1

    .prologue
    .line 1116
    iget-object v0, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$LoadCloudQueueRequest;->mMusicFile:Lcom/google/android/music/store/MusicFile;

    return-object v0
.end method

.method public getPositionMillis()J
    .locals 2

    .prologue
    .line 1124
    iget-wide v0, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$LoadCloudQueueRequest;->mPositionMillis:J

    return-wide v0
.end method

.method public getQueueBaseUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1112
    iget-object v0, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$LoadCloudQueueRequest;->mQueueBaseUrl:Ljava/lang/String;

    return-object v0
.end method

.method public isPlayOnCompletion()Z
    .locals 1

    .prologue
    .line 1128
    iget-boolean v0, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$LoadCloudQueueRequest;->mPlayOnCompletion:Z

    return v0
.end method
