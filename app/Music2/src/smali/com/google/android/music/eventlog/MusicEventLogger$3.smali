.class Lcom/google/android/music/eventlog/MusicEventLogger$3;
.super Ljava/lang/Object;
.source "MusicEventLogger.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/eventlog/MusicEventLogger;->logStopEventAsync(ZJLcom/google/android/music/store/ContainerDescriptor;JZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/eventlog/MusicEventLogger;

.field final synthetic val$containerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

.field final synthetic val$explicit:Z

.field final synthetic val$isRemote:Z

.field final synthetic val$lastUserExplicitPlayTime:J

.field final synthetic val$musicId:J


# direct methods
.method constructor <init>(Lcom/google/android/music/eventlog/MusicEventLogger;ZJLcom/google/android/music/store/ContainerDescriptor;JZ)V
    .locals 0

    .prologue
    .line 426
    iput-object p1, p0, Lcom/google/android/music/eventlog/MusicEventLogger$3;->this$0:Lcom/google/android/music/eventlog/MusicEventLogger;

    iput-boolean p2, p0, Lcom/google/android/music/eventlog/MusicEventLogger$3;->val$isRemote:Z

    iput-wide p3, p0, Lcom/google/android/music/eventlog/MusicEventLogger$3;->val$musicId:J

    iput-object p5, p0, Lcom/google/android/music/eventlog/MusicEventLogger$3;->val$containerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    iput-wide p6, p0, Lcom/google/android/music/eventlog/MusicEventLogger$3;->val$lastUserExplicitPlayTime:J

    iput-boolean p8, p0, Lcom/google/android/music/eventlog/MusicEventLogger$3;->val$explicit:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 429
    iget-object v0, p0, Lcom/google/android/music/eventlog/MusicEventLogger$3;->this$0:Lcom/google/android/music/eventlog/MusicEventLogger;

    iget-boolean v1, p0, Lcom/google/android/music/eventlog/MusicEventLogger$3;->val$isRemote:Z

    iget-wide v2, p0, Lcom/google/android/music/eventlog/MusicEventLogger$3;->val$musicId:J

    iget-object v4, p0, Lcom/google/android/music/eventlog/MusicEventLogger$3;->val$containerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    iget-wide v5, p0, Lcom/google/android/music/eventlog/MusicEventLogger$3;->val$lastUserExplicitPlayTime:J

    iget-boolean v7, p0, Lcom/google/android/music/eventlog/MusicEventLogger$3;->val$explicit:Z

    # invokes: Lcom/google/android/music/eventlog/MusicEventLogger;->logStopEvent(ZJLcom/google/android/music/store/ContainerDescriptor;JZ)V
    invoke-static/range {v0 .. v7}, Lcom/google/android/music/eventlog/MusicEventLogger;->access$100(Lcom/google/android/music/eventlog/MusicEventLogger;ZJLcom/google/android/music/store/ContainerDescriptor;JZ)V

    .line 431
    return-void
.end method
