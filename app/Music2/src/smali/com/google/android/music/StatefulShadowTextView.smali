.class public Lcom/google/android/music/StatefulShadowTextView;
.super Landroid/widget/TextView;
.source "StatefulShadowTextView.java"


# static fields
.field private static mPrimaryOffline:Landroid/content/res/ColorStateList;

.field private static mPrimaryOnline:Landroid/content/res/ColorStateList;

.field private static mSecondaryOffline:Landroid/content/res/ColorStateList;

.field private static mSecondaryOnline:Landroid/content/res/ColorStateList;

.field private static mShadowOffline:I

.field private static mShadowOnline:I


# instance fields
.field private mOnline:Z

.field private mShowShadowWhenDeselected:Z

.field private mShowShadowWhenSelected:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const v3, 0x7f0c0099

    const/4 v2, 0x1

    .line 32
    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 27
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/music/StatefulShadowTextView;->mShowShadowWhenSelected:Z

    .line 28
    iput-boolean v2, p0, Lcom/google/android/music/StatefulShadowTextView;->mShowShadowWhenDeselected:Z

    .line 29
    iput-boolean v2, p0, Lcom/google/android/music/StatefulShadowTextView;->mOnline:Z

    .line 33
    invoke-direct {p0}, Lcom/google/android/music/StatefulShadowTextView;->updateShadowState()V

    .line 35
    sget-object v1, Lcom/google/android/music/StatefulShadowTextView;->mPrimaryOnline:Landroid/content/res/ColorStateList;

    if-nez v1, :cond_0

    .line 36
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 37
    .local v0, "res":Landroid/content/res/Resources;
    const v1, 0x7f0c0143

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    sput-object v1, Lcom/google/android/music/StatefulShadowTextView;->mPrimaryOnline:Landroid/content/res/ColorStateList;

    .line 38
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    sput-object v1, Lcom/google/android/music/StatefulShadowTextView;->mPrimaryOffline:Landroid/content/res/ColorStateList;

    .line 39
    const v1, 0x7f0c0145

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    sput-object v1, Lcom/google/android/music/StatefulShadowTextView;->mSecondaryOnline:Landroid/content/res/ColorStateList;

    .line 40
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    sput-object v1, Lcom/google/android/music/StatefulShadowTextView;->mSecondaryOffline:Landroid/content/res/ColorStateList;

    .line 41
    const v1, 0x7f0c0148

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v1

    sput v1, Lcom/google/android/music/StatefulShadowTextView;->mShadowOnline:I

    .line 42
    const v1, 0x7f0c0149

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v1

    sput v1, Lcom/google/android/music/StatefulShadowTextView;->mShadowOffline:I

    .line 44
    .end local v0    # "res":Landroid/content/res/Resources;
    :cond_0
    return-void
.end method

.method private updateShadowState()V
    .locals 2

    .prologue
    .line 74
    invoke-virtual {p0}, Lcom/google/android/music/StatefulShadowTextView;->isPressed()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/music/StatefulShadowTextView;->isSelected()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/music/StatefulShadowTextView;->isFocused()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/music/StatefulShadowTextView;->mShowShadowWhenSelected:Z

    .line 81
    .local v0, "showShadow":Z
    :goto_0
    return-void

    .line 74
    .end local v0    # "showShadow":Z
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/music/StatefulShadowTextView;->mShowShadowWhenDeselected:Z

    goto :goto_0
.end method


# virtual methods
.method protected drawableStateChanged()V
    .locals 0

    .prologue
    .line 69
    invoke-super {p0}, Landroid/widget/TextView;->drawableStateChanged()V

    .line 70
    invoke-direct {p0}, Lcom/google/android/music/StatefulShadowTextView;->updateShadowState()V

    .line 71
    return-void
.end method

.method public setPrimaryAndOnline(ZZ)V
    .locals 1
    .param p1, "primary"    # Z
    .param p2, "online"    # Z

    .prologue
    .line 57
    if-eqz p1, :cond_1

    .line 58
    if-eqz p2, :cond_0

    sget-object v0, Lcom/google/android/music/StatefulShadowTextView;->mPrimaryOnline:Landroid/content/res/ColorStateList;

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/music/StatefulShadowTextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 64
    :goto_1
    iput-boolean p2, p0, Lcom/google/android/music/StatefulShadowTextView;->mOnline:Z

    .line 65
    return-void

    .line 58
    :cond_0
    sget-object v0, Lcom/google/android/music/StatefulShadowTextView;->mPrimaryOffline:Landroid/content/res/ColorStateList;

    goto :goto_0

    .line 60
    :cond_1
    if-eqz p2, :cond_2

    sget-object v0, Lcom/google/android/music/StatefulShadowTextView;->mSecondaryOnline:Landroid/content/res/ColorStateList;

    :goto_2
    invoke-virtual {p0, v0}, Lcom/google/android/music/StatefulShadowTextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    goto :goto_1

    :cond_2
    sget-object v0, Lcom/google/android/music/StatefulShadowTextView;->mSecondaryOffline:Landroid/content/res/ColorStateList;

    goto :goto_2
.end method

.method public setShowShadowWhenDeselected(Z)V
    .locals 0
    .param p1, "b"    # Z

    .prologue
    .line 52
    iput-boolean p1, p0, Lcom/google/android/music/StatefulShadowTextView;->mShowShadowWhenDeselected:Z

    .line 53
    invoke-direct {p0}, Lcom/google/android/music/StatefulShadowTextView;->updateShadowState()V

    .line 54
    return-void
.end method

.method public setShowShadowWhenSelected(Z)V
    .locals 0
    .param p1, "b"    # Z

    .prologue
    .line 47
    iput-boolean p1, p0, Lcom/google/android/music/StatefulShadowTextView;->mShowShadowWhenSelected:Z

    .line 48
    invoke-direct {p0}, Lcom/google/android/music/StatefulShadowTextView;->updateShadowState()V

    .line 49
    return-void
.end method
