.class public Lcom/google/android/music/icing/SearchCorpora;
.super Ljava/lang/Object;
.source "SearchCorpora.java"


# static fields
.field private static mInstance:Lcom/google/android/music/icing/SearchCorpora;


# instance fields
.field private final mAlbumTableStorageSpec:Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;

.field private final mArtistTableStorageSpec:Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;

.field private final mListTableStorageSpec:Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;

.field private final mSongTableStorageSpec:Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;

.field public final tableStorageSpecs:[Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 4
    .param p1, "res"    # Landroid/content/res/Resources;

    .prologue
    const/4 v3, 0x0

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    invoke-static {}, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;->builder()Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;

    move-result-object v0

    const v1, 0x7f0b02f5

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;->corpus(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;

    move-result-object v0

    const-string v1, "MUSIC"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;->table(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;

    move-result-object v0

    const-string v1, "SongId"

    invoke-virtual {v0, v1, v3}, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;->uriColumn(Ljava/lang/String;Z)Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;

    move-result-object v0

    const v1, 0x7f0b02f6

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "Title"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;->columnForSection(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;

    move-result-object v0

    const v1, 0x7f0b02f7

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "Artist"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;->columnForSection(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;

    move-result-object v0

    const v1, 0x7f0b02f8

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "AlbumId"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;->columnForSection(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;

    move-result-object v0

    const v1, 0x7f0b02f9

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "Id"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;->columnForSection(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;

    move-result-object v0

    const-string v1, "+Domain=0"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;->condition(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;

    move-result-object v0

    const-string v1, "PlayCount"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;->score(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;->build()Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/icing/SearchCorpora;->mSongTableStorageSpec:Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;

    .line 50
    invoke-static {}, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;->builder()Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;

    move-result-object v0

    const v1, 0x7f0b02fe

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;->corpus(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;

    move-result-object v0

    const-string v1, "MUSIC"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;->table(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;

    move-result-object v0

    const-string v1, "AlbumId"

    invoke-virtual {v0, v1, v3}, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;->uriColumn(Ljava/lang/String;Z)Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;

    move-result-object v0

    const v1, 0x7f0b02ff

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "Album"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;->columnForSection(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;

    move-result-object v0

    const v1, 0x7f0b0300

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "AlbumArtist"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;->columnForSection(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;

    move-result-object v0

    const-string v1, "+Domain=0"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;->condition(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;->build()Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/icing/SearchCorpora;->mAlbumTableStorageSpec:Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;

    .line 59
    invoke-static {}, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;->builder()Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;

    move-result-object v0

    const v1, 0x7f0b0305

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;->corpus(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;

    move-result-object v0

    const-string v1, "MUSIC"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;->table(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;

    move-result-object v0

    const-string v1, "AlbumArtistId"

    invoke-virtual {v0, v1, v3}, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;->uriColumn(Ljava/lang/String;Z)Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;

    move-result-object v0

    const v1, 0x7f0b0306

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "AlbumArtist"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;->columnForSection(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;

    move-result-object v0

    const-string v1, "+Domain=0"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;->condition(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;->build()Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/icing/SearchCorpora;->mArtistTableStorageSpec:Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;

    .line 67
    invoke-static {}, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;->builder()Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;

    move-result-object v0

    const v1, 0x7f0b030b

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;->corpus(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;

    move-result-object v0

    const-string v1, "LISTS"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;->table(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;

    move-result-object v0

    const-string v1, "Id"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;->uriColumn(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;

    move-result-object v0

    const v1, 0x7f0b030c

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "Name"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;->columnForSection(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;

    move-result-object v0

    const-string v1, "ListType IN (0, 1, 71)"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;->condition(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;->build()Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/icing/SearchCorpora;->mListTableStorageSpec:Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;

    .line 76
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;

    iget-object v1, p0, Lcom/google/android/music/icing/SearchCorpora;->mSongTableStorageSpec:Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;

    aput-object v1, v0, v3

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/music/icing/SearchCorpora;->mAlbumTableStorageSpec:Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/music/icing/SearchCorpora;->mArtistTableStorageSpec:Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/music/icing/SearchCorpora;->mListTableStorageSpec:Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/google/android/music/icing/SearchCorpora;->tableStorageSpecs:[Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;

    .line 78
    return-void
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/google/android/music/icing/SearchCorpora;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 29
    const-class v1, Lcom/google/android/music/icing/SearchCorpora;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/music/icing/SearchCorpora;->mInstance:Lcom/google/android/music/icing/SearchCorpora;

    if-nez v0, :cond_0

    .line 30
    new-instance v0, Lcom/google/android/music/icing/SearchCorpora;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/google/android/music/icing/SearchCorpora;-><init>(Landroid/content/res/Resources;)V

    sput-object v0, Lcom/google/android/music/icing/SearchCorpora;->mInstance:Lcom/google/android/music/icing/SearchCorpora;

    .line 32
    :cond_0
    sget-object v0, Lcom/google/android/music/icing/SearchCorpora;->mInstance:Lcom/google/android/music/icing/SearchCorpora;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 29
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
