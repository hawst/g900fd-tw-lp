.class public Lcom/google/android/music/ui/MaterialContainerHeaderView;
.super Landroid/widget/RelativeLayout;
.source "MaterialContainerHeaderView.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mAlbumYear:Landroid/widget/TextView;

.field private mAlbumYearHyphen:Landroid/widget/TextView;

.field private mAvatar:Lcom/google/android/music/AsyncAlbumArtImageView;

.field private mContainerArt:Lcom/google/android/music/AsyncAlbumArtImageView;

.field private mContainerDocument:Lcom/google/android/music/ui/cardlib/model/Document;

.field private mContainerMetadata:Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;

.field private mDescriptionContainer:Landroid/view/View;

.field private mDescriptionContainerOnClickListener:Landroid/view/View$OnClickListener;

.field private mDescriptionContent:Landroid/widget/TextView;

.field private mFabPlay:Landroid/view/View;

.field private mFabPlayOnClickListener:Landroid/view/View$OnClickListener;

.field private mFragment:Lcom/google/android/music/ui/BaseListFragment;

.field private mOverflow:Landroid/view/View;

.field private mOverflowMenuOnClickListener:Landroid/view/View$OnClickListener;

.field private mPinButton:Lcom/google/android/music/KeepOnView;

.field private mSongCount:Landroid/widget/TextView;

.field private mSongList:Lcom/google/android/music/medialist/SongList;

.field private mSubtitle:Landroid/widget/TextView;

.field private mTitle:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/google/android/music/ui/MaterialTrackContainerFragment;->TAG:Ljava/lang/String;

    sput-object v0, Lcom/google/android/music/ui/MaterialContainerHeaderView;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 116
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 57
    new-instance v0, Lcom/google/android/music/ui/MaterialContainerHeaderView$1;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/MaterialContainerHeaderView$1;-><init>(Lcom/google/android/music/ui/MaterialContainerHeaderView;)V

    iput-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView;->mFabPlayOnClickListener:Landroid/view/View$OnClickListener;

    .line 66
    new-instance v0, Lcom/google/android/music/ui/MaterialContainerHeaderView$2;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/MaterialContainerHeaderView$2;-><init>(Lcom/google/android/music/ui/MaterialContainerHeaderView;)V

    iput-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView;->mOverflowMenuOnClickListener:Landroid/view/View$OnClickListener;

    .line 105
    new-instance v0, Lcom/google/android/music/ui/MaterialContainerHeaderView$3;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/MaterialContainerHeaderView$3;-><init>(Lcom/google/android/music/ui/MaterialContainerHeaderView;)V

    iput-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView;->mDescriptionContainerOnClickListener:Landroid/view/View$OnClickListener;

    .line 117
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/ui/MaterialContainerHeaderView;)Lcom/google/android/music/medialist/SongList;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/MaterialContainerHeaderView;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView;->mSongList:Lcom/google/android/music/medialist/SongList;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/music/ui/MaterialContainerHeaderView;)Lcom/google/android/music/ui/cardlib/model/Document;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/MaterialContainerHeaderView;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView;->mContainerDocument:Lcom/google/android/music/ui/cardlib/model/Document;

    return-object v0
.end method

.method static synthetic access$102(Lcom/google/android/music/ui/MaterialContainerHeaderView;Lcom/google/android/music/ui/cardlib/model/Document;)Lcom/google/android/music/ui/cardlib/model/Document;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/MaterialContainerHeaderView;
    .param p1, "x1"    # Lcom/google/android/music/ui/cardlib/model/Document;

    .prologue
    .line 27
    iput-object p1, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView;->mContainerDocument:Lcom/google/android/music/ui/cardlib/model/Document;

    return-object p1
.end method

.method static synthetic access$200(Lcom/google/android/music/ui/MaterialContainerHeaderView;)Lcom/google/android/music/ui/BaseListFragment;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/MaterialContainerHeaderView;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView;->mFragment:Lcom/google/android/music/ui/BaseListFragment;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/music/ui/MaterialContainerHeaderView;)Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/MaterialContainerHeaderView;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView;->mContainerMetadata:Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/music/ui/MaterialContainerHeaderView;)Lcom/google/android/music/AsyncAlbumArtImageView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/MaterialContainerHeaderView;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView;->mContainerArt:Lcom/google/android/music/AsyncAlbumArtImageView;

    return-object v0
.end method

.method private asyncSetContainerArt()V
    .locals 1

    .prologue
    .line 224
    new-instance v0, Lcom/google/android/music/ui/MaterialContainerHeaderView$4;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/MaterialContainerHeaderView$4;-><init>(Lcom/google/android/music/ui/MaterialContainerHeaderView;)V

    invoke-static {v0}, Lcom/google/android/music/utils/MusicUtils;->runAsyncWithCallback(Lcom/google/android/music/utils/async/AsyncRunner;)V

    .line 253
    return-void
.end method

.method private disablePinning()V
    .locals 2

    .prologue
    .line 300
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView;->mPinButton:Lcom/google/android/music/KeepOnView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/music/KeepOnView;->setVisibility(I)V

    .line 301
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView;->mPinButton:Lcom/google/android/music/KeepOnView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/music/KeepOnView;->setOnClick(Z)V

    .line 302
    return-void
.end method

.method private enablePinning()V
    .locals 2

    .prologue
    .line 294
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView;->mPinButton:Lcom/google/android/music/KeepOnView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/music/KeepOnView;->setVisibility(I)V

    .line 295
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView;->mPinButton:Lcom/google/android/music/KeepOnView;

    iget-object v1, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView;->mSongList:Lcom/google/android/music/medialist/SongList;

    invoke-virtual {v0, v1}, Lcom/google/android/music/KeepOnView;->setSongList(Lcom/google/android/music/medialist/SongList;)V

    .line 296
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView;->mPinButton:Lcom/google/android/music/KeepOnView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/music/KeepOnView;->setOnClick(Z)V

    .line 297
    return-void
.end method

.method private isDownloadedOnlyMode()Z
    .locals 1

    .prologue
    .line 305
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/ui/UIStateManager;->isDownloadedOnlyMode()Z

    move-result v0

    return v0
.end method

.method private setAvatar(Ljava/lang/String;)V
    .locals 0
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 329
    return-void
.end method

.method private setSongCount(I)V
    .locals 8
    .param p1, "count"    # I

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 275
    invoke-direct {p0}, Lcom/google/android/music/ui/MaterialContainerHeaderView;->shouldShowSongCount()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 276
    invoke-virtual {p0}, Lcom/google/android/music/ui/MaterialContainerHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/high16 v3, 0x7f120000

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, p1, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 278
    .local v0, "displayCount":Ljava/lang/CharSequence;
    iget-object v2, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView;->mSongList:Lcom/google/android/music/medialist/SongList;

    invoke-virtual {v2}, Lcom/google/android/music/medialist/SongList;->getShouldFilter()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-direct {p0}, Lcom/google/android/music/ui/MaterialContainerHeaderView;->isDownloadedOnlyMode()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 279
    invoke-virtual {p0}, Lcom/google/android/music/ui/MaterialContainerHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0156

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v0, v4, v6

    invoke-virtual {p0}, Lcom/google/android/music/ui/MaterialContainerHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b0155

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 284
    .local v1, "displayText":Ljava/lang/CharSequence;
    iget-object v2, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView;->mSongCount:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 291
    .end local v0    # "displayCount":Ljava/lang/CharSequence;
    .end local v1    # "displayText":Ljava/lang/CharSequence;
    :goto_0
    return-void

    .line 286
    .restart local v0    # "displayCount":Ljava/lang/CharSequence;
    :cond_0
    iget-object v2, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView;->mSongCount:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 289
    .end local v0    # "displayCount":Ljava/lang/CharSequence;
    :cond_1
    iget-object v2, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView;->mSongCount:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private shouldShowSongCount()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 309
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView;->mSongList:Lcom/google/android/music/medialist/SongList;

    if-nez v0, :cond_0

    .line 319
    :goto_0
    return v2

    .line 313
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView;->mSongList:Lcom/google/android/music/medialist/SongList;

    instance-of v0, v0, Lcom/google/android/music/medialist/PlaylistSongList;

    if-eqz v0, :cond_2

    .line 315
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView;->mSongList:Lcom/google/android/music/medialist/SongList;

    check-cast v0, Lcom/google/android/music/medialist/PlaylistSongList;

    invoke-virtual {v0}, Lcom/google/android/music/medialist/PlaylistSongList;->getPlaylistType()I

    move-result v0

    const/16 v3, 0x32

    if-eq v0, v3, :cond_1

    move v0, v1

    :goto_1
    move v2, v0

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v2, v1

    .line 319
    goto :goto_0
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 121
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 124
    const v0, 0x7f0e00f4

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/MaterialContainerHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/AsyncAlbumArtImageView;

    iput-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView;->mAvatar:Lcom/google/android/music/AsyncAlbumArtImageView;

    .line 125
    const v0, 0x7f0e019b

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/MaterialContainerHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/AsyncAlbumArtImageView;

    iput-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView;->mContainerArt:Lcom/google/android/music/AsyncAlbumArtImageView;

    .line 128
    const v0, 0x7f0e00ef

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/MaterialContainerHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/KeepOnView;

    iput-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView;->mPinButton:Lcom/google/android/music/KeepOnView;

    .line 129
    const v0, 0x7f0e019a

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/MaterialContainerHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView;->mFabPlay:Landroid/view/View;

    .line 130
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView;->mFabPlay:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView;->mFabPlayOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 131
    const v0, 0x7f0e00f3

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/MaterialContainerHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView;->mOverflow:Landroid/view/View;

    .line 132
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView;->mOverflow:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView;->mOverflowMenuOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 135
    const v0, 0x7f0e00b8

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/MaterialContainerHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView;->mTitle:Landroid/widget/TextView;

    .line 136
    const v0, 0x7f0e00f5

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/MaterialContainerHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView;->mSubtitle:Landroid/widget/TextView;

    .line 137
    const v0, 0x7f0e00f8

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/MaterialContainerHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView;->mSongCount:Landroid/widget/TextView;

    .line 138
    const v0, 0x7f0e00f6

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/MaterialContainerHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView;->mAlbumYear:Landroid/widget/TextView;

    .line 139
    const v0, 0x7f0e00f7

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/MaterialContainerHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView;->mAlbumYearHyphen:Landroid/widget/TextView;

    .line 141
    const v0, 0x7f0e0141

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/MaterialContainerHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView;->mDescriptionContainer:Landroid/view/View;

    .line 142
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView;->mDescriptionContainer:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView;->mDescriptionContainerOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 143
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView;->mDescriptionContainer:Landroid/view/View;

    const v1, 0x7f0e00d0

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView;->mDescriptionContent:Landroid/widget/TextView;

    .line 144
    return-void
.end method

.method setContainerDocument(Lcom/google/android/music/ui/cardlib/model/Document;)V
    .locals 0
    .param p1, "document"    # Lcom/google/android/music/ui/cardlib/model/Document;

    .prologue
    .line 151
    iput-object p1, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView;->mContainerDocument:Lcom/google/android/music/ui/cardlib/model/Document;

    .line 152
    return-void
.end method

.method setContainerMetadata(Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;)V
    .locals 3
    .param p1, "metadata"    # Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;

    .prologue
    const/4 v2, 0x0

    .line 159
    invoke-static {p1}, Lcom/google/android/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 160
    iput-object p1, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView;->mContainerMetadata:Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;

    .line 162
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView;->mTitle:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;->primaryTitle:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 164
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView;->mSubtitle:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;->secondaryTitle:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 165
    iget-object v0, p1, Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;->description:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 166
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView;->mDescriptionContainer:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 167
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView;->mDescriptionContent:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;->description:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 172
    :goto_0
    iget v0, p1, Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;->albumYear:I

    if-eqz v0, :cond_0

    .line 173
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView;->mAlbumYear:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 174
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView;->mAlbumYearHyphen:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 175
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView;->mAlbumYear:Landroid/widget/TextView;

    iget v1, p1, Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;->albumYear:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 176
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView;->mAlbumYearHyphen:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/music/ui/MaterialContainerHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b003f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 180
    :cond_0
    iget-object v0, p1, Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;->artistUrl:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 181
    iget-object v0, p1, Lcom/google/android/music/ui/MaterialTrackContainerFragment$ContainerMetadata;->artistUrl:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/music/ui/MaterialContainerHeaderView;->setAvatar(Ljava/lang/String;)V

    .line 184
    :cond_1
    return-void

    .line 169
    :cond_2
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView;->mDescriptionContainer:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method setCursor(Landroid/database/Cursor;)V
    .locals 2
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    .line 260
    if-nez p1, :cond_1

    .line 272
    :cond_0
    :goto_0
    return-void

    .line 265
    :cond_1
    invoke-static {p1}, Lcom/google/android/music/utils/MusicUtils;->hasCount(Landroid/database/Cursor;)Z

    move-result v0

    .line 267
    .local v0, "hasCount":Z
    iget-object v1, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView;->mSongCount:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    .line 268
    if-eqz v0, :cond_0

    .line 269
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/google/android/music/ui/MaterialContainerHeaderView;->setSongCount(I)V

    goto :goto_0
.end method

.method setFragment(Lcom/google/android/music/ui/BaseListFragment;)V
    .locals 0
    .param p1, "fragment"    # Lcom/google/android/music/ui/BaseListFragment;

    .prologue
    .line 147
    iput-object p1, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView;->mFragment:Lcom/google/android/music/ui/BaseListFragment;

    .line 148
    return-void
.end method

.method setSongList(Lcom/google/android/music/medialist/SongList;)V
    .locals 3
    .param p1, "list"    # Lcom/google/android/music/medialist/SongList;

    .prologue
    .line 190
    iput-object p1, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView;->mSongList:Lcom/google/android/music/medialist/SongList;

    .line 192
    iget-object v1, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView;->mPinButton:Lcom/google/android/music/KeepOnView;

    if-eqz v1, :cond_0

    .line 193
    iget-object v1, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView;->mSongList:Lcom/google/android/music/medialist/SongList;

    invoke-virtual {p0}, Lcom/google/android/music/ui/MaterialContainerHeaderView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/music/medialist/SongList;->supportsOfflineCaching(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 194
    invoke-direct {p0}, Lcom/google/android/music/ui/MaterialContainerHeaderView;->enablePinning()V

    .line 200
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView;->mContainerArt:Lcom/google/android/music/AsyncAlbumArtImageView;

    if-eqz v1, :cond_1

    .line 201
    invoke-direct {p0}, Lcom/google/android/music/ui/MaterialContainerHeaderView;->asyncSetContainerArt()V

    .line 206
    :cond_1
    iget-object v1, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView;->mSongList:Lcom/google/android/music/medialist/SongList;

    instance-of v1, v1, Lcom/google/android/music/medialist/PlaylistSongList;

    if-eqz v1, :cond_4

    .line 207
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView;->mSongList:Lcom/google/android/music/medialist/SongList;

    check-cast v0, Lcom/google/android/music/medialist/PlaylistSongList;

    .line 208
    .local v0, "playlist":Lcom/google/android/music/medialist/PlaylistSongList;
    invoke-virtual {v0}, Lcom/google/android/music/medialist/PlaylistSongList;->getPlaylistType()I

    move-result v1

    const/16 v2, 0x47

    if-ne v1, v2, :cond_2

    .line 210
    invoke-virtual {v0}, Lcom/google/android/music/medialist/PlaylistSongList;->getOwnerProfilePhotoUrl()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/music/ui/MaterialContainerHeaderView;->setAvatar(Ljava/lang/String;)V

    .line 216
    .end local v0    # "playlist":Lcom/google/android/music/medialist/PlaylistSongList;
    :cond_2
    :goto_1
    return-void

    .line 196
    :cond_3
    invoke-direct {p0}, Lcom/google/android/music/ui/MaterialContainerHeaderView;->disablePinning()V

    goto :goto_0

    .line 212
    :cond_4
    iget-object v1, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView;->mSongList:Lcom/google/android/music/medialist/SongList;

    instance-of v1, v1, Lcom/google/android/music/medialist/SharedWithMeSongList;

    if-eqz v1, :cond_2

    .line 213
    iget-object v0, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView;->mSongList:Lcom/google/android/music/medialist/SongList;

    check-cast v0, Lcom/google/android/music/medialist/SharedWithMeSongList;

    .line 214
    .local v0, "playlist":Lcom/google/android/music/medialist/SharedWithMeSongList;
    invoke-virtual {v0}, Lcom/google/android/music/medialist/SharedWithMeSongList;->getOwnerProfilePhotoUrl()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/music/ui/MaterialContainerHeaderView;->setAvatar(Ljava/lang/String;)V

    goto :goto_1
.end method
