.class public Lcom/google/android/music/xdi/XdiPlayHelper;
.super Ljava/lang/Object;
.source "XdiPlayHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/xdi/XdiPlayHelper$1;,
        Lcom/google/android/music/xdi/XdiPlayHelper$PlaySongListRadioTask;,
        Lcom/google/android/music/xdi/XdiPlayHelper$CompletionListener;
    }
.end annotation


# static fields
.field private static final LOGV:Z


# instance fields
.field private mCompletionListener:Lcom/google/android/music/xdi/XdiPlayHelper$CompletionListener;

.field private final mContext:Landroid/content/Context;

.field private mMixDescriptor:Lcom/google/android/music/mix/MixDescriptor;

.field private mRadioAsyncTask:Lcom/google/android/music/xdi/XdiPlayHelper$PlaySongListRadioTask;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->XDI:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/xdi/XdiPlayHelper;->LOGV:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 101
    iput-object p1, p0, Lcom/google/android/music/xdi/XdiPlayHelper;->mContext:Landroid/content/Context;

    .line 102
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/xdi/XdiPlayHelper;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/xdi/XdiPlayHelper;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/music/xdi/XdiPlayHelper;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/music/xdi/XdiPlayHelper;Lcom/google/android/music/mix/MixDescriptor;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/xdi/XdiPlayHelper;
    .param p1, "x1"    # Lcom/google/android/music/mix/MixDescriptor;

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/google/android/music/xdi/XdiPlayHelper;->playRadio(Lcom/google/android/music/mix/MixDescriptor;)Z

    move-result v0

    return v0
.end method

.method private playMediaList(Lcom/google/android/music/medialist/SongList;Ljava/lang/String;I)V
    .locals 3
    .param p1, "songList"    # Lcom/google/android/music/medialist/SongList;
    .param p2, "playMode"    # Ljava/lang/String;
    .param p3, "offset"    # I

    .prologue
    .line 142
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "play"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 143
    :cond_0
    invoke-static {p1, p3}, Lcom/google/android/music/utils/MusicUtils;->playMediaList(Lcom/google/android/music/medialist/SongList;I)V

    .line 152
    :goto_0
    return-void

    .line 144
    :cond_1
    const-string v0, "shuffle"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 145
    invoke-static {p1}, Lcom/google/android/music/utils/MusicUtils;->shuffleSongs(Lcom/google/android/music/medialist/SongList;)V

    goto :goto_0

    .line 146
    :cond_2
    const-string v0, "queue"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 147
    invoke-static {p1}, Lcom/google/android/music/utils/MusicUtils;->queue(Lcom/google/android/music/medialist/SongList;)V

    goto :goto_0

    .line 149
    :cond_3
    const-string v0, "MusicXdi"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported play mode specified for song list "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private playRadio(Lcom/google/android/music/mix/MixDescriptor;)Z
    .locals 6
    .param p1, "mixDescriptor"    # Lcom/google/android/music/mix/MixDescriptor;

    .prologue
    const/4 v2, 0x0

    .line 569
    invoke-virtual {p1}, Lcom/google/android/music/mix/MixDescriptor;->toString()Ljava/lang/String;

    move-result-object v0

    .line 570
    .local v0, "mixString":Ljava/lang/String;
    const-string v3, "MusicXdi"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Starting playback of radio.  mix: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 571
    sget-object v3, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    if-nez v3, :cond_0

    .line 572
    const-string v3, "MusicXdi"

    const-string v4, "Playback service not initialized."

    invoke-static {v3, v4}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 583
    :goto_0
    return v2

    .line 576
    :cond_0
    :try_start_0
    sget-object v3, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    invoke-interface {v3, p1}, Lcom/google/android/music/playback/IMusicPlaybackService;->openMix(Lcom/google/android/music/mix/MixDescriptor;)V

    .line 577
    iput-object p1, p0, Lcom/google/android/music/xdi/XdiPlayHelper;->mMixDescriptor:Lcom/google/android/music/mix/MixDescriptor;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 583
    const/4 v2, 0x1

    goto :goto_0

    .line 578
    :catch_0
    move-exception v1

    .line 579
    .local v1, "re":Landroid/os/RemoteException;
    const-string v3, "MusicXdi"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error while playing radio.  mix: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private processIntentImpl(Landroid/content/Intent;)Z
    .locals 38
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 163
    sget-boolean v9, Lcom/google/android/music/xdi/XdiPlayHelper;->LOGV:Z

    if-eqz v9, :cond_0

    .line 164
    const-string v9, "MusicXdi"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Processing intent: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    :cond_0
    const-string v9, "container"

    const/4 v10, -0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v9, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 167
    .local v4, "container":I
    const/4 v9, -0x1

    if-ne v4, v9, :cond_1

    .line 168
    const-string v9, "MusicXdi"

    const-string v10, "Container not set in intent."

    invoke-static {v9, v10}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    const/4 v9, 0x0

    .line 541
    :goto_0
    return v9

    .line 171
    :cond_1
    const-string v9, "name"

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 172
    .local v8, "name":Ljava/lang/String;
    const-string v9, "id"

    const-wide/16 v10, -0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v9, v10, v11}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v6

    .line 173
    .local v6, "id":J
    const-string v9, "id_string"

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    .line 174
    .local v21, "idString":Ljava/lang/String;
    const-string v9, "offset"

    const/4 v10, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v9, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v34

    .line 175
    .local v34, "offset":I
    const-string v9, "art_uri"

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 176
    .local v16, "artUri":Ljava/lang/String;
    const-string v9, "play_mode"

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v35

    .line 178
    .local v35, "playMode":Ljava/lang/String;
    packed-switch v4, :pswitch_data_0

    .line 537
    :pswitch_0
    const-string v9, "MusicXdi"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Unexpected container: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    .line 538
    const/4 v9, 0x0

    goto :goto_0

    .line 180
    :pswitch_1
    const-wide/16 v10, -0x1

    cmp-long v9, v6, v10

    if-nez v9, :cond_2

    .line 181
    const-string v9, "MusicXdi"

    const-string v10, "Playlist ID not set in intent."

    invoke-static {v9, v10}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    const/4 v9, 0x0

    goto :goto_0

    .line 184
    :cond_2
    const-string v9, "MusicXdi"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Starting playback of playlist "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    new-instance v5, Lcom/google/android/music/medialist/PlaylistSongList;

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    invoke-direct/range {v5 .. v15}, Lcom/google/android/music/medialist/PlaylistSongList;-><init>(JLjava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    move-object/from16 v0, p0

    move-object/from16 v1, v35

    move/from16 v2, v34

    invoke-direct {v0, v5, v1, v2}, Lcom/google/android/music/xdi/XdiPlayHelper;->playMediaList(Lcom/google/android/music/medialist/SongList;Ljava/lang/String;I)V

    .line 541
    :cond_3
    :goto_1
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 195
    :pswitch_2
    const-wide/16 v10, -0x1

    cmp-long v9, v6, v10

    if-nez v9, :cond_4

    .line 196
    const-string v9, "MusicXdi"

    const-string v10, "Shared playlist ID not set in intent."

    invoke-static {v9, v10}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    const/4 v9, 0x0

    goto/16 :goto_0

    .line 199
    :cond_4
    const-string v9, "MusicXdi"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Starting playback of shared playlist "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    const-string v9, "playlist_share_token"

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 202
    .local v12, "shareToken":Ljava/lang/String;
    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 203
    const-string v9, "MusicXdi"

    const-string v10, "Shared playlist token not set in intent."

    invoke-static {v9, v10}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    const/4 v9, 0x0

    goto/16 :goto_0

    .line 206
    :cond_5
    const-string v9, "description"

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 207
    .local v14, "description":Ljava/lang/String;
    const-string v9, "playlist_share_token"

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 209
    .local v15, "ownerName":Ljava/lang/String;
    const-string v9, "playlist_owner_photo_url"

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 211
    .local v17, "ownerProfilePhotoUrl":Ljava/lang/String;
    new-instance v9, Lcom/google/android/music/medialist/SharedWithMeSongList;

    move-wide v10, v6

    move-object v13, v8

    invoke-direct/range {v9 .. v17}, Lcom/google/android/music/medialist/SharedWithMeSongList;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v35

    move/from16 v2, v34

    invoke-direct {v0, v9, v1, v2}, Lcom/google/android/music/xdi/XdiPlayHelper;->playMediaList(Lcom/google/android/music/medialist/SongList;Ljava/lang/String;I)V

    goto :goto_1

    .line 216
    .end local v12    # "shareToken":Ljava/lang/String;
    .end local v14    # "description":Ljava/lang/String;
    .end local v15    # "ownerName":Ljava/lang/String;
    .end local v17    # "ownerProfilePhotoUrl":Ljava/lang/String;
    :pswitch_3
    const-wide/16 v10, -0x1

    cmp-long v9, v6, v10

    if-nez v9, :cond_6

    .line 217
    const-string v9, "MusicXdi"

    const-string v10, "Album ID not set in intent."

    invoke-static {v9, v10}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    const/4 v9, 0x0

    goto/16 :goto_0

    .line 220
    :cond_6
    const-string v9, "MusicXdi"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Starting playback of album "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    new-instance v9, Lcom/google/android/music/medialist/AlbumSongList;

    const/4 v10, 0x0

    invoke-direct {v9, v6, v7, v10}, Lcom/google/android/music/medialist/AlbumSongList;-><init>(JZ)V

    move-object/from16 v0, p0

    move-object/from16 v1, v35

    move/from16 v2, v34

    invoke-direct {v0, v9, v1, v2}, Lcom/google/android/music/xdi/XdiPlayHelper;->playMediaList(Lcom/google/android/music/medialist/SongList;Ljava/lang/String;I)V

    goto/16 :goto_1

    .line 226
    :pswitch_4
    invoke-static/range {v21 .. v21}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 227
    const-string v9, "MusicXdi"

    const-string v10, "Nautilus album ID not set in intent."

    invoke-static {v9, v10}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    .line 228
    const/4 v9, 0x0

    goto/16 :goto_0

    .line 230
    :cond_7
    const-string v9, "MusicXdi"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Starting playback of Nautilus album "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move-object/from16 v0, v21

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 231
    new-instance v9, Lcom/google/android/music/medialist/NautilusAlbumSongList;

    move-object/from16 v0, v21

    invoke-direct {v9, v0}, Lcom/google/android/music/medialist/NautilusAlbumSongList;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v35

    move/from16 v2, v34

    invoke-direct {v0, v9, v1, v2}, Lcom/google/android/music/xdi/XdiPlayHelper;->playMediaList(Lcom/google/android/music/medialist/SongList;Ljava/lang/String;I)V

    goto/16 :goto_1

    .line 237
    :pswitch_5
    const-wide/16 v10, -0x1

    cmp-long v9, v6, v10

    if-nez v9, :cond_8

    .line 238
    const-string v9, "MusicXdi"

    const-string v10, "Radio ID not set in intent."

    invoke-static {v9, v10}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    .line 239
    const/4 v9, 0x0

    goto/16 :goto_0

    .line 241
    :cond_8
    const-string v9, "MusicXdi"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Starting playback of radio "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 242
    new-instance v19, Lcom/google/android/music/mix/MixDescriptor;

    sget-object v22, Lcom/google/android/music/mix/MixDescriptor$Type;->RADIO:Lcom/google/android/music/mix/MixDescriptor$Type;

    move-wide/from16 v20, v6

    move-object/from16 v23, v8

    move-object/from16 v24, v16

    invoke-direct/range {v19 .. v24}, Lcom/google/android/music/mix/MixDescriptor;-><init>(JLcom/google/android/music/mix/MixDescriptor$Type;Ljava/lang/String;Ljava/lang/String;)V

    .line 243
    .local v19, "mix":Lcom/google/android/music/mix/MixDescriptor;
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Lcom/google/android/music/xdi/XdiPlayHelper;->playRadio(Lcom/google/android/music/mix/MixDescriptor;)Z

    move-result v9

    if-nez v9, :cond_3

    .line 245
    const/4 v9, 0x0

    goto/16 :goto_0

    .line 252
    .end local v19    # "mix":Lcom/google/android/music/mix/MixDescriptor;
    :pswitch_6
    const-wide/16 v10, -0x1

    cmp-long v9, v6, v10

    if-nez v9, :cond_9

    .line 253
    const-string v9, "MusicXdi"

    const-string v10, "Artist local seed ID not set in intent."

    invoke-static {v9, v10}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    .line 254
    const/4 v9, 0x0

    goto/16 :goto_0

    .line 256
    :cond_9
    new-instance v5, Lcom/google/android/music/medialist/ArtistSongList;

    const/4 v9, -0x1

    const/4 v10, 0x0

    invoke-direct/range {v5 .. v10}, Lcom/google/android/music/medialist/ArtistSongList;-><init>(JLjava/lang/String;IZ)V

    .line 258
    .local v5, "songList":Lcom/google/android/music/medialist/SongList;
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/google/android/music/xdi/XdiPlayHelper;->startPlaySongListRadioTask(Lcom/google/android/music/medialist/SongList;)V

    .line 259
    const-string v9, "MusicXdi"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "started radio async task for artist "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 265
    .end local v5    # "songList":Lcom/google/android/music/medialist/SongList;
    :pswitch_7
    const-wide/16 v10, -0x1

    cmp-long v9, v6, v10

    if-nez v9, :cond_a

    .line 266
    const-string v9, "MusicXdi"

    const-string v10, "Album local seed ID not set in intent."

    invoke-static {v9, v10}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    .line 267
    const/4 v9, 0x0

    goto/16 :goto_0

    .line 269
    :cond_a
    new-instance v5, Lcom/google/android/music/medialist/AlbumSongList;

    const/4 v9, 0x0

    invoke-direct {v5, v6, v7, v9}, Lcom/google/android/music/medialist/AlbumSongList;-><init>(JZ)V

    .line 270
    .restart local v5    # "songList":Lcom/google/android/music/medialist/SongList;
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/google/android/music/xdi/XdiPlayHelper;->startPlaySongListRadioTask(Lcom/google/android/music/medialist/SongList;)V

    .line 271
    const-string v9, "MusicXdi"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "started radio async task for album "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 277
    .end local v5    # "songList":Lcom/google/android/music/medialist/SongList;
    :pswitch_8
    const-wide/16 v10, -0x1

    cmp-long v9, v6, v10

    if-nez v9, :cond_b

    .line 278
    const-string v9, "MusicXdi"

    const-string v10, "Track local seed ID not set in intent."

    invoke-static {v9, v10}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    .line 279
    const/4 v9, 0x0

    goto/16 :goto_0

    .line 281
    :cond_b
    invoke-static {v6, v7, v8}, Lcom/google/android/music/store/ContainerDescriptor;->newSingleSongDescriptor(JLjava/lang/String;)Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v20

    .line 282
    .local v20, "descriptor":Lcom/google/android/music/store/ContainerDescriptor;
    new-instance v5, Lcom/google/android/music/medialist/SingleSongList;

    move-object/from16 v0, v20

    invoke-direct {v5, v0, v6, v7, v8}, Lcom/google/android/music/medialist/SingleSongList;-><init>(Lcom/google/android/music/store/ContainerDescriptor;JLjava/lang/String;)V

    .line 283
    .restart local v5    # "songList":Lcom/google/android/music/medialist/SongList;
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/google/android/music/xdi/XdiPlayHelper;->startPlaySongListRadioTask(Lcom/google/android/music/medialist/SongList;)V

    .line 284
    const-string v9, "MusicXdi"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "started radio async task for single track "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 290
    .end local v5    # "songList":Lcom/google/android/music/medialist/SongList;
    .end local v20    # "descriptor":Lcom/google/android/music/store/ContainerDescriptor;
    :pswitch_9
    invoke-static/range {v21 .. v21}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_c

    .line 291
    const-string v9, "MusicXdi"

    const-string v10, "Nautilus artist remote seed ID not set in intent."

    invoke-static {v9, v10}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    .line 293
    const/4 v9, 0x0

    goto/16 :goto_0

    .line 295
    :cond_c
    new-instance v5, Lcom/google/android/music/medialist/NautilusArtistSongList;

    move-object/from16 v0, v21

    invoke-direct {v5, v0, v8}, Lcom/google/android/music/medialist/NautilusArtistSongList;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 296
    .restart local v5    # "songList":Lcom/google/android/music/medialist/SongList;
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/google/android/music/xdi/XdiPlayHelper;->startPlaySongListRadioTask(Lcom/google/android/music/medialist/SongList;)V

    .line 297
    const-string v9, "MusicXdi"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "started radio async task for nautilus artist "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move-object/from16 v0, v21

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 303
    .end local v5    # "songList":Lcom/google/android/music/medialist/SongList;
    :pswitch_a
    invoke-static/range {v21 .. v21}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_d

    .line 304
    const-string v9, "MusicXdi"

    const-string v10, "Nautilus album remote seed ID not set in intent."

    invoke-static {v9, v10}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    .line 306
    const/4 v9, 0x0

    goto/16 :goto_0

    .line 308
    :cond_d
    new-instance v5, Lcom/google/android/music/medialist/NautilusAlbumSongList;

    move-object/from16 v0, v21

    invoke-direct {v5, v0}, Lcom/google/android/music/medialist/NautilusAlbumSongList;-><init>(Ljava/lang/String;)V

    .line 309
    .restart local v5    # "songList":Lcom/google/android/music/medialist/SongList;
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/google/android/music/xdi/XdiPlayHelper;->startPlaySongListRadioTask(Lcom/google/android/music/medialist/SongList;)V

    .line 310
    const-string v9, "MusicXdi"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "started radio async task for nautilus album "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move-object/from16 v0, v21

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 316
    .end local v5    # "songList":Lcom/google/android/music/medialist/SongList;
    :pswitch_b
    invoke-static/range {v21 .. v21}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_e

    .line 317
    const-string v9, "MusicXdi"

    const-string v10, "Nautilus track remote seed ID not set in intent."

    invoke-static {v9, v10}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    .line 319
    const/4 v9, 0x0

    goto/16 :goto_0

    .line 321
    :cond_e
    invoke-static {}, Lcom/google/android/music/store/ContainerDescriptor;->newUnknownContainerDescriptor()Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v20

    .line 322
    .restart local v20    # "descriptor":Lcom/google/android/music/store/ContainerDescriptor;
    new-instance v5, Lcom/google/android/music/medialist/NautilusSingleSongList;

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-direct {v5, v0, v1, v8}, Lcom/google/android/music/medialist/NautilusSingleSongList;-><init>(Lcom/google/android/music/store/ContainerDescriptor;Ljava/lang/String;Ljava/lang/String;)V

    .line 323
    .restart local v5    # "songList":Lcom/google/android/music/medialist/SongList;
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/google/android/music/xdi/XdiPlayHelper;->startPlaySongListRadioTask(Lcom/google/android/music/medialist/SongList;)V

    .line 324
    const-string v9, "MusicXdi"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "started radio async task for single nautilus track "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move-object/from16 v0, v21

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 331
    .end local v5    # "songList":Lcom/google/android/music/medialist/SongList;
    .end local v20    # "descriptor":Lcom/google/android/music/store/ContainerDescriptor;
    :pswitch_c
    invoke-static/range {v21 .. v21}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_f

    .line 332
    const-string v9, "MusicXdi"

    const-string v10, "Artist remote seed ID not set in intent."

    invoke-static {v9, v10}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    .line 334
    const/4 v9, 0x0

    goto/16 :goto_0

    .line 338
    :cond_f
    invoke-static/range {v21 .. v21}, Lcom/google/android/music/utils/MusicUtils;->isNautilusId(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_10

    .line 339
    new-instance v5, Lcom/google/android/music/medialist/NautilusArtistSongList;

    move-object/from16 v0, v21

    invoke-direct {v5, v0, v8}, Lcom/google/android/music/medialist/NautilusArtistSongList;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 351
    .restart local v5    # "songList":Lcom/google/android/music/medialist/SongList;
    :goto_2
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/google/android/music/xdi/XdiPlayHelper;->startPlaySongListRadioTask(Lcom/google/android/music/medialist/SongList;)V

    .line 352
    const-string v9, "MusicXdi"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "started radio async task for artist "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move-object/from16 v0, v21

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 342
    .end local v5    # "songList":Lcom/google/android/music/medialist/SongList;
    :cond_10
    :try_start_0
    invoke-static/range {v21 .. v21}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v6

    .line 347
    new-instance v5, Lcom/google/android/music/medialist/ArtistSongList;

    const/4 v9, -0x1

    const/4 v10, 0x0

    invoke-direct/range {v5 .. v10}, Lcom/google/android/music/medialist/ArtistSongList;-><init>(JLjava/lang/String;IZ)V

    .restart local v5    # "songList":Lcom/google/android/music/medialist/SongList;
    goto :goto_2

    .line 343
    .end local v5    # "songList":Lcom/google/android/music/medialist/SongList;
    :catch_0
    move-exception v18

    .line 344
    .local v18, "e":Ljava/lang/NumberFormatException;
    const-string v9, "MusicXdi"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Error parsing long value from "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move-object/from16 v0, v21

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    .line 345
    const/4 v9, 0x0

    goto/16 :goto_0

    .line 358
    .end local v18    # "e":Ljava/lang/NumberFormatException;
    :pswitch_d
    invoke-static/range {v21 .. v21}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_11

    .line 359
    const-string v9, "MusicXdi"

    const-string v10, "Album remote seed ID not set in intent."

    invoke-static {v9, v10}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    .line 361
    const/4 v9, 0x0

    goto/16 :goto_0

    .line 365
    :cond_11
    invoke-static/range {v21 .. v21}, Lcom/google/android/music/utils/MusicUtils;->isNautilusId(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_12

    .line 366
    new-instance v5, Lcom/google/android/music/medialist/NautilusAlbumSongList;

    move-object/from16 v0, v21

    invoke-direct {v5, v0}, Lcom/google/android/music/medialist/NautilusAlbumSongList;-><init>(Ljava/lang/String;)V

    .line 377
    .restart local v5    # "songList":Lcom/google/android/music/medialist/SongList;
    :goto_3
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/google/android/music/xdi/XdiPlayHelper;->startPlaySongListRadioTask(Lcom/google/android/music/medialist/SongList;)V

    .line 378
    const-string v9, "MusicXdi"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "started radio async task for album "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move-object/from16 v0, v21

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 369
    .end local v5    # "songList":Lcom/google/android/music/medialist/SongList;
    :cond_12
    :try_start_1
    invoke-static/range {v21 .. v21}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-wide v6

    .line 374
    new-instance v5, Lcom/google/android/music/medialist/AlbumSongList;

    const/4 v9, 0x0

    invoke-direct {v5, v6, v7, v9}, Lcom/google/android/music/medialist/AlbumSongList;-><init>(JZ)V

    .restart local v5    # "songList":Lcom/google/android/music/medialist/SongList;
    goto :goto_3

    .line 370
    .end local v5    # "songList":Lcom/google/android/music/medialist/SongList;
    :catch_1
    move-exception v18

    .line 371
    .restart local v18    # "e":Ljava/lang/NumberFormatException;
    const-string v9, "MusicXdi"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Error parsing long value from "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move-object/from16 v0, v21

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    .line 372
    const/4 v9, 0x0

    goto/16 :goto_0

    .line 384
    .end local v18    # "e":Ljava/lang/NumberFormatException;
    :pswitch_e
    invoke-static/range {v21 .. v21}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_13

    .line 385
    const-string v9, "MusicXdi"

    const-string v10, "Track remote seed ID not set in intent."

    invoke-static {v9, v10}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    .line 387
    const/4 v9, 0x0

    goto/16 :goto_0

    .line 391
    :cond_13
    invoke-static/range {v21 .. v21}, Lcom/google/android/music/utils/MusicUtils;->isNautilusId(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_14

    .line 392
    invoke-static {}, Lcom/google/android/music/store/ContainerDescriptor;->newUnknownContainerDescriptor()Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v20

    .line 393
    .restart local v20    # "descriptor":Lcom/google/android/music/store/ContainerDescriptor;
    new-instance v5, Lcom/google/android/music/medialist/NautilusSingleSongList;

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-direct {v5, v0, v1, v8}, Lcom/google/android/music/medialist/NautilusSingleSongList;-><init>(Lcom/google/android/music/store/ContainerDescriptor;Ljava/lang/String;Ljava/lang/String;)V

    .line 405
    .restart local v5    # "songList":Lcom/google/android/music/medialist/SongList;
    :goto_4
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/google/android/music/xdi/XdiPlayHelper;->startPlaySongListRadioTask(Lcom/google/android/music/medialist/SongList;)V

    .line 406
    const-string v9, "MusicXdi"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "started radio async task for single track "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move-object/from16 v0, v21

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 396
    .end local v5    # "songList":Lcom/google/android/music/medialist/SongList;
    .end local v20    # "descriptor":Lcom/google/android/music/store/ContainerDescriptor;
    :cond_14
    :try_start_2
    invoke-static/range {v21 .. v21}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_2

    move-result-wide v6

    .line 401
    invoke-static {v6, v7, v8}, Lcom/google/android/music/store/ContainerDescriptor;->newSingleSongDescriptor(JLjava/lang/String;)Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v20

    .line 402
    .restart local v20    # "descriptor":Lcom/google/android/music/store/ContainerDescriptor;
    new-instance v5, Lcom/google/android/music/medialist/SingleSongList;

    move-object/from16 v0, v20

    invoke-direct {v5, v0, v6, v7, v8}, Lcom/google/android/music/medialist/SingleSongList;-><init>(Lcom/google/android/music/store/ContainerDescriptor;JLjava/lang/String;)V

    .restart local v5    # "songList":Lcom/google/android/music/medialist/SongList;
    goto :goto_4

    .line 397
    .end local v5    # "songList":Lcom/google/android/music/medialist/SongList;
    .end local v20    # "descriptor":Lcom/google/android/music/store/ContainerDescriptor;
    :catch_2
    move-exception v18

    .line 398
    .restart local v18    # "e":Ljava/lang/NumberFormatException;
    const-string v9, "MusicXdi"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Error parsing long value from "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move-object/from16 v0, v21

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    .line 399
    const/4 v9, 0x0

    goto/16 :goto_0

    .line 412
    .end local v18    # "e":Ljava/lang/NumberFormatException;
    :pswitch_f
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/music/xdi/XdiPlayHelper;->mContext:Landroid/content/Context;

    invoke-static {v9}, Lcom/google/android/music/mix/MixDescriptor;->getLuckyRadio(Landroid/content/Context;)Lcom/google/android/music/mix/MixDescriptor;

    move-result-object v9

    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/google/android/music/xdi/XdiPlayHelper;->playRadio(Lcom/google/android/music/mix/MixDescriptor;)Z

    .line 413
    const-string v9, "MusicXdi"

    const-string v10, "started I am feeling lucky radio."

    invoke-static {v9, v10}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 419
    :pswitch_10
    invoke-static/range {v21 .. v21}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_15

    .line 420
    const-string v9, "MusicXdi"

    const-string v10, "Genre remote seed ID not set in intent."

    invoke-static {v9, v10}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    .line 421
    const/4 v9, 0x0

    goto/16 :goto_0

    .line 424
    :cond_15
    new-instance v20, Lcom/google/android/music/mix/MixDescriptor;

    sget-object v22, Lcom/google/android/music/mix/MixDescriptor$Type;->GENRE_SEED:Lcom/google/android/music/mix/MixDescriptor$Type;

    const/16 v25, 0x0

    move-object/from16 v23, v8

    move-object/from16 v24, v16

    invoke-direct/range {v20 .. v25}, Lcom/google/android/music/mix/MixDescriptor;-><init>(Ljava/lang/String;Lcom/google/android/music/mix/MixDescriptor$Type;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 426
    .local v20, "descriptor":Lcom/google/android/music/mix/MixDescriptor;
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Lcom/google/android/music/xdi/XdiPlayHelper;->playRadio(Lcom/google/android/music/mix/MixDescriptor;)Z

    .line 427
    const-string v9, "MusicXdi"

    const-string v10, "started Genre radio."

    invoke-static {v9, v10}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 432
    .end local v20    # "descriptor":Lcom/google/android/music/mix/MixDescriptor;
    :pswitch_11
    const-wide/16 v10, -0x1

    cmp-long v9, v6, v10

    if-nez v9, :cond_16

    .line 433
    const-string v9, "MusicXdi"

    const-string v10, "Suggested mix ID not set in intent."

    invoke-static {v9, v10}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    .line 434
    const/4 v9, 0x0

    goto/16 :goto_0

    .line 436
    :cond_16
    const-string v9, "MusicXdi"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Starting playback of suggested mix "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 437
    new-instance v23, Lcom/google/android/music/medialist/PlaylistSongList;

    const/16 v27, 0x32

    const/16 v28, 0x0

    const/16 v29, 0x0

    const/16 v30, 0x0

    const/16 v31, 0x0

    const/16 v32, 0x0

    const/16 v33, 0x0

    move-wide/from16 v24, v6

    move-object/from16 v26, v8

    invoke-direct/range {v23 .. v33}, Lcom/google/android/music/medialist/PlaylistSongList;-><init>(JLjava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    const/4 v9, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    move-object/from16 v2, v35

    invoke-direct {v0, v1, v2, v9}, Lcom/google/android/music/xdi/XdiPlayHelper;->playMediaList(Lcom/google/android/music/medialist/SongList;Ljava/lang/String;I)V

    goto/16 :goto_1

    .line 445
    :pswitch_12
    const-wide/16 v10, -0x1

    cmp-long v9, v6, v10

    if-nez v9, :cond_17

    .line 446
    const-string v9, "MusicXdi"

    const-string v10, "Track ID not set in intent."

    invoke-static {v9, v10}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    .line 447
    const/4 v9, 0x0

    goto/16 :goto_0

    .line 449
    :cond_17
    const-string v9, "MusicXdi"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Starting playback of track "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 450
    invoke-static {v6, v7, v8}, Lcom/google/android/music/store/ContainerDescriptor;->newSingleSongDescriptor(JLjava/lang/String;)Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v20

    .line 451
    .local v20, "descriptor":Lcom/google/android/music/store/ContainerDescriptor;
    new-instance v9, Lcom/google/android/music/medialist/SingleSongList;

    move-object/from16 v0, v20

    invoke-direct {v9, v0, v6, v7, v8}, Lcom/google/android/music/medialist/SingleSongList;-><init>(Lcom/google/android/music/store/ContainerDescriptor;JLjava/lang/String;)V

    const/4 v10, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v35

    invoke-direct {v0, v9, v1, v10}, Lcom/google/android/music/xdi/XdiPlayHelper;->playMediaList(Lcom/google/android/music/medialist/SongList;Ljava/lang/String;I)V

    goto/16 :goto_1

    .line 456
    .end local v20    # "descriptor":Lcom/google/android/music/store/ContainerDescriptor;
    :pswitch_13
    invoke-static/range {v21 .. v21}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_18

    .line 457
    const-string v9, "MusicXdi"

    const-string v10, "Nautilus track ID not set in intent."

    invoke-static {v9, v10}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    .line 458
    const/4 v9, 0x0

    goto/16 :goto_0

    .line 460
    :cond_18
    const-string v9, "MusicXdi"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Starting playback of Nautilus track "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move-object/from16 v0, v21

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 461
    invoke-static {}, Lcom/google/android/music/store/ContainerDescriptor;->newUnknownContainerDescriptor()Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v20

    .line 462
    .restart local v20    # "descriptor":Lcom/google/android/music/store/ContainerDescriptor;
    new-instance v9, Lcom/google/android/music/medialist/NautilusSingleSongList;

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-direct {v9, v0, v1, v8}, Lcom/google/android/music/medialist/NautilusSingleSongList;-><init>(Lcom/google/android/music/store/ContainerDescriptor;Ljava/lang/String;Ljava/lang/String;)V

    const/4 v10, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v35

    invoke-direct {v0, v9, v1, v10}, Lcom/google/android/music/xdi/XdiPlayHelper;->playMediaList(Lcom/google/android/music/medialist/SongList;Ljava/lang/String;I)V

    goto/16 :goto_1

    .line 468
    .end local v20    # "descriptor":Lcom/google/android/music/store/ContainerDescriptor;
    :pswitch_14
    const-string v9, "song_list"

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v37

    .line 469
    .local v37, "songListString":Ljava/lang/String;
    invoke-static/range {v37 .. v37}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_19

    .line 470
    const-string v9, "MusicXdi"

    const-string v10, "Nautilus song list not set in intent."

    invoke-static {v9, v10}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    .line 471
    const/4 v9, 0x0

    goto/16 :goto_0

    .line 473
    :cond_19
    invoke-static/range {v37 .. v37}, Lcom/google/android/music/medialist/MediaList;->thaw(Ljava/lang/String;)Lcom/google/android/music/medialist/MediaList;

    move-result-object v36

    check-cast v36, Lcom/google/android/music/medialist/NautilusSelectedSongList;

    .line 475
    .local v36, "selectedSongList":Lcom/google/android/music/medialist/NautilusSelectedSongList;
    if-nez v36, :cond_1a

    .line 476
    const-string v9, "MusicXdi"

    const-string v10, "Error deserializing Nautilus song list in intent."

    invoke-static {v9, v10}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    .line 478
    const/4 v9, 0x0

    goto/16 :goto_0

    .line 480
    :cond_1a
    const-string v9, "MusicXdi"

    const-string v10, "Starting playback of Nautilus song list."

    invoke-static {v9, v10}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 481
    move-object/from16 v0, p0

    move-object/from16 v1, v36

    move-object/from16 v2, v35

    move/from16 v3, v34

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/music/xdi/XdiPlayHelper;->playMediaList(Lcom/google/android/music/medialist/SongList;Ljava/lang/String;I)V

    goto/16 :goto_1

    .line 487
    .end local v36    # "selectedSongList":Lcom/google/android/music/medialist/NautilusSelectedSongList;
    .end local v37    # "songListString":Ljava/lang/String;
    :pswitch_15
    const-string v9, "song_list"

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v37

    .line 488
    .restart local v37    # "songListString":Ljava/lang/String;
    invoke-static/range {v37 .. v37}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_1b

    .line 489
    const-string v9, "MusicXdi"

    const-string v10, "Local song list not set in intent."

    invoke-static {v9, v10}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    .line 490
    const/4 v9, 0x0

    goto/16 :goto_0

    .line 492
    :cond_1b
    invoke-static/range {v37 .. v37}, Lcom/google/android/music/medialist/MediaList;->thaw(Ljava/lang/String;)Lcom/google/android/music/medialist/MediaList;

    move-result-object v36

    check-cast v36, Lcom/google/android/music/medialist/SelectedSongList;

    .line 494
    .local v36, "selectedSongList":Lcom/google/android/music/medialist/SelectedSongList;
    if-nez v36, :cond_1c

    .line 495
    const-string v9, "MusicXdi"

    const-string v10, "Error deserializing song list in intent."

    invoke-static {v9, v10}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    .line 496
    const/4 v9, 0x0

    goto/16 :goto_0

    .line 498
    :cond_1c
    const-string v9, "MusicXdi"

    const-string v10, "Starting playback of song list."

    invoke-static {v9, v10}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 499
    move-object/from16 v0, p0

    move-object/from16 v1, v36

    move-object/from16 v2, v35

    move/from16 v3, v34

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/music/xdi/XdiPlayHelper;->playMediaList(Lcom/google/android/music/medialist/SongList;Ljava/lang/String;I)V

    goto/16 :goto_1

    .line 505
    .end local v36    # "selectedSongList":Lcom/google/android/music/medialist/SelectedSongList;
    .end local v37    # "songListString":Ljava/lang/String;
    :pswitch_16
    const-wide/16 v10, -0x1

    cmp-long v9, v6, v10

    if-nez v9, :cond_1d

    .line 506
    const-string v9, "MusicXdi"

    const-string v10, "Artist ID not set in intent."

    invoke-static {v9, v10}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    .line 507
    const/4 v9, 0x0

    goto/16 :goto_0

    .line 509
    :cond_1d
    const-string v9, "MusicXdi"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Starting playback of songs for artist "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 510
    new-instance v5, Lcom/google/android/music/medialist/ArtistSongList;

    const/4 v9, -0x1

    const/4 v10, 0x0

    invoke-direct/range {v5 .. v10}, Lcom/google/android/music/medialist/ArtistSongList;-><init>(JLjava/lang/String;IZ)V

    .line 512
    .restart local v5    # "songList":Lcom/google/android/music/medialist/SongList;
    const/4 v9, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v35

    invoke-direct {v0, v5, v1, v9}, Lcom/google/android/music/xdi/XdiPlayHelper;->playMediaList(Lcom/google/android/music/medialist/SongList;Ljava/lang/String;I)V

    goto/16 :goto_1

    .line 518
    .end local v5    # "songList":Lcom/google/android/music/medialist/SongList;
    :pswitch_17
    invoke-static/range {v21 .. v21}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_1e

    .line 519
    const-string v9, "MusicXdi"

    const-string v10, "Nautilus artist ID not set in intent."

    invoke-static {v9, v10}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    .line 520
    const/4 v9, 0x0

    goto/16 :goto_0

    .line 522
    :cond_1e
    const-string v9, "MusicXdi"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Starting playback of songs for Nautilus artist "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move-object/from16 v0, v21

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 524
    new-instance v5, Lcom/google/android/music/medialist/NautilusArtistSongList;

    move-object/from16 v0, v21

    invoke-direct {v5, v0, v8}, Lcom/google/android/music/medialist/NautilusArtistSongList;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 525
    .restart local v5    # "songList":Lcom/google/android/music/medialist/SongList;
    const/4 v9, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v35

    invoke-direct {v0, v5, v1, v9}, Lcom/google/android/music/xdi/XdiPlayHelper;->playMediaList(Lcom/google/android/music/medialist/SongList;Ljava/lang/String;I)V

    goto/16 :goto_1

    .line 531
    .end local v5    # "songList":Lcom/google/android/music/medialist/SongList;
    :pswitch_18
    const-string v9, "MusicXdi"

    const-string v10, "Starting shuffle all my songs"

    invoke-static {v9, v10}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 532
    invoke-static {}, Lcom/google/android/music/utils/MusicUtils;->shuffleAll()V

    goto/16 :goto_1

    .line 178
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_1
        :pswitch_11
        :pswitch_5
        :pswitch_4
        :pswitch_12
        :pswitch_13
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_2
        :pswitch_14
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_16
        :pswitch_17
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_0
        :pswitch_15
        :pswitch_18
    .end packed-switch
.end method

.method private startPlaySongListRadioTask(Lcom/google/android/music/medialist/SongList;)V
    .locals 3
    .param p1, "songList"    # Lcom/google/android/music/medialist/SongList;

    .prologue
    .line 550
    invoke-direct {p0}, Lcom/google/android/music/xdi/XdiPlayHelper;->stopPlaySongListRadioTaskIfRunning()V

    .line 552
    new-instance v0, Lcom/google/android/music/xdi/XdiPlayHelper$PlaySongListRadioTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/music/xdi/XdiPlayHelper$PlaySongListRadioTask;-><init>(Lcom/google/android/music/xdi/XdiPlayHelper;Lcom/google/android/music/xdi/XdiPlayHelper$1;)V

    iput-object v0, p0, Lcom/google/android/music/xdi/XdiPlayHelper;->mRadioAsyncTask:Lcom/google/android/music/xdi/XdiPlayHelper$PlaySongListRadioTask;

    .line 553
    iget-object v0, p0, Lcom/google/android/music/xdi/XdiPlayHelper;->mRadioAsyncTask:Lcom/google/android/music/xdi/XdiPlayHelper$PlaySongListRadioTask;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/music/medialist/SongList;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/music/xdi/XdiPlayHelper$PlaySongListRadioTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 554
    return-void
.end method

.method private stopPlaySongListRadioTaskIfRunning()V
    .locals 2

    .prologue
    .line 560
    iget-object v0, p0, Lcom/google/android/music/xdi/XdiPlayHelper;->mRadioAsyncTask:Lcom/google/android/music/xdi/XdiPlayHelper$PlaySongListRadioTask;

    if-eqz v0, :cond_1

    .line 561
    iget-object v0, p0, Lcom/google/android/music/xdi/XdiPlayHelper;->mRadioAsyncTask:Lcom/google/android/music/xdi/XdiPlayHelper$PlaySongListRadioTask;

    invoke-virtual {v0}, Lcom/google/android/music/xdi/XdiPlayHelper$PlaySongListRadioTask;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 562
    iget-object v0, p0, Lcom/google/android/music/xdi/XdiPlayHelper;->mRadioAsyncTask:Lcom/google/android/music/xdi/XdiPlayHelper$PlaySongListRadioTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/music/xdi/XdiPlayHelper$PlaySongListRadioTask;->cancel(Z)Z

    .line 564
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/xdi/XdiPlayHelper;->mRadioAsyncTask:Lcom/google/android/music/xdi/XdiPlayHelper$PlaySongListRadioTask;

    .line 566
    :cond_1
    return-void
.end method


# virtual methods
.method public callCompletionListener(Z)V
    .locals 1
    .param p1, "status"    # Z

    .prologue
    .line 125
    iget-object v0, p0, Lcom/google/android/music/xdi/XdiPlayHelper;->mCompletionListener:Lcom/google/android/music/xdi/XdiPlayHelper$CompletionListener;

    if-eqz v0, :cond_0

    .line 126
    iget-object v0, p0, Lcom/google/android/music/xdi/XdiPlayHelper;->mCompletionListener:Lcom/google/android/music/xdi/XdiPlayHelper$CompletionListener;

    invoke-interface {v0, p1}, Lcom/google/android/music/xdi/XdiPlayHelper$CompletionListener;->onComplete(Z)V

    .line 128
    :cond_0
    return-void
.end method

.method public cleanup()V
    .locals 0

    .prologue
    .line 110
    invoke-direct {p0}, Lcom/google/android/music/xdi/XdiPlayHelper;->stopPlaySongListRadioTaskIfRunning()V

    .line 111
    return-void
.end method

.method public processIntent(Landroid/content/Intent;)V
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 155
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/music/xdi/XdiPlayHelper;->mMixDescriptor:Lcom/google/android/music/mix/MixDescriptor;

    .line 156
    invoke-direct {p0, p1}, Lcom/google/android/music/xdi/XdiPlayHelper;->processIntentImpl(Landroid/content/Intent;)Z

    move-result v0

    .line 157
    .local v0, "status":Z
    iget-object v1, p0, Lcom/google/android/music/xdi/XdiPlayHelper;->mRadioAsyncTask:Lcom/google/android/music/xdi/XdiPlayHelper$PlaySongListRadioTask;

    if-eqz v1, :cond_0

    .line 158
    invoke-virtual {p0, v0}, Lcom/google/android/music/xdi/XdiPlayHelper;->callCompletionListener(Z)V

    .line 160
    :cond_0
    return-void
.end method

.method public setCompletionListener(Lcom/google/android/music/xdi/XdiPlayHelper$CompletionListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/google/android/music/xdi/XdiPlayHelper$CompletionListener;

    .prologue
    .line 121
    iput-object p1, p0, Lcom/google/android/music/xdi/XdiPlayHelper;->mCompletionListener:Lcom/google/android/music/xdi/XdiPlayHelper$CompletionListener;

    .line 122
    return-void
.end method
