.class public Lcom/google/android/music/MusicListView;
.super Landroid/widget/ListView;
.source "MusicListView.java"


# instance fields
.field mDrawingFix:Z

.field private mHeaderViews:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v2, 0x1

    .line 34
    invoke-direct {p0, p1, p2}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    iput-boolean v2, p0, Lcom/google/android/music/MusicListView;->mDrawingFix:Z

    .line 31
    invoke-static {}, Lcom/google/common/collect/Lists;->newLinkedList()Ljava/util/LinkedList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/MusicListView;->mHeaderViews:Ljava/util/List;

    .line 35
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "music_overdraw_listview"

    invoke-static {v0, v1, v2}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/music/MusicListView;->mDrawingFix:Z

    .line 37
    return-void
.end method


# virtual methods
.method public addHeaderView(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 51
    invoke-super {p0, p1}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    .line 52
    iget-object v0, p0, Lcom/google/android/music/MusicListView;->mHeaderViews:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 53
    return-void
.end method

.method public addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;
    .param p2, "data"    # Ljava/lang/Object;
    .param p3, "isSelectable"    # Z

    .prologue
    .line 43
    invoke-super {p0, p1, p2, p3}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 44
    iget-object v0, p0, Lcom/google/android/music/MusicListView;->mHeaderViews:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 45
    return-void
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 8
    .param p1, "c"    # Landroid/graphics/Canvas;

    .prologue
    .line 63
    iget-boolean v7, p0, Lcom/google/android/music/MusicListView;->mDrawingFix:Z

    if-eqz v7, :cond_4

    .line 65
    invoke-virtual {p0}, Lcom/google/android/music/MusicListView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 66
    .local v3, "d":Landroid/graphics/drawable/Drawable;
    if-eqz v3, :cond_0

    .line 67
    invoke-virtual {v3, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 69
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/music/MusicListView;->getChildCount()I

    move-result v2

    .line 70
    .local v2, "count":I
    add-int/lit8 v7, v2, -0x1

    invoke-virtual {p0, v7}, Lcom/google/android/music/MusicListView;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    .line 71
    .local v6, "v":Landroid/view/View;
    if-eqz v6, :cond_3

    .line 72
    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5}, Landroid/graphics/Rect;-><init>()V

    .line 73
    .local v5, "r":Landroid/graphics/Rect;
    invoke-virtual {p0, v5}, Lcom/google/android/music/MusicListView;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 74
    invoke-virtual {v6}, Landroid/view/View;->getBottom()I

    move-result v0

    .line 75
    .local v0, "bottom":I
    iput v0, v5, Landroid/graphics/Rect;->bottom:I

    .line 76
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getClipBounds()Landroid/graphics/Rect;

    move-result-object v1

    .line 77
    .local v1, "clip":Landroid/graphics/Rect;
    invoke-virtual {v1, v5}, Landroid/graphics/Rect;->intersect(Landroid/graphics/Rect;)Z

    .line 79
    invoke-virtual {v1}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_2

    .line 80
    invoke-virtual {p0}, Lcom/google/android/music/MusicListView;->getDivider()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 81
    if-eqz v3, :cond_1

    .line 82
    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v4

    .line 83
    .local v4, "db":Landroid/graphics/Rect;
    iget v7, v4, Landroid/graphics/Rect;->left:I

    invoke-virtual {v4, v7, v0}, Landroid/graphics/Rect;->offsetTo(II)V

    .line 84
    invoke-virtual {v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 85
    invoke-virtual {v3, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 87
    .end local v4    # "db":Landroid/graphics/Rect;
    :cond_1
    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;)Z

    .line 89
    :cond_2
    invoke-super {p0, p1}, Landroid/widget/ListView;->draw(Landroid/graphics/Canvas;)V

    .line 94
    .end local v0    # "bottom":I
    .end local v1    # "clip":Landroid/graphics/Rect;
    .end local v2    # "count":I
    .end local v3    # "d":Landroid/graphics/drawable/Drawable;
    .end local v5    # "r":Landroid/graphics/Rect;
    .end local v6    # "v":Landroid/view/View;
    :cond_3
    :goto_0
    return-void

    .line 92
    :cond_4
    invoke-super {p0, p1}, Landroid/widget/ListView;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method public invalidateViews()V
    .locals 0

    .prologue
    .line 139
    invoke-static {}, Lcom/google/android/music/utils/MusicUtils;->assertUiThread()V

    .line 140
    invoke-super {p0}, Landroid/widget/ListView;->invalidateViews()V

    .line 141
    return-void
.end method

.method protected onFocusChanged(ZILandroid/graphics/Rect;)V
    .locals 8
    .param p1, "gainFocus"    # Z
    .param p2, "direction"    # I
    .param p3, "previouslyFocusedRect"    # Landroid/graphics/Rect;

    .prologue
    .line 98
    if-eqz p1, :cond_3

    .line 108
    invoke-virtual {p0}, Lcom/google/android/music/MusicListView;->getFirstVisiblePosition()I

    move-result v3

    .line 109
    .local v3, "oldFirstVisiblePosition":I
    invoke-virtual {p0}, Lcom/google/android/music/MusicListView;->getChildCount()I

    move-result v2

    .line 110
    .local v2, "oldChildCount":I
    new-array v1, v2, [I

    .line 111
    .local v1, "mOldChildPositions":[I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_1

    .line 112
    invoke-virtual {p0, v0}, Lcom/google/android/music/MusicListView;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 113
    .local v5, "v":Landroid/view/View;
    if-nez v5, :cond_0

    const/4 v7, 0x0

    :goto_1
    aput v7, v1, v0

    .line 111
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 113
    :cond_0
    invoke-virtual {v5}, Landroid/view/View;->getTop()I

    move-result v7

    goto :goto_1

    .line 116
    .end local v5    # "v":Landroid/view/View;
    :cond_1
    invoke-super {p0, p1, p2, p3}, Landroid/widget/ListView;->onFocusChanged(ZILandroid/graphics/Rect;)V

    .line 122
    invoke-virtual {p0}, Lcom/google/android/music/MusicListView;->getSelectedItemPosition()I

    move-result v4

    .line 123
    .local v4, "selectedItemPosition":I
    sub-int v0, v4, v3

    .line 124
    if-ltz v0, :cond_2

    if-ge v0, v2, :cond_2

    .line 125
    aget v6, v1, v0

    .line 126
    .local v6, "y":I
    invoke-virtual {p0, v4, v6}, Lcom/google/android/music/MusicListView;->setSelectionFromTop(II)V

    .line 131
    .end local v0    # "i":I
    .end local v1    # "mOldChildPositions":[I
    .end local v2    # "oldChildCount":I
    .end local v3    # "oldFirstVisiblePosition":I
    .end local v4    # "selectedItemPosition":I
    .end local v6    # "y":I
    :cond_2
    :goto_2
    return-void

    .line 129
    :cond_3
    invoke-super {p0, p1, p2, p3}, Landroid/widget/ListView;->onFocusChanged(ZILandroid/graphics/Rect;)V

    goto :goto_2
.end method
