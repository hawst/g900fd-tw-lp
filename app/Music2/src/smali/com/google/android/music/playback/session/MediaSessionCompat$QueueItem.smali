.class public final Lcom/google/android/music/playback/session/MediaSessionCompat$QueueItem;
.super Ljava/lang/Object;
.source "MediaSessionCompat.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/playback/session/MediaSessionCompat;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "QueueItem"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/music/playback/session/MediaSessionCompat$QueueItem;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mDescription:Lcom/google/android/music/playback/session/MediaDescriptionCompat;

.field private final mId:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 567
    new-instance v0, Lcom/google/android/music/playback/session/MediaSessionCompat$QueueItem$1;

    invoke-direct {v0}, Lcom/google/android/music/playback/session/MediaSessionCompat$QueueItem$1;-><init>()V

    sput-object v0, Lcom/google/android/music/playback/session/MediaSessionCompat$QueueItem;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 537
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 538
    sget-object v0, Lcom/google/android/music/playback/session/MediaDescriptionCompat;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/session/MediaDescriptionCompat;

    iput-object v0, p0, Lcom/google/android/music/playback/session/MediaSessionCompat$QueueItem;->mDescription:Lcom/google/android/music/playback/session/MediaDescriptionCompat;

    .line 539
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/music/playback/session/MediaSessionCompat$QueueItem;->mId:J

    .line 540
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/google/android/music/playback/session/MediaSessionCompat$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/google/android/music/playback/session/MediaSessionCompat$1;

    .prologue
    .line 510
    invoke-direct {p0, p1}, Lcom/google/android/music/playback/session/MediaSessionCompat$QueueItem;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/music/playback/session/MediaDescriptionCompat;J)V
    .locals 2
    .param p1, "description"    # Lcom/google/android/music/playback/session/MediaDescriptionCompat;
    .param p2, "id"    # J

    .prologue
    .line 526
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 527
    if-nez p1, :cond_0

    .line 528
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Description cannot be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 530
    :cond_0
    const-wide/16 v0, -0x1

    cmp-long v0, p2, v0

    if-nez v0, :cond_1

    .line 531
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Id cannot be QueueItem.UNKNOWN_ID"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 533
    :cond_1
    iput-object p1, p0, Lcom/google/android/music/playback/session/MediaSessionCompat$QueueItem;->mDescription:Lcom/google/android/music/playback/session/MediaDescriptionCompat;

    .line 534
    iput-wide p2, p0, Lcom/google/android/music/playback/session/MediaSessionCompat$QueueItem;->mId:J

    .line 535
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 564
    const/4 v0, 0x0

    return v0
.end method

.method public getDescription()Lcom/google/android/music/playback/session/MediaDescriptionCompat;
    .locals 1

    .prologue
    .line 546
    iget-object v0, p0, Lcom/google/android/music/playback/session/MediaSessionCompat$QueueItem;->mDescription:Lcom/google/android/music/playback/session/MediaDescriptionCompat;

    return-object v0
.end method

.method public getQueueId()J
    .locals 2

    .prologue
    .line 553
    iget-wide v0, p0, Lcom/google/android/music/playback/session/MediaSessionCompat$QueueItem;->mId:J

    return-wide v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 582
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "MediaSession.QueueItem {Description="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/playback/session/MediaSessionCompat$QueueItem;->mDescription:Lcom/google/android/music/playback/session/MediaDescriptionCompat;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/music/playback/session/MediaSessionCompat$QueueItem;->mId:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " }"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 558
    iget-object v0, p0, Lcom/google/android/music/playback/session/MediaSessionCompat$QueueItem;->mDescription:Lcom/google/android/music/playback/session/MediaDescriptionCompat;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/music/playback/session/MediaDescriptionCompat;->writeToParcel(Landroid/os/Parcel;I)V

    .line 559
    iget-wide v0, p0, Lcom/google/android/music/playback/session/MediaSessionCompat$QueueItem;->mId:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 560
    return-void
.end method
