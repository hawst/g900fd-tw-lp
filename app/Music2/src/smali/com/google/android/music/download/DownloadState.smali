.class public Lcom/google/android/music/download/DownloadState;
.super Ljava/lang/Object;
.source "DownloadState.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/download/DownloadState$State;
    }
.end annotation


# instance fields
.field private mCompletedBytes:J

.field private mDownloadByteLength:J

.field private mError:I

.field private mEstimatedDownloadByteLength:J

.field private mExperiencedGlitch:Z

.field private mHttpContentType:Ljava/lang/String;

.field private mIsCpOn:Z

.field private mRecommendedBitrate:I

.field private mStartTimeUTC:J

.field private mState:Lcom/google/android/music/download/DownloadState$State;

.field private mStreamFidelity:I


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    sget-object v0, Lcom/google/android/music/download/DownloadState$State;->NOT_STARTED:Lcom/google/android/music/download/DownloadState$State;

    iput-object v0, p0, Lcom/google/android/music/download/DownloadState;->mState:Lcom/google/android/music/download/DownloadState$State;

    .line 58
    iput-wide v2, p0, Lcom/google/android/music/download/DownloadState;->mStartTimeUTC:J

    .line 59
    iput-wide v2, p0, Lcom/google/android/music/download/DownloadState;->mCompletedBytes:J

    .line 60
    iput-wide v2, p0, Lcom/google/android/music/download/DownloadState;->mDownloadByteLength:J

    .line 61
    iput-wide v2, p0, Lcom/google/android/music/download/DownloadState;->mEstimatedDownloadByteLength:J

    .line 62
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/music/download/DownloadState;->mError:I

    .line 63
    iput-boolean v1, p0, Lcom/google/android/music/download/DownloadState;->mExperiencedGlitch:Z

    .line 64
    iput v1, p0, Lcom/google/android/music/download/DownloadState;->mRecommendedBitrate:I

    .line 65
    iput-boolean v1, p0, Lcom/google/android/music/download/DownloadState;->mIsCpOn:Z

    .line 66
    iput v1, p0, Lcom/google/android/music/download/DownloadState;->mStreamFidelity:I

    .line 67
    return-void
.end method


# virtual methods
.method public adjustDownloadLengthUsingHttpContentLength(J)V
    .locals 3
    .param p1, "contentLength"    # J

    .prologue
    .line 186
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-gez v0, :cond_0

    .line 187
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Negative contentLength:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 189
    :cond_0
    iget-wide v0, p0, Lcom/google/android/music/download/DownloadState;->mCompletedBytes:J

    add-long/2addr v0, p1

    iput-wide v0, p0, Lcom/google/android/music/download/DownloadState;->mDownloadByteLength:J

    .line 190
    return-void
.end method

.method public calculateLatency()J
    .locals 4

    .prologue
    .line 273
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/music/download/DownloadState;->mStartTimeUTC:J

    sub-long/2addr v0, v2

    return-wide v0
.end method

.method public getCompletedBytes()J
    .locals 2

    .prologue
    .line 131
    iget-wide v0, p0, Lcom/google/android/music/download/DownloadState;->mCompletedBytes:J

    return-wide v0
.end method

.method public getCompletedFileSize()J
    .locals 2

    .prologue
    .line 141
    iget-boolean v0, p0, Lcom/google/android/music/download/DownloadState;->mIsCpOn:Z

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/music/download/DownloadState;->mCompletedBytes:J

    invoke-static {v0, v1}, Lcom/google/android/music/download/cp/CpUtils;->getEncryptedSize(J)J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/google/android/music/download/DownloadState;->mCompletedBytes:J

    goto :goto_0
.end method

.method public getDownloadByteLength()J
    .locals 4

    .prologue
    .line 170
    iget-wide v0, p0, Lcom/google/android/music/download/DownloadState;->mDownloadByteLength:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 171
    iget-wide v0, p0, Lcom/google/android/music/download/DownloadState;->mDownloadByteLength:J

    .line 173
    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/google/android/music/download/DownloadState;->mEstimatedDownloadByteLength:J

    goto :goto_0
.end method

.method public getError()I
    .locals 1

    .prologue
    .line 224
    iget v0, p0, Lcom/google/android/music/download/DownloadState;->mError:I

    return v0
.end method

.method public getHttpContentType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 213
    iget-object v0, p0, Lcom/google/android/music/download/DownloadState;->mHttpContentType:Ljava/lang/String;

    return-object v0
.end method

.method public getRecommendedBitrate()I
    .locals 1

    .prologue
    .line 252
    iget v0, p0, Lcom/google/android/music/download/DownloadState;->mRecommendedBitrate:I

    return v0
.end method

.method public getState()Lcom/google/android/music/download/DownloadState$State;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/music/download/DownloadState;->mState:Lcom/google/android/music/download/DownloadState$State;

    return-object v0
.end method

.method public getStreamFidelity()I
    .locals 1

    .prologue
    .line 287
    iget v0, p0, Lcom/google/android/music/download/DownloadState;->mStreamFidelity:I

    return v0
.end method

.method public incrementCompletedBytes(J)J
    .locals 3
    .param p1, "value"    # J

    .prologue
    .line 151
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-gez v0, :cond_0

    .line 152
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Negative value:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 154
    :cond_0
    iget-wide v0, p0, Lcom/google/android/music/download/DownloadState;->mCompletedBytes:J

    add-long/2addr v0, p1

    iput-wide v0, p0, Lcom/google/android/music/download/DownloadState;->mCompletedBytes:J

    .line 155
    iget-wide v0, p0, Lcom/google/android/music/download/DownloadState;->mCompletedBytes:J

    return-wide v0
.end method

.method public isCpOn()Z
    .locals 1

    .prologue
    .line 259
    iget-boolean v0, p0, Lcom/google/android/music/download/DownloadState;->mIsCpOn:Z

    return v0
.end method

.method public isDownloadByteLengthEstimated()Z
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 204
    iget-wide v0, p0, Lcom/google/android/music/download/DownloadState;->mDownloadByteLength:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/music/download/DownloadState;->mEstimatedDownloadByteLength:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isExperiencedGlitch()Z
    .locals 1

    .prologue
    .line 238
    iget-boolean v0, p0, Lcom/google/android/music/download/DownloadState;->mExperiencedGlitch:Z

    return v0
.end method

.method public resetCompletedBytes()V
    .locals 2

    .prologue
    .line 162
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/music/download/DownloadState;->mCompletedBytes:J

    .line 163
    return-void
.end method

.method public setCanceledState()V
    .locals 1

    .prologue
    .line 104
    sget-object v0, Lcom/google/android/music/download/DownloadState$State;->CANCELED:Lcom/google/android/music/download/DownloadState$State;

    iput-object v0, p0, Lcom/google/android/music/download/DownloadState;->mState:Lcom/google/android/music/download/DownloadState$State;

    .line 105
    return-void
.end method

.method public setCompletedState()V
    .locals 1

    .prologue
    .line 97
    sget-object v0, Lcom/google/android/music/download/DownloadState$State;->COMPLETED:Lcom/google/android/music/download/DownloadState$State;

    iput-object v0, p0, Lcom/google/android/music/download/DownloadState;->mState:Lcom/google/android/music/download/DownloadState$State;

    .line 98
    return-void
.end method

.method public setCp()V
    .locals 1

    .prologue
    .line 266
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/download/DownloadState;->mIsCpOn:Z

    .line 267
    return-void
.end method

.method public setDownloadingState()V
    .locals 2

    .prologue
    .line 89
    sget-object v0, Lcom/google/android/music/download/DownloadState$State;->DOWNLOADING:Lcom/google/android/music/download/DownloadState$State;

    iput-object v0, p0, Lcom/google/android/music/download/DownloadState;->mState:Lcom/google/android/music/download/DownloadState$State;

    .line 90
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/music/download/DownloadState;->mStartTimeUTC:J

    .line 91
    return-void
.end method

.method public setEstimatedDownloadByteLength(J)V
    .locals 1
    .param p1, "length"    # J

    .prologue
    .line 197
    iput-wide p1, p0, Lcom/google/android/music/download/DownloadState;->mEstimatedDownloadByteLength:J

    .line 198
    return-void
.end method

.method public setExperiencedGlitch()V
    .locals 1

    .prologue
    .line 231
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/download/DownloadState;->mExperiencedGlitch:Z

    .line 232
    return-void
.end method

.method public setFailedState(I)V
    .locals 1
    .param p1, "error"    # I

    .prologue
    .line 81
    sget-object v0, Lcom/google/android/music/download/DownloadState$State;->FAILED:Lcom/google/android/music/download/DownloadState$State;

    iput-object v0, p0, Lcom/google/android/music/download/DownloadState;->mState:Lcom/google/android/music/download/DownloadState$State;

    .line 82
    iput p1, p0, Lcom/google/android/music/download/DownloadState;->mError:I

    .line 83
    return-void
.end method

.method public setHttpContentType(Ljava/lang/String;)V
    .locals 0
    .param p1, "httpContentType"    # Ljava/lang/String;

    .prologue
    .line 217
    iput-object p1, p0, Lcom/google/android/music/download/DownloadState;->mHttpContentType:Ljava/lang/String;

    .line 218
    return-void
.end method

.method public setRecommendedBitrate(I)V
    .locals 0
    .param p1, "kbps"    # I

    .prologue
    .line 245
    iput p1, p0, Lcom/google/android/music/download/DownloadState;->mRecommendedBitrate:I

    .line 246
    return-void
.end method

.method public setStreamFidelity(I)V
    .locals 0
    .param p1, "fidelity"    # I

    .prologue
    .line 280
    iput p1, p0, Lcom/google/android/music/download/DownloadState;->mStreamFidelity:I

    .line 281
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 293
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 294
    .local v0, "builder":Ljava/lang/StringBuilder;
    const-string v1, "mState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/download/DownloadState;->mState:Lcom/google/android/music/download/DownloadState$State;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 295
    const-string v1, " mStartTimeUTC="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/music/download/DownloadState;->mStartTimeUTC:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 296
    const-string v1, " mCompletedBytes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/music/download/DownloadState;->mCompletedBytes:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 297
    const-string v1, " mDownloadByteLength="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/music/download/DownloadState;->mDownloadByteLength:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 298
    const-string v1, " mEstimatedDownloadByteLength="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/music/download/DownloadState;->mEstimatedDownloadByteLength:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 299
    const-string v1, " mError="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/music/download/DownloadState;->mError:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 300
    const-string v1, " mExperiencedGlitch="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/music/download/DownloadState;->mExperiencedGlitch:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 301
    const-string v1, " mRecommendedBitrate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/music/download/DownloadState;->mRecommendedBitrate:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 302
    const-string v1, " mHttpContentType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/download/DownloadState;->mHttpContentType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 303
    const-string v1, " mIsCpOn="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/music/download/DownloadState;->mIsCpOn:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 304
    const-string v1, " mStreamFidelity="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/music/download/DownloadState;->mStreamFidelity:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 305
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
