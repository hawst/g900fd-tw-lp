.class Lcom/google/android/music/ui/MaterialMainstageFragment$MainstageItemDecoration;
.super Landroid/support/v7/widget/RecyclerView$ItemDecoration;
.source "MaterialMainstageFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/MaterialMainstageFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MainstageItemDecoration"
.end annotation


# instance fields
.field private final mLeftRightPadding:I


# direct methods
.method private constructor <init>(Landroid/content/res/Resources;)V
    .locals 1
    .param p1, "res"    # Landroid/content/res/Resources;

    .prologue
    .line 743
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$ItemDecoration;-><init>()V

    .line 744
    const v0, 0x7f0f01b3

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$MainstageItemDecoration;->mLeftRightPadding:I

    .line 745
    return-void
.end method

.method synthetic constructor <init>(Landroid/content/res/Resources;Lcom/google/android/music/ui/MaterialMainstageFragment$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/content/res/Resources;
    .param p2, "x1"    # Lcom/google/android/music/ui/MaterialMainstageFragment$1;

    .prologue
    .line 739
    invoke-direct {p0, p1}, Lcom/google/android/music/ui/MaterialMainstageFragment$MainstageItemDecoration;-><init>(Landroid/content/res/Resources;)V

    return-void
.end method


# virtual methods
.method public getItemOffsets(Landroid/graphics/Rect;Landroid/view/View;Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/RecyclerView$State;)V
    .locals 2
    .param p1, "outRect"    # Landroid/graphics/Rect;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/support/v7/widget/RecyclerView;
    .param p4, "state"    # Landroid/support/v7/widget/RecyclerView$State;

    .prologue
    .line 750
    invoke-super {p0, p1, p2, p3, p4}, Landroid/support/v7/widget/RecyclerView$ItemDecoration;->getItemOffsets(Landroid/graphics/Rect;Landroid/view/View;Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/RecyclerView$State;)V

    .line 751
    invoke-virtual {p3, p2}, Landroid/support/v7/widget/RecyclerView;->getChildViewHolder(Landroid/view/View;)Landroid/support/v7/widget/RecyclerView$ViewHolder;

    move-result-object v0

    .line 752
    .local v0, "holder":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    instance-of v1, v0, Lcom/google/android/music/ui/MaterialMainstageFragment$SituationCardViewHolder;

    if-eqz v1, :cond_0

    .line 753
    iget v1, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$MainstageItemDecoration;->mLeftRightPadding:I

    neg-int v1, v1

    iput v1, p1, Landroid/graphics/Rect;->left:I

    .line 754
    iget v1, p0, Lcom/google/android/music/ui/MaterialMainstageFragment$MainstageItemDecoration;->mLeftRightPadding:I

    neg-int v1, v1

    iput v1, p1, Landroid/graphics/Rect;->right:I

    .line 756
    :cond_0
    return-void
.end method
