.class public Lcom/google/android/music/cloudclient/GetSharedPlaylistEntriesRequestJson;
.super Lcom/google/api/client/json/GenericJson;
.source "GetSharedPlaylistEntriesRequestJson.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/cloudclient/GetSharedPlaylistEntriesRequestJson$Entry;
    }
.end annotation


# instance fields
.field public mEntries:Ljava/util/List;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "entries"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/cloudclient/GetSharedPlaylistEntriesRequestJson$Entry;",
            ">;"
        }
    .end annotation
.end field

.field public mIncludeDeleted:Z
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "includeDeleted"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    .line 18
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/cloudclient/GetSharedPlaylistEntriesRequestJson;->mEntries:Ljava/util/List;

    .line 21
    return-void
.end method

.method public static serialize(Ljava/lang/String;ILjava/lang/String;J)[B
    .locals 3
    .param p0, "sharedToken"    # Ljava/lang/String;
    .param p1, "maxEntries"    # I
    .param p2, "continuationToken"    # Ljava/lang/String;
    .param p3, "updateMin"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 39
    new-instance v1, Lcom/google/android/music/cloudclient/GetSharedPlaylistEntriesRequestJson;

    invoke-direct {v1}, Lcom/google/android/music/cloudclient/GetSharedPlaylistEntriesRequestJson;-><init>()V

    .line 41
    .local v1, "request":Lcom/google/android/music/cloudclient/GetSharedPlaylistEntriesRequestJson;
    new-instance v0, Lcom/google/android/music/cloudclient/GetSharedPlaylistEntriesRequestJson$Entry;

    invoke-direct {v0}, Lcom/google/android/music/cloudclient/GetSharedPlaylistEntriesRequestJson$Entry;-><init>()V

    .line 42
    .local v0, "entry":Lcom/google/android/music/cloudclient/GetSharedPlaylistEntriesRequestJson$Entry;
    iput-object p0, v0, Lcom/google/android/music/cloudclient/GetSharedPlaylistEntriesRequestJson$Entry;->mSharePlaylistToken:Ljava/lang/String;

    .line 43
    iput p1, v0, Lcom/google/android/music/cloudclient/GetSharedPlaylistEntriesRequestJson$Entry;->mMaxResults:I

    .line 44
    iput-object p2, v0, Lcom/google/android/music/cloudclient/GetSharedPlaylistEntriesRequestJson$Entry;->mStartToken:Ljava/lang/String;

    .line 45
    iput-wide p3, v0, Lcom/google/android/music/cloudclient/GetSharedPlaylistEntriesRequestJson$Entry;->mUpdateMin:J

    .line 47
    iget-object v2, v1, Lcom/google/android/music/cloudclient/GetSharedPlaylistEntriesRequestJson;->mEntries:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 49
    invoke-static {v1}, Lcom/google/android/music/cloudclient/JsonUtils;->toJsonByteArray(Lcom/google/api/client/json/GenericJson;)[B

    move-result-object v2

    return-object v2
.end method
