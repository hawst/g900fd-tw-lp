.class public Lcom/google/android/music/ui/PlaylistClustersFragment;
.super Lcom/google/android/music/ui/MediaListGridFragment;
.source "PlaylistClustersFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/ui/PlaylistClustersFragment$1;,
        Lcom/google/android/music/ui/PlaylistClustersFragment$PlaylistsAdapter;,
        Lcom/google/android/music/ui/PlaylistClustersFragment$ClusterLoaderCallbacks;
    }
.end annotation


# static fields
.field public static final CURSOR_COLUMNS:[Ljava/lang/String;


# instance fields
.field private mAllPlaylistsHeader:Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;

.field private mAutoPlaylistsCluster:Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

.field private mAutoPlaylistsCursor:Landroid/database/Cursor;

.field private mCardHeap:Lcom/google/android/music/ui/cardlib/layout/PlayCardHeap;

.field private mCardType:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

.field private mClusterLoaderCallback:Lcom/google/android/music/ui/PlaylistClustersFragment$ClusterLoaderCallbacks;

.field private mRecentPlaylistsCluster:Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

.field private mRecentPlaylistsCursor:Landroid/database/Cursor;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 41
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "playlist_name"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "playlist_type"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "playlist_description"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "playlist_owner_name"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "playlist_share_token"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "playlist_art_url"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "playlist_owner_profile_photo_url"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "hasLocal"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "SourceAccount"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/ui/PlaylistClustersFragment;->CURSOR_COLUMNS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/google/android/music/ui/MediaListGridFragment;-><init>()V

    .line 75
    new-instance v0, Lcom/google/android/music/ui/PlaylistClustersFragment$ClusterLoaderCallbacks;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/music/ui/PlaylistClustersFragment$ClusterLoaderCallbacks;-><init>(Lcom/google/android/music/ui/PlaylistClustersFragment;Lcom/google/android/music/ui/PlaylistClustersFragment$1;)V

    iput-object v0, p0, Lcom/google/android/music/ui/PlaylistClustersFragment;->mClusterLoaderCallback:Lcom/google/android/music/ui/PlaylistClustersFragment$ClusterLoaderCallbacks;

    .line 77
    new-instance v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardHeap;

    invoke-direct {v0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardHeap;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/ui/PlaylistClustersFragment;->mCardHeap:Lcom/google/android/music/ui/cardlib/layout/PlayCardHeap;

    .line 78
    sget-object v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->CARD_SMALL_WRAPPED:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    iput-object v0, p0, Lcom/google/android/music/ui/PlaylistClustersFragment;->mCardType:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    .line 377
    return-void
.end method

.method static synthetic access$102(Lcom/google/android/music/ui/PlaylistClustersFragment;Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/PlaylistClustersFragment;
    .param p1, "x1"    # Landroid/database/Cursor;

    .prologue
    .line 37
    iput-object p1, p0, Lcom/google/android/music/ui/PlaylistClustersFragment;->mRecentPlaylistsCursor:Landroid/database/Cursor;

    return-object p1
.end method

.method static synthetic access$200(Lcom/google/android/music/ui/PlaylistClustersFragment;)Lcom/google/android/music/ui/cardlib/PlayCardClusterView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/PlaylistClustersFragment;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/music/ui/PlaylistClustersFragment;->mRecentPlaylistsCluster:Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/music/ui/PlaylistClustersFragment;Lcom/google/android/music/ui/cardlib/PlayCardClusterView;ILandroid/database/Cursor;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/PlaylistClustersFragment;
    .param p1, "x1"    # Lcom/google/android/music/ui/cardlib/PlayCardClusterView;
    .param p2, "x2"    # I
    .param p3, "x3"    # Landroid/database/Cursor;

    .prologue
    .line 37
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/music/ui/PlaylistClustersFragment;->populateCluster(Lcom/google/android/music/ui/cardlib/PlayCardClusterView;ILandroid/database/Cursor;)V

    return-void
.end method

.method static synthetic access$402(Lcom/google/android/music/ui/PlaylistClustersFragment;Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/PlaylistClustersFragment;
    .param p1, "x1"    # Landroid/database/Cursor;

    .prologue
    .line 37
    iput-object p1, p0, Lcom/google/android/music/ui/PlaylistClustersFragment;->mAutoPlaylistsCursor:Landroid/database/Cursor;

    return-object p1
.end method

.method static synthetic access$500(Lcom/google/android/music/ui/PlaylistClustersFragment;)Lcom/google/android/music/ui/cardlib/PlayCardClusterView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/PlaylistClustersFragment;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/music/ui/PlaylistClustersFragment;->mAutoPlaylistsCluster:Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/music/ui/PlaylistClustersFragment;)Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/PlaylistClustersFragment;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/music/ui/PlaylistClustersFragment;->mCardType:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/music/ui/cardlib/model/Document;Landroid/database/Cursor;Landroid/content/Context;)Lcom/google/android/music/ui/cardlib/model/Document;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p1, "x1"    # Landroid/database/Cursor;
    .param p2, "x2"    # Landroid/content/Context;

    .prologue
    .line 37
    invoke-static {p0, p1, p2}, Lcom/google/android/music/ui/PlaylistClustersFragment;->populatePlaylistDocument(Lcom/google/android/music/ui/cardlib/model/Document;Landroid/database/Cursor;Landroid/content/Context;)Lcom/google/android/music/ui/cardlib/model/Document;

    move-result-object v0

    return-object v0
.end method

.method private buildEmptyCluster(Landroid/view/LayoutInflater;Landroid/widget/ListView;)Lcom/google/android/music/ui/cardlib/PlayCardClusterView;
    .locals 6
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "lv"    # Landroid/widget/ListView;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 174
    const v2, 0x7f040092

    const/4 v3, 0x0

    invoke-virtual {p1, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    .line 177
    .local v0, "cluster":Lcom/google/android/music/ui/cardlib/PlayCardClusterView;
    new-instance v1, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;

    invoke-direct {v1, v4, v4}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;-><init>(II)V

    .line 178
    .local v1, "clusterMetadata":Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;
    invoke-virtual {v0, v1, v5, v5}, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->setMetadata(Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;Ljava/util/List;Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;)V

    .line 179
    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->setVisibility(I)V

    .line 180
    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->hideHeader()V

    .line 181
    return-object v0
.end method

.method private static buildPlaylistDocumentList(Landroid/database/Cursor;Landroid/content/Context;)Ljava/util/ArrayList;
    .locals 5
    .param p0, "cursor"    # Landroid/database/Cursor;
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/music/ui/cardlib/model/Document;",
            ">;"
        }
    .end annotation

    .prologue
    .line 329
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 331
    .local v3, "listDoc":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/music/ui/cardlib/model/Document;>;"
    invoke-interface {p0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 332
    invoke-interface {p0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    .line 333
    .local v1, "elementCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_0

    .line 334
    new-instance v0, Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-direct {v0}, Lcom/google/android/music/ui/cardlib/model/Document;-><init>()V

    .line 335
    .local v0, "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    invoke-static {v0, p0, p1}, Lcom/google/android/music/ui/PlaylistClustersFragment;->populatePlaylistDocument(Lcom/google/android/music/ui/cardlib/model/Document;Landroid/database/Cursor;Landroid/content/Context;)Lcom/google/android/music/ui/cardlib/model/Document;

    move-result-object v0

    .line 336
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 337
    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 341
    .end local v0    # "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    .end local v1    # "elementCount":I
    .end local v2    # "i":I
    :cond_0
    return-object v3

    .line 333
    .restart local v0    # "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    .restart local v1    # "elementCount":I
    .restart local v2    # "i":I
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private getClusterInfo(ILandroid/database/Cursor;)Lcom/google/android/music/ui/Cluster;
    .locals 11
    .param p1, "groupType"    # I
    .param p2, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v4, 0x0

    .line 231
    sget-object v6, Lcom/google/android/music/ui/cardlib/model/Document$Type;->PLAYLIST:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    .line 232
    .local v6, "docType":Lcom/google/android/music/ui/cardlib/model/Document$Type;
    invoke-virtual {p0}, Lcom/google/android/music/ui/PlaylistClustersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {p2, v0}, Lcom/google/android/music/ui/PlaylistClustersFragment;->buildPlaylistDocumentList(Landroid/database/Cursor;Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v5

    .line 233
    .local v5, "docList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/music/ui/cardlib/model/Document;>;"
    invoke-virtual {p0}, Lcom/google/android/music/ui/PlaylistClustersFragment;->getScreenColumns()I

    move-result v7

    .line 234
    .local v7, "nbColumns":I
    invoke-virtual {p0}, Lcom/google/android/music/ui/PlaylistClustersFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d000f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v8

    .line 235
    .local v8, "nbRows":I
    packed-switch p1, :pswitch_data_0

    .line 243
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected type value:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 237
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/music/ui/PlaylistClustersFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b022e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 245
    .local v3, "title":Ljava/lang/String;
    :goto_0
    new-instance v0, Lcom/google/android/music/ui/Cluster;

    invoke-virtual {p0}, Lcom/google/android/music/ui/PlaylistClustersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/PlaylistClustersFragment;->mCardType:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    move-object v9, v4

    move-object v10, v4

    invoke-direct/range {v0 .. v10}, Lcom/google/android/music/ui/Cluster;-><init>(Landroid/app/Activity;Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Lcom/google/android/music/ui/cardlib/model/Document$Type;IILandroid/view/View$OnClickListener;Lcom/google/android/music/store/ContainerDescriptor;)V

    return-object v0

    .line 240
    .end local v3    # "title":Ljava/lang/String;
    :pswitch_1
    invoke-virtual {p0}, Lcom/google/android/music/ui/PlaylistClustersFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b022d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 241
    .restart local v3    # "title":Ljava/lang/String;
    goto :goto_0

    .line 235
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private populateCluster(Lcom/google/android/music/ui/cardlib/PlayCardClusterView;ILandroid/database/Cursor;)V
    .locals 19
    .param p1, "cluster"    # Lcom/google/android/music/ui/cardlib/PlayCardClusterView;
    .param p2, "type"    # I
    .param p3, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 188
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getCount()I

    move-result v14

    if-nez v14, :cond_0

    .line 189
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->hideHeader()V

    .line 190
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->removeAllCards()V

    .line 226
    :goto_0
    return-void

    .line 194
    :cond_0
    move-object/from16 v0, p0

    move/from16 v1, p2

    move-object/from16 v2, p3

    invoke-direct {v0, v1, v2}, Lcom/google/android/music/ui/PlaylistClustersFragment;->getClusterInfo(ILandroid/database/Cursor;)Lcom/google/android/music/ui/Cluster;

    move-result-object v4

    .line 196
    .local v4, "clusterInfo":Lcom/google/android/music/ui/Cluster;
    invoke-virtual {v4}, Lcom/google/android/music/ui/Cluster;->getNbColumns()I

    move-result v11

    .line 197
    .local v11, "nbColumns":I
    invoke-virtual {v4}, Lcom/google/android/music/ui/Cluster;->getVisibleContent()Ljava/util/List;

    move-result-object v7

    .line 198
    .local v7, "docList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/ui/cardlib/model/Document;>;"
    invoke-virtual {v4}, Lcom/google/android/music/ui/Cluster;->getCardType()Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    move-result-object v3

    .line 200
    .local v3, "cardType":Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v14

    add-int/2addr v14, v11

    add-int/lit8 v14, v14, -0x1

    div-int v12, v14, v11

    .line 201
    .local v12, "nbRows":I
    new-instance v5, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;

    invoke-virtual {v3}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->getHSpan()I

    move-result v14

    mul-int/2addr v14, v11

    invoke-virtual {v3}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->getVSpan()I

    move-result v15

    mul-int/2addr v15, v12

    invoke-direct {v5, v14, v15}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;-><init>(II)V

    .line 204
    .local v5, "clusterMetadata":Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_1
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v14

    if-ge v8, v14, :cond_1

    .line 205
    div-int v14, v8, v11

    invoke-virtual {v3}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->getVSpan()I

    move-result v15

    mul-int v13, v14, v15

    .line 206
    .local v13, "row":I
    rem-int v14, v8, v11

    invoke-virtual {v3}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->getHSpan()I

    move-result v15

    mul-int v6, v14, v15

    .line 207
    .local v6, "column":I
    invoke-virtual {v5, v3, v6, v13}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->addTile(Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;

    .line 204
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 210
    .end local v6    # "column":I
    .end local v13    # "row":I
    :cond_1
    invoke-virtual {v4}, Lcom/google/android/music/ui/Cluster;->getVisibleContent()Ljava/util/List;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/music/ui/PlaylistClustersFragment;->mCardsContextMenuDelegate:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$DocumentContextMenuDelegate;

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v14, v15}, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->setMetadata(Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;Ljava/util/List;Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;)V

    .line 213
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->hasCards()Z

    move-result v14

    if-nez v14, :cond_2

    .line 214
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/music/ui/PlaylistClustersFragment;->mCardHeap:Lcom/google/android/music/ui/cardlib/layout/PlayCardHeap;

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->inflateContent(Lcom/google/android/music/ui/cardlib/layout/PlayCardHeap;)V

    .line 217
    :cond_2
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->createContent()V

    .line 219
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getCount()I

    move-result v14

    invoke-virtual {v5}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->getTileCount()I

    move-result v15

    sub-int v10, v14, v15

    .line 220
    .local v10, "moreItems":I
    if-lez v10, :cond_3

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/PlaylistClustersFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0x7f0b0260

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    aput-object v18, v16, v17

    invoke-virtual/range {v14 .. v16}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    .line 222
    .local v9, "more":Ljava/lang/String;
    :goto_2
    invoke-virtual {v4}, Lcom/google/android/music/ui/Cluster;->getTitle()Ljava/lang/String;

    move-result-object v14

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v15, v9}, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->showHeader(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    invoke-virtual {v4}, Lcom/google/android/music/ui/Cluster;->getMoreOnClickListener()Lcom/google/android/music/ui/cardlib/model/MoreClusterClickHandler;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->setMoreClickHandler(Landroid/view/View$OnClickListener;)V

    .line 225
    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->setVisibility(I)V

    goto/16 :goto_0

    .line 220
    .end local v9    # "more":Ljava/lang/String;
    :cond_3
    const/4 v9, 0x0

    goto :goto_2
.end method

.method private static populatePlaylistDocument(Lcom/google/android/music/ui/cardlib/model/Document;Landroid/database/Cursor;Landroid/content/Context;)Lcom/google/android/music/ui/cardlib/model/Document;
    .locals 10
    .param p0, "doc"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x2

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 345
    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/model/Document;->reset()V

    .line 346
    sget-object v5, Lcom/google/android/music/ui/cardlib/model/Document$Type;->PLAYLIST:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    invoke-virtual {p0, v5}, Lcom/google/android/music/ui/cardlib/model/Document;->setType(Lcom/google/android/music/ui/cardlib/model/Document$Type;)V

    .line 348
    invoke-interface {p1, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-virtual {p0, v6, v7}, Lcom/google/android/music/ui/cardlib/model/Document;->setId(J)V

    .line 349
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/google/android/music/ui/cardlib/model/Document;->setPlaylistName(Ljava/lang/String;)V

    .line 350
    invoke-interface {p1, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-virtual {p0, v5}, Lcom/google/android/music/ui/cardlib/model/Document;->setPlaylistType(I)V

    .line 351
    const/4 v5, 0x3

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/google/android/music/ui/cardlib/model/Document;->setDescription(Ljava/lang/String;)V

    .line 352
    invoke-interface {p1, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/google/android/music/ui/cardlib/model/Document;->setPlaylistOwnerName(Ljava/lang/String;)V

    .line 353
    const/4 v5, 0x5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/google/android/music/ui/cardlib/model/Document;->setPlaylistShareToken(Ljava/lang/String;)V

    .line 354
    const/4 v5, 0x6

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/google/android/music/ui/cardlib/model/Document;->setArtUrl(Ljava/lang/String;)V

    .line 355
    const/4 v5, 0x7

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/google/android/music/ui/cardlib/model/Document;->setProfilePhotoUrl(Ljava/lang/String;)V

    .line 357
    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/model/Document;->getPlaylistName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/google/android/music/ui/cardlib/model/Document;->setTitle(Ljava/lang/String;)V

    .line 360
    invoke-interface {p1, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 361
    .local v1, "playlistType":I
    invoke-interface {p1, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 362
    .local v0, "ownerName":Ljava/lang/String;
    const/16 v5, 0x47

    if-eq v1, v5, :cond_0

    const/16 v5, 0x46

    if-ne v1, v5, :cond_1

    :cond_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 365
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b0102

    new-array v7, v3, [Ljava/lang/Object;

    aput-object v0, v7, v4

    invoke-virtual {v5, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 371
    .local v2, "subtitle":Ljava/lang/String;
    :goto_0
    invoke-virtual {p0, v2}, Lcom/google/android/music/ui/cardlib/model/Document;->setSubTitle(Ljava/lang/String;)V

    .line 372
    const/16 v5, 0x8

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    if-eqz v5, :cond_2

    :goto_1
    invoke-virtual {p0, v3}, Lcom/google/android/music/ui/cardlib/model/Document;->setHasLocal(Z)V

    .line 373
    const/16 v3, 0x9

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-virtual {p0, v3}, Lcom/google/android/music/ui/cardlib/model/Document;->setSourceAccount(I)V

    .line 374
    return-object p0

    .line 368
    .end local v2    # "subtitle":Ljava/lang/String;
    :cond_1
    const-string v2, ""

    .restart local v2    # "subtitle":Ljava/lang/String;
    goto :goto_0

    :cond_2
    move v3, v4

    .line 372
    goto :goto_1
.end method


# virtual methods
.method protected initEmptyScreen()V
    .locals 1

    .prologue
    .line 167
    invoke-super {p0}, Lcom/google/android/music/ui/MediaListGridFragment;->initEmptyScreen()V

    .line 168
    invoke-virtual {p0}, Lcom/google/android/music/ui/PlaylistClustersFragment;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->getDisplayOptions()I

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0b02a4

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/PlaylistClustersFragment;->setEmptyScreenText(I)V

    .line 171
    return-void

    .line 168
    :cond_0
    const v0, 0x7f0b02a7

    goto :goto_0
.end method

.method newAsyncCursorAdapter(Landroid/database/Cursor;)Lcom/google/android/music/ui/MediaListCursorAdapter;
    .locals 2
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    .line 325
    new-instance v0, Lcom/google/android/music/ui/PlaylistClustersFragment$PlaylistsAdapter;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p0, p1, v1}, Lcom/google/android/music/ui/PlaylistClustersFragment$PlaylistsAdapter;-><init>(Lcom/google/android/music/ui/PlaylistClustersFragment;Lcom/google/android/music/ui/PlaylistClustersFragment;Landroid/database/Cursor;Lcom/google/android/music/ui/PlaylistClustersFragment$1;)V

    return-object v0
.end method

.method newLoaderCursorAdapter()Lcom/google/android/music/ui/MediaListCursorAdapter;
    .locals 2

    .prologue
    .line 320
    new-instance v0, Lcom/google/android/music/ui/PlaylistClustersFragment$PlaylistsAdapter;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p0, v1}, Lcom/google/android/music/ui/PlaylistClustersFragment$PlaylistsAdapter;-><init>(Lcom/google/android/music/ui/PlaylistClustersFragment;Lcom/google/android/music/ui/PlaylistClustersFragment;Lcom/google/android/music/ui/PlaylistClustersFragment$1;)V

    return-object v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 142
    new-instance v0, Lcom/google/android/music/medialist/AllPlaylists;

    invoke-direct {v0}, Lcom/google/android/music/medialist/AllPlaylists;-><init>()V

    sget-object v1, Lcom/google/android/music/ui/PlaylistClustersFragment;->CURSOR_COLUMNS:[Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/music/ui/PlaylistClustersFragment;->init(Lcom/google/android/music/medialist/MediaList;[Ljava/lang/String;Z)V

    .line 143
    invoke-super {p0, p1}, Lcom/google/android/music/ui/MediaListGridFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 144
    return-void
.end method

.method public onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 2
    .param p2, "data"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 151
    .local p1, "loader":Landroid/support/v4/content/Loader;, "Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_1

    .line 152
    iget-object v1, p0, Lcom/google/android/music/ui/PlaylistClustersFragment;->mAllPlaylistsHeader:Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;

    invoke-virtual {v1}, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;->show()V

    .line 156
    :goto_0
    invoke-super {p0, p1, p2}, Lcom/google/android/music/ui/MediaListGridFragment;->onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    .line 160
    iget-object v1, p0, Lcom/google/android/music/ui/PlaylistClustersFragment;->mRecentPlaylistsCluster:Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    invoke-virtual {v1}, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->hasCards()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/music/ui/PlaylistClustersFragment;->mAutoPlaylistsCluster:Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    invoke-virtual {v1}, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->hasCards()Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_0
    const/4 v0, 0x1

    .line 162
    .local v0, "hasCards":Z
    :goto_1
    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/PlaylistClustersFragment;->setIsCardHeaderShowing(Z)V

    .line 163
    return-void

    .line 154
    .end local v0    # "hasCards":Z
    :cond_1
    iget-object v1, p0, Lcom/google/android/music/ui/PlaylistClustersFragment;->mAllPlaylistsHeader:Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;

    invoke-virtual {v1}, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;->hide()V

    goto :goto_0

    .line 160
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Landroid/support/v4/content/Loader;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 37
    check-cast p2, Landroid/database/Cursor;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/music/ui/PlaylistClustersFragment;->onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onStart()V
    .locals 3

    .prologue
    .line 122
    invoke-super {p0}, Lcom/google/android/music/ui/MediaListGridFragment;->onStart()V

    .line 130
    iget-object v0, p0, Lcom/google/android/music/ui/PlaylistClustersFragment;->mRecentPlaylistsCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 131
    iget-object v0, p0, Lcom/google/android/music/ui/PlaylistClustersFragment;->mRecentPlaylistsCluster:Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/music/ui/PlaylistClustersFragment;->mRecentPlaylistsCursor:Landroid/database/Cursor;

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/music/ui/PlaylistClustersFragment;->populateCluster(Lcom/google/android/music/ui/cardlib/PlayCardClusterView;ILandroid/database/Cursor;)V

    .line 134
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/ui/PlaylistClustersFragment;->mAutoPlaylistsCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_1

    .line 135
    iget-object v0, p0, Lcom/google/android/music/ui/PlaylistClustersFragment;->mAutoPlaylistsCluster:Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/music/ui/PlaylistClustersFragment;->mAutoPlaylistsCursor:Landroid/database/Cursor;

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/music/ui/PlaylistClustersFragment;->populateCluster(Lcom/google/android/music/ui/cardlib/PlayCardClusterView;ILandroid/database/Cursor;)V

    .line 138
    :cond_1
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 8
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 92
    invoke-super {p0, p1, p2}, Lcom/google/android/music/ui/MediaListGridFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 93
    invoke-virtual {p0}, Lcom/google/android/music/ui/PlaylistClustersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    .line 94
    .local v0, "inflater":Landroid/view/LayoutInflater;
    invoke-virtual {p0}, Lcom/google/android/music/ui/PlaylistClustersFragment;->getListView()Landroid/widget/ListView;

    move-result-object v2

    .line 96
    .local v2, "lv":Landroid/widget/ListView;
    invoke-virtual {v2, v7}, Landroid/widget/ListView;->setHeaderDividersEnabled(Z)V

    .line 99
    invoke-direct {p0, v0, v2}, Lcom/google/android/music/ui/PlaylistClustersFragment;->buildEmptyCluster(Landroid/view/LayoutInflater;Landroid/widget/ListView;)Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/music/ui/PlaylistClustersFragment;->mRecentPlaylistsCluster:Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    .line 100
    iget-object v3, p0, Lcom/google/android/music/ui/PlaylistClustersFragment;->mRecentPlaylistsCluster:Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    invoke-virtual {v2, v3, v6, v7}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 103
    invoke-direct {p0, v0, v2}, Lcom/google/android/music/ui/PlaylistClustersFragment;->buildEmptyCluster(Landroid/view/LayoutInflater;Landroid/widget/ListView;)Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/music/ui/PlaylistClustersFragment;->mAutoPlaylistsCluster:Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    .line 104
    iget-object v3, p0, Lcom/google/android/music/ui/PlaylistClustersFragment;->mAutoPlaylistsCluster:Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    invoke-virtual {v2, v3, v6, v7}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 107
    const v3, 0x7f040093

    invoke-virtual {v0, v3, v2, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;

    iput-object v3, p0, Lcom/google/android/music/ui/PlaylistClustersFragment;->mAllPlaylistsHeader:Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;

    .line 109
    iget-object v3, p0, Lcom/google/android/music/ui/PlaylistClustersFragment;->mAllPlaylistsHeader:Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;

    invoke-virtual {p0}, Lcom/google/android/music/ui/PlaylistClustersFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b022f

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v6, v6}, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;->setContent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    iget-object v3, p0, Lcom/google/android/music/ui/PlaylistClustersFragment;->mAllPlaylistsHeader:Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;

    invoke-virtual {v3}, Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;->hide()V

    .line 112
    iget-object v3, p0, Lcom/google/android/music/ui/PlaylistClustersFragment;->mAllPlaylistsHeader:Lcom/google/android/music/ui/cardlib/PlayCardClusterViewHeader;

    invoke-virtual {v2, v3, v6, v7}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 115
    invoke-virtual {p0}, Lcom/google/android/music/ui/PlaylistClustersFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v1

    .line 116
    .local v1, "lm":Landroid/support/v4/app/LoaderManager;
    const/16 v3, 0x64

    iget-object v4, p0, Lcom/google/android/music/ui/PlaylistClustersFragment;->mClusterLoaderCallback:Lcom/google/android/music/ui/PlaylistClustersFragment$ClusterLoaderCallbacks;

    invoke-virtual {v1, v3, v6, v4}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 117
    const/16 v3, 0x65

    iget-object v4, p0, Lcom/google/android/music/ui/PlaylistClustersFragment;->mClusterLoaderCallback:Lcom/google/android/music/ui/PlaylistClustersFragment$ClusterLoaderCallbacks;

    invoke-virtual {v1, v3, v6, v4}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 118
    return-void
.end method
