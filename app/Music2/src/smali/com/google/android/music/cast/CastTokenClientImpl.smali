.class public Lcom/google/android/music/cast/CastTokenClientImpl;
.super Ljava/lang/Object;
.source "CastTokenClientImpl.java"

# interfaces
.implements Lcom/google/android/music/cast/CastTokenClient;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/cast/CastTokenClientImpl$Cache;
    }
.end annotation


# instance fields
.field private final mAccountChangeReceiver:Landroid/content/BroadcastReceiver;

.field private final mCastTokenCache:Lcom/google/android/music/cast/CastTokenClientImpl$Cache;

.field private final mContext:Landroid/content/Context;

.field private final mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

.field private final mPreferencesHolder:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/google/android/music/cast/CastTokenClientImpl;->mPreferencesHolder:Ljava/lang/Object;

    .line 62
    new-instance v1, Lcom/google/android/music/cast/CastTokenClientImpl$Cache;

    const/16 v2, 0x14

    invoke-direct {v1, v2}, Lcom/google/android/music/cast/CastTokenClientImpl$Cache;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/music/cast/CastTokenClientImpl;->mCastTokenCache:Lcom/google/android/music/cast/CastTokenClientImpl$Cache;

    .line 64
    new-instance v1, Lcom/google/android/music/cast/CastTokenClientImpl$1;

    invoke-direct {v1, p0}, Lcom/google/android/music/cast/CastTokenClientImpl$1;-><init>(Lcom/google/android/music/cast/CastTokenClientImpl;)V

    iput-object v1, p0, Lcom/google/android/music/cast/CastTokenClientImpl;->mAccountChangeReceiver:Landroid/content/BroadcastReceiver;

    .line 72
    iput-object p1, p0, Lcom/google/android/music/cast/CastTokenClientImpl;->mContext:Landroid/content/Context;

    .line 73
    iget-object v1, p0, Lcom/google/android/music/cast/CastTokenClientImpl;->mPreferencesHolder:Ljava/lang/Object;

    invoke-static {p1, v1}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/cast/CastTokenClientImpl;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    .line 76
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 77
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "com.google.android.music.accountchanged"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 78
    iget-object v1, p0, Lcom/google/android/music/cast/CastTokenClientImpl;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/music/cast/CastTokenClientImpl;->mAccountChangeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 79
    return-void
.end method

.method private extractToken(Lorg/apache/http/client/methods/HttpRequestBase;Lorg/apache/http/HttpResponse;)Ljava/lang/String;
    .locals 4
    .param p1, "request"    # Lorg/apache/http/client/methods/HttpRequestBase;
    .param p2, "response"    # Lorg/apache/http/HttpResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 82
    const/high16 v3, 0x10000

    invoke-static {p1, p2, v3}, Lcom/google/android/music/cloudclient/MusicRequest;->readAndReleaseShortResponse(Lorg/apache/http/client/methods/HttpRequestBase;Lorg/apache/http/HttpResponse;I)[B

    move-result-object v0

    .line 84
    .local v0, "bytes":[B
    invoke-interface {p2}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v2

    .line 85
    .local v2, "responseEntity":Lorg/apache/http/HttpEntity;
    invoke-static {v2}, Lorg/apache/http/util/EntityUtils;->getContentCharSet(Lorg/apache/http/HttpEntity;)Ljava/lang/String;

    move-result-object v1

    .line 86
    .local v1, "charset":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 87
    const-string v1, "UTF-8"

    .line 89
    :cond_0
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v0, v1}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    return-object v3
.end method

.method private fetchCastTokenFromServer(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;
    .locals 16
    .param p1, "remoteEndpointType"    # Ljava/lang/String;
    .param p2, "remoteEndpointId"    # Ljava/lang/String;
    .param p3, "useRemoteEndpointIdAsHostDeviceId"    # Z

    .prologue
    .line 134
    invoke-direct/range {p0 .. p0}, Lcom/google/android/music/cast/CastTokenClientImpl;->isLogVerbose()Z

    move-result v11

    if-eqz v11, :cond_0

    .line 135
    const-string v11, "MusicCast"

    const-string v12, "Fetching cast token."

    invoke-static {v11, v12}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    :cond_0
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/cast/CastTokenClientImpl;->mContext:Landroid/content/Context;

    invoke-static {v11}, Lcom/google/android/music/cloudclient/MusicRequest;->getSharedHttpClient(Landroid/content/Context;)Lcom/google/android/music/cloudclient/MusicHttpClient;

    move-result-object v3

    .line 141
    .local v3, "httpClient":Lcom/google/android/music/cloudclient/MusicHttpClient;
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/cast/CastTokenClientImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v11}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v11

    const-string v12, "android_id"

    const-wide/16 v14, 0x0

    invoke-static {v11, v12, v14, v15}, Lcom/google/android/gsf/Gservices;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    .line 143
    .local v2, "androidId":Ljava/lang/String;
    new-instance v8, Lorg/apache/http/client/methods/HttpPost;

    const-string v11, "https://android.clients.google.com/music/playon/createtoken"

    invoke-direct {v8, v11}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 145
    .local v8, "request":Lorg/apache/http/client/methods/HttpPost;
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v7

    .line 147
    .local v7, "params":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    if-eqz p3, :cond_1

    .line 153
    new-instance v11, Lorg/apache/http/message/BasicNameValuePair;

    const-string v12, "host_device_id"

    move-object/from16 v0, p2

    invoke-direct {v11, v12, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v7, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 154
    new-instance v11, Lorg/apache/http/message/BasicNameValuePair;

    const-string v12, "use_host_device_id_param"

    const-string v13, "1"

    invoke-direct {v11, v12, v13}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v7, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 158
    :goto_0
    new-instance v11, Lorg/apache/http/message/BasicNameValuePair;

    const-string v12, "host_device_logging_id"

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/music/cast/CastTokenClientImpl;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    invoke-virtual {v13}, Lcom/google/android/music/preferences/MusicPreferences;->getLoggingId()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v11, v12, v13}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v7, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 160
    new-instance v11, Lorg/apache/http/message/BasicNameValuePair;

    const-string v12, "remote_endpoint_type"

    move-object/from16 v0, p1

    invoke-direct {v11, v12, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v7, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 161
    new-instance v11, Lorg/apache/http/message/BasicNameValuePair;

    const-string v12, "remote_endpoint_id"

    move-object/from16 v0, p2

    invoke-direct {v11, v12, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v7, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 163
    :try_start_0
    new-instance v11, Lorg/apache/http/client/entity/UrlEncodedFormEntity;

    invoke-direct {v11, v7}, Lorg/apache/http/client/entity/UrlEncodedFormEntity;-><init>(Ljava/util/List;)V

    invoke-virtual {v8, v11}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 170
    const/4 v9, 0x0

    .line 172
    .local v9, "response":Lorg/apache/http/HttpResponse;
    :try_start_1
    new-instance v6, Lcom/google/android/music/cloudclient/MusicRequest;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/cast/CastTokenClientImpl;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/music/cast/CastTokenClientImpl;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/music/cast/CastTokenClientImpl;->mPreferencesHolder:Ljava/lang/Object;

    invoke-static {v12, v13}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v12

    invoke-direct {v6, v11, v12}, Lcom/google/android/music/cloudclient/MusicRequest;-><init>(Landroid/content/Context;Lcom/google/android/music/preferences/MusicPreferences;)V

    .line 174
    .local v6, "musicRequest":Lcom/google/android/music/cloudclient/MusicRequest;
    invoke-virtual {v6, v8, v3}, Lcom/google/android/music/cloudclient/MusicRequest;->sendRequest(Lorg/apache/http/client/methods/HttpRequestBase;Lcom/google/android/music/cloudclient/MusicHttpClient;)Lorg/apache/http/HttpResponse;

    move-result-object v9

    .line 175
    move-object/from16 v0, p0

    invoke-direct {v0, v8, v9}, Lcom/google/android/music/cast/CastTokenClientImpl;->extractToken(Lorg/apache/http/client/methods/HttpRequestBase;Lorg/apache/http/HttpResponse;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v11

    .line 183
    invoke-static {v8, v9}, Lcom/google/android/music/cloudclient/MusicRequest;->releaseResponse(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/HttpResponse;)V

    .end local v6    # "musicRequest":Lcom/google/android/music/cloudclient/MusicRequest;
    .end local v9    # "response":Lorg/apache/http/HttpResponse;
    :goto_1
    return-object v11

    .line 156
    :cond_1
    new-instance v11, Lorg/apache/http/message/BasicNameValuePair;

    const-string v12, "host_device_id"

    invoke-direct {v11, v12, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v7, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 164
    :catch_0
    move-exception v10

    .line 166
    .local v10, "uee":Ljava/io/UnsupportedEncodingException;
    const-string v11, "MusicCast"

    const-string v12, "Could not encode params -- should not happen!"

    invoke-static {v11, v12, v10}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 167
    const/4 v11, 0x0

    goto :goto_1

    .line 176
    .end local v10    # "uee":Ljava/io/UnsupportedEncodingException;
    .restart local v9    # "response":Lorg/apache/http/HttpResponse;
    :catch_1
    move-exception v5

    .line 177
    .local v5, "ioe":Ljava/io/IOException;
    :try_start_2
    const-string v11, "MusicCast"

    const-string v12, "Unable to fetch cast token!"

    invoke-static {v11, v12, v5}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 178
    const/4 v11, 0x0

    .line 183
    invoke-static {v8, v9}, Lcom/google/android/music/cloudclient/MusicRequest;->releaseResponse(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/HttpResponse;)V

    goto :goto_1

    .line 179
    .end local v5    # "ioe":Ljava/io/IOException;
    :catch_2
    move-exception v4

    .line 180
    .local v4, "ie":Ljava/lang/InterruptedException;
    :try_start_3
    const-string v11, "MusicCast"

    const-string v12, "Unable to fetch cast token!"

    invoke-static {v11, v12, v4}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 181
    const/4 v11, 0x0

    .line 183
    invoke-static {v8, v9}, Lcom/google/android/music/cloudclient/MusicRequest;->releaseResponse(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/HttpResponse;)V

    goto :goto_1

    .end local v4    # "ie":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v11

    invoke-static {v8, v9}, Lcom/google/android/music/cloudclient/MusicRequest;->releaseResponse(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/HttpResponse;)V

    throw v11
.end method

.method private isLogVerbose()Z
    .locals 1

    .prologue
    .line 194
    iget-object v0, p0, Lcom/google/android/music/cast/CastTokenClientImpl;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->isLogFilesEnabled()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public declared-synchronized clearAllCachedCastTokens()V
    .locals 1

    .prologue
    .line 129
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/cast/CastTokenClientImpl;->mCastTokenCache:Lcom/google/android/music/cast/CastTokenClientImpl$Cache;

    invoke-virtual {v0}, Lcom/google/android/music/cast/CastTokenClientImpl$Cache;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 130
    monitor-exit p0

    return-void

    .line 129
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized clearCachedCastToken(Ljava/lang/String;)V
    .locals 1
    .param p1, "remoteEndpointId"    # Ljava/lang/String;

    .prologue
    .line 124
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/cast/CastTokenClientImpl;->mCastTokenCache:Lcom/google/android/music/cast/CastTokenClientImpl$Cache;

    invoke-virtual {v0, p1}, Lcom/google/android/music/cast/CastTokenClientImpl$Cache;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 125
    monitor-exit p0

    return-void

    .line 124
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getCachedCastToken(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "remoteEndpointId"    # Ljava/lang/String;

    .prologue
    .line 119
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/cast/CastTokenClientImpl;->mCastTokenCache:Lcom/google/android/music/cast/CastTokenClientImpl$Cache;

    invoke-virtual {v0, p1}, Lcom/google/android/music/cast/CastTokenClientImpl$Cache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getCastToken(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "remoteEndpointType"    # Ljava/lang/String;
    .param p2, "remoteEndpointId"    # Ljava/lang/String;

    .prologue
    .line 94
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/music/cast/CastTokenClientImpl;->getCastToken(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getCastToken(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;
    .locals 2
    .param p1, "remoteEndpointType"    # Ljava/lang/String;
    .param p2, "remoteEndpointId"    # Ljava/lang/String;
    .param p3, "useRemoteEndpointIdAsHostDeviceId"    # Z

    .prologue
    .line 102
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/music/cast/CastTokenClientImpl;->mCastTokenCache:Lcom/google/android/music/cast/CastTokenClientImpl$Cache;

    invoke-virtual {v1, p2}, Lcom/google/android/music/cast/CastTokenClientImpl$Cache;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 103
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/music/cast/CastTokenClientImpl;->fetchCastTokenFromServer(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 105
    .local v0, "castToken":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 106
    iget-object v1, p0, Lcom/google/android/music/cast/CastTokenClientImpl;->mCastTokenCache:Lcom/google/android/music/cast/CastTokenClientImpl$Cache;

    invoke-virtual {v1, p2, v0}, Lcom/google/android/music/cast/CastTokenClientImpl$Cache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 109
    .end local v0    # "castToken":Ljava/lang/String;
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/cast/CastTokenClientImpl;->mCastTokenCache:Lcom/google/android/music/cast/CastTokenClientImpl$Cache;

    invoke-virtual {v1, p2}, Lcom/google/android/music/cast/CastTokenClientImpl$Cache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v1

    .line 102
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized hasCachedCastToken(Ljava/lang/String;)Z
    .locals 1
    .param p1, "remoteEndpointId"    # Ljava/lang/String;

    .prologue
    .line 114
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/cast/CastTokenClientImpl;->mCastTokenCache:Lcom/google/android/music/cast/CastTokenClientImpl$Cache;

    invoke-virtual {v0, p1}, Lcom/google/android/music/cast/CastTokenClientImpl$Cache;->containsKey(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public release()V
    .locals 2

    .prologue
    .line 189
    iget-object v0, p0, Lcom/google/android/music/cast/CastTokenClientImpl;->mPreferencesHolder:Ljava/lang/Object;

    invoke-static {v0}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    .line 190
    iget-object v0, p0, Lcom/google/android/music/cast/CastTokenClientImpl;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/music/cast/CastTokenClientImpl;->mAccountChangeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 191
    return-void
.end method
