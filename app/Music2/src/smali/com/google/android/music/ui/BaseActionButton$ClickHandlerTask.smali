.class public Lcom/google/android/music/ui/BaseActionButton$ClickHandlerTask;
.super Ljava/lang/Object;
.source "BaseActionButton.java"

# interfaces
.implements Lcom/google/android/music/utils/async/AsyncRunner;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/BaseActionButton;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ClickHandlerTask"
.end annotation


# instance fields
.field private mButton:Lcom/google/android/music/ui/BaseActionButton;

.field private mMediaList:Lcom/google/android/music/medialist/MediaList;

.field final synthetic this$0:Lcom/google/android/music/ui/BaseActionButton;


# direct methods
.method public constructor <init>(Lcom/google/android/music/ui/BaseActionButton;Lcom/google/android/music/ui/BaseActionButton;Lcom/google/android/music/medialist/MediaList;)V
    .locals 0
    .param p2, "button"    # Lcom/google/android/music/ui/BaseActionButton;
    .param p3, "medialist"    # Lcom/google/android/music/medialist/MediaList;

    .prologue
    .line 141
    iput-object p1, p0, Lcom/google/android/music/ui/BaseActionButton$ClickHandlerTask;->this$0:Lcom/google/android/music/ui/BaseActionButton;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 142
    iput-object p2, p0, Lcom/google/android/music/ui/BaseActionButton$ClickHandlerTask;->mButton:Lcom/google/android/music/ui/BaseActionButton;

    .line 143
    iput-object p3, p0, Lcom/google/android/music/ui/BaseActionButton$ClickHandlerTask;->mMediaList:Lcom/google/android/music/medialist/MediaList;

    .line 144
    return-void
.end method


# virtual methods
.method public backgroundTask()V
    .locals 3

    .prologue
    .line 148
    iget-object v0, p0, Lcom/google/android/music/ui/BaseActionButton$ClickHandlerTask;->mButton:Lcom/google/android/music/ui/BaseActionButton;

    iget-object v1, p0, Lcom/google/android/music/ui/BaseActionButton$ClickHandlerTask;->mButton:Lcom/google/android/music/ui/BaseActionButton;

    invoke-virtual {v1}, Lcom/google/android/music/ui/BaseActionButton;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/BaseActionButton$ClickHandlerTask;->mMediaList:Lcom/google/android/music/medialist/MediaList;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/ui/BaseActionButton;->handleAction(Landroid/content/Context;Lcom/google/android/music/medialist/MediaList;)V

    .line 149
    return-void
.end method

.method public taskCompleted()V
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/google/android/music/ui/BaseActionButton$ClickHandlerTask;->this$0:Lcom/google/android/music/ui/BaseActionButton;

    # getter for: Lcom/google/android/music/ui/BaseActionButton;->mListener:Lcom/google/android/music/ui/BaseActionButton$ActionButtonListener;
    invoke-static {v0}, Lcom/google/android/music/ui/BaseActionButton;->access$000(Lcom/google/android/music/ui/BaseActionButton;)Lcom/google/android/music/ui/BaseActionButton$ActionButtonListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/ui/BaseActionButton$ClickHandlerTask;->this$0:Lcom/google/android/music/ui/BaseActionButton;

    # getter for: Lcom/google/android/music/ui/BaseActionButton;->mListener:Lcom/google/android/music/ui/BaseActionButton$ActionButtonListener;
    invoke-static {v0}, Lcom/google/android/music/ui/BaseActionButton;->access$000(Lcom/google/android/music/ui/BaseActionButton;)Lcom/google/android/music/ui/BaseActionButton$ActionButtonListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/music/ui/BaseActionButton$ActionButtonListener;->onActionFinish()V

    .line 154
    :cond_0
    return-void
.end method
