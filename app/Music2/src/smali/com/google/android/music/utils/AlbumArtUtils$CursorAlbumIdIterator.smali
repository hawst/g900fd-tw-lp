.class Lcom/google/android/music/utils/AlbumArtUtils$CursorAlbumIdIterator;
.super Ljava/lang/Object;
.source "AlbumArtUtils.java"

# interfaces
.implements Lcom/google/android/music/utils/AlbumArtUtils$AlbumIdIterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/utils/AlbumArtUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CursorAlbumIdIterator"
.end annotation


# instance fields
.field private final mCount:I

.field private mCursor:Landroid/database/Cursor;

.field private final mIdIndex:I

.field private mPosition:I


# direct methods
.method public constructor <init>(Landroid/database/Cursor;I)V
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "idIndex"    # I

    .prologue
    .line 647
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 644
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/music/utils/AlbumArtUtils$CursorAlbumIdIterator;->mPosition:I

    .line 648
    iput-object p1, p0, Lcom/google/android/music/utils/AlbumArtUtils$CursorAlbumIdIterator;->mCursor:Landroid/database/Cursor;

    .line 649
    iget-object v0, p0, Lcom/google/android/music/utils/AlbumArtUtils$CursorAlbumIdIterator;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    iput v0, p0, Lcom/google/android/music/utils/AlbumArtUtils$CursorAlbumIdIterator;->mCount:I

    .line 650
    iput p2, p0, Lcom/google/android/music/utils/AlbumArtUtils$CursorAlbumIdIterator;->mIdIndex:I

    .line 651
    return-void
.end method


# virtual methods
.method public close()V
    .locals 1

    .prologue
    .line 671
    iget-object v0, p0, Lcom/google/android/music/utils/AlbumArtUtils$CursorAlbumIdIterator;->mCursor:Landroid/database/Cursor;

    invoke-static {v0}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 672
    return-void
.end method

.method public hasNext()Z
    .locals 2

    .prologue
    .line 655
    iget v0, p0, Lcom/google/android/music/utils/AlbumArtUtils$CursorAlbumIdIterator;->mPosition:I

    iget v1, p0, Lcom/google/android/music/utils/AlbumArtUtils$CursorAlbumIdIterator;->mCount:I

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public next()J
    .locals 3

    .prologue
    .line 660
    iget v0, p0, Lcom/google/android/music/utils/AlbumArtUtils$CursorAlbumIdIterator;->mPosition:I

    iget v1, p0, Lcom/google/android/music/utils/AlbumArtUtils$CursorAlbumIdIterator;->mCount:I

    if-lt v0, v1, :cond_0

    .line 661
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 663
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/utils/AlbumArtUtils$CursorAlbumIdIterator;->mCursor:Landroid/database/Cursor;

    iget v1, p0, Lcom/google/android/music/utils/AlbumArtUtils$CursorAlbumIdIterator;->mPosition:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/google/android/music/utils/AlbumArtUtils$CursorAlbumIdIterator;->mPosition:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 664
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 666
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/utils/AlbumArtUtils$CursorAlbumIdIterator;->mCursor:Landroid/database/Cursor;

    iget v1, p0, Lcom/google/android/music/utils/AlbumArtUtils$CursorAlbumIdIterator;->mIdIndex:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method
