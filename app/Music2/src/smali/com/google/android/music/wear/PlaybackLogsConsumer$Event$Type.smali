.class final enum Lcom/google/android/music/wear/PlaybackLogsConsumer$Event$Type;
.super Ljava/lang/Enum;
.source "PlaybackLogsConsumer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/wear/PlaybackLogsConsumer$Event;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/music/wear/PlaybackLogsConsumer$Event$Type;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/music/wear/PlaybackLogsConsumer$Event$Type;

.field public static final enum ANALYTICS_PLAYBACK:Lcom/google/android/music/wear/PlaybackLogsConsumer$Event$Type;

.field public static final enum COUNT_PLAYBACK:Lcom/google/android/music/wear/PlaybackLogsConsumer$Event$Type;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 177
    new-instance v0, Lcom/google/android/music/wear/PlaybackLogsConsumer$Event$Type;

    const-string v1, "COUNT_PLAYBACK"

    invoke-direct {v0, v1, v2}, Lcom/google/android/music/wear/PlaybackLogsConsumer$Event$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/wear/PlaybackLogsConsumer$Event$Type;->COUNT_PLAYBACK:Lcom/google/android/music/wear/PlaybackLogsConsumer$Event$Type;

    new-instance v0, Lcom/google/android/music/wear/PlaybackLogsConsumer$Event$Type;

    const-string v1, "ANALYTICS_PLAYBACK"

    invoke-direct {v0, v1, v3}, Lcom/google/android/music/wear/PlaybackLogsConsumer$Event$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/wear/PlaybackLogsConsumer$Event$Type;->ANALYTICS_PLAYBACK:Lcom/google/android/music/wear/PlaybackLogsConsumer$Event$Type;

    .line 176
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/music/wear/PlaybackLogsConsumer$Event$Type;

    sget-object v1, Lcom/google/android/music/wear/PlaybackLogsConsumer$Event$Type;->COUNT_PLAYBACK:Lcom/google/android/music/wear/PlaybackLogsConsumer$Event$Type;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/music/wear/PlaybackLogsConsumer$Event$Type;->ANALYTICS_PLAYBACK:Lcom/google/android/music/wear/PlaybackLogsConsumer$Event$Type;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/music/wear/PlaybackLogsConsumer$Event$Type;->$VALUES:[Lcom/google/android/music/wear/PlaybackLogsConsumer$Event$Type;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 176
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/music/wear/PlaybackLogsConsumer$Event$Type;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 176
    const-class v0, Lcom/google/android/music/wear/PlaybackLogsConsumer$Event$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/wear/PlaybackLogsConsumer$Event$Type;

    return-object v0
.end method

.method public static values()[Lcom/google/android/music/wear/PlaybackLogsConsumer$Event$Type;
    .locals 1

    .prologue
    .line 176
    sget-object v0, Lcom/google/android/music/wear/PlaybackLogsConsumer$Event$Type;->$VALUES:[Lcom/google/android/music/wear/PlaybackLogsConsumer$Event$Type;

    invoke-virtual {v0}, [Lcom/google/android/music/wear/PlaybackLogsConsumer$Event$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/music/wear/PlaybackLogsConsumer$Event$Type;

    return-object v0
.end method
