.class public Lcom/google/android/music/playback/LocalAsyncMediaPlayer;
.super Lcom/google/android/music/utils/LoggableHandler;
.source "LocalAsyncMediaPlayer.java"

# interfaces
.implements Lcom/google/android/music/playback/AsyncMediaPlayer;


# instance fields
.field private final LOGV:Z

.field private completionListener:Landroid/media/MediaPlayer$OnCompletionListener;

.field private errorListener:Landroid/media/MediaPlayer$OnErrorListener;

.field private infoListener:Landroid/media/MediaPlayer$OnInfoListener;

.field private mCallback:Lcom/google/android/music/playback/AsyncMediaPlayer$AsyncCommandCallback;

.field private mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

.field private final mContext:Landroid/content/Context;

.field private mCurrentSong:Lcom/google/android/music/download/ContentIdentifier;

.field private mCurrentSongSourceAccount:J

.field private mCurrentSongSourceId:Ljava/lang/String;

.field private mCurrentUrl:Ljava/lang/String;

.field private mDownloadCompletedPercent:F

.field private mDownloadError:I

.field private final mDownloadProgressListener:Lcom/google/android/music/download/IDownloadProgressListener;

.field private mDownloadState:Lcom/google/android/music/download/DownloadState$State;

.field private mDurationMs:J

.field private final mEventLogger:Lcom/google/android/music/eventlog/MusicEventLogger;

.field private mFromUserAction:Z

.field private mHasSentPlayEvent:Z

.field private mHttpSeek:J

.field private volatile mIsCurrentPlayer:Z

.field private mMediaPlayer:Lcom/google/android/music/playback/CompatMediaPlayer;

.field private mMusicFile:Lcom/google/android/music/store/MusicFile;

.field private mPlayEventKey:Ljava/lang/String;

.field private mPlayEventValue:Ljava/lang/String;

.field private mPositionMs:J

.field private final mPrefs:Lcom/google/android/music/preferences/MusicPreferences;

.field private final mServiceHooks:Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;

.field private mStatus:I

.field private mStopWatch:Lcom/google/android/music/playback/StopWatch;

.field private final mWakeLock:Landroid/os/PowerManager$WakeLock;

.field private seekListener:Landroid/media/MediaPlayer$OnSeekCompleteListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "hooks"    # Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;

    .prologue
    const-wide/16 v6, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 132
    const-string v2, "LocalAsyncMediaPlayer"

    const/4 v3, 0x5

    invoke-direct {p0, v2, v3}, Lcom/google/android/music/utils/LoggableHandler;-><init>(Ljava/lang/String;I)V

    .line 63
    sget-object v2, Lcom/google/android/music/utils/DebugUtils$MusicTag;->PLAYBACK_SERVICE:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v2}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->LOGV:Z

    .line 102
    iput-wide v6, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mDurationMs:J

    .line 103
    iput-wide v6, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mPositionMs:J

    .line 106
    iput-wide v6, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mHttpSeek:J

    .line 110
    iput-boolean v4, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mIsCurrentPlayer:Z

    .line 113
    iput-boolean v5, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mHasSentPlayEvent:Z

    .line 119
    iput v5, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mStatus:I

    .line 121
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mDownloadCompletedPercent:F

    .line 122
    sget-object v2, Lcom/google/android/music/download/DownloadState$State;->NOT_STARTED:Lcom/google/android/music/download/DownloadState$State;

    iput-object v2, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mDownloadState:Lcom/google/android/music/download/DownloadState$State;

    .line 123
    iput v4, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mDownloadError:I

    .line 886
    new-instance v2, Lcom/google/android/music/playback/LocalAsyncMediaPlayer$2;

    invoke-direct {v2, p0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer$2;-><init>(Lcom/google/android/music/playback/LocalAsyncMediaPlayer;)V

    iput-object v2, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->completionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    .line 897
    new-instance v2, Lcom/google/android/music/playback/LocalAsyncMediaPlayer$3;

    invoke-direct {v2, p0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer$3;-><init>(Lcom/google/android/music/playback/LocalAsyncMediaPlayer;)V

    iput-object v2, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->errorListener:Landroid/media/MediaPlayer$OnErrorListener;

    .line 935
    new-instance v2, Lcom/google/android/music/playback/LocalAsyncMediaPlayer$4;

    invoke-direct {v2, p0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer$4;-><init>(Lcom/google/android/music/playback/LocalAsyncMediaPlayer;)V

    iput-object v2, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->seekListener:Landroid/media/MediaPlayer$OnSeekCompleteListener;

    .line 943
    new-instance v2, Lcom/google/android/music/playback/LocalAsyncMediaPlayer$5;

    invoke-direct {v2, p0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer$5;-><init>(Lcom/google/android/music/playback/LocalAsyncMediaPlayer;)V

    iput-object v2, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->infoListener:Landroid/media/MediaPlayer$OnInfoListener;

    .line 1076
    new-instance v2, Lcom/google/android/music/playback/LocalAsyncMediaPlayer$6;

    invoke-direct {v2, p0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer$6;-><init>(Lcom/google/android/music/playback/LocalAsyncMediaPlayer;)V

    iput-object v2, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mDownloadProgressListener:Lcom/google/android/music/download/IDownloadProgressListener;

    .line 133
    invoke-static {p1}, Lcom/google/android/music/eventlog/MusicEventLogger;->getInstance(Landroid/content/Context;)Lcom/google/android/music/eventlog/MusicEventLogger;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mEventLogger:Lcom/google/android/music/eventlog/MusicEventLogger;

    .line 134
    iput-object p1, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mContext:Landroid/content/Context;

    .line 135
    invoke-static {p1, p0}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mPrefs:Lcom/google/android/music/preferences/MusicPreferences;

    .line 136
    iput-object p2, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mServiceHooks:Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;

    .line 137
    new-instance v2, Lcom/google/android/music/playback/StopWatch;

    invoke-direct {v2}, Lcom/google/android/music/playback/StopWatch;-><init>()V

    iput-object v2, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mStopWatch:Lcom/google/android/music/playback/StopWatch;

    .line 139
    const-string v2, "power"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/PowerManager;

    .line 140
    .local v1, "pm":Landroid/os/PowerManager;
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v4, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 141
    iget-object v2, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v2, v5}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    .line 145
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    .line 147
    .local v0, "mediaPlayerCreated":Ljava/lang/Object;
    new-instance v2, Lcom/google/android/music/playback/LocalAsyncMediaPlayer$1;

    invoke-direct {v2, p0, v0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer$1;-><init>(Lcom/google/android/music/playback/LocalAsyncMediaPlayer;Ljava/lang/Object;)V

    invoke-virtual {p0, v2}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->post(Ljava/lang/Runnable;)Z

    .line 172
    monitor-enter v0

    .line 173
    :goto_0
    :try_start_0
    iget-object v2, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mMediaPlayer:Lcom/google/android/music/playback/CompatMediaPlayer;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_0

    .line 175
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 176
    :catch_0
    move-exception v2

    goto :goto_0

    .line 180
    :cond_0
    :try_start_2
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 182
    iget-object v2, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mMediaPlayer:Lcom/google/android/music/playback/CompatMediaPlayer;

    iget-object v3, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v3, v4}, Lcom/google/android/music/playback/CompatMediaPlayer;->setWakeMode(Landroid/content/Context;I)V

    .line 183
    return-void

    .line 180
    :catchall_0
    move-exception v2

    :try_start_3
    monitor-exit v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v2
.end method

.method static synthetic access$000(Lcom/google/android/music/playback/LocalAsyncMediaPlayer;)Lcom/google/android/music/playback/CompatMediaPlayer;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalAsyncMediaPlayer;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mMediaPlayer:Lcom/google/android/music/playback/CompatMediaPlayer;

    return-object v0
.end method

.method static synthetic access$002(Lcom/google/android/music/playback/LocalAsyncMediaPlayer;Lcom/google/android/music/playback/CompatMediaPlayer;)Lcom/google/android/music/playback/CompatMediaPlayer;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalAsyncMediaPlayer;
    .param p1, "x1"    # Lcom/google/android/music/playback/CompatMediaPlayer;

    .prologue
    .line 60
    iput-object p1, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mMediaPlayer:Lcom/google/android/music/playback/CompatMediaPlayer;

    return-object p1
.end method

.method static synthetic access$100(Lcom/google/android/music/playback/LocalAsyncMediaPlayer;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalAsyncMediaPlayer;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 60
    invoke-direct {p0, p1}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->releaseWakeLock(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/music/playback/LocalAsyncMediaPlayer;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalAsyncMediaPlayer;

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->onWaitForBuffer()V

    return-void
.end method

.method static synthetic access$1100(Lcom/google/android/music/playback/LocalAsyncMediaPlayer;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalAsyncMediaPlayer;

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->onResumeFromBuffer()V

    return-void
.end method

.method static synthetic access$1200(Lcom/google/android/music/playback/LocalAsyncMediaPlayer;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalAsyncMediaPlayer;

    .prologue
    .line 60
    iget-boolean v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->LOGV:Z

    return v0
.end method

.method static synthetic access$1302(Lcom/google/android/music/playback/LocalAsyncMediaPlayer;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalAsyncMediaPlayer;
    .param p1, "x1"    # I

    .prologue
    .line 60
    iput p1, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mDownloadError:I

    return p1
.end method

.method static synthetic access$1402(Lcom/google/android/music/playback/LocalAsyncMediaPlayer;Lcom/google/android/music/download/DownloadState$State;)Lcom/google/android/music/download/DownloadState$State;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalAsyncMediaPlayer;
    .param p1, "x1"    # Lcom/google/android/music/download/DownloadState$State;

    .prologue
    .line 60
    iput-object p1, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mDownloadState:Lcom/google/android/music/download/DownloadState$State;

    return-object p1
.end method

.method static synthetic access$1500(Lcom/google/android/music/playback/LocalAsyncMediaPlayer;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalAsyncMediaPlayer;

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->handleStreamingChanges()V

    return-void
.end method

.method static synthetic access$1602(Lcom/google/android/music/playback/LocalAsyncMediaPlayer;F)F
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalAsyncMediaPlayer;
    .param p1, "x1"    # F

    .prologue
    .line 60
    iput p1, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mDownloadCompletedPercent:F

    return p1
.end method

.method static synthetic access$1700(Lcom/google/android/music/playback/LocalAsyncMediaPlayer;J)V
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalAsyncMediaPlayer;
    .param p1, "x1"    # J

    .prologue
    .line 60
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->updateDownloadStatus(J)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/music/playback/LocalAsyncMediaPlayer;I)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalAsyncMediaPlayer;
    .param p1, "x1"    # I

    .prologue
    .line 60
    invoke-direct {p0, p1}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->removeStatus(I)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/music/playback/LocalAsyncMediaPlayer;)Lcom/google/android/music/playback/StopWatch;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalAsyncMediaPlayer;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mStopWatch:Lcom/google/android/music/playback/StopWatch;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/music/playback/LocalAsyncMediaPlayer;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalAsyncMediaPlayer;

    .prologue
    .line 60
    iget-boolean v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mIsCurrentPlayer:Z

    return v0
.end method

.method static synthetic access$500(Lcom/google/android/music/playback/LocalAsyncMediaPlayer;)Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalAsyncMediaPlayer;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mServiceHooks:Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/music/playback/LocalAsyncMediaPlayer;)Lcom/google/android/music/download/ContentIdentifier;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalAsyncMediaPlayer;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mCurrentSong:Lcom/google/android/music/download/ContentIdentifier;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/music/playback/LocalAsyncMediaPlayer;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalAsyncMediaPlayer;
    .param p1, "x1"    # Z

    .prologue
    .line 60
    invoke-direct {p0, p1}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->notifyFailure(Z)V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/music/playback/LocalAsyncMediaPlayer;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalAsyncMediaPlayer;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$902(Lcom/google/android/music/playback/LocalAsyncMediaPlayer;J)J
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/playback/LocalAsyncMediaPlayer;
    .param p1, "x1"    # J

    .prologue
    .line 60
    iput-wide p1, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mPositionMs:J

    return-wide p1
.end method

.method private acquireWakeLockAndSendMsg(Ljava/lang/String;Landroid/os/Message;)V
    .locals 3
    .param p1, "reason"    # Ljava/lang/String;
    .param p2, "msg"    # Landroid/os/Message;

    .prologue
    .line 870
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AsyncMediaPlayer.acquireWakeLockAndSendMsg(\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/music/utils/MusicUtils;->debugLog(Ljava/lang/Object;)V

    .line 872
    iget-object v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 873
    invoke-virtual {p0, p2}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->sendMessage(Landroid/os/Message;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 874
    const-string v0, "LocalAsyncMediaPlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to send message: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 875
    iget-object v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 877
    :cond_0
    return-void
.end method

.method private declared-synchronized addStatus(I)V
    .locals 1
    .param p1, "status"    # I

    .prologue
    .line 678
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mStatus:I

    or-int/2addr v0, p1

    iput v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mStatus:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 679
    monitor-exit p0

    return-void

    .line 678
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private handleFailedDownload()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1040
    iget-boolean v1, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mIsCurrentPlayer:Z

    if-nez v1, :cond_0

    .line 1050
    :goto_0
    return v0

    .line 1043
    :cond_0
    const/16 v1, 0x8

    invoke-direct {p0, v1}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->removeStatus(I)V

    .line 1044
    const/16 v1, 0x30

    invoke-direct {p0, v1}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->addStatus(I)V

    .line 1045
    const-string v1, "LocalAsyncMediaPlayer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failing playback because download failed: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mCurrentSong:Lcom/google/android/music/download/ContentIdentifier;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 1048
    iget v1, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mDownloadError:I

    invoke-static {v1}, Lcom/google/android/music/download/DownloadErrorType;->isServerError(I)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v0, 0x1

    .line 1049
    .local v0, "skipToNext":Z
    :cond_1
    invoke-direct {p0, v0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->notifyFailure(Z)V

    goto :goto_0
.end method

.method private handleMessageImp(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v2, 0x1

    .line 197
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/music/playback/AsyncMediaPlayer$AsyncCommandCallback;

    .line 199
    .local v0, "cb":Lcom/google/android/music/playback/AsyncMediaPlayer$AsyncCommandCallback;
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->isReleased()Z

    move-result v3

    if-eqz v3, :cond_0

    iget v3, p1, Landroid/os/Message;->what:I

    const/4 v4, 0x4

    if-eq v3, v4, :cond_0

    .line 200
    const-string v2, "LocalAsyncMediaPlayer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Command after release: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->what:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 265
    :goto_0
    return-void

    .line 204
    :cond_0
    iget v3, p1, Landroid/os/Message;->what:I

    packed-switch v3, :pswitch_data_0

    goto :goto_0

    .line 208
    :pswitch_0
    iget v3, p1, Landroid/os/Message;->arg1:I

    if-ne v3, v2, :cond_1

    :goto_1
    invoke-direct {p0, v2, v0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->setDataSourceImpl(ZLcom/google/android/music/playback/AsyncMediaPlayer$AsyncCommandCallback;)V

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    .line 212
    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->startFX()V

    .line 213
    iget-object v2, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mMediaPlayer:Lcom/google/android/music/playback/CompatMediaPlayer;

    invoke-virtual {v2}, Lcom/google/android/music/playback/CompatMediaPlayer;->start()V

    .line 214
    iget-object v2, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mMediaPlayer:Lcom/google/android/music/playback/CompatMediaPlayer;

    invoke-virtual {v2}, Lcom/google/android/music/playback/CompatMediaPlayer;->getCurrentPosition()I

    move-result v2

    int-to-long v2, v2

    iput-wide v2, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mPositionMs:J

    .line 215
    iget-object v2, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mStopWatch:Lcom/google/android/music/playback/StopWatch;

    invoke-virtual {v2}, Lcom/google/android/music/playback/StopWatch;->reset()V

    .line 216
    iget-object v2, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mStopWatch:Lcom/google/android/music/playback/StopWatch;

    invoke-virtual {v2}, Lcom/google/android/music/playback/StopWatch;->start()V

    goto :goto_0

    .line 220
    :pswitch_2
    iget-object v2, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mMediaPlayer:Lcom/google/android/music/playback/CompatMediaPlayer;

    invoke-virtual {v2}, Lcom/google/android/music/playback/CompatMediaPlayer;->pause()V

    .line 221
    iget-object v2, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mMediaPlayer:Lcom/google/android/music/playback/CompatMediaPlayer;

    invoke-virtual {v2}, Lcom/google/android/music/playback/CompatMediaPlayer;->getCurrentPosition()I

    move-result v2

    int-to-long v2, v2

    iput-wide v2, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mPositionMs:J

    .line 222
    iget-object v2, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mStopWatch:Lcom/google/android/music/playback/StopWatch;

    invoke-virtual {v2}, Lcom/google/android/music/playback/StopWatch;->pause()V

    .line 223
    iget-object v2, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mStopWatch:Lcom/google/android/music/playback/StopWatch;

    invoke-virtual {v2}, Lcom/google/android/music/playback/StopWatch;->reset()V

    .line 224
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->stopFX()V

    goto :goto_0

    .line 228
    :pswitch_3
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "AsyncMediaPlayer.RELEASE "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/music/utils/MusicUtils;->debugLog(Ljava/lang/Object;)V

    .line 229
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->notifyIfSongPlayed()V

    .line 230
    iget-object v2, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mMediaPlayer:Lcom/google/android/music/playback/CompatMediaPlayer;

    invoke-virtual {v2}, Lcom/google/android/music/playback/CompatMediaPlayer;->release()V

    .line 232
    iget-object v2, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mStopWatch:Lcom/google/android/music/playback/StopWatch;

    invoke-virtual {v2}, Lcom/google/android/music/playback/StopWatch;->pause()V

    .line 233
    iget-object v2, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mStopWatch:Lcom/google/android/music/playback/StopWatch;

    invoke-virtual {v2}, Lcom/google/android/music/playback/StopWatch;->reset()V

    .line 235
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "AsyncMediaPlayer.RELEASE done "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/music/utils/MusicUtils;->debugLog(Ljava/lang/Object;)V

    .line 236
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mMediaPlayer:Lcom/google/android/music/playback/CompatMediaPlayer;

    .line 238
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->quit()V

    .line 241
    const-string v2, "release"

    invoke-direct {p0, v2}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->releaseWakeLock(Ljava/lang/String;)V

    .line 243
    invoke-static {p0}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 247
    :pswitch_4
    iget-object v2, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mStopWatch:Lcom/google/android/music/playback/StopWatch;

    invoke-virtual {v2}, Lcom/google/android/music/playback/StopWatch;->pause()V

    .line 248
    iget v2, p1, Landroid/os/Message;->arg1:I

    invoke-direct {p0, v2}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->seekImpl(I)V

    .line 249
    iget-object v2, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mStopWatch:Lcom/google/android/music/playback/StopWatch;

    invoke-virtual {v2}, Lcom/google/android/music/playback/StopWatch;->reset()V

    goto/16 :goto_0

    .line 253
    :pswitch_5
    iget-object v2, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mMediaPlayer:Lcom/google/android/music/playback/CompatMediaPlayer;

    invoke-virtual {v2}, Lcom/google/android/music/playback/CompatMediaPlayer;->reset()V

    goto/16 :goto_0

    .line 257
    :pswitch_6
    iget v2, p1, Landroid/os/Message;->arg1:I

    int-to-float v2, v2

    const/high16 v3, 0x447a0000    # 1000.0f

    div-float v1, v2, v3

    .line 258
    .local v1, "vol":F
    iget-object v2, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mMediaPlayer:Lcom/google/android/music/playback/CompatMediaPlayer;

    invoke-virtual {v2, v1, v1}, Lcom/google/android/music/playback/CompatMediaPlayer;->setVolume(FF)V

    goto/16 :goto_0

    .line 262
    .end local v1    # "vol":F
    :pswitch_7
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->updateDownloadStatusImpl()V

    goto/16 :goto_0

    .line 204
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_6
        :pswitch_4
        :pswitch_5
        :pswitch_7
    .end packed-switch
.end method

.method private handleStreamingChanges()V
    .locals 5

    .prologue
    .line 1054
    iget-boolean v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->LOGV:Z

    if-eqz v0, :cond_0

    .line 1055
    const-string v0, "LocalAsyncMediaPlayer"

    const-string v1, "handleStreamingChanges: mIsCurrentPlayer=%b mCurrentSong=%s mDownloadState=%s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-boolean v4, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mIsCurrentPlayer:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mCurrentSong:Lcom/google/android/music/download/ContentIdentifier;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mDownloadState:Lcom/google/android/music/download/DownloadState$State;

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1060
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mDownloadState:Lcom/google/android/music/download/DownloadState$State;

    sget-object v1, Lcom/google/android/music/download/DownloadState$State;->FAILED:Lcom/google/android/music/download/DownloadState$State;

    if-ne v0, v1, :cond_2

    .line 1063
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->handleFailedDownload()Z

    .line 1074
    :cond_1
    :goto_0
    return-void

    .line 1064
    :cond_2
    iget-object v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mDownloadState:Lcom/google/android/music/download/DownloadState$State;

    sget-object v1, Lcom/google/android/music/download/DownloadState$State;->NOT_STARTED:Lcom/google/android/music/download/DownloadState$State;

    if-eq v0, v1, :cond_3

    iget-object v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mDownloadState:Lcom/google/android/music/download/DownloadState$State;

    sget-object v1, Lcom/google/android/music/download/DownloadState$State;->DOWNLOADING:Lcom/google/android/music/download/DownloadState$State;

    if-eq v0, v1, :cond_3

    iget-object v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mDownloadState:Lcom/google/android/music/download/DownloadState$State;

    sget-object v1, Lcom/google/android/music/download/DownloadState$State;->COMPLETED:Lcom/google/android/music/download/DownloadState$State;

    if-ne v0, v1, :cond_4

    .line 1067
    :cond_3
    const/16 v0, 0x20

    invoke-direct {p0, v0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->addStatus(I)V

    goto :goto_0

    .line 1068
    :cond_4
    iget-object v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mDownloadState:Lcom/google/android/music/download/DownloadState$State;

    sget-object v1, Lcom/google/android/music/download/DownloadState$State;->CANCELED:Lcom/google/android/music/download/DownloadState$State;

    if-eq v0, v1, :cond_1

    .line 1070
    iget-object v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mCurrentUrl:Ljava/lang/String;

    const-string v1, "http"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1071
    const-string v0, "LocalAsyncMediaPlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Song is not downloading, but given http streamUrl: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mCurrentSong:Lcom/google/android/music/download/ContentIdentifier;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private declared-synchronized isStatusOn(I)Z
    .locals 1
    .param p1, "status"    # I

    .prologue
    .line 686
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mStatus:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    and-int/2addr v0, p1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private notifyFailure(Z)V
    .locals 1
    .param p1, "skipToNext"    # Z

    .prologue
    .line 1103
    iget-object v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mCallback:Lcom/google/android/music/playback/AsyncMediaPlayer$AsyncCommandCallback;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mIsCurrentPlayer:Z

    if-eqz v0, :cond_0

    .line 1104
    iget-object v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mCallback:Lcom/google/android/music/playback/AsyncMediaPlayer$AsyncCommandCallback;

    invoke-interface {v0, p1}, Lcom/google/android/music/playback/AsyncMediaPlayer$AsyncCommandCallback;->onFailure(Z)V

    .line 1106
    :cond_0
    return-void
.end method

.method private notifyIfSongPlayed()V
    .locals 10

    .prologue
    .line 268
    iget-object v1, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mMediaPlayer:Lcom/google/android/music/playback/CompatMediaPlayer;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mCurrentSong:Lcom/google/android/music/download/ContentIdentifier;

    if-eqz v1, :cond_0

    .line 269
    iget-object v1, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/music/eventlog/MusicEventLogger;->getInstance(Landroid/content/Context;)Lcom/google/android/music/eventlog/MusicEventLogger;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    iget-object v3, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mMusicFile:Lcom/google/android/music/store/MusicFile;

    const/4 v4, 0x0

    iget-boolean v5, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mFromUserAction:Z

    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->position()J

    move-result-wide v6

    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->duration()J

    move-result-wide v8

    invoke-virtual/range {v1 .. v9}, Lcom/google/android/music/eventlog/MusicEventLogger;->logPlayEvent(Lcom/google/android/music/store/ContainerDescriptor;Lcom/google/android/music/store/MusicFile;ZZJJ)V

    .line 271
    iget-object v1, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->duration()J

    move-result-wide v2

    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->position()J

    move-result-wide v4

    invoke-static {v1, v2, v3, v4, v5}, Lcom/google/android/music/utils/PlaybackUtils;->isPlayed(Landroid/content/Context;JJ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 273
    :try_start_0
    iget-object v1, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mServiceHooks:Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;

    iget-object v2, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mCurrentSong:Lcom/google/android/music/download/ContentIdentifier;

    invoke-interface {v1, v2}, Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;->markSongPlayed(Lcom/google/android/music/download/ContentIdentifier;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 279
    :cond_0
    :goto_0
    return-void

    .line 274
    :catch_0
    move-exception v0

    .line 275
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "LocalAsyncMediaPlayer"

    const-string v2, "Could not mark song as played"

    invoke-static {v1, v2, v0}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private onResumeFromBuffer()V
    .locals 3

    .prologue
    .line 1023
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->isPreparing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1026
    const-string v0, "LocalAsyncMediaPlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "resumeFromBuffer: id="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mCurrentSong:Lcom/google/android/music/download/ContentIdentifier;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1028
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->removeStatus(I)V

    .line 1029
    iget-object v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mStopWatch:Lcom/google/android/music/playback/StopWatch;

    invoke-virtual {v0}, Lcom/google/android/music/playback/StopWatch;->start()V

    .line 1030
    iget-boolean v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mIsCurrentPlayer:Z

    if-eqz v0, :cond_0

    .line 1031
    iget-object v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mServiceHooks:Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;

    invoke-interface {v0}, Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;->notifyPlayStateChanged()V

    .line 1034
    :cond_0
    return-void
.end method

.method private onWaitForBuffer()V
    .locals 3

    .prologue
    .line 991
    iget-object v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mDownloadState:Lcom/google/android/music/download/DownloadState$State;

    sget-object v1, Lcom/google/android/music/download/DownloadState$State;->FAILED:Lcom/google/android/music/download/DownloadState$State;

    if-ne v0, v1, :cond_1

    .line 992
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->handleFailedDownload()Z

    move-result v0

    if-nez v0, :cond_0

    .line 994
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->stop()V

    .line 1011
    :cond_0
    :goto_0
    return-void

    .line 996
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->isPreparing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1000
    const-string v0, "LocalAsyncMediaPlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "waitForBuffer: id="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mCurrentSong:Lcom/google/android/music/download/ContentIdentifier;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1002
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->addStatus(I)V

    .line 1003
    iget-object v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mMediaPlayer:Lcom/google/android/music/playback/CompatMediaPlayer;

    invoke-virtual {v0}, Lcom/google/android/music/playback/CompatMediaPlayer;->getCurrentPosition()I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mPositionMs:J

    .line 1004
    iget-object v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mStopWatch:Lcom/google/android/music/playback/StopWatch;

    invoke-virtual {v0}, Lcom/google/android/music/playback/StopWatch;->pause()V

    .line 1005
    iget-object v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mStopWatch:Lcom/google/android/music/playback/StopWatch;

    invoke-virtual {v0}, Lcom/google/android/music/playback/StopWatch;->reset()V

    .line 1006
    iget-boolean v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mIsCurrentPlayer:Z

    if-eqz v0, :cond_0

    .line 1007
    iget-object v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mServiceHooks:Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;

    invoke-interface {v0}, Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;->notifyPlayStateChanged()V

    goto :goto_0
.end method

.method private releaseWakeLock(Ljava/lang/String;)V
    .locals 2
    .param p1, "reason"    # Ljava/lang/String;

    .prologue
    .line 881
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AsyncMediaPlayer.releaseWakeLock(\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/music/utils/MusicUtils;->debugLog(Ljava/lang/Object;)V

    .line 883
    iget-object v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 884
    return-void
.end method

.method private declared-synchronized removeStatus(I)V
    .locals 2
    .param p1, "status"    # I

    .prologue
    .line 682
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mStatus:I

    const v1, 0x7fffffff

    xor-int/2addr v1, p1

    and-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mStatus:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 683
    monitor-exit p0

    return-void

    .line 682
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private seekImpl(I)V
    .locals 12
    .param p1, "ms"    # I

    .prologue
    const/16 v11, 0x10

    const/4 v10, 0x0

    const/16 v9, 0x8

    .line 596
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->isStreaming()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 597
    invoke-direct {p0, v9}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->addStatus(I)V

    .line 598
    iget-object v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mMediaPlayer:Lcom/google/android/music/playback/CompatMediaPlayer;

    invoke-virtual {v0}, Lcom/google/android/music/playback/CompatMediaPlayer;->getNextPlayer()Landroid/media/MediaPlayer;

    move-result-object v8

    .line 599
    .local v8, "nextMediaPlayer":Landroid/media/MediaPlayer;
    iget-object v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mMediaPlayer:Lcom/google/android/music/playback/CompatMediaPlayer;

    invoke-virtual {v0}, Lcom/google/android/music/playback/CompatMediaPlayer;->stop()V

    .line 600
    iget-object v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mMediaPlayer:Lcom/google/android/music/playback/CompatMediaPlayer;

    invoke-virtual {v0}, Lcom/google/android/music/playback/CompatMediaPlayer;->reset()V

    .line 602
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mIsCurrentPlayer:Z

    if-eqz v0, :cond_0

    .line 603
    iget-object v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mServiceHooks:Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;

    invoke-interface {v0}, Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;->notifyOpenStarted()V

    .line 605
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mMediaPlayer:Lcom/google/android/music/playback/CompatMediaPlayer;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/android/music/playback/CompatMediaPlayer;->setAudioStreamType(I)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    .line 607
    :try_start_1
    iget-object v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mServiceHooks:Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;

    iget-object v1, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mCurrentSong:Lcom/google/android/music/download/ContentIdentifier;

    int-to-long v2, p1

    iget-object v4, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mDownloadProgressListener:Lcom/google/android/music/download/IDownloadProgressListener;

    const/4 v5, 0x1

    const/4 v6, 0x1

    invoke-interface/range {v0 .. v6}, Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;->streamTrack(Lcom/google/android/music/download/ContentIdentifier;JLcom/google/android/music/download/IDownloadProgressListener;ZZ)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mCurrentUrl:Ljava/lang/String;
    :try_end_1
    .catch Lcom/google/android/music/download/cache/OutOfSpaceException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/google/android/music/download/stream/DownloadRequestException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    .line 624
    :try_start_2
    iget-object v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mCurrentUrl:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 625
    const-string v0, "LocalAsyncMediaPlayer"

    const-string v1, "Failed to resolve seek url"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 626
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->removeStatus(I)V

    .line 627
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->isReleased()Z

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mIsCurrentPlayer:Z

    if-eqz v0, :cond_1

    .line 628
    const/16 v0, 0x10

    invoke-direct {p0, v0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->addStatus(I)V

    .line 629
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->notifyFailure(Z)V

    .line 675
    .end local v8    # "nextMediaPlayer":Landroid/media/MediaPlayer;
    :cond_1
    :goto_0
    return-void

    .line 610
    .restart local v8    # "nextMediaPlayer":Landroid/media/MediaPlayer;
    :catch_0
    move-exception v7

    .line 611
    .local v7, "e":Lcom/google/android/music/download/cache/OutOfSpaceException;
    const-string v0, "LocalAsyncMediaPlayer"

    invoke-virtual {v7}, Lcom/google/android/music/download/cache/OutOfSpaceException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v7}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 612
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mDownloadError:I

    .line 613
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->removeStatus(I)V

    .line 614
    const/16 v0, 0x30

    invoke-direct {p0, v0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->addStatus(I)V

    .line 615
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->notifyFailure(Z)V
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    goto :goto_0

    .line 647
    .end local v7    # "e":Lcom/google/android/music/download/cache/OutOfSpaceException;
    :catch_1
    move-exception v7

    .line 648
    .local v7, "e":Ljava/lang/IllegalArgumentException;
    const-string v0, "LocalAsyncMediaPlayer"

    invoke-virtual {v7}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v7}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 649
    invoke-direct {p0, v9}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->removeStatus(I)V

    .line 650
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->isReleased()Z

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mIsCurrentPlayer:Z

    if-eqz v0, :cond_1

    .line 651
    invoke-direct {p0, v11}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->addStatus(I)V

    .line 652
    invoke-direct {p0, v10}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->notifyFailure(Z)V

    goto :goto_0

    .line 617
    .end local v7    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v7

    .line 618
    .local v7, "e":Lcom/google/android/music/download/stream/DownloadRequestException;
    :try_start_3
    const-string v0, "LocalAsyncMediaPlayer"

    const-string v1, "Failed to request seek in stream"

    invoke-static {v0, v1, v7}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 619
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->removeStatus(I)V

    .line 620
    const/16 v0, 0x30

    invoke-direct {p0, v0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->addStatus(I)V

    .line 621
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->notifyFailure(Z)V
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    goto :goto_0

    .line 655
    .end local v7    # "e":Lcom/google/android/music/download/stream/DownloadRequestException;
    :catch_3
    move-exception v7

    .line 656
    .local v7, "e":Ljava/io/IOException;
    const-string v0, "LocalAsyncMediaPlayer"

    invoke-virtual {v7}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v7}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 657
    invoke-direct {p0, v9}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->removeStatus(I)V

    .line 658
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->isReleased()Z

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mIsCurrentPlayer:Z

    if-eqz v0, :cond_1

    .line 659
    invoke-direct {p0, v11}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->addStatus(I)V

    .line 660
    invoke-direct {p0, v10}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->notifyFailure(Z)V

    goto :goto_0

    .line 633
    .end local v7    # "e":Ljava/io/IOException;
    :cond_2
    :try_start_4
    iget-object v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mCurrentUrl:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->setMediaPlayerDataSource(Ljava/lang/String;)V

    .line 634
    if-eqz v8, :cond_3

    .line 635
    iget-object v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mMediaPlayer:Lcom/google/android/music/playback/CompatMediaPlayer;

    invoke-virtual {v0, v8}, Lcom/google/android/music/playback/CompatMediaPlayer;->setNextMediaPlayerCompat(Landroid/media/MediaPlayer;)V

    .line 637
    :cond_3
    iget-object v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mMediaPlayer:Lcom/google/android/music/playback/CompatMediaPlayer;

    invoke-virtual {v0}, Lcom/google/android/music/playback/CompatMediaPlayer;->prepare()V

    .line 638
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->removeStatus(I)V

    .line 639
    int-to-long v0, p1

    iput-wide v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mHttpSeek:J

    .line 640
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mPositionMs:J

    .line 641
    iget-boolean v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mIsCurrentPlayer:Z

    if-eqz v0, :cond_4

    .line 642
    iget-object v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mServiceHooks:Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;

    invoke-interface {v0}, Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;->notifyOpenComplete()V

    .line 644
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 645
    iget-object v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mMediaPlayer:Lcom/google/android/music/playback/CompatMediaPlayer;

    invoke-virtual {v0}, Lcom/google/android/music/playback/CompatMediaPlayer;->start()V
    :try_end_4
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 672
    .end local v8    # "nextMediaPlayer":Landroid/media/MediaPlayer;
    :cond_5
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 673
    iget-object v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mStopWatch:Lcom/google/android/music/playback/StopWatch;

    invoke-virtual {v0}, Lcom/google/android/music/playback/StopWatch;->start()V

    goto/16 :goto_0

    .line 665
    :cond_6
    iget-object v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mMediaPlayer:Lcom/google/android/music/playback/CompatMediaPlayer;

    invoke-virtual {v0, p1}, Lcom/google/android/music/playback/CompatMediaPlayer;->seekTo(I)V

    .line 666
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mHttpSeek:J

    .line 667
    iget-object v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mMediaPlayer:Lcom/google/android/music/playback/CompatMediaPlayer;

    invoke-virtual {v0}, Lcom/google/android/music/playback/CompatMediaPlayer;->getCurrentPosition()I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mPositionMs:J

    .line 668
    iget-boolean v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mIsCurrentPlayer:Z

    if-eqz v0, :cond_5

    .line 669
    iget-object v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mServiceHooks:Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;

    invoke-interface {v0}, Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;->notifyPlayStateChanged()V

    goto :goto_1
.end method

.method private setDataSourceImpl(ZLcom/google/android/music/playback/AsyncMediaPlayer$AsyncCommandCallback;)V
    .locals 19
    .param p1, "fromUserAction"    # Z
    .param p2, "cb"    # Lcom/google/android/music/playback/AsyncMediaPlayer$AsyncCommandCallback;

    .prologue
    .line 327
    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mCallback:Lcom/google/android/music/playback/AsyncMediaPlayer$AsyncCommandCallback;

    .line 328
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mCurrentUrl:Ljava/lang/String;

    .line 329
    const-wide/16 v2, 0x0

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mCurrentSongSourceAccount:J

    .line 330
    const-string v2, ""

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mCurrentSongSourceId:Ljava/lang/String;

    .line 331
    const/16 v16, 0x0

    .line 332
    .local v16, "shouldStream":Z
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->LOGV:Z

    if-eqz v2, :cond_0

    .line 333
    const-string v2, "LocalAsyncMediaPlayer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setDataSourceImpl: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mCurrentSong:Lcom/google/android/music/download/ContentIdentifier;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 335
    :cond_0
    const/16 v2, 0x8

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->addStatus(I)V

    .line 337
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mCurrentSong:Lcom/google/android/music/download/ContentIdentifier;

    invoke-virtual {v2}, Lcom/google/android/music/download/ContentIdentifier;->isCacheable()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 338
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mMusicFile:Lcom/google/android/music/store/MusicFile;

    invoke-virtual {v2}, Lcom/google/android/music/store/MusicFile;->getSourceAccount()I

    move-result v2

    int-to-long v2, v2

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mCurrentSongSourceAccount:J

    .line 339
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mMusicFile:Lcom/google/android/music/store/MusicFile;

    invoke-virtual {v2}, Lcom/google/android/music/store/MusicFile;->getSourceId()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mCurrentSongSourceId:Ljava/lang/String;

    .line 341
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mMusicFile:Lcom/google/android/music/store/MusicFile;

    invoke-virtual {v2}, Lcom/google/android/music/store/MusicFile;->getLocalCopyType()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 399
    :cond_1
    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mMusicFile:Lcom/google/android/music/store/MusicFile;

    invoke-virtual {v2}, Lcom/google/android/music/store/MusicFile;->getTrackType()I

    move-result v2

    const/4 v3, 0x4

    if-eq v2, v3, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mMusicFile:Lcom/google/android/music/store/MusicFile;

    invoke-virtual {v2}, Lcom/google/android/music/store/MusicFile;->getTrackType()I

    move-result v2

    const/4 v3, 0x5

    if-ne v2, v3, :cond_9

    .line 401
    :cond_2
    const-string v2, "nautilusId"

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mPlayEventKey:Ljava/lang/String;

    .line 402
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mMusicFile:Lcom/google/android/music/store/MusicFile;

    invoke-virtual {v2}, Lcom/google/android/music/store/MusicFile;->getSourceId()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mPlayEventValue:Ljava/lang/String;

    .line 433
    :goto_1
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mIsCurrentPlayer:Z

    if-eqz v2, :cond_3

    if-eqz p1, :cond_3

    .line 434
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mServiceHooks:Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;

    invoke-interface {v2}, Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;->cancelAllStreamingTracks()V

    .line 437
    :cond_3
    if-eqz v16, :cond_4

    .line 439
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mServiceHooks:Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mCurrentSong:Lcom/google/android/music/download/ContentIdentifier;

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mPositionMs:J

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mDownloadProgressListener:Lcom/google/android/music/download/IDownloadProgressListener;

    move-object/from16 v0, p0

    iget-boolean v7, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mIsCurrentPlayer:Z

    move/from16 v8, p1

    invoke-interface/range {v2 .. v8}, Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;->streamTrack(Lcom/google/android/music/download/ContentIdentifier;JLcom/google/android/music/download/IDownloadProgressListener;ZZ)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mCurrentUrl:Ljava/lang/String;

    .line 441
    const/16 v2, 0x20

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->addStatus(I)V

    .line 442
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mPositionMs:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_4

    .line 443
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mPositionMs:J

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mHttpSeek:J

    .line 444
    const-wide/16 v2, 0x0

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mPositionMs:J
    :try_end_0
    .catch Lcom/google/android/music/download/cache/OutOfSpaceException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/music/download/stream/DownloadRequestException; {:try_start_0 .. :try_end_0} :catch_1

    .line 462
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mCurrentUrl:Ljava/lang/String;

    if-nez v2, :cond_d

    .line 463
    const-string v2, "LocalAsyncMediaPlayer"

    const-string v3, "Failed to resolve track "

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 464
    const/16 v2, 0x8

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->removeStatus(I)V

    .line 465
    const/16 v2, 0x10

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->addStatus(I)V

    .line 466
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->notifyFailure(Z)V

    .line 550
    :cond_5
    :goto_2
    return-void

    .line 346
    :sswitch_0
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mCurrentSongSourceAccount:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_6

    const-string v10, ""

    .line 348
    .local v10, "eventLogSourceId":Ljava/lang/String;
    :goto_3
    const v2, 0x12110

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mCurrentSong:Lcom/google/android/music/download/ContentIdentifier;

    invoke-virtual {v5}, Lcom/google/android/music/download/ContentIdentifier;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object v10, v3, v4

    const/4 v4, 0x2

    const/4 v5, 0x2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    .line 352
    const-string v2, "LocalAsyncMediaPlayer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Event logging MUSIC_START_PLAYBACK_REQUESTED: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mMusicFile:Lcom/google/android/music/store/MusicFile;

    invoke-virtual {v4}, Lcom/google/android/music/store/MusicFile;->getLocalId()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ": local playback"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 355
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mMusicFile:Lcom/google/android/music/store/MusicFile;

    invoke-virtual {v3}, Lcom/google/android/music/store/MusicFile;->getSourceId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mCurrentUrl:Ljava/lang/String;

    goto/16 :goto_0

    .line 346
    .end local v10    # "eventLogSourceId":Ljava/lang/String;
    :cond_6
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mCurrentSongSourceId:Ljava/lang/String;

    goto :goto_3

    .line 363
    :sswitch_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mMusicFile:Lcom/google/android/music/store/MusicFile;

    invoke-static {v2, v3}, Lcom/google/android/music/download/cache/CacheUtils;->resolveMusicPath(Landroid/content/Context;Lcom/google/android/music/store/MusicFile;)Ljava/io/File;

    move-result-object v14

    .line 364
    .local v14, "location":Ljava/io/File;
    if-eqz v14, :cond_8

    invoke-virtual {v14}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 365
    invoke-virtual {v14}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mCurrentUrl:Ljava/lang/String;

    .line 367
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v17

    .line 368
    .local v17, "store":Lcom/google/android/music/store/Store;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mMusicFile:Lcom/google/android/music/store/MusicFile;

    invoke-virtual {v2}, Lcom/google/android/music/store/MusicFile;->getLocalId()J

    move-result-wide v2

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Lcom/google/android/music/store/Store;->isLocalCopyCp(J)Z

    move-result v13

    .line 369
    .local v13, "isCp":Z
    move/from16 v16, v13

    .line 370
    if-eqz v13, :cond_1

    .line 371
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mMusicFile:Lcom/google/android/music/store/MusicFile;

    invoke-virtual {v2}, Lcom/google/android/music/store/MusicFile;->isNautilus()Z

    move-result v2

    if-nez v2, :cond_7

    .line 379
    const-string v2, "LocalAsyncMediaPlayer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "CP file is no longer nautilus: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mCurrentSong:Lcom/google/android/music/download/ContentIdentifier;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 380
    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mPrefs:Lcom/google/android/music/preferences/MusicPreferences;

    invoke-virtual {v2}, Lcom/google/android/music/preferences/MusicPreferences;->isNautilusStatusStale()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 381
    const/16 v2, 0x10

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mDownloadError:I

    .line 382
    const/16 v2, 0x8

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->removeStatus(I)V

    .line 383
    const/16 v2, 0x10

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->addStatus(I)V

    .line 384
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->notifyFailure(Z)V

    goto/16 :goto_2

    .line 389
    .end local v13    # "isCp":Z
    .end local v17    # "store":Lcom/google/android/music/store/Store;
    :cond_8
    const/16 v16, 0x1

    .line 391
    goto/16 :goto_0

    .line 395
    .end local v14    # "location":Ljava/io/File;
    :sswitch_2
    const/16 v16, 0x1

    goto/16 :goto_0

    .line 403
    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mMusicFile:Lcom/google/android/music/store/MusicFile;

    invoke-virtual {v2}, Lcom/google/android/music/store/MusicFile;->getLocalCopyType()I

    move-result v2

    const/16 v3, 0x12c

    if-ne v2, v3, :cond_a

    .line 405
    const-string v2, "sideloadedId"

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mPlayEventKey:Ljava/lang/String;

    .line 406
    const-string v2, ""

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mPlayEventValue:Ljava/lang/String;

    goto/16 :goto_1

    .line 408
    :cond_a
    const-string v2, "lockerId"

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mPlayEventKey:Ljava/lang/String;

    .line 409
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mMusicFile:Lcom/google/android/music/store/MusicFile;

    invoke-virtual {v2}, Lcom/google/android/music/store/MusicFile;->getSourceId()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mPlayEventValue:Ljava/lang/String;

    goto/16 :goto_1

    .line 413
    :cond_b
    const/16 v16, 0x1

    .line 415
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mCurrentSong:Lcom/google/android/music/download/ContentIdentifier;

    invoke-virtual {v2}, Lcom/google/android/music/download/ContentIdentifier;->isSharedDomain()Z

    move-result v2

    if-eqz v2, :cond_c

    .line 416
    const-string v2, "sharedPurchasedId"

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mPlayEventKey:Ljava/lang/String;

    .line 417
    const-string v2, ""

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mPlayEventValue:Ljava/lang/String;

    goto/16 :goto_1

    .line 419
    :cond_c
    const-string v2, "LocalAsyncMediaPlayer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unable to identify the play type for id: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mCurrentSong:Lcom/google/android/music/download/ContentIdentifier;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 446
    :catch_0
    move-exception v9

    .line 447
    .local v9, "e":Lcom/google/android/music/download/cache/OutOfSpaceException;
    const-string v2, "LocalAsyncMediaPlayer"

    invoke-virtual {v9}, Lcom/google/android/music/download/cache/OutOfSpaceException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v9}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 448
    const/4 v2, 0x2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mDownloadError:I

    .line 449
    const/16 v2, 0x8

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->removeStatus(I)V

    .line 450
    const/16 v2, 0x10

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->addStatus(I)V

    .line 451
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->notifyFailure(Z)V

    goto/16 :goto_2

    .line 453
    .end local v9    # "e":Lcom/google/android/music/download/cache/OutOfSpaceException;
    :catch_1
    move-exception v9

    .line 454
    .local v9, "e":Lcom/google/android/music/download/stream/DownloadRequestException;
    const-string v2, "LocalAsyncMediaPlayer"

    invoke-virtual {v9}, Lcom/google/android/music/download/stream/DownloadRequestException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v9}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 455
    const/16 v2, 0x8

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->removeStatus(I)V

    .line 456
    const/16 v2, 0x10

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->addStatus(I)V

    .line 457
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->notifyFailure(Z)V

    goto/16 :goto_2

    .line 470
    .end local v9    # "e":Lcom/google/android/music/download/stream/DownloadRequestException;
    :cond_d
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->isStreaming()Z

    move-result v2

    if-eqz v2, :cond_e

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mIsCurrentPlayer:Z

    if-eqz v2, :cond_e

    .line 471
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mServiceHooks:Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;

    invoke-interface {v2}, Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;->notifyOpenStarted()V

    .line 475
    :cond_e
    :try_start_1
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->LOGV:Z

    if-eqz v2, :cond_f

    .line 476
    const-string v2, "LocalAsyncMediaPlayer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setDataSourceImpl: mCurrentUrl="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mCurrentUrl:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 478
    :cond_f
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mMediaPlayer:Lcom/google/android/music/playback/CompatMediaPlayer;

    invoke-virtual {v2}, Lcom/google/android/music/playback/CompatMediaPlayer;->reset()V

    .line 479
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mMediaPlayer:Lcom/google/android/music/playback/CompatMediaPlayer;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/android/music/playback/CompatMediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 480
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mCurrentUrl:Ljava/lang/String;

    const-string v3, "content://"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_14

    .line 481
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mCurrentUrl:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v18

    .line 482
    .local v18, "uriForPath":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mMediaPlayer:Lcom/google/android/music/playback/CompatMediaPlayer;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mContext:Landroid/content/Context;

    move-object/from16 v0, v18

    invoke-virtual {v2, v3, v0}, Lcom/google/android/music/playback/CompatMediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V

    .line 483
    const/16 v2, 0x20

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->removeStatus(I)V

    .line 487
    .end local v18    # "uriForPath":Landroid/net/Uri;
    :goto_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mMediaPlayer:Lcom/google/android/music/playback/CompatMediaPlayer;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Lcom/google/android/music/playback/CompatMediaPlayer;->setAudioStreamType(I)V

    .line 489
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mMediaPlayer:Lcom/google/android/music/playback/CompatMediaPlayer;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->completionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    invoke-virtual {v2, v3}, Lcom/google/android/music/playback/CompatMediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 490
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mMediaPlayer:Lcom/google/android/music/playback/CompatMediaPlayer;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->errorListener:Landroid/media/MediaPlayer$OnErrorListener;

    invoke-virtual {v2, v3}, Lcom/google/android/music/playback/CompatMediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 491
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mMediaPlayer:Lcom/google/android/music/playback/CompatMediaPlayer;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->seekListener:Landroid/media/MediaPlayer$OnSeekCompleteListener;

    invoke-virtual {v2, v3}, Lcom/google/android/music/playback/CompatMediaPlayer;->setOnSeekCompleteListener(Landroid/media/MediaPlayer$OnSeekCompleteListener;)V

    .line 493
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->LOGV:Z

    if-eqz v2, :cond_10

    .line 494
    const-string v2, "LocalAsyncMediaPlayer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MediaPlayer.prepare: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mCurrentSong:Lcom/google/android/music/download/ContentIdentifier;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 496
    :cond_10
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mMediaPlayer:Lcom/google/android/music/playback/CompatMediaPlayer;

    invoke-virtual {v2}, Lcom/google/android/music/playback/CompatMediaPlayer;->prepare()V

    .line 498
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->LOGV:Z

    if-eqz v2, :cond_11

    .line 499
    const-string v2, "LocalAsyncMediaPlayer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MediaPlayer.prepare FINISHED: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mCurrentSong:Lcom/google/android/music/download/ContentIdentifier;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 501
    :cond_11
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->isReleased()Z

    move-result v2

    if-eqz v2, :cond_15

    .line 504
    const/16 v2, 0x8

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->removeStatus(I)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_3

    goto/16 :goto_2

    .line 522
    :catch_2
    move-exception v11

    .line 523
    .local v11, "ex":Ljava/io/IOException;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->isReleased()Z

    move-result v2

    if-nez v2, :cond_12

    .line 524
    const-string v2, "LocalAsyncMediaPlayer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Track: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mCurrentSong:Lcom/google/android/music/download/ContentIdentifier;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v11}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v11}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 532
    .end local v11    # "ex":Ljava/io/IOException;
    :cond_12
    :goto_5
    const/16 v2, 0x8

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->removeStatus(I)V

    .line 535
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->isReleased()Z

    move-result v2

    if-nez v2, :cond_13

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->isStreaming()Z

    move-result v2

    if-nez v2, :cond_13

    .line 536
    const/16 v2, 0x10

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->addStatus(I)V

    .line 537
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->notifyFailure(Z)V

    .line 540
    :cond_13
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mCurrentSongSourceAccount:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_5

    .line 542
    const-string v2, "LocalAsyncMediaPlayer"

    const-string v3, "Requesting import of media store"

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 543
    new-instance v12, Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mContext:Landroid/content/Context;

    const-class v3, Lcom/google/android/music/store/MediaStoreImportService;

    invoke-direct {v12, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 544
    .local v12, "importIntent":Landroid/content/Intent;
    const-string v2, "MediaStoreImportService.import"

    invoke-virtual {v12, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 545
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v12}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    move-result-object v15

    .line 546
    .local v15, "service":Landroid/content/ComponentName;
    if-nez v15, :cond_5

    .line 547
    const-string v2, "LocalAsyncMediaPlayer"

    const-string v3, "Failed to start MediaStoreImportService"

    new-instance v4, Ljava/lang/Exception;

    invoke-direct {v4}, Ljava/lang/Exception;-><init>()V

    invoke-static {v2, v3, v4}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_2

    .line 485
    .end local v12    # "importIntent":Landroid/content/Intent;
    .end local v15    # "service":Landroid/content/ComponentName;
    :cond_14
    :try_start_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mCurrentUrl:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->setMediaPlayerDataSource(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_3

    goto/16 :goto_4

    .line 526
    :catch_3
    move-exception v11

    .line 527
    .local v11, "ex":Ljava/lang/IllegalArgumentException;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->isReleased()Z

    move-result v2

    if-nez v2, :cond_12

    .line 528
    const-string v2, "LocalAsyncMediaPlayer"

    invoke-virtual {v11}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v11}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_5

    .line 508
    .end local v11    # "ex":Ljava/lang/IllegalArgumentException;
    :cond_15
    :try_start_3
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mPositionMs:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_16

    if-nez v16, :cond_16

    .line 509
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mMediaPlayer:Lcom/google/android/music/playback/CompatMediaPlayer;

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mPositionMs:J

    long-to-int v3, v4

    invoke-virtual {v2, v3}, Lcom/google/android/music/playback/CompatMediaPlayer;->seekTo(I)V

    .line 512
    :cond_16
    invoke-direct/range {p0 .. p0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->startFX()V

    .line 514
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mMediaPlayer:Lcom/google/android/music/playback/CompatMediaPlayer;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->infoListener:Landroid/media/MediaPlayer$OnInfoListener;

    invoke-virtual {v2, v3}, Lcom/google/android/music/playback/CompatMediaPlayer;->setOnInfoListener(Landroid/media/MediaPlayer$OnInfoListener;)V

    .line 515
    const/4 v2, 0x2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->addStatus(I)V

    .line 517
    const/16 v2, 0x8

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->removeStatus(I)V

    .line 518
    invoke-interface/range {p2 .. p2}, Lcom/google/android/music/playback/AsyncMediaPlayer$AsyncCommandCallback;->onSuccess()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_3

    goto/16 :goto_2

    .line 341
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_2
        0x64 -> :sswitch_1
        0xc8 -> :sswitch_1
        0x12c -> :sswitch_0
    .end sparse-switch
.end method

.method private setMediaPlayerDataSource(Ljava/lang/String;)V
    .locals 3
    .param p1, "path"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 561
    const-string v2, "http"

    invoke-virtual {p1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 562
    iget-object v2, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mMediaPlayer:Lcom/google/android/music/playback/CompatMediaPlayer;

    invoke-virtual {v2, p1}, Lcom/google/android/music/playback/CompatMediaPlayer;->setDataSource(Ljava/lang/String;)V

    .line 573
    :goto_0
    return-void

    .line 567
    :cond_0
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 568
    .local v1, "fis":Ljava/io/FileInputStream;
    invoke-virtual {v1}, Ljava/io/FileInputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v0

    .line 570
    .local v0, "fd":Ljava/io/FileDescriptor;
    iget-object v2, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mMediaPlayer:Lcom/google/android/music/playback/CompatMediaPlayer;

    invoke-virtual {v2, v0}, Lcom/google/android/music/playback/CompatMediaPlayer;->setDataSource(Ljava/io/FileDescriptor;)V

    .line 571
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V

    goto :goto_0
.end method

.method private startFX()V
    .locals 3

    .prologue
    .line 702
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.media.action.OPEN_AUDIO_EFFECT_CONTROL_SESSION"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 703
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "android.media.extra.AUDIO_SESSION"

    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->getAudioSessionId()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 704
    const-string v1, "android.media.extra.PACKAGE_NAME"

    iget-object v2, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 705
    iget-object v1, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 706
    return-void
.end method

.method private stopFX()V
    .locals 3

    .prologue
    .line 695
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.media.action.CLOSE_AUDIO_EFFECT_CONTROL_SESSION"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 696
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "android.media.extra.AUDIO_SESSION"

    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->getAudioSessionId()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 697
    const-string v1, "android.media.extra.PACKAGE_NAME"

    iget-object v2, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 698
    iget-object v1, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 699
    return-void
.end method

.method private updateDownloadStatus(J)V
    .locals 3
    .param p1, "millisTillNextPossibleOutage"    # J

    .prologue
    const/16 v2, 0x8

    .line 959
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->isReleased()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 966
    :cond_0
    :goto_0
    return-void

    .line 962
    :cond_1
    invoke-virtual {p0, v2}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->removeMessages(I)V

    .line 963
    iget-object v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mDownloadState:Lcom/google/android/music/download/DownloadState$State;

    sget-object v1, Lcom/google/android/music/download/DownloadState$State;->COMPLETED:Lcom/google/android/music/download/DownloadState$State;

    if-eq v0, v1, :cond_0

    .line 964
    invoke-virtual {p0, v2, p1, p2}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method

.method private updateDownloadStatusImpl()V
    .locals 12

    .prologue
    .line 969
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->duration()J

    move-result-wide v6

    .line 970
    .local v6, "totalDuration":J
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->position()J

    move-result-wide v8

    const-wide/16 v10, 0x1388

    add-long v0, v8, v10

    .line 971
    .local v0, "currentPosition":J
    long-to-float v5, v0

    long-to-float v8, v6

    div-float v4, v5, v8

    .line 973
    .local v4, "playbackCompletePercent":F
    iget v5, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mDownloadCompletedPercent:F

    cmpl-float v5, v5, v4

    if-lez v5, :cond_0

    .line 976
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->onResumeFromBuffer()V

    .line 980
    iget v5, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mDownloadCompletedPercent:F

    long-to-float v8, v6

    mul-float/2addr v5, v8

    float-to-long v8, v5

    sub-long v2, v8, v0

    .line 982
    .local v2, "nextSchedule":J
    invoke-direct {p0, v2, v3}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->updateDownloadStatus(J)V

    .line 988
    .end local v2    # "nextSchedule":J
    :goto_0
    return-void

    .line 986
    :cond_0
    invoke-direct {p0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->onWaitForBuffer()V

    goto :goto_0
.end method


# virtual methods
.method public duration()J
    .locals 2

    .prologue
    .line 841
    iget-wide v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mDurationMs:J

    return-wide v0
.end method

.method public getAudioSessionId()I
    .locals 4

    .prologue
    .line 807
    :try_start_0
    iget-object v1, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mMediaPlayer:Lcom/google/android/music/playback/CompatMediaPlayer;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "getAudioSessionId"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Class;

    invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 808
    .local v0, "m":Ljava/lang/reflect/Method;
    iget-object v1, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mMediaPlayer:Lcom/google/android/music/playback/CompatMediaPlayer;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 813
    .end local v0    # "m":Ljava/lang/reflect/Method;
    :goto_0
    return v1

    .line 809
    :catch_0
    move-exception v1

    .line 813
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public getErrorType()I
    .locals 1

    .prologue
    .line 797
    iget-object v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mCurrentSong:Lcom/google/android/music/download/ContentIdentifier;

    if-nez v0, :cond_0

    .line 798
    const/4 v0, 0x1

    .line 801
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mDownloadError:I

    goto :goto_0
.end method

.method public getRemoteSongId()Ljava/lang/String;
    .locals 4

    .prologue
    .line 582
    iget-wide v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mCurrentSongSourceAccount:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mCurrentSongSourceId:Ljava/lang/String;

    goto :goto_0
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 188
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->handleMessageImp(Landroid/os/Message;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 194
    return-void

    .line 189
    :catch_0
    move-exception v0

    .line 190
    .local v0, "t":Ljava/lang/Throwable;
    invoke-static {v0}, Lcom/google/android/music/utils/MusicUtils;->debugLog(Ljava/lang/Object;)V

    .line 191
    const-string v1, "handleMessage"

    invoke-direct {p0, v1}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->releaseWakeLock(Ljava/lang/String;)V

    .line 192
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public isInErrorState()Z
    .locals 1

    .prologue
    .line 792
    const/16 v0, 0x10

    invoke-direct {p0, v0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->isStatusOn(I)Z

    move-result v0

    return v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 691
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->isStatusOn(I)Z

    move-result v0

    return v0
.end method

.method public isPlaying()Z
    .locals 1

    .prologue
    .line 777
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->isStatusOn(I)Z

    move-result v0

    return v0
.end method

.method public isPreparing()Z
    .locals 1

    .prologue
    .line 782
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->isStatusOn(I)Z

    move-result v0

    return v0
.end method

.method public isReleased()Z
    .locals 1

    .prologue
    .line 772
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->isStatusOn(I)Z

    move-result v0

    return v0
.end method

.method public isRenderingAudioLocally()Z
    .locals 1

    .prologue
    .line 1019
    const/4 v0, 0x1

    return v0
.end method

.method public isStreaming()Z
    .locals 1

    .prologue
    .line 787
    const/16 v0, 0x20

    invoke-direct {p0, v0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->isStatusOn(I)Z

    move-result v0

    return v0
.end method

.method public pause()V
    .locals 3

    .prologue
    const/4 v2, 0x3

    .line 722
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->isReleased()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 733
    :goto_0
    return-void

    .line 725
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AsyncMediaPlayer("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mCurrentSong:Lcom/google/android/music/download/ContentIdentifier;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ").pause "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/music/utils/MusicUtils;->debugLog(Ljava/lang/Object;)V

    .line 726
    invoke-virtual {p0, v2}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->removeMessages(I)V

    .line 727
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->removeMessages(I)V

    .line 728
    const/4 v0, 0x7

    invoke-virtual {p0, v0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->removeMessages(I)V

    .line 730
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->removeStatus(I)V

    .line 732
    const-string v0, "pause"

    invoke-virtual {p0, v2}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->acquireWakeLockAndSendMsg(Ljava/lang/String;Landroid/os/Message;)V

    goto :goto_0
.end method

.method public position()J
    .locals 4

    .prologue
    .line 836
    iget-wide v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mPositionMs:J

    iget-object v2, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mStopWatch:Lcom/google/android/music/playback/StopWatch;

    invoke-virtual {v2}, Lcom/google/android/music/playback/StopWatch;->getTime()J

    move-result-wide v2

    add-long/2addr v0, v2

    iget-wide v2, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mHttpSeek:J

    add-long/2addr v0, v2

    return-wide v0
.end method

.method public release()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 753
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->isReleased()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 768
    :goto_0
    return-void

    .line 756
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AsyncMediaPlayer("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mCurrentSong:Lcom/google/android/music/download/ContentIdentifier;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ").release "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/music/utils/MusicUtils;->debugLog(Ljava/lang/Object;)V

    .line 759
    iget-object v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mMediaPlayer:Lcom/google/android/music/playback/CompatMediaPlayer;

    invoke-virtual {v0, v2}, Lcom/google/android/music/playback/CompatMediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 760
    iget-object v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mMediaPlayer:Lcom/google/android/music/playback/CompatMediaPlayer;

    invoke-virtual {v0, v2}, Lcom/google/android/music/playback/CompatMediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 761
    iget-object v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mMediaPlayer:Lcom/google/android/music/playback/CompatMediaPlayer;

    invoke-virtual {v0, v2}, Lcom/google/android/music/playback/CompatMediaPlayer;->setOnSeekCompleteListener(Landroid/media/MediaPlayer$OnSeekCompleteListener;)V

    .line 762
    iget-object v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mMediaPlayer:Lcom/google/android/music/playback/CompatMediaPlayer;

    invoke-virtual {v0, v2}, Lcom/google/android/music/playback/CompatMediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 763
    iget-object v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mMediaPlayer:Lcom/google/android/music/playback/CompatMediaPlayer;

    invoke-virtual {v0, v2}, Lcom/google/android/music/playback/CompatMediaPlayer;->setOnInfoListener(Landroid/media/MediaPlayer$OnInfoListener;)V

    .line 764
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->addStatus(I)V

    .line 765
    const/16 v0, 0xc

    invoke-direct {p0, v0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->removeStatus(I)V

    .line 766
    invoke-virtual {p0, v2}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 767
    const-string v0, "release"

    const/4 v1, 0x4

    invoke-virtual {p0, v1}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->acquireWakeLockAndSendMsg(Ljava/lang/String;Landroid/os/Message;)V

    goto :goto_0
.end method

.method public seek(J)J
    .locals 5
    .param p1, "position"    # J

    .prologue
    const-wide/16 v2, 0x0

    const/4 v4, 0x6

    .line 846
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->isReleased()Z

    move-result v1

    if-eqz v1, :cond_0

    move-wide p1, v2

    .line 855
    .end local p1    # "position":J
    :goto_0
    return-wide p1

    .line 849
    .restart local p1    # "position":J
    :cond_0
    invoke-virtual {p0, v4}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->removeMessages(I)V

    .line 850
    iput-wide p1, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mPositionMs:J

    .line 851
    iput-wide v2, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mHttpSeek:J

    .line 852
    long-to-int v1, p1

    const/4 v2, 0x0

    invoke-virtual {p0, v4, v1, v2}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    .line 853
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mStopWatch:Lcom/google/android/music/playback/StopWatch;

    invoke-virtual {v1}, Lcom/google/android/music/playback/StopWatch;->reset()V

    .line 854
    const-string v1, "seek"

    invoke-direct {p0, v1, v0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->acquireWakeLockAndSendMsg(Ljava/lang/String;Landroid/os/Message;)V

    goto :goto_0
.end method

.method public setAsCurrentPlayer()V
    .locals 1

    .prologue
    .line 323
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mIsCurrentPlayer:Z

    .line 324
    return-void
.end method

.method public setAudioSessionId(I)V
    .locals 6
    .param p1, "fxsession"    # I

    .prologue
    .line 821
    if-gez p1, :cond_0

    .line 832
    :goto_0
    return-void

    .line 826
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mMediaPlayer:Lcom/google/android/music/playback/CompatMediaPlayer;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "setAudioSessionId"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    sget-object v5, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 827
    .local v0, "m":Ljava/lang/reflect/Method;
    iget-object v1, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mMediaPlayer:Lcom/google/android/music/playback/CompatMediaPlayer;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 828
    .end local v0    # "m":Ljava/lang/reflect/Method;
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public setDataSource(Lcom/google/android/music/download/ContentIdentifier;JJZZLcom/google/android/music/store/MusicFile;Lcom/google/android/music/store/ContainerDescriptor;Lcom/google/android/music/playback/AsyncMediaPlayer$AsyncCommandCallback;)V
    .locals 8
    .param p1, "songId"    # Lcom/google/android/music/download/ContentIdentifier;
    .param p2, "positionMs"    # J
    .param p4, "durationMs"    # J
    .param p6, "isCurrentPlayer"    # Z
    .param p7, "fromUserAction"    # Z
    .param p8, "musicFile"    # Lcom/google/android/music/store/MusicFile;
    .param p9, "containerDescriptor"    # Lcom/google/android/music/store/ContainerDescriptor;
    .param p10, "cb"    # Lcom/google/android/music/playback/AsyncMediaPlayer$AsyncCommandCallback;

    .prologue
    const/4 v2, 0x1

    .line 285
    iget-boolean v3, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->LOGV:Z

    if-eqz v3, :cond_0

    .line 286
    const-string v3, "LocalAsyncMediaPlayer"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setDataSource: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 288
    :cond_0
    if-nez p1, :cond_1

    .line 289
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Invalid ContentIdentifier"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 291
    :cond_1
    iget-object v3, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mCurrentSong:Lcom/google/android/music/download/ContentIdentifier;

    if-eqz v3, :cond_2

    .line 292
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Can\'t reuse. Previously used for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mCurrentSong:Lcom/google/android/music/download/ContentIdentifier;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", now for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 295
    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "AsyncMediaPlayer setDataSource "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ": "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/music/utils/MusicUtils;->debugLog(Ljava/lang/Object;)V

    .line 296
    iput-object p1, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mCurrentSong:Lcom/google/android/music/download/ContentIdentifier;

    .line 297
    iput-wide p2, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mPositionMs:J

    .line 298
    iput-wide p4, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mDurationMs:J

    .line 299
    iput-boolean p6, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mIsCurrentPlayer:Z

    .line 300
    move-object/from16 v0, p8

    iput-object v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mMusicFile:Lcom/google/android/music/store/MusicFile;

    .line 301
    iput-boolean p7, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mFromUserAction:Z

    .line 302
    move-object/from16 v0, p9

    iput-object v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    .line 303
    move-object/from16 v0, p10

    invoke-virtual {p0, v2, v0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    .line 304
    .local v1, "msg":Landroid/os/Message;
    if-eqz p7, :cond_3

    :goto_0
    iput v2, v1, Landroid/os/Message;->arg1:I

    .line 305
    const-string v2, "setDataSource"

    invoke-direct {p0, v2, v1}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->acquireWakeLockAndSendMsg(Ljava/lang/String;Landroid/os/Message;)V

    .line 306
    return-void

    .line 304
    :cond_3
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setNextPlayer(Lcom/google/android/music/playback/AsyncMediaPlayer;)V
    .locals 3
    .param p1, "player"    # Lcom/google/android/music/playback/AsyncMediaPlayer;

    .prologue
    .line 310
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->isReleased()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 319
    :goto_0
    return-void

    .line 313
    :cond_0
    const/4 v0, 0x0

    .line 314
    .local v0, "localPlayer":Lcom/google/android/music/playback/LocalAsyncMediaPlayer;
    instance-of v1, p1, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;

    if-eqz v1, :cond_1

    move-object v0, p1

    .line 315
    check-cast v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;

    .line 318
    :cond_1
    iget-object v2, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mMediaPlayer:Lcom/google/android/music/playback/CompatMediaPlayer;

    if-eqz v0, :cond_2

    iget-object v1, v0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mMediaPlayer:Lcom/google/android/music/playback/CompatMediaPlayer;

    :goto_1
    invoke-virtual {v2, v1}, Lcom/google/android/music/playback/CompatMediaPlayer;->setNextMediaPlayerCompat(Landroid/media/MediaPlayer;)V

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public setVolume(F)V
    .locals 4
    .param p1, "vol"    # F

    .prologue
    const/4 v3, 0x5

    .line 860
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->isReleased()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 866
    :goto_0
    return-void

    .line 863
    :cond_0
    invoke-virtual {p0, v3}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->removeMessages(I)V

    .line 864
    const/high16 v1, 0x447a0000    # 1000.0f

    mul-float/2addr v1, p1

    float-to-int v1, v1

    const/4 v2, 0x0

    invoke-virtual {p0, v3, v1, v2}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    .line 865
    .local v0, "msg":Landroid/os/Message;
    const-string v1, "setVolume"

    invoke-direct {p0, v1, v0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->acquireWakeLockAndSendMsg(Ljava/lang/String;Landroid/os/Message;)V

    goto :goto_0
.end method

.method public start()V
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 710
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->isReleased()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 718
    :goto_0
    return-void

    .line 713
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AsyncMediaPlayer("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mCurrentSong:Lcom/google/android/music/download/ContentIdentifier;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ").start "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/music/utils/MusicUtils;->debugLog(Ljava/lang/Object;)V

    .line 714
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->removeMessages(I)V

    .line 715
    invoke-virtual {p0, v2}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->removeMessages(I)V

    .line 716
    const-string v0, "start"

    invoke-virtual {p0, v2}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->acquireWakeLockAndSendMsg(Ljava/lang/String;Landroid/os/Message;)V

    .line 717
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->addStatus(I)V

    goto :goto_0
.end method

.method public stop()V
    .locals 3

    .prologue
    const/4 v2, 0x7

    .line 737
    invoke-virtual {p0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->isReleased()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 749
    :goto_0
    return-void

    .line 740
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AsyncMediaPlayer("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mCurrentSong:Lcom/google/android/music/download/ContentIdentifier;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ").stop "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/music/utils/MusicUtils;->debugLog(Ljava/lang/Object;)V

    .line 741
    iget-boolean v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mIsCurrentPlayer:Z

    if-eqz v0, :cond_1

    .line 742
    iget-object v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mServiceHooks:Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;

    invoke-interface {v0}, Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;->cancelTryNext()V

    .line 744
    :cond_1
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->removeMessages(I)V

    .line 745
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->removeMessages(I)V

    .line 746
    invoke-virtual {p0, v2}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->removeMessages(I)V

    .line 747
    const/16 v0, 0xe

    invoke-direct {p0, v0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->removeStatus(I)V

    .line 748
    const-string v0, "stop"

    invoke-virtual {p0, v2}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->acquireWakeLockAndSendMsg(Ljava/lang/String;Landroid/os/Message;)V

    goto :goto_0
.end method
