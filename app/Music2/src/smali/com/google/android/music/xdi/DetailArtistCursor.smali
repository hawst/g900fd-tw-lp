.class Lcom/google/android/music/xdi/DetailArtistCursor;
.super Landroid/database/MatrixCursor;
.source "DetailArtistCursor.java"


# static fields
.field private static final PROJECTION_ARTISTS:[Ljava/lang/String;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 22
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "artist"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "artworkUrl"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/xdi/DetailArtistCursor;->PROJECTION_ARTISTS:[Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "artistId"    # Ljava/lang/String;

    .prologue
    .line 35
    invoke-direct {p0, p2}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 36
    iput-object p1, p0, Lcom/google/android/music/xdi/DetailArtistCursor;->mContext:Landroid/content/Context;

    .line 37
    new-instance v0, Lcom/google/android/music/xdi/ProjectionMap;

    invoke-direct {v0, p2}, Lcom/google/android/music/xdi/ProjectionMap;-><init>([Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/music/xdi/DetailArtistCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    .line 39
    invoke-direct {p0, p3}, Lcom/google/android/music/xdi/DetailArtistCursor;->addRowForArtist(Ljava/lang/String;)V

    .line 40
    return-void
.end method

.method private addRowForArtist(Ljava/lang/String;)V
    .locals 8
    .param p1, "artistId"    # Ljava/lang/String;

    .prologue
    .line 43
    iget-object v4, p0, Lcom/google/android/music/xdi/DetailArtistCursor;->mContext:Landroid/content/Context;

    sget-object v5, Lcom/google/android/music/xdi/DetailArtistCursor;->PROJECTION_ARTISTS:[Ljava/lang/String;

    invoke-static {v4, p1, v5}, Lcom/google/android/music/xdi/XdiUtils;->getArtistCursor(Landroid/content/Context;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 45
    .local v1, "cursor":Landroid/database/Cursor;
    if-nez v1, :cond_0

    .line 72
    :goto_0
    return-void

    .line 50
    :cond_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 51
    iget-object v4, p0, Lcom/google/android/music/xdi/DetailArtistCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    invoke-virtual {v4}, Lcom/google/android/music/xdi/ProjectionMap;->size()I

    move-result v4

    new-array v3, v4, [Ljava/lang/Object;

    .line 52
    .local v3, "values":[Ljava/lang/Object;
    const/4 v4, 0x0

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 53
    .local v2, "name":Ljava/lang/String;
    const/4 v4, 0x1

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 54
    .local v0, "artUri":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 55
    iget-object v4, p0, Lcom/google/android/music/xdi/DetailArtistCursor;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultArtistArtUri(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 58
    :cond_1
    iget-object v4, p0, Lcom/google/android/music/xdi/DetailArtistCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v5, "_id"

    invoke-interface {v1}, Landroid/database/Cursor;->getPosition()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v3, v5, v6}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 59
    iget-object v4, p0, Lcom/google/android/music/xdi/DetailArtistCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v5, "foreground_image_uri"

    invoke-virtual {v4, v3, v5, v0}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 60
    iget-object v4, p0, Lcom/google/android/music/xdi/DetailArtistCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v5, "background_image_uri"

    invoke-virtual {v4, v3, v5, v0}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 61
    iget-object v4, p0, Lcom/google/android/music/xdi/DetailArtistCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v5, "badge_uri"

    iget-object v6, p0, Lcom/google/android/music/xdi/DetailArtistCursor;->mContext:Landroid/content/Context;

    invoke-static {v6}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultBadgeUri(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v3, v5, v6}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 63
    iget-object v4, p0, Lcom/google/android/music/xdi/DetailArtistCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v5, "color_hint"

    iget-object v6, p0, Lcom/google/android/music/xdi/DetailArtistCursor;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0c00a6

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v3, v5, v6}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 65
    iget-object v4, p0, Lcom/google/android/music/xdi/DetailArtistCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v5, "text_color_hint"

    const/4 v6, 0x0

    invoke-virtual {v4, v3, v5, v6}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 67
    invoke-virtual {p0, v3}, Lcom/google/android/music/xdi/DetailArtistCursor;->addRow([Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 70
    .end local v0    # "artUri":Ljava/lang/String;
    .end local v2    # "name":Ljava/lang/String;
    .end local v3    # "values":[Ljava/lang/Object;
    :cond_2
    invoke-static {v1}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto :goto_0

    :catchall_0
    move-exception v4

    invoke-static {v1}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v4
.end method
