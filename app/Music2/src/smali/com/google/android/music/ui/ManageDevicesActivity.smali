.class public Lcom/google/android/music/ui/ManageDevicesActivity;
.super Lcom/google/android/music/ui/BaseActivity;
.source "ManageDevicesActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/google/android/music/ui/BaseActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected doesSupportDownloadOnlyBanner()Z
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x0

    return v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedState"    # Landroid/os/Bundle;

    .prologue
    .line 14
    invoke-super {p0, p1}, Lcom/google/android/music/ui/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 15
    invoke-virtual {p0}, Lcom/google/android/music/ui/ManageDevicesActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0324

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/music/ui/ManageDevicesActivity;->setActionBarTitle(Ljava/lang/String;)V

    .line 17
    invoke-virtual {p0}, Lcom/google/android/music/ui/ManageDevicesActivity;->getContent()Landroid/support/v4/app/Fragment;

    move-result-object v1

    if-nez v1, :cond_0

    .line 18
    new-instance v0, Lcom/google/android/music/ui/ManageDevicesFragment;

    invoke-direct {v0}, Lcom/google/android/music/ui/ManageDevicesFragment;-><init>()V

    .line 19
    .local v0, "fragment":Landroid/support/v4/app/Fragment;
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/music/ui/ManageDevicesActivity;->replaceContent(Landroid/support/v4/app/Fragment;Z)V

    .line 21
    .end local v0    # "fragment":Landroid/support/v4/app/Fragment;
    :cond_0
    return-void
.end method

.method protected showNowPlayingBar()Z
    .locals 1

    .prologue
    .line 29
    const/4 v0, 0x0

    return v0
.end method
