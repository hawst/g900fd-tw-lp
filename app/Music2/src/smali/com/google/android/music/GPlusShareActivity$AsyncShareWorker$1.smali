.class Lcom/google/android/music/GPlusShareActivity$AsyncShareWorker$1;
.super Ljava/lang/Object;
.source "GPlusShareActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/GPlusShareActivity$AsyncShareWorker;->handleMessage(Landroid/os/Message;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/music/GPlusShareActivity$AsyncShareWorker;

.field final synthetic val$shareUrl:Lcom/google/android/music/SharePreviewResponse;


# direct methods
.method constructor <init>(Lcom/google/android/music/GPlusShareActivity$AsyncShareWorker;Lcom/google/android/music/SharePreviewResponse;)V
    .locals 0

    .prologue
    .line 170
    iput-object p1, p0, Lcom/google/android/music/GPlusShareActivity$AsyncShareWorker$1;->this$1:Lcom/google/android/music/GPlusShareActivity$AsyncShareWorker;

    iput-object p2, p0, Lcom/google/android/music/GPlusShareActivity$AsyncShareWorker$1;->val$shareUrl:Lcom/google/android/music/SharePreviewResponse;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 173
    iget-object v0, p0, Lcom/google/android/music/GPlusShareActivity$AsyncShareWorker$1;->this$1:Lcom/google/android/music/GPlusShareActivity$AsyncShareWorker;

    iget-object v0, v0, Lcom/google/android/music/GPlusShareActivity$AsyncShareWorker;->this$0:Lcom/google/android/music/GPlusShareActivity;

    # getter for: Lcom/google/android/music/GPlusShareActivity;->mCanceled:Z
    invoke-static {v0}, Lcom/google/android/music/GPlusShareActivity;->access$000(Lcom/google/android/music/GPlusShareActivity;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 174
    iget-object v0, p0, Lcom/google/android/music/GPlusShareActivity$AsyncShareWorker$1;->val$shareUrl:Lcom/google/android/music/SharePreviewResponse;

    if-nez v0, :cond_1

    .line 175
    iget-object v0, p0, Lcom/google/android/music/GPlusShareActivity$AsyncShareWorker$1;->this$1:Lcom/google/android/music/GPlusShareActivity$AsyncShareWorker;

    iget-object v0, v0, Lcom/google/android/music/GPlusShareActivity$AsyncShareWorker;->this$0:Lcom/google/android/music/GPlusShareActivity;

    # invokes: Lcom/google/android/music/GPlusShareActivity;->transitionToError()V
    invoke-static {v0}, Lcom/google/android/music/GPlusShareActivity;->access$100(Lcom/google/android/music/GPlusShareActivity;)V

    .line 182
    :cond_0
    :goto_0
    return-void

    .line 177
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/GPlusShareActivity$AsyncShareWorker$1;->this$1:Lcom/google/android/music/GPlusShareActivity$AsyncShareWorker;

    iget-object v0, v0, Lcom/google/android/music/GPlusShareActivity$AsyncShareWorker;->this$0:Lcom/google/android/music/GPlusShareActivity;

    iget-object v1, p0, Lcom/google/android/music/GPlusShareActivity$AsyncShareWorker$1;->val$shareUrl:Lcom/google/android/music/SharePreviewResponse;

    # invokes: Lcom/google/android/music/GPlusShareActivity;->launchShare(Lcom/google/android/music/SharePreviewResponse;)V
    invoke-static {v0, v1}, Lcom/google/android/music/GPlusShareActivity;->access$200(Lcom/google/android/music/GPlusShareActivity;Lcom/google/android/music/SharePreviewResponse;)V

    goto :goto_0

    .line 179
    :cond_2
    # getter for: Lcom/google/android/music/GPlusShareActivity;->LOGV:Z
    invoke-static {}, Lcom/google/android/music/GPlusShareActivity;->access$300()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 180
    const-string v0, "GPlusShareMusic"

    const-string v1, "Ignoring share url after cancel"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
