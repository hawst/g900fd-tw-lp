.class Lcom/google/android/music/playback/LocalDevicePlayback$24;
.super Ljava/lang/Object;
.source "LocalDevicePlayback.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/playback/LocalDevicePlayback;->clearQueue()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/playback/LocalDevicePlayback;


# direct methods
.method constructor <init>(Lcom/google/android/music/playback/LocalDevicePlayback;)V
    .locals 0

    .prologue
    .line 2626
    iput-object p1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$24;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 2636
    :try_start_0
    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$24;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->clearMediaList()V
    invoke-static {v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$6800(Lcom/google/android/music/playback/LocalDevicePlayback;)V

    .line 2637
    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$24;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    invoke-virtual {v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->stopSync()V

    .line 2638
    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$24;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    invoke-virtual {v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v1

    .line 2639
    .local v1, "store":Lcom/google/android/music/store/Store;
    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$24;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    iget-object v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback$24;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    invoke-virtual {v3}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v3

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->isInCloudQueueMode(Landroid/content/Context;)Z
    invoke-static {v2, v3}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$5100(Lcom/google/android/music/playback/LocalDevicePlayback;Landroid/content/Context;)Z

    move-result v0

    .line 2640
    .local v0, "isInCloudQueueMode":Z
    invoke-virtual {v1, v0}, Lcom/google/android/music/store/Store;->caqClearQueue(Z)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2641
    if-eqz v0, :cond_0

    .line 2642
    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$24;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    invoke-virtual {v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/music/cloudclient/CloudQueueManager;->clearCloudQueue(Landroid/content/Context;)Z

    .line 2645
    :cond_0
    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$24;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    const/4 v3, -0x1

    # setter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayPos:I
    invoke-static {v2, v3}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$2802(Lcom/google/android/music/playback/LocalDevicePlayback;I)I

    .line 2646
    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$24;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    invoke-virtual {v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/music/store/QueueUtils;->notifyChange(Landroid/content/Context;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2650
    :cond_1
    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$24;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mAsyncWakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$1400(Lcom/google/android/music/playback/LocalDevicePlayback;)Landroid/os/PowerManager$WakeLock;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 2652
    return-void

    .line 2650
    .end local v0    # "isInCloudQueueMode":Z
    .end local v1    # "store":Lcom/google/android/music/store/Store;
    :catchall_0
    move-exception v2

    iget-object v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback$24;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mAsyncWakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {v3}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$1400(Lcom/google/android/music/playback/LocalDevicePlayback;)Landroid/os/PowerManager$WakeLock;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/PowerManager$WakeLock;->release()V

    throw v2
.end method
