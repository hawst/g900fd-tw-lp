.class public final enum Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;
.super Ljava/lang/Enum;
.source "ExpandingScrollView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/widgets/ExpandingScrollView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ExpandingState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

.field public static final enum COLLAPSED:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

.field public static final enum FULLY_EXPANDED:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

.field public static final enum HIDDEN:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;


# instance fields
.field final defaultExposurePercentage:F

.field next:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

.field previous:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 196
    new-instance v0, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    const-string v1, "HIDDEN"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v3, v2}, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;-><init>(Ljava/lang/String;IF)V

    sput-object v0, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->HIDDEN:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    .line 197
    new-instance v0, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    const-string v1, "COLLAPSED"

    const/high16 v2, 0x41c80000    # 25.0f

    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;-><init>(Ljava/lang/String;IF)V

    sput-object v0, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->COLLAPSED:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    .line 198
    new-instance v0, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    const-string v1, "FULLY_EXPANDED"

    const/high16 v2, 0x42c80000    # 100.0f

    invoke-direct {v0, v1, v5, v2}, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;-><init>(Ljava/lang/String;IF)V

    sput-object v0, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->FULLY_EXPANDED:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    .line 195
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    sget-object v1, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->HIDDEN:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->COLLAPSED:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->FULLY_EXPANDED:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->$VALUES:[Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    .line 201
    sget-object v0, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->HIDDEN:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    sget-object v1, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->HIDDEN:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    iput-object v1, v0, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->previous:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    .line 202
    sget-object v0, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->HIDDEN:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    sget-object v1, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->HIDDEN:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    iput-object v1, v0, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->next:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    .line 203
    sget-object v0, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->COLLAPSED:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    sget-object v1, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->COLLAPSED:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    iput-object v1, v0, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->previous:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    .line 204
    sget-object v0, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->COLLAPSED:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    sget-object v1, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->FULLY_EXPANDED:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    iput-object v1, v0, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->next:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    .line 205
    sget-object v0, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->FULLY_EXPANDED:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    sget-object v1, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->COLLAPSED:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    iput-object v1, v0, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->previous:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    .line 206
    sget-object v0, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->FULLY_EXPANDED:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    sget-object v1, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->FULLY_EXPANDED:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    iput-object v1, v0, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->next:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    .line 207
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IF)V
    .locals 0
    .param p3, "exposurePercentage"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(F)V"
        }
    .end annotation

    .prologue
    .line 213
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 214
    iput p3, p0, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->defaultExposurePercentage:F

    .line 215
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 195
    const-class v0, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    return-object v0
.end method

.method public static values()[Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;
    .locals 1

    .prologue
    .line 195
    sget-object v0, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->$VALUES:[Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    invoke-virtual {v0}, [Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    return-object v0
.end method
