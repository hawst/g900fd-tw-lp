.class public interface abstract Lcom/google/android/music/wear/util/DataApiWrapper;
.super Ljava/lang/Object;
.source "DataApiWrapper.java"


# virtual methods
.method public abstract deleteDataItems(Landroid/net/Uri;)V
.end method

.method public abstract getAllDataItems(JLjava/util/concurrent/TimeUnit;)Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/concurrent/TimeUnit;",
            ")",
            "Ljava/util/Map",
            "<",
            "Landroid/net/Uri;",
            "Lcom/google/android/gms/wearable/DataMap;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/util/concurrent/TimeoutException;,
            Lcom/google/android/music/wear/util/GmsApiException;
        }
    .end annotation
.end method

.method public abstract putDataItem(Lcom/google/android/gms/wearable/PutDataRequest;)V
.end method
