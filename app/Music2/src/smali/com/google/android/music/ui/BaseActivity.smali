.class public Lcom/google/android/music/ui/BaseActivity;
.super Landroid/support/v7/app/ActionBarActivity;
.source "BaseActivity.java"

# interfaces
.implements Lcom/google/android/music/ui/ActionBarController;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/ui/BaseActivity$AccountSwitchWarningDialogFragment;,
        Lcom/google/android/music/ui/BaseActivity$OnDownloadOnlyClickListener;,
        Lcom/google/android/music/ui/BaseActivity$RouteSelectionCallback;
    }
.end annotation


# static fields
.field private static final LOGV:Z

.field private static sUpList:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mActionBarAlpha:I

.field private mActionBarBGDrawable:Landroid/graphics/drawable/Drawable;

.field private mActionBarCustomView:Landroid/view/View;

.field private mActionBarHeight:I

.field private mActionBarHiddenFromNowPlaying:Z

.field private mActionBarTitle:Ljava/lang/String;

.field private mActionBarTitleResId:I

.field private mArgbEvaluator:Landroid/animation/ArgbEvaluator;

.field private mContentFragment:Landroid/support/v4/app/Fragment;

.field private mContentViews:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/ViewGroup;",
            ">;"
        }
    .end annotation
.end field

.field private mCurrentFailureDialogsMusicId:Lcom/google/android/music/download/ContentIdentifier;

.field private mDownloadOnlyTextSwitcher:Landroid/widget/TextSwitcher;

.field private mDrawerAutoHide:Z

.field private final mDrawerListener:Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerContentClickListener;

.field private mDrawerRestoredStateOpen:Z

.field private mEmptyScreenShowing:Z

.field private mFloatingActionBarAnimator:Landroid/animation/ValueAnimator;

.field private mGreyStatusBarColor:I

.field private mIsDestroyed:Z

.field private final mMediaPlayerBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mMediaRouteManager:Lcom/google/android/music/ui/mrp/MediaRouteManager;

.field private mNowPlayingFragment:Lcom/google/android/music/ui/NowPlayingScreenFragment;

.field private mOrangeStatusBarColor:I

.field private mPlayDrawerLayout:Lcom/google/android/play/drawer/PlayDrawerLayout;

.field protected mPrefChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

.field private mRequestSetAlphaRunnable:Ljava/lang/Runnable;

.field mRouteSelectionCallback:Lcom/google/android/music/ui/BaseActivity$RouteSelectionCallback;

.field private mShowHome:Z

.field private mShowMrpVolumeDialog:Z

.field private mShowingFailureAlert:Z

.field private mSideDrawer:Landroid/view/View;

.field private mSideDrawerContainer:Landroid/support/v4/widget/DrawerLayout;

.field private mSideDrawerEnabled:Z

.field private mSimpleDrawerListener:Landroid/support/v4/widget/DrawerLayout$SimpleDrawerListener;

.field private mTVMenuView:Landroid/widget/ListView;

.field private mTVSideDrawerContainer:Landroid/view/ViewGroup;

.field private final mUIStateChangeListener:Lcom/google/android/music/ui/UIStateManager$UIStateChangeListener;

.field private mUpIntent:Landroid/content/Intent;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 85
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->UI:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/ui/BaseActivity;->LOGV:Z

    .line 106
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    sput-object v0, Lcom/google/android/music/ui/BaseActivity;->sUpList:Ljava/util/LinkedList;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 82
    invoke-direct {p0}, Landroid/support/v7/app/ActionBarActivity;-><init>()V

    .line 108
    iput-object v1, p0, Lcom/google/android/music/ui/BaseActivity;->mContentFragment:Landroid/support/v4/app/Fragment;

    .line 109
    iput-object v1, p0, Lcom/google/android/music/ui/BaseActivity;->mNowPlayingFragment:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    .line 121
    iput-boolean v2, p0, Lcom/google/android/music/ui/BaseActivity;->mSideDrawerEnabled:Z

    .line 122
    iput-boolean v0, p0, Lcom/google/android/music/ui/BaseActivity;->mEmptyScreenShowing:Z

    .line 123
    iput-boolean v0, p0, Lcom/google/android/music/ui/BaseActivity;->mIsDestroyed:Z

    .line 133
    iput-boolean v2, p0, Lcom/google/android/music/ui/BaseActivity;->mDrawerAutoHide:Z

    .line 138
    iput-boolean v0, p0, Lcom/google/android/music/ui/BaseActivity;->mShowingFailureAlert:Z

    .line 139
    iput-object v1, p0, Lcom/google/android/music/ui/BaseActivity;->mCurrentFailureDialogsMusicId:Lcom/google/android/music/download/ContentIdentifier;

    .line 142
    iput-object v1, p0, Lcom/google/android/music/ui/BaseActivity;->mActionBarTitle:Ljava/lang/String;

    .line 144
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/music/ui/BaseActivity;->mActionBarTitleResId:I

    .line 150
    const/16 v0, 0xff

    iput v0, p0, Lcom/google/android/music/ui/BaseActivity;->mActionBarAlpha:I

    .line 158
    new-instance v0, Lcom/google/android/music/ui/BaseActivity$1;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/BaseActivity$1;-><init>(Lcom/google/android/music/ui/BaseActivity;)V

    iput-object v0, p0, Lcom/google/android/music/ui/BaseActivity;->mRequestSetAlphaRunnable:Ljava/lang/Runnable;

    .line 185
    new-instance v0, Lcom/google/android/music/ui/BaseActivity$2;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/BaseActivity$2;-><init>(Lcom/google/android/music/ui/BaseActivity;)V

    iput-object v0, p0, Lcom/google/android/music/ui/BaseActivity;->mDrawerListener:Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerContentClickListener;

    .line 236
    new-instance v0, Lcom/google/android/music/ui/BaseActivity$3;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/BaseActivity$3;-><init>(Lcom/google/android/music/ui/BaseActivity;)V

    iput-object v0, p0, Lcom/google/android/music/ui/BaseActivity;->mSimpleDrawerListener:Landroid/support/v4/widget/DrawerLayout$SimpleDrawerListener;

    .line 270
    new-instance v0, Lcom/google/android/music/ui/BaseActivity$4;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/BaseActivity$4;-><init>(Lcom/google/android/music/ui/BaseActivity;)V

    iput-object v0, p0, Lcom/google/android/music/ui/BaseActivity;->mUIStateChangeListener:Lcom/google/android/music/ui/UIStateManager$UIStateChangeListener;

    .line 289
    new-instance v0, Lcom/google/android/music/ui/BaseActivity$RouteSelectionCallback;

    invoke-direct {v0, p0, v1}, Lcom/google/android/music/ui/BaseActivity$RouteSelectionCallback;-><init>(Lcom/google/android/music/ui/BaseActivity;Lcom/google/android/music/ui/BaseActivity$1;)V

    iput-object v0, p0, Lcom/google/android/music/ui/BaseActivity;->mRouteSelectionCallback:Lcom/google/android/music/ui/BaseActivity$RouteSelectionCallback;

    .line 1411
    new-instance v0, Lcom/google/android/music/ui/BaseActivity$13;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/BaseActivity$13;-><init>(Lcom/google/android/music/ui/BaseActivity;)V

    iput-object v0, p0, Lcom/google/android/music/ui/BaseActivity;->mMediaPlayerBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 1537
    new-instance v0, Lcom/google/android/music/ui/BaseActivity$14;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/BaseActivity$14;-><init>(Lcom/google/android/music/ui/BaseActivity;)V

    iput-object v0, p0, Lcom/google/android/music/ui/BaseActivity;->mPrefChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    .line 1554
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/ui/BaseActivity;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/BaseActivity;

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/music/ui/BaseActivity;->mActionBarCustomView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/music/ui/BaseActivity;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/BaseActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 82
    invoke-direct {p0, p1}, Lcom/google/android/music/ui/BaseActivity;->showAccountSwitchWarningDialog(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/music/ui/BaseActivity;)Landroid/animation/ValueAnimator;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/BaseActivity;

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/music/ui/BaseActivity;->mFloatingActionBarAnimator:Landroid/animation/ValueAnimator;

    return-object v0
.end method

.method static synthetic access$1102(Lcom/google/android/music/ui/BaseActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/BaseActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 82
    iput-boolean p1, p0, Lcom/google/android/music/ui/BaseActivity;->mShowingFailureAlert:Z

    return p1
.end method

.method static synthetic access$1202(Lcom/google/android/music/ui/BaseActivity;Lcom/google/android/music/download/ContentIdentifier;)Lcom/google/android/music/download/ContentIdentifier;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/BaseActivity;
    .param p1, "x1"    # Lcom/google/android/music/download/ContentIdentifier;

    .prologue
    .line 82
    iput-object p1, p0, Lcom/google/android/music/ui/BaseActivity;->mCurrentFailureDialogsMusicId:Lcom/google/android/music/download/ContentIdentifier;

    return-object p1
.end method

.method static synthetic access$1300(Lcom/google/android/music/ui/BaseActivity;)Landroid/widget/TextSwitcher;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/BaseActivity;

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/music/ui/BaseActivity;->mDownloadOnlyTextSwitcher:Landroid/widget/TextSwitcher;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/google/android/music/ui/BaseActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/BaseActivity;

    .prologue
    .line 82
    invoke-direct {p0}, Lcom/google/android/music/ui/BaseActivity;->updateDownloadStripVisibility()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/music/ui/BaseActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/BaseActivity;

    .prologue
    .line 82
    invoke-direct {p0}, Lcom/google/android/music/ui/BaseActivity;->restoreActionBarTitle()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/music/ui/BaseActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/BaseActivity;

    .prologue
    .line 82
    iget v0, p0, Lcom/google/android/music/ui/BaseActivity;->mActionBarAlpha:I

    return v0
.end method

.method static synthetic access$400(Lcom/google/android/music/ui/BaseActivity;I)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/BaseActivity;
    .param p1, "x1"    # I

    .prologue
    .line 82
    invoke-direct {p0, p1}, Lcom/google/android/music/ui/BaseActivity;->setActionBarAlphaInternal(I)V

    return-void
.end method

.method static synthetic access$502(Lcom/google/android/music/ui/BaseActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/BaseActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 82
    iput-boolean p1, p0, Lcom/google/android/music/ui/BaseActivity;->mDrawerAutoHide:Z

    return p1
.end method

.method static synthetic access$600(Lcom/google/android/music/ui/BaseActivity;IZ)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/BaseActivity;
    .param p1, "x1"    # I
    .param p2, "x2"    # Z

    .prologue
    .line 82
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/ui/BaseActivity;->setActionBarTitleInternal(IZ)V

    return-void
.end method

.method static synthetic access$800()Z
    .locals 1

    .prologue
    .line 82
    sget-boolean v0, Lcom/google/android/music/ui/BaseActivity;->LOGV:Z

    return v0
.end method

.method static synthetic access$900(Lcom/google/android/music/ui/BaseActivity;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/BaseActivity;

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/music/ui/BaseActivity;->mContentViews:Ljava/util/ArrayList;

    return-object v0
.end method

.method private cancelFloatingActionBarAnimation(I)V
    .locals 1
    .param p1, "alpha"    # I

    .prologue
    .line 1299
    iget-object v0, p0, Lcom/google/android/music/ui/BaseActivity;->mFloatingActionBarAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1300
    new-instance v0, Lcom/google/android/music/ui/BaseActivity$10;

    invoke-direct {v0, p0, p1}, Lcom/google/android/music/ui/BaseActivity$10;-><init>(Lcom/google/android/music/ui/BaseActivity;I)V

    invoke-static {v0, p0}, Lcom/google/android/music/utils/MusicUtils;->runOnUiThread(Ljava/lang/Runnable;Landroid/content/Context;)V

    .line 1310
    :goto_0
    return-void

    .line 1308
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/music/ui/BaseActivity;->setActionBarAlpha(I)V

    goto :goto_0
.end method

.method private initDownloadOnlyTextSwitcher()V
    .locals 4

    .prologue
    .line 867
    const v2, 0x7f0e00e4

    invoke-virtual {p0, v2}, Lcom/google/android/music/ui/BaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextSwitcher;

    iput-object v2, p0, Lcom/google/android/music/ui/BaseActivity;->mDownloadOnlyTextSwitcher:Landroid/widget/TextSwitcher;

    .line 868
    iget-object v2, p0, Lcom/google/android/music/ui/BaseActivity;->mDownloadOnlyTextSwitcher:Landroid/widget/TextSwitcher;

    if-eqz v2, :cond_0

    .line 869
    iget-object v2, p0, Lcom/google/android/music/ui/BaseActivity;->mDownloadOnlyTextSwitcher:Landroid/widget/TextSwitcher;

    new-instance v3, Lcom/google/android/music/ui/BaseActivity$8;

    invoke-direct {v3, p0}, Lcom/google/android/music/ui/BaseActivity$8;-><init>(Lcom/google/android/music/ui/BaseActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/TextSwitcher;->setFactory(Landroid/widget/ViewSwitcher$ViewFactory;)V

    .line 888
    const v2, 0x7f050020

    invoke-static {p0, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 889
    .local v0, "in":Landroid/view/animation/Animation;
    const v2, 0x7f050021

    invoke-static {p0, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    .line 892
    .local v1, "out":Landroid/view/animation/Animation;
    iget-object v2, p0, Lcom/google/android/music/ui/BaseActivity;->mDownloadOnlyTextSwitcher:Landroid/widget/TextSwitcher;

    invoke-virtual {v2, v0}, Landroid/widget/TextSwitcher;->setInAnimation(Landroid/view/animation/Animation;)V

    .line 893
    iget-object v2, p0, Lcom/google/android/music/ui/BaseActivity;->mDownloadOnlyTextSwitcher:Landroid/widget/TextSwitcher;

    invoke-virtual {v2, v1}, Landroid/widget/TextSwitcher;->setOutAnimation(Landroid/view/animation/Animation;)V

    .line 894
    iget-object v2, p0, Lcom/google/android/music/ui/BaseActivity;->mDownloadOnlyTextSwitcher:Landroid/widget/TextSwitcher;

    new-instance v3, Lcom/google/android/music/ui/BaseActivity$OnDownloadOnlyClickListener;

    invoke-direct {v3, p0}, Lcom/google/android/music/ui/BaseActivity$OnDownloadOnlyClickListener;-><init>(Lcom/google/android/music/ui/BaseActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/TextSwitcher;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 895
    invoke-direct {p0}, Lcom/google/android/music/ui/BaseActivity;->updateDownloadStripVisibility()V

    .line 896
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/music/ui/BaseActivity;->mPrefChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-virtual {v2, v3}, Lcom/google/android/music/preferences/MusicPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 898
    .end local v0    # "in":Landroid/view/animation/Animation;
    .end local v1    # "out":Landroid/view/animation/Animation;
    :cond_0
    return-void
.end method

.method private initializeDrawerState(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 646
    if-eqz p1, :cond_0

    .line 647
    const-string v0, "music:base:drawer_state_open"

    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->getDrawerStateOpenDefault()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/music/ui/BaseActivity;->mDrawerRestoredStateOpen:Z

    .line 649
    const-string v0, "music:base:drawer_auto_hide"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/music/ui/BaseActivity;->mDrawerAutoHide:Z

    .line 653
    :cond_0
    const-string v0, "MusicBaseActivity"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 654
    const-string v0, "MusicBaseActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "initializeDrawerState, open: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/music/ui/BaseActivity;->mDrawerRestoredStateOpen:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 656
    :cond_1
    return-void
.end method

.method private loadTVSideDrawer(I)V
    .locals 4
    .param p1, "layoutId"    # I

    .prologue
    .line 556
    const v1, 0x7f0e00e2

    invoke-virtual {p0, v1}, Lcom/google/android/music/ui/BaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/google/android/music/ui/BaseActivity;->mTVSideDrawerContainer:Landroid/view/ViewGroup;

    .line 561
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/BaseActivity;->mTVSideDrawerContainer:Landroid/view/ViewGroup;

    const/4 v3, 0x0

    invoke-virtual {v1, p1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/ui/BaseActivity;->mSideDrawer:Landroid/view/View;

    .line 562
    iget-object v1, p0, Lcom/google/android/music/ui/BaseActivity;->mTVSideDrawerContainer:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/google/android/music/ui/BaseActivity;->mSideDrawer:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 564
    iget-object v1, p0, Lcom/google/android/music/ui/BaseActivity;->mSideDrawer:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 565
    .local v0, "lp":Landroid/view/ViewGroup$LayoutParams;
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->calculateSideDrawerWidth()I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 566
    return-void
.end method

.method public static putExtraDrawerState(Landroid/content/Intent;ZZ)V
    .locals 1
    .param p0, "intent"    # Landroid/content/Intent;
    .param p1, "startOpen"    # Z
    .param p2, "autoHide"    # Z

    .prologue
    .line 498
    const-string v0, "music:base:drawer_state_open"

    invoke-virtual {p0, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 499
    const-string v0, "music:base:drawer_auto_hide"

    invoke-virtual {p0, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 500
    return-void
.end method

.method private restoreActionBarTitle()V
    .locals 2

    .prologue
    .line 1192
    iget v0, p0, Lcom/google/android/music/ui/BaseActivity;->mActionBarTitleResId:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 1193
    iget v0, p0, Lcom/google/android/music/ui/BaseActivity;->mActionBarTitleResId:I

    iget-boolean v1, p0, Lcom/google/android/music/ui/BaseActivity;->mShowHome:Z

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/ui/BaseActivity;->setActionBarTitleInternal(IZ)V

    .line 1201
    :goto_0
    return-void

    .line 1194
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/ui/BaseActivity;->mActionBarTitle:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1195
    iget-object v0, p0, Lcom/google/android/music/ui/BaseActivity;->mActionBarTitle:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/music/ui/BaseActivity;->setActionBarTitleInternal(Ljava/lang/String;)V

    .line 1196
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    goto :goto_0

    .line 1199
    :cond_1
    const-string v0, ""

    invoke-direct {p0, v0}, Lcom/google/android/music/ui/BaseActivity;->setActionBarTitleInternal(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private setActionBarAlphaInternal(I)V
    .locals 5
    .param p1, "alpha"    # I

    .prologue
    const/4 v4, 0x0

    .line 1219
    iget-object v2, p0, Lcom/google/android/music/ui/BaseActivity;->mActionBarBGDrawable:Landroid/graphics/drawable/Drawable;

    if-nez v2, :cond_0

    .line 1221
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    .line 1222
    .local v0, "bar":Landroid/support/v7/app/ActionBar;
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c007d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 1223
    .local v1, "color":I
    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v2, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    iput-object v2, p0, Lcom/google/android/music/ui/BaseActivity;->mActionBarBGDrawable:Landroid/graphics/drawable/Drawable;

    .line 1225
    new-instance v2, Landroid/view/View;

    invoke-direct {v2, p0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/google/android/music/ui/BaseActivity;->mActionBarCustomView:Landroid/view/View;

    .line 1226
    iget-object v2, p0, Lcom/google/android/music/ui/BaseActivity;->mActionBarCustomView:Landroid/view/View;

    new-instance v3, Landroid/support/v7/app/ActionBar$LayoutParams;

    invoke-direct {v3, v4, v4}, Landroid/support/v7/app/ActionBar$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v2, v3}, Landroid/support/v7/app/ActionBar;->setCustomView(Landroid/view/View;Landroid/support/v7/app/ActionBar$LayoutParams;)V

    .line 1227
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/support/v7/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    .line 1228
    iget-object v2, p0, Lcom/google/android/music/ui/BaseActivity;->mActionBarBGDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v2}, Landroid/support/v7/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1229
    iget-object v2, p0, Lcom/google/android/music/ui/BaseActivity;->mActionBarCustomView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->requestLayout()V

    .line 1231
    .end local v0    # "bar":Landroid/support/v7/app/ActionBar;
    .end local v1    # "color":I
    :cond_0
    iget-object v2, p0, Lcom/google/android/music/ui/BaseActivity;->mActionBarBGDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, p1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 1232
    invoke-direct {p0}, Lcom/google/android/music/ui/BaseActivity;->setStatusBarColor()V

    .line 1237
    iget-object v2, p0, Lcom/google/android/music/ui/BaseActivity;->mRequestSetAlphaRunnable:Ljava/lang/Runnable;

    invoke-static {v2, p0}, Lcom/google/android/music/utils/MusicUtils;->runOnUiThread(Ljava/lang/Runnable;Landroid/content/Context;)V

    .line 1238
    return-void
.end method

.method private setActionBarTitleInternal(IZ)V
    .locals 5
    .param p1, "resId"    # I
    .param p2, "showHome"    # Z

    .prologue
    const/4 v3, 0x6

    .line 1179
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    .line 1180
    .local v0, "actionBar":Landroid/support/v7/app/ActionBar;
    const/4 v2, 0x6

    .line 1182
    .local v2, "optionsMask":I
    if-eqz p2, :cond_1

    move v1, v3

    .line 1183
    .local v1, "options":I
    :goto_0
    iget-object v4, p0, Lcom/google/android/music/ui/BaseActivity;->mPlayDrawerLayout:Lcom/google/android/play/drawer/PlayDrawerLayout;

    if-eqz v4, :cond_0

    .line 1184
    iget-object v4, p0, Lcom/google/android/music/ui/BaseActivity;->mPlayDrawerLayout:Lcom/google/android/play/drawer/PlayDrawerLayout;

    invoke-virtual {v4, p2}, Lcom/google/android/play/drawer/PlayDrawerLayout;->setDrawerIndicatorEnabled(Z)V

    .line 1186
    :cond_0
    invoke-virtual {v0, v1, v3}, Landroid/support/v7/app/ActionBar;->setDisplayOptions(II)V

    .line 1187
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/support/v7/app/ActionBar;->setNavigationMode(I)V

    .line 1188
    invoke-virtual {v0, p1}, Landroid/support/v7/app/ActionBar;->setTitle(I)V

    .line 1189
    return-void

    .line 1182
    .end local v1    # "options":I
    :cond_1
    const/4 v1, 0x4

    goto :goto_0
.end method

.method private setActionBarTitleInternal(Ljava/lang/String;)V
    .locals 2
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 1160
    iget-object v0, p0, Lcom/google/android/music/ui/BaseActivity;->mPlayDrawerLayout:Lcom/google/android/play/drawer/PlayDrawerLayout;

    if-eqz v0, :cond_0

    .line 1161
    iget-object v0, p0, Lcom/google/android/music/ui/BaseActivity;->mPlayDrawerLayout:Lcom/google/android/play/drawer/PlayDrawerLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/play/drawer/PlayDrawerLayout;->setDrawerIndicatorEnabled(Z)V

    .line 1163
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v7/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 1164
    return-void
.end method

.method private setActionbarHeight()V
    .locals 4

    .prologue
    .line 916
    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    .line 917
    .local v1, "tv":Landroid/util/TypedValue;
    const v0, 0x7f010125

    .line 918
    .local v0, "actionBarResId":I
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v0, v1, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 919
    iget v2, v1, Landroid/util/TypedValue;->data:I

    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/TypedValue;->complexToDimensionPixelSize(ILandroid/util/DisplayMetrics;)I

    move-result v2

    iput v2, p0, Lcom/google/android/music/ui/BaseActivity;->mActionBarHeight:I

    .line 922
    :cond_0
    return-void
.end method

.method private setStatusBarColor()V
    .locals 7

    .prologue
    .line 1313
    invoke-static {}, Lcom/google/android/music/preferences/MusicPreferences;->isLmpOrGreater()Z

    move-result v4

    if-nez v4, :cond_0

    .line 1325
    :goto_0
    return-void

    .line 1314
    :cond_0
    iget v4, p0, Lcom/google/android/music/ui/BaseActivity;->mActionBarAlpha:I

    int-to-float v4, v4

    const/high16 v5, 0x437f0000    # 255.0f

    div-float v1, v4, v5

    .line 1315
    .local v1, "alphaAB":F
    iget-object v4, p0, Lcom/google/android/music/ui/BaseActivity;->mArgbEvaluator:Landroid/animation/ArgbEvaluator;

    iget v5, p0, Lcom/google/android/music/ui/BaseActivity;->mGreyStatusBarColor:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    iget v6, p0, Lcom/google/android/music/ui/BaseActivity;->mOrangeStatusBarColor:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v1, v5, v6}, Landroid/animation/ArgbEvaluator;->evaluate(FLjava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1317
    .local v0, "actionBarColor":I
    iget-boolean v4, p0, Lcom/google/android/music/ui/BaseActivity;->mActionBarHiddenFromNowPlaying:Z

    if-eqz v4, :cond_1

    iget v3, p0, Lcom/google/android/music/ui/BaseActivity;->mGreyStatusBarColor:I

    .line 1323
    .local v3, "nowPlayingColor":I
    :goto_1
    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 1324
    .local v2, "newColor":I
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->getWindow()Landroid/view/Window;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/view/Window;->setStatusBarColor(I)V

    goto :goto_0

    .line 1317
    .end local v2    # "newColor":I
    .end local v3    # "nowPlayingColor":I
    :cond_1
    iget v3, p0, Lcom/google/android/music/ui/BaseActivity;->mOrangeStatusBarColor:I

    goto :goto_1
.end method

.method private showAccountSwitchWarningDialog(Ljava/lang/String;)V
    .locals 4
    .param p1, "selectedAccountName"    # Ljava/lang/String;

    .prologue
    .line 1503
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1504
    .local v0, "dialogArgs":Landroid/os/Bundle;
    const-string v2, "account"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1505
    new-instance v1, Lcom/google/android/music/ui/BaseActivity$AccountSwitchWarningDialogFragment;

    invoke-direct {v1}, Lcom/google/android/music/ui/BaseActivity$AccountSwitchWarningDialogFragment;-><init>()V

    .line 1506
    .local v1, "fragment":Landroid/support/v4/app/DialogFragment;
    invoke-virtual {v1, v0}, Landroid/support/v4/app/DialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 1507
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "AccountSwitchDialog"

    invoke-virtual {v1, v2, v3}, Landroid/support/v4/app/DialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 1508
    return-void
.end method

.method private syncActionBarIconState()V
    .locals 1

    .prologue
    .line 550
    iget-object v0, p0, Lcom/google/android/music/ui/BaseActivity;->mPlayDrawerLayout:Lcom/google/android/play/drawer/PlayDrawerLayout;

    if-eqz v0, :cond_0

    .line 551
    iget-object v0, p0, Lcom/google/android/music/ui/BaseActivity;->mPlayDrawerLayout:Lcom/google/android/play/drawer/PlayDrawerLayout;

    invoke-virtual {v0}, Lcom/google/android/play/drawer/PlayDrawerLayout;->syncDrawerIndicator()V

    .line 553
    :cond_0
    return-void
.end method

.method private updateDownloadStripVisibility()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1479
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/preferences/MusicPreferences;->isDownloadedOnlyMode()Z

    move-result v0

    .line 1480
    .local v0, "isDownloadedOnlyMode":Z
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->doesSupportDownloadOnlyBanner()Z

    move-result v1

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    .line 1483
    iget-object v1, p0, Lcom/google/android/music/ui/BaseActivity;->mDownloadOnlyTextSwitcher:Landroid/widget/TextSwitcher;

    invoke-virtual {v1}, Landroid/widget/TextSwitcher;->getCurrentView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c00b6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    .line 1485
    iget-object v1, p0, Lcom/google/android/music/ui/BaseActivity;->mDownloadOnlyTextSwitcher:Landroid/widget/TextSwitcher;

    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0090

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextSwitcher;->setCurrentText(Ljava/lang/CharSequence;)V

    .line 1487
    iget-object v1, p0, Lcom/google/android/music/ui/BaseActivity;->mDownloadOnlyTextSwitcher:Landroid/widget/TextSwitcher;

    invoke-virtual {v1, v4}, Landroid/widget/TextSwitcher;->setVisibility(I)V

    .line 1492
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->usesActionBarOverlay()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1493
    if-eqz v0, :cond_2

    .line 1494
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/google/android/music/ui/BaseActivity;->updateMainContainerPadding(Z)V

    .line 1495
    const/16 v1, 0xff

    invoke-virtual {p0, v1}, Lcom/google/android/music/ui/BaseActivity;->setActionBarAlpha(I)V

    .line 1500
    :cond_0
    :goto_1
    return-void

    .line 1489
    :cond_1
    iget-object v1, p0, Lcom/google/android/music/ui/BaseActivity;->mDownloadOnlyTextSwitcher:Landroid/widget/TextSwitcher;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextSwitcher;->setVisibility(I)V

    goto :goto_0

    .line 1497
    :cond_2
    invoke-direct {p0, v4}, Lcom/google/android/music/ui/BaseActivity;->updateMainContainerPadding(Z)V

    goto :goto_1
.end method

.method private updateDrawerLockMode()V
    .locals 3

    .prologue
    .line 569
    iget-boolean v1, p0, Lcom/google/android/music/ui/BaseActivity;->mSideDrawerEnabled:Z

    if-eqz v1, :cond_1

    const/4 v0, 0x0

    .line 571
    .local v0, "lockMode":I
    :goto_0
    iget-object v1, p0, Lcom/google/android/music/ui/BaseActivity;->mSideDrawerContainer:Landroid/support/v4/widget/DrawerLayout;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/music/ui/BaseActivity;->mSideDrawer:Landroid/view/View;

    if-eqz v1, :cond_2

    .line 572
    iget-object v1, p0, Lcom/google/android/music/ui/BaseActivity;->mSideDrawerContainer:Landroid/support/v4/widget/DrawerLayout;

    iget-object v2, p0, Lcom/google/android/music/ui/BaseActivity;->mSideDrawer:Landroid/view/View;

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/widget/DrawerLayout;->setDrawerLockMode(ILandroid/view/View;)V

    .line 576
    :cond_0
    :goto_1
    return-void

    .line 569
    .end local v0    # "lockMode":I
    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    .line 573
    .restart local v0    # "lockMode":I
    :cond_2
    iget-object v1, p0, Lcom/google/android/music/ui/BaseActivity;->mPlayDrawerLayout:Lcom/google/android/play/drawer/PlayDrawerLayout;

    if-eqz v1, :cond_0

    .line 574
    iget-object v1, p0, Lcom/google/android/music/ui/BaseActivity;->mPlayDrawerLayout:Lcom/google/android/play/drawer/PlayDrawerLayout;

    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, Lcom/google/android/play/drawer/PlayDrawerLayout;->setDrawerLockMode(II)V

    goto :goto_1
.end method

.method private updateMainContainerPadding(Z)V
    .locals 5
    .param p1, "addTopPadding"    # Z

    .prologue
    .line 909
    const v1, 0x7f0e00e3

    invoke-virtual {p0, v1}, Lcom/google/android/music/ui/BaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 910
    .local v0, "mainContainer":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getPaddingLeft()I

    move-result v2

    if-eqz p1, :cond_0

    iget v1, p0, Lcom/google/android/music/ui/BaseActivity;->mActionBarHeight:I

    :goto_0
    invoke-virtual {v0}, Landroid/view/View;->getPaddingRight()I

    move-result v3

    invoke-virtual {v0}, Landroid/view/View;->getPaddingBottom()I

    move-result v4

    invoke-virtual {v0, v2, v1, v3, v4}, Landroid/view/View;->setPadding(IIII)V

    .line 913
    return-void

    .line 910
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public alertFailureIfNecessary(Lcom/google/android/music/download/ContentIdentifier;I)V
    .locals 2
    .param p1, "musicId"    # Lcom/google/android/music/download/ContentIdentifier;
    .param p2, "errorType"    # I

    .prologue
    .line 1395
    iget-boolean v1, p0, Lcom/google/android/music/ui/BaseActivity;->mShowingFailureAlert:Z

    if-eqz v1, :cond_1

    .line 1409
    :cond_0
    :goto_0
    return-void

    .line 1398
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1401
    if-eqz p1, :cond_2

    iget-object v1, p0, Lcom/google/android/music/ui/BaseActivity;->mCurrentFailureDialogsMusicId:Lcom/google/android/music/download/ContentIdentifier;

    invoke-virtual {p1, v1}, Lcom/google/android/music/download/ContentIdentifier;->equals(Lcom/google/android/music/download/ContentIdentifier;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1405
    :cond_2
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1406
    .local v0, "args":Landroid/os/Bundle;
    const-string v1, "musicId"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1407
    const-string v1, "errorType"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1408
    const/16 v1, 0x6c

    invoke-virtual {p0, v1, v0}, Lcom/google/android/music/ui/BaseActivity;->showDialog(ILandroid/os/Bundle;)Z

    goto :goto_0
.end method

.method protected autoHideDrawerIfRequested()V
    .locals 4

    .prologue
    .line 671
    iget-boolean v0, p0, Lcom/google/android/music/ui/BaseActivity;->mDrawerRestoredStateOpen:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/music/ui/BaseActivity;->mDrawerAutoHide:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/music/ui/BaseActivity;->mSideDrawerContainer:Landroid/support/v4/widget/DrawerLayout;

    if-eqz v0, :cond_1

    .line 672
    const-string v0, "MusicBaseActivity"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 673
    const-string v0, "MusicBaseActivity"

    const-string v1, "Scheduling side drawer to be closed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 675
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/ui/BaseActivity;->mDrawerAutoHide:Z

    .line 676
    iget-object v0, p0, Lcom/google/android/music/ui/BaseActivity;->mSideDrawerContainer:Landroid/support/v4/widget/DrawerLayout;

    new-instance v1, Lcom/google/android/music/ui/BaseActivity$6;

    invoke-direct {v1, p0}, Lcom/google/android/music/ui/BaseActivity$6;-><init>(Lcom/google/android/music/ui/BaseActivity;)V

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/widget/DrawerLayout;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 684
    :cond_1
    return-void
.end method

.method protected calculateSideDrawerWidth()I
    .locals 6

    .prologue
    .line 506
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 507
    .local v2, "res":Landroid/content/res/Resources;
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/music/ui/UIStateManager;->getScreenWidth()I

    move-result v3

    .line 508
    .local v3, "screenWidth":I
    const v5, 0x7f0f00e9

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 509
    .local v0, "maxSize":I
    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v5

    iget v1, v5, Landroid/content/res/Configuration;->orientation:I

    .line 511
    .local v1, "orientation":I
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/music/preferences/MusicPreferences;->isTvMusic()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 512
    div-int/lit8 v5, v3, 0x5

    invoke-static {v5, v0}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 518
    .local v4, "width":I
    :goto_0
    return v4

    .line 513
    .end local v4    # "width":I
    :cond_0
    const/4 v5, 0x1

    if-ne v1, v5, :cond_1

    .line 514
    mul-int/lit8 v5, v3, 0x4

    div-int/lit8 v5, v5, 0x5

    invoke-static {v5, v0}, Ljava/lang/Math;->min(II)I

    move-result v4

    .restart local v4    # "width":I
    goto :goto_0

    .line 516
    .end local v4    # "width":I
    :cond_1
    div-int/lit8 v5, v3, 0x2

    invoke-static {v5, v0}, Ljava/lang/Math;->min(II)I

    move-result v4

    .restart local v4    # "width":I
    goto :goto_0
.end method

.method protected closeSideDrawer()V
    .locals 2

    .prologue
    .line 628
    iget-object v0, p0, Lcom/google/android/music/ui/BaseActivity;->mPlayDrawerLayout:Lcom/google/android/play/drawer/PlayDrawerLayout;

    if-eqz v0, :cond_1

    .line 629
    iget-object v0, p0, Lcom/google/android/music/ui/BaseActivity;->mPlayDrawerLayout:Lcom/google/android/play/drawer/PlayDrawerLayout;

    invoke-virtual {v0}, Lcom/google/android/play/drawer/PlayDrawerLayout;->closeDrawer()V

    .line 635
    :cond_0
    :goto_0
    return-void

    .line 631
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/ui/BaseActivity;->mSideDrawerContainer:Landroid/support/v4/widget/DrawerLayout;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/music/ui/BaseActivity;->mSideDrawerEnabled:Z

    if-eqz v0, :cond_0

    .line 632
    iget-object v0, p0, Lcom/google/android/music/ui/BaseActivity;->mSideDrawerContainer:Landroid/support/v4/widget/DrawerLayout;

    iget-object v1, p0, Lcom/google/android/music/ui/BaseActivity;->mSideDrawer:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->closeDrawer(Landroid/view/View;)V

    goto :goto_0
.end method

.method protected doesSupportDownloadOnlyBanner()Z
    .locals 1

    .prologue
    .line 316
    const/4 v0, 0x1

    return v0
.end method

.method public enableSideDrawer(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 579
    iget-boolean v0, p0, Lcom/google/android/music/ui/BaseActivity;->mSideDrawerEnabled:Z

    if-eq v0, p1, :cond_0

    .line 580
    iput-boolean p1, p0, Lcom/google/android/music/ui/BaseActivity;->mSideDrawerEnabled:Z

    .line 581
    invoke-direct {p0}, Lcom/google/android/music/ui/BaseActivity;->updateDrawerLockMode()V

    .line 583
    :cond_0
    return-void
.end method

.method public getBottomDrawer()Lcom/google/android/music/widgets/ExpandingScrollView;
    .locals 1

    .prologue
    .line 1145
    const v0, 0x7f0e00e5

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/BaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/widgets/ExpandingScrollView;

    return-object v0
.end method

.method protected getContent()Landroid/support/v4/app/Fragment;
    .locals 2

    .prologue
    .line 337
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const v1, 0x7f0e00d0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    return-object v0
.end method

.method protected getContentContainer()Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 748
    const v0, 0x7f0e00d0

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/BaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    return-object v0
.end method

.method protected getDrawerAutoHideDefault()Z
    .locals 1

    .prologue
    .line 295
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->wasSidePannelClosedOnce()Z

    move-result v0

    return v0
.end method

.method protected getDrawerStateOpenDefault()Z
    .locals 1

    .prologue
    .line 291
    const/4 v0, 0x0

    return v0
.end method

.method protected getPreferences()Lcom/google/android/music/preferences/MusicPreferences;
    .locals 1

    .prologue
    .line 755
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->getUIStateManager()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    return-object v0
.end method

.method protected final getTVMenuView()Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 299
    iget-object v0, p0, Lcom/google/android/music/ui/BaseActivity;->mTVMenuView:Landroid/widget/ListView;

    return-object v0
.end method

.method protected getUIStateManager()Lcom/google/android/music/ui/UIStateManager;
    .locals 1

    .prologue
    .line 762
    invoke-static {p0}, Lcom/google/android/music/ui/UIStateManager;->getInstance(Landroid/content/Context;)Lcom/google/android/music/ui/UIStateManager;

    move-result-object v0

    return-object v0
.end method

.method public hideActionBar()V
    .locals 1

    .prologue
    .line 1251
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/ActionBar;->hide()V

    .line 1252
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/ui/BaseActivity;->mActionBarHiddenFromNowPlaying:Z

    .line 1253
    invoke-static {}, Lcom/google/android/music/preferences/MusicPreferences;->isLmpOrGreater()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1254
    invoke-direct {p0}, Lcom/google/android/music/ui/BaseActivity;->setStatusBarColor()V

    .line 1256
    :cond_0
    return-void
.end method

.method public isActivityDestroyed()Z
    .locals 1

    .prologue
    .line 1141
    iget-boolean v0, p0, Lcom/google/android/music/ui/BaseActivity;->mIsDestroyed:Z

    return v0
.end method

.method public isEmptyScreenShowing()Z
    .locals 1

    .prologue
    .line 1445
    iget-boolean v0, p0, Lcom/google/android/music/ui/BaseActivity;->mEmptyScreenShowing:Z

    return v0
.end method

.method protected isSideDrawerOpen()Z
    .locals 2

    .prologue
    .line 609
    iget-object v0, p0, Lcom/google/android/music/ui/BaseActivity;->mPlayDrawerLayout:Lcom/google/android/play/drawer/PlayDrawerLayout;

    if-eqz v0, :cond_0

    .line 610
    iget-object v0, p0, Lcom/google/android/music/ui/BaseActivity;->mPlayDrawerLayout:Lcom/google/android/play/drawer/PlayDrawerLayout;

    invoke-virtual {v0}, Lcom/google/android/play/drawer/PlayDrawerLayout;->isDrawerOpen()Z

    move-result v0

    .line 612
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/music/ui/BaseActivity;->mSideDrawerContainer:Landroid/support/v4/widget/DrawerLayout;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/music/ui/BaseActivity;->mSideDrawerEnabled:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/music/ui/BaseActivity;->mSideDrawerContainer:Landroid/support/v4/widget/DrawerLayout;

    iget-object v1, p0, Lcom/google/android/music/ui/BaseActivity;->mSideDrawer:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->isDrawerOpen(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected loadSideDrawer(I)V
    .locals 10
    .param p1, "layoutId"    # I

    .prologue
    const/4 v2, 0x0

    .line 525
    iget-object v0, p0, Lcom/google/android/music/ui/BaseActivity;->mSideDrawerContainer:Landroid/support/v4/widget/DrawerLayout;

    if-eqz v0, :cond_0

    .line 526
    iget-object v0, p0, Lcom/google/android/music/ui/BaseActivity;->mSideDrawerContainer:Landroid/support/v4/widget/DrawerLayout;

    instance-of v0, v0, Lcom/google/android/play/drawer/PlayDrawerLayout;

    if-eqz v0, :cond_1

    .line 527
    iget-object v0, p0, Lcom/google/android/music/ui/BaseActivity;->mSideDrawerContainer:Landroid/support/v4/widget/DrawerLayout;

    check-cast v0, Lcom/google/android/play/drawer/PlayDrawerLayout;

    iput-object v0, p0, Lcom/google/android/music/ui/BaseActivity;->mPlayDrawerLayout:Lcom/google/android/play/drawer/PlayDrawerLayout;

    .line 528
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->getUIStateManager()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/ui/UIStateManager;->getPlayCommonNetworkStack()Lcom/google/android/play/utils/PlayCommonNetworkStack;

    move-result-object v9

    .line 529
    .local v9, "stack":Lcom/google/android/play/utils/PlayCommonNetworkStack;
    iget-object v0, p0, Lcom/google/android/music/ui/BaseActivity;->mPlayDrawerLayout:Lcom/google/android/play/drawer/PlayDrawerLayout;

    iget v3, p0, Lcom/google/android/music/ui/BaseActivity;->mActionBarHeight:I

    invoke-virtual {v9}, Lcom/google/android/play/utils/PlayCommonNetworkStack;->getPlayDfeApiProvider()Lcom/google/android/play/dfe/api/PlayDfeApiProvider;

    move-result-object v5

    invoke-virtual {v9}, Lcom/google/android/play/utils/PlayCommonNetworkStack;->getBitmapLoader()Lcom/google/android/play/image/BitmapLoader;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/music/ui/BaseActivity;->mDrawerListener:Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerContentClickListener;

    move-object v1, p0

    move v4, v2

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/play/drawer/PlayDrawerLayout;->configure(Landroid/app/Activity;ZIZLcom/google/android/play/dfe/api/PlayDfeApiProvider;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerContentClickListener;)V

    .line 531
    iget-object v0, p0, Lcom/google/android/music/ui/BaseActivity;->mPlayDrawerLayout:Lcom/google/android/play/drawer/PlayDrawerLayout;

    iget-object v1, p0, Lcom/google/android/music/ui/BaseActivity;->mSimpleDrawerListener:Landroid/support/v4/widget/DrawerLayout$SimpleDrawerListener;

    invoke-virtual {v0, v1}, Lcom/google/android/play/drawer/PlayDrawerLayout;->setDrawerListener(Landroid/support/v4/widget/DrawerLayout$DrawerListener;)V

    .line 532
    iget-object v0, p0, Lcom/google/android/music/ui/BaseActivity;->mPlayDrawerLayout:Lcom/google/android/play/drawer/PlayDrawerLayout;

    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->useActionBarHamburger()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/play/drawer/PlayDrawerLayout;->setDrawerIndicatorEnabled(Z)V

    .line 533
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->updateMusicDrawer()V

    .line 545
    .end local v9    # "stack":Lcom/google/android/play/utils/PlayCommonNetworkStack;
    :goto_0
    invoke-direct {p0}, Lcom/google/android/music/ui/BaseActivity;->updateDrawerLockMode()V

    .line 547
    :cond_0
    return-void

    .line 535
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/ui/BaseActivity;->mSideDrawerContainer:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v0, p1, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/BaseActivity;->mSideDrawer:Landroid/view/View;

    .line 536
    iget-object v0, p0, Lcom/google/android/music/ui/BaseActivity;->mSideDrawerContainer:Landroid/support/v4/widget/DrawerLayout;

    iget-object v1, p0, Lcom/google/android/music/ui/BaseActivity;->mSideDrawer:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->addView(Landroid/view/View;)V

    .line 537
    iget-object v0, p0, Lcom/google/android/music/ui/BaseActivity;->mSideDrawerContainer:Landroid/support/v4/widget/DrawerLayout;

    iget-object v1, p0, Lcom/google/android/music/ui/BaseActivity;->mSimpleDrawerListener:Landroid/support/v4/widget/DrawerLayout$SimpleDrawerListener;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->setDrawerListener(Landroid/support/v4/widget/DrawerLayout$DrawerListener;)V

    .line 539
    iget-object v0, p0, Lcom/google/android/music/ui/BaseActivity;->mSideDrawer:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v8

    check-cast v8, Landroid/support/v4/widget/DrawerLayout$LayoutParams;

    .line 541
    .local v8, "lp":Landroid/support/v4/widget/DrawerLayout$LayoutParams;
    const/4 v0, 0x3

    iput v0, v8, Landroid/support/v4/widget/DrawerLayout$LayoutParams;->gravity:I

    .line 542
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->calculateSideDrawerWidth()I

    move-result v0

    iput v0, v8, Landroid/support/v4/widget/DrawerLayout$LayoutParams;->width:I

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 8
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1110
    invoke-super {p0, p1, p2, p3}, Landroid/support/v7/app/ActionBarActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 1111
    if-nez p3, :cond_1

    .line 1112
    const-string v1, "MusicBaseActivity"

    const-string v2, "onActivityResult: received null Intent. requestCode=%s resultCode=%s"

    new-array v3, v7, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1125
    :cond_0
    :goto_0
    return-void

    .line 1118
    :cond_1
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 1119
    .local v0, "extras":Landroid/os/Bundle;
    const-string v2, "MusicBaseActivity"

    const-string v3, "onActivityResult: requestCode=%s resultCode=%s data=%s extras=%s"

    const/4 v1, 0x4

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v5

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v6

    aput-object p3, v4, v7

    const/4 v5, 0x3

    if-nez v0, :cond_2

    const-string v1, "null"

    :goto_1
    aput-object v1, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1122
    const/16 v1, 0x3e8

    if-ne p1, v1, :cond_0

    goto :goto_0

    .line 1119
    :cond_2
    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->bundleToString(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 738
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->isSideDrawerOpen()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 739
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->closeSideDrawer()V

    .line 745
    :goto_0
    return-void

    .line 740
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->getBottomDrawer()Lcom/google/android/music/widgets/ExpandingScrollView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/widgets/ExpandingScrollView;->getExpandingState()Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    move-result-object v0

    sget-object v1, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->FULLY_EXPANDED:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    if-ne v0, v1, :cond_1

    .line 741
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->getBottomDrawer()Lcom/google/android/music/widgets/ExpandingScrollView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/widgets/ExpandingScrollView;->collapse()Z

    goto :goto_0

    .line 743
    :cond_1
    invoke-super {p0}, Landroid/support/v7/app/ActionBarActivity;->onBackPressed()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 12
    .param p1, "savedState"    # Landroid/os/Bundle;

    .prologue
    const/4 v11, 0x3

    const/4 v10, 0x2

    .line 767
    invoke-super {p0, p1}, Landroid/support/v7/app/ActionBarActivity;->onCreate(Landroid/os/Bundle;)V

    .line 769
    invoke-static {}, Lcom/google/android/music/preferences/MusicPreferences;->isLmpOrGreater()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 770
    new-instance v6, Landroid/animation/ArgbEvaluator;

    invoke-direct {v6}, Landroid/animation/ArgbEvaluator;-><init>()V

    iput-object v6, p0, Lcom/google/android/music/ui/BaseActivity;->mArgbEvaluator:Landroid/animation/ArgbEvaluator;

    .line 771
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0c003d

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    iput v6, p0, Lcom/google/android/music/ui/BaseActivity;->mOrangeStatusBarColor:I

    .line 772
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0c00d3

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    iput v6, p0, Lcom/google/android/music/ui/BaseActivity;->mGreyStatusBarColor:I

    .line 776
    :cond_0
    new-array v6, v10, [I

    fill-array-data v6, :array_0

    invoke-static {v6}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/music/ui/BaseActivity;->mFloatingActionBarAnimator:Landroid/animation/ValueAnimator;

    .line 778
    iget-object v6, p0, Lcom/google/android/music/ui/BaseActivity;->mFloatingActionBarAnimator:Landroid/animation/ValueAnimator;

    const-wide/16 v8, 0x2bc

    invoke-virtual {v6, v8, v9}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 779
    iget-object v6, p0, Lcom/google/android/music/ui/BaseActivity;->mFloatingActionBarAnimator:Landroid/animation/ValueAnimator;

    new-instance v7, Lcom/google/android/music/ui/BaseActivity$7;

    invoke-direct {v7, p0}, Lcom/google/android/music/ui/BaseActivity$7;-><init>(Lcom/google/android/music/ui/BaseActivity;)V

    invoke-virtual {v6, v7}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 787
    const/16 v6, 0x9

    invoke-virtual {p0, v6}, Lcom/google/android/music/ui/BaseActivity;->supportRequestWindowFeature(I)Z

    .line 789
    sget-object v6, Lcom/google/android/music/utils/DebugUtils$MusicTag;->CALLS:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v6, p0}, Lcom/google/android/music/utils/DebugUtils;->maybeLogMethodName(Lcom/google/android/music/utils/DebugUtils$MusicTag;Ljava/lang/Object;)V

    .line 790
    invoke-static {p0}, Lcom/google/android/music/ui/UIStateManager;->getInstance(Landroid/content/Context;)Lcom/google/android/music/ui/UIStateManager;

    .line 792
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->getWindow()Landroid/view/Window;

    move-result-object v6

    const/16 v7, 0x3e9

    invoke-virtual {v6, v7}, Landroid/view/Window;->setType(I)V

    .line 795
    invoke-static {p0}, Lcom/google/android/music/cast/CastUtils;->showMrpVolumeDialog(Landroid/content/Context;)Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/android/music/ui/BaseActivity;->mShowMrpVolumeDialog:Z

    .line 796
    iget-boolean v6, p0, Lcom/google/android/music/ui/BaseActivity;->mShowMrpVolumeDialog:Z

    if-nez v6, :cond_1

    .line 797
    invoke-virtual {p0, v11}, Lcom/google/android/music/ui/BaseActivity;->setVolumeControlStream(I)V

    .line 800
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/music/preferences/MusicPreferences;->isTvMusic()Z

    move-result v1

    .line 801
    .local v1, "isTV":Z
    if-eqz v1, :cond_4

    .line 802
    const v6, 0x7f04001f

    invoke-virtual {p0, v6}, Lcom/google/android/music/ui/BaseActivity;->setContentView(I)V

    .line 813
    :goto_0
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6, v10}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v6, p0, Lcom/google/android/music/ui/BaseActivity;->mContentViews:Ljava/util/ArrayList;

    .line 814
    iget-object v6, p0, Lcom/google/android/music/ui/BaseActivity;->mContentViews:Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->getContentContainer()Landroid/view/ViewGroup;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 815
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->setupNowPlayingBar()V

    .line 817
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/music/ui/BaseActivity;->mUIStateChangeListener:Lcom/google/android/music/ui/UIStateManager$UIStateChangeListener;

    invoke-virtual {v6, v7}, Lcom/google/android/music/ui/UIStateManager;->registerUIStateChangeListener(Lcom/google/android/music/ui/UIStateManager$UIStateChangeListener;)V

    .line 819
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    .line 820
    .local v0, "actionBar":Landroid/support/v7/app/ActionBar;
    const/4 v3, 0x6

    .line 821
    .local v3, "optionsMask":I
    const/4 v6, 0x4

    const/4 v7, 0x6

    invoke-virtual {v0, v6, v7}, Landroid/support/v7/app/ActionBar;->setDisplayOptions(II)V

    .line 822
    invoke-direct {p0}, Lcom/google/android/music/ui/BaseActivity;->setActionbarHeight()V

    .line 823
    const v6, 0x7f0e00e2

    invoke-virtual {p0, v6}, Lcom/google/android/music/ui/BaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 825
    .local v4, "root":Landroid/view/View;
    invoke-direct {p0}, Lcom/google/android/music/ui/BaseActivity;->initDownloadOnlyTextSwitcher()V

    .line 827
    const v5, 0x7f0400da

    .line 828
    .local v5, "sideDrawerLayout":I
    if-eqz v1, :cond_5

    .line 829
    invoke-direct {p0, v5}, Lcom/google/android/music/ui/BaseActivity;->loadTVSideDrawer(I)V

    .line 830
    iget-object v6, p0, Lcom/google/android/music/ui/BaseActivity;->mContentViews:Ljava/util/ArrayList;

    iget-object v7, p0, Lcom/google/android/music/ui/BaseActivity;->mTVSideDrawerContainer:Landroid/view/ViewGroup;

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 838
    .end local v4    # "root":Landroid/view/View;
    :goto_1
    const v6, 0x7f0e025b

    invoke-virtual {p0, v6}, Lcom/google/android/music/ui/BaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ListView;

    .line 839
    .local v2, "listView":Landroid/widget/ListView;
    invoke-virtual {p0, v2}, Lcom/google/android/music/ui/BaseActivity;->setupRootMenu(Landroid/widget/ListView;)V

    .line 845
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->getDrawerStateOpenDefault()Z

    move-result v6

    if-nez v6, :cond_2

    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/music/preferences/MusicPreferences;->wasSidePannelClosedOnce()Z

    move-result v6

    if-nez v6, :cond_6

    :cond_2
    const/4 v6, 0x1

    :goto_2
    iput-boolean v6, p0, Lcom/google/android/music/ui/BaseActivity;->mDrawerRestoredStateOpen:Z

    .line 848
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->getDrawerAutoHideDefault()Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/android/music/ui/BaseActivity;->mDrawerAutoHide:Z

    .line 849
    if-eqz p1, :cond_7

    .end local p1    # "savedState":Landroid/os/Bundle;
    :goto_3
    invoke-direct {p0, p1}, Lcom/google/android/music/ui/BaseActivity;->initializeDrawerState(Landroid/os/Bundle;)V

    .line 854
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/music/preferences/MusicPreferences;->isMediaRouteSupportEnabled()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/music/preferences/MusicPreferences;->hasStreamingAccount()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 856
    new-instance v6, Lcom/google/android/music/ui/mrp/MediaRouteManager;

    invoke-direct {v6, p0}, Lcom/google/android/music/ui/mrp/MediaRouteManager;-><init>(Landroid/content/Context;)V

    iput-object v6, p0, Lcom/google/android/music/ui/BaseActivity;->mMediaRouteManager:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    .line 857
    iget-object v6, p0, Lcom/google/android/music/ui/BaseActivity;->mMediaRouteManager:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    invoke-virtual {v6}, Lcom/google/android/music/ui/mrp/MediaRouteManager;->onCreate()V

    .line 858
    iget-boolean v6, p0, Lcom/google/android/music/ui/BaseActivity;->mShowMrpVolumeDialog:Z

    if-eqz v6, :cond_3

    .line 859
    iget-object v6, p0, Lcom/google/android/music/ui/BaseActivity;->mMediaRouteManager:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    iget-object v7, p0, Lcom/google/android/music/ui/BaseActivity;->mRouteSelectionCallback:Lcom/google/android/music/ui/BaseActivity$RouteSelectionCallback;

    invoke-virtual {v6, v7}, Lcom/google/android/music/ui/mrp/MediaRouteManager;->addMediaRouterCallback(Landroid/support/v7/media/MediaRouter$Callback;)V

    .line 863
    :cond_3
    const/16 v6, 0xff

    invoke-virtual {p0, v6}, Lcom/google/android/music/ui/BaseActivity;->setActionBarAlpha(I)V

    .line 864
    return-void

    .line 804
    .end local v0    # "actionBar":Landroid/support/v7/app/ActionBar;
    .end local v2    # "listView":Landroid/widget/ListView;
    .end local v3    # "optionsMask":I
    .end local v5    # "sideDrawerLayout":I
    .restart local p1    # "savedState":Landroid/os/Bundle;
    :cond_4
    const v6, 0x7f04001e

    invoke-virtual {p0, v6}, Lcom/google/android/music/ui/BaseActivity;->setContentView(I)V

    goto/16 :goto_0

    .line 832
    .restart local v0    # "actionBar":Landroid/support/v7/app/ActionBar;
    .restart local v3    # "optionsMask":I
    .restart local v4    # "root":Landroid/view/View;
    .restart local v5    # "sideDrawerLayout":I
    :cond_5
    check-cast v4, Landroid/support/v4/widget/DrawerLayout;

    .end local v4    # "root":Landroid/view/View;
    iput-object v4, p0, Lcom/google/android/music/ui/BaseActivity;->mSideDrawerContainer:Landroid/support/v4/widget/DrawerLayout;

    .line 833
    iget-object v6, p0, Lcom/google/android/music/ui/BaseActivity;->mSideDrawerContainer:Landroid/support/v4/widget/DrawerLayout;

    const v7, 0x7f0200bb

    invoke-virtual {v6, v7, v11}, Landroid/support/v4/widget/DrawerLayout;->setDrawerShadow(II)V

    .line 836
    invoke-virtual {p0, v5}, Lcom/google/android/music/ui/BaseActivity;->loadSideDrawer(I)V

    goto :goto_1

    .line 845
    .restart local v2    # "listView":Landroid/widget/ListView;
    :cond_6
    const/4 v6, 0x0

    goto :goto_2

    .line 849
    :cond_7
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->getIntent()Landroid/content/Intent;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p1

    goto :goto_3

    .line 776
    nop

    :array_0
    .array-data 4
        0xff
        0x0
    .end array-data
.end method

.method public onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .locals 16
    .param p1, "id"    # I
    .param p2, "args"    # Landroid/os/Bundle;

    .prologue
    .line 1329
    const/4 v3, 0x0

    .line 1340
    .local v3, "dialog":Landroid/app/AlertDialog;
    const/16 v4, 0x67

    move/from16 v0, p1

    if-ne v0, v4, :cond_2

    .line 1341
    new-instance v2, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1342
    .local v2, "builder":Landroid/app/AlertDialog$Builder;
    const v4, 0x7f0b015f

    invoke-virtual {v2, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 1343
    const v4, 0x7f0b0160

    invoke-virtual {v2, v4}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 1344
    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 1345
    const v4, 0x7f0b004e

    const/4 v14, 0x0

    invoke-virtual {v2, v4, v14}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1346
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    .line 1384
    .end local v2    # "builder":Landroid/app/AlertDialog$Builder;
    :cond_0
    :goto_0
    if-eqz v3, :cond_1

    .line 1385
    new-instance v4, Lcom/google/android/music/ui/BaseActivity$12;

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-direct {v4, v0, v1}, Lcom/google/android/music/ui/BaseActivity$12;-><init>(Lcom/google/android/music/ui/BaseActivity;I)V

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 1391
    :cond_1
    return-object v3

    .line 1347
    :cond_2
    const/16 v4, 0x6b

    move/from16 v0, p1

    if-ne v0, v4, :cond_3

    .line 1348
    new-instance v2, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1349
    .restart local v2    # "builder":Landroid/app/AlertDialog$Builder;
    const v4, 0x7f0b0051

    invoke-virtual {v2, v4}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 1350
    const v4, 0x7f0b0216

    const/4 v14, 0x0

    invoke-virtual {v2, v4, v14}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1351
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    .line 1352
    goto :goto_0

    .end local v2    # "builder":Landroid/app/AlertDialog$Builder;
    :cond_3
    const/16 v4, 0x6c

    move/from16 v0, p1

    if-ne v0, v4, :cond_4

    .line 1353
    const-string v4, "errorType"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v11

    .line 1354
    .local v11, "errorType":I
    const-string v4, "musicId"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v13

    check-cast v13, Lcom/google/android/music/download/ContentIdentifier;

    .line 1356
    .local v13, "musicId":Lcom/google/android/music/download/ContentIdentifier;
    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-static {v11, v0, v4}, Lcom/google/android/music/playback/ErrorInfo;->createErrorInfo(ILandroid/content/Context;Lcom/google/android/music/net/INetworkMonitor;)Lcom/google/android/music/playback/ErrorInfo;

    move-result-object v12

    .line 1357
    .local v12, "handler":Lcom/google/android/music/playback/ErrorInfo;
    invoke-virtual {v12}, Lcom/google/android/music/playback/ErrorInfo;->canShowAlert()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1358
    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/google/android/music/ui/BaseActivity;->mCurrentFailureDialogsMusicId:Lcom/google/android/music/download/ContentIdentifier;

    .line 1359
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/android/music/ui/BaseActivity;->mShowingFailureAlert:Z

    .line 1360
    new-instance v4, Lcom/google/android/music/ui/BaseActivity$11;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Lcom/google/android/music/ui/BaseActivity$11;-><init>(Lcom/google/android/music/ui/BaseActivity;)V

    move-object/from16 v0, p0

    invoke-virtual {v12, v0, v4}, Lcom/google/android/music/playback/ErrorInfo;->createAlert(Landroid/app/Activity;Lcom/google/android/music/playback/ErrorInfo$OnErrorAlertDismissed;)Landroid/app/AlertDialog;

    move-result-object v3

    .line 1368
    if-nez v3, :cond_0

    .line 1370
    const-string v4, "MusicBaseActivity"

    const-string v14, "Unexpected null alert"

    new-instance v15, Ljava/lang/NullPointerException;

    invoke-direct {v15}, Ljava/lang/NullPointerException;-><init>()V

    invoke-static {v4, v14, v15}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 1373
    .end local v11    # "errorType":I
    .end local v12    # "handler":Lcom/google/android/music/playback/ErrorInfo;
    .end local v13    # "musicId":Lcom/google/android/music/download/ContentIdentifier;
    :cond_4
    const/16 v4, 0x6d

    move/from16 v0, p1

    if-ne v0, v4, :cond_0

    .line 1374
    const-string v4, "deleteId"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v6

    .line 1375
    .local v6, "itemId":J
    const-string v4, "deleteName"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 1377
    .local v8, "name":Ljava/lang/String;
    const-string v4, "deleteArtistName"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 1378
    .local v9, "artist":Ljava/lang/String;
    const-string v4, "deleteHasRemote"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v10

    .line 1379
    .local v10, "hasRemote":Z
    invoke-static {}, Lcom/google/android/music/DeleteConfirmationDialog$DeletionType;->values()[Lcom/google/android/music/DeleteConfirmationDialog$DeletionType;

    move-result-object v4

    const-string v14, "deleteType"

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v14

    aget-object v5, v4, v14

    .line 1382
    .local v5, "type":Lcom/google/android/music/DeleteConfirmationDialog$DeletionType;
    new-instance v3, Lcom/google/android/music/DeleteConfirmationDialog;

    .end local v3    # "dialog":Landroid/app/AlertDialog;
    move-object/from16 v4, p0

    invoke-direct/range {v3 .. v10}, Lcom/google/android/music/DeleteConfirmationDialog;-><init>(Landroid/content/Context;Lcom/google/android/music/DeleteConfirmationDialog$DeletionType;JLjava/lang/CharSequence;Ljava/lang/CharSequence;Z)V

    .restart local v3    # "dialog":Landroid/app/AlertDialog;
    goto/16 :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 1056
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 1057
    .local v0, "inflater":Landroid/view/MenuInflater;
    const v1, 0x7f140002

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 1059
    iget-object v1, p0, Lcom/google/android/music/ui/BaseActivity;->mMediaRouteManager:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    if-eqz v1, :cond_0

    .line 1060
    iget-object v1, p0, Lcom/google/android/music/ui/BaseActivity;->mMediaRouteManager:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    invoke-virtual {v1, p1}, Lcom/google/android/music/ui/mrp/MediaRouteManager;->onCreateOptionsMenu(Landroid/view/Menu;)V

    .line 1062
    :cond_0
    const/4 v1, 0x1

    return v1
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 1014
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->CALLS:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0, p0}, Lcom/google/android/music/utils/DebugUtils;->maybeLogMethodName(Lcom/google/android/music/utils/DebugUtils$MusicTag;Ljava/lang/Object;)V

    .line 1015
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/ui/BaseActivity;->mUIStateChangeListener:Lcom/google/android/music/ui/UIStateManager$UIStateChangeListener;

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/UIStateManager;->unregisterUIStateChangeListener(Lcom/google/android/music/ui/UIStateManager$UIStateChangeListener;)V

    .line 1016
    iget-object v0, p0, Lcom/google/android/music/ui/BaseActivity;->mDownloadOnlyTextSwitcher:Landroid/widget/TextSwitcher;

    if-eqz v0, :cond_0

    .line 1017
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/ui/BaseActivity;->mPrefChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-virtual {v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 1019
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/ui/BaseActivity;->mSideDrawerContainer:Landroid/support/v4/widget/DrawerLayout;

    .line 1020
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/ui/BaseActivity;->mIsDestroyed:Z

    .line 1021
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->removeUpIntent()V

    .line 1022
    iget-object v0, p0, Lcom/google/android/music/ui/BaseActivity;->mMediaRouteManager:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    if-eqz v0, :cond_1

    .line 1023
    iget-object v0, p0, Lcom/google/android/music/ui/BaseActivity;->mMediaRouteManager:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    invoke-virtual {v0}, Lcom/google/android/music/ui/mrp/MediaRouteManager;->onDestroy()V

    .line 1025
    :cond_1
    invoke-super {p0}, Landroid/support/v7/app/ActionBarActivity;->onDestroy()V

    .line 1026
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 1467
    iget-boolean v0, p0, Lcom/google/android/music/ui/BaseActivity;->mShowMrpVolumeDialog:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/ui/BaseActivity;->mMediaRouteManager:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/ui/BaseActivity;->mMediaRouteManager:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/music/ui/mrp/MediaRouteManager;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1469
    const/4 v0, 0x1

    .line 1471
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/support/v7/app/ActionBarActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onNewIntent(Landroid/content/Intent;)V
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 1044
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->CALLS:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0, p0}, Lcom/google/android/music/utils/DebugUtils;->maybeLogMethodName(Lcom/google/android/music/utils/DebugUtils$MusicTag;Ljava/lang/Object;)V

    .line 1046
    invoke-super {p0, p1}, Landroid/support/v7/app/ActionBarActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 1047
    invoke-virtual {p0, p1}, Lcom/google/android/music/ui/BaseActivity;->setIntent(Landroid/content/Intent;)V

    .line 1049
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/music/ui/BaseActivity;->initializeDrawerState(Landroid/os/Bundle;)V

    .line 1050
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->updateMusicDrawer()V

    .line 1051
    return-void
.end method

.method protected onNewNautilusStatus(Lcom/google/android/music/NautilusStatus;)V
    .locals 3
    .param p1, "newStatus"    # Lcom/google/android/music/NautilusStatus;

    .prologue
    .line 1453
    const-string v0, "MusicBaseActivity"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1454
    const-string v1, "MusicBaseActivity"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onNewNautilusStatus status: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " has menu list: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/music/ui/BaseActivity;->mTVMenuView:Landroid/widget/ListView;

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1458
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/ui/BaseActivity;->mTVMenuView:Landroid/widget/ListView;

    if-eqz v0, :cond_1

    .line 1459
    iget-object v0, p0, Lcom/google/android/music/ui/BaseActivity;->mTVMenuView:Landroid/widget/ListView;

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/BaseActivity;->setupRootMenu(Landroid/widget/ListView;)V

    .line 1462
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->supportInvalidateOptionsMenu()V

    .line 1463
    return-void

    .line 1454
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v3, 0x1

    .line 1077
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 1105
    invoke-super {p0, p1}, Landroid/support/v7/app/ActionBarActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v2

    :goto_0
    return v2

    .line 1079
    :sswitch_0
    sget-object v2, Lcom/google/android/music/ui/BaseActivity;->sUpList:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v3

    .line 1080
    .local v0, "hasUpIntent":Z
    :goto_1
    if-eqz v0, :cond_1

    sget-object v2, Lcom/google/android/music/ui/BaseActivity;->sUpList:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Intent;

    move-object v1, v2

    .line 1081
    .local v1, "upIntent":Landroid/content/Intent;
    :goto_2
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->isTaskRoot()Z

    move-result v2

    if-eqz v2, :cond_2

    instance-of v2, p0, Lcom/google/android/music/ui/HomeActivity;

    if-nez v2, :cond_2

    .line 1085
    invoke-static {p0}, Lcom/google/android/music/ui/HomeActivity;->createHomeScreenIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/music/ui/BaseActivity;->startActivity(Landroid/content/Intent;)V

    .line 1086
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->finish()V

    :goto_3
    move v2, v3

    .line 1095
    goto :goto_0

    .line 1079
    .end local v0    # "hasUpIntent":Z
    .end local v1    # "upIntent":Landroid/content/Intent;
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 1080
    .restart local v0    # "hasUpIntent":Z
    :cond_1
    const/4 v1, 0x0

    goto :goto_2

    .line 1087
    .restart local v1    # "upIntent":Landroid/content/Intent;
    :cond_2
    if-eqz v1, :cond_3

    iget-object v2, p0, Lcom/google/android/music/ui/BaseActivity;->mUpIntent:Landroid/content/Intent;

    if-eq v1, v2, :cond_3

    .line 1090
    invoke-virtual {p0, v1}, Lcom/google/android/music/ui/BaseActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_3

    .line 1093
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->onBackPressed()V

    goto :goto_3

    .line 1097
    .end local v0    # "hasUpIntent":Z
    .end local v1    # "upIntent":Landroid/content/Intent;
    :sswitch_1
    invoke-static {p0}, Lcom/google/android/music/ui/SearchActivity;->showSearch(Landroid/content/Context;)V

    move v2, v3

    .line 1098
    goto :goto_0

    .line 1077
    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f0e02a2 -> :sswitch_1
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 994
    invoke-super {p0}, Landroid/support/v7/app/ActionBarActivity;->onPause()V

    .line 995
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->CALLS:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0, p0}, Lcom/google/android/music/utils/DebugUtils;->maybeLogMethodName(Lcom/google/android/music/utils/DebugUtils$MusicTag;Ljava/lang/Object;)V

    .line 996
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/ui/UIStateManager;->onPause()V

    .line 997
    iget-object v0, p0, Lcom/google/android/music/ui/BaseActivity;->mMediaPlayerBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/BaseActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 998
    iget-object v0, p0, Lcom/google/android/music/ui/BaseActivity;->mMediaRouteManager:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    if-eqz v0, :cond_0

    .line 999
    iget-object v0, p0, Lcom/google/android/music/ui/BaseActivity;->mMediaRouteManager:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    invoke-virtual {v0}, Lcom/google/android/music/ui/mrp/MediaRouteManager;->onPause()V

    .line 1001
    :cond_0
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 3
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v2, 0x0

    .line 1067
    invoke-static {p0}, Lcom/google/android/music/ui/UIStateManager;->getInstance(Landroid/content/Context;)Lcom/google/android/music/ui/UIStateManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    .line 1068
    .local v0, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    iget-boolean v1, p0, Lcom/google/android/music/ui/BaseActivity;->mEmptyScreenShowing:Z

    if-eqz v1, :cond_0

    .line 1069
    const v1, 0x7f0e02a2

    invoke-virtual {p0, p1, v1, v2, v2}, Lcom/google/android/music/ui/BaseActivity;->setMenuVisibility(Landroid/view/Menu;IZZ)V

    .line 1071
    :cond_0
    invoke-super {p0, p1}, Landroid/support/v7/app/ActionBarActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v1

    return v1
.end method

.method protected onRestart()V
    .locals 1

    .prologue
    .line 930
    invoke-super {p0}, Landroid/support/v7/app/ActionBarActivity;->onRestart()V

    .line 931
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->CALLS:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0, p0}, Lcom/google/android/music/utils/DebugUtils;->maybeLogMethodName(Lcom/google/android/music/utils/DebugUtils$MusicTag;Ljava/lang/Object;)V

    .line 932
    iget-object v0, p0, Lcom/google/android/music/ui/BaseActivity;->mMediaRouteManager:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    if-eqz v0, :cond_0

    .line 933
    iget-object v0, p0, Lcom/google/android/music/ui/BaseActivity;->mMediaRouteManager:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    invoke-virtual {v0}, Lcom/google/android/music/ui/mrp/MediaRouteManager;->onRestart()V

    .line 935
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 973
    invoke-super {p0}, Landroid/support/v7/app/ActionBarActivity;->onResume()V

    .line 974
    sget-object v1, Lcom/google/android/music/utils/DebugUtils$MusicTag;->CALLS:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v1, p0}, Lcom/google/android/music/utils/DebugUtils;->maybeLogMethodName(Lcom/google/android/music/utils/DebugUtils$MusicTag;Ljava/lang/Object;)V

    .line 975
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/ui/UIStateManager;->onResume()V

    .line 976
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 977
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "com.android.music.playbackfailed"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 978
    iget-object v1, p0, Lcom/google/android/music/ui/BaseActivity;->mMediaPlayerBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/google/android/music/ui/BaseActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 981
    invoke-direct {p0}, Lcom/google/android/music/ui/BaseActivity;->syncActionBarIconState()V

    .line 982
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->updateMusicDrawer()V

    .line 984
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->isSideDrawerOpen()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 985
    const v1, 0x7f0b00e3

    const/4 v2, 0x1

    invoke-direct {p0, v1, v2}, Lcom/google/android/music/ui/BaseActivity;->setActionBarTitleInternal(IZ)V

    .line 987
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/ui/BaseActivity;->mMediaRouteManager:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    if-eqz v1, :cond_1

    .line 988
    iget-object v1, p0, Lcom/google/android/music/ui/BaseActivity;->mMediaRouteManager:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    invoke-virtual {v1}, Lcom/google/android/music/ui/mrp/MediaRouteManager;->onResume()V

    .line 990
    :cond_1
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 1030
    invoke-super {p0, p1}, Landroid/support/v7/app/ActionBarActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1031
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->CALLS:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0, p0}, Lcom/google/android/music/utils/DebugUtils;->maybeLogMethodName(Lcom/google/android/music/utils/DebugUtils$MusicTag;Ljava/lang/Object;)V

    .line 1036
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->isSideDrawerOpen()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/music/ui/BaseActivity;->mDrawerRestoredStateOpen:Z

    .line 1037
    const-string v0, "music:base:drawer_state_open"

    iget-boolean v1, p0, Lcom/google/android/music/ui/BaseActivity;->mDrawerRestoredStateOpen:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1039
    const-string v0, "music:base:drawer_auto_hide"

    iget-boolean v1, p0, Lcom/google/android/music/ui/BaseActivity;->mDrawerAutoHide:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1040
    return-void
.end method

.method protected onStart()V
    .locals 2

    .prologue
    const/4 v1, 0x3

    .line 939
    invoke-super {p0}, Landroid/support/v7/app/ActionBarActivity;->onStart()V

    .line 940
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->CALLS:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0, p0}, Lcom/google/android/music/utils/DebugUtils;->maybeLogMethodName(Lcom/google/android/music/utils/DebugUtils$MusicTag;Ljava/lang/Object;)V

    .line 941
    iget-object v0, p0, Lcom/google/android/music/ui/BaseActivity;->mMediaRouteManager:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    if-eqz v0, :cond_2

    .line 942
    iget-object v0, p0, Lcom/google/android/music/ui/BaseActivity;->mMediaRouteManager:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    invoke-virtual {v0}, Lcom/google/android/music/ui/mrp/MediaRouteManager;->isRemoteRouteSelected()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/music/ui/BaseActivity;->mShowMrpVolumeDialog:Z

    if-eqz v0, :cond_1

    .line 943
    const/high16 v0, -0x80000000

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/BaseActivity;->setVolumeControlStream(I)V

    .line 948
    :goto_0
    iget-boolean v0, p0, Lcom/google/android/music/ui/BaseActivity;->mShowMrpVolumeDialog:Z

    if-eqz v0, :cond_0

    .line 949
    iget-object v0, p0, Lcom/google/android/music/ui/BaseActivity;->mMediaRouteManager:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    iget-object v1, p0, Lcom/google/android/music/ui/BaseActivity;->mRouteSelectionCallback:Lcom/google/android/music/ui/BaseActivity$RouteSelectionCallback;

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/mrp/MediaRouteManager;->addMediaRouterCallback(Landroid/support/v7/media/MediaRouter$Callback;)V

    .line 951
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/ui/BaseActivity;->mMediaRouteManager:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    invoke-virtual {v0}, Lcom/google/android/music/ui/mrp/MediaRouteManager;->onStart()V

    .line 969
    :goto_1
    return-void

    .line 945
    :cond_1
    invoke-virtual {p0, v1}, Lcom/google/android/music/ui/BaseActivity;->setVolumeControlStream(I)V

    goto :goto_0

    .line 952
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->isMediaRouteSupportEnabled()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->hasStreamingAccount()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 954
    invoke-virtual {p0, v1}, Lcom/google/android/music/ui/BaseActivity;->setVolumeControlStream(I)V

    .line 962
    new-instance v0, Lcom/google/android/music/ui/mrp/MediaRouteManager;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/mrp/MediaRouteManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/music/ui/BaseActivity;->mMediaRouteManager:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    .line 963
    iget-object v0, p0, Lcom/google/android/music/ui/BaseActivity;->mMediaRouteManager:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    invoke-virtual {v0}, Lcom/google/android/music/ui/mrp/MediaRouteManager;->onCreate()V

    .line 964
    iget-object v0, p0, Lcom/google/android/music/ui/BaseActivity;->mMediaRouteManager:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    iget-object v1, p0, Lcom/google/android/music/ui/BaseActivity;->mRouteSelectionCallback:Lcom/google/android/music/ui/BaseActivity$RouteSelectionCallback;

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/mrp/MediaRouteManager;->addMediaRouterCallback(Landroid/support/v7/media/MediaRouter$Callback;)V

    .line 965
    iget-object v0, p0, Lcom/google/android/music/ui/BaseActivity;->mMediaRouteManager:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    invoke-virtual {v0}, Lcom/google/android/music/ui/mrp/MediaRouteManager;->onStart()V

    goto :goto_1

    .line 967
    :cond_3
    invoke-virtual {p0, v1}, Lcom/google/android/music/ui/BaseActivity;->setVolumeControlStream(I)V

    goto :goto_1
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 1005
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->CALLS:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0, p0}, Lcom/google/android/music/utils/DebugUtils;->maybeLogMethodName(Lcom/google/android/music/utils/DebugUtils$MusicTag;Ljava/lang/Object;)V

    .line 1006
    invoke-super {p0}, Landroid/support/v7/app/ActionBarActivity;->onStop()V

    .line 1007
    iget-object v0, p0, Lcom/google/android/music/ui/BaseActivity;->mMediaRouteManager:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    if-eqz v0, :cond_0

    .line 1008
    iget-object v0, p0, Lcom/google/android/music/ui/BaseActivity;->mMediaRouteManager:Lcom/google/android/music/ui/mrp/MediaRouteManager;

    invoke-virtual {v0}, Lcom/google/android/music/ui/mrp/MediaRouteManager;->onStop()V

    .line 1010
    :cond_0
    return-void
.end method

.method protected openSideDrawer()V
    .locals 2

    .prologue
    .line 618
    iget-object v0, p0, Lcom/google/android/music/ui/BaseActivity;->mPlayDrawerLayout:Lcom/google/android/play/drawer/PlayDrawerLayout;

    if-eqz v0, :cond_1

    .line 619
    iget-object v0, p0, Lcom/google/android/music/ui/BaseActivity;->mPlayDrawerLayout:Lcom/google/android/play/drawer/PlayDrawerLayout;

    invoke-virtual {v0}, Lcom/google/android/play/drawer/PlayDrawerLayout;->openDrawer()V

    .line 625
    :cond_0
    :goto_0
    return-void

    .line 621
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/ui/BaseActivity;->mSideDrawerContainer:Landroid/support/v4/widget/DrawerLayout;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/music/ui/BaseActivity;->mSideDrawerEnabled:Z

    if-eqz v0, :cond_0

    .line 622
    iget-object v0, p0, Lcom/google/android/music/ui/BaseActivity;->mSideDrawerContainer:Landroid/support/v4/widget/DrawerLayout;

    iget-object v1, p0, Lcom/google/android/music/ui/BaseActivity;->mSideDrawer:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->openDrawer(Landroid/view/View;)V

    goto :goto_0
.end method

.method protected removeUpIntent()V
    .locals 2

    .prologue
    .line 721
    iget-object v0, p0, Lcom/google/android/music/ui/BaseActivity;->mUpIntent:Landroid/content/Intent;

    if-nez v0, :cond_1

    .line 731
    :cond_0
    :goto_0
    return-void

    .line 722
    :cond_1
    sget-boolean v0, Lcom/google/android/music/ui/BaseActivity;->LOGV:Z

    if-eqz v0, :cond_2

    .line 723
    sget-object v0, Lcom/google/android/music/ui/BaseActivity;->sUpList:Ljava/util/LinkedList;

    iget-object v1, p0, Lcom/google/android/music/ui/BaseActivity;->mUpIntent:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 724
    const-string v0, "MusicBaseActivity"

    const-string v1, "RemoveUpIntent with intent not in up stack"

    invoke-static {v0, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 727
    :cond_2
    sget-object v0, Lcom/google/android/music/ui/BaseActivity;->sUpList:Ljava/util/LinkedList;

    iget-object v1, p0, Lcom/google/android/music/ui/BaseActivity;->mUpIntent:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 728
    sget-object v0, Lcom/google/android/music/ui/BaseActivity;->sUpList:Ljava/util/LinkedList;

    iget-object v1, p0, Lcom/google/android/music/ui/BaseActivity;->mUpIntent:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 729
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/ui/BaseActivity;->mUpIntent:Landroid/content/Intent;

    goto :goto_0
.end method

.method protected replaceContent(Landroid/support/v4/app/Fragment;Z)V
    .locals 6
    .param p1, "fragment"    # Landroid/support/v4/app/Fragment;
    .param p2, "addToStack"    # Z

    .prologue
    const/4 v4, -0x1

    .line 346
    const/4 v3, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/music/ui/BaseActivity;->replaceContent(Landroid/support/v4/app/Fragment;ZLjava/lang/String;II)V

    .line 347
    return-void
.end method

.method protected replaceContent(Landroid/support/v4/app/Fragment;ZLjava/lang/String;)V
    .locals 6
    .param p1, "fragment"    # Landroid/support/v4/app/Fragment;
    .param p2, "addToStack"    # Z
    .param p3, "tag"    # Ljava/lang/String;

    .prologue
    const/4 v4, -0x1

    .line 356
    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/music/ui/BaseActivity;->replaceContent(Landroid/support/v4/app/Fragment;ZLjava/lang/String;II)V

    .line 357
    return-void
.end method

.method protected replaceContent(Landroid/support/v4/app/Fragment;ZLjava/lang/String;II)V
    .locals 4
    .param p1, "fragment"    # Landroid/support/v4/app/Fragment;
    .param p2, "addToStack"    # Z
    .param p3, "tag"    # Ljava/lang/String;
    .param p4, "inAnim"    # I
    .param p5, "outAnim"    # I

    .prologue
    const v3, 0x7f0e00d0

    const/4 v2, -0x1

    .line 369
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 370
    .local v0, "fm":Landroid/support/v4/app/FragmentManager;
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    .line 373
    .local v1, "txn":Landroid/support/v4/app/FragmentTransaction;
    if-eq p4, v2, :cond_0

    if-eq p5, v2, :cond_0

    .line 374
    invoke-virtual {v1, p4, p5}, Landroid/support/v4/app/FragmentTransaction;->setCustomAnimations(II)Landroid/support/v4/app/FragmentTransaction;

    .line 378
    :cond_0
    if-eqz p3, :cond_2

    .line 379
    invoke-virtual {v1, v3, p1, p3}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 385
    :goto_0
    if-eqz p2, :cond_1

    .line 386
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 389
    :cond_1
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 390
    iput-object p1, p0, Lcom/google/android/music/ui/BaseActivity;->mContentFragment:Landroid/support/v4/app/Fragment;

    .line 391
    return-void

    .line 381
    :cond_2
    invoke-virtual {v1, v3, p1}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    goto :goto_0
.end method

.method public setActionBarAlpha(I)V
    .locals 1
    .param p1, "actionBarAlpha"    # I

    .prologue
    .line 1206
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->isTvMusic()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1214
    :cond_0
    :goto_0
    return-void

    .line 1210
    :cond_1
    iput p1, p0, Lcom/google/android/music/ui/BaseActivity;->mActionBarAlpha:I

    .line 1211
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->isSideDrawerOpen()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1213
    invoke-direct {p0, p1}, Lcom/google/android/music/ui/BaseActivity;->setActionBarAlphaInternal(I)V

    goto :goto_0
.end method

.method public setActionBarFloating(ZZ)V
    .locals 1
    .param p1, "isFloating"    # Z
    .param p2, "animate"    # Z

    .prologue
    .line 1262
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->isDownloadedOnlyMode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1290
    :cond_0
    :goto_0
    return-void

    .line 1266
    :cond_1
    if-eqz p1, :cond_2

    .line 1267
    const/16 v0, 0xff

    invoke-direct {p0, v0}, Lcom/google/android/music/ui/BaseActivity;->cancelFloatingActionBarAnimation(I)V

    goto :goto_0

    .line 1269
    :cond_2
    if-nez p2, :cond_3

    .line 1270
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/music/ui/BaseActivity;->cancelFloatingActionBarAnimation(I)V

    goto :goto_0

    .line 1273
    :cond_3
    iget-object v0, p0, Lcom/google/android/music/ui/BaseActivity;->mFloatingActionBarAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1281
    new-instance v0, Lcom/google/android/music/ui/BaseActivity$9;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/BaseActivity$9;-><init>(Lcom/google/android/music/ui/BaseActivity;)V

    invoke-static {v0, p0}, Lcom/google/android/music/utils/MusicUtils;->runOnUiThread(Ljava/lang/Runnable;Landroid/content/Context;)V

    goto :goto_0
.end method

.method public setActionBarTitle(IZ)V
    .locals 1
    .param p1, "resId"    # I
    .param p2, "showHome"    # Z

    .prologue
    .line 1168
    iput p1, p0, Lcom/google/android/music/ui/BaseActivity;->mActionBarTitleResId:I

    .line 1169
    iput-boolean p2, p0, Lcom/google/android/music/ui/BaseActivity;->mShowHome:Z

    .line 1170
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/ui/BaseActivity;->mActionBarTitle:Ljava/lang/String;

    .line 1171
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->isSideDrawerOpen()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1172
    const v0, 0x7f0b00e3

    invoke-direct {p0, v0, p2}, Lcom/google/android/music/ui/BaseActivity;->setActionBarTitleInternal(IZ)V

    .line 1176
    :goto_0
    return-void

    .line 1174
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/ui/BaseActivity;->setActionBarTitleInternal(IZ)V

    goto :goto_0
.end method

.method public setActionBarTitle(Ljava/lang/String;)V
    .locals 2
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 1150
    iput-object p1, p0, Lcom/google/android/music/ui/BaseActivity;->mActionBarTitle:Ljava/lang/String;

    .line 1151
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/music/ui/BaseActivity;->mActionBarTitleResId:I

    .line 1152
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->isSideDrawerOpen()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1153
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00e3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/music/ui/BaseActivity;->setActionBarTitleInternal(Ljava/lang/String;)V

    .line 1157
    :goto_0
    return-void

    .line 1155
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/music/ui/BaseActivity;->setActionBarTitleInternal(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setEmptyScreenShowing(Z)V
    .locals 0
    .param p1, "emptyScreenShowing"    # Z

    .prologue
    .line 1449
    iput-boolean p1, p0, Lcom/google/android/music/ui/BaseActivity;->mEmptyScreenShowing:Z

    .line 1450
    return-void
.end method

.method protected setMenuVisibility(Landroid/view/Menu;IZZ)V
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "id"    # I
    .param p3, "visible"    # Z
    .param p4, "enabled"    # Z

    .prologue
    .line 1128
    invoke-interface {p1, p2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 1129
    .local v0, "item":Landroid/view/MenuItem;
    if-eqz v0, :cond_0

    .line 1130
    invoke-interface {v0, p3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1131
    invoke-interface {v0, p4}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 1133
    :cond_0
    return-void
.end method

.method public setRootVisibility(I)V
    .locals 1
    .param p1, "visibility"    # I

    .prologue
    .line 1475
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->getContentContainer()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1476
    return-void
.end method

.method protected setupNowPlayingBar()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 408
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->showNowPlayingBar()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 409
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    .line 411
    .local v2, "fm":Landroid/support/v4/app/FragmentManager;
    const-string v4, "nowplaying"

    invoke-virtual {v2, v4}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v4

    if-nez v4, :cond_1

    .line 412
    invoke-virtual {v2}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v3

    .line 413
    .local v3, "txn":Landroid/support/v4/app/FragmentTransaction;
    iget-object v4, p0, Lcom/google/android/music/ui/BaseActivity;->mNowPlayingFragment:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    if-nez v4, :cond_0

    .line 414
    new-instance v4, Lcom/google/android/music/ui/NowPlayingScreenFragment;

    invoke-direct {v4}, Lcom/google/android/music/ui/NowPlayingScreenFragment;-><init>()V

    iput-object v4, p0, Lcom/google/android/music/ui/BaseActivity;->mNowPlayingFragment:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    .line 416
    :cond_0
    iget-object v4, p0, Lcom/google/android/music/ui/BaseActivity;->mNowPlayingFragment:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    const-string v5, "nowplaying"

    invoke-virtual {v3, v4, v5}, Landroid/support/v4/app/FragmentTransaction;->add(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 417
    invoke-virtual {v3}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 420
    .end local v3    # "txn":Landroid/support/v4/app/FragmentTransaction;
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->getBottomDrawer()Lcom/google/android/music/widgets/ExpandingScrollView;

    move-result-object v1

    .line 423
    .local v1, "drawer":Lcom/google/android/music/widgets/ExpandingScrollView;
    invoke-virtual {v1}, Lcom/google/android/music/widgets/ExpandingScrollView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 424
    new-instance v4, Lcom/google/android/music/ui/BaseActivity$5;

    invoke-direct {v4, p0}, Lcom/google/android/music/ui/BaseActivity$5;-><init>(Lcom/google/android/music/ui/BaseActivity;)V

    invoke-virtual {v1, v4}, Lcom/google/android/music/widgets/ExpandingScrollView;->addListener(Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingScrollViewListener;)V

    .line 488
    .end local v1    # "drawer":Lcom/google/android/music/widgets/ExpandingScrollView;
    .end local v2    # "fm":Landroid/support/v4/app/FragmentManager;
    :goto_0
    return-void

    .line 484
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->getContentContainer()Landroid/view/ViewGroup;

    move-result-object v0

    .line 485
    .local v0, "contentContainer":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getPaddingLeft()I

    move-result v4

    invoke-virtual {v0}, Landroid/view/View;->getPaddingTop()I

    move-result v5

    invoke-virtual {v0}, Landroid/view/View;->getPaddingRight()I

    move-result v6

    invoke-virtual {v0, v4, v5, v6, v7}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_0
.end method

.method protected setupRootMenu(Landroid/widget/ListView;)V
    .locals 1
    .param p1, "listView"    # Landroid/widget/ListView;

    .prologue
    .line 593
    iget-object v0, p0, Lcom/google/android/music/ui/BaseActivity;->mPlayDrawerLayout:Lcom/google/android/play/drawer/PlayDrawerLayout;

    if-eqz v0, :cond_0

    .line 594
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->updateMusicDrawer()V

    .line 599
    :goto_0
    return-void

    .line 596
    :cond_0
    invoke-static {p1, p0}, Lcom/google/android/music/ui/TvHomeMenu;->configureListView(Landroid/widget/ListView;Lcom/google/android/music/ui/BaseActivity;)Lcom/google/android/music/ui/TvHomeMenu$HomeMenuAdapter;

    .line 597
    iput-object p1, p0, Lcom/google/android/music/ui/BaseActivity;->mTVMenuView:Landroid/widget/ListView;

    goto :goto_0
.end method

.method public showActionBar()V
    .locals 1

    .prologue
    .line 1242
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/ActionBar;->show()V

    .line 1243
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/ui/BaseActivity;->mActionBarHiddenFromNowPlaying:Z

    .line 1244
    invoke-static {}, Lcom/google/android/music/preferences/MusicPreferences;->isLmpOrGreater()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1245
    invoke-direct {p0}, Lcom/google/android/music/ui/BaseActivity;->setStatusBarColor()V

    .line 1247
    :cond_0
    return-void
.end method

.method protected showDrawerIfRequested()V
    .locals 1

    .prologue
    .line 662
    iget-boolean v0, p0, Lcom/google/android/music/ui/BaseActivity;->mDrawerRestoredStateOpen:Z

    if-eqz v0, :cond_0

    .line 663
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->openSideDrawer()V

    .line 665
    :cond_0
    return-void
.end method

.method protected showNowPlayingBar()Z
    .locals 1

    .prologue
    .line 324
    const/4 v0, 0x1

    return v0
.end method

.method protected turnOffDownloadedOnlyMode()V
    .locals 2

    .prologue
    .line 586
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->setDisplayOptions(I)V

    .line 587
    return-void
.end method

.method protected updateMusicDrawer()V
    .locals 2

    .prologue
    .line 602
    invoke-static {p0}, Lcom/google/android/music/ui/HomeActivity$Screen;->getActiveScreen(Lcom/google/android/music/ui/BaseActivity;)Lcom/google/android/music/ui/HomeActivity$Screen;

    move-result-object v0

    .line 603
    .local v0, "newScreen":Lcom/google/android/music/ui/HomeActivity$Screen;
    iget-object v1, p0, Lcom/google/android/music/ui/BaseActivity;->mPlayDrawerLayout:Lcom/google/android/play/drawer/PlayDrawerLayout;

    if-eqz v1, :cond_0

    .line 604
    iget-object v1, p0, Lcom/google/android/music/ui/BaseActivity;->mPlayDrawerLayout:Lcom/google/android/play/drawer/PlayDrawerLayout;

    invoke-static {v1, p0, v0}, Lcom/google/android/music/ui/HomeMenuScreens;->configureDrawer(Lcom/google/android/play/drawer/PlayDrawerLayout;Lcom/google/android/music/ui/BaseActivity;Lcom/google/android/music/ui/HomeActivity$Screen;)V

    .line 606
    :cond_0
    return-void
.end method

.method protected useActionBarHamburger()Z
    .locals 1

    .prologue
    .line 333
    const/4 v0, 0x0

    return v0
.end method

.method protected usesActionBarOverlay()Z
    .locals 1

    .prologue
    .line 308
    const/4 v0, 0x0

    return v0
.end method
