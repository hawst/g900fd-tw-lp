.class Lcom/google/android/music/MusicUserContentNotifier$3;
.super Ljava/lang/Object;
.source "MusicUserContentNotifier.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/MusicUserContentNotifier;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/MusicUserContentNotifier;


# direct methods
.method constructor <init>(Lcom/google/android/music/MusicUserContentNotifier;)V
    .locals 0

    .prologue
    .line 72
    iput-object p1, p0, Lcom/google/android/music/MusicUserContentNotifier$3;->this$0:Lcom/google/android/music/MusicUserContentNotifier;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 76
    iget-object v2, p0, Lcom/google/android/music/MusicUserContentNotifier$3;->this$0:Lcom/google/android/music/MusicUserContentNotifier;

    # getter for: Lcom/google/android/music/MusicUserContentNotifier;->mPreferenceListenerRegistered:Z
    invoke-static {v2}, Lcom/google/android/music/MusicUserContentNotifier;->access$100(Lcom/google/android/music/MusicUserContentNotifier;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 77
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    .line 78
    .local v1, "refObject":Ljava/lang/Object;
    iget-object v2, p0, Lcom/google/android/music/MusicUserContentNotifier$3;->this$0:Lcom/google/android/music/MusicUserContentNotifier;

    # getter for: Lcom/google/android/music/MusicUserContentNotifier;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/google/android/music/MusicUserContentNotifier;->access$200(Lcom/google/android/music/MusicUserContentNotifier;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    .line 80
    .local v0, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    :try_start_0
    iget-object v2, p0, Lcom/google/android/music/MusicUserContentNotifier$3;->this$0:Lcom/google/android/music/MusicUserContentNotifier;

    # getter for: Lcom/google/android/music/MusicUserContentNotifier;->mPreferenceChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;
    invoke-static {v2}, Lcom/google/android/music/MusicUserContentNotifier;->access$300(Lcom/google/android/music/MusicUserContentNotifier;)Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/music/preferences/MusicPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 81
    iget-object v2, p0, Lcom/google/android/music/MusicUserContentNotifier$3;->this$0:Lcom/google/android/music/MusicUserContentNotifier;

    const/4 v3, 0x0

    # setter for: Lcom/google/android/music/MusicUserContentNotifier;->mPreferenceListenerRegistered:Z
    invoke-static {v2, v3}, Lcom/google/android/music/MusicUserContentNotifier;->access$102(Lcom/google/android/music/MusicUserContentNotifier;Z)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 83
    invoke-static {v1}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    .line 86
    .end local v0    # "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    .end local v1    # "refObject":Ljava/lang/Object;
    :cond_0
    iget-object v2, p0, Lcom/google/android/music/MusicUserContentNotifier$3;->this$0:Lcom/google/android/music/MusicUserContentNotifier;

    # getter for: Lcom/google/android/music/MusicUserContentNotifier;->mConfigListenerRegistered:Z
    invoke-static {v2}, Lcom/google/android/music/MusicUserContentNotifier;->access$400(Lcom/google/android/music/MusicUserContentNotifier;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 87
    iget-object v2, p0, Lcom/google/android/music/MusicUserContentNotifier$3;->this$0:Lcom/google/android/music/MusicUserContentNotifier;

    # getter for: Lcom/google/android/music/MusicUserContentNotifier;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/google/android/music/MusicUserContentNotifier;->access$200(Lcom/google/android/music/MusicUserContentNotifier;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/music/MusicUserContentNotifier$3;->this$0:Lcom/google/android/music/MusicUserContentNotifier;

    # getter for: Lcom/google/android/music/MusicUserContentNotifier;->mConfigObserver:Landroid/database/ContentObserver;
    invoke-static {v3}, Lcom/google/android/music/MusicUserContentNotifier;->access$500(Lcom/google/android/music/MusicUserContentNotifier;)Landroid/database/ContentObserver;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 88
    iget-object v2, p0, Lcom/google/android/music/MusicUserContentNotifier$3;->this$0:Lcom/google/android/music/MusicUserContentNotifier;

    # setter for: Lcom/google/android/music/MusicUserContentNotifier;->mConfigListenerRegistered:Z
    invoke-static {v2, v4}, Lcom/google/android/music/MusicUserContentNotifier;->access$402(Lcom/google/android/music/MusicUserContentNotifier;Z)Z

    .line 90
    :cond_1
    return-void

    .line 83
    .restart local v0    # "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    .restart local v1    # "refObject":Ljava/lang/Object;
    :catchall_0
    move-exception v2

    invoke-static {v1}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    throw v2
.end method
