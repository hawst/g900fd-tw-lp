.class Lcom/google/android/music/ui/HomeActivity$4;
.super Ljava/lang/Object;
.source "HomeActivity.java"

# interfaces
.implements Lcom/google/android/music/utils/async/AsyncRunner;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/HomeActivity;->updateHasAudioAndSyncState()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mHasAudio:Z

.field private mSyncPendingOrActive:Z

.field final synthetic this$0:Lcom/google/android/music/ui/HomeActivity;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/HomeActivity;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 569
    iput-object p1, p0, Lcom/google/android/music/ui/HomeActivity$4;->this$0:Lcom/google/android/music/ui/HomeActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 570
    iput-boolean v0, p0, Lcom/google/android/music/ui/HomeActivity$4;->mHasAudio:Z

    .line 571
    iput-boolean v0, p0, Lcom/google/android/music/ui/HomeActivity$4;->mSyncPendingOrActive:Z

    return-void
.end method


# virtual methods
.method public backgroundTask()V
    .locals 2

    .prologue
    .line 575
    iget-object v1, p0, Lcom/google/android/music/ui/HomeActivity$4;->this$0:Lcom/google/android/music/ui/HomeActivity;

    invoke-virtual {v1}, Lcom/google/android/music/ui/HomeActivity;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/preferences/MusicPreferences;->getStreamingAccount()Landroid/accounts/Account;

    move-result-object v0

    .line 576
    .local v0, "account":Landroid/accounts/Account;
    iget-object v1, p0, Lcom/google/android/music/ui/HomeActivity$4;->this$0:Lcom/google/android/music/ui/HomeActivity;

    invoke-virtual {v1}, Lcom/google/android/music/ui/HomeActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/music/store/MusicContent$XAudio;->hasAudio(Landroid/content/Context;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/music/ui/HomeActivity$4;->mHasAudio:Z

    .line 577
    if-eqz v0, :cond_1

    const-string v1, "com.google.android.music.MusicContent"

    invoke-static {v0, v1}, Landroid/content/ContentResolver;->isSyncActive(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "com.google.android.music.MusicContent"

    invoke-static {v0, v1}, Landroid/content/ContentResolver;->isSyncPending(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    iput-boolean v1, p0, Lcom/google/android/music/ui/HomeActivity$4;->mSyncPendingOrActive:Z

    .line 580
    return-void

    .line 577
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public taskCompleted()V
    .locals 3

    .prologue
    .line 585
    iget-object v1, p0, Lcom/google/android/music/ui/HomeActivity$4;->this$0:Lcom/google/android/music/ui/HomeActivity;

    invoke-virtual {v1}, Lcom/google/android/music/ui/HomeActivity;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/music/ui/HomeActivity$4;->this$0:Lcom/google/android/music/ui/HomeActivity;

    invoke-virtual {v1}, Lcom/google/android/music/ui/HomeActivity;->isActivityDestroyed()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 601
    :cond_0
    :goto_0
    return-void

    .line 586
    :cond_1
    iget-object v1, p0, Lcom/google/android/music/ui/HomeActivity$4;->this$0:Lcom/google/android/music/ui/HomeActivity;

    invoke-virtual {v1}, Lcom/google/android/music/ui/HomeActivity;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/preferences/MusicPreferences;->getShowSyncNotification()Z

    move-result v0

    .line 587
    .local v0, "showNotification":Z
    iget-boolean v1, p0, Lcom/google/android/music/ui/HomeActivity$4;->mHasAudio:Z

    if-nez v1, :cond_4

    .line 588
    iget-boolean v1, p0, Lcom/google/android/music/ui/HomeActivity$4;->mSyncPendingOrActive:Z

    if-eqz v1, :cond_2

    if-eqz v0, :cond_2

    .line 589
    iget-object v1, p0, Lcom/google/android/music/ui/HomeActivity$4;->this$0:Lcom/google/android/music/ui/HomeActivity;

    sget-object v2, Lcom/google/android/music/ui/HomeActivity$Screen;->SYNCING:Lcom/google/android/music/ui/HomeActivity$Screen;

    # invokes: Lcom/google/android/music/ui/HomeActivity;->showSingleFragmentScreen(Lcom/google/android/music/ui/HomeActivity$Screen;)V
    invoke-static {v1, v2}, Lcom/google/android/music/ui/HomeActivity;->access$400(Lcom/google/android/music/ui/HomeActivity;Lcom/google/android/music/ui/HomeActivity$Screen;)V

    goto :goto_0

    .line 592
    :cond_2
    iget-object v1, p0, Lcom/google/android/music/ui/HomeActivity$4;->this$0:Lcom/google/android/music/ui/HomeActivity;

    # getter for: Lcom/google/android/music/ui/HomeActivity;->mNautilusStatus:Lcom/google/android/music/NautilusStatus;
    invoke-static {v1}, Lcom/google/android/music/ui/HomeActivity;->access$500(Lcom/google/android/music/ui/HomeActivity;)Lcom/google/android/music/NautilusStatus;

    move-result-object v1

    sget-object v2, Lcom/google/android/music/NautilusStatus;->GOT_NAUTILUS:Lcom/google/android/music/NautilusStatus;

    if-eq v1, v2, :cond_3

    .line 593
    iget-object v1, p0, Lcom/google/android/music/ui/HomeActivity$4;->this$0:Lcom/google/android/music/ui/HomeActivity;

    sget-object v2, Lcom/google/android/music/ui/HomeActivity$Screen;->NO_CONTENT:Lcom/google/android/music/ui/HomeActivity$Screen;

    # invokes: Lcom/google/android/music/ui/HomeActivity;->showSingleFragmentScreen(Lcom/google/android/music/ui/HomeActivity$Screen;)V
    invoke-static {v1, v2}, Lcom/google/android/music/ui/HomeActivity;->access$400(Lcom/google/android/music/ui/HomeActivity;Lcom/google/android/music/ui/HomeActivity$Screen;)V

    goto :goto_0

    .line 595
    :cond_3
    iget-object v1, p0, Lcom/google/android/music/ui/HomeActivity$4;->this$0:Lcom/google/android/music/ui/HomeActivity;

    iget-object v2, p0, Lcom/google/android/music/ui/HomeActivity$4;->this$0:Lcom/google/android/music/ui/HomeActivity;

    # getter for: Lcom/google/android/music/ui/HomeActivity;->mTargetScreen:Lcom/google/android/music/ui/HomeActivity$Screen;
    invoke-static {v2}, Lcom/google/android/music/ui/HomeActivity;->access$600(Lcom/google/android/music/ui/HomeActivity;)Lcom/google/android/music/ui/HomeActivity$Screen;

    move-result-object v2

    # invokes: Lcom/google/android/music/ui/HomeActivity;->showSingleFragmentScreen(Lcom/google/android/music/ui/HomeActivity$Screen;)V
    invoke-static {v1, v2}, Lcom/google/android/music/ui/HomeActivity;->access$400(Lcom/google/android/music/ui/HomeActivity;Lcom/google/android/music/ui/HomeActivity$Screen;)V

    goto :goto_0

    .line 599
    :cond_4
    iget-object v1, p0, Lcom/google/android/music/ui/HomeActivity$4;->this$0:Lcom/google/android/music/ui/HomeActivity;

    iget-object v2, p0, Lcom/google/android/music/ui/HomeActivity$4;->this$0:Lcom/google/android/music/ui/HomeActivity;

    # getter for: Lcom/google/android/music/ui/HomeActivity;->mTargetScreen:Lcom/google/android/music/ui/HomeActivity$Screen;
    invoke-static {v2}, Lcom/google/android/music/ui/HomeActivity;->access$600(Lcom/google/android/music/ui/HomeActivity;)Lcom/google/android/music/ui/HomeActivity$Screen;

    move-result-object v2

    # invokes: Lcom/google/android/music/ui/HomeActivity;->showSingleFragmentScreen(Lcom/google/android/music/ui/HomeActivity$Screen;)V
    invoke-static {v1, v2}, Lcom/google/android/music/ui/HomeActivity;->access$400(Lcom/google/android/music/ui/HomeActivity;Lcom/google/android/music/ui/HomeActivity$Screen;)V

    goto :goto_0
.end method
