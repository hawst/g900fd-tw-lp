.class public Lcom/google/android/music/download/cp/CpOutputStream;
.super Ljava/lang/Object;
.source "CpOutputStream.java"

# interfaces
.implements Lcom/google/android/music/io/ChunkedOutputStream;


# instance fields
.field private final mBuffer:[B

.field private final mCipher:Ljavax/crypto/Cipher;

.field private final mIvBuffer:[B

.field private final mSecretKeySpec:Ljavax/crypto/spec/SecretKeySpec;

.field private final mSink:Ljava/io/OutputStream;

.field private mWroteShortBlock:Z


# direct methods
.method public constructor <init>(Ljava/io/OutputStream;[B)V
    .locals 3
    .param p1, "sink"    # Ljava/io/OutputStream;
    .param p2, "secretKey"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v2, 0x10

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    array-length v0, p2

    if-eq v0, v2, :cond_0

    .line 26
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "secretKey length must be 16"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 29
    :cond_0
    iput-object p1, p0, Lcom/google/android/music/download/cp/CpOutputStream;->mSink:Ljava/io/OutputStream;

    .line 30
    invoke-static {}, Lcom/google/android/music/download/cp/CpUtils;->getCipher()Ljavax/crypto/Cipher;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/download/cp/CpOutputStream;->mCipher:Ljavax/crypto/Cipher;

    .line 32
    new-instance v0, Ljavax/crypto/spec/SecretKeySpec;

    const-string v1, "AES"

    invoke-direct {v0, p2, v1}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/music/download/cp/CpOutputStream;->mSecretKeySpec:Ljavax/crypto/spec/SecretKeySpec;

    .line 33
    const/16 v0, 0x400

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/google/android/music/download/cp/CpOutputStream;->mBuffer:[B

    .line 34
    new-array v0, v2, [B

    iput-object v0, p0, Lcom/google/android/music/download/cp/CpOutputStream;->mIvBuffer:[B

    .line 35
    invoke-static {}, Lcom/google/android/music/download/cp/CpUtils;->getRandom()Ljava/security/SecureRandom;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/download/cp/CpOutputStream;->mIvBuffer:[B

    invoke-virtual {v0, v1}, Ljava/security/SecureRandom;->nextBytes([B)V

    .line 36
    invoke-static {p1}, Lcom/google/android/music/download/cp/CpUtils;->writeMagicNumber(Ljava/io/OutputStream;)V

    .line 37
    return-void
.end method

.method private updateIv()V
    .locals 3

    .prologue
    .line 82
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v1, 0x10

    if-ge v0, v1, :cond_0

    .line 83
    iget-object v1, p0, Lcom/google/android/music/download/cp/CpOutputStream;->mIvBuffer:[B

    aget-byte v2, v1, v0

    add-int/lit8 v2, v2, 0x1

    int-to-byte v2, v2

    aput-byte v2, v1, v0

    if-eqz v2, :cond_1

    .line 87
    :cond_0
    return-void

    .line 82
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/music/download/cp/CpOutputStream;->mSink:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V

    .line 47
    return-void
.end method

.method public flush()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/music/download/cp/CpOutputStream;->mSink:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V

    .line 52
    return-void
.end method

.method public getChunkSize()I
    .locals 1

    .prologue
    .line 41
    const/16 v0, 0x3f0

    return v0
.end method

.method public write([BII)V
    .locals 11
    .param p1, "buffer"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 56
    iget-boolean v0, p0, Lcom/google/android/music/download/cp/CpOutputStream;->mWroteShortBlock:Z

    if-eqz v0, :cond_0

    .line 57
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Already wrote short block."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 59
    :cond_0
    if-lez p3, :cond_1

    invoke-virtual {p0}, Lcom/google/android/music/download/cp/CpOutputStream;->getChunkSize()I

    move-result v0

    if-le p3, v0, :cond_2

    .line 60
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "length out of range 0 < length <= chunkSize: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 63
    :cond_2
    new-instance v8, Ljavax/crypto/spec/IvParameterSpec;

    iget-object v0, p0, Lcom/google/android/music/download/cp/CpOutputStream;->mIvBuffer:[B

    invoke-direct {v8, v0}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    .line 65
    .local v8, "ivParameterSpec":Ljavax/crypto/spec/IvParameterSpec;
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/download/cp/CpOutputStream;->mCipher:Ljavax/crypto/Cipher;

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/music/download/cp/CpOutputStream;->mSecretKeySpec:Ljavax/crypto/spec/SecretKeySpec;

    invoke-virtual {v0, v1, v2, v8}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V

    .line 66
    iget-object v0, p0, Lcom/google/android/music/download/cp/CpOutputStream;->mIvBuffer:[B

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/music/download/cp/CpOutputStream;->mBuffer:[B

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/music/download/cp/CpOutputStream;->mIvBuffer:[B

    array-length v4, v4

    invoke-static {v0, v1, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 67
    iget-object v0, p0, Lcom/google/android/music/download/cp/CpOutputStream;->mCipher:Ljavax/crypto/Cipher;

    iget-object v4, p0, Lcom/google/android/music/download/cp/CpOutputStream;->mBuffer:[B

    const/16 v5, 0x10

    move-object v1, p1

    move v2, p2

    move v3, p3

    invoke-virtual/range {v0 .. v5}, Ljavax/crypto/Cipher;->doFinal([BII[BI)I

    move-result v6

    .line 69
    .local v6, "bytesWritten":I
    if-eq v6, p3, :cond_3

    .line 70
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Bad update length"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 72
    .end local v6    # "bytesWritten":I
    :catch_0
    move-exception v7

    .line 73
    .local v7, "e":Ljava/lang/Exception;
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Problem with cipher"

    invoke-direct {v0, v1, v7}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    .line 75
    .end local v7    # "e":Ljava/lang/Exception;
    .restart local v6    # "bytesWritten":I
    :cond_3
    iget-object v0, p0, Lcom/google/android/music/download/cp/CpOutputStream;->mSink:Ljava/io/OutputStream;

    iget-object v1, p0, Lcom/google/android/music/download/cp/CpOutputStream;->mBuffer:[B

    add-int/lit8 v2, p3, 0x10

    invoke-virtual {v0, v1, v10, v2}, Ljava/io/OutputStream;->write([BII)V

    .line 76
    invoke-virtual {p0}, Lcom/google/android/music/download/cp/CpOutputStream;->getChunkSize()I

    move-result v0

    if-ge p3, v0, :cond_4

    move v0, v9

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/music/download/cp/CpOutputStream;->mWroteShortBlock:Z

    .line 77
    invoke-direct {p0}, Lcom/google/android/music/download/cp/CpOutputStream;->updateIv()V

    .line 78
    return-void

    :cond_4
    move v0, v10

    .line 76
    goto :goto_0
.end method
