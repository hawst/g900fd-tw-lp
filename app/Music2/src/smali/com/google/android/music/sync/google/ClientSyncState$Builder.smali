.class public Lcom/google/android/music/sync/google/ClientSyncState$Builder;
.super Ljava/lang/Object;
.source "ClientSyncState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/sync/google/ClientSyncState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private mEtagPlaylist:Ljava/lang/String;

.field private mEtagPlaylistEntry:Ljava/lang/String;

.field private mEtagTrack:Ljava/lang/String;

.field private mRemoteAccount:Ljava/lang/Integer;

.field private mRemoteBlacklistItemVersion:Ljava/lang/Long;

.field private mRemotePlaylistVersion:Ljava/lang/Long;

.field private mRemotePlentryVersion:Ljava/lang/Long;

.field private mRemoteRadioStationVersion:Ljava/lang/Long;

.field private mRemoteTrackVersion:Ljava/lang/Long;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/music/sync/google/ClientSyncState$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/music/sync/google/ClientSyncState$1;

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/google/android/music/sync/google/ClientSyncState$Builder;-><init>()V

    return-void
.end method

.method private constructor <init>(Lcom/google/android/music/sync/google/ClientSyncState;)V
    .locals 1
    .param p1, "clone"    # Lcom/google/android/music/sync/google/ClientSyncState;

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    iget-object v0, p1, Lcom/google/android/music/sync/google/ClientSyncState;->mRemoteTrackVersion:Ljava/lang/Long;

    iput-object v0, p0, Lcom/google/android/music/sync/google/ClientSyncState$Builder;->mRemoteTrackVersion:Ljava/lang/Long;

    .line 75
    iget-object v0, p1, Lcom/google/android/music/sync/google/ClientSyncState;->mRemotePlaylistVersion:Ljava/lang/Long;

    iput-object v0, p0, Lcom/google/android/music/sync/google/ClientSyncState$Builder;->mRemotePlaylistVersion:Ljava/lang/Long;

    .line 76
    iget-object v0, p1, Lcom/google/android/music/sync/google/ClientSyncState;->mRemotePlentryVersion:Ljava/lang/Long;

    iput-object v0, p0, Lcom/google/android/music/sync/google/ClientSyncState$Builder;->mRemotePlentryVersion:Ljava/lang/Long;

    .line 77
    iget-object v0, p1, Lcom/google/android/music/sync/google/ClientSyncState;->mRemoteAccount:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/android/music/sync/google/ClientSyncState$Builder;->mRemoteAccount:Ljava/lang/Integer;

    .line 78
    iget-object v0, p1, Lcom/google/android/music/sync/google/ClientSyncState;->mEtagPlaylist:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/music/sync/google/ClientSyncState$Builder;->mEtagPlaylist:Ljava/lang/String;

    .line 79
    iget-object v0, p1, Lcom/google/android/music/sync/google/ClientSyncState;->mEtagPlaylistEntry:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/music/sync/google/ClientSyncState$Builder;->mEtagPlaylistEntry:Ljava/lang/String;

    .line 80
    iget-object v0, p1, Lcom/google/android/music/sync/google/ClientSyncState;->mEtagTrack:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/music/sync/google/ClientSyncState$Builder;->mEtagTrack:Ljava/lang/String;

    .line 81
    iget-object v0, p1, Lcom/google/android/music/sync/google/ClientSyncState;->mRemoteRadioStationVersion:Ljava/lang/Long;

    iput-object v0, p0, Lcom/google/android/music/sync/google/ClientSyncState$Builder;->mRemoteRadioStationVersion:Ljava/lang/Long;

    .line 82
    iget-object v0, p1, Lcom/google/android/music/sync/google/ClientSyncState;->mRemoteBlacklistItemVersion:Ljava/lang/Long;

    iput-object v0, p0, Lcom/google/android/music/sync/google/ClientSyncState$Builder;->mRemoteBlacklistItemVersion:Ljava/lang/Long;

    .line 83
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/music/sync/google/ClientSyncState;Lcom/google/android/music/sync/google/ClientSyncState$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/music/sync/google/ClientSyncState;
    .param p2, "x1"    # Lcom/google/android/music/sync/google/ClientSyncState$1;

    .prologue
    .line 60
    invoke-direct {p0, p1}, Lcom/google/android/music/sync/google/ClientSyncState$Builder;-><init>(Lcom/google/android/music/sync/google/ClientSyncState;)V

    return-void
.end method


# virtual methods
.method public build()Lcom/google/android/music/sync/google/ClientSyncState;
    .locals 11

    .prologue
    .line 131
    new-instance v0, Lcom/google/android/music/sync/google/ClientSyncState;

    iget-object v1, p0, Lcom/google/android/music/sync/google/ClientSyncState$Builder;->mRemoteAccount:Ljava/lang/Integer;

    iget-object v2, p0, Lcom/google/android/music/sync/google/ClientSyncState$Builder;->mRemoteTrackVersion:Ljava/lang/Long;

    iget-object v3, p0, Lcom/google/android/music/sync/google/ClientSyncState$Builder;->mRemotePlaylistVersion:Ljava/lang/Long;

    iget-object v4, p0, Lcom/google/android/music/sync/google/ClientSyncState$Builder;->mRemotePlentryVersion:Ljava/lang/Long;

    iget-object v5, p0, Lcom/google/android/music/sync/google/ClientSyncState$Builder;->mRemoteRadioStationVersion:Ljava/lang/Long;

    iget-object v6, p0, Lcom/google/android/music/sync/google/ClientSyncState$Builder;->mRemoteBlacklistItemVersion:Ljava/lang/Long;

    iget-object v7, p0, Lcom/google/android/music/sync/google/ClientSyncState$Builder;->mEtagTrack:Ljava/lang/String;

    iget-object v8, p0, Lcom/google/android/music/sync/google/ClientSyncState$Builder;->mEtagPlaylist:Ljava/lang/String;

    iget-object v9, p0, Lcom/google/android/music/sync/google/ClientSyncState$Builder;->mEtagPlaylistEntry:Ljava/lang/String;

    const/4 v10, 0x0

    invoke-direct/range {v0 .. v10}, Lcom/google/android/music/sync/google/ClientSyncState;-><init>(Ljava/lang/Integer;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/music/sync/google/ClientSyncState$1;)V

    return-object v0
.end method

.method public setEtagPlaylist(Ljava/lang/String;)Lcom/google/android/music/sync/google/ClientSyncState$Builder;
    .locals 0
    .param p1, "etagPlaylist"    # Ljava/lang/String;

    .prologue
    .line 121
    iput-object p1, p0, Lcom/google/android/music/sync/google/ClientSyncState$Builder;->mEtagPlaylist:Ljava/lang/String;

    .line 122
    return-object p0
.end method

.method public setEtagPlaylistEntry(Ljava/lang/String;)Lcom/google/android/music/sync/google/ClientSyncState$Builder;
    .locals 0
    .param p1, "etagPlaylistEntry"    # Ljava/lang/String;

    .prologue
    .line 126
    iput-object p1, p0, Lcom/google/android/music/sync/google/ClientSyncState$Builder;->mEtagPlaylistEntry:Ljava/lang/String;

    .line 127
    return-object p0
.end method

.method public setEtagTrack(Ljava/lang/String;)Lcom/google/android/music/sync/google/ClientSyncState$Builder;
    .locals 0
    .param p1, "etagTrack"    # Ljava/lang/String;

    .prologue
    .line 116
    iput-object p1, p0, Lcom/google/android/music/sync/google/ClientSyncState$Builder;->mEtagTrack:Ljava/lang/String;

    .line 117
    return-object p0
.end method

.method public setRemoteAccount(I)Lcom/google/android/music/sync/google/ClientSyncState$Builder;
    .locals 1
    .param p1, "remoteAccount"    # I

    .prologue
    .line 86
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/sync/google/ClientSyncState$Builder;->mRemoteAccount:Ljava/lang/Integer;

    .line 87
    return-object p0
.end method

.method public setRemoteBlacklistItemVersion(J)Lcom/google/android/music/sync/google/ClientSyncState$Builder;
    .locals 1
    .param p1, "remoteVersion"    # J

    .prologue
    .line 111
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/sync/google/ClientSyncState$Builder;->mRemoteBlacklistItemVersion:Ljava/lang/Long;

    .line 112
    return-object p0
.end method

.method public setRemotePlaylistVersion(J)Lcom/google/android/music/sync/google/ClientSyncState$Builder;
    .locals 1
    .param p1, "remoteVersion"    # J

    .prologue
    .line 96
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/sync/google/ClientSyncState$Builder;->mRemotePlaylistVersion:Ljava/lang/Long;

    .line 97
    return-object p0
.end method

.method public setRemotePlentryVersion(J)Lcom/google/android/music/sync/google/ClientSyncState$Builder;
    .locals 1
    .param p1, "remoteVersion"    # J

    .prologue
    .line 101
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/sync/google/ClientSyncState$Builder;->mRemotePlentryVersion:Ljava/lang/Long;

    .line 102
    return-object p0
.end method

.method public setRemoteRadioStationVersion(J)Lcom/google/android/music/sync/google/ClientSyncState$Builder;
    .locals 1
    .param p1, "remoteVersion"    # J

    .prologue
    .line 106
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/sync/google/ClientSyncState$Builder;->mRemoteRadioStationVersion:Ljava/lang/Long;

    .line 107
    return-object p0
.end method

.method public setRemoteTrackVersion(J)Lcom/google/android/music/sync/google/ClientSyncState$Builder;
    .locals 1
    .param p1, "remoteVersion"    # J

    .prologue
    .line 91
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/sync/google/ClientSyncState$Builder;->mRemoteTrackVersion:Ljava/lang/Long;

    .line 92
    return-object p0
.end method
