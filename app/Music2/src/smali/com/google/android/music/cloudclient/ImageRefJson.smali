.class public Lcom/google/android/music/cloudclient/ImageRefJson;
.super Lcom/google/api/client/json/GenericJson;
.source "ImageRefJson.java"


# instance fields
.field public mHeight:I
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "height"
    .end annotation
.end field

.field public mUrl:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "url"
    .end annotation
.end field

.field public mWidth:I
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "width"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    return-void
.end method
