.class public Lcom/google/android/music/mix/PlayQueueFeeder;
.super Lcom/google/android/music/utils/LoggableHandler;
.source "PlayQueueFeeder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/mix/PlayQueueFeeder$2;,
        Lcom/google/android/music/mix/PlayQueueFeeder$MixRequestData;,
        Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;
    }
.end annotation


# instance fields
.field private final LOGV:Z

.field private final mCloudClient:Lcom/google/android/music/cloudclient/MusicCloud;

.field private final mContext:Landroid/content/Context;

.field private final mListener:Lcom/google/android/music/mix/PlayQueueFeederListener;

.field private volatile mMixCancelled:Z

.field private final mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/music/mix/PlayQueueFeederListener;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/google/android/music/mix/PlayQueueFeederListener;

    .prologue
    .line 89
    const-string v0, "PlayQueueFeeder"

    invoke-direct {p0, v0}, Lcom/google/android/music/utils/LoggableHandler;-><init>(Ljava/lang/String;)V

    .line 55
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->PLAYBACK_SERVICE:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/music/mix/PlayQueueFeeder;->LOGV:Z

    .line 80
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/mix/PlayQueueFeeder;->mMixCancelled:Z

    .line 90
    if-nez p1, :cond_0

    .line 91
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Missing context"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 94
    :cond_0
    if-nez p2, :cond_1

    .line 95
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Missing listener"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 97
    :cond_1
    iput-object p1, p0, Lcom/google/android/music/mix/PlayQueueFeeder;->mContext:Landroid/content/Context;

    .line 98
    new-instance v0, Lcom/google/android/music/cloudclient/MusicCloudImpl;

    invoke-direct {v0, p1}, Lcom/google/android/music/cloudclient/MusicCloudImpl;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/music/mix/PlayQueueFeeder;->mCloudClient:Lcom/google/android/music/cloudclient/MusicCloud;

    .line 99
    iput-object p2, p0, Lcom/google/android/music/mix/PlayQueueFeeder;->mListener:Lcom/google/android/music/mix/PlayQueueFeederListener;

    .line 100
    iget-object v0, p0, Lcom/google/android/music/mix/PlayQueueFeeder;->mContext:Landroid/content/Context;

    invoke-static {v0, p0}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/mix/PlayQueueFeeder;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    .line 101
    return-void
.end method

.method private addRadioToRecentAsync(J)V
    .locals 3
    .param p1, "radioId"    # J

    .prologue
    .line 603
    iget-object v0, p0, Lcom/google/android/music/mix/PlayQueueFeeder;->mContext:Landroid/content/Context;

    .line 604
    .local v0, "context":Landroid/content/Context;
    sget-object v1, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    new-instance v2, Lcom/google/android/music/mix/PlayQueueFeeder$1;

    invoke-direct {v2, p0, v0, p1, p2}, Lcom/google/android/music/mix/PlayQueueFeeder$1;-><init>(Lcom/google/android/music/mix/PlayQueueFeeder;Landroid/content/Context;J)V

    invoke-static {v1, v2}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsync(Lcom/google/android/music/utils/LoggableHandler;Ljava/lang/Runnable;)V

    .line 609
    return-void
.end method

.method private createRadioStation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZI)Landroid/util/Pair;
    .locals 24
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "artLocation"    # Ljava/lang/String;
    .param p3, "remoteSeedId"    # Ljava/lang/String;
    .param p4, "seedType"    # I
    .param p5, "includeFeed"    # Z
    .param p6, "maxSize"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "IZI)",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Long;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/sync/google/model/Track;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 533
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/mix/PlayQueueFeeder;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/music/preferences/MusicPreferences;->getContentFilter()I

    move-result v5

    .line 534
    .local v5, "contentFilter":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/mix/PlayQueueFeeder;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/music/preferences/MusicPreferences;->getStreamingAccount()Landroid/accounts/Account;

    move-result-object v4

    .line 535
    .local v4, "account":Landroid/accounts/Account;
    if-nez v4, :cond_0

    .line 536
    const-string v20, "PlayQueueFeeder"

    const-string v21, "Failed to get account"

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 537
    const/16 v20, 0x0

    .line 597
    :goto_0
    return-object v20

    .line 541
    :cond_0
    :try_start_0
    new-instance v17, Lcom/google/android/music/sync/google/model/SyncableRadioStation;

    invoke-direct/range {v17 .. v17}, Lcom/google/android/music/sync/google/model/SyncableRadioStation;-><init>()V

    .line 542
    .local v17, "syncableRadioStation":Lcom/google/android/music/sync/google/model/SyncableRadioStation;
    move-object/from16 v0, p1

    move-object/from16 v1, v17

    iput-object v0, v1, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mName:Ljava/lang/String;

    .line 543
    invoke-static {}, Lcom/google/android/music/store/Store;->generateClientId()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    iput-object v0, v1, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mClientId:Ljava/lang/String;

    .line 544
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v20

    const-wide/16 v22, 0x3e8

    mul-long v20, v20, v22

    move-wide/from16 v0, v20

    move-object/from16 v2, v17

    iput-wide v0, v2, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mRecentTimestampMicrosec:J

    .line 545
    const/16 v20, 0x1

    move/from16 v0, v20

    move-object/from16 v1, v17

    iput v0, v1, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mImageType:I

    .line 546
    invoke-static/range {p2 .. p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v20

    if-nez v20, :cond_1

    .line 547
    new-instance v20, Ljava/util/ArrayList;

    invoke-direct/range {v20 .. v20}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    iput-object v0, v1, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mImageUrls:Ljava/util/List;

    .line 548
    invoke-static/range {p2 .. p2}, Lcom/google/android/music/utils/MusicUtils;->decodeStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v19

    .line 549
    .local v19, "urls":[Ljava/lang/String;
    if-eqz v19, :cond_1

    .line 550
    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->setImageUrls([Ljava/lang/String;)V

    .line 553
    .end local v19    # "urls":[Ljava/lang/String;
    :cond_1
    invoke-static/range {p3 .. p4}, Lcom/google/android/music/sync/google/model/RadioSeed;->createRadioSeed(Ljava/lang/String;I)Lcom/google/android/music/sync/google/model/RadioSeed;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    iput-object v0, v1, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mSeed:Lcom/google/android/music/sync/google/model/RadioSeed;

    .line 555
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/mix/PlayQueueFeeder;->mCloudClient:Lcom/google/android/music/cloudclient/MusicCloud;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    move/from16 v2, p5

    move/from16 v3, p6

    invoke-interface {v0, v1, v2, v3, v5}, Lcom/google/android/music/cloudclient/MusicCloud;->createRadioStation(Lcom/google/android/music/sync/google/model/SyncableRadioStation;ZII)Lcom/google/android/music/cloudclient/RadioEditStationsResponse;

    move-result-object v15

    .line 557
    .local v15, "result":Lcom/google/android/music/cloudclient/RadioEditStationsResponse;
    invoke-static {v15}, Lcom/google/android/music/cloudclient/RadioEditStationsResponse;->validateResponse(Lcom/google/android/music/cloudclient/RadioEditStationsResponse;)Z

    move-result v20

    if-nez v20, :cond_2

    .line 558
    const/16 v20, 0x0

    goto :goto_0

    .line 561
    :cond_2
    iget-object v0, v15, Lcom/google/android/music/cloudclient/RadioEditStationsResponse;->mMutateResponses:Ljava/util/List;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-interface/range {v20 .. v21}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/google/android/music/cloudclient/RadioEditStationsResponse$MutateResponse;

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/google/android/music/cloudclient/RadioEditStationsResponse$MutateResponse;->mRadioStation:Lcom/google/android/music/sync/google/model/SyncableRadioStation;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v8, v0, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mImageUrls:Ljava/util/List;

    .line 563
    .local v8, "imageUrlList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/cloudclient/ImageRefJson;>;"
    if-eqz v8, :cond_4

    .line 564
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v20

    move/from16 v0, v20

    new-array v9, v0, [Ljava/lang/String;

    .line 565
    .local v9, "imageUrls":[Ljava/lang/String;
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .line 566
    .local v10, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/google/android/music/cloudclient/ImageRefJson;>;"
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_1
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v20

    move/from16 v0, v20

    if-ge v7, v0, :cond_3

    .line 567
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/google/android/music/cloudclient/ImageRefJson;

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/google/android/music/cloudclient/ImageRefJson;->mUrl:Ljava/lang/String;

    move-object/from16 v20, v0

    aput-object v20, v9, v7

    .line 566
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 569
    :cond_3
    move-object/from16 v0, v17

    invoke-virtual {v0, v9}, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->setImageUrls([Ljava/lang/String;)V

    .line 572
    .end local v7    # "i":I
    .end local v9    # "imageUrls":[Ljava/lang/String;
    .end local v10    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/google/android/music/cloudclient/ImageRefJson;>;"
    :cond_4
    iget-object v0, v15, Lcom/google/android/music/cloudclient/RadioEditStationsResponse;->mMutateResponses:Ljava/util/List;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-interface/range {v20 .. v21}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/google/android/music/cloudclient/RadioEditStationsResponse$MutateResponse;

    move-object/from16 v0, v20

    iget-object v14, v0, Lcom/google/android/music/cloudclient/RadioEditStationsResponse$MutateResponse;->mId:Ljava/lang/String;

    .line 573
    .local v14, "remoteRadioId":Ljava/lang/String;
    iget-object v0, v15, Lcom/google/android/music/cloudclient/RadioEditStationsResponse;->mMutateResponses:Ljava/util/List;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-interface/range {v20 .. v21}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/google/android/music/cloudclient/RadioEditStationsResponse$MutateResponse;

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/google/android/music/cloudclient/RadioEditStationsResponse$MutateResponse;->mRadioStation:Lcom/google/android/music/sync/google/model/SyncableRadioStation;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mTracks:Ljava/util/List;

    move-object/from16 v18, v0

    .line 575
    .local v18, "tracks":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/sync/google/model/Track;>;"
    const/16 v20, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->formatAsRadioStation(Lcom/google/android/music/store/RadioStation;)Lcom/google/android/music/store/RadioStation;

    move-result-object v11

    .line 576
    .local v11, "radioStation":Lcom/google/android/music/store/RadioStation;
    invoke-virtual {v11, v14}, Lcom/google/android/music/store/RadioStation;->setSourceId(Ljava/lang/String;)V

    .line 577
    invoke-static {v4}, Lcom/google/android/music/store/Store;->computeAccountHash(Landroid/accounts/Account;)I

    move-result v20

    move/from16 v0, v20

    invoke-virtual {v11, v0}, Lcom/google/android/music/store/RadioStation;->setSourceAccount(I)V

    .line 578
    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-virtual {v11, v0}, Lcom/google/android/music/store/RadioStation;->setNeedsSync(Z)V

    .line 579
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/mix/PlayQueueFeeder;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v16

    .line 580
    .local v16, "store":Lcom/google/android/music/store/Store;
    move-object/from16 v0, v16

    invoke-virtual {v0, v11}, Lcom/google/android/music/store/Store;->saveOrTransitionRadioStation(Lcom/google/android/music/store/RadioStation;)J

    move-result-wide v12

    .line 581
    .local v12, "localRadioId":J
    const-wide/16 v20, -0x1

    cmp-long v20, v12, v20

    if-nez v20, :cond_5

    .line 584
    move-object/from16 v0, v16

    move-object/from16 v1, p3

    move/from16 v2, p4

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/Store;->getRadioLocalIdBySeedAndType(Ljava/lang/String;I)J

    move-result-wide v12

    .line 586
    :cond_5
    const-wide/16 v20, -0x1

    cmp-long v20, v12, v20

    if-nez v20, :cond_6

    .line 587
    const/16 v20, 0x0

    goto/16 :goto_0

    .line 589
    :cond_6
    new-instance v20, Landroid/util/Pair;

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v21

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    goto/16 :goto_0

    .line 591
    .end local v8    # "imageUrlList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/cloudclient/ImageRefJson;>;"
    .end local v11    # "radioStation":Lcom/google/android/music/store/RadioStation;
    .end local v12    # "localRadioId":J
    .end local v14    # "remoteRadioId":Ljava/lang/String;
    .end local v15    # "result":Lcom/google/android/music/cloudclient/RadioEditStationsResponse;
    .end local v16    # "store":Lcom/google/android/music/store/Store;
    .end local v17    # "syncableRadioStation":Lcom/google/android/music/sync/google/model/SyncableRadioStation;
    .end local v18    # "tracks":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/sync/google/model/Track;>;"
    :catch_0
    move-exception v6

    .line 592
    .local v6, "e":Ljava/io/IOException;
    const-string v20, "PlayQueueFeeder"

    const-string v21, "Failed to create radio station: "

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-static {v0, v1, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 593
    const/16 v20, 0x0

    goto/16 :goto_0

    .line 594
    .end local v6    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v6

    .line 595
    .local v6, "e":Ljava/lang/InterruptedException;
    const-string v20, "PlayQueueFeeder"

    const-string v21, "Interrupted while creating radio station"

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 597
    const/16 v20, 0x0

    goto/16 :goto_0
.end method

.method private getCachedRadioSongs(JZ)Lcom/google/android/music/medialist/SongList;
    .locals 7
    .param p1, "localRadioId"    # J
    .param p3, "allowUsedCachedSongs"    # Z

    .prologue
    .line 400
    const/4 v3, 0x0

    .line 402
    .local v3, "songList":Lcom/google/android/music/medialist/SongList;
    iget-object v4, p0, Lcom/google/android/music/mix/PlayQueueFeeder;->mContext:Landroid/content/Context;

    invoke-static {v4, p1, p2, p3}, Lcom/google/android/music/store/KeepOnRadioUtils;->useCachedSongs(Landroid/content/Context;JZ)Ljava/util/List;

    move-result-object v0

    .line 404
    .local v0, "cachedSongs":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 405
    const-string v4, ""

    invoke-static {p1, p2, v4}, Lcom/google/android/music/store/ContainerDescriptor;->newRadioDescriptor(JLjava/lang/String;)Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v1

    .line 407
    .local v1, "container":Lcom/google/android/music/store/ContainerDescriptor;
    new-instance v3, Lcom/google/android/music/medialist/SelectedSongList;

    .end local v3    # "songList":Lcom/google/android/music/medialist/SongList;
    invoke-static {v0}, Lcom/google/common/primitives/Longs;->toArray(Ljava/util/Collection;)[J

    move-result-object v4

    invoke-direct {v3, v1, v4}, Lcom/google/android/music/medialist/SelectedSongList;-><init>(Lcom/google/android/music/store/ContainerDescriptor;[J)V

    .line 412
    .restart local v3    # "songList":Lcom/google/android/music/medialist/SongList;
    const-wide/16 v4, 0x3e8

    :try_start_0
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 417
    .end local v1    # "container":Lcom/google/android/music/store/ContainerDescriptor;
    :cond_0
    :goto_0
    return-object v3

    .line 413
    .restart local v1    # "container":Lcom/google/android/music/store/ContainerDescriptor;
    :catch_0
    move-exception v2

    .line 414
    .local v2, "e":Ljava/lang/InterruptedException;
    const-string v4, "PlayQueueFeeder"

    const-string v5, "Interrupted while getting the cached songs"

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private static getMaxMixSize(Landroid/content/ContentResolver;)I
    .locals 2
    .param p0, "resolver"    # Landroid/content/ContentResolver;

    .prologue
    .line 612
    const-string v0, "music_radio_feed_size"

    const/16 v1, 0x19

    invoke-static {p0, v0, v1}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method private getTracksFromServer(Lcom/google/android/music/mix/MixDescriptor;Lcom/google/android/music/store/Store;ILjava/util/List;J)Ljava/util/List;
    .locals 7
    .param p1, "mix"    # Lcom/google/android/music/mix/MixDescriptor;
    .param p2, "store"    # Lcom/google/android/music/store/Store;
    .param p3, "maxSize"    # I
    .param p5, "localRadioId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/music/mix/MixDescriptor;",
            "Lcom/google/android/music/store/Store;",
            "I",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/cloudclient/MixTrackId;",
            ">;J)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/sync/google/model/Track;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;,
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 360
    .local p4, "recentlyPlayedTrackIds":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/cloudclient/MixTrackId;>;"
    const/4 v3, 0x0

    .line 362
    .local v3, "tracks":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/sync/google/model/Track;>;"
    iget-object v4, p0, Lcom/google/android/music/mix/PlayQueueFeeder;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    invoke-virtual {v4}, Lcom/google/android/music/preferences/MusicPreferences;->getContentFilter()I

    move-result v0

    .line 364
    .local v0, "contentFilter":I
    invoke-virtual {p1}, Lcom/google/android/music/mix/MixDescriptor;->getType()Lcom/google/android/music/mix/MixDescriptor$Type;

    move-result-object v4

    sget-object v5, Lcom/google/android/music/mix/MixDescriptor$Type;->LUCKY_RADIO:Lcom/google/android/music/mix/MixDescriptor$Type;

    if-ne v4, v5, :cond_2

    .line 365
    iget-object v4, p0, Lcom/google/android/music/mix/PlayQueueFeeder;->mCloudClient:Lcom/google/android/music/cloudclient/MusicCloud;

    invoke-interface {v4, p3, p4, v0}, Lcom/google/android/music/cloudclient/MusicCloud;->getLuckyRadioFeed(ILjava/util/List;I)Lcom/google/android/music/cloudclient/RadioFeedResponse;

    move-result-object v2

    .line 373
    .local v2, "result":Lcom/google/android/music/cloudclient/RadioFeedResponse;
    :goto_0
    iget-boolean v4, p0, Lcom/google/android/music/mix/PlayQueueFeeder;->LOGV:Z

    if-eqz v4, :cond_0

    .line 374
    iget-object v4, v2, Lcom/google/android/music/cloudclient/RadioFeedResponse;->mRadioData:Lcom/google/android/music/cloudclient/RadioFeedResponse$RadioData;

    if-eqz v4, :cond_3

    iget-object v4, v2, Lcom/google/android/music/cloudclient/RadioFeedResponse;->mRadioData:Lcom/google/android/music/cloudclient/RadioFeedResponse$RadioData;

    iget-object v4, v4, Lcom/google/android/music/cloudclient/RadioFeedResponse$RadioData;->mRadioStations:Ljava/util/List;

    if-eqz v4, :cond_3

    .line 375
    const-string v4, "PlayQueueFeeder"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Radio stations result size: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v2, Lcom/google/android/music/cloudclient/RadioFeedResponse;->mRadioData:Lcom/google/android/music/cloudclient/RadioFeedResponse$RadioData;

    iget-object v6, v6, Lcom/google/android/music/cloudclient/RadioFeedResponse$RadioData;->mRadioStations:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 383
    :cond_0
    :goto_1
    iget-object v4, v2, Lcom/google/android/music/cloudclient/RadioFeedResponse;->mRadioData:Lcom/google/android/music/cloudclient/RadioFeedResponse$RadioData;

    if-eqz v4, :cond_1

    iget-object v4, v2, Lcom/google/android/music/cloudclient/RadioFeedResponse;->mRadioData:Lcom/google/android/music/cloudclient/RadioFeedResponse$RadioData;

    iget-object v4, v4, Lcom/google/android/music/cloudclient/RadioFeedResponse$RadioData;->mRadioStations:Ljava/util/List;

    if-eqz v4, :cond_1

    iget-object v4, v2, Lcom/google/android/music/cloudclient/RadioFeedResponse;->mRadioData:Lcom/google/android/music/cloudclient/RadioFeedResponse$RadioData;

    iget-object v4, v4, Lcom/google/android/music/cloudclient/RadioFeedResponse$RadioData;->mRadioStations:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-eqz v4, :cond_1

    .line 385
    iget-object v4, v2, Lcom/google/android/music/cloudclient/RadioFeedResponse;->mRadioData:Lcom/google/android/music/cloudclient/RadioFeedResponse$RadioData;

    iget-object v4, v4, Lcom/google/android/music/cloudclient/RadioFeedResponse$RadioData;->mRadioStations:Ljava/util/List;

    const/4 v5, 0x0

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/music/sync/google/model/SyncableRadioStation;

    iget-object v3, v4, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mTracks:Ljava/util/List;

    .line 388
    :cond_1
    return-object v3

    .line 368
    .end local v2    # "result":Lcom/google/android/music/cloudclient/RadioFeedResponse;
    :cond_2
    invoke-virtual {p2, p5, p6}, Lcom/google/android/music/store/Store;->getRadioRemoteId(J)Ljava/lang/String;

    move-result-object v1

    .line 369
    .local v1, "remoteRadioId":Ljava/lang/String;
    iget-object v4, p0, Lcom/google/android/music/mix/PlayQueueFeeder;->mCloudClient:Lcom/google/android/music/cloudclient/MusicCloud;

    invoke-interface {v4, v1, p3, p4, v0}, Lcom/google/android/music/cloudclient/MusicCloud;->getRadioFeed(Ljava/lang/String;ILjava/util/List;I)Lcom/google/android/music/cloudclient/RadioFeedResponse;

    move-result-object v2

    .restart local v2    # "result":Lcom/google/android/music/cloudclient/RadioFeedResponse;
    goto :goto_0

    .line 379
    .end local v1    # "remoteRadioId":Ljava/lang/String;
    :cond_3
    const-string v4, "PlayQueueFeeder"

    const-string v5, "Radio stations is null"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private handleCreateRadioStation(Lcom/google/android/music/mix/MixDescriptor;Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;)V
    .locals 10
    .param p1, "mix"    # Lcom/google/android/music/mix/MixDescriptor;
    .param p2, "action"    # Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;

    .prologue
    const/4 v9, 0x0

    .line 179
    iget-boolean v5, p0, Lcom/google/android/music/mix/PlayQueueFeeder;->LOGV:Z

    if-eqz v5, :cond_0

    .line 180
    const-string v5, "PlayQueueFeeder"

    const-string v6, "handleCreateRadioStation: mix=%s action=%s"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    aput-object p1, v7, v9

    const/4 v8, 0x1

    aput-object p2, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 183
    :cond_0
    iget-object v5, p0, Lcom/google/android/music/mix/PlayQueueFeeder;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v4

    .line 184
    .local v4, "store":Lcom/google/android/music/store/Store;
    const/4 v2, 0x0

    .line 186
    .local v2, "newMix":Lcom/google/android/music/mix/MixDescriptor;
    invoke-virtual {p1}, Lcom/google/android/music/mix/MixDescriptor;->getIsRecommendation()Z

    move-result v5

    invoke-direct {p0, v4, p1, v5, v9}, Lcom/google/android/music/mix/PlayQueueFeeder;->transitionSeedToRadioStationIfNeeded(Lcom/google/android/music/store/Store;Lcom/google/android/music/mix/MixDescriptor;ZI)Landroid/util/Pair;

    move-result-object v3

    .line 190
    .local v3, "newMixResult":Landroid/util/Pair;, "Landroid/util/Pair<Lcom/google/android/music/mix/MixDescriptor;Ljava/util/List<Lcom/google/android/music/sync/google/model/Track;>;>;"
    if-eqz v3, :cond_2

    .line 191
    iget-object v2, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    .end local v2    # "newMix":Lcom/google/android/music/mix/MixDescriptor;
    check-cast v2, Lcom/google/android/music/mix/MixDescriptor;

    .line 198
    .restart local v2    # "newMix":Lcom/google/android/music/mix/MixDescriptor;
    invoke-virtual {p1}, Lcom/google/android/music/mix/MixDescriptor;->getLocalRadioId()J

    move-result-wide v0

    .line 199
    .local v0, "localRadioId":J
    if-eqz v2, :cond_1

    .line 200
    invoke-virtual {v2}, Lcom/google/android/music/mix/MixDescriptor;->getLocalRadioId()J

    move-result-wide v0

    .line 203
    :cond_1
    invoke-direct {p0, v0, v1}, Lcom/google/android/music/mix/PlayQueueFeeder;->addRadioToRecentAsync(J)V

    .line 205
    iget-object v5, p0, Lcom/google/android/music/mix/PlayQueueFeeder;->mListener:Lcom/google/android/music/mix/PlayQueueFeederListener;

    const/4 v6, 0x0

    invoke-interface {v5, p1, v2, v6, p2}, Lcom/google/android/music/mix/PlayQueueFeederListener;->onSuccess(Lcom/google/android/music/mix/MixDescriptor;Lcom/google/android/music/mix/MixDescriptor;Lcom/google/android/music/medialist/SongList;Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;)V

    .line 206
    .end local v0    # "localRadioId":J
    :goto_0
    return-void

    .line 193
    :cond_2
    const-string v5, "PlayQueueFeeder"

    const-string v6, "Can\'t generate new mix result."

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 194
    iget-object v5, p0, Lcom/google/android/music/mix/PlayQueueFeeder;->mListener:Lcom/google/android/music/mix/PlayQueueFeederListener;

    invoke-interface {v5, p1, p2}, Lcom/google/android/music/mix/PlayQueueFeederListener;->onFailure(Lcom/google/android/music/mix/MixDescriptor;Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;)V

    goto :goto_0
.end method

.method private handleMoreContent(Lcom/google/android/music/mix/MixDescriptor;Ljava/util/List;ZLcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;)V
    .locals 20
    .param p1, "mix"    # Lcom/google/android/music/mix/MixDescriptor;
    .param p3, "canStream"    # Z
    .param p4, "action"    # Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/music/mix/MixDescriptor;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/download/ContentIdentifier;",
            ">;Z",
            "Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;",
            ")V"
        }
    .end annotation

    .prologue
    .line 210
    .local p2, "recentlyPlayed":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/download/ContentIdentifier;>;"
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/music/mix/PlayQueueFeeder;->LOGV:Z

    if-eqz v5, :cond_0

    .line 211
    const-string v5, "PlayQueueFeeder"

    const-string v6, "handleMoreContent: mix=%s recentlyPlayed=%s canStream=%s action=%s"

    const/16 v17, 0x4

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    aput-object p1, v17, v18

    const/16 v18, 0x1

    invoke-static/range {p2 .. p2}, Lcom/google/android/music/utils/DebugUtils;->listToString(Ljava/util/List;)Ljava/lang/String;

    move-result-object v19

    aput-object v19, v17, v18

    const/16 v18, 0x2

    invoke-static/range {p3 .. p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v19

    aput-object v19, v17, v18

    const/16 v18, 0x3

    aput-object p4, v17, v18

    move-object/from16 v0, v17

    invoke-static {v6, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 216
    :cond_0
    const/4 v15, 0x0

    .line 217
    .local v15, "songList":Lcom/google/android/music/medialist/SongList;
    const/4 v14, 0x0

    .line 218
    .local v14, "newMix":Lcom/google/android/music/mix/MixDescriptor;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/music/mix/PlayQueueFeeder;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v7

    .line 219
    .local v7, "store":Lcom/google/android/music/store/Store;
    const-wide/16 v10, 0x0

    .line 220
    .local v10, "localRadioId":J
    move-object/from16 v0, p0

    move/from16 v1, p3

    invoke-direct {v0, v1}, Lcom/google/android/music/mix/PlayQueueFeeder;->shouldIncludeUsedCachedSongs(Z)Z

    move-result v4

    .line 222
    .local v4, "allowUsedCachedSongs":Z
    :try_start_0
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/music/mix/PlayQueueFeeder;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/music/mix/PlayQueueFeeder;->getMaxMixSize(Landroid/content/ContentResolver;)I

    move-result v8

    .line 223
    .local v8, "maxSize":I
    const/16 v16, 0x0

    .line 225
    .local v16, "tracks":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/sync/google/model/Track;>;"
    move-object/from16 v0, p2

    invoke-virtual {v7, v0}, Lcom/google/android/music/store/Store;->getServerTrackIds(Ljava/util/List;)Ljava/util/List;

    move-result-object v9

    .line 226
    .local v9, "recentlyPlayedTrackIds":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/cloudclient/MixTrackId;>;"
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/music/mix/PlayQueueFeeder;->LOGV:Z

    if-eqz v5, :cond_1

    .line 227
    const-string v5, "PlayQueueFeeder"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "Track ids: "

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v17, ","

    move-object/from16 v0, v17

    invoke-static {v0, v9}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 230
    :cond_1
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/music/mix/PlayQueueFeeder;->mMixCancelled:Z

    if-eqz v5, :cond_2

    sget-object v5, Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;->OPEN:Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;

    move-object/from16 v0, p4

    if-ne v0, v5, :cond_2

    .line 231
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p4

    invoke-direct {v0, v1, v2}, Lcom/google/android/music/mix/PlayQueueFeeder;->notifyFailureAndResetCancelFlag(Lcom/google/android/music/mix/MixDescriptor;Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;)V

    .line 308
    .end local v8    # "maxSize":I
    .end local v9    # "recentlyPlayedTrackIds":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/cloudclient/MixTrackId;>;"
    .end local v16    # "tracks":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/sync/google/model/Track;>;"
    :goto_0
    return-void

    .line 235
    .restart local v8    # "maxSize":I
    .restart local v9    # "recentlyPlayedTrackIds":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/cloudclient/MixTrackId;>;"
    .restart local v16    # "tracks":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/sync/google/model/Track;>;"
    :cond_2
    const/4 v5, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v7, v1, v5, v8}, Lcom/google/android/music/mix/PlayQueueFeeder;->transitionSeedToRadioStationIfNeeded(Lcom/google/android/music/store/Store;Lcom/google/android/music/mix/MixDescriptor;ZI)Landroid/util/Pair;

    move-result-object v13

    .line 237
    .local v13, "mixResult":Landroid/util/Pair;, "Landroid/util/Pair<Lcom/google/android/music/mix/MixDescriptor;Ljava/util/List<Lcom/google/android/music/sync/google/model/Track;>;>;"
    if-eqz v13, :cond_6

    .line 238
    iget-object v5, v13, Landroid/util/Pair;->first:Ljava/lang/Object;

    move-object v0, v5

    check-cast v0, Lcom/google/android/music/mix/MixDescriptor;

    move-object v14, v0

    .line 239
    iget-object v0, v13, Landroid/util/Pair;->second:Ljava/lang/Object;

    move-object/from16 v16, v0

    .end local v16    # "tracks":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/sync/google/model/Track;>;"
    check-cast v16, Ljava/util/List;

    .line 246
    .restart local v16    # "tracks":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/sync/google/model/Track;>;"
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/mix/MixDescriptor;->getLocalRadioId()J

    move-result-wide v10

    .line 247
    if-eqz v14, :cond_3

    .line 248
    invoke-virtual {v14}, Lcom/google/android/music/mix/MixDescriptor;->getLocalRadioId()J

    move-result-wide v10

    .line 251
    :cond_3
    if-nez v16, :cond_a

    .line 253
    move-object/from16 v0, p0

    invoke-direct {v0, v10, v11, v4}, Lcom/google/android/music/mix/PlayQueueFeeder;->getCachedRadioSongs(JZ)Lcom/google/android/music/medialist/SongList;

    move-result-object v15

    .line 254
    if-nez v15, :cond_a

    .line 255
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/music/mix/PlayQueueFeeder;->mMixCancelled:Z

    if-eqz v5, :cond_7

    sget-object v5, Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;->OPEN:Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;

    move-object/from16 v0, p4

    if-ne v0, v5, :cond_7

    .line 256
    move-object/from16 v0, p0

    move-object/from16 v1, p4

    invoke-direct {v0, v14, v1}, Lcom/google/android/music/mix/PlayQueueFeeder;->notifyFailureAndResetCancelFlag(Lcom/google/android/music/mix/MixDescriptor;Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 282
    .end local v8    # "maxSize":I
    .end local v9    # "recentlyPlayedTrackIds":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/cloudclient/MixTrackId;>;"
    .end local v13    # "mixResult":Landroid/util/Pair;, "Landroid/util/Pair<Lcom/google/android/music/mix/MixDescriptor;Ljava/util/List<Lcom/google/android/music/sync/google/model/Track;>;>;"
    .end local v16    # "tracks":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/sync/google/model/Track;>;"
    :catch_0
    move-exception v12

    .line 283
    .local v12, "e":Ljava/io/IOException;
    const-string v5, "PlayQueueFeeder"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "Failed to get mix entries:"

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v12}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 288
    .end local v12    # "e":Ljava/io/IOException;
    :cond_4
    :goto_1
    if-nez v15, :cond_5

    if-nez v4, :cond_5

    const-wide/16 v18, 0x0

    cmp-long v5, v10, v18

    if-eqz v5, :cond_5

    .line 293
    const/4 v5, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v10, v11, v5}, Lcom/google/android/music/mix/PlayQueueFeeder;->getCachedRadioSongs(JZ)Lcom/google/android/music/medialist/SongList;

    move-result-object v15

    .line 294
    if-eqz v15, :cond_5

    .line 295
    const-string v5, "PlayQueueFeeder"

    const-string v6, "Could not get online tracks. Falling back to offline ones."

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 299
    :cond_5
    if-nez v15, :cond_c

    .line 300
    const-string v5, "PlayQueueFeeder"

    const-string v6, "Failed to create song list"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 301
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput-boolean v5, v0, Lcom/google/android/music/mix/PlayQueueFeeder;->mMixCancelled:Z

    .line 302
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/music/mix/PlayQueueFeeder;->mListener:Lcom/google/android/music/mix/PlayQueueFeederListener;

    move-object/from16 v0, p1

    move-object/from16 v1, p4

    invoke-interface {v5, v0, v1}, Lcom/google/android/music/mix/PlayQueueFeederListener;->onFailure(Lcom/google/android/music/mix/MixDescriptor;Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;)V

    goto/16 :goto_0

    .line 241
    .restart local v8    # "maxSize":I
    .restart local v9    # "recentlyPlayedTrackIds":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/cloudclient/MixTrackId;>;"
    .restart local v13    # "mixResult":Landroid/util/Pair;, "Landroid/util/Pair<Lcom/google/android/music/mix/MixDescriptor;Ljava/util/List<Lcom/google/android/music/sync/google/model/Track;>;>;"
    .restart local v16    # "tracks":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/sync/google/model/Track;>;"
    :cond_6
    :try_start_1
    const-string v5, "PlayQueueFeeder"

    const-string v6, "Can\'t transition seed to radio station."

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 242
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/music/mix/PlayQueueFeeder;->mListener:Lcom/google/android/music/mix/PlayQueueFeederListener;

    move-object/from16 v0, p1

    move-object/from16 v1, p4

    invoke-interface {v5, v0, v1}, Lcom/google/android/music/mix/PlayQueueFeederListener;->onFailure(Lcom/google/android/music/mix/MixDescriptor;Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    .line 284
    .end local v8    # "maxSize":I
    .end local v9    # "recentlyPlayedTrackIds":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/cloudclient/MixTrackId;>;"
    .end local v13    # "mixResult":Landroid/util/Pair;, "Landroid/util/Pair<Lcom/google/android/music/mix/MixDescriptor;Ljava/util/List<Lcom/google/android/music/sync/google/model/Track;>;>;"
    .end local v16    # "tracks":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/sync/google/model/Track;>;"
    :catch_1
    move-exception v12

    .line 285
    .local v12, "e":Ljava/lang/InterruptedException;
    const-string v5, "PlayQueueFeeder"

    const-string v6, "Interrupted while getting mix entries"

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 259
    .end local v12    # "e":Ljava/lang/InterruptedException;
    .restart local v8    # "maxSize":I
    .restart local v9    # "recentlyPlayedTrackIds":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/cloudclient/MixTrackId;>;"
    .restart local v13    # "mixResult":Landroid/util/Pair;, "Landroid/util/Pair<Lcom/google/android/music/mix/MixDescriptor;Ljava/util/List<Lcom/google/android/music/sync/google/model/Track;>;>;"
    .restart local v16    # "tracks":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/sync/google/model/Track;>;"
    :cond_7
    :try_start_2
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/music/mix/PlayQueueFeeder;->LOGV:Z

    if-eqz v5, :cond_8

    .line 260
    const-string v5, "PlayQueueFeeder"

    const-string v6, "Didn\'t pick cached tracks. Requesting from server"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 262
    :cond_8
    if-nez p3, :cond_9

    .line 263
    const-string v5, "PlayQueueFeeder"

    const-string v6, "Can\'t stream and failed to find cached songs."

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 264
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/music/mix/PlayQueueFeeder;->mListener:Lcom/google/android/music/mix/PlayQueueFeederListener;

    move-object/from16 v0, p1

    move-object/from16 v1, p4

    invoke-interface {v5, v0, v1}, Lcom/google/android/music/mix/PlayQueueFeederListener;->onFailure(Lcom/google/android/music/mix/MixDescriptor;Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;)V

    goto/16 :goto_0

    :cond_9
    move-object/from16 v5, p0

    move-object/from16 v6, p1

    .line 268
    invoke-direct/range {v5 .. v11}, Lcom/google/android/music/mix/PlayQueueFeeder;->getTracksFromServer(Lcom/google/android/music/mix/MixDescriptor;Lcom/google/android/music/store/Store;ILjava/util/List;J)Ljava/util/List;

    move-result-object v16

    .line 273
    :cond_a
    if-nez v15, :cond_b

    if-eqz v16, :cond_b

    .line 274
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v16

    invoke-direct {v0, v1, v2, v10, v11}, Lcom/google/android/music/mix/PlayQueueFeeder;->validateServerTracksAndCreateSongList(Lcom/google/android/music/mix/MixDescriptor;Ljava/util/List;J)Lcom/google/android/music/medialist/SongList;

    move-result-object v15

    .line 277
    :cond_b
    if-eqz v15, :cond_4

    .line 279
    move-object/from16 v0, p0

    invoke-direct {v0, v10, v11}, Lcom/google/android/music/mix/PlayQueueFeeder;->addRadioToRecentAsync(J)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_1

    .line 303
    .end local v8    # "maxSize":I
    .end local v9    # "recentlyPlayedTrackIds":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/cloudclient/MixTrackId;>;"
    .end local v13    # "mixResult":Landroid/util/Pair;, "Landroid/util/Pair<Lcom/google/android/music/mix/MixDescriptor;Ljava/util/List<Lcom/google/android/music/sync/google/model/Track;>;>;"
    .end local v16    # "tracks":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/sync/google/model/Track;>;"
    :cond_c
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/music/mix/PlayQueueFeeder;->mMixCancelled:Z

    if-eqz v5, :cond_d

    sget-object v5, Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;->OPEN:Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;

    move-object/from16 v0, p4

    if-ne v0, v5, :cond_d

    .line 304
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p4

    invoke-direct {v0, v1, v2}, Lcom/google/android/music/mix/PlayQueueFeeder;->notifyFailureAndResetCancelFlag(Lcom/google/android/music/mix/MixDescriptor;Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;)V

    goto/16 :goto_0

    .line 306
    :cond_d
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/music/mix/PlayQueueFeeder;->mListener:Lcom/google/android/music/mix/PlayQueueFeederListener;

    move-object/from16 v0, p1

    move-object/from16 v1, p4

    invoke-interface {v5, v0, v14, v15, v1}, Lcom/google/android/music/mix/PlayQueueFeederListener;->onSuccess(Lcom/google/android/music/mix/MixDescriptor;Lcom/google/android/music/mix/MixDescriptor;Lcom/google/android/music/medialist/SongList;Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;)V

    goto/16 :goto_0
.end method

.method private notifyFailureAndResetCancelFlag(Lcom/google/android/music/mix/MixDescriptor;Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;)V
    .locals 3
    .param p1, "mix"    # Lcom/google/android/music/mix/MixDescriptor;
    .param p2, "action"    # Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;

    .prologue
    .line 328
    const-string v0, "PlayQueueFeeder"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Mix: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " has been cancelled by the user while waiting for action:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 330
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/mix/PlayQueueFeeder;->mMixCancelled:Z

    .line 331
    iget-object v0, p0, Lcom/google/android/music/mix/PlayQueueFeeder;->mListener:Lcom/google/android/music/mix/PlayQueueFeederListener;

    invoke-interface {v0, p1, p2}, Lcom/google/android/music/mix/PlayQueueFeederListener;->onFailure(Lcom/google/android/music/mix/MixDescriptor;Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;)V

    .line 332
    return-void
.end method

.method private shouldIncludeUsedCachedSongs(Z)Z
    .locals 4
    .param p1, "canStream"    # Z

    .prologue
    const/4 v1, 0x0

    .line 311
    if-nez p1, :cond_3

    const/4 v0, 0x1

    .line 312
    .local v0, "usedCachedSongsOk":Z
    :goto_0
    if-nez v0, :cond_0

    .line 315
    iget-object v2, p0, Lcom/google/android/music/mix/PlayQueueFeeder;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    invoke-virtual {v2}, Lcom/google/android/music/preferences/MusicPreferences;->isDownloadedOnlyMode()Z

    move-result v0

    .line 317
    :cond_0
    if-nez v0, :cond_1

    .line 318
    iget-object v2, p0, Lcom/google/android/music/mix/PlayQueueFeeder;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "music_keepon_radio_always_use_downloaded_songs"

    invoke-static {v2, v3, v1}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    .line 323
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/music/mix/PlayQueueFeeder;->LOGV:Z

    if-eqz v1, :cond_2

    const-string v1, "PlayQueueFeeder"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "usedCachedSongsOk: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 324
    :cond_2
    return v0

    .end local v0    # "usedCachedSongsOk":Z
    :cond_3
    move v0, v1

    .line 311
    goto :goto_0
.end method

.method private transitionSeedToRadioStationIfNeeded(Lcom/google/android/music/store/Store;Lcom/google/android/music/mix/MixDescriptor;ZI)Landroid/util/Pair;
    .locals 20
    .param p1, "store"    # Lcom/google/android/music/store/Store;
    .param p2, "mix"    # Lcom/google/android/music/mix/MixDescriptor;
    .param p3, "feedOnCreate"    # Z
    .param p4, "maxSize"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/music/store/Store;",
            "Lcom/google/android/music/mix/MixDescriptor;",
            "ZI)",
            "Landroid/util/Pair",
            "<",
            "Lcom/google/android/music/mix/MixDescriptor;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/sync/google/model/Track;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 422
    const/4 v15, 0x0

    .line 423
    .local v15, "newMix":Lcom/google/android/music/mix/MixDescriptor;
    const/16 v19, 0x0

    .line 424
    .local v19, "tracks":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/sync/google/model/Track;>;"
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/music/mix/MixDescriptor;->getType()Lcom/google/android/music/mix/MixDescriptor$Type;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/music/mix/MixDescriptor$Type;->isPersistentSeed()Z

    move-result v2

    if-eqz v2, :cond_c

    .line 425
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/music/mix/MixDescriptor;->hasLocalSeedId()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/music/mix/MixDescriptor;->hasRemoteSeedId()Z

    move-result v2

    if-nez v2, :cond_0

    .line 426
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Seed has neither local nor remote id"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 429
    :cond_0
    const/4 v5, 0x0

    .line 430
    .local v5, "remoteSeedId":Ljava/lang/String;
    const/4 v6, 0x1

    .line 431
    .local v6, "seedType":I
    sget-object v2, Lcom/google/android/music/mix/PlayQueueFeeder$2;->$SwitchMap$com$google$android$music$mix$MixDescriptor$Type:[I

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/music/mix/MixDescriptor;->getType()Lcom/google/android/music/mix/MixDescriptor$Type;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/music/mix/MixDescriptor$Type;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 477
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "Unhandled seed type"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 433
    :pswitch_0
    const/16 v17, 0x0

    .line 434
    .local v17, "remoteSeedTrackId":Lcom/google/android/music/cloudclient/MixTrackId;
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/music/mix/MixDescriptor;->hasLocalSeedId()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 436
    :try_start_0
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/music/mix/MixDescriptor;->getLocalSeedId()J

    move-result-wide v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Lcom/google/android/music/store/Store;->getServerTrackId(J)Lcom/google/android/music/cloudclient/MixTrackId;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v17

    .line 441
    invoke-virtual/range {v17 .. v17}, Lcom/google/android/music/cloudclient/MixTrackId;->getRemoteId()Ljava/lang/String;

    move-result-object v5

    .line 442
    invoke-virtual/range {v17 .. v17}, Lcom/google/android/music/cloudclient/MixTrackId;->getType()Lcom/google/android/music/cloudclient/MixTrackId$Type;

    move-result-object v2

    sget-object v3, Lcom/google/android/music/cloudclient/MixTrackId$Type;->LOCKER:Lcom/google/android/music/cloudclient/MixTrackId$Type;

    if-ne v2, v3, :cond_3

    .line 443
    const/4 v6, 0x1

    .line 480
    .end local v17    # "remoteSeedTrackId":Lcom/google/android/music/cloudclient/MixTrackId;
    :goto_0
    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v6}, Lcom/google/android/music/store/Store;->getRadioIdsBySeedAndType(Ljava/lang/String;I)Landroid/util/Pair;

    move-result-object v16

    .line 482
    .local v16, "radioIds":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Long;Ljava/lang/String;>;"
    if-eqz v16, :cond_5

    move-object/from16 v0, v16

    iget-object v2, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    .line 483
    .local v8, "localRadioId":J
    :goto_1
    if-eqz v16, :cond_7

    move-object/from16 v0, v16

    iget-object v2, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/CharSequence;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_6

    const/4 v14, 0x1

    .line 485
    .local v14, "hasSourceId":Z
    :goto_2
    const-wide/16 v2, -0x1

    cmp-long v2, v8, v2

    if-eqz v2, :cond_1

    if-nez v14, :cond_2

    .line 490
    :cond_1
    if-eqz p3, :cond_9

    .line 492
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/music/mix/MixDescriptor;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/music/mix/MixDescriptor;->getArtLocation()Ljava/lang/String;

    move-result-object v4

    const/4 v7, 0x1

    move-object/from16 v2, p0

    move/from16 v8, p4

    invoke-direct/range {v2 .. v8}, Lcom/google/android/music/mix/PlayQueueFeeder;->createRadioStation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZI)Landroid/util/Pair;

    .end local v8    # "localRadioId":J
    move-result-object v18

    .line 496
    .local v18, "result":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Long;Ljava/util/List<Lcom/google/android/music/sync/google/model/Track;>;>;"
    if-eqz v18, :cond_8

    .line 497
    move-object/from16 v0, v18

    iget-object v2, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    .line 498
    .restart local v8    # "localRadioId":J
    move-object/from16 v0, v18

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    move-object/from16 v19, v0

    .end local v19    # "tracks":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/sync/google/model/Track;>;"
    check-cast v19, Ljava/util/List;

    .line 517
    .end local v18    # "result":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Long;Ljava/util/List<Lcom/google/android/music/sync/google/model/Track;>;>;"
    .restart local v19    # "tracks":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/sync/google/model/Track;>;"
    :cond_2
    :goto_3
    const-wide/16 v2, -0x1

    cmp-long v2, v8, v2

    if-nez v2, :cond_b

    .line 518
    const-string v2, "PlayQueueFeeder"

    const-string v3, "Can\'t find local radio Id when starting radio."

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 519
    const/4 v2, 0x0

    move-object v7, v15

    .line 525
    .end local v5    # "remoteSeedId":Ljava/lang/String;
    .end local v6    # "seedType":I
    .end local v8    # "localRadioId":J
    .end local v14    # "hasSourceId":Z
    .end local v15    # "newMix":Lcom/google/android/music/mix/MixDescriptor;
    .end local v16    # "radioIds":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Long;Ljava/lang/String;>;"
    .local v7, "newMix":Lcom/google/android/music/mix/MixDescriptor;
    :goto_4
    return-object v2

    .line 437
    .end local v7    # "newMix":Lcom/google/android/music/mix/MixDescriptor;
    .restart local v5    # "remoteSeedId":Ljava/lang/String;
    .restart local v6    # "seedType":I
    .restart local v15    # "newMix":Lcom/google/android/music/mix/MixDescriptor;
    .restart local v17    # "remoteSeedTrackId":Lcom/google/android/music/cloudclient/MixTrackId;
    :catch_0
    move-exception v13

    .line 438
    .local v13, "e":Ljava/io/FileNotFoundException;
    const-string v2, "PlayQueueFeeder"

    const-string v3, "Failed to get remote track id"

    invoke-static {v2, v3, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 439
    const/4 v2, 0x0

    move-object v7, v15

    .end local v15    # "newMix":Lcom/google/android/music/mix/MixDescriptor;
    .restart local v7    # "newMix":Lcom/google/android/music/mix/MixDescriptor;
    goto :goto_4

    .line 446
    .end local v7    # "newMix":Lcom/google/android/music/mix/MixDescriptor;
    .end local v13    # "e":Ljava/io/FileNotFoundException;
    .restart local v15    # "newMix":Lcom/google/android/music/mix/MixDescriptor;
    :cond_3
    const/4 v6, 0x2

    goto :goto_0

    .line 451
    :cond_4
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/music/mix/MixDescriptor;->getRemoteSeedId()Ljava/lang/String;

    move-result-object v5

    .line 452
    const/4 v6, 0x2

    .line 455
    goto :goto_0

    .line 457
    .end local v17    # "remoteSeedTrackId":Lcom/google/android/music/cloudclient/MixTrackId;
    :pswitch_1
    const/4 v6, 0x3

    .line 458
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/music/mix/MixDescriptor;->getRemoteSeedId()Ljava/lang/String;

    move-result-object v5

    .line 459
    goto :goto_0

    .line 461
    :pswitch_2
    const/4 v6, 0x4

    .line 462
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/music/mix/MixDescriptor;->getRemoteSeedId()Ljava/lang/String;

    move-result-object v5

    .line 463
    goto :goto_0

    .line 465
    :pswitch_3
    const/4 v6, 0x5

    .line 466
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/music/mix/MixDescriptor;->getRemoteSeedId()Ljava/lang/String;

    move-result-object v5

    .line 467
    goto/16 :goto_0

    .line 469
    :pswitch_4
    const/16 v6, 0x8

    .line 470
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/music/mix/MixDescriptor;->getRemoteSeedId()Ljava/lang/String;

    move-result-object v5

    .line 471
    goto/16 :goto_0

    .line 473
    :pswitch_5
    const/16 v6, 0x9

    .line 474
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/music/mix/MixDescriptor;->getRemoteSeedId()Ljava/lang/String;

    move-result-object v5

    .line 475
    goto/16 :goto_0

    .line 482
    .restart local v16    # "radioIds":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Long;Ljava/lang/String;>;"
    :cond_5
    const-wide/16 v8, -0x1

    goto/16 :goto_1

    .line 483
    .restart local v8    # "localRadioId":J
    :cond_6
    const/4 v14, 0x0

    goto :goto_2

    :cond_7
    const/4 v14, 0x0

    goto :goto_2

    .line 500
    .end local v8    # "localRadioId":J
    .restart local v14    # "hasSourceId":Z
    .restart local v18    # "result":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Long;Ljava/util/List<Lcom/google/android/music/sync/google/model/Track;>;>;"
    :cond_8
    const-string v2, "PlayQueueFeeder"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Couldn\'t create a radio station with track feed for mix: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 502
    const/4 v2, 0x0

    move-object v7, v15

    .end local v15    # "newMix":Lcom/google/android/music/mix/MixDescriptor;
    .restart local v7    # "newMix":Lcom/google/android/music/mix/MixDescriptor;
    goto :goto_4

    .line 506
    .end local v7    # "newMix":Lcom/google/android/music/mix/MixDescriptor;
    .end local v18    # "result":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Long;Ljava/util/List<Lcom/google/android/music/sync/google/model/Track;>;>;"
    .restart local v8    # "localRadioId":J
    .restart local v15    # "newMix":Lcom/google/android/music/mix/MixDescriptor;
    :cond_9
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/music/mix/MixDescriptor;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/music/mix/MixDescriptor;->getArtLocation()Ljava/lang/String;

    move-result-object v4

    const/4 v7, 0x0

    move-object/from16 v2, p0

    move/from16 v8, p4

    invoke-direct/range {v2 .. v8}, Lcom/google/android/music/mix/PlayQueueFeeder;->createRadioStation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZI)Landroid/util/Pair;

    .end local v8    # "localRadioId":J
    move-result-object v18

    .line 509
    .restart local v18    # "result":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Long;Ljava/util/List<Lcom/google/android/music/sync/google/model/Track;>;>;"
    if-nez v18, :cond_a

    .line 510
    const-string v2, "PlayQueueFeeder"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Couldn\'t create a radio station without track feed for mix: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 512
    const/4 v2, 0x0

    move-object v7, v15

    .end local v15    # "newMix":Lcom/google/android/music/mix/MixDescriptor;
    .restart local v7    # "newMix":Lcom/google/android/music/mix/MixDescriptor;
    goto/16 :goto_4

    .line 514
    .end local v7    # "newMix":Lcom/google/android/music/mix/MixDescriptor;
    .restart local v15    # "newMix":Lcom/google/android/music/mix/MixDescriptor;
    :cond_a
    move-object/from16 v0, v18

    iget-object v2, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    .restart local v8    # "localRadioId":J
    goto/16 :goto_3

    .line 521
    .end local v18    # "result":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Long;Ljava/util/List<Lcom/google/android/music/sync/google/model/Track;>;>;"
    :cond_b
    new-instance v7, Lcom/google/android/music/mix/MixDescriptor;

    sget-object v10, Lcom/google/android/music/mix/MixDescriptor$Type;->RADIO:Lcom/google/android/music/mix/MixDescriptor$Type;

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/music/mix/MixDescriptor;->getName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/music/mix/MixDescriptor;->getArtLocation()Ljava/lang/String;

    move-result-object v12

    invoke-direct/range {v7 .. v12}, Lcom/google/android/music/mix/MixDescriptor;-><init>(JLcom/google/android/music/mix/MixDescriptor$Type;Ljava/lang/String;Ljava/lang/String;)V

    .line 525
    .end local v5    # "remoteSeedId":Ljava/lang/String;
    .end local v6    # "seedType":I
    .end local v8    # "localRadioId":J
    .end local v14    # "hasSourceId":Z
    .end local v15    # "newMix":Lcom/google/android/music/mix/MixDescriptor;
    .end local v16    # "radioIds":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Long;Ljava/lang/String;>;"
    .restart local v7    # "newMix":Lcom/google/android/music/mix/MixDescriptor;
    :goto_5
    new-instance v2, Landroid/util/Pair;

    move-object/from16 v0, v19

    invoke-direct {v2, v7, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_4

    .end local v7    # "newMix":Lcom/google/android/music/mix/MixDescriptor;
    .restart local v15    # "newMix":Lcom/google/android/music/mix/MixDescriptor;
    :cond_c
    move-object v7, v15

    .end local v15    # "newMix":Lcom/google/android/music/mix/MixDescriptor;
    .restart local v7    # "newMix":Lcom/google/android/music/mix/MixDescriptor;
    goto :goto_5

    .line 431
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private validateServerTracksAndCreateSongList(Lcom/google/android/music/mix/MixDescriptor;Ljava/util/List;J)Lcom/google/android/music/medialist/SongList;
    .locals 5
    .param p1, "mix"    # Lcom/google/android/music/mix/MixDescriptor;
    .param p3, "localRadioId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/music/mix/MixDescriptor;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/sync/google/model/Track;",
            ">;J)",
            "Lcom/google/android/music/medialist/SongList;"
        }
    .end annotation

    .prologue
    .line 337
    .local p2, "tracks":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/sync/google/model/Track;>;"
    const/4 v1, 0x0

    .line 338
    .local v1, "songList":Lcom/google/android/music/medialist/SongList;
    if-eqz p2, :cond_1

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x1

    if-le v2, v3, :cond_1

    .line 340
    invoke-virtual {p1}, Lcom/google/android/music/mix/MixDescriptor;->getType()Lcom/google/android/music/mix/MixDescriptor$Type;

    move-result-object v2

    sget-object v3, Lcom/google/android/music/mix/MixDescriptor$Type;->LUCKY_RADIO:Lcom/google/android/music/mix/MixDescriptor$Type;

    if-ne v2, v3, :cond_0

    .line 341
    invoke-static {}, Lcom/google/android/music/store/ContainerDescriptor;->newLuckyRadioDescriptor()Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v0

    .line 342
    .local v0, "container":Lcom/google/android/music/store/ContainerDescriptor;
    invoke-static {p2, v0}, Lcom/google/android/music/medialist/TracksSongList;->createList(Ljava/util/List;Lcom/google/android/music/store/ContainerDescriptor;)Lcom/google/android/music/medialist/TracksSongList;

    move-result-object v1

    .line 353
    .end local v0    # "container":Lcom/google/android/music/store/ContainerDescriptor;
    :goto_0
    return-object v1

    .line 345
    :cond_0
    const-string v2, ""

    invoke-static {p3, p4, v2}, Lcom/google/android/music/store/ContainerDescriptor;->newRadioDescriptor(JLjava/lang/String;)Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v0

    .line 347
    .restart local v0    # "container":Lcom/google/android/music/store/ContainerDescriptor;
    invoke-static {p2, v0}, Lcom/google/android/music/medialist/TracksSongList;->createList(Ljava/util/List;Lcom/google/android/music/store/ContainerDescriptor;)Lcom/google/android/music/medialist/TracksSongList;

    move-result-object v1

    .line 348
    goto :goto_0

    .line 351
    .end local v0    # "container":Lcom/google/android/music/store/ContainerDescriptor;
    :cond_1
    const-string v2, "PlayQueueFeeder"

    const-string v3, "Radio station is empty or too small."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public cancelMix()V
    .locals 2

    .prologue
    .line 141
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/music/mix/PlayQueueFeeder;->mMixCancelled:Z

    .line 142
    const/4 v1, 0x3

    invoke-virtual {p0, v1}, Lcom/google/android/music/mix/PlayQueueFeeder;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 143
    .local v0, "msg":Landroid/os/Message;
    invoke-virtual {p0, v0}, Lcom/google/android/music/mix/PlayQueueFeeder;->sendMessage(Landroid/os/Message;)Z

    .line 144
    return-void
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 148
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    .line 149
    .local v0, "mixRequestData":Lcom/google/android/music/mix/PlayQueueFeeder$MixRequestData;
    :goto_0
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 176
    :goto_1
    return-void

    .line 148
    .end local v0    # "mixRequestData":Lcom/google/android/music/mix/PlayQueueFeeder$MixRequestData;
    :cond_0
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/music/mix/PlayQueueFeeder$MixRequestData;

    move-object v0, v1

    goto :goto_0

    .line 151
    .restart local v0    # "mixRequestData":Lcom/google/android/music/mix/PlayQueueFeeder$MixRequestData;
    :pswitch_0
    if-eqz v0, :cond_1

    .line 152
    invoke-virtual {v0}, Lcom/google/android/music/mix/PlayQueueFeeder$MixRequestData;->getMixDescriptor()Lcom/google/android/music/mix/MixDescriptor;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/music/mix/PlayQueueFeeder$MixRequestData;->getRecentlyPlayed()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/music/mix/PlayQueueFeeder$MixRequestData;->getCanStream()Z

    move-result v3

    invoke-virtual {v0}, Lcom/google/android/music/mix/PlayQueueFeeder$MixRequestData;->getPostProcessingAction()Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;

    move-result-object v4

    invoke-direct {p0, v1, v2, v3, v4}, Lcom/google/android/music/mix/PlayQueueFeeder;->handleMoreContent(Lcom/google/android/music/mix/MixDescriptor;Ljava/util/List;ZLcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;)V

    goto :goto_1

    .line 157
    :cond_1
    const-string v1, "PlayQueueFeeder"

    const-string v2, "Missing request data for request content"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 162
    :pswitch_1
    if-eqz v0, :cond_2

    .line 163
    invoke-virtual {v0}, Lcom/google/android/music/mix/PlayQueueFeeder$MixRequestData;->getMixDescriptor()Lcom/google/android/music/mix/MixDescriptor;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/music/mix/PlayQueueFeeder$MixRequestData;->getPostProcessingAction()Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/google/android/music/mix/PlayQueueFeeder;->handleCreateRadioStation(Lcom/google/android/music/mix/MixDescriptor;Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;)V

    goto :goto_1

    .line 166
    :cond_2
    const-string v1, "PlayQueueFeeder"

    const-string v2, "Missing request data for create radio"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 172
    :pswitch_2
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/music/mix/PlayQueueFeeder;->mMixCancelled:Z

    goto :goto_1

    .line 149
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 107
    invoke-virtual {p0}, Lcom/google/android/music/mix/PlayQueueFeeder;->quit()V

    .line 108
    invoke-static {p0}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    .line 109
    return-void
.end method

.method public requestContent(Lcom/google/android/music/mix/MixDescriptor;Ljava/util/List;ZLcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;)V
    .locals 3
    .param p1, "mix"    # Lcom/google/android/music/mix/MixDescriptor;
    .param p3, "canStream"    # Z
    .param p4, "action"    # Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/music/mix/MixDescriptor;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/download/ContentIdentifier;",
            ">;Z",
            "Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;",
            ")V"
        }
    .end annotation

    .prologue
    .line 121
    .local p2, "recentlyPlayed":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/download/ContentIdentifier;>;"
    new-instance v1, Lcom/google/android/music/mix/PlayQueueFeeder$MixRequestData;

    invoke-direct {v1, p1, p2, p3, p4}, Lcom/google/android/music/mix/PlayQueueFeeder$MixRequestData;-><init>(Lcom/google/android/music/mix/MixDescriptor;Ljava/util/List;ZLcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;)V

    .line 122
    .local v1, "requestData":Lcom/google/android/music/mix/PlayQueueFeeder$MixRequestData;
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/google/android/music/mix/PlayQueueFeeder;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 123
    .local v0, "msg":Landroid/os/Message;
    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 124
    invoke-virtual {p0, v0}, Lcom/google/android/music/mix/PlayQueueFeeder;->sendMessage(Landroid/os/Message;)Z

    .line 125
    return-void
.end method

.method public requestCreateRadioStation(Lcom/google/android/music/mix/MixDescriptor;Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;)V
    .locals 4
    .param p1, "mix"    # Lcom/google/android/music/mix/MixDescriptor;
    .param p2, "action"    # Lcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;

    .prologue
    .line 128
    new-instance v1, Lcom/google/android/music/mix/PlayQueueFeeder$MixRequestData;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-direct {v1, p1, v2, v3, p2}, Lcom/google/android/music/mix/PlayQueueFeeder$MixRequestData;-><init>(Lcom/google/android/music/mix/MixDescriptor;Ljava/util/List;ZLcom/google/android/music/mix/PlayQueueFeeder$PostProcessingAction;)V

    .line 131
    .local v1, "requestData":Lcom/google/android/music/mix/PlayQueueFeeder$MixRequestData;
    const/4 v2, 0x2

    invoke-virtual {p0, v2}, Lcom/google/android/music/mix/PlayQueueFeeder;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 132
    .local v0, "msg":Landroid/os/Message;
    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 133
    invoke-virtual {p0, v0}, Lcom/google/android/music/mix/PlayQueueFeeder;->sendMessage(Landroid/os/Message;)Z

    .line 134
    return-void
.end method
