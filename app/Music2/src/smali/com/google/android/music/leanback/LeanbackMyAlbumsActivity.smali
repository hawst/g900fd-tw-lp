.class public Lcom/google/android/music/leanback/LeanbackMyAlbumsActivity;
.super Lcom/google/android/music/leanback/LeanbackGridActivity;
.source "LeanbackMyAlbumsActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackGridActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected bindCursor(Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;Landroid/database/Cursor;)Ljava/lang/Object;
    .locals 7
    .param p1, "leanbackCursorObjectAdapter"    # Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;
    .param p2, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 34
    const/4 v5, 0x0

    invoke-interface {p2, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 35
    .local v2, "id":J
    const/4 v5, 0x1

    invoke-interface {p2, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 36
    .local v4, "name":Ljava/lang/String;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 37
    const v5, 0x7f0b00c5

    invoke-virtual {p0, v5}, Lcom/google/android/music/leanback/LeanbackMyAlbumsActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 39
    :cond_0
    const/4 v5, 0x2

    invoke-interface {p2, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 40
    .local v0, "artist":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 41
    const v5, 0x7f0b00c4

    invoke-virtual {p0, v5}, Lcom/google/android/music/leanback/LeanbackMyAlbumsActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 43
    :cond_1
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-static {p0, v5}, Lcom/google/android/music/leanback/LeanbackUtils;->getAlbumDetailsIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 45
    .local v1, "intent":Landroid/content/Intent;
    new-instance v5, Lcom/google/android/music/leanback/Item$Builder;

    invoke-direct {v5}, Lcom/google/android/music/leanback/Item$Builder;-><init>()V

    invoke-virtual {v5, v4}, Lcom/google/android/music/leanback/Item$Builder;->title(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v5

    invoke-virtual {v5, v0}, Lcom/google/android/music/leanback/Item$Builder;->description(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v5

    new-instance v6, Lcom/google/android/music/leanback/LeanbackMyAlbumsActivity$1;

    invoke-direct {v6, p0, v2, v3}, Lcom/google/android/music/leanback/LeanbackMyAlbumsActivity$1;-><init>(Lcom/google/android/music/leanback/LeanbackMyAlbumsActivity;J)V

    invoke-virtual {v5, v6}, Lcom/google/android/music/leanback/Item$Builder;->iconUri(Lcom/google/android/music/leanback/Item$StringGetter;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v5

    invoke-virtual {v5, v1}, Lcom/google/android/music/leanback/Item$Builder;->intent(Landroid/content/Intent;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v5

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/music/leanback/Item$Builder;->id(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/music/leanback/Item$Builder;->build()Lcom/google/android/music/leanback/Item;

    move-result-object v5

    return-object v5
.end method

.method protected getGridTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    const v0, 0x7f0b009f

    invoke-virtual {p0, v0}, Lcom/google/android/music/leanback/LeanbackMyAlbumsActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getMediaList()Lcom/google/android/music/medialist/MediaList;
    .locals 1

    .prologue
    .line 23
    new-instance v0, Lcom/google/android/music/medialist/AllAlbumsList;

    invoke-direct {v0}, Lcom/google/android/music/medialist/AllAlbumsList;-><init>()V

    return-object v0
.end method

.method protected getProjection()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/google/android/music/ui/AlbumsAdapter;->PROJECTION:[Ljava/lang/String;

    return-object v0
.end method
