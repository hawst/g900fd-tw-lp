.class final Lcom/google/android/music/ui/RecommendedRadioFragment$RecommendedRadioAdapter;
.super Lcom/google/android/music/ui/MediaListCardAdapter;
.source "RecommendedRadioFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/RecommendedRadioFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "RecommendedRadioAdapter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/RecommendedRadioFragment;


# direct methods
.method private constructor <init>(Lcom/google/android/music/ui/RecommendedRadioFragment;Lcom/google/android/music/ui/RecommendedRadioFragment;)V
    .locals 1
    .param p2, "fragment"    # Lcom/google/android/music/ui/RecommendedRadioFragment;

    .prologue
    .line 62
    iput-object p1, p0, Lcom/google/android/music/ui/RecommendedRadioFragment$RecommendedRadioAdapter;->this$0:Lcom/google/android/music/ui/RecommendedRadioFragment;

    .line 63
    # getter for: Lcom/google/android/music/ui/RecommendedRadioFragment;->mTileMetadata:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;
    invoke-static {p1}, Lcom/google/android/music/ui/RecommendedRadioFragment;->access$200(Lcom/google/android/music/ui/RecommendedRadioFragment;)Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->getLayoutId()I

    move-result v0

    invoke-direct {p0, p2, v0}, Lcom/google/android/music/ui/MediaListCardAdapter;-><init>(Lcom/google/android/music/ui/MusicFragment;I)V

    .line 64
    return-void
.end method

.method private constructor <init>(Lcom/google/android/music/ui/RecommendedRadioFragment;Lcom/google/android/music/ui/RecommendedRadioFragment;Landroid/database/Cursor;)V
    .locals 1
    .param p2, "fragment"    # Lcom/google/android/music/ui/RecommendedRadioFragment;
    .param p3, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/google/android/music/ui/RecommendedRadioFragment$RecommendedRadioAdapter;->this$0:Lcom/google/android/music/ui/RecommendedRadioFragment;

    .line 67
    # getter for: Lcom/google/android/music/ui/RecommendedRadioFragment;->mTileMetadata:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;
    invoke-static {p1}, Lcom/google/android/music/ui/RecommendedRadioFragment;->access$200(Lcom/google/android/music/ui/RecommendedRadioFragment;)Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->getLayoutId()I

    move-result v0

    invoke-direct {p0, p2, v0, p3}, Lcom/google/android/music/ui/MediaListCardAdapter;-><init>(Lcom/google/android/music/ui/MusicFragment;ILandroid/database/Cursor;)V

    .line 68
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/music/ui/RecommendedRadioFragment;Lcom/google/android/music/ui/RecommendedRadioFragment;Landroid/database/Cursor;Lcom/google/android/music/ui/RecommendedRadioFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/music/ui/RecommendedRadioFragment;
    .param p2, "x1"    # Lcom/google/android/music/ui/RecommendedRadioFragment;
    .param p3, "x2"    # Landroid/database/Cursor;
    .param p4, "x3"    # Lcom/google/android/music/ui/RecommendedRadioFragment$1;

    .prologue
    .line 60
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/music/ui/RecommendedRadioFragment$RecommendedRadioAdapter;-><init>(Lcom/google/android/music/ui/RecommendedRadioFragment;Lcom/google/android/music/ui/RecommendedRadioFragment;Landroid/database/Cursor;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/music/ui/RecommendedRadioFragment;Lcom/google/android/music/ui/RecommendedRadioFragment;Lcom/google/android/music/ui/RecommendedRadioFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/music/ui/RecommendedRadioFragment;
    .param p2, "x1"    # Lcom/google/android/music/ui/RecommendedRadioFragment;
    .param p3, "x2"    # Lcom/google/android/music/ui/RecommendedRadioFragment$1;

    .prologue
    .line 60
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/ui/RecommendedRadioFragment$RecommendedRadioAdapter;-><init>(Lcom/google/android/music/ui/RecommendedRadioFragment;Lcom/google/android/music/ui/RecommendedRadioFragment;)V

    return-void
.end method


# virtual methods
.method protected bindViewToLoadingItem(Landroid/view/View;Landroid/content/Context;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 103
    instance-of v1, p1, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 104
    check-cast v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    .line 105
    .local v0, "pcv":Lcom/google/android/music/ui/cardlib/layout/PlayCardView;
    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->bindLoading()V

    .line 107
    .end local v0    # "pcv":Lcom/google/android/music/ui/cardlib/layout/PlayCardView;
    :cond_0
    return-void
.end method

.method protected bindViewToMediaListItem(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;J)V
    .locals 8
    .param p1, "view"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "cursor"    # Landroid/database/Cursor;
    .param p4, "itemId"    # J

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 83
    invoke-static {p1}, Lcom/google/android/music/utils/MusicUtils;->getDocument(Landroid/view/View;)Lcom/google/android/music/ui/cardlib/model/Document;

    move-result-object v0

    .line 85
    .local v0, "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    invoke-interface {p3, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 86
    .local v2, "id":J
    invoke-virtual {v0, v2, v3}, Lcom/google/android/music/ui/cardlib/model/Document;->setId(J)V

    .line 87
    sget-object v7, Lcom/google/android/music/ui/cardlib/model/Document$Type;->PLAYLIST:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    invoke-virtual {v0, v7}, Lcom/google/android/music/ui/cardlib/model/Document;->setType(Lcom/google/android/music/ui/cardlib/model/Document$Type;)V

    .line 89
    invoke-interface {p3, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 90
    .local v4, "playlistName":Ljava/lang/String;
    invoke-virtual {v0, v4}, Lcom/google/android/music/ui/cardlib/model/Document;->setTitle(Ljava/lang/String;)V

    .line 91
    invoke-virtual {v0, v4}, Lcom/google/android/music/ui/cardlib/model/Document;->setPlaylistName(Ljava/lang/String;)V

    .line 92
    const/4 v7, 0x2

    invoke-interface {p3, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    invoke-virtual {v0, v7}, Lcom/google/android/music/ui/cardlib/model/Document;->setPlaylistType(I)V

    .line 93
    const/4 v7, 0x3

    invoke-interface {p3, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    if-eqz v7, :cond_1

    :goto_0
    invoke-virtual {v0, v5}, Lcom/google/android/music/ui/cardlib/model/Document;->setHasLocal(Z)V

    .line 95
    instance-of v5, p1, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    if-eqz v5, :cond_0

    move-object v1, p1

    .line 96
    check-cast v1, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    .line 97
    .local v1, "pcv":Lcom/google/android/music/ui/cardlib/layout/PlayCardView;
    iget-object v5, p0, Lcom/google/android/music/ui/RecommendedRadioFragment$RecommendedRadioAdapter;->this$0:Lcom/google/android/music/ui/RecommendedRadioFragment;

    iget-object v5, v5, Lcom/google/android/music/ui/RecommendedRadioFragment;->mCardsContextMenuDelegate:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$DocumentContextMenuDelegate;

    invoke-virtual {v1, v0, v5}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->bind(Lcom/google/android/music/ui/cardlib/model/Document;Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;)V

    .line 99
    .end local v1    # "pcv":Lcom/google/android/music/ui/cardlib/layout/PlayCardView;
    :cond_0
    return-void

    :cond_1
    move v5, v6

    .line 93
    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 72
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/music/ui/MediaListCardAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 73
    .local v0, "view":Landroid/view/View;
    instance-of v1, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    if-eqz v1, :cond_0

    move-object v1, v0

    .line 74
    check-cast v1, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    iget-object v2, p0, Lcom/google/android/music/ui/RecommendedRadioFragment$RecommendedRadioAdapter;->this$0:Lcom/google/android/music/ui/RecommendedRadioFragment;

    # getter for: Lcom/google/android/music/ui/RecommendedRadioFragment;->mTileMetadata:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;
    invoke-static {v2}, Lcom/google/android/music/ui/RecommendedRadioFragment;->access$200(Lcom/google/android/music/ui/RecommendedRadioFragment;)Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->getThumbnailAspectRatio()F

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->setThumbnailAspectRatio(F)V

    .line 77
    :cond_0
    return-object v0
.end method
