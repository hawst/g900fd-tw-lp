.class abstract Lcom/google/android/music/leanback/bitmap/UriBitmapGettersGetter;
.super Lcom/google/android/music/leanback/bitmap/LoaderBitmapGettersGetter;
.source "UriBitmapGettersGetter.java"


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/support/v4/app/LoaderManager;IJ)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "loaderManager"    # Landroid/support/v4/app/LoaderManager;
    .param p3, "loaderId"    # I
    .param p4, "seed"    # J

    .prologue
    .line 14
    invoke-direct/range {p0 .. p5}, Lcom/google/android/music/leanback/bitmap/LoaderBitmapGettersGetter;-><init>(Landroid/content/Context;Landroid/support/v4/app/LoaderManager;IJ)V

    .line 15
    return-void
.end method


# virtual methods
.method protected getCursorLoader()Landroid/support/v4/content/Loader;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 23
    new-instance v0, Landroid/support/v4/content/CursorLoader;

    iget-object v1, p0, Lcom/google/android/music/leanback/bitmap/UriBitmapGettersGetter;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/google/android/music/leanback/bitmap/UriBitmapGettersGetter;->getUri()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/music/leanback/bitmap/UriBitmapGettersGetter;->getProjection()[Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method protected abstract getProjection()[Ljava/lang/String;
.end method

.method protected abstract getUri()Landroid/net/Uri;
.end method
