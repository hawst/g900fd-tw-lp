.class public Lcom/google/android/music/ui/EmptyLibraryFragment;
.super Landroid/support/v4/app/Fragment;
.source "EmptyLibraryFragment.java"


# instance fields
.field private mList:Landroid/widget/ListView;

.field private mTitle:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method private initListView()V
    .locals 11

    .prologue
    const v10, 0x7f0b0144

    .line 65
    new-instance v5, Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter$OtherWaysToPlayListEntry;

    const v7, 0x7f020158

    const v8, 0x7f0b0136

    invoke-direct {v5, v7, v8, v10}, Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter$OtherWaysToPlayListEntry;-><init>(III)V

    .line 68
    .local v5, "tryNautilusEntry":Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter$OtherWaysToPlayListEntry;
    new-instance v6, Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter$OtherWaysToPlayListEntry;

    const v7, 0x7f0200dd

    const v8, 0x7f0b0138

    invoke-direct {v6, v7, v8, v10}, Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter$OtherWaysToPlayListEntry;-><init>(III)V

    .line 71
    .local v6, "uploadMusicEntry":Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter$OtherWaysToPlayListEntry;
    new-instance v3, Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter$OtherWaysToPlayListEntry;

    const v7, 0x7f020154

    const v8, 0x7f0b0137

    const v9, 0x7f0b0145

    invoke-direct {v3, v7, v8, v9}, Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter$OtherWaysToPlayListEntry;-><init>(III)V

    .line 74
    .local v3, "shopEntry":Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter$OtherWaysToPlayListEntry;
    new-instance v4, Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter$OtherWaysToPlayListEntry;

    const v7, 0x7f02016f

    const v8, 0x7f0b0139

    invoke-direct {v4, v7, v8, v10}, Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter$OtherWaysToPlayListEntry;-><init>(III)V

    .line 78
    .local v4, "sideloadEntry":Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter$OtherWaysToPlayListEntry;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 79
    .local v1, "listData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter$OtherWaysToPlayListEntry;>;"
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v2

    .line 80
    .local v2, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    invoke-virtual {v2}, Lcom/google/android/music/preferences/MusicPreferences;->hasStreamingAccount()Z

    move-result v7

    if-nez v7, :cond_0

    .line 82
    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 83
    iget-object v7, p0, Lcom/google/android/music/ui/EmptyLibraryFragment;->mTitle:Landroid/widget/TextView;

    const v8, 0x7f0b02ac

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(I)V

    .line 94
    :goto_0
    new-instance v0, Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter;

    invoke-virtual {p0}, Lcom/google/android/music/ui/EmptyLibraryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v7

    const v8, 0x7f040086

    invoke-direct {v0, v7, v2, v8, v1}, Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter;-><init>(Landroid/content/Context;Lcom/google/android/music/preferences/MusicPreferences;ILjava/util/ArrayList;)V

    .line 96
    .local v0, "adapter":Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter;
    iget-object v7, p0, Lcom/google/android/music/ui/EmptyLibraryFragment;->mList:Landroid/widget/ListView;

    invoke-virtual {v7, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 97
    return-void

    .line 85
    .end local v0    # "adapter":Lcom/google/android/music/tutorial/OtherWaysToPlayListAdapter;
    :cond_0
    invoke-virtual {v2}, Lcom/google/android/music/preferences/MusicPreferences;->getNautilusStatus()Lcom/google/android/music/NautilusStatus;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/music/NautilusStatus;->isFreeTrialAvailable()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 86
    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 88
    :cond_1
    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 89
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 90
    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 91
    iget-object v7, p0, Lcom/google/android/music/ui/EmptyLibraryFragment;->mTitle:Landroid/widget/TextView;

    const v8, 0x7f0b02ab

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 38
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 40
    invoke-virtual {p0}, Lcom/google/android/music/ui/EmptyLibraryFragment;->getView()Landroid/view/View;

    move-result-object v0

    .line 41
    .local v0, "root":Landroid/view/View;
    if-nez v0, :cond_0

    .line 42
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Content view not yet created"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 45
    :cond_0
    const v1, 0x7f0e01c6

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    iput-object v1, p0, Lcom/google/android/music/ui/EmptyLibraryFragment;->mList:Landroid/widget/ListView;

    .line 46
    const v1, 0x7f0e01c5

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/music/ui/EmptyLibraryFragment;->mTitle:Landroid/widget/TextView;

    .line 47
    iget-object v1, p0, Lcom/google/android/music/ui/EmptyLibraryFragment;->mTitle:Landroid/widget/TextView;

    const/4 v2, 0x2

    invoke-static {v1, v2}, Lcom/google/android/music/utils/TypefaceUtil;->setTypeface(Landroid/widget/TextView;I)V

    .line 48
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 33
    const v0, 0x7f040085

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onStart()V
    .locals 0

    .prologue
    .line 52
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStart()V

    .line 60
    invoke-direct {p0}, Lcom/google/android/music/ui/EmptyLibraryFragment;->initListView()V

    .line 61
    return-void
.end method
