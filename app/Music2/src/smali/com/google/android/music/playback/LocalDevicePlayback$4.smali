.class Lcom/google/android/music/playback/LocalDevicePlayback$4;
.super Lcom/google/android/music/download/cache/IDeleteFilter$Stub;
.source "LocalDevicePlayback.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/playback/LocalDevicePlayback;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/playback/LocalDevicePlayback;


# direct methods
.method constructor <init>(Lcom/google/android/music/playback/LocalDevicePlayback;)V
    .locals 0

    .prologue
    .line 390
    iput-object p1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$4;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    invoke-direct {p0}, Lcom/google/android/music/download/cache/IDeleteFilter$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public getFilteredIds()[Lcom/google/android/music/download/ContentIdentifier;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 402
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$4;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mStreamingClient:Lcom/google/android/music/download/stream/StreamingClient;
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$800(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/download/stream/StreamingClient;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 403
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$4;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mStreamingClient:Lcom/google/android/music/download/stream/StreamingClient;
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$800(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/download/stream/StreamingClient;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/download/stream/StreamingClient;->getFilteredIds()[Lcom/google/android/music/download/ContentIdentifier;

    move-result-object v0

    .line 405
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public shouldFilter(Ljava/lang/String;)Z
    .locals 1
    .param p1, "fullFilePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 393
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$4;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mStreamingClient:Lcom/google/android/music/download/stream/StreamingClient;
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$800(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/download/stream/StreamingClient;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 394
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$4;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mStreamingClient:Lcom/google/android/music/download/stream/StreamingClient;
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$800(Lcom/google/android/music/playback/LocalDevicePlayback;)Lcom/google/android/music/download/stream/StreamingClient;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/music/download/stream/StreamingClient;->shouldFilter(Ljava/lang/String;)Z

    move-result v0

    .line 396
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
