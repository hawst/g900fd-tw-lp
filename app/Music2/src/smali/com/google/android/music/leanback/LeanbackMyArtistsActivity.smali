.class public Lcom/google/android/music/leanback/LeanbackMyArtistsActivity;
.super Lcom/google/android/music/leanback/LeanbackGridActivity;
.source "LeanbackMyArtistsActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackGridActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected bindCursor(Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;Landroid/database/Cursor;)Ljava/lang/Object;
    .locals 8
    .param p1, "leanbackCursorObjectAdapter"    # Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;
    .param p2, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v7, 0x1

    .line 33
    const/4 v5, 0x0

    invoke-interface {p2, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 34
    .local v2, "id":J
    invoke-interface {p2, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 35
    .local v4, "name":Ljava/lang/String;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 36
    const v5, 0x7f0b00c4

    invoke-virtual {p0, v5}, Lcom/google/android/music/leanback/LeanbackMyArtistsActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 38
    :cond_0
    const/4 v5, 0x3

    invoke-interface {p2, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 39
    .local v0, "artUri":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 40
    const/4 v0, 0x0

    .line 42
    :cond_1
    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    invoke-static {p0, v5}, Lcom/google/android/music/leanback/LeanbackUtils;->getArtistDetailsIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 44
    .local v1, "intent":Landroid/content/Intent;
    new-instance v5, Lcom/google/android/music/leanback/Item$Builder;

    invoke-direct {v5}, Lcom/google/android/music/leanback/Item$Builder;-><init>()V

    invoke-virtual {v5, v4}, Lcom/google/android/music/leanback/Item$Builder;->title(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v5

    new-instance v6, Lcom/google/android/music/leanback/LeanbackMyArtistsActivity$1;

    invoke-direct {v6, p0, v2, v3}, Lcom/google/android/music/leanback/LeanbackMyArtistsActivity$1;-><init>(Lcom/google/android/music/leanback/LeanbackMyArtistsActivity;J)V

    invoke-virtual {v5, v6}, Lcom/google/android/music/leanback/Item$Builder;->description(Lcom/google/android/music/leanback/Item$StringGetter;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v5

    invoke-virtual {v5, v0}, Lcom/google/android/music/leanback/Item$Builder;->iconUri(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v5

    invoke-virtual {v5, v1}, Lcom/google/android/music/leanback/Item$Builder;->intent(Landroid/content/Intent;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v5

    invoke-virtual {v5, v7}, Lcom/google/android/music/leanback/Item$Builder;->wide(Z)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/music/leanback/Item$Builder;->build()Lcom/google/android/music/leanback/Item;

    move-result-object v5

    return-object v5
.end method

.method protected createItemPresenter()Lcom/google/android/music/leanback/ItemPresenter;
    .locals 1

    .prologue
    .line 75
    new-instance v0, Lcom/google/android/music/leanback/ItemPresenter;

    invoke-direct {v0, p0}, Lcom/google/android/music/leanback/ItemPresenter;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method protected getGridTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    const v0, 0x7f0b009e

    invoke-virtual {p0, v0}, Lcom/google/android/music/leanback/LeanbackMyArtistsActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getMediaList()Lcom/google/android/music/medialist/MediaList;
    .locals 1

    .prologue
    .line 22
    new-instance v0, Lcom/google/android/music/medialist/AllArtistsList;

    invoke-direct {v0}, Lcom/google/android/music/medialist/AllArtistsList;-><init>()V

    return-object v0
.end method

.method protected getNumberOfColumns()I
    .locals 1

    .prologue
    .line 70
    const/4 v0, 0x3

    return v0
.end method

.method protected getProjection()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/google/android/music/ui/ArtistGridFragment;->PROJECTION:[Ljava/lang/String;

    return-object v0
.end method
