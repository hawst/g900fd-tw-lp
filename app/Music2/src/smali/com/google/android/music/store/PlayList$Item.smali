.class public Lcom/google/android/music/store/PlayList$Item;
.super Lcom/google/android/music/store/Syncable;
.source "PlayList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/store/PlayList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Item"
.end annotation


# static fields
.field private static final ITEM_PROJECTION:[Ljava/lang/String;

.field private static final ITEM_TOMBSTONE_PROJECTION:[Ljava/lang/String;

.field private static final ORDER_PROJECTION:[Ljava/lang/String;

.field private static ORDER_PROJECTION_INDEX_CLIENT:I

.field private static ORDER_PROJECTION_INDEX_SERVER:I

.field private static TOMBSTONE_PROJECTION_INDEX_ID:I

.field private static TOMBSTONE_PROJECTION_SOURCE_ID:I

.field private static TOMBSTONE_PROJECTION_SOURCE_VERSION:I


# instance fields
.field private mClientId:Ljava/lang/String;

.field private mClientPosition:J

.field private mId:J

.field private mListId:J

.field private mMusicId:J

.field private mPlayGroupId:J

.field private mServerPosition:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1927
    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "LISTITEMS.Id"

    aput-object v1, v0, v3

    const-string v1, "LISTITEMS.ListId"

    aput-object v1, v0, v4

    const-string v1, "LISTITEMS.MusicId"

    aput-object v1, v0, v5

    const-string v1, "LISTITEMS.ClientPosition"

    aput-object v1, v0, v6

    const/4 v1, 0x4

    const-string v2, "LISTITEMS.ServerOrder"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "LISTITEMS._sync_version"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "LISTITEMS._sync_dirty"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "LISTITEMS.SourceAccount"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "LISTITEMS.SourceId"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "LISTITEMS.ClientId"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "LISTITEMS.PlayGroupId"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/store/PlayList$Item;->ITEM_PROJECTION:[Ljava/lang/String;

    .line 1953
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "Id"

    aput-object v1, v0, v3

    const-string v1, "SourceId"

    aput-object v1, v0, v4

    const-string v1, "_sync_version"

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/music/store/PlayList$Item;->ITEM_TOMBSTONE_PROJECTION:[Ljava/lang/String;

    .line 1959
    sput v3, Lcom/google/android/music/store/PlayList$Item;->TOMBSTONE_PROJECTION_INDEX_ID:I

    .line 1960
    sput v4, Lcom/google/android/music/store/PlayList$Item;->TOMBSTONE_PROJECTION_SOURCE_ID:I

    .line 1961
    sput v5, Lcom/google/android/music/store/PlayList$Item;->TOMBSTONE_PROJECTION_SOURCE_VERSION:I

    .line 1963
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "ServerOrder"

    aput-object v1, v0, v3

    const-string v1, "ClientPosition"

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/music/store/PlayList$Item;->ORDER_PROJECTION:[Ljava/lang/String;

    .line 1967
    sput v3, Lcom/google/android/music/store/PlayList$Item;->ORDER_PROJECTION_INDEX_SERVER:I

    .line 1968
    sput v4, Lcom/google/android/music/store/PlayList$Item;->ORDER_PROJECTION_INDEX_CLIENT:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1978
    invoke-direct {p0}, Lcom/google/android/music/store/Syncable;-><init>()V

    .line 1979
    return-void
.end method

.method static synthetic access$000()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 1856
    sget-object v0, Lcom/google/android/music/store/PlayList$Item;->ORDER_PROJECTION:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100()I
    .locals 1

    .prologue
    .line 1856
    sget v0, Lcom/google/android/music/store/PlayList$Item;->ORDER_PROJECTION_INDEX_SERVER:I

    return v0
.end method

.method static synthetic access$200()I
    .locals 1

    .prologue
    .line 1856
    sget v0, Lcom/google/android/music/store/PlayList$Item;->ORDER_PROJECTION_INDEX_CLIENT:I

    return v0
.end method

.method static synthetic access$300()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 1856
    sget-object v0, Lcom/google/android/music/store/PlayList$Item;->ITEM_PROJECTION:[Ljava/lang/String;

    return-object v0
.end method

.method public static compileItemDeleteStatement(Landroid/database/sqlite/SQLiteDatabase;)Landroid/database/sqlite/SQLiteStatement;
    .locals 1
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 2081
    const-string v0, "delete from LISTITEMS where SourceAccount=? AND SourceId=?"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v0

    return-object v0
.end method

.method public static compileItemInsertStatement(Landroid/database/sqlite/SQLiteDatabase;)Landroid/database/sqlite/SQLiteStatement;
    .locals 1
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 2073
    const-string v0, "insert into LISTITEMS ( ListId, MusicId, ClientPosition, ServerOrder, _sync_version, _sync_dirty, SourceAccount, SourceId, ClientId, PlayGroupId) values (?,?,?,?,?,?,?,?,?,?)"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v0

    return-object v0
.end method

.method public static compileItemUpdateStatement(Landroid/database/sqlite/SQLiteDatabase;)Landroid/database/sqlite/SQLiteStatement;
    .locals 1
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 2077
    const-string v0, "update LISTITEMS set ListId=?, MusicId=?, ClientPosition=?, ServerOrder=?, _sync_version=?, _sync_dirty=?,SourceAccount=?,SourceId=?,ClientId=?,PlayGroupId=? where Id=?"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v0

    return-object v0
.end method

.method public static deleteById(Landroid/database/sqlite/SQLiteDatabase;J)V
    .locals 5
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "localListId"    # J

    .prologue
    .line 2321
    const-string v0, "LISTITEMS"

    const-string v1, "Id=?"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2323
    return-void
.end method

.method public static deleteBySourceInfo(Landroid/database/sqlite/SQLiteStatement;ILjava/lang/String;)V
    .locals 4
    .param p0, "deleteStatement"    # Landroid/database/sqlite/SQLiteStatement;
    .param p1, "sourceAccount"    # I
    .param p2, "sourceId"    # Ljava/lang/String;

    .prologue
    .line 2309
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    .line 2311
    const/4 v0, 0x1

    int-to-long v2, p1

    invoke-virtual {p0, v0, v2, v3}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 2312
    const/4 v0, 0x2

    invoke-virtual {p0, v0, p2}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 2314
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteStatement;->execute()V

    .line 2315
    return-void
.end method

.method public static getItemTombstones(Landroid/database/sqlite/SQLiteDatabase;)Landroid/database/Cursor;
    .locals 8
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    const/4 v3, 0x0

    .line 2440
    const-string v1, "LISTITEM_TOMBSTONES"

    sget-object v2, Lcom/google/android/music/store/PlayList$Item;->ITEM_TOMBSTONE_PROJECTION:[Ljava/lang/String;

    move-object v0, p0

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public static getItemsToSync(Landroid/database/sqlite/SQLiteDatabase;)Landroid/database/Cursor;
    .locals 8
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    const/4 v4, 0x0

    .line 2422
    const-string v1, "LISTITEMS JOIN MUSIC ON (LISTITEMS.MusicId=MUSIC.Id) "

    sget-object v2, Lcom/google/android/music/store/PlayList$Item;->ITEM_PROJECTION:[Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "LISTITEMS._sync_dirty=1 AND MUSIC.SourceAccount<>"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v3, Lcom/google/android/music/store/MusicFile;->MEDIA_STORE_SOURCE_ACCOUNT_AS_STRING:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v7, "ServerOrder, ClientPosition"

    move-object v0, p0

    move-object v5, v4

    move-object v6, v4

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method private prepareInsertOrFullUpdate(Landroid/database/sqlite/SQLiteStatement;)V
    .locals 7
    .param p1, "statement"    # Landroid/database/sqlite/SQLiteStatement;

    .prologue
    const/16 v6, 0x9

    const/16 v5, 0x8

    const/4 v4, 0x5

    .line 2085
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    .line 2087
    const/4 v0, 0x1

    iget-wide v2, p0, Lcom/google/android/music/store/PlayList$Item;->mListId:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 2088
    const/4 v0, 0x3

    iget-wide v2, p0, Lcom/google/android/music/store/PlayList$Item;->mClientPosition:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 2089
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/music/store/PlayList$Item;->mServerPosition:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 2091
    const/4 v0, 0x2

    iget-wide v2, p0, Lcom/google/android/music/store/PlayList$Item;->mMusicId:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 2093
    const/4 v2, 0x6

    iget-boolean v0, p0, Lcom/google/android/music/store/PlayList$Item;->mNeedsSync:Z

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x1

    :goto_0
    invoke-virtual {p1, v2, v0, v1}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 2096
    iget-object v0, p0, Lcom/google/android/music/store/PlayList$Item;->mSourceVersion:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 2097
    invoke-virtual {p1, v4}, Landroid/database/sqlite/SQLiteStatement;->bindNull(I)V

    .line 2102
    :goto_1
    const/4 v0, 0x7

    iget v1, p0, Lcom/google/android/music/store/PlayList$Item;->mSourceAccount:I

    int-to-long v2, v1

    invoke-virtual {p1, v0, v2, v3}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 2103
    iget-object v0, p0, Lcom/google/android/music/store/PlayList$Item;->mSourceId:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 2104
    invoke-virtual {p1, v5}, Landroid/database/sqlite/SQLiteStatement;->bindNull(I)V

    .line 2108
    :goto_2
    iget-object v0, p0, Lcom/google/android/music/store/PlayList$Item;->mClientId:Ljava/lang/String;

    if-nez v0, :cond_3

    .line 2109
    invoke-virtual {p1, v6}, Landroid/database/sqlite/SQLiteStatement;->bindNull(I)V

    .line 2113
    :goto_3
    const/16 v0, 0xa

    iget-wide v2, p0, Lcom/google/android/music/store/PlayList$Item;->mPlayGroupId:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 2114
    return-void

    .line 2093
    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0

    .line 2099
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/store/PlayList$Item;->mSourceVersion:Ljava/lang/String;

    invoke-virtual {p1, v4, v0}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    goto :goto_1

    .line 2106
    :cond_2
    iget-object v0, p0, Lcom/google/android/music/store/PlayList$Item;->mSourceId:Ljava/lang/String;

    invoke-virtual {p1, v5, v0}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    goto :goto_2

    .line 2111
    :cond_3
    iget-object v0, p0, Lcom/google/android/music/store/PlayList$Item;->mClientId:Ljava/lang/String;

    invoke-virtual {p1, v6, v0}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    goto :goto_3
.end method

.method public static readItem(Landroid/database/sqlite/SQLiteDatabase;JLcom/google/android/music/store/PlayList$Item;)Lcom/google/android/music/store/PlayList$Item;
    .locals 11
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "localItemId"    # J
    .param p3, "item"    # Lcom/google/android/music/store/PlayList$Item;

    .prologue
    const/4 v5, 0x0

    .line 2333
    const-string v1, "LISTITEMS"

    sget-object v2, Lcom/google/android/music/store/PlayList$Item;->ITEM_PROJECTION:[Ljava/lang/String;

    const-string v3, "Id=?"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v0

    move-object v0, p0

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 2339
    .local v8, "c":Landroid/database/Cursor;
    if-eqz v8, :cond_1

    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2340
    if-nez p3, :cond_0

    .line 2341
    new-instance v9, Lcom/google/android/music/store/PlayList$Item;

    invoke-direct {v9}, Lcom/google/android/music/store/PlayList$Item;-><init>()V

    .end local p3    # "item":Lcom/google/android/music/store/PlayList$Item;
    .local v9, "item":Lcom/google/android/music/store/PlayList$Item;
    move-object p3, v9

    .line 2343
    .end local v9    # "item":Lcom/google/android/music/store/PlayList$Item;
    .restart local p3    # "item":Lcom/google/android/music/store/PlayList$Item;
    :cond_0
    invoke-virtual {p3, v8}, Lcom/google/android/music/store/PlayList$Item;->populateFromFullProjectionCursor(Landroid/database/Cursor;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2349
    invoke-static {v8}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    move-object v5, p3

    :goto_0
    return-object v5

    :cond_1
    invoke-static {v8}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v8}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method public static readItem(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/music/store/PlayList$Item;)Lcom/google/android/music/store/PlayList$Item;
    .locals 10
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "sourceAccount"    # Ljava/lang/String;
    .param p2, "sourceId"    # Ljava/lang/String;
    .param p3, "item"    # Lcom/google/android/music/store/PlayList$Item;

    .prologue
    const/4 v5, 0x0

    .line 2361
    const-string v1, "LISTITEMS"

    sget-object v2, Lcom/google/android/music/store/PlayList$Item;->ITEM_PROJECTION:[Ljava/lang/String;

    const-string v3, "SourceAccount=? AND SourceId=?"

    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p1, v4, v0

    const/4 v0, 0x1

    aput-object p2, v4, v0

    move-object v0, p0

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 2368
    .local v8, "c":Landroid/database/Cursor;
    if-eqz v8, :cond_1

    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2369
    if-nez p3, :cond_0

    .line 2370
    new-instance v9, Lcom/google/android/music/store/PlayList$Item;

    invoke-direct {v9}, Lcom/google/android/music/store/PlayList$Item;-><init>()V

    .end local p3    # "item":Lcom/google/android/music/store/PlayList$Item;
    .local v9, "item":Lcom/google/android/music/store/PlayList$Item;
    move-object p3, v9

    .line 2372
    .end local v9    # "item":Lcom/google/android/music/store/PlayList$Item;
    .restart local p3    # "item":Lcom/google/android/music/store/PlayList$Item;
    :cond_0
    invoke-virtual {p3, v8}, Lcom/google/android/music/store/PlayList$Item;->populateFromFullProjectionCursor(Landroid/database/Cursor;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2378
    invoke-static {v8}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    move-object v5, p3

    :goto_0
    return-object v5

    :cond_1
    invoke-static {v8}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v8}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method


# virtual methods
.method delete(Landroid/database/sqlite/SQLiteDatabase;Z)Z
    .locals 8
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "sync"    # Z

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x1

    .line 2392
    iget-wide v4, p0, Lcom/google/android/music/store/PlayList$Item;->mId:J

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-nez v3, :cond_0

    .line 2393
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "Cannot delete object that was not loaded or created"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 2397
    :cond_0
    const-string v3, "LISTITEMS"

    const-string v4, "Id=?"

    new-array v5, v2, [Ljava/lang/String;

    iget-wide v6, p0, Lcom/google/android/music/store/PlayList$Item;->mId:J

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v0

    invoke-virtual {p1, v3, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 2401
    .local v1, "numberOfDeletedLists":I
    if-lez v1, :cond_1

    move v0, v2

    .line 2402
    .local v0, "deleted":Z
    :cond_1
    if-le v1, v2, :cond_2

    .line 2403
    const-string v2, "PlayList"

    const-string v3, "Deleted multiple objects"

    invoke-static {v2, v3}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 2406
    :cond_2
    if-eqz p2, :cond_3

    if-eqz v0, :cond_3

    .line 2407
    const-string v2, "LISTITEM_TOMBSTONES"

    invoke-virtual {p0, p1, v2}, Lcom/google/android/music/store/PlayList$Item;->createTombstoneIfNeeded(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Z

    .line 2410
    :cond_3
    return v0
.end method

.method public findFollowingItem(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/music/store/PlayList$Item;I)Lcom/google/android/music/store/PlayList$Item;
    .locals 11
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "item"    # Lcom/google/android/music/store/PlayList$Item;
    .param p3, "accountId"    # I

    .prologue
    const-wide/16 v2, 0x0

    const/4 v5, 0x0

    .line 2249
    iget-wide v0, p0, Lcom/google/android/music/store/PlayList$Item;->mId:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/music/store/PlayList$Item;->mListId:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 2284
    :cond_0
    :goto_0
    return-object v5

    .line 2253
    :cond_1
    const/4 v0, 0x5

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    iget-wide v2, p0, Lcom/google/android/music/store/PlayList$Item;->mListId:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/music/store/PlayList$Item;->mServerPosition:Ljava/lang/String;

    aput-object v1, v4, v0

    const/4 v0, 0x2

    iget-wide v2, p0, Lcom/google/android/music/store/PlayList$Item;->mClientPosition:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/music/store/PlayList$Item;->mServerPosition:Ljava/lang/String;

    aput-object v1, v4, v0

    const/4 v0, 0x4

    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    .line 2262
    .local v4, "selectionArgs":[Ljava/lang/String;
    const-string v1, "LISTITEMS JOIN MUSIC ON (LISTITEMS.MusicId=MUSIC.Id) "

    sget-object v2, Lcom/google/android/music/store/PlayList$Item;->ITEM_PROJECTION:[Ljava/lang/String;

    const-string v3, "LISTITEMS.ListId=? AND ((LISTITEMS.ServerOrder=? AND LISTITEMS.ClientPosition>?) OR LISTITEMS.ServerOrder>?) AND MUSIC.SourceAccount=?"

    const-string v7, "ServerOrder, ClientPosition"

    const-string v8, "1"

    move-object v0, p1

    move-object v6, v5

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 2273
    .local v9, "c":Landroid/database/Cursor;
    if-eqz v9, :cond_3

    :try_start_0
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2274
    if-nez p2, :cond_2

    .line 2275
    new-instance v10, Lcom/google/android/music/store/PlayList$Item;

    invoke-direct {v10}, Lcom/google/android/music/store/PlayList$Item;-><init>()V

    .end local p2    # "item":Lcom/google/android/music/store/PlayList$Item;
    .local v10, "item":Lcom/google/android/music/store/PlayList$Item;
    move-object p2, v10

    .line 2277
    .end local v10    # "item":Lcom/google/android/music/store/PlayList$Item;
    .restart local p2    # "item":Lcom/google/android/music/store/PlayList$Item;
    :cond_2
    invoke-virtual {p2, v9}, Lcom/google/android/music/store/PlayList$Item;->populateFromFullProjectionCursor(Landroid/database/Cursor;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2281
    invoke-static {v9}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    move-object v5, p2

    goto :goto_0

    :cond_3
    invoke-static {v9}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v9}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method public findPrecedingItem(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/music/store/PlayList$Item;I)Lcom/google/android/music/store/PlayList$Item;
    .locals 11
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "item"    # Lcom/google/android/music/store/PlayList$Item;
    .param p3, "accountId"    # I

    .prologue
    const-wide/16 v2, 0x0

    const/4 v5, 0x0

    .line 2202
    iget-wide v0, p0, Lcom/google/android/music/store/PlayList$Item;->mId:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/music/store/PlayList$Item;->mListId:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 2237
    :cond_0
    :goto_0
    return-object v5

    .line 2206
    :cond_1
    const/4 v0, 0x5

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    iget-wide v2, p0, Lcom/google/android/music/store/PlayList$Item;->mListId:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/music/store/PlayList$Item;->mServerPosition:Ljava/lang/String;

    aput-object v1, v4, v0

    const/4 v0, 0x2

    iget-wide v2, p0, Lcom/google/android/music/store/PlayList$Item;->mClientPosition:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/music/store/PlayList$Item;->mServerPosition:Ljava/lang/String;

    aput-object v1, v4, v0

    const/4 v0, 0x4

    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    .line 2215
    .local v4, "selectionArgs":[Ljava/lang/String;
    const-string v1, "LISTITEMS JOIN MUSIC ON (LISTITEMS.MusicId=MUSIC.Id) "

    sget-object v2, Lcom/google/android/music/store/PlayList$Item;->ITEM_PROJECTION:[Ljava/lang/String;

    const-string v3, "LISTITEMS.ListId=? AND ((LISTITEMS.ServerOrder=? AND LISTITEMS.ClientPosition<?) OR LISTITEMS.ServerOrder<?) AND MUSIC.SourceAccount=?"

    const-string v7, "ServerOrder DESC, ClientPosition DESC"

    const-string v8, "1"

    move-object v0, p1

    move-object v6, v5

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 2226
    .local v9, "c":Landroid/database/Cursor;
    if-eqz v9, :cond_3

    :try_start_0
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2227
    if-nez p2, :cond_2

    .line 2228
    new-instance v10, Lcom/google/android/music/store/PlayList$Item;

    invoke-direct {v10}, Lcom/google/android/music/store/PlayList$Item;-><init>()V

    .end local p2    # "item":Lcom/google/android/music/store/PlayList$Item;
    .local v10, "item":Lcom/google/android/music/store/PlayList$Item;
    move-object p2, v10

    .line 2230
    .end local v10    # "item":Lcom/google/android/music/store/PlayList$Item;
    .restart local p2    # "item":Lcom/google/android/music/store/PlayList$Item;
    :cond_2
    invoke-virtual {p2, v9}, Lcom/google/android/music/store/PlayList$Item;->populateFromFullProjectionCursor(Landroid/database/Cursor;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2234
    invoke-static {v9}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    move-object v5, p2

    goto :goto_0

    :cond_3
    invoke-static {v9}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v9}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method public final getClientId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2029
    iget-object v0, p0, Lcom/google/android/music/store/PlayList$Item;->mClientId:Ljava/lang/String;

    return-object v0
.end method

.method public final getClientPosition()J
    .locals 2

    .prologue
    .line 2045
    iget-wide v0, p0, Lcom/google/android/music/store/PlayList$Item;->mClientPosition:J

    return-wide v0
.end method

.method public final getId()J
    .locals 2

    .prologue
    .line 2013
    iget-wide v0, p0, Lcom/google/android/music/store/PlayList$Item;->mId:J

    return-wide v0
.end method

.method public final getListId()J
    .locals 2

    .prologue
    .line 2037
    iget-wide v0, p0, Lcom/google/android/music/store/PlayList$Item;->mListId:J

    return-wide v0
.end method

.method public final getMusicId()J
    .locals 2

    .prologue
    .line 2021
    iget-wide v0, p0, Lcom/google/android/music/store/PlayList$Item;->mMusicId:J

    return-wide v0
.end method

.method public insertItem(Landroid/database/sqlite/SQLiteStatement;)J
    .locals 6
    .param p1, "insertStatement"    # Landroid/database/sqlite/SQLiteStatement;

    .prologue
    const-wide/16 v4, 0x0

    .line 2166
    iget-wide v2, p0, Lcom/google/android/music/store/PlayList$Item;->mId:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    .line 2167
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "The local id of a playlist item must not be set for an insert."

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 2170
    :cond_0
    iget-wide v2, p0, Lcom/google/android/music/store/PlayList$Item;->mListId:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_1

    .line 2171
    new-instance v2, Lcom/google/android/music/store/InvalidDataException;

    const-string v3, "The list id of a playlist item must be set for an insert."

    invoke-direct {v2, v3}, Lcom/google/android/music/store/InvalidDataException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 2174
    :cond_1
    iget-wide v2, p0, Lcom/google/android/music/store/PlayList$Item;->mMusicId:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    .line 2175
    new-instance v2, Lcom/google/android/music/store/InvalidDataException;

    const-string v3, "The music id of a playlist item must be set for an insert."

    invoke-direct {v2, v3}, Lcom/google/android/music/store/InvalidDataException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 2178
    :cond_2
    iget-object v2, p0, Lcom/google/android/music/store/PlayList$Item;->mClientId:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 2179
    invoke-static {}, Lcom/google/android/music/store/Store;->generateClientId()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/store/PlayList$Item;->mClientId:Ljava/lang/String;

    .line 2182
    :cond_3
    invoke-direct {p0, p1}, Lcom/google/android/music/store/PlayList$Item;->prepareInsertOrFullUpdate(Landroid/database/sqlite/SQLiteStatement;)V

    .line 2183
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteStatement;->executeInsert()J

    move-result-wide v0

    .line 2184
    .local v0, "insertedId":J
    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-nez v2, :cond_4

    .line 2185
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "Failed to insert into LISTS"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 2187
    :cond_4
    iput-wide v0, p0, Lcom/google/android/music/store/PlayList$Item;->mId:J

    .line 2190
    iget-wide v2, p0, Lcom/google/android/music/store/PlayList$Item;->mId:J

    return-wide v2
.end method

.method public populateFromFullProjectionCursor(Landroid/database/Cursor;)V
    .locals 6
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    const/4 v5, 0x7

    const/4 v4, 0x5

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2117
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/music/store/PlayList$Item;->mId:J

    .line 2118
    invoke-interface {p1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2119
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/google/android/music/store/PlayList$Item;->mListId:J

    .line 2124
    :goto_0
    const/4 v2, 0x2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/music/store/PlayList$Item;->mMusicId:J

    .line 2126
    const/4 v2, 0x3

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/music/store/PlayList$Item;->mClientPosition:J

    .line 2127
    const/4 v2, 0x4

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/store/PlayList$Item;->mServerPosition:Ljava/lang/String;

    .line 2129
    invoke-interface {p1, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2130
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/music/store/PlayList$Item;->mSourceVersion:Ljava/lang/String;

    .line 2134
    :goto_1
    const/4 v2, 0x6

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-ne v2, v0, :cond_2

    :goto_2
    iput-boolean v0, p0, Lcom/google/android/music/store/PlayList$Item;->mNeedsSync:Z

    .line 2136
    invoke-interface {p1, v5}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2137
    iput v1, p0, Lcom/google/android/music/store/PlayList$Item;->mSourceAccount:I

    .line 2142
    :goto_3
    const/16 v0, 0x8

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/store/PlayList$Item;->mSourceId:Ljava/lang/String;

    .line 2144
    const/16 v0, 0x9

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/store/PlayList$Item;->mClientId:Ljava/lang/String;

    .line 2145
    const/16 v0, 0xa

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/music/store/PlayList$Item;->mPlayGroupId:J

    .line 2146
    return-void

    .line 2121
    :cond_0
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/music/store/PlayList$Item;->mListId:J

    goto :goto_0

    .line 2132
    :cond_1
    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/store/PlayList$Item;->mSourceVersion:Ljava/lang/String;

    goto :goto_1

    :cond_2
    move v0, v1

    .line 2134
    goto :goto_2

    .line 2139
    :cond_3
    invoke-interface {p1, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/store/PlayList$Item;->mSourceAccount:I

    goto :goto_3
.end method

.method public populateFromTombstoneProjectionCursor(Landroid/database/Cursor;)V
    .locals 2
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    .line 2149
    sget v0, Lcom/google/android/music/store/PlayList$Item;->TOMBSTONE_PROJECTION_INDEX_ID:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/music/store/PlayList$Item;->mId:J

    .line 2150
    sget v0, Lcom/google/android/music/store/PlayList$Item;->TOMBSTONE_PROJECTION_SOURCE_ID:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2151
    # getter for: Lcom/google/android/music/store/PlayList;->TOMBSTONE_PROJECTION_SOURCE_ID:I
    invoke-static {}, Lcom/google/android/music/store/PlayList;->access$400()I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/store/PlayList$Item;->mSourceId:Ljava/lang/String;

    .line 2153
    :cond_0
    sget v0, Lcom/google/android/music/store/PlayList$Item;->TOMBSTONE_PROJECTION_SOURCE_VERSION:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2154
    # getter for: Lcom/google/android/music/store/PlayList;->TOMBSTONE_PROJECTION_SOURCE_VERSION:I
    invoke-static {}, Lcom/google/android/music/store/PlayList;->access$500()I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/store/PlayList$Item;->mSourceVersion:Ljava/lang/String;

    .line 2156
    :cond_1
    return-void
.end method

.method public reset()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 1982
    invoke-super {p0}, Lcom/google/android/music/store/Syncable;->reset()V

    .line 1983
    iput-wide v2, p0, Lcom/google/android/music/store/PlayList$Item;->mId:J

    .line 1984
    iput-wide v2, p0, Lcom/google/android/music/store/PlayList$Item;->mMusicId:J

    .line 1985
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/store/PlayList$Item;->mClientId:Ljava/lang/String;

    .line 1986
    iput-wide v2, p0, Lcom/google/android/music/store/PlayList$Item;->mListId:J

    .line 1987
    const-wide/32 v0, -0x80000000

    iput-wide v0, p0, Lcom/google/android/music/store/PlayList$Item;->mClientPosition:J

    .line 1988
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/music/store/PlayList$Item;->mServerPosition:Ljava/lang/String;

    .line 1989
    iput-wide v2, p0, Lcom/google/android/music/store/PlayList$Item;->mPlayGroupId:J

    .line 1990
    return-void
.end method

.method public final setClientPosition(J)V
    .locals 1
    .param p1, "position"    # J

    .prologue
    .line 2049
    iput-wide p1, p0, Lcom/google/android/music/store/PlayList$Item;->mClientPosition:J

    .line 2050
    return-void
.end method

.method public final setListId(J)V
    .locals 1
    .param p1, "listId"    # J

    .prologue
    .line 2041
    iput-wide p1, p0, Lcom/google/android/music/store/PlayList$Item;->mListId:J

    .line 2042
    return-void
.end method

.method public final setMusicId(J)V
    .locals 1
    .param p1, "musicId"    # J

    .prologue
    .line 2025
    iput-wide p1, p0, Lcom/google/android/music/store/PlayList$Item;->mMusicId:J

    .line 2026
    return-void
.end method

.method public final setPlayGroupId(J)V
    .locals 1
    .param p1, "id"    # J

    .prologue
    .line 2068
    iput-wide p1, p0, Lcom/google/android/music/store/PlayList$Item;->mPlayGroupId:J

    .line 2069
    return-void
.end method

.method public final setServerPosition(Ljava/lang/String;)V
    .locals 2
    .param p1, "serverPosition"    # Ljava/lang/String;

    .prologue
    .line 2057
    if-nez p1, :cond_0

    .line 2058
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Server position cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2060
    :cond_0
    iput-object p1, p0, Lcom/google/android/music/store/PlayList$Item;->mServerPosition:Ljava/lang/String;

    .line 2061
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1994
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1995
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1996
    const-string v1, "mId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/music/store/PlayList$Item;->mId:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1997
    const-string v1, "mMusicId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/music/store/PlayList$Item;->mMusicId:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1998
    const-string v1, "mClientId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/store/PlayList$Item;->mClientId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1999
    const-string v1, "mListId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/music/store/PlayList$Item;->mListId:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2000
    const-string v1, "mClientPosition="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/music/store/PlayList$Item;->mClientPosition:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2001
    const-string v1, "mServerPosition="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/store/PlayList$Item;->mServerPosition:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2002
    const-string v1, "mPlayGroupId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/music/store/PlayList$Item;->mPlayGroupId:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2003
    const-string v1, "mSourceAccount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/music/store/PlayList$Item;->mSourceAccount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2004
    const-string v1, "mSourceVersion="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/store/PlayList$Item;->mSourceVersion:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2005
    const-string v1, "mSourceId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/store/PlayList$Item;->mSourceId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2006
    const-string v1, "mNeedsSync="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/music/store/PlayList$Item;->mNeedsSync:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 2007
    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2009
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public update(Landroid/database/sqlite/SQLiteStatement;)V
    .locals 4
    .param p1, "updateStatement"    # Landroid/database/sqlite/SQLiteStatement;

    .prologue
    .line 2293
    iget-wide v0, p0, Lcom/google/android/music/store/PlayList$Item;->mId:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 2294
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Object cannot be updated before it\'s created"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2297
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/music/store/PlayList$Item;->prepareInsertOrFullUpdate(Landroid/database/sqlite/SQLiteStatement;)V

    .line 2298
    const/16 v0, 0xb

    iget-wide v2, p0, Lcom/google/android/music/store/PlayList$Item;->mId:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 2299
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteStatement;->execute()V

    .line 2300
    return-void
.end method
