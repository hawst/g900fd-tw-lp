.class Lcom/google/android/music/store/MainstageContentProviderHelper;
.super Ljava/lang/Object;
.source "MainstageContentProviderHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;
    }
.end annotation


# static fields
.field private static final LOGV:Z

.field private static sCategoryMapping:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;",
            ">;"
        }
    .end annotation
.end field

.field private static final sCategoryMappingLock:Ljava/lang/Object;

.field private static sRecentProjectionMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sRecommendationsProjectionMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sSuggestedMixesProjectionMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    .line 40
    sget-object v2, Lcom/google/android/music/utils/DebugUtils$MusicTag;->CONTENT_PROVIDER:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v2}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v2

    sput-boolean v2, Lcom/google/android/music/store/MainstageContentProviderHelper;->LOGV:Z

    .line 47
    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    sput-object v2, Lcom/google/android/music/store/MainstageContentProviderHelper;->sCategoryMappingLock:Ljava/lang/Object;

    .line 55
    sget-object v2, Lcom/google/android/music/store/MusicContent;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "albumorfauxart"

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 59
    .local v0, "baseAlbumOrFauxArtUri":Landroid/net/Uri;
    sget-object v2, Lcom/google/android/music/store/MusicContent;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "playlistfauxart"

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 63
    .local v1, "basePlaylistFauxArtUri":Landroid/net/Uri;
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    sput-object v2, Lcom/google/android/music/store/MainstageContentProviderHelper;->sRecentProjectionMap:Ljava/util/HashMap;

    .line 65
    sget-object v2, Lcom/google/android/music/store/MainstageContentProviderHelper;->sRecentProjectionMap:Ljava/util/HashMap;

    invoke-static {v2}, Lcom/google/android/music/store/MusicContentProvider;->addNullAlbumMappings(Ljava/util/HashMap;)V

    .line 66
    sget-object v2, Lcom/google/android/music/store/MainstageContentProviderHelper;->sRecentProjectionMap:Ljava/util/HashMap;

    invoke-static {v2}, Lcom/google/android/music/store/MusicContentProvider;->addDefaultPlaylistMappings(Ljava/util/HashMap;)V

    .line 67
    sget-object v2, Lcom/google/android/music/store/MainstageContentProviderHelper;->sRecentProjectionMap:Ljava/util/HashMap;

    const-string v3, "count(*)"

    invoke-static {v2, v3}, Lcom/google/android/music/store/MusicContentProvider;->addExistsAndCountMapping(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 69
    sget-object v2, Lcom/google/android/music/store/MainstageContentProviderHelper;->sRecentProjectionMap:Ljava/util/HashMap;

    const-string v3, "playlist_id"

    const-string v4, "RecentListId"

    invoke-static {v2, v3, v4}, Lcom/google/android/music/store/MainstageContentProviderHelper;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    sget-object v2, Lcom/google/android/music/store/MainstageContentProviderHelper;->sRecentProjectionMap:Ljava/util/HashMap;

    const-string v3, "RecentReason"

    const-string v4, "RecentReason"

    invoke-static {v2, v3, v4}, Lcom/google/android/music/store/MainstageContentProviderHelper;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    sget-object v2, Lcom/google/android/music/store/MainstageContentProviderHelper;->sRecentProjectionMap:Ljava/util/HashMap;

    const-string v3, "_id"

    const-string v4, "RecentId"

    invoke-static {v2, v3, v4}, Lcom/google/android/music/store/MainstageContentProviderHelper;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    sget-object v2, Lcom/google/android/music/store/MainstageContentProviderHelper;->sRecentProjectionMap:Ljava/util/HashMap;

    const-string v3, "album_id"

    const-string v4, "RecentAlbumId"

    invoke-static {v2, v3, v4}, Lcom/google/android/music/store/MainstageContentProviderHelper;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    sget-object v2, Lcom/google/android/music/store/MainstageContentProviderHelper;->sRecentProjectionMap:Ljava/util/HashMap;

    const-string v3, "album_artist_id"

    const-string v4, "AlbumArtistId"

    invoke-static {v2, v3, v4}, Lcom/google/android/music/store/MainstageContentProviderHelper;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    sget-object v2, Lcom/google/android/music/store/MainstageContentProviderHelper;->sRecentProjectionMap:Ljava/util/HashMap;

    const-string v3, "StoreAlbumId"

    const-string v4, "RecentNautilusAlbumId"

    invoke-static {v2, v3, v4}, Lcom/google/android/music/store/MainstageContentProviderHelper;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    sget-object v2, Lcom/google/android/music/store/MainstageContentProviderHelper;->sRecentProjectionMap:Ljava/util/HashMap;

    const-string v3, "ArtistMetajamId"

    const-string v4, "RecentNautilusAlbumArtistId"

    invoke-static {v2, v3, v4}, Lcom/google/android/music/store/MainstageContentProviderHelper;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    sget-object v2, Lcom/google/android/music/store/MainstageContentProviderHelper;->sRecentProjectionMap:Ljava/util/HashMap;

    const-string v3, "artworkUrl"

    const-string v4, "RecentNautilusAlbumArt"

    invoke-static {v2, v3, v4}, Lcom/google/android/music/store/MainstageContentProviderHelper;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    const-string v2, "album_name"

    const-string v3, "Album"

    const-string v4, "RecentNautilusAlbum"

    invoke-static {v2, v3, v4}, Lcom/google/android/music/store/MainstageContentProviderHelper;->addLocalAndNautilusAlbumMapping(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    const-string v2, "album_artist"

    const-string v3, "MUSIC.AlbumArtist"

    const-string v4, "RecentNautilusAlbumArtist"

    invoke-static {v2, v3, v4}, Lcom/google/android/music/store/MainstageContentProviderHelper;->addLocalAndNautilusAlbumMapping(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    const-string v2, "album_artist_sort"

    const-string v3, "CanonicalAlbumArtist"

    const-string v4, "RecentNautilusAlbumArtist"

    invoke-static {v2, v3, v4}, Lcom/google/android/music/store/MainstageContentProviderHelper;->addLocalAndNautilusAlbumMapping(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    const-string v2, "hasPersistNautilus"

    const-string v3, "EXISTS(select 1 from MUSIC WHERE AlbumId=RecentAlbumId AND TrackType=5 LIMIT 1)"

    const-string v4, "0"

    invoke-static {v2, v3, v4}, Lcom/google/android/music/store/MainstageContentProviderHelper;->addLocalAndNautilusAlbumMapping(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    const-string v2, "StoreAlbumId"

    const-string v3, "max(StoreAlbumId)"

    const-string v4, "RecentNautilusAlbumId"

    invoke-static {v2, v3, v4}, Lcom/google/android/music/store/MainstageContentProviderHelper;->addLocalAndNautilusAlbumMapping(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    const-string v2, "hasLocal"

    const-string v3, "0"

    const-string v4, "EXISTS(select 1 from MUSIC WHERE AlbumId=RecentAlbumId AND LocalCopyType IN (100,200,300) LIMIT 1)"

    const-string v5, "EXISTS(select 1 from LISTITEMS JOIN MUSIC ON (LISTITEMS.MusicId=MUSIC.Id)  WHERE ListId=RecentListId AND LocalCopyType IN (100,200,300) LIMIT 1)"

    const-string v6, "EXISTS(select 1 from RADIO_SONGS JOIN MUSIC ON (RADIO_SONGS.MusicId=MUSIC.Id)  WHERE RadioStationId=RecentRadioId AND LocalCopyType IN (100,200,300) LIMIT 1)"

    invoke-static {v2, v3, v4, v5, v6}, Lcom/google/android/music/store/MainstageContentProviderHelper;->addOfflineMapping(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    const-string v2, "hasRemote"

    const-string v3, "1"

    const-string v4, "EXISTS(select 1 from MUSIC WHERE AlbumId=RecentAlbumId AND LocalCopyType != 300 LIMIT 1)"

    const-string v5, "EXISTS(select 1 from LISTITEMS JOIN MUSIC ON (LISTITEMS.MusicId=MUSIC.Id)  WHERE ListId=RecentListId AND LocalCopyType != 300 LIMIT 1)"

    const-string v6, "1"

    invoke-static {v2, v3, v4, v5, v6}, Lcom/google/android/music/store/MainstageContentProviderHelper;->addOfflineMapping(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    const-string v2, "KeepOnId"

    const-string v3, "null"

    const-string v4, "(select KeepOnId from KEEPON WHERE AlbumId=RecentAlbumId LIMIT 1)"

    const-string v5, "(select KeepOnId from KEEPON WHERE ListId=RecentListId LIMIT 1)"

    const-string v6, "(select KeepOnId from KEEPON WHERE RadioStationId=RecentRadioId LIMIT 1)"

    invoke-static {v2, v3, v4, v5, v6}, Lcom/google/android/music/store/MainstageContentProviderHelper;->addOfflineMapping(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    const-string v2, "isAllLocal"

    const-string v3, "0"

    const-string v4, "NOT EXISTS(select 1 from MUSIC AS m  WHERE MUSIC.AlbumId=m.AlbumId GROUP BY m.SongId HAVING MAX(m.LocalCopyType = 0) LIMIT 1)"

    const-string v5, "NOT EXISTS(select 1 from MUSIC AS m, LISTITEMS as i WHERE i.ListId=LISTS.Id AND m.Id=i.MusicId GROUP BY m.SongId HAVING MAX(m.LocalCopyType = 0) LIMIT 1)"

    const-string v6, "0"

    invoke-static {v2, v3, v4, v5, v6}, Lcom/google/android/music/store/MainstageContentProviderHelper;->addOfflineMapping(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    sget-object v2, Lcom/google/android/music/store/MainstageContentProviderHelper;->sRecentProjectionMap:Ljava/util/HashMap;

    invoke-static {v2}, Lcom/google/android/music/store/MusicContentProvider;->addDefaultRadioMappings(Ljava/util/HashMap;)V

    .line 156
    sget-object v2, Lcom/google/android/music/store/MainstageContentProviderHelper;->sRecentProjectionMap:Ljava/util/HashMap;

    const-string v3, "radio_id"

    const-string v4, "RADIO_STATIONS.Id"

    invoke-static {v2, v3, v4}, Lcom/google/android/music/store/MainstageContentProviderHelper;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    sget-object v2, Lcom/google/android/music/store/MainstageContentProviderHelper;->sRecentProjectionMap:Ljava/util/HashMap;

    const-string v3, "ItemDate"

    const-string v4, "ItemDate"

    invoke-static {v2, v3, v4}, Lcom/google/android/music/store/MainstageContentProviderHelper;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    sget-object v2, Lcom/google/android/music/store/MainstageContentProviderHelper;->sRecentProjectionMap:Ljava/util/HashMap;

    const-string v3, "RecentReason"

    const-string v4, "RecentReason"

    invoke-static {v2, v3, v4}, Lcom/google/android/music/store/MainstageContentProviderHelper;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    sget-object v2, Lcom/google/android/music/store/MainstageContentProviderHelper;->sRecentProjectionMap:Ljava/util/HashMap;

    const-string v3, "suggest_text_1"

    const-string v4, "Album"

    const-string v5, "LISTS.Name"

    invoke-static {v2, v3, v4, v5}, Lcom/google/android/music/store/MainstageContentProviderHelper;->addRecentAlbumOrPlaylistMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    sget-object v2, Lcom/google/android/music/store/MainstageContentProviderHelper;->sRecentProjectionMap:Ljava/util/HashMap;

    const-string v3, "suggest_text_2"

    const-string v4, "AlbumArtist"

    invoke-static {v2, v3, v4}, Lcom/google/android/music/store/MainstageContentProviderHelper;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    sget-object v2, Lcom/google/android/music/store/MainstageContentProviderHelper;->sRecentProjectionMap:Ljava/util/HashMap;

    const-string v3, "suggest_icon_1"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/\' || "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "RecentAlbumId"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/\' || "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "RecentListId"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v3, v4, v5}, Lcom/google/android/music/store/MainstageContentProviderHelper;->addRecentAlbumOrPlaylistMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    sget-object v2, Lcom/google/android/music/store/MainstageContentProviderHelper;->sRecentProjectionMap:Ljava/util/HashMap;

    const-string v3, "suggest_icon_large"

    const-string v4, "null"

    invoke-static {v2, v3, v4}, Lcom/google/android/music/store/MainstageContentProviderHelper;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    sget-object v2, Lcom/google/android/music/store/MainstageContentProviderHelper;->sRecentProjectionMap:Ljava/util/HashMap;

    const-string v3, "suggest_intent_data"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/google/android/music/ui/SearchActivity;->SUGGEST_DATA_ALBUM:Landroid/net/Uri;

    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/google/android/music/ui/SearchActivity;->SUGGEST_DATA_PLAYLIST:Landroid/net/Uri;

    invoke-virtual {v6}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v3, v4, v5}, Lcom/google/android/music/store/MainstageContentProviderHelper;->addRecentAlbumOrPlaylistMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    sget-object v2, Lcom/google/android/music/store/MainstageContentProviderHelper;->sRecentProjectionMap:Ljava/util/HashMap;

    const-string v3, "suggest_intent_data_id"

    const-string v4, "RecentAlbumId"

    const-string v5, "RecentListId"

    invoke-static {v2, v3, v4, v5}, Lcom/google/android/music/store/MainstageContentProviderHelper;->addRecentAlbumOrPlaylistMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    sget-object v2, Lcom/google/android/music/store/MainstageContentProviderHelper;->sRecentProjectionMap:Ljava/util/HashMap;

    const-string v3, "suggest_last_access_hint"

    const-string v4, "ItemDate"

    invoke-static {v2, v3, v4}, Lcom/google/android/music/store/MainstageContentProviderHelper;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    sget-object v2, Lcom/google/android/music/store/MainstageContentProviderHelper;->sRecentProjectionMap:Ljava/util/HashMap;

    const-string v3, "suggest_shortcut_id"

    const-string v4, "\'_-1\'"

    invoke-static {v2, v3, v4}, Lcom/google/android/music/store/MainstageContentProviderHelper;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 183
    sget-object v2, Lcom/google/android/music/store/MainstageContentProviderHelper;->sRecentProjectionMap:Ljava/util/HashMap;

    const-string v3, "MIN(count(distinct RecentId),50)"

    invoke-static {v2, v3}, Lcom/google/android/music/store/MusicContentProvider;->addExistsAndCountMapping(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 187
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    sput-object v2, Lcom/google/android/music/store/MainstageContentProviderHelper;->sRecommendationsProjectionMap:Ljava/util/HashMap;

    .line 188
    sget-object v2, Lcom/google/android/music/store/MainstageContentProviderHelper;->sRecommendationsProjectionMap:Ljava/util/HashMap;

    invoke-static {v2}, Lcom/google/android/music/store/MusicContentProvider;->addNullAlbumMappings(Ljava/util/HashMap;)V

    .line 189
    sget-object v2, Lcom/google/android/music/store/MainstageContentProviderHelper;->sRecommendationsProjectionMap:Ljava/util/HashMap;

    invoke-static {v2}, Lcom/google/android/music/store/MusicContentProvider;->addDefaultPlaylistMappings(Ljava/util/HashMap;)V

    .line 190
    sget-object v2, Lcom/google/android/music/store/MainstageContentProviderHelper;->sRecommendationsProjectionMap:Ljava/util/HashMap;

    invoke-static {v2}, Lcom/google/android/music/store/MusicContentProvider;->addDefaultRadioMappings(Ljava/util/HashMap;)V

    .line 191
    sget-object v2, Lcom/google/android/music/store/MainstageContentProviderHelper;->sRecommendationsProjectionMap:Ljava/util/HashMap;

    const-string v3, "StoreAlbumId"

    const-string v4, "RecentNautilusAlbumId"

    invoke-static {v2, v3, v4}, Lcom/google/android/music/store/MainstageContentProviderHelper;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    sget-object v2, Lcom/google/android/music/store/MainstageContentProviderHelper;->sRecommendationsProjectionMap:Ljava/util/HashMap;

    const-string v3, "artworkUrl"

    const-string v4, "RecentNautilusAlbumArt"

    invoke-static {v2, v3, v4}, Lcom/google/android/music/store/MainstageContentProviderHelper;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    sget-object v2, Lcom/google/android/music/store/MainstageContentProviderHelper;->sRecommendationsProjectionMap:Ljava/util/HashMap;

    const-string v3, "album_name"

    const-string v4, "RecentNautilusAlbum"

    invoke-static {v2, v3, v4}, Lcom/google/android/music/store/MainstageContentProviderHelper;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    sget-object v2, Lcom/google/android/music/store/MainstageContentProviderHelper;->sRecommendationsProjectionMap:Ljava/util/HashMap;

    const-string v3, "album_artist"

    const-string v4, "RecentNautilusAlbumArtist"

    invoke-static {v2, v3, v4}, Lcom/google/android/music/store/MainstageContentProviderHelper;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    sget-object v2, Lcom/google/android/music/store/MainstageContentProviderHelper;->sRecommendationsProjectionMap:Ljava/util/HashMap;

    const-string v3, "album_artist_sort"

    const-string v4, "RecentNautilusAlbumArtist"

    invoke-static {v2, v3, v4}, Lcom/google/android/music/store/MainstageContentProviderHelper;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    sget-object v2, Lcom/google/android/music/store/MainstageContentProviderHelper;->sRecommendationsProjectionMap:Ljava/util/HashMap;

    const-string v3, "ArtistMetajamId"

    const-string v4, "RecentNautilusAlbumArtistId"

    invoke-static {v2, v3, v4}, Lcom/google/android/music/store/MainstageContentProviderHelper;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    sget-object v2, Lcom/google/android/music/store/MainstageContentProviderHelper;->sRecommendationsProjectionMap:Ljava/util/HashMap;

    const-string v3, "_id"

    const-string v4, "RecentId"

    invoke-static {v2, v3, v4}, Lcom/google/android/music/store/MainstageContentProviderHelper;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    sget-object v2, Lcom/google/android/music/store/MainstageContentProviderHelper;->sRecommendationsProjectionMap:Ljava/util/HashMap;

    const-string v3, "reason"

    const-string v4, "RecentReason"

    invoke-static {v2, v3, v4}, Lcom/google/android/music/store/MainstageContentProviderHelper;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 207
    sget-object v2, Lcom/google/android/music/store/MainstageContentProviderHelper;->sRecommendationsProjectionMap:Ljava/util/HashMap;

    const-string v3, "reason_text"

    const-string v4, "RecentReasonText"

    invoke-static {v2, v3, v4}, Lcom/google/android/music/store/MainstageContentProviderHelper;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 209
    sget-object v2, Lcom/google/android/music/store/MainstageContentProviderHelper;->sRecommendationsProjectionMap:Ljava/util/HashMap;

    const-string v3, "hasLocal"

    const-string v4, "0"

    invoke-static {v2, v3, v4}, Lcom/google/android/music/store/MainstageContentProviderHelper;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    sget-object v2, Lcom/google/android/music/store/MainstageContentProviderHelper;->sRecommendationsProjectionMap:Ljava/util/HashMap;

    const-string v3, "isAllLocal"

    const-string v4, "0"

    invoke-static {v2, v3, v4}, Lcom/google/android/music/store/MainstageContentProviderHelper;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 213
    sget-object v2, Lcom/google/android/music/store/MainstageContentProviderHelper;->sRecommendationsProjectionMap:Ljava/util/HashMap;

    const-string v3, "hasRemote"

    const-string v4, "1"

    invoke-static {v2, v3, v4}, Lcom/google/android/music/store/MainstageContentProviderHelper;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 215
    sget-object v2, Lcom/google/android/music/store/MainstageContentProviderHelper;->sRecommendationsProjectionMap:Ljava/util/HashMap;

    const-string v3, "playlist_id"

    const-string v4, "RecentListId"

    invoke-static {v2, v3, v4}, Lcom/google/android/music/store/MainstageContentProviderHelper;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 217
    sget-object v2, Lcom/google/android/music/store/MainstageContentProviderHelper;->sRecommendationsProjectionMap:Ljava/util/HashMap;

    const-string v3, "album_artist_profile_image"

    const-string v4, "RecentNautilusAlbumArtistProfileImage"

    invoke-static {v2, v3, v4}, Lcom/google/android/music/store/MainstageContentProviderHelper;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    sget-object v2, Lcom/google/android/music/store/MainstageContentProviderHelper;->sRecommendationsProjectionMap:Ljava/util/HashMap;

    const-string v3, "mainstage_description"

    const/4 v4, 0x6

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "RecentNautilusAlbumId"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "RecentNautilusAlbumDescription"

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-string v6, "RecentListId"

    aput-object v6, v4, v5

    const/4 v5, 0x3

    const-string v6, "LISTS.Description"

    aput-object v6, v4, v5

    const/4 v5, 0x4

    const-string v6, "RecentRadioId"

    aput-object v6, v4, v5

    const/4 v5, 0x5

    const-string v6, "RADIO_STATIONS.Description"

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, Lcom/google/android/music/store/MusicContentProvider;->addNotNullCaseMapping(Ljava/util/HashMap;Ljava/lang/String;[Ljava/lang/String;)V

    .line 225
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    sput-object v2, Lcom/google/android/music/store/MainstageContentProviderHelper;->sSuggestedMixesProjectionMap:Ljava/util/HashMap;

    .line 228
    sget-object v2, Lcom/google/android/music/store/MainstageContentProviderHelper;->sSuggestedMixesProjectionMap:Ljava/util/HashMap;

    invoke-static {v2}, Lcom/google/android/music/store/MusicContentProvider;->addNullAlbumMappings(Ljava/util/HashMap;)V

    .line 229
    sget-object v2, Lcom/google/android/music/store/MainstageContentProviderHelper;->sSuggestedMixesProjectionMap:Ljava/util/HashMap;

    invoke-static {v2}, Lcom/google/android/music/store/MusicContentProvider;->addNullRadioMappings(Ljava/util/HashMap;)V

    .line 230
    sget-object v2, Lcom/google/android/music/store/MainstageContentProviderHelper;->sSuggestedMixesProjectionMap:Ljava/util/HashMap;

    invoke-static {v2}, Lcom/google/android/music/store/MusicContentProvider;->addDefaultPlaylistMappings(Ljava/util/HashMap;)V

    .line 232
    sget-object v2, Lcom/google/android/music/store/MainstageContentProviderHelper;->sSuggestedMixesProjectionMap:Ljava/util/HashMap;

    const-string v3, "item_id"

    const-string v4, "SUGGESTED_SEEDS.SeedListId"

    invoke-static {v2, v3, v4}, Lcom/google/android/music/store/MainstageContentProviderHelper;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 234
    sget-object v2, Lcom/google/android/music/store/MainstageContentProviderHelper;->sSuggestedMixesProjectionMap:Ljava/util/HashMap;

    const-string v3, "item_type"

    const-string v4, "\'4\'"

    invoke-static {v2, v3, v4}, Lcom/google/android/music/store/MainstageContentProviderHelper;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    sget-object v2, Lcom/google/android/music/store/MainstageContentProviderHelper;->sSuggestedMixesProjectionMap:Ljava/util/HashMap;

    const-string v3, "item_name"

    const-string v4, "LISTS.Name"

    invoke-static {v2, v3, v4}, Lcom/google/android/music/store/MainstageContentProviderHelper;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 238
    sget-object v2, Lcom/google/android/music/store/MainstageContentProviderHelper;->sSuggestedMixesProjectionMap:Ljava/util/HashMap;

    const-string v3, "reason"

    const-string v4, "\'100\'"

    invoke-static {v2, v3, v4}, Lcom/google/android/music/store/MainstageContentProviderHelper;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    sget-object v2, Lcom/google/android/music/store/MainstageContentProviderHelper;->sSuggestedMixesProjectionMap:Ljava/util/HashMap;

    const-string v3, "reason_text"

    const-string v4, "null"

    invoke-static {v2, v3, v4}, Lcom/google/android/music/store/MainstageContentProviderHelper;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    sget-object v2, Lcom/google/android/music/store/MainstageContentProviderHelper;->sSuggestedMixesProjectionMap:Ljava/util/HashMap;

    const-string v3, "mainstage_description"

    const-string v4, "null"

    invoke-static {v2, v3, v4}, Lcom/google/android/music/store/MainstageContentProviderHelper;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 242
    sget-object v2, Lcom/google/android/music/store/MainstageContentProviderHelper;->sSuggestedMixesProjectionMap:Ljava/util/HashMap;

    const-string v3, "album_artist_profile_image"

    const-string v4, "null"

    invoke-static {v2, v3, v4}, Lcom/google/android/music/store/MainstageContentProviderHelper;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    sget-object v2, Lcom/google/android/music/store/MainstageContentProviderHelper;->sSuggestedMixesProjectionMap:Ljava/util/HashMap;

    const-string v3, "radio_highlight_color"

    const-string v4, "null"

    invoke-static {v2, v3, v4}, Lcom/google/android/music/store/MainstageContentProviderHelper;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 246
    sget-object v2, Lcom/google/android/music/store/MainstageContentProviderHelper;->sSuggestedMixesProjectionMap:Ljava/util/HashMap;

    const-string v3, "radio_profile_image"

    const-string v4, "null"

    invoke-static {v2, v3, v4}, Lcom/google/android/music/store/MainstageContentProviderHelper;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 249
    sget-object v2, Lcom/google/android/music/store/MainstageContentProviderHelper;->sSuggestedMixesProjectionMap:Ljava/util/HashMap;

    const-string v3, "_id"

    const-string v4, "SUGGESTED_SEEDS.SeedListId"

    invoke-static {v2, v3, v4}, Lcom/google/android/music/store/MainstageContentProviderHelper;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 252
    sget-object v2, Lcom/google/android/music/store/MainstageContentProviderHelper;->sSuggestedMixesProjectionMap:Ljava/util/HashMap;

    const-string v3, "KeepOnId"

    invoke-static {v2, v3}, Lcom/google/android/music/store/MusicContentProvider;->addNullMapping(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 255
    sget-object v2, Lcom/google/android/music/store/MainstageContentProviderHelper;->sSuggestedMixesProjectionMap:Ljava/util/HashMap;

    const-string v3, "isAllLocal"

    const-string v4, "NOT EXISTS(select 1 from MUSIC AS m, LISTITEMS as i WHERE i.ListId=LISTS.Id AND m.Id=i.MusicId GROUP BY m.SongId HAVING MAX(m.LocalCopyType = 0) LIMIT 1)"

    invoke-static {v2, v3, v4}, Lcom/google/android/music/store/MainstageContentProviderHelper;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 257
    sget-object v2, Lcom/google/android/music/store/MainstageContentProviderHelper;->sSuggestedMixesProjectionMap:Ljava/util/HashMap;

    const-string v3, "hasLocal"

    const-string v4, "EXISTS (SELECT 1 FROM LISTITEMS JOIN MUSIC ON (LISTITEMS.MusicId=MUSIC.Id)  WHERE (ListId=LISTS.Id) AND LocalCopyType IN (100,200,300) LIMIT 1)"

    invoke-static {v2, v3, v4}, Lcom/google/android/music/store/MainstageContentProviderHelper;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 259
    sget-object v2, Lcom/google/android/music/store/MainstageContentProviderHelper;->sSuggestedMixesProjectionMap:Ljava/util/HashMap;

    const-string v3, "hasRemote"

    const-string v4, "EXISTS (SELECT 1 FROM LISTITEMS JOIN MUSIC ON (LISTITEMS.MusicId=MUSIC.Id)  WHERE (ListId=LISTS.Id) AND LocalCopyType != 300 LIMIT 1)"

    invoke-static {v2, v3, v4}, Lcom/google/android/music/store/MainstageContentProviderHelper;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 261
    sget-object v2, Lcom/google/android/music/store/MainstageContentProviderHelper;->sSuggestedMixesProjectionMap:Ljava/util/HashMap;

    const-string v3, "ArtistMetajamId"

    invoke-static {v2, v3}, Lcom/google/android/music/store/MusicContentProvider;->addNullMapping(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 263
    sget-object v2, Lcom/google/android/music/store/MainstageContentProviderHelper;->sSuggestedMixesProjectionMap:Ljava/util/HashMap;

    const-string v3, "radio_seed_source_id"

    const-string v4, "SUGGESTED_SEEDS.SeedTrackSourceId"

    invoke-static {v2, v3, v4}, Lcom/google/android/music/store/MainstageContentProviderHelper;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    return-void
.end method

.method private static addLocalAndNautilusAlbumMapping(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p0, "columnToMap"    # Ljava/lang/String;
    .param p1, "localExpression"    # Ljava/lang/String;
    .param p2, "nautilusExpression"    # Ljava/lang/String;

    .prologue
    .line 321
    sget-object v0, Lcom/google/android/music/store/MainstageContentProviderHelper;->sRecentProjectionMap:Ljava/util/HashMap;

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "RecentAlbumId"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object p1, v1, v2

    const/4 v2, 0x2

    const-string v3, "RecentNautilusAlbumId"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    aput-object p2, v1, v2

    invoke-static {v0, p0, v1}, Lcom/google/android/music/store/MusicContentProvider;->addNotNullCaseMapping(Ljava/util/HashMap;Ljava/lang/String;[Ljava/lang/String;)V

    .line 325
    return-void
.end method

.method private static addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "from"    # Ljava/lang/String;
    .param p2, "to"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 308
    .local p0, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-static {p0, p1, p2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 309
    return-void
.end method

.method private static addOfflineMapping(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p0, "columnToMap"    # Ljava/lang/String;
    .param p1, "onlineExpression"    # Ljava/lang/String;
    .param p2, "albumExpression"    # Ljava/lang/String;
    .param p3, "playlistExpression"    # Ljava/lang/String;
    .param p4, "radioExpression"    # Ljava/lang/String;

    .prologue
    .line 341
    sget-object v0, Lcom/google/android/music/store/MainstageContentProviderHelper;->sRecentProjectionMap:Ljava/util/HashMap;

    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "RecentAlbumId"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object p2, v1, v2

    const/4 v2, 0x2

    const-string v3, "RecentListId"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    aput-object p3, v1, v2

    const/4 v2, 0x4

    const-string v3, "RecentRadioId"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    aput-object p4, v1, v2

    invoke-static {v0, p0, p1, v1}, Lcom/google/android/music/store/MusicContentProvider;->addNotNullCaseMappingWithDefault(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    .line 346
    return-void
.end method

.method private static addRecentAlbumOrPlaylistMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "albumExpression"    # Ljava/lang/String;
    .param p3, "playlistExpression"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 351
    .local p0, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "RecentAlbumId"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    aput-object p2, v0, v1

    const/4 v1, 0x2

    const-string v2, "RecentListId"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    aput-object p3, v0, v1

    invoke-static {p0, p1, v0}, Lcom/google/android/music/store/MusicContentProvider;->addNotNullCaseMapping(Ljava/util/HashMap;Ljava/lang/String;[Ljava/lang/String;)V

    .line 355
    return-void
.end method

.method static dismissItem(Landroid/content/Context;Lcom/google/android/music/store/Store;Landroid/net/Uri;Landroid/accounts/Account;)V
    .locals 33
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "store"    # Lcom/google/android/music/store/Store;
    .param p2, "uri"    # Landroid/net/Uri;
    .param p3, "account"    # Landroid/accounts/Account;

    .prologue
    .line 819
    const-string v2, "dismissPos"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    .line 820
    .local v28, "posString":Ljava/lang/String;
    const-string v2, "reasonType"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v30

    .line 822
    .local v30, "reasonTypeString":Ljava/lang/String;
    invoke-static/range {v28 .. v28}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static/range {v30 .. v30}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 886
    :cond_0
    :goto_0
    return-void

    .line 826
    :cond_1
    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v27

    .line 827
    .local v27, "pos":I
    invoke-static/range {v30 .. v30}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    .line 829
    .local v9, "reason":I
    const-string v2, "albumTitle"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 830
    .local v4, "albumTitle":Ljava/lang/String;
    const-string v2, "albumArtist"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 831
    .local v5, "albumArtist":Ljava/lang/String;
    const-string v2, "albumLocalId"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    .line 832
    .local v21, "albumLocalId":Ljava/lang/String;
    const-string v2, "albumMetajamId"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 836
    .local v8, "albumMetajamId":Ljava/lang/String;
    invoke-static/range {v21 .. v21}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_5

    const-wide/16 v2, -0x1

    :goto_1
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v22

    .line 838
    .local v22, "albumLocalIdLong":Ljava/lang/Long;
    const-string v2, "listLocalId"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    .line 839
    .local v26, "listLocalId":Ljava/lang/String;
    const-string v2, "listShareToken"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 842
    .local v14, "listShareToken":Ljava/lang/String;
    const-string v2, "radioRemoteId"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 843
    .local v17, "radioRemoteId":Ljava/lang/String;
    const-string v2, "radioSeedId"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 844
    .local v18, "radioSeedId":Ljava/lang/String;
    const-string v2, "radioSeedType"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    .line 846
    .local v29, "radioSeedType":Ljava/lang/String;
    invoke-static/range {v21 .. v21}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_2
    const/16 v23, 0x1

    .line 847
    .local v23, "isAlbum":Z
    :goto_2
    invoke-static/range {v26 .. v26}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_7

    :cond_3
    const/16 v24, 0x1

    .line 848
    .local v24, "isList":Z
    :goto_3
    invoke-static/range {v18 .. v18}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_8

    invoke-static/range {v29 .. v29}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_8

    const/16 v25, 0x1

    .line 850
    .local v25, "isRadio":Z
    :goto_4
    const/16 v32, 0x0

    .line 852
    .local v32, "success":Z
    if-eqz v23, :cond_9

    .line 853
    invoke-virtual/range {v22 .. v22}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    move-object/from16 v2, p1

    move-object/from16 v3, p3

    invoke-virtual/range {v2 .. v9}, Lcom/google/android/music/store/Store;->blacklistAlbum(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;I)Z

    move-result v32

    .line 871
    :goto_5
    if-eqz v32, :cond_0

    .line 878
    sget-object v3, Lcom/google/android/music/store/MainstageContentProviderHelper;->sCategoryMappingLock:Ljava/lang/Object;

    monitor-enter v3

    .line 879
    :try_start_0
    sget-object v2, Lcom/google/android/music/store/MainstageContentProviderHelper;->sCategoryMapping:Ljava/util/List;

    if-eqz v2, :cond_4

    .line 880
    sget-object v2, Lcom/google/android/music/store/MainstageContentProviderHelper;->sCategoryMapping:Ljava/util/List;

    move/from16 v0, v27

    invoke-interface {v2, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v31

    check-cast v31, Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;

    .line 882
    .local v31, "removedCategory":Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;
    sget-object v2, Lcom/google/android/music/store/MainstageContentProviderHelper;->sCategoryMapping:Ljava/util/List;

    move-object/from16 v0, v31

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 884
    .end local v31    # "removedCategory":Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;
    :cond_4
    monitor-exit v3

    goto/16 :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 836
    .end local v14    # "listShareToken":Ljava/lang/String;
    .end local v17    # "radioRemoteId":Ljava/lang/String;
    .end local v18    # "radioSeedId":Ljava/lang/String;
    .end local v22    # "albumLocalIdLong":Ljava/lang/Long;
    .end local v23    # "isAlbum":Z
    .end local v24    # "isList":Z
    .end local v25    # "isRadio":Z
    .end local v26    # "listLocalId":Ljava/lang/String;
    .end local v29    # "radioSeedType":Ljava/lang/String;
    .end local v32    # "success":Z
    :cond_5
    invoke-static/range {v21 .. v21}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    goto/16 :goto_1

    .line 846
    .restart local v14    # "listShareToken":Ljava/lang/String;
    .restart local v17    # "radioRemoteId":Ljava/lang/String;
    .restart local v18    # "radioSeedId":Ljava/lang/String;
    .restart local v22    # "albumLocalIdLong":Ljava/lang/Long;
    .restart local v26    # "listLocalId":Ljava/lang/String;
    .restart local v29    # "radioSeedType":Ljava/lang/String;
    :cond_6
    const/16 v23, 0x0

    goto :goto_2

    .line 847
    .restart local v23    # "isAlbum":Z
    :cond_7
    const/16 v24, 0x0

    goto :goto_3

    .line 848
    .restart local v24    # "isList":Z
    :cond_8
    const/16 v25, 0x0

    goto :goto_4

    .line 856
    .restart local v25    # "isRadio":Z
    .restart local v32    # "success":Z
    :cond_9
    if-eqz v24, :cond_b

    .line 857
    sget v2, Lcom/google/android/music/store/Schema$MainstageBlacklist;->REASON_TYPE_VALUE_RECOMMENDED_FOR_YOU_RADIO:I

    if-ne v9, v2, :cond_a

    .line 859
    invoke-static/range {v26 .. v26}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    move-object/from16 v0, p1

    move-object/from16 v1, p3

    invoke-virtual {v0, v1, v2, v3, v9}, Lcom/google/android/music/store/Store;->blacklistSuggestedSeed(Landroid/accounts/Account;JI)Z

    move-result v32

    goto :goto_5

    .line 861
    :cond_a
    invoke-static/range {v26 .. v26}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    move-object/from16 v10, p1

    move-object/from16 v11, p3

    move v15, v9

    invoke-virtual/range {v10 .. v15}, Lcom/google/android/music/store/Store;->blacklistList(Landroid/accounts/Account;JLjava/lang/String;I)Z

    move-result v32

    goto :goto_5

    .line 864
    :cond_b
    if-eqz v25, :cond_c

    .line 865
    invoke-static/range {v29 .. v29}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v19

    move-object/from16 v15, p1

    move-object/from16 v16, p3

    move/from16 v20, v9

    invoke-virtual/range {v15 .. v20}, Lcom/google/android/music/store/Store;->blacklistRadio(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;II)Z

    move-result v32

    goto :goto_5

    .line 868
    :cond_c
    const-string v2, "MainstageContentProvider"

    const-string v3, "No item was specified for dismissal"

    invoke-static {v2, v3}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5
.end method

.method private static generateCategoryMapping()Ljava/util/List;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v13, 0x0

    .line 736
    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    .line 739
    .local v3, "categoryMapping":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;>;"
    invoke-static {}, Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;->values()[Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;

    move-result-object v1

    .line 740
    .local v1, "categories":[Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;
    new-instance v7, Ljava/util/HashMap;

    array-length v9, v1

    invoke-direct {v7, v9}, Ljava/util/HashMap;-><init>(I)V

    .line 742
    .local v7, "itemCountTracker":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;Ljava/lang/Integer;>;"
    move-object v0, v1

    .local v0, "arr$":[Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;
    array-length v8, v0

    .local v8, "len$":I
    const/4 v6, 0x0

    .local v6, "i$":I
    :goto_0
    if-ge v6, v8, :cond_0

    aget-object v2, v0, v6

    .line 743
    .local v2, "category":Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;
    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-interface {v7, v2, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 742
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 748
    .end local v2    # "category":Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;
    :cond_0
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_1
    const/16 v9, 0x1e

    if-ge v5, v9, :cond_4

    .line 749
    # invokes: Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;->getRandomCategory()Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;
    invoke-static {}, Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;->access$000()Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;

    move-result-object v2

    .line 750
    .restart local v2    # "category":Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;
    if-nez v2, :cond_1

    .line 754
    const-string v9, "MainstageContentProvider"

    const-string v10, "Null category"

    new-instance v11, Ljava/lang/Exception;

    const-string v12, "Null category"

    invoke-direct {v11, v12}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    invoke-static {v9, v10, v11}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 755
    aget-object v2, v1, v13

    .line 757
    :cond_1
    invoke-interface {v7, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 758
    .local v4, "countSoFar":I
    # getter for: Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;->mMaxNumItems:I
    invoke-static {v2}, Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;->access$100(Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;)I

    move-result v9

    if-ge v4, v9, :cond_3

    .line 759
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 760
    add-int/lit8 v9, v4, 0x1

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-interface {v7, v2, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 748
    :cond_2
    :goto_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 762
    :cond_3
    invoke-static {v2, v7}, Lcom/google/android/music/store/MainstageContentProviderHelper;->getFallbackCategory(Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;Ljava/util/Map;)Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;

    move-result-object v2

    .line 763
    if-eqz v2, :cond_2

    .line 765
    invoke-interface {v7, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 766
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 767
    add-int/lit8 v9, v4, 0x1

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-interface {v7, v2, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 770
    .end local v2    # "category":Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;
    .end local v4    # "countSoFar":I
    :cond_4
    sget-boolean v9, Lcom/google/android/music/store/MainstageContentProviderHelper;->LOGV:Z

    if-eqz v9, :cond_5

    .line 771
    const-string v9, "MainstageContentProvider"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Generated category mappings: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 772
    const-string v9, "MainstageContentProvider"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Category counts: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 775
    :cond_5
    return-object v3
.end method

.method private static getCursorsMap(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Lcom/google/android/music/store/Store;)Ljava/util/Map;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "queryUri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "store"    # Lcom/google/android/music/store/Store;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/net/Uri;",
            "[",
            "Ljava/lang/String;",
            "Lcom/google/android/music/store/Store;",
            ")",
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 537
    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1}, Ljava/util/LinkedHashMap;-><init>()V

    .line 540
    .local v1, "groups":Ljava/util/LinkedHashMap;, "Ljava/util/LinkedHashMap<Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;Landroid/database/Cursor;>;"
    invoke-static {p1}, Lcom/google/android/music/store/MusicContentProvider;->getMusicFilterIndex(Landroid/net/Uri;)I

    move-result v0

    .line 543
    .local v0, "filterIndex":I
    sget-object v2, Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;->SUGGESTED_MIX:Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;

    invoke-static {p1, p2, p3, v0}, Lcom/google/android/music/store/MainstageContentProviderHelper;->querySuggestedMixes(Landroid/net/Uri;[Ljava/lang/String;Lcom/google/android/music/store/Store;I)Landroid/database/Cursor;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 545
    sget-object v2, Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;->RECOMMENDATIONS:Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;

    invoke-static {p0, p1, p2, p3, v0}, Lcom/google/android/music/store/MainstageContentProviderHelper;->queryLockerRecs(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Lcom/google/android/music/store/Store;I)Landroid/database/Cursor;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 548
    return-object v1
.end method

.method private static getFallbackCategory(Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;Ljava/util/Map;)Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;
    .locals 8
    .param p0, "current"    # Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;",
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;",
            "Ljava/lang/Integer;",
            ">;)",
            "Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;"
        }
    .end annotation

    .prologue
    .line 789
    .local p1, "itemCountTracker":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;Ljava/lang/Integer;>;"
    invoke-static {}, Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;->values()[Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;

    move-result-object v0

    .line 790
    .local v0, "categories":[Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;
    invoke-virtual {p0}, Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;->ordinal()I

    move-result v5

    add-int/lit8 v4, v5, 0x1

    .line 791
    .local v4, "ordinal":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    array-length v5, v0

    if-ge v3, v5, :cond_2

    .line 792
    array-length v5, v0

    rem-int v5, v4, v5

    aget-object v1, v0, v5

    .line 793
    .local v1, "category":Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;
    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 794
    .local v2, "countSoFar":I
    # getter for: Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;->mMaxNumItems:I
    invoke-static {v1}, Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;->access$100(Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;)I

    move-result v5

    if-ge v2, v5, :cond_1

    .line 796
    sget-boolean v5, Lcom/google/android/music/store/MainstageContentProviderHelper;->LOGV:Z

    if-eqz v5, :cond_0

    .line 797
    const-string v5, "MainstageContentProvider"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Falling back to "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " from "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 807
    .end local v1    # "category":Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;
    .end local v2    # "countSoFar":I
    :cond_0
    :goto_1
    return-object v1

    .line 802
    .restart local v1    # "category":Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;
    .restart local v2    # "countSoFar":I
    :cond_1
    add-int/lit8 v4, v4, 0x1

    .line 791
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 804
    .end local v1    # "category":Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;
    .end local v2    # "countSoFar":I
    :cond_2
    sget-boolean v5, Lcom/google/android/music/store/MainstageContentProviderHelper;->LOGV:Z

    if-eqz v5, :cond_3

    .line 805
    const-string v5, "MainstageContentProvider"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Could not fall back from "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 807
    :cond_3
    const/4 v1, 0x0

    goto :goto_1
.end method

.method static getRecentContent(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Lcom/google/android/music/store/Store;)Landroid/database/Cursor;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "queryUri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "store"    # Lcom/google/android/music/store/Store;

    .prologue
    .line 301
    invoke-static {p1}, Lcom/google/android/music/store/MusicContentProvider;->getMusicFilterIndex(Landroid/net/Uri;)I

    move-result v0

    invoke-static {p0, p1, p2, p3, v0}, Lcom/google/android/music/store/MainstageContentProviderHelper;->queryRecent(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Lcom/google/android/music/store/Store;I)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method static getRecommendedContent(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Lcom/google/android/music/store/Store;)Landroid/database/Cursor;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "queryUri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "store"    # Lcom/google/android/music/store/Store;

    .prologue
    .line 286
    invoke-static {p1}, Lcom/google/android/music/store/MusicContentProvider;->getMusicFilterIndex(Landroid/net/Uri;)I

    move-result v0

    invoke-static {p0, p1, p2, p3, v0}, Lcom/google/android/music/store/MainstageContentProviderHelper;->queryRecommendations(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Lcom/google/android/music/store/Store;I)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method private static queryLockerRecs(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Lcom/google/android/music/store/Store;I)Landroid/database/Cursor;
    .locals 12
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "queryUri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "store"    # Lcom/google/android/music/store/Store;
    .param p4, "filterIndex"    # I

    .prologue
    .line 565
    invoke-static/range {p4 .. p4}, Lcom/google/android/music/store/Filters;->doesExcludeOnlineMusic(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 569
    new-instance v10, Landroid/database/MatrixCursor;

    invoke-direct {v10, p2}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 570
    .local v10, "c":Landroid/database/Cursor;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/google/android/music/store/MusicContent$Mainstage;->CONTENT_URI:Landroid/net/Uri;

    invoke-interface {v10, v2, v3}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    .line 603
    :goto_0
    return-object v10

    .line 574
    .end local v10    # "c":Landroid/database/Cursor;
    :cond_0
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 575
    .local v0, "qb":Landroid/database/sqlite/SQLiteQueryBuilder;
    const-string v2, "RECENT LEFT  JOIN LISTS ON (RecentListId=LISTS.Id)  LEFT  JOIN RADIO_STATIONS ON (RecentRadioId=RADIO_STATIONS.Id) "

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 577
    sget-object v2, Lcom/google/android/music/store/MainstageContentProviderHelper;->sRecommendationsProjectionMap:Ljava/util/HashMap;

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    .line 579
    const-string v5, "RecentId"

    .line 580
    .local v5, "groupBy":Ljava/lang/String;
    const-string v7, "RecentId"

    .line 582
    .local v7, "sortOrder":Ljava/lang/String;
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    .line 583
    .local v11, "conditions":Ljava/lang/StringBuilder;
    const-string v2, "RecentReason IN (7,8,4,9)"

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 584
    const-string v2, "((RECENT.RecentNautilusAlbumId NOT NULL AND RECENT.RecentNautilusAlbumId NOT IN (SELECT AlbumMetajamId FROM MAINSTAGE_BLACKLIST WHERE AlbumMetajamId NOT NULL)) OR (RADIO_STATIONS.SeedSourceId NOT NULL AND RADIO_STATIONS.SeedSourceType NOT NULL AND  NOT EXISTS (SELECT 1 FROM MAINSTAGE_BLACKLIST WHERE RadioSeedId=RADIO_STATIONS.SeedSourceId AND RadioSeedType=RADIO_STATIONS.SeedSourceType)) OR (LISTS.ShareToken NOT NULL AND LISTS.ShareToken NOT IN (SELECT ListShareToken FROM MAINSTAGE_BLACKLIST WHERE ListShareToken NOT NULL)))"

    invoke-static {v11, v2}, Lcom/google/android/music/store/MusicContentProvider;->appendAndCondition(Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 586
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 588
    const-string v8, "30"

    .line 590
    .local v8, "limit":Ljava/lang/String;
    invoke-virtual {p3}, Lcom/google/android/music/store/Store;->beginRead()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 592
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v9, 0x0

    move-object v2, p2

    :try_start_0
    invoke-static/range {v0 .. v9}, Lcom/google/android/music/utils/PostFroyoUtils$SQLiteDatabaseComp;->query(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/music/utils/PostFroyoUtils$CancellationSignalComp;)Landroid/database/Cursor;

    move-result-object v10

    .line 596
    .restart local v10    # "c":Landroid/database/Cursor;
    if-eqz v10, :cond_1

    .line 597
    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I

    .line 598
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/google/android/music/store/MusicContent$Mainstage;->CONTENT_URI:Landroid/net/Uri;

    invoke-interface {v10, v2, v3}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 603
    :cond_1
    invoke-virtual {p3, v1}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0

    .end local v10    # "c":Landroid/database/Cursor;
    :catchall_0
    move-exception v2

    invoke-virtual {p3, v1}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    throw v2
.end method

.method private static queryRecent(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Lcom/google/android/music/store/Store;I)Landroid/database/Cursor;
    .locals 22
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "queryUri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "store"    # Lcom/google/android/music/store/Store;
    .param p4, "filterIndex"    # I

    .prologue
    .line 371
    new-instance v2, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v2}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 372
    .local v2, "qb":Landroid/database/sqlite/SQLiteQueryBuilder;
    const-string v4, "RECENT LEFT JOIN MUSIC ON (RecentAlbumId=MUSIC.AlbumId)  LEFT JOIN LISTS ON (RecentListId=LISTS.Id)  LEFT  JOIN RADIO_STATIONS ON (RecentRadioId=RADIO_STATIONS.Id)  LEFT  JOIN KEEPON ON (KEEPON.RadioStationId = RADIO_STATIONS.Id) "

    invoke-virtual {v2, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 375
    sget-object v4, Lcom/google/android/music/store/MainstageContentProviderHelper;->sRecentProjectionMap:Ljava/util/HashMap;

    invoke-virtual {v2, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    .line 377
    const/16 v4, 0x64

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    .line 378
    .local v10, "limit":Ljava/lang/String;
    invoke-static/range {p2 .. p2}, Lcom/google/android/music/store/MusicContentProvider;->hasCount([Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    const/4 v7, 0x0

    .line 379
    .local v7, "groupBy":Ljava/lang/String;
    :goto_0
    const-string v9, "Priority DESC, ItemDate DESC"

    .line 380
    .local v9, "sortOrder":Ljava/lang/String;
    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    .line 385
    .local v16, "conditions":Ljava/lang/StringBuilder;
    const/4 v4, 0x1

    move/from16 v0, p4

    invoke-static {v0, v4}, Lcom/google/android/music/store/Filters;->setExternalFiltering(IZ)I

    move-result v18

    .line 386
    .local v18, "includeExternalFilterIdx":I
    const/4 v4, 0x0

    move/from16 v0, p4

    invoke-static {v0, v4}, Lcom/google/android/music/store/Filters;->setExternalFiltering(IZ)I

    move-result v17

    .line 388
    .local v17, "excludeExternalFilterIdx":I
    const-string v13, ""

    .line 389
    .local v13, "andPlaylistItemFilter":Ljava/lang/String;
    if-eqz v18, :cond_0

    .line 390
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    .line 391
    .local v19, "playlistItemConditions":Ljava/lang/StringBuilder;
    move-object/from16 v0, v19

    move/from16 v1, v18

    invoke-static {v0, v1}, Lcom/google/android/music/store/MusicContentProvider;->appendPlaylistFilteringCondition(Ljava/lang/StringBuilder;I)V

    .line 393
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    if-lez v4, :cond_0

    .line 394
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " AND "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 398
    .end local v19    # "playlistItemConditions":Ljava/lang/StringBuilder;
    :cond_0
    const-string v12, ""

    .line 399
    .local v12, "andExcludeExternalFilter":Ljava/lang/String;
    const/4 v4, 0x3

    move/from16 v0, v17

    if-eq v0, v4, :cond_1

    .line 400
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " AND "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static/range {v17 .. v17}, Lcom/google/android/music/store/Filters;->getFilter(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 403
    :cond_1
    const-string v14, ""

    .line 404
    .local v14, "andRadioItemFilter":Ljava/lang/String;
    if-eqz v18, :cond_2

    .line 405
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    .line 406
    .local v20, "radioItemsConditions":Ljava/lang/StringBuilder;
    move-object/from16 v0, v20

    move/from16 v1, v18

    invoke-static {v0, v1}, Lcom/google/android/music/store/MusicContentProvider;->appendRadioFilteringCondition(Ljava/lang/StringBuilder;I)V

    .line 408
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " AND "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 415
    .end local v20    # "radioItemsConditions":Ljava/lang/StringBuilder;
    :cond_2
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "((LISTS.Id NOT NULL "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " OR "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "RecentNautilusAlbumId"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " NOT NULL"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " OR "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "RADIO_STATIONS.Id"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " NOT NULL "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " OR "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "MUSIC.AlbumId"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " NOT NULL "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "))"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " AND "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "RecentReason"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " NOT IN("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/4 v5, 0x5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/4 v5, 0x6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    .line 427
    .local v21, "recentItemsCondition":Ljava/lang/String;
    move-object/from16 v0, v16

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Lcom/google/android/music/store/MusicContentProvider;->appendAndCondition(Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 429
    const-string v4, "NOT RecentReason IN (7,8,4,9)"

    move-object/from16 v0, v16

    invoke-static {v0, v4}, Lcom/google/android/music/store/MusicContentProvider;->appendAndCondition(Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 432
    sget-object v4, Lcom/google/android/music/store/Schema;->RECENT_MAINSTAGE_BLACKLIST_FILTER_WHERE:Ljava/lang/String;

    move-object/from16 v0, v16

    invoke-static {v0, v4}, Lcom/google/android/music/store/MusicContentProvider;->appendAndCondition(Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 436
    invoke-static/range {p4 .. p4}, Lcom/google/android/music/store/Filters;->doesExcludeOnlineMusic(I)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 440
    const-string v4, "RecentNautilusAlbumId IS NULL "

    move-object/from16 v0, v16

    invoke-static {v0, v4}, Lcom/google/android/music/store/MusicContentProvider;->appendAndCondition(Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 443
    :cond_3
    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 445
    invoke-virtual/range {p3 .. p3}, Lcom/google/android/music/store/Store;->beginRead()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 447
    .local v3, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v8, 0x0

    const/4 v11, 0x0

    move-object/from16 v4, p2

    :try_start_0
    invoke-static/range {v2 .. v11}, Lcom/google/android/music/utils/PostFroyoUtils$SQLiteDatabaseComp;->query(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/music/utils/PostFroyoUtils$CancellationSignalComp;)Landroid/database/Cursor;

    move-result-object v15

    .line 450
    .local v15, "c":Landroid/database/Cursor;
    if-eqz v15, :cond_4

    .line 451
    invoke-interface {v15}, Landroid/database/Cursor;->getCount()I

    .line 452
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-interface {v15, v4, v0}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 456
    :cond_4
    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    return-object v15

    .line 378
    .end local v3    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v7    # "groupBy":Ljava/lang/String;
    .end local v9    # "sortOrder":Ljava/lang/String;
    .end local v12    # "andExcludeExternalFilter":Ljava/lang/String;
    .end local v13    # "andPlaylistItemFilter":Ljava/lang/String;
    .end local v14    # "andRadioItemFilter":Ljava/lang/String;
    .end local v15    # "c":Landroid/database/Cursor;
    .end local v16    # "conditions":Ljava/lang/StringBuilder;
    .end local v17    # "excludeExternalFilterIdx":I
    .end local v18    # "includeExternalFilterIdx":I
    .end local v21    # "recentItemsCondition":Ljava/lang/String;
    :cond_5
    const-string v7, "RecentId"

    goto/16 :goto_0

    .line 456
    .restart local v3    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .restart local v7    # "groupBy":Ljava/lang/String;
    .restart local v9    # "sortOrder":Ljava/lang/String;
    .restart local v12    # "andExcludeExternalFilter":Ljava/lang/String;
    .restart local v13    # "andPlaylistItemFilter":Ljava/lang/String;
    .restart local v14    # "andRadioItemFilter":Ljava/lang/String;
    .restart local v16    # "conditions":Ljava/lang/StringBuilder;
    .restart local v17    # "excludeExternalFilterIdx":I
    .restart local v18    # "includeExternalFilterIdx":I
    .restart local v21    # "recentItemsCondition":Ljava/lang/String;
    :catchall_0
    move-exception v4

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    throw v4
.end method

.method private static queryRecommendations(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Lcom/google/android/music/store/Store;I)Landroid/database/Cursor;
    .locals 11
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "contentUri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "store"    # Lcom/google/android/music/store/Store;
    .param p4, "filterIndex"    # I

    .prologue
    .line 463
    new-instance v7, Landroid/database/MatrixCursor;

    invoke-direct {v7, p2}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 466
    .local v7, "mainstageCursor":Landroid/database/MatrixCursor;
    invoke-static {p0, p1, p2, p3}, Lcom/google/android/music/store/MainstageContentProviderHelper;->getCursorsMap(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Lcom/google/android/music/store/Store;)Ljava/util/Map;

    move-result-object v3

    .line 471
    .local v3, "cursorsMap":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;Landroid/database/Cursor;>;"
    sget-object v9, Lcom/google/android/music/store/MainstageContentProviderHelper;->sCategoryMappingLock:Ljava/lang/Object;

    monitor-enter v9

    .line 472
    :try_start_0
    sget-object v8, Lcom/google/android/music/store/MainstageContentProviderHelper;->sCategoryMapping:Ljava/util/List;

    if-nez v8, :cond_0

    .line 473
    invoke-static {}, Lcom/google/android/music/store/MainstageContentProviderHelper;->generateCategoryMapping()Ljava/util/List;

    move-result-object v8

    sput-object v8, Lcom/google/android/music/store/MainstageContentProviderHelper;->sCategoryMapping:Ljava/util/List;

    .line 477
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    sget-object v8, Lcom/google/android/music/store/MainstageContentProviderHelper;->sCategoryMapping:Ljava/util/List;

    invoke-direct {v2, v8}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 478
    .local v2, "categoryMapping":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;>;"
    monitor-exit v9
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 482
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;

    .line 483
    .local v1, "category":Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;
    invoke-interface {v3}, Ljava/util/Map;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_3

    .line 517
    .end local v1    # "category":Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;
    :cond_2
    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_7

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 518
    .local v0, "c":Landroid/database/Cursor;
    invoke-static {v0}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto :goto_1

    .line 478
    .end local v0    # "c":Landroid/database/Cursor;
    .end local v2    # "categoryMapping":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;>;"
    .end local v5    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v8

    :try_start_1
    monitor-exit v9
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v8

    .line 488
    .restart local v1    # "category":Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;
    .restart local v2    # "categoryMapping":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;>;"
    .restart local v5    # "i$":Ljava/util/Iterator;
    :cond_3
    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 489
    .restart local v0    # "c":Landroid/database/Cursor;
    if-eqz v0, :cond_4

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v8

    if-eqz v8, :cond_4

    .line 490
    invoke-static {v7, v0}, Lcom/google/android/music/utils/DbUtils;->addRowToMatrixCursor(Landroid/database/MatrixCursor;Landroid/database/Cursor;)V

    goto :goto_0

    .line 493
    :cond_4
    sget-boolean v8, Lcom/google/android/music/store/MainstageContentProviderHelper;->LOGV:Z

    if-eqz v8, :cond_5

    .line 494
    const-string v8, "MainstageContentProvider"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "No data found for "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ". Falling back."

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 496
    :cond_5
    invoke-interface {v3, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 497
    invoke-static {v0}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 501
    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .line 502
    .local v6, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/database/Cursor;>;"
    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 503
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/database/Cursor;

    .line 504
    .local v4, "fallbackCursor":Landroid/database/Cursor;
    if-eqz v4, :cond_6

    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z

    move-result v8

    if-eqz v8, :cond_6

    .line 505
    invoke-static {v7, v4}, Lcom/google/android/music/utils/DbUtils;->addRowToMatrixCursor(Landroid/database/MatrixCursor;Landroid/database/Cursor;)V

    goto/16 :goto_0

    .line 509
    :cond_6
    invoke-interface {v6}, Ljava/util/Iterator;->remove()V

    .line 510
    invoke-static {v4}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto :goto_2

    .line 521
    .end local v0    # "c":Landroid/database/Cursor;
    .end local v1    # "category":Lcom/google/android/music/store/MainstageContentProviderHelper$ItemCategory;
    .end local v4    # "fallbackCursor":Landroid/database/Cursor;
    .end local v6    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/database/Cursor;>;"
    :cond_7
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    invoke-virtual {v7, v8, p1}, Landroid/database/MatrixCursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    .line 522
    return-object v7
.end method

.method private static querySuggestedMixes(Landroid/net/Uri;[Ljava/lang/String;Lcom/google/android/music/store/Store;I)Landroid/database/Cursor;
    .locals 14
    .param p0, "queryUri"    # Landroid/net/Uri;
    .param p1, "projection"    # [Ljava/lang/String;
    .param p2, "store"    # Lcom/google/android/music/store/Store;
    .param p3, "filterIndex"    # I

    .prologue
    .line 619
    new-instance v1, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v1}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 620
    .local v1, "qb":Landroid/database/sqlite/SQLiteQueryBuilder;
    const/16 v3, 0xa

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    .line 621
    .local v9, "limit":Ljava/lang/String;
    const-string v3, "LISTS JOIN SUGGESTED_SEEDS ON (LISTS.Id=SUGGESTED_SEEDS.SeedListId)  LEFT  JOIN KEEPON ON (KEEPON.ListId = LISTS.Id) "

    invoke-virtual {v1, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 623
    const-string v8, "SeedOrder"

    .line 624
    .local v8, "sortOrder":Ljava/lang/String;
    sget-object v3, Lcom/google/android/music/store/MainstageContentProviderHelper;->sSuggestedMixesProjectionMap:Ljava/util/HashMap;

    invoke-virtual {v1, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    .line 627
    const/4 v3, 0x1

    move/from16 v0, p3

    invoke-static {v0, v3}, Lcom/google/android/music/store/Filters;->setExternalFiltering(IZ)I

    move-result v13

    .line 628
    .local v13, "includeExternalFilter":I
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    .line 629
    .local v12, "conditions":Ljava/lang/StringBuilder;
    const-string v3, "(SUGGESTED_SEEDS.SeedTrackSourceId NOT NULL AND  NOT EXISTS (SELECT 1 FROM MAINSTAGE_BLACKLIST WHERE RadioSeedId=SUGGESTED_SEEDS.SeedTrackSourceId AND RadioSeedType IN (2,1)))"

    invoke-static {v12, v3}, Lcom/google/android/music/store/MusicContentProvider;->appendAndCondition(Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 631
    invoke-static {v12, v13}, Lcom/google/android/music/store/MusicContentProvider;->appendPlaylistFilteringCondition(Ljava/lang/StringBuilder;I)V

    .line 632
    invoke-static {v12}, Lcom/google/android/music/store/MusicContentProvider;->appendIgnoreRecommendedPlaylistCondition(Ljava/lang/StringBuilder;)V

    .line 633
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    if-lez v3, :cond_0

    .line 634
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 636
    :cond_0
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/music/store/Store;->beginRead()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 639
    .local v2, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v10, 0x0

    move-object v3, p1

    :try_start_0
    invoke-static/range {v1 .. v10}, Lcom/google/android/music/utils/PostFroyoUtils$SQLiteDatabaseComp;->query(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/music/utils/PostFroyoUtils$CancellationSignalComp;)Landroid/database/Cursor;

    move-result-object v11

    .line 642
    .local v11, "c":Landroid/database/Cursor;
    if-eqz v11, :cond_1

    .line 643
    invoke-interface {v11}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 647
    :cond_1
    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    return-object v11

    .end local v11    # "c":Landroid/database/Cursor;
    :catchall_0
    move-exception v3

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    throw v3
.end method
