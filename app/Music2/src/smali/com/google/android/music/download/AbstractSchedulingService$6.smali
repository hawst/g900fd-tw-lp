.class Lcom/google/android/music/download/AbstractSchedulingService$6;
.super Ljava/lang/Object;
.source "AbstractSchedulingService.java"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/download/AbstractSchedulingService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/download/AbstractSchedulingService;


# direct methods
.method constructor <init>(Lcom/google/android/music/download/AbstractSchedulingService;)V
    .locals 0

    .prologue
    .line 196
    iput-object p1, p0, Lcom/google/android/music/download/AbstractSchedulingService$6;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 2
    .param p1, "sharedPreferences"    # Landroid/content/SharedPreferences;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 199
    iget-object v0, p0, Lcom/google/android/music/download/AbstractSchedulingService$6;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    # getter for: Lcom/google/android/music/download/AbstractSchedulingService;->mState:Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;
    invoke-static {v0}, Lcom/google/android/music/download/AbstractSchedulingService;->access$500(Lcom/google/android/music/download/AbstractSchedulingService;)Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;

    move-result-object v0

    sget-object v1, Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;->DISABLED:Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/music/download/AbstractSchedulingService$6;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    # getter for: Lcom/google/android/music/download/AbstractSchedulingService;->mState:Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;
    invoke-static {v0}, Lcom/google/android/music/download/AbstractSchedulingService;->access$500(Lcom/google/android/music/download/AbstractSchedulingService;)Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;

    move-result-object v0

    sget-object v1, Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;->WORKING:Lcom/google/android/music/download/AbstractSchedulingService$ServiceState;

    if-ne v0, v1, :cond_1

    .line 200
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/download/AbstractSchedulingService$6;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    # invokes: Lcom/google/android/music/download/AbstractSchedulingService;->sendUpdateEnabledMessage()V
    invoke-static {v0}, Lcom/google/android/music/download/AbstractSchedulingService;->access$600(Lcom/google/android/music/download/AbstractSchedulingService;)V

    .line 202
    :cond_1
    return-void
.end method
