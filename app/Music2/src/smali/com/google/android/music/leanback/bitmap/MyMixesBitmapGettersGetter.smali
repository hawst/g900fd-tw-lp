.class public Lcom/google/android/music/leanback/bitmap/MyMixesBitmapGettersGetter;
.super Lcom/google/android/music/leanback/bitmap/MediaListBitmapGettersGetter;
.source "MyMixesBitmapGettersGetter.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/support/v4/app/LoaderManager;IJ)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "loaderManager"    # Landroid/support/v4/app/LoaderManager;
    .param p3, "loaderId"    # I
    .param p4, "seed"    # J

    .prologue
    .line 22
    invoke-direct/range {p0 .. p5}, Lcom/google/android/music/leanback/bitmap/MediaListBitmapGettersGetter;-><init>(Landroid/content/Context;Landroid/support/v4/app/LoaderManager;IJ)V

    .line 23
    invoke-virtual {p0}, Lcom/google/android/music/leanback/bitmap/MyMixesBitmapGettersGetter;->startLoading()V

    .line 24
    return-void
.end method


# virtual methods
.method protected addBitmapGetters(Landroid/database/Cursor;Ljava/util/ArrayList;)V
    .locals 5
    .param p1, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/music/leanback/bitmap/BitmapGetter;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "bitmapGettersList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/music/leanback/bitmap/BitmapGetter;>;"
    const/4 v2, 0x0

    .line 38
    const/4 v4, 0x3

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/music/utils/MusicUtils;->decodeStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 40
    .local v1, "artUrls":[Ljava/lang/String;
    const/4 v0, 0x0

    .line 41
    .local v0, "artUri":Ljava/lang/String;
    if-eqz v1, :cond_0

    array-length v4, v1

    if-lez v4, :cond_0

    .line 42
    aget-object v0, v1, v2

    .line 44
    :cond_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 45
    const/4 v4, 0x6

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 46
    .local v3, "seedType":I
    const/4 v4, 0x4

    if-ne v3, v4, :cond_1

    const/4 v2, 0x1

    .line 48
    .local v2, "isArtistSeedType":Z
    :cond_1
    new-instance v4, Lcom/google/android/music/leanback/bitmap/DecodedNonAlbumArtUriBitmapGetter;

    invoke-direct {v4, v0, v2}, Lcom/google/android/music/leanback/bitmap/DecodedNonAlbumArtUriBitmapGetter;-><init>(Ljava/lang/String;Z)V

    invoke-virtual {p2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 51
    .end local v2    # "isArtistSeedType":Z
    .end local v3    # "seedType":I
    :cond_2
    return-void
.end method

.method public bridge synthetic getBitmapGetters()[Lcom/google/android/music/leanback/bitmap/BitmapGetter;
    .locals 1

    .prologue
    .line 18
    invoke-super {p0}, Lcom/google/android/music/leanback/bitmap/MediaListBitmapGettersGetter;->getBitmapGetters()[Lcom/google/android/music/leanback/bitmap/BitmapGetter;

    move-result-object v0

    return-object v0
.end method

.method protected getMediaList()Lcom/google/android/music/medialist/MediaList;
    .locals 1

    .prologue
    .line 28
    new-instance v0, Lcom/google/android/music/medialist/MyRadioList;

    invoke-direct {v0}, Lcom/google/android/music/medialist/MyRadioList;-><init>()V

    return-object v0
.end method

.method protected getProjection()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/google/android/music/ui/MyRadioFragment;->PROJECTION:[Ljava/lang/String;

    return-object v0
.end method

.method public bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 0
    .param p1, "x0"    # Landroid/support/v4/content/Loader;
    .param p2, "x1"    # Landroid/database/Cursor;

    .prologue
    .line 18
    invoke-super {p0, p1, p2}, Lcom/google/android/music/leanback/bitmap/MediaListBitmapGettersGetter;->onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public bridge synthetic onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 0
    .param p1, "x0"    # Landroid/support/v4/content/Loader;

    .prologue
    .line 18
    invoke-super {p0, p1}, Lcom/google/android/music/leanback/bitmap/MediaListBitmapGettersGetter;->onLoaderReset(Landroid/support/v4/content/Loader;)V

    return-void
.end method

.method public bridge synthetic startLoading()V
    .locals 0

    .prologue
    .line 18
    invoke-super {p0}, Lcom/google/android/music/leanback/bitmap/MediaListBitmapGettersGetter;->startLoading()V

    return-void
.end method
