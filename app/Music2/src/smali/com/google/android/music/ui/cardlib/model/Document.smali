.class public Lcom/google/android/music/ui/cardlib/model/Document;
.super Ljava/lang/Object;
.source "Document.java"

# interfaces
.implements Landroid/os/Parcelable;
.implements Lcom/google/android/music/ui/cardlib/PlayDocument;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/ui/cardlib/model/Document$2;,
        Lcom/google/android/music/ui/cardlib/model/Document$Type;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/music/ui/cardlib/model/Document;",
            ">;"
        }
    .end annotation
.end field

.field private static UNKNOWN_ALBUM:Ljava/lang/String;

.field private static UNKNOWN_ARTIST:Ljava/lang/String;


# instance fields
.field private mAlbumId:J

.field private mAlbumMetajamId:Ljava/lang/String;

.field private mAlbumName:Ljava/lang/String;

.field private mArtUrl:Ljava/lang/String;

.field private mArtistId:J

.field private mArtistMetajamId:Ljava/lang/String;

.field private mArtistName:Ljava/lang/String;

.field private mCanAddtoLibrary:Z

.field private mCanFollow:Z

.field private mCanRemoveFromLibrary:Z

.field private mDescription:Ljava/lang/String;

.field private mDuration:J

.field private mGenreId:Ljava/lang/String;

.field private mHasLocal:Z

.field private mHasSubSituations:Z

.field private mId:J

.field private mIsEmulatedRadio:Z

.field private mIsFromSearch:Z

.field private mIsNautilus:Z

.field private mMainstageReason:I

.field private mParentGenreId:Ljava/lang/String;

.field private mPlaylistEditorArtwork:Ljava/lang/String;

.field private mPlaylistMemberId:J

.field private mPlaylistName:Ljava/lang/String;

.field private mPlaylistOwnerName:Ljava/lang/String;

.field private mPlaylistShareToken:Ljava/lang/String;

.field private mPlaylistSharedState:I

.field private mPlaylistType:I

.field private mPosition:I

.field private mProfilePhotoUrl:Ljava/lang/String;

.field private mQueueItemContainerId:J

.field private mQueueItemContainerName:Ljava/lang/String;

.field private mQueueItemContainerType:I

.field private mQueueItemId:J

.field private mQueueItemState:I

.field private mRadioHighlightColor:Ljava/lang/String;

.field private mRadioRemoteId:Ljava/lang/String;

.field private mRadioSeedId:Ljava/lang/String;

.field private mRadioSeedType:I

.field private mReason1:Ljava/lang/String;

.field private mSearchString:Ljava/lang/String;

.field private mSituationId:Ljava/lang/String;

.field private mSongStoreId:Ljava/lang/String;

.field private mSourceAccount:I

.field private mSubTitle:Ljava/lang/String;

.field private mSubgenreCount:I

.field private mTitle:Ljava/lang/String;

.field private mTrackMetajamId:Ljava/lang/String;

.field private mType:Lcom/google/android/music/ui/cardlib/model/Document$Type;

.field private mVideoId:Ljava/lang/String;

.field private mVideoThumbnailUrl:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 53
    sput-object v0, Lcom/google/android/music/ui/cardlib/model/Document;->UNKNOWN_ALBUM:Ljava/lang/String;

    .line 54
    sput-object v0, Lcom/google/android/music/ui/cardlib/model/Document;->UNKNOWN_ARTIST:Ljava/lang/String;

    .line 1153
    new-instance v0, Lcom/google/android/music/ui/cardlib/model/Document$1;

    invoke-direct {v0}, Lcom/google/android/music/ui/cardlib/model/Document$1;-><init>()V

    sput-object v0, Lcom/google/android/music/ui/cardlib/model/Document;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 200
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 201
    sget-object v0, Lcom/google/android/music/ui/cardlib/model/Document;->UNKNOWN_ALBUM:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/music/ui/cardlib/model/Document;->UNKNOWN_ARTIST:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 202
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Document.init(Context) should be called before getting here"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 206
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/model/Document;->reset()V

    .line 207
    return-void
.end method

.method static synthetic access$002(Lcom/google/android/music/ui/cardlib/model/Document;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mArtUrl:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1002(Lcom/google/android/music/ui/cardlib/model/Document;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mArtistName:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$102(Lcom/google/android/music/ui/cardlib/model/Document;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mTitle:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1102(Lcom/google/android/music/ui/cardlib/model/Document;Lcom/google/android/music/ui/cardlib/model/Document$Type;)Lcom/google/android/music/ui/cardlib/model/Document$Type;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p1, "x1"    # Lcom/google/android/music/ui/cardlib/model/Document$Type;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mType:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    return-object p1
.end method

.method static synthetic access$1202(Lcom/google/android/music/ui/cardlib/model/Document;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mPlaylistName:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1302(Lcom/google/android/music/ui/cardlib/model/Document;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p1, "x1"    # I

    .prologue
    .line 47
    iput p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mPlaylistType:I

    return p1
.end method

.method static synthetic access$1402(Lcom/google/android/music/ui/cardlib/model/Document;J)J
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p1, "x1"    # J

    .prologue
    .line 47
    iput-wide p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mPlaylistMemberId:J

    return-wide p1
.end method

.method static synthetic access$1502(Lcom/google/android/music/ui/cardlib/model/Document;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mPlaylistShareToken:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1602(Lcom/google/android/music/ui/cardlib/model/Document;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mPlaylistOwnerName:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1702(Lcom/google/android/music/ui/cardlib/model/Document;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mProfilePhotoUrl:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1802(Lcom/google/android/music/ui/cardlib/model/Document;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p1, "x1"    # I

    .prologue
    .line 47
    iput p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mPlaylistSharedState:I

    return p1
.end method

.method static synthetic access$1902(Lcom/google/android/music/ui/cardlib/model/Document;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p1, "x1"    # Z

    .prologue
    .line 47
    iput-boolean p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mIsEmulatedRadio:Z

    return p1
.end method

.method static synthetic access$2002(Lcom/google/android/music/ui/cardlib/model/Document;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mGenreId:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$202(Lcom/google/android/music/ui/cardlib/model/Document;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mSubTitle:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$2102(Lcom/google/android/music/ui/cardlib/model/Document;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mParentGenreId:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$2202(Lcom/google/android/music/ui/cardlib/model/Document;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p1, "x1"    # I

    .prologue
    .line 47
    iput p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mSubgenreCount:I

    return p1
.end method

.method static synthetic access$2302(Lcom/google/android/music/ui/cardlib/model/Document;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p1, "x1"    # Z

    .prologue
    .line 47
    iput-boolean p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mHasLocal:Z

    return p1
.end method

.method static synthetic access$2402(Lcom/google/android/music/ui/cardlib/model/Document;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p1, "x1"    # Z

    .prologue
    .line 47
    iput-boolean p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mIsNautilus:Z

    return p1
.end method

.method static synthetic access$2502(Lcom/google/android/music/ui/cardlib/model/Document;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mTrackMetajamId:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$2602(Lcom/google/android/music/ui/cardlib/model/Document;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mAlbumMetajamId:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$2702(Lcom/google/android/music/ui/cardlib/model/Document;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mArtistMetajamId:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$2802(Lcom/google/android/music/ui/cardlib/model/Document;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p1, "x1"    # Z

    .prologue
    .line 47
    iput-boolean p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mCanAddtoLibrary:Z

    return p1
.end method

.method static synthetic access$2902(Lcom/google/android/music/ui/cardlib/model/Document;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p1, "x1"    # Z

    .prologue
    .line 47
    iput-boolean p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mCanRemoveFromLibrary:Z

    return p1
.end method

.method static synthetic access$3002(Lcom/google/android/music/ui/cardlib/model/Document;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p1, "x1"    # Z

    .prologue
    .line 47
    iput-boolean p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mCanFollow:Z

    return p1
.end method

.method static synthetic access$302(Lcom/google/android/music/ui/cardlib/model/Document;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mDescription:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$3102(Lcom/google/android/music/ui/cardlib/model/Document;J)J
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p1, "x1"    # J

    .prologue
    .line 47
    iput-wide p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mDuration:J

    return-wide p1
.end method

.method static synthetic access$3202(Lcom/google/android/music/ui/cardlib/model/Document;J)J
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p1, "x1"    # J

    .prologue
    .line 47
    iput-wide p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mQueueItemId:J

    return-wide p1
.end method

.method static synthetic access$3302(Lcom/google/android/music/ui/cardlib/model/Document;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p1, "x1"    # I

    .prologue
    .line 47
    iput p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mQueueItemState:I

    return p1
.end method

.method static synthetic access$3402(Lcom/google/android/music/ui/cardlib/model/Document;J)J
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p1, "x1"    # J

    .prologue
    .line 47
    iput-wide p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mQueueItemContainerId:J

    return-wide p1
.end method

.method static synthetic access$3502(Lcom/google/android/music/ui/cardlib/model/Document;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p1, "x1"    # I

    .prologue
    .line 47
    iput p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mQueueItemContainerType:I

    return p1
.end method

.method static synthetic access$3602(Lcom/google/android/music/ui/cardlib/model/Document;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mQueueItemContainerName:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$3702(Lcom/google/android/music/ui/cardlib/model/Document;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p1, "x1"    # Z

    .prologue
    .line 47
    iput-boolean p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mIsFromSearch:Z

    return p1
.end method

.method static synthetic access$3802(Lcom/google/android/music/ui/cardlib/model/Document;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mSearchString:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$3902(Lcom/google/android/music/ui/cardlib/model/Document;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p1, "x1"    # I

    .prologue
    .line 47
    iput p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mPosition:I

    return p1
.end method

.method static synthetic access$4002(Lcom/google/android/music/ui/cardlib/model/Document;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p1, "x1"    # I

    .prologue
    .line 47
    iput p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mRadioSeedType:I

    return p1
.end method

.method static synthetic access$402(Lcom/google/android/music/ui/cardlib/model/Document;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mReason1:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$4102(Lcom/google/android/music/ui/cardlib/model/Document;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mRadioSeedId:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$4202(Lcom/google/android/music/ui/cardlib/model/Document;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p1, "x1"    # I

    .prologue
    .line 47
    iput p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mMainstageReason:I

    return p1
.end method

.method static synthetic access$4302(Lcom/google/android/music/ui/cardlib/model/Document;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mRadioRemoteId:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$4402(Lcom/google/android/music/ui/cardlib/model/Document;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p1, "x1"    # I

    .prologue
    .line 47
    iput p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mSourceAccount:I

    return p1
.end method

.method static synthetic access$4502(Lcom/google/android/music/ui/cardlib/model/Document;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mVideoId:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$4602(Lcom/google/android/music/ui/cardlib/model/Document;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mVideoThumbnailUrl:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$4702(Lcom/google/android/music/ui/cardlib/model/Document;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mPlaylistEditorArtwork:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$4802(Lcom/google/android/music/ui/cardlib/model/Document;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mRadioHighlightColor:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$4902(Lcom/google/android/music/ui/cardlib/model/Document;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mSituationId:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$5002(Lcom/google/android/music/ui/cardlib/model/Document;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p1, "x1"    # Z

    .prologue
    .line 47
    iput-boolean p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mHasSubSituations:Z

    return p1
.end method

.method static synthetic access$502(Lcom/google/android/music/ui/cardlib/model/Document;J)J
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p1, "x1"    # J

    .prologue
    .line 47
    iput-wide p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mId:J

    return-wide p1
.end method

.method static synthetic access$602(Lcom/google/android/music/ui/cardlib/model/Document;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mSongStoreId:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$702(Lcom/google/android/music/ui/cardlib/model/Document;J)J
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p1, "x1"    # J

    .prologue
    .line 47
    iput-wide p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mAlbumId:J

    return-wide p1
.end method

.method static synthetic access$802(Lcom/google/android/music/ui/cardlib/model/Document;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mAlbumName:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$902(Lcom/google/android/music/ui/cardlib/model/Document;J)J
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p1, "x1"    # J

    .prologue
    .line 47
    iput-wide p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mArtistId:J

    return-wide p1
.end method

.method private decodeAndStripIfNeeded(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "remoteUrl"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 431
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 442
    .end local p1    # "remoteUrl":Ljava/lang/String;
    :goto_0
    return-object p1

    .line 434
    .restart local p1    # "remoteUrl":Ljava/lang/String;
    :cond_0
    invoke-static {p1}, Lcom/google/android/music/utils/MusicUtils;->decodeStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 435
    .local v0, "res":[Ljava/lang/String;
    if-eqz v0, :cond_1

    array-length v1, v0

    if-nez v1, :cond_2

    .line 436
    :cond_1
    const/4 p1, 0x0

    goto :goto_0

    .line 438
    :cond_2
    array-length v1, v0

    if-eq v1, v6, :cond_3

    .line 439
    const-string v1, "Document"

    const-string v2, "decodeAndStripIfNeeded: res.length=%s remoteUrl=%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    array-length v4, v0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    aput-object p1, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 442
    :cond_3
    aget-object p1, v0, v5

    goto :goto_0
.end method

.method public static fromSongList(Landroid/content/Context;Lcom/google/android/music/medialist/SongList;)Lcom/google/android/music/ui/cardlib/model/Document;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "songlist"    # Lcom/google/android/music/medialist/SongList;

    .prologue
    .line 1372
    new-instance v2, Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-direct {v2}, Lcom/google/android/music/ui/cardlib/model/Document;-><init>()V

    .line 1373
    .local v2, "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    instance-of v6, p1, Lcom/google/android/music/medialist/ExternalSongList;

    if-eqz v6, :cond_3

    .line 1374
    const/4 v6, 0x1

    invoke-virtual {v2, v6}, Lcom/google/android/music/ui/cardlib/model/Document;->setIsNautilus(Z)V

    .line 1375
    instance-of v6, p1, Lcom/google/android/music/medialist/NautilusAlbumSongList;

    if-eqz v6, :cond_0

    .line 1376
    sget-object v6, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ALBUM:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    invoke-virtual {v2, v6}, Lcom/google/android/music/ui/cardlib/model/Document;->setType(Lcom/google/android/music/ui/cardlib/model/Document$Type;)V

    .line 1377
    check-cast p1, Lcom/google/android/music/medialist/NautilusAlbumSongList;

    .end local p1    # "songlist":Lcom/google/android/music/medialist/SongList;
    invoke-virtual {p1}, Lcom/google/android/music/medialist/NautilusAlbumSongList;->getNautilusId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/google/android/music/ui/cardlib/model/Document;->setAlbumMetajamId(Ljava/lang/String;)V

    .line 1433
    :goto_0
    return-object v2

    .line 1379
    .restart local p1    # "songlist":Lcom/google/android/music/medialist/SongList;
    :cond_0
    instance-of v6, p1, Lcom/google/android/music/medialist/NautilusArtistSongList;

    if-eqz v6, :cond_1

    .line 1380
    sget-object v6, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ALL_SONGS_ARTIST:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    invoke-virtual {v2, v6}, Lcom/google/android/music/ui/cardlib/model/Document;->setType(Lcom/google/android/music/ui/cardlib/model/Document$Type;)V

    move-object v6, p1

    .line 1381
    check-cast v6, Lcom/google/android/music/medialist/NautilusArtistSongList;

    invoke-virtual {v6}, Lcom/google/android/music/medialist/NautilusArtistSongList;->getNautilusId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/google/android/music/ui/cardlib/model/Document;->setArtistMetajamId(Ljava/lang/String;)V

    .line 1382
    check-cast p1, Lcom/google/android/music/medialist/NautilusArtistSongList;

    .end local p1    # "songlist":Lcom/google/android/music/medialist/SongList;
    invoke-virtual {p1, p0}, Lcom/google/android/music/medialist/NautilusArtistSongList;->getName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/google/android/music/ui/cardlib/model/Document;->setArtistName(Ljava/lang/String;)V

    goto :goto_0

    .line 1383
    .restart local p1    # "songlist":Lcom/google/android/music/medialist/SongList;
    :cond_1
    instance-of v6, p1, Lcom/google/android/music/medialist/SharedWithMeSongList;

    if-eqz v6, :cond_2

    .line 1384
    sget-object v6, Lcom/google/android/music/ui/cardlib/model/Document$Type;->PLAYLIST:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    invoke-virtual {v2, v6}, Lcom/google/android/music/ui/cardlib/model/Document;->setType(Lcom/google/android/music/ui/cardlib/model/Document$Type;)V

    .line 1385
    const/16 v6, 0x46

    invoke-virtual {v2, v6}, Lcom/google/android/music/ui/cardlib/model/Document;->setPlaylistType(I)V

    move-object v6, p1

    .line 1386
    check-cast v6, Lcom/google/android/music/medialist/SharedWithMeSongList;

    invoke-virtual {v6}, Lcom/google/android/music/medialist/SharedWithMeSongList;->getShareToken()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/google/android/music/ui/cardlib/model/Document;->setPlaylistShareToken(Ljava/lang/String;)V

    .line 1387
    invoke-virtual {p1, p0}, Lcom/google/android/music/medialist/SongList;->getName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/google/android/music/ui/cardlib/model/Document;->setPlaylistName(Ljava/lang/String;)V

    goto :goto_0

    .line 1389
    :cond_2
    const-string v6, "Document"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Unsupported ExternalSongList type: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1392
    :cond_3
    instance-of v6, p1, Lcom/google/android/music/medialist/AlbumSongList;

    if-eqz v6, :cond_4

    .line 1393
    sget-object v6, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ALBUM:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    invoke-virtual {v2, v6}, Lcom/google/android/music/ui/cardlib/model/Document;->setType(Lcom/google/android/music/ui/cardlib/model/Document$Type;)V

    move-object v0, p1

    .line 1394
    check-cast v0, Lcom/google/android/music/medialist/AlbumSongList;

    .line 1395
    .local v0, "albumSongList":Lcom/google/android/music/medialist/AlbumSongList;
    invoke-virtual {v0, p0}, Lcom/google/android/music/medialist/AlbumSongList;->getAlbumId(Landroid/content/Context;)J

    move-result-wide v6

    invoke-virtual {v2, v6, v7}, Lcom/google/android/music/ui/cardlib/model/Document;->setAlbumId(J)V

    .line 1396
    invoke-virtual {v0, p0}, Lcom/google/android/music/medialist/AlbumSongList;->getAlbumMetajamId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/google/android/music/ui/cardlib/model/Document;->setAlbumMetajamId(Ljava/lang/String;)V

    goto :goto_0

    .line 1398
    .end local v0    # "albumSongList":Lcom/google/android/music/medialist/AlbumSongList;
    :cond_4
    instance-of v6, p1, Lcom/google/android/music/medialist/ArtistSongList;

    if-eqz v6, :cond_5

    .line 1399
    sget-object v6, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ALL_SONGS_ARTIST:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    invoke-virtual {v2, v6}, Lcom/google/android/music/ui/cardlib/model/Document;->setType(Lcom/google/android/music/ui/cardlib/model/Document$Type;)V

    move-object v1, p1

    .line 1400
    check-cast v1, Lcom/google/android/music/medialist/ArtistSongList;

    .line 1401
    .local v1, "artistSongList":Lcom/google/android/music/medialist/ArtistSongList;
    invoke-virtual {v1, p0}, Lcom/google/android/music/medialist/ArtistSongList;->getArtistId(Landroid/content/Context;)J

    move-result-wide v6

    invoke-virtual {v2, v6, v7}, Lcom/google/android/music/ui/cardlib/model/Document;->setArtistId(J)V

    .line 1402
    invoke-virtual {v1, p0}, Lcom/google/android/music/medialist/ArtistSongList;->getArtistMetajamId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/google/android/music/ui/cardlib/model/Document;->setArtistMetajamId(Ljava/lang/String;)V

    .line 1403
    invoke-virtual {v1}, Lcom/google/android/music/medialist/ArtistSongList;->getArtistName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/google/android/music/ui/cardlib/model/Document;->setArtistName(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1404
    .end local v1    # "artistSongList":Lcom/google/android/music/medialist/ArtistSongList;
    :cond_5
    instance-of v6, p1, Lcom/google/android/music/medialist/PlaylistSongList;

    if-eqz v6, :cond_6

    .line 1405
    sget-object v6, Lcom/google/android/music/ui/cardlib/model/Document$Type;->PLAYLIST:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    invoke-virtual {v2, v6}, Lcom/google/android/music/ui/cardlib/model/Document;->setType(Lcom/google/android/music/ui/cardlib/model/Document$Type;)V

    move-object v4, p1

    .line 1406
    check-cast v4, Lcom/google/android/music/medialist/PlaylistSongList;

    .line 1407
    .local v4, "playlist":Lcom/google/android/music/medialist/PlaylistSongList;
    invoke-virtual {v4}, Lcom/google/android/music/medialist/PlaylistSongList;->getId()J

    move-result-wide v6

    invoke-virtual {v2, v6, v7}, Lcom/google/android/music/ui/cardlib/model/Document;->setId(J)V

    .line 1408
    invoke-virtual {v4}, Lcom/google/android/music/medialist/PlaylistSongList;->getPlaylistName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/google/android/music/ui/cardlib/model/Document;->setTitle(Ljava/lang/String;)V

    .line 1409
    invoke-virtual {v4}, Lcom/google/android/music/medialist/PlaylistSongList;->getPlaylistName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/google/android/music/ui/cardlib/model/Document;->setPlaylistName(Ljava/lang/String;)V

    .line 1410
    invoke-virtual {v4}, Lcom/google/android/music/medialist/PlaylistSongList;->getShareToken()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/google/android/music/ui/cardlib/model/Document;->setPlaylistShareToken(Ljava/lang/String;)V

    .line 1411
    invoke-virtual {v4}, Lcom/google/android/music/medialist/PlaylistSongList;->getPlaylistType()I

    move-result v6

    invoke-virtual {v2, v6}, Lcom/google/android/music/ui/cardlib/model/Document;->setPlaylistType(I)V

    goto/16 :goto_0

    .line 1412
    .end local v4    # "playlist":Lcom/google/android/music/medialist/PlaylistSongList;
    :cond_6
    instance-of v6, p1, Lcom/google/android/music/medialist/AutoPlaylistSongList;

    if-eqz v6, :cond_7

    .line 1413
    sget-object v6, Lcom/google/android/music/ui/cardlib/model/Document$Type;->PLAYLIST:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    invoke-virtual {v2, v6}, Lcom/google/android/music/ui/cardlib/model/Document;->setType(Lcom/google/android/music/ui/cardlib/model/Document$Type;)V

    move-object v4, p1

    .line 1414
    check-cast v4, Lcom/google/android/music/medialist/AutoPlaylistSongList;

    .line 1415
    .local v4, "playlist":Lcom/google/android/music/medialist/AutoPlaylistSongList;
    invoke-virtual {v4}, Lcom/google/android/music/medialist/AutoPlaylistSongList;->getId()J

    move-result-wide v6

    invoke-virtual {v2, v6, v7}, Lcom/google/android/music/ui/cardlib/model/Document;->setId(J)V

    .line 1416
    const/16 v6, 0x64

    invoke-virtual {v2, v6}, Lcom/google/android/music/ui/cardlib/model/Document;->setPlaylistType(I)V

    goto/16 :goto_0

    .line 1417
    .end local v4    # "playlist":Lcom/google/android/music/medialist/AutoPlaylistSongList;
    :cond_7
    instance-of v6, p1, Lcom/google/android/music/medialist/GenreSongList;

    if-eqz v6, :cond_8

    .line 1418
    sget-object v6, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ALL_SONGS_GENRE:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    invoke-virtual {v2, v6}, Lcom/google/android/music/ui/cardlib/model/Document;->setType(Lcom/google/android/music/ui/cardlib/model/Document$Type;)V

    move-object v3, p1

    .line 1419
    check-cast v3, Lcom/google/android/music/medialist/GenreSongList;

    .line 1420
    .local v3, "genreSongList":Lcom/google/android/music/medialist/GenreSongList;
    invoke-virtual {v3}, Lcom/google/android/music/medialist/GenreSongList;->getGenreId()J

    move-result-wide v6

    invoke-virtual {v2, v6, v7}, Lcom/google/android/music/ui/cardlib/model/Document;->setId(J)V

    .line 1421
    invoke-virtual {p1, p0}, Lcom/google/android/music/medialist/SongList;->getName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/google/android/music/ui/cardlib/model/Document;->setTitle(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1422
    .end local v3    # "genreSongList":Lcom/google/android/music/medialist/GenreSongList;
    :cond_8
    instance-of v6, p1, Lcom/google/android/music/medialist/RadioStationSongList;

    if-eqz v6, :cond_9

    .line 1423
    sget-object v6, Lcom/google/android/music/ui/cardlib/model/Document$Type;->RADIO:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    invoke-virtual {v2, v6}, Lcom/google/android/music/ui/cardlib/model/Document;->setType(Lcom/google/android/music/ui/cardlib/model/Document$Type;)V

    move-object v5, p1

    .line 1424
    check-cast v5, Lcom/google/android/music/medialist/RadioStationSongList;

    .line 1425
    .local v5, "radioSongList":Lcom/google/android/music/medialist/RadioStationSongList;
    invoke-virtual {v5}, Lcom/google/android/music/medialist/RadioStationSongList;->getRadioStationId()J

    move-result-wide v6

    invoke-virtual {v2, v6, v7}, Lcom/google/android/music/ui/cardlib/model/Document;->setId(J)V

    .line 1426
    invoke-virtual {v5}, Lcom/google/android/music/medialist/RadioStationSongList;->getRadioStationName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/google/android/music/ui/cardlib/model/Document;->setTitle(Ljava/lang/String;)V

    .line 1427
    invoke-virtual {v5}, Lcom/google/android/music/medialist/RadioStationSongList;->getDescription()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/google/android/music/ui/cardlib/model/Document;->setDescription(Ljava/lang/String;)V

    .line 1428
    invoke-virtual {v5}, Lcom/google/android/music/medialist/RadioStationSongList;->getArtUrl()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/google/android/music/ui/cardlib/model/Document;->setArtUrl(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1430
    .end local v5    # "radioSongList":Lcom/google/android/music/medialist/RadioStationSongList;
    :cond_9
    const-string v6, "Document"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Unsupported SongList type: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public static getImFeelingLuckyDocument(Landroid/content/Context;ZZZZ)Lcom/google/android/music/ui/cardlib/model/Document;
    .locals 15
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "showSubtitle"    # Z
    .param p2, "showReason"    # Z
    .param p3, "reasonIsSubtitle"    # Z
    .param p4, "preventTransparent"    # Z

    .prologue
    .line 1453
    new-instance v0, Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-direct {v0}, Lcom/google/android/music/ui/cardlib/model/Document;-><init>()V

    .line 1454
    .local v0, "d":Lcom/google/android/music/ui/cardlib/model/Document;
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 1455
    .local v6, "res":Landroid/content/res/Resources;
    sget-object v11, Lcom/google/android/music/ui/cardlib/model/Document$Type;->IM_FEELING_LUCKY:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    invoke-virtual {v0, v11}, Lcom/google/android/music/ui/cardlib/model/Document;->setType(Lcom/google/android/music/ui/cardlib/model/Document$Type;)V

    .line 1458
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v10

    .line 1459
    .local v10, "ui":Lcom/google/android/music/ui/UIStateManager;
    invoke-virtual {v10}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v4

    .line 1460
    .local v4, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    invoke-virtual {v4}, Lcom/google/android/music/preferences/MusicPreferences;->isNautilusEnabled()Z

    move-result v3

    .line 1461
    .local v3, "nautilus":Z
    invoke-static {p0}, Lcom/google/android/music/utils/MusicUtils;->canStartImFeelingLucky(Landroid/content/Context;)Z

    move-result v1

    .line 1462
    .local v1, "iflRadioEnabled":Z
    invoke-virtual {v4}, Lcom/google/android/music/preferences/MusicPreferences;->hasStreamingAccount()Z

    move-result v11

    if-nez v11, :cond_6

    const/4 v2, 0x1

    .line 1465
    .local v2, "isSideloadedOnly":Z
    :goto_0
    if-nez v1, :cond_7

    .line 1466
    const v11, 0x7f0b0054

    invoke-virtual {v6, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 1473
    .local v9, "title":Ljava/lang/String;
    :goto_1
    invoke-virtual {v0, v9}, Lcom/google/android/music/ui/cardlib/model/Document;->setTitle(Ljava/lang/String;)V

    .line 1474
    const/4 v7, 0x0

    .line 1475
    .local v7, "subtitle":Ljava/lang/String;
    const/4 v5, 0x0

    .line 1476
    .local v5, "reason1":Ljava/lang/String;
    if-eqz p1, :cond_0

    .line 1477
    if-eqz v1, :cond_a

    .line 1478
    if-eqz v3, :cond_9

    const v8, 0x7f0b0264

    .line 1480
    .local v8, "subtitleId":I
    :goto_2
    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 1485
    .end local v8    # "subtitleId":I
    :cond_0
    :goto_3
    if-eqz p2, :cond_1

    .line 1486
    if-eqz v1, :cond_b

    .line 1487
    const v11, 0x7f0b02e6

    invoke-virtual {v6, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 1498
    :cond_1
    :goto_4
    if-eqz p3, :cond_2

    .line 1499
    move-object v7, v5

    .line 1500
    const/4 v5, 0x0

    .line 1502
    :cond_2
    if-eqz v7, :cond_3

    .line 1503
    invoke-virtual {v0, v7}, Lcom/google/android/music/ui/cardlib/model/Document;->setSubTitle(Ljava/lang/String;)V

    .line 1505
    :cond_3
    if-eqz v5, :cond_4

    .line 1506
    invoke-virtual {v0, v5}, Lcom/google/android/music/ui/cardlib/model/Document;->setReason1(Ljava/lang/String;)V

    .line 1508
    :cond_4
    if-eqz p4, :cond_5

    .line 1509
    const/4 v11, 0x1

    iput-boolean v11, v0, Lcom/google/android/music/ui/cardlib/model/Document;->mHasLocal:Z

    .line 1511
    :cond_5
    return-object v0

    .line 1462
    .end local v2    # "isSideloadedOnly":Z
    .end local v5    # "reason1":Ljava/lang/String;
    .end local v7    # "subtitle":Ljava/lang/String;
    .end local v9    # "title":Ljava/lang/String;
    :cond_6
    const/4 v2, 0x0

    goto :goto_0

    .line 1468
    .restart local v2    # "isSideloadedOnly":Z
    :cond_7
    if-eqz v3, :cond_8

    const v11, 0x7f0b02eb

    :goto_5
    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    const v14, 0x7f0b02ed

    invoke-virtual {v6, v14}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-virtual {v6, v11, v12}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    .restart local v9    # "title":Ljava/lang/String;
    goto :goto_1

    .end local v9    # "title":Ljava/lang/String;
    :cond_8
    const v11, 0x7f0b02ec

    goto :goto_5

    .line 1478
    .restart local v5    # "reason1":Ljava/lang/String;
    .restart local v7    # "subtitle":Ljava/lang/String;
    .restart local v9    # "title":Ljava/lang/String;
    :cond_9
    const v8, 0x7f0b0265

    goto :goto_2

    .line 1482
    :cond_a
    const-string v7, ""

    goto :goto_3

    .line 1489
    :cond_b
    if-eqz v2, :cond_c

    .line 1490
    const v11, 0x7f0b02e7

    invoke-virtual {v6, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    goto :goto_4

    .line 1493
    :cond_c
    const v11, 0x7f0b02e8

    invoke-virtual {v6, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    goto :goto_4
.end method

.method public static declared-synchronized init(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 274
    const-class v1, Lcom/google/android/music/ui/cardlib/model/Document;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/music/ui/cardlib/model/Document;->UNKNOWN_ALBUM:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/music/ui/cardlib/model/Document;->UNKNOWN_ARTIST:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 275
    :cond_0
    const v0, 0x7f0b00c5

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/ui/cardlib/model/Document;->UNKNOWN_ALBUM:Ljava/lang/String;

    .line 276
    const v0, 0x7f0b00c4

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/ui/cardlib/model/Document;->UNKNOWN_ARTIST:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 278
    :cond_1
    monitor-exit v1

    return-void

    .line 274
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static isRadioSupportedForPlaylistType(Landroid/content/Context;I)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "playlistType"    # I

    .prologue
    const/4 v1, 0x1

    .line 1516
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "music_enable_playlist_radio"

    invoke-static {v2, v3, v1}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    .line 1519
    .local v0, "enablePlaylistRadio":Z
    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    const/16 v2, 0x47

    if-eq p1, v2, :cond_0

    const/16 v2, 0x46

    if-ne p1, v2, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private makeSongListForPlayList(Landroid/content/Context;)Lcom/google/android/music/medialist/SongList;
    .locals 12
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v11, 0x1

    .line 1039
    iget v1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mPlaylistType:I

    sparse-switch v1, :sswitch_data_0

    .line 1058
    const-string v1, "Document"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unexpected playlist type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mPlaylistType:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    .line 1059
    const/4 v1, 0x0

    :goto_0
    return-object v1

    .line 1041
    :sswitch_0
    invoke-static {p1}, Lcom/google/android/music/ui/UIStateManager;->getInstance(Landroid/content/Context;)Lcom/google/android/music/ui/UIStateManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    .line 1042
    .local v0, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/model/Document;->getId()J

    move-result-wide v2

    invoke-static {v2, v3, v11, v0}, Lcom/google/android/music/medialist/AutoPlaylistSongList;->getAutoPlaylist(JZLcom/google/android/music/preferences/MusicPreferences;)Lcom/google/android/music/medialist/AutoPlaylistSongList;

    move-result-object v1

    goto :goto_0

    .line 1049
    .end local v0    # "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    :sswitch_1
    new-instance v1, Lcom/google/android/music/medialist/PlaylistSongList;

    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/model/Document;->getId()J

    move-result-wide v2

    iget-object v4, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mPlaylistName:Ljava/lang/String;

    iget v5, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mPlaylistType:I

    iget-object v6, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mDescription:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mPlaylistOwnerName:Ljava/lang/String;

    iget-object v8, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mPlaylistShareToken:Ljava/lang/String;

    iget-object v9, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mArtUrl:Ljava/lang/String;

    iget-object v10, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mProfilePhotoUrl:Ljava/lang/String;

    invoke-direct/range {v1 .. v11}, Lcom/google/android/music/medialist/PlaylistSongList;-><init>(JLjava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0

    .line 1055
    :sswitch_2
    new-instance v1, Lcom/google/android/music/medialist/SharedWithMeSongList;

    iget-wide v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mId:J

    iget-object v4, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mPlaylistShareToken:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mPlaylistName:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mDescription:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mPlaylistOwnerName:Ljava/lang/String;

    iget-object v8, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mArtUrl:Ljava/lang/String;

    iget-object v9, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mProfilePhotoUrl:Ljava/lang/String;

    invoke-direct/range {v1 .. v9}, Lcom/google/android/music/medialist/SharedWithMeSongList;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1039
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_1
        0x1 -> :sswitch_1
        0x32 -> :sswitch_1
        0x33 -> :sswitch_2
        0x3c -> :sswitch_1
        0x46 -> :sswitch_2
        0x47 -> :sswitch_1
        0x50 -> :sswitch_1
        0x64 -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public canAddToLibrary()Z
    .locals 1

    .prologue
    .line 694
    iget-boolean v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mCanAddtoLibrary:Z

    return v0
.end method

.method public canFollow()Z
    .locals 1

    .prologue
    .line 710
    iget-boolean v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mCanFollow:Z

    return v0
.end method

.method public canRemoveFromLibrary()Z
    .locals 1

    .prologue
    .line 702
    iget-boolean v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mCanRemoveFromLibrary:Z

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 1086
    const/4 v0, 0x0

    return v0
.end method

.method public dismissFromMainstage(Landroid/content/ContentResolver;)V
    .locals 9
    .param p1, "cr"    # Landroid/content/ContentResolver;

    .prologue
    .line 1540
    sget-object v0, Lcom/google/android/music/ui/cardlib/model/Document$2;->$SwitchMap$com$google$android$music$ui$cardlib$model$Document$Type:[I

    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1569
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Dismissing and unsupported doc type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1542
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/model/Document;->getPosition()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/model/Document;->getMainstageReason()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/model/Document;->getAlbumName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/model/Document;->getArtistName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/model/Document;->getAlbumId()J

    move-result-wide v6

    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/model/Document;->getAlbumMetajamId()Ljava/lang/String;

    move-result-object v8

    move-object v1, p1

    invoke-static/range {v1 .. v8}, Lcom/google/android/music/store/MusicContent$Mainstage;->dismissAlbum(Landroid/content/ContentResolver;IILjava/lang/String;Ljava/lang/String;JLjava/lang/String;)V

    .line 1572
    :goto_0
    return-void

    .line 1552
    :pswitch_1
    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/model/Document;->getPosition()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/model/Document;->getMainstageReason()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/model/Document;->getId()J

    move-result-wide v4

    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/model/Document;->getPlaylistShareToken()Ljava/lang/String;

    move-result-object v6

    move-object v1, p1

    invoke-static/range {v1 .. v6}, Lcom/google/android/music/store/MusicContent$Mainstage;->dismissList(Landroid/content/ContentResolver;IIJLjava/lang/String;)V

    goto :goto_0

    .line 1560
    :pswitch_2
    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/model/Document;->getPosition()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/model/Document;->getMainstageReason()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/model/Document;->getRadioRemoteId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/model/Document;->getRadioSeedId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/model/Document;->getRadioSeedType()I

    move-result v5

    move-object v0, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/store/MusicContent$Mainstage;->dismissRadioStation(Landroid/content/ContentResolver;IILjava/lang/String;Ljava/lang/String;I)V

    goto :goto_0

    .line 1540
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public doesSupportKeepOn(Landroid/content/Context;)Z
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1064
    sget-object v2, Lcom/google/android/music/ui/cardlib/model/Document$2;->$SwitchMap$com$google$android$music$ui$cardlib$model$Document$Type:[I

    iget-object v3, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mType:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    invoke-virtual {v3}, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    move v0, v1

    .line 1080
    :goto_0
    :pswitch_0
    return v0

    .line 1068
    :pswitch_1
    iget v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mPlaylistType:I

    invoke-static {v0}, Lcom/google/android/music/medialist/PlaylistSongList;->doesPlaylistSupportKeepOn(I)Z

    move-result v0

    goto :goto_0

    .line 1070
    :pswitch_2
    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/model/Document;->getRadioRemoteId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    .line 1072
    goto :goto_0

    .line 1074
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "music_enable_keepon_radio"

    invoke-static {v1, v2, v0}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0

    .line 1064
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1254
    if-ne p0, p1, :cond_1

    .line 1358
    :cond_0
    :goto_0
    return v1

    .line 1257
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 1258
    goto :goto_0

    .line 1260
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 1261
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 1263
    check-cast v0, Lcom/google/android/music/ui/cardlib/model/Document;

    .line 1264
    .local v0, "other":Lcom/google/android/music/ui/cardlib/model/Document;
    iget-wide v4, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mAlbumId:J

    iget-wide v6, v0, Lcom/google/android/music/ui/cardlib/model/Document;->mAlbumId:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_4

    move v1, v2

    .line 1265
    goto :goto_0

    .line 1267
    :cond_4
    iget-object v3, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mAlbumMetajamId:Ljava/lang/String;

    if-nez v3, :cond_5

    .line 1268
    iget-object v3, v0, Lcom/google/android/music/ui/cardlib/model/Document;->mAlbumMetajamId:Ljava/lang/String;

    if-eqz v3, :cond_6

    move v1, v2

    .line 1269
    goto :goto_0

    .line 1271
    :cond_5
    iget-object v3, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mAlbumMetajamId:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/music/ui/cardlib/model/Document;->mAlbumMetajamId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    move v1, v2

    .line 1272
    goto :goto_0

    .line 1274
    :cond_6
    iget-wide v4, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mArtistId:J

    iget-wide v6, v0, Lcom/google/android/music/ui/cardlib/model/Document;->mArtistId:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_7

    move v1, v2

    .line 1275
    goto :goto_0

    .line 1277
    :cond_7
    iget-object v3, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mArtistMetajamId:Ljava/lang/String;

    if-nez v3, :cond_8

    .line 1278
    iget-object v3, v0, Lcom/google/android/music/ui/cardlib/model/Document;->mArtistMetajamId:Ljava/lang/String;

    if-eqz v3, :cond_9

    move v1, v2

    .line 1279
    goto :goto_0

    .line 1281
    :cond_8
    iget-object v3, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mArtistMetajamId:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/music/ui/cardlib/model/Document;->mArtistMetajamId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_9

    move v1, v2

    .line 1282
    goto :goto_0

    .line 1284
    :cond_9
    iget-object v3, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mGenreId:Ljava/lang/String;

    if-nez v3, :cond_a

    .line 1285
    iget-object v3, v0, Lcom/google/android/music/ui/cardlib/model/Document;->mGenreId:Ljava/lang/String;

    if-eqz v3, :cond_b

    move v1, v2

    .line 1286
    goto :goto_0

    .line 1288
    :cond_a
    iget-object v3, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mGenreId:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/music/ui/cardlib/model/Document;->mGenreId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_b

    move v1, v2

    .line 1289
    goto :goto_0

    .line 1291
    :cond_b
    iget-wide v4, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mId:J

    iget-wide v6, v0, Lcom/google/android/music/ui/cardlib/model/Document;->mId:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_c

    move v1, v2

    .line 1292
    goto :goto_0

    .line 1294
    :cond_c
    iget-object v3, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mParentGenreId:Ljava/lang/String;

    if-nez v3, :cond_d

    .line 1295
    iget-object v3, v0, Lcom/google/android/music/ui/cardlib/model/Document;->mParentGenreId:Ljava/lang/String;

    if-eqz v3, :cond_e

    move v1, v2

    .line 1296
    goto :goto_0

    .line 1298
    :cond_d
    iget-object v3, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mParentGenreId:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/music/ui/cardlib/model/Document;->mParentGenreId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_e

    move v1, v2

    .line 1299
    goto/16 :goto_0

    .line 1301
    :cond_e
    iget-wide v4, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mPlaylistMemberId:J

    iget-wide v6, v0, Lcom/google/android/music/ui/cardlib/model/Document;->mPlaylistMemberId:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_f

    move v1, v2

    .line 1302
    goto/16 :goto_0

    .line 1304
    :cond_f
    iget-object v3, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mPlaylistShareToken:Ljava/lang/String;

    if-nez v3, :cond_10

    .line 1305
    iget-object v3, v0, Lcom/google/android/music/ui/cardlib/model/Document;->mPlaylistShareToken:Ljava/lang/String;

    if-eqz v3, :cond_11

    move v1, v2

    .line 1306
    goto/16 :goto_0

    .line 1308
    :cond_10
    iget-object v3, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mPlaylistShareToken:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/music/ui/cardlib/model/Document;->mPlaylistShareToken:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_11

    move v1, v2

    .line 1309
    goto/16 :goto_0

    .line 1311
    :cond_11
    iget v3, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mPlaylistType:I

    iget v4, v0, Lcom/google/android/music/ui/cardlib/model/Document;->mPlaylistType:I

    if-eq v3, v4, :cond_12

    move v1, v2

    .line 1312
    goto/16 :goto_0

    .line 1314
    :cond_12
    iget-wide v4, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mQueueItemId:J

    iget-wide v6, v0, Lcom/google/android/music/ui/cardlib/model/Document;->mQueueItemId:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_13

    move v1, v2

    .line 1315
    goto/16 :goto_0

    .line 1317
    :cond_13
    iget-object v3, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mRadioRemoteId:Ljava/lang/String;

    if-nez v3, :cond_14

    .line 1318
    iget-object v3, v0, Lcom/google/android/music/ui/cardlib/model/Document;->mRadioRemoteId:Ljava/lang/String;

    if-eqz v3, :cond_15

    move v1, v2

    .line 1319
    goto/16 :goto_0

    .line 1321
    :cond_14
    iget-object v3, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mRadioRemoteId:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/music/ui/cardlib/model/Document;->mRadioRemoteId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_15

    move v1, v2

    .line 1322
    goto/16 :goto_0

    .line 1324
    :cond_15
    iget-object v3, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mRadioSeedId:Ljava/lang/String;

    if-nez v3, :cond_16

    .line 1325
    iget-object v3, v0, Lcom/google/android/music/ui/cardlib/model/Document;->mRadioSeedId:Ljava/lang/String;

    if-eqz v3, :cond_17

    move v1, v2

    .line 1326
    goto/16 :goto_0

    .line 1328
    :cond_16
    iget-object v3, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mRadioSeedId:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/music/ui/cardlib/model/Document;->mRadioSeedId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_17

    move v1, v2

    .line 1329
    goto/16 :goto_0

    .line 1331
    :cond_17
    iget v3, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mRadioSeedType:I

    iget v4, v0, Lcom/google/android/music/ui/cardlib/model/Document;->mRadioSeedType:I

    if-eq v3, v4, :cond_18

    move v1, v2

    .line 1332
    goto/16 :goto_0

    .line 1334
    :cond_18
    iget-object v3, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mSongStoreId:Ljava/lang/String;

    if-nez v3, :cond_19

    .line 1335
    iget-object v3, v0, Lcom/google/android/music/ui/cardlib/model/Document;->mSongStoreId:Ljava/lang/String;

    if-eqz v3, :cond_1a

    move v1, v2

    .line 1336
    goto/16 :goto_0

    .line 1338
    :cond_19
    iget-object v3, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mSongStoreId:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/music/ui/cardlib/model/Document;->mSongStoreId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1a

    move v1, v2

    .line 1339
    goto/16 :goto_0

    .line 1341
    :cond_1a
    iget-object v3, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mTrackMetajamId:Ljava/lang/String;

    if-nez v3, :cond_1b

    .line 1342
    iget-object v3, v0, Lcom/google/android/music/ui/cardlib/model/Document;->mTrackMetajamId:Ljava/lang/String;

    if-eqz v3, :cond_1c

    move v1, v2

    .line 1343
    goto/16 :goto_0

    .line 1345
    :cond_1b
    iget-object v3, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mTrackMetajamId:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/music/ui/cardlib/model/Document;->mTrackMetajamId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1c

    move v1, v2

    .line 1346
    goto/16 :goto_0

    .line 1348
    :cond_1c
    iget-object v3, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mType:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    iget-object v4, v0, Lcom/google/android/music/ui/cardlib/model/Document;->mType:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-eq v3, v4, :cond_1d

    move v1, v2

    .line 1349
    goto/16 :goto_0

    .line 1351
    :cond_1d
    iget-object v3, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mSituationId:Ljava/lang/String;

    if-nez v3, :cond_1e

    .line 1352
    iget-object v3, v0, Lcom/google/android/music/ui/cardlib/model/Document;->mSituationId:Ljava/lang/String;

    if-eqz v3, :cond_0

    move v1, v2

    .line 1353
    goto/16 :goto_0

    .line 1355
    :cond_1e
    iget-object v3, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mSituationId:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/music/ui/cardlib/model/Document;->mSituationId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 1356
    goto/16 :goto_0
.end method

.method public getAlbumId()J
    .locals 2

    .prologue
    .line 383
    iget-wide v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mAlbumId:J

    return-wide v0
.end method

.method public getAlbumMetajamId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 569
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mAlbumMetajamId:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mAlbumMetajamId:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x42

    if-eq v0, v1, :cond_0

    .line 570
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "B"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mAlbumMetajamId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 572
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mAlbumMetajamId:Ljava/lang/String;

    goto :goto_0
.end method

.method public getAlbumName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 391
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mAlbumName:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/music/utils/MusicUtils;->isUnknown(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/music/ui/cardlib/model/Document;->UNKNOWN_ALBUM:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mAlbumName:Ljava/lang/String;

    goto :goto_0
.end method

.method public getArtUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 415
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mArtUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getArtistId()J
    .locals 2

    .prologue
    .line 399
    iget-wide v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mArtistId:J

    return-wide v0
.end method

.method public getArtistMetajamId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 582
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mArtistMetajamId:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mArtistMetajamId:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x41

    if-eq v0, v1, :cond_0

    .line 583
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "A"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mArtistMetajamId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 585
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mArtistMetajamId:Ljava/lang/String;

    goto :goto_0
.end method

.method public getArtistName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 407
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mArtistName:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/music/utils/MusicUtils;->isUnknown(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/music/ui/cardlib/model/Document;->UNKNOWN_ARTIST:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mArtistName:Ljava/lang/String;

    goto :goto_0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 505
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mDescription:Ljava/lang/String;

    return-object v0
.end method

.method public getDuration()J
    .locals 2

    .prologue
    .line 497
    iget-wide v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mDuration:J

    return-wide v0
.end method

.method public getGenreId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 613
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mGenreId:Ljava/lang/String;

    return-object v0
.end method

.method public getId()J
    .locals 2

    .prologue
    .line 353
    iget-wide v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mId:J

    return-wide v0
.end method

.method public getIdInParent()J
    .locals 2

    .prologue
    .line 364
    iget-wide v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mPlaylistMemberId:J

    return-wide v0
.end method

.method public getIsAvailable()Z
    .locals 1

    .prologue
    .line 690
    iget-boolean v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mHasLocal:Z

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/ui/UIStateManager;->isStreamingEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getIsEmulateRadio()Z
    .locals 1

    .prologue
    .line 285
    iget-boolean v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mIsEmulatedRadio:Z

    return v0
.end method

.method public getMainstageReason()I
    .locals 1

    .prologue
    .line 841
    iget v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mMainstageReason:I

    return v0
.end method

.method public getParentGenreId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 617
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mParentGenreId:Ljava/lang/String;

    return-object v0
.end method

.method public getPlaylistName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 521
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mPlaylistName:Ljava/lang/String;

    return-object v0
.end method

.method public getPlaylistOwnerName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 605
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mPlaylistOwnerName:Ljava/lang/String;

    return-object v0
.end method

.method public getPlaylistShareState()I
    .locals 1

    .prologue
    .line 452
    iget v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mPlaylistSharedState:I

    return v0
.end method

.method public getPlaylistShareToken()Ljava/lang/String;
    .locals 1

    .prologue
    .line 597
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mPlaylistShareToken:Ljava/lang/String;

    return-object v0
.end method

.method public getPlaylistType()I
    .locals 1

    .prologue
    .line 529
    iget v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mPlaylistType:I

    return v0
.end method

.method public getPosition()I
    .locals 1

    .prologue
    .line 805
    iget v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mPosition:I

    return v0
.end method

.method public getProfilePhotoUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 423
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mProfilePhotoUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getQueueItemId()J
    .locals 2

    .prologue
    .line 749
    iget-wide v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mQueueItemId:J

    return-wide v0
.end method

.method public getRadioHighlightColor()Ljava/lang/String;
    .locals 1

    .prologue
    .line 889
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mRadioHighlightColor:Ljava/lang/String;

    return-object v0
.end method

.method public getRadioRemoteId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 849
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mRadioRemoteId:Ljava/lang/String;

    return-object v0
.end method

.method public getRadioSeedId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 637
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mRadioSeedId:Ljava/lang/String;

    return-object v0
.end method

.method public getRadioSeedType()I
    .locals 1

    .prologue
    .line 657
    iget v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mRadioSeedType:I

    return v0
.end method

.method public getReason1()Ljava/lang/String;
    .locals 1

    .prologue
    .line 513
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mReason1:Ljava/lang/String;

    return-object v0
.end method

.method public getSearchString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 797
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mSearchString:Ljava/lang/String;

    return-object v0
.end method

.method public getSituationId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 897
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mSituationId:Ljava/lang/String;

    return-object v0
.end method

.method public getSongList(Landroid/content/Context;)Lcom/google/android/music/medialist/SongList;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 978
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/music/ui/cardlib/model/Document;->getSongList(Landroid/content/Context;Z)Lcom/google/android/music/medialist/SongList;

    move-result-object v0

    return-object v0
.end method

.method public getSongList(Landroid/content/Context;Z)Lcom/google/android/music/medialist/SongList;
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "filter"    # Z

    .prologue
    const/4 v8, 0x0

    const/4 v5, -0x1

    .line 982
    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v0

    sget-object v1, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ALBUM:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-ne v0, v1, :cond_1

    .line 983
    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/model/Document;->isNautilus()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 984
    new-instance v0, Lcom/google/android/music/medialist/NautilusAlbumSongList;

    iget-object v1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mAlbumMetajamId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mArtistMetajamId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mAlbumName:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mArtistName:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mDescription:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mArtUrl:Ljava/lang/String;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/music/medialist/NautilusAlbumSongList;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v0

    .line 1034
    :goto_0
    return-object v1

    .line 987
    :cond_0
    new-instance v1, Lcom/google/android/music/medialist/AlbumSongList;

    iget-wide v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mAlbumId:J

    iget-wide v4, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mArtistId:J

    iget-object v6, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mAlbumName:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mArtistName:Ljava/lang/String;

    move v9, p2

    invoke-direct/range {v1 .. v9}, Lcom/google/android/music/medialist/AlbumSongList;-><init>(JJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0

    .line 990
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v0

    sget-object v1, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ARTIST:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-ne v0, v1, :cond_3

    .line 991
    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/model/Document;->isNautilus()Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v1, Lcom/google/android/music/medialist/NautilusArtistSongList;

    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mArtistMetajamId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mArtistName:Ljava/lang/String;

    invoke-direct {v1, v0, v2}, Lcom/google/android/music/medialist/NautilusArtistSongList;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    new-instance v1, Lcom/google/android/music/medialist/ArtistSongList;

    iget-wide v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mArtistId:J

    iget-object v4, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mArtistName:Ljava/lang/String;

    move v6, p2

    invoke-direct/range {v1 .. v6}, Lcom/google/android/music/medialist/ArtistSongList;-><init>(JLjava/lang/String;IZ)V

    goto :goto_0

    .line 993
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v0

    sget-object v1, Lcom/google/android/music/ui/cardlib/model/Document$Type;->PLAYLIST:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-ne v0, v1, :cond_4

    .line 994
    invoke-direct {p0, p1}, Lcom/google/android/music/ui/cardlib/model/Document;->makeSongListForPlayList(Landroid/content/Context;)Lcom/google/android/music/medialist/SongList;

    move-result-object v1

    goto :goto_0

    .line 995
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v0

    sget-object v1, Lcom/google/android/music/ui/cardlib/model/Document$Type;->TRACK:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-ne v0, v1, :cond_8

    .line 996
    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/model/Document;->isFromSearch()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 997
    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/model/Document;->isNautilus()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 998
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mTrackMetajamId:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mSearchString:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/music/store/ContainerDescriptor;->newSearchResultsDescriptor(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v10

    .line 1001
    .local v10, "descriptor":Lcom/google/android/music/store/ContainerDescriptor;
    new-instance v1, Lcom/google/android/music/medialist/NautilusSingleSongList;

    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mTrackMetajamId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mTitle:Ljava/lang/String;

    invoke-direct {v1, v10, v0, v2}, Lcom/google/android/music/medialist/NautilusSingleSongList;-><init>(Lcom/google/android/music/store/ContainerDescriptor;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1004
    .end local v10    # "descriptor":Lcom/google/android/music/store/ContainerDescriptor;
    :cond_5
    iget-wide v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mId:J

    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mSearchString:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/ContainerDescriptor;->newSearchResultsDescriptor(JLjava/lang/String;)Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v10

    .line 1007
    .restart local v10    # "descriptor":Lcom/google/android/music/store/ContainerDescriptor;
    new-instance v1, Lcom/google/android/music/medialist/SingleSongList;

    iget-wide v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mId:J

    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mTitle:Ljava/lang/String;

    invoke-direct {v1, v10, v2, v3, v0}, Lcom/google/android/music/medialist/SingleSongList;-><init>(Lcom/google/android/music/store/ContainerDescriptor;JLjava/lang/String;)V

    goto :goto_0

    .line 1012
    .end local v10    # "descriptor":Lcom/google/android/music/store/ContainerDescriptor;
    :cond_6
    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/model/Document;->isNautilus()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1013
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mTrackMetajamId:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mTitle:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/music/store/ContainerDescriptor;->newNautilusSingleSongDescriptor(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v10

    .line 1016
    .restart local v10    # "descriptor":Lcom/google/android/music/store/ContainerDescriptor;
    new-instance v1, Lcom/google/android/music/medialist/NautilusSingleSongList;

    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mTrackMetajamId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mTitle:Ljava/lang/String;

    invoke-direct {v1, v10, v0, v2}, Lcom/google/android/music/medialist/NautilusSingleSongList;-><init>(Lcom/google/android/music/store/ContainerDescriptor;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1018
    .end local v10    # "descriptor":Lcom/google/android/music/store/ContainerDescriptor;
    :cond_7
    iget-wide v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mId:J

    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mTitle:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/ContainerDescriptor;->newSingleSongDescriptor(JLjava/lang/String;)Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v10

    .line 1020
    .restart local v10    # "descriptor":Lcom/google/android/music/store/ContainerDescriptor;
    new-instance v1, Lcom/google/android/music/medialist/SingleSongList;

    iget-wide v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mId:J

    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mTitle:Ljava/lang/String;

    invoke-direct {v1, v10, v2, v3, v0}, Lcom/google/android/music/medialist/SingleSongList;-><init>(Lcom/google/android/music/store/ContainerDescriptor;JLjava/lang/String;)V

    goto/16 :goto_0

    .line 1024
    .end local v10    # "descriptor":Lcom/google/android/music/store/ContainerDescriptor;
    :cond_8
    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v0

    sget-object v1, Lcom/google/android/music/ui/cardlib/model/Document$Type;->GENRE:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-ne v0, v1, :cond_9

    .line 1025
    new-instance v1, Lcom/google/android/music/medialist/GenreSongList;

    iget-wide v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mId:J

    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mTitle:Ljava/lang/String;

    invoke-direct {v1, v2, v3, v0, v5}, Lcom/google/android/music/medialist/GenreSongList;-><init>(JLjava/lang/String;I)V

    goto/16 :goto_0

    .line 1026
    :cond_9
    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v0

    sget-object v1, Lcom/google/android/music/ui/cardlib/model/Document$Type;->RADIO:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-ne v0, v1, :cond_a

    .line 1027
    new-instance v1, Lcom/google/android/music/medialist/RadioStationSongList;

    iget-wide v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mId:J

    iget-object v4, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mRadioRemoteId:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mTitle:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mDescription:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mArtUrl:Ljava/lang/String;

    invoke-direct/range {v1 .. v7}, Lcom/google/android/music/medialist/RadioStationSongList;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1028
    :cond_a
    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v0

    sget-object v1, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ALL_SONGS_ARTIST:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-ne v0, v1, :cond_b

    .line 1029
    new-instance v1, Lcom/google/android/music/medialist/ArtistSongList;

    iget-wide v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mArtistId:J

    iget-object v4, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mArtistName:Ljava/lang/String;

    move v6, p2

    invoke-direct/range {v1 .. v6}, Lcom/google/android/music/medialist/ArtistSongList;-><init>(JLjava/lang/String;IZ)V

    goto/16 :goto_0

    .line 1030
    :cond_b
    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v0

    sget-object v1, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ALL_SONGS_GENRE:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-ne v0, v1, :cond_c

    .line 1031
    new-instance v1, Lcom/google/android/music/medialist/GenreSongList;

    iget-wide v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mId:J

    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mTitle:Ljava/lang/String;

    invoke-direct {v1, v2, v3, v0, v5}, Lcom/google/android/music/medialist/GenreSongList;-><init>(JLjava/lang/String;I)V

    goto/16 :goto_0

    .line 1033
    :cond_c
    const-string v0, "Document"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getSongList, unexpected document type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v8

    .line 1034
    goto/16 :goto_0
.end method

.method public getSongStoreId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 372
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mSongStoreId:Ljava/lang/String;

    return-object v0
.end method

.method public getSourceAccount()I
    .locals 1

    .prologue
    .line 857
    iget v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mSourceAccount:I

    return v0
.end method

.method public getSubTitle()Ljava/lang/String;
    .locals 2

    .prologue
    .line 483
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mSubTitle:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/music/utils/MusicUtils;->isUnknown(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 484
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mType:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    sget-object v1, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ALBUM:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-ne v0, v1, :cond_0

    .line 486
    sget-object v0, Lcom/google/android/music/ui/cardlib/model/Document;->UNKNOWN_ARTIST:Ljava/lang/String;

    .line 489
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mSubTitle:Ljava/lang/String;

    goto :goto_0
.end method

.method public getSubgenreCount()I
    .locals 1

    .prologue
    .line 625
    iget v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mSubgenreCount:I

    return v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 2

    .prologue
    .line 468
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mTitle:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/music/utils/MusicUtils;->isUnknown(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 469
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mType:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    sget-object v1, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ALBUM:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-ne v0, v1, :cond_0

    .line 470
    sget-object v0, Lcom/google/android/music/ui/cardlib/model/Document;->UNKNOWN_ALBUM:Ljava/lang/String;

    .line 475
    :goto_0
    return-object v0

    .line 471
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mType:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    sget-object v1, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ARTIST:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-ne v0, v1, :cond_1

    .line 472
    sget-object v0, Lcom/google/android/music/ui/cardlib/model/Document;->UNKNOWN_ARTIST:Ljava/lang/String;

    goto :goto_0

    .line 475
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mTitle:Ljava/lang/String;

    goto :goto_0
.end method

.method public getTrackMetajamId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 559
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mTrackMetajamId:Ljava/lang/String;

    return-object v0
.end method

.method public getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;
    .locals 1

    .prologue
    .line 460
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mType:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    return-object v0
.end method

.method public getVideoId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 865
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mVideoId:Ljava/lang/String;

    return-object v0
.end method

.method public getVideoThumbnailUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 873
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mVideoThumbnailUrl:Ljava/lang/String;

    return-object v0
.end method

.method public hasSubSituations()Z
    .locals 1

    .prologue
    .line 905
    iget-boolean v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mHasSubSituations:Z

    return v0
.end method

.method public hashCode()I
    .locals 9

    .prologue
    const/16 v8, 0x20

    const/4 v3, 0x0

    .line 1228
    const/16 v0, 0x1f

    .line 1229
    .local v0, "prime":I
    const/4 v1, 0x1

    .line 1230
    .local v1, "result":I
    iget-wide v4, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mAlbumId:J

    iget-wide v6, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mAlbumId:J

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v2, v4

    add-int/lit8 v1, v2, 0x1f

    .line 1231
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mAlbumMetajamId:Ljava/lang/String;

    if-nez v2, :cond_0

    move v2, v3

    :goto_0
    add-int v1, v4, v2

    .line 1232
    mul-int/lit8 v2, v1, 0x1f

    iget-wide v4, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mArtistId:J

    iget-wide v6, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mArtistId:J

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v4, v4

    add-int v1, v2, v4

    .line 1233
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mArtistMetajamId:Ljava/lang/String;

    if-nez v2, :cond_1

    move v2, v3

    :goto_1
    add-int v1, v4, v2

    .line 1234
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mGenreId:Ljava/lang/String;

    if-nez v2, :cond_2

    move v2, v3

    :goto_2
    add-int v1, v4, v2

    .line 1235
    mul-int/lit8 v2, v1, 0x1f

    iget-wide v4, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mId:J

    iget-wide v6, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mId:J

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v4, v4

    add-int v1, v2, v4

    .line 1236
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mParentGenreId:Ljava/lang/String;

    if-nez v2, :cond_3

    move v2, v3

    :goto_3
    add-int v1, v4, v2

    .line 1237
    mul-int/lit8 v2, v1, 0x1f

    iget-wide v4, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mPlaylistMemberId:J

    iget-wide v6, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mPlaylistMemberId:J

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v4, v4

    add-int v1, v2, v4

    .line 1238
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mPlaylistShareToken:Ljava/lang/String;

    if-nez v2, :cond_4

    move v2, v3

    :goto_4
    add-int v1, v4, v2

    .line 1240
    mul-int/lit8 v2, v1, 0x1f

    iget v4, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mPlaylistType:I

    add-int v1, v2, v4

    .line 1241
    mul-int/lit8 v2, v1, 0x1f

    iget-wide v4, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mQueueItemId:J

    iget-wide v6, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mQueueItemId:J

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v4, v4

    add-int v1, v2, v4

    .line 1242
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mRadioRemoteId:Ljava/lang/String;

    if-nez v2, :cond_5

    move v2, v3

    :goto_5
    add-int v1, v4, v2

    .line 1243
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mRadioSeedId:Ljava/lang/String;

    if-nez v2, :cond_6

    move v2, v3

    :goto_6
    add-int v1, v4, v2

    .line 1244
    mul-int/lit8 v2, v1, 0x1f

    iget v4, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mRadioSeedType:I

    add-int v1, v2, v4

    .line 1245
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mSongStoreId:Ljava/lang/String;

    if-nez v2, :cond_7

    move v2, v3

    :goto_7
    add-int v1, v4, v2

    .line 1246
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mTrackMetajamId:Ljava/lang/String;

    if-nez v2, :cond_8

    move v2, v3

    :goto_8
    add-int v1, v4, v2

    .line 1247
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mType:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-nez v2, :cond_9

    move v2, v3

    :goto_9
    add-int v1, v4, v2

    .line 1248
    mul-int/lit8 v2, v1, 0x1f

    iget-object v4, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mSituationId:Ljava/lang/String;

    if-nez v4, :cond_a

    :goto_a
    add-int v1, v2, v3

    .line 1249
    return v1

    .line 1231
    :cond_0
    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mAlbumMetajamId:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_0

    .line 1233
    :cond_1
    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mArtistMetajamId:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_1

    .line 1234
    :cond_2
    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mGenreId:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_2

    .line 1236
    :cond_3
    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mParentGenreId:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_3

    .line 1238
    :cond_4
    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mPlaylistShareToken:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_4

    .line 1242
    :cond_5
    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mRadioRemoteId:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_5

    .line 1243
    :cond_6
    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mRadioSeedId:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_6

    .line 1245
    :cond_7
    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mSongStoreId:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_7

    .line 1246
    :cond_8
    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mTrackMetajamId:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_8

    .line 1247
    :cond_9
    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mType:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    invoke-virtual {v2}, Lcom/google/android/music/ui/cardlib/model/Document$Type;->hashCode()I

    move-result v2

    goto :goto_9

    .line 1248
    :cond_a
    iget-object v3, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mSituationId:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    goto :goto_a
.end method

.method public isFromSearch()Z
    .locals 1

    .prologue
    .line 789
    iget-boolean v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mIsFromSearch:Z

    return v0
.end method

.method public isGenreRadio()Z
    .locals 1

    .prologue
    .line 633
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mGenreId:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mIsEmulatedRadio:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isNautilus()Z
    .locals 2

    .prologue
    .line 543
    iget-boolean v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mIsNautilus:Z

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mId:J

    invoke-static {v0, v1}, Lcom/google/android/music/store/ProjectionUtils;->isFauxNautilusId(J)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isOnMainstage()Z
    .locals 2

    .prologue
    .line 1530
    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/model/Document;->getMainstageReason()I

    move-result v0

    const/4 v1, -0x1

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isTopLevelGenre()Z
    .locals 2

    .prologue
    .line 678
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mType:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    sget-object v1, Lcom/google/android/music/ui/cardlib/model/Document$Type;->NAUTILUS_GENRE:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mParentGenreId:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public reset()V
    .locals 7

    .prologue
    const/4 v6, -0x1

    const-wide/16 v4, -0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 289
    iput-object v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mArtUrl:Ljava/lang/String;

    .line 290
    iput-object v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mTitle:Ljava/lang/String;

    .line 291
    iput-object v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mSubTitle:Ljava/lang/String;

    .line 292
    iput-object v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mDescription:Ljava/lang/String;

    .line 293
    iput-object v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mReason1:Ljava/lang/String;

    .line 294
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mDuration:J

    .line 296
    iput-wide v4, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mId:J

    .line 297
    iput-boolean v3, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mIsNautilus:Z

    .line 298
    iput-object v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mTrackMetajamId:Ljava/lang/String;

    .line 299
    iput-object v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mAlbumMetajamId:Ljava/lang/String;

    .line 300
    iput-object v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mArtistMetajamId:Ljava/lang/String;

    .line 301
    iput-wide v4, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mPlaylistMemberId:J

    .line 302
    iput-object v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mSongStoreId:Ljava/lang/String;

    .line 303
    iput-wide v4, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mAlbumId:J

    .line 304
    iput-object v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mAlbumName:Ljava/lang/String;

    .line 305
    iput-wide v4, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mArtistId:J

    .line 306
    iput-object v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mArtistName:Ljava/lang/String;

    .line 307
    sget-object v0, Lcom/google/android/music/ui/cardlib/model/Document$Type;->UNKNOWN:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    iput-object v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mType:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    .line 309
    iput-object v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mPlaylistName:Ljava/lang/String;

    .line 310
    iput v6, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mPlaylistType:I

    .line 311
    iput-object v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mPlaylistShareToken:Ljava/lang/String;

    .line 312
    iput-object v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mPlaylistOwnerName:Ljava/lang/String;

    .line 313
    iput-object v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mProfilePhotoUrl:Ljava/lang/String;

    .line 314
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mPlaylistSharedState:I

    .line 317
    iput-boolean v3, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mIsEmulatedRadio:Z

    .line 319
    iput-object v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mGenreId:Ljava/lang/String;

    .line 320
    iput-object v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mParentGenreId:Ljava/lang/String;

    .line 321
    iput v3, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mSubgenreCount:I

    .line 324
    iput-boolean v3, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mHasLocal:Z

    .line 326
    iput-boolean v3, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mCanAddtoLibrary:Z

    .line 328
    iput-boolean v3, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mCanRemoveFromLibrary:Z

    .line 329
    iput-boolean v3, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mCanFollow:Z

    .line 331
    iput-wide v4, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mQueueItemId:J

    .line 332
    iput v3, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mQueueItemState:I

    .line 333
    iput-wide v4, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mQueueItemContainerId:J

    .line 334
    iput v3, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mQueueItemContainerType:I

    .line 335
    iput-object v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mQueueItemContainerName:Ljava/lang/String;

    .line 336
    iput-boolean v3, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mIsFromSearch:Z

    .line 337
    iput-object v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mSearchString:Ljava/lang/String;

    .line 338
    iput v6, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mPosition:I

    .line 339
    iput-object v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mRadioSeedId:Ljava/lang/String;

    .line 340
    iput v3, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mRadioSeedType:I

    .line 341
    iput v6, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mMainstageReason:I

    .line 342
    iput-object v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mRadioRemoteId:Ljava/lang/String;

    .line 343
    sget-object v0, Lcom/google/android/music/store/MusicFile;->MEDIA_STORE_SOURCE_ACCOUNT_AS_INTEGER:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mSourceAccount:I

    .line 344
    iput-object v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mVideoId:Ljava/lang/String;

    .line 345
    iput-object v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mVideoThumbnailUrl:Ljava/lang/String;

    .line 346
    iput-object v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mPlaylistEditorArtwork:Ljava/lang/String;

    .line 347
    iput-object v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mRadioHighlightColor:Ljava/lang/String;

    .line 348
    iput-object v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mSituationId:Ljava/lang/String;

    .line 349
    iput-boolean v3, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mHasSubSituations:Z

    .line 350
    return-void
.end method

.method public setAlbumId(J)V
    .locals 1
    .param p1, "albumId"    # J

    .prologue
    .line 387
    iput-wide p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mAlbumId:J

    .line 388
    return-void
.end method

.method public setAlbumMetajamId(Ljava/lang/String;)V
    .locals 0
    .param p1, "albumMetajamId"    # Ljava/lang/String;

    .prologue
    .line 576
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mAlbumMetajamId:Ljava/lang/String;

    .line 577
    return-void
.end method

.method public setAlbumName(Ljava/lang/String;)V
    .locals 0
    .param p1, "albumName"    # Ljava/lang/String;

    .prologue
    .line 395
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mAlbumName:Ljava/lang/String;

    .line 396
    return-void
.end method

.method public setArtUrl(Ljava/lang/String;)V
    .locals 0
    .param p1, "artUrl"    # Ljava/lang/String;

    .prologue
    .line 419
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mArtUrl:Ljava/lang/String;

    .line 420
    return-void
.end method

.method public setArtistId(J)V
    .locals 1
    .param p1, "artistId"    # J

    .prologue
    .line 403
    iput-wide p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mArtistId:J

    .line 404
    return-void
.end method

.method public setArtistMetajamId(Ljava/lang/String;)V
    .locals 0
    .param p1, "artistMetajamId"    # Ljava/lang/String;

    .prologue
    .line 589
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mArtistMetajamId:Ljava/lang/String;

    .line 590
    return-void
.end method

.method public setArtistName(Ljava/lang/String;)V
    .locals 0
    .param p1, "artistName"    # Ljava/lang/String;

    .prologue
    .line 411
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mArtistName:Ljava/lang/String;

    .line 412
    return-void
.end method

.method public setCanAddToLibrary(Z)V
    .locals 0
    .param p1, "addToLibrary"    # Z

    .prologue
    .line 698
    iput-boolean p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mCanAddtoLibrary:Z

    .line 699
    return-void
.end method

.method public setCanFollow(Z)V
    .locals 0
    .param p1, "canFollow"    # Z

    .prologue
    .line 714
    iput-boolean p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mCanFollow:Z

    .line 715
    return-void
.end method

.method public setCanRemoveFromLibrary(Z)V
    .locals 0
    .param p1, "canRemoveFromLibrary"    # Z

    .prologue
    .line 706
    iput-boolean p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mCanRemoveFromLibrary:Z

    .line 707
    return-void
.end method

.method public setDescription(Ljava/lang/String;)V
    .locals 0
    .param p1, "description"    # Ljava/lang/String;

    .prologue
    .line 509
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mDescription:Ljava/lang/String;

    .line 510
    return-void
.end method

.method public setDuration(J)V
    .locals 1
    .param p1, "duration"    # J

    .prologue
    .line 501
    iput-wide p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mDuration:J

    .line 502
    return-void
.end method

.method public setGenreId(Ljava/lang/String;)V
    .locals 0
    .param p1, "genreId"    # Ljava/lang/String;

    .prologue
    .line 609
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mGenreId:Ljava/lang/String;

    .line 610
    return-void
.end method

.method public setHasLocal(Z)V
    .locals 0
    .param p1, "hasLocal"    # Z

    .prologue
    .line 682
    iput-boolean p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mHasLocal:Z

    .line 683
    return-void
.end method

.method public setHasSubSituations(Z)V
    .locals 0
    .param p1, "hasSubSituations"    # Z

    .prologue
    .line 901
    iput-boolean p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mHasSubSituations:Z

    .line 902
    return-void
.end method

.method public setId(J)V
    .locals 1
    .param p1, "id"    # J

    .prologue
    .line 360
    iput-wide p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mId:J

    .line 361
    return-void
.end method

.method public setIdInParent(J)V
    .locals 1
    .param p1, "id"    # J

    .prologue
    .line 368
    iput-wide p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mPlaylistMemberId:J

    .line 369
    return-void
.end method

.method public setIsEmulatedRadio(Z)V
    .locals 0
    .param p1, "isEmulatedRadio"    # Z

    .prologue
    .line 281
    iput-boolean p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mIsEmulatedRadio:Z

    .line 282
    return-void
.end method

.method public setIsFromSearch(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 785
    iput-boolean p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mIsFromSearch:Z

    .line 786
    return-void
.end method

.method public setIsNautilus(Z)V
    .locals 0
    .param p1, "isNautilus"    # Z

    .prologue
    .line 555
    iput-boolean p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mIsNautilus:Z

    .line 556
    return-void
.end method

.method public setMainstageReason(I)V
    .locals 0
    .param p1, "mainstageReason"    # I

    .prologue
    .line 823
    invoke-static {p1}, Lcom/google/android/music/store/MusicContent$Mainstage;->throwIfInvalidReason(I)V

    .line 824
    iput p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mMainstageReason:I

    .line 825
    return-void
.end method

.method public setParentGenreId(Ljava/lang/String;)V
    .locals 0
    .param p1, "parentGenreId"    # Ljava/lang/String;

    .prologue
    .line 621
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mParentGenreId:Ljava/lang/String;

    .line 622
    return-void
.end method

.method public setPlaylistEditorArtwork(Ljava/lang/String;)V
    .locals 0
    .param p1, "playlistEditorArtwork"    # Ljava/lang/String;

    .prologue
    .line 877
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mPlaylistEditorArtwork:Ljava/lang/String;

    .line 878
    return-void
.end method

.method public setPlaylistName(Ljava/lang/String;)V
    .locals 0
    .param p1, "playlistName"    # Ljava/lang/String;

    .prologue
    .line 525
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mPlaylistName:Ljava/lang/String;

    .line 526
    return-void
.end method

.method public setPlaylistOwnerName(Ljava/lang/String;)V
    .locals 0
    .param p1, "ownerName"    # Ljava/lang/String;

    .prologue
    .line 601
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mPlaylistOwnerName:Ljava/lang/String;

    .line 602
    return-void
.end method

.method public setPlaylistShareState(I)V
    .locals 0
    .param p1, "shareState"    # I

    .prologue
    .line 456
    iput p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mPlaylistSharedState:I

    .line 457
    return-void
.end method

.method public setPlaylistShareToken(Ljava/lang/String;)V
    .locals 0
    .param p1, "sharedToken"    # Ljava/lang/String;

    .prologue
    .line 593
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mPlaylistShareToken:Ljava/lang/String;

    .line 594
    return-void
.end method

.method public setPlaylistType(I)V
    .locals 0
    .param p1, "playlistType"    # I

    .prologue
    .line 533
    iput p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mPlaylistType:I

    .line 534
    return-void
.end method

.method public setPosition(I)V
    .locals 0
    .param p1, "position"    # I

    .prologue
    .line 801
    iput p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mPosition:I

    .line 802
    return-void
.end method

.method public setProfilePhotoUrl(Ljava/lang/String;)V
    .locals 1
    .param p1, "ownerProfilePhotoUrl"    # Ljava/lang/String;

    .prologue
    .line 427
    invoke-direct {p0, p1}, Lcom/google/android/music/ui/cardlib/model/Document;->decodeAndStripIfNeeded(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mProfilePhotoUrl:Ljava/lang/String;

    .line 428
    return-void
.end method

.method public setQueueItemContainerId(J)V
    .locals 1
    .param p1, "id"    # J

    .prologue
    .line 761
    iput-wide p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mQueueItemContainerId:J

    .line 762
    return-void
.end method

.method public setQueueItemContainerName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 777
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mQueueItemContainerName:Ljava/lang/String;

    .line 778
    return-void
.end method

.method public setQueueItemContainerType(I)V
    .locals 0
    .param p1, "type"    # I

    .prologue
    .line 769
    iput p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mQueueItemContainerType:I

    .line 770
    return-void
.end method

.method public setQueueItemId(J)V
    .locals 1
    .param p1, "id"    # J

    .prologue
    .line 745
    iput-wide p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mQueueItemId:J

    .line 746
    return-void
.end method

.method public setQueueItemState(I)V
    .locals 0
    .param p1, "state"    # I

    .prologue
    .line 753
    iput p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mQueueItemState:I

    .line 754
    return-void
.end method

.method public setRadioHighlightColor(Ljava/lang/String;)V
    .locals 0
    .param p1, "radioHighlightColor"    # Ljava/lang/String;

    .prologue
    .line 885
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mRadioHighlightColor:Ljava/lang/String;

    .line 886
    return-void
.end method

.method public setRadioRemoteId(Ljava/lang/String;)V
    .locals 0
    .param p1, "remoteId"    # Ljava/lang/String;

    .prologue
    .line 845
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mRadioRemoteId:Ljava/lang/String;

    .line 846
    return-void
.end method

.method public setRadioSeedId(Ljava/lang/String;)V
    .locals 0
    .param p1, "radioSeedId"    # Ljava/lang/String;

    .prologue
    .line 641
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mRadioSeedId:Ljava/lang/String;

    .line 642
    return-void
.end method

.method public setRadioSeedType(I)V
    .locals 0
    .param p1, "radioSeedType"    # I

    .prologue
    .line 674
    iput p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mRadioSeedType:I

    .line 675
    return-void
.end method

.method public setReason1(Ljava/lang/String;)V
    .locals 0
    .param p1, "reason1"    # Ljava/lang/String;

    .prologue
    .line 517
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mReason1:Ljava/lang/String;

    .line 518
    return-void
.end method

.method public setSearchString(Ljava/lang/String;)V
    .locals 0
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 793
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mSearchString:Ljava/lang/String;

    .line 794
    return-void
.end method

.method public setSituationId(Ljava/lang/String;)V
    .locals 0
    .param p1, "situationId"    # Ljava/lang/String;

    .prologue
    .line 893
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mSituationId:Ljava/lang/String;

    .line 894
    return-void
.end method

.method public setSongStoreId(Ljava/lang/String;)V
    .locals 0
    .param p1, "storeId"    # Ljava/lang/String;

    .prologue
    .line 379
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mSongStoreId:Ljava/lang/String;

    .line 380
    return-void
.end method

.method public setSourceAccount(I)V
    .locals 0
    .param p1, "sourceAccount"    # I

    .prologue
    .line 853
    iput p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mSourceAccount:I

    .line 854
    return-void
.end method

.method public setSubTitle(Ljava/lang/String;)V
    .locals 0
    .param p1, "subTitle"    # Ljava/lang/String;

    .prologue
    .line 493
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mSubTitle:Ljava/lang/String;

    .line 494
    return-void
.end method

.method public setSubgenreCount(I)V
    .locals 0
    .param p1, "subgenreCount"    # I

    .prologue
    .line 629
    iput p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mSubgenreCount:I

    .line 630
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 479
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mTitle:Ljava/lang/String;

    .line 480
    return-void
.end method

.method public setTrackMetajamId(Ljava/lang/String;)V
    .locals 0
    .param p1, "trackMetajamId"    # Ljava/lang/String;

    .prologue
    .line 563
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mTrackMetajamId:Ljava/lang/String;

    .line 564
    return-void
.end method

.method public setType(Lcom/google/android/music/ui/cardlib/model/Document$Type;)V
    .locals 0
    .param p1, "type"    # Lcom/google/android/music/ui/cardlib/model/Document$Type;

    .prologue
    .line 464
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mType:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    .line 465
    return-void
.end method

.method public setVideoId(Ljava/lang/String;)V
    .locals 0
    .param p1, "videoId"    # Ljava/lang/String;

    .prologue
    .line 861
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mVideoId:Ljava/lang/String;

    .line 862
    return-void
.end method

.method public setVideoThumbnailUrl(Ljava/lang/String;)V
    .locals 0
    .param p1, "thumbnailUrl"    # Ljava/lang/String;

    .prologue
    .line 869
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mVideoThumbnailUrl:Ljava/lang/String;

    .line 870
    return-void
.end method

.method public shouldShowContextMenu()Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 721
    iget-object v3, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mType:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    sget-object v4, Lcom/google/android/music/ui/cardlib/model/Document$Type;->PLAYLIST:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-ne v3, v4, :cond_1

    iget v3, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mPlaylistType:I

    const/16 v4, 0x32

    if-ne v3, v4, :cond_1

    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/model/Document;->isOnMainstage()Z

    move-result v3

    if-nez v3, :cond_1

    .line 741
    :cond_0
    :goto_0
    return v2

    .line 727
    :cond_1
    iget-object v3, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mType:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    sget-object v4, Lcom/google/android/music/ui/cardlib/model/Document$Type;->RADIO:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-ne v3, v4, :cond_2

    iget-object v3, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mRadioRemoteId:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget v3, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mRadioSeedType:I

    const/16 v4, 0x9

    if-eq v3, v4, :cond_0

    .line 732
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/model/Document;->isGenreRadio()Z

    move-result v3

    if-nez v3, :cond_4

    iget-object v3, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mType:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    sget-object v4, Lcom/google/android/music/ui/cardlib/model/Document$Type;->NAUTILUS_GENRE:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-eq v3, v4, :cond_4

    iget-object v3, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mType:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    sget-object v4, Lcom/google/android/music/ui/cardlib/model/Document$Type;->IM_FEELING_LUCKY:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-eq v3, v4, :cond_4

    iget-object v3, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mType:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    sget-object v4, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ALL_SONGS_ARTIST:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-eq v3, v4, :cond_4

    iget-object v3, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mType:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    sget-object v4, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ALL_SONGS_GENRE:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-eq v3, v4, :cond_4

    iget-object v3, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mType:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    sget-object v4, Lcom/google/android/music/ui/cardlib/model/Document$Type;->VIDEO:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-eq v3, v4, :cond_4

    move v0, v1

    .line 738
    .local v0, "show":Z
    :goto_1
    if-eqz v0, :cond_3

    iget-object v3, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mType:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    sget-object v4, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ARTIST:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-ne v3, v4, :cond_3

    .line 739
    iget-wide v4, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mArtistId:J

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-eqz v3, :cond_5

    move v0, v1

    :cond_3
    :goto_2
    move v2, v0

    .line 741
    goto :goto_0

    .end local v0    # "show":Z
    :cond_4
    move v0, v2

    .line 732
    goto :goto_1

    .restart local v0    # "show":Z
    :cond_5
    move v0, v2

    .line 739
    goto :goto_2
.end method

.method public shouldShowContextMenuForArtist()Z
    .locals 1

    .prologue
    .line 909
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->hasStreamingAccount()Z

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 914
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 915
    .local v0, "builder":Ljava/lang/StringBuilder;
    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 916
    const-string v1, "mArtUrl="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mArtUrl:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 917
    const-string v1, ", mTitle="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mTitle:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 918
    const-string v1, ", mSubTitle="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mSubTitle:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 919
    const-string v1, ", mDescription="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mDescription:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 920
    const-string v1, ", mReason1="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mReason1:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 921
    const-string v1, ", mDuration="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mDuration:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 922
    const-string v1, ", mId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mId:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 923
    const-string v1, ", mAlbumId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mAlbumId:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 924
    const-string v1, ", mAlbumName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mAlbumName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 925
    const-string v1, ", mArtistId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mArtistId:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 926
    const-string v1, ", mArtistName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mArtistName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 927
    const-string v1, ", mType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mType:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 928
    const-string v1, ", mIsNautilus="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/model/Document;->isNautilus()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 929
    const-string v1, ", mTrackMetajamId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mTrackMetajamId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 930
    const-string v1, ", mAlbumMetajamId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mAlbumMetajamId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 931
    const-string v1, ", mArtistMetajamId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mArtistMetajamId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 932
    const-string v1, ", mPlaylistName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mPlaylistName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 933
    const-string v1, ", mPlaylistType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mPlaylistType:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 934
    const-string v1, ", mPlaylistShareToken="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mPlaylistShareToken:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 935
    const-string v1, ", mPlaylistOwnerName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mPlaylistOwnerName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 936
    const-string v1, ", mProfilePhotoUrl="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mProfilePhotoUrl:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 937
    const-string v1, ", mPlaylistSharedState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mPlaylistSharedState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 938
    const-string v1, ", mPlaylistMemberId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mPlaylistMemberId:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 939
    const-string v1, ", mSongStoreId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mSongStoreId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 940
    const-string v1, ", mIsEmulatedRadio="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mIsEmulatedRadio:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 941
    const-string v1, ", mGenreId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mGenreId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 942
    const-string v1, ", mParentGenreId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mParentGenreId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 943
    const-string v1, ", mSubgenreCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mSubgenreCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 944
    const-string v1, ", mHasLocal="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mHasLocal:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 945
    const-string v1, ", mCanAddtoLibrary="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mCanAddtoLibrary:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 946
    const-string v1, ", mQueueItemId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mQueueItemId:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 947
    const-string v1, ", mQueueItemState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mQueueItemState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 948
    const-string v1, ", mQueueItemContainerId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mQueueItemContainerId:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 949
    const-string v1, ", mQueueItemContainerType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mQueueItemContainerType:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 950
    const-string v1, ", mQueueItemContainerName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mQueueItemContainerName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 951
    const-string v1, ", mIsFromSearch="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mIsFromSearch:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 952
    const-string v1, ", mSearchString="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mSearchString:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 953
    const-string v1, ", mPosition="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mPosition:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 954
    const-string v1, ", mRadioSourceType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mRadioSeedType:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 955
    const-string v1, ", mRadioSourceId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mRadioSeedId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 956
    const-string v1, ", mMainstageReason="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mMainstageReason:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 957
    const-string v1, ", mRadioRemoteId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mRadioRemoteId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 958
    const-string v1, ", mSourceAccount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mSourceAccount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 959
    const-string v1, ", mVideoId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mVideoId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 960
    const-string v1, ", mVideoThumbnailUrl="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mVideoThumbnailUrl:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 961
    const-string v1, ", mPlaylistEditorArtwork="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mPlaylistEditorArtwork:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 962
    const-string v1, ", mRadioHighlightColor="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mRadioHighlightColor:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 963
    const-string v1, ", mSituationId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mSituationId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 964
    const-string v1, ", mHasSubSituations="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mHasSubSituations:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 965
    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 967
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 6
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1091
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mArtUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1092
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mTitle:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1093
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mSubTitle:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1094
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mDescription:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1095
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mReason1:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1097
    iget-wide v4, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mId:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 1098
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mSongStoreId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1099
    iget-wide v4, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mAlbumId:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 1100
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mAlbumName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1102
    iget-wide v4, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mArtistId:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 1103
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mArtistName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1105
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mType:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1107
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mPlaylistName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1108
    iget v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mPlaylistType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1109
    iget-wide v4, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mPlaylistMemberId:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 1111
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mPlaylistShareToken:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1112
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mPlaylistOwnerName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1113
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mProfilePhotoUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1114
    iget v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mPlaylistSharedState:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1115
    iget-boolean v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mIsEmulatedRadio:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1116
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mGenreId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1117
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mParentGenreId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1118
    iget v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mSubgenreCount:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1119
    iget-boolean v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mHasLocal:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1121
    iget-boolean v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mIsNautilus:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1122
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mTrackMetajamId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1123
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mAlbumMetajamId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1124
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mArtistMetajamId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1126
    iget-boolean v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mCanAddtoLibrary:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1127
    iget-boolean v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mCanRemoveFromLibrary:Z

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1128
    iget-boolean v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mCanFollow:Z

    if-eqz v0, :cond_5

    move v0, v1

    :goto_5
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1130
    iget-wide v4, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mDuration:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 1132
    iget-wide v4, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mQueueItemId:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 1133
    iget v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mQueueItemState:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1134
    iget-wide v4, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mQueueItemContainerId:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 1135
    iget v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mQueueItemContainerType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1136
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mQueueItemContainerName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1137
    iget-boolean v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mIsFromSearch:Z

    if-eqz v0, :cond_6

    move v0, v1

    :goto_6
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1138
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mSearchString:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1139
    iget v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mPosition:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1140
    iget v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mRadioSeedType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1141
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mRadioSeedId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1142
    iget v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mMainstageReason:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1143
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mRadioRemoteId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1144
    iget v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mSourceAccount:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1145
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mVideoId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1146
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mVideoThumbnailUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1147
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mPlaylistEditorArtwork:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1148
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mRadioHighlightColor:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1149
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mSituationId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1150
    iget-boolean v0, p0, Lcom/google/android/music/ui/cardlib/model/Document;->mHasSubSituations:Z

    if-eqz v0, :cond_7

    :goto_7
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1151
    return-void

    :cond_0
    move v0, v2

    .line 1115
    goto/16 :goto_0

    :cond_1
    move v0, v2

    .line 1119
    goto/16 :goto_1

    :cond_2
    move v0, v2

    .line 1121
    goto/16 :goto_2

    :cond_3
    move v0, v2

    .line 1126
    goto/16 :goto_3

    :cond_4
    move v0, v2

    .line 1127
    goto/16 :goto_4

    :cond_5
    move v0, v2

    .line 1128
    goto :goto_5

    :cond_6
    move v0, v2

    .line 1137
    goto :goto_6

    :cond_7
    move v1, v2

    .line 1150
    goto :goto_7
.end method
