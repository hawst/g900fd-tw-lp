.class public Lcom/google/android/music/store/ConfigContentProvider;
.super Landroid/content/ContentProvider;
.source "ConfigContentProvider.java"


# static fields
.field private static final LOGV:Z

.field private static final sConfigProjectionMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final sUriMatcher:Landroid/content/UriMatcher;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 31
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->CONTENT_PROVIDER:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/store/ConfigContentProvider;->LOGV:Z

    .line 42
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    sput-object v0, Lcom/google/android/music/store/ConfigContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    .line 44
    sget-object v0, Lcom/google/android/music/store/ConfigContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.ConfigContent"

    const-string v2, "server"

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 45
    sget-object v0, Lcom/google/android/music/store/ConfigContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.ConfigContent"

    const-string v2, "server/*"

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 47
    sget-object v0, Lcom/google/android/music/store/ConfigContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.ConfigContent"

    const-string v2, "app"

    const/4 v3, 0x3

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 48
    sget-object v0, Lcom/google/android/music/store/ConfigContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.ConfigContent"

    const-string v2, "app/*"

    const/4 v3, 0x4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 50
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/music/store/ConfigContentProvider;->sConfigProjectionMap:Ljava/util/HashMap;

    .line 51
    sget-object v0, Lcom/google/android/music/store/ConfigContentProvider;->sConfigProjectionMap:Ljava/util/HashMap;

    const-string v1, "_id"

    const-string v2, "id"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    sget-object v0, Lcom/google/android/music/store/ConfigContentProvider;->sConfigProjectionMap:Ljava/util/HashMap;

    const-string v1, "count(*)"

    invoke-static {v0, v1}, Lcom/google/android/music/store/MusicContentProvider;->addExistsAndCountMapping(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 54
    sget-object v0, Lcom/google/android/music/store/ConfigContentProvider;->sConfigProjectionMap:Ljava/util/HashMap;

    const-string v1, "Name"

    const-string v2, "Name"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    sget-object v0, Lcom/google/android/music/store/ConfigContentProvider;->sConfigProjectionMap:Ljava/util/HashMap;

    const-string v1, "Value"

    const-string v2, "Value"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    sget-object v0, Lcom/google/android/music/store/ConfigContentProvider;->sConfigProjectionMap:Ljava/util/HashMap;

    const-string v1, "Type"

    const-string v2, "Type"

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContentProvider;->addMapping(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    return-void
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 8
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 168
    const/4 v2, 0x0

    .line 169
    .local v2, "deleted":I
    sget-object v5, Lcom/google/android/music/store/ConfigContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v5, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v5

    packed-switch v5, :pswitch_data_0

    .line 194
    new-instance v5, Ljava/lang/IllegalStateException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Unmatched uri: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 171
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/music/store/ConfigContentProvider;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/music/store/ConfigStore;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/ConfigStore;

    move-result-object v0

    .line 172
    .local v0, "configStore":Lcom/google/android/music/store/ConfigStore;
    invoke-virtual {v0}, Lcom/google/android/music/store/ConfigStore;->beginWriteTxn()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 173
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v4, 0x0

    .line 174
    .local v4, "isTxnSuccessful":Z
    invoke-static {v1}, Lcom/google/android/music/store/ConfigItem;->compileInsertStatement(Landroid/database/sqlite/SQLiteDatabase;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v3

    .line 176
    .local v3, "insertStatement":Landroid/database/sqlite/SQLiteStatement;
    :try_start_0
    invoke-static {v1}, Lcom/google/android/music/store/ConfigItem;->deleteAllServerSettings(Landroid/database/sqlite/SQLiteDatabase;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    .line 177
    const/4 v4, 0x1

    .line 179
    invoke-virtual {v0, v1, v4}, Lcom/google/android/music/store/ConfigStore;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    .line 180
    invoke-static {v3}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/sqlite/SQLiteProgram;)V

    .line 182
    return v2

    .line 179
    :catchall_0
    move-exception v5

    invoke-virtual {v0, v1, v4}, Lcom/google/android/music/store/ConfigStore;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    .line 180
    invoke-static {v3}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/sqlite/SQLiteProgram;)V

    throw v5

    .line 186
    .end local v0    # "configStore":Lcom/google/android/music/store/ConfigStore;
    .end local v1    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v3    # "insertStatement":Landroid/database/sqlite/SQLiteStatement;
    .end local v4    # "isTxnSuccessful":Z
    :pswitch_1
    new-instance v5, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v5}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v5

    .line 189
    :pswitch_2
    new-instance v5, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v5}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v5

    .line 192
    :pswitch_3
    new-instance v5, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v5}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v5

    .line 169
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 146
    sget-object v0, Lcom/google/android/music/store/ConfigContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 156
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unmatched uri: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 148
    :pswitch_0
    const-string v0, "vnd.android.cursor.dir/vnd.google.music.config"

    .line 154
    :goto_0
    return-object v0

    .line 150
    :pswitch_1
    const-string v0, "vnd.android.cursor.item/vnd.google.music.config"

    goto :goto_0

    .line 152
    :pswitch_2
    const-string v0, "vnd.android.cursor.dir/vnd.google.music.config"

    goto :goto_0

    .line 154
    :pswitch_3
    const-string v0, "vnd.android.cursor.item/vnd.google.music.config"

    goto :goto_0

    .line 146
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 163
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public onCreate()Z
    .locals 1

    .prologue
    .line 64
    invoke-virtual {p0}, Lcom/google/android/music/store/ConfigContentProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/music/utils/ConfigUtils;->init(Landroid/content/ContentResolver;)V

    .line 65
    const/4 v0, 0x1

    return v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 17
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "ignoreSelection"    # Ljava/lang/String;
    .param p4, "ignoreSelectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 72
    sget-boolean v3, Lcom/google/android/music/store/ConfigContentProvider;->LOGV:Z

    if-eqz v3, :cond_0

    .line 73
    const-string v3, "ConfigContentProvider"

    const-string v6, "query: uri: %s projectionIn=%s selection=%s selectionArgs=%s sortOrder=%s"

    const/4 v7, 0x5

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object p1, v7, v8

    const/4 v8, 0x1

    invoke-static/range {p2 .. p2}, Lcom/google/android/music/utils/DebugUtils;->arrayToString([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x2

    aput-object p3, v7, v8

    const/4 v8, 0x3

    invoke-static/range {p4 .. p4}, Lcom/google/android/music/utils/DebugUtils;->arrayToString([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x4

    aput-object p5, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 78
    :cond_0
    sget-object v15, Lcom/google/android/music/store/ConfigContentProvider;->sConfigProjectionMap:Ljava/util/HashMap;

    .line 79
    .local v15, "projectionMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/store/ConfigContentProvider;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/music/store/ConfigStore;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/ConfigStore;

    move-result-object v16

    .line 80
    .local v16, "store":Lcom/google/android/music/store/ConfigStore;
    new-instance v1, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v1}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 81
    .local v1, "qb":Landroid/database/sqlite/SQLiteQueryBuilder;
    const/4 v4, 0x0

    .line 82
    .local v4, "selection":Ljava/lang/String;
    const/4 v5, 0x0

    .line 83
    .local v5, "selectionArgs":[Ljava/lang/String;
    sget-object v3, Lcom/google/android/music/store/ConfigContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v13

    .line 84
    .local v13, "matchUri":I
    packed-switch v13, :pswitch_data_0

    .line 126
    new-instance v3, Ljava/lang/IllegalStateException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Unmatched uri:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 86
    :pswitch_0
    const-string v3, "CONFIG"

    invoke-virtual {v1, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 87
    const-string v4, "Type=?"

    .line 88
    const/4 v3, 0x1

    new-array v5, v3, [Ljava/lang/String;

    .end local v5    # "selectionArgs":[Ljava/lang/String;
    const/4 v3, 0x0

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v3

    .line 129
    .restart local v5    # "selectionArgs":[Ljava/lang/String;
    :goto_0
    invoke-virtual/range {v16 .. v16}, Lcom/google/android/music/store/ConfigStore;->beginRead()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 130
    .local v2, "db":Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v1, v15}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    .line 132
    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object/from16 v3, p2

    move-object/from16 v8, p5

    :try_start_0
    invoke-static/range {v1 .. v10}, Lcom/google/android/music/utils/PostFroyoUtils$SQLiteDatabaseComp;->query(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/music/utils/PostFroyoUtils$CancellationSignalComp;)Landroid/database/Cursor;

    move-result-object v11

    .line 135
    .local v11, "c":Landroid/database/Cursor;
    if-eqz v11, :cond_1

    .line 136
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/store/ConfigContentProvider;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-interface {v11, v3, v0}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 140
    :cond_1
    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Lcom/google/android/music/store/ConfigStore;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    return-object v11

    .line 95
    .end local v2    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v11    # "c":Landroid/database/Cursor;
    :pswitch_1
    const-string v3, "CONFIG"

    invoke-virtual {v1, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 96
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v3

    const/4 v6, 0x1

    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    .line 97
    .local v12, "encodedName":Ljava/lang/String;
    invoke-static {v12}, Lcom/google/android/music/ui/cardlib/utils/Utils;->urlDecode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 98
    .local v14, "name":Ljava/lang/String;
    const-string v4, "Type=? AND Name=?"

    .line 99
    const/4 v3, 0x2

    new-array v5, v3, [Ljava/lang/String;

    .end local v5    # "selectionArgs":[Ljava/lang/String;
    const/4 v3, 0x0

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v3

    const/4 v3, 0x1

    aput-object v14, v5, v3

    .line 102
    .restart local v5    # "selectionArgs":[Ljava/lang/String;
    goto :goto_0

    .line 106
    .end local v12    # "encodedName":Ljava/lang/String;
    .end local v14    # "name":Ljava/lang/String;
    :pswitch_2
    const-string v3, "CONFIG"

    invoke-virtual {v1, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 107
    const-string v4, "Type=?"

    .line 108
    const/4 v3, 0x1

    new-array v5, v3, [Ljava/lang/String;

    .end local v5    # "selectionArgs":[Ljava/lang/String;
    const/4 v3, 0x0

    const/4 v6, 0x2

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v3

    .line 111
    .restart local v5    # "selectionArgs":[Ljava/lang/String;
    goto :goto_0

    .line 115
    :pswitch_3
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v3

    const/4 v6, 0x1

    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    .line 116
    .restart local v12    # "encodedName":Ljava/lang/String;
    invoke-static {v12}, Lcom/google/android/music/ui/cardlib/utils/Utils;->urlDecode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 117
    .restart local v14    # "name":Ljava/lang/String;
    const-string v3, "CONFIG"

    invoke-virtual {v1, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 118
    const-string v4, "Type=? AND Name=?"

    .line 119
    const/4 v3, 0x2

    new-array v5, v3, [Ljava/lang/String;

    .end local v5    # "selectionArgs":[Ljava/lang/String;
    const/4 v3, 0x0

    const/4 v6, 0x2

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v3

    const/4 v3, 0x1

    aput-object v14, v5, v3

    .line 122
    .restart local v5    # "selectionArgs":[Ljava/lang/String;
    goto/16 :goto_0

    .line 140
    .end local v12    # "encodedName":Ljava/lang/String;
    .end local v14    # "name":Ljava/lang/String;
    .restart local v2    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_0
    move-exception v3

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Lcom/google/android/music/store/ConfigStore;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    throw v3

    .line 84
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 201
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
