.class Lcom/google/android/music/art/ArtResolver2$2;
.super Ljava/lang/Object;
.source "ArtResolver2.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/art/ArtResolver2;->notifyRequestListeners(Lcom/google/android/music/art/ArtRequest2;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/art/ArtResolver2;

.field final synthetic val$request:Lcom/google/android/music/art/ArtRequest2;


# direct methods
.method constructor <init>(Lcom/google/android/music/art/ArtResolver2;Lcom/google/android/music/art/ArtRequest2;)V
    .locals 0

    .prologue
    .line 353
    iput-object p1, p0, Lcom/google/android/music/art/ArtResolver2$2;->this$0:Lcom/google/android/music/art/ArtResolver2;

    iput-object p2, p0, Lcom/google/android/music/art/ArtResolver2$2;->val$request:Lcom/google/android/music/art/ArtRequest2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 357
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/art/ArtResolver2$2;->val$request:Lcom/google/android/music/art/ArtRequest2;

    invoke-virtual {v0}, Lcom/google/android/music/art/ArtRequest2;->isCancelled()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 368
    iget-object v0, p0, Lcom/google/android/music/art/ArtResolver2$2;->val$request:Lcom/google/android/music/art/ArtRequest2;

    invoke-virtual {v0}, Lcom/google/android/music/art/ArtRequest2;->release()I

    .line 370
    :goto_0
    return-void

    .line 358
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/music/art/ArtResolver2$2;->val$request:Lcom/google/android/music/art/ArtRequest2;

    invoke-virtual {v0}, Lcom/google/android/music/art/ArtRequest2;->getListener()Lcom/google/android/music/art/ArtResolver2$RequestListener;

    move-result-object v0

    if-nez v0, :cond_1

    .line 360
    const-string v0, "ArtResolver"

    const-string v1, "request listener is null, this should only happen if the request has been cancelled but it is not cancelled"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 368
    iget-object v0, p0, Lcom/google/android/music/art/ArtResolver2$2;->val$request:Lcom/google/android/music/art/ArtRequest2;

    invoke-virtual {v0}, Lcom/google/android/music/art/ArtRequest2;->release()I

    goto :goto_0

    .line 364
    :cond_1
    :try_start_2
    iget-object v0, p0, Lcom/google/android/music/art/ArtResolver2$2;->val$request:Lcom/google/android/music/art/ArtRequest2;

    invoke-virtual {v0}, Lcom/google/android/music/art/ArtRequest2;->getListener()Lcom/google/android/music/art/ArtResolver2$RequestListener;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/art/ArtResolver2$2;->val$request:Lcom/google/android/music/art/ArtRequest2;

    invoke-interface {v0, v1}, Lcom/google/android/music/art/ArtResolver2$RequestListener;->onArtRequestComplete(Lcom/google/android/music/art/ArtRequest2;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 368
    iget-object v0, p0, Lcom/google/android/music/art/ArtResolver2$2;->val$request:Lcom/google/android/music/art/ArtRequest2;

    invoke-virtual {v0}, Lcom/google/android/music/art/ArtRequest2;->release()I

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/music/art/ArtResolver2$2;->val$request:Lcom/google/android/music/art/ArtRequest2;

    invoke-virtual {v1}, Lcom/google/android/music/art/ArtRequest2;->release()I

    throw v0
.end method
