.class Lcom/google/android/music/dial/DialMediaRouteProvider$1;
.super Landroid/support/v7/media/MediaRouteProvider$RouteController;
.source "DialMediaRouteProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/dial/DialMediaRouteProvider;->onCreateRouteController(Ljava/lang/String;)Landroid/support/v7/media/MediaRouteProvider$RouteController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/dial/DialMediaRouteProvider;

.field final synthetic val$routeId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/music/dial/DialMediaRouteProvider;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 81
    iput-object p1, p0, Lcom/google/android/music/dial/DialMediaRouteProvider$1;->this$0:Lcom/google/android/music/dial/DialMediaRouteProvider;

    iput-object p2, p0, Lcom/google/android/music/dial/DialMediaRouteProvider$1;->val$routeId:Ljava/lang/String;

    invoke-direct {p0}, Landroid/support/v7/media/MediaRouteProvider$RouteController;-><init>()V

    return-void
.end method


# virtual methods
.method public onSetVolume(I)V
    .locals 3
    .param p1, "volume"    # I

    .prologue
    .line 84
    # getter for: Lcom/google/android/music/dial/DialMediaRouteProvider;->LOGV:Z
    invoke-static {}, Lcom/google/android/music/dial/DialMediaRouteProvider;->access$000()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 85
    const-string v0, "DialMediaRouteProvider"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onSetVolume: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    :cond_0
    new-instance v0, Lcom/google/android/music/dial/DialMediaRouteProvider$1$1;

    invoke-direct {v0, p0, p1}, Lcom/google/android/music/dial/DialMediaRouteProvider$1$1;-><init>(Lcom/google/android/music/dial/DialMediaRouteProvider$1;I)V

    iget-object v1, p0, Lcom/google/android/music/dial/DialMediaRouteProvider$1;->this$0:Lcom/google/android/music/dial/DialMediaRouteProvider;

    invoke-virtual {v1}, Lcom/google/android/music/dial/DialMediaRouteProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/utils/MusicUtils;->runOnUiThread(Ljava/lang/Runnable;Landroid/content/Context;)V

    .line 93
    return-void
.end method

.method public onUpdateVolume(I)V
    .locals 3
    .param p1, "direction"    # I

    .prologue
    .line 97
    # getter for: Lcom/google/android/music/dial/DialMediaRouteProvider;->LOGV:Z
    invoke-static {}, Lcom/google/android/music/dial/DialMediaRouteProvider;->access$000()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 98
    const-string v0, "DialMediaRouteProvider"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onUpdateVolume: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    :cond_0
    new-instance v0, Lcom/google/android/music/dial/DialMediaRouteProvider$1$2;

    invoke-direct {v0, p0, p1}, Lcom/google/android/music/dial/DialMediaRouteProvider$1$2;-><init>(Lcom/google/android/music/dial/DialMediaRouteProvider$1;I)V

    iget-object v1, p0, Lcom/google/android/music/dial/DialMediaRouteProvider$1;->this$0:Lcom/google/android/music/dial/DialMediaRouteProvider;

    invoke-virtual {v1}, Lcom/google/android/music/dial/DialMediaRouteProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/utils/MusicUtils;->runOnUiThread(Ljava/lang/Runnable;Landroid/content/Context;)V

    .line 106
    return-void
.end method
