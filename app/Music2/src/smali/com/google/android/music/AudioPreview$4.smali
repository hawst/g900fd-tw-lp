.class Lcom/google/android/music/AudioPreview$4;
.super Landroid/content/BroadcastReceiver;
.source "AudioPreview.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/AudioPreview;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/AudioPreview;


# direct methods
.method constructor <init>(Lcom/google/android/music/AudioPreview;)V
    .locals 0

    .prologue
    .line 402
    iput-object p1, p0, Lcom/google/android/music/AudioPreview$4;->this$0:Lcom/google/android/music/AudioPreview;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 405
    if-eqz p2, :cond_0

    const-string v0, "android.media.AUDIO_BECOMING_NOISY"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/AudioPreview$4;->this$0:Lcom/google/android/music/AudioPreview;

    # getter for: Lcom/google/android/music/AudioPreview;->mPlayer:Lcom/google/android/music/AudioPreview$PreviewPlayer;
    invoke-static {v0}, Lcom/google/android/music/AudioPreview;->access$300(Lcom/google/android/music/AudioPreview;)Lcom/google/android/music/AudioPreview$PreviewPlayer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/AudioPreview$PreviewPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 408
    iget-object v0, p0, Lcom/google/android/music/AudioPreview$4;->this$0:Lcom/google/android/music/AudioPreview;

    # getter for: Lcom/google/android/music/AudioPreview;->mPlayer:Lcom/google/android/music/AudioPreview$PreviewPlayer;
    invoke-static {v0}, Lcom/google/android/music/AudioPreview;->access$300(Lcom/google/android/music/AudioPreview;)Lcom/google/android/music/AudioPreview$PreviewPlayer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/AudioPreview$PreviewPlayer;->pause()V

    .line 409
    iget-object v0, p0, Lcom/google/android/music/AudioPreview$4;->this$0:Lcom/google/android/music/AudioPreview;

    # invokes: Lcom/google/android/music/AudioPreview;->updatePlayPause()V
    invoke-static {v0}, Lcom/google/android/music/AudioPreview;->access$700(Lcom/google/android/music/AudioPreview;)V

    .line 411
    :cond_0
    return-void
.end method
