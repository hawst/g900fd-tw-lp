.class Lcom/google/android/music/browse/MediaBrowserService$1;
.super Ljava/lang/Object;
.source "MediaBrowserService.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/browse/MediaBrowserService;->onCreate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/browse/MediaBrowserService;


# direct methods
.method constructor <init>(Lcom/google/android/music/browse/MediaBrowserService;)V
    .locals 0

    .prologue
    .line 59
    iput-object p1, p0, Lcom/google/android/music/browse/MediaBrowserService$1;->this$0:Lcom/google/android/music/browse/MediaBrowserService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1, "componentName"    # Landroid/content/ComponentName;
    .param p2, "iBinder"    # Landroid/os/IBinder;

    .prologue
    .line 63
    # getter for: Lcom/google/android/music/browse/MediaBrowserService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/music/browse/MediaBrowserService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Bound to music playback service"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "componentName"    # Landroid/content/ComponentName;

    .prologue
    .line 69
    # getter for: Lcom/google/android/music/browse/MediaBrowserService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/music/browse/MediaBrowserService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Failed to bound to music playback service"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    return-void
.end method
