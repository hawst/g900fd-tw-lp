.class public Lcom/google/android/music/ui/SubGenresExploreTabFragment;
.super Lcom/google/android/music/ui/SubFragmentsPagerFragment;
.source "SubGenresExploreTabFragment.java"


# instance fields
.field private mGenreId:Ljava/lang/String;

.field private mGenreName:Ljava/lang/String;

.field private mParentGenreId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/google/android/music/ui/SubFragmentsPagerFragment;-><init>()V

    .line 29
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedState"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x1

    .line 33
    invoke-super {p0, p1}, Lcom/google/android/music/ui/SubFragmentsPagerFragment;->onCreate(Landroid/os/Bundle;)V

    .line 34
    invoke-virtual {p0}, Lcom/google/android/music/ui/SubGenresExploreTabFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    .line 35
    .local v4, "bundle":Landroid/os/Bundle;
    const-string v0, "nautilusId"

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/SubGenresExploreTabFragment;->mGenreId:Ljava/lang/String;

    .line 36
    const-string v0, "name"

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/SubGenresExploreTabFragment;->mGenreName:Ljava/lang/String;

    .line 37
    const-string v0, "parentNautilusId"

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/SubGenresExploreTabFragment;->mParentGenreId:Ljava/lang/String;

    .line 38
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 39
    .local v6, "tabs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/music/ui/FragmentTabInfo;>;"
    new-instance v0, Lcom/google/android/music/ui/FragmentTabInfo;

    const-string v1, "top_charts"

    const v2, 0x7f0b00a6

    const-class v3, Lcom/google/android/music/ui/TopChartsExploreFragment;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/music/ui/FragmentTabInfo;-><init>(Ljava/lang/String;ILjava/lang/Class;Landroid/os/Bundle;Z)V

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 41
    new-instance v0, Lcom/google/android/music/ui/FragmentTabInfo;

    const-string v1, "new_releases"

    const v2, 0x7f0b00a5

    const-class v3, Lcom/google/android/music/ui/NewReleasesExploreFragment;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/music/ui/FragmentTabInfo;-><init>(Ljava/lang/String;ILjava/lang/Class;Landroid/os/Bundle;Z)V

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 43
    const-string v0, "top_charts"

    invoke-virtual {p0, v6, v0}, Lcom/google/android/music/ui/SubGenresExploreTabFragment;->init(Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 44
    invoke-virtual {p0, v5}, Lcom/google/android/music/ui/SubGenresExploreTabFragment;->setHasOptionsMenu(Z)V

    .line 45
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "inflater"    # Landroid/view/MenuInflater;

    .prologue
    .line 49
    invoke-virtual {p0}, Lcom/google/android/music/ui/SubGenresExploreTabFragment;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    .line 50
    .local v0, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->showStartRadioButtonsInActionBar()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/music/ui/SubGenresExploreTabFragment;->getBaseActivity()Lcom/google/android/music/ui/BaseActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/ui/BaseActivity;->isSideDrawerOpen()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/preferences/MusicPreferences;->isNautilusEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 52
    const v1, 0x7f140001

    invoke-virtual {p2, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 54
    :cond_0
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 6
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 58
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 82
    invoke-super {p0, p1}, Lcom/google/android/music/ui/SubFragmentsPagerFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 60
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/music/ui/SubGenresExploreTabFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 61
    .local v2, "context":Landroid/content/Context;
    iget-object v3, p0, Lcom/google/android/music/ui/SubGenresExploreTabFragment;->mGenreId:Ljava/lang/String;

    .line 62
    .local v3, "genreId":Ljava/lang/String;
    iget-object v5, p0, Lcom/google/android/music/ui/SubGenresExploreTabFragment;->mGenreName:Ljava/lang/String;

    .line 63
    .local v5, "genreName":Ljava/lang/String;
    iget-object v4, p0, Lcom/google/android/music/ui/SubGenresExploreTabFragment;->mParentGenreId:Ljava/lang/String;

    .line 64
    .local v4, "parentGenreId":Ljava/lang/String;
    new-instance v0, Lcom/google/android/music/ui/SubGenresExploreTabFragment$1;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/music/ui/SubGenresExploreTabFragment$1;-><init>(Lcom/google/android/music/ui/SubGenresExploreTabFragment;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/google/android/music/utils/MusicUtils;->runAsyncWithCallback(Lcom/google/android/music/utils/async/AsyncRunner;)V

    .line 80
    const/4 v0, 0x1

    goto :goto_0

    .line 58
    nop

    :pswitch_data_0
    .packed-switch 0x7f0e02a1
        :pswitch_0
    .end packed-switch
.end method
