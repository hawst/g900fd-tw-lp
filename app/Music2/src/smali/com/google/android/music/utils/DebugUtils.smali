.class public Lcom/google/android/music/utils/DebugUtils;
.super Ljava/lang/Object;
.source "DebugUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/utils/DebugUtils$MusicTag;
    }
.end annotation


# static fields
.field public static final HTTP_TAG:Ljava/lang/String;

.field public static final IS_DEBUG_BUILD:Z

.field private static final sAutoDebugOff:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/music/utils/DebugUtils$MusicTag;",
            ">;"
        }
    .end annotation
.end field

.field private static final sAutoDebugOn:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/music/utils/DebugUtils$MusicTag;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile sAutoLogAll:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 29
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->HTTP:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-virtual {v0}, Lcom/google/android/music/utils/DebugUtils$MusicTag;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/utils/DebugUtils;->HTTP_TAG:Ljava/lang/String;

    .line 86
    new-instance v0, Lcom/google/common/collect/ImmutableSet$Builder;

    invoke-direct {v0}, Lcom/google/common/collect/ImmutableSet$Builder;-><init>()V

    sget-object v1, Lcom/google/android/music/utils/DebugUtils$MusicTag;->PLAYBACK_SERVICE:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-virtual {v0, v1}, Lcom/google/common/collect/ImmutableSet$Builder;->add(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/android/music/utils/DebugUtils$MusicTag;->DOWNLOAD:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-virtual {v0, v1}, Lcom/google/common/collect/ImmutableSet$Builder;->add(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/android/music/utils/DebugUtils$MusicTag;->CONTENT_PROVIDER:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-virtual {v0, v1}, Lcom/google/common/collect/ImmutableSet$Builder;->add(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/android/music/utils/DebugUtils$MusicTag;->STORE_IMPORTER:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-virtual {v0, v1}, Lcom/google/common/collect/ImmutableSet$Builder;->add(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/android/music/utils/DebugUtils$MusicTag;->SYNC:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-virtual {v0, v1}, Lcom/google/common/collect/ImmutableSet$Builder;->add(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/android/music/utils/DebugUtils$MusicTag;->HTTP:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-virtual {v0, v1}, Lcom/google/common/collect/ImmutableSet$Builder;->add(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/android/music/utils/DebugUtils$MusicTag;->LOG_FILE:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-virtual {v0, v1}, Lcom/google/common/collect/ImmutableSet$Builder;->add(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/android/music/utils/DebugUtils$MusicTag;->MEDIA_LIST:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-virtual {v0, v1}, Lcom/google/common/collect/ImmutableSet$Builder;->add(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/android/music/utils/DebugUtils$MusicTag;->SEARCH:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-virtual {v0, v1}, Lcom/google/common/collect/ImmutableSet$Builder;->add(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/android/music/utils/DebugUtils$MusicTag;->CALLS:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-virtual {v0, v1}, Lcom/google/common/collect/ImmutableSet$Builder;->add(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/android/music/utils/DebugUtils$MusicTag;->CLOUD_CLIENT:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-virtual {v0, v1}, Lcom/google/common/collect/ImmutableSet$Builder;->add(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/android/music/utils/DebugUtils$MusicTag;->LEANBACK:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-virtual {v0, v1}, Lcom/google/common/collect/ImmutableSet$Builder;->add(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/android/music/utils/DebugUtils$MusicTag;->CAST:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-virtual {v0, v1}, Lcom/google/common/collect/ImmutableSet$Builder;->add(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/android/music/utils/DebugUtils$MusicTag;->CAST_REMOTE:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-virtual {v0, v1}, Lcom/google/common/collect/ImmutableSet$Builder;->add(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/android/music/utils/DebugUtils$MusicTag;->CACHE:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-virtual {v0, v1}, Lcom/google/common/collect/ImmutableSet$Builder;->add(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/android/music/utils/DebugUtils$MusicTag;->ART_RESOLVER:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-virtual {v0, v1}, Lcom/google/common/collect/ImmutableSet$Builder;->add(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/android/music/utils/DebugUtils$MusicTag;->EVENT_LOG:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-virtual {v0, v1}, Lcom/google/common/collect/ImmutableSet$Builder;->add(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/android/music/utils/DebugUtils$MusicTag;->DIAL_MRP:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-virtual {v0, v1}, Lcom/google/common/collect/ImmutableSet$Builder;->add(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/android/music/utils/DebugUtils$MusicTag;->DIAL_MRP_DISCOVERY:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-virtual {v0, v1}, Lcom/google/common/collect/ImmutableSet$Builder;->add(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/android/music/utils/DebugUtils$MusicTag;->DIAL_MRP_MANAGEMENT:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-virtual {v0, v1}, Lcom/google/common/collect/ImmutableSet$Builder;->add(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/android/music/utils/DebugUtils$MusicTag;->DIAL_MRP_PLAYBACK:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-virtual {v0, v1}, Lcom/google/common/collect/ImmutableSet$Builder;->add(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/android/music/utils/DebugUtils$MusicTag;->DIAL_MRP_COMMUNICATION:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-virtual {v0, v1}, Lcom/google/common/collect/ImmutableSet$Builder;->add(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/collect/ImmutableSet$Builder;->build()Lcom/google/common/collect/ImmutableSet;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/utils/DebugUtils;->sAutoDebugOff:Ljava/util/Set;

    .line 115
    new-instance v0, Lcom/google/common/collect/ImmutableSet$Builder;

    invoke-direct {v0}, Lcom/google/common/collect/ImmutableSet$Builder;-><init>()V

    invoke-virtual {v0}, Lcom/google/common/collect/ImmutableSet$Builder;->build()Lcom/google/common/collect/ImmutableSet;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/utils/DebugUtils;->sAutoDebugOn:Ljava/util/Set;

    .line 118
    sget-object v0, Landroid/os/Build;->TYPE:Ljava/lang/String;

    const-string v1, "debug"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Landroid/os/Build;->TYPE:Ljava/lang/String;

    const-string v1, "eng"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/music/utils/DebugUtils;->IS_DEBUG_BUILD:Z

    .line 124
    sget-boolean v0, Lcom/google/android/music/utils/DebugUtils;->IS_DEBUG_BUILD:Z

    sput-boolean v0, Lcom/google/android/music/utils/DebugUtils;->sAutoLogAll:Z

    return-void

    .line 118
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static arrayToString([Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "input"    # [Ljava/lang/String;

    .prologue
    .line 199
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 201
    .local v1, "strBuilder":Ljava/lang/StringBuilder;
    if-eqz p0, :cond_1

    .line 202
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p0

    if-ge v0, v2, :cond_1

    .line 203
    if-lez v0, :cond_0

    .line 204
    const-string v2, "#"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 206
    :cond_0
    aget-object v2, p0, v0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 202
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 209
    .end local v0    # "i":I
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public static final bundleToString(Landroid/os/Bundle;)Ljava/lang/String;
    .locals 1
    .param p0, "b"    # Landroid/os/Bundle;

    .prologue
    .line 219
    if-nez p0, :cond_0

    const-string v0, "NULL"

    .line 221
    :goto_0
    return-object v0

    .line 220
    :cond_0
    invoke-static {p0}, Lcom/google/android/music/utils/DebugUtils;->unparcel(Landroid/os/Bundle;)V

    .line 221
    invoke-virtual {p0}, Landroid/os/Bundle;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static isAutoLogAll()Z
    .locals 1

    .prologue
    .line 131
    sget-boolean v0, Lcom/google/android/music/utils/DebugUtils;->sAutoLogAll:Z

    return v0
.end method

.method public static final isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z
    .locals 2
    .param p0, "tag"    # Lcom/google/android/music/utils/DebugUtils$MusicTag;

    .prologue
    .line 135
    sget-boolean v0, Lcom/google/android/music/utils/DebugUtils;->sAutoLogAll:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/music/utils/DebugUtils;->sAutoDebugOff:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/music/utils/DebugUtils$MusicTag;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/android/music/utils/DebugUtils;->sAutoDebugOff:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/google/android/music/utils/DebugUtils;->sAutoDebugOn:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static listToString(Ljava/util/List;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/download/ContentIdentifier;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 239
    .local p0, "input":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/download/ContentIdentifier;>;"
    if-nez p0, :cond_0

    .line 240
    const-string v0, "null"

    .line 242
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ","

    invoke-static {v0, p0}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static final maybeLogMethodName(Lcom/google/android/music/utils/DebugUtils$MusicTag;Ljava/lang/Object;)V
    .locals 5
    .param p0, "tag"    # Lcom/google/android/music/utils/DebugUtils$MusicTag;
    .param p1, "instance"    # Ljava/lang/Object;

    .prologue
    .line 145
    invoke-static {p0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 146
    new-instance v1, Ljava/lang/Throwable;

    invoke-direct {v1}, Ljava/lang/Throwable;-><init>()V

    .line 147
    .local v1, "t":Ljava/lang/Throwable;
    invoke-virtual {v1}, Ljava/lang/Throwable;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v2

    const/4 v3, 0x1

    aget-object v0, v2, v3

    .line 148
    .local v0, "element":Ljava/lang/StackTraceElement;
    invoke-virtual {p0}, Lcom/google/android/music/utils/DebugUtils$MusicTag;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "#"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    .end local v0    # "element":Ljava/lang/StackTraceElement;
    .end local v1    # "t":Ljava/lang/Throwable;
    :cond_0
    return-void
.end method

.method public static setAutoLogAll(Z)V
    .locals 0
    .param p0, "value"    # Z

    .prologue
    .line 127
    sput-boolean p0, Lcom/google/android/music/utils/DebugUtils;->sAutoLogAll:Z

    .line 128
    return-void
.end method

.method private static unparcel(Landroid/os/Bundle;)V
    .locals 4
    .param p0, "b"    # Landroid/os/Bundle;

    .prologue
    .line 225
    invoke-virtual {p0}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 226
    .local v1, "key":Ljava/lang/String;
    invoke-virtual {p0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    .line 227
    .local v2, "value":Ljava/lang/Object;
    instance-of v3, v2, Landroid/os/Bundle;

    if-eqz v3, :cond_0

    .line 228
    check-cast v2, Landroid/os/Bundle;

    .end local v2    # "value":Ljava/lang/Object;
    invoke-static {v2}, Lcom/google/android/music/utils/DebugUtils;->unparcel(Landroid/os/Bundle;)V

    goto :goto_0

    .line 231
    .end local v1    # "key":Ljava/lang/String;
    :cond_1
    return-void
.end method
