.class Lcom/google/android/music/ui/ModifyPlaylistFragment$4;
.super Ljava/lang/Object;
.source "ModifyPlaylistFragment.java"

# interfaces
.implements Lcom/google/android/music/utils/async/AsyncRunner;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/ModifyPlaylistFragment;->handleEdit()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private volatile mResult:I

.field final synthetic this$0:Lcom/google/android/music/ui/ModifyPlaylistFragment;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$description:Ljava/lang/String;

.field final synthetic val$name:Ljava/lang/String;

.field final synthetic val$shareStateArg:I


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/ModifyPlaylistFragment;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 356
    iput-object p1, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment$4;->this$0:Lcom/google/android/music/ui/ModifyPlaylistFragment;

    iput-object p2, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment$4;->val$context:Landroid/content/Context;

    iput-object p3, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment$4;->val$name:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment$4;->val$description:Ljava/lang/String;

    iput p5, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment$4;->val$shareStateArg:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public backgroundTask()V
    .locals 7

    .prologue
    .line 361
    iget-object v0, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment$4;->val$context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment$4;->this$0:Lcom/google/android/music/ui/ModifyPlaylistFragment;

    # getter for: Lcom/google/android/music/ui/ModifyPlaylistFragment;->mPlaylistSongList:Lcom/google/android/music/medialist/PlaylistSongList;
    invoke-static {v0}, Lcom/google/android/music/ui/ModifyPlaylistFragment;->access$000(Lcom/google/android/music/ui/ModifyPlaylistFragment;)Lcom/google/android/music/medialist/PlaylistSongList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/medialist/PlaylistSongList;->getId()J

    move-result-wide v2

    iget-object v4, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment$4;->val$name:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment$4;->val$description:Ljava/lang/String;

    iget v6, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment$4;->val$shareStateArg:I

    invoke-static/range {v1 .. v6}, Lcom/google/android/music/store/MusicContent$Playlists;->updatePlaylist(Landroid/content/ContentResolver;JLjava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment$4;->mResult:I

    .line 365
    return-void
.end method

.method public taskCompleted()V
    .locals 3

    .prologue
    .line 369
    iget v0, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment$4;->mResult:I

    if-nez v0, :cond_0

    .line 370
    iget-object v0, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment$4;->val$context:Landroid/content/Context;

    const v1, 0x7f0b034e

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 373
    :cond_0
    return-void
.end method
