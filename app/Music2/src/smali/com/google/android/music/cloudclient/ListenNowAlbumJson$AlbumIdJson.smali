.class public Lcom/google/android/music/cloudclient/ListenNowAlbumJson$AlbumIdJson;
.super Lcom/google/api/client/json/GenericJson;
.source "ListenNowAlbumJson.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/cloudclient/ListenNowAlbumJson;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AlbumIdJson"
.end annotation


# instance fields
.field public mArtist:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "artist"
    .end annotation
.end field

.field public mMetajamId:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "metajamCompactKey"
    .end annotation
.end field

.field public mTitle:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "title"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    return-void
.end method
