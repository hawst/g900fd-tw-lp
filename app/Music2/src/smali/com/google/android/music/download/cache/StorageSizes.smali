.class public Lcom/google/android/music/download/cache/StorageSizes;
.super Ljava/lang/Object;
.source "StorageSizes.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/music/download/cache/StorageSizes;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mFreeBytes:J

.field private final mMusicBytes:J

.field private final mNotDownloadedBytes:J

.field private final mTotalBytes:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 85
    new-instance v0, Lcom/google/android/music/download/cache/StorageSizes$1;

    invoke-direct {v0}, Lcom/google/android/music/download/cache/StorageSizes$1;-><init>()V

    sput-object v0, Lcom/google/android/music/download/cache/StorageSizes;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(JJJJ)V
    .locals 1
    .param p1, "totalBytes"    # J
    .param p3, "musicBytes"    # J
    .param p5, "freeBytes"    # J
    .param p7, "notDownloadedBytes"    # J

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-wide p1, p0, Lcom/google/android/music/download/cache/StorageSizes;->mTotalBytes:J

    .line 25
    iput-wide p3, p0, Lcom/google/android/music/download/cache/StorageSizes;->mMusicBytes:J

    .line 26
    iput-wide p5, p0, Lcom/google/android/music/download/cache/StorageSizes;->mFreeBytes:J

    .line 27
    iput-wide p7, p0, Lcom/google/android/music/download/cache/StorageSizes;->mNotDownloadedBytes:J

    .line 28
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "p"    # Landroid/os/Parcel;

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/music/download/cache/StorageSizes;->mTotalBytes:J

    .line 32
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/music/download/cache/StorageSizes;->mMusicBytes:J

    .line 33
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/music/download/cache/StorageSizes;->mFreeBytes:J

    .line 34
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/music/download/cache/StorageSizes;->mNotDownloadedBytes:J

    .line 35
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/google/android/music/download/cache/StorageSizes$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/google/android/music/download/cache/StorageSizes$1;

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/google/android/music/download/cache/StorageSizes;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 99
    const/4 v0, 0x0

    return v0
.end method

.method public getFreeBytes()J
    .locals 2

    .prologue
    .line 63
    iget-wide v0, p0, Lcom/google/android/music/download/cache/StorageSizes;->mFreeBytes:J

    return-wide v0
.end method

.method public getMusicBytes()J
    .locals 2

    .prologue
    .line 56
    iget-wide v0, p0, Lcom/google/android/music/download/cache/StorageSizes;->mMusicBytes:J

    return-wide v0
.end method

.method public getNotDownloadedBytes()J
    .locals 2

    .prologue
    .line 70
    iget-wide v0, p0, Lcom/google/android/music/download/cache/StorageSizes;->mNotDownloadedBytes:J

    return-wide v0
.end method

.method public getTotalBytes()J
    .locals 2

    .prologue
    .line 49
    iget-wide v0, p0, Lcom/google/android/music/download/cache/StorageSizes;->mTotalBytes:J

    return-wide v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 75
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 76
    .local v0, "builder":Ljava/lang/StringBuilder;
    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 77
    const-string v1, "mTotalBytes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/music/download/cache/StorageSizes;->mTotalBytes:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 78
    const-string v1, " mMusicBytes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/music/download/cache/StorageSizes;->mMusicBytes:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 79
    const-string v1, " mFreeBytes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/music/download/cache/StorageSizes;->mFreeBytes:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 80
    const-string v1, " mNotDownloadedBytes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/music/download/cache/StorageSizes;->mNotDownloadedBytes:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 81
    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 82
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 39
    iget-wide v0, p0, Lcom/google/android/music/download/cache/StorageSizes;->mTotalBytes:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 40
    iget-wide v0, p0, Lcom/google/android/music/download/cache/StorageSizes;->mMusicBytes:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 41
    iget-wide v0, p0, Lcom/google/android/music/download/cache/StorageSizes;->mFreeBytes:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 42
    iget-wide v0, p0, Lcom/google/android/music/download/cache/StorageSizes;->mNotDownloadedBytes:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 43
    return-void
.end method
