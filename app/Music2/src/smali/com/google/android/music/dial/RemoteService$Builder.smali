.class public Lcom/google/android/music/dial/RemoteService$Builder;
.super Ljava/lang/Object;
.source "RemoteService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/dial/RemoteService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private mControlUrl:Ljava/lang/String;

.field private mEventSubUrl:Ljava/lang/String;

.field private mScpdUrl:Ljava/lang/String;

.field private mServiceId:Ljava/lang/String;

.field private mServiceType:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/music/dial/RemoteService$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/dial/RemoteService$Builder;

    .prologue
    .line 13
    iget-object v0, p0, Lcom/google/android/music/dial/RemoteService$Builder;->mServiceType:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/music/dial/RemoteService$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/dial/RemoteService$Builder;

    .prologue
    .line 13
    iget-object v0, p0, Lcom/google/android/music/dial/RemoteService$Builder;->mServiceId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/music/dial/RemoteService$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/dial/RemoteService$Builder;

    .prologue
    .line 13
    iget-object v0, p0, Lcom/google/android/music/dial/RemoteService$Builder;->mControlUrl:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/music/dial/RemoteService$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/dial/RemoteService$Builder;

    .prologue
    .line 13
    iget-object v0, p0, Lcom/google/android/music/dial/RemoteService$Builder;->mEventSubUrl:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/music/dial/RemoteService$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/dial/RemoteService$Builder;

    .prologue
    .line 13
    iget-object v0, p0, Lcom/google/android/music/dial/RemoteService$Builder;->mScpdUrl:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public build()Lcom/google/android/music/dial/RemoteService;
    .locals 2

    .prologue
    .line 22
    new-instance v0, Lcom/google/android/music/dial/RemoteService;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/music/dial/RemoteService;-><init>(Lcom/google/android/music/dial/RemoteService$Builder;Lcom/google/android/music/dial/RemoteService$1;)V

    return-object v0
.end method

.method public setControlUrl(Ljava/lang/String;)Lcom/google/android/music/dial/RemoteService$Builder;
    .locals 0
    .param p1, "controlUrl"    # Ljava/lang/String;

    .prologue
    .line 36
    iput-object p1, p0, Lcom/google/android/music/dial/RemoteService$Builder;->mControlUrl:Ljava/lang/String;

    .line 37
    return-object p0
.end method

.method public setEventSubUrl(Ljava/lang/String;)Lcom/google/android/music/dial/RemoteService$Builder;
    .locals 0
    .param p1, "eventSubUrl"    # Ljava/lang/String;

    .prologue
    .line 41
    iput-object p1, p0, Lcom/google/android/music/dial/RemoteService$Builder;->mEventSubUrl:Ljava/lang/String;

    .line 42
    return-object p0
.end method

.method public setScpdUrl(Ljava/lang/String;)Lcom/google/android/music/dial/RemoteService$Builder;
    .locals 0
    .param p1, "scpdUrl"    # Ljava/lang/String;

    .prologue
    .line 46
    iput-object p1, p0, Lcom/google/android/music/dial/RemoteService$Builder;->mScpdUrl:Ljava/lang/String;

    .line 47
    return-object p0
.end method

.method public setServiceId(Ljava/lang/String;)Lcom/google/android/music/dial/RemoteService$Builder;
    .locals 0
    .param p1, "serviceId"    # Ljava/lang/String;

    .prologue
    .line 31
    iput-object p1, p0, Lcom/google/android/music/dial/RemoteService$Builder;->mServiceId:Ljava/lang/String;

    .line 32
    return-object p0
.end method

.method public setServiceType(Ljava/lang/String;)Lcom/google/android/music/dial/RemoteService$Builder;
    .locals 0
    .param p1, "serviceType"    # Ljava/lang/String;

    .prologue
    .line 26
    iput-object p1, p0, Lcom/google/android/music/dial/RemoteService$Builder;->mServiceType:Ljava/lang/String;

    .line 27
    return-object p0
.end method
