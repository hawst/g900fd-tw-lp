.class Lcom/google/android/music/utils/RouteChecker$SHA1;
.super Ljava/lang/Object;
.source "RouteChecker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/utils/RouteChecker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SHA1"
.end annotation


# instance fields
.field private mDigest:[B


# direct methods
.method public constructor <init>([B)V
    .locals 2
    .param p1, "digest"    # [B

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    if-eqz p1, :cond_0

    array-length v0, p1

    const/16 v1, 0x14

    if-eq v0, v1, :cond_1

    .line 47
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Expected SHA1 digest"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 49
    :cond_1
    iput-object p1, p0, Lcom/google/android/music/utils/RouteChecker$SHA1;->mDigest:[B

    .line 50
    return-void
.end method

.method public static createFromHexString(Ljava/lang/String;)Lcom/google/android/music/utils/RouteChecker$SHA1;
    .locals 8
    .param p0, "hex"    # Ljava/lang/String;

    .prologue
    const/16 v7, 0x14

    .line 53
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    const/16 v6, 0x28

    if-eq v5, v6, :cond_1

    .line 54
    :cond_0
    new-instance v5, Ljava/lang/NumberFormatException;

    const-string v6, "Expected SHA1 hex string"

    invoke-direct {v5, v6}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 56
    :cond_1
    new-array v0, v7, [B

    .line 57
    .local v0, "digest":[B
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v7, :cond_2

    .line 58
    mul-int/lit8 v1, v3, 0x2

    .line 59
    .local v1, "hexIndex":I
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-static {v5}, Lcom/google/android/music/utils/RouteChecker$SHA1;->parseHexDigit(C)B

    move-result v2

    .line 60
    .local v2, "high":B
    add-int/lit8 v5, v1, 0x1

    invoke-virtual {p0, v5}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-static {v5}, Lcom/google/android/music/utils/RouteChecker$SHA1;->parseHexDigit(C)B

    move-result v4

    .line 61
    .local v4, "low":B
    shl-int/lit8 v5, v2, 0x4

    or-int/2addr v5, v4

    int-to-byte v5, v5

    aput-byte v5, v0, v3

    .line 57
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 63
    .end local v1    # "hexIndex":I
    .end local v2    # "high":B
    .end local v4    # "low":B
    :cond_2
    new-instance v5, Lcom/google/android/music/utils/RouteChecker$SHA1;

    invoke-direct {v5, v0}, Lcom/google/android/music/utils/RouteChecker$SHA1;-><init>([B)V

    return-object v5
.end method

.method private static parseHexDigit(C)B
    .locals 3
    .param p0, "c"    # C

    .prologue
    .line 67
    const/16 v0, 0x30

    if-lt p0, v0, :cond_0

    const/16 v0, 0x39

    if-gt p0, v0, :cond_0

    .line 68
    add-int/lit8 v0, p0, -0x30

    int-to-byte v0, v0

    .line 72
    :goto_0
    return v0

    .line 69
    :cond_0
    const/16 v0, 0x41

    if-lt p0, v0, :cond_1

    const/16 v0, 0x46

    if-gt p0, v0, :cond_1

    .line 70
    add-int/lit8 v0, p0, -0x41

    add-int/lit8 v0, v0, 0xa

    int-to-byte v0, v0

    goto :goto_0

    .line 71
    :cond_1
    const/16 v0, 0x61

    if-lt p0, v0, :cond_2

    const/16 v0, 0x66

    if-gt p0, v0, :cond_2

    .line 72
    add-int/lit8 v0, p0, -0x61

    add-int/lit8 v0, v0, 0xa

    int-to-byte v0, v0

    goto :goto_0

    .line 74
    :cond_2
    new-instance v0, Ljava/lang/NumberFormatException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected hex character got "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static toHexDigit(B)C
    .locals 3
    .param p0, "b"    # B

    .prologue
    .line 91
    if-ltz p0, :cond_0

    const/16 v0, 0x9

    if-gt p0, v0, :cond_0

    .line 92
    add-int/lit8 v0, p0, 0x30

    int-to-char v0, v0

    .line 94
    :goto_0
    return v0

    .line 93
    :cond_0
    const/16 v0, 0xf

    if-gt p0, v0, :cond_1

    .line 94
    add-int/lit8 v0, p0, -0xa

    add-int/lit8 v0, v0, 0x41

    int-to-char v0, v0

    goto :goto_0

    .line 96
    :cond_1
    new-instance v0, Ljava/lang/NumberFormatException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "byte out of range 0..15: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    .line 102
    instance-of v0, p1, Lcom/google/android/music/utils/RouteChecker$SHA1;

    if-nez v0, :cond_0

    .line 103
    const/4 v0, 0x0

    .line 105
    .end local p1    # "other":Ljava/lang/Object;
    :goto_0
    return v0

    .restart local p1    # "other":Ljava/lang/Object;
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/utils/RouteChecker$SHA1;->mDigest:[B

    check-cast p1, Lcom/google/android/music/utils/RouteChecker$SHA1;

    .end local p1    # "other":Ljava/lang/Object;
    iget-object v1, p1, Lcom/google/android/music/utils/RouteChecker$SHA1;->mDigest:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/google/android/music/utils/RouteChecker$SHA1;->mDigest:[B

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([B)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 8

    .prologue
    .line 80
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 81
    .local v6, "sb":Ljava/lang/StringBuilder;
    iget-object v0, p0, Lcom/google/android/music/utils/RouteChecker$SHA1;->mDigest:[B

    .local v0, "arr$":[B
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_0

    aget-byte v1, v0, v3

    .line 82
    .local v1, "b":B
    shr-int/lit8 v7, v1, 0x4

    and-int/lit8 v7, v7, 0xf

    int-to-byte v2, v7

    .line 83
    .local v2, "high":B
    and-int/lit8 v7, v1, 0xf

    int-to-byte v5, v7

    .line 84
    .local v5, "low":B
    invoke-static {v2}, Lcom/google/android/music/utils/RouteChecker$SHA1;->toHexDigit(B)C

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 85
    invoke-static {v5}, Lcom/google/android/music/utils/RouteChecker$SHA1;->toHexDigit(B)C

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 81
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 87
    .end local v1    # "b":B
    .end local v2    # "high":B
    .end local v5    # "low":B
    :cond_0
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    return-object v7
.end method
