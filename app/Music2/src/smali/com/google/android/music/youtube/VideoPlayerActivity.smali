.class public Lcom/google/android/music/youtube/VideoPlayerActivity;
.super Lcom/google/android/youtube/player/YouTubeBaseActivity;
.source "VideoPlayerActivity.java"

# interfaces
.implements Lcom/google/android/youtube/player/YouTubePlayer$OnInitializedListener;
.implements Lcom/google/android/youtube/player/YouTubePlayer$PlayerStateChangeListener;


# instance fields
.field private mVideoId:Ljava/lang/String;

.field private mYouTubePlayerView:Lcom/google/android/youtube/player/YouTubePlayerView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/google/android/youtube/player/YouTubeBaseActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 108
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 110
    iget-object v0, p0, Lcom/google/android/music/youtube/VideoPlayerActivity;->mYouTubePlayerView:Lcom/google/android/youtube/player/YouTubePlayerView;

    const-string v1, "AIzaSyCQ8d_gKWCBARS-s47D9rCG9QtnNxxSz-I"

    invoke-virtual {v0, v1, p0}, Lcom/google/android/youtube/player/YouTubePlayerView;->initialize(Ljava/lang/String;Lcom/google/android/youtube/player/YouTubePlayer$OnInitializedListener;)V

    .line 112
    :cond_0
    return-void
.end method

.method public onAdStarted()V
    .locals 0

    .prologue
    .line 124
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 39
    invoke-super {p0, p1}, Lcom/google/android/youtube/player/YouTubeBaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 40
    const v1, 0x7f040107

    invoke-virtual {p0, v1}, Lcom/google/android/music/youtube/VideoPlayerActivity;->setContentView(I)V

    .line 41
    const v1, 0x7f0e028e

    invoke-virtual {p0, v1}, Lcom/google/android/music/youtube/VideoPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/player/YouTubePlayerView;

    iput-object v1, p0, Lcom/google/android/music/youtube/VideoPlayerActivity;->mYouTubePlayerView:Lcom/google/android/youtube/player/YouTubePlayerView;

    .line 42
    iget-object v1, p0, Lcom/google/android/music/youtube/VideoPlayerActivity;->mYouTubePlayerView:Lcom/google/android/youtube/player/YouTubePlayerView;

    const-string v2, "AIzaSyCQ8d_gKWCBARS-s47D9rCG9QtnNxxSz-I"

    invoke-virtual {v1, v2, p0}, Lcom/google/android/youtube/player/YouTubePlayerView;->initialize(Ljava/lang/String;Lcom/google/android/youtube/player/YouTubePlayer$OnInitializedListener;)V

    .line 43
    invoke-virtual {p0}, Lcom/google/android/music/youtube/VideoPlayerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 44
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "videoId"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/youtube/VideoPlayerActivity;->mVideoId:Ljava/lang/String;

    .line 45
    iget-object v1, p0, Lcom/google/android/music/youtube/VideoPlayerActivity;->mVideoId:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 46
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Invalid video ID in intent."

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 48
    :cond_0
    return-void
.end method

.method public onError(Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;)V
    .locals 0
    .param p1, "arg0"    # Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;

    .prologue
    .line 129
    return-void
.end method

.method public onInitializationFailure(Lcom/google/android/youtube/player/YouTubePlayer$Provider;Lcom/google/android/youtube/player/YouTubeInitializationResult;)V
    .locals 6
    .param p1, "provider"    # Lcom/google/android/youtube/player/YouTubePlayer$Provider;
    .param p2, "errorReason"    # Lcom/google/android/youtube/player/YouTubeInitializationResult;

    .prologue
    const/4 v5, 0x1

    .line 96
    invoke-virtual {p2}, Lcom/google/android/youtube/player/YouTubeInitializationResult;->isUserRecoverableError()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 97
    invoke-virtual {p2, p0, v5}, Lcom/google/android/youtube/player/YouTubeInitializationResult;->getErrorDialog(Landroid/app/Activity;I)Landroid/app/Dialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    .line 104
    :goto_0
    return-void

    .line 99
    :cond_0
    const v1, 0x7f0b004d

    invoke-virtual {p0, v1}, Lcom/google/android/music/youtube/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p2}, Lcom/google/android/youtube/player/YouTubeInitializationResult;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 102
    .local v0, "errorMsg":Ljava/lang/String;
    invoke-static {p0, v0, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public onInitializationSuccess(Lcom/google/android/youtube/player/YouTubePlayer$Provider;Lcom/google/android/youtube/player/YouTubePlayer;Z)V
    .locals 4
    .param p1, "provider"    # Lcom/google/android/youtube/player/YouTubePlayer$Provider;
    .param p2, "player"    # Lcom/google/android/youtube/player/YouTubePlayer;
    .param p3, "wasRestored"    # Z

    .prologue
    .line 68
    const/16 v2, 0x8

    invoke-interface {p2, v2}, Lcom/google/android/youtube/player/YouTubePlayer;->addFullscreenControlFlag(I)V

    .line 71
    const/4 v2, 0x0

    invoke-interface {p2, v2}, Lcom/google/android/youtube/player/YouTubePlayer;->setShowFullscreenButton(Z)V

    .line 73
    invoke-interface {p2, p0}, Lcom/google/android/youtube/player/YouTubePlayer;->setPlayerStateChangeListener(Lcom/google/android/youtube/player/YouTubePlayer$PlayerStateChangeListener;)V

    .line 76
    sget-object v1, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    .line 77
    .local v1, "musicPlaybackService":Lcom/google/android/music/playback/IMusicPlaybackService;
    if-eqz v1, :cond_1

    .line 79
    :try_start_0
    invoke-interface {v1}, Lcom/google/android/music/playback/IMusicPlaybackService;->pause()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 88
    :goto_0
    if-nez p3, :cond_0

    .line 89
    iget-object v2, p0, Lcom/google/android/music/youtube/VideoPlayerActivity;->mVideoId:Ljava/lang/String;

    invoke-interface {p2, v2}, Lcom/google/android/youtube/player/YouTubePlayer;->loadVideo(Ljava/lang/String;)V

    .line 91
    :cond_0
    return-void

    .line 80
    :catch_0
    move-exception v0

    .line 81
    .local v0, "e":Landroid/os/RemoteException;
    const-string v2, "VideoPlayerActivity"

    const-string v3, "Failed to pause the service instance"

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 84
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_1
    const-string v2, "VideoPlayerActivity"

    const-string v3, "Failed to get service instance"

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onLoaded(Ljava/lang/String;)V
    .locals 0
    .param p1, "arg0"    # Ljava/lang/String;

    .prologue
    .line 134
    return-void
.end method

.method public onLoading()V
    .locals 0

    .prologue
    .line 139
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 58
    invoke-super {p0}, Lcom/google/android/youtube/player/YouTubeBaseActivity;->onPause()V

    .line 59
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/ui/UIStateManager;->onPause()V

    .line 60
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 52
    invoke-super {p0}, Lcom/google/android/youtube/player/YouTubeBaseActivity;->onResume()V

    .line 53
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/ui/UIStateManager;->onResume()V

    .line 54
    return-void
.end method

.method public onVideoEnded()V
    .locals 0

    .prologue
    .line 118
    invoke-virtual {p0}, Lcom/google/android/music/youtube/VideoPlayerActivity;->finish()V

    .line 119
    return-void
.end method

.method public onVideoStarted()V
    .locals 0

    .prologue
    .line 144
    return-void
.end method
