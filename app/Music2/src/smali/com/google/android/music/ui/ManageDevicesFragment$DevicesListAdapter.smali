.class Lcom/google/android/music/ui/ManageDevicesFragment$DevicesListAdapter;
.super Landroid/widget/BaseAdapter;
.source "ManageDevicesFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/ManageDevicesFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DevicesListAdapter"
.end annotation


# instance fields
.field private final DEVICE_ROW:I

.field private final GROUP_ROW:I

.field final synthetic this$0:Lcom/google/android/music/ui/ManageDevicesFragment;


# direct methods
.method private constructor <init>(Lcom/google/android/music/ui/ManageDevicesFragment;)V
    .locals 1

    .prologue
    .line 215
    iput-object p1, p0, Lcom/google/android/music/ui/ManageDevicesFragment$DevicesListAdapter;->this$0:Lcom/google/android/music/ui/ManageDevicesFragment;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 216
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/music/ui/ManageDevicesFragment$DevicesListAdapter;->GROUP_ROW:I

    .line 217
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/music/ui/ManageDevicesFragment$DevicesListAdapter;->DEVICE_ROW:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/music/ui/ManageDevicesFragment;Lcom/google/android/music/ui/ManageDevicesFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/music/ui/ManageDevicesFragment;
    .param p2, "x1"    # Lcom/google/android/music/ui/ManageDevicesFragment$1;

    .prologue
    .line 215
    invoke-direct {p0, p1}, Lcom/google/android/music/ui/ManageDevicesFragment$DevicesListAdapter;-><init>(Lcom/google/android/music/ui/ManageDevicesFragment;)V

    return-void
.end method

.method private newViewForGroup(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1, "position"    # I
    .param p2, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 325
    iget-object v2, p0, Lcom/google/android/music/ui/ManageDevicesFragment$DevicesListAdapter;->this$0:Lcom/google/android/music/ui/ManageDevicesFragment;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/android/music/ui/ManageDevicesFragment;->getLayoutInflater(Landroid/os/Bundle;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 326
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v2, 0x7f040069

    const/4 v3, 0x0

    invoke-virtual {v0, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 328
    .local v1, "vg":Landroid/widget/TextView;
    invoke-virtual {p0, p1}, Lcom/google/android/music/ui/ManageDevicesFragment$DevicesListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 329
    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/music/utils/TypefaceUtil;->setTypeface(Landroid/widget/TextView;I)V

    .line 330
    return-object v1
.end method

.method private newViewForItem(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v5, 0x0

    .line 312
    iget-object v3, p0, Lcom/google/android/music/ui/ManageDevicesFragment$DevicesListAdapter;->this$0:Lcom/google/android/music/ui/ManageDevicesFragment;

    invoke-virtual {v3, v5}, Lcom/google/android/music/ui/ManageDevicesFragment;->getLayoutInflater(Landroid/os/Bundle;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 313
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v3, 0x7f04006a

    const/4 v4, 0x0

    invoke-virtual {v0, v3, p1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 315
    .local v1, "vg":Landroid/view/ViewGroup;
    new-instance v2, Lcom/google/android/music/ui/ManageDevicesFragment$DeviceInfoViewHolder;

    invoke-direct {v2, v5}, Lcom/google/android/music/ui/ManageDevicesFragment$DeviceInfoViewHolder;-><init>(Lcom/google/android/music/ui/ManageDevicesFragment$1;)V

    .line 316
    .local v2, "vh":Lcom/google/android/music/ui/ManageDevicesFragment$DeviceInfoViewHolder;
    const v3, 0x7f0e0187

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, v2, Lcom/google/android/music/ui/ManageDevicesFragment$DeviceInfoViewHolder;->mDeviceArt:Landroid/widget/ImageView;

    .line 317
    const v3, 0x7f0e0189

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v2, Lcom/google/android/music/ui/ManageDevicesFragment$DeviceInfoViewHolder;->mDeviceName:Landroid/widget/TextView;

    .line 318
    const v3, 0x7f0e018a

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v2, Lcom/google/android/music/ui/ManageDevicesFragment$DeviceInfoViewHolder;->mLastUsedTime:Landroid/widget/TextView;

    .line 320
    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setTag(Ljava/lang/Object;)V

    .line 321
    return-object v1
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 242
    iget-object v0, p0, Lcom/google/android/music/ui/ManageDevicesFragment$DevicesListAdapter;->this$0:Lcom/google/android/music/ui/ManageDevicesFragment;

    # getter for: Lcom/google/android/music/ui/ManageDevicesFragment;->mAdapterData:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/music/ui/ManageDevicesFragment;->access$600(Lcom/google/android/music/ui/ManageDevicesFragment;)Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/music/ui/ManageDevicesFragment$DevicesListAdapter;->this$0:Lcom/google/android/music/ui/ManageDevicesFragment;

    # getter for: Lcom/google/android/music/ui/ManageDevicesFragment;->mAdapterData:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/music/ui/ManageDevicesFragment;->access$600(Lcom/google/android/music/ui/ManageDevicesFragment;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 247
    iget-object v0, p0, Lcom/google/android/music/ui/ManageDevicesFragment$DevicesListAdapter;->this$0:Lcom/google/android/music/ui/ManageDevicesFragment;

    # getter for: Lcom/google/android/music/ui/ManageDevicesFragment;->mAdapterData:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/music/ui/ManageDevicesFragment;->access$600(Lcom/google/android/music/ui/ManageDevicesFragment;)Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/music/ui/ManageDevicesFragment$DevicesListAdapter;->this$0:Lcom/google/android/music/ui/ManageDevicesFragment;

    # getter for: Lcom/google/android/music/ui/ManageDevicesFragment;->mAdapterData:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/music/ui/ManageDevicesFragment;->access$600(Lcom/google/android/music/ui/ManageDevicesFragment;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 254
    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 230
    invoke-virtual {p0, p1}, Lcom/google/android/music/ui/ManageDevicesFragment$DevicesListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 231
    const/4 v0, 0x0

    .line 233
    :goto_0
    return v0

    .line 232
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/music/ui/ManageDevicesFragment$DevicesListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/music/cloudclient/UserDeviceInfoJson;

    if-eqz v0, :cond_1

    .line 233
    const/4 v0, 0x1

    goto :goto_0

    .line 235
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Incorrect data type for manage device"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0, p1}, Lcom/google/android/music/ui/ManageDevicesFragment$DevicesListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 12
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v6, 0x0

    const/4 v11, 0x0

    .line 259
    invoke-virtual {p0, p1}, Lcom/google/android/music/ui/ManageDevicesFragment$DevicesListAdapter;->getItemViewType(I)I

    move-result v7

    if-nez v7, :cond_1

    .line 260
    invoke-direct {p0, p1, p3}, Lcom/google/android/music/ui/ManageDevicesFragment$DevicesListAdapter;->newViewForGroup(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    .line 308
    :cond_0
    :goto_0
    return-object v6

    .line 263
    :cond_1
    if-eqz p2, :cond_2

    instance-of v7, p2, Landroid/widget/TextView;

    if-eqz v7, :cond_3

    .line 264
    :cond_2
    invoke-direct {p0, p3}, Lcom/google/android/music/ui/ManageDevicesFragment$DevicesListAdapter;->newViewForItem(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 267
    :cond_3
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/music/ui/ManageDevicesFragment$DeviceInfoViewHolder;

    .line 268
    .local v3, "vh":Lcom/google/android/music/ui/ManageDevicesFragment$DeviceInfoViewHolder;
    invoke-virtual {p0, p1}, Lcom/google/android/music/ui/ManageDevicesFragment$DevicesListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/cloudclient/UserDeviceInfoJson;

    .line 269
    .local v1, "deviceInfo":Lcom/google/android/music/cloudclient/UserDeviceInfoJson;
    if-eqz v1, :cond_0

    .line 273
    iget-object v2, v1, Lcom/google/android/music/cloudclient/UserDeviceInfoJson;->mType:Ljava/lang/String;

    .line 275
    .local v2, "deviceType":Ljava/lang/String;
    const-string v7, "DESKTOP_APP"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 276
    iget-object v7, v3, Lcom/google/android/music/ui/ManageDevicesFragment$DeviceInfoViewHolder;->mDeviceArt:Landroid/widget/ImageView;

    const v8, 0x7f0200e0

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 283
    :goto_1
    iget-object v7, v1, Lcom/google/android/music/cloudclient/UserDeviceInfoJson;->mDeviceName:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 284
    iget-object v7, v3, Lcom/google/android/music/ui/ManageDevicesFragment$DeviceInfoViewHolder;->mDeviceName:Landroid/widget/TextView;

    iget-object v8, p0, Lcom/google/android/music/ui/ManageDevicesFragment$DevicesListAdapter;->this$0:Lcom/google/android/music/ui/ManageDevicesFragment;

    const v9, 0x7f0b032e

    invoke-virtual {v8, v9}, Lcom/google/android/music/ui/ManageDevicesFragment;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 289
    :goto_2
    iget-object v7, p0, Lcom/google/android/music/ui/ManageDevicesFragment$DevicesListAdapter;->this$0:Lcom/google/android/music/ui/ManageDevicesFragment;

    # getter for: Lcom/google/android/music/ui/ManageDevicesFragment;->mCurrentDeviceId:Ljava/lang/String;
    invoke-static {v7}, Lcom/google/android/music/ui/ManageDevicesFragment;->access$700(Lcom/google/android/music/ui/ManageDevicesFragment;)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_7

    iget-object v7, p0, Lcom/google/android/music/ui/ManageDevicesFragment$DevicesListAdapter;->this$0:Lcom/google/android/music/ui/ManageDevicesFragment;

    # getter for: Lcom/google/android/music/ui/ManageDevicesFragment;->mCurrentDeviceId:Ljava/lang/String;
    invoke-static {v7}, Lcom/google/android/music/ui/ManageDevicesFragment;->access$700(Lcom/google/android/music/ui/ManageDevicesFragment;)Ljava/lang/String;

    move-result-object v7

    iget-object v8, v1, Lcom/google/android/music/cloudclient/UserDeviceInfoJson;->mId:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 290
    iget-object v7, v3, Lcom/google/android/music/ui/ManageDevicesFragment$DeviceInfoViewHolder;->mLastUsedTime:Landroid/widget/TextView;

    const v8, 0x7f0b0326

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(I)V

    .line 291
    iget-object v7, v3, Lcom/google/android/music/ui/ManageDevicesFragment$DeviceInfoViewHolder;->mLastUsedTime:Landroid/widget/TextView;

    invoke-virtual {v7, v11}, Landroid/widget/TextView;->setVisibility(I)V

    .line 305
    :goto_3
    iget-object v7, v1, Lcom/google/android/music/cloudclient/UserDeviceInfoJson;->mId:Ljava/lang/String;

    iput-object v7, v3, Lcom/google/android/music/ui/ManageDevicesFragment$DeviceInfoViewHolder;->mDeviceId:Ljava/lang/String;

    .line 307
    new-instance v7, Lcom/google/android/music/ui/ManageDevicesFragment$DeauthorizeDeviceClickListner;

    iget-object v8, p0, Lcom/google/android/music/ui/ManageDevicesFragment$DevicesListAdapter;->this$0:Lcom/google/android/music/ui/ManageDevicesFragment;

    invoke-direct {v7, v8, v6}, Lcom/google/android/music/ui/ManageDevicesFragment$DeauthorizeDeviceClickListner;-><init>(Lcom/google/android/music/ui/ManageDevicesFragment;Lcom/google/android/music/ui/ManageDevicesFragment$1;)V

    invoke-virtual {p2, v7}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object v6, p2

    .line 308
    goto :goto_0

    .line 277
    :cond_4
    const-string v7, "ANDROID"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 278
    iget-object v7, v3, Lcom/google/android/music/ui/ManageDevicesFragment$DeviceInfoViewHolder;->mDeviceArt:Landroid/widget/ImageView;

    const v8, 0x7f0200ca

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    .line 280
    :cond_5
    iget-object v7, v3, Lcom/google/android/music/ui/ManageDevicesFragment$DeviceInfoViewHolder;->mDeviceArt:Landroid/widget/ImageView;

    const v8, 0x7f020108

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    .line 286
    :cond_6
    iget-object v7, v3, Lcom/google/android/music/ui/ManageDevicesFragment$DeviceInfoViewHolder;->mDeviceName:Landroid/widget/TextView;

    iget-object v8, v1, Lcom/google/android/music/cloudclient/UserDeviceInfoJson;->mDeviceName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 293
    :cond_7
    iget-wide v4, v1, Lcom/google/android/music/cloudclient/UserDeviceInfoJson;->mLastAccessedTimeMs:J

    .line 294
    .local v4, "lastAccessedTimeMs":J
    const-wide/16 v8, -0x1

    cmp-long v7, v4, v8

    if-eqz v7, :cond_8

    .line 295
    iget-object v7, p0, Lcom/google/android/music/ui/ManageDevicesFragment$DevicesListAdapter;->this$0:Lcom/google/android/music/ui/ManageDevicesFragment;

    invoke-virtual {v7}, Lcom/google/android/music/ui/ManageDevicesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v7

    invoke-static {v7}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v7

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 297
    .local v0, "date":Ljava/lang/String;
    iget-object v7, v3, Lcom/google/android/music/ui/ManageDevicesFragment$DeviceInfoViewHolder;->mLastUsedTime:Landroid/widget/TextView;

    iget-object v8, p0, Lcom/google/android/music/ui/ManageDevicesFragment$DevicesListAdapter;->this$0:Lcom/google/android/music/ui/ManageDevicesFragment;

    const v9, 0x7f0b0327

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    aput-object v0, v10, v11

    invoke-virtual {v8, v9, v10}, Lcom/google/android/music/ui/ManageDevicesFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 299
    iget-object v7, v3, Lcom/google/android/music/ui/ManageDevicesFragment$DeviceInfoViewHolder;->mLastUsedTime:Landroid/widget/TextView;

    invoke-virtual {v7, v11}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_3

    .line 301
    .end local v0    # "date":Ljava/lang/String;
    :cond_8
    iget-object v7, v3, Lcom/google/android/music/ui/ManageDevicesFragment$DeviceInfoViewHolder;->mLastUsedTime:Landroid/widget/TextView;

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_3
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 221
    iget-object v0, p0, Lcom/google/android/music/ui/ManageDevicesFragment$DevicesListAdapter;->this$0:Lcom/google/android/music/ui/ManageDevicesFragment;

    # getter for: Lcom/google/android/music/ui/ManageDevicesFragment;->mSmartPhones:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/music/ui/ManageDevicesFragment;->access$500(Lcom/google/android/music/ui/ManageDevicesFragment;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/ui/ManageDevicesFragment$DevicesListAdapter;->this$0:Lcom/google/android/music/ui/ManageDevicesFragment;

    # getter for: Lcom/google/android/music/ui/ManageDevicesFragment;->mUserDevices:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/music/ui/ManageDevicesFragment;->access$300(Lcom/google/android/music/ui/ManageDevicesFragment;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 222
    const/4 v0, 0x2

    .line 224
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
