.class Lcom/google/android/music/ui/NowPlayingHeaderPageFragment$1;
.super Landroid/content/BroadcastReceiver;
.source "NowPlayingHeaderPageFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;)V
    .locals 0

    .prologue
    .line 69
    iput-object p1, p0, Lcom/google/android/music/ui/NowPlayingHeaderPageFragment$1;->this$0:Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 72
    const-string v2, "ListPosition"

    const-wide/16 v4, -0x1

    invoke-virtual {p2, v2, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    .line 77
    .local v0, "currentPos":J
    iget-object v2, p0, Lcom/google/android/music/ui/NowPlayingHeaderPageFragment$1;->this$0:Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;

    # getter for: Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;->mQueuePosition:I
    invoke-static {v2}, Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;->access$000(Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;)I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    iget-object v2, p0, Lcom/google/android/music/ui/NowPlayingHeaderPageFragment$1;->this$0:Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;

    # getter for: Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;->mQueuePosition:I
    invoke-static {v2}, Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;->access$000(Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;)I

    move-result v2

    int-to-long v2, v2

    cmp-long v2, v0, v2

    if-eqz v2, :cond_1

    .line 83
    :cond_0
    :goto_0
    return-void

    .line 82
    :cond_1
    iget-object v2, p0, Lcom/google/android/music/ui/NowPlayingHeaderPageFragment$1;->this$0:Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;

    # invokes: Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;->updateStreaming(Landroid/content/Intent;)V
    invoke-static {v2, p2}, Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;->access$100(Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;Landroid/content/Intent;)V

    goto :goto_0
.end method
