.class public Lcom/google/android/music/preferences/MusicPreferences;
.super Ljava/lang/Object;
.source "MusicPreferences.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/preferences/MusicPreferences$AsyncPreferenceUpdate;
    }
.end annotation


# static fields
.field private static final ACCOUNT_DEPENDENT_PREF_KEYS:[Ljava/lang/String;

.field private static final LOGV:Z

.field private static final MSG_REFRESH_STREAMING_ENABLED:I

.field private static sMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

.field private static sPreferenceReferences:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field


# instance fields
.field private mCachedPreferences:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final mContext:Landroid/content/Context;

.field private final mIsLargeScreen:Z

.field private final mIsTv:Z

.field private final mIsVoiceCapable:Z

.field private final mIsXLargeScreen:Z

.field private mPreferenceChangeListener:Lcom/google/android/music/preferences/IPreferenceChangeListener;

.field private mPreferenceChangeListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;",
            ">;"
        }
    .end annotation
.end field

.field private mPreferenceService:Lcom/google/android/music/preferences/IPreferenceService;

.field private mPreferenceServiceBound:Z

.field private mPreferenceServiceConnected:Z

.field private mPreferenceServiceSafeConnection:Lcom/google/android/music/utils/SafeServiceConnection;

.field private final mRefreshStreamingEnabledRunnable:Ljava/lang/Runnable;

.field private final mRunOnceServiceConnected:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 67
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->PREFERENCES:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/preferences/MusicPreferences;->LOGV:Z

    .line 198
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "DisplayOptions"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/preferences/MusicPreferences;->ACCOUNT_DEPENDENT_PREF_KEYS:[Ljava/lang/String;

    .line 228
    sget-object v0, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    invoke-static {v0}, Lcom/google/android/music/utils/async/AsyncWorkers;->getUniqueMessageType(Lcom/google/android/music/utils/LoggableHandler;)I

    move-result v0

    sput v0, Lcom/google/android/music/preferences/MusicPreferences;->MSG_REFRESH_STREAMING_ENABLED:I

    .line 1711
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/music/preferences/MusicPreferences;->sMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    .line 1713
    invoke-static {}, Lcom/google/common/collect/Lists;->newLinkedList()Ljava/util/LinkedList;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/preferences/MusicPreferences;->sPreferenceReferences:Ljava/util/LinkedList;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    .line 244
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 214
    invoke-static {}, Lcom/google/common/collect/Lists;->newLinkedList()Ljava/util/LinkedList;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/preferences/MusicPreferences;->mPreferenceChangeListeners:Ljava/util/List;

    .line 221
    invoke-static {}, Lcom/google/common/collect/Lists;->newLinkedList()Ljava/util/LinkedList;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/preferences/MusicPreferences;->mRunOnceServiceConnected:Ljava/util/List;

    .line 223
    iput-boolean v3, p0, Lcom/google/android/music/preferences/MusicPreferences;->mPreferenceServiceBound:Z

    .line 226
    iput-boolean v3, p0, Lcom/google/android/music/preferences/MusicPreferences;->mPreferenceServiceConnected:Z

    .line 231
    new-instance v1, Lcom/google/android/music/preferences/MusicPreferences$1;

    invoke-direct {v1, p0}, Lcom/google/android/music/preferences/MusicPreferences$1;-><init>(Lcom/google/android/music/preferences/MusicPreferences;)V

    iput-object v1, p0, Lcom/google/android/music/preferences/MusicPreferences;->mPreferenceServiceSafeConnection:Lcom/google/android/music/utils/SafeServiceConnection;

    .line 328
    new-instance v1, Lcom/google/android/music/preferences/MusicPreferences$2;

    invoke-direct {v1, p0}, Lcom/google/android/music/preferences/MusicPreferences$2;-><init>(Lcom/google/android/music/preferences/MusicPreferences;)V

    iput-object v1, p0, Lcom/google/android/music/preferences/MusicPreferences;->mRefreshStreamingEnabledRunnable:Ljava/lang/Runnable;

    .line 1786
    new-instance v1, Lcom/google/android/music/preferences/MusicPreferences$7;

    invoke-direct {v1, p0}, Lcom/google/android/music/preferences/MusicPreferences$7;-><init>(Lcom/google/android/music/preferences/MusicPreferences;)V

    iput-object v1, p0, Lcom/google/android/music/preferences/MusicPreferences;->mPreferenceChangeListener:Lcom/google/android/music/preferences/IPreferenceChangeListener;

    .line 245
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/preferences/MusicPreferences;->mContext:Landroid/content/Context;

    .line 247
    invoke-direct {p0}, Lcom/google/android/music/preferences/MusicPreferences;->getIsXLargeScreen()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/music/preferences/MusicPreferences;->mIsXLargeScreen:Z

    .line 248
    invoke-direct {p0}, Lcom/google/android/music/preferences/MusicPreferences;->getIsLargeScreen()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/music/preferences/MusicPreferences;->mIsLargeScreen:Z

    .line 249
    invoke-direct {p0}, Lcom/google/android/music/preferences/MusicPreferences;->getIsVoiceCapable()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/music/preferences/MusicPreferences;->mIsVoiceCapable:Z

    .line 250
    invoke-static {p1}, Lcom/google/android/music/utils/SystemUtils;->isTv(Landroid/content/Context;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/music/preferences/MusicPreferences;->mIsTv:Z

    .line 251
    iget-boolean v1, p0, Lcom/google/android/music/preferences/MusicPreferences;->mIsTv:Z

    if-eqz v1, :cond_0

    sget-boolean v1, Lcom/google/android/music/preferences/MusicPreferences;->LOGV:Z

    if-eqz v1, :cond_0

    .line 252
    const-string v1, "MusicPreferences"

    const-string v2, "Found TV feature, using TV experience."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 255
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/preferences/MusicPreferences;->mContext:Landroid/content/Context;

    const-string v2, "MusicPreferences"

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 257
    .local v0, "sharedPreferences":Landroid/content/SharedPreferences;
    invoke-interface {v0}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/preferences/MusicPreferences;->mCachedPreferences:Ljava/util/Map;

    .line 258
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/preferences/MusicPreferences;Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/preferences/MusicPreferences;
    .param p1, "x1"    # Landroid/content/ComponentName;
    .param p2, "x2"    # Landroid/os/IBinder;

    .prologue
    .line 64
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/preferences/MusicPreferences;->onServiceConnectedImp(Landroid/content/ComponentName;Landroid/os/IBinder;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/music/preferences/MusicPreferences;Landroid/content/ComponentName;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/preferences/MusicPreferences;
    .param p1, "x1"    # Landroid/content/ComponentName;

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lcom/google/android/music/preferences/MusicPreferences;->onServiceDisconnectedImp(Landroid/content/ComponentName;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/music/preferences/MusicPreferences;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/preferences/MusicPreferences;

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/google/android/music/preferences/MusicPreferences;->refreshStreamingEnabledSync()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/music/preferences/MusicPreferences;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/preferences/MusicPreferences;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/music/preferences/MusicPreferences;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/music/preferences/MusicPreferences;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/preferences/MusicPreferences;

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/google/android/music/preferences/MusicPreferences;->hasPreferenceService()Z

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/google/android/music/preferences/MusicPreferences;)Lcom/google/android/music/preferences/IPreferenceService;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/preferences/MusicPreferences;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/music/preferences/MusicPreferences;->mPreferenceService:Lcom/google/android/music/preferences/IPreferenceService;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/music/preferences/MusicPreferences;)Ljava/util/Map;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/preferences/MusicPreferences;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/music/preferences/MusicPreferences;->mCachedPreferences:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/music/preferences/MusicPreferences;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/preferences/MusicPreferences;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lcom/google/android/music/preferences/MusicPreferences;->notifyPreferenceChangeListeners(Ljava/lang/String;)V

    return-void
.end method

.method private declared-synchronized bindToPreferenceService()V
    .locals 5

    .prologue
    .line 261
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/music/preferences/MusicPreferences;->mPreferenceServiceBound:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 270
    :goto_0
    monitor-exit p0

    return-void

    .line 264
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/music/preferences/MusicPreferences;->mPreferenceServiceSafeConnection:Lcom/google/android/music/utils/SafeServiceConnection;

    iget-object v1, p0, Lcom/google/android/music/preferences/MusicPreferences;->mContext:Landroid/content/Context;

    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lcom/google/android/music/preferences/MusicPreferences;->mContext:Landroid/content/Context;

    const-class v4, Lcom/google/android/music/preferences/MusicPreferenceService$MusicPreferenceServiceBinder;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/music/utils/SafeServiceConnection;->bindService(Landroid/content/Context;Landroid/content/Intent;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 267
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Could not connect to the preference service"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 261
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 269
    :cond_1
    const/4 v0, 0x1

    :try_start_2
    iput-boolean v0, p0, Lcom/google/android/music/preferences/MusicPreferences;->mPreferenceServiceBound:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method private clearAccountDependentPreferences()V
    .locals 4

    .prologue
    .line 872
    sget-object v0, Lcom/google/android/music/preferences/MusicPreferences;->ACCOUNT_DEPENDENT_PREF_KEYS:[Ljava/lang/String;

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v2, v0, v1

    .line 873
    .local v2, "key":Ljava/lang/String;
    invoke-direct {p0, v2}, Lcom/google/android/music/preferences/MusicPreferences;->resetPreference(Ljava/lang/String;)V

    .line 872
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 875
    .end local v2    # "key":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method private getBooleanPreference(Ljava/lang/String;Z)Z
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "defValue"    # Z

    .prologue
    .line 1585
    iget-object v0, p0, Lcom/google/android/music/preferences/MusicPreferences;->mCachedPreferences:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1586
    iget-object v0, p0, Lcom/google/android/music/preferences/MusicPreferences;->mCachedPreferences:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    .line 1588
    .end local p2    # "defValue":Z
    :cond_0
    return p2
.end method

.method private getIntPreference(Ljava/lang/String;I)I
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "defValue"    # I

    .prologue
    .line 1611
    iget-object v0, p0, Lcom/google/android/music/preferences/MusicPreferences;->mCachedPreferences:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1612
    iget-object v0, p0, Lcom/google/android/music/preferences/MusicPreferences;->mCachedPreferences:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result p2

    .line 1614
    .end local p2    # "defValue":I
    :cond_0
    return p2
.end method

.method private getIsLargeScreen()Z
    .locals 3

    .prologue
    .line 443
    iget-object v2, p0, Lcom/google/android/music/preferences/MusicPreferences;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 444
    .local v0, "config":Landroid/content/res/Configuration;
    iget v2, v0, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v1, v2, 0x3

    .line 445
    .local v1, "screenSize":I
    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private getIsVoiceCapable()Z
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 458
    iget-object v6, p0, Lcom/google/android/music/preferences/MusicPreferences;->mContext:Landroid/content/Context;

    const-string v7, "phone"

    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/telephony/TelephonyManager;

    .line 460
    .local v4, "telephonyManager":Landroid/telephony/TelephonyManager;
    if-nez v4, :cond_0

    .line 481
    :goto_0
    return v5

    .line 465
    :cond_0
    const/4 v5, 0x0

    :try_start_0
    new-array v1, v5, [Ljava/lang/Class;

    .line 466
    .local v1, "partypes":[Ljava/lang/Class;
    const-class v5, Landroid/telephony/TelephonyManager;

    const-string v6, "isVoiceCapable"

    invoke-virtual {v5, v6, v1}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    .line 469
    .local v3, "sIsVoiceCapable":Ljava/lang/reflect/Method;
    const/4 v5, 0x0

    new-array v0, v5, [Ljava/lang/Object;

    .line 470
    .local v0, "arglist":[Ljava/lang/Object;
    invoke-virtual {v3, v4, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 471
    .local v2, "retobj":Ljava/lang/Object;
    check-cast v2, Ljava/lang/Boolean;

    .end local v2    # "retobj":Ljava/lang/Object;
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    goto :goto_0

    .line 479
    .end local v0    # "arglist":[Ljava/lang/Object;
    .end local v1    # "partypes":[Ljava/lang/Class;
    .end local v3    # "sIsVoiceCapable":Ljava/lang/reflect/Method;
    :catch_0
    move-exception v5

    .line 481
    :goto_1
    const/4 v5, 0x1

    goto :goto_0

    .line 476
    :catch_1
    move-exception v5

    goto :goto_1

    .line 472
    :catch_2
    move-exception v5

    goto :goto_1
.end method

.method private getIsXLargeScreen()Z
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 422
    :try_start_0
    const-class v4, Landroid/content/res/Configuration;

    const-string v5, "SCREENLAYOUT_SIZE_XLARGE"

    invoke-virtual {v4, v5}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 425
    .local v1, "sScreenLayoutSizeXLarge":Ljava/lang/reflect/Field;
    iget-object v4, p0, Lcom/google/android/music/preferences/MusicPreferences;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 427
    .local v0, "config":Landroid/content/res/Configuration;
    :try_start_1
    invoke-virtual {v1, v0}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v4

    and-int/lit8 v2, v4, 0xf

    .line 429
    .local v2, "xlargeValue":I
    iget v4, v0, Landroid/content/res/Configuration;->screenLayout:I
    :try_end_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/NoSuchFieldException; {:try_start_1 .. :try_end_1} :catch_0

    and-int/lit8 v4, v4, 0xf

    if-ne v2, v4, :cond_0

    const/4 v3, 0x1

    .line 439
    .end local v0    # "config":Landroid/content/res/Configuration;
    .end local v1    # "sScreenLayoutSizeXLarge":Ljava/lang/reflect/Field;
    .end local v2    # "xlargeValue":I
    :cond_0
    :goto_0
    return v3

    .line 436
    :catch_0
    move-exception v4

    goto :goto_0

    .line 432
    .restart local v0    # "config":Landroid/content/res/Configuration;
    .restart local v1    # "sScreenLayoutSizeXLarge":Ljava/lang/reflect/Field;
    :catch_1
    move-exception v4

    goto :goto_0
.end method

.method private getLastUpgradeSyncVersion()I
    .locals 2

    .prologue
    .line 1915
    const-string v0, "lastUpgradeSyncVersion"

    const/4 v1, -0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->getIntPreference(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method private getLongPreference(Ljava/lang/String;J)J
    .locals 2
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "defValue"    # J

    .prologue
    .line 1605
    iget-object v0, p0, Lcom/google/android/music/preferences/MusicPreferences;->mCachedPreferences:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1606
    iget-object v0, p0, Lcom/google/android/music/preferences/MusicPreferences;->mCachedPreferences:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide p2

    .line 1608
    .end local p2    # "defValue":J
    :cond_0
    return-wide p2
.end method

.method public static getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "reference"    # Ljava/lang/Object;

    .prologue
    .line 1716
    sget-object v2, Lcom/google/android/music/preferences/MusicPreferences;->sPreferenceReferences:Ljava/util/LinkedList;

    monitor-enter v2

    .line 1717
    :try_start_0
    sget-object v1, Lcom/google/android/music/preferences/MusicPreferences;->sPreferenceReferences:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    .line 1718
    .local v0, "firstReference":Z
    sget-object v1, Lcom/google/android/music/preferences/MusicPreferences;->sPreferenceReferences:Ljava/util/LinkedList;

    new-instance v3, Ljava/lang/ref/WeakReference;

    invoke-direct {v3, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v1, v3}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 1719
    sget-object v1, Lcom/google/android/music/preferences/MusicPreferences;->sMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    if-nez v1, :cond_0

    .line 1720
    new-instance v1, Lcom/google/android/music/preferences/MusicPreferences;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v1, v3}, Lcom/google/android/music/preferences/MusicPreferences;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/google/android/music/preferences/MusicPreferences;->sMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    .line 1722
    :cond_0
    if-eqz v0, :cond_1

    .line 1723
    sget-object v1, Lcom/google/android/music/preferences/MusicPreferences;->sMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    invoke-direct {v1}, Lcom/google/android/music/preferences/MusicPreferences;->bindToPreferenceService()V

    .line 1726
    :cond_1
    sget-object v1, Lcom/google/android/music/preferences/MusicPreferences;->sMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    monitor-exit v2

    return-object v1

    .line 1727
    .end local v0    # "firstReference":Z
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private getStringPreference(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "defValue"    # Ljava/lang/String;

    .prologue
    .line 1592
    iget-object v0, p0, Lcom/google/android/music/preferences/MusicPreferences;->mCachedPreferences:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1593
    iget-object v0, p0, Lcom/google/android/music/preferences/MusicPreferences;->mCachedPreferences:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1595
    :goto_0
    return-object v0

    :cond_0
    move-object v0, p2

    goto :goto_0
.end method

.method private declared-synchronized hasPreferenceService()Z
    .locals 1

    .prologue
    .line 1859
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/preferences/MusicPreferences;->mPreferenceService:Lcom/google/android/music/preferences/IPreferenceService;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private isConfiguredSyncAccount()Z
    .locals 2

    .prologue
    .line 759
    const-string v0, "SelectedAccount"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->getStringPreference(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isGingerbreadOrGreater()Z
    .locals 2

    .prologue
    .line 607
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x9

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isGlass()Z
    .locals 2

    .prologue
    .line 682
    sget-object v0, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    const-string v1, "glass"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static isHoneycombMr1OrGreater()Z
    .locals 2

    .prologue
    .line 623
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xc

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isHoneycombOrGreater()Z
    .locals 2

    .prologue
    .line 615
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isICSOrGreater()Z
    .locals 2

    .prologue
    .line 631
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isJellyBeanMR1OrGreater()Z
    .locals 2

    .prologue
    .line 647
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isJellyBeanMR2OrGreater()Z
    .locals 2

    .prologue
    .line 655
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isJellyBeanOrGreater()Z
    .locals 2

    .prologue
    .line 639
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isKitKatOrGreater()Z
    .locals 2

    .prologue
    .line 662
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isLmpMr1OrGreater()Z
    .locals 2

    .prologue
    .line 676
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x16

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isLmpOrGreater()Z
    .locals 2

    .prologue
    .line 669
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x14

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private logValueRemoved(Ljava/lang/String;)V
    .locals 3
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 1648
    sget-boolean v0, Lcom/google/android/music/preferences/MusicPreferences;->LOGV:Z

    if-eqz v0, :cond_0

    .line 1649
    const-string v0, "MusicPreferences"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Removed pref key: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1651
    :cond_0
    return-void
.end method

.method private logValueSet(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 3
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/Object;

    .prologue
    .line 1642
    sget-boolean v0, Lcom/google/android/music/preferences/MusicPreferences;->LOGV:Z

    if-eqz v0, :cond_0

    .line 1643
    const-string v0, "MusicPreferences"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Set pref key "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1645
    :cond_0
    return-void
.end method

.method private notifyPreferenceChangeListeners(Ljava/lang/String;)V
    .locals 4
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 1825
    iget-object v3, p0, Lcom/google/android/music/preferences/MusicPreferences;->mPreferenceChangeListeners:Ljava/util/List;

    monitor-enter v3

    .line 1826
    :try_start_0
    iget-object v2, p0, Lcom/google/android/music/preferences/MusicPreferences;->mPreferenceChangeListeners:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    .line 1827
    .local v1, "listener":Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;
    const/4 v2, 0x0

    invoke-interface {v1, v2, p1}, Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;->onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V

    goto :goto_0

    .line 1829
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "listener":Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1830
    return-void
.end method

.method private onServiceConnectedImp(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 1756
    monitor-enter p0

    .line 1757
    :try_start_0
    invoke-static {p2}, Lcom/google/android/music/preferences/IPreferenceService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/google/android/music/preferences/IPreferenceService;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/preferences/MusicPreferences;->mPreferenceService:Lcom/google/android/music/preferences/IPreferenceService;

    .line 1758
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/music/preferences/MusicPreferences;->mPreferenceServiceConnected:Z

    .line 1759
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1761
    :try_start_1
    iget-object v1, p0, Lcom/google/android/music/preferences/MusicPreferences;->mPreferenceService:Lcom/google/android/music/preferences/IPreferenceService;

    iget-object v2, p0, Lcom/google/android/music/preferences/MusicPreferences;->mPreferenceChangeListener:Lcom/google/android/music/preferences/IPreferenceChangeListener;

    invoke-interface {v1, v2}, Lcom/google/android/music/preferences/IPreferenceService;->registerPreferenceChangeListener(Lcom/google/android/music/preferences/IPreferenceChangeListener;)V

    .line 1765
    iget-object v1, p0, Lcom/google/android/music/preferences/MusicPreferences;->mPreferenceService:Lcom/google/android/music/preferences/IPreferenceService;

    invoke-interface {v1}, Lcom/google/android/music/preferences/IPreferenceService;->getCurrentPreferences()Ljava/util/Map;

    move-result-object v1

    invoke-static {v1}, Lcom/google/common/collect/Maps;->newLinkedHashMap(Ljava/util/Map;)Ljava/util/LinkedHashMap;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/preferences/MusicPreferences;->mCachedPreferences:Ljava/util/Map;
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1770
    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1772
    invoke-direct {p0}, Lcom/google/android/music/preferences/MusicPreferences;->refreshStreamingEnabled()V

    .line 1776
    invoke-virtual {p0}, Lcom/google/android/music/preferences/MusicPreferences;->clearSyncForUnselectedAccounts()V

    .line 1778
    invoke-direct {p0}, Lcom/google/android/music/preferences/MusicPreferences;->runSavedServiceTasks()V

    .line 1780
    monitor-enter p0

    .line 1781
    :try_start_3
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 1782
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1783
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->notifyPreferenceChangeListeners(Ljava/lang/String;)V

    .line 1784
    :goto_0
    return-void

    .line 1766
    :catch_0
    move-exception v0

    .line 1767
    .local v0, "e":Landroid/os/RemoteException;
    :try_start_4
    const-string v1, "MusicPreferences"

    const-string v2, "Could not register remote preference change listener"

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1768
    monitor-exit p0

    goto :goto_0

    .line 1770
    .end local v0    # "e":Landroid/os/RemoteException;
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v1

    .line 1782
    :catchall_1
    move-exception v1

    :try_start_5
    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v1
.end method

.method private declared-synchronized onServiceDisconnectedImp(Landroid/content/ComponentName;)V
    .locals 1
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 1854
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/google/android/music/preferences/MusicPreferences;->mPreferenceService:Lcom/google/android/music/preferences/IPreferenceService;

    .line 1855
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/preferences/MusicPreferences;->mPreferenceServiceConnected:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1856
    monitor-exit p0

    return-void

    .line 1854
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private refreshNetworkUsageComponent()V
    .locals 5

    .prologue
    .line 948
    invoke-direct {p0}, Lcom/google/android/music/preferences/MusicPreferences;->isConfiguredSyncAccount()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 949
    const/4 v0, 0x1

    .line 954
    .local v0, "newState":I
    :goto_0
    iget-object v1, p0, Lcom/google/android/music/preferences/MusicPreferences;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    new-instance v2, Landroid/content/ComponentName;

    iget-object v3, p0, Lcom/google/android/music/preferences/MusicPreferences;->mContext:Landroid/content/Context;

    const-string v4, "com.google.android.music.Settings.NetworkUsage"

    invoke-direct {v2, v3, v4}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 957
    return-void

    .line 951
    .end local v0    # "newState":I
    :cond_0
    const/4 v0, 0x2

    .restart local v0    # "newState":I
    goto :goto_0
.end method

.method private refreshStreamingEnabled()V
    .locals 4

    .prologue
    .line 322
    iget-object v0, p0, Lcom/google/android/music/preferences/MusicPreferences;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/music/utils/MusicUtils;->isMainProcess(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 323
    sget-object v0, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    sget v1, Lcom/google/android/music/preferences/MusicPreferences;->MSG_REFRESH_STREAMING_ENABLED:I

    iget-object v2, p0, Lcom/google/android/music/preferences/MusicPreferences;->mRefreshStreamingEnabledRunnable:Ljava/lang/Runnable;

    const/4 v3, 0x1

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsync(Lcom/google/android/music/utils/LoggableHandler;ILjava/lang/Runnable;Z)V

    .line 326
    :cond_0
    return-void
.end method

.method private refreshStreamingEnabledSync()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 336
    invoke-direct {p0}, Lcom/google/android/music/preferences/MusicPreferences;->upgradeLegacyInvalidAccount()Z

    move-result v1

    .line 337
    .local v1, "upgraded":Z
    if-nez v1, :cond_0

    .line 338
    invoke-virtual {p0}, Lcom/google/android/music/preferences/MusicPreferences;->getSyncAccount()Landroid/accounts/Account;

    move-result-object v0

    .line 339
    .local v0, "account":Landroid/accounts/Account;
    if-nez v0, :cond_1

    .line 340
    invoke-virtual {p0, v5}, Lcom/google/android/music/preferences/MusicPreferences;->setStreamingAccount(Landroid/accounts/Account;)V

    .line 355
    .end local v0    # "account":Landroid/accounts/Account;
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/google/android/music/preferences/MusicPreferences;->refreshNetworkUsageComponent()V

    .line 357
    invoke-direct {p0, v5}, Lcom/google/android/music/preferences/MusicPreferences;->notifyPreferenceChangeListeners(Ljava/lang/String;)V

    .line 359
    invoke-virtual {p0}, Lcom/google/android/music/preferences/MusicPreferences;->maybeEnableUpgradeSync()V

    .line 360
    return-void

    .line 348
    .restart local v0    # "account":Landroid/accounts/Account;
    :cond_1
    sget-boolean v2, Lcom/google/android/music/preferences/MusicPreferences;->LOGV:Z

    if-eqz v2, :cond_2

    .line 349
    const-string v2, "MusicPreferences"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Re-setting music sync state for account "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 351
    :cond_2
    const-string v2, "com.google.android.music.MusicContent"

    const/4 v3, 0x1

    invoke-static {v0, v2, v3}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public static releaseMusicPreferences(Ljava/lang/Object;)V
    .locals 7
    .param p0, "reference"    # Ljava/lang/Object;

    .prologue
    .line 1731
    sget-object v4, Lcom/google/android/music/preferences/MusicPreferences;->sPreferenceReferences:Ljava/util/LinkedList;

    monitor-enter v4

    .line 1732
    :try_start_0
    sget-object v3, Lcom/google/android/music/preferences/MusicPreferences;->sPreferenceReferences:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 1733
    .local v0, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/ref/WeakReference<Ljava/lang/Object;>;>;"
    const/4 v2, 0x0

    .line 1734
    .local v2, "referenceFound":Z
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1735
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/ref/WeakReference;

    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    .line 1736
    .local v1, "possibleReference":Ljava/lang/Object;
    if-eqz v1, :cond_1

    if-ne v1, p0, :cond_0

    .line 1737
    :cond_1
    if-eqz v1, :cond_2

    .line 1738
    const/4 v2, 0x1

    .line 1740
    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 1752
    .end local v0    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/ref/WeakReference<Ljava/lang/Object;>;>;"
    .end local v1    # "possibleReference":Ljava/lang/Object;
    .end local v2    # "referenceFound":Z
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .line 1743
    .restart local v0    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/ref/WeakReference<Ljava/lang/Object;>;>;"
    .restart local v2    # "referenceFound":Z
    :cond_3
    if-nez v2, :cond_4

    .line 1744
    :try_start_1
    const-string v3, "MusicPreferences"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Could not find reference holding on to MusicPreferences: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/Throwable;

    invoke-direct {v6}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v3, v5, v6}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1747
    :cond_4
    sget-object v3, Lcom/google/android/music/preferences/MusicPreferences;->sPreferenceReferences:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_5

    sget-object v3, Lcom/google/android/music/preferences/MusicPreferences;->sMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    if-eqz v3, :cond_5

    .line 1750
    sget-object v3, Lcom/google/android/music/preferences/MusicPreferences;->sMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    invoke-direct {v3}, Lcom/google/android/music/preferences/MusicPreferences;->unbindFromPreferenceService()V

    .line 1752
    :cond_5
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1753
    return-void
.end method

.method private resetPreference(Ljava/lang/String;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 1623
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/music/preferences/MusicPreferences;->setPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1624
    return-void
.end method

.method private declared-synchronized runSavedServiceTasks()V
    .locals 3

    .prologue
    .line 1847
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/music/preferences/MusicPreferences;->mRunOnceServiceConnected:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Runnable;

    .line 1848
    .local v1, "task":Ljava/lang/Runnable;
    sget-object v2, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    invoke-static {v2, v1}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsync(Lcom/google/android/music/utils/LoggableHandler;Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1847
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "task":Ljava/lang/Runnable;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 1850
    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/google/android/music/preferences/MusicPreferences;->mRunOnceServiceConnected:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1851
    monitor-exit p0

    return-void
.end method

.method private setPreference(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/Object;

    .prologue
    .line 1618
    invoke-virtual {p0, p1, p2}, Lcom/google/android/music/preferences/MusicPreferences;->changeValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1619
    new-instance v0, Lcom/google/android/music/preferences/MusicPreferences$AsyncPreferenceUpdate;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/music/preferences/MusicPreferences$AsyncPreferenceUpdate;-><init>(Lcom/google/android/music/preferences/MusicPreferences;Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {p0, v0}, Lcom/google/android/music/preferences/MusicPreferences;->runWithPreferenceService(Ljava/lang/Runnable;)V

    .line 1620
    return-void
.end method

.method private setPreferenceWithExtraTask(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Runnable;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/Object;
    .param p3, "extraTask"    # Ljava/lang/Runnable;

    .prologue
    .line 1627
    invoke-virtual {p0, p1, p2}, Lcom/google/android/music/preferences/MusicPreferences;->changeValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1628
    new-instance v0, Lcom/google/android/music/preferences/MusicPreferences$AsyncPreferenceUpdate;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/music/preferences/MusicPreferences$AsyncPreferenceUpdate;-><init>(Lcom/google/android/music/preferences/MusicPreferences;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Runnable;)V

    invoke-virtual {p0, v0}, Lcom/google/android/music/preferences/MusicPreferences;->runWithPreferenceService(Ljava/lang/Runnable;)V

    .line 1629
    return-void
.end method

.method private setStreamingAccount(Landroid/accounts/Account;Z)V
    .locals 6
    .param p1, "newAccount"    # Landroid/accounts/Account;
    .param p2, "isValid"    # Z

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x0

    .line 832
    if-eqz p1, :cond_0

    .line 833
    const-string v3, "com.google"

    iget-object v4, p1, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 834
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Invalid account type: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p1, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 838
    :cond_0
    invoke-virtual {p0, p2}, Lcom/google/android/music/preferences/MusicPreferences;->setIsValidAccount(Z)V

    .line 841
    const-string v3, "SelectedAccount"

    invoke-direct {p0, v3, v2}, Lcom/google/android/music/preferences/MusicPreferences;->getStringPreference(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 843
    .local v1, "oldAccountName":Ljava/lang/String;
    if-nez v1, :cond_3

    move-object v0, v2

    .line 845
    .local v0, "oldAccount":Landroid/accounts/Account;
    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {v0, p1}, Landroid/accounts/Account;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    :cond_1
    if-nez v0, :cond_4

    if-nez p1, :cond_4

    .line 869
    :cond_2
    :goto_1
    return-void

    .line 843
    .end local v0    # "oldAccount":Landroid/accounts/Account;
    :cond_3
    new-instance v0, Landroid/accounts/Account;

    const-string v3, "com.google"

    invoke-direct {v0, v1, v3}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 850
    .restart local v0    # "oldAccount":Landroid/accounts/Account;
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/music/preferences/MusicPreferences;->clearTempNautilusStatus()V

    .line 852
    const/4 v3, 0x1

    invoke-virtual {p0, v3}, Lcom/google/android/music/preferences/MusicPreferences;->setShowSyncNotification(Z)V

    .line 853
    const-string v3, "SelectedAccount"

    if-nez p1, :cond_7

    :goto_2
    invoke-direct {p0, v3, v2}, Lcom/google/android/music/preferences/MusicPreferences;->setPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 856
    invoke-direct {p0}, Lcom/google/android/music/preferences/MusicPreferences;->clearAccountDependentPreferences()V

    .line 858
    if-eqz v0, :cond_5

    .line 859
    const-string v2, "com.google.android.music.MusicContent"

    invoke-static {v0, v2}, Landroid/content/ContentResolver;->cancelSync(Landroid/accounts/Account;Ljava/lang/String;)V

    .line 860
    const-string v2, "com.google.android.music.MusicContent"

    invoke-static {v0, v2, v5}, Landroid/content/ContentResolver;->setSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;Z)V

    .line 861
    const-string v2, "com.google.android.music.MusicContent"

    invoke-static {v0, v2, v5}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    .line 864
    :cond_5
    sget-boolean v2, Lcom/google/android/music/preferences/MusicPreferences;->LOGV:Z

    if-eqz v2, :cond_6

    .line 865
    const-string v3, "MusicPreferences"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "New Active account: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    if-nez p1, :cond_8

    const-string v2, "null"

    :goto_3
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 867
    :cond_6
    invoke-direct {p0, v0, p1}, Lcom/google/android/music/preferences/MusicPreferences;->switchAccountAsync(Landroid/accounts/Account;Landroid/accounts/Account;)V

    .line 868
    invoke-direct {p0}, Lcom/google/android/music/preferences/MusicPreferences;->refreshNetworkUsageComponent()V

    goto :goto_1

    .line 853
    :cond_7
    iget-object v2, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    goto :goto_2

    .line 865
    :cond_8
    iget-object v2, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    goto :goto_3
.end method

.method private switchAccountAsync(Landroid/accounts/Account;Landroid/accounts/Account;)V
    .locals 1
    .param p1, "oldAccount"    # Landroid/accounts/Account;
    .param p2, "newAccount"    # Landroid/accounts/Account;

    .prologue
    .line 911
    new-instance v0, Lcom/google/android/music/preferences/MusicPreferences$3;

    invoke-direct {v0, p0, p2, p1}, Lcom/google/android/music/preferences/MusicPreferences$3;-><init>(Lcom/google/android/music/preferences/MusicPreferences;Landroid/accounts/Account;Landroid/accounts/Account;)V

    invoke-static {v0}, Lcom/google/android/music/utils/MusicUtils;->runAsync(Ljava/lang/Runnable;)V

    .line 940
    return-void
.end method

.method private declared-synchronized unbindFromPreferenceService()V
    .locals 2

    .prologue
    .line 273
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/music/preferences/MusicPreferences;->mPreferenceServiceBound:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 281
    :goto_0
    monitor-exit p0

    return-void

    .line 277
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/music/preferences/MusicPreferences;->mPreferenceServiceSafeConnection:Lcom/google/android/music/utils/SafeServiceConnection;

    iget-object v1, p0, Lcom/google/android/music/preferences/MusicPreferences;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/google/android/music/utils/SafeServiceConnection;->unbindService(Landroid/content/Context;)V

    .line 278
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/preferences/MusicPreferences;->mPreferenceServiceBound:Z

    .line 279
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/preferences/MusicPreferences;->mPreferenceServiceConnected:Z

    .line 280
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 273
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private upgradeLegacyInvalidAccount()Z
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 373
    const/4 v3, 0x0

    .line 374
    .local v3, "success":Z
    const-string v4, "InvalidSelectedAccount"

    invoke-direct {p0, v4, v7}, Lcom/google/android/music/preferences/MusicPreferences;->getStringPreference(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 377
    .local v1, "accountToUpgrade":Ljava/lang/String;
    if-eqz v1, :cond_1

    .line 379
    const-string v4, "MusicPreferences"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Attempting to upgrade legacy account "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 382
    const-string v4, "InvalidSelectedAccount"

    invoke-direct {p0, v4, v7}, Lcom/google/android/music/preferences/MusicPreferences;->setPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 384
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 386
    const-string v4, "MusicPreferences"

    const-string v5, "Skipping upgrade of empty of legacy invalid account"

    invoke-static {v4, v5}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 387
    const/4 v4, 0x0

    .line 411
    :goto_0
    return v4

    .line 391
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/music/preferences/MusicPreferences;->getSyncAccount()Landroid/accounts/Account;

    move-result-object v2

    .line 392
    .local v2, "currentAccount":Landroid/accounts/Account;
    if-eqz v2, :cond_3

    .line 393
    iget-object v4, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 394
    const-string v4, "MusicPreferences"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Skipping upgrade of legacy invalid account "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ". It\'s aleady selected"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .end local v2    # "currentAccount":Landroid/accounts/Account;
    :cond_1
    :goto_1
    move v4, v3

    .line 411
    goto :goto_0

    .line 397
    .restart local v2    # "currentAccount":Landroid/accounts/Account;
    :cond_2
    const-string v4, "MusicPreferences"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Skipping upgrade of legacy invalid account "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ". Another account is aleady selected "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 401
    :cond_3
    invoke-virtual {p0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->resolveAccount(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    .line 402
    .local v0, "account":Landroid/accounts/Account;
    if-nez v0, :cond_4

    .line 403
    const-string v4, "MusicPreferences"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed to resolve legacy invalid account "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 405
    :cond_4
    invoke-virtual {p0, v0}, Lcom/google/android/music/preferences/MusicPreferences;->setInvalidStreamingAccount(Landroid/accounts/Account;)V

    .line 406
    const/4 v3, 0x1

    goto :goto_1
.end method

.method private declared-synchronized waitForServiceConnection()Z
    .locals 2

    .prologue
    .line 307
    monitor-enter p0

    :try_start_0
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 308
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "waitForServiceConnection() was called on the main thread"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 307
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 311
    :cond_0
    :goto_0
    :try_start_1
    iget-boolean v0, p0, Lcom/google/android/music/preferences/MusicPreferences;->mPreferenceServiceBound:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/music/preferences/MusicPreferences;->mPreferenceServiceConnected:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v0, :cond_1

    .line 313
    :try_start_2
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 314
    :catch_0
    move-exception v0

    goto :goto_0

    .line 318
    :cond_1
    :try_start_3
    iget-boolean v0, p0, Lcom/google/android/music/preferences/MusicPreferences;->mPreferenceServiceConnected:Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    monitor-exit p0

    return v0
.end method


# virtual methods
.method public autoSelectStreamingAccount(Landroid/accounts/Account;)Z
    .locals 2
    .param p1, "newAccount"    # Landroid/accounts/Account;

    .prologue
    const/4 v0, 0x0

    .line 791
    invoke-direct {p0}, Lcom/google/android/music/preferences/MusicPreferences;->isConfiguredSyncAccount()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 803
    :cond_0
    :goto_0
    return v0

    .line 796
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/music/preferences/MusicPreferences;->isAccountAutoSelectEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 800
    invoke-virtual {p0, p1}, Lcom/google/android/music/preferences/MusicPreferences;->setStreamingAccount(Landroid/accounts/Account;)V

    .line 801
    const-string v0, "MusicPreferences"

    const-string v1, "Account auto-selected"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 803
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public changeValue(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/Object;

    .prologue
    .line 1632
    if-nez p2, :cond_0

    .line 1633
    iget-object v0, p0, Lcom/google/android/music/preferences/MusicPreferences;->mCachedPreferences:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1634
    invoke-direct {p0, p1}, Lcom/google/android/music/preferences/MusicPreferences;->logValueRemoved(Ljava/lang/String;)V

    .line 1639
    :goto_0
    return-void

    .line 1636
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/preferences/MusicPreferences;->mCachedPreferences:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1637
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/preferences/MusicPreferences;->logValueSet(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public clearSelectedStorageVolume()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1400
    invoke-virtual {p0, v0, v0, v0}, Lcom/google/android/music/preferences/MusicPreferences;->setSelectedStorageVolume(Lcom/google/android/music/download/cache/CacheLocation;Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;Lcom/google/android/music/download/cache/CacheLocationManager$VolumeInformation;)V

    .line 1401
    return-void
.end method

.method public clearSyncForUnselectedAccounts()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 771
    iget-object v6, p0, Lcom/google/android/music/preferences/MusicPreferences;->mContext:Landroid/content/Context;

    invoke-static {v6}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v6

    const-string v7, "com.google"

    invoke-virtual {v6, v7}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v1

    .line 772
    .local v1, "accounts":[Landroid/accounts/Account;
    invoke-virtual {p0}, Lcom/google/android/music/preferences/MusicPreferences;->getSyncAccount()Landroid/accounts/Account;

    move-result-object v5

    .line 773
    .local v5, "selectedAccount":Landroid/accounts/Account;
    move-object v2, v1

    .local v2, "arr$":[Landroid/accounts/Account;
    array-length v4, v2

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v0, v2, v3

    .line 774
    .local v0, "account":Landroid/accounts/Account;
    invoke-virtual {v0, v5}, Landroid/accounts/Account;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 775
    const-string v6, "com.google.android.music.MusicContent"

    invoke-static {v0, v6}, Landroid/content/ContentResolver;->cancelSync(Landroid/accounts/Account;Ljava/lang/String;)V

    .line 776
    const-string v6, "com.google.android.music.MusicContent"

    invoke-static {v0, v6, v8}, Landroid/content/ContentResolver;->setSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;Z)V

    .line 777
    const-string v6, "com.google.android.music.MusicContent"

    invoke-static {v0, v6, v8}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    .line 773
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 780
    .end local v0    # "account":Landroid/accounts/Account;
    :cond_1
    return-void
.end method

.method public clearTempNautilusStatus()V
    .locals 4

    .prologue
    .line 1497
    const-string v0, "TempNautilusStatusTimestamp"

    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->setPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1498
    const-string v0, "TempNautilusStatus"

    const/4 v1, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->setPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1499
    return-void
.end method

.method public disableAutoSelect()V
    .locals 2

    .prologue
    .line 763
    const-string v0, "AccountAutoSelectEnabled"

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->setPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 764
    return-void
.end method

.method public getAllSongsSortOrder()I
    .locals 2

    .prologue
    .line 1028
    const-string v0, "AllSongsSortOrder"

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->getIntPreference(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public getArtistSongsSortOrder()I
    .locals 2

    .prologue
    .line 1036
    const-string v0, "ArtistSongsSortOrder"

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->getIntPreference(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public getAutoCacheDurationInMinutes()I
    .locals 2

    .prologue
    .line 1360
    const-string v0, "AutoCacheDurationInMinutes"

    const/16 v1, 0x12c

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->getIntPreference(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public getAutoCacheMinutesSinceMidnight()I
    .locals 2

    .prologue
    .line 1351
    const-string v0, "AutoCacheMinutesSinceMidnight"

    const/16 v1, 0x3c

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->getIntPreference(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public getClearNautilusContent()Z
    .locals 2

    .prologue
    .line 978
    const-string v0, "clearNautilusContent"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->getBooleanPreference(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getContentFilter()I
    .locals 2

    .prologue
    .line 1365
    const-string v0, "ContentFilter"

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->getIntPreference(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public getDatabaseVersion()I
    .locals 2

    .prologue
    .line 1863
    const-string v0, "databaseVersion"

    const/4 v1, -0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->getIntPreference(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public getDisplayOptions()I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1082
    invoke-virtual {p0}, Lcom/google/android/music/preferences/MusicPreferences;->isOnDeviceMusicCapable()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1086
    :goto_0
    return v0

    .line 1085
    :cond_0
    const-string v1, "DisplayOptions"

    invoke-direct {p0, v1, v0}, Lcom/google/android/music/preferences/MusicPreferences;->getIntPreference(Ljava/lang/String;I)I

    move-result v0

    .line 1086
    .local v0, "displayOptions":I
    goto :goto_0
.end method

.method public getLastTimeSuggestedMixPlayedMillis()J
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 1974
    const-string v2, "lastPlayedSuggestedMix"

    invoke-direct {p0, v2, v4, v5}, Lcom/google/android/music/preferences/MusicPreferences;->getLongPreference(Ljava/lang/String;J)J

    move-result-wide v0

    .line 1976
    .local v0, "result":J
    cmp-long v2, v0, v4

    if-nez v2, :cond_0

    .line 1979
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 1980
    invoke-virtual {p0, v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->setLastTimeSuggestedMixPlayedMillis(J)V

    .line 1983
    :cond_0
    return-wide v0
.end method

.method public getLastTimeTutorialCardDismissed()J
    .locals 4

    .prologue
    .line 1957
    const-string v0, "lastTimeTutorialCardDismissed"

    const-wide/16 v2, 0x0

    invoke-direct {p0, v0, v2, v3}, Lcom/google/android/music/preferences/MusicPreferences;->getLongPreference(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public getLoggingId()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1289
    const-string v1, "StringNonNegativeLoggingId"

    const/4 v2, 0x0

    invoke-direct {p0, v1, v2}, Lcom/google/android/music/preferences/MusicPreferences;->getStringPreference(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1290
    .local v0, "loggingId":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 1291
    new-instance v1, Ljava/security/SecureRandom;

    invoke-direct {v1}, Ljava/security/SecureRandom;-><init>()V

    invoke-virtual {v1}, Ljava/security/SecureRandom;->nextLong()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v0

    .line 1292
    const-string v1, "StringNonNegativeLoggingId"

    invoke-direct {p0, v1, v0}, Lcom/google/android/music/preferences/MusicPreferences;->setPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1294
    :cond_0
    return-object v0
.end method

.method public getNautilusStatus()Lcom/google/android/music/NautilusStatus;
    .locals 12

    .prologue
    const/4 v7, -0x1

    const-wide/16 v10, 0x0

    .line 1451
    const-string v6, "TempNautilusStatusTimestamp"

    invoke-direct {p0, v6, v10, v11}, Lcom/google/android/music/preferences/MusicPreferences;->getLongPreference(Ljava/lang/String;J)J

    move-result-wide v4

    .line 1452
    .local v4, "tempTimeStamp":J
    cmp-long v6, v4, v10

    if-lez v6, :cond_1

    .line 1453
    const-string v6, "TempNautilusStatus"

    invoke-direct {p0, v6, v7}, Lcom/google/android/music/preferences/MusicPreferences;->getIntPreference(Ljava/lang/String;I)I

    move-result v2

    .line 1454
    .local v2, "ordinal":I
    if-le v2, v7, :cond_1

    .line 1455
    invoke-static {}, Lcom/google/android/music/NautilusStatus;->values()[Lcom/google/android/music/NautilusStatus;

    move-result-object v3

    .line 1456
    .local v3, "values":[Lcom/google/android/music/NautilusStatus;
    array-length v6, v3

    if-ge v2, v6, :cond_1

    .line 1458
    iget-object v6, p0, Lcom/google/android/music/preferences/MusicPreferences;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "music_temp_nautilus_status_exp_time_in_seconds"

    const-wide/32 v8, 0x15180

    invoke-static {v6, v7, v8, v9}, Lcom/google/android/gsf/Gservices;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v0

    .line 1462
    .local v0, "expireTimeInSeconds":J
    cmp-long v6, v0, v10

    if-lez v6, :cond_0

    const-wide/16 v6, 0x3e8

    mul-long/2addr v6, v0

    add-long/2addr v6, v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    cmp-long v6, v6, v8

    if-lez v6, :cond_0

    .line 1465
    aget-object v6, v3, v2

    .line 1474
    .end local v0    # "expireTimeInSeconds":J
    .end local v2    # "ordinal":I
    .end local v3    # "values":[Lcom/google/android/music/NautilusStatus;
    :goto_0
    return-object v6

    .line 1468
    .restart local v0    # "expireTimeInSeconds":J
    .restart local v2    # "ordinal":I
    .restart local v3    # "values":[Lcom/google/android/music/NautilusStatus;
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/music/preferences/MusicPreferences;->clearTempNautilusStatus()V

    .line 1474
    .end local v0    # "expireTimeInSeconds":J
    .end local v2    # "ordinal":I
    .end local v3    # "values":[Lcom/google/android/music/NautilusStatus;
    :cond_1
    invoke-static {}, Lcom/google/android/music/utils/ConfigUtils;->getNautilusStatus()Lcom/google/android/music/NautilusStatus;

    move-result-object v6

    goto :goto_0
.end method

.method public getRecentlyAddedSongsSortOrder()I
    .locals 2

    .prologue
    .line 1052
    const-string v0, "RecentlyAddedSongsSortOrder"

    const/4 v1, 0x4

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->getIntPreference(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public getResetSyncState()Z
    .locals 2

    .prologue
    .line 986
    const-string v0, "resetSyncState"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->getBooleanPreference(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getSelectedStorageVolumeCapacity()J
    .locals 4

    .prologue
    .line 1442
    const-string v0, "selectedStorageVolumeCapacity"

    const-wide/16 v2, 0x0

    invoke-direct {p0, v0, v2, v3}, Lcom/google/android/music/preferences/MusicPreferences;->getLongPreference(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public getSelectedStorageVolumeId()Ljava/util/UUID;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1386
    const-string v3, "selectedStorageVolumeId"

    invoke-direct {p0, v3, v2}, Lcom/google/android/music/preferences/MusicPreferences;->getStringPreference(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1387
    .local v1, "s":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 1392
    :goto_0
    return-object v2

    .line 1389
    :cond_0
    :try_start_0
    invoke-static {v1}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 1390
    .local v2, "uuid":Ljava/util/UUID;
    goto :goto_0

    .line 1391
    .end local v2    # "uuid":Ljava/util/UUID;
    :catch_0
    move-exception v0

    .line 1392
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    goto :goto_0
.end method

.method public getSeletectedAccountForDisplay()Ljava/lang/String;
    .locals 3

    .prologue
    .line 899
    invoke-virtual {p0}, Lcom/google/android/music/preferences/MusicPreferences;->getSyncAccount()Landroid/accounts/Account;

    move-result-object v0

    .line 900
    .local v0, "account":Landroid/accounts/Account;
    if-eqz v0, :cond_0

    .line 901
    iget-object v1, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 905
    :goto_0
    return-object v1

    :cond_0
    const-string v1, "InvalidSelectedAccount"

    const/4 v2, 0x0

    invoke-direct {p0, v1, v2}, Lcom/google/android/music/preferences/MusicPreferences;->getStringPreference(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getShowSongzaWelcomeCard()Z
    .locals 2

    .prologue
    .line 1121
    const-string v0, "showSongzaWelcomeCard"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->getBooleanPreference(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getShowSyncNotification()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1268
    iget-object v1, p0, Lcom/google/android/music/preferences/MusicPreferences;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "music_enable_sync_notifications"

    invoke-static {v1, v2, v0}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1270
    const-string v0, "showSyncNotification"

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->getBooleanPreference(Ljava/lang/String;Z)Z

    move-result v0

    .line 1272
    :cond_0
    return v0
.end method

.method public getStoreAvailableLastChecked()J
    .locals 4

    .prologue
    .line 994
    const-string v0, "storeAvailableLastChecked"

    const-wide/16 v2, 0x0

    invoke-direct {p0, v0, v2, v3}, Lcom/google/android/music/preferences/MusicPreferences;->getLongPreference(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public getStoreSongsSortOrder()I
    .locals 2

    .prologue
    .line 1060
    const-string v0, "StoreSongsSortOrder"

    const/4 v1, 0x4

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->getIntPreference(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public getStreamQuality()I
    .locals 2

    .prologue
    .line 1183
    invoke-virtual {p0}, Lcom/google/android/music/preferences/MusicPreferences;->isTvMusic()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1184
    const/4 v0, 0x2

    .line 1186
    :goto_0
    return v0

    :cond_0
    const-string v0, "StreamQuality"

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->getIntPreference(Ljava/lang/String;I)I

    move-result v0

    goto :goto_0
.end method

.method public getStreamingAccount()Landroid/accounts/Account;
    .locals 1

    .prologue
    .line 696
    invoke-virtual {p0}, Lcom/google/android/music/preferences/MusicPreferences;->isValidAccount()Z

    move-result v0

    if-nez v0, :cond_0

    .line 697
    const/4 v0, 0x0

    .line 699
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/music/preferences/MusicPreferences;->getSyncAccount()Landroid/accounts/Account;

    move-result-object v0

    goto :goto_0
.end method

.method public getSyncAccount()Landroid/accounts/Account;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 720
    invoke-direct {p0}, Lcom/google/android/music/preferences/MusicPreferences;->isConfiguredSyncAccount()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 721
    const-string v1, "SelectedAccount"

    invoke-direct {p0, v1, v0}, Lcom/google/android/music/preferences/MusicPreferences;->getStringPreference(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/music/preferences/MusicPreferences;->resolveAccount(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    .line 723
    :cond_0
    return-object v0
.end method

.method public getThumbsUpSongsSortOrder()I
    .locals 2

    .prologue
    .line 1068
    const-string v0, "ThumbsUpSongsSortOrder"

    const/4 v1, 0x5

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->getIntPreference(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public getUpgradeSyncDone()Z
    .locals 2

    .prologue
    .line 1903
    const-string v0, "upgradeSyncDone"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->getBooleanPreference(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public hasStreamingAccount()Z
    .locals 1

    .prologue
    .line 707
    invoke-virtual {p0}, Lcom/google/android/music/preferences/MusicPreferences;->getStreamingAccount()Landroid/accounts/Account;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isAccountAutoSelectEnabled()Z
    .locals 2

    .prologue
    .line 767
    const-string v0, "AccountAutoSelectEnabled"

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->getBooleanPreference(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public isAutoCachingAvailable()Z
    .locals 3

    .prologue
    .line 1301
    iget-object v0, p0, Lcom/google/android/music/preferences/MusicPreferences;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "music_enable_autocache"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public isAutoCachingEnabled()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1310
    invoke-virtual {p0}, Lcom/google/android/music/preferences/MusicPreferences;->isAutoCachingAvailable()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1326
    :cond_0
    :goto_0
    return v0

    .line 1314
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/music/preferences/MusicPreferences;->isCachedStreamingMusicEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1319
    iget-object v1, p0, Lcom/google/android/music/preferences/MusicPreferences;->mCachedPreferences:Ljava/util/Map;

    const-string v2, "AutoCacheOn"

    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1320
    iget-object v0, p0, Lcom/google/android/music/preferences/MusicPreferences;->mCachedPreferences:Ljava/util/Map;

    const-string v1, "AutoCacheOn"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0

    .line 1321
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/music/preferences/MusicPreferences;->isNautilusEnabled()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1322
    iget-object v0, p0, Lcom/google/android/music/preferences/MusicPreferences;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "music_autocache_opt_in_default_nautilus"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0

    .line 1326
    :cond_3
    iget-object v1, p0, Lcom/google/android/music/preferences/MusicPreferences;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "music_autocache_opt_in_default"

    invoke-static {v1, v2, v0}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0
.end method

.method public isCachedStreamingMusicEnabled()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1011
    invoke-virtual {p0}, Lcom/google/android/music/preferences/MusicPreferences;->isCachingFeatureAvailable()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "CacheStreamed"

    invoke-direct {p0, v1, v0}, Lcom/google/android/music/preferences/MusicPreferences;->getBooleanPreference(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isCachingFeatureAvailable()Z
    .locals 1

    .prologue
    .line 550
    invoke-virtual {p0}, Lcom/google/android/music/preferences/MusicPreferences;->isTvMusic()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isDialMediaRouteSupportEnabled()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1569
    iget-object v2, p0, Lcom/google/android/music/preferences/MusicPreferences;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "music_enable_dial_cloud_queue"

    invoke-static {v2, v3, v1}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    .line 1573
    .local v0, "isDialMrpEnabled":Z
    iget-boolean v2, p0, Lcom/google/android/music/preferences/MusicPreferences;->mIsTv:Z

    if-nez v2, :cond_0

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method public isDownloadedOnlyMode()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1076
    invoke-virtual {p0}, Lcom/google/android/music/preferences/MusicPreferences;->getDisplayOptions()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isFindVideoEnabled()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1341
    invoke-virtual {p0}, Lcom/google/android/music/preferences/MusicPreferences;->isNautilusEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/music/preferences/MusicPreferences;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "music_enable_find_video"

    invoke-static {v1, v2, v0}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public isHighStreamQuality()Z
    .locals 2

    .prologue
    .line 1179
    invoke-virtual {p0}, Lcom/google/android/music/preferences/MusicPreferences;->getStreamQuality()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isKeepOnDownloadingPaused()Z
    .locals 2

    .prologue
    .line 1112
    const-string v0, "isKeepOnDownloadPaused"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->getBooleanPreference(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public isLargeScreen()Z
    .locals 1

    .prologue
    .line 494
    iget-boolean v0, p0, Lcom/google/android/music/preferences/MusicPreferences;->mIsLargeScreen:Z

    return v0
.end method

.method public isLogFilesEnabled()Z
    .locals 2

    .prologue
    .line 1020
    const-string v0, "LogFilesEnabled"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->getBooleanPreference(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public isLowStreamQuality()Z
    .locals 1

    .prologue
    .line 1171
    invoke-virtual {p0}, Lcom/google/android/music/preferences/MusicPreferences;->getStreamQuality()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isMediaRouteSupportEnabled()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 1557
    iget-object v2, p0, Lcom/google/android/music/preferences/MusicPreferences;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "music_enable_cast_remote_playback"

    invoke-static {v2, v3, v1}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    .line 1561
    .local v0, "isCastEnabled":Z
    iget-boolean v2, p0, Lcom/google/android/music/preferences/MusicPreferences;->mIsTv:Z

    if-nez v2, :cond_0

    if-eqz v0, :cond_0

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isNautilusEnabled()Z
    .locals 1

    .prologue
    .line 1446
    invoke-virtual {p0}, Lcom/google/android/music/preferences/MusicPreferences;->getNautilusStatus()Lcom/google/android/music/NautilusStatus;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/NautilusStatus;->isNautilusEnabled()Z

    move-result v0

    return v0
.end method

.method public isNautilusStatusStale()Z
    .locals 14

    .prologue
    const-wide/32 v12, 0x5265c00

    const-wide/16 v10, 0x0

    const/4 v8, 0x0

    .line 1521
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 1522
    .local v6, "now":J
    const-string v9, "NautilusTimestampMillisec"

    invoke-direct {p0, v9, v10, v11}, Lcom/google/android/music/preferences/MusicPreferences;->getLongPreference(Ljava/lang/String;J)J

    move-result-wide v4

    .line 1523
    .local v4, "nautilusTimestamp":J
    cmp-long v9, v4, v10

    if-eqz v9, :cond_0

    add-long v10, v6, v12

    cmp-long v9, v4, v10

    if-lez v9, :cond_2

    .line 1525
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/music/preferences/MusicPreferences;->updateNautilusTimestamp()V

    .line 1538
    :cond_1
    :goto_0
    return v8

    .line 1528
    :cond_2
    iget-object v9, p0, Lcom/google/android/music/preferences/MusicPreferences;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    const-string v10, "music_nautlus_offline_status_ttl_days"

    const/16 v11, 0x1e

    invoke-static {v9, v10, v11}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v9

    int-to-long v0, v9

    .line 1531
    .local v0, "daysTtl":J
    mul-long v2, v12, v0

    .line 1532
    .local v2, "millisecTtl":J
    add-long v10, v4, v2

    cmp-long v9, v6, v10

    if-ltz v9, :cond_1

    .line 1535
    const-string v8, "MusicPreferences"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Stale nautilus state detected. Last updated: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    new-instance v10, Ljava/util/Date;

    invoke-direct {v10, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ". Exp: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " days."

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1538
    const/4 v8, 0x1

    goto :goto_0
.end method

.method public isNormalStreamQuality()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1175
    invoke-virtual {p0}, Lcom/google/android/music/preferences/MusicPreferences;->getStreamQuality()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isOfflineDLOnlyOnWifi()Z
    .locals 2

    .prologue
    .line 1197
    const-string v0, "OfflineDLOnlyOnWifi"

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->getBooleanPreference(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public isOfflineFeatureAvailable()Z
    .locals 1

    .prologue
    .line 543
    invoke-virtual {p0}, Lcom/google/android/music/preferences/MusicPreferences;->hasStreamingAccount()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/music/preferences/MusicPreferences;->isTvMusic()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isOnDeviceMusicCapable()Z
    .locals 1

    .prologue
    .line 1150
    invoke-virtual {p0}, Lcom/google/android/music/preferences/MusicPreferences;->isTvMusic()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPlayCacheCleaned()Z
    .locals 2

    .prologue
    .line 1544
    const-string v0, "playCacheCleaned"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->getBooleanPreference(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public isRadioAnimationEnabled()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 1373
    invoke-static {}, Lcom/google/android/music/preferences/MusicPreferences;->isJellyBeanOrGreater()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/music/preferences/MusicPreferences;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "music_show_radio_animation"

    invoke-static {v1, v2, v0}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isStoreAvailablilityVerified()Z
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 969
    const-string v0, "storeAvailable"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->getBooleanPreference(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public isStreamOnlyOnWifi()Z
    .locals 2

    .prologue
    .line 1154
    const-string v0, "StreamOnlyOnWifi"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->getBooleanPreference(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public isTabletMusicExperience()Z
    .locals 1

    .prologue
    .line 503
    invoke-virtual {p0}, Lcom/google/android/music/preferences/MusicPreferences;->isLargeScreen()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/music/preferences/MusicPreferences;->isXLargeScreen()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/music/preferences/MusicPreferences;->isTvMusic()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isTvMusic()Z
    .locals 1

    .prologue
    .line 529
    iget-boolean v0, p0, Lcom/google/android/music/preferences/MusicPreferences;->mIsTv:Z

    return v0
.end method

.method public isValidAccount()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 886
    const-string v1, "isInvalidAccount"

    invoke-direct {p0, v1, v0}, Lcom/google/android/music/preferences/MusicPreferences;->getBooleanPreference(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public isWearSyncAvailable()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 574
    iget-object v2, p0, Lcom/google/android/music/preferences/MusicPreferences;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "music_wear_sync_enabled"

    const/4 v4, 0x1

    invoke-static {v2, v3, v4}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    .line 577
    .local v0, "GServiceEnabled":Z
    if-nez v0, :cond_0

    .line 581
    :goto_0
    return v1

    :cond_0
    const-string v2, "wearSyncAvailable"

    invoke-direct {p0, v2, v1}, Lcom/google/android/music/preferences/MusicPreferences;->getBooleanPreference(Ljava/lang/String;Z)Z

    move-result v1

    goto :goto_0
.end method

.method public isWearSyncEnabled()Z
    .locals 2

    .prologue
    .line 599
    const-string v0, "WearSyncEnabled"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->getBooleanPreference(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public isXLargeScreen()Z
    .locals 1

    .prologue
    .line 490
    iget-boolean v0, p0, Lcom/google/android/music/preferences/MusicPreferences;->mIsXLargeScreen:Z

    return v0
.end method

.method public isYouTubeAvailable(Landroid/app/Activity;)Z
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 1580
    invoke-virtual {p0}, Lcom/google/android/music/preferences/MusicPreferences;->isNautilusEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/android/music/utils/ConfigUtils;->isVsAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/music/preferences/MusicPreferences;->isDownloadedOnlyMode()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1}, Lcom/google/android/music/youtube/YouTubeUtils;->canStartVideo(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public maybeEnableUpgradeSync()V
    .locals 6

    .prologue
    .line 1878
    :try_start_0
    iget-object v3, p0, Lcom/google/android/music/preferences/MusicPreferences;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/music/preferences/MusicPreferences;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v3

    iget v0, v3, Landroid/content/pm/PackageInfo;->versionCode:I

    .line 1880
    .local v0, "currentVersion":I
    iget-object v3, p0, Lcom/google/android/music/preferences/MusicPreferences;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "music_min_version_upgrade_sync"

    const/16 v5, 0x4b5

    invoke-static {v3, v4, v5}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    .line 1884
    .local v2, "minVersion":I
    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    if-lt v0, v2, :cond_0

    invoke-direct {p0}, Lcom/google/android/music/preferences/MusicPreferences;->getLastUpgradeSyncVersion()I

    move-result v3

    if-ge v3, v2, :cond_0

    .line 1887
    const/4 v3, 0x0

    invoke-virtual {p0, v3}, Lcom/google/android/music/preferences/MusicPreferences;->setUpgradeSyncDone(Z)V

    .line 1894
    .end local v0    # "currentVersion":I
    .end local v2    # "minVersion":I
    :goto_0
    return-void

    .line 1889
    .restart local v0    # "currentVersion":I
    .restart local v2    # "minVersion":I
    :cond_0
    const/4 v3, 0x1

    invoke-virtual {p0, v3}, Lcom/google/android/music/preferences/MusicPreferences;->setUpgradeSyncDone(Z)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1891
    .end local v0    # "currentVersion":I
    .end local v2    # "minVersion":I
    :catch_0
    move-exception v1

    .line 1892
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v3, "MusicPreferences"

    const-string v4, "Couldn\'t get current version code."

    invoke-static {v3, v4, v1}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V
    .locals 2
    .param p1, "l"    # Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    .prologue
    .line 1700
    iget-object v1, p0, Lcom/google/android/music/preferences/MusicPreferences;->mPreferenceChangeListeners:Ljava/util/List;

    monitor-enter v1

    .line 1701
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/preferences/MusicPreferences;->mPreferenceChangeListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1702
    monitor-exit v1

    .line 1703
    return-void

    .line 1702
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public resolveAccount(Ljava/lang/String;)Landroid/accounts/Account;
    .locals 5
    .param p1, "username"    # Ljava/lang/String;

    .prologue
    .line 730
    if-nez p1, :cond_1

    .line 731
    const/4 v2, 0x0

    .line 742
    :cond_0
    :goto_0
    return-object v2

    .line 733
    :cond_1
    iget-object v4, p0, Lcom/google/android/music/preferences/MusicPreferences;->mContext:Landroid/content/Context;

    invoke-static {v4}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    .line 734
    .local v0, "accountManager":Landroid/accounts/AccountManager;
    const-string v4, "com.google"

    invoke-virtual {v0, v4}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v3

    .line 735
    .local v3, "tmpaccounts":[Landroid/accounts/Account;
    const/4 v2, 0x0

    .line 736
    .local v2, "selectedAccount":Landroid/accounts/Account;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    array-length v4, v3

    if-ge v1, v4, :cond_0

    .line 737
    aget-object v4, v3, v1

    iget-object v4, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 738
    aget-object v2, v3, v1

    .line 739
    goto :goto_0

    .line 736
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public declared-synchronized runWithPreferenceService(Ljava/lang/Runnable;)V
    .locals 1
    .param p1, "task"    # Ljava/lang/Runnable;

    .prologue
    .line 1839
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/music/preferences/MusicPreferences;->hasPreferenceService()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1840
    iget-object v0, p0, Lcom/google/android/music/preferences/MusicPreferences;->mRunOnceServiceConnected:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1844
    :goto_0
    monitor-exit p0

    return-void

    .line 1842
    :cond_0
    :try_start_1
    sget-object v0, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    invoke-static {v0, p1}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsync(Lcom/google/android/music/utils/LoggableHandler;Ljava/lang/Runnable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1839
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setAllSongsSortOrder(I)V
    .locals 2
    .param p1, "order"    # I

    .prologue
    .line 1032
    const-string v0, "AllSongsSortOrder"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->setPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1033
    return-void
.end method

.method public setArtistSongsSortOrder(I)V
    .locals 2
    .param p1, "order"    # I

    .prologue
    .line 1040
    const-string v0, "ArtistSongsSortOrder"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->setPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1041
    return-void
.end method

.method public setAutoCachingEnabled(Z)V
    .locals 2
    .param p1, "enabled"    # Z

    .prologue
    .line 1333
    const-string v0, "AutoCacheOn"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->setPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1334
    return-void
.end method

.method public setCachedStreamingMusicEnabled(Z)V
    .locals 2
    .param p1, "cache"    # Z

    .prologue
    .line 1016
    const-string v0, "CacheStreamed"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->setPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1017
    return-void
.end method

.method public setClearNautilusContent(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 982
    const-string v0, "clearNautilusContent"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->setPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 983
    return-void
.end method

.method public setContentFilter(I)V
    .locals 2
    .param p1, "contentFilter"    # I

    .prologue
    .line 1369
    const-string v0, "ContentFilter"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->setPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1370
    return-void
.end method

.method public setDatabaseVersion(I)V
    .locals 2
    .param p1, "version"    # I

    .prologue
    .line 1867
    const-string v0, "databaseVersion"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->setPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1868
    return-void
.end method

.method public setDisplayOptions(I)V
    .locals 3
    .param p1, "choice"    # I

    .prologue
    .line 1093
    invoke-virtual {p0}, Lcom/google/android/music/preferences/MusicPreferences;->isOnDeviceMusicCapable()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1109
    :goto_0
    return-void

    .line 1098
    :cond_0
    const-string v0, "DisplayOptions"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Lcom/google/android/music/preferences/MusicPreferences$4;

    invoke-direct {v2, p0, p1}, Lcom/google/android/music/preferences/MusicPreferences$4;-><init>(Lcom/google/android/music/preferences/MusicPreferences;I)V

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/music/preferences/MusicPreferences;->setPreferenceWithExtraTask(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public setGenreSongsSortOrder(I)V
    .locals 2
    .param p1, "order"    # I

    .prologue
    .line 1048
    const-string v0, "GenreSongsSortOrder"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->setPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1049
    return-void
.end method

.method public setInvalidStreamingAccount(Landroid/accounts/Account;)V
    .locals 1
    .param p1, "newAccount"    # Landroid/accounts/Account;

    .prologue
    .line 824
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/music/preferences/MusicPreferences;->setStreamingAccount(Landroid/accounts/Account;Z)V

    .line 825
    return-void
.end method

.method public setIsValidAccount(Z)V
    .locals 2
    .param p1, "isValid"    # Z

    .prologue
    .line 891
    const-string v1, "isInvalidAccount"

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-direct {p0, v1, v0}, Lcom/google/android/music/preferences/MusicPreferences;->setPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 892
    return-void

    .line 891
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setKeepOnDownloadPaused(Z)V
    .locals 4
    .param p1, "isPaused"    # Z

    .prologue
    .line 1116
    const-string v0, "isKeepOnDownloadPaused"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->setPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1117
    iget-object v0, p0, Lcom/google/android/music/preferences/MusicPreferences;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/music/store/MusicContent;->DOWNLOAD_QUEUE_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    .line 1118
    return-void
.end method

.method public setLastTimeSuggestedMixPlayedMillis(J)V
    .locals 3
    .param p1, "timeMillis"    # J

    .prologue
    .line 1966
    const-string v0, "lastPlayedSuggestedMix"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->setPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1967
    return-void
.end method

.method public setLastTimeTutorialCardDismissed()V
    .locals 4

    .prologue
    .line 1950
    const-string v0, "lastTimeTutorialCardDismissed"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->setPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1951
    return-void
.end method

.method public setLastUpgradeSyncVersion()V
    .locals 5

    .prologue
    .line 1920
    :try_start_0
    iget-object v2, p0, Lcom/google/android/music/preferences/MusicPreferences;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/music/preferences/MusicPreferences;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    iget v0, v2, Landroid/content/pm/PackageInfo;->versionCode:I

    .line 1922
    .local v0, "currentVersion":I
    const-string v2, "lastUpgradeSyncVersion"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {p0, v2, v3}, Lcom/google/android/music/preferences/MusicPreferences;->setPreference(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1926
    .end local v0    # "currentVersion":I
    :goto_0
    return-void

    .line 1923
    :catch_0
    move-exception v1

    .line 1924
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v2, "MusicPreferences"

    const-string v3, "Couldn\'t get current version code."

    invoke-static {v2, v3, v1}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public setLogFilesEnable(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 1024
    const-string v0, "LogFilesEnabled"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->setPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1025
    return-void
.end method

.method public setOffineDLOnlyOnWifi(Z)V
    .locals 4
    .param p1, "value"    # Z

    .prologue
    .line 1201
    const-string v0, "OfflineDLOnlyOnWifi"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->setPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1202
    iget-object v0, p0, Lcom/google/android/music/preferences/MusicPreferences;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/music/store/MusicContent;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    .line 1203
    return-void
.end method

.method public setPlayCacheCleaned()V
    .locals 2

    .prologue
    .line 1548
    const-string v0, "MusicPreferences"

    const-string v1, "Setting play cache cleaned "

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1550
    const-string v0, "playCacheCleaned"

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->setPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1551
    return-void
.end method

.method public setPrioritizeRecommendationsOnMainstage(Z)V
    .locals 2
    .param p1, "prioritize"    # Z

    .prologue
    .line 1934
    const-string v0, "prioritizeRecommendations"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->setPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1935
    return-void
.end method

.method public setRecentlyAddedSongsSortOrder(I)V
    .locals 2
    .param p1, "order"    # I

    .prologue
    .line 1056
    const-string v0, "RecentlyAddedSongsSortOrder"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->setPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1057
    return-void
.end method

.method public setResetSyncState(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 990
    const-string v0, "resetSyncState"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->setPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 991
    return-void
.end method

.method public setSelectedStorageVolume(Lcom/google/android/music/download/cache/CacheLocation;Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;Lcom/google/android/music/download/cache/CacheLocationManager$VolumeInformation;)V
    .locals 7
    .param p1, "location"    # Lcom/google/android/music/download/cache/CacheLocation;
    .param p2, "metadata"    # Lcom/google/android/music/download/cache/CacheLocationManager$StorageMetadata;
    .param p3, "information"    # Lcom/google/android/music/download/cache/CacheLocationManager$VolumeInformation;

    .prologue
    const-wide/16 v0, -0x1

    const/4 v6, 0x0

    .line 1413
    if-nez p1, :cond_1

    .line 1414
    const-string v4, "selectedStorageVolumeId"

    invoke-direct {p0, v4, v6}, Lcom/google/android/music/preferences/MusicPreferences;->setPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1415
    const-string v4, "selectedStorageVolumeCapacity"

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-direct {p0, v4, v5}, Lcom/google/android/music/preferences/MusicPreferences;->setPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1416
    iget-object v4, p0, Lcom/google/android/music/preferences/MusicPreferences;->mContext:Landroid/content/Context;

    invoke-static {v4, v6}, Lcom/google/android/music/download/cache/StorageMigrationService;->startMigration(Landroid/content/Context;Ljava/util/UUID;)V

    .line 1435
    :cond_0
    :goto_0
    return-void

    .line 1420
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/music/download/cache/CacheLocation;->getVolumeId()Ljava/util/UUID;

    move-result-object v2

    .line 1422
    .local v2, "id":Ljava/util/UUID;
    invoke-virtual {p0}, Lcom/google/android/music/preferences/MusicPreferences;->getSelectedStorageVolumeId()Ljava/util/UUID;

    move-result-object v3

    .line 1423
    .local v3, "previous":Ljava/util/UUID;
    invoke-virtual {v2, v3}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1426
    if-eqz p3, :cond_2

    invoke-virtual {p3}, Lcom/google/android/music/download/cache/CacheLocationManager$VolumeInformation;->getTotalSpace()J

    move-result-wide v0

    .line 1427
    .local v0, "capacity":J
    :cond_2
    if-nez v2, :cond_3

    .line 1428
    const-string v4, "selectedStorageVolumeId"

    invoke-direct {p0, v4, v6}, Lcom/google/android/music/preferences/MusicPreferences;->setPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1429
    const-string v4, "selectedStorageVolumeCapacity"

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-direct {p0, v4, v5}, Lcom/google/android/music/preferences/MusicPreferences;->setPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1434
    :goto_1
    iget-object v4, p0, Lcom/google/android/music/preferences/MusicPreferences;->mContext:Landroid/content/Context;

    invoke-static {v4, v2}, Lcom/google/android/music/download/cache/StorageMigrationService;->startMigration(Landroid/content/Context;Ljava/util/UUID;)V

    goto :goto_0

    .line 1431
    :cond_3
    const-string v4, "selectedStorageVolumeId"

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v4, v5}, Lcom/google/android/music/preferences/MusicPreferences;->setPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1432
    const-string v4, "selectedStorageVolumeCapacity"

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-direct {p0, v4, v5}, Lcom/google/android/music/preferences/MusicPreferences;->setPreference(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public setShouldShowTutorialCard(Ljava/lang/String;Z)V
    .locals 2
    .param p1, "tutorialKey"    # Ljava/lang/String;
    .param p2, "show"    # Z

    .prologue
    .line 1261
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ShowTutorialCard"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->setPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1262
    return-void
.end method

.method public setShowSongzaWelcomeCard(Z)V
    .locals 2
    .param p1, "show"    # Z

    .prologue
    .line 1125
    const-string v0, "showSongzaWelcomeCard"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->setPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1126
    return-void
.end method

.method public setShowSyncNotification(Z)V
    .locals 2
    .param p1, "show"    # Z

    .prologue
    .line 1281
    const-string v0, "showSyncNotification"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->setPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1282
    return-void
.end method

.method public setShowUnpinDialog(Z)V
    .locals 2
    .param p1, "showUnpinDialog"    # Z

    .prologue
    .line 1140
    const-string v0, "showUnpinDialog"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->setPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1141
    return-void
.end method

.method public setSidePannelWasClosedOnce(Z)V
    .locals 2
    .param p1, "wasClosed"    # Z

    .prologue
    .line 1245
    const-string v0, "SidePannelWasClosedOnce2"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->setPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1246
    return-void
.end method

.method public setStoreAvailabilityVerified(Z)V
    .locals 3
    .param p1, "value"    # Z

    .prologue
    .line 973
    const-string v0, "MusicPreferences"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Setting store availability: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 974
    const-string v0, "storeAvailable"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->setPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 975
    return-void
.end method

.method public setStoreAvailableLastChecked(J)V
    .locals 3
    .param p1, "value"    # J

    .prologue
    .line 998
    const-string v0, "storeAvailableLastChecked"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->setPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 999
    return-void
.end method

.method public setStoreSongsSortOrder(I)V
    .locals 2
    .param p1, "order"    # I

    .prologue
    .line 1064
    const-string v0, "StoreSongsSortOrder"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->setPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1065
    return-void
.end method

.method public setStreamOnlyOnWifi(Z)V
    .locals 3
    .param p1, "streamOnlyOnWifi"    # Z

    .prologue
    .line 1158
    const-string v0, "StreamOnlyOnWifi"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    new-instance v2, Lcom/google/android/music/preferences/MusicPreferences$5;

    invoke-direct {v2, p0, p1}, Lcom/google/android/music/preferences/MusicPreferences$5;-><init>(Lcom/google/android/music/preferences/MusicPreferences;Z)V

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/music/preferences/MusicPreferences;->setPreferenceWithExtraTask(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Runnable;)V

    .line 1168
    return-void
.end method

.method public setStreamQuality(I)V
    .locals 4
    .param p1, "quality"    # I

    .prologue
    .line 1191
    const-string v0, "StreamQuality"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->setPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1192
    iget-object v0, p0, Lcom/google/android/music/preferences/MusicPreferences;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/music/store/MusicContent;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    .line 1193
    return-void
.end method

.method public setStreamingAccount(Landroid/accounts/Account;)V
    .locals 1
    .param p1, "newAccount"    # Landroid/accounts/Account;

    .prologue
    .line 814
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/music/preferences/MusicPreferences;->setStreamingAccount(Landroid/accounts/Account;Z)V

    .line 815
    return-void
.end method

.method public setTempNautilusStatus(Lcom/google/android/music/NautilusStatus;)V
    .locals 4
    .param p1, "status"    # Lcom/google/android/music/NautilusStatus;

    .prologue
    .line 1487
    const-string v0, "TempNautilusStatus"

    invoke-virtual {p1}, Lcom/google/android/music/NautilusStatus;->ordinal()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->setPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1488
    const-string v0, "TempNautilusStatusTimestamp"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->setPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1489
    invoke-virtual {p0}, Lcom/google/android/music/preferences/MusicPreferences;->updateNautilusTimestamp()V

    .line 1490
    const-string v0, "MusicPreferences"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Set temporary nautilus status to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/music/NautilusStatus;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1491
    return-void
.end method

.method public setThumbsUpSongsSortOrder(I)V
    .locals 2
    .param p1, "order"    # I

    .prologue
    .line 1072
    const-string v0, "ThumbsUpSongsSortOrder"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->setPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1073
    return-void
.end method

.method public setTutorialViewed(Z)V
    .locals 2
    .param p1, "viewed"    # Z

    .prologue
    .line 1237
    const-string v0, "TutorialViewed2"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->setPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1238
    return-void
.end method

.method public setUpgradeSyncDone(Z)V
    .locals 2
    .param p1, "syncDone"    # Z

    .prologue
    .line 1911
    const-string v0, "upgradeSyncDone"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->setPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1912
    return-void
.end method

.method public setWearSyncAvailable(Z)V
    .locals 2
    .param p1, "available"    # Z

    .prologue
    .line 592
    const-string v0, "wearSyncAvailable"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->setPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 593
    return-void
.end method

.method public setWearSyncEnabled(Z)V
    .locals 3
    .param p1, "value"    # Z

    .prologue
    .line 1209
    const-string v0, "WearSyncEnabled"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    new-instance v2, Lcom/google/android/music/preferences/MusicPreferences$6;

    invoke-direct {v2, p0}, Lcom/google/android/music/preferences/MusicPreferences$6;-><init>(Lcom/google/android/music/preferences/MusicPreferences;)V

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/music/preferences/MusicPreferences;->setPreferenceWithExtraTask(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Runnable;)V

    .line 1217
    return-void
.end method

.method public shouldShowTutorialCard(Ljava/lang/String;)Z
    .locals 2
    .param p1, "tutorialKey"    # Ljava/lang/String;

    .prologue
    .line 1253
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ShowTutorialCard"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->getBooleanPreference(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public shouldShowUnpinDialog()Z
    .locals 2

    .prologue
    .line 1132
    const-string v0, "showUnpinDialog"

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->getBooleanPreference(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public showStartRadioButtonsInActionBar()Z
    .locals 1

    .prologue
    .line 511
    invoke-virtual {p0}, Lcom/google/android/music/preferences/MusicPreferences;->isTabletMusicExperience()Z

    move-result v0

    return v0
.end method

.method public unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V
    .locals 2
    .param p1, "l"    # Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    .prologue
    .line 1706
    iget-object v1, p0, Lcom/google/android/music/preferences/MusicPreferences;->mPreferenceChangeListeners:Ljava/util/List;

    monitor-enter v1

    .line 1707
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/preferences/MusicPreferences;->mPreferenceChangeListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1708
    monitor-exit v1

    .line 1709
    return-void

    .line 1708
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public updateNautilusTimestamp()V
    .locals 10

    .prologue
    .line 1506
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 1507
    .local v4, "now":J
    const-string v6, "NautilusTimestampMillisec"

    const-wide/16 v8, 0x0

    invoke-direct {p0, v6, v8, v9}, Lcom/google/android/music/preferences/MusicPreferences;->getLongPreference(Ljava/lang/String;J)J

    move-result-wide v2

    .line 1509
    .local v2, "nautilusTimestamp":J
    const-wide/32 v0, 0x2932e00

    .line 1512
    .local v0, "minDiffMillsec":J
    sub-long v6, v4, v2

    invoke-static {v6, v7}, Ljava/lang/Math;->abs(J)J

    move-result-wide v6

    const-wide/32 v8, 0x2932e00

    cmp-long v6, v6, v8

    if-lez v6, :cond_0

    .line 1513
    const-string v6, "NautilusTimestampMillisec"

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-direct {p0, v6, v7}, Lcom/google/android/music/preferences/MusicPreferences;->setPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1514
    sget-boolean v6, Lcom/google/android/music/preferences/MusicPreferences;->LOGV:Z

    if-eqz v6, :cond_0

    .line 1515
    const-string v6, "MusicPreferences"

    const-string v7, "Nautlus status updated"

    invoke-static {v6, v7}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1518
    :cond_0
    return-void
.end method

.method public validateStreamingAccount()V
    .locals 1

    .prologue
    .line 749
    invoke-direct {p0}, Lcom/google/android/music/preferences/MusicPreferences;->isConfiguredSyncAccount()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/music/preferences/MusicPreferences;->getSyncAccount()Landroid/accounts/Account;

    move-result-object v0

    if-nez v0, :cond_0

    .line 750
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/music/preferences/MusicPreferences;->setStreamingAccount(Landroid/accounts/Account;)V

    .line 752
    :cond_0
    return-void
.end method

.method public declared-synchronized waitForFullyLoaded()Z
    .locals 1

    .prologue
    .line 291
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/music/preferences/MusicPreferences;->waitForServiceConnection()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public wasSidePannelClosedOnce()Z
    .locals 2

    .prologue
    .line 1241
    const-string v0, "SidePannelWasClosedOnce2"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->getBooleanPreference(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public wasTutorialViewed()Z
    .locals 2

    .prologue
    .line 1233
    const-string v0, "TutorialViewed2"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->getBooleanPreference(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method
