.class public Lcom/google/android/music/art/MainstageArtView;
.super Lcom/google/android/music/art/DocumentArtView;
.source "MainstageArtView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/art/MainstageArtView$1;
    }
.end annotation


# instance fields
.field private mOverlayView:Landroid/view/View;

.field private mTextBackgroundView:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/google/android/music/art/DocumentArtView;-><init>(Landroid/content/Context;)V

    .line 27
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/art/DocumentArtView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 34
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/music/art/DocumentArtView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 35
    return-void
.end method

.method private updateOverlay()V
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x0

    .line 44
    iget-object v2, p0, Lcom/google/android/music/art/MainstageArtView;->mCurrentRequest:Lcom/google/android/music/art/ArtRequest2;

    invoke-static {v2}, Lcom/google/android/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    iget-object v2, p0, Lcom/google/android/music/art/MainstageArtView;->mCurrentRequest:Lcom/google/android/music/art/ArtRequest2;

    invoke-virtual {v2}, Lcom/google/android/music/art/ArtRequest2;->getPalette()Landroid/support/v7/graphics/Palette;

    move-result-object v2

    const/high16 v3, -0x1000000

    invoke-virtual {v2, v3}, Landroid/support/v7/graphics/Palette;->getDarkVibrantColor(I)I

    move-result v0

    .line 46
    .local v0, "bgColor":I
    sget-object v2, Lcom/google/android/music/art/MainstageArtView$1;->$SwitchMap$com$google$android$music$ui$cardlib$model$Document$Type:[I

    iget-object v3, p0, Lcom/google/android/music/art/MainstageArtView;->mDocument:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v3}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 66
    iget-object v2, p0, Lcom/google/android/music/art/MainstageArtView;->mOverlayView:Landroid/view/View;

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    .line 68
    :goto_0
    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v1, v0}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 69
    .local v1, "drawable":Landroid/graphics/drawable/ColorDrawable;
    const/16 v2, 0xe5

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/ColorDrawable;->setAlpha(I)V

    .line 70
    iget-object v2, p0, Lcom/google/android/music/art/MainstageArtView;->mTextBackgroundView:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 71
    return-void

    .line 48
    .end local v1    # "drawable":Landroid/graphics/drawable/ColorDrawable;
    :pswitch_0
    iget-object v2, p0, Lcom/google/android/music/art/MainstageArtView;->mOverlayView:Landroid/view/View;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 49
    iget-object v2, p0, Lcom/google/android/music/art/MainstageArtView;->mDocument:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v2}, Lcom/google/android/music/ui/cardlib/model/Document;->getRadioSeedType()I

    move-result v2

    const/4 v3, 0x5

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/google/android/music/art/MainstageArtView;->mDocument:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v2}, Lcom/google/android/music/ui/cardlib/model/Document;->getRadioHighlightColor()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 52
    iget-object v2, p0, Lcom/google/android/music/art/MainstageArtView;->mDocument:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v2}, Lcom/google/android/music/ui/cardlib/model/Document;->getRadioHighlightColor()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    .line 54
    :cond_0
    iget-object v2, p0, Lcom/google/android/music/art/MainstageArtView;->mOverlayView:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_0

    .line 57
    :pswitch_1
    iget-object v2, p0, Lcom/google/android/music/art/MainstageArtView;->mDocument:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v2}, Lcom/google/android/music/ui/cardlib/model/Document;->getPlaylistType()I

    move-result v2

    const/16 v3, 0x32

    if-ne v2, v3, :cond_1

    .line 59
    iget-object v2, p0, Lcom/google/android/music/art/MainstageArtView;->mOverlayView:Landroid/view/View;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 60
    iget-object v2, p0, Lcom/google/android/music/art/MainstageArtView;->mOverlayView:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_0

    .line 62
    :cond_1
    iget-object v2, p0, Lcom/google/android/music/art/MainstageArtView;->mOverlayView:Landroid/view/View;

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 46
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method protected onArtRequestCompletedSuccessfully()V
    .locals 0

    .prologue
    .line 75
    invoke-direct {p0}, Lcom/google/android/music/art/MainstageArtView;->updateOverlay()V

    .line 76
    return-void
.end method

.method protected onReset()V
    .locals 2

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/music/art/MainstageArtView;->mOverlayView:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 40
    iget-object v0, p0, Lcom/google/android/music/art/MainstageArtView;->mTextBackgroundView:Landroid/view/View;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 41
    return-void
.end method

.method public setViews(Landroid/view/View;Landroid/view/View;)V
    .locals 0
    .param p1, "overlayView"    # Landroid/view/View;
    .param p2, "textBackgroundView"    # Landroid/view/View;

    .prologue
    .line 79
    iput-object p1, p0, Lcom/google/android/music/art/MainstageArtView;->mOverlayView:Landroid/view/View;

    .line 80
    iput-object p2, p0, Lcom/google/android/music/art/MainstageArtView;->mTextBackgroundView:Landroid/view/View;

    .line 81
    return-void
.end method
