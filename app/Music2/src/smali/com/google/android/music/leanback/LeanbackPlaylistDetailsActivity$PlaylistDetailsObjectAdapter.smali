.class Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;
.super Landroid/support/v17/leanback/widget/ObjectAdapter;
.source "LeanbackPlaylistDetailsActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PlaylistDetailsObjectAdapter"
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mDetailsObjectAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

.field private mExtraDataLoaded:Z

.field private mSize:I

.field private final mSongFooterRow:Lcom/google/android/music/leanback/SongFooterRow;

.field private mSongListObjectAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

.field private mSongListObjectAdapterStartIndex:I

.field private final mSongTitleRow:Lcom/google/android/music/leanback/SongTitleRow;

.field final synthetic this$0:Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;


# direct methods
.method constructor <init>(Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;Landroid/content/Context;Landroid/support/v17/leanback/widget/PresenterSelector;Landroid/support/v17/leanback/widget/ObjectAdapter;Lcom/google/android/music/leanback/SongTitleRow;Lcom/google/android/music/leanback/SongFooterRow;)V
    .locals 2
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "presenterSelector"    # Landroid/support/v17/leanback/widget/PresenterSelector;
    .param p4, "detailsObjectAdapter"    # Landroid/support/v17/leanback/widget/ObjectAdapter;
    .param p5, "songTitleRow"    # Lcom/google/android/music/leanback/SongTitleRow;
    .param p6, "songFooterRow"    # Lcom/google/android/music/leanback/SongFooterRow;

    .prologue
    .line 175
    iput-object p1, p0, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;->this$0:Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;

    .line 176
    invoke-direct {p0, p3}, Landroid/support/v17/leanback/widget/ObjectAdapter;-><init>(Landroid/support/v17/leanback/widget/PresenterSelector;)V

    .line 171
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;->mExtraDataLoaded:Z

    .line 177
    iput-object p2, p0, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;->mContext:Landroid/content/Context;

    .line 178
    iput-object p4, p0, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;->mDetailsObjectAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    .line 179
    iput-object p5, p0, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;->mSongTitleRow:Lcom/google/android/music/leanback/SongTitleRow;

    .line 180
    iput-object p6, p0, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;->mSongFooterRow:Lcom/google/android/music/leanback/SongFooterRow;

    .line 181
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;->mDetailsObjectAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    new-instance v1, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter$1;

    invoke-direct {v1, p0, p1}, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter$1;-><init>(Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;)V

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/ObjectAdapter;->registerObserver(Landroid/support/v17/leanback/widget/ObjectAdapter$DataObserver;)V

    .line 211
    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;->recalculateSize()V

    .line 212
    return-void
.end method

.method static synthetic access$1100(Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;

    .prologue
    .line 163
    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;->notifyChanged()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;

    .prologue
    .line 163
    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;->recalculateSize()V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;

    .prologue
    .line 163
    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;->notifyChanged()V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;II)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 163
    invoke-virtual {p0, p1, p2}, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;->notifyItemRangeChanged(II)V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;II)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 163
    invoke-virtual {p0, p1, p2}, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;->notifyItemRangeInserted(II)V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;II)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 163
    invoke-virtual {p0, p1, p2}, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;->notifyItemRangeRemoved(II)V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;

    .prologue
    .line 163
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;II)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 163
    invoke-virtual {p0, p1, p2}, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;->notifyItemRangeChanged(II)V

    return-void
.end method

.method private recalculateSize()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 238
    iget-object v3, p0, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;->mDetailsObjectAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    invoke-virtual {v3}, Landroid/support/v17/leanback/widget/ObjectAdapter;->size()I

    move-result v3

    invoke-static {v3, v8}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 239
    .local v2, "size":I
    if-lez v2, :cond_0

    iget-boolean v3, p0, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;->mExtraDataLoaded:Z

    if-nez v3, :cond_0

    .line 240
    iget-object v3, p0, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;->mDetailsObjectAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    invoke-virtual {v3, v7}, Landroid/support/v17/leanback/widget/ObjectAdapter;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v17/leanback/widget/DetailsOverviewRow;

    .line 242
    .local v0, "detailsOverviewRow":Landroid/support/v17/leanback/widget/DetailsOverviewRow;
    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/DetailsOverviewRow;->getItem()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/leanback/Item;

    .line 243
    .local v1, "item":Lcom/google/android/music/leanback/Item;
    iget-object v3, p0, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Lcom/google/android/music/leanback/Item;->getBitmapGettersGetter()Lcom/google/android/music/leanback/bitmap/BitmapGettersGetter;

    move-result-object v4

    invoke-virtual {v1}, Lcom/google/android/music/leanback/Item;->getWide()Z

    move-result v5

    new-instance v6, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter$2;

    invoke-direct {v6, p0, v0}, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter$2;-><init>(Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;Landroid/support/v17/leanback/widget/DetailsOverviewRow;)V

    invoke-static {v3, v4, v5, v7, v6}, Lcom/google/android/music/leanback/bitmap/BitmapFactory;->getBitmap(Landroid/content/Context;Lcom/google/android/music/leanback/bitmap/BitmapGettersGetter;ZZLcom/google/android/music/leanback/bitmap/BitmapListener;)V

    .line 252
    iget-object v3, p0, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;->this$0:Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;

    # invokes: Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;->createSongList(Lcom/google/android/music/leanback/Item;)Landroid/support/v17/leanback/widget/ObjectAdapter;
    invoke-static {v3, v1}, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;->access$1000(Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity;Lcom/google/android/music/leanback/Item;)Landroid/support/v17/leanback/widget/ObjectAdapter;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;->mSongListObjectAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    .line 253
    iget-object v3, p0, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;->mSongListObjectAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    new-instance v4, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter$3;

    invoke-direct {v4, p0}, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter$3;-><init>(Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;)V

    invoke-virtual {v3, v4}, Landroid/support/v17/leanback/widget/ObjectAdapter;->registerObserver(Landroid/support/v17/leanback/widget/ObjectAdapter$DataObserver;)V

    .line 260
    iput-boolean v8, p0, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;->mExtraDataLoaded:Z

    .line 262
    .end local v0    # "detailsOverviewRow":Landroid/support/v17/leanback/widget/DetailsOverviewRow;
    .end local v1    # "item":Lcom/google/android/music/leanback/Item;
    :cond_0
    iget-object v3, p0, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;->mSongListObjectAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    if-eqz v3, :cond_1

    .line 263
    add-int/lit8 v2, v2, 0x1

    .line 264
    iput v2, p0, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;->mSongListObjectAdapterStartIndex:I

    .line 265
    iget-object v3, p0, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;->mSongListObjectAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    invoke-virtual {v3}, Landroid/support/v17/leanback/widget/ObjectAdapter;->size()I

    move-result v3

    add-int/2addr v2, v3

    .line 266
    add-int/lit8 v2, v2, 0x1

    .line 270
    :goto_0
    iput v2, p0, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;->mSize:I

    .line 271
    return-void

    .line 268
    :cond_1
    const/4 v3, -0x1

    iput v3, p0, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;->mSongListObjectAdapterStartIndex:I

    goto :goto_0
.end method


# virtual methods
.method public get(I)Ljava/lang/Object;
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 221
    iget v0, p0, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;->mSize:I

    if-ge p1, v0, :cond_3

    .line 222
    iget v0, p0, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;->mSize:I

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_0

    .line 223
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;->mSongFooterRow:Lcom/google/android/music/leanback/SongFooterRow;

    .line 231
    :goto_0
    return-object v0

    .line 225
    :cond_0
    iget v0, p0, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;->mSongListObjectAdapterStartIndex:I

    if-lt p1, v0, :cond_1

    .line 226
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;->mSongListObjectAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    iget v1, p0, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;->mSongListObjectAdapterStartIndex:I

    sub-int v1, p1, v1

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/ObjectAdapter;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 228
    :cond_1
    iget v0, p0, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;->mSongListObjectAdapterStartIndex:I

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_2

    .line 229
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;->mSongTitleRow:Lcom/google/android/music/leanback/SongTitleRow;

    goto :goto_0

    .line 231
    :cond_2
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;->mDetailsObjectAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    invoke-virtual {v0, p1}, Landroid/support/v17/leanback/widget/ObjectAdapter;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 233
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Tried to get unexpected position "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". Size is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;->mSize:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 216
    iget v0, p0, Lcom/google/android/music/leanback/LeanbackPlaylistDetailsActivity$PlaylistDetailsObjectAdapter;->mSize:I

    return v0
.end method
