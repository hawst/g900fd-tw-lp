.class public Lcom/google/android/music/art/UpdateBackgroundTask;
.super Ljava/lang/Object;
.source "UpdateBackgroundTask.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/art/UpdateBackgroundTask$Callback;
    }
.end annotation


# instance fields
.field private final mBackgroundColor:I

.field private final mBackgroundMode:I

.field private final mContext:Landroid/content/Context;

.field private final mCropToCircle:Z

.field private final mDefaultArtId:I

.field private final mHeight:I

.field private volatile mIsCanceled:Z

.field private final mOwner:Lcom/google/android/music/art/UpdateBackgroundTask$Callback;

.field private final mWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/music/art/UpdateBackgroundTask$Callback;IIIZII)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "owner"    # Lcom/google/android/music/art/UpdateBackgroundTask$Callback;
    .param p3, "backgroundColor"    # I
    .param p4, "backgroundMode"    # I
    .param p5, "defaultArtId"    # I
    .param p6, "cropToCircle"    # Z
    .param p7, "width"    # I
    .param p8, "height"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-boolean v2, p0, Lcom/google/android/music/art/UpdateBackgroundTask;->mIsCanceled:Z

    .line 34
    if-lez p7, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "width must be > 0"

    invoke-static {v0, v3}, Lcom/google/android/common/base/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    .line 35
    if-lez p8, :cond_1

    :goto_1
    const-string v0, "height must be > 0"

    invoke-static {v1, v0}, Lcom/google/android/common/base/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    .line 36
    invoke-static {p2}, Lcom/google/android/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 37
    iput-object p1, p0, Lcom/google/android/music/art/UpdateBackgroundTask;->mContext:Landroid/content/Context;

    .line 38
    iput-object p2, p0, Lcom/google/android/music/art/UpdateBackgroundTask;->mOwner:Lcom/google/android/music/art/UpdateBackgroundTask$Callback;

    .line 39
    iput p3, p0, Lcom/google/android/music/art/UpdateBackgroundTask;->mBackgroundColor:I

    .line 40
    iput p4, p0, Lcom/google/android/music/art/UpdateBackgroundTask;->mBackgroundMode:I

    .line 41
    iput p5, p0, Lcom/google/android/music/art/UpdateBackgroundTask;->mDefaultArtId:I

    .line 42
    iput-boolean p6, p0, Lcom/google/android/music/art/UpdateBackgroundTask;->mCropToCircle:Z

    .line 43
    iput p7, p0, Lcom/google/android/music/art/UpdateBackgroundTask;->mWidth:I

    .line 44
    iput p8, p0, Lcom/google/android/music/art/UpdateBackgroundTask;->mHeight:I

    .line 45
    return-void

    :cond_0
    move v0, v2

    .line 34
    goto :goto_0

    :cond_1
    move v1, v2

    .line 35
    goto :goto_1
.end method

.method static synthetic access$000(Lcom/google/android/music/art/UpdateBackgroundTask;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/art/UpdateBackgroundTask;

    .prologue
    .line 18
    iget-boolean v0, p0, Lcom/google/android/music/art/UpdateBackgroundTask;->mIsCanceled:Z

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/music/art/UpdateBackgroundTask;)Lcom/google/android/music/art/UpdateBackgroundTask$Callback;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/art/UpdateBackgroundTask;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/google/android/music/art/UpdateBackgroundTask;->mOwner:Lcom/google/android/music/art/UpdateBackgroundTask$Callback;

    return-object v0
.end method

.method private renderBackground()Landroid/graphics/drawable/Drawable;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 78
    iget v4, p0, Lcom/google/android/music/art/UpdateBackgroundTask;->mBackgroundMode:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_1

    .line 80
    iget-object v4, p0, Lcom/google/android/music/art/UpdateBackgroundTask;->mContext:Landroid/content/Context;

    iget v5, p0, Lcom/google/android/music/art/UpdateBackgroundTask;->mDefaultArtId:I

    invoke-static {v4, v5}, Lcom/google/android/music/art/StaticArt;->getArt(Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 81
    .local v0, "bmp":Landroid/graphics/Bitmap;
    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v2, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    .line 87
    .end local v0    # "bmp":Landroid/graphics/Bitmap;
    .local v2, "drawable":Landroid/graphics/drawable/Drawable;
    :goto_0
    iget-boolean v4, p0, Lcom/google/android/music/art/UpdateBackgroundTask;->mCropToCircle:Z

    if-eqz v4, :cond_0

    .line 88
    iget v4, p0, Lcom/google/android/music/art/UpdateBackgroundTask;->mWidth:I

    iget v5, p0, Lcom/google/android/music/art/UpdateBackgroundTask;->mHeight:I

    sget-object v6, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v4, v5, v6}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 89
    .restart local v0    # "bmp":Landroid/graphics/Bitmap;
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 90
    .local v1, "canvas":Landroid/graphics/Canvas;
    invoke-virtual {v2, v1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 91
    invoke-virtual {v1}, Landroid/graphics/Canvas;->save()I

    .line 92
    invoke-static {v0}, Lcom/google/android/music/art/ArtRenderingUtils;->cropToCircle(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 93
    .local v3, "image":Landroid/graphics/Bitmap;
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 96
    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    .end local v2    # "drawable":Landroid/graphics/drawable/Drawable;
    invoke-direct {v2, v3}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    .line 98
    .end local v0    # "bmp":Landroid/graphics/Bitmap;
    .end local v1    # "canvas":Landroid/graphics/Canvas;
    .end local v3    # "image":Landroid/graphics/Bitmap;
    .restart local v2    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_0
    return-object v2

    .line 84
    .end local v2    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_1
    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    iget v4, p0, Lcom/google/android/music/art/UpdateBackgroundTask;->mBackgroundColor:I

    invoke-direct {v2, v4}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 85
    .restart local v2    # "drawable":Landroid/graphics/drawable/Drawable;
    iget v4, p0, Lcom/google/android/music/art/UpdateBackgroundTask;->mWidth:I

    iget v5, p0, Lcom/google/android/music/art/UpdateBackgroundTask;->mHeight:I

    invoke-virtual {v2, v6, v6, v4, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    goto :goto_0
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .prologue
    .line 65
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/art/UpdateBackgroundTask;->mIsCanceled:Z

    .line 66
    invoke-static {}, Lcom/google/android/music/art/ArtUtils;->getHandler()Lcom/google/android/music/art/ArtUtils$ArtHandler;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/music/art/ArtUtils$ArtHandler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 67
    return-void
.end method

.method public run()V
    .locals 3

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/google/android/music/art/UpdateBackgroundTask;->renderBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 50
    .local v0, "drawable":Landroid/graphics/drawable/Drawable;
    invoke-static {}, Lcom/google/android/music/art/ArtUtils;->getHandler()Lcom/google/android/music/art/ArtUtils$ArtHandler;

    move-result-object v1

    new-instance v2, Lcom/google/android/music/art/UpdateBackgroundTask$1;

    invoke-direct {v2, p0, v0}, Lcom/google/android/music/art/UpdateBackgroundTask$1;-><init>(Lcom/google/android/music/art/UpdateBackgroundTask;Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v1, v2}, Lcom/google/android/music/art/ArtUtils$ArtHandler;->postMainThread(Ljava/lang/Runnable;)Z

    .line 57
    return-void
.end method

.method public final schedule()V
    .locals 1

    .prologue
    .line 73
    invoke-static {}, Lcom/google/android/music/art/ArtUtils;->getHandler()Lcom/google/android/music/art/ArtUtils$ArtHandler;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/music/art/ArtUtils$ArtHandler;->post(Ljava/lang/Runnable;)Z

    .line 74
    return-void
.end method
