.class Lcom/google/android/music/AsyncAlbumArtImageView$ShuffleAllMode;
.super Lcom/google/android/music/AsyncAlbumArtImageView$Mode;
.source "AsyncAlbumArtImageView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/AsyncAlbumArtImageView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ShuffleAllMode"
.end annotation


# instance fields
.field public parentName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 284
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/music/AsyncAlbumArtImageView$Mode;-><init>(Lcom/google/android/music/AsyncAlbumArtImageView$1;)V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/music/AsyncAlbumArtImageView$ShuffleAllMode;)V
    .locals 1
    .param p1, "other"    # Lcom/google/android/music/AsyncAlbumArtImageView$ShuffleAllMode;

    .prologue
    .line 286
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/music/AsyncAlbumArtImageView$Mode;-><init>(Lcom/google/android/music/AsyncAlbumArtImageView$1;)V

    .line 287
    iget-object v0, p1, Lcom/google/android/music/AsyncAlbumArtImageView$ShuffleAllMode;->parentName:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView$ShuffleAllMode;->parentName:Ljava/lang/String;

    .line 288
    return-void
.end method


# virtual methods
.method public copy()Lcom/google/android/music/AsyncAlbumArtImageView$Mode;
    .locals 1

    .prologue
    .line 315
    new-instance v0, Lcom/google/android/music/AsyncAlbumArtImageView$ShuffleAllMode;

    invoke-direct {v0, p0}, Lcom/google/android/music/AsyncAlbumArtImageView$ShuffleAllMode;-><init>(Lcom/google/android/music/AsyncAlbumArtImageView$ShuffleAllMode;)V

    return-object v0
.end method

.method public createBitmap(Landroid/content/Context;IILcom/google/android/music/download/artwork/AlbumIdSink;Lcom/google/android/music/download/artwork/RemoteUrlSink;)Landroid/graphics/Bitmap;
    .locals 13
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "missingAlbumSink"    # Lcom/google/android/music/download/artwork/AlbumIdSink;
    .param p5, "missingUrlSink"    # Lcom/google/android/music/download/artwork/RemoteUrlSink;

    .prologue
    .line 293
    const/4 v2, 0x5

    const/4 v3, 0x1

    iget-object v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView$ShuffleAllMode;->parentName:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    int-to-long v4, v0

    iget-object v8, p0, Lcom/google/android/music/AsyncAlbumArtImageView$ShuffleAllMode;->parentName:Ljava/lang/String;

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v12, 0x0

    move-object v1, p1

    move v6, p2

    move/from16 v7, p3

    move-object/from16 v11, p4

    invoke-static/range {v1 .. v12}, Lcom/google/android/music/utils/AlbumArtUtils;->getFauxAlbumArt(Landroid/content/Context;IZJIILjava/lang/String;Ljava/lang/String;Lcom/google/android/music/utils/AlbumArtUtils$AlbumIdIteratorFactory;Lcom/google/android/music/download/artwork/AlbumIdSink;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    .line 306
    instance-of v1, p1, Lcom/google/android/music/AsyncAlbumArtImageView$ShuffleAllMode;

    if-nez v1, :cond_0

    .line 307
    const/4 v1, 0x0

    .line 310
    :goto_0
    return v1

    :cond_0
    move-object v0, p1

    .line 309
    check-cast v0, Lcom/google/android/music/AsyncAlbumArtImageView$ShuffleAllMode;

    .line 310
    .local v0, "o":Lcom/google/android/music/AsyncAlbumArtImageView$ShuffleAllMode;
    iget-object v1, p0, Lcom/google/android/music/AsyncAlbumArtImageView$ShuffleAllMode;->parentName:Ljava/lang/String;

    iget-object v2, v0, Lcom/google/android/music/AsyncAlbumArtImageView$ShuffleAllMode;->parentName:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/music/AsyncAlbumArtImageView$ShuffleAllMode;->isEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 301
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ShuffleAllMode<parentName:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/AsyncAlbumArtImageView$ShuffleAllMode;->parentName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
