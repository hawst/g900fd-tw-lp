.class public Lcom/google/android/music/ui/RadioFragment;
.super Lcom/google/android/music/ui/SubFragmentsPagerFragment;
.source "RadioFragment.java"


# instance fields
.field private mCreateRadioMenuItem:Landroid/view/MenuItem;

.field private mPrefChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# direct methods
.method public constructor <init>()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 42
    invoke-direct {p0}, Lcom/google/android/music/ui/SubFragmentsPagerFragment;-><init>()V

    .line 23
    new-instance v0, Lcom/google/android/music/ui/RadioFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/RadioFragment$1;-><init>(Lcom/google/android/music/ui/RadioFragment;)V

    iput-object v0, p0, Lcom/google/android/music/ui/RadioFragment;->mPrefChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    .line 43
    invoke-virtual {p0}, Lcom/google/android/music/ui/RadioFragment;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v7

    .line 44
    .local v7, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    invoke-static {v9}, Lcom/google/android/music/ui/GenreRadioGenreListFragment;->buildArguments(I)Landroid/os/Bundle;

    move-result-object v4

    .line 46
    .local v4, "bundle":Landroid/os/Bundle;
    invoke-virtual {v7}, Lcom/google/android/music/preferences/MusicPreferences;->isTabletMusicExperience()Z

    move-result v0

    if-eqz v0, :cond_0

    const/high16 v5, 0x3f000000    # 0.5f

    .line 48
    .local v5, "genreWidth":F
    :goto_0
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 49
    .local v8, "tabs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/music/ui/FragmentTabInfo;>;"
    new-instance v0, Lcom/google/android/music/ui/FragmentTabInfo;

    const-string v1, "genres"

    const v2, 0x7f0b00ac

    const-class v3, Lcom/google/android/music/ui/GenreRadioGenreListFragment;

    const/4 v6, 0x1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/music/ui/FragmentTabInfo;-><init>(Ljava/lang/String;ILjava/lang/Class;Landroid/os/Bundle;FZ)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 51
    new-instance v0, Lcom/google/android/music/ui/FragmentTabInfo;

    const-string v1, "stations"

    const v2, 0x7f0b00ab

    const-class v3, Lcom/google/android/music/ui/MyRadioFragment;

    invoke-direct {v0, v1, v2, v3, v9}, Lcom/google/android/music/ui/FragmentTabInfo;-><init>(Ljava/lang/String;ILjava/lang/Class;Z)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 53
    new-instance v0, Lcom/google/android/music/ui/FragmentTabInfo;

    const-string v1, "recommended"

    const v2, 0x7f0b00aa

    const-class v3, Lcom/google/android/music/ui/RecommendedRadioFragment;

    invoke-direct {v0, v1, v2, v3, v9}, Lcom/google/android/music/ui/FragmentTabInfo;-><init>(Ljava/lang/String;ILjava/lang/Class;Z)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 55
    const-string v0, "stations"

    invoke-virtual {p0, v8, v0}, Lcom/google/android/music/ui/RadioFragment;->init(Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 56
    return-void

    .line 46
    .end local v5    # "genreWidth":F
    .end local v8    # "tabs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/music/ui/FragmentTabInfo;>;"
    :cond_0
    const/high16 v5, 0x3f800000    # 1.0f

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/google/android/music/ui/RadioFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/RadioFragment;

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/google/android/music/ui/RadioFragment;->updateFragment()V

    return-void
.end method

.method private updateFragment()V
    .locals 2

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/android/music/ui/RadioFragment;->mCreateRadioMenuItem:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    .line 99
    invoke-virtual {p0}, Lcom/google/android/music/ui/RadioFragment;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->isDownloadedOnlyMode()Z

    move-result v0

    if-nez v0, :cond_1

    .line 100
    iget-object v0, p0, Lcom/google/android/music/ui/RadioFragment;->mCreateRadioMenuItem:Landroid/view/MenuItem;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 105
    :cond_0
    :goto_0
    return-void

    .line 102
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/ui/RadioFragment;->mCreateRadioMenuItem:Landroid/view/MenuItem;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedState"    # Landroid/os/Bundle;

    .prologue
    .line 61
    invoke-super {p0, p1}, Lcom/google/android/music/ui/SubFragmentsPagerFragment;->onCreate(Landroid/os/Bundle;)V

    .line 62
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/RadioFragment;->setHasOptionsMenu(Z)V

    .line 63
    invoke-virtual {p0}, Lcom/google/android/music/ui/RadioFragment;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/ui/RadioFragment;->mPrefChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-virtual {v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 64
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 3
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "inflater"    # Landroid/view/MenuInflater;

    .prologue
    .line 74
    invoke-virtual {p0}, Lcom/google/android/music/ui/RadioFragment;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    .line 75
    .local v0, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->showStartRadioButtonsInActionBar()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/music/ui/RadioFragment;->getBaseActivity()Lcom/google/android/music/ui/BaseActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/ui/BaseActivity;->isSideDrawerOpen()Z

    move-result v1

    if-nez v1, :cond_0

    .line 76
    const v1, 0x7f140003

    invoke-virtual {p2, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 77
    const v1, 0x7f0e02a3

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/ui/RadioFragment;->mCreateRadioMenuItem:Landroid/view/MenuItem;

    .line 78
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/preferences/MusicPreferences;->isNautilusEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 79
    iget-object v1, p0, Lcom/google/android/music/ui/RadioFragment;->mCreateRadioMenuItem:Landroid/view/MenuItem;

    const v2, 0x7f0b0259

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    .line 84
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/google/android/music/ui/RadioFragment;->updateFragment()V

    .line 85
    return-void

    .line 81
    :cond_1
    iget-object v1, p0, Lcom/google/android/music/ui/RadioFragment;->mCreateRadioMenuItem:Landroid/view/MenuItem;

    const v2, 0x7f0b025b

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 68
    invoke-virtual {p0}, Lcom/google/android/music/ui/RadioFragment;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/ui/RadioFragment;->mPrefChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-virtual {v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 69
    invoke-super {p0}, Lcom/google/android/music/ui/SubFragmentsPagerFragment;->onDestroyView()V

    .line 70
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 89
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 94
    invoke-super {p0, p1}, Lcom/google/android/music/ui/SubFragmentsPagerFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 91
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/music/ui/RadioFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/music/ui/SearchActivity;->showCreateRadioSearch(Landroid/content/Context;)V

    .line 92
    const/4 v0, 0x1

    goto :goto_0

    .line 89
    nop

    :pswitch_data_0
    .packed-switch 0x7f0e02a3
        :pswitch_0
    .end packed-switch
.end method
