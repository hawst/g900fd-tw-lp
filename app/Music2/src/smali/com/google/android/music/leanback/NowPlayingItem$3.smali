.class Lcom/google/android/music/leanback/NowPlayingItem$3;
.super Landroid/os/Handler;
.source "NowPlayingItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/leanback/NowPlayingItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/leanback/NowPlayingItem;


# direct methods
.method constructor <init>(Lcom/google/android/music/leanback/NowPlayingItem;)V
    .locals 0

    .prologue
    .line 170
    iput-object p1, p0, Lcom/google/android/music/leanback/NowPlayingItem$3;->this$0:Lcom/google/android/music/leanback/NowPlayingItem;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method private equalBundles(Landroid/os/Bundle;Landroid/os/Bundle;Ljava/util/HashSet;)Z
    .locals 9
    .param p1, "one"    # Landroid/os/Bundle;
    .param p2, "two"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "Landroid/os/Bundle;",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p3, "keysToCompare":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 234
    if-nez p1, :cond_0

    if-nez p2, :cond_0

    move v5, v7

    .line 264
    :goto_0
    return v5

    .line 238
    :cond_0
    if-eqz p1, :cond_1

    if-nez p2, :cond_2

    :cond_1
    move v5, v8

    .line 239
    goto :goto_0

    .line 242
    :cond_2
    invoke-virtual {p1}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v2

    .line 246
    .local v2, "setOne":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_7

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 247
    .local v1, "key":Ljava/lang/String;
    invoke-virtual {p3, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 250
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    .line 251
    .local v3, "valueOne":Ljava/lang/Object;
    invoke-virtual {p2, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    .line 252
    .local v4, "valueTwo":Ljava/lang/Object;
    instance-of v5, v3, Landroid/os/Bundle;

    if-eqz v5, :cond_4

    instance-of v5, v4, Landroid/os/Bundle;

    if-eqz v5, :cond_4

    move-object v5, v3

    check-cast v5, Landroid/os/Bundle;

    move-object v6, v4

    check-cast v6, Landroid/os/Bundle;

    invoke-direct {p0, v5, v6, p3}, Lcom/google/android/music/leanback/NowPlayingItem$3;->equalBundles(Landroid/os/Bundle;Landroid/os/Bundle;Ljava/util/HashSet;)Z

    move-result v5

    if-nez v5, :cond_4

    move v5, v8

    .line 254
    goto :goto_0

    .line 255
    :cond_4
    if-nez v3, :cond_6

    .line 256
    if-nez v4, :cond_5

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_3

    :cond_5
    move v5, v8

    .line 257
    goto :goto_0

    .line 259
    :cond_6
    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    move v5, v8

    .line 260
    goto :goto_0

    .end local v1    # "key":Ljava/lang/String;
    .end local v3    # "valueOne":Ljava/lang/Object;
    .end local v4    # "valueTwo":Ljava/lang/Object;
    :cond_7
    move v5, v7

    .line 264
    goto :goto_0
.end method

.method private updateTrackInfoImpl(Landroid/os/Bundle;)V
    .locals 13
    .param p1, "data"    # Landroid/os/Bundle;

    .prologue
    const/4 v0, 0x0

    .line 186
    const/4 v10, 0x0

    .line 187
    .local v10, "dataChanged":Z
    iget-object v1, p0, Lcom/google/android/music/leanback/NowPlayingItem$3;->this$0:Lcom/google/android/music/leanback/NowPlayingItem;

    # getter for: Lcom/google/android/music/leanback/NowPlayingItem;->mDataLock:Ljava/lang/Object;
    invoke-static {v1}, Lcom/google/android/music/leanback/NowPlayingItem;->access$500(Lcom/google/android/music/leanback/NowPlayingItem;)Ljava/lang/Object;

    move-result-object v12

    monitor-enter v12

    .line 188
    :try_start_0
    iget-object v1, p0, Lcom/google/android/music/leanback/NowPlayingItem$3;->this$0:Lcom/google/android/music/leanback/NowPlayingItem;

    # getter for: Lcom/google/android/music/leanback/NowPlayingItem;->mData:Landroid/os/Bundle;
    invoke-static {v1}, Lcom/google/android/music/leanback/NowPlayingItem;->access$600(Lcom/google/android/music/leanback/NowPlayingItem;)Landroid/os/Bundle;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/leanback/NowPlayingItem$3;->this$0:Lcom/google/android/music/leanback/NowPlayingItem;

    # getter for: Lcom/google/android/music/leanback/NowPlayingItem;->mRelevantKeys:Ljava/util/HashSet;
    invoke-static {v2}, Lcom/google/android/music/leanback/NowPlayingItem;->access$700(Lcom/google/android/music/leanback/NowPlayingItem;)Ljava/util/HashSet;

    move-result-object v2

    invoke-direct {p0, p1, v1, v2}, Lcom/google/android/music/leanback/NowPlayingItem$3;->equalBundles(Landroid/os/Bundle;Landroid/os/Bundle;Ljava/util/HashSet;)Z

    move-result v1

    if-nez v1, :cond_4

    const/4 v10, 0x1

    .line 189
    :goto_0
    if-eqz v10, :cond_2

    .line 190
    if-eqz p1, :cond_5

    const-string v0, "currentSongLoaded"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 192
    new-instance v11, Lcom/google/android/music/download/ContentIdentifier;

    const-string v0, "id"

    const-wide/16 v2, -0x1

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    invoke-static {}, Lcom/google/android/music/download/ContentIdentifier$Domain;->values()[Lcom/google/android/music/download/ContentIdentifier$Domain;

    move-result-object v2

    const-string v3, "domain"

    const/4 v4, 0x0

    invoke-virtual {p1, v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    aget-object v2, v2, v3

    invoke-direct {v11, v0, v1, v2}, Lcom/google/android/music/download/ContentIdentifier;-><init>(JLcom/google/android/music/download/ContentIdentifier$Domain;)V

    .line 197
    .local v11, "songId":Lcom/google/android/music/download/ContentIdentifier;
    iget-object v0, p0, Lcom/google/android/music/leanback/NowPlayingItem$3;->this$0:Lcom/google/android/music/leanback/NowPlayingItem;

    # setter for: Lcom/google/android/music/leanback/NowPlayingItem;->mData:Landroid/os/Bundle;
    invoke-static {v0, p1}, Lcom/google/android/music/leanback/NowPlayingItem;->access$602(Lcom/google/android/music/leanback/NowPlayingItem;Landroid/os/Bundle;)Landroid/os/Bundle;

    .line 198
    const-string v0, "artist"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 199
    .local v9, "artistName":Ljava/lang/String;
    invoke-static {v9}, Lcom/google/android/music/utils/MusicUtils;->isUnknown(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 200
    iget-object v0, p0, Lcom/google/android/music/leanback/NowPlayingItem$3;->this$0:Lcom/google/android/music/leanback/NowPlayingItem;

    # getter for: Lcom/google/android/music/leanback/NowPlayingItem;->mLeanbackItemActivity:Lcom/google/android/music/leanback/LeanbackItemActivity;
    invoke-static {v0}, Lcom/google/android/music/leanback/NowPlayingItem;->access$800(Lcom/google/android/music/leanback/NowPlayingItem;)Lcom/google/android/music/leanback/LeanbackItemActivity;

    move-result-object v0

    const v1, 0x7f0b00c4

    invoke-virtual {v0, v1}, Lcom/google/android/music/leanback/LeanbackItemActivity;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 203
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/leanback/NowPlayingItem$3;->this$0:Lcom/google/android/music/leanback/NowPlayingItem;

    # setter for: Lcom/google/android/music/leanback/NowPlayingItem;->mArtistName:Ljava/lang/String;
    invoke-static {v0, v9}, Lcom/google/android/music/leanback/NowPlayingItem;->access$902(Lcom/google/android/music/leanback/NowPlayingItem;Ljava/lang/String;)Ljava/lang/String;

    .line 205
    const-string v0, "album"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 206
    .local v8, "albumName":Ljava/lang/String;
    invoke-static {v8}, Lcom/google/android/music/utils/MusicUtils;->isUnknown(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 207
    iget-object v0, p0, Lcom/google/android/music/leanback/NowPlayingItem$3;->this$0:Lcom/google/android/music/leanback/NowPlayingItem;

    # getter for: Lcom/google/android/music/leanback/NowPlayingItem;->mLeanbackItemActivity:Lcom/google/android/music/leanback/LeanbackItemActivity;
    invoke-static {v0}, Lcom/google/android/music/leanback/NowPlayingItem;->access$800(Lcom/google/android/music/leanback/NowPlayingItem;)Lcom/google/android/music/leanback/LeanbackItemActivity;

    move-result-object v0

    const v1, 0x7f0b00c5

    invoke-virtual {v0, v1}, Lcom/google/android/music/leanback/LeanbackItemActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 210
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/leanback/NowPlayingItem$3;->this$0:Lcom/google/android/music/leanback/NowPlayingItem;

    # setter for: Lcom/google/android/music/leanback/NowPlayingItem;->mAlbumName:Ljava/lang/String;
    invoke-static {v0, v8}, Lcom/google/android/music/leanback/NowPlayingItem;->access$1002(Lcom/google/android/music/leanback/NowPlayingItem;Ljava/lang/String;)Ljava/lang/String;

    .line 212
    iget-object v0, p0, Lcom/google/android/music/leanback/NowPlayingItem$3;->this$0:Lcom/google/android/music/leanback/NowPlayingItem;

    const-string v1, "albumId"

    const-wide/16 v2, -0x1

    invoke-virtual {p1, v1, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    # setter for: Lcom/google/android/music/leanback/NowPlayingItem;->mAlbumId:J
    invoke-static {v0, v2, v3}, Lcom/google/android/music/leanback/NowPlayingItem;->access$1102(Lcom/google/android/music/leanback/NowPlayingItem;J)J

    .line 213
    iget-object v0, p0, Lcom/google/android/music/leanback/NowPlayingItem$3;->this$0:Lcom/google/android/music/leanback/NowPlayingItem;

    # getter for: Lcom/google/android/music/leanback/NowPlayingItem;->mNowPlayingBitmapGetter:Lcom/google/android/music/leanback/bitmap/NowPlayingBitmapGetter;
    invoke-static {v0}, Lcom/google/android/music/leanback/NowPlayingItem;->access$1200(Lcom/google/android/music/leanback/NowPlayingItem;)Lcom/google/android/music/leanback/bitmap/NowPlayingBitmapGetter;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/leanback/NowPlayingItem$3;->this$0:Lcom/google/android/music/leanback/NowPlayingItem;

    # getter for: Lcom/google/android/music/leanback/NowPlayingItem;->mData:Landroid/os/Bundle;
    invoke-static {v1}, Lcom/google/android/music/leanback/NowPlayingItem;->access$600(Lcom/google/android/music/leanback/NowPlayingItem;)Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "albumArtFromService"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iget-object v2, p0, Lcom/google/android/music/leanback/NowPlayingItem$3;->this$0:Lcom/google/android/music/leanback/NowPlayingItem;

    # getter for: Lcom/google/android/music/leanback/NowPlayingItem;->mData:Landroid/os/Bundle;
    invoke-static {v2}, Lcom/google/android/music/leanback/NowPlayingItem;->access$600(Lcom/google/android/music/leanback/NowPlayingItem;)Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "externalAlbumArtUrl"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/music/leanback/NowPlayingItem$3;->this$0:Lcom/google/android/music/leanback/NowPlayingItem;

    # getter for: Lcom/google/android/music/leanback/NowPlayingItem;->mData:Landroid/os/Bundle;
    invoke-static {v3}, Lcom/google/android/music/leanback/NowPlayingItem;->access$600(Lcom/google/android/music/leanback/NowPlayingItem;)Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "albumArtResourceId"

    const/4 v5, -0x1

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    iget-object v4, p0, Lcom/google/android/music/leanback/NowPlayingItem$3;->this$0:Lcom/google/android/music/leanback/NowPlayingItem;

    # getter for: Lcom/google/android/music/leanback/NowPlayingItem;->mArtistName:Ljava/lang/String;
    invoke-static {v4}, Lcom/google/android/music/leanback/NowPlayingItem;->access$900(Lcom/google/android/music/leanback/NowPlayingItem;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/music/leanback/NowPlayingItem$3;->this$0:Lcom/google/android/music/leanback/NowPlayingItem;

    # getter for: Lcom/google/android/music/leanback/NowPlayingItem;->mAlbumName:Ljava/lang/String;
    invoke-static {v5}, Lcom/google/android/music/leanback/NowPlayingItem;->access$1000(Lcom/google/android/music/leanback/NowPlayingItem;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/music/leanback/NowPlayingItem$3;->this$0:Lcom/google/android/music/leanback/NowPlayingItem;

    # getter for: Lcom/google/android/music/leanback/NowPlayingItem;->mAlbumId:J
    invoke-static {v6}, Lcom/google/android/music/leanback/NowPlayingItem;->access$1100(Lcom/google/android/music/leanback/NowPlayingItem;)J

    move-result-wide v6

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/music/leanback/bitmap/NowPlayingBitmapGetter;->setData(ZLjava/lang/String;ILjava/lang/String;Ljava/lang/String;J)V

    .line 218
    iget-object v0, p0, Lcom/google/android/music/leanback/NowPlayingItem$3;->this$0:Lcom/google/android/music/leanback/NowPlayingItem;

    const-string v1, "track"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/google/android/music/leanback/NowPlayingItem;->mTrackName:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/google/android/music/leanback/NowPlayingItem;->access$1302(Lcom/google/android/music/leanback/NowPlayingItem;Ljava/lang/String;)Ljava/lang/String;

    .line 219
    iget-object v0, p0, Lcom/google/android/music/leanback/NowPlayingItem$3;->this$0:Lcom/google/android/music/leanback/NowPlayingItem;

    invoke-virtual {v11}, Lcom/google/android/music/download/ContentIdentifier;->getId()J

    move-result-wide v2

    iget-object v1, p0, Lcom/google/android/music/leanback/NowPlayingItem$3;->this$0:Lcom/google/android/music/leanback/NowPlayingItem;

    # getter for: Lcom/google/android/music/leanback/NowPlayingItem;->mTrackName:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/music/leanback/NowPlayingItem;->access$1300(Lcom/google/android/music/leanback/NowPlayingItem;)Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/google/android/music/leanback/NowPlayingItem;->handleSongId(JLjava/lang/String;)Z
    invoke-static {v0, v2, v3, v1}, Lcom/google/android/music/leanback/NowPlayingItem;->access$1400(Lcom/google/android/music/leanback/NowPlayingItem;JLjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 220
    # getter for: Lcom/google/android/music/leanback/NowPlayingItem;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/music/leanback/NowPlayingItem;->access$300()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Artist: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    # getter for: Lcom/google/android/music/leanback/NowPlayingItem;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/music/leanback/NowPlayingItem;->access$300()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Album:  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    .end local v8    # "albumName":Ljava/lang/String;
    .end local v9    # "artistName":Ljava/lang/String;
    .end local v11    # "songId":Lcom/google/android/music/download/ContentIdentifier;
    :cond_2
    :goto_1
    monitor-exit v12
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 228
    if-eqz v10, :cond_3

    .line 229
    iget-object v0, p0, Lcom/google/android/music/leanback/NowPlayingItem$3;->this$0:Lcom/google/android/music/leanback/NowPlayingItem;

    # getter for: Lcom/google/android/music/leanback/NowPlayingItem;->mListener:Lcom/google/android/music/leanback/NowPlayingItem$Listener;
    invoke-static {v0}, Lcom/google/android/music/leanback/NowPlayingItem;->access$1500(Lcom/google/android/music/leanback/NowPlayingItem;)Lcom/google/android/music/leanback/NowPlayingItem$Listener;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/music/leanback/NowPlayingItem$Listener;->onNowPlayingChanged()V

    .line 231
    :cond_3
    return-void

    :cond_4
    move v10, v0

    .line 188
    goto/16 :goto_0

    .line 224
    :cond_5
    :try_start_1
    iget-object v0, p0, Lcom/google/android/music/leanback/NowPlayingItem$3;->this$0:Lcom/google/android/music/leanback/NowPlayingItem;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/music/leanback/NowPlayingItem;->mData:Landroid/os/Bundle;
    invoke-static {v0, v1}, Lcom/google/android/music/leanback/NowPlayingItem;->access$602(Lcom/google/android/music/leanback/NowPlayingItem;Landroid/os/Bundle;)Landroid/os/Bundle;

    goto :goto_1

    .line 227
    :catchall_0
    move-exception v0

    monitor-exit v12
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 1
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 173
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 181
    :goto_0
    return-void

    .line 175
    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/os/Bundle;

    invoke-direct {p0, v0}, Lcom/google/android/music/leanback/NowPlayingItem$3;->updateTrackInfoImpl(Landroid/os/Bundle;)V

    goto :goto_0

    .line 173
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method
