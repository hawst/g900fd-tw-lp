.class Lcom/google/android/music/download/AbstractSchedulingService$4;
.super Lcom/google/android/music/download/cache/IDeleteFilter$Stub;
.source "AbstractSchedulingService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/download/AbstractSchedulingService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/download/AbstractSchedulingService;


# direct methods
.method constructor <init>(Lcom/google/android/music/download/AbstractSchedulingService;)V
    .locals 0

    .prologue
    .line 158
    iput-object p1, p0, Lcom/google/android/music/download/AbstractSchedulingService$4;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    invoke-direct {p0}, Lcom/google/android/music/download/cache/IDeleteFilter$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public getFilteredIds()[Lcom/google/android/music/download/ContentIdentifier;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 174
    iget-object v2, p0, Lcom/google/android/music/download/AbstractSchedulingService$4;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    iget-object v3, v2, Lcom/google/android/music/download/AbstractSchedulingService;->mRequests:Ljava/util/List;

    monitor-enter v3

    .line 175
    :try_start_0
    iget-object v2, p0, Lcom/google/android/music/download/AbstractSchedulingService$4;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    iget-object v2, v2, Lcom/google/android/music/download/AbstractSchedulingService;->mRequests:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_0

    .line 176
    const/4 v1, 0x0

    monitor-exit v3

    .line 182
    :goto_0
    return-object v1

    .line 178
    :cond_0
    iget-object v2, p0, Lcom/google/android/music/download/AbstractSchedulingService$4;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    iget-object v2, v2, Lcom/google/android/music/download/AbstractSchedulingService;->mRequests:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    new-array v1, v2, [Lcom/google/android/music/download/ContentIdentifier;

    .line 179
    .local v1, "ids":[Lcom/google/android/music/download/ContentIdentifier;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    array-length v2, v1

    if-ge v0, v2, :cond_1

    .line 180
    iget-object v2, p0, Lcom/google/android/music/download/AbstractSchedulingService$4;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    iget-object v2, v2, Lcom/google/android/music/download/AbstractSchedulingService;->mRequests:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/download/TrackDownloadRequest;

    invoke-virtual {v2}, Lcom/google/android/music/download/TrackDownloadRequest;->getId()Lcom/google/android/music/download/ContentIdentifier;

    move-result-object v2

    aput-object v2, v1, v0

    .line 179
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 182
    :cond_1
    monitor-exit v3

    goto :goto_0

    .line 183
    .end local v0    # "i":I
    .end local v1    # "ids":[Lcom/google/android/music/download/ContentIdentifier;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public shouldFilter(Ljava/lang/String;)Z
    .locals 4
    .param p1, "fullFilePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 161
    iget-object v2, p0, Lcom/google/android/music/download/AbstractSchedulingService$4;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    iget-object v3, v2, Lcom/google/android/music/download/AbstractSchedulingService;->mRequests:Ljava/util/List;

    monitor-enter v3

    .line 162
    :try_start_0
    iget-object v2, p0, Lcom/google/android/music/download/AbstractSchedulingService$4;->this$0:Lcom/google/android/music/download/AbstractSchedulingService;

    iget-object v2, v2, Lcom/google/android/music/download/AbstractSchedulingService;->mRequests:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/download/TrackDownloadRequest;

    .line 163
    .local v1, "request":Lcom/google/android/music/download/TrackDownloadRequest;
    invoke-virtual {v1}, Lcom/google/android/music/download/TrackDownloadRequest;->getFileLocation()Lcom/google/android/music/download/cache/FileLocation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/music/download/cache/FileLocation;->getFullPath()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 165
    const/4 v2, 0x1

    monitor-exit v3

    .line 169
    .end local v1    # "request":Lcom/google/android/music/download/TrackDownloadRequest;
    :goto_0
    return v2

    .line 168
    :cond_1
    monitor-exit v3

    .line 169
    const/4 v2, 0x0

    goto :goto_0

    .line 168
    .end local v0    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method
