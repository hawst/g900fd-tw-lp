.class public Lcom/google/android/music/download/ServerRejectionException;
.super Ljava/io/IOException;
.source "ServerRejectionException.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/download/ServerRejectionException$RejectionReason;
    }
.end annotation


# instance fields
.field private final mRejectionReason:Lcom/google/android/music/download/ServerRejectionException$RejectionReason;


# direct methods
.method public constructor <init>(Lcom/google/android/music/download/ServerRejectionException$RejectionReason;)V
    .locals 0
    .param p1, "reason"    # Lcom/google/android/music/download/ServerRejectionException$RejectionReason;

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/io/IOException;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/google/android/music/download/ServerRejectionException;->mRejectionReason:Lcom/google/android/music/download/ServerRejectionException$RejectionReason;

    .line 25
    return-void
.end method


# virtual methods
.method public getRejectionReason()Lcom/google/android/music/download/ServerRejectionException$RejectionReason;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/music/download/ServerRejectionException;->mRejectionReason:Lcom/google/android/music/download/ServerRejectionException$RejectionReason;

    return-object v0
.end method
