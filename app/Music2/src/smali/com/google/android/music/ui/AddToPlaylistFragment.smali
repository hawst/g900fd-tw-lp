.class public Lcom/google/android/music/ui/AddToPlaylistFragment;
.super Lcom/google/android/music/ui/PlaylistDialogFragment;
.source "AddToPlaylistFragment.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/music/ui/PlaylistDialogFragment;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field final mItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

.field private mSongsToAdd:Lcom/google/android/music/medialist/SongList;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 40
    const v0, 0x7f0b00c9

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/ui/PlaylistDialogFragment;-><init>(IZ)V

    .line 57
    new-instance v0, Lcom/google/android/music/ui/AddToPlaylistFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/AddToPlaylistFragment$1;-><init>(Lcom/google/android/music/ui/AddToPlaylistFragment;)V

    iput-object v0, p0, Lcom/google/android/music/ui/AddToPlaylistFragment;->mItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    .line 41
    iget-object v0, p0, Lcom/google/android/music/ui/AddToPlaylistFragment;->mItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/AddToPlaylistFragment;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 42
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/ui/AddToPlaylistFragment;)Lcom/google/android/music/medialist/SongList;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/AddToPlaylistFragment;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/music/ui/AddToPlaylistFragment;->mSongsToAdd:Lcom/google/android/music/medialist/SongList;

    return-object v0
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 46
    invoke-virtual {p0}, Lcom/google/android/music/ui/AddToPlaylistFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 47
    .local v0, "args":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 48
    const-string v1, "songList"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/medialist/SongList;

    iput-object v1, p0, Lcom/google/android/music/ui/AddToPlaylistFragment;->mSongsToAdd:Lcom/google/android/music/medialist/SongList;

    .line 54
    :goto_0
    invoke-super {p0, p1}, Lcom/google/android/music/ui/PlaylistDialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v1

    return-object v1

    .line 51
    :cond_0
    const-string v1, "AddToPlaylistFragment"

    const-string v2, "Created without arguments, exiting"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 52
    invoke-virtual {p0}, Lcom/google/android/music/ui/AddToPlaylistFragment;->dismiss()V

    goto :goto_0
.end method
