.class Lcom/google/android/music/ui/ContainerHeaderView$5;
.super Ljava/lang/Object;
.source "ContainerHeaderView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/ContainerHeaderView;->setupArtistArtSlideShow(Landroid/database/Cursor;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/ContainerHeaderView;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/ContainerHeaderView;)V
    .locals 0

    .prologue
    .line 724
    iput-object p1, p0, Lcom/google/android/music/ui/ContainerHeaderView$5;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 727
    iget-object v3, p0, Lcom/google/android/music/ui/ContainerHeaderView$5;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/ContainerHeaderView;->mArtistArtUrls:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/google/android/music/ui/ContainerHeaderView;->access$2400(Lcom/google/android/music/ui/ContainerHeaderView;)Ljava/util/ArrayList;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/music/ui/ContainerHeaderView$5;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/ContainerHeaderView;->mArtistArtUrls:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/google/android/music/ui/ContainerHeaderView;->access$2400(Lcom/google/android/music/ui/ContainerHeaderView;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 729
    :cond_0
    const-string v3, "ContainerHeader"

    const-string v4, "mSlideRefreshRunnable: the artist url list is empty"

    invoke-static {v3, v4}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 747
    :goto_0
    return-void

    .line 732
    :cond_1
    # getter for: Lcom/google/android/music/ui/ContainerHeaderView;->LOGV:Z
    invoke-static {}, Lcom/google/android/music/ui/ContainerHeaderView;->access$2200()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 733
    const-string v3, "ContainerHeader"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Slide refresh. Showing first slide="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/music/ui/ContainerHeaderView$5;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/ContainerHeaderView;->mShowingFirstSlide:Z
    invoke-static {v5}, Lcom/google/android/music/ui/ContainerHeaderView;->access$2000(Lcom/google/android/music/ui/ContainerHeaderView;)Z

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 737
    :cond_2
    iget-object v3, p0, Lcom/google/android/music/ui/ContainerHeaderView$5;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/ContainerHeaderView;->mShowingFirstSlide:Z
    invoke-static {v3}, Lcom/google/android/music/ui/ContainerHeaderView;->access$2000(Lcom/google/android/music/ui/ContainerHeaderView;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/android/music/ui/ContainerHeaderView$5;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/ContainerHeaderView;->mArtistArt2:Landroid/widget/ImageView;
    invoke-static {v3}, Lcom/google/android/music/ui/ContainerHeaderView;->access$2500(Lcom/google/android/music/ui/ContainerHeaderView;)Landroid/widget/ImageView;

    move-result-object v2

    .line 738
    .local v2, "slideToRefresh":Landroid/widget/ImageView;
    :goto_1
    iget-object v3, p0, Lcom/google/android/music/ui/ContainerHeaderView$5;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/ContainerHeaderView;->mShowingFirstSlide:Z
    invoke-static {v3}, Lcom/google/android/music/ui/ContainerHeaderView;->access$2000(Lcom/google/android/music/ui/ContainerHeaderView;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/google/android/music/ui/ContainerHeaderView$5;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/ContainerHeaderView;->mArtistArtHelper2:Lcom/google/android/music/ui/ContainerHeaderView$ArtHelper;
    invoke-static {v3}, Lcom/google/android/music/ui/ContainerHeaderView;->access$2100(Lcom/google/android/music/ui/ContainerHeaderView;)Lcom/google/android/music/ui/ContainerHeaderView$ArtHelper;

    move-result-object v0

    .line 740
    .local v0, "helper":Lcom/google/android/music/ui/ContainerHeaderView$ArtHelper;
    :goto_2
    iget-object v3, p0, Lcom/google/android/music/ui/ContainerHeaderView$5;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    # invokes: Lcom/google/android/music/ui/ContainerHeaderView;->clearArtistArt(Landroid/widget/ImageView;)V
    invoke-static {v3, v2}, Lcom/google/android/music/ui/ContainerHeaderView;->access$2600(Lcom/google/android/music/ui/ContainerHeaderView;Landroid/widget/ImageView;)V

    .line 741
    iget-object v3, p0, Lcom/google/android/music/ui/ContainerHeaderView$5;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    # invokes: Lcom/google/android/music/ui/ContainerHeaderView;->getNextArtistUrl()Ljava/lang/String;
    invoke-static {v3}, Lcom/google/android/music/ui/ContainerHeaderView;->access$2700(Lcom/google/android/music/ui/ContainerHeaderView;)Ljava/lang/String;

    move-result-object v1

    .line 746
    .local v1, "nextUrl":Ljava/lang/String;
    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/ContainerHeaderView$ArtHelper;->requestBitmap(Ljava/lang/String;)V

    goto :goto_0

    .line 737
    .end local v0    # "helper":Lcom/google/android/music/ui/ContainerHeaderView$ArtHelper;
    .end local v1    # "nextUrl":Ljava/lang/String;
    .end local v2    # "slideToRefresh":Landroid/widget/ImageView;
    :cond_3
    iget-object v3, p0, Lcom/google/android/music/ui/ContainerHeaderView$5;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/ContainerHeaderView;->mArtistArt1:Landroid/widget/ImageView;
    invoke-static {v3}, Lcom/google/android/music/ui/ContainerHeaderView;->access$1000(Lcom/google/android/music/ui/ContainerHeaderView;)Landroid/widget/ImageView;

    move-result-object v2

    goto :goto_1

    .line 738
    .restart local v2    # "slideToRefresh":Landroid/widget/ImageView;
    :cond_4
    iget-object v3, p0, Lcom/google/android/music/ui/ContainerHeaderView$5;->this$0:Lcom/google/android/music/ui/ContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/ContainerHeaderView;->mArtistArtHelper1:Lcom/google/android/music/ui/ContainerHeaderView$ArtHelper;
    invoke-static {v3}, Lcom/google/android/music/ui/ContainerHeaderView;->access$900(Lcom/google/android/music/ui/ContainerHeaderView;)Lcom/google/android/music/ui/ContainerHeaderView$ArtHelper;

    move-result-object v0

    goto :goto_2
.end method
