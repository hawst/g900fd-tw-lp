.class public Lcom/google/android/music/ui/GenreRadioGenreListFragment;
.super Lcom/google/android/music/ui/MediaListFragment;
.source "GenreRadioGenreListFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/ui/GenreRadioGenreListFragment$RadioGenresAdapter;
    }
.end annotation


# static fields
.field static final PROJECTION:[Ljava/lang/String;


# instance fields
.field private mGenreLevel:I

.field private mParentGenre:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 25
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "genreServerId"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "name"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "subgenreCount"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "genreArtUris"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/ui/GenreRadioGenreListFragment;->PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/google/android/music/ui/MediaListFragment;-><init>()V

    .line 95
    return-void
.end method

.method public static buildArguments(I)Landroid/os/Bundle;
    .locals 2
    .param p0, "genreLevel"    # I

    .prologue
    .line 39
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 40
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "level"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 41
    return-object v0
.end method


# virtual methods
.method protected initEmptyScreen()V
    .locals 1

    .prologue
    .line 79
    invoke-super {p0}, Lcom/google/android/music/ui/MediaListFragment;->initEmptyScreen()V

    .line 80
    const v0, 0x7f0b02a6

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/GenreRadioGenreListFragment;->setEmptyScreenText(I)V

    .line 81
    const v0, 0x7f0200f2

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/GenreRadioGenreListFragment;->setEmptyImageView(I)V

    .line 82
    return-void
.end method

.method newAsyncCursorAdapter(Landroid/database/Cursor;)Lcom/google/android/music/ui/MediaListCursorAdapter;
    .locals 1
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    .line 68
    new-instance v0, Lcom/google/android/music/ui/GenreRadioGenreListFragment$RadioGenresAdapter;

    invoke-direct {v0, p0, p0, p1}, Lcom/google/android/music/ui/GenreRadioGenreListFragment$RadioGenresAdapter;-><init>(Lcom/google/android/music/ui/GenreRadioGenreListFragment;Lcom/google/android/music/ui/GenreRadioGenreListFragment;Landroid/database/Cursor;)V

    return-object v0
.end method

.method newLoaderCursorAdapter()Lcom/google/android/music/ui/MediaListCursorAdapter;
    .locals 1

    .prologue
    .line 63
    new-instance v0, Lcom/google/android/music/ui/GenreRadioGenreListFragment$RadioGenresAdapter;

    invoke-direct {v0, p0, p0}, Lcom/google/android/music/ui/GenreRadioGenreListFragment$RadioGenresAdapter;-><init>(Lcom/google/android/music/ui/GenreRadioGenreListFragment;Lcom/google/android/music/ui/GenreRadioGenreListFragment;)V

    return-object v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 46
    invoke-virtual {p0}, Lcom/google/android/music/ui/GenreRadioGenreListFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 47
    .local v0, "extra":Landroid/os/Bundle;
    if-nez v0, :cond_0

    .line 48
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Level of genres should be provided to explore genres"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 51
    :cond_0
    const-string v1, "nautilusId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/ui/GenreRadioGenreListFragment;->mParentGenre:Ljava/lang/String;

    .line 52
    const-string v1, "level"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/google/android/music/ui/GenreRadioGenreListFragment;->mGenreLevel:I

    .line 53
    invoke-virtual {p0}, Lcom/google/android/music/ui/GenreRadioGenreListFragment;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_1

    .line 54
    new-instance v1, Lcom/google/android/music/ui/GenreExploreList;

    iget-object v2, p0, Lcom/google/android/music/ui/GenreRadioGenreListFragment;->mParentGenre:Ljava/lang/String;

    invoke-direct {v1, v2}, Lcom/google/android/music/ui/GenreExploreList;-><init>(Ljava/lang/String;)V

    sget-object v2, Lcom/google/android/music/ui/GenreRadioGenreListFragment;->PROJECTION:[Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {p0, v1, v2, v3}, Lcom/google/android/music/ui/GenreRadioGenreListFragment;->init(Lcom/google/android/music/medialist/MediaList;[Ljava/lang/String;Z)V

    .line 58
    :cond_1
    invoke-super {p0, p1}, Lcom/google/android/music/ui/MediaListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 59
    return-void
.end method

.method public onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 6
    .param p1, "l"    # Landroid/widget/ListView;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    const/4 v5, 0x0

    .line 86
    invoke-virtual {p0}, Lcom/google/android/music/ui/GenreRadioGenreListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 87
    .local v0, "activity":Landroid/app/Activity;
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Bundle;

    .line 88
    .local v1, "extra":Landroid/os/Bundle;
    const-string v2, "nautilusId"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "name"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "artUrls"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v2, v3, v4}, Lcom/google/android/music/ui/GenreRadioActivity;->buildStartIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/music/ui/GenreRadioGenreListFragment;->startActivity(Landroid/content/Intent;)V

    .line 92
    invoke-virtual {v0, v5, v5}, Landroid/app/Activity;->overridePendingTransition(II)V

    .line 93
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 73
    invoke-super {p0, p1, p2}, Lcom/google/android/music/ui/MediaListFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 74
    invoke-virtual {p0}, Lcom/google/android/music/ui/GenreRadioGenreListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setFastScrollEnabled(Z)V

    .line 75
    return-void
.end method
