.class public Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu$PopupListAdapter;
.super Landroid/widget/ArrayAdapter;
.source "LegacyPopupMenu.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PopupListAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu$PopupAction;",
        ">;"
    }
.end annotation


# instance fields
.field private final mAllowCustomView:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;ZLjava/util/List;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "allowCustomView"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Z",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu$PopupAction;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 85
    .local p3, "popupActions":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu$PopupAction;>;"
    const v0, 0x7f04007e

    const v1, 0x1020014

    invoke-direct {p0, p1, v0, v1, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;IILjava/util/List;)V

    .line 87
    iput-boolean p2, p0, Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu$PopupListAdapter;->mAllowCustomView:Z

    .line 88
    return-void
.end method


# virtual methods
.method public areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 103
    const/4 v0, 0x1

    return v0
.end method

.method public getItemViewType(I)I
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 97
    invoke-virtual {p0, p1}, Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu$PopupListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu$PopupAction;

    .line 98
    .local v0, "popupAction":Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu$PopupAction;
    # getter for: Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu$PopupAction;->mCustomView:Landroid/view/View;
    invoke-static {v0}, Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu$PopupAction;->access$000(Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu$PopupAction;)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v9, -0x2

    .line 109
    invoke-virtual {p0, p1}, Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu$PopupListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu$PopupAction;

    .line 111
    .local v3, "popupAction":Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu$PopupAction;
    iget-boolean v7, p0, Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu$PopupListAdapter;->mAllowCustomView:Z

    if-eqz v7, :cond_1

    # getter for: Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu$PopupAction;->mCustomView:Landroid/view/View;
    invoke-static {v3}, Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu$PopupAction;->access$000(Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu$PopupAction;)Landroid/view/View;

    move-result-object v7

    if-eqz v7, :cond_1

    .line 112
    # getter for: Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu$PopupAction;->mCachedCustomRow:Landroid/view/View;
    invoke-static {v3}, Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu$PopupAction;->access$100(Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu$PopupAction;)Landroid/view/View;

    move-result-object v7

    if-nez v7, :cond_0

    .line 113
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 114
    .local v1, "infl":Landroid/view/LayoutInflater;
    const v7, 0x7f0400bc

    const/4 v8, 0x0

    invoke-virtual {v1, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 116
    .local v4, "row":Landroid/view/View;
    const v7, 0x1020014

    invoke-virtual {v4, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 117
    .local v5, "textView":Landroid/widget/TextView;
    invoke-virtual {v3}, Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu$PopupAction;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 119
    const v7, 0x1020002

    invoke-virtual {v4, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 120
    .local v0, "holder":Landroid/view/ViewGroup;
    # getter for: Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu$PopupAction;->mCustomView:Landroid/view/View;
    invoke-static {v3}, Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu$PopupAction;->access$000(Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu$PopupAction;)Landroid/view/View;

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/music/utils/ViewUtils;->removeViewFromParent(Landroid/view/View;)V

    .line 122
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v2, v9, v9}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 125
    .local v2, "lp":Landroid/widget/LinearLayout$LayoutParams;
    const/16 v7, 0x15

    iput v7, v2, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 127
    # getter for: Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu$PopupAction;->mCustomView:Landroid/view/View;
    invoke-static {v3}, Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu$PopupAction;->access$000(Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu$PopupAction;)Landroid/view/View;

    move-result-object v7

    invoke-virtual {v0, v7, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 129
    # setter for: Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu$PopupAction;->mCachedCustomRow:Landroid/view/View;
    invoke-static {v3, v4}, Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu$PopupAction;->access$102(Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu$PopupAction;Landroid/view/View;)Landroid/view/View;

    .line 131
    .end local v0    # "holder":Landroid/view/ViewGroup;
    .end local v1    # "infl":Landroid/view/LayoutInflater;
    .end local v2    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    .end local v4    # "row":Landroid/view/View;
    .end local v5    # "textView":Landroid/widget/TextView;
    :cond_0
    # getter for: Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu$PopupAction;->mCachedCustomRow:Landroid/view/View;
    invoke-static {v3}, Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu$PopupAction;->access$100(Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu$PopupAction;)Landroid/view/View;

    move-result-object v6

    .line 135
    .local v6, "view":Landroid/view/View;
    :goto_0
    # getter for: Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu$PopupAction;->mIsEnabled:Z
    invoke-static {v3}, Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu$PopupAction;->access$200(Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu$PopupAction;)Z

    move-result v7

    invoke-virtual {v6, v7}, Landroid/view/View;->setEnabled(Z)V

    .line 136
    return-object v6

    .line 133
    .end local v6    # "view":Landroid/view/View;
    :cond_1
    invoke-super {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    .restart local v6    # "view":Landroid/view/View;
    goto :goto_0
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 92
    iget-boolean v0, p0, Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu$PopupListAdapter;->mAllowCustomView:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onSelect(I)V
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 144
    invoke-virtual {p0, p1}, Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu$PopupListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu$PopupAction;

    # getter for: Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu$PopupAction;->mActionListener:Lcom/google/android/music/ui/cardlib/layout/PlayPopupMenu$OnActionSelectedListener;
    invoke-static {v1}, Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu$PopupAction;->access$300(Lcom/google/android/music/ui/cardlib/layout/LegacyPopupMenu$PopupAction;)Lcom/google/android/music/ui/cardlib/layout/PlayPopupMenu$OnActionSelectedListener;

    move-result-object v0

    .line 145
    .local v0, "listener":Lcom/google/android/music/ui/cardlib/layout/PlayPopupMenu$OnActionSelectedListener;
    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/google/android/music/ui/cardlib/layout/PlayPopupMenu$OnActionSelectedListener;->onActionSelected()V

    .line 146
    :cond_0
    return-void
.end method
