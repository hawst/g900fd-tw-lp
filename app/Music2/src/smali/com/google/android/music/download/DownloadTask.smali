.class public interface abstract Lcom/google/android/music/download/DownloadTask;
.super Ljava/lang/Object;
.source "DownloadTask.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<DownloadRequestType::",
        "Lcom/google/android/music/download/DownloadRequest;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/lang/Runnable;"
    }
.end annotation


# virtual methods
.method public abstract cancel()V
.end method

.method public abstract getDownloadRequest()Lcom/google/android/music/download/DownloadRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TDownloadRequestType;"
        }
    .end annotation
.end method

.method public abstract upgrade(Lcom/google/android/music/download/DownloadTask;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/music/download/DownloadTask",
            "<TDownloadRequestType;>;)Z"
        }
    .end annotation
.end method
