.class public Lcom/google/android/music/PlayPauseButton;
.super Landroid/widget/ImageButton;
.source "PlayPauseButton.java"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mCurrentPlayState:I

.field private final mPauseImage:Landroid/graphics/drawable/Drawable;

.field private final mPlayImage:Landroid/graphics/drawable/Drawable;

.field private final mStopImage:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 20
    const/4 v2, 0x3

    iput v2, p0, Lcom/google/android/music/PlayPauseButton;->mCurrentPlayState:I

    .line 30
    iput-object p1, p0, Lcom/google/android/music/PlayPauseButton;->mContext:Landroid/content/Context;

    .line 31
    sget-object v2, Lcom/google/android/music/R$styleable;->PlayPauseImages:[I

    invoke-virtual {p1, p2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 32
    .local v0, "a":Landroid/content/res/TypedArray;
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 34
    .local v1, "r":Landroid/content/res/Resources;
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/PlayPauseButton;->mPauseImage:Landroid/graphics/drawable/Drawable;

    .line 35
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/PlayPauseButton;->mPlayImage:Landroid/graphics/drawable/Drawable;

    .line 36
    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/PlayPauseButton;->mStopImage:Landroid/graphics/drawable/Drawable;

    .line 38
    invoke-direct {p0}, Lcom/google/android/music/PlayPauseButton;->updateDrawable()V

    .line 39
    return-void
.end method

.method private updateDrawable()V
    .locals 2

    .prologue
    .line 53
    iget v0, p0, Lcom/google/android/music/PlayPauseButton;->mCurrentPlayState:I

    packed-switch v0, :pswitch_data_0

    .line 67
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/music/PlayPauseButton;->postInvalidate()V

    .line 68
    return-void

    .line 55
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/music/PlayPauseButton;->mStopImage:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v0}, Lcom/google/android/music/PlayPauseButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 56
    iget-object v0, p0, Lcom/google/android/music/PlayPauseButton;->mContext:Landroid/content/Context;

    const v1, 0x7f0b01e7

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/music/PlayPauseButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 59
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/music/PlayPauseButton;->mPlayImage:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v0}, Lcom/google/android/music/PlayPauseButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 60
    iget-object v0, p0, Lcom/google/android/music/PlayPauseButton;->mContext:Landroid/content/Context;

    const v1, 0x7f0b01e5

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/music/PlayPauseButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 63
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/music/PlayPauseButton;->mPauseImage:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v0}, Lcom/google/android/music/PlayPauseButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 64
    iget-object v0, p0, Lcom/google/android/music/PlayPauseButton;->mContext:Landroid/content/Context;

    const v1, 0x7f0b01e6

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/music/PlayPauseButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 53
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public setCurrentPlayState(I)V
    .locals 1
    .param p1, "state"    # I

    .prologue
    .line 46
    iget v0, p0, Lcom/google/android/music/PlayPauseButton;->mCurrentPlayState:I

    if-eq p1, v0, :cond_0

    .line 47
    iput p1, p0, Lcom/google/android/music/PlayPauseButton;->mCurrentPlayState:I

    .line 48
    invoke-direct {p0}, Lcom/google/android/music/PlayPauseButton;->updateDrawable()V

    .line 50
    :cond_0
    return-void
.end method
