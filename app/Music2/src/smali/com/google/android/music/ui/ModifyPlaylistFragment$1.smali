.class Lcom/google/android/music/ui/ModifyPlaylistFragment$1;
.super Ljava/lang/Object;
.source "ModifyPlaylistFragment.java"

# interfaces
.implements Lcom/google/android/music/utils/async/AsyncRunner;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/ModifyPlaylistFragment;->launchSetPlaylistNameTask()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private volatile mDescription:Ljava/lang/String;

.field private volatile mName:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/music/ui/ModifyPlaylistFragment;

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/ModifyPlaylistFragment;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 213
    iput-object p1, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment$1;->this$0:Lcom/google/android/music/ui/ModifyPlaylistFragment;

    iput-object p2, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment$1;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public backgroundTask()V
    .locals 2

    .prologue
    .line 219
    iget-object v0, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment$1;->this$0:Lcom/google/android/music/ui/ModifyPlaylistFragment;

    # getter for: Lcom/google/android/music/ui/ModifyPlaylistFragment;->mPlaylistSongList:Lcom/google/android/music/medialist/PlaylistSongList;
    invoke-static {v0}, Lcom/google/android/music/ui/ModifyPlaylistFragment;->access$000(Lcom/google/android/music/ui/ModifyPlaylistFragment;)Lcom/google/android/music/medialist/PlaylistSongList;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment$1;->val$context:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/google/android/music/medialist/PlaylistSongList;->refreshMetaData(Landroid/content/Context;)V

    .line 220
    iget-object v0, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment$1;->this$0:Lcom/google/android/music/ui/ModifyPlaylistFragment;

    # getter for: Lcom/google/android/music/ui/ModifyPlaylistFragment;->mPlaylistSongList:Lcom/google/android/music/medialist/PlaylistSongList;
    invoke-static {v0}, Lcom/google/android/music/ui/ModifyPlaylistFragment;->access$000(Lcom/google/android/music/ui/ModifyPlaylistFragment;)Lcom/google/android/music/medialist/PlaylistSongList;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment$1;->val$context:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/google/android/music/medialist/PlaylistSongList;->getName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment$1;->mName:Ljava/lang/String;

    .line 221
    iget-object v0, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment$1;->this$0:Lcom/google/android/music/ui/ModifyPlaylistFragment;

    # getter for: Lcom/google/android/music/ui/ModifyPlaylistFragment;->mPlaylistSongList:Lcom/google/android/music/medialist/PlaylistSongList;
    invoke-static {v0}, Lcom/google/android/music/ui/ModifyPlaylistFragment;->access$000(Lcom/google/android/music/ui/ModifyPlaylistFragment;)Lcom/google/android/music/medialist/PlaylistSongList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/medialist/PlaylistSongList;->getDescription()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment$1;->mDescription:Ljava/lang/String;

    .line 222
    return-void
.end method

.method public taskCompleted()V
    .locals 2

    .prologue
    .line 226
    iget-object v0, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment$1;->this$0:Lcom/google/android/music/ui/ModifyPlaylistFragment;

    # getter for: Lcom/google/android/music/ui/ModifyPlaylistFragment;->mIsDismissed:Z
    invoke-static {v0}, Lcom/google/android/music/ui/ModifyPlaylistFragment;->access$100(Lcom/google/android/music/ui/ModifyPlaylistFragment;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 236
    :cond_0
    :goto_0
    return-void

    .line 229
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment$1;->mName:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 230
    iget-object v0, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment$1;->this$0:Lcom/google/android/music/ui/ModifyPlaylistFragment;

    # getter for: Lcom/google/android/music/ui/ModifyPlaylistFragment;->mPlaylistNameEditor:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/google/android/music/ui/ModifyPlaylistFragment;->access$200(Lcom/google/android/music/ui/ModifyPlaylistFragment;)Landroid/widget/EditText;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment$1;->mName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 232
    :cond_2
    iget-object v0, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment$1;->mDescription:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 233
    iget-object v0, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment$1;->this$0:Lcom/google/android/music/ui/ModifyPlaylistFragment;

    # getter for: Lcom/google/android/music/ui/ModifyPlaylistFragment;->mPlaylistDescriptionEditor:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/google/android/music/ui/ModifyPlaylistFragment;->access$300(Lcom/google/android/music/ui/ModifyPlaylistFragment;)Landroid/widget/EditText;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment$1;->mDescription:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
