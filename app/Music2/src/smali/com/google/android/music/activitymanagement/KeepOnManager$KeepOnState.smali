.class public interface abstract Lcom/google/android/music/activitymanagement/KeepOnManager$KeepOnState;
.super Ljava/lang/Object;
.source "KeepOnManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/activitymanagement/KeepOnManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "KeepOnState"
.end annotation


# virtual methods
.method public abstract getDownloadFraction()F
.end method

.method public abstract invalidate()V
.end method

.method public abstract isPinnable()Z
.end method

.method public abstract isPinned()Z
.end method

.method public abstract setDownloadFraction(F)V
.end method

.method public abstract setIsUnwindAnimationInProcess(Z)V
.end method

.method public abstract setPinnable(Z)V
.end method

.method public abstract setPinned(Z)V
.end method
