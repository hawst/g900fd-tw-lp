.class Lcom/google/android/music/ui/BaseActivity$13;
.super Landroid/content/BroadcastReceiver;
.source "BaseActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/BaseActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/BaseActivity;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/BaseActivity;)V
    .locals 0

    .prologue
    .line 1411
    iput-object p1, p0, Lcom/google/android/music/ui/BaseActivity$13;->this$0:Lcom/google/android/music/ui/BaseActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const-wide/16 v6, -0x1

    const/4 v9, -0x1

    .line 1415
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 1416
    .local v0, "action":Ljava/lang/String;
    const-string v4, "com.android.music.playbackfailed"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1417
    const-string v4, "errorType"

    invoke-virtual {p2, v4, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 1420
    .local v2, "errorType":I
    const-string v4, "id"

    invoke-virtual {p2, v4, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    .line 1421
    .local v3, "extraSongId":Ljava/lang/Long;
    const/4 v1, 0x0

    .line 1423
    .local v1, "audioId":Lcom/google/android/music/download/ContentIdentifier;
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v4, v4, v6

    if-eqz v4, :cond_0

    .line 1424
    new-instance v1, Lcom/google/android/music/download/ContentIdentifier;

    .end local v1    # "audioId":Lcom/google/android/music/download/ContentIdentifier;
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {}, Lcom/google/android/music/download/ContentIdentifier$Domain;->values()[Lcom/google/android/music/download/ContentIdentifier$Domain;

    move-result-object v6

    const-string v7, "domain"

    const/4 v8, 0x0

    invoke-virtual {p2, v7, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v7

    aget-object v6, v6, v7

    invoke-direct {v1, v4, v5, v6}, Lcom/google/android/music/download/ContentIdentifier;-><init>(JLcom/google/android/music/download/ContentIdentifier$Domain;)V

    .line 1429
    .restart local v1    # "audioId":Lcom/google/android/music/download/ContentIdentifier;
    :cond_0
    if-ne v2, v9, :cond_1

    .line 1430
    const-string v4, "MusicBaseActivity"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Expected extra: errorType for action: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " in mMediaPlayerBroadcastReceiver"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1439
    .end local v1    # "audioId":Lcom/google/android/music/download/ContentIdentifier;
    .end local v2    # "errorType":I
    .end local v3    # "extraSongId":Ljava/lang/Long;
    :goto_0
    return-void

    .line 1435
    .restart local v1    # "audioId":Lcom/google/android/music/download/ContentIdentifier;
    .restart local v2    # "errorType":I
    .restart local v3    # "extraSongId":Ljava/lang/Long;
    :cond_1
    iget-object v4, p0, Lcom/google/android/music/ui/BaseActivity$13;->this$0:Lcom/google/android/music/ui/BaseActivity;

    invoke-virtual {v4, v1, v2}, Lcom/google/android/music/ui/BaseActivity;->alertFailureIfNecessary(Lcom/google/android/music/download/ContentIdentifier;I)V

    goto :goto_0

    .line 1437
    .end local v1    # "audioId":Lcom/google/android/music/download/ContentIdentifier;
    .end local v2    # "errorType":I
    .end local v3    # "extraSongId":Ljava/lang/Long;
    :cond_2
    const-string v4, "MusicBaseActivity"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unknown action coming to mMediaPlayerBroadcastReceiver: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
