.class public Lcom/google/android/music/dial/SsdpClient;
.super Ljava/lang/Object;
.source "SsdpClient.java"


# static fields
.field private static final LOGV:Z


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mListener:Lcom/google/android/music/dial/SsdpListener;

.field private mMulticastAddress:Ljava/net/InetAddress;

.field private final mNetworkInterface:Ljava/net/NetworkInterface;

.field private mReader:Lcom/google/android/music/dial/SsdpReader;

.field private mReaderThread:Ljava/lang/Thread;

.field private final mSearchTarget:Ljava/lang/String;

.field private mSocket:Ljava/net/MulticastSocket;

.field private mWriter:Lcom/google/android/music/dial/SsdpWriter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->DIAL_MRP_COMMUNICATION:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/dial/SsdpClient;->LOGV:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/net/NetworkInterface;Ljava/lang/String;Lcom/google/android/music/dial/SsdpListener;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "networkInterface"    # Ljava/net/NetworkInterface;
    .param p3, "searchTarget"    # Ljava/lang/String;
    .param p4, "listener"    # Lcom/google/android/music/dial/SsdpListener;

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object p1, p0, Lcom/google/android/music/dial/SsdpClient;->mContext:Landroid/content/Context;

    .line 47
    iput-object p2, p0, Lcom/google/android/music/dial/SsdpClient;->mNetworkInterface:Ljava/net/NetworkInterface;

    .line 48
    iput-object p3, p0, Lcom/google/android/music/dial/SsdpClient;->mSearchTarget:Ljava/lang/String;

    .line 49
    iput-object p4, p0, Lcom/google/android/music/dial/SsdpClient;->mListener:Lcom/google/android/music/dial/SsdpListener;

    .line 50
    return-void
.end method

.method private stopAndJoinThread(Ljava/lang/Thread;)V
    .locals 1
    .param p1, "thread"    # Ljava/lang/Thread;

    .prologue
    .line 118
    invoke-virtual {p1}, Ljava/lang/Thread;->interrupt()V

    .line 121
    :goto_0
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Thread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 127
    return-void

    .line 123
    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public initialize()Z
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 63
    :try_start_0
    const-string v0, "239.255.255.250"

    invoke-static {v0}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/dial/SsdpClient;->mMulticastAddress:Ljava/net/InetAddress;

    .line 64
    new-instance v0, Ljava/net/MulticastSocket;

    invoke-direct {v0}, Ljava/net/MulticastSocket;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/dial/SsdpClient;->mSocket:Ljava/net/MulticastSocket;

    .line 65
    iget-object v0, p0, Lcom/google/android/music/dial/SsdpClient;->mSocket:Ljava/net/MulticastSocket;

    iget-object v1, p0, Lcom/google/android/music/dial/SsdpClient;->mNetworkInterface:Ljava/net/NetworkInterface;

    invoke-virtual {v0, v1}, Ljava/net/MulticastSocket;->setNetworkInterface(Ljava/net/NetworkInterface;)V

    .line 66
    iget-object v0, p0, Lcom/google/android/music/dial/SsdpClient;->mSocket:Ljava/net/MulticastSocket;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/net/MulticastSocket;->setTimeToLive(I)V

    .line 67
    iget-object v0, p0, Lcom/google/android/music/dial/SsdpClient;->mSocket:Ljava/net/MulticastSocket;

    iget-object v1, p0, Lcom/google/android/music/dial/SsdpClient;->mMulticastAddress:Ljava/net/InetAddress;

    invoke-virtual {v0, v1}, Ljava/net/MulticastSocket;->joinGroup(Ljava/net/InetAddress;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 74
    new-instance v0, Lcom/google/android/music/dial/SsdpReader;

    iget-object v1, p0, Lcom/google/android/music/dial/SsdpClient;->mSocket:Ljava/net/MulticastSocket;

    iget-object v2, p0, Lcom/google/android/music/dial/SsdpClient;->mListener:Lcom/google/android/music/dial/SsdpListener;

    invoke-direct {v0, v1, v2}, Lcom/google/android/music/dial/SsdpReader;-><init>(Ljava/net/MulticastSocket;Lcom/google/android/music/dial/SsdpListener;)V

    iput-object v0, p0, Lcom/google/android/music/dial/SsdpClient;->mReader:Lcom/google/android/music/dial/SsdpReader;

    .line 75
    new-instance v0, Ljava/lang/Thread;

    iget-object v1, p0, Lcom/google/android/music/dial/SsdpClient;->mReader:Lcom/google/android/music/dial/SsdpReader;

    const-string v2, "SsdpReader"

    invoke-direct {v0, v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/music/dial/SsdpClient;->mReaderThread:Ljava/lang/Thread;

    .line 76
    iget-object v0, p0, Lcom/google/android/music/dial/SsdpClient;->mReaderThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 78
    new-instance v0, Lcom/google/android/music/dial/SsdpWriter;

    iget-object v1, p0, Lcom/google/android/music/dial/SsdpClient;->mSocket:Ljava/net/MulticastSocket;

    iget-object v2, p0, Lcom/google/android/music/dial/SsdpClient;->mSearchTarget:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/music/dial/SsdpClient;->mMulticastAddress:Ljava/net/InetAddress;

    const/16 v4, 0x76c

    iget-object v5, p0, Lcom/google/android/music/dial/SsdpClient;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/google/android/music/dial/Utils;->getSsdpMaxWaitTimeSeconds(Landroid/content/Context;)I

    move-result v5

    iget-object v6, p0, Lcom/google/android/music/dial/SsdpClient;->mListener:Lcom/google/android/music/dial/SsdpListener;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/music/dial/SsdpWriter;-><init>(Ljava/net/MulticastSocket;Ljava/lang/String;Ljava/net/InetAddress;IILcom/google/android/music/dial/SsdpListener;)V

    iput-object v0, p0, Lcom/google/android/music/dial/SsdpClient;->mWriter:Lcom/google/android/music/dial/SsdpWriter;

    .line 82
    :try_start_1
    iget-object v0, p0, Lcom/google/android/music/dial/SsdpClient;->mWriter:Lcom/google/android/music/dial/SsdpWriter;

    invoke-virtual {v0}, Lcom/google/android/music/dial/SsdpWriter;->initialize()V
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_1

    .line 88
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 68
    :catch_0
    move-exception v7

    .line 69
    .local v7, "e":Ljava/io/IOException;
    const-string v0, "SsdpClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error initializing multicast socket for interface "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/dial/SsdpClient;->mNetworkInterface:Ljava/net/NetworkInterface;

    invoke-virtual {v2}, Ljava/net/NetworkInterface;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v0, v8

    .line 71
    goto :goto_0

    .line 83
    .end local v7    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v7

    .line 84
    .local v7, "e":Ljava/io/UnsupportedEncodingException;
    const-string v0, "SsdpClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error initializing SsdpWriter for interface "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/dial/SsdpClient;->mNetworkInterface:Ljava/net/NetworkInterface;

    invoke-virtual {v2}, Ljava/net/NetworkInterface;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v0, v8

    .line 86
    goto :goto_0
.end method

.method public shutdown()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 109
    iget-object v0, p0, Lcom/google/android/music/dial/SsdpClient;->mReader:Lcom/google/android/music/dial/SsdpReader;

    invoke-virtual {v0}, Lcom/google/android/music/dial/SsdpReader;->setDone()V

    .line 110
    iget-object v0, p0, Lcom/google/android/music/dial/SsdpClient;->mReaderThread:Ljava/lang/Thread;

    invoke-direct {p0, v0}, Lcom/google/android/music/dial/SsdpClient;->stopAndJoinThread(Ljava/lang/Thread;)V

    .line 112
    iget-object v0, p0, Lcom/google/android/music/dial/SsdpClient;->mSocket:Ljava/net/MulticastSocket;

    iget-object v1, p0, Lcom/google/android/music/dial/SsdpClient;->mMulticastAddress:Ljava/net/InetAddress;

    invoke-virtual {v0, v1}, Ljava/net/MulticastSocket;->leaveGroup(Ljava/net/InetAddress;)V

    .line 113
    iget-object v0, p0, Lcom/google/android/music/dial/SsdpClient;->mSocket:Ljava/net/MulticastSocket;

    invoke-virtual {v0}, Ljava/net/MulticastSocket;->close()V

    .line 114
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/dial/SsdpClient;->mSocket:Ljava/net/MulticastSocket;

    .line 115
    return-void
.end method

.method public startScan()V
    .locals 2

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/music/dial/SsdpClient;->mWriter:Lcom/google/android/music/dial/SsdpWriter;

    invoke-virtual {v0}, Lcom/google/android/music/dial/SsdpWriter;->sendSearch()V

    .line 96
    sget-boolean v0, Lcom/google/android/music/dial/SsdpClient;->LOGV:Z

    if-eqz v0, :cond_0

    .line 97
    const-string v0, "SsdpClient"

    const-string v1, "Sent M-SEARCH"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    :cond_0
    return-void
.end method
