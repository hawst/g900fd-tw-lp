.class Lcom/google/android/music/art/ArtUtils$OverlayKey;
.super Ljava/lang/Object;
.source "ArtUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/art/ArtUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "OverlayKey"
.end annotation


# instance fields
.field private final mCircleCropped:Z

.field private final mHeight:I

.field private final mWidth:I


# direct methods
.method private constructor <init>(IIZ)V
    .locals 0
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "circleCropped"    # Z

    .prologue
    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 93
    iput p1, p0, Lcom/google/android/music/art/ArtUtils$OverlayKey;->mWidth:I

    .line 94
    iput p2, p0, Lcom/google/android/music/art/ArtUtils$OverlayKey;->mHeight:I

    .line 95
    iput-boolean p3, p0, Lcom/google/android/music/art/ArtUtils$OverlayKey;->mCircleCropped:Z

    .line 96
    return-void
.end method

.method synthetic constructor <init>(IIZLcom/google/android/music/art/ArtUtils$1;)V
    .locals 0
    .param p1, "x0"    # I
    .param p2, "x1"    # I
    .param p3, "x2"    # Z
    .param p4, "x3"    # Lcom/google/android/music/art/ArtUtils$1;

    .prologue
    .line 87
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/music/art/ArtUtils$OverlayKey;-><init>(IIZ)V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 100
    if-ne p0, p1, :cond_1

    .line 109
    :cond_0
    :goto_0
    return v1

    .line 101
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    :cond_2
    move v1, v2

    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 103
    check-cast v0, Lcom/google/android/music/art/ArtUtils$OverlayKey;

    .line 105
    .local v0, "that":Lcom/google/android/music/art/ArtUtils$OverlayKey;
    iget-boolean v3, p0, Lcom/google/android/music/art/ArtUtils$OverlayKey;->mCircleCropped:Z

    iget-boolean v4, v0, Lcom/google/android/music/art/ArtUtils$OverlayKey;->mCircleCropped:Z

    if-eq v3, v4, :cond_4

    move v1, v2

    goto :goto_0

    .line 106
    :cond_4
    iget v3, p0, Lcom/google/android/music/art/ArtUtils$OverlayKey;->mHeight:I

    iget v4, v0, Lcom/google/android/music/art/ArtUtils$OverlayKey;->mHeight:I

    if-eq v3, v4, :cond_5

    move v1, v2

    goto :goto_0

    .line 107
    :cond_5
    iget v3, p0, Lcom/google/android/music/art/ArtUtils$OverlayKey;->mWidth:I

    iget v4, v0, Lcom/google/android/music/art/ArtUtils$OverlayKey;->mWidth:I

    if-eq v3, v4, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 114
    iget v0, p0, Lcom/google/android/music/art/ArtUtils$OverlayKey;->mWidth:I

    .line 115
    .local v0, "result":I
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/android/music/art/ArtUtils$OverlayKey;->mHeight:I

    add-int v0, v1, v2

    .line 116
    mul-int/lit8 v2, v0, 0x1f

    iget-boolean v1, p0, Lcom/google/android/music/art/ArtUtils$OverlayKey;->mCircleCropped:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    add-int v0, v2, v1

    .line 117
    return v0

    .line 116
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
