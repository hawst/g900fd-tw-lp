.class Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$EditPlaylist;
.super Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$UIThreadMenuEntry;
.source "DocumentMenuHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EditPlaylist"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;Landroid/content/res/Resources;)V
    .locals 2
    .param p2, "res"    # Landroid/content/res/Resources;

    .prologue
    .line 1012
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$EditPlaylist;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    .line 1013
    const v0, 0x7f0e0024

    const v1, 0x7f0b00ca

    invoke-direct {p0, p2, v0, v1}, Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$UIThreadMenuEntry;-><init>(Landroid/content/res/Resources;II)V

    .line 1014
    return-void
.end method


# virtual methods
.method public onActionSelected()V
    .locals 10

    .prologue
    .line 1018
    iget-object v8, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$EditPlaylist;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v8, v8, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v8}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v8

    sget-object v9, Lcom/google/android/music/ui/cardlib/model/Document$Type;->PLAYLIST:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-ne v8, v9, :cond_3

    .line 1019
    iget-object v8, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$EditPlaylist;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v8, v8, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mMusicFragment:Lcom/google/android/music/ui/MusicFragment;

    invoke-interface {v8}, Lcom/google/android/music/ui/MusicFragment;->getFragment()Landroid/support/v4/app/Fragment;

    move-result-object v8

    invoke-virtual {v8}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 1020
    .local v0, "activity":Landroid/support/v4/app/FragmentActivity;
    if-eqz v0, :cond_1

    .line 1021
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v5

    .line 1022
    .local v5, "manager":Lcom/google/android/music/ui/UIStateManager;
    invoke-virtual {v5}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v6

    .line 1024
    .local v6, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    invoke-virtual {v6}, Lcom/google/android/music/preferences/MusicPreferences;->hasStreamingAccount()Z

    move-result v8

    if-eqz v8, :cond_0

    iget-object v8, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$EditPlaylist;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v8, v8, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v8}, Lcom/google/android/music/ui/cardlib/model/Document;->getSourceAccount()I

    move-result v8

    if-nez v8, :cond_2

    :cond_0
    const/4 v4, 0x1

    .line 1026
    .local v4, "isSideloaded":Z
    :goto_0
    iget-object v8, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$EditPlaylist;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v8, v8, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    iget-object v9, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$EditPlaylist;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v9, v9, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mContext:Landroid/app/Activity;

    invoke-virtual {v8, v9}, Lcom/google/android/music/ui/cardlib/model/Document;->getSongList(Landroid/content/Context;)Lcom/google/android/music/medialist/SongList;

    move-result-object v7

    check-cast v7, Lcom/google/android/music/medialist/PlaylistSongList;

    .line 1027
    .local v7, "songList":Lcom/google/android/music/medialist/PlaylistSongList;
    new-instance v3, Lcom/google/android/music/ui/ModifyPlaylistFragment;

    invoke-direct {v3}, Lcom/google/android/music/ui/ModifyPlaylistFragment;-><init>()V

    .line 1028
    .local v3, "fragment":Landroid/support/v4/app/DialogFragment;
    iget-object v8, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$EditPlaylist;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v8, v8, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v8}, Lcom/google/android/music/ui/cardlib/model/Document;->getPlaylistShareState()I

    move-result v8

    invoke-static {v7, v8, v4}, Lcom/google/android/music/ui/ModifyPlaylistFragment;->makeEditArgs(Lcom/google/android/music/medialist/PlaylistSongList;IZ)Landroid/os/Bundle;

    move-result-object v1

    .line 1030
    .local v1, "args":Landroid/os/Bundle;
    invoke-virtual {v3, v1}, Landroid/support/v4/app/DialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 1031
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    .line 1032
    .local v2, "fm":Landroid/support/v4/app/FragmentManager;
    const-string v8, "ModifyPlaylistFragment"

    invoke-virtual {v3, v2, v8}, Landroid/support/v4/app/DialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 1037
    .end local v0    # "activity":Landroid/support/v4/app/FragmentActivity;
    .end local v1    # "args":Landroid/os/Bundle;
    .end local v2    # "fm":Landroid/support/v4/app/FragmentManager;
    .end local v3    # "fragment":Landroid/support/v4/app/DialogFragment;
    .end local v4    # "isSideloaded":Z
    .end local v5    # "manager":Lcom/google/android/music/ui/UIStateManager;
    .end local v6    # "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    .end local v7    # "songList":Lcom/google/android/music/medialist/PlaylistSongList;
    :cond_1
    :goto_1
    return-void

    .line 1024
    .restart local v0    # "activity":Landroid/support/v4/app/FragmentActivity;
    .restart local v5    # "manager":Lcom/google/android/music/ui/UIStateManager;
    .restart local v6    # "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    :cond_2
    const/4 v4, 0x0

    goto :goto_0

    .line 1035
    .end local v0    # "activity":Landroid/support/v4/app/FragmentActivity;
    .end local v5    # "manager":Lcom/google/android/music/ui/UIStateManager;
    .end local v6    # "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    :cond_3
    const-string v8, "DocumentMenuHandler"

    const-string v9, "Unexpected doc type"

    invoke-static {v8, v9}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method
