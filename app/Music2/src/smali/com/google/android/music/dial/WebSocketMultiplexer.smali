.class public Lcom/google/android/music/dial/WebSocketMultiplexer;
.super Ljava/lang/Object;
.source "WebSocketMultiplexer.java"


# static fields
.field private static final LOGV:Z

.field private static sInstance:Lcom/google/android/music/dial/WebSocketMultiplexer;


# instance fields
.field private volatile mAborted:Z

.field private mCharset:Ljava/nio/charset/Charset;

.field private final mNewSockets:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/google/android/music/dial/WebSocket;",
            ">;"
        }
    .end annotation
.end field

.field private final mNewSocketsAdded:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final mRandom:Ljava/security/SecureRandom;

.field private mSelector:Ljava/nio/channels/Selector;

.field private volatile mSelectorThrowable:Ljava/lang/Throwable;

.field private volatile mShouldStop:Z

.field private final mSockets:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/google/android/music/dial/WebSocket;",
            ">;"
        }
    .end annotation
.end field

.field private mStartupLatch:Ljava/util/concurrent/CountDownLatch;

.field private volatile mThread:Ljava/lang/Thread;

.field private final mWakeLock:Landroid/os/PowerManager$WakeLock;

.field private mWakeLockAcquisitionTime:J

.field private mWakeLockHeld:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->DIAL_MRP_COMMUNICATION:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/dial/WebSocketMultiplexer;->LOGV:Z

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    iput-object v2, p0, Lcom/google/android/music/dial/WebSocketMultiplexer;->mSockets:Ljava/util/LinkedList;

    .line 81
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    iput-object v2, p0, Lcom/google/android/music/dial/WebSocketMultiplexer;->mNewSockets:Ljava/util/LinkedList;

    .line 82
    new-instance v2, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v2, v4}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v2, p0, Lcom/google/android/music/dial/WebSocketMultiplexer;->mNewSocketsAdded:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 83
    new-instance v2, Ljava/security/SecureRandom;

    invoke-direct {v2}, Ljava/security/SecureRandom;-><init>()V

    iput-object v2, p0, Lcom/google/android/music/dial/WebSocketMultiplexer;->mRandom:Ljava/security/SecureRandom;

    .line 84
    const-string v2, "power"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/PowerManager;

    .line 85
    .local v1, "pm":Landroid/os/PowerManager;
    const/4 v2, 0x1

    const-string v3, "WebSocketMultiplexer"

    invoke-virtual {v1, v2, v3}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/dial/WebSocketMultiplexer;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 86
    iget-object v2, p0, Lcom/google/android/music/dial/WebSocketMultiplexer;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v2, v4}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    .line 88
    :try_start_0
    const-string v2, "UTF-8"

    invoke-static {v2}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/dial/WebSocketMultiplexer;->mCharset:Ljava/nio/charset/Charset;
    :try_end_0
    .catch Ljava/nio/charset/IllegalCharsetNameException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/nio/charset/UnsupportedCharsetException; {:try_start_0 .. :try_end_0} :catch_1

    .line 96
    :goto_0
    return-void

    .line 89
    :catch_0
    move-exception v0

    .line 91
    .local v0, "e":Ljava/nio/charset/IllegalCharsetNameException;
    const-string v2, "WebSocketMultiplexer"

    const-string v3, "Can\'t find charset UTF-8"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 92
    .end local v0    # "e":Ljava/nio/charset/IllegalCharsetNameException;
    :catch_1
    move-exception v0

    .line 94
    .local v0, "e":Ljava/nio/charset/UnsupportedCharsetException;
    const-string v2, "WebSocketMultiplexer"

    const-string v3, "Can\'t find charset UTF-8"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/google/android/music/dial/WebSocketMultiplexer;)Ljava/util/concurrent/CountDownLatch;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/dial/WebSocketMultiplexer;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/music/dial/WebSocketMultiplexer;->mStartupLatch:Ljava/util/concurrent/CountDownLatch;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/music/dial/WebSocketMultiplexer;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/dial/WebSocketMultiplexer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/music/dial/WebSocketMultiplexer;->selectorLoop()V

    return-void
.end method

.method static synthetic access$202(Lcom/google/android/music/dial/WebSocketMultiplexer;Ljava/lang/Throwable;)Ljava/lang/Throwable;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/dial/WebSocketMultiplexer;
    .param p1, "x1"    # Ljava/lang/Throwable;

    .prologue
    .line 35
    iput-object p1, p0, Lcom/google/android/music/dial/WebSocketMultiplexer;->mSelectorThrowable:Ljava/lang/Throwable;

    return-object p1
.end method

.method static synthetic access$302(Lcom/google/android/music/dial/WebSocketMultiplexer;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/dial/WebSocketMultiplexer;
    .param p1, "x1"    # Z

    .prologue
    .line 35
    iput-boolean p1, p0, Lcom/google/android/music/dial/WebSocketMultiplexer;->mAborted:Z

    return p1
.end method

.method static synthetic access$400(Lcom/google/android/music/dial/WebSocketMultiplexer;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/dial/WebSocketMultiplexer;

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/music/dial/WebSocketMultiplexer;->disposeWakeLock()V

    return-void
.end method

.method static synthetic access$502(Lcom/google/android/music/dial/WebSocketMultiplexer;Ljava/lang/Thread;)Ljava/lang/Thread;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/dial/WebSocketMultiplexer;
    .param p1, "x1"    # Ljava/lang/Thread;

    .prologue
    .line 35
    iput-object p1, p0, Lcom/google/android/music/dial/WebSocketMultiplexer;->mThread:Ljava/lang/Thread;

    return-object p1
.end method

.method private declared-synchronized acquireWakeLock()V
    .locals 2

    .prologue
    .line 388
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/music/dial/WebSocketMultiplexer;->mWakeLockHeld:Z

    if-nez v0, :cond_1

    .line 389
    sget-boolean v0, Lcom/google/android/music/dial/WebSocketMultiplexer;->LOGV:Z

    if-eqz v0, :cond_0

    .line 390
    const-string v0, "WebSocketMultiplexer"

    const-string v1, "acquireWakeLock"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 392
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/dial/WebSocketMultiplexer;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 393
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/dial/WebSocketMultiplexer;->mWakeLockHeld:Z

    .line 395
    :cond_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/music/dial/WebSocketMultiplexer;->mWakeLockAcquisitionTime:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 396
    monitor-exit p0

    return-void

    .line 388
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private checkRunning()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 368
    iget-boolean v2, p0, Lcom/google/android/music/dial/WebSocketMultiplexer;->mAborted:Z

    if-eqz v2, :cond_1

    .line 369
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 370
    .local v0, "sb":Ljava/lang/StringBuffer;
    const-string v2, "selector thread aborted due to "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 371
    iget-object v2, p0, Lcom/google/android/music/dial/WebSocketMultiplexer;->mSelectorThrowable:Ljava/lang/Throwable;

    if-eqz v2, :cond_0

    .line 372
    iget-object v2, p0, Lcom/google/android/music/dial/WebSocketMultiplexer;->mSelectorThrowable:Ljava/lang/Throwable;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 373
    iget-object v2, p0, Lcom/google/android/music/dial/WebSocketMultiplexer;->mSelectorThrowable:Ljava/lang/Throwable;

    invoke-virtual {v2}, Ljava/lang/Throwable;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v1

    .line 374
    .local v1, "stack":[Ljava/lang/StackTraceElement;
    const-string v2, " at "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    aget-object v3, v1, v4

    invoke-virtual {v3}, Ljava/lang/StackTraceElement;->getFileName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const/16 v3, 0x3a

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v2

    aget-object v3, v1, v4

    invoke-virtual {v3}, Ljava/lang/StackTraceElement;->getLineNumber()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 379
    .end local v1    # "stack":[Ljava/lang/StackTraceElement;
    :goto_0
    new-instance v2, Ljava/lang/IllegalStateException;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 377
    :cond_0
    const-string v2, "unknown condition"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 382
    .end local v0    # "sb":Ljava/lang/StringBuffer;
    :cond_1
    iget-object v2, p0, Lcom/google/android/music/dial/WebSocketMultiplexer;->mThread:Ljava/lang/Thread;

    if-nez v2, :cond_2

    .line 383
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "not started; call start()"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 385
    :cond_2
    return-void
.end method

.method private disposeWakeLock()V
    .locals 2

    .prologue
    .line 417
    const-wide/16 v0, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/dial/WebSocketMultiplexer;->releaseWakeLockIfHeldFor(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 418
    const-string v0, "WebSocketMultiplexer"

    const-string v1, "disposeWakeLock: Unbalanced call in releasing the wake lock."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 420
    :cond_0
    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/google/android/music/dial/WebSocketMultiplexer;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 73
    sget-object v0, Lcom/google/android/music/dial/WebSocketMultiplexer;->sInstance:Lcom/google/android/music/dial/WebSocketMultiplexer;

    if-nez v0, :cond_0

    .line 74
    new-instance v0, Lcom/google/android/music/dial/WebSocketMultiplexer;

    invoke-direct {v0, p0}, Lcom/google/android/music/dial/WebSocketMultiplexer;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/music/dial/WebSocketMultiplexer;->sInstance:Lcom/google/android/music/dial/WebSocketMultiplexer;

    .line 76
    :cond_0
    sget-object v0, Lcom/google/android/music/dial/WebSocketMultiplexer;->sInstance:Lcom/google/android/music/dial/WebSocketMultiplexer;

    return-object v0
.end method

.method private declared-synchronized releaseWakeLockIfHeldFor(J)Z
    .locals 7
    .param p1, "interval"    # J

    .prologue
    const/4 v0, 0x0

    .line 403
    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lcom/google/android/music/dial/WebSocketMultiplexer;->mWakeLockHeld:Z

    if-eqz v1, :cond_1

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/music/dial/WebSocketMultiplexer;->mWakeLockAcquisitionTime:J

    sub-long/2addr v2, v4

    cmp-long v1, v2, p1

    if-lez v1, :cond_1

    .line 405
    sget-boolean v0, Lcom/google/android/music/dial/WebSocketMultiplexer;->LOGV:Z

    if-eqz v0, :cond_0

    .line 406
    const-string v0, "WebSocketMultiplexer"

    const-string v1, "releaseWakeLock"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 408
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/dial/WebSocketMultiplexer;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 409
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/dial/WebSocketMultiplexer;->mWakeLockHeld:Z

    .line 410
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/music/dial/WebSocketMultiplexer;->mWakeLockAcquisitionTime:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 411
    const/4 v0, 0x1

    .line 413
    :cond_1
    monitor-exit p0

    return v0

    .line 403
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private selectorLoop()V
    .locals 24
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 209
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    .line 211
    .local v18, "unconnectableSockets":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/dial/WebSocket;>;"
    :cond_0
    :goto_0
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/music/dial/WebSocketMultiplexer;->mShouldStop:Z

    move/from16 v19, v0

    if-nez v19, :cond_10

    .line 212
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v12

    .line 215
    .local v12, "now":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/dial/WebSocketMultiplexer;->mNewSocketsAdded:Ljava/util/concurrent/atomic/AtomicBoolean;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v19

    if-eqz v19, :cond_4

    .line 216
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/dial/WebSocketMultiplexer;->mNewSockets:Ljava/util/LinkedList;

    move-object/from16 v20, v0

    monitor-enter v20

    .line 217
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/dial/WebSocketMultiplexer;->mNewSockets:Ljava/util/LinkedList;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-eqz v19, :cond_3

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/google/android/music/dial/WebSocket;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 219
    .local v14, "socket":Lcom/google/android/music/dial/WebSocket;
    :try_start_1
    invoke-virtual {v14}, Lcom/google/android/music/dial/WebSocket;->startConnect()Ljava/nio/channels/SocketChannel;

    move-result-object v4

    .line 220
    .local v4, "channel":Ljava/nio/channels/SocketChannel;
    if-eqz v4, :cond_2

    .line 221
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/dial/WebSocketMultiplexer;->mSelector:Ljava/nio/channels/Selector;

    move-object/from16 v19, v0

    const/16 v21, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v21

    invoke-virtual {v4, v0, v1}, Ljava/nio/channels/SocketChannel;->register(Ljava/nio/channels/Selector;I)Ljava/nio/channels/SelectionKey;

    move-result-object v9

    .line 222
    .local v9, "key":Ljava/nio/channels/SelectionKey;
    invoke-virtual {v9, v14}, Ljava/nio/channels/SelectionKey;->attach(Ljava/lang/Object;)Ljava/lang/Object;

    .line 223
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/dial/WebSocketMultiplexer;->mSockets:Ljava/util/LinkedList;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v14}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 228
    .end local v4    # "channel":Ljava/nio/channels/SocketChannel;
    .end local v9    # "key":Ljava/nio/channels/SelectionKey;
    :catch_0
    move-exception v6

    .line 229
    .local v6, "e":Ljava/lang/Exception;
    :try_start_2
    sget-boolean v19, Lcom/google/android/music/dial/WebSocketMultiplexer;->LOGV:Z

    if-eqz v19, :cond_1

    .line 230
    const-string v19, "WebSocketMultiplexer"

    const-string v21, "Error while connecting socket."

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-static {v0, v1, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 233
    :cond_1
    move-object/from16 v0, v18

    invoke-interface {v0, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 237
    .end local v6    # "e":Ljava/lang/Exception;
    .end local v8    # "i$":Ljava/util/Iterator;
    .end local v14    # "socket":Lcom/google/android/music/dial/WebSocket;
    :catchall_0
    move-exception v19

    monitor-exit v20
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v19

    .line 226
    .restart local v4    # "channel":Ljava/nio/channels/SocketChannel;
    .restart local v8    # "i$":Ljava/util/Iterator;
    .restart local v14    # "socket":Lcom/google/android/music/dial/WebSocket;
    :cond_2
    :try_start_3
    move-object/from16 v0, v18

    invoke-interface {v0, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 236
    .end local v4    # "channel":Ljava/nio/channels/SocketChannel;
    .end local v14    # "socket":Lcom/google/android/music/dial/WebSocket;
    :cond_3
    :try_start_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/dial/WebSocketMultiplexer;->mNewSockets:Ljava/util/LinkedList;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/util/LinkedList;->clear()V

    .line 237
    monitor-exit v20
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 241
    .end local v8    # "i$":Ljava/util/Iterator;
    :cond_4
    invoke-interface/range {v18 .. v18}, Ljava/util/List;->isEmpty()Z

    move-result v19

    if-nez v19, :cond_6

    .line 242
    invoke-interface/range {v18 .. v18}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .restart local v8    # "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-eqz v19, :cond_5

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/google/android/music/dial/WebSocket;

    .line 243
    .restart local v14    # "socket":Lcom/google/android/music/dial/WebSocket;
    const/16 v19, 0x2

    move/from16 v0, v19

    invoke-virtual {v14, v0}, Lcom/google/android/music/dial/WebSocket;->shutdown(I)V

    goto :goto_2

    .line 245
    .end local v14    # "socket":Lcom/google/android/music/dial/WebSocket;
    :cond_5
    invoke-interface/range {v18 .. v18}, Ljava/util/List;->clear()V

    .line 249
    .end local v8    # "i$":Ljava/util/Iterator;
    :cond_6
    const/4 v5, 0x0

    .line 250
    .local v5, "connectionActionsPending":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/dial/WebSocketMultiplexer;->mSockets:Ljava/util/LinkedList;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v15

    .line 251
    .local v15, "socketIter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/google/android/music/dial/WebSocket;>;"
    :cond_7
    :goto_3
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-eqz v19, :cond_e

    .line 252
    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/google/android/music/dial/WebSocket;

    .line 253
    .restart local v14    # "socket":Lcom/google/android/music/dial/WebSocket;
    invoke-virtual {v14}, Lcom/google/android/music/dial/WebSocket;->getSocketChannel()Ljava/nio/channels/SocketChannel;

    move-result-object v4

    .line 256
    .restart local v4    # "channel":Ljava/nio/channels/SocketChannel;
    if-eqz v4, :cond_8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/dial/WebSocketMultiplexer;->mSelector:Ljava/nio/channels/Selector;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/nio/channels/SocketChannel;->keyFor(Ljava/nio/channels/Selector;)Ljava/nio/channels/SelectionKey;

    move-result-object v19

    if-nez v19, :cond_9

    .line 257
    :cond_8
    invoke-interface {v15}, Ljava/util/Iterator;->remove()V

    goto :goto_3

    .line 258
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/dial/WebSocketMultiplexer;->mSelector:Ljava/nio/channels/Selector;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/nio/channels/SocketChannel;->keyFor(Ljava/nio/channels/Selector;)Ljava/nio/channels/SelectionKey;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v14, v0, v12, v13}, Lcom/google/android/music/dial/WebSocket;->updateSelectionKey(Ljava/nio/channels/SelectionKey;J)I

    move-result v11

    .local v11, "result":I
    const/16 v19, -0x1

    move/from16 v0, v19

    if-eq v11, v0, :cond_b

    .line 260
    sget-boolean v19, Lcom/google/android/music/dial/WebSocketMultiplexer;->LOGV:Z

    if-eqz v19, :cond_a

    .line 261
    const-string v19, "WebSocketMultiplexer"

    const-string v20, "Removing socket %s result %d"

    const/16 v21, 0x2

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    aput-object v14, v21, v22

    const/16 v22, 0x1

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    aput-object v23, v21, v22

    invoke-static/range {v20 .. v21}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 263
    :cond_a
    invoke-interface {v15}, Ljava/util/Iterator;->remove()V

    .line 264
    invoke-virtual {v14, v11}, Lcom/google/android/music/dial/WebSocket;->shutdown(I)V

    goto :goto_3

    .line 265
    :cond_b
    invoke-virtual {v14}, Lcom/google/android/music/dial/WebSocket;->isConnecting()Z

    move-result v19

    if-nez v19, :cond_c

    invoke-virtual {v14}, Lcom/google/android/music/dial/WebSocket;->isDisconnecting()Z

    move-result v19

    if-eqz v19, :cond_7

    .line 266
    :cond_c
    sget-boolean v19, Lcom/google/android/music/dial/WebSocketMultiplexer;->LOGV:Z

    if-eqz v19, :cond_d

    .line 267
    const-string v19, "WebSocketMultiplexer"

    const-string v20, "Socket connecting|disconnecting.  connectionActionsPending=%b"

    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v23

    aput-object v23, v21, v22

    invoke-static/range {v20 .. v21}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 271
    :cond_d
    const/4 v5, 0x1

    goto/16 :goto_3

    .line 276
    .end local v4    # "channel":Ljava/nio/channels/SocketChannel;
    .end local v11    # "result":I
    .end local v14    # "socket":Lcom/google/android/music/dial/WebSocket;
    :cond_e
    if-eqz v5, :cond_11

    const-wide/16 v16, 0x3e8

    .line 278
    .local v16, "timeout":J
    :goto_4
    :try_start_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/dial/WebSocketMultiplexer;->mSelector:Ljava/nio/channels/Selector;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move-wide/from16 v1, v16

    invoke-virtual {v0, v1, v2}, Ljava/nio/channels/Selector;->select(J)I

    move-result v7

    .line 279
    .local v7, "eventCount":I
    sget-boolean v19, Lcom/google/android/music/dial/WebSocketMultiplexer;->LOGV:Z

    if-eqz v19, :cond_f

    .line 280
    const-string v19, "WebSocketMultiplexer"

    const-string v20, "eventCount=%d, connectionActionsPending=%b, mSockets.size=%d, keys.size=%d, timeout=%d"

    const/16 v21, 0x5

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    aput-object v23, v21, v22

    const/16 v22, 0x1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v23

    aput-object v23, v21, v22

    const/16 v22, 0x2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/dial/WebSocketMultiplexer;->mSockets:Ljava/util/LinkedList;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Ljava/util/LinkedList;->size()I

    move-result v23

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    aput-object v23, v21, v22

    const/16 v22, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/dial/WebSocketMultiplexer;->mSelector:Ljava/nio/channels/Selector;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Ljava/nio/channels/Selector;->selectedKeys()Ljava/util/Set;

    move-result-object v23

    invoke-interface/range {v23 .. v23}, Ljava/util/Set;->size()I

    move-result v23

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    aput-object v23, v21, v22

    const/16 v22, 0x4

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v23

    aput-object v23, v21, v22

    invoke-static/range {v20 .. v21}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 286
    :cond_f
    if-nez v7, :cond_13

    .line 288
    const-wide/16 v20, 0x1388

    move-object/from16 v0, p0

    move-wide/from16 v1, v20

    invoke-direct {v0, v1, v2}, Lcom/google/android/music/dial/WebSocketMultiplexer;->releaseWakeLockIfHeldFor(J)Z
    :try_end_5
    .catch Ljava/lang/IllegalArgumentException; {:try_start_5 .. :try_end_5} :catch_1

    goto/16 :goto_0

    .line 293
    .end local v7    # "eventCount":I
    .end local v16    # "timeout":J
    :catch_1
    move-exception v19

    .line 297
    :goto_5
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/music/dial/WebSocketMultiplexer;->mShouldStop:Z

    move/from16 v19, v0

    if-eqz v19, :cond_14

    .line 353
    .end local v5    # "connectionActionsPending":Z
    .end local v12    # "now":J
    .end local v15    # "socketIter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/google/android/music/dial/WebSocket;>;"
    :cond_10
    return-void

    .line 276
    .restart local v5    # "connectionActionsPending":Z
    .restart local v12    # "now":J
    .restart local v15    # "socketIter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/google/android/music/dial/WebSocket;>;"
    :cond_11
    :try_start_6
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/music/dial/WebSocketMultiplexer;->mWakeLockHeld:Z

    move/from16 v19, v0

    if-eqz v19, :cond_12

    const-wide/16 v16, 0x1388

    goto/16 :goto_4

    :cond_12
    const-wide/16 v16, 0x0

    goto/16 :goto_4

    .line 291
    .restart local v7    # "eventCount":I
    .restart local v16    # "timeout":J
    :cond_13
    invoke-direct/range {p0 .. p0}, Lcom/google/android/music/dial/WebSocketMultiplexer;->acquireWakeLock()V
    :try_end_6
    .catch Ljava/lang/IllegalArgumentException; {:try_start_6 .. :try_end_6} :catch_1

    goto :goto_5

    .line 301
    .end local v7    # "eventCount":I
    .end local v16    # "timeout":J
    :cond_14
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/dial/WebSocketMultiplexer;->mSelector:Ljava/nio/channels/Selector;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/nio/channels/Selector;->selectedKeys()Ljava/util/Set;

    move-result-object v19

    invoke-interface/range {v19 .. v19}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .line 302
    .local v10, "keyIter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/nio/channels/SelectionKey;>;"
    :goto_6
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-eqz v19, :cond_0

    .line 304
    :try_start_7
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/nio/channels/SelectionKey;

    .line 305
    .restart local v9    # "key":Ljava/nio/channels/SelectionKey;
    invoke-virtual {v9}, Ljava/nio/channels/SelectionKey;->attachment()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/google/android/music/dial/WebSocket;

    .line 307
    .restart local v14    # "socket":Lcom/google/android/music/dial/WebSocket;
    invoke-virtual {v9}, Ljava/nio/channels/SelectionKey;->isConnectable()Z

    move-result v19

    if-eqz v19, :cond_16

    .line 308
    invoke-virtual {v14}, Lcom/google/android/music/dial/WebSocket;->onConnectable()I

    move-result v11

    .line 309
    .restart local v11    # "result":I
    const/16 v19, -0x1

    move/from16 v0, v19

    if-eq v11, v0, :cond_16

    .line 310
    sget-boolean v19, Lcom/google/android/music/dial/WebSocketMultiplexer;->LOGV:Z

    if-eqz v19, :cond_15

    .line 311
    const-string v19, "WebSocketMultiplexer"

    const-string v20, "Removing socket %s (onConnectable)"

    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    aput-object v14, v21, v22

    invoke-static/range {v20 .. v21}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 314
    :cond_15
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/dial/WebSocketMultiplexer;->mSockets:Ljava/util/LinkedList;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v14}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 315
    invoke-virtual {v9}, Ljava/nio/channels/SelectionKey;->cancel()V

    .line 316
    invoke-virtual {v14, v11}, Lcom/google/android/music/dial/WebSocket;->shutdown(I)V

    .line 320
    .end local v11    # "result":I
    :cond_16
    invoke-virtual {v9}, Ljava/nio/channels/SelectionKey;->isReadable()Z

    move-result v19

    if-eqz v19, :cond_18

    .line 321
    invoke-virtual {v14}, Lcom/google/android/music/dial/WebSocket;->onReadable()I

    move-result v11

    .line 322
    .restart local v11    # "result":I
    const/16 v19, -0x1

    move/from16 v0, v19

    if-eq v11, v0, :cond_18

    .line 323
    sget-boolean v19, Lcom/google/android/music/dial/WebSocketMultiplexer;->LOGV:Z

    if-eqz v19, :cond_17

    .line 324
    const-string v19, "WebSocketMultiplexer"

    const-string v20, "Removing socket %s (onReadable)"

    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    aput-object v14, v21, v22

    invoke-static/range {v20 .. v21}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 327
    :cond_17
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/dial/WebSocketMultiplexer;->mSockets:Ljava/util/LinkedList;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v14}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 328
    invoke-virtual {v9}, Ljava/nio/channels/SelectionKey;->cancel()V

    .line 329
    invoke-virtual {v14, v11}, Lcom/google/android/music/dial/WebSocket;->shutdown(I)V

    .line 333
    .end local v11    # "result":I
    :cond_18
    invoke-virtual {v9}, Ljava/nio/channels/SelectionKey;->isWritable()Z

    move-result v19

    if-eqz v19, :cond_1a

    .line 334
    invoke-virtual {v14}, Lcom/google/android/music/dial/WebSocket;->onWritable()I

    move-result v11

    .line 335
    .restart local v11    # "result":I
    const/16 v19, -0x1

    move/from16 v0, v19

    if-eq v11, v0, :cond_1a

    .line 336
    sget-boolean v19, Lcom/google/android/music/dial/WebSocketMultiplexer;->LOGV:Z

    if-eqz v19, :cond_19

    .line 337
    const-string v19, "WebSocketMultiplexer"

    const-string v20, "Removing socket %s (onWritable)"

    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    aput-object v14, v21, v22

    invoke-static/range {v20 .. v21}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 340
    :cond_19
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/dial/WebSocketMultiplexer;->mSockets:Ljava/util/LinkedList;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v14}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 341
    invoke-virtual {v9}, Ljava/nio/channels/SelectionKey;->cancel()V

    .line 342
    invoke-virtual {v14, v11}, Lcom/google/android/music/dial/WebSocket;->shutdown(I)V
    :try_end_7
    .catch Ljava/nio/channels/CancelledKeyException; {:try_start_7 .. :try_end_7} :catch_2

    .line 350
    .end local v9    # "key":Ljava/nio/channels/SelectionKey;
    .end local v11    # "result":I
    .end local v14    # "socket":Lcom/google/android/music/dial/WebSocket;
    :cond_1a
    :goto_7
    invoke-interface {v10}, Ljava/util/Iterator;->remove()V

    goto/16 :goto_6

    .line 345
    :catch_2
    move-exception v19

    goto :goto_7
.end method


# virtual methods
.method public declared-synchronized addSocket(Lcom/google/android/music/dial/WebSocket;)V
    .locals 5
    .param p1, "socket"    # Lcom/google/android/music/dial/WebSocket;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 184
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/music/dial/WebSocketMultiplexer;->checkRunning()V

    .line 186
    iget-object v1, p0, Lcom/google/android/music/dial/WebSocketMultiplexer;->mNewSockets:Ljava/util/LinkedList;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 187
    :try_start_1
    iget-object v0, p0, Lcom/google/android/music/dial/WebSocketMultiplexer;->mNewSockets:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 188
    sget-boolean v0, Lcom/google/android/music/dial/WebSocketMultiplexer;->LOGV:Z

    if-eqz v0, :cond_0

    .line 189
    const-string v0, "WebSocketMultiplexer"

    const-string v2, "added socket %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 191
    :cond_0
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 192
    :try_start_2
    iget-object v0, p0, Lcom/google/android/music/dial/WebSocketMultiplexer;->mNewSocketsAdded:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 193
    iget-object v0, p0, Lcom/google/android/music/dial/WebSocketMultiplexer;->mSelector:Ljava/nio/channels/Selector;

    invoke-virtual {v0}, Ljava/nio/channels/Selector;->wakeup()Ljava/nio/channels/Selector;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 194
    monitor-exit p0

    return-void

    .line 191
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 184
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method generateMask()[B
    .locals 2

    .prologue
    .line 442
    const/4 v1, 0x4

    new-array v0, v1, [B

    .line 443
    .local v0, "mask":[B
    iget-object v1, p0, Lcom/google/android/music/dial/WebSocketMultiplexer;->mRandom:Ljava/security/SecureRandom;

    invoke-virtual {v1, v0}, Ljava/security/SecureRandom;->nextBytes([B)V

    .line 444
    return-object v0
.end method

.method generateRandomKey()Ljava/lang/String;
    .locals 4

    .prologue
    .line 433
    const/16 v1, 0x10

    new-array v0, v1, [B

    .line 434
    .local v0, "key":[B
    iget-object v1, p0, Lcom/google/android/music/dial/WebSocketMultiplexer;->mRandom:Ljava/security/SecureRandom;

    invoke-virtual {v1, v0}, Ljava/security/SecureRandom;->nextBytes([B)V

    .line 435
    const/4 v1, 0x0

    array-length v2, v0

    const/4 v3, 0x2

    invoke-static {v0, v1, v2, v3}, Landroid/util/Base64;->encodeToString([BIII)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method getCharset()Ljava/nio/charset/Charset;
    .locals 1

    .prologue
    .line 426
    iget-object v0, p0, Lcom/google/android/music/dial/WebSocketMultiplexer;->mCharset:Ljava/nio/charset/Charset;

    return-object v0
.end method

.method declared-synchronized start()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 103
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/music/dial/WebSocketMultiplexer;->mThread:Ljava/lang/Thread;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_1

    .line 144
    :cond_0
    monitor-exit p0

    return-void

    .line 107
    :cond_1
    :try_start_1
    new-instance v1, Ljava/util/concurrent/CountDownLatch;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/music/dial/WebSocketMultiplexer;->mStartupLatch:Ljava/util/concurrent/CountDownLatch;

    .line 109
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/music/dial/WebSocketMultiplexer;->mAborted:Z

    .line 110
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/music/dial/WebSocketMultiplexer;->mShouldStop:Z

    .line 111
    invoke-static {}, Ljava/nio/channels/Selector;->open()Ljava/nio/channels/Selector;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/dial/WebSocketMultiplexer;->mSelector:Ljava/nio/channels/Selector;

    .line 112
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/google/android/music/dial/WebSocketMultiplexer$1;

    invoke-direct {v2, p0}, Lcom/google/android/music/dial/WebSocketMultiplexer$1;-><init>(Lcom/google/android/music/dial/WebSocketMultiplexer;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v1, p0, Lcom/google/android/music/dial/WebSocketMultiplexer;->mThread:Ljava/lang/Thread;

    .line 129
    iget-object v1, p0, Lcom/google/android/music/dial/WebSocketMultiplexer;->mThread:Ljava/lang/Thread;

    const-string v2, "WebSocketMultiplexer"

    invoke-virtual {v1, v2}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 130
    invoke-direct {p0}, Lcom/google/android/music/dial/WebSocketMultiplexer;->acquireWakeLock()V

    .line 131
    iget-object v1, p0, Lcom/google/android/music/dial/WebSocketMultiplexer;->mThread:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 133
    const/4 v0, 0x0

    .line 136
    .local v0, "ok":Z
    :try_start_2
    iget-object v1, p0, Lcom/google/android/music/dial/WebSocketMultiplexer;->mStartupLatch:Ljava/util/concurrent/CountDownLatch;

    const-wide/16 v2, 0x2710

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, v2, v3, v4}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v0

    .line 141
    :goto_0
    if-nez v0, :cond_0

    .line 142
    :try_start_3
    new-instance v1, Ljava/io/IOException;

    const-string v2, "timed out or interrupted waiting for muxer thread to start"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 103
    .end local v0    # "ok":Z
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 137
    .restart local v0    # "ok":Z
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public declared-synchronized wakeup()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 200
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/music/dial/WebSocketMultiplexer;->checkRunning()V

    .line 201
    iget-object v0, p0, Lcom/google/android/music/dial/WebSocketMultiplexer;->mSelector:Ljava/nio/channels/Selector;

    invoke-virtual {v0}, Ljava/nio/channels/Selector;->wakeup()Ljava/nio/channels/Selector;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 202
    monitor-exit p0

    return-void

    .line 200
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
