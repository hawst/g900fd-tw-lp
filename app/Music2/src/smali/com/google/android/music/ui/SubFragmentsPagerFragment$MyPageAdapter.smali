.class Lcom/google/android/music/ui/SubFragmentsPagerFragment$MyPageAdapter;
.super Landroid/support/v4/app/FragmentStatePagerAdapter;
.source "SubFragmentsPagerFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/SubFragmentsPagerFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyPageAdapter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/SubFragmentsPagerFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/music/ui/SubFragmentsPagerFragment;Landroid/support/v4/app/FragmentManager;)V
    .locals 0
    .param p2, "fm"    # Landroid/support/v4/app/FragmentManager;

    .prologue
    .line 149
    iput-object p1, p0, Lcom/google/android/music/ui/SubFragmentsPagerFragment$MyPageAdapter;->this$0:Lcom/google/android/music/ui/SubFragmentsPagerFragment;

    .line 150
    invoke-direct {p0, p2}, Landroid/support/v4/app/FragmentStatePagerAdapter;-><init>(Landroid/support/v4/app/FragmentManager;)V

    .line 151
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lcom/google/android/music/ui/SubFragmentsPagerFragment$MyPageAdapter;->this$0:Lcom/google/android/music/ui/SubFragmentsPagerFragment;

    # getter for: Lcom/google/android/music/ui/SubFragmentsPagerFragment;->mTabs:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/google/android/music/ui/SubFragmentsPagerFragment;->access$200(Lcom/google/android/music/ui/SubFragmentsPagerFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Landroid/support/v4/app/Fragment;
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 160
    iget-object v1, p0, Lcom/google/android/music/ui/SubFragmentsPagerFragment$MyPageAdapter;->this$0:Lcom/google/android/music/ui/SubFragmentsPagerFragment;

    # getter for: Lcom/google/android/music/ui/SubFragmentsPagerFragment;->mTabs:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/google/android/music/ui/SubFragmentsPagerFragment;->access$200(Lcom/google/android/music/ui/SubFragmentsPagerFragment;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/ui/FragmentTabInfo;

    .line 161
    .local v0, "tabInfo":Lcom/google/android/music/ui/FragmentTabInfo;
    iget-object v1, p0, Lcom/google/android/music/ui/SubFragmentsPagerFragment$MyPageAdapter;->this$0:Lcom/google/android/music/ui/SubFragmentsPagerFragment;

    invoke-virtual {v1}, Lcom/google/android/music/ui/SubFragmentsPagerFragment;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/preferences/MusicPreferences;->getDisplayOptions()I

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, v0, Lcom/google/android/music/ui/FragmentTabInfo;->mOnlineOnly:Z

    if-eqz v1, :cond_0

    .line 163
    iget-object v1, p0, Lcom/google/android/music/ui/SubFragmentsPagerFragment$MyPageAdapter;->this$0:Lcom/google/android/music/ui/SubFragmentsPagerFragment;

    invoke-virtual {v1}, Lcom/google/android/music/ui/SubFragmentsPagerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/google/android/music/ui/OfflineModeEmptyFragment;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/support/v4/app/Fragment;->instantiate(Landroid/content/Context;Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    .line 165
    :goto_0
    return-object v1

    :cond_0
    iget-object v1, v0, Lcom/google/android/music/ui/FragmentTabInfo;->mFragmentInfo:Lcom/google/android/music/ui/FragmentInfo;

    iget-object v2, p0, Lcom/google/android/music/ui/SubFragmentsPagerFragment$MyPageAdapter;->this$0:Lcom/google/android/music/ui/SubFragmentsPagerFragment;

    invoke-virtual {v2}, Lcom/google/android/music/ui/SubFragmentsPagerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/music/ui/FragmentInfo;->instantiate(Landroid/content/Context;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    goto :goto_0
.end method

.method public getItemPosition(Ljava/lang/Object;)I
    .locals 1
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 184
    const/4 v0, -0x2

    return v0
.end method

.method public getPageTitle(I)Ljava/lang/CharSequence;
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 171
    iget-object v0, p0, Lcom/google/android/music/ui/SubFragmentsPagerFragment$MyPageAdapter;->this$0:Lcom/google/android/music/ui/SubFragmentsPagerFragment;

    invoke-virtual {v0}, Lcom/google/android/music/ui/SubFragmentsPagerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/music/ui/SubFragmentsPagerFragment$MyPageAdapter;->this$0:Lcom/google/android/music/ui/SubFragmentsPagerFragment;

    # getter for: Lcom/google/android/music/ui/SubFragmentsPagerFragment;->mTabs:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/google/android/music/ui/SubFragmentsPagerFragment;->access$200(Lcom/google/android/music/ui/SubFragmentsPagerFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/ui/FragmentTabInfo;

    iget v0, v0, Lcom/google/android/music/ui/FragmentTabInfo;->mResId:I

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPageWidth(I)F
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 176
    iget-object v0, p0, Lcom/google/android/music/ui/SubFragmentsPagerFragment$MyPageAdapter;->this$0:Lcom/google/android/music/ui/SubFragmentsPagerFragment;

    # getter for: Lcom/google/android/music/ui/SubFragmentsPagerFragment;->mTabs:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/google/android/music/ui/SubFragmentsPagerFragment;->access$200(Lcom/google/android/music/ui/SubFragmentsPagerFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/ui/FragmentTabInfo;

    iget v0, v0, Lcom/google/android/music/ui/FragmentTabInfo;->mPageWidth:F

    return v0
.end method
