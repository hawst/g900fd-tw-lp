.class public final enum Lcom/google/android/music/download/ContentIdentifier$Domain;
.super Ljava/lang/Enum;
.source "ContentIdentifier.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/download/ContentIdentifier;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Domain"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/music/download/ContentIdentifier$Domain;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/music/download/ContentIdentifier$Domain;

.field public static final enum DEFAULT:Lcom/google/android/music/download/ContentIdentifier$Domain;

.field public static final enum NAUTILUS:Lcom/google/android/music/download/ContentIdentifier$Domain;

.field public static final enum SHARED:Lcom/google/android/music/download/ContentIdentifier$Domain;


# instance fields
.field private mDbValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 29
    new-instance v0, Lcom/google/android/music/download/ContentIdentifier$Domain;

    const-string v1, "DEFAULT"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/android/music/download/ContentIdentifier$Domain;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/music/download/ContentIdentifier$Domain;->DEFAULT:Lcom/google/android/music/download/ContentIdentifier$Domain;

    .line 30
    new-instance v0, Lcom/google/android/music/download/ContentIdentifier$Domain;

    const-string v1, "SHARED"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/android/music/download/ContentIdentifier$Domain;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/music/download/ContentIdentifier$Domain;->SHARED:Lcom/google/android/music/download/ContentIdentifier$Domain;

    .line 31
    new-instance v0, Lcom/google/android/music/download/ContentIdentifier$Domain;

    const-string v1, "NAUTILUS"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v5, v2}, Lcom/google/android/music/download/ContentIdentifier$Domain;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/music/download/ContentIdentifier$Domain;->NAUTILUS:Lcom/google/android/music/download/ContentIdentifier$Domain;

    .line 28
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/music/download/ContentIdentifier$Domain;

    sget-object v1, Lcom/google/android/music/download/ContentIdentifier$Domain;->DEFAULT:Lcom/google/android/music/download/ContentIdentifier$Domain;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/music/download/ContentIdentifier$Domain;->SHARED:Lcom/google/android/music/download/ContentIdentifier$Domain;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/music/download/ContentIdentifier$Domain;->NAUTILUS:Lcom/google/android/music/download/ContentIdentifier$Domain;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/music/download/ContentIdentifier$Domain;->$VALUES:[Lcom/google/android/music/download/ContentIdentifier$Domain;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "dbValue"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 40
    iput p3, p0, Lcom/google/android/music/download/ContentIdentifier$Domain;->mDbValue:I

    .line 41
    return-void
.end method

.method public static fromDBValue(I)Lcom/google/android/music/download/ContentIdentifier$Domain;
    .locals 7
    .param p0, "value"    # I

    .prologue
    .line 48
    invoke-static {}, Lcom/google/android/music/download/ContentIdentifier$Domain;->values()[Lcom/google/android/music/download/ContentIdentifier$Domain;

    move-result-object v0

    .local v0, "arr$":[Lcom/google/android/music/download/ContentIdentifier$Domain;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v1, v0, v2

    .line 49
    .local v1, "domain":Lcom/google/android/music/download/ContentIdentifier$Domain;
    iget v4, v1, Lcom/google/android/music/download/ContentIdentifier$Domain;->mDbValue:I

    if-ne v4, p0, :cond_0

    .line 50
    return-object v1

    .line 48
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 53
    .end local v1    # "domain":Lcom/google/android/music/download/ContentIdentifier$Domain;
    :cond_1
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unknown value: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/music/download/ContentIdentifier$Domain;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 28
    const-class v0, Lcom/google/android/music/download/ContentIdentifier$Domain;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/download/ContentIdentifier$Domain;

    return-object v0
.end method

.method public static values()[Lcom/google/android/music/download/ContentIdentifier$Domain;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/google/android/music/download/ContentIdentifier$Domain;->$VALUES:[Lcom/google/android/music/download/ContentIdentifier$Domain;

    invoke-virtual {v0}, [Lcom/google/android/music/download/ContentIdentifier$Domain;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/music/download/ContentIdentifier$Domain;

    return-object v0
.end method


# virtual methods
.method public getDBValue()I
    .locals 1

    .prologue
    .line 44
    iget v0, p0, Lcom/google/android/music/download/ContentIdentifier$Domain;->mDbValue:I

    return v0
.end method

.method public isDefaultDomain()Z
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/google/android/music/download/ContentIdentifier$Domain;->DEFAULT:Lcom/google/android/music/download/ContentIdentifier$Domain;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
