.class public Lcom/google/android/music/ui/ManageDevicesFragment;
.super Lcom/google/android/music/ui/BaseListFragment;
.source "ManageDevicesFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/ui/ManageDevicesFragment$DeauthorizeDeviceClickListner;,
        Lcom/google/android/music/ui/ManageDevicesFragment$DevicesListAdapter;,
        Lcom/google/android/music/ui/ManageDevicesFragment$DeviceInfoViewHolder;
    }
.end annotation


# instance fields
.field private mAdapter:Lcom/google/android/music/ui/ManageDevicesFragment$DevicesListAdapter;

.field private mAdapterData:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private mCurrentDeviceId:Ljava/lang/String;

.field private volatile mDestroyed:Z

.field private mNumOfOtherDevices:I

.field private mNumOfSmartPhones:I

.field private mOtherDevices:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/cloudclient/UserDeviceInfoJson;",
            ">;"
        }
    .end annotation
.end field

.field private mSmartPhones:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/cloudclient/UserDeviceInfoJson;",
            ">;"
        }
    .end annotation
.end field

.field private mUserDevices:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/cloudclient/UserDeviceInfoJson;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/google/android/music/ui/BaseListFragment;-><init>()V

    .line 42
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/ui/ManageDevicesFragment;->mSmartPhones:Ljava/util/List;

    .line 43
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/ui/ManageDevicesFragment;->mOtherDevices:Ljava/util/List;

    .line 51
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/ui/ManageDevicesFragment;->mDestroyed:Z

    .line 334
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/ui/ManageDevicesFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/ManageDevicesFragment;

    .prologue
    .line 37
    iget-boolean v0, p0, Lcom/google/android/music/ui/ManageDevicesFragment;->mDestroyed:Z

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/music/ui/ManageDevicesFragment;)Lcom/google/android/music/ui/ManageDevicesFragment$DevicesListAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/ManageDevicesFragment;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/music/ui/ManageDevicesFragment;->mAdapter:Lcom/google/android/music/ui/ManageDevicesFragment$DevicesListAdapter;

    return-object v0
.end method

.method static synthetic access$102(Lcom/google/android/music/ui/ManageDevicesFragment;Lcom/google/android/music/ui/ManageDevicesFragment$DevicesListAdapter;)Lcom/google/android/music/ui/ManageDevicesFragment$DevicesListAdapter;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/ManageDevicesFragment;
    .param p1, "x1"    # Lcom/google/android/music/ui/ManageDevicesFragment$DevicesListAdapter;

    .prologue
    .line 37
    iput-object p1, p0, Lcom/google/android/music/ui/ManageDevicesFragment;->mAdapter:Lcom/google/android/music/ui/ManageDevicesFragment$DevicesListAdapter;

    return-object p1
.end method

.method static synthetic access$300(Lcom/google/android/music/ui/ManageDevicesFragment;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/ManageDevicesFragment;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/music/ui/ManageDevicesFragment;->mUserDevices:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/android/music/ui/ManageDevicesFragment;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/ManageDevicesFragment;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 37
    iput-object p1, p0, Lcom/google/android/music/ui/ManageDevicesFragment;->mUserDevices:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$400(Lcom/google/android/music/ui/ManageDevicesFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/ManageDevicesFragment;

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/google/android/music/ui/ManageDevicesFragment;->groupUserDevices()V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/music/ui/ManageDevicesFragment;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/ManageDevicesFragment;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/music/ui/ManageDevicesFragment;->mSmartPhones:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/music/ui/ManageDevicesFragment;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/ManageDevicesFragment;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/music/ui/ManageDevicesFragment;->mAdapterData:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/music/ui/ManageDevicesFragment;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/ManageDevicesFragment;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/music/ui/ManageDevicesFragment;->mCurrentDeviceId:Ljava/lang/String;

    return-object v0
.end method

.method private groupUserDevices()V
    .locals 5

    .prologue
    .line 130
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/google/android/music/ui/ManageDevicesFragment;->mSmartPhones:Ljava/util/List;

    .line 131
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/google/android/music/ui/ManageDevicesFragment;->mOtherDevices:Ljava/util/List;

    .line 132
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/google/android/music/ui/ManageDevicesFragment;->mAdapterData:Ljava/util/List;

    .line 134
    iget-object v2, p0, Lcom/google/android/music/ui/ManageDevicesFragment;->mUserDevices:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/cloudclient/UserDeviceInfoJson;

    .line 135
    .local v0, "deviceInfoJson":Lcom/google/android/music/cloudclient/UserDeviceInfoJson;
    iget-boolean v2, v0, Lcom/google/android/music/cloudclient/UserDeviceInfoJson;->mIsSmartPhone:Z

    if-eqz v2, :cond_0

    .line 136
    iget-object v2, p0, Lcom/google/android/music/ui/ManageDevicesFragment;->mSmartPhones:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 138
    :cond_0
    iget-object v2, p0, Lcom/google/android/music/ui/ManageDevicesFragment;->mOtherDevices:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 142
    .end local v0    # "deviceInfoJson":Lcom/google/android/music/cloudclient/UserDeviceInfoJson;
    :cond_1
    iget-object v2, p0, Lcom/google/android/music/ui/ManageDevicesFragment;->mSmartPhones:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    iput v2, p0, Lcom/google/android/music/ui/ManageDevicesFragment;->mNumOfSmartPhones:I

    .line 143
    iget-object v2, p0, Lcom/google/android/music/ui/ManageDevicesFragment;->mOtherDevices:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    iput v2, p0, Lcom/google/android/music/ui/ManageDevicesFragment;->mNumOfOtherDevices:I

    .line 145
    iget-object v2, p0, Lcom/google/android/music/ui/ManageDevicesFragment;->mSmartPhones:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/music/ui/ManageDevicesFragment;->mOtherDevices:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 146
    iget-object v2, p0, Lcom/google/android/music/ui/ManageDevicesFragment;->mAdapterData:Ljava/util/List;

    const/4 v3, 0x0

    const v4, 0x7f0b0354

    invoke-virtual {p0, v4}, Lcom/google/android/music/ui/ManageDevicesFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 147
    iget-object v2, p0, Lcom/google/android/music/ui/ManageDevicesFragment;->mAdapterData:Ljava/util/List;

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/music/ui/ManageDevicesFragment;->mSmartPhones:Ljava/util/List;

    invoke-interface {v2, v3, v4}, Ljava/util/List;->addAll(ILjava/util/Collection;)Z

    .line 148
    iget-object v2, p0, Lcom/google/android/music/ui/ManageDevicesFragment;->mAdapterData:Ljava/util/List;

    iget v3, p0, Lcom/google/android/music/ui/ManageDevicesFragment;->mNumOfSmartPhones:I

    add-int/lit8 v3, v3, 0x1

    const v4, 0x7f0b0355

    invoke-virtual {p0, v4}, Lcom/google/android/music/ui/ManageDevicesFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 149
    iget-object v2, p0, Lcom/google/android/music/ui/ManageDevicesFragment;->mAdapterData:Ljava/util/List;

    iget v3, p0, Lcom/google/android/music/ui/ManageDevicesFragment;->mNumOfSmartPhones:I

    add-int/lit8 v3, v3, 0x2

    iget-object v4, p0, Lcom/google/android/music/ui/ManageDevicesFragment;->mOtherDevices:Ljava/util/List;

    invoke-interface {v2, v3, v4}, Ljava/util/List;->addAll(ILjava/util/Collection;)Z

    .line 154
    :goto_1
    iget-object v2, p0, Lcom/google/android/music/ui/ManageDevicesFragment;->mAdapter:Lcom/google/android/music/ui/ManageDevicesFragment$DevicesListAdapter;

    invoke-virtual {v2}, Lcom/google/android/music/ui/ManageDevicesFragment$DevicesListAdapter;->notifyDataSetChanged()V

    .line 155
    iget-object v2, p0, Lcom/google/android/music/ui/ManageDevicesFragment;->mAdapterData:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/music/ui/ManageDevicesFragment;->setEmptyScreenVisible(Z)V

    .line 156
    return-void

    .line 151
    :cond_2
    iget-object v2, p0, Lcom/google/android/music/ui/ManageDevicesFragment;->mAdapterData:Ljava/util/List;

    iget-object v3, p0, Lcom/google/android/music/ui/ManageDevicesFragment;->mSmartPhones:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 152
    iget-object v2, p0, Lcom/google/android/music/ui/ManageDevicesFragment;->mAdapterData:Ljava/util/List;

    iget-object v3, p0, Lcom/google/android/music/ui/ManageDevicesFragment;->mOtherDevices:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1
.end method


# virtual methods
.method public deauthorizeDevice(Ljava/lang/String;)V
    .locals 2
    .param p1, "deviceId"    # Ljava/lang/String;

    .prologue
    .line 159
    invoke-virtual {p0}, Lcom/google/android/music/ui/ManageDevicesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 161
    .local v0, "context":Landroid/content/Context;
    new-instance v1, Lcom/google/android/music/ui/ManageDevicesFragment$2;

    invoke-direct {v1, p0, v0, p1}, Lcom/google/android/music/ui/ManageDevicesFragment$2;-><init>(Lcom/google/android/music/ui/ManageDevicesFragment;Landroid/content/Context;Ljava/lang/String;)V

    invoke-static {v1}, Lcom/google/android/music/utils/MusicUtils;->runAsyncWithCallback(Lcom/google/android/music/utils/async/AsyncRunner;)V

    .line 193
    return-void
.end method

.method public getUserDevices()V
    .locals 2

    .prologue
    .line 86
    invoke-virtual {p0}, Lcom/google/android/music/ui/ManageDevicesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 88
    .local v0, "context":Landroid/content/Context;
    new-instance v1, Lcom/google/android/music/ui/ManageDevicesFragment$1;

    invoke-direct {v1, p0, v0}, Lcom/google/android/music/ui/ManageDevicesFragment$1;-><init>(Lcom/google/android/music/ui/ManageDevicesFragment;Landroid/content/Context;)V

    invoke-static {v1}, Lcom/google/android/music/utils/MusicUtils;->runAsyncWithCallback(Lcom/google/android/music/utils/async/AsyncRunner;)V

    .line 127
    return-void
.end method

.method protected initEmptyScreen()V
    .locals 1

    .prologue
    .line 197
    invoke-super {p0}, Lcom/google/android/music/ui/BaseListFragment;->initEmptyScreen()V

    .line 198
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/ManageDevicesFragment;->setEmptyImageViewVisibile(Z)V

    .line 199
    const v0, 0x7f0b032f

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/ManageDevicesFragment;->setEmptyScreenText(I)V

    .line 200
    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 80
    invoke-super {p0, p1}, Lcom/google/android/music/ui/BaseListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 82
    invoke-virtual {p0}, Lcom/google/android/music/ui/ManageDevicesFragment;->getUserDevices()V

    .line 83
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 57
    invoke-super {p0, p1}, Lcom/google/android/music/ui/BaseListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 58
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/ManageDevicesFragment;->setRetainInstance(Z)V

    .line 59
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 204
    invoke-super {p0}, Lcom/google/android/music/ui/BaseListFragment;->onDestroy()V

    .line 205
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/ui/ManageDevicesFragment;->mDestroyed:Z

    .line 206
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 9
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v8, 0x0

    .line 63
    invoke-super {p0, p1, p2}, Lcom/google/android/music/ui/BaseListFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 65
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "0x"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/music/ui/ManageDevicesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "android_id"

    const-wide/16 v6, 0x0

    invoke-static {v3, v4, v6, v7}, Lcom/google/android/gsf/Gservices;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/ui/ManageDevicesFragment;->mCurrentDeviceId:Ljava/lang/String;

    .line 69
    invoke-virtual {p0}, Lcom/google/android/music/ui/ManageDevicesFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    .line 70
    .local v0, "listView":Landroid/widget/ListView;
    invoke-virtual {v0, v8}, Landroid/widget/ListView;->setCacheColorHint(I)V

    .line 71
    const/high16 v2, 0x2000000

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setScrollBarStyle(I)V

    .line 73
    invoke-virtual {p0}, Lcom/google/android/music/ui/ManageDevicesFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f0142

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 75
    .local v1, "padding":I
    invoke-virtual {v0, v1, v8, v1, v8}, Landroid/widget/ListView;->setPadding(IIII)V

    .line 76
    return-void
.end method
