.class Lcom/google/android/music/playback/LocalAsyncMediaPlayer$1;
.super Ljava/lang/Object;
.source "LocalAsyncMediaPlayer.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/playback/LocalAsyncMediaPlayer;-><init>(Landroid/content/Context;Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/playback/LocalAsyncMediaPlayer;

.field final synthetic val$mediaPlayerCreated:Ljava/lang/Object;


# direct methods
.method constructor <init>(Lcom/google/android/music/playback/LocalAsyncMediaPlayer;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 147
    iput-object p1, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer$1;->this$0:Lcom/google/android/music/playback/LocalAsyncMediaPlayer;

    iput-object p2, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer$1;->val$mediaPlayerCreated:Ljava/lang/Object;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 154
    iget-object v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer$1;->this$0:Lcom/google/android/music/playback/LocalAsyncMediaPlayer;

    new-instance v1, Lcom/google/android/music/playback/CompatMediaPlayer;

    invoke-direct {v1}, Lcom/google/android/music/playback/CompatMediaPlayer;-><init>()V

    # setter for: Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mMediaPlayer:Lcom/google/android/music/playback/CompatMediaPlayer;
    invoke-static {v0, v1}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->access$002(Lcom/google/android/music/playback/LocalAsyncMediaPlayer;Lcom/google/android/music/playback/CompatMediaPlayer;)Lcom/google/android/music/playback/CompatMediaPlayer;

    .line 156
    iget-object v1, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer$1;->val$mediaPlayerCreated:Ljava/lang/Object;

    monitor-enter v1

    .line 157
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer$1;->val$mediaPlayerCreated:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 158
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 163
    invoke-static {}, Landroid/os/Looper;->myQueue()Landroid/os/MessageQueue;

    move-result-object v0

    new-instance v1, Lcom/google/android/music/playback/LocalAsyncMediaPlayer$1$1;

    invoke-direct {v1, p0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer$1$1;-><init>(Lcom/google/android/music/playback/LocalAsyncMediaPlayer$1;)V

    invoke-virtual {v0, v1}, Landroid/os/MessageQueue;->addIdleHandler(Landroid/os/MessageQueue$IdleHandler;)V

    .line 169
    return-void

    .line 158
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
