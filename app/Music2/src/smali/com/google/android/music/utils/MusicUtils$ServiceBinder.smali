.class Lcom/google/android/music/utils/MusicUtils$ServiceBinder;
.super Ljava/lang/Object;
.source "MusicUtils.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/utils/MusicUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ServiceBinder"
.end annotation


# instance fields
.field mCallback:Landroid/content/ServiceConnection;

.field private mContextWrapper:Landroid/content/ContextWrapper;

.field private mOnServiceConnectedHasBeenCalled:Z

.field private mUnbindFromServiceAfterServiceConnectedHasBeenCalled:Z


# direct methods
.method constructor <init>(Landroid/content/ServiceConnection;)V
    .locals 0
    .param p1, "callback"    # Landroid/content/ServiceConnection;

    .prologue
    .line 334
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 335
    iput-object p1, p0, Lcom/google/android/music/utils/MusicUtils$ServiceBinder;->mCallback:Landroid/content/ServiceConnection;

    .line 336
    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 1
    .param p1, "className"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 340
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/utils/MusicUtils$ServiceBinder;->mOnServiceConnectedHasBeenCalled:Z

    .line 341
    invoke-static {p2}, Lcom/google/android/music/playback/IMusicPlaybackService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/google/android/music/playback/IMusicPlaybackService;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    .line 342
    iget-object v0, p0, Lcom/google/android/music/utils/MusicUtils$ServiceBinder;->mCallback:Landroid/content/ServiceConnection;

    if-eqz v0, :cond_0

    .line 343
    iget-object v0, p0, Lcom/google/android/music/utils/MusicUtils$ServiceBinder;->mCallback:Landroid/content/ServiceConnection;

    invoke-interface {v0, p1, p2}, Landroid/content/ServiceConnection;->onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V

    .line 345
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/utils/MusicUtils$ServiceBinder;->mContextWrapper:Landroid/content/ContextWrapper;

    if-eqz v0, :cond_1

    .line 346
    iget-object v0, p0, Lcom/google/android/music/utils/MusicUtils$ServiceBinder;->mContextWrapper:Landroid/content/ContextWrapper;

    invoke-virtual {v0, p0}, Landroid/content/ContextWrapper;->unbindService(Landroid/content/ServiceConnection;)V

    .line 348
    :cond_1
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 1
    .param p1, "className"    # Landroid/content/ComponentName;

    .prologue
    .line 352
    iget-object v0, p0, Lcom/google/android/music/utils/MusicUtils$ServiceBinder;->mCallback:Landroid/content/ServiceConnection;

    if-eqz v0, :cond_0

    .line 353
    iget-object v0, p0, Lcom/google/android/music/utils/MusicUtils$ServiceBinder;->mCallback:Landroid/content/ServiceConnection;

    invoke-interface {v0, p1}, Landroid/content/ServiceConnection;->onServiceDisconnected(Landroid/content/ComponentName;)V

    .line 355
    :cond_0
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    .line 356
    return-void
.end method

.method public unbindFromServiceAfterServiceConnected(Landroid/content/ContextWrapper;)V
    .locals 2
    .param p1, "cw"    # Landroid/content/ContextWrapper;

    .prologue
    const/4 v1, 0x1

    .line 374
    iget-boolean v0, p0, Lcom/google/android/music/utils/MusicUtils$ServiceBinder;->mUnbindFromServiceAfterServiceConnectedHasBeenCalled:Z

    if-ne v0, v1, :cond_0

    .line 375
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "unbindFromServiceAfterServiceConnected called more than once."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 378
    :cond_0
    iput-boolean v1, p0, Lcom/google/android/music/utils/MusicUtils$ServiceBinder;->mUnbindFromServiceAfterServiceConnectedHasBeenCalled:Z

    .line 379
    iget-boolean v0, p0, Lcom/google/android/music/utils/MusicUtils$ServiceBinder;->mOnServiceConnectedHasBeenCalled:Z

    if-eqz v0, :cond_1

    .line 380
    invoke-virtual {p1, p0}, Landroid/content/ContextWrapper;->unbindService(Landroid/content/ServiceConnection;)V

    .line 387
    :goto_0
    return-void

    .line 384
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/utils/MusicUtils$ServiceBinder;->mCallback:Landroid/content/ServiceConnection;

    .line 385
    iput-object p1, p0, Lcom/google/android/music/utils/MusicUtils$ServiceBinder;->mContextWrapper:Landroid/content/ContextWrapper;

    goto :goto_0
.end method
