.class public Lcom/google/android/music/dial/model/LoadCloudQueueCommandJson$LoadCloudQueueCommandBody;
.super Lcom/google/api/client/json/GenericJson;
.source "LoadCloudQueueCommandJson.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/dial/model/LoadCloudQueueCommandJson;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LoadCloudQueueCommandBody"
.end annotation


# instance fields
.field public mContentType:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "contentType"
    .end annotation
.end field

.field public mFirstTrackMetadata:Lcom/google/android/music/dial/model/TrackMetadataJson;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "firstTrackMetadata"
    .end annotation
.end field

.field public mHttpHeaders:Ljava/util/Map;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "httpHeaders"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public mItemId:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "itemId"
    .end annotation
.end field

.field public mPlayOnCompletion:Z
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "playOnCompletion"
    .end annotation
.end field

.field public mPositionMillis:J
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "positionMillis"
    .end annotation
.end field

.field public mQueueBaseUrl:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "queueBaseUrl"
    .end annotation
.end field

.field public mSessionId:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "sessionId"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    return-void
.end method
