.class Lcom/google/android/music/ui/BaseTrackListView$2;
.super Ljava/lang/Object;
.source "BaseTrackListView.java"

# interfaces
.implements Lcom/mobeta/android/dslv/DragSortListView$DropListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/BaseTrackListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/BaseTrackListView;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/BaseTrackListView;)V
    .locals 0

    .prologue
    .line 60
    iput-object p1, p0, Lcom/google/android/music/ui/BaseTrackListView$2;->this$0:Lcom/google/android/music/ui/BaseTrackListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public drop(II)V
    .locals 3
    .param p1, "from"    # I
    .param p2, "to"    # I

    .prologue
    .line 63
    iget-object v1, p0, Lcom/google/android/music/ui/BaseTrackListView$2;->this$0:Lcom/google/android/music/ui/BaseTrackListView;

    # getter for: Lcom/google/android/music/ui/BaseTrackListView;->mAdapter:Lcom/google/android/music/ui/TrackListAdapter;
    invoke-static {v1}, Lcom/google/android/music/ui/BaseTrackListView;->access$000(Lcom/google/android/music/ui/BaseTrackListView;)Lcom/google/android/music/ui/TrackListAdapter;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 64
    iget-object v1, p0, Lcom/google/android/music/ui/BaseTrackListView$2;->this$0:Lcom/google/android/music/ui/BaseTrackListView;

    # getter for: Lcom/google/android/music/ui/BaseTrackListView;->mAdapter:Lcom/google/android/music/ui/TrackListAdapter;
    invoke-static {v1}, Lcom/google/android/music/ui/BaseTrackListView;->access$000(Lcom/google/android/music/ui/BaseTrackListView;)Lcom/google/android/music/ui/TrackListAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/ui/TrackListAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 65
    .local v0, "c":Landroid/database/Cursor;
    if-eqz v0, :cond_0

    .line 68
    iget-object v1, p0, Lcom/google/android/music/ui/BaseTrackListView$2;->this$0:Lcom/google/android/music/ui/BaseTrackListView;

    # getter for: Lcom/google/android/music/ui/BaseTrackListView;->mAdapter:Lcom/google/android/music/ui/TrackListAdapter;
    invoke-static {v1}, Lcom/google/android/music/ui/BaseTrackListView;->access$000(Lcom/google/android/music/ui/BaseTrackListView;)Lcom/google/android/music/ui/TrackListAdapter;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Lcom/google/android/music/ui/TrackListAdapter;->moveItemTemp(II)V

    .line 69
    iget-object v1, p0, Lcom/google/android/music/ui/BaseTrackListView$2;->this$0:Lcom/google/android/music/ui/BaseTrackListView;

    invoke-virtual {v1}, Lcom/google/android/music/ui/BaseTrackListView;->invalidateViews()V

    .line 72
    instance-of v1, v0, Lcom/google/android/music/medialist/MediaList$MediaCursor;

    if-eqz v1, :cond_1

    .line 73
    check-cast v0, Lcom/google/android/music/medialist/MediaList$MediaCursor;

    .end local v0    # "c":Landroid/database/Cursor;
    invoke-virtual {v0, p1, p2}, Lcom/google/android/music/medialist/MediaList$MediaCursor;->moveItem(II)V

    .line 83
    :cond_0
    :goto_0
    return-void

    .line 74
    .restart local v0    # "c":Landroid/database/Cursor;
    :cond_1
    iget-object v1, p0, Lcom/google/android/music/ui/BaseTrackListView$2;->this$0:Lcom/google/android/music/ui/BaseTrackListView;

    # getter for: Lcom/google/android/music/ui/BaseTrackListView;->mSongList:Lcom/google/android/music/medialist/SongList;
    invoke-static {v1}, Lcom/google/android/music/ui/BaseTrackListView;->access$100(Lcom/google/android/music/ui/BaseTrackListView;)Lcom/google/android/music/medialist/SongList;

    move-result-object v1

    instance-of v1, v1, Lcom/google/android/music/medialist/CaqPlayQueueSongList;

    if-eqz v1, :cond_2

    .line 75
    iget-object v1, p0, Lcom/google/android/music/ui/BaseTrackListView$2;->this$0:Lcom/google/android/music/ui/BaseTrackListView;

    invoke-virtual {v1}, Lcom/google/android/music/ui/BaseTrackListView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, p1, p2}, Lcom/google/android/music/utils/MusicUtils;->moveQueueItem(Landroid/content/Context;II)V

    goto :goto_0

    .line 78
    :cond_2
    iget-object v1, p0, Lcom/google/android/music/ui/BaseTrackListView$2;->this$0:Lcom/google/android/music/ui/BaseTrackListView;

    invoke-virtual {v1}, Lcom/google/android/music/ui/BaseTrackListView;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v1, p0, Lcom/google/android/music/ui/BaseTrackListView$2;->this$0:Lcom/google/android/music/ui/BaseTrackListView;

    # getter for: Lcom/google/android/music/ui/BaseTrackListView;->mSongList:Lcom/google/android/music/medialist/SongList;
    invoke-static {v1}, Lcom/google/android/music/ui/BaseTrackListView;->access$100(Lcom/google/android/music/ui/BaseTrackListView;)Lcom/google/android/music/medialist/SongList;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/medialist/PlaylistSongList;

    invoke-static {v2, v0, v1, p1, p2}, Lcom/google/android/music/utils/MusicUtils;->movePlaylistItem(Landroid/content/Context;Landroid/database/Cursor;Lcom/google/android/music/medialist/PlaylistSongList;II)V

    goto :goto_0
.end method
