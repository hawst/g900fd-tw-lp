.class Lcom/google/android/music/ui/ContainerPlayHeaderListLayout$1;
.super Ljava/lang/Object;
.source "ContainerPlayHeaderListLayout.java"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/ContainerPlayHeaderListLayout;->addOnScrollListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mListViewItemHeights:Ljava/util/Dictionary;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Dictionary",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/google/android/music/ui/ContainerPlayHeaderListLayout;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/ContainerPlayHeaderListLayout;)V
    .locals 1

    .prologue
    .line 87
    iput-object p1, p0, Lcom/google/android/music/ui/ContainerPlayHeaderListLayout$1;->this$0:Lcom/google/android/music/ui/ContainerPlayHeaderListLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 89
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/ui/ContainerPlayHeaderListLayout$1;->mListViewItemHeights:Ljava/util/Dictionary;

    return-void
.end method

.method private updateAbsoluteY(Landroid/widget/AbsListView;)V
    .locals 8
    .param p1, "view"    # Landroid/widget/AbsListView;

    .prologue
    .line 103
    const/4 v4, -0x1

    .line 104
    .local v4, "y":I
    const/4 v5, 0x0

    invoke-virtual {p1, v5}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 105
    .local v3, "v":Landroid/view/View;
    if-eqz v3, :cond_1

    .line 106
    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v5

    neg-int v4, v5

    .line 109
    invoke-virtual {p1}, Landroid/widget/AbsListView;->getFirstVisiblePosition()I

    move-result v0

    .line 112
    .local v0, "firstVisiblePos":I
    iget-object v5, p0, Lcom/google/android/music/ui/ContainerPlayHeaderListLayout$1;->mListViewItemHeights:Ljava/util/Dictionary;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Ljava/util/Dictionary;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 117
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_1

    .line 122
    iget-object v5, p0, Lcom/google/android/music/ui/ContainerPlayHeaderListLayout$1;->mListViewItemHeights:Ljava/util/Dictionary;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/Dictionary;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 123
    iget-object v5, p0, Lcom/google/android/music/ui/ContainerPlayHeaderListLayout$1;->mListViewItemHeights:Ljava/util/Dictionary;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/Dictionary;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    add-int/2addr v4, v5

    .line 117
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .end local v0    # "firstVisiblePos":I
    .end local v1    # "i":I
    :cond_1
    move-object v2, p1

    .line 129
    check-cast v2, Lcom/google/android/music/ui/BaseTrackListView;

    .line 130
    .local v2, "listView":Lcom/google/android/music/ui/BaseTrackListView;
    if-eqz v2, :cond_2

    .line 131
    invoke-virtual {v2, v4}, Lcom/google/android/music/ui/BaseTrackListView;->setAbsoluteY(I)V

    .line 133
    :cond_2
    return-void
.end method


# virtual methods
.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 0
    .param p1, "view"    # Landroid/widget/AbsListView;
    .param p2, "firstVisibleItem"    # I
    .param p3, "visibleItemCount"    # I
    .param p4, "totalItemCount"    # I

    .prologue
    .line 95
    invoke-direct {p0, p1}, Lcom/google/android/music/ui/ContainerPlayHeaderListLayout$1;->updateAbsoluteY(Landroid/widget/AbsListView;)V

    .line 96
    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0
    .param p1, "view"    # Landroid/widget/AbsListView;
    .param p2, "scrollState"    # I

    .prologue
    .line 100
    return-void
.end method
