.class public Lcom/google/android/music/ui/GridAdapterWrapperWithHeader;
.super Landroid/widget/BaseAdapter;
.source "GridAdapterWrapperWithHeader.java"


# static fields
.field private static sClusterList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mCardHeap:Lcom/google/android/music/ui/cardlib/layout/PlayCardHeap;

.field private final mClusterConfig:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;

.field private final mContext:Landroid/content/Context;

.field private final mLayoutInflater:Landroid/view/LayoutInflater;

.field private final mNumColumns:I

.field private final mWrappedAdapter:Landroid/widget/ListAdapter;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/16 v8, 0x8

    const/4 v7, 0x1

    const/4 v6, 0x6

    const/4 v5, 0x4

    const/4 v4, 0x0

    .line 48
    invoke-static {}, Lcom/google/android/music/ui/cardlib/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/ui/GridAdapterWrapperWithHeader;->sClusterList:Ljava/util/List;

    .line 50
    sget-object v0, Lcom/google/android/music/ui/GridAdapterWrapperWithHeader;->sClusterList:Ljava/util/List;

    new-instance v1, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;

    const/4 v2, 0x2

    invoke-direct {v1, v5, v2}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;-><init>(II)V

    sget-object v2, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->CARD_MEDIUM:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    invoke-virtual {v1, v2, v4, v4}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->addTile(Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;

    move-result-object v1

    invoke-interface {v0, v4, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 53
    sget-object v0, Lcom/google/android/music/ui/GridAdapterWrapperWithHeader;->sClusterList:Ljava/util/List;

    new-instance v1, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;

    const/4 v2, 0x3

    invoke-direct {v1, v6, v2}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;-><init>(II)V

    sget-object v2, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->CARD_MEDIUM_16x9:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    invoke-virtual {v1, v2, v4, v4}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->addTile(Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;

    move-result-object v1

    sget-object v2, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    invoke-virtual {v1, v2, v5, v4}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->addTile(Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;

    move-result-object v1

    invoke-interface {v0, v7, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 57
    sget-object v0, Lcom/google/android/music/ui/GridAdapterWrapperWithHeader;->sClusterList:Ljava/util/List;

    const/4 v1, 0x2

    new-instance v2, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;

    invoke-direct {v2, v6, v6}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;-><init>(II)V

    sget-object v3, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->CARD_LARGE:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    invoke-virtual {v2, v3, v4, v4}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->addTile(Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;

    move-result-object v2

    sget-object v3, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    invoke-virtual {v2, v3, v5, v4}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->addTile(Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;

    move-result-object v2

    sget-object v3, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    invoke-virtual {v2, v3, v5, v7}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->addTile(Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 62
    sget-object v0, Lcom/google/android/music/ui/GridAdapterWrapperWithHeader;->sClusterList:Ljava/util/List;

    const/4 v1, 0x3

    new-instance v2, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;

    const/16 v3, 0xa

    invoke-direct {v2, v3, v6}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;-><init>(II)V

    sget-object v3, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->CARD_LARGE:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    invoke-virtual {v2, v3, v4, v4}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->addTile(Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;

    move-result-object v2

    sget-object v3, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    invoke-virtual {v2, v3, v5, v4}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->addTile(Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;

    move-result-object v2

    sget-object v3, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    invoke-virtual {v2, v3, v6, v4}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->addTile(Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;

    move-result-object v2

    sget-object v3, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    invoke-virtual {v2, v3, v8, v4}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->addTile(Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;

    move-result-object v2

    sget-object v3, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    invoke-virtual {v2, v3, v5, v7}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->addTile(Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;

    move-result-object v2

    sget-object v3, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    invoke-virtual {v2, v3, v6, v7}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->addTile(Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;

    move-result-object v2

    sget-object v3, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    invoke-virtual {v2, v3, v8, v7}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->addTile(Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 71
    sget-object v0, Lcom/google/android/music/ui/GridAdapterWrapperWithHeader;->sClusterList:Ljava/util/List;

    new-instance v1, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;

    invoke-direct {v1, v8, v6}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;-><init>(II)V

    sget-object v2, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->CARD_LARGE:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    invoke-virtual {v1, v2, v4, v4}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->addTile(Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;

    move-result-object v1

    sget-object v2, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    invoke-virtual {v1, v2, v5, v4}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->addTile(Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;

    move-result-object v1

    sget-object v2, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    invoke-virtual {v1, v2, v6, v4}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->addTile(Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;

    move-result-object v1

    sget-object v2, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    invoke-virtual {v1, v2, v5, v7}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->addTile(Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;

    move-result-object v1

    sget-object v2, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    invoke-virtual {v1, v2, v6, v7}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->addTile(Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;

    move-result-object v1

    invoke-interface {v0, v5, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 78
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/widget/ListAdapter;II)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "adapter"    # Landroid/widget/ListAdapter;
    .param p3, "numColumns"    # I
    .param p4, "numRows"    # I

    .prologue
    .line 102
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 103
    iput-object p1, p0, Lcom/google/android/music/ui/GridAdapterWrapperWithHeader;->mContext:Landroid/content/Context;

    .line 104
    iput-object p2, p0, Lcom/google/android/music/ui/GridAdapterWrapperWithHeader;->mWrappedAdapter:Landroid/widget/ListAdapter;

    .line 105
    iget-object v0, p0, Lcom/google/android/music/ui/GridAdapterWrapperWithHeader;->mWrappedAdapter:Landroid/widget/ListAdapter;

    new-instance v1, Lcom/google/android/music/ui/GridAdapterWrapperWithHeader$1;

    invoke-direct {v1, p0}, Lcom/google/android/music/ui/GridAdapterWrapperWithHeader$1;-><init>(Lcom/google/android/music/ui/GridAdapterWrapperWithHeader;)V

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 110
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/GridAdapterWrapperWithHeader;->mLayoutInflater:Landroid/view/LayoutInflater;

    .line 111
    new-instance v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardHeap;

    invoke-direct {v0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardHeap;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/ui/GridAdapterWrapperWithHeader;->mCardHeap:Lcom/google/android/music/ui/cardlib/layout/PlayCardHeap;

    .line 112
    iput p3, p0, Lcom/google/android/music/ui/GridAdapterWrapperWithHeader;->mNumColumns:I

    .line 113
    invoke-direct {p0, p3, p4}, Lcom/google/android/music/ui/GridAdapterWrapperWithHeader;->getClusterMetadata(II)Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/GridAdapterWrapperWithHeader;->mClusterConfig:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;

    .line 114
    return-void
.end method

.method private getCardGridMetadata()Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;
    .locals 1

    .prologue
    .line 98
    sget-object v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    return-object v0
.end method

.method private getClusterMetadata(II)Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;
    .locals 2
    .param p1, "numColumns"    # I
    .param p2, "numRows"    # I

    .prologue
    const/4 v0, 0x1

    .line 81
    packed-switch p1, :pswitch_data_0

    .line 91
    new-instance v0, Ljava/security/InvalidParameterException;

    const-string v1, "Unsupported cluster configuration"

    invoke-direct {v0, v1}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 83
    :pswitch_0
    sget-object v0, Lcom/google/android/music/ui/GridAdapterWrapperWithHeader;->sClusterList:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;

    .line 89
    :goto_0
    return-object v0

    .line 85
    :pswitch_1
    sget-object v1, Lcom/google/android/music/ui/GridAdapterWrapperWithHeader;->sClusterList:Ljava/util/List;

    if-ne p2, v0, :cond_0

    :goto_1
    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;

    goto :goto_0

    :cond_0
    const/4 v0, 0x2

    goto :goto_1

    .line 87
    :pswitch_2
    sget-object v0, Lcom/google/android/music/ui/GridAdapterWrapperWithHeader;->sClusterList:Ljava/util/List;

    const/4 v1, 0x4

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;

    goto :goto_0

    .line 89
    :pswitch_3
    sget-object v0, Lcom/google/android/music/ui/GridAdapterWrapperWithHeader;->sClusterList:Ljava/util/List;

    const/4 v1, 0x3

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;

    goto :goto_0

    .line 81
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private getHeaderClusterView(Landroid/view/View;Landroid/view/ViewGroup;I)Landroid/view/View;
    .locals 9
    .param p1, "convertView"    # Landroid/view/View;
    .param p2, "parent"    # Landroid/view/ViewGroup;
    .param p3, "dataCount"    # I

    .prologue
    .line 153
    iget-object v6, p0, Lcom/google/android/music/ui/GridAdapterWrapperWithHeader;->mClusterConfig:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;

    invoke-virtual {v6}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->getTileCount()I

    move-result v5

    .line 154
    .local v5, "tileCount":I
    if-gt p3, v5, :cond_1

    move v3, p3

    .line 156
    .local v3, "countToBind":I
    :goto_0
    instance-of v6, p1, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    if-eqz v6, :cond_2

    check-cast p1, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    .end local p1    # "convertView":Landroid/view/View;
    move-object v1, p1

    .line 160
    .local v1, "cluster":Lcom/google/android/music/ui/cardlib/PlayCardClusterView;
    :goto_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 161
    .local v2, "clusterDocs":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/ui/cardlib/model/Document;>;"
    invoke-virtual {v1}, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->hideHeader()V

    .line 163
    iget-object v6, p0, Lcom/google/android/music/ui/GridAdapterWrapperWithHeader;->mClusterConfig:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;

    const/4 v7, 0x0

    invoke-virtual {v1, v6, v2, v7}, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->setMetadata(Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;Ljava/util/List;Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;)V

    .line 164
    invoke-virtual {v1}, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->hasCards()Z

    move-result v6

    if-nez v6, :cond_0

    .line 165
    iget-object v6, p0, Lcom/google/android/music/ui/GridAdapterWrapperWithHeader;->mCardHeap:Lcom/google/android/music/ui/cardlib/layout/PlayCardHeap;

    invoke-virtual {v1, v6}, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->inflateContent(Lcom/google/android/music/ui/cardlib/layout/PlayCardHeap;)V

    .line 168
    :cond_0
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_2
    if-ge v4, v3, :cond_3

    .line 169
    invoke-virtual {v1, v4}, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->getCardChild(I)Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    move-result-object v0

    .line 170
    .local v0, "card":Landroid/view/View;
    iget-object v6, p0, Lcom/google/android/music/ui/GridAdapterWrapperWithHeader;->mWrappedAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v6, v4, v0, p2}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 171
    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-interface {v2, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 168
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .end local v0    # "card":Landroid/view/View;
    .end local v1    # "cluster":Lcom/google/android/music/ui/cardlib/PlayCardClusterView;
    .end local v2    # "clusterDocs":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/ui/cardlib/model/Document;>;"
    .end local v3    # "countToBind":I
    .end local v4    # "i":I
    .restart local p1    # "convertView":Landroid/view/View;
    :cond_1
    move v3, v5

    .line 154
    goto :goto_0

    .line 156
    .restart local v3    # "countToBind":I
    :cond_2
    iget-object v6, p0, Lcom/google/android/music/ui/GridAdapterWrapperWithHeader;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v7, 0x7f040092

    const/4 v8, 0x0

    invoke-virtual {v6, v7, p2, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    move-object v1, v6

    goto :goto_1

    .line 173
    .end local p1    # "convertView":Landroid/view/View;
    .restart local v1    # "cluster":Lcom/google/android/music/ui/cardlib/PlayCardClusterView;
    .restart local v2    # "clusterDocs":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/ui/cardlib/model/Document;>;"
    .restart local v4    # "i":I
    :cond_3
    invoke-virtual {v1}, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;->createContent()V

    .line 175
    return-object v1
.end method


# virtual methods
.method public areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 228
    const/4 v0, 0x0

    return v0
.end method

.method public getCount()I
    .locals 6

    .prologue
    .line 130
    iget-object v2, p0, Lcom/google/android/music/ui/GridAdapterWrapperWithHeader;->mWrappedAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v2}, Landroid/widget/ListAdapter;->getCount()I

    move-result v1

    .line 131
    .local v1, "dataCount":I
    iget-object v2, p0, Lcom/google/android/music/ui/GridAdapterWrapperWithHeader;->mClusterConfig:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;

    invoke-virtual {v2}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->getTileCount()I

    move-result v0

    .line 132
    .local v0, "clusterCardCount":I
    if-nez v1, :cond_0

    .line 133
    const/4 v2, 0x0

    .line 138
    :goto_0
    return v2

    .line 134
    :cond_0
    if-gt v1, v0, :cond_1

    .line 135
    const/4 v2, 0x1

    goto :goto_0

    .line 138
    :cond_1
    sub-int v2, v1, v0

    int-to-double v2, v2

    iget v4, p0, Lcom/google/android/music/ui/GridAdapterWrapperWithHeader;->mNumColumns:I

    int-to-double v4, v4

    div-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v2, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 144
    const/4 v0, 0x0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 149
    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 119
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 12
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v11, 0x4

    const/4 v10, -0x2

    const/4 v7, 0x0

    const/4 v9, 0x0

    .line 180
    iget-object v6, p0, Lcom/google/android/music/ui/GridAdapterWrapperWithHeader;->mWrappedAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v6}, Landroid/widget/ListAdapter;->getCount()I

    move-result v5

    .line 182
    .local v5, "wrappedCount":I
    if-nez v5, :cond_1

    move-object v2, v7

    .line 222
    :cond_0
    :goto_0
    return-object v2

    .line 184
    :cond_1
    if-nez p1, :cond_2

    invoke-direct {p0, p2, p3, v5}, Lcom/google/android/music/ui/GridAdapterWrapperWithHeader;->getHeaderClusterView(Landroid/view/View;Landroid/view/ViewGroup;I)Landroid/view/View;

    move-result-object v2

    goto :goto_0

    .line 186
    :cond_2
    if-eqz p2, :cond_3

    instance-of v6, p2, Lcom/google/android/music/ui/cardlib/PlayCardClusterView;

    if-eqz v6, :cond_6

    .line 188
    :cond_3
    new-instance v2, Landroid/widget/LinearLayout;

    iget-object v6, p0, Lcom/google/android/music/ui/GridAdapterWrapperWithHeader;->mContext:Landroid/content/Context;

    invoke-direct {v2, v6}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 189
    .local v2, "group":Landroid/widget/LinearLayout;
    invoke-virtual {v2, v9}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 190
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    iget v6, p0, Lcom/google/android/music/ui/GridAdapterWrapperWithHeader;->mNumColumns:I

    if-ge v3, v6, :cond_0

    .line 191
    add-int/lit8 v6, p1, -0x1

    iget v8, p0, Lcom/google/android/music/ui/GridAdapterWrapperWithHeader;->mNumColumns:I

    mul-int/2addr v6, v8

    add-int/2addr v6, v3

    iget-object v8, p0, Lcom/google/android/music/ui/GridAdapterWrapperWithHeader;->mClusterConfig:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;

    invoke-virtual {v8}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->getTileCount()I

    move-result v8

    add-int v4, v6, v8

    .line 193
    .local v4, "mappedPosition":I
    if-ge v4, v5, :cond_5

    .line 194
    iget-object v6, p0, Lcom/google/android/music/ui/GridAdapterWrapperWithHeader;->mWrappedAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v6, v4, v7, v2}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 201
    .local v0, "child":Landroid/view/View;
    :goto_2
    instance-of v6, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    if-eqz v6, :cond_4

    move-object v6, v0

    .line 202
    check-cast v6, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    invoke-direct {p0}, Lcom/google/android/music/ui/GridAdapterWrapperWithHeader;->getCardGridMetadata()Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->getThumbnailAspectRatio()F

    move-result v8

    invoke-virtual {v6, v8}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->setThumbnailAspectRatio(F)V

    .line 205
    :cond_4
    new-instance v6, Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v8, 0x3f800000    # 1.0f

    invoke-direct {v6, v10, v10, v8}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    invoke-virtual {v0, v6}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 207
    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 190
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 198
    .end local v0    # "child":Landroid/view/View;
    :cond_5
    iget-object v6, p0, Lcom/google/android/music/ui/GridAdapterWrapperWithHeader;->mWrappedAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v6, v9, v7, v2}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 199
    .restart local v0    # "child":Landroid/view/View;
    invoke-virtual {v0, v11}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    .end local v0    # "child":Landroid/view/View;
    .end local v2    # "group":Landroid/widget/LinearLayout;
    .end local v3    # "i":I
    .end local v4    # "mappedPosition":I
    :cond_6
    move-object v2, p2

    .line 211
    check-cast v2, Landroid/view/ViewGroup;

    .line 212
    .local v2, "group":Landroid/view/ViewGroup;
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_3
    invoke-virtual {v2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v6

    if-ge v3, v6, :cond_0

    .line 213
    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 214
    .local v1, "childConvertView":Landroid/view/View;
    add-int/lit8 v6, p1, -0x1

    iget v7, p0, Lcom/google/android/music/ui/GridAdapterWrapperWithHeader;->mNumColumns:I

    mul-int/2addr v6, v7

    add-int/2addr v6, v3

    iget-object v7, p0, Lcom/google/android/music/ui/GridAdapterWrapperWithHeader;->mClusterConfig:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;

    invoke-virtual {v7}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->getTileCount()I

    move-result v7

    add-int v4, v6, v7

    .line 215
    .restart local v4    # "mappedPosition":I
    if-ge v4, v5, :cond_7

    .line 216
    iget-object v6, p0, Lcom/google/android/music/ui/GridAdapterWrapperWithHeader;->mWrappedAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v6, v4, v1, v2}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    .line 217
    invoke-virtual {v1, v9}, Landroid/view/View;->setVisibility(I)V

    .line 212
    :goto_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 219
    :cond_7
    invoke-virtual {v1, v11}, Landroid/view/View;->setVisibility(I)V

    goto :goto_4
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 125
    const/4 v0, 0x2

    return v0
.end method

.method public isEnabled(I)Z
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 233
    const/4 v0, 0x0

    return v0
.end method
