.class public Lcom/google/android/music/dial/SsdpMessage$Builder;
.super Ljava/lang/Object;
.source "SsdpMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/dial/SsdpMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private final mHeaders:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mMessageType:Lcom/google/android/music/dial/SsdpMessage$MessageType;


# direct methods
.method public constructor <init>(Lcom/google/android/music/dial/SsdpMessage$MessageType;)V
    .locals 1
    .param p1, "messageType"    # Lcom/google/android/music/dial/SsdpMessage$MessageType;

    .prologue
    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    invoke-static {}, Lcom/google/common/collect/Maps;->newLinkedHashMap()Ljava/util/LinkedHashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/dial/SsdpMessage$Builder;->mHeaders:Ljava/util/LinkedHashMap;

    .line 83
    iput-object p1, p0, Lcom/google/android/music/dial/SsdpMessage$Builder;->mMessageType:Lcom/google/android/music/dial/SsdpMessage$MessageType;

    .line 84
    return-void
.end method


# virtual methods
.method public build()Lcom/google/android/music/dial/SsdpMessage;
    .locals 4

    .prologue
    .line 87
    new-instance v0, Lcom/google/android/music/dial/SsdpMessage;

    iget-object v1, p0, Lcom/google/android/music/dial/SsdpMessage$Builder;->mMessageType:Lcom/google/android/music/dial/SsdpMessage$MessageType;

    iget-object v2, p0, Lcom/google/android/music/dial/SsdpMessage$Builder;->mHeaders:Ljava/util/LinkedHashMap;

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/music/dial/SsdpMessage;-><init>(Lcom/google/android/music/dial/SsdpMessage$MessageType;Ljava/util/LinkedHashMap;Lcom/google/android/music/dial/SsdpMessage$1;)V

    return-object v0
.end method

.method public withHeader(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/music/dial/SsdpMessage$Builder;
    .locals 5
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 91
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 92
    .local v0, "nName":Ljava/lang/String;
    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 94
    .local v1, "nValue":Ljava/lang/String;
    iget-object v2, p0, Lcom/google/android/music/dial/SsdpMessage$Builder;->mHeaders:Ljava/util/LinkedHashMap;

    invoke-virtual {v2, v0}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 95
    iget-object v2, p0, Lcom/google/android/music/dial/SsdpMessage$Builder;->mHeaders:Ljava/util/LinkedHashMap;

    invoke-virtual {v2, v0}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 99
    :goto_0
    return-object p0

    .line 97
    :cond_0
    iget-object v2, p0, Lcom/google/android/music/dial/SsdpMessage$Builder;->mHeaders:Ljava/util/LinkedHashMap;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    invoke-static {v3}, Lcom/google/common/collect/Lists;->newArrayList([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
