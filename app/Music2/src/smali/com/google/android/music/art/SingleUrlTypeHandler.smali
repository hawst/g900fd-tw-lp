.class public Lcom/google/android/music/art/SingleUrlTypeHandler;
.super Lcom/google/android/music/art/ArtTypeHandler;
.source "SingleUrlTypeHandler.java"


# instance fields
.field private final mSingleImageCropPostProcessor:Lcom/google/android/music/art/SingleImageCropPostProcessor;


# direct methods
.method constructor <init>(Lcom/google/android/music/art/ArtResolver2;Landroid/content/Context;)V
    .locals 1
    .param p1, "resolver"    # Lcom/google/android/music/art/ArtResolver2;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/art/ArtTypeHandler;-><init>(Lcom/google/android/music/art/ArtResolver2;Landroid/content/Context;)V

    .line 15
    invoke-static {}, Lcom/google/android/music/art/SingleImageCropPostProcessor;->getInstance()Lcom/google/android/music/art/SingleImageCropPostProcessor;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/art/SingleUrlTypeHandler;->mSingleImageCropPostProcessor:Lcom/google/android/music/art/SingleImageCropPostProcessor;

    .line 20
    return-void
.end method


# virtual methods
.method public fillNeededItemsImpl(Lcom/google/android/music/art/ArtRequest2;)V
    .locals 3
    .param p1, "request"    # Lcom/google/android/music/art/ArtRequest2;

    .prologue
    .line 29
    const-string v1, "request must not be null"

    invoke-static {p1, v1}, Lcom/google/android/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 30
    invoke-virtual {p1}, Lcom/google/android/music/art/ArtRequest2;->getDescriptor()Lcom/google/android/music/art/ArtDescriptor;

    move-result-object v1

    instance-of v1, v1, Lcom/google/android/music/art/SingleUrlArtDescriptor;

    const-string v2, "SingleUrlTypeHandler only handles SingleUrlArtDescriptors"

    invoke-static {v1, v2}, Lcom/google/android/common/base/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    .line 32
    invoke-virtual {p1}, Lcom/google/android/music/art/ArtRequest2;->getDescriptor()Lcom/google/android/music/art/ArtDescriptor;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/art/SingleUrlArtDescriptor;

    .line 33
    .local v0, "descriptor":Lcom/google/android/music/art/SingleUrlArtDescriptor;
    iget-object v1, v0, Lcom/google/android/music/art/SingleUrlArtDescriptor;->artUrl:Lcom/google/android/music/art/ArtResolver2$ArtUrl;

    const-string v2, "Descriptor\'s artUrl must not be null"

    invoke-static {v1, v2}, Lcom/google/android/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 34
    iget-object v1, v0, Lcom/google/android/music/art/SingleUrlArtDescriptor;->artUrl:Lcom/google/android/music/art/ArtResolver2$ArtUrl;

    invoke-virtual {p1, v1}, Lcom/google/android/music/art/ArtRequest2;->addNeededItem(Lcom/google/android/music/art/ArtResolver2$ArtUrl;)V

    .line 35
    return-void
.end method

.method public getPostProcessor(Lcom/google/android/music/art/ArtDescriptor;)Lcom/google/android/music/art/ArtPostProcessor;
    .locals 1
    .param p1, "descriptor"    # Lcom/google/android/music/art/ArtDescriptor;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/music/art/SingleUrlTypeHandler;->mSingleImageCropPostProcessor:Lcom/google/android/music/art/SingleImageCropPostProcessor;

    return-object v0
.end method
