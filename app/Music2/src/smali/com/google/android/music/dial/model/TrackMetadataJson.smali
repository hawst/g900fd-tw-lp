.class public Lcom/google/android/music/dial/model/TrackMetadataJson;
.super Lcom/google/api/client/json/GenericJson;
.source "TrackMetadataJson.java"


# instance fields
.field public mAlbumArtUrl:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "albumArtUrl"
    .end annotation
.end field

.field public mAlbumArtist:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "albumArtist"
    .end annotation
.end field

.field public mAlbumTitle:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "albumTitle"
    .end annotation
.end field

.field public mContentType:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "contentType"
    .end annotation
.end field

.field public mDurationMillis:J
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "durationMillis"
    .end annotation
.end field

.field public mTrackArtist:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "trackArtist"
    .end annotation
.end field

.field public mTrackTitle:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "trackTitle"
    .end annotation
.end field

.field public mTrackUrl:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "trackUrl"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    return-void
.end method
