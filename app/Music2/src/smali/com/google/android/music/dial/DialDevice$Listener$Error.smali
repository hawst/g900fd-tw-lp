.class public final enum Lcom/google/android/music/dial/DialDevice$Listener$Error;
.super Ljava/lang/Enum;
.source "DialDevice.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/dial/DialDevice$Listener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Error"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/music/dial/DialDevice$Listener$Error;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/music/dial/DialDevice$Listener$Error;

.field public static final enum SOCKET_CONNECT:Lcom/google/android/music/dial/DialDevice$Listener$Error;

.field public static final enum SOCKET_RECONNECT:Lcom/google/android/music/dial/DialDevice$Listener$Error;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 59
    new-instance v0, Lcom/google/android/music/dial/DialDevice$Listener$Error;

    const-string v1, "SOCKET_CONNECT"

    invoke-direct {v0, v1, v2}, Lcom/google/android/music/dial/DialDevice$Listener$Error;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/dial/DialDevice$Listener$Error;->SOCKET_CONNECT:Lcom/google/android/music/dial/DialDevice$Listener$Error;

    .line 60
    new-instance v0, Lcom/google/android/music/dial/DialDevice$Listener$Error;

    const-string v1, "SOCKET_RECONNECT"

    invoke-direct {v0, v1, v3}, Lcom/google/android/music/dial/DialDevice$Listener$Error;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/dial/DialDevice$Listener$Error;->SOCKET_RECONNECT:Lcom/google/android/music/dial/DialDevice$Listener$Error;

    .line 58
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/music/dial/DialDevice$Listener$Error;

    sget-object v1, Lcom/google/android/music/dial/DialDevice$Listener$Error;->SOCKET_CONNECT:Lcom/google/android/music/dial/DialDevice$Listener$Error;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/music/dial/DialDevice$Listener$Error;->SOCKET_RECONNECT:Lcom/google/android/music/dial/DialDevice$Listener$Error;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/music/dial/DialDevice$Listener$Error;->$VALUES:[Lcom/google/android/music/dial/DialDevice$Listener$Error;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 58
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/music/dial/DialDevice$Listener$Error;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 58
    const-class v0, Lcom/google/android/music/dial/DialDevice$Listener$Error;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/dial/DialDevice$Listener$Error;

    return-object v0
.end method

.method public static values()[Lcom/google/android/music/dial/DialDevice$Listener$Error;
    .locals 1

    .prologue
    .line 58
    sget-object v0, Lcom/google/android/music/dial/DialDevice$Listener$Error;->$VALUES:[Lcom/google/android/music/dial/DialDevice$Listener$Error;

    invoke-virtual {v0}, [Lcom/google/android/music/dial/DialDevice$Listener$Error;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/music/dial/DialDevice$Listener$Error;

    return-object v0
.end method
