.class public Lcom/google/android/music/ui/BaseFragment;
.super Landroid/support/v4/app/Fragment;
.source "BaseFragment.java"

# interfaces
.implements Lcom/google/android/music/ui/MusicFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method


# virtual methods
.method protected getActionBarController()Lcom/google/android/music/ui/ActionBarController;
    .locals 1

    .prologue
    .line 178
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseFragment;->getBaseActivity()Lcom/google/android/music/ui/BaseActivity;

    move-result-object v0

    .line 179
    .local v0, "controller":Lcom/google/android/music/ui/ActionBarController;
    if-nez v0, :cond_0

    .line 180
    new-instance v0, Lcom/google/android/music/ui/NoOpActionBarControllerImpl;

    .end local v0    # "controller":Lcom/google/android/music/ui/ActionBarController;
    invoke-direct {v0}, Lcom/google/android/music/ui/NoOpActionBarControllerImpl;-><init>()V

    .line 182
    .restart local v0    # "controller":Lcom/google/android/music/ui/ActionBarController;
    :cond_0
    return-object v0
.end method

.method public getAlbumId()J
    .locals 2

    .prologue
    .line 53
    const-wide/16 v0, -0x1

    return-wide v0
.end method

.method public getAlbumMetajamId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x0

    return-object v0
.end method

.method public getArtistId()J
    .locals 2

    .prologue
    .line 48
    const-wide/16 v0, -0x1

    return-wide v0
.end method

.method public getArtistMetajamId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getBaseActivity()Lcom/google/android/music/ui/BaseActivity;
    .locals 1

    .prologue
    .line 64
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/ui/BaseActivity;

    return-object v0
.end method

.method protected getBottomDrawer()Lcom/google/android/music/widgets/ExpandingScrollView;
    .locals 2

    .prologue
    .line 190
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseFragment;->getBaseActivity()Lcom/google/android/music/ui/BaseActivity;

    move-result-object v0

    .line 191
    .local v0, "activity":Lcom/google/android/music/ui/BaseActivity;
    if-eqz v0, :cond_0

    .line 192
    invoke-virtual {v0}, Lcom/google/android/music/ui/BaseActivity;->getBottomDrawer()Lcom/google/android/music/widgets/ExpandingScrollView;

    move-result-object v1

    .line 194
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final getFragment()Landroid/support/v4/app/Fragment;
    .locals 0

    .prologue
    .line 28
    return-object p0
.end method

.method public getFragmentMediaList()Lcom/google/android/music/medialist/MediaList;
    .locals 1

    .prologue
    .line 33
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getPreferences()Lcom/google/android/music/preferences/MusicPreferences;
    .locals 1

    .prologue
    .line 71
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseFragment;->getUIStateManager()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    return-object v0
.end method

.method protected getUIStateManager()Lcom/google/android/music/ui/UIStateManager;
    .locals 1

    .prologue
    .line 78
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v0

    return-object v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 127
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 128
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->CALLS:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0, p0}, Lcom/google/android/music/utils/DebugUtils;->maybeLogMethodName(Lcom/google/android/music/utils/DebugUtils$MusicTag;Ljava/lang/Object;)V

    .line 129
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 3
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 101
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 102
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->CALLS:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0, p0}, Lcom/google/android/music/utils/DebugUtils;->maybeLogMethodName(Lcom/google/android/music/utils/DebugUtils$MusicTag;Ljava/lang/Object;)V

    .line 105
    instance-of v0, p1, Lcom/google/android/music/ui/BaseActivity;

    if-nez v0, :cond_0

    .line 106
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "BaseFragment is hosted by "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " which is not a BaseActivity"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 109
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 113
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 114
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->CALLS:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0, p0}, Lcom/google/android/music/utils/DebugUtils;->maybeLogMethodName(Lcom/google/android/music/utils/DebugUtils$MusicTag;Ljava/lang/Object;)V

    .line 115
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 120
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    .line 121
    .local v0, "result":Landroid/view/View;
    sget-object v1, Lcom/google/android/music/utils/DebugUtils$MusicTag;->CALLS:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v1, p0}, Lcom/google/android/music/utils/DebugUtils;->maybeLogMethodName(Lcom/google/android/music/utils/DebugUtils$MusicTag;Ljava/lang/Object;)V

    .line 122
    return-object v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 163
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 164
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->CALLS:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0, p0}, Lcom/google/android/music/utils/DebugUtils;->maybeLogMethodName(Lcom/google/android/music/utils/DebugUtils$MusicTag;Ljava/lang/Object;)V

    .line 165
    return-void
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 157
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    .line 158
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->CALLS:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0, p0}, Lcom/google/android/music/utils/DebugUtils;->maybeLogMethodName(Lcom/google/android/music/utils/DebugUtils$MusicTag;Ljava/lang/Object;)V

    .line 159
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 169
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    .line 170
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->CALLS:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0, p0}, Lcom/google/android/music/utils/DebugUtils;->maybeLogMethodName(Lcom/google/android/music/utils/DebugUtils$MusicTag;Ljava/lang/Object;)V

    .line 171
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 145
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 146
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->CALLS:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0, p0}, Lcom/google/android/music/utils/DebugUtils;->maybeLogMethodName(Lcom/google/android/music/utils/DebugUtils$MusicTag;Ljava/lang/Object;)V

    .line 147
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 139
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 140
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->CALLS:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0, p0}, Lcom/google/android/music/utils/DebugUtils;->maybeLogMethodName(Lcom/google/android/music/utils/DebugUtils$MusicTag;Ljava/lang/Object;)V

    .line 141
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 95
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 96
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->CALLS:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0, p0}, Lcom/google/android/music/utils/DebugUtils;->maybeLogMethodName(Lcom/google/android/music/utils/DebugUtils$MusicTag;Ljava/lang/Object;)V

    .line 97
    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 133
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStart()V

    .line 134
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->CALLS:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0, p0}, Lcom/google/android/music/utils/DebugUtils;->maybeLogMethodName(Lcom/google/android/music/utils/DebugUtils$MusicTag;Ljava/lang/Object;)V

    .line 135
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 151
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStop()V

    .line 152
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->CALLS:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0, p0}, Lcom/google/android/music/utils/DebugUtils;->maybeLogMethodName(Lcom/google/android/music/utils/DebugUtils$MusicTag;Ljava/lang/Object;)V

    .line 153
    return-void
.end method

.method public onTutorialCardClosed()V
    .locals 0

    .prologue
    .line 58
    return-void
.end method
