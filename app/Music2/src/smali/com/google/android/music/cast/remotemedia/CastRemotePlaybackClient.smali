.class public Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;
.super Lcom/google/android/music/utils/LoggableHandler;
.source "CastRemotePlaybackClient.java"

# interfaces
.implements Lcom/google/android/music/cast/remotemedia/RemotePlaybackClient;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$SkipToItemRequest;,
        Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$SeekRequest;,
        Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$LoadCloudQueueRequest;,
        Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$CustomMessageCallback;,
        Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;,
        Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionFailedListener;,
        Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$CastListener;
    }
.end annotation


# instance fields
.field private final LOGV:Z

.field private mApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

.field private mAppContext:Ljava/lang/String;

.field private final mCastDevice:Lcom/google/android/gms/cast/CastDevice;

.field private final mCastListener:Lcom/google/android/gms/cast/Cast$Listener;

.field private final mCastTokenClient:Lcom/google/android/music/cast/CastTokenClient;

.field private final mConnectionCallbacks:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;

.field private final mConnectionFailedListener:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionFailedListener;

.field private final mContext:Landroid/content/Context;

.field private final mCustomMessageCallback:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$CustomMessageCallback;

.field private mInitializationRetriesLeft:I

.field private mIsPreparing:Z

.field private mIsQueueLoaded:Z

.field private mIsSessionInitialized:Z

.field private mIsStreaming:Z

.field private mItemId:Ljava/lang/String;

.field private mKnownPositionMillis:J

.field private mLastRefreshTimestampMillis:J

.field private final mLock:Ljava/lang/Object;

.field private mMusicFile:Lcom/google/android/music/store/MusicFile;

.field private mPositionDeltaStopWatch:Lcom/google/android/music/playback/StopWatch;

.field private mRemoteMediaPlayer:Lcom/google/android/gms/cast/RemoteMediaPlayer;

.field private final mRoute:Landroid/support/v7/media/MediaRouter$RouteInfo;

.field private final mServiceHooks:Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;

.field private mSessionId:Ljava/lang/String;

.field private mVolume:D


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/support/v7/media/MediaRouter$RouteInfo;Lcom/google/android/gms/cast/CastDevice;Lcom/google/android/music/cast/CastTokenClient;Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;Ljava/lang/String;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "route"    # Landroid/support/v7/media/MediaRouter$RouteInfo;
    .param p3, "castDevice"    # Lcom/google/android/gms/cast/CastDevice;
    .param p4, "castTokenClient"    # Lcom/google/android/music/cast/CastTokenClient;
    .param p5, "serviceHooks"    # Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;
    .param p6, "appId"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 103
    const-string v0, "CastRemotePlaybackClient"

    invoke-direct {p0, v0}, Lcom/google/android/music/utils/LoggableHandler;-><init>(Ljava/lang/String;)V

    .line 54
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->CAST_REMOTE:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->LOGV:Z

    .line 80
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mLock:Ljava/lang/Object;

    .line 91
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mKnownPositionMillis:J

    .line 92
    new-instance v0, Lcom/google/android/music/playback/StopWatch;

    invoke-direct {v0}, Lcom/google/android/music/playback/StopWatch;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mPositionDeltaStopWatch:Lcom/google/android/music/playback/StopWatch;

    .line 104
    iput-object p1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mContext:Landroid/content/Context;

    .line 105
    iput-object p2, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mRoute:Landroid/support/v7/media/MediaRouter$RouteInfo;

    .line 106
    iput-object p3, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mCastDevice:Lcom/google/android/gms/cast/CastDevice;

    .line 107
    iput-object p4, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mCastTokenClient:Lcom/google/android/music/cast/CastTokenClient;

    .line 108
    iput-object p5, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mServiceHooks:Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;

    .line 111
    new-instance v0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$CastListener;

    invoke-direct {v0, p0, v3}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$CastListener;-><init>(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$1;)V

    iput-object v0, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mCastListener:Lcom/google/android/gms/cast/Cast$Listener;

    .line 112
    new-instance v0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;

    invoke-direct {v0, p0, p6, v3}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;-><init>(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;Ljava/lang/String;Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$1;)V

    iput-object v0, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mConnectionCallbacks:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;

    .line 113
    new-instance v0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionFailedListener;

    invoke-direct {v0, p0, v3}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionFailedListener;-><init>(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$1;)V

    iput-object v0, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mConnectionFailedListener:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionFailedListener;

    .line 114
    new-instance v0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$CustomMessageCallback;

    invoke-direct {v0, p0, v3}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$CustomMessageCallback;-><init>(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$1;)V

    iput-object v0, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mCustomMessageCallback:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$CustomMessageCallback;

    .line 116
    iput-boolean v2, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mIsQueueLoaded:Z

    .line 117
    iput-boolean v2, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mIsSessionInitialized:Z

    .line 118
    iput-boolean v2, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mIsPreparing:Z

    .line 119
    iput-boolean v2, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mIsStreaming:Z

    .line 121
    const/16 v0, 0x14

    iput v0, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mInitializationRetriesLeft:I

    .line 122
    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mServiceHooks:Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->tearDown()V

    return-void
.end method

.method static synthetic access$1200(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)Lcom/google/android/gms/common/api/GoogleApiClient;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mSessionId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1302(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 50
    iput-object p1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mSessionId:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1500(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mAppContext:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1602(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;
    .param p1, "x1"    # Z

    .prologue
    .line 50
    iput-boolean p1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mIsSessionInitialized:Z

    return p1
.end method

.method static synthetic access$1700(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$CustomMessageCallback;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mCustomMessageCallback:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$CustomMessageCallback;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)Lcom/google/android/gms/cast/RemoteMediaPlayer;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mRemoteMediaPlayer:Lcom/google/android/gms/cast/RemoteMediaPlayer;

    return-object v0
.end method

.method static synthetic access$1802(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/RemoteMediaPlayer;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;
    .param p1, "x1"    # Lcom/google/android/gms/cast/RemoteMediaPlayer;

    .prologue
    .line 50
    iput-object p1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mRemoteMediaPlayer:Lcom/google/android/gms/cast/RemoteMediaPlayer;

    return-object p1
.end method

.method static synthetic access$2000(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;Lorg/json/JSONObject;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;
    .param p1, "x1"    # Lorg/json/JSONObject;

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->updateCurrentItemIdFromCustomData(Lorg/json/JSONObject;)V

    return-void
.end method

.method static synthetic access$2102(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;D)D
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;
    .param p1, "x1"    # D

    .prologue
    .line 50
    iput-wide p1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mVolume:D

    return-wide p1
.end method

.method static synthetic access$2200(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mItemId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    .prologue
    .line 50
    iget-boolean v0, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->LOGV:Z

    return v0
.end method

.method static synthetic access$502(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;
    .param p1, "x1"    # Z

    .prologue
    .line 50
    iput-boolean p1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mIsStreaming:Z

    return p1
.end method

.method static synthetic access$600(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;J)V
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;
    .param p1, "x1"    # J

    .prologue
    .line 50
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->updatePosition(J)V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)Lcom/google/android/music/playback/StopWatch;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mPositionDeltaStopWatch:Lcom/google/android/music/playback/StopWatch;

    return-object v0
.end method

.method static synthetic access$802(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;
    .param p1, "x1"    # Z

    .prologue
    .line 50
    iput-boolean p1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mIsQueueLoaded:Z

    return p1
.end method

.method static synthetic access$902(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;
    .param p1, "x1"    # Z

    .prologue
    .line 50
    iput-boolean p1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mIsPreparing:Z

    return p1
.end method

.method private getFreshCastToken()Ljava/lang/String;
    .locals 5

    .prologue
    .line 1270
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "receiver:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mRoute:Landroid/support/v7/media/MediaRouter$RouteInfo;

    invoke-virtual {v2}, Landroid/support/v7/media/MediaRouter$RouteInfo;->getId()Ljava/lang/String;

    move-result-object v2

    const-string v3, ":"

    const-string v4, "_"

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1272
    .local v0, "remoteEndpointId":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mCastTokenClient:Lcom/google/android/music/cast/CastTokenClient;

    invoke-interface {v1, v0}, Lcom/google/android/music/cast/CastTokenClient;->clearCachedCastToken(Ljava/lang/String;)V

    .line 1273
    iget-object v1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mCastTokenClient:Lcom/google/android/music/cast/CastTokenClient;

    iget-object v2, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mRoute:Landroid/support/v7/media/MediaRouter$RouteInfo;

    invoke-virtual {v2}, Landroid/support/v7/media/MediaRouter$RouteInfo;->getProvider()Landroid/support/v7/media/MediaRouter$ProviderInfo;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v7/media/MediaRouter$ProviderInfo;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-interface {v1, v2, v0, v3}, Lcom/google/android/music/cast/CastTokenClient;->getCastToken(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private handleJoinSession(Ljava/lang/String;)V
    .locals 4
    .param p1, "appContext"    # Ljava/lang/String;

    .prologue
    .line 325
    iput-object p1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mAppContext:Ljava/lang/String;

    .line 329
    iget-object v1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mCastDevice:Lcom/google/android/gms/cast/CastDevice;

    iget-object v2, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mCastListener:Lcom/google/android/gms/cast/Cast$Listener;

    invoke-static {v1, v2}, Lcom/google/android/gms/cast/Cast$CastOptions;->builder(Lcom/google/android/gms/cast/CastDevice;Lcom/google/android/gms/cast/Cast$Listener;)Lcom/google/android/gms/cast/Cast$CastOptions$Builder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/android/gms/cast/Cast$CastOptions$Builder;->setVerboseLoggingEnabled(Z)Lcom/google/android/gms/cast/Cast$CastOptions$Builder;

    move-result-object v0

    .line 332
    .local v0, "apiOptionsBuilder":Lcom/google/android/gms/cast/Cast$CastOptions$Builder;
    new-instance v1, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    iget-object v2, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;-><init>(Landroid/content/Context;)V

    sget-object v2, Lcom/google/android/gms/cast/Cast;->API:Lcom/google/android/gms/common/api/Api;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/Cast$CastOptions$Builder;->build()Lcom/google/android/gms/cast/Cast$CastOptions;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;->addApi(Lcom/google/android/gms/common/api/Api;Lcom/google/android/gms/common/api/Api$ApiOptions$HasOptions;)Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mConnectionCallbacks:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;->addConnectionCallbacks(Lcom/google/android/gms/common/api/GoogleApiClient$ConnectionCallbacks;)Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mConnectionFailedListener:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionFailedListener;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;->addOnConnectionFailedListener(Lcom/google/android/gms/common/api/GoogleApiClient$OnConnectionFailedListener;)Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;->build()Lcom/google/android/gms/common/api/GoogleApiClient;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    .line 337
    iget-object v1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-interface {v1}, Lcom/google/android/gms/common/api/GoogleApiClient;->connect()V

    .line 338
    return-void
.end method

.method private handleLeaveSession()V
    .locals 2

    .prologue
    .line 341
    iget-object v0, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/GoogleApiClient;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 342
    sget-object v0, Lcom/google/android/gms/cast/Cast;->CastApi:Lcom/google/android/gms/cast/Cast$CastApi;

    iget-object v1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-interface {v0, v1}, Lcom/google/android/gms/cast/Cast$CastApi;->leaveApplication(Lcom/google/android/gms/common/api/GoogleApiClient;)Lcom/google/android/gms/common/api/PendingResult;

    move-result-object v0

    new-instance v1, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$1;

    invoke-direct {v1, p0}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$1;-><init>(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/PendingResult;->setResultCallback(Lcom/google/android/gms/common/api/ResultCallback;)V

    .line 355
    invoke-direct {p0}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->tearDown()V

    .line 357
    :cond_0
    return-void
.end method

.method private handleLoadCloudQueue(Ljava/lang/String;Lcom/google/android/music/store/MusicFile;Ljava/lang/String;JZ)V
    .locals 16
    .param p1, "queueBaseUrl"    # Ljava/lang/String;
    .param p2, "musicFile"    # Lcom/google/android/music/store/MusicFile;
    .param p3, "itemId"    # Ljava/lang/String;
    .param p4, "positionMillis"    # J
    .param p6, "playOnCompletion"    # Z

    .prologue
    .line 366
    if-nez p6, :cond_0

    .line 450
    :goto_0
    return-void

    .line 369
    :cond_0
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mIsPreparing:Z

    .line 370
    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mMusicFile:Lcom/google/android/music/store/MusicFile;

    .line 371
    move-object/from16 v0, p3

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mItemId:Ljava/lang/String;

    .line 373
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mServiceHooks:Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;

    invoke-interface {v2}, Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;->notifyOpenStarted()V

    .line 375
    invoke-direct/range {p0 .. p0}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->readyForLoad()Z

    move-result v2

    if-nez v2, :cond_2

    .line 376
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mInitializationRetriesLeft:I

    add-int/lit8 v3, v2, -0x1

    move-object/from16 v0, p0

    iput v3, v0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mInitializationRetriesLeft:I

    if-lez v2, :cond_1

    .line 377
    const-string v2, "MusicCastRemote"

    const-string v3, "Not yet ready for playback, will try to loadCloudQueue again later."

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 378
    invoke-direct/range {p0 .. p6}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->loadCloudQueueDelayed(Ljava/lang/String;Lcom/google/android/music/store/MusicFile;Ljava/lang/String;JZ)V

    goto :goto_0

    .line 381
    :cond_1
    const-string v2, "MusicCastRemote"

    const-string v3, "No more retries left, cannot loadCloudQueue."

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 387
    :cond_2
    invoke-direct/range {p0 .. p0}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->getFreshCastToken()Ljava/lang/String;

    move-result-object v10

    .line 388
    .local v10, "castToken":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mContext:Landroid/content/Context;

    const/4 v3, 0x1

    move-object/from16 v0, p2

    invoke-static {v2, v3, v0}, Lcom/google/android/music/cast/CastUtils;->generateMplayUrl(Landroid/content/Context;ZLcom/google/android/music/store/MusicFile;)Ljava/lang/String;

    move-result-object v13

    .line 390
    .local v13, "mplayUrl":Ljava/lang/String;
    new-instance v12, Lcom/google/android/gms/cast/MediaMetadata;

    const/4 v2, 0x3

    invoke-direct {v12, v2}, Lcom/google/android/gms/cast/MediaMetadata;-><init>(I)V

    .line 392
    .local v12, "mediaMetadata":Lcom/google/android/gms/cast/MediaMetadata;
    const-string v2, "com.google.android.gms.cast.metadata.TITLE"

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/music/store/MusicFile;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v12, v2, v3}, Lcom/google/android/gms/cast/MediaMetadata;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 393
    const-string v2, "com.google.android.gms.cast.metadata.ALBUM_ARTIST"

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/music/store/MusicFile;->getAlbumArtist()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v12, v2, v3}, Lcom/google/android/gms/cast/MediaMetadata;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 394
    const-string v2, "com.google.android.gms.cast.metadata.ARTIST"

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/music/store/MusicFile;->getTrackArtist()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v12, v2, v3}, Lcom/google/android/gms/cast/MediaMetadata;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 395
    const-string v2, "com.google.android.gms.cast.metadata.ALBUM_TITLE"

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/music/store/MusicFile;->getAlbumName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v12, v2, v3}, Lcom/google/android/gms/cast/MediaMetadata;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 396
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/music/store/MusicFile;->getAlbumArtLocation()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/music/utils/AlbumArtUtils;->stripDimensionFromImageUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 398
    .local v9, "albumArtLocation":Ljava/lang/String;
    new-instance v2, Lcom/google/android/gms/common/images/WebImage;

    new-instance v3, Landroid/net/Uri$Builder;

    invoke-direct {v3}, Landroid/net/Uri$Builder;-><init>()V

    invoke-virtual {v3, v9}, Landroid/net/Uri$Builder;->encodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/android/gms/common/images/WebImage;-><init>(Landroid/net/Uri;)V

    invoke-virtual {v12, v2}, Lcom/google/android/gms/cast/MediaMetadata;->addImage(Lcom/google/android/gms/common/images/WebImage;)V

    .line 400
    new-instance v2, Lcom/google/android/gms/cast/MediaInfo$Builder;

    invoke-direct {v2, v13}, Lcom/google/android/gms/cast/MediaInfo$Builder;-><init>(Ljava/lang/String;)V

    const-string v3, "audio/mpeg"

    invoke-virtual {v2, v3}, Lcom/google/android/gms/cast/MediaInfo$Builder;->setContentType(Ljava/lang/String;)Lcom/google/android/gms/cast/MediaInfo$Builder;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/google/android/gms/cast/MediaInfo$Builder;->setStreamType(I)Lcom/google/android/gms/cast/MediaInfo$Builder;

    move-result-object v2

    invoke-virtual {v2, v12}, Lcom/google/android/gms/cast/MediaInfo$Builder;->setMetadata(Lcom/google/android/gms/cast/MediaMetadata;)Lcom/google/android/gms/cast/MediaInfo$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/cast/MediaInfo$Builder;->build()Lcom/google/android/gms/cast/MediaInfo;

    move-result-object v4

    .line 408
    .local v4, "mediaInfo":Lcom/google/android/gms/cast/MediaInfo;
    new-instance v8, Lorg/json/JSONObject;

    invoke-direct {v8}, Lorg/json/JSONObject;-><init>()V

    .line 410
    .local v8, "json":Lorg/json/JSONObject;
    :try_start_0
    const-string v2, "httpHeaders"

    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    const-string v5, "Authorization"

    const-string v6, "playon=%s"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object v10, v7, v14

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v3

    invoke-virtual {v8, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 412
    const-string v2, "itemId"

    move-object/from16 v0, p3

    invoke-virtual {v8, v2, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 413
    const-string v2, "queueBaseUrl"

    move-object/from16 v0, p1

    invoke-virtual {v8, v2, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 414
    const-string v2, "contentType"

    const-string v3, "application/x-cloud-queue"

    invoke-virtual {v8, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 416
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->LOGV:Z

    if-eqz v2, :cond_3

    .line 417
    const-string v2, "MusicCastRemote"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Custom data: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v8}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 424
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mRemoteMediaPlayer:Lcom/google/android/gms/cast/RemoteMediaPlayer;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    move/from16 v5, p6

    move-wide/from16 v6, p4

    invoke-virtual/range {v2 .. v8}, Lcom/google/android/gms/cast/RemoteMediaPlayer;->load(Lcom/google/android/gms/common/api/GoogleApiClient;Lcom/google/android/gms/cast/MediaInfo;ZJLorg/json/JSONObject;)Lcom/google/android/gms/common/api/PendingResult;

    move-result-object v2

    new-instance v3, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$2;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$2;-><init>(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)V

    invoke-interface {v2, v3}, Lcom/google/android/gms/common/api/PendingResult;->setResultCallback(Lcom/google/android/gms/common/api/ResultCallback;)V

    goto/16 :goto_0

    .line 419
    :catch_0
    move-exception v11

    .line 420
    .local v11, "e":Lorg/json/JSONException;
    const-string v2, "MusicCastRemote"

    const-string v3, "Could not set playon token"

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private handlePause()V
    .locals 2

    .prologue
    .line 467
    iget-boolean v0, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mIsQueueLoaded:Z

    if-eqz v0, :cond_0

    .line 468
    iget-object v0, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mRemoteMediaPlayer:Lcom/google/android/gms/cast/RemoteMediaPlayer;

    iget-object v1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/cast/RemoteMediaPlayer;->pause(Lcom/google/android/gms/common/api/GoogleApiClient;)Lcom/google/android/gms/common/api/PendingResult;

    move-result-object v0

    new-instance v1, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$4;

    invoke-direct {v1, p0}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$4;-><init>(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/PendingResult;->setResultCallback(Lcom/google/android/gms/common/api/ResultCallback;)V

    .line 479
    :cond_0
    return-void
.end method

.method private handlePlay()V
    .locals 2

    .prologue
    .line 453
    iget-boolean v0, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mIsQueueLoaded:Z

    if-eqz v0, :cond_0

    .line 454
    iget-object v0, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mRemoteMediaPlayer:Lcom/google/android/gms/cast/RemoteMediaPlayer;

    iget-object v1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/cast/RemoteMediaPlayer;->play(Lcom/google/android/gms/common/api/GoogleApiClient;)Lcom/google/android/gms/common/api/PendingResult;

    move-result-object v0

    new-instance v1, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$3;

    invoke-direct {v1, p0}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$3;-><init>(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/PendingResult;->setResultCallback(Lcom/google/android/gms/common/api/ResultCallback;)V

    .line 464
    :cond_0
    return-void
.end method

.method private handleRefreshCloudQueue()V
    .locals 6

    .prologue
    .line 586
    iget-boolean v2, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mIsQueueLoaded:Z

    if-eqz v2, :cond_1

    .line 587
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 589
    .local v1, "json":Lorg/json/JSONObject;
    :try_start_0
    const-string v2, "command"

    const-string v3, "refreshCloudQueue"

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 595
    iget-boolean v2, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->LOGV:Z

    if-eqz v2, :cond_0

    .line 596
    const-string v2, "MusicCastRemote"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Issuing refresh command: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 598
    :cond_0
    sget-object v2, Lcom/google/android/gms/cast/Cast;->CastApi:Lcom/google/android/gms/cast/Cast$CastApi;

    iget-object v3, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    iget-object v4, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mCustomMessageCallback:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$CustomMessageCallback;

    invoke-virtual {v4}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$CustomMessageCallback;->getNamespace()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v3, v4, v5}, Lcom/google/android/gms/cast/Cast$CastApi;->sendMessage(Lcom/google/android/gms/common/api/GoogleApiClient;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/api/PendingResult;

    .line 602
    .end local v1    # "json":Lorg/json/JSONObject;
    :cond_1
    :goto_0
    return-void

    .line 591
    .restart local v1    # "json":Lorg/json/JSONObject;
    :catch_0
    move-exception v0

    .line 592
    .local v0, "e":Lorg/json/JSONException;
    const-string v2, "MusicCastRemote"

    const-string v3, "Error creating refreshCloudQueue JSON"

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private handleSeek(Ljava/lang/String;Ljava/lang/String;J)V
    .locals 5
    .param p1, "version"    # Ljava/lang/String;
    .param p2, "itemId"    # Ljava/lang/String;
    .param p3, "positionMillis"    # J

    .prologue
    .line 483
    iget-boolean v2, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mIsQueueLoaded:Z

    if-eqz v2, :cond_1

    .line 484
    invoke-direct {p0, p3, p4}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->updatePosition(J)V

    .line 486
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 488
    .local v1, "json":Lorg/json/JSONObject;
    :try_start_0
    const-string v2, "queueVersion"

    invoke-virtual {v1, v2, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 489
    const-string v2, "itemId"

    invoke-virtual {v1, v2, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 490
    iget-boolean v2, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->LOGV:Z

    if-eqz v2, :cond_0

    .line 491
    const-string v2, "MusicCastRemote"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Custom data: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 498
    :cond_0
    iget-object v2, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mRemoteMediaPlayer:Lcom/google/android/gms/cast/RemoteMediaPlayer;

    iget-object v3, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-virtual {v2, v3, p3, p4}, Lcom/google/android/gms/cast/RemoteMediaPlayer;->seek(Lcom/google/android/gms/common/api/GoogleApiClient;J)Lcom/google/android/gms/common/api/PendingResult;

    move-result-object v2

    new-instance v3, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$5;

    invoke-direct {v3, p0}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$5;-><init>(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)V

    invoke-interface {v2, v3}, Lcom/google/android/gms/common/api/PendingResult;->setResultCallback(Lcom/google/android/gms/common/api/ResultCallback;)V

    .line 508
    .end local v1    # "json":Lorg/json/JSONObject;
    :cond_1
    :goto_0
    return-void

    .line 493
    .restart local v1    # "json":Lorg/json/JSONObject;
    :catch_0
    move-exception v0

    .line 494
    .local v0, "e":Lorg/json/JSONException;
    const-string v2, "MusicCastRemote"

    const-string v3, "Error creating seek custom JSON data"

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private handleSetVolume(D)V
    .locals 3
    .param p1, "volume"    # D

    .prologue
    .line 570
    invoke-direct {p0}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->readyForLoad()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 571
    iput-wide p1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mVolume:D

    .line 572
    iget-object v0, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mRemoteMediaPlayer:Lcom/google/android/gms/cast/RemoteMediaPlayer;

    iget-object v1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-virtual {v0, v1, p1, p2}, Lcom/google/android/gms/cast/RemoteMediaPlayer;->setStreamVolume(Lcom/google/android/gms/common/api/GoogleApiClient;D)Lcom/google/android/gms/common/api/PendingResult;

    move-result-object v0

    new-instance v1, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$7;

    invoke-direct {v1, p0}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$7;-><init>(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/PendingResult;->setResultCallback(Lcom/google/android/gms/common/api/ResultCallback;)V

    .line 583
    :cond_0
    return-void
.end method

.method private handleSkipToItem(Ljava/lang/String;Lcom/google/android/music/store/MusicFile;Ljava/lang/String;J)V
    .locals 8
    .param p1, "version"    # Ljava/lang/String;
    .param p2, "musicFile"    # Lcom/google/android/music/store/MusicFile;
    .param p3, "itemId"    # Ljava/lang/String;
    .param p4, "positionMillis"    # J

    .prologue
    .line 512
    iget-boolean v5, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mIsQueueLoaded:Z

    if-eqz v5, :cond_1

    .line 513
    invoke-direct {p0, p4, p5}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->updatePosition(J)V

    .line 515
    iget-object v5, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mContext:Landroid/content/Context;

    const/4 v6, 0x1

    invoke-static {v5, v6, p2}, Lcom/google/android/music/cast/CastUtils;->generateMplayUrl(Landroid/content/Context;ZLcom/google/android/music/store/MusicFile;)Ljava/lang/String;

    move-result-object v3

    .line 516
    .local v3, "mplayUrl":Ljava/lang/String;
    invoke-virtual {p2}, Lcom/google/android/music/store/MusicFile;->getAlbumArtLocation()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/music/utils/AlbumArtUtils;->stripDimensionFromImageUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 519
    .local v0, "albumArtLocation":Ljava/lang/String;
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 521
    .local v2, "json":Lorg/json/JSONObject;
    :try_start_0
    const-string v5, "queueVersion"

    invoke-virtual {v2, v5, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 522
    const-string v5, "itemId"

    invoke-virtual {v2, v5, p3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 524
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4}, Lorg/json/JSONObject;-><init>()V

    .line 525
    .local v4, "trackMetadata":Lorg/json/JSONObject;
    const-string v5, "trackUrl"

    invoke-virtual {v4, v5, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 526
    const-string v5, "contentType"

    const-string v6, "audio/mpeg"

    invoke-virtual {v4, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 528
    const-string v5, "durationMillis"

    invoke-virtual {p2}, Lcom/google/android/music/store/MusicFile;->getDurationInMilliSec()J

    move-result-wide v6

    invoke-virtual {v4, v5, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 530
    const-string v5, "trackTitle"

    invoke-virtual {p2}, Lcom/google/android/music/store/MusicFile;->getTitle()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 531
    const-string v5, "albumArtist"

    invoke-virtual {p2}, Lcom/google/android/music/store/MusicFile;->getAlbumArtist()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 532
    const-string v5, "trackArtist"

    invoke-virtual {p2}, Lcom/google/android/music/store/MusicFile;->getTrackArtist()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 533
    const-string v5, "albumTitle"

    invoke-virtual {p2}, Lcom/google/android/music/store/MusicFile;->getAlbumName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 534
    const-string v5, "albumArtUrl"

    new-instance v6, Landroid/net/Uri$Builder;

    invoke-direct {v6}, Landroid/net/Uri$Builder;-><init>()V

    invoke-virtual {v6, v0}, Landroid/net/Uri$Builder;->encodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 537
    const-string v5, "trackMetadata"

    invoke-virtual {v2, v5, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 538
    iget-boolean v5, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->LOGV:Z

    if-eqz v5, :cond_0

    .line 539
    const-string v5, "MusicCastRemote"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "skipToItem custom data: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 546
    :cond_0
    iget-object v5, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mRemoteMediaPlayer:Lcom/google/android/gms/cast/RemoteMediaPlayer;

    iget-object v6, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-virtual {v5, v6, v2}, Lcom/google/android/gms/cast/RemoteMediaPlayer;->play(Lcom/google/android/gms/common/api/GoogleApiClient;Lorg/json/JSONObject;)Lcom/google/android/gms/common/api/PendingResult;

    move-result-object v5

    new-instance v6, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$6;

    invoke-direct {v6, p0}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$6;-><init>(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)V

    invoke-interface {v5, v6}, Lcom/google/android/gms/common/api/PendingResult;->setResultCallback(Lcom/google/android/gms/common/api/ResultCallback;)V

    .line 567
    .end local v0    # "albumArtLocation":Ljava/lang/String;
    .end local v2    # "json":Lorg/json/JSONObject;
    .end local v3    # "mplayUrl":Ljava/lang/String;
    .end local v4    # "trackMetadata":Lorg/json/JSONObject;
    :cond_1
    :goto_0
    return-void

    .line 541
    .restart local v0    # "albumArtLocation":Ljava/lang/String;
    .restart local v2    # "json":Lorg/json/JSONObject;
    .restart local v3    # "mplayUrl":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 542
    .local v1, "e":Lorg/json/JSONException;
    const-string v5, "MusicCastRemote"

    const-string v6, "Error creating skipToItem custom JSON data"

    invoke-static {v5, v6}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private loadCloudQueueDelayed(Ljava/lang/String;Lcom/google/android/music/store/MusicFile;Ljava/lang/String;JZ)V
    .locals 10
    .param p1, "queueBaseUrl"    # Ljava/lang/String;
    .param p2, "musicFile"    # Lcom/google/android/music/store/MusicFile;
    .param p3, "itemId"    # Ljava/lang/String;
    .param p4, "positionMillis"    # J
    .param p6, "playOnCompletion"    # Z

    .prologue
    .line 152
    const/4 v1, 0x3

    invoke-virtual {p0, v1}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 153
    .local v0, "msg":Landroid/os/Message;
    new-instance v1, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$LoadCloudQueueRequest;

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-wide v6, p4

    move/from16 v8, p6

    invoke-direct/range {v1 .. v8}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$LoadCloudQueueRequest;-><init>(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;Ljava/lang/String;Lcom/google/android/music/store/MusicFile;Ljava/lang/String;JZ)V

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 155
    const-wide/16 v2, 0x1f4

    invoke-virtual {p0, v0, v2, v3}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 156
    return-void
.end method

.method private readyForLoad()Z
    .locals 1

    .prologue
    .line 1258
    iget-object v0, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/GoogleApiClient;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mRemoteMediaPlayer:Lcom/google/android/gms/cast/RemoteMediaPlayer;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private tearDown()V
    .locals 5

    .prologue
    .line 1215
    iget-boolean v1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->LOGV:Z

    if-eqz v1, :cond_0

    .line 1216
    const-string v1, "MusicCastRemote"

    const-string v2, "tearing down"

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 1219
    :cond_0
    iget-object v2, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mLock:Ljava/lang/Object;

    monitor-enter v2

    .line 1220
    :try_start_0
    iget-object v1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_4

    .line 1222
    :try_start_1
    iget-object v1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mRemoteMediaPlayer:Lcom/google/android/gms/cast/RemoteMediaPlayer;

    if-eqz v1, :cond_1

    .line 1223
    sget-object v1, Lcom/google/android/gms/cast/Cast;->CastApi:Lcom/google/android/gms/cast/Cast$CastApi;

    iget-object v3, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    iget-object v4, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mRemoteMediaPlayer:Lcom/google/android/gms/cast/RemoteMediaPlayer;

    invoke-virtual {v4}, Lcom/google/android/gms/cast/RemoteMediaPlayer;->getNamespace()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v3, v4}, Lcom/google/android/gms/cast/Cast$CastApi;->removeMessageReceivedCallbacks(Lcom/google/android/gms/common/api/GoogleApiClient;Ljava/lang/String;)V

    .line 1228
    :cond_1
    iget-object v1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mCustomMessageCallback:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$CustomMessageCallback;

    if-eqz v1, :cond_2

    .line 1229
    sget-object v1, Lcom/google/android/gms/cast/Cast;->CastApi:Lcom/google/android/gms/cast/Cast$CastApi;

    iget-object v3, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    iget-object v4, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mCustomMessageCallback:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$CustomMessageCallback;

    invoke-virtual {v4}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$CustomMessageCallback;->getNamespace()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v3, v4}, Lcom/google/android/gms/cast/Cast$CastApi;->removeMessageReceivedCallbacks(Lcom/google/android/gms/common/api/GoogleApiClient;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1237
    :cond_2
    :goto_0
    :try_start_2
    iget-boolean v1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->LOGV:Z

    if-eqz v1, :cond_3

    .line 1238
    const-string v1, "MusicCastRemote"

    const-string v3, "Unregistering connection callbacks"

    invoke-static {v1, v3}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 1240
    :cond_3
    iget-object v1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    iget-object v3, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mConnectionCallbacks:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionCallbacks;

    invoke-interface {v1, v3}, Lcom/google/android/gms/common/api/GoogleApiClient;->unregisterConnectionCallbacks(Lcom/google/android/gms/common/api/GoogleApiClient$ConnectionCallbacks;)V

    .line 1241
    iget-object v1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    iget-object v3, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mConnectionFailedListener:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionFailedListener;

    invoke-interface {v1, v3}, Lcom/google/android/gms/common/api/GoogleApiClient;->unregisterConnectionFailedListener(Lcom/google/android/gms/common/api/GoogleApiClient$OnConnectionFailedListener;)V

    .line 1243
    iget-object v1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-interface {v1}, Lcom/google/android/gms/common/api/GoogleApiClient;->disconnect()V

    .line 1246
    :cond_4
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mRemoteMediaPlayer:Lcom/google/android/gms/cast/RemoteMediaPlayer;

    .line 1247
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    .line 1248
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mIsQueueLoaded:Z

    .line 1249
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mIsSessionInitialized:Z

    .line 1250
    const/16 v1, 0x14

    iput v1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mInitializationRetriesLeft:I

    .line 1251
    monitor-exit v2

    .line 1252
    return-void

    .line 1233
    :catch_0
    move-exception v0

    .line 1234
    .local v0, "e":Ljava/io/IOException;
    const-string v1, "MusicCastRemote"

    const-string v3, "Could not tear down message received callbacks"

    invoke-static {v1, v3, v0}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1251
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

.method private updateCurrentItemIdFromCustomData(Lorg/json/JSONObject;)V
    .locals 4
    .param p1, "customData"    # Lorg/json/JSONObject;

    .prologue
    .line 950
    if-eqz p1, :cond_1

    .line 952
    :try_start_0
    const-string v2, "itemId"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 953
    .local v1, "remoteItemId":Ljava/lang/String;
    iget-object v2, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mItemId:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mItemId:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 954
    :cond_0
    iput-object v1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mItemId:Ljava/lang/String;

    .line 955
    iget-object v2, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mServiceHooks:Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;

    invoke-interface {v2, v1}, Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;->onCloudQueueTrackChanged(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 961
    .end local v1    # "remoteItemId":Ljava/lang/String;
    :cond_1
    :goto_0
    return-void

    .line 957
    :catch_0
    move-exception v0

    .line 958
    .local v0, "e":Lorg/json/JSONException;
    const-string v2, "MusicCastRemote"

    const-string v3, "Could not parse JSON custom data from the receiver"

    invoke-static {v2, v3, v0}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private updatePosition(J)V
    .locals 3
    .param p1, "millis"    # J

    .prologue
    .line 1204
    iget-object v1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 1205
    :try_start_0
    iput-wide p1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mKnownPositionMillis:J

    .line 1206
    iget-object v0, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mPositionDeltaStopWatch:Lcom/google/android/music/playback/StopWatch;

    invoke-virtual {v0}, Lcom/google/android/music/playback/StopWatch;->reset()V

    .line 1207
    monitor-exit v1

    .line 1208
    return-void

    .line 1207
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public getPosition()J
    .locals 6

    .prologue
    .line 204
    iget-wide v2, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mKnownPositionMillis:J

    iget-object v4, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mPositionDeltaStopWatch:Lcom/google/android/music/playback/StopWatch;

    invoke-virtual {v4}, Lcom/google/android/music/playback/StopWatch;->getTime()J

    move-result-wide v4

    add-long v0, v2, v4

    .line 205
    .local v0, "position":J
    iget-object v2, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mMusicFile:Lcom/google/android/music/store/MusicFile;

    if-eqz v2, :cond_0

    .line 206
    iget-object v2, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mMusicFile:Lcom/google/android/music/store/MusicFile;

    invoke-virtual {v2}, Lcom/google/android/music/store/MusicFile;->getDurationInMilliSec()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    .line 208
    .end local v0    # "position":J
    :cond_0
    return-wide v0
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 12
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 254
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 316
    const-string v0, "MusicCastRemote"

    const-string v1, "Unknown message"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    .line 318
    :goto_0
    return-void

    .line 256
    :pswitch_0
    iget-object v7, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v7, Ljava/lang/String;

    .line 257
    .local v7, "appContext":Ljava/lang/String;
    if-nez v7, :cond_0

    .line 258
    const-string v0, "MusicCastRemote"

    const-string v1, "Missing request data for joinSession request content"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 261
    :cond_0
    invoke-direct {p0, v7}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->handleJoinSession(Ljava/lang/String;)V

    goto :goto_0

    .line 264
    .end local v7    # "appContext":Ljava/lang/String;
    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->handleLeaveSession()V

    goto :goto_0

    .line 267
    :pswitch_2
    iget-object v8, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v8, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$LoadCloudQueueRequest;

    .line 268
    .local v8, "loadCloudQueueRequest":Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$LoadCloudQueueRequest;
    if-nez v8, :cond_1

    .line 269
    const-string v0, "MusicCastRemote"

    const-string v1, "Missing request data for loadCloudQueue request content"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 272
    :cond_1
    invoke-virtual {v8}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$LoadCloudQueueRequest;->getQueueBaseUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$LoadCloudQueueRequest;->getMusicFile()Lcom/google/android/music/store/MusicFile;

    move-result-object v2

    invoke-virtual {v8}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$LoadCloudQueueRequest;->getItemId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v8}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$LoadCloudQueueRequest;->getPositionMillis()J

    move-result-wide v4

    invoke-virtual {v8}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$LoadCloudQueueRequest;->isPlayOnCompletion()Z

    move-result v6

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->handleLoadCloudQueue(Ljava/lang/String;Lcom/google/android/music/store/MusicFile;Ljava/lang/String;JZ)V

    goto :goto_0

    .line 280
    .end local v8    # "loadCloudQueueRequest":Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$LoadCloudQueueRequest;
    :pswitch_3
    invoke-direct {p0}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->handlePlay()V

    goto :goto_0

    .line 283
    :pswitch_4
    invoke-direct {p0}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->handlePause()V

    goto :goto_0

    .line 286
    :pswitch_5
    iget-object v9, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v9, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$SeekRequest;

    .line 287
    .local v9, "seekRequest":Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$SeekRequest;
    if-nez v9, :cond_2

    .line 288
    const-string v0, "MusicCastRemote"

    const-string v1, "Missing request data for seek request content"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 291
    :cond_2
    invoke-virtual {v9}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$SeekRequest;->getQueueVersion()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$SeekRequest;->getItemId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$SeekRequest;->getPositionMillis()J

    move-result-wide v2

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->handleSeek(Ljava/lang/String;Ljava/lang/String;J)V

    goto :goto_0

    .line 295
    .end local v9    # "seekRequest":Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$SeekRequest;
    :pswitch_6
    iget-object v11, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v11, Ljava/lang/Double;

    .line 296
    .local v11, "volume":Ljava/lang/Double;
    if-nez v11, :cond_3

    .line 297
    const-string v0, "MusicCastRemote"

    const-string v1, "Missing request data for setVolume request content"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 300
    :cond_3
    invoke-virtual {v11}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->handleSetVolume(D)V

    goto :goto_0

    .line 303
    .end local v11    # "volume":Ljava/lang/Double;
    :pswitch_7
    invoke-direct {p0}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->handleRefreshCloudQueue()V

    goto :goto_0

    .line 306
    :pswitch_8
    iget-object v10, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v10, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$SkipToItemRequest;

    .line 307
    .local v10, "skipToItemRequest":Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$SkipToItemRequest;
    if-nez v10, :cond_4

    .line 308
    const-string v0, "MusicCastRemote"

    const-string v1, "Missing request data for skipToItem request content"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 311
    :cond_4
    invoke-virtual {v10}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$SkipToItemRequest;->getQueueVersion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v10}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$SkipToItemRequest;->getMusicFile()Lcom/google/android/music/store/MusicFile;

    move-result-object v2

    invoke-virtual {v10}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$SkipToItemRequest;->getItemId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$SkipToItemRequest;->getPositionMillis()J

    move-result-wide v4

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->handleSkipToItem(Ljava/lang/String;Lcom/google/android/music/store/MusicFile;Ljava/lang/String;J)V

    goto/16 :goto_0

    .line 254
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public isPreparing()Z
    .locals 1

    .prologue
    .line 249
    iget-boolean v0, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mIsPreparing:Z

    return v0
.end method

.method public isQueueLoaded()Z
    .locals 1

    .prologue
    .line 234
    iget-boolean v0, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mIsQueueLoaded:Z

    return v0
.end method

.method public isSessionInitialized()Z
    .locals 1

    .prologue
    .line 239
    iget-boolean v0, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mIsSessionInitialized:Z

    return v0
.end method

.method public isStreaming()Z
    .locals 1

    .prologue
    .line 244
    iget-boolean v0, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mIsStreaming:Z

    return v0
.end method

.method public joinSession(Ljava/lang/String;)V
    .locals 2
    .param p1, "appContext"    # Ljava/lang/String;

    .prologue
    .line 130
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 131
    .local v0, "msg":Landroid/os/Message;
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 132
    invoke-virtual {p0, v0}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->sendMessage(Landroid/os/Message;)Z

    .line 133
    return-void
.end method

.method public leaveSession()V
    .locals 2

    .prologue
    .line 137
    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 138
    .local v0, "msg":Landroid/os/Message;
    invoke-virtual {p0, v0}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->sendMessage(Landroid/os/Message;)Z

    .line 139
    return-void
.end method

.method public loadCloudQueue(Ljava/lang/String;Lcom/google/android/music/store/MusicFile;Ljava/lang/String;JZ)V
    .locals 10
    .param p1, "queueBaseUrl"    # Ljava/lang/String;
    .param p2, "musicFile"    # Lcom/google/android/music/store/MusicFile;
    .param p3, "itemId"    # Ljava/lang/String;
    .param p4, "positionMillis"    # J
    .param p6, "playOnCompletion"    # Z

    .prologue
    .line 144
    const/4 v1, 0x3

    invoke-virtual {p0, v1}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 145
    .local v0, "msg":Landroid/os/Message;
    new-instance v1, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$LoadCloudQueueRequest;

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-wide v6, p4

    move/from16 v8, p6

    invoke-direct/range {v1 .. v8}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$LoadCloudQueueRequest;-><init>(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;Ljava/lang/String;Lcom/google/android/music/store/MusicFile;Ljava/lang/String;JZ)V

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 147
    invoke-virtual {p0, v0}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->sendMessage(Landroid/os/Message;)Z

    .line 148
    return-void
.end method

.method public pause()V
    .locals 2

    .prologue
    .line 166
    const/4 v1, 0x5

    invoke-virtual {p0, v1}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 167
    .local v0, "msg":Landroid/os/Message;
    invoke-virtual {p0, v0}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->sendMessage(Landroid/os/Message;)Z

    .line 168
    return-void
.end method

.method public play()V
    .locals 2

    .prologue
    .line 160
    const/4 v1, 0x4

    invoke-virtual {p0, v1}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 161
    .local v0, "msg":Landroid/os/Message;
    invoke-virtual {p0, v0}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->sendMessage(Landroid/os/Message;)Z

    .line 162
    return-void
.end method

.method public refreshCloudQueue()V
    .locals 8

    .prologue
    .line 218
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 219
    .local v2, "now":J
    iget-wide v4, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mLastRefreshTimestampMillis:J

    sub-long v4, v2, v4

    const-wide/16 v6, 0x3e8

    cmp-long v1, v4, v6

    if-lez v1, :cond_1

    .line 222
    iput-wide v2, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mLastRefreshTimestampMillis:J

    .line 223
    const/16 v1, 0x8

    invoke-virtual {p0, v1}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 224
    .local v0, "msg":Landroid/os/Message;
    invoke-virtual {p0, v0}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->sendMessage(Landroid/os/Message;)Z

    .line 230
    .end local v0    # "msg":Landroid/os/Message;
    :cond_0
    :goto_0
    return-void

    .line 226
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->LOGV:Z

    if-eqz v1, :cond_0

    .line 227
    const-string v1, "MusicCastRemote"

    const-string v4, "Rate-limiting refreshCloudQueue"

    invoke-static {v1, v4}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public seek(Ljava/lang/String;Ljava/lang/String;J)V
    .locals 7
    .param p1, "queueVersion"    # Ljava/lang/String;
    .param p2, "itemId"    # Ljava/lang/String;
    .param p3, "positionMillis"    # J

    .prologue
    .line 172
    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->obtainMessage(I)Landroid/os/Message;

    move-result-object v6

    .line 173
    .local v6, "msg":Landroid/os/Message;
    new-instance v0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$SeekRequest;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$SeekRequest;-><init>(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;Ljava/lang/String;Ljava/lang/String;J)V

    iput-object v0, v6, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 174
    invoke-virtual {p0, v6}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->sendMessage(Landroid/os/Message;)Z

    .line 175
    return-void
.end method

.method public setVolume(D)V
    .locals 3
    .param p1, "volume"    # D

    .prologue
    .line 187
    const/4 v1, 0x7

    invoke-virtual {p0, v1}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 188
    .local v0, "msg":Landroid/os/Message;
    new-instance v1, Ljava/lang/Double;

    invoke-direct {v1, p1, p2}, Ljava/lang/Double;-><init>(D)V

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 189
    invoke-virtual {p0, v0}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->sendMessage(Landroid/os/Message;)Z

    .line 190
    return-void
.end method

.method public skipToItem(Ljava/lang/String;Lcom/google/android/music/store/MusicFile;Ljava/lang/String;JZ)V
    .locals 8
    .param p1, "queueVersion"    # Ljava/lang/String;
    .param p2, "musicFile"    # Lcom/google/android/music/store/MusicFile;
    .param p3, "itemId"    # Ljava/lang/String;
    .param p4, "positionMillis"    # J
    .param p6, "playOnCompletion"    # Z

    .prologue
    .line 180
    const/16 v1, 0x9

    invoke-virtual {p0, v1}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 181
    .local v0, "msg":Landroid/os/Message;
    new-instance v1, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$SkipToItemRequest;

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-wide v6, p4

    invoke-direct/range {v1 .. v7}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$SkipToItemRequest;-><init>(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;Ljava/lang/String;Lcom/google/android/music/store/MusicFile;Ljava/lang/String;J)V

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 182
    invoke-virtual {p0, v0}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->sendMessage(Landroid/os/Message;)Z

    .line 183
    return-void
.end method
