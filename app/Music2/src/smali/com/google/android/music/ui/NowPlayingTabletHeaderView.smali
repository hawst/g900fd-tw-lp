.class public Lcom/google/android/music/ui/NowPlayingTabletHeaderView;
.super Landroid/widget/RelativeLayout;
.source "NowPlayingTabletHeaderView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private mAlbumId:J

.field private mAlbumName:Ljava/lang/String;

.field private mAlbumSmall:Lcom/google/android/music/AsyncAlbumArtImageView;

.field private mArtUrl:Ljava/lang/String;

.field private mArtistName:Ljava/lang/String;

.field private mArtistNameView:Landroid/widget/TextView;

.field private mBufferingProgress:Landroid/widget/ProgressBar;

.field private mClosedWidth:I

.field private mIsClosed:Z

.field private mMetadataView:Landroid/view/View;

.field private mOpenHeaderRightPadding:I

.field private mStatusListener:Landroid/content/BroadcastReceiver;

.field private mTitle:Ljava/lang/String;

.field private mTrackNameView:Lcom/google/android/music/widgets/MarqueeTextView;

.field private mVideoIcon:Landroid/widget/ImageView;

.field private mVideoId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 73
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 42
    const/4 v1, -0x2

    iput v1, p0, Lcom/google/android/music/ui/NowPlayingTabletHeaderView;->mClosedWidth:I

    .line 57
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/music/ui/NowPlayingTabletHeaderView;->mIsClosed:Z

    .line 59
    new-instance v1, Lcom/google/android/music/ui/NowPlayingTabletHeaderView$1;

    invoke-direct {v1, p0}, Lcom/google/android/music/ui/NowPlayingTabletHeaderView$1;-><init>(Lcom/google/android/music/ui/NowPlayingTabletHeaderView;)V

    iput-object v1, p0, Lcom/google/android/music/ui/NowPlayingTabletHeaderView;->mStatusListener:Landroid/content/BroadcastReceiver;

    .line 74
    const v1, 0x7f04008b

    invoke-static {p1, v1, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 76
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 77
    .local v0, "playbackStatusFilter":Landroid/content/IntentFilter;
    const-string v1, "com.android.music.playstatechanged"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 78
    const-string v1, "com.android.music.metachanged"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 79
    const-string v1, "com.android.music.asyncopencomplete"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 80
    const-string v1, "com.android.music.asyncopenstart"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 81
    const-string v1, "com.android.music.playbackfailed"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 82
    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingTabletHeaderView;->mStatusListener:Landroid/content/BroadcastReceiver;

    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 84
    invoke-virtual {p0}, Lcom/google/android/music/ui/NowPlayingTabletHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f00da

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/music/ui/NowPlayingTabletHeaderView;->mOpenHeaderRightPadding:I

    .line 87
    invoke-direct {p0}, Lcom/google/android/music/ui/NowPlayingTabletHeaderView;->initializeView()V

    .line 89
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/ui/NowPlayingTabletHeaderView;ZZZ)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/NowPlayingTabletHeaderView;
    .param p1, "x1"    # Z
    .param p2, "x2"    # Z
    .param p3, "x3"    # Z

    .prologue
    .line 30
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/music/ui/NowPlayingTabletHeaderView;->updateStreaming(ZZZ)V

    return-void
.end method

.method private initializeView()V
    .locals 1

    .prologue
    .line 92
    const v0, 0x7f0e01d0

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/NowPlayingTabletHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/AsyncAlbumArtImageView;

    iput-object v0, p0, Lcom/google/android/music/ui/NowPlayingTabletHeaderView;->mAlbumSmall:Lcom/google/android/music/AsyncAlbumArtImageView;

    .line 93
    const v0, 0x7f0e01c1

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/NowPlayingTabletHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/widgets/MarqueeTextView;

    iput-object v0, p0, Lcom/google/android/music/ui/NowPlayingTabletHeaderView;->mTrackNameView:Lcom/google/android/music/widgets/MarqueeTextView;

    .line 94
    const v0, 0x7f0e01d4

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/NowPlayingTabletHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/music/ui/NowPlayingTabletHeaderView;->mArtistNameView:Landroid/widget/TextView;

    .line 95
    const v0, 0x7f0e01d2

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/NowPlayingTabletHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/music/ui/NowPlayingTabletHeaderView;->mBufferingProgress:Landroid/widget/ProgressBar;

    .line 96
    const v0, 0x7f0e01d1

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/NowPlayingTabletHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/music/ui/NowPlayingTabletHeaderView;->mVideoIcon:Landroid/widget/ImageView;

    .line 97
    const v0, 0x7f0e01cf

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/NowPlayingTabletHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/NowPlayingTabletHeaderView;->mMetadataView:Landroid/view/View;

    .line 99
    invoke-virtual {p0, p0}, Lcom/google/android/music/ui/NowPlayingTabletHeaderView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 100
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingTabletHeaderView;->mMetadataView:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 101
    return-void
.end method

.method private updateStreaming(ZZZ)V
    .locals 3
    .param p1, "isInErrorState"    # Z
    .param p2, "isStreaming"    # Z
    .param p3, "isPreparing"    # Z

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 129
    if-eqz p1, :cond_0

    .line 130
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingTabletHeaderView;->mAlbumSmall:Lcom/google/android/music/AsyncAlbumArtImageView;

    invoke-virtual {v0, v1}, Lcom/google/android/music/AsyncAlbumArtImageView;->setAvailable(Z)V

    .line 131
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingTabletHeaderView;->mBufferingProgress:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 139
    :goto_0
    return-void

    .line 132
    :cond_0
    if-eqz p3, :cond_1

    if-eqz p2, :cond_1

    .line 133
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingTabletHeaderView;->mAlbumSmall:Lcom/google/android/music/AsyncAlbumArtImageView;

    invoke-virtual {v0, v1}, Lcom/google/android/music/AsyncAlbumArtImageView;->setAvailable(Z)V

    .line 134
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingTabletHeaderView;->mBufferingProgress:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0

    .line 136
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingTabletHeaderView;->mAlbumSmall:Lcom/google/android/music/AsyncAlbumArtImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/music/AsyncAlbumArtImageView;->setAvailable(Z)V

    .line 137
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingTabletHeaderView;->mBufferingProgress:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0
.end method

.method private updateWidth()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 171
    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingTabletHeaderView;->mMetadataView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 172
    .local v0, "params":Landroid/view/ViewGroup$LayoutParams;
    iget-boolean v1, p0, Lcom/google/android/music/ui/NowPlayingTabletHeaderView;->mIsClosed:Z

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/google/android/music/ui/NowPlayingTabletHeaderView;->mClosedWidth:I

    :goto_0
    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 173
    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingTabletHeaderView;->mMetadataView:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 179
    iget-object v3, p0, Lcom/google/android/music/ui/NowPlayingTabletHeaderView;->mMetadataView:Landroid/view/View;

    iget-boolean v1, p0, Lcom/google/android/music/ui/NowPlayingTabletHeaderView;->mIsClosed:Z

    if-eqz v1, :cond_1

    move v1, v2

    :goto_1
    invoke-virtual {v3, v2, v2, v1, v2}, Landroid/view/View;->setPadding(IIII)V

    .line 180
    return-void

    .line 172
    :cond_0
    const/4 v1, -0x2

    goto :goto_0

    .line 179
    :cond_1
    iget v1, p0, Lcom/google/android/music/ui/NowPlayingTabletHeaderView;->mOpenHeaderRightPadding:I

    goto :goto_1
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 143
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 144
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.google.android.music.nowplaying.HEADER_CLICKED"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 145
    invoke-virtual {p0}, Lcom/google/android/music/ui/NowPlayingTabletHeaderView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 146
    return-void
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 124
    invoke-virtual {p0}, Lcom/google/android/music/ui/NowPlayingTabletHeaderView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingTabletHeaderView;->mStatusListener:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 125
    return-void
.end method

.method public setIsClosed(Z)V
    .locals 3
    .param p1, "closed"    # Z

    .prologue
    const/4 v1, -0x1

    .line 160
    iput-boolean p1, p0, Lcom/google/android/music/ui/NowPlayingTabletHeaderView;->mIsClosed:Z

    .line 161
    iget-object v2, p0, Lcom/google/android/music/ui/NowPlayingTabletHeaderView;->mMetadataView:Landroid/view/View;

    if-eqz p1, :cond_0

    const v0, 0x7f0e01b5

    :goto_0
    invoke-virtual {v2, v0}, Landroid/view/View;->setNextFocusRightId(I)V

    .line 162
    iget-object v2, p0, Lcom/google/android/music/ui/NowPlayingTabletHeaderView;->mMetadataView:Landroid/view/View;

    if-eqz p1, :cond_1

    const v0, 0x7f0e025b

    :goto_1
    invoke-virtual {v2, v0}, Landroid/view/View;->setNextFocusUpId(I)V

    .line 163
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingTabletHeaderView;->mMetadataView:Landroid/view/View;

    if-eqz p1, :cond_2

    :goto_2
    invoke-virtual {v0, v1}, Landroid/view/View;->setNextFocusDownId(I)V

    .line 164
    invoke-direct {p0}, Lcom/google/android/music/ui/NowPlayingTabletHeaderView;->updateWidth()V

    .line 167
    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingTabletHeaderView;->mTrackNameView:Lcom/google/android/music/widgets/MarqueeTextView;

    if-nez p1, :cond_3

    const/4 v0, 0x1

    :goto_3
    invoke-virtual {v1, v0}, Lcom/google/android/music/widgets/MarqueeTextView;->doMarquee(Z)V

    .line 168
    return-void

    .line 161
    :cond_0
    const v0, 0x7f0e01dc

    goto :goto_0

    :cond_1
    move v0, v1

    .line 162
    goto :goto_1

    .line 163
    :cond_2
    const v1, 0x7f0e01b7

    goto :goto_2

    .line 167
    :cond_3
    const/4 v0, 0x0

    goto :goto_3
.end method

.method public setMetadataViewWidth(I)V
    .locals 2
    .param p1, "width"    # I

    .prologue
    const/4 v1, -0x1

    .line 149
    const/4 v0, -0x2

    if-eq p1, v0, :cond_0

    if-eq p1, v1, :cond_0

    if-ne p1, v1, :cond_1

    .line 152
    :cond_0
    iput p1, p0, Lcom/google/android/music/ui/NowPlayingTabletHeaderView;->mClosedWidth:I

    .line 156
    :goto_0
    invoke-direct {p0}, Lcom/google/android/music/ui/NowPlayingTabletHeaderView;->updateWidth()V

    .line 157
    return-void

    .line 154
    :cond_1
    int-to-float v0, p1

    const v1, 0x3e99999a    # 0.3f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/google/android/music/ui/NowPlayingTabletHeaderView;->mClosedWidth:I

    goto :goto_0
.end method

.method public updateView(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "artistName"    # Ljava/lang/String;
    .param p3, "albumName"    # Ljava/lang/String;
    .param p4, "albumId"    # Ljava/lang/Long;
    .param p5, "artUrl"    # Ljava/lang/String;
    .param p6, "videoId"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 105
    iput-object p1, p0, Lcom/google/android/music/ui/NowPlayingTabletHeaderView;->mTitle:Ljava/lang/String;

    .line 106
    iput-object p2, p0, Lcom/google/android/music/ui/NowPlayingTabletHeaderView;->mArtistName:Ljava/lang/String;

    .line 107
    iput-object p3, p0, Lcom/google/android/music/ui/NowPlayingTabletHeaderView;->mAlbumName:Ljava/lang/String;

    .line 108
    invoke-virtual {p4}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/music/ui/NowPlayingTabletHeaderView;->mAlbumId:J

    .line 109
    iput-object p5, p0, Lcom/google/android/music/ui/NowPlayingTabletHeaderView;->mArtUrl:Ljava/lang/String;

    .line 110
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingTabletHeaderView;->mArtUrl:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 111
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingTabletHeaderView;->mAlbumSmall:Lcom/google/android/music/AsyncAlbumArtImageView;

    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingTabletHeaderView;->mArtUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/music/AsyncAlbumArtImageView;->setExternalAlbumArt(Ljava/lang/String;)V

    .line 115
    :goto_0
    iput-object p6, p0, Lcom/google/android/music/ui/NowPlayingTabletHeaderView;->mVideoId:Ljava/lang/String;

    .line 116
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingTabletHeaderView;->mTrackNameView:Lcom/google/android/music/widgets/MarqueeTextView;

    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingTabletHeaderView;->mTitle:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/music/widgets/MarqueeTextView;->setText(Ljava/lang/CharSequence;)V

    .line 117
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingTabletHeaderView;->mArtistNameView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingTabletHeaderView;->mArtistName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 118
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingTabletHeaderView;->mVideoIcon:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 119
    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingTabletHeaderView;->mVideoIcon:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingTabletHeaderView;->mVideoId:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/16 v0, 0x8

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 121
    :cond_0
    return-void

    .line 113
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingTabletHeaderView;->mAlbumSmall:Lcom/google/android/music/AsyncAlbumArtImageView;

    iget-wide v2, p0, Lcom/google/android/music/ui/NowPlayingTabletHeaderView;->mAlbumId:J

    invoke-virtual {v0, v2, v3, v4, v4}, Lcom/google/android/music/AsyncAlbumArtImageView;->setAlbumId(JLjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 119
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method
