.class public Lcom/google/android/music/ssl/TlsOnlySSLSocket;
.super Lcom/google/android/music/ssl/DelegatingSSLSocket;
.source "TlsOnlySSLSocket.java"


# direct methods
.method public constructor <init>(Ljavax/net/ssl/SSLSocket;)V
    .locals 1
    .param p1, "delegate"    # Ljavax/net/ssl/SSLSocket;

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/google/android/music/ssl/DelegatingSSLSocket;-><init>(Ljavax/net/ssl/SSLSocket;)V

    .line 23
    invoke-virtual {p1}, Ljavax/net/ssl/SSLSocket;->getEnabledProtocols()[Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/music/ssl/TlsOnlySSLSocket;->getProtocolsToEnable([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljavax/net/ssl/SSLSocket;->setEnabledProtocols([Ljava/lang/String;)V

    .line 24
    return-void
.end method

.method private getProtocolsToEnable([Ljava/lang/String;)[Ljava/lang/String;
    .locals 2
    .param p1, "requestedProtocols"    # [Ljava/lang/String;

    .prologue
    .line 46
    if-eqz p1, :cond_0

    array-length v1, p1

    if-nez v1, :cond_2

    :cond_0
    move-object v0, p1

    .line 58
    :cond_1
    :goto_0
    return-object v0

    .line 50
    :cond_2
    invoke-static {p1}, Lcom/google/android/music/ssl/TlsOnlySSLSocket;->retainTlsProtocolsOnly([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 51
    .local v0, "result":[Ljava/lang/String;
    array-length v1, v0

    if-nez v1, :cond_1

    .line 55
    invoke-virtual {p0}, Lcom/google/android/music/ssl/TlsOnlySSLSocket;->getSupportedProtocols()[Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static retainTlsProtocolsOnly([Ljava/lang/String;)[Ljava/lang/String;
    .locals 6
    .param p0, "protocols"    # [Ljava/lang/String;

    .prologue
    .line 62
    new-instance v4, Ljava/util/ArrayList;

    array-length v5, p0

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 63
    .local v4, "result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    move-object v0, p0

    .local v0, "arr$":[Ljava/lang/String;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 64
    .local v3, "protocol":Ljava/lang/String;
    const-string v5, "TLS"

    invoke-virtual {v3, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 65
    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 63
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 68
    .end local v3    # "protocol":Ljava/lang/String;
    :cond_1
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    new-array v5, v5, [Ljava/lang/String;

    invoke-interface {v4, v5}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Ljava/lang/String;

    return-object v5
.end method


# virtual methods
.method public getSupportedProtocols()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    invoke-super {p0}, Lcom/google/android/music/ssl/DelegatingSSLSocket;->getSupportedProtocols()[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/music/ssl/TlsOnlySSLSocket;->retainTlsProtocolsOnly([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setEnabledProtocols([Ljava/lang/String;)V
    .locals 1
    .param p1, "protocols"    # [Ljava/lang/String;

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/google/android/music/ssl/TlsOnlySSLSocket;->getProtocolsToEnable([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/google/android/music/ssl/DelegatingSSLSocket;->setEnabledProtocols([Ljava/lang/String;)V

    .line 29
    return-void
.end method

.method public setSSLParameters(Ljavax/net/ssl/SSLParameters;)V
    .locals 1
    .param p1, "p"    # Ljavax/net/ssl/SSLParameters;

    .prologue
    .line 33
    invoke-super {p0, p1}, Lcom/google/android/music/ssl/DelegatingSSLSocket;->setSSLParameters(Ljavax/net/ssl/SSLParameters;)V

    .line 34
    invoke-virtual {p1}, Ljavax/net/ssl/SSLParameters;->getProtocols()[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/music/ssl/TlsOnlySSLSocket;->setEnabledProtocols([Ljava/lang/String;)V

    .line 35
    return-void
.end method
