.class Lcom/google/android/music/MusicApplication$2;
.super Ljava/lang/Object;
.source "MusicApplication.java"

# interfaces
.implements Ljava/lang/Thread$UncaughtExceptionHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/MusicApplication;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/MusicApplication;


# direct methods
.method constructor <init>(Lcom/google/android/music/MusicApplication;)V
    .locals 0

    .prologue
    .line 206
    iput-object p1, p0, Lcom/google/android/music/MusicApplication$2;->this$0:Lcom/google/android/music/MusicApplication;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V
    .locals 4
    .param p1, "thread"    # Ljava/lang/Thread;
    .param p2, "ex"    # Ljava/lang/Throwable;

    .prologue
    .line 210
    :try_start_0
    instance-of v2, p2, Ljava/lang/OutOfMemoryError;

    if-eqz v2, :cond_0

    .line 211
    move-object v0, p2

    check-cast v0, Ljava/lang/OutOfMemoryError;

    move-object v1, v0

    .line 212
    .local v1, "oom":Ljava/lang/OutOfMemoryError;
    invoke-static {v1}, Lcom/google/android/music/utils/AlbumArtUtils;->report(Ljava/lang/OutOfMemoryError;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 216
    .end local v1    # "oom":Ljava/lang/OutOfMemoryError;
    :cond_0
    iget-object v2, p0, Lcom/google/android/music/MusicApplication$2;->this$0:Lcom/google/android/music/MusicApplication;

    iget-object v2, v2, Lcom/google/android/music/MusicApplication;->mOldUncaughtExceptionHandler:Ljava/lang/Thread$UncaughtExceptionHandler;

    invoke-interface {v2, p1, p2}, Ljava/lang/Thread$UncaughtExceptionHandler;->uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V

    .line 218
    :goto_0
    return-void

    .line 214
    :catch_0
    move-exception v2

    .line 216
    iget-object v2, p0, Lcom/google/android/music/MusicApplication$2;->this$0:Lcom/google/android/music/MusicApplication;

    iget-object v2, v2, Lcom/google/android/music/MusicApplication;->mOldUncaughtExceptionHandler:Ljava/lang/Thread$UncaughtExceptionHandler;

    invoke-interface {v2, p1, p2}, Ljava/lang/Thread$UncaughtExceptionHandler;->uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V

    goto :goto_0

    :catchall_0
    move-exception v2

    iget-object v3, p0, Lcom/google/android/music/MusicApplication$2;->this$0:Lcom/google/android/music/MusicApplication;

    iget-object v3, v3, Lcom/google/android/music/MusicApplication;->mOldUncaughtExceptionHandler:Ljava/lang/Thread$UncaughtExceptionHandler;

    invoke-interface {v3, p1, p2}, Ljava/lang/Thread$UncaughtExceptionHandler;->uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V

    throw v2
.end method
