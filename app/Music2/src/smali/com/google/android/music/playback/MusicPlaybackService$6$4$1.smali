.class Lcom/google/android/music/playback/MusicPlaybackService$6$4$1;
.super Ljava/lang/Object;
.source "MusicPlaybackService.java"

# interfaces
.implements Lcom/google/android/music/utils/VoiceActionHelper$SearchCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/playback/MusicPlaybackService$6$4;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$2:Lcom/google/android/music/playback/MusicPlaybackService$6$4;


# direct methods
.method constructor <init>(Lcom/google/android/music/playback/MusicPlaybackService$6$4;)V
    .locals 0

    .prologue
    .line 791
    iput-object p1, p0, Lcom/google/android/music/playback/MusicPlaybackService$6$4$1;->this$2:Lcom/google/android/music/playback/MusicPlaybackService$6$4;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSearchComplete(ILcom/google/android/music/medialist/SongList;Lcom/google/android/music/mix/MixDescriptor;)V
    .locals 4
    .param p1, "code"    # I
    .param p2, "songList"    # Lcom/google/android/music/medialist/SongList;
    .param p3, "mix"    # Lcom/google/android/music/mix/MixDescriptor;

    .prologue
    const/4 v3, 0x1

    .line 795
    # getter for: Lcom/google/android/music/playback/MusicPlaybackService;->LOGV:Z
    invoke-static {}, Lcom/google/android/music/playback/MusicPlaybackService;->access$100()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 796
    const-string v0, "MusicPlaybackService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onSearchComplete "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 798
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 821
    :goto_0
    return-void

    .line 801
    :pswitch_0
    if-eqz p2, :cond_3

    .line 802
    instance-of v0, p2, Lcom/google/android/music/medialist/NautilusArtistSongList;

    if-nez v0, :cond_1

    instance-of v0, p2, Lcom/google/android/music/medialist/ArtistSongList;

    if-eqz v0, :cond_2

    .line 804
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$6$4$1;->this$2:Lcom/google/android/music/playback/MusicPlaybackService$6$4;

    iget-object v0, v0, Lcom/google/android/music/playback/MusicPlaybackService$6$4;->this$1:Lcom/google/android/music/playback/MusicPlaybackService$6;

    iget-object v0, v0, Lcom/google/android/music/playback/MusicPlaybackService$6;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v0, p2}, Lcom/google/android/music/playback/MusicPlaybackService;->shuffleSongs(Lcom/google/android/music/medialist/SongList;)V

    goto :goto_0

    .line 806
    :cond_2
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$6$4$1;->this$2:Lcom/google/android/music/playback/MusicPlaybackService$6$4;

    iget-object v0, v0, Lcom/google/android/music/playback/MusicPlaybackService$6$4;->this$1:Lcom/google/android/music/playback/MusicPlaybackService$6;

    iget-object v0, v0, Lcom/google/android/music/playback/MusicPlaybackService$6;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    const/4 v1, 0x0

    invoke-virtual {v0, p2, v1, v3}, Lcom/google/android/music/playback/MusicPlaybackService;->open(Lcom/google/android/music/medialist/SongList;IZ)V

    goto :goto_0

    .line 808
    :cond_3
    if-eqz p3, :cond_4

    .line 809
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$6$4$1;->this$2:Lcom/google/android/music/playback/MusicPlaybackService$6$4;

    iget-object v0, v0, Lcom/google/android/music/playback/MusicPlaybackService$6$4;->this$1:Lcom/google/android/music/playback/MusicPlaybackService$6;

    iget-object v0, v0, Lcom/google/android/music/playback/MusicPlaybackService$6;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    invoke-virtual {v0, p3}, Lcom/google/android/music/playback/MusicPlaybackService;->openMix(Lcom/google/android/music/mix/MixDescriptor;)V

    goto :goto_0

    .line 811
    :cond_4
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$6$4$1;->this$2:Lcom/google/android/music/playback/MusicPlaybackService$6$4;

    iget-object v0, v0, Lcom/google/android/music/playback/MusicPlaybackService$6$4;->this$1:Lcom/google/android/music/playback/MusicPlaybackService$6;

    iget-object v0, v0, Lcom/google/android/music/playback/MusicPlaybackService$6;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    iget-object v1, p0, Lcom/google/android/music/playback/MusicPlaybackService$6$4$1;->this$2:Lcom/google/android/music/playback/MusicPlaybackService$6$4;

    iget-object v1, v1, Lcom/google/android/music/playback/MusicPlaybackService$6$4;->this$1:Lcom/google/android/music/playback/MusicPlaybackService$6;

    iget-object v1, v1, Lcom/google/android/music/playback/MusicPlaybackService$6;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    const v2, 0x7f0b00e6

    invoke-virtual {v1, v2}, Lcom/google/android/music/playback/MusicPlaybackService;->getString(I)Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/google/android/music/playback/MusicPlaybackService;->updateMediaSessionPlaystate(ZLjava/lang/String;)V
    invoke-static {v0, v3, v1}, Lcom/google/android/music/playback/MusicPlaybackService;->access$800(Lcom/google/android/music/playback/MusicPlaybackService;ZLjava/lang/String;)V

    goto :goto_0

    .line 816
    :pswitch_1
    const-string v0, "MusicPlaybackService"

    const-string v1, "Cannot play from search No results"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 817
    iget-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$6$4$1;->this$2:Lcom/google/android/music/playback/MusicPlaybackService$6$4;

    iget-object v0, v0, Lcom/google/android/music/playback/MusicPlaybackService$6$4;->this$1:Lcom/google/android/music/playback/MusicPlaybackService$6;

    iget-object v0, v0, Lcom/google/android/music/playback/MusicPlaybackService$6;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    iget-object v1, p0, Lcom/google/android/music/playback/MusicPlaybackService$6$4$1;->this$2:Lcom/google/android/music/playback/MusicPlaybackService$6$4;

    iget-object v1, v1, Lcom/google/android/music/playback/MusicPlaybackService$6$4;->this$1:Lcom/google/android/music/playback/MusicPlaybackService$6;

    iget-object v1, v1, Lcom/google/android/music/playback/MusicPlaybackService$6;->this$0:Lcom/google/android/music/playback/MusicPlaybackService;

    const v2, 0x7f0b0375

    invoke-virtual {v1, v2}, Lcom/google/android/music/playback/MusicPlaybackService;->getString(I)Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/google/android/music/playback/MusicPlaybackService;->updateMediaSessionPlaystate(ZLjava/lang/String;)V
    invoke-static {v0, v3, v1}, Lcom/google/android/music/playback/MusicPlaybackService;->access$800(Lcom/google/android/music/playback/MusicPlaybackService;ZLjava/lang/String;)V

    goto :goto_0

    .line 798
    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
