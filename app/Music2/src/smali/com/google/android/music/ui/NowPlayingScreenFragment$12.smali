.class Lcom/google/android/music/ui/NowPlayingScreenFragment$12;
.super Landroid/os/Handler;
.source "NowPlayingScreenFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/NowPlayingScreenFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/NowPlayingScreenFragment;)V
    .locals 0

    .prologue
    .line 1227
    iput-object p1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$12;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 1230
    iget v2, p1, Landroid/os/Message;->what:I

    sparse-switch v2, :sswitch_data_0

    .line 1248
    :cond_0
    :goto_0
    return-void

    .line 1232
    :sswitch_0
    iget-object v2, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$12;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # invokes: Lcom/google/android/music/ui/NowPlayingScreenFragment;->refreshNow()J
    invoke-static {v2}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$2100(Lcom/google/android/music/ui/NowPlayingScreenFragment;)J

    move-result-wide v0

    .line 1233
    .local v0, "next":J
    iget-object v2, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$12;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    invoke-virtual {v2}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->isResumed()Z

    move-result v2

    if-eqz v2, :cond_0

    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    .line 1234
    iget-object v2, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$12;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # invokes: Lcom/google/android/music/ui/NowPlayingScreenFragment;->queueNextRefresh(J)V
    invoke-static {v2, v0, v1}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$2200(Lcom/google/android/music/ui/NowPlayingScreenFragment;J)V

    goto :goto_0

    .line 1239
    .end local v0    # "next":J
    :sswitch_1
    iget-object v3, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$12;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Landroid/content/Intent;

    # invokes: Lcom/google/android/music/ui/NowPlayingScreenFragment;->updateTrackInfoImpl(Landroid/content/Intent;)V
    invoke-static {v3, v2}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$2400(Lcom/google/android/music/ui/NowPlayingScreenFragment;Landroid/content/Intent;)V

    goto :goto_0

    .line 1243
    :sswitch_2
    iget-object v2, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$12;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # invokes: Lcom/google/android/music/ui/NowPlayingScreenFragment;->turnLightsOff()V
    invoke-static {v2}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$2500(Lcom/google/android/music/ui/NowPlayingScreenFragment;)V

    goto :goto_0

    .line 1230
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x5 -> :sswitch_1
        0x7 -> :sswitch_2
    .end sparse-switch
.end method
