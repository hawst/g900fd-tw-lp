.class public final Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$MultipleTracksRemovedInfo;
.super Lcom/google/protobuf/nano/MessageNano;
.source "PlayMusicLogClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "MultipleTracksRemovedInfo"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$MultipleTracksRemovedInfo;


# instance fields
.field public container:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;

.field public numTracksRemoved:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1710
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$MultipleTracksRemovedInfo;

    sput-object v0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$MultipleTracksRemovedInfo;->EMPTY_ARRAY:[Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$MultipleTracksRemovedInfo;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1711
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 1716
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$MultipleTracksRemovedInfo;->container:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;

    .line 1711
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 1731
    const/4 v0, 0x0

    .line 1732
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$MultipleTracksRemovedInfo;->numTracksRemoved:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 1733
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$MultipleTracksRemovedInfo;->numTracksRemoved:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1736
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$MultipleTracksRemovedInfo;->container:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;

    if-eqz v1, :cond_1

    .line 1737
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$MultipleTracksRemovedInfo;->container:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1740
    :cond_1
    iput v0, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$MultipleTracksRemovedInfo;->cachedSize:I

    .line 1741
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$MultipleTracksRemovedInfo;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1749
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 1750
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 1754
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1755
    :sswitch_0
    return-object p0

    .line 1760
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$MultipleTracksRemovedInfo;->numTracksRemoved:Ljava/lang/Integer;

    goto :goto_0

    .line 1764
    :sswitch_2
    iget-object v1, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$MultipleTracksRemovedInfo;->container:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;

    if-nez v1, :cond_1

    .line 1765
    new-instance v1, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;

    invoke-direct {v1}, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;-><init>()V

    iput-object v1, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$MultipleTracksRemovedInfo;->container:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;

    .line 1767
    :cond_1
    iget-object v1, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$MultipleTracksRemovedInfo;->container:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1750
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1707
    invoke-virtual {p0, p1}, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$MultipleTracksRemovedInfo;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$MultipleTracksRemovedInfo;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1721
    iget-object v0, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$MultipleTracksRemovedInfo;->numTracksRemoved:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 1722
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$MultipleTracksRemovedInfo;->numTracksRemoved:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1724
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$MultipleTracksRemovedInfo;->container:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;

    if-eqz v0, :cond_1

    .line 1725
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$QueueEventInfo$MultipleTracksRemovedInfo;->container:Lcom/google/android/music/eventlogprotos/PlayMusicLogClient$PlaylogMusicClientExtension$Container;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1727
    :cond_1
    return-void
.end method
