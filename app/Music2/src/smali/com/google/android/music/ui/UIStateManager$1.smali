.class Lcom/google/android/music/ui/UIStateManager$1;
.super Ljava/lang/Object;
.source "UIStateManager.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/UIStateManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/UIStateManager;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/UIStateManager;)V
    .locals 0

    .prologue
    .line 118
    iput-object p1, p0, Lcom/google/android/music/ui/UIStateManager$1;->this$0:Lcom/google/android/music/ui/UIStateManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 4
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 128
    # getter for: Lcom/google/android/music/ui/UIStateManager;->LOGV:Z
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->access$000()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 129
    const-string v2, "UIStateManager"

    const-string v3, "Connected to playback service"

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    :cond_0
    iget-object v2, p0, Lcom/google/android/music/ui/UIStateManager$1;->this$0:Lcom/google/android/music/ui/UIStateManager;

    # getter for: Lcom/google/android/music/ui/UIStateManager;->mRunOnPlaybackServiceConnected:Ljava/util/List;
    invoke-static {v2}, Lcom/google/android/music/ui/UIStateManager;->access$100(Lcom/google/android/music/ui/UIStateManager;)Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 132
    iget-object v2, p0, Lcom/google/android/music/ui/UIStateManager$1;->this$0:Lcom/google/android/music/ui/UIStateManager;

    # getter for: Lcom/google/android/music/ui/UIStateManager;->mRunOnPlaybackServiceConnected:Ljava/util/List;
    invoke-static {v2}, Lcom/google/android/music/ui/UIStateManager;->access$100(Lcom/google/android/music/ui/UIStateManager;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Runnable;

    .line 133
    .local v1, "run":Ljava/lang/Runnable;
    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    .line 136
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "run":Ljava/lang/Runnable;
    :cond_1
    iget-object v2, p0, Lcom/google/android/music/ui/UIStateManager$1;->this$0:Lcom/google/android/music/ui/UIStateManager;

    const/4 v3, 0x0

    # setter for: Lcom/google/android/music/ui/UIStateManager;->mRunOnPlaybackServiceConnected:Ljava/util/List;
    invoke-static {v2, v3}, Lcom/google/android/music/ui/UIStateManager;->access$102(Lcom/google/android/music/ui/UIStateManager;Ljava/util/List;)Ljava/util/List;

    .line 137
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 122
    const-string v0, "UIStateManager"

    const-string v1, "Disconnected from playback service"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    return-void
.end method
