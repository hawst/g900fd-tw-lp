.class public Lcom/google/android/music/GoogleHttpClientFactory;
.super Ljava/lang/Object;
.source "GoogleHttpClientFactory.java"


# static fields
.field private static final sIsGMSCoreInstalled:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 26
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    sput-object v0, Lcom/google/android/music/GoogleHttpClientFactory;->sIsGMSCoreInstalled:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-void
.end method

.method public static configure(Z)V
    .locals 1
    .param p0, "isGMSCoreInstalled"    # Z

    .prologue
    .line 35
    sget-object v0, Lcom/google/android/music/GoogleHttpClientFactory;->sIsGMSCoreInstalled:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, p0}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 36
    return-void
.end method

.method public static createGoogleHttpClient(Landroid/content/Context;Ljava/lang/String;Z)Lcom/google/android/music/cloudclient/MusicHttpClient;
    .locals 12
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "userAgent"    # Ljava/lang/String;
    .param p2, "gzipCapable"    # Z

    .prologue
    const v11, 0xea60

    .line 40
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string v8, "music_http_client_configure_sni"

    const/4 v9, 0x1

    invoke-static {v7, v8, v9}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v1

    .line 44
    .local v1, "configureSni":Z
    sget-object v7, Lcom/google/android/music/GoogleHttpClientFactory;->sIsGMSCoreInstalled:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v7}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v7

    if-nez v7, :cond_1

    .line 46
    new-instance v0, Lcom/google/android/common/http/GoogleHttpClient;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/common/http/GoogleHttpClient;-><init>(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 50
    .local v0, "commonHttpClient":Lcom/google/android/common/http/GoogleHttpClient;
    const/4 v6, 0x0

    .line 52
    .local v6, "switchToGMSCore":Z
    if-eqz v1, :cond_0

    .line 54
    new-instance v2, Lcom/google/android/music/ssl/ConfigureSniSSLSocketFactory;

    invoke-static {}, Ljavax/net/ssl/SSLSocketFactory;->getDefault()Ljavax/net/SocketFactory;

    move-result-object v7

    check-cast v7, Ljavax/net/ssl/SSLSocketFactory;

    invoke-direct {v2, v7, v11}, Lcom/google/android/music/ssl/ConfigureSniSSLSocketFactory;-><init>(Ljavax/net/ssl/SSLSocketFactory;I)V

    .line 58
    .local v2, "configureSniSSLSocketFactory":Lcom/google/android/music/ssl/ConfigureSniSSLSocketFactory;
    new-instance v5, Lcom/google/android/music/ssl/ApacheSSLSocketFactoryAdaptor;

    invoke-static {}, Ljavax/net/ssl/HttpsURLConnection;->getDefaultHostnameVerifier()Ljavax/net/ssl/HostnameVerifier;

    move-result-object v7

    invoke-direct {v5, v2, v7}, Lcom/google/android/music/ssl/ApacheSSLSocketFactoryAdaptor;-><init>(Ljavax/net/ssl/SSLSocketFactory;Ljavax/net/ssl/HostnameVerifier;)V

    .line 65
    .local v5, "sniSslSocketFactoryForApacheHttpClient":Lcom/google/android/music/ssl/ApacheSSLSocketFactoryAdaptor;
    :try_start_0
    invoke-virtual {v0}, Lcom/google/android/common/http/GoogleHttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v7

    invoke-interface {v7}, Lorg/apache/http/conn/ClientConnectionManager;->getSchemeRegistry()Lorg/apache/http/conn/scheme/SchemeRegistry;

    move-result-object v7

    new-instance v8, Lorg/apache/http/conn/scheme/Scheme;

    const-string v9, "https"

    const/16 v10, 0x1bb

    invoke-direct {v8, v9, v5, v10}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v7, v8}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0

    .line 74
    .end local v2    # "configureSniSSLSocketFactory":Lcom/google/android/music/ssl/ConfigureSniSSLSocketFactory;
    .end local v5    # "sniSslSocketFactoryForApacheHttpClient":Lcom/google/android/music/ssl/ApacheSSLSocketFactoryAdaptor;
    :cond_0
    :goto_0
    if-nez v6, :cond_1

    .line 75
    const-string v7, "GHttpClientFactory"

    const-string v8, "Using the common GoogleHttpClient"

    invoke-static {v7, v8}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    new-instance v7, Lcom/google/android/music/cloudclient/MusicHttpClient;

    invoke-direct {v7, p0, v0}, Lcom/google/android/music/cloudclient/MusicHttpClient;-><init>(Landroid/content/Context;Lorg/apache/http/client/HttpClient;)V

    .line 102
    .end local v0    # "commonHttpClient":Lcom/google/android/common/http/GoogleHttpClient;
    .end local v6    # "switchToGMSCore":Z
    :goto_1
    return-object v7

    .line 67
    .restart local v0    # "commonHttpClient":Lcom/google/android/common/http/GoogleHttpClient;
    .restart local v2    # "configureSniSSLSocketFactory":Lcom/google/android/music/ssl/ConfigureSniSSLSocketFactory;
    .restart local v5    # "sniSslSocketFactoryForApacheHttpClient":Lcom/google/android/music/ssl/ApacheSSLSocketFactoryAdaptor;
    .restart local v6    # "switchToGMSCore":Z
    :catch_0
    move-exception v3

    .line 70
    .local v3, "e":Ljava/lang/UnsupportedOperationException;
    const/4 v6, 0x1

    goto :goto_0

    .line 81
    .end local v0    # "commonHttpClient":Lcom/google/android/common/http/GoogleHttpClient;
    .end local v2    # "configureSniSSLSocketFactory":Lcom/google/android/music/ssl/ConfigureSniSSLSocketFactory;
    .end local v3    # "e":Ljava/lang/UnsupportedOperationException;
    .end local v5    # "sniSslSocketFactoryForApacheHttpClient":Lcom/google/android/music/ssl/ApacheSSLSocketFactoryAdaptor;
    .end local v6    # "switchToGMSCore":Z
    :cond_1
    new-instance v4, Lcom/google/android/gms/http/GoogleHttpClient;

    invoke-direct {v4, p0, p1, p2}, Lcom/google/android/gms/http/GoogleHttpClient;-><init>(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 85
    .local v4, "gmsGoogleHttpClient":Lcom/google/android/gms/http/GoogleHttpClient;
    if-eqz v1, :cond_2

    .line 95
    new-instance v2, Lcom/google/android/music/ssl/ConfigureSniSSLSocketFactory;

    invoke-static {}, Ljavax/net/ssl/SSLSocketFactory;->getDefault()Ljavax/net/SocketFactory;

    move-result-object v7

    check-cast v7, Ljavax/net/ssl/SSLSocketFactory;

    invoke-direct {v2, v7, v11}, Lcom/google/android/music/ssl/ConfigureSniSSLSocketFactory;-><init>(Ljavax/net/ssl/SSLSocketFactory;I)V

    .line 98
    .restart local v2    # "configureSniSSLSocketFactory":Lcom/google/android/music/ssl/ConfigureSniSSLSocketFactory;
    invoke-virtual {v4, v2}, Lcom/google/android/gms/http/GoogleHttpClient;->setSslSocketFactory(Ljavax/net/ssl/SSLSocketFactory;)V

    .line 101
    .end local v2    # "configureSniSSLSocketFactory":Lcom/google/android/music/ssl/ConfigureSniSSLSocketFactory;
    :cond_2
    const-string v7, "GHttpClientFactory"

    const-string v8, "Using the GMSCore\'s GoogleHttpClient"

    invoke-static {v7, v8}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    new-instance v7, Lcom/google/android/music/cloudclient/MusicHttpClient;

    invoke-direct {v7, p0, v4}, Lcom/google/android/music/cloudclient/MusicHttpClient;-><init>(Landroid/content/Context;Lorg/apache/http/client/HttpClient;)V

    goto :goto_1
.end method
