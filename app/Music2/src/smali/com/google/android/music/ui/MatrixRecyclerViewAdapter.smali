.class public abstract Lcom/google/android/music/ui/MatrixRecyclerViewAdapter;
.super Landroid/support/v7/widget/RecyclerView$Adapter;
.source "MatrixRecyclerViewAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/ui/MatrixRecyclerViewAdapter$1;,
        Lcom/google/android/music/ui/MatrixRecyclerViewAdapter$Source;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v7/widget/RecyclerView$Adapter",
        "<",
        "Landroid/support/v7/widget/RecyclerView$ViewHolder;",
        ">;"
    }
.end annotation


# instance fields
.field private mSources:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/android/music/ui/MatrixRecyclerViewAdapter$Source;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$Adapter;-><init>()V

    .line 32
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/ui/MatrixRecyclerViewAdapter;->mSources:Ljava/util/HashMap;

    .line 33
    return-void
.end method

.method private getItem(I)Lcom/google/android/music/ui/MatrixRecyclerViewAdapter$Source;
    .locals 4
    .param p1, "position"    # I

    .prologue
    .line 140
    iget-object v0, p0, Lcom/google/android/music/ui/MatrixRecyclerViewAdapter;->mSources:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 141
    iget-object v0, p0, Lcom/google/android/music/ui/MatrixRecyclerViewAdapter;->mSources:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Lcom/google/android/music/ui/MatrixRecyclerViewAdapter$Source;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Lcom/google/android/music/ui/MatrixRecyclerViewAdapter$Source;-><init>(Lcom/google/android/music/ui/MatrixRecyclerViewAdapter$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 143
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/ui/MatrixRecyclerViewAdapter;->mSources:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/ui/MatrixRecyclerViewAdapter$Source;

    return-object v0
.end method


# virtual methods
.method protected clearItems()V
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/google/android/music/ui/MatrixRecyclerViewAdapter;->mSources:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 131
    return-void
.end method

.method public getItemCount()I
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/music/ui/MatrixRecyclerViewAdapter;->mSources:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    return v0
.end method

.method protected getItemCursorOffset(I)I
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 119
    iget-object v1, p0, Lcom/google/android/music/ui/MatrixRecyclerViewAdapter;->mSources:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/ui/MatrixRecyclerViewAdapter$Source;

    .line 120
    .local v0, "source":Lcom/google/android/music/ui/MatrixRecyclerViewAdapter$Source;
    if-eqz v0, :cond_0

    # getter for: Lcom/google/android/music/ui/MatrixRecyclerViewAdapter$Source;->mCursor:Landroid/database/Cursor;
    invoke-static {v0}, Lcom/google/android/music/ui/MatrixRecyclerViewAdapter$Source;->access$200(Lcom/google/android/music/ui/MatrixRecyclerViewAdapter$Source;)Landroid/database/Cursor;

    move-result-object v1

    if-nez v1, :cond_1

    .line 121
    :cond_0
    const/4 v1, -0x1

    .line 123
    :goto_0
    return v1

    :cond_1
    # getter for: Lcom/google/android/music/ui/MatrixRecyclerViewAdapter$Source;->mCursorOffset:I
    invoke-static {v0}, Lcom/google/android/music/ui/MatrixRecyclerViewAdapter$Source;->access$300(Lcom/google/android/music/ui/MatrixRecyclerViewAdapter$Source;)I

    move-result v1

    goto :goto_0
.end method

.method protected getItemData(I)Landroid/database/Cursor;
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 92
    iget-object v1, p0, Lcom/google/android/music/ui/MatrixRecyclerViewAdapter;->mSources:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/ui/MatrixRecyclerViewAdapter$Source;

    .line 93
    .local v0, "source":Lcom/google/android/music/ui/MatrixRecyclerViewAdapter$Source;
    if-eqz v0, :cond_0

    # getter for: Lcom/google/android/music/ui/MatrixRecyclerViewAdapter$Source;->mCursor:Landroid/database/Cursor;
    invoke-static {v0}, Lcom/google/android/music/ui/MatrixRecyclerViewAdapter$Source;->access$200(Lcom/google/android/music/ui/MatrixRecyclerViewAdapter$Source;)Landroid/database/Cursor;

    move-result-object v1

    if-nez v1, :cond_1

    .line 94
    :cond_0
    const/4 v1, 0x0

    .line 97
    :goto_0
    return-object v1

    .line 96
    :cond_1
    # getter for: Lcom/google/android/music/ui/MatrixRecyclerViewAdapter$Source;->mCursor:Landroid/database/Cursor;
    invoke-static {v0}, Lcom/google/android/music/ui/MatrixRecyclerViewAdapter$Source;->access$200(Lcom/google/android/music/ui/MatrixRecyclerViewAdapter$Source;)Landroid/database/Cursor;

    move-result-object v1

    # getter for: Lcom/google/android/music/ui/MatrixRecyclerViewAdapter$Source;->mCursorOffset:I
    invoke-static {v0}, Lcom/google/android/music/ui/MatrixRecyclerViewAdapter$Source;->access$300(Lcom/google/android/music/ui/MatrixRecyclerViewAdapter$Source;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 97
    # getter for: Lcom/google/android/music/ui/MatrixRecyclerViewAdapter$Source;->mCursor:Landroid/database/Cursor;
    invoke-static {v0}, Lcom/google/android/music/ui/MatrixRecyclerViewAdapter$Source;->access$200(Lcom/google/android/music/ui/MatrixRecyclerViewAdapter$Source;)Landroid/database/Cursor;

    move-result-object v1

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/music/ui/MatrixRecyclerViewAdapter;->mSources:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    const-wide/16 v0, -0x1

    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/music/ui/MatrixRecyclerViewAdapter;->mSources:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/ui/MatrixRecyclerViewAdapter$Source;

    # getter for: Lcom/google/android/music/ui/MatrixRecyclerViewAdapter$Source;->mId:J
    invoke-static {v0}, Lcom/google/android/music/ui/MatrixRecyclerViewAdapter$Source;->access$100(Lcom/google/android/music/ui/MatrixRecyclerViewAdapter$Source;)J

    move-result-wide v0

    goto :goto_0
.end method

.method public getItemViewType(I)I
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/music/ui/MatrixRecyclerViewAdapter;->mSources:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/music/ui/MatrixRecyclerViewAdapter;->mSources:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/ui/MatrixRecyclerViewAdapter$Source;

    # getter for: Lcom/google/android/music/ui/MatrixRecyclerViewAdapter$Source;->mViewType:I
    invoke-static {v0}, Lcom/google/android/music/ui/MatrixRecyclerViewAdapter$Source;->access$000(Lcom/google/android/music/ui/MatrixRecyclerViewAdapter$Source;)I

    move-result v0

    goto :goto_0
.end method

.method protected setItemData(Landroid/database/Cursor;II)V
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "cursorOffset"    # I
    .param p3, "position"    # I

    .prologue
    .line 107
    invoke-direct {p0, p3}, Lcom/google/android/music/ui/MatrixRecyclerViewAdapter;->getItem(I)Lcom/google/android/music/ui/MatrixRecyclerViewAdapter$Source;

    move-result-object v0

    .line 108
    .local v0, "source":Lcom/google/android/music/ui/MatrixRecyclerViewAdapter$Source;
    # setter for: Lcom/google/android/music/ui/MatrixRecyclerViewAdapter$Source;->mCursor:Landroid/database/Cursor;
    invoke-static {v0, p1}, Lcom/google/android/music/ui/MatrixRecyclerViewAdapter$Source;->access$202(Lcom/google/android/music/ui/MatrixRecyclerViewAdapter$Source;Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 109
    # setter for: Lcom/google/android/music/ui/MatrixRecyclerViewAdapter$Source;->mCursorOffset:I
    invoke-static {v0, p2}, Lcom/google/android/music/ui/MatrixRecyclerViewAdapter$Source;->access$302(Lcom/google/android/music/ui/MatrixRecyclerViewAdapter$Source;I)I

    .line 110
    return-void
.end method

.method protected setItemId(JI)V
    .locals 1
    .param p1, "id"    # J
    .param p3, "position"    # I

    .prologue
    .line 80
    invoke-direct {p0, p3}, Lcom/google/android/music/ui/MatrixRecyclerViewAdapter;->getItem(I)Lcom/google/android/music/ui/MatrixRecyclerViewAdapter$Source;

    move-result-object v0

    .line 81
    .local v0, "source":Lcom/google/android/music/ui/MatrixRecyclerViewAdapter$Source;
    # setter for: Lcom/google/android/music/ui/MatrixRecyclerViewAdapter$Source;->mId:J
    invoke-static {v0, p1, p2}, Lcom/google/android/music/ui/MatrixRecyclerViewAdapter$Source;->access$102(Lcom/google/android/music/ui/MatrixRecyclerViewAdapter$Source;J)J

    .line 82
    return-void
.end method

.method protected setItemViewType(II)V
    .locals 1
    .param p1, "viewType"    # I
    .param p2, "position"    # I

    .prologue
    .line 60
    invoke-direct {p0, p2}, Lcom/google/android/music/ui/MatrixRecyclerViewAdapter;->getItem(I)Lcom/google/android/music/ui/MatrixRecyclerViewAdapter$Source;

    move-result-object v0

    .line 61
    .local v0, "source":Lcom/google/android/music/ui/MatrixRecyclerViewAdapter$Source;
    # setter for: Lcom/google/android/music/ui/MatrixRecyclerViewAdapter$Source;->mViewType:I
    invoke-static {v0, p1}, Lcom/google/android/music/ui/MatrixRecyclerViewAdapter$Source;->access$002(Lcom/google/android/music/ui/MatrixRecyclerViewAdapter$Source;I)I

    .line 62
    return-void
.end method
