.class final Lcom/google/android/music/playback/session/MediaSessionCompat$Token$1;
.super Ljava/lang/Object;
.source "MediaSessionCompat.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/playback/session/MediaSessionCompat$Token;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/android/music/playback/session/MediaSessionCompat$Token;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 624
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/google/android/music/playback/session/MediaSessionCompat$Token;
    .locals 2
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 627
    new-instance v0, Lcom/google/android/music/playback/session/MediaSessionCompat$Token;

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/music/playback/session/MediaSessionCompat$Token;-><init>(Landroid/os/Parcelable;)V

    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Landroid/os/Parcel;

    .prologue
    .line 624
    invoke-virtual {p0, p1}, Lcom/google/android/music/playback/session/MediaSessionCompat$Token$1;->createFromParcel(Landroid/os/Parcel;)Lcom/google/android/music/playback/session/MediaSessionCompat$Token;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/google/android/music/playback/session/MediaSessionCompat$Token;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 632
    new-array v0, p1, [Lcom/google/android/music/playback/session/MediaSessionCompat$Token;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 624
    invoke-virtual {p0, p1}, Lcom/google/android/music/playback/session/MediaSessionCompat$Token$1;->newArray(I)[Lcom/google/android/music/playback/session/MediaSessionCompat$Token;

    move-result-object v0

    return-object v0
.end method
