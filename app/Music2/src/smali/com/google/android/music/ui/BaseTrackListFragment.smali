.class public abstract Lcom/google/android/music/ui/BaseTrackListFragment;
.super Lcom/google/android/music/ui/MediaListFragment;
.source "BaseTrackListFragment.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/google/android/music/ui/MediaListFragment;-><init>()V

    .line 26
    return-void
.end method

.method private getTrackListAdapter()Lcom/google/android/music/ui/TrackListAdapter;
    .locals 1

    .prologue
    .line 68
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseTrackListFragment;->getListAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/ui/TrackListAdapter;

    return-object v0
.end method


# virtual methods
.method protected getTrackListView()Lcom/google/android/music/ui/BaseTrackListView;
    .locals 1

    .prologue
    .line 64
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseTrackListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/ui/BaseTrackListView;

    return-object v0
.end method

.method newAsyncCursorAdapter(Landroid/database/Cursor;)Lcom/google/android/music/ui/MediaListCursorAdapter;
    .locals 3
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 48
    new-instance v0, Lcom/google/android/music/ui/TrackListAdapter;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseTrackListFragment;->getMediaList()Lcom/google/android/music/medialist/MediaList;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2, p1}, Lcom/google/android/music/ui/TrackListAdapter;-><init>(Lcom/google/android/music/ui/MusicFragment;ZLcom/google/android/music/medialist/MediaList;Landroid/database/Cursor;)V

    return-object v0
.end method

.method newLoaderCursorAdapter()Lcom/google/android/music/ui/MediaListCursorAdapter;
    .locals 3

    .prologue
    .line 43
    new-instance v0, Lcom/google/android/music/ui/TrackListAdapter;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseTrackListFragment;->getMediaList()Lcom/google/android/music/medialist/MediaList;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/music/ui/TrackListAdapter;-><init>(Lcom/google/android/music/ui/MusicFragment;ZLcom/google/android/music/medialist/MediaList;)V

    return-object v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 37
    invoke-super {p0, p1}, Lcom/google/android/music/ui/MediaListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 38
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseTrackListFragment;->getTrackListView()Lcom/google/android/music/ui/BaseTrackListView;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseTrackListFragment;->getMediaList()Lcom/google/android/music/medialist/MediaList;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/medialist/SongList;

    invoke-virtual {v1, v0}, Lcom/google/android/music/ui/BaseTrackListView;->setSongList(Lcom/google/android/music/medialist/SongList;)V

    .line 39
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 31
    const v0, 0x7f0400f5

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 6
    .param p1, "l"    # Landroid/widget/ListView;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    .line 53
    instance-of v0, p1, Lcom/google/android/music/ui/BaseTrackListView;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 54
    check-cast v0, Lcom/google/android/music/ui/BaseTrackListView;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-wide v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/music/ui/BaseTrackListView;->handleItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V

    .line 58
    :goto_0
    return-void

    .line 56
    :cond_0
    const-string v0, "BaseTrackListFragment"

    const-string v1, "Unknown listview type"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 73
    invoke-super {p0}, Lcom/google/android/music/ui/MediaListFragment;->onPause()V

    .line 78
    invoke-direct {p0}, Lcom/google/android/music/ui/BaseTrackListFragment;->getTrackListAdapter()Lcom/google/android/music/ui/TrackListAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/ui/TrackListAdapter;->disablePlaybackIndicator()V

    .line 79
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 83
    invoke-direct {p0}, Lcom/google/android/music/ui/BaseTrackListFragment;->getTrackListAdapter()Lcom/google/android/music/ui/TrackListAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/ui/TrackListAdapter;->enablePlaybackIndicator()V

    .line 84
    invoke-super {p0}, Lcom/google/android/music/ui/MediaListFragment;->onResume()V

    .line 85
    return-void
.end method
