.class Lcom/google/android/music/ui/MatrixRecyclerViewAdapter$Source;
.super Ljava/lang/Object;
.source "MatrixRecyclerViewAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/MatrixRecyclerViewAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Source"
.end annotation


# instance fields
.field private mCursor:Landroid/database/Cursor;

.field private mCursorOffset:I

.field private mId:J

.field private mViewType:I


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/music/ui/MatrixRecyclerViewAdapter$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/music/ui/MatrixRecyclerViewAdapter$1;

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/google/android/music/ui/MatrixRecyclerViewAdapter$Source;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/ui/MatrixRecyclerViewAdapter$Source;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/MatrixRecyclerViewAdapter$Source;

    .prologue
    .line 22
    iget v0, p0, Lcom/google/android/music/ui/MatrixRecyclerViewAdapter$Source;->mViewType:I

    return v0
.end method

.method static synthetic access$002(Lcom/google/android/music/ui/MatrixRecyclerViewAdapter$Source;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/MatrixRecyclerViewAdapter$Source;
    .param p1, "x1"    # I

    .prologue
    .line 22
    iput p1, p0, Lcom/google/android/music/ui/MatrixRecyclerViewAdapter$Source;->mViewType:I

    return p1
.end method

.method static synthetic access$100(Lcom/google/android/music/ui/MatrixRecyclerViewAdapter$Source;)J
    .locals 2
    .param p0, "x0"    # Lcom/google/android/music/ui/MatrixRecyclerViewAdapter$Source;

    .prologue
    .line 22
    iget-wide v0, p0, Lcom/google/android/music/ui/MatrixRecyclerViewAdapter$Source;->mId:J

    return-wide v0
.end method

.method static synthetic access$102(Lcom/google/android/music/ui/MatrixRecyclerViewAdapter$Source;J)J
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/MatrixRecyclerViewAdapter$Source;
    .param p1, "x1"    # J

    .prologue
    .line 22
    iput-wide p1, p0, Lcom/google/android/music/ui/MatrixRecyclerViewAdapter$Source;->mId:J

    return-wide p1
.end method

.method static synthetic access$200(Lcom/google/android/music/ui/MatrixRecyclerViewAdapter$Source;)Landroid/database/Cursor;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/MatrixRecyclerViewAdapter$Source;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/google/android/music/ui/MatrixRecyclerViewAdapter$Source;->mCursor:Landroid/database/Cursor;

    return-object v0
.end method

.method static synthetic access$202(Lcom/google/android/music/ui/MatrixRecyclerViewAdapter$Source;Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/MatrixRecyclerViewAdapter$Source;
    .param p1, "x1"    # Landroid/database/Cursor;

    .prologue
    .line 22
    iput-object p1, p0, Lcom/google/android/music/ui/MatrixRecyclerViewAdapter$Source;->mCursor:Landroid/database/Cursor;

    return-object p1
.end method

.method static synthetic access$300(Lcom/google/android/music/ui/MatrixRecyclerViewAdapter$Source;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/MatrixRecyclerViewAdapter$Source;

    .prologue
    .line 22
    iget v0, p0, Lcom/google/android/music/ui/MatrixRecyclerViewAdapter$Source;->mCursorOffset:I

    return v0
.end method

.method static synthetic access$302(Lcom/google/android/music/ui/MatrixRecyclerViewAdapter$Source;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/MatrixRecyclerViewAdapter$Source;
    .param p1, "x1"    # I

    .prologue
    .line 22
    iput p1, p0, Lcom/google/android/music/ui/MatrixRecyclerViewAdapter$Source;->mCursorOffset:I

    return p1
.end method
