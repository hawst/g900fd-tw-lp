.class Lcom/google/android/music/icing/WriteTxnHandler$1;
.super Ljava/lang/Object;
.source "WriteTxnHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/icing/WriteTxnHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/icing/WriteTxnHandler;


# direct methods
.method constructor <init>(Lcom/google/android/music/icing/WriteTxnHandler;)V
    .locals 0

    .prologue
    .line 29
    iput-object p1, p0, Lcom/google/android/music/icing/WriteTxnHandler$1;->this$0:Lcom/google/android/music/icing/WriteTxnHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 33
    iget-object v5, p0, Lcom/google/android/music/icing/WriteTxnHandler$1;->this$0:Lcom/google/android/music/icing/WriteTxnHandler;

    # getter for: Lcom/google/android/music/icing/WriteTxnHandler;->mListener:Ljava/util/concurrent/atomic/AtomicReference;
    invoke-static {v5}, Lcom/google/android/music/icing/WriteTxnHandler;->access$000(Lcom/google/android/music/icing/WriteTxnHandler;)Ljava/util/concurrent/atomic/AtomicReference;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/gms/appdatasearch/util/AppDataSearchDatabase$TableChangeListener;

    .line 34
    .local v3, "listener":Lcom/google/android/gms/appdatasearch/util/AppDataSearchDatabase$TableChangeListener;
    if-eqz v3, :cond_0

    .line 36
    iget-object v5, p0, Lcom/google/android/music/icing/WriteTxnHandler$1;->this$0:Lcom/google/android/music/icing/WriteTxnHandler;

    # getter for: Lcom/google/android/music/icing/WriteTxnHandler;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/google/android/music/icing/WriteTxnHandler;->access$100(Lcom/google/android/music/icing/WriteTxnHandler;)Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/music/icing/SearchCorpora;->getInstance(Landroid/content/Context;)Lcom/google/android/music/icing/SearchCorpora;

    move-result-object v5

    iget-object v0, v5, Lcom/google/android/music/icing/SearchCorpora;->tableStorageSpecs:[Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;

    .local v0, "arr$":[Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v4, v0, v1

    .line 37
    .local v4, "spec":Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;
    invoke-interface {v3, v4}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchDatabase$TableChangeListener;->onTableChanged(Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;)Z

    .line 36
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 40
    .end local v0    # "arr$":[Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;
    .end local v1    # "i$":I
    .end local v2    # "len$":I
    .end local v4    # "spec":Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;
    :cond_0
    return-void
.end method
