.class public Lcom/google/android/music/leanback/LeanbackMyMixesActivity;
.super Lcom/google/android/music/leanback/LeanbackGridActivity;
.source "LeanbackMyMixesActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackGridActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected bindCursor(Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;Landroid/database/Cursor;)Ljava/lang/Object;
    .locals 10
    .param p1, "leanbackCursorObjectAdapter"    # Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;
    .param p2, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v8, 0x1

    const/4 v1, 0x0

    .line 36
    invoke-interface {p2, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 37
    .local v2, "id":J
    invoke-interface {p2, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 38
    .local v5, "name":Ljava/lang/String;
    const/4 v4, 0x6

    invoke-interface {p2, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    .line 39
    .local v9, "seedType":I
    const/4 v4, 0x4

    if-ne v9, v4, :cond_0

    .line 41
    .local v8, "isArtistSeedType":Z
    :goto_0
    const/4 v1, 0x3

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 42
    .local v6, "artUriEncoded":Ljava/lang/String;
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/preferences/MusicPreferences;->isNautilusEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    const v1, 0x7f0b0109

    invoke-virtual {p0, v1}, Lcom/google/android/music/leanback/LeanbackMyMixesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 46
    .local v0, "description":Ljava/lang/String;
    :goto_1
    new-instance v1, Lcom/google/android/music/mix/MixDescriptor;

    sget-object v4, Lcom/google/android/music/mix/MixDescriptor$Type;->RADIO:Lcom/google/android/music/mix/MixDescriptor$Type;

    invoke-direct/range {v1 .. v6}, Lcom/google/android/music/mix/MixDescriptor;-><init>(JLcom/google/android/music/mix/MixDescriptor$Type;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p0, v1}, Lcom/google/android/music/leanback/LeanbackUtils;->newPlayIntent(Landroid/content/Context;Lcom/google/android/music/mix/MixDescriptor;)Landroid/content/Intent;

    move-result-object v7

    .line 49
    .local v7, "intent":Landroid/content/Intent;
    new-instance v1, Lcom/google/android/music/leanback/Item$Builder;

    invoke-direct {v1}, Lcom/google/android/music/leanback/Item$Builder;-><init>()V

    invoke-virtual {v1, v5}, Lcom/google/android/music/leanback/Item$Builder;->title(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/music/leanback/Item$Builder;->description(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v1

    invoke-virtual {v1, v6}, Lcom/google/android/music/leanback/Item$Builder;->iconUri(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v1

    invoke-virtual {v1, v7}, Lcom/google/android/music/leanback/Item$Builder;->intent(Landroid/content/Intent;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v1

    invoke-virtual {v1, v8}, Lcom/google/android/music/leanback/Item$Builder;->wide(Z)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v1

    const v4, 0x7f0200cf

    invoke-virtual {v1, v4}, Lcom/google/android/music/leanback/Item$Builder;->overlayResourceId(I)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/leanback/Item$Builder;->build()Lcom/google/android/music/leanback/Item;

    move-result-object v1

    return-object v1

    .end local v0    # "description":Ljava/lang/String;
    .end local v6    # "artUriEncoded":Ljava/lang/String;
    .end local v7    # "intent":Landroid/content/Intent;
    .end local v8    # "isArtistSeedType":Z
    :cond_0
    move v8, v1

    .line 39
    goto :goto_0

    .line 42
    .restart local v6    # "artUriEncoded":Ljava/lang/String;
    .restart local v8    # "isArtistSeedType":Z
    :cond_1
    const v1, 0x7f0b010a

    invoke-virtual {p0, v1}, Lcom/google/android/music/leanback/LeanbackMyMixesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method protected createItemPresenter()Lcom/google/android/music/leanback/ItemPresenter;
    .locals 2

    .prologue
    .line 61
    new-instance v0, Lcom/google/android/music/leanback/ItemPresenter;

    const/4 v1, 0x1

    invoke-direct {v0, p0, v1}, Lcom/google/android/music/leanback/ItemPresenter;-><init>(Landroid/content/Context;Z)V

    return-object v0
.end method

.method protected getGridTitle()Ljava/lang/String;
    .locals 2

    .prologue
    .line 56
    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackMyMixesActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "title"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getMediaList()Lcom/google/android/music/medialist/MediaList;
    .locals 1

    .prologue
    .line 25
    new-instance v0, Lcom/google/android/music/medialist/MyRadioList;

    invoke-direct {v0}, Lcom/google/android/music/medialist/MyRadioList;-><init>()V

    return-object v0
.end method

.method protected getProjection()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/google/android/music/ui/MyRadioFragment;->PROJECTION:[Ljava/lang/String;

    return-object v0
.end method
