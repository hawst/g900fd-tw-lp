.class public Lcom/google/android/music/store/OldMusicFile50;
.super Lcom/google/android/music/store/Syncable;
.source "OldMusicFile50.java"


# static fields
.field private static final DELETE_PROJECTION:[Ljava/lang/String;

.field static final FULL_PROJECTION:[Ljava/lang/String;

.field public static final MEDIA_STORE_SOURCE_ACCOUNT_AS_INTEGER:Ljava/lang/Integer;

.field public static final MEDIA_STORE_SOURCE_ACCOUNT_AS_STRING:Ljava/lang/String;

.field public static final PLAYCOUNT_SYNC_PROJECTION:[Ljava/lang/String;

.field private static SUMMARY_PROJECTION:[Ljava/lang/String;


# instance fields
.field private mAddedTime:J

.field private mAlbumArtLocation:Ljava/lang/String;

.field private mAlbumArtLocationChanged:Z

.field private mAlbumArtist:Ljava/lang/String;

.field private mAlbumArtistId:J

.field private mAlbumArtistOrigin:I

.field private mAlbumId:J

.field private mAlbumName:Ljava/lang/String;

.field private mBitrate:I

.field private mCanonicalAlbum:Ljava/lang/String;

.field private mCanonicalAlbumArtist:Ljava/lang/String;

.field private mCanonicalGenre:Ljava/lang/String;

.field private mCanonicalTitle:Ljava/lang/String;

.field private mCanonicalTrackArtist:Ljava/lang/String;

.field private mCompilation:Z

.field private mComposer:Ljava/lang/String;

.field private mDiscCount:S

.field private mDiscPosition:S

.field private mDomain:I

.field private mDurationInMilliSec:J

.field private mFileType:I

.field private mGenre:Ljava/lang/String;

.field private mGenreId:J

.field private mLastPlayDate:J

.field private mLocalCopyBitrate:I

.field private mLocalCopyPath:Ljava/lang/String;

.field private mLocalCopySize:J

.field private mLocalCopyStorageType:I

.field private mLocalCopyType:I

.field private mLocalId:J

.field private mNormalizer:Lcom/google/android/music/store/TagNormalizer;

.field private mPlayCount:I

.field private mRating:I

.field private mSize:J

.field private mSongId:J

.field private mStoreAlbumId:Ljava/lang/String;

.field private mStoreSongId:Ljava/lang/String;

.field private mTitle:Ljava/lang/String;

.field private mTrackArtist:Ljava/lang/String;

.field private mTrackArtistId:J

.field private mTrackArtistOrigin:I

.field private mTrackCountInAlbum:S

.field private mTrackPositionInAlbum:S

.field private mTrackType:I

.field private mYear:S


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 36
    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/store/OldMusicFile50;->MEDIA_STORE_SOURCE_ACCOUNT_AS_STRING:Ljava/lang/String;

    .line 38
    new-instance v0, Ljava/lang/Integer;

    invoke-direct {v0, v3}, Ljava/lang/Integer;-><init>(I)V

    sput-object v0, Lcom/google/android/music/store/OldMusicFile50;->MEDIA_STORE_SOURCE_ACCOUNT_AS_INTEGER:Ljava/lang/Integer;

    .line 42
    const/16 v0, 0xf

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "MUSIC.Id"

    aput-object v1, v0, v3

    const-string v1, "SourceAccount"

    aput-object v1, v0, v4

    const-string v1, "SourceId"

    aput-object v1, v0, v5

    const-string v1, "Size"

    aput-object v1, v0, v6

    const-string v1, "LocalCopyPath"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "LocalCopyType"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "Duration"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "Album"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "AlbumArtist"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "AlbumArtistOrigin"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "Artist"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "Title"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "LocalCopyStorageType"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "LocalCopySize"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "Domain"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/store/OldMusicFile50;->SUMMARY_PROJECTION:[Ljava/lang/String;

    .line 76
    const/16 v0, 0x2f

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "MUSIC.Id"

    aput-object v1, v0, v3

    const-string v1, "SourceAccount"

    aput-object v1, v0, v4

    const-string v1, "SourceId"

    aput-object v1, v0, v5

    const-string v1, "_sync_version"

    aput-object v1, v0, v6

    const-string v1, "Size"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "FileType"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "FileDate"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "LocalCopyPath"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "LocalCopyType"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "Title"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "Album"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "Artist"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "AlbumArtist"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "Composer"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "Genre"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "Year"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "Duration"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "TrackCount"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "TrackNumber"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "DiscCount"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "DiscNumber"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "Compilation"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "BitRate"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "AlbumArtLocation"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "SongId"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "AlbumId"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "AlbumArtistId"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "GenreId"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "CanonicalName"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "CanonicalAlbum"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "CanonicalAlbumArtist"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, "CanonicalGenre"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, "PlayCount"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, "LastPlayDate"

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string v2, "AlbumArtistOrigin"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string v2, "LocalCopySize"

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string v2, "LocalCopyBitrate"

    aput-object v2, v0, v1

    const/16 v1, 0x25

    const-string v2, "TrackType"

    aput-object v2, v0, v1

    const/16 v1, 0x26

    const-string v2, "ArtistOrigin"

    aput-object v2, v0, v1

    const/16 v1, 0x27

    const-string v2, "ArtistId"

    aput-object v2, v0, v1

    const/16 v1, 0x28

    const-string v2, "CanonicalArtist"

    aput-object v2, v0, v1

    const/16 v1, 0x29

    const-string v2, "Rating"

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    const-string v2, "_sync_dirty"

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    const-string v2, "StoreId"

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    const-string v2, "StoreAlbumId"

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    const-string v2, "LocalCopyStorageType"

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    const-string v2, "Domain"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/store/OldMusicFile50;->FULL_PROJECTION:[Ljava/lang/String;

    .line 325
    new-array v0, v7, [Ljava/lang/String;

    const-string v1, "Id"

    aput-object v1, v0, v3

    const-string v1, "LocalCopyType"

    aput-object v1, v0, v4

    const-string v1, "LocalCopyPath"

    aput-object v1, v0, v5

    const-string v1, "LocalCopyStorageType"

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/android/music/store/OldMusicFile50;->DELETE_PROJECTION:[Ljava/lang/String;

    .line 336
    new-array v0, v7, [Ljava/lang/String;

    const-string v1, "Id"

    aput-object v1, v0, v3

    const-string v1, "SourceAccount"

    aput-object v1, v0, v4

    const-string v1, "SourceId"

    aput-object v1, v0, v5

    const-string v1, "PlayCount"

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/android/music/store/OldMusicFile50;->PLAYCOUNT_SYNC_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 28
    invoke-direct {p0}, Lcom/google/android/music/store/Syncable;-><init>()V

    .line 353
    iput v0, p0, Lcom/google/android/music/store/OldMusicFile50;->mTrackType:I

    .line 355
    iput v0, p0, Lcom/google/android/music/store/OldMusicFile50;->mFileType:I

    .line 358
    iput v0, p0, Lcom/google/android/music/store/OldMusicFile50;->mLocalCopyType:I

    .line 361
    iput v0, p0, Lcom/google/android/music/store/OldMusicFile50;->mLocalCopyStorageType:I

    .line 364
    iput v0, p0, Lcom/google/android/music/store/OldMusicFile50;->mTrackArtistOrigin:I

    .line 366
    iput v0, p0, Lcom/google/android/music/store/OldMusicFile50;->mAlbumArtistOrigin:I

    .line 379
    iput-boolean v0, p0, Lcom/google/android/music/store/OldMusicFile50;->mAlbumArtLocationChanged:Z

    .line 390
    iput v0, p0, Lcom/google/android/music/store/OldMusicFile50;->mDomain:I

    .line 395
    iput v0, p0, Lcom/google/android/music/store/OldMusicFile50;->mRating:I

    return-void
.end method

.method public static fixUnknownAlbumsAndArtists50(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 24
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 1906
    const/16 v23, 0x0

    .line 1907
    .local v23, "update":Landroid/database/sqlite/SQLiteStatement;
    const-string v5, "MUSIC"

    const/4 v4, 0x1

    new-array v6, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v7, "MUSIC.Id"

    aput-object v7, v6, v4

    const-string v7, "CanonicalAlbum=\"\" OR CanonicalAlbumArtist=\"\""

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v4, p0

    invoke-virtual/range {v4 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    .line 1912
    .local v13, "c":Landroid/database/Cursor;
    if-eqz v13, :cond_1

    .line 1913
    :try_start_0
    new-instance v15, Lcom/google/android/music/store/OldMusicFile50;

    invoke-direct {v15}, Lcom/google/android/music/store/OldMusicFile50;-><init>()V

    .line 1914
    .local v15, "file":Lcom/google/android/music/store/OldMusicFile50;
    const-string v4, "update MUSIC set SourceAccount=?, SourceId=?, _sync_version=?, Size=?, FileType=?, FileDate=?, LocalCopyPath=?, LocalCopyType=?, Title=?, Album=?, Artist=?, AlbumArtist=?, AlbumArtistOrigin=?, Composer=?, Genre=?, Year=?, Duration=?, TrackCount=?, TrackNumber=?, DiscCount=?, DiscNumber=?, Compilation=?, BitRate=?, AlbumArtLocation=?, SongId=?, AlbumId=?, AlbumArtistId=?, GenreId=?, CanonicalName=?, CanonicalAlbum=?, CanonicalAlbumArtist=?, CanonicalGenre=?, PlayCount=?, LastPlayDate=?, LocalCopySize=?, LocalCopyBitrate=?, TrackType=?, ArtistOrigin=?, ArtistId=?, CanonicalArtist=?, Rating=?, _sync_dirty=?, StoreId=?,StoreAlbumId=?,LocalCopyStorageType=?,Domain=? where Id=?"

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v23

    .line 1919
    invoke-interface {v13}, Landroid/database/Cursor;->getCount()I

    move-result v4

    new-array v0, v4, [J

    move-object/from16 v19, v0

    .line 1920
    .local v19, "ids":[J
    const/16 v16, 0x0

    .local v16, "i":I
    move/from16 v17, v16

    .line 1921
    .end local v16    # "i":I
    .local v17, "i":I
    :goto_0
    invoke-interface {v13}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1922
    const/4 v4, 0x0

    invoke-interface {v13, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v20

    .line 1923
    .local v20, "id":J
    add-int/lit8 v16, v17, 0x1

    .end local v17    # "i":I
    .restart local v16    # "i":I
    aput-wide v20, v19, v17

    move/from16 v17, v16

    .line 1924
    .end local v16    # "i":I
    .restart local v17    # "i":I
    goto :goto_0

    .line 1925
    .end local v20    # "id":J
    :cond_0
    invoke-static {v13}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 1926
    const/4 v13, 0x0

    .line 1927
    move-object/from16 v12, v19

    .local v12, "arr$":[J
    array-length v0, v12

    move/from16 v22, v0

    .local v22, "len$":I
    const/16 v18, 0x0

    .local v18, "i$":I
    :goto_1
    move/from16 v0, v18

    move/from16 v1, v22

    if-ge v0, v1, :cond_1

    aget-wide v20, v12, v18
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1929
    .restart local v20    # "id":J
    :try_start_1
    move-object/from16 v0, p0

    move-wide/from16 v1, v20

    invoke-virtual {v15, v0, v1, v2}, Lcom/google/android/music/store/OldMusicFile50;->load(Landroid/database/sqlite/SQLiteDatabase;J)V

    .line 1930
    invoke-virtual {v15}, Lcom/google/android/music/store/OldMusicFile50;->resetDerivedFields()V

    .line 1931
    move-object/from16 v0, v23

    move-object/from16 v1, p0

    invoke-virtual {v15, v0, v1}, Lcom/google/android/music/store/OldMusicFile50;->updateMusicFile(Landroid/database/sqlite/SQLiteStatement;Landroid/database/sqlite/SQLiteDatabase;)V
    :try_end_1
    .catch Lcom/google/android/music/store/DataNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1927
    :goto_2
    add-int/lit8 v18, v18, 0x1

    goto :goto_1

    .line 1932
    :catch_0
    move-exception v14

    .line 1933
    .local v14, "e":Lcom/google/android/music/store/DataNotFoundException;
    :try_start_2
    const-string v4, "OldMusicFile50"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed to load music file for id: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, v20

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 1938
    .end local v12    # "arr$":[J
    .end local v14    # "e":Lcom/google/android/music/store/DataNotFoundException;
    .end local v15    # "file":Lcom/google/android/music/store/OldMusicFile50;
    .end local v17    # "i":I
    .end local v18    # "i$":I
    .end local v19    # "ids":[J
    .end local v20    # "id":J
    .end local v22    # "len$":I
    :catchall_0
    move-exception v4

    invoke-static {v13}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 1939
    invoke-static/range {v23 .. v23}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/sqlite/SQLiteProgram;)V

    throw v4

    .line 1938
    :cond_1
    invoke-static {v13}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 1939
    invoke-static/range {v23 .. v23}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/sqlite/SQLiteProgram;)V

    .line 1941
    return-void
.end method

.method private prepareInsertOrFullUpdate(Landroid/database/sqlite/SQLiteStatement;)V
    .locals 11
    .param p1, "statement"    # Landroid/database/sqlite/SQLiteStatement;

    .prologue
    const/4 v10, 0x7

    const/4 v9, 0x2

    const/4 v8, 0x3

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 990
    iget-object v6, p0, Lcom/google/android/music/store/OldMusicFile50;->mSourceId:Ljava/lang/String;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/google/android/music/store/OldMusicFile50;->mSourceId:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    if-nez v6, :cond_1

    .line 991
    :cond_0
    new-instance v4, Lcom/google/android/music/store/InvalidDataException;

    const-string v5, "Source id must be set before saving to DB"

    invoke-direct {v4, v5}, Lcom/google/android/music/store/InvalidDataException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 994
    :cond_1
    iget-object v6, p0, Lcom/google/android/music/store/OldMusicFile50;->mStoreSongId:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_3

    move v1, v4

    .line 995
    .local v1, "hasStoreSongId":Z
    :goto_0
    iget v6, p0, Lcom/google/android/music/store/OldMusicFile50;->mTrackType:I

    if-eq v6, v4, :cond_2

    iget v6, p0, Lcom/google/android/music/store/OldMusicFile50;->mTrackType:I

    if-eq v6, v8, :cond_2

    iget v6, p0, Lcom/google/android/music/store/OldMusicFile50;->mTrackType:I

    if-ne v6, v9, :cond_4

    :cond_2
    move v3, v4

    .line 998
    .local v3, "storeTrack":Z
    :goto_1
    if-eqz v1, :cond_5

    if-nez v3, :cond_5

    .line 999
    new-instance v4, Lcom/google/android/music/store/InvalidDataException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Store song id is set for track that is not promo or purchase. Store song id: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/music/store/OldMusicFile50;->mStoreSongId:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " ServerId:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/music/store/OldMusicFile50;->mSourceId:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " Title: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/music/store/OldMusicFile50;->mTitle:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/google/android/music/store/InvalidDataException;-><init>(Ljava/lang/String;)V

    throw v4

    .end local v1    # "hasStoreSongId":Z
    .end local v3    # "storeTrack":Z
    :cond_3
    move v1, v5

    .line 994
    goto :goto_0

    .restart local v1    # "hasStoreSongId":Z
    :cond_4
    move v3, v5

    .line 995
    goto :goto_1

    .line 1005
    .restart local v3    # "storeTrack":Z
    :cond_5
    iget-object v6, p0, Lcom/google/android/music/store/OldMusicFile50;->mStoreAlbumId:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_6

    move v0, v4

    .line 1006
    .local v0, "hasStoreAlbumId":Z
    :goto_2
    if-eqz v0, :cond_7

    if-nez v3, :cond_7

    .line 1007
    new-instance v4, Lcom/google/android/music/store/InvalidDataException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Store album ID is set for track that is not promo or purchase. Store album ID: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/music/store/OldMusicFile50;->mStoreAlbumId:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " ServerId:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/music/store/OldMusicFile50;->mSourceId:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " Title: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/music/store/OldMusicFile50;->mTitle:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/google/android/music/store/InvalidDataException;-><init>(Ljava/lang/String;)V

    throw v4

    .end local v0    # "hasStoreAlbumId":Z
    :cond_6
    move v0, v5

    .line 1005
    goto :goto_2

    .line 1013
    .restart local v0    # "hasStoreAlbumId":Z
    :cond_7
    iget-object v5, p0, Lcom/google/android/music/store/OldMusicFile50;->mLocalCopyPath:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    .line 1014
    .local v2, "isEmptyPath":Z
    iget v5, p0, Lcom/google/android/music/store/OldMusicFile50;->mLocalCopyStorageType:I

    packed-switch v5, :pswitch_data_0

    .line 1033
    new-instance v4, Lcom/google/android/music/store/InvalidDataException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Invalid storage type:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/google/android/music/store/OldMusicFile50;->mLocalCopyStorageType:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/google/android/music/store/InvalidDataException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 1016
    :pswitch_0
    if-nez v2, :cond_8

    .line 1017
    new-instance v4, Lcom/google/android/music/store/InvalidDataException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Local path is set for storage type NONE: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/music/store/OldMusicFile50;->mLocalCopyPath:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/google/android/music/store/InvalidDataException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 1022
    :pswitch_1
    if-eqz v2, :cond_8

    .line 1023
    new-instance v4, Lcom/google/android/music/store/InvalidDataException;

    const-string v5, "Local path is not set for storage type INTERNAL"

    invoke-direct {v4, v5}, Lcom/google/android/music/store/InvalidDataException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 1028
    :pswitch_2
    if-eqz v2, :cond_8

    .line 1029
    new-instance v4, Lcom/google/android/music/store/InvalidDataException;

    const-string v5, "Local path is not set for storage type EXTERNAL"

    invoke-direct {v4, v5}, Lcom/google/android/music/store/InvalidDataException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 1036
    :cond_8
    invoke-virtual {p0}, Lcom/google/android/music/store/OldMusicFile50;->setDerivedFields()V

    .line 1038
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    .line 1040
    iget v5, p0, Lcom/google/android/music/store/OldMusicFile50;->mSourceAccount:I

    int-to-long v6, v5

    invoke-virtual {p1, v4, v6, v7}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 1041
    const/16 v4, 0x25

    iget v5, p0, Lcom/google/android/music/store/OldMusicFile50;->mTrackType:I

    int-to-long v6, v5

    invoke-virtual {p1, v4, v6, v7}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 1042
    iget-object v4, p0, Lcom/google/android/music/store/OldMusicFile50;->mSourceId:Ljava/lang/String;

    invoke-virtual {p1, v9, v4}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 1043
    iget-object v4, p0, Lcom/google/android/music/store/OldMusicFile50;->mSourceVersion:Ljava/lang/String;

    if-nez v4, :cond_a

    .line 1044
    invoke-virtual {p1, v8}, Landroid/database/sqlite/SQLiteStatement;->bindNull(I)V

    .line 1049
    :goto_3
    const/4 v4, 0x4

    iget-wide v6, p0, Lcom/google/android/music/store/OldMusicFile50;->mSize:J

    invoke-virtual {p1, v4, v6, v7}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 1050
    const/4 v4, 0x5

    iget v5, p0, Lcom/google/android/music/store/OldMusicFile50;->mFileType:I

    int-to-long v6, v5

    invoke-virtual {p1, v4, v6, v7}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 1052
    const/4 v4, 0x6

    iget-wide v6, p0, Lcom/google/android/music/store/OldMusicFile50;->mAddedTime:J

    invoke-virtual {p1, v4, v6, v7}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 1054
    if-eqz v2, :cond_b

    .line 1055
    invoke-virtual {p1, v10}, Landroid/database/sqlite/SQLiteStatement;->bindNull(I)V

    .line 1059
    :goto_4
    const/16 v4, 0x8

    iget v5, p0, Lcom/google/android/music/store/OldMusicFile50;->mLocalCopyType:I

    int-to-long v6, v5

    invoke-virtual {p1, v4, v6, v7}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 1060
    const/16 v4, 0x2d

    iget v5, p0, Lcom/google/android/music/store/OldMusicFile50;->mLocalCopyStorageType:I

    int-to-long v6, v5

    invoke-virtual {p1, v4, v6, v7}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 1061
    const/16 v4, 0x24

    iget v5, p0, Lcom/google/android/music/store/OldMusicFile50;->mLocalCopyBitrate:I

    int-to-long v6, v5

    invoke-virtual {p1, v4, v6, v7}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 1063
    const/16 v5, 0x9

    iget-object v4, p0, Lcom/google/android/music/store/OldMusicFile50;->mTitle:Ljava/lang/String;

    if-nez v4, :cond_c

    const-string v4, ""

    :goto_5
    invoke-virtual {p1, v5, v4}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 1064
    const/16 v5, 0xa

    iget-object v4, p0, Lcom/google/android/music/store/OldMusicFile50;->mAlbumName:Ljava/lang/String;

    if-nez v4, :cond_d

    const-string v4, ""

    :goto_6
    invoke-virtual {p1, v5, v4}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 1065
    const/16 v5, 0xb

    iget-object v4, p0, Lcom/google/android/music/store/OldMusicFile50;->mTrackArtist:Ljava/lang/String;

    if-nez v4, :cond_e

    const-string v4, ""

    :goto_7
    invoke-virtual {p1, v5, v4}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 1067
    const/16 v4, 0x26

    iget v5, p0, Lcom/google/android/music/store/OldMusicFile50;->mTrackArtistOrigin:I

    int-to-long v6, v5

    invoke-virtual {p1, v4, v6, v7}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 1069
    const/16 v5, 0xc

    iget-object v4, p0, Lcom/google/android/music/store/OldMusicFile50;->mAlbumArtist:Ljava/lang/String;

    if-nez v4, :cond_f

    const-string v4, ""

    :goto_8
    invoke-virtual {p1, v5, v4}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 1071
    const/16 v4, 0xd

    iget v5, p0, Lcom/google/android/music/store/OldMusicFile50;->mAlbumArtistOrigin:I

    int-to-long v6, v5

    invoke-virtual {p1, v4, v6, v7}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 1072
    const/16 v5, 0xe

    iget-object v4, p0, Lcom/google/android/music/store/OldMusicFile50;->mComposer:Ljava/lang/String;

    if-nez v4, :cond_10

    const-string v4, ""

    :goto_9
    invoke-virtual {p1, v5, v4}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 1073
    const/16 v5, 0xf

    iget-object v4, p0, Lcom/google/android/music/store/OldMusicFile50;->mGenre:Ljava/lang/String;

    if-nez v4, :cond_11

    const-string v4, ""

    :goto_a
    invoke-virtual {p1, v5, v4}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 1074
    const/16 v4, 0x10

    iget-short v5, p0, Lcom/google/android/music/store/OldMusicFile50;->mYear:S

    int-to-long v6, v5

    invoke-virtual {p1, v4, v6, v7}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 1075
    const/16 v4, 0x11

    iget-wide v6, p0, Lcom/google/android/music/store/OldMusicFile50;->mDurationInMilliSec:J

    invoke-virtual {p1, v4, v6, v7}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 1076
    const/16 v4, 0x12

    iget-short v5, p0, Lcom/google/android/music/store/OldMusicFile50;->mTrackCountInAlbum:S

    int-to-long v6, v5

    invoke-virtual {p1, v4, v6, v7}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 1077
    const/16 v4, 0x13

    iget-short v5, p0, Lcom/google/android/music/store/OldMusicFile50;->mTrackPositionInAlbum:S

    int-to-long v6, v5

    invoke-virtual {p1, v4, v6, v7}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 1078
    const/16 v4, 0x14

    iget-short v5, p0, Lcom/google/android/music/store/OldMusicFile50;->mDiscCount:S

    int-to-long v6, v5

    invoke-virtual {p1, v4, v6, v7}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 1079
    const/16 v4, 0x15

    iget-short v5, p0, Lcom/google/android/music/store/OldMusicFile50;->mDiscPosition:S

    int-to-long v6, v5

    invoke-virtual {p1, v4, v6, v7}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 1080
    const/16 v6, 0x16

    iget-boolean v4, p0, Lcom/google/android/music/store/OldMusicFile50;->mCompilation:Z

    if-eqz v4, :cond_12

    const-wide/16 v4, 0x1

    :goto_b
    invoke-virtual {p1, v6, v4, v5}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 1081
    const/16 v4, 0x17

    iget v5, p0, Lcom/google/android/music/store/OldMusicFile50;->mBitrate:I

    int-to-long v6, v5

    invoke-virtual {p1, v4, v6, v7}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 1082
    iget-object v4, p0, Lcom/google/android/music/store/OldMusicFile50;->mAlbumArtLocation:Ljava/lang/String;

    if-eqz v4, :cond_9

    iget-object v4, p0, Lcom/google/android/music/store/OldMusicFile50;->mAlbumArtLocation:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_13

    .line 1083
    :cond_9
    const/16 v4, 0x18

    invoke-virtual {p1, v4}, Landroid/database/sqlite/SQLiteStatement;->bindNull(I)V

    .line 1087
    :goto_c
    const/16 v4, 0x19

    iget-wide v6, p0, Lcom/google/android/music/store/OldMusicFile50;->mSongId:J

    invoke-virtual {p1, v4, v6, v7}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 1088
    const/16 v4, 0x1a

    iget-wide v6, p0, Lcom/google/android/music/store/OldMusicFile50;->mAlbumId:J

    invoke-virtual {p1, v4, v6, v7}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 1089
    const/16 v4, 0x1b

    iget-wide v6, p0, Lcom/google/android/music/store/OldMusicFile50;->mAlbumArtistId:J

    invoke-virtual {p1, v4, v6, v7}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 1090
    const/16 v4, 0x27

    iget-wide v6, p0, Lcom/google/android/music/store/OldMusicFile50;->mTrackArtistId:J

    invoke-virtual {p1, v4, v6, v7}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 1091
    const/16 v4, 0x1c

    iget-wide v6, p0, Lcom/google/android/music/store/OldMusicFile50;->mGenreId:J

    invoke-virtual {p1, v4, v6, v7}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 1092
    const/16 v5, 0x1d

    iget-object v4, p0, Lcom/google/android/music/store/OldMusicFile50;->mCanonicalTitle:Ljava/lang/String;

    if-nez v4, :cond_14

    const-string v4, ""

    :goto_d
    invoke-virtual {p1, v5, v4}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 1094
    const/16 v5, 0x1e

    iget-object v4, p0, Lcom/google/android/music/store/OldMusicFile50;->mCanonicalAlbum:Ljava/lang/String;

    if-nez v4, :cond_15

    const-string v4, ""

    :goto_e
    invoke-virtual {p1, v5, v4}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 1096
    const/16 v5, 0x1f

    iget-object v4, p0, Lcom/google/android/music/store/OldMusicFile50;->mCanonicalAlbumArtist:Ljava/lang/String;

    if-nez v4, :cond_16

    const-string v4, ""

    :goto_f
    invoke-virtual {p1, v5, v4}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 1098
    const/16 v5, 0x28

    iget-object v4, p0, Lcom/google/android/music/store/OldMusicFile50;->mCanonicalTrackArtist:Ljava/lang/String;

    if-nez v4, :cond_17

    const-string v4, ""

    :goto_10
    invoke-virtual {p1, v5, v4}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 1100
    const/16 v5, 0x20

    iget-object v4, p0, Lcom/google/android/music/store/OldMusicFile50;->mCanonicalGenre:Ljava/lang/String;

    if-nez v4, :cond_18

    const-string v4, ""

    :goto_11
    invoke-virtual {p1, v5, v4}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 1102
    const/16 v4, 0x21

    iget v5, p0, Lcom/google/android/music/store/OldMusicFile50;->mPlayCount:I

    int-to-long v6, v5

    invoke-virtual {p1, v4, v6, v7}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 1103
    const/16 v4, 0x22

    iget-wide v6, p0, Lcom/google/android/music/store/OldMusicFile50;->mLastPlayDate:J

    invoke-virtual {p1, v4, v6, v7}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 1104
    const/16 v4, 0x23

    iget-wide v6, p0, Lcom/google/android/music/store/OldMusicFile50;->mLocalCopySize:J

    invoke-virtual {p1, v4, v6, v7}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 1105
    const/16 v4, 0x29

    iget v5, p0, Lcom/google/android/music/store/OldMusicFile50;->mRating:I

    int-to-long v6, v5

    invoke-virtual {p1, v4, v6, v7}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 1106
    const/16 v6, 0x2a

    iget-boolean v4, p0, Lcom/google/android/music/store/OldMusicFile50;->mNeedsSync:Z

    if-eqz v4, :cond_19

    const-wide/16 v4, 0x1

    :goto_12
    invoke-virtual {p1, v6, v4, v5}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 1108
    if-eqz v1, :cond_1a

    .line 1109
    const/16 v4, 0x2b

    iget-object v5, p0, Lcom/google/android/music/store/OldMusicFile50;->mStoreSongId:Ljava/lang/String;

    invoke-virtual {p1, v4, v5}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 1113
    :goto_13
    if-eqz v0, :cond_1b

    .line 1114
    const/16 v4, 0x2c

    iget-object v5, p0, Lcom/google/android/music/store/OldMusicFile50;->mStoreAlbumId:Ljava/lang/String;

    invoke-virtual {p1, v4, v5}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 1118
    :goto_14
    const/16 v4, 0x2e

    iget v5, p0, Lcom/google/android/music/store/OldMusicFile50;->mDomain:I

    int-to-long v6, v5

    invoke-virtual {p1, v4, v6, v7}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 1119
    return-void

    .line 1046
    :cond_a
    iget-object v4, p0, Lcom/google/android/music/store/OldMusicFile50;->mSourceVersion:Ljava/lang/String;

    invoke-virtual {p1, v8, v4}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    goto/16 :goto_3

    .line 1057
    :cond_b
    iget-object v4, p0, Lcom/google/android/music/store/OldMusicFile50;->mLocalCopyPath:Ljava/lang/String;

    invoke-virtual {p1, v10, v4}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    goto/16 :goto_4

    .line 1063
    :cond_c
    iget-object v4, p0, Lcom/google/android/music/store/OldMusicFile50;->mTitle:Ljava/lang/String;

    goto/16 :goto_5

    .line 1064
    :cond_d
    iget-object v4, p0, Lcom/google/android/music/store/OldMusicFile50;->mAlbumName:Ljava/lang/String;

    goto/16 :goto_6

    .line 1065
    :cond_e
    iget-object v4, p0, Lcom/google/android/music/store/OldMusicFile50;->mTrackArtist:Ljava/lang/String;

    goto/16 :goto_7

    .line 1069
    :cond_f
    iget-object v4, p0, Lcom/google/android/music/store/OldMusicFile50;->mAlbumArtist:Ljava/lang/String;

    goto/16 :goto_8

    .line 1072
    :cond_10
    iget-object v4, p0, Lcom/google/android/music/store/OldMusicFile50;->mComposer:Ljava/lang/String;

    goto/16 :goto_9

    .line 1073
    :cond_11
    iget-object v4, p0, Lcom/google/android/music/store/OldMusicFile50;->mGenre:Ljava/lang/String;

    goto/16 :goto_a

    .line 1080
    :cond_12
    const-wide/16 v4, 0x0

    goto/16 :goto_b

    .line 1085
    :cond_13
    const/16 v4, 0x18

    iget-object v5, p0, Lcom/google/android/music/store/OldMusicFile50;->mAlbumArtLocation:Ljava/lang/String;

    invoke-virtual {p1, v4, v5}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    goto/16 :goto_c

    .line 1092
    :cond_14
    iget-object v4, p0, Lcom/google/android/music/store/OldMusicFile50;->mCanonicalTitle:Ljava/lang/String;

    goto/16 :goto_d

    .line 1094
    :cond_15
    iget-object v4, p0, Lcom/google/android/music/store/OldMusicFile50;->mCanonicalAlbum:Ljava/lang/String;

    goto/16 :goto_e

    .line 1096
    :cond_16
    iget-object v4, p0, Lcom/google/android/music/store/OldMusicFile50;->mCanonicalAlbumArtist:Ljava/lang/String;

    goto/16 :goto_f

    .line 1098
    :cond_17
    iget-object v4, p0, Lcom/google/android/music/store/OldMusicFile50;->mCanonicalTrackArtist:Ljava/lang/String;

    goto/16 :goto_10

    .line 1100
    :cond_18
    iget-object v4, p0, Lcom/google/android/music/store/OldMusicFile50;->mCanonicalGenre:Ljava/lang/String;

    goto/16 :goto_11

    .line 1106
    :cond_19
    const-wide/16 v4, 0x0

    goto :goto_12

    .line 1111
    :cond_1a
    const/16 v4, 0x2b

    invoke-virtual {p1, v4}, Landroid/database/sqlite/SQLiteStatement;->bindNull(I)V

    goto :goto_13

    .line 1116
    :cond_1b
    const/16 v4, 0x2c

    invoke-virtual {p1, v4}, Landroid/database/sqlite/SQLiteStatement;->bindNull(I)V

    goto :goto_14

    .line 1014
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private final setAlbumArtist(Ljava/lang/String;I)V
    .locals 4
    .param p1, "albumArtist"    # Ljava/lang/String;
    .param p2, "albumArtistOrigin"    # I

    .prologue
    const-wide/16 v2, 0x0

    .line 603
    iget v0, p0, Lcom/google/android/music/store/OldMusicFile50;->mAlbumArtistOrigin:I

    if-ne v0, p2, :cond_0

    iget-object v0, p0, Lcom/google/android/music/store/OldMusicFile50;->mAlbumArtist:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/google/android/music/store/OldMusicFile50;->stringChanged(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 604
    :cond_0
    iput-object p1, p0, Lcom/google/android/music/store/OldMusicFile50;->mAlbumArtist:Ljava/lang/String;

    .line 605
    iput-wide v2, p0, Lcom/google/android/music/store/OldMusicFile50;->mAlbumArtistId:J

    .line 606
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/store/OldMusicFile50;->mCanonicalAlbumArtist:Ljava/lang/String;

    .line 607
    iput-wide v2, p0, Lcom/google/android/music/store/OldMusicFile50;->mAlbumId:J

    .line 608
    iput-wide v2, p0, Lcom/google/android/music/store/OldMusicFile50;->mSongId:J

    .line 609
    iput p2, p0, Lcom/google/android/music/store/OldMusicFile50;->mAlbumArtistOrigin:I

    .line 611
    :cond_1
    return-void
.end method

.method private final setTrackArtist(Ljava/lang/String;I)V
    .locals 3
    .param p1, "trackArtist"    # Ljava/lang/String;
    .param p2, "trackArtistOrigin"    # I

    .prologue
    const/4 v2, 0x1

    .line 559
    iget v0, p0, Lcom/google/android/music/store/OldMusicFile50;->mTrackArtistOrigin:I

    if-ne v0, p2, :cond_0

    iget-object v0, p0, Lcom/google/android/music/store/OldMusicFile50;->mTrackArtist:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/google/android/music/store/OldMusicFile50;->stringChanged(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 560
    :cond_0
    iput-object p1, p0, Lcom/google/android/music/store/OldMusicFile50;->mTrackArtist:Ljava/lang/String;

    .line 561
    iput p2, p0, Lcom/google/android/music/store/OldMusicFile50;->mTrackArtistOrigin:I

    .line 562
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/store/OldMusicFile50;->mCanonicalTrackArtist:Ljava/lang/String;

    .line 563
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/music/store/OldMusicFile50;->mTrackArtistId:J

    .line 564
    iget v0, p0, Lcom/google/android/music/store/OldMusicFile50;->mAlbumArtistOrigin:I

    if-ne v0, v2, :cond_2

    .line 566
    iget v0, p0, Lcom/google/android/music/store/OldMusicFile50;->mTrackArtistOrigin:I

    if-ne v0, v2, :cond_1

    .line 567
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Both track artist and album artist can\'t be derived"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 570
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/store/OldMusicFile50;->mTrackArtist:Ljava/lang/String;

    invoke-direct {p0, v0, v2}, Lcom/google/android/music/store/OldMusicFile50;->setAlbumArtist(Ljava/lang/String;I)V

    .line 573
    :cond_2
    return-void
.end method

.method private static stringChanged(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p0, "oldValue"    # Ljava/lang/String;
    .param p1, "newValue"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 904
    if-nez p0, :cond_2

    .line 905
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_1

    .line 909
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 905
    goto :goto_0

    .line 906
    :cond_2
    if-nez p1, :cond_3

    .line 907
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    if-gtz v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 909
    :cond_3
    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method load(Landroid/database/sqlite/SQLiteDatabase;J)V
    .locals 10
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "localId"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/store/DataNotFoundException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 1845
    const-string v1, "MUSIC"

    sget-object v2, Lcom/google/android/music/store/OldMusicFile50;->FULL_PROJECTION:[Ljava/lang/String;

    const-string v3, "Id=?"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v0

    move-object v0, p1

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 1850
    .local v8, "c":Landroid/database/Cursor;
    if-eqz v8, :cond_0

    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1851
    invoke-virtual {p0, v8}, Lcom/google/android/music/store/OldMusicFile50;->populateFromFullProjectionCursor(Landroid/database/Cursor;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1856
    invoke-static {v8}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 1858
    return-void

    .line 1853
    :cond_0
    :try_start_1
    new-instance v0, Lcom/google/android/music/store/DataNotFoundException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MusicFile with id "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not found"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/music/store/DataNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1856
    :catchall_0
    move-exception v0

    invoke-static {v8}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method public populateFromFullProjectionCursor(Landroid/database/Cursor;)V
    .locals 8
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    const/16 v7, 0x17

    const/4 v6, 0x3

    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1634
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/music/store/OldMusicFile50;->mLocalId:J

    .line 1635
    const/16 v0, 0x25

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/store/OldMusicFile50;->mTrackType:I

    .line 1636
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/store/OldMusicFile50;->mSourceAccount:I

    .line 1637
    const/4 v0, 0x2

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/store/OldMusicFile50;->mSourceId:Ljava/lang/String;

    .line 1638
    invoke-interface {p1, v6}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1639
    iput-object v3, p0, Lcom/google/android/music/store/OldMusicFile50;->mSourceVersion:Ljava/lang/String;

    .line 1644
    :goto_0
    const/4 v0, 0x4

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/music/store/OldMusicFile50;->mSize:J

    .line 1645
    const/4 v0, 0x5

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/store/OldMusicFile50;->mFileType:I

    .line 1646
    const/4 v0, 0x6

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/music/store/OldMusicFile50;->mAddedTime:J

    .line 1647
    const/4 v0, 0x7

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/store/OldMusicFile50;->mLocalCopyPath:Ljava/lang/String;

    .line 1648
    const/16 v0, 0x8

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/store/OldMusicFile50;->mLocalCopyType:I

    .line 1649
    const/16 v0, 0x2d

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/store/OldMusicFile50;->mLocalCopyStorageType:I

    .line 1650
    const/16 v0, 0x24

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/store/OldMusicFile50;->mLocalCopyBitrate:I

    .line 1651
    const/16 v0, 0x9

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/store/OldMusicFile50;->mTitle:Ljava/lang/String;

    .line 1652
    const/16 v0, 0xa

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/store/OldMusicFile50;->mAlbumName:Ljava/lang/String;

    .line 1653
    const/16 v0, 0xb

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/store/OldMusicFile50;->mTrackArtist:Ljava/lang/String;

    .line 1654
    const/16 v0, 0x26

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/store/OldMusicFile50;->mTrackArtistOrigin:I

    .line 1655
    const/16 v0, 0xc

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/store/OldMusicFile50;->mAlbumArtist:Ljava/lang/String;

    .line 1656
    const/16 v0, 0x22

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/store/OldMusicFile50;->mAlbumArtistOrigin:I

    .line 1657
    const/16 v0, 0xd

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/store/OldMusicFile50;->mComposer:Ljava/lang/String;

    .line 1658
    const/16 v0, 0xe

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/store/OldMusicFile50;->mGenre:Ljava/lang/String;

    .line 1659
    const/16 v0, 0xf

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getShort(I)S

    move-result v0

    iput-short v0, p0, Lcom/google/android/music/store/OldMusicFile50;->mYear:S

    .line 1660
    const/16 v0, 0x10

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/music/store/OldMusicFile50;->mDurationInMilliSec:J

    .line 1661
    const/16 v0, 0x11

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getShort(I)S

    move-result v0

    iput-short v0, p0, Lcom/google/android/music/store/OldMusicFile50;->mTrackCountInAlbum:S

    .line 1662
    const/16 v0, 0x12

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getShort(I)S

    move-result v0

    iput-short v0, p0, Lcom/google/android/music/store/OldMusicFile50;->mTrackPositionInAlbum:S

    .line 1663
    const/16 v0, 0x13

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getShort(I)S

    move-result v0

    iput-short v0, p0, Lcom/google/android/music/store/OldMusicFile50;->mDiscCount:S

    .line 1664
    const/16 v0, 0x14

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getShort(I)S

    move-result v0

    iput-short v0, p0, Lcom/google/android/music/store/OldMusicFile50;->mDiscPosition:S

    .line 1665
    const/16 v0, 0x15

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/music/store/OldMusicFile50;->mCompilation:Z

    .line 1666
    const/16 v0, 0x16

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/store/OldMusicFile50;->mBitrate:I

    .line 1667
    invoke-interface {p1, v7}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1668
    iput-object v3, p0, Lcom/google/android/music/store/OldMusicFile50;->mAlbumArtLocation:Ljava/lang/String;

    .line 1672
    :goto_2
    iput-boolean v2, p0, Lcom/google/android/music/store/OldMusicFile50;->mAlbumArtLocationChanged:Z

    .line 1673
    const/16 v0, 0x18

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/music/store/OldMusicFile50;->mSongId:J

    .line 1674
    const/16 v0, 0x19

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/music/store/OldMusicFile50;->mAlbumId:J

    .line 1675
    const/16 v0, 0x1a

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/music/store/OldMusicFile50;->mAlbumArtistId:J

    .line 1676
    const/16 v0, 0x27

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/music/store/OldMusicFile50;->mTrackArtistId:J

    .line 1677
    const/16 v0, 0x1b

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/music/store/OldMusicFile50;->mGenreId:J

    .line 1678
    const/16 v0, 0x1c

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/store/OldMusicFile50;->mCanonicalTitle:Ljava/lang/String;

    .line 1679
    const/16 v0, 0x1d

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/store/OldMusicFile50;->mCanonicalAlbum:Ljava/lang/String;

    .line 1680
    const/16 v0, 0x1e

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/store/OldMusicFile50;->mCanonicalAlbumArtist:Ljava/lang/String;

    .line 1681
    const/16 v0, 0x28

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/store/OldMusicFile50;->mCanonicalTrackArtist:Ljava/lang/String;

    .line 1682
    const/16 v0, 0x1f

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/store/OldMusicFile50;->mCanonicalGenre:Ljava/lang/String;

    .line 1683
    const/16 v0, 0x20

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/store/OldMusicFile50;->mPlayCount:I

    .line 1684
    const/16 v0, 0x21

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/music/store/OldMusicFile50;->mLastPlayDate:J

    .line 1685
    const/16 v0, 0x23

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/music/store/OldMusicFile50;->mLocalCopySize:J

    .line 1686
    const/16 v0, 0x29

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/store/OldMusicFile50;->mRating:I

    .line 1687
    const/16 v0, 0x2a

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v0, v1, :cond_3

    :goto_3
    iput-boolean v1, p0, Lcom/google/android/music/store/OldMusicFile50;->mNeedsSync:Z

    .line 1688
    const/16 v0, 0x2b

    invoke-interface {p1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1689
    iput-object v3, p0, Lcom/google/android/music/store/OldMusicFile50;->mStoreSongId:Ljava/lang/String;

    .line 1693
    :goto_4
    const/16 v0, 0x2c

    invoke-interface {p1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1694
    iput-object v3, p0, Lcom/google/android/music/store/OldMusicFile50;->mStoreAlbumId:Ljava/lang/String;

    .line 1698
    :goto_5
    const/16 v0, 0x2e

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/store/OldMusicFile50;->mDomain:I

    .line 1699
    return-void

    .line 1641
    :cond_0
    invoke-interface {p1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/store/OldMusicFile50;->mSourceVersion:Ljava/lang/String;

    goto/16 :goto_0

    :cond_1
    move v0, v2

    .line 1665
    goto/16 :goto_1

    .line 1670
    :cond_2
    invoke-interface {p1, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/store/OldMusicFile50;->mAlbumArtLocation:Ljava/lang/String;

    goto/16 :goto_2

    :cond_3
    move v1, v2

    .line 1687
    goto :goto_3

    .line 1691
    :cond_4
    const/16 v0, 0x2b

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/store/OldMusicFile50;->mStoreSongId:Ljava/lang/String;

    goto :goto_4

    .line 1696
    :cond_5
    const/16 v0, 0x2c

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/store/OldMusicFile50;->mStoreAlbumId:Ljava/lang/String;

    goto :goto_5
.end method

.method public reset()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 914
    invoke-super {p0}, Lcom/google/android/music/store/Syncable;->reset()V

    .line 915
    iput-wide v2, p0, Lcom/google/android/music/store/OldMusicFile50;->mLocalId:J

    .line 916
    iput v0, p0, Lcom/google/android/music/store/OldMusicFile50;->mTrackType:I

    .line 917
    iput-wide v2, p0, Lcom/google/android/music/store/OldMusicFile50;->mAddedTime:J

    .line 918
    iput-wide v2, p0, Lcom/google/android/music/store/OldMusicFile50;->mSize:J

    .line 919
    iput v0, p0, Lcom/google/android/music/store/OldMusicFile50;->mFileType:I

    .line 920
    iput-wide v2, p0, Lcom/google/android/music/store/OldMusicFile50;->mAddedTime:J

    .line 921
    iput-object v1, p0, Lcom/google/android/music/store/OldMusicFile50;->mLocalCopyPath:Ljava/lang/String;

    .line 922
    iput v0, p0, Lcom/google/android/music/store/OldMusicFile50;->mLocalCopyType:I

    .line 923
    iput-wide v2, p0, Lcom/google/android/music/store/OldMusicFile50;->mLocalCopySize:J

    .line 924
    iput v0, p0, Lcom/google/android/music/store/OldMusicFile50;->mLocalCopyBitrate:I

    .line 925
    iput v0, p0, Lcom/google/android/music/store/OldMusicFile50;->mLocalCopyStorageType:I

    .line 926
    iput v0, p0, Lcom/google/android/music/store/OldMusicFile50;->mPlayCount:I

    .line 927
    iput-wide v2, p0, Lcom/google/android/music/store/OldMusicFile50;->mLastPlayDate:J

    .line 928
    iput-object v1, p0, Lcom/google/android/music/store/OldMusicFile50;->mTitle:Ljava/lang/String;

    .line 929
    iput-object v1, p0, Lcom/google/android/music/store/OldMusicFile50;->mTrackArtist:Ljava/lang/String;

    .line 930
    iput v0, p0, Lcom/google/android/music/store/OldMusicFile50;->mTrackArtistOrigin:I

    .line 931
    iput-object v1, p0, Lcom/google/android/music/store/OldMusicFile50;->mAlbumArtist:Ljava/lang/String;

    .line 932
    iput v0, p0, Lcom/google/android/music/store/OldMusicFile50;->mAlbumArtistOrigin:I

    .line 933
    iput-object v1, p0, Lcom/google/android/music/store/OldMusicFile50;->mAlbumName:Ljava/lang/String;

    .line 934
    iput-object v1, p0, Lcom/google/android/music/store/OldMusicFile50;->mComposer:Ljava/lang/String;

    .line 935
    iput-object v1, p0, Lcom/google/android/music/store/OldMusicFile50;->mGenre:Ljava/lang/String;

    .line 936
    iput-short v0, p0, Lcom/google/android/music/store/OldMusicFile50;->mYear:S

    .line 937
    iput-short v0, p0, Lcom/google/android/music/store/OldMusicFile50;->mTrackCountInAlbum:S

    .line 938
    iput-short v0, p0, Lcom/google/android/music/store/OldMusicFile50;->mTrackPositionInAlbum:S

    .line 939
    iput-short v0, p0, Lcom/google/android/music/store/OldMusicFile50;->mDiscCount:S

    .line 940
    iput-short v0, p0, Lcom/google/android/music/store/OldMusicFile50;->mDiscPosition:S

    .line 941
    iput-boolean v0, p0, Lcom/google/android/music/store/OldMusicFile50;->mCompilation:Z

    .line 942
    iput v0, p0, Lcom/google/android/music/store/OldMusicFile50;->mBitrate:I

    .line 943
    iput-wide v2, p0, Lcom/google/android/music/store/OldMusicFile50;->mDurationInMilliSec:J

    .line 944
    iput-object v1, p0, Lcom/google/android/music/store/OldMusicFile50;->mAlbumArtLocation:Ljava/lang/String;

    .line 945
    iput-boolean v0, p0, Lcom/google/android/music/store/OldMusicFile50;->mAlbumArtLocationChanged:Z

    .line 946
    iput v0, p0, Lcom/google/android/music/store/OldMusicFile50;->mRating:I

    .line 947
    iput-object v1, p0, Lcom/google/android/music/store/OldMusicFile50;->mStoreSongId:Ljava/lang/String;

    .line 948
    iput-object v1, p0, Lcom/google/android/music/store/OldMusicFile50;->mStoreAlbumId:Ljava/lang/String;

    .line 949
    iput v0, p0, Lcom/google/android/music/store/OldMusicFile50;->mDomain:I

    .line 950
    invoke-virtual {p0}, Lcom/google/android/music/store/OldMusicFile50;->resetDerivedFields()V

    .line 951
    return-void
.end method

.method public resetDerivedFields()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v0, 0x0

    .line 959
    iput-wide v2, p0, Lcom/google/android/music/store/OldMusicFile50;->mSongId:J

    .line 960
    iput-wide v2, p0, Lcom/google/android/music/store/OldMusicFile50;->mAlbumArtistId:J

    .line 961
    iput-wide v2, p0, Lcom/google/android/music/store/OldMusicFile50;->mTrackArtistId:J

    .line 962
    iput-wide v2, p0, Lcom/google/android/music/store/OldMusicFile50;->mAlbumId:J

    .line 963
    iput-wide v2, p0, Lcom/google/android/music/store/OldMusicFile50;->mGenreId:J

    .line 964
    iput-object v0, p0, Lcom/google/android/music/store/OldMusicFile50;->mCanonicalTitle:Ljava/lang/String;

    .line 965
    iput-object v0, p0, Lcom/google/android/music/store/OldMusicFile50;->mCanonicalAlbum:Ljava/lang/String;

    .line 966
    iput-object v0, p0, Lcom/google/android/music/store/OldMusicFile50;->mCanonicalAlbumArtist:Ljava/lang/String;

    .line 967
    iput-object v0, p0, Lcom/google/android/music/store/OldMusicFile50;->mCanonicalTrackArtist:Ljava/lang/String;

    .line 968
    iput-object v0, p0, Lcom/google/android/music/store/OldMusicFile50;->mCanonicalGenre:Ljava/lang/String;

    .line 969
    return-void
.end method

.method setDerivedFields()V
    .locals 15

    .prologue
    const/4 v8, 0x0

    const/4 v14, 0x0

    const/16 v11, 0x1f

    const/4 v7, 0x1

    const-wide/16 v12, 0x0

    .line 1244
    sget-object v9, Lcom/google/android/music/store/Schema$Music;->EMPTY_CANONICAL_SORT_KEY:Ljava/lang/String;

    iget-object v10, p0, Lcom/google/android/music/store/OldMusicFile50;->mCanonicalAlbum:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 1245
    iput-object v14, p0, Lcom/google/android/music/store/OldMusicFile50;->mCanonicalAlbum:Ljava/lang/String;

    .line 1247
    :cond_0
    sget-object v9, Lcom/google/android/music/store/Schema$Music;->EMPTY_CANONICAL_SORT_KEY:Ljava/lang/String;

    iget-object v10, p0, Lcom/google/android/music/store/OldMusicFile50;->mCanonicalAlbumArtist:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 1248
    iput-object v14, p0, Lcom/google/android/music/store/OldMusicFile50;->mCanonicalAlbumArtist:Ljava/lang/String;

    .line 1250
    :cond_1
    sget-object v9, Lcom/google/android/music/store/Schema$Music;->EMPTY_CANONICAL_SORT_KEY:Ljava/lang/String;

    iget-object v10, p0, Lcom/google/android/music/store/OldMusicFile50;->mCanonicalTrackArtist:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 1251
    iput-object v14, p0, Lcom/google/android/music/store/OldMusicFile50;->mCanonicalTrackArtist:Ljava/lang/String;

    .line 1254
    :cond_2
    iget-object v9, p0, Lcom/google/android/music/store/OldMusicFile50;->mNormalizer:Lcom/google/android/music/store/TagNormalizer;

    if-nez v9, :cond_3

    .line 1255
    new-instance v9, Lcom/google/android/music/store/TagNormalizer;

    invoke-direct {v9}, Lcom/google/android/music/store/TagNormalizer;-><init>()V

    iput-object v9, p0, Lcom/google/android/music/store/OldMusicFile50;->mNormalizer:Lcom/google/android/music/store/TagNormalizer;

    .line 1257
    :cond_3
    iget-object v9, p0, Lcom/google/android/music/store/OldMusicFile50;->mAlbumArtist:Ljava/lang/String;

    if-eqz v9, :cond_8

    iget-object v9, p0, Lcom/google/android/music/store/OldMusicFile50;->mAlbumArtist:Ljava/lang/String;

    invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    if-lez v9, :cond_8

    move v2, v7

    .line 1258
    .local v2, "hasAlbumArtist":Z
    :goto_0
    iget-object v9, p0, Lcom/google/android/music/store/OldMusicFile50;->mTrackArtist:Ljava/lang/String;

    if-eqz v9, :cond_9

    iget-object v9, p0, Lcom/google/android/music/store/OldMusicFile50;->mTrackArtist:Ljava/lang/String;

    invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    if-lez v9, :cond_9

    move v3, v7

    .line 1259
    .local v3, "hasTrackArtist":Z
    :goto_1
    if-nez v2, :cond_a

    if-eqz v3, :cond_a

    .line 1260
    iget-object v8, p0, Lcom/google/android/music/store/OldMusicFile50;->mTrackArtist:Ljava/lang/String;

    invoke-direct {p0, v8, v7}, Lcom/google/android/music/store/OldMusicFile50;->setAlbumArtist(Ljava/lang/String;I)V

    .line 1265
    :cond_4
    :goto_2
    iget-wide v8, p0, Lcom/google/android/music/store/OldMusicFile50;->mGenreId:J

    cmp-long v8, v8, v12

    if-nez v8, :cond_6

    .line 1266
    iget-object v8, p0, Lcom/google/android/music/store/OldMusicFile50;->mNormalizer:Lcom/google/android/music/store/TagNormalizer;

    iget-object v9, p0, Lcom/google/android/music/store/OldMusicFile50;->mGenre:Ljava/lang/String;

    invoke-virtual {v8, v9}, Lcom/google/android/music/store/TagNormalizer;->normalize(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/music/store/OldMusicFile50;->mCanonicalGenre:Ljava/lang/String;

    .line 1267
    iget-object v8, p0, Lcom/google/android/music/store/OldMusicFile50;->mCanonicalGenre:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_5

    iget-object v8, p0, Lcom/google/android/music/store/OldMusicFile50;->mGenre:Ljava/lang/String;

    if-eqz v8, :cond_5

    .line 1268
    iget-object v8, p0, Lcom/google/android/music/store/OldMusicFile50;->mGenre:Ljava/lang/String;

    iput-object v8, p0, Lcom/google/android/music/store/OldMusicFile50;->mCanonicalGenre:Ljava/lang/String;

    .line 1270
    :cond_5
    iget-object v8, p0, Lcom/google/android/music/store/OldMusicFile50;->mCanonicalGenre:Ljava/lang/String;

    invoke-static {v8}, Lcom/google/android/music/store/Store;->generateId(Ljava/lang/String;)J

    move-result-wide v8

    iput-wide v8, p0, Lcom/google/android/music/store/OldMusicFile50;->mGenreId:J

    .line 1273
    :cond_6
    iget-object v8, p0, Lcom/google/android/music/store/OldMusicFile50;->mCanonicalTitle:Ljava/lang/String;

    if-nez v8, :cond_c

    .line 1274
    iget-object v8, p0, Lcom/google/android/music/store/OldMusicFile50;->mTitle:Ljava/lang/String;

    if-eqz v8, :cond_7

    iget-object v8, p0, Lcom/google/android/music/store/OldMusicFile50;->mTitle:Ljava/lang/String;

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    if-nez v8, :cond_b

    .line 1275
    :cond_7
    new-instance v7, Lcom/google/android/music/store/InvalidDataException;

    const-string v8, "Song title must not be empty"

    invoke-direct {v7, v8}, Lcom/google/android/music/store/InvalidDataException;-><init>(Ljava/lang/String;)V

    throw v7

    .end local v2    # "hasAlbumArtist":Z
    .end local v3    # "hasTrackArtist":Z
    :cond_8
    move v2, v8

    .line 1257
    goto :goto_0

    .restart local v2    # "hasAlbumArtist":Z
    :cond_9
    move v3, v8

    .line 1258
    goto :goto_1

    .line 1261
    .restart local v3    # "hasTrackArtist":Z
    :cond_a
    if-nez v3, :cond_4

    if-eqz v2, :cond_4

    .line 1262
    iget-object v8, p0, Lcom/google/android/music/store/OldMusicFile50;->mAlbumArtist:Ljava/lang/String;

    invoke-direct {p0, v8, v7}, Lcom/google/android/music/store/OldMusicFile50;->setTrackArtist(Ljava/lang/String;I)V

    goto :goto_2

    .line 1278
    :cond_b
    iget-object v8, p0, Lcom/google/android/music/store/OldMusicFile50;->mNormalizer:Lcom/google/android/music/store/TagNormalizer;

    iget-object v9, p0, Lcom/google/android/music/store/OldMusicFile50;->mTitle:Ljava/lang/String;

    invoke-virtual {v8, v9}, Lcom/google/android/music/store/TagNormalizer;->normalize(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/music/store/OldMusicFile50;->mCanonicalTitle:Ljava/lang/String;

    .line 1279
    iget-object v8, p0, Lcom/google/android/music/store/OldMusicFile50;->mCanonicalTitle:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_c

    iget-object v8, p0, Lcom/google/android/music/store/OldMusicFile50;->mTitle:Ljava/lang/String;

    if-eqz v8, :cond_c

    .line 1280
    iget-object v8, p0, Lcom/google/android/music/store/OldMusicFile50;->mTitle:Ljava/lang/String;

    iput-object v8, p0, Lcom/google/android/music/store/OldMusicFile50;->mCanonicalTitle:Ljava/lang/String;

    .line 1284
    :cond_c
    iget-object v8, p0, Lcom/google/android/music/store/OldMusicFile50;->mCanonicalAlbum:Ljava/lang/String;

    if-nez v8, :cond_d

    .line 1285
    iget-object v8, p0, Lcom/google/android/music/store/OldMusicFile50;->mNormalizer:Lcom/google/android/music/store/TagNormalizer;

    iget-object v9, p0, Lcom/google/android/music/store/OldMusicFile50;->mAlbumName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Lcom/google/android/music/store/TagNormalizer;->normalize(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/music/store/OldMusicFile50;->mCanonicalAlbum:Ljava/lang/String;

    .line 1286
    iget-object v8, p0, Lcom/google/android/music/store/OldMusicFile50;->mCanonicalAlbum:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_d

    iget-object v8, p0, Lcom/google/android/music/store/OldMusicFile50;->mAlbumName:Ljava/lang/String;

    if-eqz v8, :cond_d

    .line 1287
    iget-object v8, p0, Lcom/google/android/music/store/OldMusicFile50;->mAlbumName:Ljava/lang/String;

    iput-object v8, p0, Lcom/google/android/music/store/OldMusicFile50;->mCanonicalAlbum:Ljava/lang/String;

    .line 1291
    :cond_d
    iget-object v8, p0, Lcom/google/android/music/store/OldMusicFile50;->mCanonicalAlbumArtist:Ljava/lang/String;

    if-eqz v8, :cond_e

    iget-object v8, p0, Lcom/google/android/music/store/OldMusicFile50;->mCanonicalTrackArtist:Ljava/lang/String;

    if-eqz v8, :cond_e

    iget-wide v8, p0, Lcom/google/android/music/store/OldMusicFile50;->mAlbumArtistId:J

    cmp-long v8, v8, v12

    if-eqz v8, :cond_e

    iget-wide v8, p0, Lcom/google/android/music/store/OldMusicFile50;->mTrackArtistId:J

    cmp-long v8, v8, v12

    if-nez v8, :cond_11

    .line 1294
    :cond_e
    iget-object v8, p0, Lcom/google/android/music/store/OldMusicFile50;->mNormalizer:Lcom/google/android/music/store/TagNormalizer;

    iget-object v9, p0, Lcom/google/android/music/store/OldMusicFile50;->mAlbumArtist:Ljava/lang/String;

    invoke-virtual {v8, v9}, Lcom/google/android/music/store/TagNormalizer;->normalize(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/music/store/OldMusicFile50;->mCanonicalAlbumArtist:Ljava/lang/String;

    .line 1295
    iget-object v8, p0, Lcom/google/android/music/store/OldMusicFile50;->mCanonicalAlbumArtist:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_f

    iget-object v8, p0, Lcom/google/android/music/store/OldMusicFile50;->mAlbumArtist:Ljava/lang/String;

    if-eqz v8, :cond_f

    .line 1296
    iget-object v8, p0, Lcom/google/android/music/store/OldMusicFile50;->mAlbumArtist:Ljava/lang/String;

    iput-object v8, p0, Lcom/google/android/music/store/OldMusicFile50;->mCanonicalAlbumArtist:Ljava/lang/String;

    .line 1298
    :cond_f
    iget-object v8, p0, Lcom/google/android/music/store/OldMusicFile50;->mCanonicalAlbumArtist:Ljava/lang/String;

    invoke-static {v8}, Lcom/google/android/music/store/Store;->generateId(Ljava/lang/String;)J

    move-result-wide v8

    iput-wide v8, p0, Lcom/google/android/music/store/OldMusicFile50;->mAlbumArtistId:J

    .line 1300
    iget v8, p0, Lcom/google/android/music/store/OldMusicFile50;->mAlbumArtistOrigin:I

    if-eq v8, v7, :cond_10

    iget v8, p0, Lcom/google/android/music/store/OldMusicFile50;->mTrackArtistOrigin:I

    if-eq v8, v7, :cond_10

    iget-object v7, p0, Lcom/google/android/music/store/OldMusicFile50;->mTrackArtist:Ljava/lang/String;

    iget-object v8, p0, Lcom/google/android/music/store/OldMusicFile50;->mAlbumArtist:Ljava/lang/String;

    invoke-static {v7, v8}, Lcom/google/android/music/store/OldMusicFile50;->stringChanged(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_1d

    .line 1303
    :cond_10
    iget-object v7, p0, Lcom/google/android/music/store/OldMusicFile50;->mCanonicalAlbumArtist:Ljava/lang/String;

    iput-object v7, p0, Lcom/google/android/music/store/OldMusicFile50;->mCanonicalTrackArtist:Ljava/lang/String;

    .line 1304
    iget-wide v8, p0, Lcom/google/android/music/store/OldMusicFile50;->mAlbumArtistId:J

    iput-wide v8, p0, Lcom/google/android/music/store/OldMusicFile50;->mTrackArtistId:J

    .line 1314
    :cond_11
    :goto_3
    iget-wide v8, p0, Lcom/google/android/music/store/OldMusicFile50;->mSongId:J

    cmp-long v7, v8, v12

    if-eqz v7, :cond_12

    iget-wide v8, p0, Lcom/google/android/music/store/OldMusicFile50;->mAlbumId:J

    cmp-long v7, v8, v12

    if-nez v7, :cond_19

    .line 1315
    :cond_12
    const/16 v5, 0x1f

    .line 1316
    .local v5, "separator":C
    iget-object v7, p0, Lcom/google/android/music/store/OldMusicFile50;->mCanonicalAlbum:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    .line 1317
    .local v4, "isUnknownAlbum":Z
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 1318
    .local v1, "builder":Ljava/lang/StringBuilder;
    iget-object v7, p0, Lcom/google/android/music/store/OldMusicFile50;->mCanonicalAlbum:Ljava/lang/String;

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1319
    const/4 v0, 0x0

    .line 1320
    .local v0, "albumIdHasArtist":Z
    iget v7, p0, Lcom/google/android/music/store/OldMusicFile50;->mAlbumArtistOrigin:I

    if-eqz v7, :cond_13

    iget v7, p0, Lcom/google/android/music/store/OldMusicFile50;->mAlbumArtistOrigin:I

    const/4 v8, 0x2

    if-ne v7, v8, :cond_1f

    :cond_13
    if-eqz v4, :cond_14

    iget-object v7, p0, Lcom/google/android/music/store/OldMusicFile50;->mCanonicalAlbumArtist:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1f

    .line 1326
    :cond_14
    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/music/store/OldMusicFile50;->mCanonicalAlbumArtist:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1327
    const/4 v0, 0x1

    .line 1335
    :cond_15
    :goto_4
    iget-wide v8, p0, Lcom/google/android/music/store/OldMusicFile50;->mAlbumId:J

    cmp-long v7, v8, v12

    if-nez v7, :cond_16

    .line 1336
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/music/store/Store;->generateId(Ljava/lang/String;)J

    move-result-wide v8

    iput-wide v8, p0, Lcom/google/android/music/store/OldMusicFile50;->mAlbumId:J

    .line 1339
    :cond_16
    iget-wide v8, p0, Lcom/google/android/music/store/OldMusicFile50;->mSongId:J

    cmp-long v7, v8, v12

    if-nez v7, :cond_19

    .line 1340
    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/music/store/OldMusicFile50;->mCanonicalTitle:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1342
    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-short v8, p0, Lcom/google/android/music/store/OldMusicFile50;->mDiscPosition:S

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-short v8, p0, Lcom/google/android/music/store/OldMusicFile50;->mTrackPositionInAlbum:S

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1344
    iget-object v7, p0, Lcom/google/android/music/store/OldMusicFile50;->mCanonicalTrackArtist:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    if-lez v7, :cond_18

    if-eqz v0, :cond_17

    iget-object v7, p0, Lcom/google/android/music/store/OldMusicFile50;->mCanonicalTrackArtist:Ljava/lang/String;

    iget-object v8, p0, Lcom/google/android/music/store/OldMusicFile50;->mCanonicalAlbumArtist:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_18

    .line 1347
    :cond_17
    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/music/store/OldMusicFile50;->mCanonicalTrackArtist:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1349
    :cond_18
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 1350
    .local v6, "songKey":Ljava/lang/String;
    invoke-static {v6}, Lcom/google/android/music/store/Store;->generateId(Ljava/lang/String;)J

    move-result-wide v8

    iput-wide v8, p0, Lcom/google/android/music/store/OldMusicFile50;->mSongId:J

    .line 1358
    .end local v0    # "albumIdHasArtist":Z
    .end local v1    # "builder":Ljava/lang/StringBuilder;
    .end local v4    # "isUnknownAlbum":Z
    .end local v5    # "separator":C
    .end local v6    # "songKey":Ljava/lang/String;
    :cond_19
    iget-object v7, p0, Lcom/google/android/music/store/OldMusicFile50;->mCanonicalAlbum:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_1a

    .line 1359
    sget-object v7, Lcom/google/android/music/store/Schema$Music;->EMPTY_CANONICAL_SORT_KEY:Ljava/lang/String;

    iput-object v7, p0, Lcom/google/android/music/store/OldMusicFile50;->mCanonicalAlbum:Ljava/lang/String;

    .line 1361
    :cond_1a
    iget-object v7, p0, Lcom/google/android/music/store/OldMusicFile50;->mCanonicalAlbumArtist:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_1b

    .line 1362
    sget-object v7, Lcom/google/android/music/store/Schema$Music;->EMPTY_CANONICAL_SORT_KEY:Ljava/lang/String;

    iput-object v7, p0, Lcom/google/android/music/store/OldMusicFile50;->mCanonicalAlbumArtist:Ljava/lang/String;

    .line 1364
    :cond_1b
    iget-object v7, p0, Lcom/google/android/music/store/OldMusicFile50;->mCanonicalTrackArtist:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_1c

    .line 1365
    sget-object v7, Lcom/google/android/music/store/Schema$Music;->EMPTY_CANONICAL_SORT_KEY:Ljava/lang/String;

    iput-object v7, p0, Lcom/google/android/music/store/OldMusicFile50;->mCanonicalTrackArtist:Ljava/lang/String;

    .line 1367
    :cond_1c
    return-void

    .line 1306
    :cond_1d
    iget-object v7, p0, Lcom/google/android/music/store/OldMusicFile50;->mNormalizer:Lcom/google/android/music/store/TagNormalizer;

    iget-object v8, p0, Lcom/google/android/music/store/OldMusicFile50;->mTrackArtist:Ljava/lang/String;

    invoke-virtual {v7, v8}, Lcom/google/android/music/store/TagNormalizer;->normalize(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/google/android/music/store/OldMusicFile50;->mCanonicalTrackArtist:Ljava/lang/String;

    .line 1307
    iget-object v7, p0, Lcom/google/android/music/store/OldMusicFile50;->mCanonicalTrackArtist:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_1e

    iget-object v7, p0, Lcom/google/android/music/store/OldMusicFile50;->mTrackArtist:Ljava/lang/String;

    if-eqz v7, :cond_1e

    .line 1308
    iget-object v7, p0, Lcom/google/android/music/store/OldMusicFile50;->mTrackArtist:Ljava/lang/String;

    iput-object v7, p0, Lcom/google/android/music/store/OldMusicFile50;->mCanonicalTrackArtist:Ljava/lang/String;

    .line 1310
    :cond_1e
    iget-object v7, p0, Lcom/google/android/music/store/OldMusicFile50;->mCanonicalTrackArtist:Ljava/lang/String;

    invoke-static {v7}, Lcom/google/android/music/store/Store;->generateId(Ljava/lang/String;)J

    move-result-wide v8

    iput-wide v8, p0, Lcom/google/android/music/store/OldMusicFile50;->mTrackArtistId:J

    goto/16 :goto_3

    .line 1328
    .restart local v0    # "albumIdHasArtist":Z
    .restart local v1    # "builder":Ljava/lang/StringBuilder;
    .restart local v4    # "isUnknownAlbum":Z
    .restart local v5    # "separator":C
    :cond_1f
    if-eqz v4, :cond_15

    iget-object v7, p0, Lcom/google/android/music/store/OldMusicFile50;->mCanonicalTrackArtist:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_15

    .line 1331
    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/music/store/OldMusicFile50;->mCanonicalTrackArtist:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1332
    const/4 v0, 0x1

    goto/16 :goto_4
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 406
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 407
    .local v0, "buffer":Ljava/lang/StringBuilder;
    const-string v1, "id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/music/store/OldMusicFile50;->mLocalId:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 408
    const-string v1, "account="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/music/store/OldMusicFile50;->mSourceAccount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 409
    const-string v1, "sourceId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/store/OldMusicFile50;->mSourceId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 410
    const-string v1, "addedTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/music/store/OldMusicFile50;->mAddedTime:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 411
    const-string v1, "size="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/music/store/OldMusicFile50;->mSize:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 412
    const-string v1, "fileType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/music/store/OldMusicFile50;->mFileType:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 413
    const-string v1, "localPath="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/store/OldMusicFile50;->mLocalCopyPath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 414
    const-string v1, "localCopyType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/music/store/OldMusicFile50;->mLocalCopyType:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 415
    const-string v1, "localCopyStorageType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/music/store/OldMusicFile50;->mLocalCopyStorageType:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 416
    const-string v1, "title="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/store/OldMusicFile50;->mTitle:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 417
    const-string v1, "artist="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/store/OldMusicFile50;->mTrackArtist:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 418
    const-string v1, "artistOrigin="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/music/store/OldMusicFile50;->mTrackArtistOrigin:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 419
    const-string v1, "albumArtist="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/store/OldMusicFile50;->mAlbumArtist:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 420
    const-string v1, "albumArtistOrigin="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/music/store/OldMusicFile50;->mAlbumArtistOrigin:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 421
    const-string v1, "album="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/store/OldMusicFile50;->mAlbumName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 422
    const-string v1, "composer="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/store/OldMusicFile50;->mComposer:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 423
    const-string v1, "genre="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/store/OldMusicFile50;->mGenre:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 424
    const-string v1, "position="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-short v2, p0, Lcom/google/android/music/store/OldMusicFile50;->mTrackPositionInAlbum:S

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 425
    const-string v1, "year="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-short v2, p0, Lcom/google/android/music/store/OldMusicFile50;->mYear:S

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 426
    const-string v1, "artLocation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/store/OldMusicFile50;->mAlbumArtLocation:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 427
    const-string v1, "rating="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/music/store/OldMusicFile50;->mRating:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 428
    iget-object v1, p0, Lcom/google/android/music/store/OldMusicFile50;->mStoreSongId:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 429
    const-string v1, "storeSongId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/store/OldMusicFile50;->mStoreSongId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 431
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/store/OldMusicFile50;->mStoreAlbumId:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 432
    const-string v1, "storeAlbumId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/store/OldMusicFile50;->mStoreAlbumId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 434
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public final updateMusicFile(Landroid/database/sqlite/SQLiteStatement;Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 6
    .param p1, "fullUpdateStatement"    # Landroid/database/sqlite/SQLiteStatement;
    .param p2, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 1198
    iget-wide v0, p0, Lcom/google/android/music/store/OldMusicFile50;->mLocalId:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 1199
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Music file ID must be known in order to update db record"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1203
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/music/store/OldMusicFile50;->prepareInsertOrFullUpdate(Landroid/database/sqlite/SQLiteStatement;)V

    .line 1205
    const/16 v0, 0x2f

    iget-wide v2, p0, Lcom/google/android/music/store/OldMusicFile50;->mLocalId:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 1207
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteStatement;->execute()V

    .line 1209
    iget-boolean v0, p0, Lcom/google/android/music/store/OldMusicFile50;->mAlbumArtLocationChanged:Z

    if-eqz v0, :cond_1

    .line 1210
    const-string v0, "ARTWORK"

    const-string v1, "AlbumId=?"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-wide v4, p0, Lcom/google/android/music/store/OldMusicFile50;->mAlbumId:J

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p2, v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1213
    :cond_1
    return-void
.end method
