.class public Lcom/google/android/music/ui/SDCardPinNotAllowedDialogFragment;
.super Landroid/support/v4/app/DialogFragment;
.source "SDCardPinNotAllowedDialogFragment.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 17
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/music/ui/SDCardPinNotAllowedDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 18
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    const v2, 0x7f0b02bc

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 19
    const v2, 0x7f0b0216

    new-instance v3, Lcom/google/android/music/ui/SDCardPinNotAllowedDialogFragment$1;

    invoke-direct {v3, p0}, Lcom/google/android/music/ui/SDCardPinNotAllowedDialogFragment$1;-><init>(Lcom/google/android/music/ui/SDCardPinNotAllowedDialogFragment;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 25
    const v2, 0x7f0b02bd

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 26
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 27
    .local v1, "dialog":Landroid/app/Dialog;
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 28
    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    .line 29
    return-object v1
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 1
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 34
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 35
    invoke-virtual {p0}, Lcom/google/android/music/ui/SDCardPinNotAllowedDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 36
    .local v0, "activity":Landroid/app/Activity;
    if-eqz v0, :cond_0

    .line 37
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 39
    :cond_0
    return-void
.end method
