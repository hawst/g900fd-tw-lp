.class public Lcom/google/android/music/cloudclient/ExploreEntityGroupJson;
.super Lcom/google/api/client/json/GenericJson;
.source "ExploreEntityGroupJson.java"


# instance fields
.field public mDescription:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "description"
    .end annotation
.end field

.field public mEntities:Ljava/util/List;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "entities"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/cloudclient/ExploreEntityJson;",
            ">;"
        }
    .end annotation
.end field

.field public mId:J

.field public mTitle:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "title"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    return-void
.end method
