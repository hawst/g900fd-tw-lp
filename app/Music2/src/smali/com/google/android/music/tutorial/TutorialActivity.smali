.class abstract Lcom/google/android/music/tutorial/TutorialActivity;
.super Landroid/support/v4/app/FragmentActivity;
.source "TutorialActivity.java"


# instance fields
.field private mIsDestroyed:Z

.field private mPrefs:Lcom/google/android/music/preferences/MusicPreferences;

.field protected mTracker:Lcom/google/android/music/eventlog/MusicEventLogger;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Landroid/support/v4/app/FragmentActivity;-><init>()V

    .line 29
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/tutorial/TutorialActivity;->mIsDestroyed:Z

    return-void
.end method


# virtual methods
.method protected getPrefs()Lcom/google/android/music/preferences/MusicPreferences;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/music/tutorial/TutorialActivity;->mPrefs:Lcom/google/android/music/preferences/MusicPreferences;

    return-object v0
.end method

.method public isActivityDestroyed()Z
    .locals 1

    .prologue
    .line 56
    iget-boolean v0, p0, Lcom/google/android/music/tutorial/TutorialActivity;->mIsDestroyed:Z

    return v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 39
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 40
    const v0, 0x7f050007

    const v1, 0x7f050008

    invoke-virtual {p0, v0, v1}, Lcom/google/android/music/tutorial/TutorialActivity;->overridePendingTransition(II)V

    .line 41
    invoke-static {p0}, Lcom/google/android/music/eventlog/MusicEventLogger;->getInstance(Landroid/content/Context;)Lcom/google/android/music/eventlog/MusicEventLogger;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/tutorial/TutorialActivity;->mTracker:Lcom/google/android/music/eventlog/MusicEventLogger;

    .line 42
    invoke-static {p0, p0}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/tutorial/TutorialActivity;->mPrefs:Lcom/google/android/music/preferences/MusicPreferences;

    .line 44
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/music/tutorial/TutorialActivity;->requestWindowFeature(I)Z

    .line 45
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 49
    invoke-static {p0}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    .line 50
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/tutorial/TutorialActivity;->mPrefs:Lcom/google/android/music/preferences/MusicPreferences;

    .line 51
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/tutorial/TutorialActivity;->mIsDestroyed:Z

    .line 52
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onDestroy()V

    .line 53
    return-void
.end method
