.class Lcom/google/android/music/preferences/MusicPreferences$5;
.super Ljava/lang/Object;
.source "MusicPreferences.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/preferences/MusicPreferences;->setStreamOnlyOnWifi(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/preferences/MusicPreferences;

.field final synthetic val$streamOnlyOnWifi:Z


# direct methods
.method constructor <init>(Lcom/google/android/music/preferences/MusicPreferences;Z)V
    .locals 0

    .prologue
    .line 1158
    iput-object p1, p0, Lcom/google/android/music/preferences/MusicPreferences$5;->this$0:Lcom/google/android/music/preferences/MusicPreferences;

    iput-boolean p2, p0, Lcom/google/android/music/preferences/MusicPreferences$5;->val$streamOnlyOnWifi:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 1160
    iget-object v1, p0, Lcom/google/android/music/preferences/MusicPreferences$5;->this$0:Lcom/google/android/music/preferences/MusicPreferences;

    # getter for: Lcom/google/android/music/preferences/MusicPreferences;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/google/android/music/preferences/MusicPreferences;->access$300(Lcom/google/android/music/preferences/MusicPreferences;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/google/android/music/store/MusicContent;->CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    .line 1163
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.music.STREAM_ONLY_ON_WIFI"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1164
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "stream_on_wifi_only"

    iget-boolean v2, p0, Lcom/google/android/music/preferences/MusicPreferences$5;->val$streamOnlyOnWifi:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1165
    iget-object v1, p0, Lcom/google/android/music/preferences/MusicPreferences$5;->this$0:Lcom/google/android/music/preferences/MusicPreferences;

    # getter for: Lcom/google/android/music/preferences/MusicPreferences;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/google/android/music/preferences/MusicPreferences;->access$300(Lcom/google/android/music/preferences/MusicPreferences;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 1166
    return-void
.end method
