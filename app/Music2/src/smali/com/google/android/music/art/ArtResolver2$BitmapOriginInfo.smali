.class Lcom/google/android/music/art/ArtResolver2$BitmapOriginInfo;
.super Ljava/lang/Object;
.source "ArtResolver2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/art/ArtResolver2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "BitmapOriginInfo"
.end annotation


# instance fields
.field public final artType:Lcom/google/android/music/art/ArtType;

.field public final artUrl:Lcom/google/android/music/art/ArtResolver2$ArtUrl;

.field public final sizeBucket:I

.field public final source:I


# direct methods
.method public constructor <init>(Lcom/google/android/music/art/ArtResolver2$ArtUrl;ILcom/google/android/music/art/ArtType;I)V
    .locals 0
    .param p1, "artUrl"    # Lcom/google/android/music/art/ArtResolver2$ArtUrl;
    .param p2, "source"    # I
    .param p3, "artType"    # Lcom/google/android/music/art/ArtType;
    .param p4, "sizeBucket"    # I

    .prologue
    .line 658
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 659
    iput-object p1, p0, Lcom/google/android/music/art/ArtResolver2$BitmapOriginInfo;->artUrl:Lcom/google/android/music/art/ArtResolver2$ArtUrl;

    .line 660
    iput p2, p0, Lcom/google/android/music/art/ArtResolver2$BitmapOriginInfo;->source:I

    .line 661
    iput-object p3, p0, Lcom/google/android/music/art/ArtResolver2$BitmapOriginInfo;->artType:Lcom/google/android/music/art/ArtType;

    .line 662
    iput p4, p0, Lcom/google/android/music/art/ArtResolver2$BitmapOriginInfo;->sizeBucket:I

    .line 663
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 667
    if-ne p0, p1, :cond_1

    .line 677
    :cond_0
    :goto_0
    return v1

    .line 668
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    :cond_2
    move v1, v2

    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 670
    check-cast v0, Lcom/google/android/music/art/ArtResolver2$BitmapOriginInfo;

    .line 672
    .local v0, "that":Lcom/google/android/music/art/ArtResolver2$BitmapOriginInfo;
    iget v3, p0, Lcom/google/android/music/art/ArtResolver2$BitmapOriginInfo;->sizeBucket:I

    iget v4, v0, Lcom/google/android/music/art/ArtResolver2$BitmapOriginInfo;->sizeBucket:I

    if-eq v3, v4, :cond_4

    move v1, v2

    goto :goto_0

    .line 673
    :cond_4
    iget v3, p0, Lcom/google/android/music/art/ArtResolver2$BitmapOriginInfo;->source:I

    iget v4, v0, Lcom/google/android/music/art/ArtResolver2$BitmapOriginInfo;->source:I

    if-eq v3, v4, :cond_5

    move v1, v2

    goto :goto_0

    .line 674
    :cond_5
    iget-object v3, p0, Lcom/google/android/music/art/ArtResolver2$BitmapOriginInfo;->artType:Lcom/google/android/music/art/ArtType;

    iget-object v4, v0, Lcom/google/android/music/art/ArtResolver2$BitmapOriginInfo;->artType:Lcom/google/android/music/art/ArtType;

    if-eq v3, v4, :cond_6

    move v1, v2

    goto :goto_0

    .line 675
    :cond_6
    iget-object v3, p0, Lcom/google/android/music/art/ArtResolver2$BitmapOriginInfo;->artUrl:Lcom/google/android/music/art/ArtResolver2$ArtUrl;

    iget-object v4, v0, Lcom/google/android/music/art/ArtResolver2$BitmapOriginInfo;->artUrl:Lcom/google/android/music/art/ArtResolver2$ArtUrl;

    invoke-virtual {v3, v4}, Lcom/google/android/music/art/ArtResolver2$ArtUrl;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 682
    iget-object v1, p0, Lcom/google/android/music/art/ArtResolver2$BitmapOriginInfo;->artUrl:Lcom/google/android/music/art/ArtResolver2$ArtUrl;

    invoke-virtual {v1}, Lcom/google/android/music/art/ArtResolver2$ArtUrl;->hashCode()I

    move-result v0

    .line 683
    .local v0, "result":I
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/android/music/art/ArtResolver2$BitmapOriginInfo;->source:I

    add-int v0, v1, v2

    .line 684
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/music/art/ArtResolver2$BitmapOriginInfo;->artType:Lcom/google/android/music/art/ArtType;

    invoke-virtual {v2}, Lcom/google/android/music/art/ArtType;->hashCode()I

    move-result v2

    add-int v0, v1, v2

    .line 685
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/android/music/art/ArtResolver2$BitmapOriginInfo;->sizeBucket:I

    add-int v0, v1, v2

    .line 686
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 691
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "BitmapOriginInfo{artUrl="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/art/ArtResolver2$BitmapOriginInfo;->artUrl:Lcom/google/android/music/art/ArtResolver2$ArtUrl;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", source="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/music/art/ArtResolver2$BitmapOriginInfo;->source:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", artType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/art/ArtResolver2$BitmapOriginInfo;->artType:Lcom/google/android/music/art/ArtType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", sizeBucket="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/music/art/ArtResolver2$BitmapOriginInfo;->sizeBucket:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
