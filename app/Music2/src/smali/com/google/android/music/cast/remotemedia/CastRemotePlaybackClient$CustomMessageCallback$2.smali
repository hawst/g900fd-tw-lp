.class Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$CustomMessageCallback$2;
.super Ljava/lang/Object;
.source "CastRemotePlaybackClient.java"

# interfaces
.implements Lcom/google/android/music/utils/async/AsyncRunner;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$CustomMessageCallback;->onMessageReceived(Lcom/google/android/gms/cast/CastDevice;Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$CustomMessageCallback;


# direct methods
.method constructor <init>(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$CustomMessageCallback;)V
    .locals 0

    .prologue
    .line 1028
    iput-object p1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$CustomMessageCallback$2;->this$1:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$CustomMessageCallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public backgroundTask()V
    .locals 2

    .prologue
    .line 1032
    iget-object v0, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$CustomMessageCallback$2;->this$1:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$CustomMessageCallback;

    iget-object v0, v0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$CustomMessageCallback;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    # getter for: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->LOGV:Z
    invoke-static {v0}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$400(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1033
    const-string v0, "MusicCastRemote"

    const-string v1, "Getting cloud queue"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 1035
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$CustomMessageCallback$2;->this$1:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$CustomMessageCallback;

    iget-object v0, v0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$CustomMessageCallback;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    # getter for: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$2200(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/music/cloudclient/CloudQueueManager;->getCloudQueue(Landroid/content/Context;)V

    .line 1036
    return-void
.end method

.method public taskCompleted()V
    .locals 2

    .prologue
    .line 1040
    iget-object v0, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$CustomMessageCallback$2;->this$1:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$CustomMessageCallback;

    iget-object v0, v0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$CustomMessageCallback;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    # getter for: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mServiceHooks:Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;
    invoke-static {v0}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$1000(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$CustomMessageCallback$2;->this$1:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$CustomMessageCallback;

    iget-object v1, v1, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$CustomMessageCallback;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    # getter for: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mItemId:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$2300(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;->onCloudQueueAdditionalSenderConnected(Ljava/lang/String;)V

    .line 1041
    return-void
.end method
