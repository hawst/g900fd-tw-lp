.class Lcom/google/android/music/tutorial/TutorialLaunchActivity$2;
.super Ljava/lang/Object;
.source "TutorialLaunchActivity.java"

# interfaces
.implements Lcom/google/android/music/utils/async/AsyncRunner;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/tutorial/TutorialLaunchActivity;->initTutorial()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/tutorial/TutorialLaunchActivity;

.field final synthetic val$appContext:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/google/android/music/tutorial/TutorialLaunchActivity;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 91
    iput-object p1, p0, Lcom/google/android/music/tutorial/TutorialLaunchActivity$2;->this$0:Lcom/google/android/music/tutorial/TutorialLaunchActivity;

    iput-object p2, p0, Lcom/google/android/music/tutorial/TutorialLaunchActivity$2;->val$appContext:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public backgroundTask()V
    .locals 3

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/music/tutorial/TutorialLaunchActivity$2;->val$appContext:Landroid/content/Context;

    sget-object v1, Lcom/google/android/music/store/MusicContent$XAudio;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/google/android/music/store/MusicContent;->existsContent(Landroid/content/Context;Landroid/net/Uri;Z)Z

    .line 98
    return-void
.end method

.method public taskCompleted()V
    .locals 2

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/android/music/tutorial/TutorialLaunchActivity$2;->this$0:Lcom/google/android/music/tutorial/TutorialLaunchActivity;

    invoke-virtual {v0}, Lcom/google/android/music/tutorial/TutorialLaunchActivity;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 103
    iget-object v0, p0, Lcom/google/android/music/tutorial/TutorialLaunchActivity$2;->this$0:Lcom/google/android/music/tutorial/TutorialLaunchActivity;

    invoke-virtual {v0}, Lcom/google/android/music/tutorial/TutorialLaunchActivity;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/tutorial/TutorialLaunchActivity$2;->this$0:Lcom/google/android/music/tutorial/TutorialLaunchActivity;

    # getter for: Lcom/google/android/music/tutorial/TutorialLaunchActivity;->mPreferenceChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;
    invoke-static {v1}, Lcom/google/android/music/tutorial/TutorialLaunchActivity;->access$100(Lcom/google/android/music/tutorial/TutorialLaunchActivity;)Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 105
    iget-object v0, p0, Lcom/google/android/music/tutorial/TutorialLaunchActivity$2;->this$0:Lcom/google/android/music/tutorial/TutorialLaunchActivity;

    # invokes: Lcom/google/android/music/tutorial/TutorialLaunchActivity;->checkAccount()V
    invoke-static {v0}, Lcom/google/android/music/tutorial/TutorialLaunchActivity;->access$200(Lcom/google/android/music/tutorial/TutorialLaunchActivity;)V

    .line 107
    :cond_0
    return-void
.end method
