.class public Lcom/google/android/music/utils/RouteChecker;
.super Ljava/lang/Object;
.source "RouteChecker.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/utils/RouteChecker$PackageEntry;,
        Lcom/google/android/music/utils/RouteChecker$SHA1;
    }
.end annotation


# instance fields
.field private final LOGV:Z

.field private final mContentObserver:Landroid/database/ContentObserver;

.field private mContext:Landroid/content/Context;

.field private mShouldValidateMediaRoutes:Z

.field private final mValidPackages:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/music/utils/RouteChecker$PackageEntry;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 169
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->CAST:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/music/utils/RouteChecker;->LOGV:Z

    .line 149
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/utils/RouteChecker;->mValidPackages:Ljava/util/Set;

    .line 182
    new-instance v0, Lcom/google/android/music/utils/RouteChecker$1;

    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-direct {v0, p0, v1}, Lcom/google/android/music/utils/RouteChecker$1;-><init>(Lcom/google/android/music/utils/RouteChecker;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/google/android/music/utils/RouteChecker;->mContentObserver:Landroid/database/ContentObserver;

    .line 170
    invoke-static {}, Lcom/google/android/music/utils/ConfigUtils;->getShouldValidateMediaRoutes()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/music/utils/RouteChecker;->mShouldValidateMediaRoutes:Z

    .line 171
    iget-boolean v0, p0, Lcom/google/android/music/utils/RouteChecker;->mShouldValidateMediaRoutes:Z

    if-nez v0, :cond_0

    .line 172
    const-string v0, "RouteChecker"

    const-string v1, "Not validating media routes"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    :cond_0
    iput-object p1, p0, Lcom/google/android/music/utils/RouteChecker;->mContext:Landroid/content/Context;

    .line 175
    iget-boolean v0, p0, Lcom/google/android/music/utils/RouteChecker;->mShouldValidateMediaRoutes:Z

    if-eqz v0, :cond_1

    .line 176
    invoke-direct {p0}, Lcom/google/android/music/utils/RouteChecker;->registerPackages()V

    .line 178
    :cond_1
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/music/store/ConfigContent;->SERVER_SETTINGS_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/music/utils/RouteChecker;->mContentObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 180
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/utils/RouteChecker;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/utils/RouteChecker;

    .prologue
    .line 34
    iget-boolean v0, p0, Lcom/google/android/music/utils/RouteChecker;->mShouldValidateMediaRoutes:Z

    return v0
.end method

.method static synthetic access$002(Lcom/google/android/music/utils/RouteChecker;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/utils/RouteChecker;
    .param p1, "x1"    # Z

    .prologue
    .line 34
    iput-boolean p1, p0, Lcom/google/android/music/utils/RouteChecker;->mShouldValidateMediaRoutes:Z

    return p1
.end method

.method static synthetic access$100(Lcom/google/android/music/utils/RouteChecker;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/utils/RouteChecker;

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/google/android/music/utils/RouteChecker;->registerPackages()V

    return-void
.end method

.method private anySignaturesMatch(Ljava/lang/String;[Landroid/content/pm/Signature;)Z
    .locals 12
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "signatures"    # [Landroid/content/pm/Signature;

    .prologue
    const/4 v8, 0x0

    .line 290
    const/4 v6, 0x0

    .line 292
    .local v6, "md":Ljava/security/MessageDigest;
    :try_start_0
    const-string v9, "SHA-1"

    invoke-static {v9}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 297
    move-object v0, p2

    .local v0, "arr$":[Landroid/content/pm/Signature;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_0

    aget-object v7, v0, v4

    .line 298
    .local v7, "signature":Landroid/content/pm/Signature;
    new-instance v1, Lcom/google/android/music/utils/RouteChecker$SHA1;

    invoke-virtual {v7}, Landroid/content/pm/Signature;->toByteArray()[B

    move-result-object v9

    invoke-virtual {v6, v9}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v9

    invoke-direct {v1, v9}, Lcom/google/android/music/utils/RouteChecker$SHA1;-><init>([B)V

    .line 299
    .local v1, "digest":Lcom/google/android/music/utils/RouteChecker$SHA1;
    new-instance v3, Lcom/google/android/music/utils/RouteChecker$PackageEntry;

    invoke-direct {v3, p0, p1, v1}, Lcom/google/android/music/utils/RouteChecker$PackageEntry;-><init>(Lcom/google/android/music/utils/RouteChecker;Ljava/lang/String;Lcom/google/android/music/utils/RouteChecker$SHA1;)V

    .line 300
    .local v3, "entry":Lcom/google/android/music/utils/RouteChecker$PackageEntry;
    iget-object v9, p0, Lcom/google/android/music/utils/RouteChecker;->mValidPackages:Ljava/util/Set;

    invoke-interface {v9, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 301
    const/4 v8, 0x1

    .line 310
    .end local v0    # "arr$":[Landroid/content/pm/Signature;
    .end local v1    # "digest":Lcom/google/android/music/utils/RouteChecker$SHA1;
    .end local v3    # "entry":Lcom/google/android/music/utils/RouteChecker$PackageEntry;
    .end local v4    # "i$":I
    .end local v5    # "len$":I
    .end local v7    # "signature":Landroid/content/pm/Signature;
    :cond_0
    :goto_1
    return v8

    .line 294
    :catch_0
    move-exception v2

    .line 295
    .local v2, "e":Ljava/security/NoSuchAlgorithmException;
    goto :goto_1

    .line 303
    .end local v2    # "e":Ljava/security/NoSuchAlgorithmException;
    .restart local v0    # "arr$":[Landroid/content/pm/Signature;
    .restart local v1    # "digest":Lcom/google/android/music/utils/RouteChecker$SHA1;
    .restart local v3    # "entry":Lcom/google/android/music/utils/RouteChecker$PackageEntry;
    .restart local v4    # "i$":I
    .restart local v5    # "len$":I
    .restart local v7    # "signature":Landroid/content/pm/Signature;
    :cond_1
    iget-boolean v9, p0, Lcom/google/android/music/utils/RouteChecker;->LOGV:Z

    if-eqz v9, :cond_2

    .line 304
    const-string v9, "RouteChecker"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Did not find "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 297
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 306
    :cond_2
    const-string v9, "RouteChecker"

    const-string v10, "Unsupported route"

    invoke-static {v9, v10}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method private getPackageSignatures(Landroid/content/pm/PackageManager;Ljava/lang/String;)[Landroid/content/pm/Signature;
    .locals 3
    .param p1, "packageManager"    # Landroid/content/pm/PackageManager;
    .param p2, "packageName"    # Ljava/lang/String;

    .prologue
    .line 316
    const/16 v2, 0x40

    :try_start_0
    invoke-virtual {p1, p2, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 320
    .local v1, "info":Landroid/content/pm/PackageInfo;
    iget-object v2, v1, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    .end local v1    # "info":Landroid/content/pm/PackageInfo;
    :goto_0
    return-object v2

    .line 317
    :catch_0
    move-exception v0

    .line 318
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v2, 0x0

    new-array v2, v2, [Landroid/content/pm/Signature;

    goto :goto_0
.end method

.method private registerJsonPackageSpec(Ljava/lang/String;)Z
    .locals 4
    .param p1, "packageSignatureSpec"    # Ljava/lang/String;

    .prologue
    .line 212
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/music/utils/RouteChecker;->registerJsonPackageSpecImpl(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 213
    const/4 v1, 0x1

    .line 216
    :goto_0
    return v1

    .line 214
    :catch_0
    move-exception v0

    .line 215
    .local v0, "ex":Lorg/json/JSONException;
    const-string v1, "RouteChecker"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Could not register packages: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 216
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private registerJsonPackageSpecImpl(Ljava/lang/String;)V
    .locals 20
    .param p1, "packageSignatureSpec"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 234
    new-instance v17, Lorg/json/JSONTokener;

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Lorg/json/JSONTokener;-><init>(Ljava/lang/String;)V

    .line 235
    .local v17, "tokenizer":Lorg/json/JSONTokener;
    invoke-virtual/range {v17 .. v17}, Lorg/json/JSONTokener;->nextValue()Ljava/lang/Object;

    move-result-object v14

    .line 236
    .local v14, "result":Ljava/lang/Object;
    instance-of v0, v14, Lorg/json/JSONArray;

    move/from16 v18, v0

    if-eqz v18, :cond_4

    move-object v3, v14

    .line 237
    check-cast v3, Lorg/json/JSONArray;

    .line 238
    .local v3, "entries":Lorg/json/JSONArray;
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v10

    .line 239
    .local v10, "length":I
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    if-ge v5, v10, :cond_4

    .line 240
    invoke-virtual {v3, v5}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 242
    .local v4, "entry":Lorg/json/JSONObject;
    const-string v18, "packages"

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v7

    .line 243
    .local v7, "jsonPackages":Lorg/json/JSONArray;
    invoke-virtual {v7}, Lorg/json/JSONArray;->length()I

    move-result v13

    .line 244
    .local v13, "packagesLength":I
    new-array v12, v13, [Ljava/lang/String;

    .line 245
    .local v12, "packages":[Ljava/lang/String;
    const/4 v6, 0x0

    .local v6, "j":I
    :goto_1
    if-ge v6, v13, :cond_0

    .line 246
    invoke-virtual {v7, v6}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v18

    aput-object v18, v12, v6

    .line 245
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 249
    :cond_0
    const-string v18, "sha1s"

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v8

    .line 250
    .local v8, "jsonSha1s":Lorg/json/JSONArray;
    invoke-virtual {v8}, Lorg/json/JSONArray;->length()I

    move-result v16

    .line 251
    .local v16, "sha1sLength":I
    move/from16 v0, v16

    new-array v15, v0, [Lcom/google/android/music/utils/RouteChecker$SHA1;

    .line 252
    .local v15, "sha1s":[Lcom/google/android/music/utils/RouteChecker$SHA1;
    const/4 v6, 0x0

    :goto_2
    move/from16 v0, v16

    if-ge v6, v0, :cond_1

    .line 253
    invoke-virtual {v8, v6}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/google/android/music/utils/RouteChecker$SHA1;->createFromHexString(Ljava/lang/String;)Lcom/google/android/music/utils/RouteChecker$SHA1;

    move-result-object v18

    aput-object v18, v15, v6

    .line 252
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 256
    :cond_1
    const/4 v6, 0x0

    :goto_3
    if-ge v6, v13, :cond_3

    .line 257
    const/4 v9, 0x0

    .local v9, "k":I
    :goto_4
    move/from16 v0, v16

    if-ge v9, v0, :cond_2

    .line 258
    new-instance v11, Lcom/google/android/music/utils/RouteChecker$PackageEntry;

    aget-object v18, v12, v6

    aget-object v19, v15, v9

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-direct {v11, v0, v1, v2}, Lcom/google/android/music/utils/RouteChecker$PackageEntry;-><init>(Lcom/google/android/music/utils/RouteChecker;Ljava/lang/String;Lcom/google/android/music/utils/RouteChecker$SHA1;)V

    .line 260
    .local v11, "packageEntry":Lcom/google/android/music/utils/RouteChecker$PackageEntry;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/utils/RouteChecker;->mValidPackages:Ljava/util/Set;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-interface {v0, v11}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 257
    add-int/lit8 v9, v9, 0x1

    goto :goto_4

    .line 256
    .end local v11    # "packageEntry":Lcom/google/android/music/utils/RouteChecker$PackageEntry;
    :cond_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    .line 239
    .end local v9    # "k":I
    :cond_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 265
    .end local v3    # "entries":Lorg/json/JSONArray;
    .end local v4    # "entry":Lorg/json/JSONObject;
    .end local v5    # "i":I
    .end local v6    # "j":I
    .end local v7    # "jsonPackages":Lorg/json/JSONArray;
    .end local v8    # "jsonSha1s":Lorg/json/JSONArray;
    .end local v10    # "length":I
    .end local v12    # "packages":[Ljava/lang/String;
    .end local v13    # "packagesLength":I
    .end local v15    # "sha1s":[Lcom/google/android/music/utils/RouteChecker$SHA1;
    .end local v16    # "sha1sLength":I
    :cond_4
    invoke-virtual/range {v17 .. v17}, Lorg/json/JSONTokener;->more()Z

    move-result v18

    if-eqz v18, :cond_5

    .line 266
    const-string v18, "Unexpected data at end of JSON string."

    invoke-virtual/range {v17 .. v18}, Lorg/json/JSONTokener;->syntaxError(Ljava/lang/String;)Lorg/json/JSONException;

    move-result-object v18

    throw v18

    .line 268
    :cond_5
    return-void
.end method

.method private registerPackages()V
    .locals 3

    .prologue
    .line 196
    iget-object v1, p0, Lcom/google/android/music/utils/RouteChecker;->mValidPackages:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->clear()V

    .line 198
    invoke-static {}, Lcom/google/android/music/utils/ConfigUtils;->getMediaRoutePackageSignatures()Ljava/lang/String;

    move-result-object v0

    .line 199
    .local v0, "mediaRoutePackages":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-direct {p0, v0}, Lcom/google/android/music/utils/RouteChecker;->registerJsonPackageSpec(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 200
    :cond_0
    const-string v1, "RouteChecker"

    const-string v2, "Using default route package signatures."

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    const-string v1, "[{\"packages\": [\"com.google.android.music\", \"com.google.android.setupwarlock\", \"com.google.android.gms\"], \"sha1s\": [\"38918A453D07199354F8B19AF05EC6562CED5788\", \"58E1C4133F7441EC3D2C270270A14802DA47BA0E\"]}]"

    invoke-direct {p0, v1}, Lcom/google/android/music/utils/RouteChecker;->registerJsonPackageSpec(Ljava/lang/String;)Z

    .line 203
    :cond_1
    return-void
.end method


# virtual methods
.method public isAcceptableRoute(Landroid/support/v7/media/MediaRouter$RouteInfo;)Z
    .locals 7
    .param p1, "route"    # Landroid/support/v7/media/MediaRouter$RouteInfo;

    .prologue
    const/4 v4, 0x1

    .line 271
    iget-boolean v5, p0, Lcom/google/android/music/utils/RouteChecker;->mShouldValidateMediaRoutes:Z

    if-nez v5, :cond_1

    .line 286
    :cond_0
    :goto_0
    return v4

    .line 274
    :cond_1
    invoke-virtual {p1}, Landroid/support/v7/media/MediaRouter$RouteInfo;->getProvider()Landroid/support/v7/media/MediaRouter$ProviderInfo;

    move-result-object v3

    .line 275
    .local v3, "providerInfo":Landroid/support/v7/media/MediaRouter$ProviderInfo;
    invoke-virtual {v3}, Landroid/support/v7/media/MediaRouter$ProviderInfo;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 277
    .local v1, "packageName":Ljava/lang/String;
    const-string v5, "android"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 280
    iget-object v5, p0, Lcom/google/android/music/utils/RouteChecker;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 281
    .local v0, "packageManager":Landroid/content/pm/PackageManager;
    invoke-direct {p0, v0, v1}, Lcom/google/android/music/utils/RouteChecker;->getPackageSignatures(Landroid/content/pm/PackageManager;Ljava/lang/String;)[Landroid/content/pm/Signature;

    move-result-object v2

    .line 282
    .local v2, "packageSignatures":[Landroid/content/pm/Signature;
    invoke-direct {p0, v1, v2}, Lcom/google/android/music/utils/RouteChecker;->anySignaturesMatch(Ljava/lang/String;[Landroid/content/pm/Signature;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 285
    const-string v4, "RouteChecker"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Rejecting package: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 286
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 325
    iget-object v0, p0, Lcom/google/android/music/utils/RouteChecker;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/utils/RouteChecker;->mContentObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 326
    return-void
.end method
