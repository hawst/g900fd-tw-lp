.class public Lcom/google/android/music/wear/NodeCapabilityRefresher;
.super Ljava/lang/Object;
.source "NodeCapabilityRefresher.java"


# instance fields
.field private final mIntentReceiver:Lcom/google/android/wear/intentstarter/IntentReceiver;

.field private final mMicroAppDetector:Lcom/google/android/music/wear/MicroAppDetector;

.field private final mNodeCapabilityRegistry:Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry;


# direct methods
.method public constructor <init>(Lcom/google/android/wear/intentstarter/IntentReceiver;Lcom/google/android/music/wear/MicroAppDetector;Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry;)V
    .locals 0
    .param p1, "intentReceiver"    # Lcom/google/android/wear/intentstarter/IntentReceiver;
    .param p2, "microAppDetector"    # Lcom/google/android/music/wear/MicroAppDetector;
    .param p3, "nodeCapabilityRegistry"    # Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry;

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/google/android/music/wear/NodeCapabilityRefresher;->mIntentReceiver:Lcom/google/android/wear/intentstarter/IntentReceiver;

    .line 33
    iput-object p2, p0, Lcom/google/android/music/wear/NodeCapabilityRefresher;->mMicroAppDetector:Lcom/google/android/music/wear/MicroAppDetector;

    .line 34
    iput-object p3, p0, Lcom/google/android/music/wear/NodeCapabilityRefresher;->mNodeCapabilityRegistry:Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry;

    .line 35
    return-void
.end method

.method public static createFromContext(Landroid/content/Context;)Lcom/google/android/music/wear/NodeCapabilityRefresher;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 22
    new-instance v0, Lcom/google/android/music/wear/NodeCapabilityRefresher;

    new-instance v1, Lcom/google/android/wear/intentstarter/IntentReceiver;

    invoke-direct {v1, p0}, Lcom/google/android/wear/intentstarter/IntentReceiver;-><init>(Landroid/content/Context;)V

    new-instance v2, Lcom/google/android/music/wear/MicroAppDetector;

    invoke-direct {v2, p0}, Lcom/google/android/music/wear/MicroAppDetector;-><init>(Landroid/content/Context;)V

    new-instance v3, Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry;

    invoke-direct {v3, p0}, Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry;-><init>(Landroid/content/Context;)V

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/music/wear/NodeCapabilityRefresher;-><init>(Lcom/google/android/wear/intentstarter/IntentReceiver;Lcom/google/android/music/wear/MicroAppDetector;Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry;)V

    return-object v0
.end method


# virtual methods
.method public refreshNodeCapabilities()V
    .locals 2

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/music/wear/NodeCapabilityRefresher;->mIntentReceiver:Lcom/google/android/wear/intentstarter/IntentReceiver;

    invoke-virtual {v0}, Lcom/google/android/wear/intentstarter/IntentReceiver;->registerAsReceiverInWearNetwork()V

    .line 44
    iget-object v0, p0, Lcom/google/android/music/wear/NodeCapabilityRefresher;->mMicroAppDetector:Lcom/google/android/music/wear/MicroAppDetector;

    invoke-virtual {v0}, Lcom/google/android/music/wear/MicroAppDetector;->refreshMicroAppPresence()V

    .line 45
    iget-object v0, p0, Lcom/google/android/music/wear/NodeCapabilityRefresher;->mNodeCapabilityRegistry:Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry;

    const-string v1, "music_phone_app"

    invoke-virtual {v0, v1}, Lcom/google/android/wear/capabilityregistry/NodeCapabilityRegistry;->registerCapabilityPath(Ljava/lang/String;)V

    .line 47
    return-void
.end method

.method public shouldRefreshForDataItemUri(Landroid/net/Uri;)Z
    .locals 1
    .param p1, "dataItemUri"    # Landroid/net/Uri;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/music/wear/NodeCapabilityRefresher;->mMicroAppDetector:Lcom/google/android/music/wear/MicroAppDetector;

    invoke-virtual {v0, p1}, Lcom/google/android/music/wear/MicroAppDetector;->shouldRefreshForDataItem(Landroid/net/Uri;)Z

    move-result v0

    return v0
.end method
