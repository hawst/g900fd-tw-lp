.class public Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;
.super Ljava/lang/Object;
.source "MusicDownstreamMerger.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/sync/google/MusicDownstreamMerger;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MusicBlockMerger"
.end annotation


# instance fields
.field private final mBlacklistItem:Lcom/google/android/music/store/BlacklistItem;

.field private mCacheFilepaths:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mContext:Landroid/content/Context;

.field private final mDb:Landroid/database/sqlite/SQLiteDatabase;

.field private mDeletePlaylistStatement:Landroid/database/sqlite/SQLiteStatement;

.field private mDeletePlentryStatement:Landroid/database/sqlite/SQLiteStatement;

.field private mInsertBlacklistItemStatement:Landroid/database/sqlite/SQLiteStatement;

.field private mInsertPlaylistStatement:Landroid/database/sqlite/SQLiteStatement;

.field private mInsertPlentryStatement:Landroid/database/sqlite/SQLiteStatement;

.field private mInsertRadioStationStatement:Landroid/database/sqlite/SQLiteStatement;

.field private mInsertTrackStatement:Landroid/database/sqlite/SQLiteStatement;

.field private final mMusicFile:Lcom/google/android/music/store/MusicFile;

.field private final mPlayList:Lcom/google/android/music/store/PlayList;

.field private final mPlaylistItem:Lcom/google/android/music/store/PlayList$Item;

.field private final mProtocolState:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final mRadioStation:Lcom/google/android/music/store/RadioStation;

.field private final mRemoteAccount:I

.field private final mTag:Ljava/lang/String;

.field private mUpdatePlaylistStatement:Landroid/database/sqlite/SQLiteStatement;

.field private mUpdatePlentryStatement:Landroid/database/sqlite/SQLiteStatement;

.field private mUpdateRadioStationStatement:Landroid/database/sqlite/SQLiteStatement;

.field private mUpdateTrackStatement:Landroid/database/sqlite/SQLiteStatement;

.field private final mUseVerboseLogging:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;Ljava/util/Map;ZLjava/lang/String;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p4, "useVerboseLogging"    # Z
    .param p5, "logTag"    # Ljava/lang/String;
    .param p6, "remoteAccount"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;Z",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 239
    .local p3, "protcolState":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 240
    iput-object p1, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mContext:Landroid/content/Context;

    .line 241
    iput-object p2, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    .line 242
    iput-object p3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mProtocolState:Ljava/util/Map;

    .line 243
    iput-boolean p4, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mUseVerboseLogging:Z

    .line 244
    iput-object p5, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mTag:Ljava/lang/String;

    .line 245
    new-instance v0, Lcom/google/android/music/store/MusicFile;

    invoke-direct {v0}, Lcom/google/android/music/store/MusicFile;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mMusicFile:Lcom/google/android/music/store/MusicFile;

    .line 246
    new-instance v0, Lcom/google/android/music/store/PlayList;

    invoke-direct {v0}, Lcom/google/android/music/store/PlayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mPlayList:Lcom/google/android/music/store/PlayList;

    .line 247
    new-instance v0, Lcom/google/android/music/store/PlayList$Item;

    invoke-direct {v0}, Lcom/google/android/music/store/PlayList$Item;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mPlaylistItem:Lcom/google/android/music/store/PlayList$Item;

    .line 248
    new-instance v0, Lcom/google/android/music/store/RadioStation;

    invoke-direct {v0}, Lcom/google/android/music/store/RadioStation;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mRadioStation:Lcom/google/android/music/store/RadioStation;

    .line 249
    iput p6, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mRemoteAccount:I

    .line 250
    new-instance v0, Lcom/google/android/music/store/BlacklistItem;

    invoke-direct {v0}, Lcom/google/android/music/store/BlacklistItem;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mBlacklistItem:Lcom/google/android/music/store/BlacklistItem;

    .line 251
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;

    .prologue
    .line 212
    iget-object v0, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mTag:Ljava/lang/String;

    return-object v0
.end method

.method private processMergeBlacklistItemImpl(Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;)Z
    .locals 6
    .param p1, "serverBlacklistItem"    # Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;
    .param p2, "clientBlacklistItem"    # Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;

    .prologue
    const/4 v1, 0x0

    .line 588
    iget-object v2, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mBlacklistItem:Lcom/google/android/music/store/BlacklistItem;

    invoke-virtual {v2}, Lcom/google/android/music/store/BlacklistItem;->reset()V

    .line 589
    iget-object v2, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mBlacklistItem:Lcom/google/android/music/store/BlacklistItem;

    invoke-virtual {p1, v2}, Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;->formatAsBlacklistItem(Lcom/google/android/music/store/BlacklistItem;)Lcom/google/android/music/store/BlacklistItem;

    .line 590
    iget-object v2, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mBlacklistItem:Lcom/google/android/music/store/BlacklistItem;

    iget v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mRemoteAccount:I

    invoke-virtual {v2, v3}, Lcom/google/android/music/store/BlacklistItem;->setSourceAccount(I)V

    .line 591
    iget-object v2, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mBlacklistItem:Lcom/google/android/music/store/BlacklistItem;

    invoke-virtual {v2, v1}, Lcom/google/android/music/store/BlacklistItem;->setNeedsSync(Z)V

    .line 593
    iget-boolean v2, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mUseVerboseLogging:Z

    if-eqz v2, :cond_0

    .line 594
    iget-object v2, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mTag:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Inserting blacklist item "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p1, Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;->mRemoteId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " at version "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, p1, Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;->mDismissalTimestampMillsec:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 597
    :cond_0
    iget-object v2, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mInsertBlacklistItemStatement:Landroid/database/sqlite/SQLiteStatement;

    if-nez v2, :cond_1

    .line 598
    iget-object v2, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    invoke-static {v2}, Lcom/google/android/music/store/BlacklistItem;->compileInsertStatement(Landroid/database/sqlite/SQLiteDatabase;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mInsertBlacklistItemStatement:Landroid/database/sqlite/SQLiteStatement;

    .line 602
    :cond_1
    :try_start_0
    iget-object v2, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mBlacklistItem:Lcom/google/android/music/store/BlacklistItem;

    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mInsertBlacklistItemStatement:Landroid/database/sqlite/SQLiteStatement;

    invoke-virtual {v2, v3}, Lcom/google/android/music/store/BlacklistItem;->insert(Landroid/database/sqlite/SQLiteStatement;)J
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 603
    const/4 v1, 0x1

    .line 607
    :goto_0
    return v1

    .line 604
    :catch_0
    move-exception v0

    .line 605
    .local v0, "e":Ljava/lang/RuntimeException;
    iget-object v2, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mTag:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Ignoring blacklist item: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private processMergeEntryImpl(Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;)Z
    .locals 8
    .param p1, "serverEntry"    # Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;
    .param p2, "clientEntry"    # Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;

    .prologue
    const/4 v2, 0x0

    .line 278
    :try_start_0
    iget-boolean v3, p1, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mIsDeleted:Z

    if-eqz v3, :cond_2

    .line 279
    iget-boolean v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mUseVerboseLogging:Z

    if-eqz v3, :cond_0

    .line 280
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mTag:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Deleting plentry "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p1, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mRemoteId:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " in playlist "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p1, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mRemotePlaylistId:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " at version "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v6, p1, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mLastModifiedTimestamp:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 285
    :cond_0
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mDeletePlentryStatement:Landroid/database/sqlite/SQLiteStatement;

    if-nez v3, :cond_1

    .line 286
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    invoke-static {v3}, Lcom/google/android/music/store/PlayList$Item;->compileItemDeleteStatement(Landroid/database/sqlite/SQLiteDatabase;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mDeletePlentryStatement:Landroid/database/sqlite/SQLiteStatement;

    .line 289
    :cond_1
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mDeletePlentryStatement:Landroid/database/sqlite/SQLiteStatement;

    iget v4, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mRemoteAccount:I

    iget-object v5, p1, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mRemoteId:Ljava/lang/String;

    invoke-static {v3, v4, v5}, Lcom/google/android/music/store/PlayList$Item;->deleteBySourceInfo(Landroid/database/sqlite/SQLiteStatement;ILjava/lang/String;)V

    .line 359
    :goto_0
    const/4 v2, 0x1

    .line 362
    :goto_1
    return v2

    .line 292
    :cond_2
    if-nez p2, :cond_6

    .line 293
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mPlaylistItem:Lcom/google/android/music/store/PlayList$Item;

    invoke-virtual {v3}, Lcom/google/android/music/store/PlayList$Item;->reset()V

    .line 294
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mPlaylistItem:Lcom/google/android/music/store/PlayList$Item;

    invoke-virtual {p1, v3}, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->formatAsPlayListItem(Lcom/google/android/music/store/PlayList$Item;)Lcom/google/android/music/store/PlayList$Item;

    .line 295
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mPlaylistItem:Lcom/google/android/music/store/PlayList$Item;

    iget v4, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mRemoteAccount:I

    invoke-virtual {v3, v4}, Lcom/google/android/music/store/PlayList$Item;->setSourceAccount(I)V
    :try_end_0
    .catch Lcom/google/android/music/store/InvalidDataException; {:try_start_0 .. :try_end_0} :catch_0

    .line 298
    :try_start_1
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mPlaylistItem:Lcom/google/android/music/store/PlayList$Item;

    iget-object v4, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    iget v5, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mRemoteAccount:I

    iget-object v6, p1, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mTrackId:Ljava/lang/String;

    invoke-static {v4, v5, v6}, Lcom/google/android/music/store/Store;->getMusicIdForSourceId(Landroid/database/sqlite/SQLiteDatabase;ILjava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/google/android/music/store/PlayList$Item;->setMusicId(J)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lcom/google/android/music/store/InvalidDataException; {:try_start_1 .. :try_end_1} :catch_0

    .line 307
    :try_start_2
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mPlayList:Lcom/google/android/music/store/PlayList;

    if-nez v3, :cond_3

    .line 308
    const-string v3, "MusicSyncAdapter"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Trying into insert a playlist entry "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mPlaylistItem:Lcom/google/android/music/store/PlayList$Item;

    invoke-virtual {v5}, Lcom/google/android/music/store/PlayList$Item;->getSourceId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " into a playlist "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p1, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mRemotePlaylistId:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " that doesn\'t exist locally"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Lcom/google/android/music/store/InvalidDataException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1

    .line 360
    :catch_0
    move-exception v0

    .line 361
    .local v0, "e":Lcom/google/android/music/store/InvalidDataException;
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mTag:Ljava/lang/String;

    const-string v4, "Unable to merge a playlist entry.  Skipping entry."

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 301
    .end local v0    # "e":Lcom/google/android/music/store/InvalidDataException;
    :catch_1
    move-exception v0

    .line 302
    .local v0, "e":Ljava/io/FileNotFoundException;
    :try_start_3
    const-string v3, "MusicSyncAdapter"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed to process a playlist entry insert for item id: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p1, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mTrackId:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 314
    .end local v0    # "e":Ljava/io/FileNotFoundException;
    :cond_3
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mPlayList:Lcom/google/android/music/store/PlayList;

    invoke-virtual {v3}, Lcom/google/android/music/store/PlayList;->reset()V

    .line 315
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    iget v4, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mRemoteAccount:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p1, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mRemotePlaylistId:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mPlayList:Lcom/google/android/music/store/PlayList;

    invoke-static {v3, v4, v5, v6}, Lcom/google/android/music/store/PlayList;->readPlayList(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/music/store/PlayList;)Lcom/google/android/music/store/PlayList;

    .line 318
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mPlaylistItem:Lcom/google/android/music/store/PlayList$Item;

    iget-object v4, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mPlayList:Lcom/google/android/music/store/PlayList;

    invoke-virtual {v4}, Lcom/google/android/music/store/PlayList;->getId()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/google/android/music/store/PlayList$Item;->setListId(J)V

    .line 320
    iget-boolean v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mUseVerboseLogging:Z

    if-eqz v3, :cond_4

    .line 321
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mTag:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Inserting plentry "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p1, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mRemoteId:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " in playlist "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p1, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mRemotePlaylistId:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " at version "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v6, p1, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mLastModifiedTimestamp:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 325
    :cond_4
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mInsertPlentryStatement:Landroid/database/sqlite/SQLiteStatement;

    if-nez v3, :cond_5

    .line 326
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    invoke-static {v3}, Lcom/google/android/music/store/PlayList$Item;->compileItemInsertStatement(Landroid/database/sqlite/SQLiteDatabase;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mInsertPlentryStatement:Landroid/database/sqlite/SQLiteStatement;

    .line 329
    :cond_5
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mPlaylistItem:Lcom/google/android/music/store/PlayList$Item;

    iget-object v4, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mInsertPlentryStatement:Landroid/database/sqlite/SQLiteStatement;

    invoke-virtual {v3, v4}, Lcom/google/android/music/store/PlayList$Item;->insertItem(Landroid/database/sqlite/SQLiteStatement;)J

    goto/16 :goto_0

    .line 332
    :cond_6
    invoke-virtual {p2}, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->getEncapsulatedItem()Lcom/google/android/music/store/PlayList$Item;

    move-result-object v1

    .line 334
    .local v1, "item":Lcom/google/android/music/store/PlayList$Item;
    invoke-virtual {p1, v1}, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->formatAsPlayListItem(Lcom/google/android/music/store/PlayList$Item;)Lcom/google/android/music/store/PlayList$Item;
    :try_end_3
    .catch Lcom/google/android/music/store/InvalidDataException; {:try_start_3 .. :try_end_3} :catch_0

    .line 336
    :try_start_4
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    iget v4, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mRemoteAccount:I

    iget-object v5, p1, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mTrackId:Ljava/lang/String;

    invoke-static {v3, v4, v5}, Lcom/google/android/music/store/Store;->getMusicIdForSourceId(Landroid/database/sqlite/SQLiteDatabase;ILjava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Lcom/google/android/music/store/PlayList$Item;->setMusicId(J)V
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Lcom/google/android/music/store/InvalidDataException; {:try_start_4 .. :try_end_4} :catch_0

    .line 346
    :try_start_5
    iget-boolean v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mUseVerboseLogging:Z

    if-eqz v3, :cond_7

    .line 347
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mTag:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Updating plentry "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p1, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mRemoteId:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " in playlist "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p1, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mRemotePlaylistId:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " at version "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v6, p1, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mLastModifiedTimestamp:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 351
    :cond_7
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mUpdatePlentryStatement:Landroid/database/sqlite/SQLiteStatement;

    if-nez v3, :cond_8

    .line 352
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    invoke-static {v3}, Lcom/google/android/music/store/PlayList$Item;->compileItemUpdateStatement(Landroid/database/sqlite/SQLiteDatabase;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mUpdatePlentryStatement:Landroid/database/sqlite/SQLiteStatement;

    .line 355
    :cond_8
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Lcom/google/android/music/store/PlayList$Item;->setNeedsSync(Z)V

    .line 356
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mUpdatePlentryStatement:Landroid/database/sqlite/SQLiteStatement;

    invoke-virtual {v1, v3}, Lcom/google/android/music/store/PlayList$Item;->update(Landroid/database/sqlite/SQLiteStatement;)V

    goto/16 :goto_0

    .line 339
    :catch_2
    move-exception v0

    .line 340
    .restart local v0    # "e":Ljava/io/FileNotFoundException;
    const-string v3, "MusicSyncAdapter"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed to process a playlist entry update for item id: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p1, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mTrackId:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catch Lcom/google/android/music/store/InvalidDataException; {:try_start_5 .. :try_end_5} :catch_0

    goto/16 :goto_1
.end method

.method private processMergePlaylistImpl(Lcom/google/android/music/sync/google/model/SyncablePlaylist;Lcom/google/android/music/sync/google/model/SyncablePlaylist;)Z
    .locals 8
    .param p1, "serverPlaylist"    # Lcom/google/android/music/sync/google/model/SyncablePlaylist;
    .param p2, "clientPlaylist"    # Lcom/google/android/music/sync/google/model/SyncablePlaylist;

    .prologue
    const/4 v2, 0x0

    .line 369
    :try_start_0
    iget-boolean v3, p1, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mIsDeleted:Z

    if-eqz v3, :cond_3

    .line 370
    iget-boolean v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mUseVerboseLogging:Z

    if-eqz v3, :cond_0

    .line 371
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mTag:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Deleting playlist "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p1, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mRemoteId:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " at version "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v6, p1, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mLastModifiedTimestamp:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 374
    :cond_0
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mDeletePlaylistStatement:Landroid/database/sqlite/SQLiteStatement;

    if-nez v3, :cond_1

    .line 375
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    invoke-static {v3}, Lcom/google/android/music/store/PlayList;->compilePlaylistDeleteStatement(Landroid/database/sqlite/SQLiteDatabase;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mDeletePlaylistStatement:Landroid/database/sqlite/SQLiteStatement;

    .line 378
    :cond_1
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mDeletePlaylistStatement:Landroid/database/sqlite/SQLiteStatement;

    iget v4, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mRemoteAccount:I

    iget-object v5, p1, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mRemoteId:Ljava/lang/String;

    invoke-static {v3, v4, v5}, Lcom/google/android/music/store/PlayList;->deleteBySourceInfo(Landroid/database/sqlite/SQLiteStatement;ILjava/lang/String;)V

    .line 419
    :cond_2
    :goto_0
    const/4 v2, 0x1

    .line 423
    :goto_1
    return v2

    .line 381
    :cond_3
    if-nez p2, :cond_6

    .line 382
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mPlayList:Lcom/google/android/music/store/PlayList;

    invoke-virtual {v3}, Lcom/google/android/music/store/PlayList;->reset()V

    .line 383
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mPlayList:Lcom/google/android/music/store/PlayList;

    invoke-virtual {p1, v3}, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->formatAsPlayList(Lcom/google/android/music/store/PlayList;)Lcom/google/android/music/store/PlayList;

    .line 384
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mPlayList:Lcom/google/android/music/store/PlayList;

    iget v4, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mRemoteAccount:I

    invoke-virtual {v3, v4}, Lcom/google/android/music/store/PlayList;->setSourceAccount(I)V

    .line 385
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mPlayList:Lcom/google/android/music/store/PlayList;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/google/android/music/store/PlayList;->setNeedsSync(Z)V

    .line 387
    iget-boolean v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mUseVerboseLogging:Z

    if-eqz v3, :cond_4

    .line 388
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mTag:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Inserting playlist "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p1, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mRemoteId:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " at version "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v6, p1, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mLastModifiedTimestamp:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 391
    :cond_4
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mInsertPlaylistStatement:Landroid/database/sqlite/SQLiteStatement;

    if-nez v3, :cond_5

    .line 392
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    invoke-static {v3}, Lcom/google/android/music/store/PlayList;->compilePlayListInsertStatement(Landroid/database/sqlite/SQLiteDatabase;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mInsertPlaylistStatement:Landroid/database/sqlite/SQLiteStatement;

    .line 395
    :cond_5
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mPlayList:Lcom/google/android/music/store/PlayList;

    iget-object v4, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mInsertPlaylistStatement:Landroid/database/sqlite/SQLiteStatement;

    invoke-virtual {v3, v4}, Lcom/google/android/music/store/PlayList;->insertList(Landroid/database/sqlite/SQLiteStatement;)J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-gtz v3, :cond_2

    goto :goto_1

    .line 400
    :cond_6
    invoke-virtual {p2}, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->getEncapsulatedPlayList()Lcom/google/android/music/store/PlayList;

    move-result-object v1

    .line 402
    .local v1, "playList":Lcom/google/android/music/store/PlayList;
    invoke-virtual {p1, v1}, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->formatAsPlayList(Lcom/google/android/music/store/PlayList;)Lcom/google/android/music/store/PlayList;

    .line 403
    iget v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mRemoteAccount:I

    invoke-virtual {v1, v3}, Lcom/google/android/music/store/PlayList;->setSourceAccount(I)V

    .line 404
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Lcom/google/android/music/store/PlayList;->setNeedsSync(Z)V

    .line 406
    iget-boolean v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mUseVerboseLogging:Z

    if-eqz v3, :cond_7

    .line 407
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mTag:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Updating playlist "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p1, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mRemoteId:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " at version "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v6, p1, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mLastModifiedTimestamp:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " with local id "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/google/android/music/store/PlayList;->getId()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 411
    :cond_7
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mUpdatePlaylistStatement:Landroid/database/sqlite/SQLiteStatement;

    if-nez v3, :cond_8

    .line 412
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    invoke-static {v3}, Lcom/google/android/music/store/PlayList;->compilePlayListUpdateStatement(Landroid/database/sqlite/SQLiteDatabase;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mUpdatePlaylistStatement:Landroid/database/sqlite/SQLiteStatement;

    .line 415
    :cond_8
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Lcom/google/android/music/store/PlayList;->setNeedsSync(Z)V

    .line 416
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mUpdatePlaylistStatement:Landroid/database/sqlite/SQLiteStatement;

    invoke-virtual {v1, v3}, Lcom/google/android/music/store/PlayList;->update(Landroid/database/sqlite/SQLiteStatement;)V
    :try_end_0
    .catch Lcom/google/android/music/store/InvalidDataException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 420
    .end local v1    # "playList":Lcom/google/android/music/store/PlayList;
    :catch_0
    move-exception v0

    .line 422
    .local v0, "e":Lcom/google/android/music/store/InvalidDataException;
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mTag:Ljava/lang/String;

    const-string v4, "Unable to merge a playlist.  Skipping entry."

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_1
.end method

.method private processMergeRadioStationImpl(Lcom/google/android/music/sync/google/model/SyncableRadioStation;Lcom/google/android/music/sync/google/model/SyncableRadioStation;)Z
    .locals 8
    .param p1, "serverRadioStation"    # Lcom/google/android/music/sync/google/model/SyncableRadioStation;
    .param p2, "clientRadioStation"    # Lcom/google/android/music/sync/google/model/SyncableRadioStation;

    .prologue
    const/4 v2, 0x0

    .line 531
    :try_start_0
    iget-boolean v3, p1, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mIsDeleted:Z

    if-eqz v3, :cond_1

    .line 532
    iget-boolean v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mUseVerboseLogging:Z

    if-eqz v3, :cond_0

    .line 533
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mTag:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Deleting radio station "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p1, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mRemoteId:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " at version "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v6, p1, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mLastModifiedTimestamp:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 536
    :cond_0
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    iget v4, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mRemoteAccount:I

    iget-object v5, p1, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mRemoteId:Ljava/lang/String;

    invoke-static {v3, v4, v5}, Lcom/google/android/music/store/RadioStation;->deleteBySourceInfo(Landroid/database/sqlite/SQLiteDatabase;ILjava/lang/String;)V

    .line 578
    :goto_0
    const/4 v2, 0x1

    .line 581
    :goto_1
    return v2

    .line 539
    :cond_1
    if-nez p2, :cond_4

    .line 540
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mRadioStation:Lcom/google/android/music/store/RadioStation;

    invoke-virtual {v3}, Lcom/google/android/music/store/RadioStation;->reset()V

    .line 541
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mRadioStation:Lcom/google/android/music/store/RadioStation;

    invoke-virtual {p1, v3}, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->formatAsRadioStation(Lcom/google/android/music/store/RadioStation;)Lcom/google/android/music/store/RadioStation;

    .line 542
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mRadioStation:Lcom/google/android/music/store/RadioStation;

    iget v4, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mRemoteAccount:I

    invoke-virtual {v3, v4}, Lcom/google/android/music/store/RadioStation;->setSourceAccount(I)V

    .line 543
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mRadioStation:Lcom/google/android/music/store/RadioStation;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/google/android/music/store/RadioStation;->setNeedsSync(Z)V

    .line 545
    iget-boolean v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mUseVerboseLogging:Z

    if-eqz v3, :cond_2

    .line 546
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mTag:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Inserting radio station "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p1, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mRemoteId:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " at version "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v6, p1, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mLastModifiedTimestamp:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 549
    :cond_2
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mInsertRadioStationStatement:Landroid/database/sqlite/SQLiteStatement;

    if-nez v3, :cond_3

    .line 550
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    invoke-static {v3}, Lcom/google/android/music/store/RadioStation;->compileInsertStatement(Landroid/database/sqlite/SQLiteDatabase;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mInsertRadioStationStatement:Landroid/database/sqlite/SQLiteStatement;
    :try_end_0
    .catch Lcom/google/android/music/store/InvalidDataException; {:try_start_0 .. :try_end_0} :catch_1

    .line 554
    :cond_3
    :try_start_1
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mRadioStation:Lcom/google/android/music/store/RadioStation;

    iget-object v4, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mInsertRadioStationStatement:Landroid/database/sqlite/SQLiteStatement;

    invoke-virtual {v3, v4}, Lcom/google/android/music/store/RadioStation;->insert(Landroid/database/sqlite/SQLiteStatement;)J
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/google/android/music/store/InvalidDataException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 555
    :catch_0
    move-exception v0

    .line 556
    .local v0, "e":Ljava/lang/RuntimeException;
    :try_start_2
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mTag:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Ignoring radio station: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Lcom/google/android/music/store/InvalidDataException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 579
    .end local v0    # "e":Ljava/lang/RuntimeException;
    :catch_1
    move-exception v0

    .line 580
    .local v0, "e":Lcom/google/android/music/store/InvalidDataException;
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mTag:Ljava/lang/String;

    const-string v4, "Unable to merge a radio station.  Skipping."

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 560
    .end local v0    # "e":Lcom/google/android/music/store/InvalidDataException;
    :cond_4
    :try_start_3
    invoke-virtual {p2}, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->getEncapsulatedRadioStation()Lcom/google/android/music/store/RadioStation;

    move-result-object v1

    .line 562
    .local v1, "radioStation":Lcom/google/android/music/store/RadioStation;
    invoke-virtual {p1, v1}, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->formatAsRadioStation(Lcom/google/android/music/store/RadioStation;)Lcom/google/android/music/store/RadioStation;

    .line 563
    iget v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mRemoteAccount:I

    invoke-virtual {v1, v3}, Lcom/google/android/music/store/RadioStation;->setSourceAccount(I)V

    .line 564
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Lcom/google/android/music/store/RadioStation;->setNeedsSync(Z)V

    .line 566
    iget-boolean v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mUseVerboseLogging:Z

    if-eqz v3, :cond_5

    .line 567
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mTag:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Updating radio station "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p1, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mRemoteId:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " at version "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v6, p1, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mLastModifiedTimestamp:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " with local id "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/google/android/music/store/RadioStation;->getId()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 571
    :cond_5
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mUpdateRadioStationStatement:Landroid/database/sqlite/SQLiteStatement;

    if-nez v3, :cond_6

    .line 572
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    invoke-static {v3}, Lcom/google/android/music/store/RadioStation;->compileUpdateStatement(Landroid/database/sqlite/SQLiteDatabase;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mUpdateRadioStationStatement:Landroid/database/sqlite/SQLiteStatement;

    .line 574
    :cond_6
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Lcom/google/android/music/store/RadioStation;->setNeedsSync(Z)V

    .line 575
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mUpdateRadioStationStatement:Landroid/database/sqlite/SQLiteStatement;

    invoke-virtual {v1, v3}, Lcom/google/android/music/store/RadioStation;->update(Landroid/database/sqlite/SQLiteStatement;)V
    :try_end_3
    .catch Lcom/google/android/music/store/InvalidDataException; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_0
.end method

.method private processMergeTrackImpl(Lcom/google/android/music/sync/google/model/Track;Lcom/google/android/music/sync/google/model/Track;)Z
    .locals 12
    .param p1, "serverTrack"    # Lcom/google/android/music/sync/google/model/Track;
    .param p2, "clientTrack"    # Lcom/google/android/music/sync/google/model/Track;

    .prologue
    .line 430
    :try_start_0
    iget-boolean v7, p1, Lcom/google/android/music/sync/google/model/Track;->mIsDeleted:Z

    if-eqz v7, :cond_3

    .line 431
    iget-boolean v7, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mUseVerboseLogging:Z

    if-eqz v7, :cond_0

    .line 432
    iget-object v7, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mTag:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Deleting track "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p1, Lcom/google/android/music/sync/google/model/Track;->mRemoteId:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " at version "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-wide v10, p1, Lcom/google/android/music/sync/google/model/Track;->mLastModifiedTimestamp:J

    invoke-virtual {v8, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 438
    :cond_0
    iget-object v7, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mContext:Landroid/content/Context;

    iget-object v8, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    iget v9, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mRemoteAccount:I

    iget-object v10, p1, Lcom/google/android/music/sync/google/model/Track;->mRemoteId:Ljava/lang/String;

    invoke-static {v7, v8, v9, v10}, Lcom/google/android/music/store/MusicFile;->deleteAndGetLocalCacheFilepath(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 440
    .local v4, "filepath":Ljava/lang/String;
    if-eqz v4, :cond_2

    .line 441
    iget-object v7, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mCacheFilepaths:Ljava/util/List;

    if-nez v7, :cond_1

    .line 442
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    iput-object v7, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mCacheFilepaths:Ljava/util/List;

    .line 444
    :cond_1
    iget-object v7, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mCacheFilepaths:Ljava/util/List;

    invoke-interface {v7, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 521
    .end local v4    # "filepath":Ljava/lang/String;
    :cond_2
    :goto_0
    const/4 v7, 0x1

    .line 524
    :goto_1
    return v7

    .line 451
    :cond_3
    invoke-virtual {p1}, Lcom/google/android/music/sync/google/model/Track;->getTrackType()I

    move-result v7

    const/4 v8, 0x7

    if-ne v7, v8, :cond_4

    .line 452
    iget-object v7, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    iget v8, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mRemoteAccount:I

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1}, Lcom/google/android/music/sync/google/model/Track;->getNormalizedNautilusId()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    invoke-static {v7, v8, v9, v10}, Lcom/google/android/music/store/MusicFile;->readMusicFile(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/music/store/MusicFile;)Lcom/google/android/music/store/MusicFile;

    move-result-object v5

    .line 455
    .local v5, "musicFile":Lcom/google/android/music/store/MusicFile;
    if-eqz v5, :cond_4

    .line 456
    invoke-static {v5}, Lcom/google/android/music/sync/google/model/Track;->parse(Lcom/google/android/music/store/MusicFile;)Lcom/google/android/music/sync/google/model/Track;

    move-result-object p2

    .line 460
    .end local v5    # "musicFile":Lcom/google/android/music/store/MusicFile;
    :cond_4
    if-nez p2, :cond_8

    .line 461
    iget-object v7, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mMusicFile:Lcom/google/android/music/store/MusicFile;

    invoke-virtual {v7}, Lcom/google/android/music/store/MusicFile;->reset()V

    .line 462
    iget-object v7, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mMusicFile:Lcom/google/android/music/store/MusicFile;

    invoke-virtual {p1, v7}, Lcom/google/android/music/sync/google/model/Track;->formatAsMusicFile(Lcom/google/android/music/store/MusicFile;)Lcom/google/android/music/store/MusicFile;

    .line 464
    iget-object v7, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mMusicFile:Lcom/google/android/music/store/MusicFile;

    iget v8, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mRemoteAccount:I

    invoke-virtual {v7, v8}, Lcom/google/android/music/store/MusicFile;->setSourceAccount(I)V

    .line 465
    iget-boolean v7, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mUseVerboseLogging:Z

    if-eqz v7, :cond_5

    .line 466
    iget-object v7, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mTag:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Inserting track "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p1}, Lcom/google/android/music/sync/google/model/Track;->getEffectiveRemoteId()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " at version "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-wide v10, p1, Lcom/google/android/music/sync/google/model/Track;->mLastModifiedTimestamp:J

    invoke-virtual {v8, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 470
    :cond_5
    iget-object v7, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mInsertTrackStatement:Landroid/database/sqlite/SQLiteStatement;

    if-nez v7, :cond_6

    .line 471
    iget-object v7, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    invoke-static {v7}, Lcom/google/android/music/store/MusicFile;->compileMusicInsertStatement(Landroid/database/sqlite/SQLiteDatabase;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v7

    iput-object v7, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mInsertTrackStatement:Landroid/database/sqlite/SQLiteStatement;

    .line 473
    :cond_6
    iget-object v7, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mMusicFile:Lcom/google/android/music/store/MusicFile;

    iget-object v8, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mInsertTrackStatement:Landroid/database/sqlite/SQLiteStatement;

    invoke-virtual {v7, v8}, Lcom/google/android/music/store/MusicFile;->insertMusicFile(Landroid/database/sqlite/SQLiteStatement;)J

    .line 475
    iget-object v7, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mMusicFile:Lcom/google/android/music/store/MusicFile;

    invoke-virtual {v7}, Lcom/google/android/music/store/MusicFile;->isPurchasedTrack()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    iget-object v7, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mMusicFile:Lcom/google/android/music/store/MusicFile;

    invoke-virtual {v7}, Lcom/google/android/music/store/MusicFile;->getAddedTime()J

    move-result-wide v10

    sub-long/2addr v8, v10

    const-wide/32 v10, 0x112a880

    cmp-long v7, v8, v10

    if-gez v7, :cond_2

    .line 478
    iget-object v7, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mProtocolState:Ljava/util/Map;

    const-string v8, "new_purchased_count"

    invoke-interface {v7, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v7}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v6

    .line 481
    .local v6, "newTrackCount":I
    const/4 v7, 0x1

    if-ne v6, v7, :cond_7

    .line 483
    iget-object v7, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mProtocolState:Ljava/util/Map;

    const-string v8, "new_purchased_album_name"

    iget-object v9, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mMusicFile:Lcom/google/android/music/store/MusicFile;

    invoke-virtual {v9}, Lcom/google/android/music/store/MusicFile;->getAlbumName()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v7, v8, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 485
    iget-object v7, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mProtocolState:Ljava/util/Map;

    const-string v8, "new_purchased_artist_name"

    iget-object v9, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mMusicFile:Lcom/google/android/music/store/MusicFile;

    invoke-virtual {v9}, Lcom/google/android/music/store/MusicFile;->getAlbumArtist()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v7, v8, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 487
    iget-object v7, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mProtocolState:Ljava/util/Map;

    const-string v8, "new_purchased_albumId"

    iget-object v9, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mMusicFile:Lcom/google/android/music/store/MusicFile;

    invoke-virtual {v9}, Lcom/google/android/music/store/MusicFile;->getAlbumId()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-interface {v7, v8, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 489
    iget-object v7, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mProtocolState:Ljava/util/Map;

    const-string v8, "new_purchased_song_title"

    iget-object v9, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mMusicFile:Lcom/google/android/music/store/MusicFile;

    invoke-virtual {v9}, Lcom/google/android/music/store/MusicFile;->getTitle()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v7, v8, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/android/music/store/InvalidDataException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 522
    .end local v6    # "newTrackCount":I
    :catch_0
    move-exception v0

    .line 523
    .local v0, "e":Lcom/google/android/music/store/InvalidDataException;
    iget-object v7, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mTag:Ljava/lang/String;

    const-string v8, "Unable to merge a track.  Skipping entry."

    invoke-static {v7, v8, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 524
    const/4 v7, 0x0

    goto/16 :goto_1

    .line 493
    .end local v0    # "e":Lcom/google/android/music/store/InvalidDataException;
    .restart local v6    # "newTrackCount":I
    :cond_7
    :try_start_1
    iget-object v7, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mProtocolState:Ljava/util/Map;

    const-string v8, "new_purchased_albumId"

    invoke-interface {v7, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Long;

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 495
    .local v2, "existingAlbumId":J
    iget-object v7, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mMusicFile:Lcom/google/android/music/store/MusicFile;

    invoke-virtual {v7}, Lcom/google/android/music/store/MusicFile;->getAlbumId()J

    move-result-wide v8

    cmp-long v7, v2, v8

    if-eqz v7, :cond_2

    .line 496
    iget-object v7, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mProtocolState:Ljava/util/Map;

    const-string v8, "new_purchased_same_album"

    const/4 v9, 0x0

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    invoke-interface {v7, v8, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 505
    .end local v2    # "existingAlbumId":J
    .end local v6    # "newTrackCount":I
    :cond_8
    invoke-virtual {p2}, Lcom/google/android/music/sync/google/model/Track;->getEncapsulatedMusicFile()Lcom/google/android/music/store/MusicFile;

    move-result-object v1

    .line 506
    .local v1, "file":Lcom/google/android/music/store/MusicFile;
    invoke-virtual {p1, v1}, Lcom/google/android/music/sync/google/model/Track;->formatAsMusicFile(Lcom/google/android/music/store/MusicFile;)Lcom/google/android/music/store/MusicFile;

    .line 507
    iget v7, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mRemoteAccount:I

    invoke-virtual {v1, v7}, Lcom/google/android/music/store/MusicFile;->setSourceAccount(I)V

    .line 509
    iget-boolean v7, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mUseVerboseLogging:Z

    if-eqz v7, :cond_9

    .line 510
    iget-object v7, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mTag:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Updating track "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p1}, Lcom/google/android/music/sync/google/model/Track;->getEffectiveRemoteId()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " at version "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-wide v10, p1, Lcom/google/android/music/sync/google/model/Track;->mLastModifiedTimestamp:J

    invoke-virtual {v8, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " with local id "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v1}, Lcom/google/android/music/store/MusicFile;->getLocalId()J

    move-result-wide v10

    invoke-virtual {v8, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 515
    :cond_9
    iget-object v7, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mUpdateTrackStatement:Landroid/database/sqlite/SQLiteStatement;

    if-nez v7, :cond_a

    .line 516
    iget-object v7, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    invoke-static {v7}, Lcom/google/android/music/store/MusicFile;->compileFullUpdateStatement(Landroid/database/sqlite/SQLiteDatabase;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v7

    iput-object v7, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mUpdateTrackStatement:Landroid/database/sqlite/SQLiteStatement;

    .line 518
    :cond_a
    iget-object v7, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mUpdateTrackStatement:Landroid/database/sqlite/SQLiteStatement;

    iget-object v8, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1, v7, v8}, Lcom/google/android/music/store/MusicFile;->updateMusicFile(Landroid/database/sqlite/SQLiteStatement;Landroid/database/sqlite/SQLiteDatabase;)V
    :try_end_1
    .catch Lcom/google/android/music/store/InvalidDataException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method


# virtual methods
.method public cleanupLocallyCachedFiles()V
    .locals 3

    .prologue
    .line 629
    iget-object v1, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mCacheFilepaths:Ljava/util/List;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mCacheFilepaths:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 633
    iget-object v1, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mCacheFilepaths:Ljava/util/List;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 634
    .local v0, "filesToDelete":[Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mCacheFilepaths:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 636
    sget-object v1, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    new-instance v2, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger$1;

    invoke-direct {v2, p0, v0}, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger$1;-><init>(Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;[Ljava/lang/String;)V

    invoke-static {v1, v2}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsync(Lcom/google/android/music/utils/LoggableHandler;Ljava/lang/Runnable;)V

    .line 651
    .end local v0    # "filesToDelete":[Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public processMergeItem(Lcom/google/android/music/sync/common/QueueableSyncEntity;Lcom/google/android/music/sync/common/QueueableSyncEntity;)Z
    .locals 2
    .param p1, "serverEntity"    # Lcom/google/android/music/sync/common/QueueableSyncEntity;
    .param p2, "clientEntity"    # Lcom/google/android/music/sync/common/QueueableSyncEntity;

    .prologue
    .line 255
    instance-of v0, p1, Lcom/google/android/music/sync/google/model/Track;

    if-eqz v0, :cond_0

    .line 256
    const-class v0, Lcom/google/android/music/sync/google/model/Track;

    invoke-virtual {v0, p1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/sync/google/model/Track;

    const-class v1, Lcom/google/android/music/sync/google/model/Track;

    invoke-virtual {v1, p2}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/sync/google/model/Track;

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->processMergeTrackImpl(Lcom/google/android/music/sync/google/model/Track;Lcom/google/android/music/sync/google/model/Track;)Z

    move-result v0

    .line 271
    :goto_0
    return v0

    .line 258
    :cond_0
    instance-of v0, p1, Lcom/google/android/music/sync/google/model/SyncablePlaylist;

    if-eqz v0, :cond_1

    .line 259
    const-class v0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;

    invoke-virtual {v0, p1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;

    const-class v1, Lcom/google/android/music/sync/google/model/SyncablePlaylist;

    invoke-virtual {v1, p2}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/sync/google/model/SyncablePlaylist;

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->processMergePlaylistImpl(Lcom/google/android/music/sync/google/model/SyncablePlaylist;Lcom/google/android/music/sync/google/model/SyncablePlaylist;)Z

    move-result v0

    goto :goto_0

    .line 261
    :cond_1
    instance-of v0, p1, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;

    if-eqz v0, :cond_2

    .line 262
    const-class v0, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;

    invoke-virtual {v0, p1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;

    const-class v1, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;

    invoke-virtual {v1, p2}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->processMergeEntryImpl(Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;)Z

    move-result v0

    goto :goto_0

    .line 264
    :cond_2
    instance-of v0, p1, Lcom/google/android/music/sync/google/model/SyncableRadioStation;

    if-eqz v0, :cond_3

    .line 265
    const-class v0, Lcom/google/android/music/sync/google/model/SyncableRadioStation;

    invoke-virtual {v0, p1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/sync/google/model/SyncableRadioStation;

    const-class v1, Lcom/google/android/music/sync/google/model/SyncableRadioStation;

    invoke-virtual {v1, p2}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/sync/google/model/SyncableRadioStation;

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->processMergeRadioStationImpl(Lcom/google/android/music/sync/google/model/SyncableRadioStation;Lcom/google/android/music/sync/google/model/SyncableRadioStation;)Z

    move-result v0

    goto :goto_0

    .line 267
    :cond_3
    instance-of v0, p1, Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;

    if-eqz v0, :cond_4

    .line 268
    const-class v0, Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;

    invoke-virtual {v0, p1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;

    const-class v1, Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;

    invoke-virtual {v1, p2}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->processMergeBlacklistItemImpl(Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;)Z

    move-result v0

    goto :goto_0

    .line 271
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public safelyCloseStatements()V
    .locals 1

    .prologue
    .line 611
    iget-object v0, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mInsertTrackStatement:Landroid/database/sqlite/SQLiteStatement;

    invoke-static {v0}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/sqlite/SQLiteProgram;)V

    .line 612
    iget-object v0, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mUpdateTrackStatement:Landroid/database/sqlite/SQLiteStatement;

    invoke-static {v0}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/sqlite/SQLiteProgram;)V

    .line 613
    iget-object v0, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mInsertPlaylistStatement:Landroid/database/sqlite/SQLiteStatement;

    invoke-static {v0}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/sqlite/SQLiteProgram;)V

    .line 614
    iget-object v0, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mUpdatePlaylistStatement:Landroid/database/sqlite/SQLiteStatement;

    invoke-static {v0}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/sqlite/SQLiteProgram;)V

    .line 615
    iget-object v0, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mDeletePlaylistStatement:Landroid/database/sqlite/SQLiteStatement;

    invoke-static {v0}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/sqlite/SQLiteProgram;)V

    .line 616
    iget-object v0, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mInsertPlentryStatement:Landroid/database/sqlite/SQLiteStatement;

    invoke-static {v0}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/sqlite/SQLiteProgram;)V

    .line 617
    iget-object v0, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mUpdatePlentryStatement:Landroid/database/sqlite/SQLiteStatement;

    invoke-static {v0}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/sqlite/SQLiteProgram;)V

    .line 618
    iget-object v0, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mDeletePlentryStatement:Landroid/database/sqlite/SQLiteStatement;

    invoke-static {v0}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/sqlite/SQLiteProgram;)V

    .line 619
    iget-object v0, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mInsertRadioStationStatement:Landroid/database/sqlite/SQLiteStatement;

    invoke-static {v0}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/sqlite/SQLiteProgram;)V

    .line 620
    iget-object v0, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mUpdateRadioStationStatement:Landroid/database/sqlite/SQLiteStatement;

    invoke-static {v0}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/sqlite/SQLiteProgram;)V

    .line 621
    iget-object v0, p0, Lcom/google/android/music/sync/google/MusicDownstreamMerger$MusicBlockMerger;->mInsertBlacklistItemStatement:Landroid/database/sqlite/SQLiteStatement;

    invoke-static {v0}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/sqlite/SQLiteProgram;)V

    .line 622
    return-void
.end method
