.class Lcom/google/android/music/ui/SearchActivity$4;
.super Ljava/lang/Object;
.source "SearchActivity.java"

# interfaces
.implements Lcom/google/android/music/utils/async/AsyncRunner;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/SearchActivity;->handleSearchResult(Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final cols:[Ljava/lang/String;

.field mArtistName:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/music/ui/SearchActivity;

.field final synthetic val$artistId:J

.field final synthetic val$shouldAutoPlay:Z


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/SearchActivity;JZ)V
    .locals 4

    .prologue
    .line 452
    iput-object p1, p0, Lcom/google/android/music/ui/SearchActivity$4;->this$0:Lcom/google/android/music/ui/SearchActivity;

    iput-wide p2, p0, Lcom/google/android/music/ui/SearchActivity$4;->val$artistId:J

    iput-boolean p4, p0, Lcom/google/android/music/ui/SearchActivity$4;->val$shouldAutoPlay:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 454
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "artist"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/google/android/music/ui/SearchActivity$4;->cols:[Ljava/lang/String;

    .line 458
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/ui/SearchActivity$4;->mArtistName:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public backgroundTask()V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 461
    iget-object v0, p0, Lcom/google/android/music/ui/SearchActivity$4;->this$0:Lcom/google/android/music/ui/SearchActivity;

    iget-wide v4, p0, Lcom/google/android/music/ui/SearchActivity$4;->val$artistId:J

    invoke-static {v4, v5}, Lcom/google/android/music/store/MusicContent$Artists;->getArtistsUri(J)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/SearchActivity$4;->cols:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 465
    .local v6, "c":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 466
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/SearchActivity$4;->mArtistName:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 469
    :cond_0
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 471
    return-void

    .line 469
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method public taskCompleted()V
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 474
    iget-object v0, p0, Lcom/google/android/music/ui/SearchActivity$4;->this$0:Lcom/google/android/music/ui/SearchActivity;

    iget-wide v2, p0, Lcom/google/android/music/ui/SearchActivity$4;->val$artistId:J

    iget-object v4, p0, Lcom/google/android/music/ui/SearchActivity$4;->mArtistName:Ljava/lang/String;

    invoke-static {v0, v2, v3, v4, v5}, Lcom/google/android/music/ui/ArtistPageActivity;->showArtist(Landroid/content/Context;JLjava/lang/String;Z)V

    .line 476
    iget-boolean v0, p0, Lcom/google/android/music/ui/SearchActivity$4;->val$shouldAutoPlay:Z

    if-eqz v0, :cond_0

    .line 477
    new-instance v1, Lcom/google/android/music/medialist/ArtistSongList;

    iget-wide v2, p0, Lcom/google/android/music/ui/SearchActivity$4;->val$artistId:J

    iget-object v4, p0, Lcom/google/android/music/ui/SearchActivity$4;->mArtistName:Ljava/lang/String;

    move v6, v5

    invoke-direct/range {v1 .. v6}, Lcom/google/android/music/medialist/ArtistSongList;-><init>(JLjava/lang/String;IZ)V

    .line 479
    .local v1, "artistSongList":Lcom/google/android/music/medialist/SongList;
    iget-object v0, p0, Lcom/google/android/music/ui/SearchActivity$4;->this$0:Lcom/google/android/music/ui/SearchActivity;

    iget-object v2, p0, Lcom/google/android/music/ui/SearchActivity$4;->this$0:Lcom/google/android/music/ui/SearchActivity;

    invoke-virtual {v2}, Lcom/google/android/music/ui/SearchActivity;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/music/utils/MusicUtils;->playArtistShuffle(Landroid/content/Context;Lcom/google/android/music/medialist/SongList;Lcom/google/android/music/preferences/MusicPreferences;)V

    .line 482
    .end local v1    # "artistSongList":Lcom/google/android/music/medialist/SongList;
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/ui/SearchActivity$4;->this$0:Lcom/google/android/music/ui/SearchActivity;

    invoke-virtual {v0}, Lcom/google/android/music/ui/SearchActivity;->finish()V

    .line 483
    return-void
.end method
