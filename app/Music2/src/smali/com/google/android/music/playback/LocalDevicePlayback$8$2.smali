.class Lcom/google/android/music/playback/LocalDevicePlayback$8$2;
.super Ljava/lang/Object;
.source "LocalDevicePlayback.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/playback/LocalDevicePlayback$8;->handleMessage(Landroid/os/Message;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/music/playback/LocalDevicePlayback$8;


# direct methods
.method constructor <init>(Lcom/google/android/music/playback/LocalDevicePlayback$8;)V
    .locals 0

    .prologue
    .line 972
    iput-object p1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$8$2;->this$1:Lcom/google/android/music/playback/LocalDevicePlayback$8;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 976
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$8$2;->this$1:Lcom/google/android/music/playback/LocalDevicePlayback$8;

    iget-object v0, v0, Lcom/google/android/music/playback/LocalDevicePlayback$8;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    invoke-virtual {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->isPlaying()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$8$2;->this$1:Lcom/google/android/music/playback/LocalDevicePlayback$8;

    iget-object v0, v0, Lcom/google/android/music/playback/LocalDevicePlayback$8;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mPausedByTransientLossOfFocus:Z
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$4400(Lcom/google/android/music/playback/LocalDevicePlayback;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 977
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$8$2;->this$1:Lcom/google/android/music/playback/LocalDevicePlayback$8;

    iget-object v0, v0, Lcom/google/android/music/playback/LocalDevicePlayback$8;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaplayerHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$2000(Lcom/google/android/music/playback/LocalDevicePlayback;)Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 984
    :goto_0
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$8$2;->this$1:Lcom/google/android/music/playback/LocalDevicePlayback$8;

    iget-object v0, v0, Lcom/google/android/music/playback/LocalDevicePlayback$8;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mAsyncWakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$1400(Lcom/google/android/music/playback/LocalDevicePlayback;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 986
    return-void

    .line 980
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$8$2;->this$1:Lcom/google/android/music/playback/LocalDevicePlayback$8;

    iget-object v0, v0, Lcom/google/android/music/playback/LocalDevicePlayback$8;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaplayerHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$2000(Lcom/google/android/music/playback/LocalDevicePlayback;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 981
    iget-object v0, p0, Lcom/google/android/music/playback/LocalDevicePlayback$8$2;->this$1:Lcom/google/android/music/playback/LocalDevicePlayback$8;

    iget-object v0, v0, Lcom/google/android/music/playback/LocalDevicePlayback$8;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mMediaplayerHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$2000(Lcom/google/android/music/playback/LocalDevicePlayback;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 984
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$8$2;->this$1:Lcom/google/android/music/playback/LocalDevicePlayback$8;

    iget-object v1, v1, Lcom/google/android/music/playback/LocalDevicePlayback$8;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mAsyncWakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {v1}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$1400(Lcom/google/android/music/playback/LocalDevicePlayback;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    throw v0
.end method
