.class Lcom/google/android/music/ui/NowPlayingScreenFragment$NowPlayingHeaderPageAdapter;
.super Landroid/support/v4/app/FragmentStatePagerAdapter;
.source "NowPlayingScreenFragment.java"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/NowPlayingScreenFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "NowPlayingHeaderPageAdapter"
.end annotation


# instance fields
.field private mButtonsHidden:Z

.field private mPageScrolling:Z

.field private mPager:Landroid/support/v4/view/ViewPager;

.field final synthetic this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/music/ui/NowPlayingScreenFragment;Landroid/support/v4/view/ViewPager;)V
    .locals 1
    .param p2, "pager"    # Landroid/support/v4/view/ViewPager;

    .prologue
    .line 2024
    iput-object p1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$NowPlayingHeaderPageAdapter;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    .line 2025
    invoke-virtual {p1}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/support/v4/app/FragmentStatePagerAdapter;-><init>(Landroid/support/v4/app/FragmentManager;)V

    .line 2026
    iput-object p2, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$NowPlayingHeaderPageAdapter;->mPager:Landroid/support/v4/view/ViewPager;

    .line 2027
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$NowPlayingHeaderPageAdapter;->mPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p0}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 2028
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$NowPlayingHeaderPageAdapter;->mPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p0}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 2029
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 2049
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$NowPlayingHeaderPageAdapter;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # invokes: Lcom/google/android/music/ui/NowPlayingScreenFragment;->getPageCount()I
    invoke-static {v0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$3000(Lcom/google/android/music/ui/NowPlayingScreenFragment;)I

    move-result v0

    return v0
.end method

.method public getItem(I)Landroid/support/v4/app/Fragment;
    .locals 24
    .param p1, "position"    # I

    .prologue
    .line 2056
    invoke-static {}, Lcom/google/android/music/utils/MusicUtils;->assertUiThread()V

    .line 2058
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/ui/NowPlayingScreenFragment$NowPlayingHeaderPageAdapter;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # getter for: Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueCursor:Landroid/database/Cursor;
    invoke-static {v2}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$2900(Lcom/google/android/music/ui/NowPlayingScreenFragment;)Landroid/database/Cursor;

    move-result-object v2

    if-eqz v2, :cond_5

    .line 2059
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/ui/NowPlayingScreenFragment$NowPlayingHeaderPageAdapter;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # getter for: Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueCursor:Landroid/database/Cursor;
    invoke-static {v2}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$2900(Lcom/google/android/music/ui/NowPlayingScreenFragment;)Landroid/database/Cursor;

    move-result-object v2

    invoke-interface {v2}, Landroid/database/Cursor;->getPosition()I

    move-result v21

    .line 2061
    .local v21, "oldPos":I
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/ui/NowPlayingScreenFragment$NowPlayingHeaderPageAdapter;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # getter for: Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueCursor:Landroid/database/Cursor;
    invoke-static {v2}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$2900(Lcom/google/android/music/ui/NowPlayingScreenFragment;)Landroid/database/Cursor;

    move-result-object v2

    move/from16 v0, p1

    invoke-interface {v2, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2062
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/ui/NowPlayingScreenFragment$NowPlayingHeaderPageAdapter;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # getter for: Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueCursor:Landroid/database/Cursor;
    invoke-static {v2}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$2900(Lcom/google/android/music/ui/NowPlayingScreenFragment;)Landroid/database/Cursor;

    move-result-object v2

    const-string v10, "title"

    invoke-interface {v2, v10}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v22

    .line 2064
    .local v22, "titleIdx":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/ui/NowPlayingScreenFragment$NowPlayingHeaderPageAdapter;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # getter for: Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueCursor:Landroid/database/Cursor;
    invoke-static {v2}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$2900(Lcom/google/android/music/ui/NowPlayingScreenFragment;)Landroid/database/Cursor;

    move-result-object v2

    const-string v10, "artist"

    invoke-interface {v2, v10}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v20

    .line 2066
    .local v20, "artistNameIdx":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/ui/NowPlayingScreenFragment$NowPlayingHeaderPageAdapter;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # getter for: Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueCursor:Landroid/database/Cursor;
    invoke-static {v2}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$2900(Lcom/google/android/music/ui/NowPlayingScreenFragment;)Landroid/database/Cursor;

    move-result-object v2

    const-string v10, "album_id"

    invoke-interface {v2, v10}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v18

    .line 2068
    .local v18, "albumIdIdx":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/ui/NowPlayingScreenFragment$NowPlayingHeaderPageAdapter;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # getter for: Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueCursor:Landroid/database/Cursor;
    invoke-static {v2}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$2900(Lcom/google/android/music/ui/NowPlayingScreenFragment;)Landroid/database/Cursor;

    move-result-object v2

    const-string v10, "album"

    invoke-interface {v2, v10}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v19

    .line 2070
    .local v19, "albumNameIdx":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/ui/NowPlayingScreenFragment$NowPlayingHeaderPageAdapter;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # getter for: Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueCursor:Landroid/database/Cursor;
    invoke-static {v2}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$2900(Lcom/google/android/music/ui/NowPlayingScreenFragment;)Landroid/database/Cursor;

    move-result-object v2

    const-string v10, "Vid"

    invoke-interface {v2, v10}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v23

    .line 2073
    .local v23, "videoIdIdx":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/ui/NowPlayingScreenFragment$NowPlayingHeaderPageAdapter;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # getter for: Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueCursor:Landroid/database/Cursor;
    invoke-static {v2}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$2900(Lcom/google/android/music/ui/NowPlayingScreenFragment;)Landroid/database/Cursor;

    move-result-object v2

    move/from16 v0, v22

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 2074
    .local v3, "title":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/ui/NowPlayingScreenFragment$NowPlayingHeaderPageAdapter;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # getter for: Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueCursor:Landroid/database/Cursor;
    invoke-static {v2}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$2900(Lcom/google/android/music/ui/NowPlayingScreenFragment;)Landroid/database/Cursor;

    move-result-object v2

    move/from16 v0, v19

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 2075
    .local v4, "albumName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/ui/NowPlayingScreenFragment$NowPlayingHeaderPageAdapter;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # getter for: Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueCursor:Landroid/database/Cursor;
    invoke-static {v2}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$2900(Lcom/google/android/music/ui/NowPlayingScreenFragment;)Landroid/database/Cursor;

    move-result-object v2

    move/from16 v0, v20

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 2076
    .local v5, "artistName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/ui/NowPlayingScreenFragment$NowPlayingHeaderPageAdapter;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # getter for: Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueCursor:Landroid/database/Cursor;
    invoke-static {v2}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$2900(Lcom/google/android/music/ui/NowPlayingScreenFragment;)Landroid/database/Cursor;

    move-result-object v2

    move/from16 v0, v18

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 2080
    .local v6, "albumId":J
    const/4 v9, 0x0

    .line 2081
    .local v9, "videoId":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/ui/NowPlayingScreenFragment$NowPlayingHeaderPageAdapter;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # getter for: Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueCursor:Landroid/database/Cursor;
    invoke-static {v2}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$2900(Lcom/google/android/music/ui/NowPlayingScreenFragment;)Landroid/database/Cursor;

    move-result-object v2

    move/from16 v0, v23

    invoke-interface {v2, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2082
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/ui/NowPlayingScreenFragment$NowPlayingHeaderPageAdapter;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # getter for: Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueCursor:Landroid/database/Cursor;
    invoke-static {v2}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$2900(Lcom/google/android/music/ui/NowPlayingScreenFragment;)Landroid/database/Cursor;

    move-result-object v2

    move/from16 v0, v23

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 2085
    :cond_0
    invoke-static {v4}, Lcom/google/android/music/utils/MusicUtils;->isUnknown(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2086
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/ui/NowPlayingScreenFragment$NowPlayingHeaderPageAdapter;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    invoke-virtual {v2}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const v10, 0x7f0b00c5

    invoke-virtual {v2, v10}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 2088
    :cond_1
    invoke-static {v5}, Lcom/google/android/music/utils/MusicUtils;->isUnknown(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2089
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/ui/NowPlayingScreenFragment$NowPlayingHeaderPageAdapter;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    invoke-virtual {v2}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const v10, 0x7f0b00c4

    invoke-virtual {v2, v10}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 2092
    :cond_2
    const/4 v8, 0x0

    .line 2093
    .local v8, "artUrl":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/ui/NowPlayingScreenFragment$NowPlayingHeaderPageAdapter;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # getter for: Lcom/google/android/music/ui/NowPlayingScreenFragment;->mSongList:Lcom/google/android/music/medialist/SongList;
    invoke-static {v2}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$3100(Lcom/google/android/music/ui/NowPlayingScreenFragment;)Lcom/google/android/music/medialist/SongList;

    move-result-object v2

    instance-of v2, v2, Lcom/google/android/music/medialist/SharedSongList;

    if-eqz v2, :cond_3

    .line 2094
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/ui/NowPlayingScreenFragment$NowPlayingHeaderPageAdapter;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # getter for: Lcom/google/android/music/ui/NowPlayingScreenFragment;->mSongList:Lcom/google/android/music/medialist/SongList;
    invoke-static {v2}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$3100(Lcom/google/android/music/ui/NowPlayingScreenFragment;)Lcom/google/android/music/medialist/SongList;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/medialist/SharedSongList;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/music/ui/NowPlayingScreenFragment$NowPlayingHeaderPageAdapter;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    invoke-virtual {v10}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v10

    invoke-virtual {v2, v10}, Lcom/google/android/music/medialist/SharedSongList;->getAlbumArtUrl(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v8

    :cond_3
    move/from16 v2, p1

    .line 2101
    invoke-static/range {v2 .. v9}, Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;->newInstance(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 2105
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/music/ui/NowPlayingScreenFragment$NowPlayingHeaderPageAdapter;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # getter for: Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueCursor:Landroid/database/Cursor;
    invoke-static {v10}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$2900(Lcom/google/android/music/ui/NowPlayingScreenFragment;)Landroid/database/Cursor;

    move-result-object v10

    move/from16 v0, v21

    invoke-interface {v10, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 2110
    .end local v3    # "title":Ljava/lang/String;
    .end local v4    # "albumName":Ljava/lang/String;
    .end local v5    # "artistName":Ljava/lang/String;
    .end local v6    # "albumId":J
    .end local v8    # "artUrl":Ljava/lang/String;
    .end local v9    # "videoId":Ljava/lang/String;
    .end local v18    # "albumIdIdx":I
    .end local v19    # "albumNameIdx":I
    .end local v20    # "artistNameIdx":I
    .end local v21    # "oldPos":I
    .end local v22    # "titleIdx":I
    .end local v23    # "videoIdIdx":I
    :goto_0
    return-object v2

    .line 2105
    .restart local v21    # "oldPos":I
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/ui/NowPlayingScreenFragment$NowPlayingHeaderPageAdapter;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # getter for: Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueCursor:Landroid/database/Cursor;
    invoke-static {v2}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$2900(Lcom/google/android/music/ui/NowPlayingScreenFragment;)Landroid/database/Cursor;

    move-result-object v2

    move/from16 v0, v21

    invoke-interface {v2, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 2108
    .end local v21    # "oldPos":I
    :cond_5
    const-string v2, "NowPlayingFragment"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Could not extract metadata for queue position "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move/from16 v0, p1

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v2, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2110
    const-string v11, ""

    const-string v12, ""

    const-string v13, ""

    const-wide/16 v14, 0x0

    const-string v16, ""

    const-string v17, ""

    move/from16 v10, p1

    invoke-static/range {v10 .. v17}, Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;->newInstance(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;

    move-result-object v2

    goto :goto_0

    .line 2105
    .restart local v21    # "oldPos":I
    :catchall_0
    move-exception v2

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/music/ui/NowPlayingScreenFragment$NowPlayingHeaderPageAdapter;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # getter for: Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueCursor:Landroid/database/Cursor;
    invoke-static {v10}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$2900(Lcom/google/android/music/ui/NowPlayingScreenFragment;)Landroid/database/Cursor;

    move-result-object v10

    move/from16 v0, v21

    invoke-interface {v10, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    throw v2
.end method

.method public getItemPosition(Ljava/lang/Object;)I
    .locals 1
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 2044
    const/4 v0, -0x2

    return v0
.end method

.method public onPageScrollStateChanged(I)V
    .locals 6
    .param p1, "state"    # I

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2115
    if-nez p1, :cond_1

    .line 2116
    iget-boolean v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$NowPlayingHeaderPageAdapter;->mButtonsHidden:Z

    if-eqz v1, :cond_0

    .line 2117
    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$NowPlayingHeaderPageAdapter;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # getter for: Lcom/google/android/music/ui/NowPlayingScreenFragment;->mHeaderButtonsWrapper:Landroid/view/View;
    invoke-static {v1}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$3300(Lcom/google/android/music/ui/NowPlayingScreenFragment;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->clearAnimation()V

    .line 2118
    new-instance v0, Lcom/google/android/music/animator/StatefulAlphaAnimation;

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v2}, Lcom/google/android/music/animator/StatefulAlphaAnimation;-><init>(FF)V

    .line 2119
    .local v0, "alphaAnim":Lcom/google/android/music/animator/StatefulAlphaAnimation;
    const-wide/16 v2, 0x64

    invoke-virtual {v0, v2, v3}, Lcom/google/android/music/animator/StatefulAlphaAnimation;->setDuration(J)V

    .line 2120
    invoke-virtual {v0, v5}, Lcom/google/android/music/animator/StatefulAlphaAnimation;->setFillAfter(Z)V

    .line 2121
    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$NowPlayingHeaderPageAdapter;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # getter for: Lcom/google/android/music/ui/NowPlayingScreenFragment;->mHeaderButtonsWrapper:Landroid/view/View;
    invoke-static {v1}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$3300(Lcom/google/android/music/ui/NowPlayingScreenFragment;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2122
    iput-boolean v4, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$NowPlayingHeaderPageAdapter;->mPageScrolling:Z

    .line 2123
    iput-boolean v4, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$NowPlayingHeaderPageAdapter;->mButtonsHidden:Z

    .line 2128
    .end local v0    # "alphaAnim":Lcom/google/android/music/animator/StatefulAlphaAnimation;
    :cond_0
    :goto_0
    return-void

    .line 2126
    :cond_1
    iput-boolean v5, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$NowPlayingHeaderPageAdapter;->mPageScrolling:Z

    goto :goto_0
.end method

.method public onPageScrolled(IFI)V
    .locals 9
    .param p1, "position"    # I
    .param p2, "positionOffset"    # F
    .param p3, "positionOffsetPixels"    # I

    .prologue
    const/4 v8, 0x1

    const-wide/high16 v6, 0x3fe0000000000000L    # 0.5

    .line 2132
    iget-boolean v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$NowPlayingHeaderPageAdapter;->mPageScrolling:Z

    if-eqz v1, :cond_2

    iget-boolean v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$NowPlayingHeaderPageAdapter;->mButtonsHidden:Z

    if-nez v1, :cond_2

    .line 2133
    float-to-double v2, p2

    const-wide v4, 0x3f847ae147ae147bL    # 0.01

    cmpl-double v1, v2, v4

    if-lez v1, :cond_0

    float-to-double v2, p2

    cmpg-double v1, v2, v6

    if-ltz v1, :cond_1

    :cond_0
    float-to-double v2, p2

    const-wide v4, 0x3fefae147ae147aeL    # 0.99

    cmpg-double v1, v2, v4

    if-gez v1, :cond_2

    float-to-double v2, p2

    cmpl-double v1, v2, v6

    if-lez v1, :cond_2

    .line 2137
    :cond_1
    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$NowPlayingHeaderPageAdapter;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # getter for: Lcom/google/android/music/ui/NowPlayingScreenFragment;->mHeaderButtonsWrapper:Landroid/view/View;
    invoke-static {v1}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$3300(Lcom/google/android/music/ui/NowPlayingScreenFragment;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->clearAnimation()V

    .line 2138
    new-instance v0, Lcom/google/android/music/animator/StatefulAlphaAnimation;

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/music/animator/StatefulAlphaAnimation;-><init>(FF)V

    .line 2139
    .local v0, "alphaAnim":Lcom/google/android/music/animator/StatefulAlphaAnimation;
    const-wide/16 v2, 0x64

    invoke-virtual {v0, v2, v3}, Lcom/google/android/music/animator/StatefulAlphaAnimation;->setDuration(J)V

    .line 2140
    invoke-virtual {v0, v8}, Lcom/google/android/music/animator/StatefulAlphaAnimation;->setFillAfter(Z)V

    .line 2141
    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$NowPlayingHeaderPageAdapter;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # getter for: Lcom/google/android/music/ui/NowPlayingScreenFragment;->mHeaderButtonsWrapper:Landroid/view/View;
    invoke-static {v1}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$3300(Lcom/google/android/music/ui/NowPlayingScreenFragment;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2142
    iput-boolean v8, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$NowPlayingHeaderPageAdapter;->mButtonsHidden:Z

    .line 2145
    .end local v0    # "alphaAnim":Lcom/google/android/music/animator/StatefulAlphaAnimation;
    :cond_2
    return-void
.end method

.method public onPageSelected(I)V
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 2149
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$NowPlayingHeaderPageAdapter;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # invokes: Lcom/google/android/music/ui/NowPlayingScreenFragment;->handlePageSelected(I)V
    invoke-static {v0, p1}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$3200(Lcom/google/android/music/ui/NowPlayingScreenFragment;I)V

    .line 2150
    return-void
.end method

.method public saveState()Landroid/os/Parcelable;
    .locals 1

    .prologue
    .line 2035
    const/4 v0, 0x0

    return-object v0
.end method
