.class public Lcom/google/android/music/download/ContentIdentifier;
.super Ljava/lang/Object;
.source "ContentIdentifier.java"

# interfaces
.implements Landroid/os/Parcelable;
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/download/ContentIdentifier$Domain;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/google/android/music/download/ContentIdentifier;",
        ">;"
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/music/download/ContentIdentifier;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mDomain:Lcom/google/android/music/download/ContentIdentifier$Domain;

.field private final mId:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 195
    new-instance v0, Lcom/google/android/music/download/ContentIdentifier$1;

    invoke-direct {v0}, Lcom/google/android/music/download/ContentIdentifier$1;-><init>()V

    sput-object v0, Lcom/google/android/music/download/ContentIdentifier;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(JLcom/google/android/music/download/ContentIdentifier$Domain;)V
    .locals 1
    .param p1, "id"    # J
    .param p3, "domain"    # Lcom/google/android/music/download/ContentIdentifier$Domain;

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    iput-wide p1, p0, Lcom/google/android/music/download/ContentIdentifier;->mId:J

    .line 63
    iput-object p3, p0, Lcom/google/android/music/download/ContentIdentifier;->mDomain:Lcom/google/android/music/download/ContentIdentifier$Domain;

    .line 64
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "p"    # Landroid/os/Parcel;

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/music/download/ContentIdentifier;->mId:J

    .line 68
    invoke-static {}, Lcom/google/android/music/download/ContentIdentifier$Domain;->values()[Lcom/google/android/music/download/ContentIdentifier$Domain;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/google/android/music/download/ContentIdentifier;->mDomain:Lcom/google/android/music/download/ContentIdentifier$Domain;

    .line 69
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/google/android/music/download/ContentIdentifier$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/google/android/music/download/ContentIdentifier$1;

    .prologue
    .line 13
    invoke-direct {p0, p1}, Lcom/google/android/music/download/ContentIdentifier;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public compareTo(Lcom/google/android/music/download/ContentIdentifier;)I
    .locals 4
    .param p1, "obj"    # Lcom/google/android/music/download/ContentIdentifier;

    .prologue
    .line 208
    new-instance v0, Ljava/lang/Long;

    iget-wide v2, p0, Lcom/google/android/music/download/ContentIdentifier;->mId:J

    invoke-direct {v0, v2, v3}, Ljava/lang/Long;-><init>(J)V

    iget-wide v2, p1, Lcom/google/android/music/download/ContentIdentifier;->mId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Long;->compareTo(Ljava/lang/Long;)I

    move-result v0

    return v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 13
    check-cast p1, Lcom/google/android/music/download/ContentIdentifier;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/music/download/ContentIdentifier;->compareTo(Lcom/google/android/music/download/ContentIdentifier;)I

    move-result v0

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 164
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Lcom/google/android/music/download/ContentIdentifier;)Z
    .locals 6
    .param p1, "id"    # Lcom/google/android/music/download/ContentIdentifier;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 186
    if-nez p1, :cond_1

    move v0, v1

    .line 192
    :cond_0
    :goto_0
    return v0

    .line 189
    :cond_1
    if-eq p0, p1, :cond_0

    .line 192
    iget-wide v2, p1, Lcom/google/android/music/download/ContentIdentifier;->mId:J

    iget-wide v4, p0, Lcom/google/android/music/download/ContentIdentifier;->mId:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    iget-object v2, p1, Lcom/google/android/music/download/ContentIdentifier;->mDomain:Lcom/google/android/music/download/ContentIdentifier$Domain;

    iget-object v3, p0, Lcom/google/android/music/download/ContentIdentifier;->mDomain:Lcom/google/android/music/download/ContentIdentifier$Domain;

    if-eq v2, v3, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v0, 0x0

    .line 179
    instance-of v1, p1, Lcom/google/android/music/download/ContentIdentifier;

    if-eqz v1, :cond_1

    .line 180
    if-eq p0, p1, :cond_0

    check-cast p1, Lcom/google/android/music/download/ContentIdentifier;

    .end local p1    # "o":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/music/download/ContentIdentifier;->equals(Lcom/google/android/music/download/ContentIdentifier;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 182
    :cond_1
    return v0
.end method

.method public getDomain()Lcom/google/android/music/download/ContentIdentifier$Domain;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/music/download/ContentIdentifier;->mDomain:Lcom/google/android/music/download/ContentIdentifier$Domain;

    return-object v0
.end method

.method public getId()J
    .locals 2

    .prologue
    .line 72
    iget-wide v0, p0, Lcom/google/android/music/download/ContentIdentifier;->mId:J

    return-wide v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 174
    iget-wide v0, p0, Lcom/google/android/music/download/ContentIdentifier;->mId:J

    const-wide/16 v2, 0xa

    mul-long/2addr v0, v2

    iget-object v2, p0, Lcom/google/android/music/download/ContentIdentifier;->mDomain:Lcom/google/android/music/download/ContentIdentifier$Domain;

    invoke-virtual {v2}, Lcom/google/android/music/download/ContentIdentifier$Domain;->ordinal()I

    move-result v2

    int-to-long v2, v2

    add-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method public isCacheable()Z
    .locals 2

    .prologue
    .line 96
    iget-object v0, p0, Lcom/google/android/music/download/ContentIdentifier;->mDomain:Lcom/google/android/music/download/ContentIdentifier$Domain;

    sget-object v1, Lcom/google/android/music/download/ContentIdentifier$Domain;->DEFAULT:Lcom/google/android/music/download/ContentIdentifier$Domain;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/music/download/ContentIdentifier;->mDomain:Lcom/google/android/music/download/ContentIdentifier$Domain;

    sget-object v1, Lcom/google/android/music/download/ContentIdentifier$Domain;->NAUTILUS:Lcom/google/android/music/download/ContentIdentifier$Domain;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isDefaultDomain()Z
    .locals 2

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/music/download/ContentIdentifier;->mDomain:Lcom/google/android/music/download/ContentIdentifier$Domain;

    sget-object v1, Lcom/google/android/music/download/ContentIdentifier$Domain;->DEFAULT:Lcom/google/android/music/download/ContentIdentifier$Domain;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isNautilusDomain()Z
    .locals 2

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/music/download/ContentIdentifier;->mDomain:Lcom/google/android/music/download/ContentIdentifier$Domain;

    sget-object v1, Lcom/google/android/music/download/ContentIdentifier$Domain;->NAUTILUS:Lcom/google/android/music/download/ContentIdentifier$Domain;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSharedDomain()Z
    .locals 2

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/music/download/ContentIdentifier;->mDomain:Lcom/google/android/music/download/ContentIdentifier$Domain;

    sget-object v1, Lcom/google/android/music/download/ContentIdentifier$Domain;->SHARED:Lcom/google/android/music/download/ContentIdentifier$Domain;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toFileSystemString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 141
    iget-object v0, p0, Lcom/google/android/music/download/ContentIdentifier;->mDomain:Lcom/google/android/music/download/ContentIdentifier$Domain;

    sget-object v1, Lcom/google/android/music/download/ContentIdentifier$Domain;->DEFAULT:Lcom/google/android/music/download/ContentIdentifier$Domain;

    if-ne v0, v1, :cond_0

    .line 142
    iget-wide v0, p0, Lcom/google/android/music/download/ContentIdentifier;->mId:J

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    .line 144
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/music/download/ContentIdentifier;->toUrlString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 101
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 102
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 103
    iget-wide v2, p0, Lcom/google/android/music/download/ContentIdentifier;->mId:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 104
    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 105
    iget-object v1, p0, Lcom/google/android/music/download/ContentIdentifier;->mDomain:Lcom/google/android/music/download/ContentIdentifier$Domain;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 106
    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 107
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public toUrlString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 111
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 112
    .local v0, "sb":Ljava/lang/StringBuilder;
    iget-object v1, p0, Lcom/google/android/music/download/ContentIdentifier;->mDomain:Lcom/google/android/music/download/ContentIdentifier$Domain;

    invoke-virtual {v1}, Lcom/google/android/music/download/ContentIdentifier$Domain;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 113
    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 114
    iget-wide v2, p0, Lcom/google/android/music/download/ContentIdentifier;->mId:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 115
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 168
    iget-wide v0, p0, Lcom/google/android/music/download/ContentIdentifier;->mId:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 169
    iget-object v0, p0, Lcom/google/android/music/download/ContentIdentifier;->mDomain:Lcom/google/android/music/download/ContentIdentifier$Domain;

    invoke-virtual {v0}, Lcom/google/android/music/download/ContentIdentifier$Domain;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 170
    return-void
.end method
