.class public Lcom/google/android/music/ui/SearchActivity;
.super Lcom/google/android/music/ui/BaseActivity;
.source "SearchActivity.java"

# interfaces
.implements Landroid/support/v7/widget/SearchView$OnQueryTextListener;


# static fields
.field private static final LOGV:Z

.field public static final SUGGEST_DATA_ALBUM:Landroid/net/Uri;

.field public static final SUGGEST_DATA_ALBUM_ARTIST:Landroid/net/Uri;

.field public static final SUGGEST_DATA_ARTIST:Landroid/net/Uri;

.field public static final SUGGEST_DATA_PLAYLIST:Landroid/net/Uri;

.field public static final SUGGEST_DATA_TRACK:Landroid/net/Uri;


# instance fields
.field private mCreateRadio:Z

.field private mExecutor:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

.field private mFilterString:Ljava/lang/String;

.field private mHandler:Landroid/os/Handler;

.field private mInstantSearchEnabled:Z

.field private mLogger:Lcom/google/android/music/eventlog/MusicEventLogger;

.field private mSearchDelayMs:J

.field private mSearchFuture:Ljava/util/concurrent/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Future",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private mSearchTask:Ljava/util/concurrent/Callable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Callable",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private mSearchView:Landroid/support/v7/widget/SearchView;

.field private mSurpressKeyboard:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 52
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->UI:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/ui/SearchActivity;->LOGV:Z

    .line 58
    sget-object v0, Lcom/google/android/music/store/MusicContent;->CONTENT_URI:Landroid/net/Uri;

    const-string v1, "albumartist"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/ui/SearchActivity;->SUGGEST_DATA_ALBUM_ARTIST:Landroid/net/Uri;

    .line 60
    sget-object v0, Lcom/google/android/music/store/MusicContent;->CONTENT_URI:Landroid/net/Uri;

    const-string v1, "artist"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/ui/SearchActivity;->SUGGEST_DATA_ARTIST:Landroid/net/Uri;

    .line 62
    sget-object v0, Lcom/google/android/music/store/MusicContent;->CONTENT_URI:Landroid/net/Uri;

    const-string v1, "album"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/ui/SearchActivity;->SUGGEST_DATA_ALBUM:Landroid/net/Uri;

    .line 64
    sget-object v0, Lcom/google/android/music/store/MusicContent;->CONTENT_URI:Landroid/net/Uri;

    const-string v1, "playlist"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/ui/SearchActivity;->SUGGEST_DATA_PLAYLIST:Landroid/net/Uri;

    .line 66
    sget-object v0, Lcom/google/android/music/store/MusicContent;->CONTENT_URI:Landroid/net/Uri;

    const-string v1, "track"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/ui/SearchActivity;->SUGGEST_DATA_TRACK:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/google/android/music/ui/BaseActivity;-><init>()V

    .line 97
    new-instance v0, Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/music/ui/SearchActivity;->mExecutor:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    .line 102
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/ui/SearchActivity;->mHandler:Landroid/os/Handler;

    .line 105
    new-instance v0, Lcom/google/android/music/ui/SearchActivity$1;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/SearchActivity$1;-><init>(Lcom/google/android/music/ui/SearchActivity;)V

    iput-object v0, p0, Lcom/google/android/music/ui/SearchActivity;->mSearchTask:Ljava/util/concurrent/Callable;

    return-void
.end method

.method static synthetic access$000()Z
    .locals 1

    .prologue
    .line 48
    sget-boolean v0, Lcom/google/android/music/ui/SearchActivity;->LOGV:Z

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/music/ui/SearchActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/SearchActivity;

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/google/android/music/ui/SearchActivity;->handleSearchResult()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/music/ui/SearchActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/SearchActivity;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/music/ui/SearchActivity;->mFilterString:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/music/ui/SearchActivity;)Lcom/google/android/music/eventlog/MusicEventLogger;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/SearchActivity;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/music/ui/SearchActivity;->mLogger:Lcom/google/android/music/eventlog/MusicEventLogger;

    return-object v0
.end method

.method private static getFilterStringForIntent(Landroid/content/Intent;)Ljava/lang/String;
    .locals 11
    .param p0, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v7, 0x0

    .line 334
    if-eqz p0, :cond_3

    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 335
    .local v0, "action":Ljava/lang/String;
    :goto_0
    sget-boolean v8, Lcom/google/android/music/ui/SearchActivity;->LOGV:Z

    if-eqz v8, :cond_0

    .line 336
    const-string v8, "SearchActivity"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "getFilterStringForIntent: action = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 337
    if-eqz p0, :cond_4

    invoke-virtual {p0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    .line 338
    .local v3, "extras":Landroid/os/Bundle;
    :goto_1
    if-eqz v3, :cond_0

    .line 339
    const-string v7, "SearchActivity"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "  - has extras: size = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v3}, Landroid/os/Bundle;->size()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 340
    const-string v7, "SearchActivity"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "  - extras = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 344
    .end local v3    # "extras":Landroid/os/Bundle;
    :cond_0
    const-string v7, "query"

    invoke-virtual {p0, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 346
    .local v4, "filterString":Ljava/lang/String;
    const-string v7, "android.intent.action.MEDIA_SEARCH"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 347
    const-string v7, "android.intent.extra.focus"

    invoke-virtual {p0, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 348
    .local v5, "focus":Ljava/lang/String;
    const-string v7, "android.intent.extra.artist"

    invoke-virtual {p0, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 349
    .local v2, "artist":Ljava/lang/String;
    const-string v7, "android.intent.extra.album"

    invoke-virtual {p0, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 350
    .local v1, "album":Ljava/lang/String;
    const-string v7, "android.intent.extra.title"

    invoke-virtual {p0, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 351
    .local v6, "title":Ljava/lang/String;
    if-eqz v5, :cond_1

    .line 352
    const-string v7, "audio/"

    invoke-virtual {v5, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_5

    if-eqz v6, :cond_5

    .line 353
    move-object v4, v6

    .line 371
    .end local v1    # "album":Ljava/lang/String;
    .end local v2    # "artist":Ljava/lang/String;
    .end local v5    # "focus":Ljava/lang/String;
    .end local v6    # "title":Ljava/lang/String;
    :cond_1
    :goto_2
    sget-boolean v7, Lcom/google/android/music/ui/SearchActivity;->LOGV:Z

    if-eqz v7, :cond_2

    const-string v7, "SearchActivity"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "getFilterStringForIntent: new filter is \'"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "\'"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 372
    :cond_2
    return-object v4

    .end local v0    # "action":Ljava/lang/String;
    .end local v4    # "filterString":Ljava/lang/String;
    :cond_3
    move-object v0, v7

    .line 334
    goto/16 :goto_0

    .restart local v0    # "action":Ljava/lang/String;
    :cond_4
    move-object v3, v7

    .line 337
    goto/16 :goto_1

    .line 354
    .restart local v1    # "album":Ljava/lang/String;
    .restart local v2    # "artist":Ljava/lang/String;
    .restart local v4    # "filterString":Ljava/lang/String;
    .restart local v5    # "focus":Ljava/lang/String;
    .restart local v6    # "title":Ljava/lang/String;
    :cond_5
    const-string v7, "vnd.android.cursor.item/album"

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 355
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 356
    move-object v4, v1

    goto :goto_2

    .line 361
    :cond_6
    const-string v7, "vnd.android.cursor.item/artist"

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 362
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 363
    move-object v4, v2

    goto :goto_2

    .line 367
    :cond_7
    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    aput-object v2, v7, v8

    const/4 v8, 0x1

    aput-object v1, v7, v8

    const/4 v8, 0x2

    aput-object v6, v7, v8

    invoke-static {v7}, Lcom/google/android/music/ui/SearchActivity;->getFirstNonNullString([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto :goto_2
.end method

.method private static varargs getFirstNonNullString([Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "variables"    # [Ljava/lang/String;

    .prologue
    .line 376
    move-object v0, p0

    .local v0, "arr$":[Ljava/lang/String;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 377
    .local v3, "variable":Ljava/lang/String;
    invoke-static {v3}, Lcom/google/android/music/utils/MusicUtils;->isUnknown(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 381
    .end local v3    # "variable":Ljava/lang/String;
    :goto_1
    return-object v3

    .line 376
    .restart local v3    # "variable":Ljava/lang/String;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 381
    .end local v3    # "variable":Ljava/lang/String;
    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method private handleSearchResult()V
    .locals 2

    .prologue
    .line 122
    iget-object v0, p0, Lcom/google/android/music/ui/SearchActivity;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/music/ui/SearchActivity$2;

    invoke-direct {v1, p0}, Lcom/google/android/music/ui/SearchActivity$2;-><init>(Lcom/google/android/music/ui/SearchActivity;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 136
    return-void
.end method

.method private handleSearchResult(Landroid/content/Intent;)V
    .locals 25
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 390
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v24

    .line 391
    .local v24, "uri":Landroid/net/Uri;
    if-nez v24, :cond_1

    const-string v11, ""

    .line 392
    .local v11, "path":Ljava/lang/String;
    :goto_0
    const-string v5, "autoPlay"

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v22

    .line 393
    .local v22, "shouldAutoPlay":Z
    sget-object v5, Lcom/google/android/music/ui/SearchActivity;->SUGGEST_DATA_TRACK:Landroid/net/Uri;

    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v11, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 394
    invoke-virtual/range {v24 .. v24}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v16

    .line 395
    .local v16, "id":J
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/music/ui/SearchActivity;->mFilterString:Ljava/lang/String;

    move-wide/from16 v0, v16

    invoke-static {v0, v1, v5}, Lcom/google/android/music/store/ContainerDescriptor;->newSearchResultsDescriptor(JLjava/lang/String;)Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v4

    .line 398
    .local v4, "descriptor":Lcom/google/android/music/store/ContainerDescriptor;
    new-instance v5, Lcom/google/android/music/medialist/SingleSongList;

    const-string v6, ""

    move-wide/from16 v0, v16

    invoke-direct {v5, v4, v0, v1, v6}, Lcom/google/android/music/medialist/SingleSongList;-><init>(Lcom/google/android/music/store/ContainerDescriptor;JLjava/lang/String;)V

    const/4 v6, -0x1

    move-object/from16 v0, p0

    invoke-static {v0, v5, v6}, Lcom/google/android/music/utils/MusicUtils;->playMediaList(Landroid/content/Context;Lcom/google/android/music/medialist/SongList;I)V

    .line 401
    invoke-static/range {p0 .. p0}, Lcom/google/android/music/ui/AppNavigation;->goHome(Landroid/content/Context;)V

    .line 402
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/SearchActivity;->finish()V

    .line 528
    .end local v4    # "descriptor":Lcom/google/android/music/store/ContainerDescriptor;
    .end local v16    # "id":J
    :cond_0
    :goto_1
    return-void

    .line 391
    .end local v11    # "path":Ljava/lang/String;
    .end local v22    # "shouldAutoPlay":Z
    :cond_1
    invoke-virtual/range {v24 .. v24}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v11

    goto :goto_0

    .line 403
    .restart local v11    # "path":Ljava/lang/String;
    .restart local v22    # "shouldAutoPlay":Z
    :cond_2
    sget-object v5, Lcom/google/android/music/ui/SearchActivity;->SUGGEST_DATA_ALBUM_ARTIST:Landroid/net/Uri;

    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v11, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 405
    invoke-virtual/range {v24 .. v24}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v18

    .line 407
    .local v18, "localArtistId":J
    new-instance v5, Lcom/google/android/music/ui/SearchActivity$3;

    move-object/from16 v0, p0

    move-wide/from16 v1, v18

    move/from16 v3, v22

    invoke-direct {v5, v0, v1, v2, v3}, Lcom/google/android/music/ui/SearchActivity$3;-><init>(Lcom/google/android/music/ui/SearchActivity;JZ)V

    invoke-static {v5}, Lcom/google/android/music/utils/MusicUtils;->runAsyncWithCallback(Lcom/google/android/music/utils/async/AsyncRunner;)V

    goto :goto_1

    .line 440
    .end local v18    # "localArtistId":J
    :cond_3
    sget-object v5, Lcom/google/android/music/ui/SearchActivity;->SUGGEST_DATA_ALBUM:Landroid/net/Uri;

    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v11, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 441
    invoke-virtual/range {v24 .. v24}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object/from16 v5, p0

    invoke-static/range {v5 .. v10}, Lcom/google/android/music/ui/TrackContainerActivity;->showAlbum(Landroid/content/Context;JLcom/google/android/music/ui/cardlib/model/Document;ZLandroid/view/View;)V

    .line 444
    if-eqz v22, :cond_4

    .line 445
    invoke-virtual/range {v24 .. v24}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v12

    .line 446
    .local v12, "albumId":J
    new-instance v23, Lcom/google/android/music/medialist/AlbumSongList;

    const/4 v5, 0x0

    move-object/from16 v0, v23

    invoke-direct {v0, v12, v13, v5}, Lcom/google/android/music/medialist/AlbumSongList;-><init>(JZ)V

    .line 447
    .local v23, "songlist":Lcom/google/android/music/medialist/SongList;
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-static {v0, v1}, Lcom/google/android/music/utils/MusicUtils;->playMediaList(Landroid/content/Context;Lcom/google/android/music/medialist/SongList;)V

    .line 449
    .end local v12    # "albumId":J
    .end local v23    # "songlist":Lcom/google/android/music/medialist/SongList;
    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/SearchActivity;->finish()V

    goto :goto_1

    .line 450
    :cond_5
    sget-object v5, Lcom/google/android/music/ui/SearchActivity;->SUGGEST_DATA_ARTIST:Landroid/net/Uri;

    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v11, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 451
    invoke-virtual/range {v24 .. v24}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    .line 452
    .local v14, "artistId":J
    new-instance v5, Lcom/google/android/music/ui/SearchActivity$4;

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-direct {v5, v0, v14, v15, v1}, Lcom/google/android/music/ui/SearchActivity$4;-><init>(Lcom/google/android/music/ui/SearchActivity;JZ)V

    invoke-static {v5}, Lcom/google/android/music/utils/MusicUtils;->runAsyncWithCallback(Lcom/google/android/music/utils/async/AsyncRunner;)V

    goto/16 :goto_1

    .line 485
    .end local v14    # "artistId":J
    :cond_6
    sget-object v5, Lcom/google/android/music/ui/SearchActivity;->SUGGEST_DATA_PLAYLIST:Landroid/net/Uri;

    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v11, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 486
    invoke-virtual/range {v24 .. v24}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v20

    .line 487
    .local v20, "playlistId":J
    new-instance v5, Lcom/google/android/music/ui/SearchActivity$5;

    move-object/from16 v0, p0

    move-wide/from16 v1, v20

    move/from16 v3, v22

    invoke-direct {v5, v0, v1, v2, v3}, Lcom/google/android/music/ui/SearchActivity$5;-><init>(Lcom/google/android/music/ui/SearchActivity;JZ)V

    invoke-static {v5}, Lcom/google/android/music/utils/MusicUtils;->runAsyncWithCallback(Lcom/google/android/music/utils/async/AsyncRunner;)V

    goto/16 :goto_1
.end method

.method private hideSoftKeyboard()V
    .locals 3

    .prologue
    .line 313
    iget-object v1, p0, Lcom/google/android/music/ui/SearchActivity;->mSearchView:Landroid/support/v7/widget/SearchView;

    if-eqz v1, :cond_0

    .line 314
    iget-object v1, p0, Lcom/google/android/music/ui/SearchActivity;->mSearchView:Landroid/support/v7/widget/SearchView;

    invoke-virtual {v1}, Landroid/support/v7/widget/SearchView;->clearFocus()V

    .line 315
    invoke-virtual {p0}, Lcom/google/android/music/ui/SearchActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "input_method"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 317
    .local v0, "inputMethodManager":Landroid/view/inputmethod/InputMethodManager;
    iget-object v1, p0, Lcom/google/android/music/ui/SearchActivity;->mSearchView:Landroid/support/v7/widget/SearchView;

    invoke-virtual {v1}, Landroid/support/v7/widget/SearchView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 319
    .end local v0    # "inputMethodManager":Landroid/view/inputmethod/InputMethodManager;
    :cond_0
    return-void
.end method

.method private processNewSearchQuery()V
    .locals 5

    .prologue
    .line 291
    sget-boolean v2, Lcom/google/android/music/ui/SearchActivity;->LOGV:Z

    if-eqz v2, :cond_0

    const-string v2, "SearchActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "processNewSearchQuery: mFilterString \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/music/ui/SearchActivity;->mFilterString:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'..."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 294
    :cond_0
    iget-object v2, p0, Lcom/google/android/music/ui/SearchActivity;->mSearchFuture:Ljava/util/concurrent/Future;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/music/ui/SearchActivity;->mSearchFuture:Ljava/util/concurrent/Future;

    invoke-interface {v2}, Ljava/util/concurrent/Future;->isDone()Z

    move-result v2

    if-nez v2, :cond_2

    .line 295
    sget-boolean v2, Lcom/google/android/music/ui/SearchActivity;->LOGV:Z

    if-eqz v2, :cond_1

    .line 296
    const-string v2, "SearchActivity"

    const-string v3, "Cancelling a previous search."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 299
    :cond_1
    iget-object v2, p0, Lcom/google/android/music/ui/SearchActivity;->mSearchFuture:Ljava/util/concurrent/Future;

    const/4 v3, 0x1

    invoke-interface {v2, v3}, Ljava/util/concurrent/Future;->cancel(Z)Z

    move-result v2

    if-nez v2, :cond_2

    .line 300
    const-string v2, "SearchActivity"

    const-string v3, "Failed to cancel a previous search."

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 304
    :cond_2
    iget-object v2, p0, Lcom/google/android/music/ui/SearchActivity;->mFilterString:Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/music/ui/SearchActivity;->mFilterString:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x2

    if-lt v2, v3, :cond_3

    iget-wide v0, p0, Lcom/google/android/music/ui/SearchActivity;->mSearchDelayMs:J

    .line 309
    .local v0, "delay":J
    :goto_0
    iget-object v2, p0, Lcom/google/android/music/ui/SearchActivity;->mExecutor:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    iget-object v3, p0, Lcom/google/android/music/ui/SearchActivity;->mSearchTask:Ljava/util/concurrent/Callable;

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, v3, v0, v1, v4}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;->schedule(Ljava/util/concurrent/Callable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/ui/SearchActivity;->mSearchFuture:Ljava/util/concurrent/Future;

    .line 310
    return-void

    .line 304
    .end local v0    # "delay":J
    :cond_3
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method private setupSearchView(Landroid/support/v7/app/ActionBar;)V
    .locals 4
    .param p1, "actionbar"    # Landroid/support/v7/app/ActionBar;

    .prologue
    .line 203
    new-instance v1, Landroid/support/v7/widget/SearchView;

    invoke-virtual {p1}, Landroid/support/v7/app/ActionBar;->getThemedContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/support/v7/widget/SearchView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/music/ui/SearchActivity;->mSearchView:Landroid/support/v7/widget/SearchView;

    .line 204
    iget-object v1, p0, Lcom/google/android/music/ui/SearchActivity;->mSearchView:Landroid/support/v7/widget/SearchView;

    invoke-virtual {p1, v1}, Landroid/support/v7/app/ActionBar;->setCustomView(Landroid/view/View;)V

    .line 205
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Landroid/support/v7/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    .line 206
    iget-object v1, p0, Lcom/google/android/music/ui/SearchActivity;->mSearchView:Landroid/support/v7/widget/SearchView;

    invoke-virtual {v1}, Landroid/support/v7/widget/SearchView;->onActionViewExpanded()V

    .line 208
    iget-object v1, p0, Lcom/google/android/music/ui/SearchActivity;->mSearchView:Landroid/support/v7/widget/SearchView;

    invoke-virtual {v1, p0}, Landroid/support/v7/widget/SearchView;->setOnQueryTextListener(Landroid/support/v7/widget/SearchView$OnQueryTextListener;)V

    .line 210
    iget-boolean v1, p0, Lcom/google/android/music/ui/SearchActivity;->mSurpressKeyboard:Z

    if-eqz v1, :cond_0

    .line 213
    iget-object v1, p0, Lcom/google/android/music/ui/SearchActivity;->mSearchView:Landroid/support/v7/widget/SearchView;

    invoke-virtual {v1}, Landroid/support/v7/widget/SearchView;->clearFocus()V

    .line 216
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/ui/SearchActivity;->mSearchView:Landroid/support/v7/widget/SearchView;

    invoke-virtual {p0}, Lcom/google/android/music/ui/SearchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b00f0

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/SearchView;->setQueryHint(Ljava/lang/CharSequence;)V

    .line 217
    iget-object v1, p0, Lcom/google/android/music/ui/SearchActivity;->mSearchView:Landroid/support/v7/widget/SearchView;

    invoke-virtual {v1}, Landroid/support/v7/widget/SearchView;->getImeOptions()I

    move-result v0

    .line 218
    .local v0, "flags":I
    iget-object v1, p0, Lcom/google/android/music/ui/SearchActivity;->mSearchView:Landroid/support/v7/widget/SearchView;

    const/high16 v2, 0x10000000

    or-int/2addr v2, v0

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/SearchView;->setImeOptions(I)V

    .line 220
    iget-object v1, p0, Lcom/google/android/music/ui/SearchActivity;->mFilterString:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 221
    iget-object v1, p0, Lcom/google/android/music/ui/SearchActivity;->mSearchView:Landroid/support/v7/widget/SearchView;

    iget-object v2, p0, Lcom/google/android/music/ui/SearchActivity;->mFilterString:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/support/v7/widget/SearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    .line 223
    :cond_1
    return-void
.end method

.method public static final showCreateRadioSearch(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 243
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/music/ui/SearchActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 244
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "createNewRadioStation"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 245
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 246
    return-void
.end method

.method public static final showSearch(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 235
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/music/ui/SearchActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 236
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "createNewRadioStation"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 238
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 239
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 240
    return-void
.end method


# virtual methods
.method getSearchView()Landroid/support/v7/widget/SearchView;
    .locals 1

    .prologue
    .line 385
    iget-object v0, p0, Lcom/google/android/music/ui/SearchActivity;->mSearchView:Landroid/support/v7/widget/SearchView;

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 12
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const-wide/16 v10, 0xfa

    const/4 v6, 0x1

    const/4 v8, 0x0

    .line 145
    invoke-super {p0, p1}, Lcom/google/android/music/ui/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 147
    invoke-virtual {p0}, Lcom/google/android/music/ui/SearchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    .line 148
    .local v3, "intent":Landroid/content/Intent;
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 150
    .local v0, "action":Ljava/lang/String;
    :goto_0
    if-eqz v3, :cond_1

    invoke-static {p0}, Lcom/google/android/music/leanback/LeanbackUtils;->isLeanbackEnvironment(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 152
    const-class v5, Lcom/google/android/music/leanback/LeanbackSearchActivity;

    invoke-virtual {v3, p0, v5}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 153
    invoke-virtual {p0, v3}, Lcom/google/android/music/ui/SearchActivity;->startActivity(Landroid/content/Intent;)V

    .line 154
    invoke-virtual {p0}, Lcom/google/android/music/ui/SearchActivity;->finish()V

    .line 200
    :goto_1
    return-void

    .line 148
    .end local v0    # "action":Ljava/lang/String;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 158
    .restart local v0    # "action":Ljava/lang/String;
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/music/ui/SearchActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v1

    .line 159
    .local v1, "actionbar":Landroid/support/v7/app/ActionBar;
    invoke-virtual {v1, v6}, Landroid/support/v7/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 160
    invoke-virtual {v1, v6}, Landroid/support/v7/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 161
    const-string v5, ""

    invoke-virtual {v1, v5}, Landroid/support/v7/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 163
    invoke-virtual {p0}, Lcom/google/android/music/ui/SearchActivity;->getContent()Landroid/support/v4/app/Fragment;

    move-result-object v5

    if-nez v5, :cond_2

    .line 165
    new-instance v5, Ljava/lang/String;

    invoke-direct {v5}, Ljava/lang/String;-><init>()V

    iput-object v5, p0, Lcom/google/android/music/ui/SearchActivity;->mFilterString:Ljava/lang/String;

    .line 168
    invoke-virtual {p0}, Lcom/google/android/music/ui/SearchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    .line 169
    .local v4, "searchIntent":Landroid/content/Intent;
    const-string v5, "createNewRadioStation"

    invoke-virtual {v4, v5, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    iput-boolean v5, p0, Lcom/google/android/music/ui/SearchActivity;->mCreateRadio:Z

    .line 172
    iget-boolean v5, p0, Lcom/google/android/music/ui/SearchActivity;->mCreateRadio:Z

    if-nez v5, :cond_5

    .line 173
    new-instance v2, Lcom/google/android/music/ui/SearchMusicClustersFragment;

    iget-object v5, p0, Lcom/google/android/music/ui/SearchActivity;->mFilterString:Ljava/lang/String;

    invoke-direct {v2, v5}, Lcom/google/android/music/ui/SearchMusicClustersFragment;-><init>(Ljava/lang/String;)V

    .line 177
    .local v2, "fragment":Landroid/support/v4/app/Fragment;
    :goto_2
    invoke-virtual {p0, v2, v8}, Lcom/google/android/music/ui/SearchActivity;->replaceContent(Landroid/support/v4/app/Fragment;Z)V

    .line 180
    .end local v2    # "fragment":Landroid/support/v4/app/Fragment;
    .end local v4    # "searchIntent":Landroid/content/Intent;
    :cond_2
    if-eqz p1, :cond_6

    .line 181
    const-string v5, "com.google.android.music.ui.searchactivity.filterstring"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/music/ui/SearchActivity;->mFilterString:Ljava/lang/String;

    .line 182
    const-string v5, "createNewRadioStation"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    iput-boolean v5, p0, Lcom/google/android/music/ui/SearchActivity;->mCreateRadio:Z

    .line 190
    :cond_3
    :goto_3
    invoke-direct {p0, v1}, Lcom/google/android/music/ui/SearchActivity;->setupSearchView(Landroid/support/v7/app/ActionBar;)V

    .line 192
    invoke-static {}, Lcom/google/android/music/utils/ConfigUtils;->isInstantSearchEnabled()Z

    move-result v5

    iput-boolean v5, p0, Lcom/google/android/music/ui/SearchActivity;->mInstantSearchEnabled:Z

    .line 193
    invoke-static {}, Lcom/google/android/music/utils/ConfigUtils;->getInstantSearchDelayMs()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/android/music/ui/SearchActivity;->mSearchDelayMs:J

    .line 194
    iget-wide v6, p0, Lcom/google/android/music/ui/SearchActivity;->mSearchDelayMs:J

    cmp-long v5, v6, v10

    if-gez v5, :cond_4

    .line 195
    iput-wide v10, p0, Lcom/google/android/music/ui/SearchActivity;->mSearchDelayMs:J

    .line 198
    :cond_4
    const-string v5, "surpressKeyboard"

    invoke-virtual {v3, v5, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    iput-boolean v5, p0, Lcom/google/android/music/ui/SearchActivity;->mSurpressKeyboard:Z

    .line 199
    invoke-static {p0}, Lcom/google/android/music/eventlog/MusicEventLogger;->getInstance(Landroid/content/Context;)Lcom/google/android/music/eventlog/MusicEventLogger;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/music/ui/SearchActivity;->mLogger:Lcom/google/android/music/eventlog/MusicEventLogger;

    goto :goto_1

    .line 175
    .restart local v4    # "searchIntent":Landroid/content/Intent;
    :cond_5
    new-instance v2, Lcom/google/android/music/ui/CreateNewStationClusterFragment;

    iget-object v5, p0, Lcom/google/android/music/ui/SearchActivity;->mFilterString:Ljava/lang/String;

    invoke-direct {v2, v5}, Lcom/google/android/music/ui/CreateNewStationClusterFragment;-><init>(Ljava/lang/String;)V

    .restart local v2    # "fragment":Landroid/support/v4/app/Fragment;
    goto :goto_2

    .line 183
    .end local v2    # "fragment":Landroid/support/v4/app/Fragment;
    .end local v4    # "searchIntent":Landroid/content/Intent;
    :cond_6
    const-string v5, "android.intent.action.SEARCH"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 184
    invoke-static {v3}, Lcom/google/android/music/ui/SearchActivity;->getFilterStringForIntent(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/music/ui/SearchActivity;->mFilterString:Ljava/lang/String;

    .line 185
    invoke-direct {p0}, Lcom/google/android/music/ui/SearchActivity;->processNewSearchQuery()V

    goto :goto_3

    .line 186
    :cond_7
    const-string v5, "android.intent.action.SEARCH_RESULT"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 187
    invoke-direct {p0, v3}, Lcom/google/android/music/ui/SearchActivity;->handleSearchResult(Landroid/content/Intent;)V

    goto :goto_3
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 250
    const/4 v0, 0x1

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 255
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/music/ui/SearchActivity;->mFilterString:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 256
    invoke-static {p0}, Lcom/google/android/music/ui/AppNavigation;->goHome(Landroid/content/Context;)V

    .line 257
    const/4 v0, 0x1

    .line 259
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/music/ui/BaseActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public onQueryTextChange(Ljava/lang/String;)Z
    .locals 2
    .param p1, "newText"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 280
    iput-boolean v1, p0, Lcom/google/android/music/ui/SearchActivity;->mSurpressKeyboard:Z

    .line 283
    iget-boolean v0, p0, Lcom/google/android/music/ui/SearchActivity;->mInstantSearchEnabled:Z

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/music/ui/SearchActivity;->mFilterString:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 284
    iput-object p1, p0, Lcom/google/android/music/ui/SearchActivity;->mFilterString:Ljava/lang/String;

    .line 285
    invoke-direct {p0}, Lcom/google/android/music/ui/SearchActivity;->processNewSearchQuery()V

    .line 287
    :cond_0
    return v1
.end method

.method public onQueryTextSubmit(Ljava/lang/String;)Z
    .locals 2
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 265
    iput-boolean v1, p0, Lcom/google/android/music/ui/SearchActivity;->mSurpressKeyboard:Z

    .line 267
    iget-boolean v0, p0, Lcom/google/android/music/ui/SearchActivity;->mInstantSearchEnabled:Z

    if-nez v0, :cond_0

    .line 269
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/music/ui/SearchActivity;->mFilterString:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 270
    iput-object p1, p0, Lcom/google/android/music/ui/SearchActivity;->mFilterString:Ljava/lang/String;

    .line 271
    invoke-direct {p0}, Lcom/google/android/music/ui/SearchActivity;->processNewSearchQuery()V

    .line 274
    :cond_0
    invoke-direct {p0}, Lcom/google/android/music/ui/SearchActivity;->hideSoftKeyboard()V

    .line 275
    return v1
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 227
    invoke-super {p0, p1}, Lcom/google/android/music/ui/BaseActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 228
    iget-object v0, p0, Lcom/google/android/music/ui/SearchActivity;->mFilterString:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 229
    const-string v0, "com.google.android.music.ui.searchactivity.filterstring"

    iget-object v1, p0, Lcom/google/android/music/ui/SearchActivity;->mFilterString:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 231
    :cond_0
    const-string v0, "createNewRadioStation"

    iget-boolean v1, p0, Lcom/google/android/music/ui/SearchActivity;->mCreateRadio:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 232
    return-void
.end method

.method protected useActionBarHamburger()Z
    .locals 1

    .prologue
    .line 140
    const/4 v0, 0x0

    return v0
.end method
