.class Lcom/google/android/music/KeepOnView$3;
.super Ljava/lang/Object;
.source "KeepOnView.java"

# interfaces
.implements Lcom/google/android/music/utils/async/AsyncRunner;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/KeepOnView;->setSongListName()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final mList:Lcom/google/android/music/medialist/SongList;

.field private mListName:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/music/KeepOnView;


# direct methods
.method constructor <init>(Lcom/google/android/music/KeepOnView;)V
    .locals 1

    .prologue
    .line 270
    iput-object p1, p0, Lcom/google/android/music/KeepOnView$3;->this$0:Lcom/google/android/music/KeepOnView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 272
    iget-object v0, p0, Lcom/google/android/music/KeepOnView$3;->this$0:Lcom/google/android/music/KeepOnView;

    # getter for: Lcom/google/android/music/KeepOnView;->mSongList:Lcom/google/android/music/medialist/SongList;
    invoke-static {v0}, Lcom/google/android/music/KeepOnView;->access$100(Lcom/google/android/music/KeepOnView;)Lcom/google/android/music/medialist/SongList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/KeepOnView$3;->mList:Lcom/google/android/music/medialist/SongList;

    .line 273
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/music/KeepOnView$3;->mListName:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public backgroundTask()V
    .locals 2

    .prologue
    .line 277
    iget-object v0, p0, Lcom/google/android/music/KeepOnView$3;->mList:Lcom/google/android/music/medialist/SongList;

    if-eqz v0, :cond_0

    .line 278
    iget-object v0, p0, Lcom/google/android/music/KeepOnView$3;->mList:Lcom/google/android/music/medialist/SongList;

    iget-object v1, p0, Lcom/google/android/music/KeepOnView$3;->this$0:Lcom/google/android/music/KeepOnView;

    invoke-virtual {v1}, Lcom/google/android/music/KeepOnView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/music/medialist/SongList;->getName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/KeepOnView$3;->mListName:Ljava/lang/String;

    .line 280
    :cond_0
    return-void
.end method

.method public taskCompleted()V
    .locals 2

    .prologue
    .line 284
    iget-object v0, p0, Lcom/google/android/music/KeepOnView$3;->mList:Lcom/google/android/music/medialist/SongList;

    iget-object v1, p0, Lcom/google/android/music/KeepOnView$3;->this$0:Lcom/google/android/music/KeepOnView;

    # getter for: Lcom/google/android/music/KeepOnView;->mSongList:Lcom/google/android/music/medialist/SongList;
    invoke-static {v1}, Lcom/google/android/music/KeepOnView;->access$100(Lcom/google/android/music/KeepOnView;)Lcom/google/android/music/medialist/SongList;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 285
    iget-object v0, p0, Lcom/google/android/music/KeepOnView$3;->this$0:Lcom/google/android/music/KeepOnView;

    iget-object v1, p0, Lcom/google/android/music/KeepOnView$3;->mListName:Ljava/lang/String;

    # setter for: Lcom/google/android/music/KeepOnView;->mSongListName:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/google/android/music/KeepOnView;->access$202(Lcom/google/android/music/KeepOnView;Ljava/lang/String;)Ljava/lang/String;

    .line 287
    :cond_0
    return-void
.end method
