.class Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$2;
.super Ljava/lang/Object;
.source "LeanbackExploreGenresActivity.java"

# interfaces
.implements Lcom/google/android/music/leanback/Item$IntentGetter;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;->getRadioItem(Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/music/leanback/Item;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;

.field final synthetic val$artUriGetter:Lcom/google/android/music/leanback/Item$StringGetter;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$genreId:Ljava/lang/String;

.field final synthetic val$genreName:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/music/leanback/Item$StringGetter;)V
    .locals 0

    .prologue
    .line 188
    iput-object p1, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$2;->this$0:Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;

    iput-object p2, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$2;->val$context:Landroid/content/Context;

    iput-object p3, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$2;->val$genreId:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$2;->val$genreName:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$2;->val$artUriGetter:Lcom/google/android/music/leanback/Item$StringGetter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getIntent()Landroid/content/Intent;
    .locals 7

    .prologue
    .line 191
    iget-object v6, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$2;->val$context:Landroid/content/Context;

    new-instance v0, Lcom/google/android/music/mix/MixDescriptor;

    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$2;->val$genreId:Ljava/lang/String;

    sget-object v2, Lcom/google/android/music/mix/MixDescriptor$Type;->GENRE_SEED:Lcom/google/android/music/mix/MixDescriptor$Type;

    iget-object v3, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$2;->val$genreName:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$2;->val$artUriGetter:Lcom/google/android/music/leanback/Item$StringGetter;

    invoke-interface {v4}, Lcom/google/android/music/leanback/Item$StringGetter;->getString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/music/mix/MixDescriptor;-><init>(Ljava/lang/String;Lcom/google/android/music/mix/MixDescriptor$Type;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-static {v6, v0}, Lcom/google/android/music/leanback/LeanbackUtils;->newPlayIntent(Landroid/content/Context;Lcom/google/android/music/mix/MixDescriptor;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method
