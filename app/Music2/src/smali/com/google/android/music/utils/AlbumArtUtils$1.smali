.class final Lcom/google/android/music/utils/AlbumArtUtils$1;
.super Ljava/lang/Object;
.source "AlbumArtUtils.java"

# interfaces
.implements Lcom/google/android/music/utils/AlbumArtUtils$AlbumIdIteratorFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/utils/AlbumArtUtils;->createAlbumIdIteratorFactoryForContentUri(Landroid/content/Context;Landroid/net/Uri;)Lcom/google/android/music/utils/AlbumArtUtils$AlbumIdIteratorFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$contentUri:Landroid/net/Uri;

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 695
    iput-object p1, p0, Lcom/google/android/music/utils/AlbumArtUtils$1;->val$context:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/music/utils/AlbumArtUtils$1;->val$contentUri:Landroid/net/Uri;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createIterator()Lcom/google/android/music/utils/AlbumArtUtils$AlbumIdIterator;
    .locals 9

    .prologue
    const/4 v6, 0x0

    const/4 v3, 0x0

    .line 701
    iget-object v0, p0, Lcom/google/android/music/utils/AlbumArtUtils$1;->val$context:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/music/utils/AlbumArtUtils$1;->val$contentUri:Landroid/net/Uri;

    # getter for: Lcom/google/android/music/utils/AlbumArtUtils;->sAlbumIdCols:[Ljava/lang/String;
    invoke-static {}, Lcom/google/android/music/utils/AlbumArtUtils;->access$400()[Ljava/lang/String;

    move-result-object v2

    const/4 v7, 0x1

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v7}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZZ)Landroid/database/Cursor;

    move-result-object v8

    .line 703
    .local v8, "c":Landroid/database/Cursor;
    new-instance v0, Lcom/google/android/music/utils/AlbumArtUtils$CursorAlbumIdIterator;

    invoke-direct {v0, v8, v6}, Lcom/google/android/music/utils/AlbumArtUtils$CursorAlbumIdIterator;-><init>(Landroid/database/Cursor;I)V

    return-object v0
.end method
