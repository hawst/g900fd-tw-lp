.class Lcom/google/android/music/dial/DialDevice$6;
.super Ljava/lang/Object;
.source "DialDevice.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/dial/DialDevice;->onDisconnected(II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/dial/DialDevice;


# direct methods
.method constructor <init>(Lcom/google/android/music/dial/DialDevice;)V
    .locals 0

    .prologue
    .line 487
    iput-object p1, p0, Lcom/google/android/music/dial/DialDevice$6;->this$0:Lcom/google/android/music/dial/DialDevice;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 492
    iget-object v0, p0, Lcom/google/android/music/dial/DialDevice$6;->this$0:Lcom/google/android/music/dial/DialDevice;

    iget-object v1, p0, Lcom/google/android/music/dial/DialDevice$6;->this$0:Lcom/google/android/music/dial/DialDevice;

    # invokes: Lcom/google/android/music/dial/DialDevice;->getSocketInternal()Lcom/google/android/music/dial/WebSocket;
    invoke-static {v1}, Lcom/google/android/music/dial/DialDevice;->access$000(Lcom/google/android/music/dial/DialDevice;)Lcom/google/android/music/dial/WebSocket;

    move-result-object v1

    # setter for: Lcom/google/android/music/dial/DialDevice;->mSocket:Lcom/google/android/music/dial/WebSocket;
    invoke-static {v0, v1}, Lcom/google/android/music/dial/DialDevice;->access$502(Lcom/google/android/music/dial/DialDevice;Lcom/google/android/music/dial/WebSocket;)Lcom/google/android/music/dial/WebSocket;

    .line 494
    iget-object v0, p0, Lcom/google/android/music/dial/DialDevice$6;->this$0:Lcom/google/android/music/dial/DialDevice;

    # getter for: Lcom/google/android/music/dial/DialDevice;->mSocket:Lcom/google/android/music/dial/WebSocket;
    invoke-static {v0}, Lcom/google/android/music/dial/DialDevice;->access$500(Lcom/google/android/music/dial/DialDevice;)Lcom/google/android/music/dial/WebSocket;

    move-result-object v0

    if-nez v0, :cond_0

    .line 495
    const-string v0, "DialDevice"

    const-string v1, "socket onDisconnected %s.  Error re-connecting. Re-establishing connection"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/music/dial/DialDevice$6;->this$0:Lcom/google/android/music/dial/DialDevice;

    # getter for: Lcom/google/android/music/dial/DialDevice;->mRemoteDeviceInfo:Lcom/google/android/music/dial/RemoteDeviceInfo;
    invoke-static {v4}, Lcom/google/android/music/dial/DialDevice;->access$100(Lcom/google/android/music/dial/DialDevice;)Lcom/google/android/music/dial/RemoteDeviceInfo;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 498
    iget-object v0, p0, Lcom/google/android/music/dial/DialDevice$6;->this$0:Lcom/google/android/music/dial/DialDevice;

    iget-object v1, p0, Lcom/google/android/music/dial/DialDevice$6;->this$0:Lcom/google/android/music/dial/DialDevice;

    # getter for: Lcom/google/android/music/dial/DialDevice;->mRouteId:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/music/dial/DialDevice;->access$200(Lcom/google/android/music/dial/DialDevice;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/android/music/dial/DialDevice$Listener$Error;->SOCKET_RECONNECT:Lcom/google/android/music/dial/DialDevice$Listener$Error;

    # invokes: Lcom/google/android/music/dial/DialDevice;->callListenersOnError(Ljava/lang/String;Lcom/google/android/music/dial/DialDevice$Listener$Error;)V
    invoke-static {v0, v1, v2}, Lcom/google/android/music/dial/DialDevice;->access$300(Lcom/google/android/music/dial/DialDevice;Ljava/lang/String;Lcom/google/android/music/dial/DialDevice$Listener$Error;)V

    .line 500
    :cond_0
    return-void
.end method
