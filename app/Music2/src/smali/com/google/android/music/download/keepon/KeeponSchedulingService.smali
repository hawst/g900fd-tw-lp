.class public Lcom/google/android/music/download/keepon/KeeponSchedulingService;
.super Lcom/google/android/music/download/AbstractSchedulingService;
.source "KeeponSchedulingService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/download/keepon/KeeponSchedulingService$2;,
        Lcom/google/android/music/download/keepon/KeeponSchedulingService$KeeponStartupReceiver;
    }
.end annotation


# static fields
.field private static final LOGV:Z

.field private static final SUPPORTS_RICH_NOTIFICATIONS:Z


# instance fields
.field private mDownloadProgressNotificationBuilder:Landroid/support/v4/app/NotificationCompat$Builder;

.field private volatile mIsRadioOnly:Z

.field private final mKeeponCallbackManager:Lcom/google/android/music/download/keepon/KeeponCallbackManagerImpl;

.field private mKeeponNotificationInfo:Lcom/google/android/music/store/KeeponNotificationInfo;

.field private mNotification:Landroid/app/Notification;

.field private volatile mNotificationManager:Landroid/app/NotificationManager;

.field private mPrefChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

.field private volatile mShowConnectivityError:Z

.field private volatile mStartId:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 58
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->DOWNLOAD:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->LOGV:Z

    .line 62
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->SUPPORTS_RICH_NOTIFICATIONS:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 98
    const-string v0, "KeeponService"

    sget-object v1, Lcom/google/android/music/download/TrackOwner;->KEEPON:Lcom/google/android/music/download/TrackOwner;

    const-class v2, Lcom/google/android/music/download/keepon/KeeponSchedulingService$KeeponStartupReceiver;

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/music/download/AbstractSchedulingService;-><init>(Ljava/lang/String;Lcom/google/android/music/download/DownloadRequest$Owner;Ljava/lang/Class;)V

    .line 87
    new-instance v0, Lcom/google/android/music/download/keepon/KeeponCallbackManagerImpl;

    invoke-direct {v0}, Lcom/google/android/music/download/keepon/KeeponCallbackManagerImpl;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->mKeeponCallbackManager:Lcom/google/android/music/download/keepon/KeeponCallbackManagerImpl;

    .line 88
    iput-boolean v3, p0, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->mIsRadioOnly:Z

    .line 89
    new-instance v0, Lcom/google/android/music/store/KeeponNotificationInfo;

    invoke-direct {v0}, Lcom/google/android/music/store/KeeponNotificationInfo;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->mKeeponNotificationInfo:Lcom/google/android/music/store/KeeponNotificationInfo;

    .line 91
    iput-boolean v3, p0, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->mShowConnectivityError:Z

    .line 567
    new-instance v0, Lcom/google/android/music/download/keepon/KeeponSchedulingService$1;

    invoke-direct {v0, p0}, Lcom/google/android/music/download/keepon/KeeponSchedulingService$1;-><init>(Lcom/google/android/music/download/keepon/KeeponSchedulingService;)V

    iput-object v0, p0, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->mPrefChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    .line 99
    return-void
.end method

.method static synthetic access$000()Z
    .locals 1

    .prologue
    .line 55
    sget-boolean v0, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->LOGV:Z

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/music/download/keepon/KeeponSchedulingService;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/download/keepon/KeeponSchedulingService;

    .prologue
    .line 55
    iget v0, p0, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->mStartId:I

    return v0
.end method

.method static synthetic access$200(Lcom/google/android/music/download/keepon/KeeponSchedulingService;I)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/download/keepon/KeeponSchedulingService;
    .param p1, "x1"    # I

    .prologue
    .line 55
    invoke-virtual {p0, p1}, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->sendCancelDownloadsMessage(I)V

    return-void
.end method

.method private createDownloadingProgressNotificationBuilder()Landroid/support/v4/app/NotificationCompat$Builder;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 317
    invoke-virtual {p0}, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c007d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 318
    .local v0, "appColor":I
    new-instance v1, Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-direct {v1, p0}, Landroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Landroid/support/v4/app/NotificationCompat$Builder;->setWhen(J)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/support/v4/app/NotificationCompat$Builder;->setDefaults(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/support/v4/app/NotificationCompat$Builder;->setOngoing(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/support/v4/app/NotificationCompat$Builder;->setOnlyAlertOnce(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v2

    sget-boolean v1, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->SUPPORTS_RICH_NOTIFICATIONS:Z

    if-eqz v1, :cond_0

    const v1, 0x1080081

    :goto_0
    invoke-virtual {v2, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->setColor(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/support/v4/app/NotificationCompat$Builder;->setLocalOnly(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    return-object v1

    :cond_0
    const v1, 0x7f02011d

    goto :goto_0
.end method

.method private createNotificationBuilder()Landroid/support/v4/app/NotificationCompat$Builder;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 300
    invoke-virtual {p0}, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c007d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 301
    .local v0, "appColor":I
    new-instance v1, Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-direct {v1, p0}, Landroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Landroid/support/v4/app/NotificationCompat$Builder;->setWhen(J)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/support/v4/app/NotificationCompat$Builder;->setDefaults(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/support/v4/app/NotificationCompat$Builder;->setAutoCancel(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/support/v4/app/NotificationCompat$Builder;->setOnlyAlertOnce(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v2

    sget-boolean v1, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->SUPPORTS_RICH_NOTIFICATIONS:Z

    if-eqz v1, :cond_0

    const v1, 0x1080081

    :goto_0
    invoke-virtual {v2, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->setColor(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/support/v4/app/NotificationCompat$Builder;->setLocalOnly(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    return-object v1

    :cond_0
    const v1, 0x7f02011d

    goto :goto_0
.end method

.method private getContentIntent()Landroid/app/PendingIntent;
    .locals 1

    .prologue
    .line 505
    invoke-static {}, Lcom/google/android/music/utils/ConfigUtils;->isManageDownloadsEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 506
    invoke-static {p0}, Lcom/google/android/music/ui/AppNavigation;->getIntentToOpenDownloadContainerActivity(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object v0

    .line 508
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, Lcom/google/android/music/ui/AppNavigation;->getIntentToOpenApp(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object v0

    goto :goto_0
.end method

.method private getDownloadingContainerTitle()Ljava/lang/String;
    .locals 6

    .prologue
    .line 521
    iget-object v3, p0, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->mKeeponNotificationInfo:Lcom/google/android/music/store/KeeponNotificationInfo;

    monitor-enter v3

    .line 522
    :try_start_0
    iget-object v2, p0, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->mKeeponNotificationInfo:Lcom/google/android/music/store/KeeponNotificationInfo;

    invoke-virtual {v2}, Lcom/google/android/music/store/KeeponNotificationInfo;->hasError()Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->mKeeponNotificationInfo:Lcom/google/android/music/store/KeeponNotificationInfo;

    invoke-virtual {v2}, Lcom/google/android/music/store/KeeponNotificationInfo;->isSingleContainer()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 524
    iget-object v2, p0, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->mKeeponNotificationInfo:Lcom/google/android/music/store/KeeponNotificationInfo;

    invoke-virtual {v2}, Lcom/google/android/music/store/KeeponNotificationInfo;->getName()Ljava/lang/String;

    move-result-object v1

    .line 525
    .local v1, "name":Ljava/lang/String;
    iget-object v2, p0, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->mKeeponNotificationInfo:Lcom/google/android/music/store/KeeponNotificationInfo;

    invoke-virtual {v2}, Lcom/google/android/music/store/KeeponNotificationInfo;->getArtistName()Ljava/lang/String;

    move-result-object v0

    .line 527
    .local v0, "artistName":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 528
    const v2, 0x7f0b01bb

    invoke-virtual {p0, v2}, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->getString(I)Ljava/lang/String;

    move-result-object v1

    .end local v1    # "name":Ljava/lang/String;
    monitor-exit v3

    .line 542
    .end local v0    # "artistName":Ljava/lang/String;
    :goto_0
    return-object v1

    .line 529
    .restart local v0    # "artistName":Ljava/lang/String;
    .restart local v1    # "name":Ljava/lang/String;
    :cond_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 530
    monitor-exit v3

    goto :goto_0

    .line 544
    .end local v0    # "artistName":Ljava/lang/String;
    .end local v1    # "name":Ljava/lang/String;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 532
    .restart local v0    # "artistName":Ljava/lang/String;
    .restart local v1    # "name":Ljava/lang/String;
    :cond_1
    const v2, 0x7f0b01be

    const/4 v4, 0x2

    :try_start_1
    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    const/4 v5, 0x1

    aput-object v0, v4, v5

    invoke-virtual {p0, v2, v4}, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .end local v1    # "name":Ljava/lang/String;
    monitor-exit v3

    goto :goto_0

    .line 535
    .end local v0    # "artistName":Ljava/lang/String;
    :cond_2
    iget-boolean v2, p0, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->mIsRadioOnly:Z

    if-eqz v2, :cond_4

    .line 536
    invoke-virtual {p0}, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->getMusicPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/music/preferences/MusicPreferences;->isNautilusEnabled()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 537
    const v2, 0x7f0b01bc

    invoke-virtual {p0, v2}, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->getString(I)Ljava/lang/String;

    move-result-object v1

    monitor-exit v3

    goto :goto_0

    .line 539
    :cond_3
    const v2, 0x7f0b01bd

    invoke-virtual {p0, v2}, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->getString(I)Ljava/lang/String;

    move-result-object v1

    monitor-exit v3

    goto :goto_0

    .line 542
    :cond_4
    const v2, 0x7f0b01bb

    invoke-virtual {p0, v2}, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->getString(I)Ljava/lang/String;

    move-result-object v1

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method private notifyUi()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 382
    invoke-virtual {p0}, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/music/store/MusicContent;->DOWNLOAD_QUEUE_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    .line 388
    invoke-virtual {p0}, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/music/store/MusicContent;->KEEP_ON_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    .line 390
    return-void
.end method

.method private onDownloadSuccess()V
    .locals 1

    .prologue
    .line 374
    invoke-static {p0}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/store/Store;->updateKeeponDownloadSongCounts()V

    .line 375
    invoke-direct {p0}, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->notifyUi()V

    .line 376
    return-void
.end method

.method public static sendAllDownloadsCompletedBroadcast(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 551
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.music.KEEPON_DOWNLOADS_COMPLETED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 552
    .local v0, "update":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 553
    return-void
.end method


# virtual methods
.method protected getNextDownloads(Lcom/google/android/music/download/cache/ICacheManager;Ljava/util/Collection;)Ljava/util/List;
    .locals 25
    .param p1, "cacheManager"    # Lcom/google/android/music/download/cache/ICacheManager;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/music/download/cache/ICacheManager;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/download/TrackDownloadRequest;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/download/cache/OutOfSpaceException;
        }
    .end annotation

    .prologue
    .line 144
    .local p2, "blacklistedIds":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Long;>;"
    new-instance v21, Ljava/util/ArrayList;

    invoke-direct/range {v21 .. v21}, Ljava/util/ArrayList;-><init>()V

    .line 146
    .local v21, "requests":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/download/TrackDownloadRequest;>;"
    invoke-static/range {p0 .. p0}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v23

    .line 148
    .local v23, "store":Lcom/google/android/music/store/Store;
    invoke-static {}, Lcom/google/android/music/utils/ConfigUtils;->isUpgradeTempCacheForKeeponEnabled()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 150
    move-object/from16 v0, v23

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/google/android/music/store/Store;->upgradeTempCacheForKeepon(Ljava/util/Collection;)I

    move-result v22

    .line 151
    .local v22, "rowsUpdated":I
    if-lez v22, :cond_0

    .line 154
    invoke-direct/range {p0 .. p0}, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->onDownloadSuccess()V

    .line 160
    .end local v22    # "rowsUpdated":I
    :cond_0
    const/4 v3, 0x1

    move-object/from16 v0, v23

    move-object/from16 v1, p2

    invoke-virtual {v0, v3, v1}, Lcom/google/android/music/store/Store;->getNextKeeponToDownload(ILjava/util/Collection;)[Lcom/google/android/music/download/ContentIdentifier;

    move-result-object v24

    .line 162
    .local v24, "trackIds":[Lcom/google/android/music/download/ContentIdentifier;
    if-nez v24, :cond_2

    .line 163
    sget-boolean v3, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->LOGV:Z

    if-eqz v3, :cond_1

    .line 164
    const-string v3, "KeeponService"

    const-string v5, "getNextDownloads: trackIds=null"

    invoke-static {v3, v5}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 217
    :cond_1
    :goto_0
    return-object v21

    .line 169
    :cond_2
    sget-boolean v3, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->LOGV:Z

    if-eqz v3, :cond_3

    .line 170
    const-string v3, "KeeponService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getNextDownloads: trackIds.length="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v24

    array-length v6, v0

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    :cond_3
    move-object/from16 v2, v24

    .local v2, "arr$":[Lcom/google/android/music/download/ContentIdentifier;
    array-length v0, v2

    move/from16 v18, v0

    .local v18, "len$":I
    const/16 v17, 0x0

    .local v17, "i$":I
    :goto_1
    move/from16 v0, v17

    move/from16 v1, v18

    if-ge v0, v1, :cond_6

    aget-object v4, v2, v17

    .line 174
    .local v4, "id":Lcom/google/android/music/download/ContentIdentifier;
    const/16 v20, 0x0

    .line 176
    .local v20, "musicFile":Lcom/google/android/music/store/MusicFile;
    const/4 v3, 0x0

    :try_start_0
    invoke-virtual {v4}, Lcom/google/android/music/download/ContentIdentifier;->getId()J

    move-result-wide v6

    move-object/from16 v0, v23

    invoke-static {v0, v3, v6, v7}, Lcom/google/android/music/store/MusicFile;->getSummaryMusicFile(Lcom/google/android/music/store/Store;Lcom/google/android/music/store/MusicFile;J)Lcom/google/android/music/store/MusicFile;
    :try_end_0
    .catch Lcom/google/android/music/store/DataNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v20

    .line 180
    :goto_2
    if-nez v20, :cond_4

    .line 173
    :goto_3
    add-int/lit8 v17, v17, 0x1

    goto :goto_1

    .line 177
    :catch_0
    move-exception v16

    .line 178
    .local v16, "e":Lcom/google/android/music/store/DataNotFoundException;
    const-string v3, "KeeponService"

    const-string v5, "Failed to load track data: "

    move-object/from16 v0, v16

    invoke-static {v3, v5, v0}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 183
    .end local v16    # "e":Lcom/google/android/music/store/DataNotFoundException;
    :cond_4
    const/4 v13, 0x0

    .line 186
    .local v13, "fileLocation":Lcom/google/android/music/download/cache/FileLocation;
    :try_start_1
    sget-object v3, Lcom/google/android/music/download/TrackOwner;->KEEPON:Lcom/google/android/music/download/TrackOwner;

    invoke-virtual {v3}, Lcom/google/android/music/download/TrackOwner;->ordinal()I

    move-result v5

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/music/store/MusicFile;->getSize()J

    move-result-wide v6

    const/4 v8, 0x3

    move-object/from16 v3, p1

    invoke-interface/range {v3 .. v8}, Lcom/google/android/music/download/cache/ICacheManager;->getTempFileLocation(Lcom/google/android/music/download/ContentIdentifier;IJI)Lcom/google/android/music/download/cache/FileLocation;

    move-result-object v13

    .line 189
    if-nez v13, :cond_5

    .line 190
    const-string v19, "Failed to get file location."

    .line 191
    .local v19, "msg":Ljava/lang/String;
    const-string v3, "KeeponService"

    move-object/from16 v0, v19

    invoke-static {v3, v0}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    new-instance v3, Lcom/google/android/music/download/cache/OutOfSpaceException;

    move-object/from16 v0, v19

    invoke-direct {v3, v0}, Lcom/google/android/music/download/cache/OutOfSpaceException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    .line 194
    .end local v19    # "msg":Ljava/lang/String;
    :catch_1
    move-exception v16

    .line 195
    .local v16, "e":Landroid/os/RemoteException;
    const-string v3, "KeeponService"

    const-string v5, "Failed to get temp file location"

    invoke-static {v3, v5}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 198
    .end local v16    # "e":Landroid/os/RemoteException;
    :cond_5
    new-instance v3, Lcom/google/android/music/download/TrackDownloadRequest;

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/music/store/MusicFile;->getTitle()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/music/store/MusicFile;->getSourceId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/music/store/MusicFile;->getSourceAccount()I

    move-result v7

    sget v8, Lcom/google/android/music/download/TrackDownloadRequest;->PRIORITY_KEEPON:I

    sget-object v9, Lcom/google/android/music/download/TrackOwner;->KEEPON:Lcom/google/android/music/download/TrackOwner;

    const-wide/16 v10, 0x0

    const/4 v12, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    invoke-direct/range {v3 .. v15}, Lcom/google/android/music/download/TrackDownloadRequest;-><init>(Lcom/google/android/music/download/ContentIdentifier;Ljava/lang/String;Ljava/lang/String;IILcom/google/android/music/download/TrackOwner;JZLcom/google/android/music/download/cache/FileLocation;Ljava/lang/String;Z)V

    move-object/from16 v0, v21

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 203
    .end local v4    # "id":Lcom/google/android/music/download/ContentIdentifier;
    .end local v13    # "fileLocation":Lcom/google/android/music/download/cache/FileLocation;
    .end local v20    # "musicFile":Lcom/google/android/music/store/MusicFile;
    :cond_6
    invoke-interface/range {v21 .. v21}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    .line 206
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->mKeeponNotificationInfo:Lcom/google/android/music/store/KeeponNotificationInfo;

    monitor-enter v5

    .line 207
    :try_start_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->mKeeponNotificationInfo:Lcom/google/android/music/store/KeeponNotificationInfo;

    move-object/from16 v0, v23

    move-object/from16 v1, p2

    invoke-virtual {v0, v1, v3}, Lcom/google/android/music/store/Store;->populateKeeponNotificationInformation(Ljava/util/Collection;Lcom/google/android/music/store/KeeponNotificationInfo;)V

    .line 210
    sget-boolean v3, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->LOGV:Z

    if-eqz v3, :cond_7

    .line 211
    const-string v3, "KeeponService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "KeeponNotificationInfo populated: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->mKeeponNotificationInfo:Lcom/google/android/music/store/KeeponNotificationInfo;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 213
    :cond_7
    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 215
    move-object/from16 v0, v23

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/google/android/music/store/Store;->isRadioOnlyKeepOn(Ljava/util/Collection;)Z

    move-result v3

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->mIsRadioOnly:Z

    goto/16 :goto_0

    .line 213
    :catchall_0
    move-exception v3

    :try_start_3
    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v3
.end method

.method protected getTotalDownloadSize()J
    .locals 4

    .prologue
    .line 222
    invoke-static {p0}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/store/Store;->getTotalNeedToKeepOn()Landroid/util/Pair;

    move-result-object v0

    .line 223
    .local v0, "downloadInfo":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Long;>;"
    if-nez v0, :cond_0

    .line 224
    const-string v1, "KeeponService"

    const-string v2, "Failed to retrieve the keepon download size"

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 225
    const-wide/16 v2, 0x0

    .line 227
    :goto_0
    return-wide v2

    :cond_0
    iget-object v1, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    goto :goto_0
.end method

.method protected isDownloadingPaused()Z
    .locals 1

    .prologue
    .line 587
    invoke-virtual {p0}, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->getMusicPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->isKeepOnDownloadingPaused()Z

    move-result v0

    return v0
.end method

.method protected notifyAllWorkFinished()V
    .locals 5

    .prologue
    const v4, 0x7f0b01c5

    .line 332
    sget-boolean v2, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->LOGV:Z

    if-eqz v2, :cond_0

    .line 333
    const-string v2, "KeeponService"

    const-string v3, "notifyAllWorkFinished"

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 336
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->stopForegroundService(Z)V

    .line 340
    iget-boolean v2, p0, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->mIsRadioOnly:Z

    if-nez v2, :cond_1

    .line 341
    invoke-virtual {p0}, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 342
    .local v1, "res":Landroid/content/res/Resources;
    invoke-direct {p0}, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->createNotificationBuilder()Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    .line 343
    .local v0, "builder":Landroid/support/v4/app/NotificationCompat$Builder;
    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/support/v4/app/NotificationCompat$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v2

    const v3, 0x7f02011e

    invoke-virtual {v2, v3}, Landroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v2

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v2

    invoke-direct {p0}, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->getContentIntent()Landroid/app/PendingIntent;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/NotificationCompat$Builder;->getNotification()Landroid/app/Notification;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->mNotification:Landroid/app/Notification;

    .line 350
    iget-object v2, p0, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->mNotificationManager:Landroid/app/NotificationManager;

    const/16 v3, 0xa

    iget-object v4, p0, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->mNotification:Landroid/app/Notification;

    invoke-virtual {v2, v3, v4}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 353
    .end local v0    # "builder":Landroid/support/v4/app/NotificationCompat$Builder;
    .end local v1    # "res":Landroid/content/res/Resources;
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->sendAllDownloadsCompletedBroadcast(Landroid/content/Context;)V

    .line 354
    return-void
.end method

.method protected notifyDisabled(Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;)V
    .locals 12
    .param p1, "reason"    # Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;

    .prologue
    .line 400
    sget-boolean v8, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->LOGV:Z

    if-eqz v8, :cond_0

    .line 401
    const-string v8, "KeeponService"

    const-string v9, "notifyDisabled: reason=%s"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object p1, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 403
    :cond_0
    const/4 v8, 0x1

    invoke-virtual {p0, v8}, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->stopForegroundService(Z)V

    .line 404
    const/4 v8, 0x0

    iput-object v8, p0, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->mNotification:Landroid/app/Notification;

    .line 406
    invoke-virtual {p0}, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 407
    .local v5, "res":Landroid/content/res/Resources;
    invoke-direct {p0}, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->createNotificationBuilder()Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v2

    .line 408
    .local v2, "builder":Landroid/support/v4/app/NotificationCompat$Builder;
    const v8, 0x7f0b01bf

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Landroid/support/v4/app/NotificationCompat$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v8

    const v9, 0x7f0b01bf

    invoke-virtual {v5, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v8

    const v9, 0x7f020120

    invoke-virtual {v8, v9}, Landroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v2

    .line 413
    sget-object v8, Lcom/google/android/music/download/keepon/KeeponSchedulingService$2;->$SwitchMap$com$google$android$music$download$AbstractSchedulingService$DisableReason:[I

    invoke-virtual {p1}, Lcom/google/android/music/download/AbstractSchedulingService$DisableReason;->ordinal()I

    move-result v9

    aget v8, v8, v9

    packed-switch v8, :pswitch_data_0

    .line 494
    const-string v8, "KeeponService"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Unhandled disable reason: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 497
    :cond_1
    :goto_0
    return-void

    .line 415
    :pswitch_0
    const v8, 0x7f0b01c4

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v8

    invoke-virtual {v8}, Landroid/support/v4/app/NotificationCompat$Builder;->getNotification()Landroid/app/Notification;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->mNotification:Landroid/app/Notification;

    .line 419
    iget-object v8, p0, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->mNotification:Landroid/app/Notification;

    invoke-direct {p0}, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->getContentIntent()Landroid/app/PendingIntent;

    move-result-object v9

    iput-object v9, v8, Landroid/app/Notification;->contentIntent:Landroid/app/PendingIntent;

    .line 420
    iget-object v8, p0, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->mNotificationManager:Landroid/app/NotificationManager;

    const/16 v9, 0xa

    iget-object v10, p0, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->mNotification:Landroid/app/Notification;

    invoke-virtual {v8, v9, v10}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto :goto_0

    .line 424
    :pswitch_1
    iget-boolean v8, p0, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->mShowConnectivityError:Z

    if-eqz v8, :cond_1

    .line 425
    const v8, 0x7f0b01c1

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v8

    const/4 v9, 0x0

    new-instance v10, Landroid/content/Intent;

    const-string v11, "android.net.wifi.PICK_WIFI_NETWORK"

    invoke-direct {v10, v11}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/4 v11, 0x0

    invoke-static {p0, v9, v10, v11}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v8

    invoke-virtual {v8}, Landroid/support/v4/app/NotificationCompat$Builder;->getNotification()Landroid/app/Notification;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->mNotification:Landroid/app/Notification;

    .line 431
    iget-object v8, p0, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->mNotificationManager:Landroid/app/NotificationManager;

    const/16 v9, 0xa

    iget-object v10, p0, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->mNotification:Landroid/app/Notification;

    invoke-virtual {v8, v9, v10}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto :goto_0

    .line 436
    :pswitch_2
    iget-boolean v8, p0, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->mShowConnectivityError:Z

    if-eqz v8, :cond_1

    .line 437
    const v8, 0x7f0b01c2

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v8

    invoke-direct {p0}, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->getContentIntent()Landroid/app/PendingIntent;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v8

    invoke-virtual {v8}, Landroid/support/v4/app/NotificationCompat$Builder;->getNotification()Landroid/app/Notification;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->mNotification:Landroid/app/Notification;

    .line 442
    iget-object v8, p0, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->mNotificationManager:Landroid/app/NotificationManager;

    const/16 v9, 0xa

    iget-object v10, p0, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->mNotification:Landroid/app/Notification;

    invoke-virtual {v8, v9, v10}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto :goto_0

    .line 447
    :pswitch_3
    const v8, 0x7f0b01c3

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v8

    invoke-direct {p0}, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->getContentIntent()Landroid/app/PendingIntent;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v8

    invoke-virtual {v8}, Landroid/support/v4/app/NotificationCompat$Builder;->getNotification()Landroid/app/Notification;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->mNotification:Landroid/app/Notification;

    .line 452
    iget-object v8, p0, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->mNotificationManager:Landroid/app/NotificationManager;

    const/16 v9, 0xa

    iget-object v10, p0, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->mNotification:Landroid/app/Notification;

    invoke-virtual {v8, v9, v10}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto/16 :goto_0

    .line 456
    :pswitch_4
    invoke-virtual {p0}, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->getMusicPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v4

    .line 457
    .local v4, "musicPreferences":Lcom/google/android/music/preferences/MusicPreferences;
    const/4 v1, 0x0

    .line 458
    .local v1, "accountName":Ljava/lang/String;
    if-eqz v4, :cond_3

    .line 459
    invoke-virtual {v4}, Lcom/google/android/music/preferences/MusicPreferences;->getSyncAccount()Landroid/accounts/Account;

    move-result-object v0

    .line 460
    .local v0, "account":Landroid/accounts/Account;
    if-eqz v0, :cond_2

    .line 461
    iget-object v1, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 466
    .end local v0    # "account":Landroid/accounts/Account;
    :cond_2
    :goto_1
    const v8, 0x7f0b0171

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 467
    .local v7, "url":Ljava/lang/String;
    invoke-static {v7, v1}, Lcom/google/android/music/utils/MusicUtils;->buildUriWithAccountName(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    .line 469
    .local v6, "uri":Landroid/net/Uri;
    new-instance v3, Landroid/content/Intent;

    const-string v8, "android.intent.action.VIEW"

    invoke-direct {v3, v8, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 470
    .local v3, "intent":Landroid/content/Intent;
    const v8, 0x7f0b017c

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v8

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-static {p0, v9, v3, v10}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v8

    invoke-virtual {v8}, Landroid/support/v4/app/NotificationCompat$Builder;->getNotification()Landroid/app/Notification;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->mNotification:Landroid/app/Notification;

    .line 476
    iget-object v8, p0, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->mNotificationManager:Landroid/app/NotificationManager;

    const/16 v9, 0xa

    iget-object v10, p0, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->mNotification:Landroid/app/Notification;

    invoke-virtual {v8, v9, v10}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto/16 :goto_0

    .line 464
    .end local v3    # "intent":Landroid/content/Intent;
    .end local v6    # "uri":Landroid/net/Uri;
    .end local v7    # "url":Ljava/lang/String;
    :cond_3
    const-string v8, "KeeponService"

    const-string v9, "Failed to get music preferences"

    invoke-static {v8, v9}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 480
    .end local v1    # "accountName":Ljava/lang/String;
    .end local v4    # "musicPreferences":Lcom/google/android/music/preferences/MusicPreferences;
    :pswitch_5
    invoke-direct {p0}, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->getDownloadingContainerTitle()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Landroid/support/v4/app/NotificationCompat$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v8

    invoke-direct {p0}, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->getDownloadingContainerTitle()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v8

    const v9, 0x7f0b01c0

    invoke-virtual {v5, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v8

    const v9, 0x7f02011f

    invoke-virtual {v8, v9}, Landroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v8

    invoke-virtual {v8}, Landroid/support/v4/app/NotificationCompat$Builder;->getNotification()Landroid/app/Notification;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->mNotification:Landroid/app/Notification;

    .line 488
    iget-object v8, p0, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->mNotification:Landroid/app/Notification;

    invoke-direct {p0}, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->getContentIntent()Landroid/app/PendingIntent;

    move-result-object v9

    iput-object v9, v8, Landroid/app/Notification;->contentIntent:Landroid/app/PendingIntent;

    .line 490
    iget-object v8, p0, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->mNotificationManager:Landroid/app/NotificationManager;

    const/16 v9, 0xa

    iget-object v10, p0, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->mNotification:Landroid/app/Notification;

    invoke-virtual {v8, v9, v10}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto/16 :goto_0

    .line 413
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method protected notifyDownloadCompleted(Lcom/google/android/music/download/TrackDownloadRequest;Lcom/google/android/music/download/TrackDownloadProgress;)V
    .locals 7
    .param p1, "request"    # Lcom/google/android/music/download/TrackDownloadRequest;
    .param p2, "progress"    # Lcom/google/android/music/download/TrackDownloadProgress;

    .prologue
    .line 359
    invoke-virtual {p2}, Lcom/google/android/music/download/TrackDownloadProgress;->isSavable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 360
    invoke-virtual {p2}, Lcom/google/android/music/download/TrackDownloadProgress;->getHttpContentType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Lcom/google/android/music/download/TrackDownloadProgress;->getDownloadByteLength()J

    move-result-wide v4

    invoke-virtual {p2}, Lcom/google/android/music/download/TrackDownloadProgress;->getStreamFidelity()I

    move-result v6

    move-object v1, p0

    move-object v2, p1

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->storeInCache(Lcom/google/android/music/download/TrackDownloadRequest;Ljava/lang/String;JI)V

    .line 362
    invoke-direct {p0}, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->onDownloadSuccess()V

    .line 367
    :goto_0
    return-void

    .line 364
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->deleteFromStorage(Lcom/google/android/music/download/TrackDownloadRequest;)V

    .line 365
    invoke-direct {p0}, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->notifyUi()V

    goto :goto_0
.end method

.method protected notifyDownloadFailed(Lcom/google/android/music/download/TrackDownloadRequest;Lcom/google/android/music/download/TrackDownloadProgress;)V
    .locals 0
    .param p1, "request"    # Lcom/google/android/music/download/TrackDownloadRequest;
    .param p2, "progress"    # Lcom/google/android/music/download/TrackDownloadProgress;

    .prologue
    .line 395
    invoke-virtual {p0, p1}, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->deleteFromStorage(Lcom/google/android/music/download/TrackDownloadRequest;)V

    .line 396
    return-void
.end method

.method protected notifyDownloadProgress(FLcom/google/android/music/download/TrackDownloadProgress;)V
    .locals 11
    .param p1, "progressRatio"    # F
    .param p2, "progress"    # Lcom/google/android/music/download/TrackDownloadProgress;

    .prologue
    const/16 v10, 0x64

    const/16 v9, 0xa

    const/4 v8, 0x0

    .line 247
    sget-boolean v5, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->LOGV:Z

    if-eqz v5, :cond_0

    .line 248
    const-string v5, "KeeponService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "progress: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 251
    :cond_0
    iget-object v5, p0, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->mKeeponCallbackManager:Lcom/google/android/music/download/keepon/KeeponCallbackManagerImpl;

    invoke-virtual {v5, p2}, Lcom/google/android/music/download/keepon/KeeponCallbackManagerImpl;->notifyListeners(Lcom/google/android/music/download/DownloadProgress;)V

    .line 256
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->mShowConnectivityError:Z

    .line 258
    invoke-static {p0}, Lcom/google/android/music/utils/SystemUtils;->isTalkbackOn(Landroid/content/Context;)Z

    move-result v1

    .line 262
    .local v1, "isTalkbackOn":Z
    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->isForeground()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 294
    :goto_0
    return-void

    .line 264
    :cond_1
    iget-object v5, p0, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->mDownloadProgressNotificationBuilder:Landroid/support/v4/app/NotificationCompat$Builder;

    if-nez v5, :cond_2

    .line 265
    invoke-direct {p0}, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->createDownloadingProgressNotificationBuilder()Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->mDownloadProgressNotificationBuilder:Landroid/support/v4/app/NotificationCompat$Builder;

    .line 268
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 270
    .local v4, "res":Landroid/content/res/Resources;
    const/high16 v5, 0x42c80000    # 100.0f

    mul-float/2addr v5, p1

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v5

    invoke-static {v8, v5}, Ljava/lang/Math;->max(II)I

    move-result v5

    invoke-static {v10, v5}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 272
    .local v0, "intProgress":I
    iget-object v5, p0, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->mDownloadProgressNotificationBuilder:Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-direct {p0}, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->getDownloadingContainerTitle()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 275
    if-nez v1, :cond_3

    .line 276
    iget-object v5, p0, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->mDownloadProgressNotificationBuilder:Landroid/support/v4/app/NotificationCompat$Builder;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "%"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 277
    iget-object v5, p0, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->mDownloadProgressNotificationBuilder:Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-virtual {v5, v10, v0, v8}, Landroid/support/v4/app/NotificationCompat$Builder;->setProgress(IIZ)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 280
    :cond_3
    iget-object v5, p0, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->mDownloadProgressNotificationBuilder:Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-virtual {v5}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v3

    .line 281
    .local v3, "notification":Landroid/app/Notification;
    invoke-direct {p0}, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->getContentIntent()Landroid/app/PendingIntent;

    move-result-object v5

    iput-object v5, v3, Landroid/app/Notification;->contentIntent:Landroid/app/PendingIntent;

    .line 287
    invoke-virtual {p0}, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->isForeground()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 288
    const-string v5, "notification"

    invoke-virtual {p0, v5}, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/NotificationManager;

    .line 290
    .local v2, "nmgr":Landroid/app/NotificationManager;
    invoke-virtual {v2, v9, v3}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto :goto_0

    .line 292
    .end local v2    # "nmgr":Landroid/app/NotificationManager;
    :cond_4
    invoke-virtual {p0, v9, v3}, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->startForegroundService(ILandroid/app/Notification;)V

    goto :goto_0
.end method

.method protected notifyDownloadStarting()V
    .locals 5

    .prologue
    .line 232
    sget-boolean v2, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->LOGV:Z

    if-eqz v2, :cond_0

    .line 233
    const-string v2, "KeeponService"

    const-string v3, "notifyDownloadStarting"

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 237
    .local v1, "res":Landroid/content/res/Resources;
    invoke-direct {p0}, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->createNotificationBuilder()Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    .line 238
    .local v0, "builder":Landroid/support/v4/app/NotificationCompat$Builder;
    invoke-direct {p0}, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->getDownloadingContainerTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v2

    invoke-direct {p0}, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->getContentIntent()Landroid/app/PendingIntent;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/NotificationCompat$Builder;->getNotification()Landroid/app/Notification;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->mNotification:Landroid/app/Notification;

    .line 242
    iget-object v2, p0, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->mNotificationManager:Landroid/app/NotificationManager;

    const/16 v3, 0xa

    iget-object v4, p0, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->mNotification:Landroid/app/Notification;

    invoke-virtual {v2, v3, v4}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 243
    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 137
    iget-object v0, p0, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->mKeeponCallbackManager:Lcom/google/android/music/download/keepon/KeeponCallbackManagerImpl;

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 124
    invoke-super {p0}, Lcom/google/android/music/download/AbstractSchedulingService;->onCreate()V

    .line 125
    const-string v0, "notification"

    invoke-virtual {p0, v0}, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->mNotificationManager:Landroid/app/NotificationManager;

    .line 126
    invoke-virtual {p0}, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->getMusicPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->mPrefChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-virtual {v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 127
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 131
    invoke-virtual {p0}, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->getMusicPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->mPrefChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-virtual {v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 132
    invoke-super {p0}, Lcom/google/android/music/download/AbstractSchedulingService;->onDestroy()V

    .line 133
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 103
    sget-boolean v1, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->LOGV:Z

    if-eqz v1, :cond_0

    .line 104
    const-string v1, "KeeponService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "intent="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    :cond_0
    iput p3, p0, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->mStartId:I

    .line 109
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 111
    .local v0, "action":Ljava/lang/String;
    :goto_0
    const-string v1, "com.google.android.music.download.keepon.KeeponSchedulingService.STOP_DOWNLOAD"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 112
    invoke-virtual {p0}, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->stopSelf()V

    .line 119
    :goto_1
    const/4 v1, 0x3

    return v1

    .line 109
    .end local v0    # "action":Ljava/lang/String;
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 113
    .restart local v0    # "action":Ljava/lang/String;
    :cond_2
    const-string v1, "com.google.android.music.download.keepon.KeeponSchedulingService.START_DOWNLOAD"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 114
    invoke-virtual {p0, p3}, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->sendInitScheduleMessage(I)V

    goto :goto_1

    .line 116
    :cond_3
    const-string v1, "KeeponService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown action="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    invoke-virtual {p0}, Lcom/google/android/music/download/keepon/KeeponSchedulingService;->stopSelf()V

    goto :goto_1
.end method
