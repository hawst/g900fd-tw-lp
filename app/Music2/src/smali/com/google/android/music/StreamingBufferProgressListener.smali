.class public Lcom/google/android/music/StreamingBufferProgressListener;
.super Lcom/google/android/music/BufferProgressListener;
.source "StreamingBufferProgressListener.java"


# static fields
.field private static final LOGV:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->UI:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/StreamingBufferProgressListener;->LOGV:Z

    return-void
.end method

.method public constructor <init>(Landroid/widget/ProgressBar;)V
    .locals 0
    .param p1, "progress"    # Landroid/widget/ProgressBar;

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lcom/google/android/music/BufferProgressListener;-><init>(Landroid/widget/ProgressBar;)V

    .line 25
    return-void
.end method


# virtual methods
.method protected connectListener(Lcom/google/android/music/download/ContentIdentifier;)V
    .locals 3
    .param p1, "trackId"    # Lcom/google/android/music/download/ContentIdentifier;

    .prologue
    .line 30
    :try_start_0
    sget-object v1, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    if-eqz v1, :cond_0

    .line 31
    sget-object v1, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    iget-object v2, p0, Lcom/google/android/music/StreamingBufferProgressListener;->mDownloadProgressListener:Lcom/google/android/music/download/IDownloadProgressListener;

    invoke-interface {v1, p1, v2}, Lcom/google/android/music/playback/IMusicPlaybackService;->addDownloadProgressListener(Lcom/google/android/music/download/ContentIdentifier;Lcom/google/android/music/download/IDownloadProgressListener;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 36
    :cond_0
    :goto_0
    return-void

    .line 33
    :catch_0
    move-exception v0

    .line 34
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "SBufferProgressListener"

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method protected disconnectListener()V
    .locals 3

    .prologue
    .line 41
    :try_start_0
    sget-object v1, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    if-eqz v1, :cond_0

    .line 42
    sget-object v1, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    iget-object v2, p0, Lcom/google/android/music/StreamingBufferProgressListener;->mDownloadProgressListener:Lcom/google/android/music/download/IDownloadProgressListener;

    invoke-interface {v1, v2}, Lcom/google/android/music/playback/IMusicPlaybackService;->removeDownloadProgressListener(Lcom/google/android/music/download/IDownloadProgressListener;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 47
    :cond_0
    :goto_0
    return-void

    .line 44
    :catch_0
    move-exception v0

    .line 45
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "SBufferProgressListener"

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
