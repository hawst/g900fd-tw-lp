.class Lcom/google/android/music/cast/CastSessionManager$3;
.super Landroid/support/v7/media/MediaRouter$ControlRequestCallback;
.source "CastSessionManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/cast/CastSessionManager;->handleEndSession()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/cast/CastSessionManager;


# direct methods
.method constructor <init>(Lcom/google/android/music/cast/CastSessionManager;)V
    .locals 0

    .prologue
    .line 208
    iput-object p1, p0, Lcom/google/android/music/cast/CastSessionManager$3;->this$0:Lcom/google/android/music/cast/CastSessionManager;

    invoke-direct {p0}, Landroid/support/v7/media/MediaRouter$ControlRequestCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 3
    .param p1, "error"    # Ljava/lang/String;
    .param p2, "data"    # Landroid/os/Bundle;

    .prologue
    .line 221
    const-string v0, "MusicCastSession"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error encountered requesting remote end session: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p2}, Lcom/google/android/music/utils/DebugUtils;->bundleToString(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 223
    iget-object v0, p0, Lcom/google/android/music/cast/CastSessionManager$3;->this$0:Lcom/google/android/music/cast/CastSessionManager;

    # invokes: Lcom/google/android/music/cast/CastSessionManager;->reportSessionEnded()V
    invoke-static {v0}, Lcom/google/android/music/cast/CastSessionManager;->access$700(Lcom/google/android/music/cast/CastSessionManager;)V

    .line 224
    return-void
.end method

.method public onResult(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "data"    # Landroid/os/Bundle;

    .prologue
    .line 211
    iget-object v0, p0, Lcom/google/android/music/cast/CastSessionManager$3;->this$0:Lcom/google/android/music/cast/CastSessionManager;

    # invokes: Lcom/google/android/music/cast/CastSessionManager;->isLogVerbose()Z
    invoke-static {v0}, Lcom/google/android/music/cast/CastSessionManager;->access$200(Lcom/google/android/music/cast/CastSessionManager;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 212
    const-string v0, "MusicCastSession"

    const-string v1, "onResult data=%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Lcom/google/android/music/utils/DebugUtils;->bundleToString(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 215
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/cast/CastSessionManager$3;->this$0:Lcom/google/android/music/cast/CastSessionManager;

    # invokes: Lcom/google/android/music/cast/CastSessionManager;->processControlRequestResultBundle(Landroid/os/Bundle;)V
    invoke-static {v0, p1}, Lcom/google/android/music/cast/CastSessionManager;->access$600(Lcom/google/android/music/cast/CastSessionManager;Landroid/os/Bundle;)V

    .line 216
    iget-object v0, p0, Lcom/google/android/music/cast/CastSessionManager$3;->this$0:Lcom/google/android/music/cast/CastSessionManager;

    # invokes: Lcom/google/android/music/cast/CastSessionManager;->reportSessionEnded()V
    invoke-static {v0}, Lcom/google/android/music/cast/CastSessionManager;->access$700(Lcom/google/android/music/cast/CastSessionManager;)V

    .line 217
    return-void
.end method
