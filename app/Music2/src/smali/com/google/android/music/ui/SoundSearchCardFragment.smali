.class public Lcom/google/android/music/ui/SoundSearchCardFragment;
.super Landroid/support/v4/app/Fragment;
.source "SoundSearchCardFragment.java"


# instance fields
.field private mContent:Landroid/widget/TextView;

.field private mTitle:Landroid/widget/TextView;

.field private mTryButton:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x2

    .line 27
    const v1, 0x7f0400e7

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 28
    .local v0, "rootView":Landroid/view/View;
    const v1, 0x7f0e017a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/music/ui/SoundSearchCardFragment;->mTitle:Landroid/widget/TextView;

    .line 29
    const v1, 0x7f0e017b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/music/ui/SoundSearchCardFragment;->mContent:Landroid/widget/TextView;

    .line 31
    iget-object v1, p0, Lcom/google/android/music/ui/SoundSearchCardFragment;->mTitle:Landroid/widget/TextView;

    invoke-static {v1, v3}, Lcom/google/android/music/utils/TypefaceUtil;->setTypeface(Landroid/widget/TextView;I)V

    .line 32
    iget-object v1, p0, Lcom/google/android/music/ui/SoundSearchCardFragment;->mContent:Landroid/widget/TextView;

    invoke-static {v1, v3}, Lcom/google/android/music/utils/TypefaceUtil;->setTypeface(Landroid/widget/TextView;I)V

    .line 34
    const v1, 0x7f0e0267

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/ui/SoundSearchCardFragment;->mTryButton:Landroid/view/View;

    .line 35
    iget-object v1, p0, Lcom/google/android/music/ui/SoundSearchCardFragment;->mTryButton:Landroid/view/View;

    new-instance v2, Lcom/google/android/music/ui/SoundSearchCardFragment$1;

    invoke-direct {v2, p0}, Lcom/google/android/music/ui/SoundSearchCardFragment$1;-><init>(Lcom/google/android/music/ui/SoundSearchCardFragment;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 44
    return-object v0
.end method
