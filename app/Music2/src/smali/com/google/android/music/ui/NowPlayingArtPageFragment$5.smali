.class Lcom/google/android/music/ui/NowPlayingArtPageFragment$5;
.super Ljava/lang/Object;
.source "NowPlayingArtPageFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/NowPlayingArtPageFragment;->initializeView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/NowPlayingArtPageFragment;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/NowPlayingArtPageFragment;)V
    .locals 0

    .prologue
    .line 314
    iput-object p1, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment$5;->this$0:Lcom/google/android/music/ui/NowPlayingArtPageFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 317
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment$5;->this$0:Lcom/google/android/music/ui/NowPlayingArtPageFragment;

    # getter for: Lcom/google/android/music/ui/NowPlayingArtPageFragment;->VIDEO_WIDGET_STATE_START:I
    invoke-static {}, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->access$1200()I

    move-result v1

    # setter for: Lcom/google/android/music/ui/NowPlayingArtPageFragment;->mVideoWidgetState:I
    invoke-static {v0, v1}, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->access$1102(Lcom/google/android/music/ui/NowPlayingArtPageFragment;I)I

    .line 318
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment$5;->this$0:Lcom/google/android/music/ui/NowPlayingArtPageFragment;

    # invokes: Lcom/google/android/music/ui/NowPlayingArtPageFragment;->updateYouTubeWidgets()V
    invoke-static {v0}, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->access$1300(Lcom/google/android/music/ui/NowPlayingArtPageFragment;)V

    .line 319
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingArtPageFragment$5;->this$0:Lcom/google/android/music/ui/NowPlayingArtPageFragment;

    # invokes: Lcom/google/android/music/ui/NowPlayingArtPageFragment;->scheduleYouTubeAnimation()V
    invoke-static {v0}, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->access$1400(Lcom/google/android/music/ui/NowPlayingArtPageFragment;)V

    .line 320
    return-void
.end method
