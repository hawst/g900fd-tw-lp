.class Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;
.super Landroid/support/v17/leanback/widget/ObjectAdapter;
.source "LeanbackArtistDetailsActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ArtistDetailsObjectAdapter"
.end annotation


# instance fields
.field private mAlbumsInLockerRow:Landroid/support/v17/leanback/widget/ListRow;

.field private mAlbumsInLockerRowIndex:I

.field private mAllAlbumsRow:Landroid/support/v17/leanback/widget/ListRow;

.field private mAllAlbumsRowIndex:I

.field private final mDetailsObjectAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

.field private mDetailsObjectAdapterStartIndex:I

.field private mExtraDataLoaded:Z

.field private mRelatedArtistsRow:Landroid/support/v17/leanback/widget/ListRow;

.field private mRelatedArtistsRowIndex:I

.field private mSize:I

.field private final mSongFooterRow:Lcom/google/android/music/leanback/SongFooterRow;

.field private mSongFooterRowIndex:I

.field private mSongListObjectAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

.field private mSongListObjectAdapterStartIndex:I

.field private final mSongTitleRow:Lcom/google/android/music/leanback/SongTitleRow;

.field final synthetic this$0:Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;


# direct methods
.method constructor <init>(Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;Landroid/support/v17/leanback/widget/PresenterSelector;Landroid/support/v17/leanback/widget/ObjectAdapter;Lcom/google/android/music/leanback/SongTitleRow;Lcom/google/android/music/leanback/SongFooterRow;)V
    .locals 2
    .param p2, "presenterSelector"    # Landroid/support/v17/leanback/widget/PresenterSelector;
    .param p3, "detailsObjectAdapter"    # Landroid/support/v17/leanback/widget/ObjectAdapter;
    .param p4, "songTitleRow"    # Lcom/google/android/music/leanback/SongTitleRow;
    .param p5, "songFooterRow"    # Lcom/google/android/music/leanback/SongFooterRow;

    .prologue
    .line 375
    iput-object p1, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->this$0:Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;

    .line 376
    invoke-direct {p0, p2}, Landroid/support/v17/leanback/widget/ObjectAdapter;-><init>(Landroid/support/v17/leanback/widget/PresenterSelector;)V

    .line 371
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->mExtraDataLoaded:Z

    .line 377
    iput-object p3, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->mDetailsObjectAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    .line 378
    iput-object p4, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->mSongTitleRow:Lcom/google/android/music/leanback/SongTitleRow;

    .line 379
    iput-object p5, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->mSongFooterRow:Lcom/google/android/music/leanback/SongFooterRow;

    .line 380
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->mDetailsObjectAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    new-instance v1, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter$1;

    invoke-direct {v1, p0, p1}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter$1;-><init>(Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;)V

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/ObjectAdapter;->registerObserver(Landroid/support/v17/leanback/widget/ObjectAdapter$DataObserver;)V

    .line 410
    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->recalculateSize()V

    .line 411
    return-void
.end method

.method static synthetic access$1100(Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;II)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 356
    invoke-virtual {p0, p1, p2}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->notifyItemRangeChanged(II)V

    return-void
.end method

.method static synthetic access$1300(Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;

    .prologue
    .line 356
    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->notifyChanged()V

    return-void
.end method

.method static synthetic access$1500(Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;

    .prologue
    .line 356
    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->notifyChanged()V

    return-void
.end method

.method static synthetic access$1700(Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;

    .prologue
    .line 356
    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->notifyChanged()V

    return-void
.end method

.method static synthetic access$1900(Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;

    .prologue
    .line 356
    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->notifyChanged()V

    return-void
.end method

.method static synthetic access$2100(Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;)Landroid/support/v17/leanback/widget/ObjectAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;

    .prologue
    .line 356
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->mSongListObjectAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;II)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 356
    invoke-virtual {p0, p1, p2}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->notifyItemRangeChanged(II)V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;

    .prologue
    .line 356
    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->recalculateSize()V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;

    .prologue
    .line 356
    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->notifyChanged()V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;II)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 356
    invoke-virtual {p0, p1, p2}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->notifyItemRangeChanged(II)V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;II)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 356
    invoke-virtual {p0, p1, p2}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->notifyItemRangeInserted(II)V

    return-void
.end method

.method static synthetic access$900(Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;II)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 356
    invoke-virtual {p0, p1, p2}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->notifyItemRangeRemoved(II)V

    return-void
.end method

.method private loadExtraData(Landroid/support/v17/leanback/widget/DetailsOverviewRow;)V
    .locals 6
    .param p1, "detailsOverviewRow"    # Landroid/support/v17/leanback/widget/DetailsOverviewRow;

    .prologue
    .line 494
    invoke-virtual {p1}, Landroid/support/v17/leanback/widget/DetailsOverviewRow;->getItem()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/leanback/Item;

    .line 495
    .local v0, "artistItem":Lcom/google/android/music/leanback/Item;
    new-instance v2, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter$2;

    invoke-direct {v2, p0, v0, p1}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter$2;-><init>(Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;Lcom/google/android/music/leanback/Item;Landroid/support/v17/leanback/widget/DetailsOverviewRow;)V

    invoke-static {v2}, Lcom/google/android/music/utils/MusicUtils;->runAsyncWithCallback(Lcom/google/android/music/utils/async/AsyncRunner;)V

    .line 511
    invoke-virtual {v0}, Lcom/google/android/music/leanback/Item;->getSecondaryId()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    .line 512
    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->this$0:Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;

    invoke-virtual {v0}, Lcom/google/android/music/leanback/Item;->getSecondaryId()J

    move-result-wide v4

    invoke-virtual {v0}, Lcom/google/android/music/leanback/Item;->getTitle()Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;->createLockerAlbumsRow(JLjava/lang/String;)Landroid/support/v17/leanback/widget/ListRow;
    invoke-static {v2, v4, v5, v3}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;->access$1200(Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;JLjava/lang/String;)Landroid/support/v17/leanback/widget/ListRow;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->mAlbumsInLockerRow:Landroid/support/v17/leanback/widget/ListRow;

    .line 514
    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->mAlbumsInLockerRow:Landroid/support/v17/leanback/widget/ListRow;

    invoke-virtual {v2}, Landroid/support/v17/leanback/widget/ListRow;->getAdapter()Landroid/support/v17/leanback/widget/ObjectAdapter;

    move-result-object v2

    new-instance v3, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter$3;

    invoke-direct {v3, p0}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter$3;-><init>(Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;)V

    invoke-virtual {v2, v3}, Landroid/support/v17/leanback/widget/ObjectAdapter;->registerObserver(Landroid/support/v17/leanback/widget/ObjectAdapter$DataObserver;)V

    .line 522
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/music/leanback/Item;->getId()Ljava/lang/String;

    move-result-object v1

    .line 523
    .local v1, "nautilusArtistId":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 562
    :cond_1
    :goto_0
    return-void

    .line 526
    :cond_2
    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->this$0:Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;

    invoke-virtual {v0}, Lcom/google/android/music/leanback/Item;->getTitle()Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;->createNautilusAlbumsRow(Ljava/lang/String;Ljava/lang/String;)Landroid/support/v17/leanback/widget/ListRow;
    invoke-static {v2, v1, v3}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;->access$1400(Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;Ljava/lang/String;Ljava/lang/String;)Landroid/support/v17/leanback/widget/ListRow;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->mAllAlbumsRow:Landroid/support/v17/leanback/widget/ListRow;

    .line 527
    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->mAllAlbumsRow:Landroid/support/v17/leanback/widget/ListRow;

    if-eqz v2, :cond_3

    .line 528
    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->mAllAlbumsRow:Landroid/support/v17/leanback/widget/ListRow;

    invoke-virtual {v2}, Landroid/support/v17/leanback/widget/ListRow;->getAdapter()Landroid/support/v17/leanback/widget/ObjectAdapter;

    move-result-object v2

    new-instance v3, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter$4;

    invoke-direct {v3, p0}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter$4;-><init>(Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;)V

    invoke-virtual {v2, v3}, Landroid/support/v17/leanback/widget/ObjectAdapter;->registerObserver(Landroid/support/v17/leanback/widget/ObjectAdapter$DataObserver;)V

    .line 536
    :cond_3
    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->this$0:Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;

    # invokes: Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;->createRelatedArtistsRow(Ljava/lang/String;)Landroid/support/v17/leanback/widget/ListRow;
    invoke-static {v2, v1}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;->access$1600(Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;Ljava/lang/String;)Landroid/support/v17/leanback/widget/ListRow;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->mRelatedArtistsRow:Landroid/support/v17/leanback/widget/ListRow;

    .line 537
    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->mRelatedArtistsRow:Landroid/support/v17/leanback/widget/ListRow;

    if-eqz v2, :cond_4

    .line 538
    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->mRelatedArtistsRow:Landroid/support/v17/leanback/widget/ListRow;

    invoke-virtual {v2}, Landroid/support/v17/leanback/widget/ListRow;->getAdapter()Landroid/support/v17/leanback/widget/ObjectAdapter;

    move-result-object v2

    new-instance v3, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter$5;

    invoke-direct {v3, p0}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter$5;-><init>(Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;)V

    invoke-virtual {v2, v3}, Landroid/support/v17/leanback/widget/ObjectAdapter;->registerObserver(Landroid/support/v17/leanback/widget/ObjectAdapter$DataObserver;)V

    .line 546
    :cond_4
    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->this$0:Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;

    # invokes: Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;->createSongList(Ljava/lang/String;Lcom/google/android/music/leanback/Item;)Landroid/support/v17/leanback/widget/ObjectAdapter;
    invoke-static {v2, v1, v0}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;->access$1800(Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;Ljava/lang/String;Lcom/google/android/music/leanback/Item;)Landroid/support/v17/leanback/widget/ObjectAdapter;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->mSongListObjectAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    .line 547
    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->mSongListObjectAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    if-eqz v2, :cond_1

    .line 548
    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->mSongListObjectAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    new-instance v3, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter$6;

    invoke-direct {v3, p0, p1}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter$6;-><init>(Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;Landroid/support/v17/leanback/widget/DetailsOverviewRow;)V

    invoke-virtual {v2, v3}, Landroid/support/v17/leanback/widget/ObjectAdapter;->registerObserver(Landroid/support/v17/leanback/widget/ObjectAdapter$DataObserver;)V

    goto :goto_0
.end method

.method private recalculateSize()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, -0x1

    .line 449
    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->mDetailsObjectAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    invoke-virtual {v2}, Landroid/support/v17/leanback/widget/ObjectAdapter;->size()I

    move-result v2

    invoke-static {v2, v5}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 450
    .local v1, "size":I
    if-lez v1, :cond_1

    .line 451
    iput v4, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->mDetailsObjectAdapterStartIndex:I

    .line 452
    iget-boolean v2, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->mExtraDataLoaded:Z

    if-nez v2, :cond_0

    .line 453
    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->mDetailsObjectAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    invoke-virtual {v2, v4}, Landroid/support/v17/leanback/widget/ObjectAdapter;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v17/leanback/widget/DetailsOverviewRow;

    .line 455
    .local v0, "detailsOverviewRow":Landroid/support/v17/leanback/widget/DetailsOverviewRow;
    invoke-direct {p0, v0}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->loadExtraData(Landroid/support/v17/leanback/widget/DetailsOverviewRow;)V

    .line 456
    iput-boolean v5, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->mExtraDataLoaded:Z

    .line 461
    .end local v0    # "detailsOverviewRow":Landroid/support/v17/leanback/widget/DetailsOverviewRow;
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->mSongListObjectAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->mSongListObjectAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    invoke-virtual {v2}, Landroid/support/v17/leanback/widget/ObjectAdapter;->size()I

    move-result v2

    if-lez v2, :cond_2

    .line 462
    add-int/lit8 v1, v1, 0x1

    .line 463
    iput v1, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->mSongListObjectAdapterStartIndex:I

    .line 464
    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->mSongListObjectAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    invoke-virtual {v2}, Landroid/support/v17/leanback/widget/ObjectAdapter;->size()I

    move-result v2

    add-int/2addr v1, v2

    .line 465
    iput v1, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->mSongFooterRowIndex:I

    .line 466
    add-int/lit8 v1, v1, 0x1

    .line 471
    :goto_1
    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->mAlbumsInLockerRow:Landroid/support/v17/leanback/widget/ListRow;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->mAlbumsInLockerRow:Landroid/support/v17/leanback/widget/ListRow;

    invoke-virtual {v2}, Landroid/support/v17/leanback/widget/ListRow;->getAdapter()Landroid/support/v17/leanback/widget/ObjectAdapter;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v17/leanback/widget/ObjectAdapter;->size()I

    move-result v2

    if-lez v2, :cond_3

    .line 472
    iput v1, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->mAlbumsInLockerRowIndex:I

    .line 473
    add-int/lit8 v1, v1, 0x1

    .line 477
    :goto_2
    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->mAllAlbumsRow:Landroid/support/v17/leanback/widget/ListRow;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->mAllAlbumsRow:Landroid/support/v17/leanback/widget/ListRow;

    invoke-virtual {v2}, Landroid/support/v17/leanback/widget/ListRow;->getAdapter()Landroid/support/v17/leanback/widget/ObjectAdapter;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v17/leanback/widget/ObjectAdapter;->size()I

    move-result v2

    if-lez v2, :cond_4

    .line 478
    iput v1, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->mAllAlbumsRowIndex:I

    .line 479
    add-int/lit8 v1, v1, 0x1

    .line 483
    :goto_3
    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->mRelatedArtistsRow:Landroid/support/v17/leanback/widget/ListRow;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->mRelatedArtistsRow:Landroid/support/v17/leanback/widget/ListRow;

    invoke-virtual {v2}, Landroid/support/v17/leanback/widget/ListRow;->getAdapter()Landroid/support/v17/leanback/widget/ObjectAdapter;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v17/leanback/widget/ObjectAdapter;->size()I

    move-result v2

    if-lez v2, :cond_5

    .line 484
    iput v1, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->mRelatedArtistsRowIndex:I

    .line 485
    add-int/lit8 v1, v1, 0x1

    .line 490
    :goto_4
    iput v1, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->mSize:I

    .line 491
    return-void

    .line 459
    :cond_1
    iput v3, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->mDetailsObjectAdapterStartIndex:I

    goto :goto_0

    .line 468
    :cond_2
    iput v3, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->mSongFooterRowIndex:I

    .line 469
    iput v3, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->mSongListObjectAdapterStartIndex:I

    goto :goto_1

    .line 475
    :cond_3
    iput v3, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->mAlbumsInLockerRowIndex:I

    goto :goto_2

    .line 481
    :cond_4
    iput v3, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->mAllAlbumsRowIndex:I

    goto :goto_3

    .line 487
    :cond_5
    iput v3, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->mRelatedArtistsRowIndex:I

    goto :goto_4
.end method


# virtual methods
.method public get(I)Ljava/lang/Object;
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 420
    iget v0, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->mSize:I

    if-ge p1, v0, :cond_7

    .line 421
    iget v0, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->mRelatedArtistsRowIndex:I

    if-ne p1, v0, :cond_0

    .line 422
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->mRelatedArtistsRow:Landroid/support/v17/leanback/widget/ListRow;

    .line 442
    :goto_0
    return-object v0

    .line 424
    :cond_0
    iget v0, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->mAllAlbumsRowIndex:I

    if-ne p1, v0, :cond_1

    .line 425
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->mAllAlbumsRow:Landroid/support/v17/leanback/widget/ListRow;

    goto :goto_0

    .line 427
    :cond_1
    iget v0, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->mAlbumsInLockerRowIndex:I

    if-ne p1, v0, :cond_2

    .line 428
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->mAlbumsInLockerRow:Landroid/support/v17/leanback/widget/ListRow;

    goto :goto_0

    .line 430
    :cond_2
    iget v0, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->mDetailsObjectAdapterStartIndex:I

    if-ne p1, v0, :cond_3

    .line 431
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->mDetailsObjectAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/ObjectAdapter;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 433
    :cond_3
    iget v0, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->mSongFooterRowIndex:I

    if-ne p1, v0, :cond_4

    .line 434
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->mSongFooterRow:Lcom/google/android/music/leanback/SongFooterRow;

    goto :goto_0

    .line 436
    :cond_4
    iget v0, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->mSongListObjectAdapterStartIndex:I

    if-lt p1, v0, :cond_5

    .line 437
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->mSongListObjectAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    iget v1, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->mSongListObjectAdapterStartIndex:I

    sub-int v1, p1, v1

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/ObjectAdapter;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 439
    :cond_5
    iget v0, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->mSongListObjectAdapterStartIndex:I

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_6

    .line 440
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->mSongTitleRow:Lcom/google/android/music/leanback/SongTitleRow;

    goto :goto_0

    .line 442
    :cond_6
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->mDetailsObjectAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    invoke-virtual {v0, p1}, Landroid/support/v17/leanback/widget/ObjectAdapter;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 444
    :cond_7
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Tried to get unexpected position "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". Size is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->mSize:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 415
    iget v0, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$ArtistDetailsObjectAdapter;->mSize:I

    return v0
.end method
