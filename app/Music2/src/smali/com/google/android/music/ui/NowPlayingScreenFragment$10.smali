.class Lcom/google/android/music/ui/NowPlayingScreenFragment$10;
.super Landroid/support/v4/view/AccessibilityDelegateCompat;
.source "NowPlayingScreenFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/NowPlayingScreenFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/NowPlayingScreenFragment;)V
    .locals 0

    .prologue
    .line 973
    iput-object p1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$10;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    invoke-direct {p0}, Landroid/support/v4/view/AccessibilityDelegateCompat;-><init>()V

    return-void
.end method


# virtual methods
.method public sendAccessibilityEvent(Landroid/view/View;I)V
    .locals 18
    .param p1, "host"    # Landroid/view/View;
    .param p2, "eventType"    # I

    .prologue
    .line 977
    const v11, 0x8000

    move/from16 v0, p2

    if-eq v0, v11, :cond_0

    const/16 v11, 0x80

    move/from16 v0, p2

    if-eq v0, v11, :cond_0

    const/16 v11, 0x8

    move/from16 v0, p2

    if-ne v0, v11, :cond_2

    :cond_0
    const/4 v7, 0x1

    .line 982
    .local v7, "handledEvent":Z
    :goto_0
    if-nez v7, :cond_3

    .line 1014
    :cond_1
    :goto_1
    return-void

    .line 977
    .end local v7    # "handledEvent":Z
    :cond_2
    const/4 v7, 0x0

    goto :goto_0

    .line 985
    .restart local v7    # "handledEvent":Z
    :cond_3
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/ui/NowPlayingScreenFragment$10;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # getter for: Lcom/google/android/music/ui/NowPlayingScreenFragment;->mDuration:J
    invoke-static {v11}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$1600(Lcom/google/android/music/ui/NowPlayingScreenFragment;)J

    move-result-wide v12

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/ui/NowPlayingScreenFragment$10;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # getter for: Lcom/google/android/music/ui/NowPlayingScreenFragment;->mProgress:Lcom/google/android/music/SizableTrackSeekBar;
    invoke-static {v11}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$1200(Lcom/google/android/music/ui/NowPlayingScreenFragment;)Lcom/google/android/music/SizableTrackSeekBar;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/android/music/SizableTrackSeekBar;->getProgress()I

    move-result v11

    int-to-long v14, v11

    mul-long/2addr v12, v14

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/ui/NowPlayingScreenFragment$10;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # getter for: Lcom/google/android/music/ui/NowPlayingScreenFragment;->mProgress:Lcom/google/android/music/SizableTrackSeekBar;
    invoke-static {v11}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$1200(Lcom/google/android/music/ui/NowPlayingScreenFragment;)Lcom/google/android/music/SizableTrackSeekBar;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/android/music/SizableTrackSeekBar;->getMax()I

    move-result v11

    int-to-long v14, v11

    div-long v4, v12, v14

    .line 986
    .local v4, "currentProgress":J
    const-wide/16 v12, 0x3e8

    div-long v12, v4, v12

    const-wide/16 v14, 0x3c

    rem-long/2addr v12, v14

    long-to-int v9, v12

    .line 987
    .local v9, "progressSeconds":I
    const-wide/16 v12, 0x3e8

    div-long v12, v4, v12

    const-wide/16 v14, 0x3c

    div-long/2addr v12, v14

    const-wide/16 v14, 0x3c

    rem-long/2addr v12, v14

    long-to-int v8, v12

    .line 988
    .local v8, "progressMinutes":I
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/ui/NowPlayingScreenFragment$10;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    invoke-virtual {v11}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    .line 989
    .local v3, "context":Landroid/content/Context;
    if-eqz v3, :cond_1

    .line 993
    const-string v11, "accessibility"

    invoke-virtual {v3, v11}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/accessibility/AccessibilityManager;

    .line 995
    .local v2, "accessibilityManager":Landroid/view/accessibility/AccessibilityManager;
    invoke-virtual {v2}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v11

    if-eqz v11, :cond_1

    .line 999
    invoke-static {}, Lcom/google/android/music/preferences/MusicPreferences;->isJellyBeanOrGreater()Z

    move-result v11

    if-eqz v11, :cond_4

    const/16 v11, 0x4000

    :goto_2
    invoke-static {v11}, Landroid/view/accessibility/AccessibilityEvent;->obtain(I)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v6

    .line 1003
    .local v6, "event":Landroid/view/accessibility/AccessibilityEvent;
    const/4 v11, 0x1

    invoke-virtual {v6, v11}, Landroid/view/accessibility/AccessibilityEvent;->setEnabled(Z)V

    .line 1004
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v6, v11}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    .line 1005
    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v6, v11}, Landroid/view/accessibility/AccessibilityEvent;->setPackageName(Ljava/lang/CharSequence;)V

    .line 1006
    invoke-static {v6}, Landroid/support/v4/view/accessibility/AccessibilityEventCompat;->asRecord(Landroid/view/accessibility/AccessibilityEvent;)Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;

    move-result-object v10

    .line 1007
    .local v10, "record":Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/music/ui/NowPlayingScreenFragment$10;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # getter for: Lcom/google/android/music/ui/NowPlayingScreenFragment;->mProgress:Lcom/google/android/music/SizableTrackSeekBar;
    invoke-static {v11}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$1200(Lcom/google/android/music/ui/NowPlayingScreenFragment;)Lcom/google/android/music/SizableTrackSeekBar;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;->setSource(Landroid/view/View;)V

    .line 1008
    invoke-virtual {v6}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v11

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f120009

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-virtual {v13, v14, v8, v15}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f12000a

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-virtual {v13, v14, v9, v15}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-interface {v11, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1013
    invoke-virtual {v2, v6}, Landroid/view/accessibility/AccessibilityManager;->sendAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    goto/16 :goto_1

    .line 999
    .end local v6    # "event":Landroid/view/accessibility/AccessibilityEvent;
    .end local v10    # "record":Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;
    :cond_4
    const/16 v11, 0x8

    goto :goto_2
.end method
