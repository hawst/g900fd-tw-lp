.class public Lcom/google/android/music/sync/google/model/TrackStat;
.super Lcom/google/api/client/json/GenericJson;
.source "TrackStat.java"

# interfaces
.implements Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/sync/google/model/TrackStat$TrackEvent;
    }
.end annotation


# static fields
.field public static final LOCKER_TYPE:I = 0x1

.field public static final NAUTILUS_TYPE:I = 0x2

.field public static final UNKNOWN_TYPE:I


# instance fields
.field public mIncrementalPlays:I
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "incremental_plays"
    .end annotation
.end field

.field public mLastPlayTimeMillis:J
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "last_play_time_millis"
    .end annotation
.end field

.field private mLocalId:J

.field public mRemoteId:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "id"
    .end annotation
.end field

.field public mTrackEvents:Ljava/util/List;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "track_events"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/sync/google/model/TrackStat$TrackEvent;",
            ">;"
        }
    .end annotation
.end field

.field public mType:I
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "type"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v0, 0x0

    .line 31
    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    .line 41
    iput v0, p0, Lcom/google/android/music/sync/google/model/TrackStat;->mIncrementalPlays:I

    .line 44
    iput-wide v2, p0, Lcom/google/android/music/sync/google/model/TrackStat;->mLastPlayTimeMillis:J

    .line 47
    iput v0, p0, Lcom/google/android/music/sync/google/model/TrackStat;->mType:I

    .line 50
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/sync/google/model/TrackStat;->mTrackEvents:Ljava/util/List;

    .line 53
    iput-wide v2, p0, Lcom/google/android/music/sync/google/model/TrackStat;->mLocalId:J

    .line 56
    return-void
.end method

.method private static convertSourceTypeToTrackStatType(I)I
    .locals 3
    .param p0, "sourceType"    # I

    .prologue
    const/4 v0, 0x2

    .line 229
    if-ne p0, v0, :cond_1

    .line 230
    const/4 v0, 0x1

    .line 232
    :cond_0
    return v0

    .line 231
    :cond_1
    const/4 v1, 0x3

    if-eq p0, v1, :cond_0

    .line 234
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid sourceType: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static parse(Landroid/database/Cursor;)Lcom/google/android/music/sync/google/model/TrackStat;
    .locals 4
    .param p0, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 208
    new-instance v1, Lcom/google/android/music/sync/google/model/TrackStat;

    invoke-direct {v1}, Lcom/google/android/music/sync/google/model/TrackStat;-><init>()V

    .line 209
    .local v1, "stat":Lcom/google/android/music/sync/google/model/TrackStat;
    const/4 v2, 0x0

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, v1, Lcom/google/android/music/sync/google/model/TrackStat;->mLocalId:J

    .line 210
    const/4 v2, 0x2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/music/sync/google/model/TrackStat;->mRemoteId:Ljava/lang/String;

    .line 211
    const/4 v2, 0x3

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v1, Lcom/google/android/music/sync/google/model/TrackStat;->mIncrementalPlays:I

    .line 213
    const/4 v2, 0x4

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, v1, Lcom/google/android/music/sync/google/model/TrackStat;->mLastPlayTimeMillis:J

    .line 215
    const/4 v2, 0x5

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 216
    .local v0, "sourceType":I
    invoke-static {v0}, Lcom/google/android/music/sync/google/model/TrackStat;->convertSourceTypeToTrackStatType(I)I

    move-result v2

    iput v2, v1, Lcom/google/android/music/sync/google/model/TrackStat;->mType:I

    .line 217
    return-object v1
.end method

.method public static parse(Lcom/google/android/music/store/MusicFile;)Lcom/google/android/music/sync/google/model/TrackStat;
    .locals 4
    .param p0, "musicFile"    # Lcom/google/android/music/store/MusicFile;

    .prologue
    .line 193
    new-instance v0, Lcom/google/android/music/sync/google/model/TrackStat;

    invoke-direct {v0}, Lcom/google/android/music/sync/google/model/TrackStat;-><init>()V

    .line 194
    .local v0, "stat":Lcom/google/android/music/sync/google/model/TrackStat;
    invoke-virtual {p0}, Lcom/google/android/music/store/MusicFile;->getSourceId()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/music/sync/google/model/TrackStat;->mRemoteId:Ljava/lang/String;

    .line 195
    invoke-virtual {p0}, Lcom/google/android/music/store/MusicFile;->getPlayCount()I

    move-result v1

    iput v1, v0, Lcom/google/android/music/sync/google/model/TrackStat;->mIncrementalPlays:I

    .line 196
    invoke-virtual {p0}, Lcom/google/android/music/store/MusicFile;->getLastPlayDate()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/google/android/music/sync/google/model/TrackStat;->mLastPlayTimeMillis:J

    .line 197
    invoke-virtual {p0}, Lcom/google/android/music/store/MusicFile;->getLocalId()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/google/android/music/sync/google/model/TrackStat;->mLocalId:J

    .line 198
    invoke-virtual {p0}, Lcom/google/android/music/store/MusicFile;->getSourceType()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/music/sync/google/model/TrackStat;->convertSourceTypeToTrackStatType(I)I

    move-result v1

    iput v1, v0, Lcom/google/android/music/sync/google/model/TrackStat;->mType:I

    .line 200
    return-object v0
.end method


# virtual methods
.method public getBatchMutationUrl(Landroid/content/Context;)Lcom/google/android/music/sync/api/MusicUrl;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 170
    invoke-static {}, Lcom/google/android/music/sync/api/MusicUrl;->forTrackStatsBatchReport()Lcom/google/android/music/sync/api/MusicUrl;

    move-result-object v0

    return-object v0
.end method

.method public getCreationTimestamp()J
    .locals 2

    .prologue
    .line 118
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "mCreationTimestamp is not defined for this type."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getFeedUrl(Landroid/content/Context;)Lcom/google/android/music/sync/api/MusicUrl;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 160
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Only BatchMutationUrl is supported on this type."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getFeedUrlAsPost(Landroid/content/Context;)Lcom/google/android/music/sync/api/MusicUrl;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 165
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Only BatchMutationUrl is supported on this type."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getIncrementalPlays()I
    .locals 1

    .prologue
    .line 84
    iget v0, p0, Lcom/google/android/music/sync/google/model/TrackStat;->mIncrementalPlays:I

    return v0
.end method

.method public getLastModifiedTimestamp()J
    .locals 2

    .prologue
    .line 108
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "mLastModifiedTimestamp is not defined for this type."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getLocalId()J
    .locals 2

    .prologue
    .line 96
    iget-wide v0, p0, Lcom/google/android/music/sync/google/model/TrackStat;->mLocalId:J

    return-wide v0
.end method

.method public getRemoteId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/android/music/sync/google/model/TrackStat;->mRemoteId:Ljava/lang/String;

    return-object v0
.end method

.method public getUrl(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/music/sync/api/MusicUrl;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "id"    # Ljava/lang/String;

    .prologue
    .line 155
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Only BatchMutationUrl is supported on this type."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public isDeleted()Z
    .locals 2

    .prologue
    .line 128
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "mIsDeleted is not defined for this type."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public isInsert()Z
    .locals 1

    .prologue
    .line 221
    iget-object v0, p0, Lcom/google/android/music/sync/google/model/TrackStat;->mRemoteId:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isUpdate()Z
    .locals 1

    .prologue
    .line 225
    iget-object v0, p0, Lcom/google/android/music/sync/google/model/TrackStat;->mRemoteId:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public serializeAsJson()[B
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/store/InvalidDataException;
        }
    .end annotation

    .prologue
    .line 138
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 140
    .local v0, "byteStream":Ljava/io/ByteArrayOutputStream;
    :try_start_0
    sget-object v3, Lcom/google/api/client/json/Json;->JSON_FACTORY:Lorg/codehaus/jackson/JsonFactory;

    sget-object v4, Lorg/codehaus/jackson/JsonEncoding;->UTF8:Lorg/codehaus/jackson/JsonEncoding;

    invoke-virtual {v3, v0, v4}, Lorg/codehaus/jackson/JsonFactory;->createJsonGenerator(Ljava/io/OutputStream;Lorg/codehaus/jackson/JsonEncoding;)Lorg/codehaus/jackson/JsonGenerator;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 143
    .local v2, "generator":Lorg/codehaus/jackson/JsonGenerator;
    :try_start_1
    invoke-static {v2, p0}, Lcom/google/api/client/json/Json;->serialize(Lorg/codehaus/jackson/JsonGenerator;Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 145
    :try_start_2
    invoke-virtual {v2}, Lorg/codehaus/jackson/JsonGenerator;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 151
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    return-object v3

    .line 145
    :catchall_0
    move-exception v3

    :try_start_3
    invoke-virtual {v2}, Lorg/codehaus/jackson/JsonGenerator;->close()V

    throw v3
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    .line 147
    .end local v2    # "generator":Lorg/codehaus/jackson/JsonGenerator;
    :catch_0
    move-exception v1

    .line 148
    .local v1, "e":Ljava/io/IOException;
    const-string v3, "MusicSyncAdapter"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unable to serialize a TrackStat as JSON: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/music/sync/google/model/TrackStat;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ": "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 149
    new-instance v3, Lcom/google/android/music/store/InvalidDataException;

    const-string v4, "Unable to serialize track stat for upstream sync."

    invoke-direct {v3, v4, v1}, Lcom/google/android/music/store/InvalidDataException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3
.end method

.method public setCreationTimestamp(J)V
    .locals 2
    .param p1, "time"    # J

    .prologue
    .line 123
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "mCreationTimestamp is not defined for this type."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setIsDeleted(Z)V
    .locals 2
    .param p1, "isDeleted"    # Z

    .prologue
    .line 133
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "mIsDeleted is not defined for this type."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setLastModifiedTimestamp(J)V
    .locals 2
    .param p1, "time"    # J

    .prologue
    .line 113
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "mLastModifiedTimestamp is not defined for this type."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setRemoteId(Ljava/lang/String;)V
    .locals 0
    .param p1, "remoteId"    # Ljava/lang/String;

    .prologue
    .line 104
    iput-object p1, p0, Lcom/google/android/music/sync/google/model/TrackStat;->mRemoteId:Ljava/lang/String;

    .line 105
    return-void
.end method

.method public validateForUpstreamDelete()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/store/InvalidDataException;
        }
    .end annotation

    .prologue
    .line 188
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Upstream deletion is not defined for this type."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public validateForUpstreamInsert()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/store/InvalidDataException;
        }
    .end annotation

    .prologue
    .line 174
    iget-object v0, p0, Lcom/google/android/music/sync/google/model/TrackStat;->mRemoteId:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 175
    new-instance v0, Lcom/google/android/music/store/InvalidDataException;

    const-string v1, "mRemoteId should not be null or empty."

    invoke-direct {v0, v1}, Lcom/google/android/music/store/InvalidDataException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 177
    :cond_0
    iget v0, p0, Lcom/google/android/music/sync/google/model/TrackStat;->mIncrementalPlays:I

    if-nez v0, :cond_1

    .line 178
    new-instance v0, Lcom/google/android/music/store/InvalidDataException;

    const-string v1, "mIncrementalPlays should be a positive int."

    invoke-direct {v0, v1}, Lcom/google/android/music/store/InvalidDataException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 180
    :cond_1
    return-void
.end method

.method public validateForUpstreamUpdate()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/store/InvalidDataException;
        }
    .end annotation

    .prologue
    .line 183
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Upstream update is not defined for this type."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public wipeAllFields()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 88
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/sync/google/model/TrackStat;->mRemoteId:Ljava/lang/String;

    .line 89
    iput v1, p0, Lcom/google/android/music/sync/google/model/TrackStat;->mIncrementalPlays:I

    .line 90
    iput-wide v2, p0, Lcom/google/android/music/sync/google/model/TrackStat;->mLastPlayTimeMillis:J

    .line 91
    iput-wide v2, p0, Lcom/google/android/music/sync/google/model/TrackStat;->mLocalId:J

    .line 92
    iput v1, p0, Lcom/google/android/music/sync/google/model/TrackStat;->mType:I

    .line 93
    return-void
.end method
