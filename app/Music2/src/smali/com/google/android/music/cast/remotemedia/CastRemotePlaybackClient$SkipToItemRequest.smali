.class Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$SkipToItemRequest;
.super Ljava/lang/Object;
.source "CastRemotePlaybackClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SkipToItemRequest"
.end annotation


# instance fields
.field private final mItemId:Ljava/lang/String;

.field private final mMusicFile:Lcom/google/android/music/store/MusicFile;

.field private final mPositionMillis:J

.field private final mQueueVersion:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;


# direct methods
.method public constructor <init>(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;Ljava/lang/String;Lcom/google/android/music/store/MusicFile;Ljava/lang/String;J)V
    .locals 1
    .param p2, "queueVersion"    # Ljava/lang/String;
    .param p3, "musicFile"    # Lcom/google/android/music/store/MusicFile;
    .param p4, "itemId"    # Ljava/lang/String;
    .param p5, "positionMillis"    # J

    .prologue
    .line 1173
    iput-object p1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$SkipToItemRequest;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1174
    iput-object p2, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$SkipToItemRequest;->mQueueVersion:Ljava/lang/String;

    .line 1175
    iput-object p3, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$SkipToItemRequest;->mMusicFile:Lcom/google/android/music/store/MusicFile;

    .line 1176
    iput-object p4, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$SkipToItemRequest;->mItemId:Ljava/lang/String;

    .line 1177
    iput-wide p5, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$SkipToItemRequest;->mPositionMillis:J

    .line 1178
    return-void
.end method


# virtual methods
.method public getItemId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1189
    iget-object v0, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$SkipToItemRequest;->mItemId:Ljava/lang/String;

    return-object v0
.end method

.method public getMusicFile()Lcom/google/android/music/store/MusicFile;
    .locals 1

    .prologue
    .line 1185
    iget-object v0, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$SkipToItemRequest;->mMusicFile:Lcom/google/android/music/store/MusicFile;

    return-object v0
.end method

.method public getPositionMillis()J
    .locals 2

    .prologue
    .line 1193
    iget-wide v0, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$SkipToItemRequest;->mPositionMillis:J

    return-wide v0
.end method

.method public getQueueVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1181
    iget-object v0, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$SkipToItemRequest;->mQueueVersion:Ljava/lang/String;

    return-object v0
.end method
