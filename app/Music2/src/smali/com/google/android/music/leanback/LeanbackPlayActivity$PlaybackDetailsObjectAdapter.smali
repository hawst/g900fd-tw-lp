.class Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;
.super Landroid/support/v17/leanback/widget/ObjectAdapter;
.source "LeanbackPlayActivity.java"

# interfaces
.implements Lcom/google/android/music/leanback/NowPlayingItem$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/leanback/LeanbackPlayActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PlaybackDetailsObjectAdapter"
.end annotation


# instance fields
.field private mSize:I

.field private final mSongFooterRow:Lcom/google/android/music/leanback/SongFooterRow;

.field private mSongListObjectAdapter:Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;

.field private mSongListObjectAdapterStartIndex:I

.field private final mSongTitleRow:Lcom/google/android/music/leanback/SongTitleRow;

.field final synthetic this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;


# direct methods
.method constructor <init>(Lcom/google/android/music/leanback/LeanbackPlayActivity;Landroid/support/v17/leanback/widget/PresenterSelector;Lcom/google/android/music/leanback/SongTitleRow;Lcom/google/android/music/leanback/SongFooterRow;)V
    .locals 0
    .param p2, "presenterSelector"    # Landroid/support/v17/leanback/widget/PresenterSelector;
    .param p3, "songTitleRow"    # Lcom/google/android/music/leanback/SongTitleRow;
    .param p4, "songFooterRow"    # Lcom/google/android/music/leanback/SongFooterRow;

    .prologue
    .line 1002
    iput-object p1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

    .line 1003
    invoke-direct {p0, p2}, Landroid/support/v17/leanback/widget/ObjectAdapter;-><init>(Landroid/support/v17/leanback/widget/PresenterSelector;)V

    .line 1004
    iput-object p3, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->mSongTitleRow:Lcom/google/android/music/leanback/SongTitleRow;

    .line 1005
    iput-object p4, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->mSongFooterRow:Lcom/google/android/music/leanback/SongFooterRow;

    .line 1006
    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->recalculateSize()V

    .line 1007
    return-void
.end method

.method static synthetic access$2400(Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;

    .prologue
    .line 993
    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->recalculateSize()V

    return-void
.end method

.method static synthetic access$2500(Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;

    .prologue
    .line 993
    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->notifyChanged()V

    return-void
.end method

.method static synthetic access$2600(Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;

    .prologue
    .line 993
    iget v0, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->mSongListObjectAdapterStartIndex:I

    return v0
.end method

.method static synthetic access$2700(Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;II)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 993
    invoke-virtual {p0, p1, p2}, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->notifyItemRangeChanged(II)V

    return-void
.end method

.method static synthetic access$2800(Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;

    .prologue
    .line 993
    iget v0, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->mSize:I

    return v0
.end method

.method static synthetic access$2900(Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;II)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 993
    invoke-virtual {p0, p1, p2}, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->notifyItemRangeChanged(II)V

    return-void
.end method

.method static synthetic access$3000(Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;II)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 993
    invoke-virtual {p0, p1, p2}, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->notifyItemRangeInserted(II)V

    return-void
.end method

.method static synthetic access$3100(Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;II)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 993
    invoke-virtual {p0, p1, p2}, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->notifyItemRangeRemoved(II)V

    return-void
.end method

.method static synthetic access$3400(Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;II)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 993
    invoke-virtual {p0, p1, p2}, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->notifyItemRangeChanged(II)V

    return-void
.end method

.method private recalculateSize()V
    .locals 2

    .prologue
    .line 1123
    const/4 v0, 0x1

    .line 1124
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->mSongListObjectAdapter:Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->mSongListObjectAdapter:Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;

    invoke-virtual {v1}, Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 1125
    add-int/lit8 v0, v0, 0x1

    .line 1126
    iput v0, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->mSongListObjectAdapterStartIndex:I

    .line 1127
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->mSongListObjectAdapter:Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;

    invoke-virtual {v1}, Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;->size()I

    move-result v1

    add-int/lit8 v0, v1, 0x2

    .line 1128
    add-int/lit8 v0, v0, 0x1

    .line 1132
    :goto_0
    iput v0, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->mSize:I

    .line 1133
    return-void

    .line 1130
    :cond_0
    const/4 v1, -0x1

    iput v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->mSongListObjectAdapterStartIndex:I

    goto :goto_0
.end method


# virtual methods
.method public get(I)Ljava/lang/Object;
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 1101
    iget v0, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->mSize:I

    if-ge p1, v0, :cond_3

    .line 1102
    iget v0, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->mSongListObjectAdapterStartIndex:I

    if-ltz v0, :cond_2

    .line 1103
    iget v0, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->mSize:I

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_0

    .line 1104
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->mSongFooterRow:Lcom/google/android/music/leanback/SongFooterRow;

    .line 1115
    :goto_0
    return-object v0

    .line 1106
    :cond_0
    iget v0, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->mSongListObjectAdapterStartIndex:I

    if-lt p1, v0, :cond_1

    .line 1107
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->mSongListObjectAdapter:Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;

    iget v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->mSongListObjectAdapterStartIndex:I

    sub-int v1, p1, v1

    invoke-virtual {v0, v1}, Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 1110
    :cond_1
    iget v0, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->mSongListObjectAdapterStartIndex:I

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_2

    .line 1111
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->mSongTitleRow:Lcom/google/android/music/leanback/SongTitleRow;

    goto :goto_0

    .line 1114
    :cond_2
    if-nez p1, :cond_3

    .line 1115
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

    # getter for: Lcom/google/android/music/leanback/LeanbackPlayActivity;->mPlaybackControlsRow:Landroid/support/v17/leanback/widget/PlaybackControlsRow;
    invoke-static {v0}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->access$3300(Lcom/google/android/music/leanback/LeanbackPlayActivity;)Landroid/support/v17/leanback/widget/PlaybackControlsRow;

    move-result-object v0

    goto :goto_0

    .line 1118
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Tried to get unexpected position "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". Size is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->mSize:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onNowPlayingChanged()V
    .locals 6

    .prologue
    .line 1074
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

    # getter for: Lcom/google/android/music/leanback/LeanbackPlayActivity;->mNowPlayingItem:Lcom/google/android/music/leanback/NowPlayingItem;
    invoke-static {v1}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->access$3200(Lcom/google/android/music/leanback/LeanbackPlayActivity;)Lcom/google/android/music/leanback/NowPlayingItem;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/leanback/NowPlayingItem;->getBitmapGettersGetter()Lcom/google/android/music/leanback/bitmap/BitmapGettersGetter;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

    invoke-static {v2}, Lcom/google/android/music/leanback/bitmap/BitmapFactory;->getCardImageHeightPixels(Landroid/content/Context;)I

    move-result v2

    iget-object v3, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

    invoke-static {v3}, Lcom/google/android/music/leanback/bitmap/BitmapFactory;->getCardImageHeightPixels(Landroid/content/Context;)I

    move-result v3

    const/4 v4, 0x1

    new-instance v5, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter$2;

    invoke-direct {v5, p0}, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter$2;-><init>(Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;)V

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/leanback/bitmap/BitmapFactory;->getBitmap(Landroid/content/Context;Lcom/google/android/music/leanback/bitmap/BitmapGettersGetter;IIZLcom/google/android/music/leanback/bitmap/BitmapListener;)V

    .line 1092
    return-void
.end method

.method onQueuePositionChanged(I)V
    .locals 3
    .param p1, "queuePosition"    # I

    .prologue
    .line 1010
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->mSongListObjectAdapter:Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;

    if-nez v0, :cond_0

    .line 1016
    :goto_0
    return-void

    .line 1013
    :cond_0
    # getter for: Lcom/google/android/music/leanback/LeanbackPlayActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->access$200()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Notifying row "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->mSongListObjectAdapterStartIndex:I

    add-int/2addr v2, p1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " changed"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1015
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->mSongListObjectAdapter:Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;

    invoke-virtual {v0, p1}, Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;->notifyPositionChanged(I)V

    goto :goto_0
.end method

.method setSongListObjectAdapter(Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;)V
    .locals 2
    .param p1, "adapter"    # Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;

    .prologue
    .line 1019
    iput-object p1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->mSongListObjectAdapter:Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;

    .line 1020
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->mSongListObjectAdapter:Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;

    if-nez v0, :cond_0

    .line 1021
    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->recalculateSize()V

    .line 1022
    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->notifyChanged()V

    .line 1070
    :goto_0
    return-void

    .line 1025
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->mSongListObjectAdapter:Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;

    new-instance v1, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter$1;

    invoke-direct {v1, p0}, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter$1;-><init>(Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;)V

    invoke-virtual {v0, v1}, Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;->registerObserver(Landroid/support/v17/leanback/widget/ObjectAdapter$DataObserver;)V

    .line 1069
    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->recalculateSize()V

    goto :goto_0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 1096
    iget v0, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$PlaybackDetailsObjectAdapter;->mSize:I

    return v0
.end method
