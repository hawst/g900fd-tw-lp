.class public Lcom/google/android/music/widgets/LinkableViewPager;
.super Landroid/support/v4/view/ViewPager;
.source "LinkableViewPager.java"


# instance fields
.field private mForwardingToChild:Z

.field private mIsFakeDragging:Z

.field private mOriginalDownX:F

.field private mOriginalDownY:F

.field private final mTouchSlop:F

.field private mViewPagers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/widgets/LinkableViewPager;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v2, 0x0

    .line 37
    invoke-direct {p0, p1, p2}, Landroid/support/v4/view/ViewPager;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 22
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    iput-object v1, p0, Lcom/google/android/music/widgets/LinkableViewPager;->mViewPagers:Ljava/util/List;

    .line 24
    iput-boolean v2, p0, Lcom/google/android/music/widgets/LinkableViewPager;->mForwardingToChild:Z

    .line 34
    iput-boolean v2, p0, Lcom/google/android/music/widgets/LinkableViewPager;->mIsFakeDragging:Z

    .line 38
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 39
    .local v0, "configuration":Landroid/view/ViewConfiguration;
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Lcom/google/android/music/widgets/LinkableViewPager;->mTouchSlop:F

    .line 40
    return-void
.end method

.method private superOnTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v1, 0x1

    .line 125
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_1

    .line 126
    iput-boolean v1, p0, Lcom/google/android/music/widgets/LinkableViewPager;->mIsFakeDragging:Z

    .line 131
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/support/v4/view/ViewPager;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0

    .line 127
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-eq v0, v1, :cond_2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 129
    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/widgets/LinkableViewPager;->mIsFakeDragging:Z

    goto :goto_0
.end method

.method private superSetCurrentItem(I)V
    .locals 0
    .param p1, "i"    # I

    .prologue
    .line 138
    invoke-super {p0, p1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 139
    return-void
.end method

.method private superSetCurrentItem(IZ)V
    .locals 0
    .param p1, "i"    # I
    .param p2, "smoothScroll"    # Z

    .prologue
    .line 145
    invoke-super {p0, p1, p2}, Landroid/support/v4/view/ViewPager;->setCurrentItem(IZ)V

    .line 146
    return-void
.end method


# virtual methods
.method public link(Lcom/google/android/music/widgets/LinkableViewPager;)V
    .locals 1
    .param p1, "pager"    # Lcom/google/android/music/widgets/LinkableViewPager;

    .prologue
    .line 47
    iget-object v0, p1, Lcom/google/android/music/widgets/LinkableViewPager;->mViewPagers:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 48
    iget-object v0, p0, Lcom/google/android/music/widgets/LinkableViewPager;->mViewPagers:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 49
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 72
    const/4 v0, 0x1

    return v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v5, 0x1

    const/4 v7, 0x0

    .line 77
    iget-boolean v6, p0, Lcom/google/android/music/widgets/LinkableViewPager;->mIsFakeDragging:Z

    if-eqz v6, :cond_0

    .line 117
    :goto_0
    return v5

    .line 85
    :cond_0
    iget-object v6, p0, Lcom/google/android/music/widgets/LinkableViewPager;->mViewPagers:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/music/widgets/LinkableViewPager;

    .line 86
    .local v3, "pager":Lcom/google/android/music/widgets/LinkableViewPager;
    invoke-virtual {v3}, Lcom/google/android/music/widgets/LinkableViewPager;->getVisibility()I

    move-result v6

    if-nez v6, :cond_1

    .line 88
    invoke-direct {v3, p1}, Lcom/google/android/music/widgets/LinkableViewPager;->superOnTouchEvent(Landroid/view/MotionEvent;)Z

    goto :goto_1

    .line 93
    .end local v3    # "pager":Lcom/google/android/music/widgets/LinkableViewPager;
    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v6

    if-nez v6, :cond_5

    .line 94
    iput-boolean v5, p0, Lcom/google/android/music/widgets/LinkableViewPager;->mForwardingToChild:Z

    .line 95
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    iput v5, p0, Lcom/google/android/music/widgets/LinkableViewPager;->mOriginalDownX:F

    .line 96
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    iput v5, p0, Lcom/google/android/music/widgets/LinkableViewPager;->mOriginalDownY:F

    .line 106
    :cond_3
    :goto_2
    iget-boolean v5, p0, Lcom/google/android/music/widgets/LinkableViewPager;->mForwardingToChild:Z

    if-eqz v5, :cond_4

    .line 107
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    invoke-virtual {p0}, Lcom/google/android/music/widgets/LinkableViewPager;->getScrollX()I

    move-result v6

    int-to-float v6, v6

    add-float v4, v5, v6

    .line 108
    .local v4, "x":F
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_3
    invoke-virtual {p0}, Lcom/google/android/music/widgets/LinkableViewPager;->getChildCount()I

    move-result v5

    if-ge v1, v5, :cond_4

    .line 109
    invoke-virtual {p0, v1}, Lcom/google/android/music/widgets/LinkableViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 111
    .local v0, "child":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v5

    int-to-float v5, v5

    cmpg-float v5, v5, v4

    if-gez v5, :cond_8

    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v5

    int-to-float v5, v5

    cmpl-float v5, v5, v4

    if-lez v5, :cond_8

    .line 112
    invoke-virtual {v0, p1}, Landroid/view/View;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 117
    .end local v0    # "child":Landroid/view/View;
    .end local v1    # "i":I
    .end local v4    # "x":F
    :cond_4
    invoke-super {p0, p1}, Landroid/support/v4/view/ViewPager;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v5

    goto :goto_0

    .line 97
    :cond_5
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_7

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    iget v6, p0, Lcom/google/android/music/widgets/LinkableViewPager;->mOriginalDownY:F

    sub-float/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    iget v6, p0, Lcom/google/android/music/widgets/LinkableViewPager;->mTouchSlop:F

    cmpl-float v5, v5, v6

    if-gtz v5, :cond_6

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    iget v6, p0, Lcom/google/android/music/widgets/LinkableViewPager;->mOriginalDownX:F

    sub-float/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    iget v6, p0, Lcom/google/android/music/widgets/LinkableViewPager;->mTouchSlop:F

    cmpl-float v5, v5, v6

    if-lez v5, :cond_7

    .line 101
    :cond_6
    iput-boolean v7, p0, Lcom/google/android/music/widgets/LinkableViewPager;->mForwardingToChild:Z

    goto :goto_2

    .line 102
    :cond_7
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v5

    const/4 v6, 0x3

    if-ne v5, v6, :cond_3

    .line 103
    iput-boolean v7, p0, Lcom/google/android/music/widgets/LinkableViewPager;->mForwardingToChild:Z

    goto :goto_2

    .line 108
    .restart local v0    # "child":Landroid/view/View;
    .restart local v1    # "i":I
    .restart local v4    # "x":F
    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_3
.end method

.method public setCurrentItem(I)V
    .locals 3
    .param p1, "i"    # I

    .prologue
    .line 54
    iget-object v2, p0, Lcom/google/android/music/widgets/LinkableViewPager;->mViewPagers:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/widgets/LinkableViewPager;

    .line 55
    .local v1, "pager":Lcom/google/android/music/widgets/LinkableViewPager;
    invoke-direct {v1, p1}, Lcom/google/android/music/widgets/LinkableViewPager;->superSetCurrentItem(I)V

    goto :goto_0

    .line 57
    .end local v1    # "pager":Lcom/google/android/music/widgets/LinkableViewPager;
    :cond_0
    invoke-super {p0, p1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 58
    return-void
.end method

.method public setCurrentItem(IZ)V
    .locals 3
    .param p1, "i"    # I
    .param p2, "smoothScroll"    # Z

    .prologue
    .line 63
    iget-object v2, p0, Lcom/google/android/music/widgets/LinkableViewPager;->mViewPagers:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/widgets/LinkableViewPager;

    .line 64
    .local v1, "pager":Lcom/google/android/music/widgets/LinkableViewPager;
    invoke-direct {v1, p1, p2}, Lcom/google/android/music/widgets/LinkableViewPager;->superSetCurrentItem(IZ)V

    goto :goto_0

    .line 66
    .end local v1    # "pager":Lcom/google/android/music/widgets/LinkableViewPager;
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/support/v4/view/ViewPager;->setCurrentItem(IZ)V

    .line 67
    return-void
.end method
