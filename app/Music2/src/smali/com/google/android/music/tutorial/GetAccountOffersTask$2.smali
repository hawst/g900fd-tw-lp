.class Lcom/google/android/music/tutorial/GetAccountOffersTask$2;
.super Ljava/lang/Object;
.source "GetAccountOffersTask.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/tutorial/GetAccountOffersTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/tutorial/GetAccountOffersTask;


# direct methods
.method constructor <init>(Lcom/google/android/music/tutorial/GetAccountOffersTask;)V
    .locals 0

    .prologue
    .line 122
    iput-object p1, p0, Lcom/google/android/music/tutorial/GetAccountOffersTask$2;->this$0:Lcom/google/android/music/tutorial/GetAccountOffersTask;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 125
    iget-object v0, p0, Lcom/google/android/music/tutorial/GetAccountOffersTask$2;->this$0:Lcom/google/android/music/tutorial/GetAccountOffersTask;

    # getter for: Lcom/google/android/music/tutorial/GetAccountOffersTask;->mIsCanceled:Z
    invoke-static {v0}, Lcom/google/android/music/tutorial/GetAccountOffersTask;->access$000(Lcom/google/android/music/tutorial/GetAccountOffersTask;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 140
    :goto_0
    return-void

    .line 128
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/tutorial/GetAccountOffersTask$2;->this$0:Lcom/google/android/music/tutorial/GetAccountOffersTask;

    # getter for: Lcom/google/android/music/tutorial/GetAccountOffersTask;->mResponse:Lcom/google/android/music/cloudclient/OffersResponseJson;
    invoke-static {v0}, Lcom/google/android/music/tutorial/GetAccountOffersTask;->access$400(Lcom/google/android/music/tutorial/GetAccountOffersTask;)Lcom/google/android/music/cloudclient/OffersResponseJson;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 129
    iget-object v0, p0, Lcom/google/android/music/tutorial/GetAccountOffersTask$2;->this$0:Lcom/google/android/music/tutorial/GetAccountOffersTask;

    # getter for: Lcom/google/android/music/tutorial/GetAccountOffersTask;->mResponse:Lcom/google/android/music/cloudclient/OffersResponseJson;
    invoke-static {v0}, Lcom/google/android/music/tutorial/GetAccountOffersTask;->access$400(Lcom/google/android/music/tutorial/GetAccountOffersTask;)Lcom/google/android/music/cloudclient/OffersResponseJson;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/cloudclient/OffersResponseJson;->isValidMusicAccount()Z

    move-result v0

    if-nez v0, :cond_1

    .line 130
    iget-object v0, p0, Lcom/google/android/music/tutorial/GetAccountOffersTask$2;->this$0:Lcom/google/android/music/tutorial/GetAccountOffersTask;

    # getter for: Lcom/google/android/music/tutorial/GetAccountOffersTask;->mCallbacks:Lcom/google/android/music/tutorial/GetAccountOffersTask$Callbacks;
    invoke-static {v0}, Lcom/google/android/music/tutorial/GetAccountOffersTask;->access$900(Lcom/google/android/music/tutorial/GetAccountOffersTask;)Lcom/google/android/music/tutorial/GetAccountOffersTask$Callbacks;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/tutorial/GetAccountOffersTask$2;->this$0:Lcom/google/android/music/tutorial/GetAccountOffersTask;

    # getter for: Lcom/google/android/music/tutorial/GetAccountOffersTask;->mAccount:Landroid/accounts/Account;
    invoke-static {v1}, Lcom/google/android/music/tutorial/GetAccountOffersTask;->access$600(Lcom/google/android/music/tutorial/GetAccountOffersTask;)Landroid/accounts/Account;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/music/tutorial/GetAccountOffersTask$Callbacks;->onAccountInvalid(Landroid/accounts/Account;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 138
    :goto_1
    iget-object v0, p0, Lcom/google/android/music/tutorial/GetAccountOffersTask$2;->this$0:Lcom/google/android/music/tutorial/GetAccountOffersTask;

    # invokes: Lcom/google/android/music/tutorial/GetAccountOffersTask;->cleanup()V
    invoke-static {v0}, Lcom/google/android/music/tutorial/GetAccountOffersTask;->access$1000(Lcom/google/android/music/tutorial/GetAccountOffersTask;)V

    goto :goto_0

    .line 132
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/music/tutorial/GetAccountOffersTask$2;->this$0:Lcom/google/android/music/tutorial/GetAccountOffersTask;

    # getter for: Lcom/google/android/music/tutorial/GetAccountOffersTask;->mCallbacks:Lcom/google/android/music/tutorial/GetAccountOffersTask$Callbacks;
    invoke-static {v0}, Lcom/google/android/music/tutorial/GetAccountOffersTask;->access$900(Lcom/google/android/music/tutorial/GetAccountOffersTask;)Lcom/google/android/music/tutorial/GetAccountOffersTask$Callbacks;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/tutorial/GetAccountOffersTask$2;->this$0:Lcom/google/android/music/tutorial/GetAccountOffersTask;

    # getter for: Lcom/google/android/music/tutorial/GetAccountOffersTask;->mAccount:Landroid/accounts/Account;
    invoke-static {v1}, Lcom/google/android/music/tutorial/GetAccountOffersTask;->access$600(Lcom/google/android/music/tutorial/GetAccountOffersTask;)Landroid/accounts/Account;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/tutorial/GetAccountOffersTask$2;->this$0:Lcom/google/android/music/tutorial/GetAccountOffersTask;

    # getter for: Lcom/google/android/music/tutorial/GetAccountOffersTask;->mResponse:Lcom/google/android/music/cloudclient/OffersResponseJson;
    invoke-static {v2}, Lcom/google/android/music/tutorial/GetAccountOffersTask;->access$400(Lcom/google/android/music/tutorial/GetAccountOffersTask;)Lcom/google/android/music/cloudclient/OffersResponseJson;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/music/tutorial/GetAccountOffersTask$Callbacks;->onAccountOffersSuccess(Landroid/accounts/Account;Lcom/google/android/music/cloudclient/OffersResponseJson;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 138
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/music/tutorial/GetAccountOffersTask$2;->this$0:Lcom/google/android/music/tutorial/GetAccountOffersTask;

    # invokes: Lcom/google/android/music/tutorial/GetAccountOffersTask;->cleanup()V
    invoke-static {v1}, Lcom/google/android/music/tutorial/GetAccountOffersTask;->access$1000(Lcom/google/android/music/tutorial/GetAccountOffersTask;)V

    throw v0

    .line 135
    :cond_2
    :try_start_2
    iget-object v0, p0, Lcom/google/android/music/tutorial/GetAccountOffersTask$2;->this$0:Lcom/google/android/music/tutorial/GetAccountOffersTask;

    # getter for: Lcom/google/android/music/tutorial/GetAccountOffersTask;->mCallbacks:Lcom/google/android/music/tutorial/GetAccountOffersTask$Callbacks;
    invoke-static {v0}, Lcom/google/android/music/tutorial/GetAccountOffersTask;->access$900(Lcom/google/android/music/tutorial/GetAccountOffersTask;)Lcom/google/android/music/tutorial/GetAccountOffersTask$Callbacks;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/tutorial/GetAccountOffersTask$2;->this$0:Lcom/google/android/music/tutorial/GetAccountOffersTask;

    # getter for: Lcom/google/android/music/tutorial/GetAccountOffersTask;->mAccount:Landroid/accounts/Account;
    invoke-static {v1}, Lcom/google/android/music/tutorial/GetAccountOffersTask;->access$600(Lcom/google/android/music/tutorial/GetAccountOffersTask;)Landroid/accounts/Account;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/music/tutorial/GetAccountOffersTask$Callbacks;->onAccountOffersError(Landroid/accounts/Account;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method
