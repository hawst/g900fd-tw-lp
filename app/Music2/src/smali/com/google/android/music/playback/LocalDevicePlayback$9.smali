.class Lcom/google/android/music/playback/LocalDevicePlayback$9;
.super Landroid/database/ContentObserver;
.source "LocalDevicePlayback.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/playback/LocalDevicePlayback;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/playback/LocalDevicePlayback;


# direct methods
.method constructor <init>(Lcom/google/android/music/playback/LocalDevicePlayback;Landroid/os/Handler;)V
    .locals 0
    .param p2, "x0"    # Landroid/os/Handler;

    .prologue
    .line 1485
    iput-object p1, p0, Lcom/google/android/music/playback/LocalDevicePlayback$9;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 6
    .param p1, "selfChange"    # Z

    .prologue
    const/4 v2, 0x1

    .line 1488
    iget-object v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback$9;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    iget-object v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback$9;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    invoke-virtual {v4}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v4

    # invokes: Lcom/google/android/music/playback/LocalDevicePlayback;->isInCloudQueueMode(Landroid/content/Context;)Z
    invoke-static {v3, v4}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$5100(Lcom/google/android/music/playback/LocalDevicePlayback;Landroid/content/Context;)Z

    move-result v0

    .line 1489
    .local v0, "isInCloudQueueMode":Z
    if-eqz v0, :cond_1

    .line 1497
    :cond_0
    :goto_0
    return-void

    .line 1492
    :cond_1
    iget-object v3, p0, Lcom/google/android/music/playback/LocalDevicePlayback$9;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    invoke-virtual {v3}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/music/playback/LocalDevicePlayback$9;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mPlayPos:I
    invoke-static {v4}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$2800(Lcom/google/android/music/playback/LocalDevicePlayback;)I

    move-result v4

    iget-object v5, p0, Lcom/google/android/music/playback/LocalDevicePlayback$9;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    # getter for: Lcom/google/android/music/playback/LocalDevicePlayback;->mShuffleMode:I
    invoke-static {v5}, Lcom/google/android/music/playback/LocalDevicePlayback;->access$5200(Lcom/google/android/music/playback/LocalDevicePlayback;)I

    move-result v5

    if-ne v5, v2, :cond_2

    :goto_1
    invoke-virtual {v3, v4, v2, v0}, Lcom/google/android/music/store/Store;->caqRefreshQueue(IZZ)Z

    move-result v1

    .line 1494
    .local v1, "isQueueChanged":Z
    if-eqz v1, :cond_0

    .line 1495
    iget-object v2, p0, Lcom/google/android/music/playback/LocalDevicePlayback$9;->this$0:Lcom/google/android/music/playback/LocalDevicePlayback;

    invoke-virtual {v2}, Lcom/google/android/music/playback/LocalDevicePlayback;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/music/store/QueueUtils;->notifyChange(Landroid/content/Context;)V

    goto :goto_0

    .line 1492
    .end local v1    # "isQueueChanged":Z
    :cond_2
    const/4 v2, 0x0

    goto :goto_1
.end method
