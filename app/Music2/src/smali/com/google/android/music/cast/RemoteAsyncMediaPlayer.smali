.class public Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;
.super Ljava/lang/Object;
.source "RemoteAsyncMediaPlayer.java"

# interfaces
.implements Lcom/google/android/music/playback/AsyncMediaPlayer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$8;,
        Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$ItemStatusReceiver;,
        Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler;,
        Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$ErrorHandlingAction;,
        Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$InternalErrorCallback;,
        Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$InternalHttpErrorCallback;,
        Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$InternalStateCallback;,
        Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;
    }
.end annotation


# static fields
.field private static final ACTION_ENQUEUE_BASE_INTENT:Landroid/content/Intent;

.field private static final ACTION_START_SESSION_BASE_INTENT:Landroid/content/Intent;


# instance fields
.field private mAudioSessionId:I

.field private volatile mCallback:Lcom/google/android/music/playback/AsyncMediaPlayer$AsyncCommandCallback;

.field private final mCastTokenClient:Lcom/google/android/music/cast/CastTokenClient;

.field private mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

.field private final mContext:Landroid/content/Context;

.field private volatile mCurrentSongId:Lcom/google/android/music/download/ContentIdentifier;

.field private volatile mDownloadError:I

.field private final mEventLogger:Lcom/google/android/music/eventlog/MusicEventLogger;

.field private volatile mFetchedCastToken:Z

.field private mFromUserAction:Z

.field private final mHandler:Landroid/os/Handler;

.field private mHasSentPlayEvent:Z

.field private volatile mInternalErrorCallback:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$InternalErrorCallback;

.field private volatile mInternalHttpErrorCallback:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$InternalHttpErrorCallback;

.field private volatile mInternalStateCallback:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$InternalStateCallback;

.field private mIsCurrent:Z

.field private volatile mIsPlaybackRequested:Z

.field private volatile mItemId:Ljava/lang/String;

.field private final mItemStatusCategory:Ljava/lang/String;

.field private mItemStatusReceiver:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$ItemStatusReceiver;

.field private mKnownPositionMillis:J

.field private final mLock:Ljava/lang/Object;

.field private mMediaRouter:Landroid/support/v7/media/MediaRouter;

.field private volatile mMusicFile:Lcom/google/android/music/store/MusicFile;

.field private final mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

.field private mNextPlayer:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

.field private volatile mNumberOfRetries:I

.field private mPositionDeltaStopWatch:Lcom/google/android/music/playback/StopWatch;

.field private final mPrequeueItems:Z

.field private volatile mRetryingInvalidSessionId:Z

.field private mRoute:Landroid/support/v7/media/MediaRouter$RouteInfo;

.field private final mRouteChecker:Lcom/google/android/music/utils/RouteChecker;

.field private final mServiceHooks:Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;

.field private final mSessionManager:Lcom/google/android/music/cast/CastSessionManager;

.field private volatile mState:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

.field private volatile mTryRefreshCastToken:Z

.field private mUrl:Ljava/lang/String;

.field private mWakeLock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 84
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    sput-object v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->ACTION_ENQUEUE_BASE_INTENT:Landroid/content/Intent;

    .line 85
    sget-object v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->ACTION_ENQUEUE_BASE_INTENT:Landroid/content/Intent;

    const-string v1, "android.media.intent.category.REMOTE_PLAYBACK"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 86
    sget-object v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->ACTION_ENQUEUE_BASE_INTENT:Landroid/content/Intent;

    const-string v1, "https://mclients.googleapis.com/music/mplay"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const-string v2, "audio/mpeg"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 88
    sget-object v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->ACTION_ENQUEUE_BASE_INTENT:Landroid/content/Intent;

    const-string v1, "android.media.intent.action.ENQUEUE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 90
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    sput-object v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->ACTION_START_SESSION_BASE_INTENT:Landroid/content/Intent;

    .line 91
    sget-object v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->ACTION_START_SESSION_BASE_INTENT:Landroid/content/Intent;

    const-string v1, "android.media.intent.category.REMOTE_PLAYBACK"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 92
    sget-object v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->ACTION_START_SESSION_BASE_INTENT:Landroid/content/Intent;

    const-string v1, "android.media.intent.action.START_SESSION"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 93
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/music/cast/CastTokenClient;Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;ZLcom/google/android/music/cast/CastSessionManager;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "castTokenClient"    # Lcom/google/android/music/cast/CastTokenClient;
    .param p3, "serviceHooks"    # Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;
    .param p4, "prequeueItems"    # Z
    .param p5, "sessionManager"    # Lcom/google/android/music/cast/CastSessionManager;

    .prologue
    const/4 v5, 0x1

    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 502
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 465
    iput-object v1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mRoute:Landroid/support/v7/media/MediaRouter$RouteInfo;

    .line 466
    iput-object v1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mItemStatusReceiver:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$ItemStatusReceiver;

    .line 467
    iput-boolean v4, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mFetchedCastToken:Z

    .line 468
    iput-boolean v4, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mIsCurrent:Z

    .line 469
    iput-object v1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mNextPlayer:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    .line 470
    iput-object v1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mMusicFile:Lcom/google/android/music/store/MusicFile;

    .line 471
    iput-object v1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mCurrentSongId:Lcom/google/android/music/download/ContentIdentifier;

    .line 473
    iput-boolean v4, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mFromUserAction:Z

    .line 474
    iput-object v1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mUrl:Ljava/lang/String;

    .line 476
    iput-object v1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mItemId:Ljava/lang/String;

    .line 477
    const/4 v1, -0x1

    iput v1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mAudioSessionId:I

    .line 478
    new-instance v1, Lcom/google/android/music/playback/StopWatch;

    invoke-direct {v1}, Lcom/google/android/music/playback/StopWatch;-><init>()V

    iput-object v1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mPositionDeltaStopWatch:Lcom/google/android/music/playback/StopWatch;

    .line 479
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mKnownPositionMillis:J

    .line 480
    sget-object v1, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->NONE:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    iput-object v1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mState:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    .line 481
    iput v5, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mDownloadError:I

    .line 482
    iput-boolean v4, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mTryRefreshCastToken:Z

    .line 483
    iput-boolean v4, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mRetryingInvalidSessionId:Z

    .line 484
    iput v4, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mNumberOfRetries:I

    .line 487
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mLock:Ljava/lang/Object;

    .line 490
    iput-boolean v4, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mIsPlaybackRequested:Z

    .line 494
    iput-boolean v4, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mHasSentPlayEvent:Z

    .line 503
    iput-object p1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mContext:Landroid/content/Context;

    .line 504
    invoke-static {p1, p0}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    .line 505
    iput-object p2, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mCastTokenClient:Lcom/google/android/music/cast/CastTokenClient;

    .line 506
    new-instance v1, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler;

    invoke-direct {v1, p0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$MyHandler;-><init>(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;)V

    iput-object v1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mHandler:Landroid/os/Handler;

    .line 507
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mItemStatusCategory:Ljava/lang/String;

    .line 508
    iput-object p3, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mServiceHooks:Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;

    .line 509
    iput-boolean p4, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mPrequeueItems:Z

    .line 510
    new-instance v1, Lcom/google/android/music/utils/RouteChecker;

    invoke-direct {v1, p1}, Lcom/google/android/music/utils/RouteChecker;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mRouteChecker:Lcom/google/android/music/utils/RouteChecker;

    .line 511
    iput-object p5, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mSessionManager:Lcom/google/android/music/cast/CastSessionManager;

    .line 512
    invoke-static {p1}, Lcom/google/android/music/eventlog/MusicEventLogger;->getInstance(Landroid/content/Context;)Lcom/google/android/music/eventlog/MusicEventLogger;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mEventLogger:Lcom/google/android/music/eventlog/MusicEventLogger;

    .line 514
    invoke-direct {p0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->isLogVerbose()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 515
    const-string v1, "Created RemoteAsyncMediaPlayer."

    invoke-direct {p0, v1}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->logv(Ljava/lang/String;)V

    .line 518
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mContext:Landroid/content/Context;

    const-string v2, "power"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 519
    .local v0, "pm":Landroid/os/PowerManager;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".mWakeLock"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v5, v1}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 522
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->isLogVerbose()Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;)Landroid/support/v7/media/MediaRouter$RouteInfo;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mRoute:Landroid/support/v7/media/MediaRouter$RouteInfo;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->requestRemotePlaybackOnMainThread()V

    return-void
.end method

.method static synthetic access$1100(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->requestRemotePauseOnMainThread()V

    return-void
.end method

.method static synthetic access$1200(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->requestRemoteResumeOnMainThread()V

    return-void
.end method

.method static synthetic access$1300(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->requestRemoteSeekOnMainThread()V

    return-void
.end method

.method static synthetic access$1400(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;F)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;
    .param p1, "x1"    # F

    .prologue
    .line 59
    invoke-direct {p0, p1}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->requestRemoteSetVolumeOnMainThread(F)V

    return-void
.end method

.method static synthetic access$1500(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;)Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mNextPlayer:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->enqueue()V

    return-void
.end method

.method static synthetic access$1700(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->requestRemoveOnMainThread()V

    return-void
.end method

.method static synthetic access$1800(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;)Lcom/google/android/music/cast/CastSessionManager;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mSessionManager:Lcom/google/android/music/cast/CastSessionManager;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->requestCastV2RemoteSyncStatusOnMainThread()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;)Lcom/google/android/music/cast/CastTokenClient;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mCastTokenClient:Lcom/google/android/music/cast/CastTokenClient;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 59
    invoke-direct {p0, p1}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->logv(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$2100(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;Landroid/support/v7/media/MediaItemStatus;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;
    .param p1, "x1"    # Landroid/support/v7/media/MediaItemStatus;

    .prologue
    .line 59
    invoke-direct {p0, p1}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->processMediaItemStatus(Landroid/support/v7/media/MediaItemStatus;)V

    return-void
.end method

.method static synthetic access$2300(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 59
    invoke-direct {p0, p1}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->logd(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$2400(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mItemId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2402(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 59
    iput-object p1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mItemId:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$2502(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;
    .param p1, "x1"    # Z

    .prologue
    .line 59
    iput-boolean p1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mRetryingInvalidSessionId:Z

    return p1
.end method

.method static synthetic access$2600(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Landroid/os/Bundle;

    .prologue
    .line 59
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->processError(Ljava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method

.method static synthetic access$2700(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;Landroid/os/Bundle;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;
    .param p1, "x1"    # Landroid/os/Bundle;

    .prologue
    .line 59
    invoke-direct {p0, p1}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->processControlRequestResultBundle(Landroid/os/Bundle;)V

    return-void
.end method

.method static synthetic access$2800(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    .prologue
    .line 59
    iget-boolean v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mPrequeueItems:Z

    return v0
.end method

.method static synthetic access$2900(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;
    .param p1, "x1"    # Z

    .prologue
    .line 59
    invoke-direct {p0, p1}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->clearCurrentItem(Z)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    .prologue
    .line 59
    iget-boolean v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mFetchedCastToken:Z

    return v0
.end method

.method static synthetic access$3000(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;Landroid/os/Bundle;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;
    .param p1, "x1"    # Landroid/os/Bundle;

    .prologue
    .line 59
    invoke-direct {p0, p1}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->handleErrorStatus(Landroid/os/Bundle;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$302(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;
    .param p1, "x1"    # Z

    .prologue
    .line 59
    iput-boolean p1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mFetchedCastToken:Z

    return p1
.end method

.method static synthetic access$3102(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;
    .param p1, "x1"    # Z

    .prologue
    .line 59
    iput-boolean p1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mIsPlaybackRequested:Z

    return p1
.end method

.method static synthetic access$3200(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;)Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mServiceHooks:Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;

    return-object v0
.end method

.method static synthetic access$3300(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/Exception;

    .prologue
    .line 59
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->logw(Ljava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 59
    invoke-direct {p0, p1}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->logw(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    .prologue
    .line 59
    iget v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mNumberOfRetries:I

    return v0
.end method

.method static synthetic access$508(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;)I
    .locals 2
    .param p0, "x0"    # Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    .prologue
    .line 59
    iget v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mNumberOfRetries:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mNumberOfRetries:I

    return v0
.end method

.method static synthetic access$600(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;)Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mState:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;
    .param p1, "x1"    # Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    .prologue
    .line 59
    invoke-direct {p0, p1}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->setState(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;)V

    return-void
.end method

.method static synthetic access$900(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;
    .param p1, "x1"    # Z

    .prologue
    .line 59
    invoke-direct {p0, p1}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->notifyFailure(Z)V

    return-void
.end method

.method private clearCurrentItem(Z)V
    .locals 3
    .param p1, "clearCastToken"    # Z

    .prologue
    const/4 v2, 0x0

    .line 1620
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mItemId:Ljava/lang/String;

    .line 1621
    iput-boolean v2, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mIsPlaybackRequested:Z

    .line 1622
    if-eqz p1, :cond_0

    .line 1623
    iput-boolean v2, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mFetchedCastToken:Z

    .line 1624
    iget-object v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mRoute:Landroid/support/v7/media/MediaRouter$RouteInfo;

    if-eqz v0, :cond_0

    .line 1625
    iget-object v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mCastTokenClient:Lcom/google/android/music/cast/CastTokenClient;

    iget-object v1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mRoute:Landroid/support/v7/media/MediaRouter$RouteInfo;

    invoke-virtual {v1}, Landroid/support/v7/media/MediaRouter$RouteInfo;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/music/cast/CastTokenClient;->clearCachedCastToken(Ljava/lang/String;)V

    .line 1628
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mIsCurrent:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mNextPlayer:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    if-eqz v0, :cond_1

    .line 1629
    iget-object v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mNextPlayer:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    invoke-direct {v0, p1}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->clearCurrentItem(Z)V

    .line 1631
    :cond_1
    iput-boolean v2, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mRetryingInvalidSessionId:Z

    .line 1632
    return-void
.end method

.method private enqueue()V
    .locals 4

    .prologue
    .line 553
    invoke-direct {p0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->isLogVerbose()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 554
    const-string v2, "enqueue"

    invoke-direct {p0, v2}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->logd(Ljava/lang/String;)V

    .line 556
    :cond_0
    iget-object v2, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mSessionManager:Lcom/google/android/music/cast/CastSessionManager;

    invoke-virtual {v2}, Lcom/google/android/music/cast/CastSessionManager;->getSessionId()Ljava/lang/String;

    move-result-object v1

    .line 557
    .local v1, "sessionId":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 558
    const-string v2, "Empty session id"

    invoke-direct {p0, v2}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->logw(Ljava/lang/String;)V

    .line 576
    :cond_1
    :goto_0
    return-void

    .line 562
    :cond_2
    iget-boolean v2, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mIsPlaybackRequested:Z

    if-eqz v2, :cond_3

    .line 563
    const-string v2, "Playback already requested"

    invoke-direct {p0, v2}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->logw(Ljava/lang/String;)V

    goto :goto_0

    .line 567
    :cond_3
    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->clearCurrentItem(Z)V

    .line 569
    iget-boolean v2, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mPrequeueItems:Z

    if-eqz v2, :cond_1

    .line 570
    invoke-direct {p0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->isLogVerbose()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 571
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Requesting queued playback for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mUrl:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->logv(Ljava/lang/String;)V

    .line 573
    :cond_4
    iget-object v2, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mHandler:Landroid/os/Handler;

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    .line 574
    .local v0, "msg":Landroid/os/Message;
    iget-object v2, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private getSelectedRouteOnMainThread()Landroid/support/v7/media/MediaRouter$RouteInfo;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1004
    iget-object v2, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mRoute:Landroid/support/v7/media/MediaRouter$RouteInfo;

    if-eqz v2, :cond_0

    .line 1005
    iget-object v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mRoute:Landroid/support/v7/media/MediaRouter$RouteInfo;

    .line 1032
    :goto_0
    return-object v0

    .line 1008
    :cond_0
    iget-object v2, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mMediaRouter:Landroid/support/v7/media/MediaRouter;

    if-nez v2, :cond_1

    .line 1009
    iget-object v2, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/support/v7/media/MediaRouter;->getInstance(Landroid/content/Context;)Landroid/support/v7/media/MediaRouter;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mMediaRouter:Landroid/support/v7/media/MediaRouter;

    .line 1011
    :cond_1
    iget-object v2, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mMediaRouter:Landroid/support/v7/media/MediaRouter;

    invoke-virtual {v2}, Landroid/support/v7/media/MediaRouter;->getSelectedRoute()Landroid/support/v7/media/MediaRouter$RouteInfo;

    move-result-object v0

    .line 1013
    .local v0, "route":Landroid/support/v7/media/MediaRouter$RouteInfo;
    if-nez v0, :cond_2

    .line 1014
    const-string v2, "Selected route is null -- should not happen!"

    invoke-direct {p0, v2}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->logw(Ljava/lang/String;)V

    .line 1015
    invoke-direct {p0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->notifyFailureAndRouteInvalidated()V

    move-object v0, v1

    .line 1016
    goto :goto_0

    .line 1019
    :cond_2
    iget-object v2, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mContext:Landroid/content/Context;

    invoke-static {v2, v0}, Lcom/google/android/music/cast/CastUtils;->isRemoteControlIntentRoute(Landroid/content/Context;Landroid/support/v7/media/MediaRouter$RouteInfo;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 1020
    const-string v2, "Selected route is no longer remote -- bailing!"

    invoke-direct {p0, v2}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->logw(Ljava/lang/String;)V

    .line 1021
    invoke-direct {p0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->notifyFailureAndRouteInvalidated()V

    move-object v0, v1

    .line 1022
    goto :goto_0

    .line 1025
    :cond_3
    iget-object v2, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mRouteChecker:Lcom/google/android/music/utils/RouteChecker;

    invoke-virtual {v2, v0}, Lcom/google/android/music/utils/RouteChecker;->isAcceptableRoute(Landroid/support/v7/media/MediaRouter$RouteInfo;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 1026
    const-string v2, "Selected route is not acceptable -- bailing!"

    invoke-direct {p0, v2}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->logw(Ljava/lang/String;)V

    .line 1027
    invoke-direct {p0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->notifyFailureAndRouteInvalidated()V

    move-object v0, v1

    .line 1028
    goto :goto_0

    .line 1031
    :cond_4
    iput-object v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mRoute:Landroid/support/v7/media/MediaRouter$RouteInfo;

    goto :goto_0
.end method

.method private handleErrorStatus(Landroid/os/Bundle;)Z
    .locals 6
    .param p1, "data"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1593
    if-eqz p1, :cond_1

    const-string v4, "android.media.intent.extra.ERROR_CODE"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1594
    const-string v4, "android.media.intent.extra.ERROR_CODE"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 1595
    .local v0, "errorCode":I
    iget-object v1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mInternalErrorCallback:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$InternalErrorCallback;

    .line 1596
    .local v1, "internalErrorCallback":Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$InternalErrorCallback;
    if-eqz v1, :cond_0

    .line 1597
    invoke-interface {v1, v0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$InternalErrorCallback;->onError(I)V

    .line 1599
    :cond_0
    const/4 v4, 0x2

    if-ne v0, v4, :cond_1

    .line 1600
    iget-boolean v4, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mRetryingInvalidSessionId:Z

    if-eqz v4, :cond_2

    .line 1610
    .end local v0    # "errorCode":I
    .end local v1    # "internalErrorCallback":Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$InternalErrorCallback;
    :cond_1
    :goto_0
    return v2

    .line 1603
    .restart local v0    # "errorCode":I
    .restart local v1    # "internalErrorCallback":Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$InternalErrorCallback;
    :cond_2
    const-string v2, "Handling invalid session id"

    invoke-direct {p0, v2}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->logd(Ljava/lang/String;)V

    .line 1604
    invoke-direct {p0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->resetTheSessionId()V

    .line 1605
    iput-boolean v3, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mRetryingInvalidSessionId:Z

    .line 1606
    const-wide/16 v4, 0x3e8

    invoke-direct {p0, v4, v5}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->retryPlaybackRequest(J)V

    move v2, v3

    .line 1607
    goto :goto_0
.end method

.method private handleRemoteHttpError(ILandroid/os/Bundle;)Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$ErrorHandlingAction;
    .locals 17
    .param p1, "statusCode"    # I
    .param p2, "headers"    # Landroid/os/Bundle;

    .prologue
    .line 1504
    const-string v9, "handleRemoteHttpError: statusCode=%s headers=%s"

    const/4 v14, 0x2

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v14, v15

    const/4 v15, 0x1

    invoke-static/range {p2 .. p2}, Lcom/google/android/music/utils/DebugUtils;->bundleToString(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-static {v9, v14}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->logd(Ljava/lang/String;)V

    .line 1510
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->toLowerCaseKeys(Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v4

    .line 1511
    .local v4, "lowerCaseKeysBundle":Landroid/os/Bundle;
    sget-object v2, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$ErrorHandlingAction;->REPORT_ERROR:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$ErrorHandlingAction;

    .line 1513
    .local v2, "action":Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$ErrorHandlingAction;
    const/16 v9, 0x191

    move/from16 v0, p1

    if-ne v0, v9, :cond_2

    .line 1514
    move-object/from16 v0, p0

    iget-boolean v9, v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mTryRefreshCastToken:Z

    if-nez v9, :cond_1

    .line 1515
    const-string v9, "Refreshing the cast token"

    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->logd(Ljava/lang/String;)V

    .line 1516
    const/4 v9, 0x1

    move-object/from16 v0, p0

    iput-boolean v9, v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mTryRefreshCastToken:Z

    .line 1517
    sget-object v2, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$ErrorHandlingAction;->RETRY:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$ErrorHandlingAction;

    .line 1518
    const/4 v9, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->clearCurrentItem(Z)V

    .line 1519
    sget-object v9, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->WAITING_FOR_CAST_TOKEN:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->setState(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;)V

    .line 1523
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mHandler:Landroid/os/Handler;

    const/4 v14, 0x1

    invoke-virtual {v9, v14}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v5

    .line 1524
    .local v5, "message":Landroid/os/Message;
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mHandler:Landroid/os/Handler;

    invoke-virtual {v9, v5}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1527
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mHandler:Landroid/os/Handler;

    const/4 v14, 0x2

    invoke-virtual {v9, v14}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v5

    .line 1528
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mHandler:Landroid/os/Handler;

    const-wide/16 v14, 0x2710

    invoke-virtual {v9, v5, v14, v15}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 1588
    .end local v5    # "message":Landroid/os/Message;
    :cond_0
    :goto_0
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Error handling action: "

    invoke-virtual {v9, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->logd(Ljava/lang/String;)V

    .line 1589
    return-object v2

    .line 1530
    :cond_1
    const-string v9, "Already tried to refresh the cast token once - bailing"

    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->logd(Ljava/lang/String;)V

    .line 1531
    const/4 v9, 0x4

    move-object/from16 v0, p0

    iput v9, v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mDownloadError:I

    goto :goto_0

    .line 1533
    :cond_2
    const/16 v9, 0x193

    move/from16 v0, p1

    if-ne v0, v9, :cond_7

    .line 1534
    const-string v9, "X-Rejected-Reason"

    invoke-virtual {v9}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 1537
    .local v7, "rejectionReason":Ljava/lang/String;
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_0

    .line 1538
    const-string v9, "DEVICE_NOT_AUTHORIZED"

    invoke-virtual {v9, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 1540
    const/4 v9, 0x5

    move-object/from16 v0, p0

    iput v9, v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mDownloadError:I

    goto :goto_0

    .line 1541
    :cond_3
    const-string v9, "ANOTHER_STREAM_BEING_PLAYED"

    invoke-virtual {v9, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 1543
    const/4 v9, 0x6

    move-object/from16 v0, p0

    iput v9, v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mDownloadError:I

    goto :goto_0

    .line 1544
    :cond_4
    const-string v9, "STREAM_RATE_LIMIT_REACHED"

    invoke-virtual {v9, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 1546
    const/4 v9, 0x7

    move-object/from16 v0, p0

    iput v9, v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mDownloadError:I

    goto :goto_0

    .line 1547
    :cond_5
    const-string v9, "TRACK_NOT_IN_SUBSCRIPTION"

    invoke-virtual {v9, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 1549
    const/16 v9, 0xd

    move-object/from16 v0, p0

    iput v9, v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mDownloadError:I

    .line 1551
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mCurrentSongId:Lcom/google/android/music/download/ContentIdentifier;

    .line 1552
    .local v3, "id":Lcom/google/android/music/download/ContentIdentifier;
    if-eqz v3, :cond_6

    .line 1553
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mContext:Landroid/content/Context;

    invoke-static {v9, v3}, Lcom/google/android/music/download/DownloadUtils;->purgeNautilusTrackByLocalId(Landroid/content/Context;Lcom/google/android/music/download/ContentIdentifier;)V

    .line 1555
    :cond_6
    sget-object v2, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$ErrorHandlingAction;->REPORT_ERROR_AND_SKIP:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$ErrorHandlingAction;

    goto/16 :goto_0

    .line 1558
    .end local v3    # "id":Lcom/google/android/music/download/ContentIdentifier;
    .end local v7    # "rejectionReason":Ljava/lang/String;
    :cond_7
    const/16 v9, 0x194

    move/from16 v0, p1

    if-ne v0, v9, :cond_8

    .line 1559
    sget-object v2, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$ErrorHandlingAction;->REPORT_ERROR_AND_SKIP:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$ErrorHandlingAction;

    goto/16 :goto_0

    .line 1560
    :cond_8
    const/16 v9, 0x1f7

    move/from16 v0, p1

    if-ne v0, v9, :cond_b

    .line 1561
    move-object/from16 v0, p0

    iget v9, v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mNumberOfRetries:I

    const/4 v14, 0x5

    if-ge v9, v14, :cond_0

    .line 1562
    move-object/from16 v0, p0

    iget v9, v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mNumberOfRetries:I

    add-int/lit8 v9, v9, 0x1

    move-object/from16 v0, p0

    iput v9, v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mNumberOfRetries:I

    .line 1563
    sget-object v2, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$ErrorHandlingAction;->RETRY:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$ErrorHandlingAction;

    .line 1564
    const-wide/16 v10, 0x0

    .line 1565
    .local v10, "retryAfterSeconds":J
    const-string v9, "Retry-After"

    invoke-virtual {v9}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 1566
    .local v8, "retryAfter":Ljava/lang/String;
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_9

    .line 1568
    :try_start_0
    invoke-static {v8}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    .line 1569
    const-string v9, "Server said to retry after %s sec"

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-static {v9, v14}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->logw(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1576
    :goto_1
    const-wide/16 v14, 0x0

    cmp-long v9, v10, v14

    if-eqz v9, :cond_a

    const-wide/16 v14, 0x3e8

    mul-long v12, v10, v14

    .line 1578
    .local v12, "retryDelaysMs":J
    :goto_2
    move-object/from16 v0, p0

    invoke-direct {v0, v12, v13}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->retryPlaybackRequest(J)V

    goto/16 :goto_0

    .line 1570
    .end local v12    # "retryDelaysMs":J
    :catch_0
    move-exception v6

    .line 1571
    .local v6, "ne":Ljava/lang/NumberFormatException;
    const-string v9, "Received 503 with invalid Retry-After header"

    move-object/from16 v0, p0

    invoke-direct {v0, v9, v6}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->logw(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1

    .line 1574
    .end local v6    # "ne":Ljava/lang/NumberFormatException;
    :cond_9
    const-string v9, "Received 503 with no Retry-After header"

    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->logw(Ljava/lang/String;)V

    goto :goto_1

    .line 1576
    :cond_a
    const-wide/16 v12, 0x3e8

    goto :goto_2

    .line 1580
    .end local v8    # "retryAfter":Ljava/lang/String;
    .end local v10    # "retryAfterSeconds":J
    :cond_b
    const/16 v9, 0x1f6

    move/from16 v0, p1

    if-ne v0, v9, :cond_0

    .line 1581
    move-object/from16 v0, p0

    iget v9, v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mNumberOfRetries:I

    const/4 v14, 0x5

    if-ge v9, v14, :cond_0

    .line 1582
    move-object/from16 v0, p0

    iget v9, v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mNumberOfRetries:I

    add-int/lit8 v9, v9, 0x1

    move-object/from16 v0, p0

    iput v9, v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mNumberOfRetries:I

    .line 1583
    sget-object v2, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$ErrorHandlingAction;->RETRY:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$ErrorHandlingAction;

    .line 1584
    const-wide/16 v14, 0x3e8

    move-object/from16 v0, p0

    invoke-direct {v0, v14, v15}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->retryPlaybackRequest(J)V

    goto/16 :goto_0
.end method

.method public static isActionEnqueueSupported(Landroid/support/v7/media/MediaRouter$RouteInfo;)Z
    .locals 2
    .param p0, "route"    # Landroid/support/v7/media/MediaRouter$RouteInfo;

    .prologue
    .line 1658
    if-nez p0, :cond_0

    .line 1659
    const-string v0, "MusicCast"

    const-string v1, "isActionEnqueueSupported: null route"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 1660
    const/4 v0, 0x0

    .line 1662
    :goto_0
    return v0

    :cond_0
    sget-object v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->ACTION_ENQUEUE_BASE_INTENT:Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/support/v7/media/MediaRouter$RouteInfo;->supportsControlRequest(Landroid/content/Intent;)Z

    move-result v0

    goto :goto_0
.end method

.method public static isActionStartSessionSupported(Landroid/support/v7/media/MediaRouter$RouteInfo;)Z
    .locals 2
    .param p0, "route"    # Landroid/support/v7/media/MediaRouter$RouteInfo;

    .prologue
    .line 1666
    if-nez p0, :cond_0

    .line 1667
    const-string v0, "MusicCast"

    const-string v1, "isActionStartSessionSupported: null route"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 1668
    const/4 v0, 0x0

    .line 1670
    :goto_0
    return v0

    :cond_0
    sget-object v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->ACTION_START_SESSION_BASE_INTENT:Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/support/v7/media/MediaRouter$RouteInfo;->supportsControlRequest(Landroid/content/Intent;)Z

    move-result v0

    goto :goto_0
.end method

.method private isLogVerbose()Z
    .locals 2

    .prologue
    .line 1649
    const-string v0, "MusicCast"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->isLogFilesEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private logd(Ljava/lang/String;)V
    .locals 6
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 539
    const-string v0, "MusicCast"

    const-string v1, "%s[%s(%s/%s, %s) isCurrent=%s pos=%s]: %s"

    const/16 v2, 0x8

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mItemStatusCategory:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mState:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mSessionManager:Lcom/google/android/music/cast/CastSessionManager;

    invoke-virtual {v4}, Lcom/google/android/music/cast/CastSessionManager;->getSessionId()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    iget-object v4, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mItemId:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x4

    iget-object v4, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mCurrentSongId:Lcom/google/android/music/download/ContentIdentifier;

    aput-object v4, v2, v3

    const/4 v3, 0x5

    iget-boolean v4, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mIsCurrent:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x6

    invoke-virtual {p0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->position()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x7

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 543
    return-void
.end method

.method private logv(Ljava/lang/String;)V
    .locals 6
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 546
    const-string v0, "MusicCast"

    const-string v1, "%s[%s(%s/%s, %s) isCurrent=%s pos=%s]: %s "

    const/16 v2, 0x8

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mItemStatusCategory:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mState:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mSessionManager:Lcom/google/android/music/cast/CastSessionManager;

    invoke-virtual {v4}, Lcom/google/android/music/cast/CastSessionManager;->getSessionId()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    iget-object v4, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mItemId:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x4

    iget-object v4, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mCurrentSongId:Lcom/google/android/music/download/ContentIdentifier;

    aput-object v4, v2, v3

    const/4 v3, 0x5

    iget-boolean v4, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mIsCurrent:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x6

    invoke-virtual {p0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->position()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x7

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 550
    return-void
.end method

.method private logw(Ljava/lang/String;)V
    .locals 6
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 525
    const-string v0, "MusicCast"

    const-string v1, "%s[%s(%s/%s, %s) isCurrent=%s pos=%s]: %s"

    const/16 v2, 0x8

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mItemStatusCategory:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mState:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mSessionManager:Lcom/google/android/music/cast/CastSessionManager;

    invoke-virtual {v4}, Lcom/google/android/music/cast/CastSessionManager;->getSessionId()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    iget-object v4, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mItemId:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x4

    iget-object v4, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mCurrentSongId:Lcom/google/android/music/download/ContentIdentifier;

    aput-object v4, v2, v3

    const/4 v3, 0x5

    iget-boolean v4, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mIsCurrent:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x6

    invoke-virtual {p0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->position()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x7

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 529
    return-void
.end method

.method private logw(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 6
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "e"    # Ljava/lang/Exception;

    .prologue
    .line 532
    const-string v0, "MusicCast"

    const-string v1, "%s[%s(%s/%s, %s) isCurrent=%s pos=%s]: %s"

    const/16 v2, 0x8

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mItemStatusCategory:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mState:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mSessionManager:Lcom/google/android/music/cast/CastSessionManager;

    invoke-virtual {v4}, Lcom/google/android/music/cast/CastSessionManager;->getSessionId()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    iget-object v4, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mItemId:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x4

    iget-object v4, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mCurrentSongId:Lcom/google/android/music/download/ContentIdentifier;

    aput-object v4, v2, v3

    const/4 v3, 0x5

    iget-boolean v4, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mIsCurrent:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x6

    invoke-virtual {p0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->position()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x7

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p2}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 536
    return-void
.end method

.method private notifyFailure(Z)V
    .locals 2
    .param p1, "skipToNext"    # Z

    .prologue
    .line 1642
    iget-object v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mCallback:Lcom/google/android/music/playback/AsyncMediaPlayer$AsyncCommandCallback;

    .line 1643
    .local v0, "callback":Lcom/google/android/music/playback/AsyncMediaPlayer$AsyncCommandCallback;
    if-eqz v0, :cond_0

    iget-boolean v1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mIsCurrent:Z

    if-eqz v1, :cond_0

    .line 1644
    invoke-interface {v0, p1}, Lcom/google/android/music/playback/AsyncMediaPlayer$AsyncCommandCallback;->onFailure(Z)V

    .line 1646
    :cond_0
    return-void
.end method

.method private notifyFailureAndRouteInvalidated()V
    .locals 1

    .prologue
    .line 1690
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->notifyFailure(Z)V

    .line 1691
    iget-boolean v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mIsCurrent:Z

    if-eqz v0, :cond_0

    .line 1692
    iget-object v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mServiceHooks:Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;

    invoke-interface {v0}, Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;->notifyMediaRouteInvalidated()V

    .line 1694
    :cond_0
    return-void
.end method

.method private notifyIfSongPlayed()V
    .locals 10

    .prologue
    .line 1357
    iget-object v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mCurrentSongId:Lcom/google/android/music/download/ContentIdentifier;

    .line 1358
    .local v0, "songId":Lcom/google/android/music/download/ContentIdentifier;
    if-eqz v0, :cond_0

    iget-boolean v1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mIsCurrent:Z

    if-eqz v1, :cond_0

    .line 1359
    iget-object v1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mEventLogger:Lcom/google/android/music/eventlog/MusicEventLogger;

    iget-object v2, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    iget-object v3, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mMusicFile:Lcom/google/android/music/store/MusicFile;

    const/4 v4, 0x1

    iget-boolean v5, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mFromUserAction:Z

    invoke-virtual {p0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->position()J

    move-result-wide v6

    invoke-virtual {p0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->duration()J

    move-result-wide v8

    invoke-virtual/range {v1 .. v9}, Lcom/google/android/music/eventlog/MusicEventLogger;->logPlayEventAsync(Lcom/google/android/music/store/ContainerDescriptor;Lcom/google/android/music/store/MusicFile;ZZJJ)V

    .line 1361
    iget-object v1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->duration()J

    move-result-wide v2

    invoke-virtual {p0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->position()J

    move-result-wide v4

    invoke-static {v1, v2, v3, v4, v5}, Lcom/google/android/music/utils/PlaybackUtils;->isPlayed(Landroid/content/Context;JJ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1362
    sget-object v1, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    new-instance v2, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$7;

    invoke-direct {v2, p0, v0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$7;-><init>(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;Lcom/google/android/music/download/ContentIdentifier;)V

    invoke-static {v1, v2}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsync(Lcom/google/android/music/utils/LoggableHandler;Ljava/lang/Runnable;)V

    .line 1375
    :cond_0
    return-void
.end method

.method private notifyOpenStarted()V
    .locals 1

    .prologue
    .line 1716
    iget-boolean v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mIsCurrent:Z

    if-eqz v0, :cond_0

    .line 1717
    iget-object v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mServiceHooks:Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;

    invoke-interface {v0}, Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;->notifyOpenStarted()V

    .line 1719
    :cond_0
    return-void
.end method

.method private notifyTrackEnded()V
    .locals 1

    .prologue
    .line 1710
    iget-boolean v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mIsCurrent:Z

    if-eqz v0, :cond_0

    .line 1711
    iget-object v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mServiceHooks:Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;

    invoke-interface {v0}, Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;->reportTrackEnded()V

    .line 1713
    :cond_0
    return-void
.end method

.method private notifyTrackInvalidated()V
    .locals 1

    .prologue
    .line 1682
    iget-boolean v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mIsCurrent:Z

    if-eqz v0, :cond_0

    .line 1683
    iget-object v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mServiceHooks:Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;

    invoke-interface {v0}, Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;->reportTrackPaused()V

    .line 1684
    iget-object v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mServiceHooks:Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;

    invoke-interface {v0}, Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;->notifyPlayStateChanged()V

    .line 1685
    iget-object v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mServiceHooks:Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;

    invoke-interface {v0}, Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;->notifyMediaRouteInvalidated()V

    .line 1687
    :cond_0
    return-void
.end method

.method private notifyTrackPaused()V
    .locals 1

    .prologue
    .line 1697
    iget-boolean v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mIsCurrent:Z

    if-eqz v0, :cond_0

    .line 1698
    iget-object v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mServiceHooks:Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;

    invoke-interface {v0}, Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;->reportTrackPaused()V

    .line 1699
    iget-object v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mServiceHooks:Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;

    invoke-interface {v0}, Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;->notifyPlayStateChanged()V

    .line 1701
    :cond_0
    return-void
.end method

.method private notifyTrackPlayStateChanged()V
    .locals 1

    .prologue
    .line 1704
    iget-boolean v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mIsCurrent:Z

    if-eqz v0, :cond_0

    .line 1705
    iget-object v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mServiceHooks:Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;

    invoke-interface {v0}, Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;->notifyPlayStateChanged()V

    .line 1707
    :cond_0
    return-void
.end method

.method private notifyTrackPlaying()V
    .locals 1

    .prologue
    .line 1674
    iget-boolean v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mIsCurrent:Z

    if-eqz v0, :cond_0

    .line 1675
    iget-object v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mServiceHooks:Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;

    invoke-interface {v0}, Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;->notifyOpenComplete()V

    .line 1676
    iget-object v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mServiceHooks:Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;

    invoke-interface {v0}, Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;->reportTrackPlaying()V

    .line 1677
    iget-object v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mServiceHooks:Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;

    invoke-interface {v0}, Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;->notifyPlayStateChanged()V

    .line 1679
    :cond_0
    return-void
.end method

.method private populateSessionId(Landroid/content/Intent;)V
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 806
    iget-object v1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mSessionManager:Lcom/google/android/music/cast/CastSessionManager;

    invoke-virtual {v1}, Lcom/google/android/music/cast/CastSessionManager;->getSessionId()Ljava/lang/String;

    move-result-object v0

    .line 807
    .local v0, "sessionId":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 808
    const-string v1, "android.media.intent.extra.SESSION_ID"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 810
    :cond_0
    return-void
.end method

.method private preparePlayIntent(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/music/store/MusicFile;)Landroid/content/Intent;
    .locals 13
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "castToken"    # Ljava/lang/String;
    .param p3, "musicFile"    # Lcom/google/android/music/store/MusicFile;

    .prologue
    .line 1036
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 1037
    .local v3, "playIntent":Landroid/content/Intent;
    const-string v8, "android.media.intent.category.REMOTE_PLAYBACK"

    invoke-virtual {v3, v8}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 1038
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    const-string v9, "audio/mpeg"

    invoke-virtual {v3, v8, v9}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 1040
    iget-boolean v8, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mIsCurrent:Z

    if-eqz v8, :cond_2

    .line 1041
    const-string v8, "android.media.intent.action.PLAY"

    invoke-virtual {v3, v8}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1052
    :goto_0
    invoke-direct {p0, v3}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->populateSessionId(Landroid/content/Intent;)V

    .line 1054
    invoke-virtual {p0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->position()J

    move-result-wide v4

    .line 1055
    .local v4, "position":J
    const-wide/16 v8, 0x0

    cmp-long v8, v4, v8

    if-lez v8, :cond_0

    .line 1056
    const-string v8, "android.media.intent.extra.ITEM_POSITION"

    invoke-virtual {v3, v8, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1060
    :cond_0
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1061
    .local v1, "headers":Landroid/os/Bundle;
    const-string v8, "Authorization"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "playon="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v8, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1063
    const-string v8, "android.media.intent.extra.HTTP_HEADERS"

    invoke-virtual {v3, v8, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 1065
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 1066
    .local v2, "metadata":Landroid/os/Bundle;
    const-string v8, "android.media.metadata.TITLE"

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/music/store/MusicFile;->getTitle()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2, v8, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1067
    const-string v8, "android.media.metadata.ALBUM_ARTIST"

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/music/store/MusicFile;->getAlbumArtist()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2, v8, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1068
    const-string v8, "android.media.metadata.ARTIST"

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/music/store/MusicFile;->getTrackArtist()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2, v8, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1069
    const-string v8, "android.media.metadata.ALBUM_TITLE"

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/music/store/MusicFile;->getAlbumName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2, v8, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1070
    invoke-virtual/range {p3 .. p3}, Lcom/google/android/music/store/MusicFile;->getAlbumArtLocation()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/google/android/music/utils/AlbumArtUtils;->stripDimensionFromImageUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1072
    .local v0, "albumArtLocation":Ljava/lang/String;
    const-string v8, "android.media.metadata.ARTWORK_URI"

    invoke-virtual {v2, v8, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1073
    const-string v8, "android.media.metadata.DURATION"

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/music/store/MusicFile;->getDurationInMilliSec()J

    move-result-wide v10

    invoke-virtual {v2, v8, v10, v11}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 1074
    invoke-direct {p0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->isLogVerbose()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 1075
    const-string v8, "MusicCast"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v10, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mItemStatusCategory:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ": Metadata:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {v2}, Lcom/google/android/music/utils/DebugUtils;->bundleToString(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 1079
    :cond_1
    const-string v8, "android.media.intent.extra.ITEM_METADATA"

    invoke-virtual {v3, v8, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 1080
    new-instance v7, Landroid/content/Intent;

    const-string v8, "com.google.android.music.cast.ACTION_STATUS_CHANGED"

    invoke-direct {v7, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1081
    .local v7, "statusIntent":Landroid/content/Intent;
    iget-object v8, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mItemStatusCategory:Ljava/lang/String;

    invoke-virtual {v7, v8}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 1082
    iget-object v8, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mContext:Landroid/content/Context;

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-static {v8, v9, v7, v10}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v6

    .line 1085
    .local v6, "receiveUpdatesIntent":Landroid/app/PendingIntent;
    const-string v8, "android.media.intent.extra.ITEM_STATUS_UPDATE_RECEIVER"

    invoke-virtual {v3, v8, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1088
    return-object v3

    .line 1043
    .end local v0    # "albumArtLocation":Ljava/lang/String;
    .end local v1    # "headers":Landroid/os/Bundle;
    .end local v2    # "metadata":Landroid/os/Bundle;
    .end local v4    # "position":J
    .end local v6    # "receiveUpdatesIntent":Landroid/app/PendingIntent;
    .end local v7    # "statusIntent":Landroid/content/Intent;
    :cond_2
    iget-boolean v8, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mPrequeueItems:Z

    if-eqz v8, :cond_3

    iget-object v8, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mSessionManager:Lcom/google/android/music/cast/CastSessionManager;

    invoke-virtual {v8}, Lcom/google/android/music/cast/CastSessionManager;->getSessionId()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 1044
    :cond_3
    const-string v8, "MusicCast"

    const-string v9, "Action ENQUEUE requested with mPrequeueItems=%s, mSessionId=%s"

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    iget-boolean v12, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mPrequeueItems:Z

    invoke-static {v12}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    iget-object v12, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mSessionManager:Lcom/google/android/music/cast/CastSessionManager;

    invoke-virtual {v12}, Lcom/google/android/music/cast/CastSessionManager;->getSessionId()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    .line 1048
    :cond_4
    const-string v8, "android.media.intent.action.ENQUEUE"

    invoke-virtual {v3, v8}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_0
.end method

.method private processControlRequestResultBundle(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "data"    # Landroid/os/Bundle;

    .prologue
    .line 959
    if-nez p1, :cond_0

    .line 960
    const-string v1, "Result data is null!"

    invoke-direct {p0, v1, p1}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->processError(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 972
    :goto_0
    return-void

    .line 961
    :cond_0
    const-string v1, "android.media.intent.extra.ITEM_STATUS"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 962
    const-string v1, "android.media.intent.extra.ITEM_STATUS"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    invoke-static {v1}, Landroid/support/v7/media/MediaItemStatus;->fromBundle(Landroid/os/Bundle;)Landroid/support/v7/media/MediaItemStatus;

    move-result-object v0

    .line 964
    .local v0, "itemStatus":Landroid/support/v7/media/MediaItemStatus;
    if-nez v0, :cond_1

    .line 965
    const-string v1, "ITEM_STATUS is null!"

    invoke-direct {p0, v1, p1}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->processError(Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0

    .line 967
    :cond_1
    invoke-direct {p0, v0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->processMediaItemStatus(Landroid/support/v7/media/MediaItemStatus;)V

    goto :goto_0

    .line 970
    .end local v0    # "itemStatus":Landroid/support/v7/media/MediaItemStatus;
    :cond_2
    const-string v1, "Result data contains no status!"

    invoke-direct {p0, v1, p1}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->processError(Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method private processError(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "e"    # Ljava/lang/String;
    .param p2, "data"    # Landroid/os/Bundle;

    .prologue
    .line 982
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Error encountered processing returned bundle: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " Bundle: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p2}, Lcom/google/android/music/utils/DebugUtils;->bundleToString(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->logw(Ljava/lang/String;)V

    .line 984
    sget-object v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->ERROR:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    invoke-direct {p0, v0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->setState(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;)V

    .line 985
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->notifyFailure(Z)V

    .line 986
    return-void
.end method

.method private processMediaItemStatus(Landroid/support/v7/media/MediaItemStatus;)V
    .locals 18
    .param p1, "status"    # Landroid/support/v7/media/MediaItemStatus;

    .prologue
    .line 321
    const/4 v6, 0x0

    .line 323
    .local v6, "log":Z
    invoke-virtual/range {p1 .. p1}, Landroid/support/v7/media/MediaItemStatus;->getPlaybackState()I

    move-result v7

    .line 329
    .local v7, "state":I
    const/4 v9, 0x7

    if-ne v7, v9, :cond_0

    .line 330
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mPositionDeltaStopWatch:Lcom/google/android/music/playback/StopWatch;

    invoke-virtual {v9}, Lcom/google/android/music/playback/StopWatch;->pause()V

    .line 332
    :cond_0
    invoke-virtual/range {p1 .. p1}, Landroid/support/v7/media/MediaItemStatus;->getContentPosition()J

    move-result-wide v12

    .line 335
    .local v12, "updatedPosition":J
    const-wide/16 v14, -0x1

    cmp-long v9, v12, v14

    if-lez v9, :cond_1

    const/4 v9, 0x4

    if-eq v7, v9, :cond_1

    .line 336
    move-object/from16 v0, p0

    invoke-direct {v0, v12, v13}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->updatePosition(J)V

    .line 339
    :cond_1
    packed-switch v7, :pswitch_data_0

    .line 452
    :cond_2
    :goto_0
    if-eqz v6, :cond_3

    invoke-direct/range {p0 .. p0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->isLogVerbose()Z

    move-result v9

    if-eqz v9, :cond_3

    .line 453
    const-string v9, "Processing status status=%s status.extras=%s"

    const/4 v14, 0x2

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    aput-object p1, v14, v15

    const/4 v15, 0x1

    invoke-virtual/range {p1 .. p1}, Landroid/support/v7/media/MediaItemStatus;->getExtras()Landroid/os/Bundle;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Lcom/google/android/music/utils/DebugUtils;->bundleToString(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-static {v9, v14}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->logd(Ljava/lang/String;)V

    .line 457
    :cond_3
    return-void

    .line 341
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mState:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    sget-object v14, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->BUFFERING:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    if-eq v9, v14, :cond_2

    .line 342
    const/4 v6, 0x1

    .line 343
    sget-object v9, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->BUFFERING:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->setState(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;)V

    .line 344
    move-object/from16 v0, p0

    iget-boolean v9, v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mIsCurrent:Z

    if-nez v9, :cond_2

    .line 345
    const-string v9, "Buffering state should be only for the current player"

    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->logw(Ljava/lang/String;)V

    goto :goto_0

    .line 350
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mState:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    sget-object v14, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->INVALIDATED:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    if-eq v9, v14, :cond_2

    .line 351
    const/4 v6, 0x1

    .line 352
    sget-object v9, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->INVALIDATED:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->setState(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;)V

    .line 353
    invoke-direct/range {p0 .. p0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->notifyTrackInvalidated()V

    goto :goto_0

    .line 357
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mState:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    sget-object v14, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->CANCELED:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    if-eq v9, v14, :cond_2

    .line 358
    const/4 v6, 0x1

    .line 359
    sget-object v9, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->CANCELED:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->setState(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;)V

    .line 360
    invoke-direct/range {p0 .. p0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->notifyTrackPaused()V

    goto :goto_0

    .line 364
    :pswitch_3
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mState:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    sget-object v14, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->ERROR:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    if-eq v9, v14, :cond_2

    .line 365
    const/4 v6, 0x1

    .line 366
    invoke-virtual/range {p1 .. p1}, Landroid/support/v7/media/MediaItemStatus;->getExtras()Landroid/os/Bundle;

    move-result-object v8

    .line 367
    .local v8, "statusExtras":Landroid/os/Bundle;
    if-nez v8, :cond_4

    .line 368
    sget-object v9, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->ERROR:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->setState(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;)V

    .line 369
    const-string v9, "statusExtra=null"

    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->logw(Ljava/lang/String;)V

    .line 370
    const/4 v9, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->notifyFailure(Z)V

    goto/16 :goto_0

    .line 371
    :cond_4
    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->handleErrorStatus(Landroid/os/Bundle;)Z

    move-result v9

    if-nez v9, :cond_2

    .line 372
    const-string v9, "android.media.status.extra.HTTP_RESPONSE_HEADERS"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v3

    .line 374
    .local v3, "httpHeaders":Landroid/os/Bundle;
    const-string v9, "android.media.status.extra.HTTP_STATUS_CODE"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    .line 377
    .local v4, "httpStatusCode":I
    move-object/from16 v0, p0

    invoke-direct {v0, v4, v3}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->handleRemoteHttpError(ILandroid/os/Bundle;)Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$ErrorHandlingAction;

    move-result-object v2

    .line 379
    .local v2, "action":Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$ErrorHandlingAction;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mInternalHttpErrorCallback:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$InternalHttpErrorCallback;

    .line 381
    .local v5, "internalHttpErrorCallback":Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$InternalHttpErrorCallback;
    if-eqz v5, :cond_5

    .line 382
    invoke-interface {v5, v4, v2}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$InternalHttpErrorCallback;->onHttpError(ILcom/google/android/music/cast/RemoteAsyncMediaPlayer$ErrorHandlingAction;)V

    .line 384
    :cond_5
    sget-object v9, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$8;->$SwitchMap$com$google$android$music$cast$RemoteAsyncMediaPlayer$ErrorHandlingAction:[I

    invoke-virtual {v2}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$ErrorHandlingAction;->ordinal()I

    move-result v14

    aget v9, v9, v14

    packed-switch v9, :pswitch_data_1

    goto/16 :goto_0

    .line 386
    :pswitch_4
    sget-object v9, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->ERROR:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->setState(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;)V

    .line 387
    move-object/from16 v0, p0

    iget-boolean v9, v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mIsCurrent:Z

    if-eqz v9, :cond_2

    .line 388
    const/4 v9, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->notifyFailure(Z)V

    goto/16 :goto_0

    .line 392
    :pswitch_5
    sget-object v9, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->ERROR:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->setState(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;)V

    .line 393
    move-object/from16 v0, p0

    iget-boolean v9, v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mIsCurrent:Z

    if-eqz v9, :cond_2

    .line 394
    const/4 v9, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->notifyFailure(Z)V

    goto/16 :goto_0

    .line 404
    .end local v2    # "action":Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$ErrorHandlingAction;
    .end local v3    # "httpHeaders":Landroid/os/Bundle;
    .end local v4    # "httpStatusCode":I
    .end local v5    # "internalHttpErrorCallback":Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$InternalHttpErrorCallback;
    .end local v8    # "statusExtras":Landroid/os/Bundle;
    :pswitch_6
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mState:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    sget-object v14, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->PAUSED:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    if-eq v9, v14, :cond_2

    .line 405
    const/4 v6, 0x1

    .line 406
    sget-object v9, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->PAUSED:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->setState(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;)V

    .line 407
    move-object/from16 v0, p0

    iget-boolean v9, v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mIsCurrent:Z

    if-eqz v9, :cond_6

    .line 408
    invoke-direct/range {p0 .. p0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->notifyTrackPaused()V

    goto/16 :goto_0

    .line 410
    :cond_6
    const-string v9, "Paused state should be only for the current player"

    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->logw(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 415
    :pswitch_7
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mState:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    sget-object v14, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->PLAYING:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    if-eq v9, v14, :cond_2

    .line 416
    const/4 v9, 0x0

    move-object/from16 v0, p0

    iput-boolean v9, v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mTryRefreshCastToken:Z

    .line 417
    const/4 v9, 0x0

    move-object/from16 v0, p0

    iput v9, v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mNumberOfRetries:I

    .line 418
    sget-object v9, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->PLAYING:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->setState(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;)V

    .line 419
    move-object/from16 v0, p0

    iget-boolean v9, v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mIsCurrent:Z

    if-eqz v9, :cond_7

    .line 420
    invoke-direct/range {p0 .. p0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->notifyTrackPlaying()V

    goto/16 :goto_0

    .line 422
    :cond_7
    const-string v9, "Playing state should be only for the current player"

    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->logw(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 427
    :pswitch_8
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mState:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    sget-object v14, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->PENDING:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    if-eq v9, v14, :cond_2

    .line 428
    const/4 v6, 0x1

    .line 429
    sget-object v9, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->PENDING:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->setState(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;)V

    goto/16 :goto_0

    .line 433
    :pswitch_9
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mState:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    sget-object v14, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->FINISHED:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    if-eq v9, v14, :cond_2

    .line 434
    const/4 v6, 0x1

    .line 435
    sget-object v9, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->FINISHED:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->setState(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;)V

    .line 437
    move-object/from16 v0, p0

    iget-boolean v9, v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mIsCurrent:Z

    if-eqz v9, :cond_8

    .line 438
    invoke-direct/range {p0 .. p0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->notifyTrackEnded()V

    .line 443
    :goto_1
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    const-string v14, "music_remote_player_temp_wake_lock_timeout_ms"

    const-wide/16 v16, 0x1388

    move-wide/from16 v0, v16

    invoke-static {v9, v14, v0, v1}, Lcom/google/android/gsf/Gservices;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v10

    .line 447
    .local v10, "timeoutMs":J
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v9, v10, v11}, Landroid/os/PowerManager$WakeLock;->acquire(J)V

    goto/16 :goto_0

    .line 440
    .end local v10    # "timeoutMs":J
    :cond_8
    const-string v9, "FINISHED state should be only for the current player"

    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->logw(Ljava/lang/String;)V

    goto :goto_1

    .line 339
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_0
        :pswitch_9
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch

    .line 384
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private requestCastV2RemoteSyncStatusOnMainThread()V
    .locals 6

    .prologue
    .line 917
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.google.android.gms.cast.ACTION_SYNC_STATUS"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 918
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "android.media.intent.category.REMOTE_PLAYBACK"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 919
    invoke-direct {p0, v0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->populateSessionId(Landroid/content/Intent;)V

    .line 921
    invoke-direct {p0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->getSelectedRouteOnMainThread()Landroid/support/v7/media/MediaRouter$RouteInfo;

    move-result-object v1

    .line 923
    .local v1, "route":Landroid/support/v7/media/MediaRouter$RouteInfo;
    if-nez v1, :cond_0

    .line 952
    :goto_0
    return-void

    .line 927
    :cond_0
    invoke-direct {p0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->isLogVerbose()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 928
    const-string v2, "Sending intent=%s extras=%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v4, 0x1

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/music/utils/DebugUtils;->bundleToString(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->logd(Ljava/lang/String;)V

    .line 933
    :cond_1
    new-instance v2, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$6;

    invoke-direct {v2, p0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$6;-><init>(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;)V

    invoke-virtual {v1, v0, v2}, Landroid/support/v7/media/MediaRouter$RouteInfo;->sendControlRequest(Landroid/content/Intent;Landroid/support/v7/media/MediaRouter$ControlRequestCallback;)V

    goto :goto_0
.end method

.method private requestRemotePauseOnMainThread()V
    .locals 6

    .prologue
    .line 773
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.media.intent.action.PAUSE"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 774
    .local v0, "pauseIntent":Landroid/content/Intent;
    const-string v2, "android.media.intent.category.REMOTE_PLAYBACK"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 775
    invoke-direct {p0, v0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->populateSessionId(Landroid/content/Intent;)V

    .line 777
    invoke-direct {p0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->getSelectedRouteOnMainThread()Landroid/support/v7/media/MediaRouter$RouteInfo;

    move-result-object v1

    .line 779
    .local v1, "route":Landroid/support/v7/media/MediaRouter$RouteInfo;
    if-nez v1, :cond_0

    .line 803
    :goto_0
    return-void

    .line 783
    :cond_0
    invoke-direct {p0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->isLogVerbose()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 784
    const-string v2, "Sending intent=%s extras=%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v4, 0x1

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/music/utils/DebugUtils;->bundleToString(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->logd(Ljava/lang/String;)V

    .line 789
    :cond_1
    new-instance v2, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$2;

    invoke-direct {v2, p0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$2;-><init>(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;)V

    invoke-virtual {v1, v0, v2}, Landroid/support/v7/media/MediaRouter$RouteInfo;->sendControlRequest(Landroid/content/Intent;Landroid/support/v7/media/MediaRouter$ControlRequestCallback;)V

    goto :goto_0
.end method

.method private requestRemotePlaybackOnMainThread()V
    .locals 14

    .prologue
    const/4 v11, 0x2

    const-wide/16 v12, 0x2710

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 655
    invoke-direct {p0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->getSelectedRouteOnMainThread()Landroid/support/v7/media/MediaRouter$RouteInfo;

    move-result-object v5

    .line 656
    .local v5, "route":Landroid/support/v7/media/MediaRouter$RouteInfo;
    if-nez v5, :cond_0

    .line 770
    :goto_0
    return-void

    .line 660
    :cond_0
    invoke-virtual {v5}, Landroid/support/v7/media/MediaRouter$RouteInfo;->getId()Ljava/lang/String;

    move-result-object v6

    .line 662
    .local v6, "routeId":Ljava/lang/String;
    iget-object v7, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mCastTokenClient:Lcom/google/android/music/cast/CastTokenClient;

    invoke-interface {v7, v6}, Lcom/google/android/music/cast/CastTokenClient;->hasCachedCastToken(Ljava/lang/String;)Z

    move-result v7

    iput-boolean v7, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mFetchedCastToken:Z

    .line 664
    iget-object v7, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mSessionManager:Lcom/google/android/music/cast/CastSessionManager;

    invoke-virtual {v7}, Lcom/google/android/music/cast/CastSessionManager;->getSessionId()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-static {v5}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->isActionStartSessionSupported(Landroid/support/v7/media/MediaRouter$RouteInfo;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 665
    sget-object v7, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->WAITING_FOR_SESSION_ID:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    invoke-direct {p0, v7}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->setState(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;)V

    .line 666
    iget-object v7, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mHandler:Landroid/os/Handler;

    const/16 v8, 0xc

    invoke-virtual {v7, v8}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    .line 667
    .local v2, "message":Landroid/os/Message;
    iget-object v7, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mHandler:Landroid/os/Handler;

    invoke-virtual {v7, v2, v12, v13}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    .line 670
    .end local v2    # "message":Landroid/os/Message;
    :cond_1
    iget-boolean v7, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mFetchedCastToken:Z

    if-nez v7, :cond_2

    .line 671
    sget-object v7, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->WAITING_FOR_CAST_TOKEN:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    invoke-direct {p0, v7}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->setState(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;)V

    .line 675
    iget-object v7, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mHandler:Landroid/os/Handler;

    invoke-virtual {v7, v10}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    .line 676
    .restart local v2    # "message":Landroid/os/Message;
    iget-object v7, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mHandler:Landroid/os/Handler;

    invoke-virtual {v7, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 679
    iget-object v7, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mHandler:Landroid/os/Handler;

    invoke-virtual {v7, v11}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    .line 680
    iget-object v7, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mHandler:Landroid/os/Handler;

    invoke-virtual {v7, v2, v12, v13}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    .line 683
    .end local v2    # "message":Landroid/os/Message;
    :cond_2
    invoke-static {v5}, Lcom/google/android/music/cast/CastUtils;->isCastV2Route(Landroid/support/v7/media/MediaRouter$RouteInfo;)Z

    move-result v7

    if-eqz v7, :cond_3

    iget-object v7, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mSessionManager:Lcom/google/android/music/cast/CastSessionManager;

    invoke-virtual {v7}, Lcom/google/android/music/cast/CastSessionManager;->isCastV2ReceiverLoaded()Z

    move-result v7

    if-nez v7, :cond_3

    .line 684
    sget-object v7, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->WAITING_FOR_RECEIVER_LOADED:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    invoke-direct {p0, v7}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->setState(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;)V

    .line 685
    iget-object v7, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mHandler:Landroid/os/Handler;

    const/16 v8, 0xd

    invoke-virtual {v7, v8}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    .line 686
    .restart local v2    # "message":Landroid/os/Message;
    iget-object v7, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mHandler:Landroid/os/Handler;

    invoke-virtual {v7, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 687
    iget-object v7, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mHandler:Landroid/os/Handler;

    const/16 v8, 0xe

    invoke-virtual {v7, v8}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    .line 688
    iget-object v7, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mHandler:Landroid/os/Handler;

    invoke-virtual {v7, v2, v12, v13}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    .line 691
    .end local v2    # "message":Landroid/os/Message;
    :cond_3
    iget-object v7, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mCastTokenClient:Lcom/google/android/music/cast/CastTokenClient;

    invoke-interface {v7, v6}, Lcom/google/android/music/cast/CastTokenClient;->getCachedCastToken(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 692
    .local v0, "castToken":Ljava/lang/String;
    if-nez v0, :cond_4

    .line 693
    const-string v7, "Could not fetch cast token from server."

    invoke-direct {p0, v7}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->logw(Ljava/lang/String;)V

    .line 694
    invoke-direct {p0, v9}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->notifyFailure(Z)V

    goto/16 :goto_0

    .line 698
    :cond_4
    iget-object v7, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mState:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    invoke-virtual {v7}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->isPlaybackRequestAllowed()Z

    move-result v7

    if-nez v7, :cond_5

    .line 699
    const-string v7, "Playback request not allowed in the current state"

    invoke-direct {p0, v7}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->logw(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 703
    :cond_5
    iget-object v7, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mItemStatusReceiver:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$ItemStatusReceiver;

    if-nez v7, :cond_6

    .line 704
    new-instance v7, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$ItemStatusReceiver;

    const/4 v8, 0x0

    invoke-direct {v7, p0, v8}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$ItemStatusReceiver;-><init>(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$1;)V

    iput-object v7, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mItemStatusReceiver:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$ItemStatusReceiver;

    .line 705
    new-instance v1, Landroid/content/IntentFilter;

    const-string v7, "com.google.android.music.cast.ACTION_STATUS_CHANGED"

    invoke-direct {v1, v7}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 706
    .local v1, "filter":Landroid/content/IntentFilter;
    iget-object v7, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mItemStatusCategory:Ljava/lang/String;

    invoke-virtual {v1, v7}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    .line 707
    iget-object v7, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mContext:Landroid/content/Context;

    iget-object v8, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mItemStatusReceiver:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$ItemStatusReceiver;

    invoke-virtual {v7, v8, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 712
    .end local v1    # "filter":Landroid/content/IntentFilter;
    :goto_1
    iget-object v3, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mMusicFile:Lcom/google/android/music/store/MusicFile;

    .line 713
    .local v3, "musicFile":Lcom/google/android/music/store/MusicFile;
    if-nez v3, :cond_7

    .line 714
    const-string v7, "Missing MusicFile object"

    invoke-direct {p0, v7}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->logw(Ljava/lang/String;)V

    .line 715
    invoke-direct {p0, v9}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->notifyFailure(Z)V

    goto/16 :goto_0

    .line 709
    .end local v3    # "musicFile":Lcom/google/android/music/store/MusicFile;
    :cond_6
    const-string v7, "ItemStatusReceiver already registered!"

    invoke-direct {p0, v7}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->logw(Ljava/lang/String;)V

    goto :goto_1

    .line 719
    .restart local v3    # "musicFile":Lcom/google/android/music/store/MusicFile;
    :cond_7
    iget-object v7, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mUrl:Ljava/lang/String;

    invoke-direct {p0, v7, v0, v3}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->preparePlayIntent(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/music/store/MusicFile;)Landroid/content/Intent;

    move-result-object v4

    .line 721
    .local v4, "playIntent":Landroid/content/Intent;
    invoke-direct {p0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->isLogVerbose()Z

    move-result v7

    if-eqz v7, :cond_8

    .line 722
    const-string v7, "Sending intent=%s extras=%s"

    new-array v8, v11, [Ljava/lang/Object;

    aput-object v4, v8, v9

    invoke-virtual {v4}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v9

    invoke-static {v9}, Lcom/google/android/music/utils/DebugUtils;->bundleToString(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v10

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->logd(Ljava/lang/String;)V

    .line 727
    :cond_8
    sget-object v7, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->PLAY_REQUEST_SENT:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    invoke-direct {p0, v7}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->setState(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;)V

    .line 729
    new-instance v7, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$1;

    invoke-direct {v7, p0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$1;-><init>(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;)V

    invoke-virtual {v5, v4, v7}, Landroid/support/v7/media/MediaRouter$RouteInfo;->sendControlRequest(Landroid/content/Intent;Landroid/support/v7/media/MediaRouter$ControlRequestCallback;)V

    .line 769
    iput-boolean v10, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mIsPlaybackRequested:Z

    goto/16 :goto_0
.end method

.method private requestRemoteResumeOnMainThread()V
    .locals 6

    .prologue
    .line 813
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.media.intent.action.RESUME"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 814
    .local v0, "resumeIntent":Landroid/content/Intent;
    const-string v2, "android.media.intent.category.REMOTE_PLAYBACK"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 815
    invoke-direct {p0, v0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->populateSessionId(Landroid/content/Intent;)V

    .line 817
    invoke-direct {p0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->getSelectedRouteOnMainThread()Landroid/support/v7/media/MediaRouter$RouteInfo;

    move-result-object v1

    .line 819
    .local v1, "route":Landroid/support/v7/media/MediaRouter$RouteInfo;
    if-nez v1, :cond_0

    .line 843
    :goto_0
    return-void

    .line 823
    :cond_0
    invoke-direct {p0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->isLogVerbose()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 824
    const-string v2, "Sending intent=%s extras=%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v4, 0x1

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/music/utils/DebugUtils;->bundleToString(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->logd(Ljava/lang/String;)V

    .line 829
    :cond_1
    new-instance v2, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$3;

    invoke-direct {v2, p0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$3;-><init>(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;)V

    invoke-virtual {v1, v0, v2}, Landroid/support/v7/media/MediaRouter$RouteInfo;->sendControlRequest(Landroid/content/Intent;Landroid/support/v7/media/MediaRouter$ControlRequestCallback;)V

    goto :goto_0
.end method

.method private requestRemoteSeekOnMainThread()V
    .locals 6

    .prologue
    .line 846
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.media.intent.action.SEEK"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 847
    .local v1, "seekIntent":Landroid/content/Intent;
    const-string v2, "android.media.intent.category.REMOTE_PLAYBACK"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 848
    invoke-direct {p0, v1}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->populateSessionId(Landroid/content/Intent;)V

    .line 850
    const-string v2, "android.media.intent.extra.ITEM_ID"

    iget-object v3, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mItemId:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 851
    const-string v2, "android.media.intent.extra.ITEM_POSITION"

    iget-wide v4, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mKnownPositionMillis:J

    invoke-virtual {v1, v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 853
    invoke-direct {p0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->getSelectedRouteOnMainThread()Landroid/support/v7/media/MediaRouter$RouteInfo;

    move-result-object v0

    .line 855
    .local v0, "route":Landroid/support/v7/media/MediaRouter$RouteInfo;
    if-nez v0, :cond_0

    .line 879
    :goto_0
    return-void

    .line 859
    :cond_0
    invoke-direct {p0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->isLogVerbose()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 860
    const-string v2, "Sending intent=%s extras=%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    const/4 v4, 0x1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/music/utils/DebugUtils;->bundleToString(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->logd(Ljava/lang/String;)V

    .line 865
    :cond_1
    new-instance v2, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$4;

    invoke-direct {v2, p0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$4;-><init>(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;)V

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/media/MediaRouter$RouteInfo;->sendControlRequest(Landroid/content/Intent;Landroid/support/v7/media/MediaRouter$ControlRequestCallback;)V

    goto :goto_0
.end method

.method private requestRemoteSetVolumeOnMainThread(F)V
    .locals 5
    .param p1, "volume"    # F

    .prologue
    .line 989
    invoke-direct {p0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->getSelectedRouteOnMainThread()Landroid/support/v7/media/MediaRouter$RouteInfo;

    move-result-object v2

    .line 991
    .local v2, "route":Landroid/support/v7/media/MediaRouter$RouteInfo;
    if-nez v2, :cond_1

    .line 1000
    :cond_0
    :goto_0
    return-void

    .line 995
    :cond_1
    invoke-virtual {v2}, Landroid/support/v7/media/MediaRouter$RouteInfo;->getVolumeHandling()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 996
    invoke-virtual {v2}, Landroid/support/v7/media/MediaRouter$RouteInfo;->getVolumeMax()I

    move-result v1

    .line 997
    .local v1, "remoteVolume":I
    float-to-int v3, p1

    mul-int v0, v3, v1

    .line 998
    .local v0, "newRemoteVolume":I
    invoke-direct {p0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->getSelectedRouteOnMainThread()Landroid/support/v7/media/MediaRouter$RouteInfo;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/support/v7/media/MediaRouter$RouteInfo;->requestSetVolume(I)V

    goto :goto_0
.end method

.method private requestRemoveOnMainThread()V
    .locals 6

    .prologue
    .line 882
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.media.intent.action.REMOVE"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 883
    .local v0, "removeIntent":Landroid/content/Intent;
    const-string v2, "android.media.intent.category.REMOTE_PLAYBACK"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 884
    invoke-direct {p0, v0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->populateSessionId(Landroid/content/Intent;)V

    .line 885
    const-string v2, "android.media.intent.extra.ITEM_ID"

    iget-object v3, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mItemId:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 887
    invoke-direct {p0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->getSelectedRouteOnMainThread()Landroid/support/v7/media/MediaRouter$RouteInfo;

    move-result-object v1

    .line 889
    .local v1, "route":Landroid/support/v7/media/MediaRouter$RouteInfo;
    if-nez v1, :cond_0

    .line 914
    :goto_0
    return-void

    .line 893
    :cond_0
    invoke-direct {p0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->isLogVerbose()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 894
    const-string v2, "Sending intent=%s extras=%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v4, 0x1

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/music/utils/DebugUtils;->bundleToString(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->logd(Ljava/lang/String;)V

    .line 899
    :cond_1
    new-instance v2, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$5;

    invoke-direct {v2, p0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$5;-><init>(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;)V

    invoke-virtual {v1, v0, v2}, Landroid/support/v7/media/MediaRouter$RouteInfo;->sendControlRequest(Landroid/content/Intent;Landroid/support/v7/media/MediaRouter$ControlRequestCallback;)V

    goto :goto_0
.end method

.method private resetTheSessionId()V
    .locals 2

    .prologue
    .line 1635
    iget-object v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mSessionManager:Lcom/google/android/music/cast/CastSessionManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/music/cast/CastSessionManager;->setSessionId(Ljava/lang/String;)V

    .line 1636
    iget-object v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mNextPlayer:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    if-eqz v0, :cond_0

    .line 1637
    iget-object v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mNextPlayer:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->clearCurrentItem(Z)V

    .line 1639
    :cond_0
    return-void
.end method

.method private retryPlaybackRequest(J)V
    .locals 3
    .param p1, "delayMillis"    # J

    .prologue
    .line 1614
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->clearCurrentItem(Z)V

    .line 1615
    iget-object v1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    .line 1616
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0, p1, p2}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 1617
    return-void
.end method

.method private setState(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;)V
    .locals 5
    .param p1, "state"    # Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    .prologue
    .line 586
    invoke-direct {p0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->isLogVerbose()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 587
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setState: state="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->logv(Ljava/lang/String;)V

    .line 589
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mState:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    .line 590
    .local v1, "oldState":Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;
    iget-object v3, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mLock:Ljava/lang/Object;

    monitor-enter v3

    .line 591
    :try_start_0
    iput-object p1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mState:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    .line 592
    sget-object v2, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$8;->$SwitchMap$com$google$android$music$cast$RemoteAsyncMediaPlayer$State:[I

    invoke-virtual {p1}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->ordinal()I

    move-result v4

    aget v2, v2, v4

    packed-switch v2, :pswitch_data_0

    .line 612
    :goto_0
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 613
    iget-object v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mInternalStateCallback:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$InternalStateCallback;

    .line 614
    .local v0, "internallStateCallback":Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$InternalStateCallback;
    if-eqz v0, :cond_1

    .line 615
    invoke-interface {v0, v1, p1}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$InternalStateCallback;->onStateSet(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;)V

    .line 617
    :cond_1
    return-void

    .line 606
    .end local v0    # "internallStateCallback":Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$InternalStateCallback;
    :pswitch_0
    :try_start_1
    iget-object v2, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mPositionDeltaStopWatch:Lcom/google/android/music/playback/StopWatch;

    invoke-virtual {v2}, Lcom/google/android/music/playback/StopWatch;->pause()V

    goto :goto_0

    .line 612
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    .line 609
    :pswitch_1
    :try_start_2
    iget-object v2, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mPositionDeltaStopWatch:Lcom/google/android/music/playback/StopWatch;

    invoke-virtual {v2}, Lcom/google/android/music/playback/StopWatch;->start()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 592
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private toLowerCaseKeys(Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 5
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 1490
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 1491
    .local v2, "lowerCaseKeysBundle":Landroid/os/Bundle;
    invoke-virtual {p1}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1492
    .local v1, "key":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1494
    .end local v1    # "key":Ljava/lang/String;
    :cond_0
    return-object v2
.end method

.method private updatePosition(J)V
    .locals 3
    .param p1, "millis"    # J

    .prologue
    .line 579
    iget-object v1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 580
    :try_start_0
    iput-wide p1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mKnownPositionMillis:J

    .line 581
    iget-object v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mPositionDeltaStopWatch:Lcom/google/android/music/playback/StopWatch;

    invoke-virtual {v0}, Lcom/google/android/music/playback/StopWatch;->reset()V

    .line 582
    monitor-exit v1

    .line 583
    return-void

    .line 582
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public duration()J
    .locals 4

    .prologue
    .line 1433
    iget-object v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mMusicFile:Lcom/google/android/music/store/MusicFile;

    .line 1434
    .local v0, "musicFile":Lcom/google/android/music/store/MusicFile;
    if-eqz v0, :cond_0

    .line 1435
    invoke-virtual {v0}, Lcom/google/android/music/store/MusicFile;->getDurationInMilliSec()J

    move-result-wide v2

    .line 1437
    :goto_0
    return-wide v2

    :cond_0
    const-wide/16 v2, -0x1

    goto :goto_0
.end method

.method public getAudioSessionId()I
    .locals 1

    .prologue
    .line 1411
    iget v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mAudioSessionId:I

    return v0
.end method

.method public getErrorType()I
    .locals 1

    .prologue
    .line 1405
    iget v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mDownloadError:I

    return v0
.end method

.method public getRemoteSongId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1211
    iget-object v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mMusicFile:Lcom/google/android/music/store/MusicFile;

    .line 1212
    .local v0, "musicFile":Lcom/google/android/music/store/MusicFile;
    if-eqz v0, :cond_0

    .line 1213
    invoke-virtual {v0}, Lcom/google/android/music/store/MusicFile;->getSourceId()Ljava/lang/String;

    move-result-object v1

    .line 1215
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isInErrorState()Z
    .locals 1

    .prologue
    .line 1400
    iget-object v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mState:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    invoke-virtual {v0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->isError()Z

    move-result v0

    return v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 1234
    iget-object v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mState:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    invoke-virtual {v0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public isPlaying()Z
    .locals 1

    .prologue
    .line 1384
    iget-object v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mState:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    invoke-virtual {v0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->isPlaying()Z

    move-result v0

    return v0
.end method

.method public isPreparing()Z
    .locals 1

    .prologue
    .line 1390
    iget-object v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mState:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    invoke-virtual {v0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->isWaitingForPlayback()Z

    move-result v0

    return v0
.end method

.method public isReleased()Z
    .locals 1

    .prologue
    .line 1379
    iget-object v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mState:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    invoke-virtual {v0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->isReleased()Z

    move-result v0

    return v0
.end method

.method public isRenderingAudioLocally()Z
    .locals 1

    .prologue
    .line 1474
    const/4 v0, 0x0

    return v0
.end method

.method public isStreaming()Z
    .locals 1

    .prologue
    .line 1395
    iget-object v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mState:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    invoke-virtual {v0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->isPlaying()Z

    move-result v0

    return v0
.end method

.method public pause()V
    .locals 4

    .prologue
    .line 1299
    iget-object v1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mState:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    invoke-virtual {v1}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->canBePlayed()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mState:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    invoke-virtual {v1}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mItemId:Ljava/lang/String;

    if-nez v1, :cond_2

    .line 1300
    :cond_1
    const-string v1, "Not playable -- attempting to pause anyway."

    invoke-direct {p0, v1}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->logw(Ljava/lang/String;)V

    .line 1303
    :cond_2
    iget-object v1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mUrl:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 1304
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1305
    return-void
.end method

.method public position()J
    .locals 8

    .prologue
    .line 1421
    iget-wide v4, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mKnownPositionMillis:J

    iget-object v1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mPositionDeltaStopWatch:Lcom/google/android/music/playback/StopWatch;

    invoke-virtual {v1}, Lcom/google/android/music/playback/StopWatch;->getTime()J

    move-result-wide v6

    add-long v2, v4, v6

    .line 1423
    .local v2, "position":J
    iget-object v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mMusicFile:Lcom/google/android/music/store/MusicFile;

    .line 1424
    .local v0, "musicFile":Lcom/google/android/music/store/MusicFile;
    if-eqz v0, :cond_0

    .line 1425
    invoke-virtual {v0}, Lcom/google/android/music/store/MusicFile;->getDurationInMilliSec()J

    move-result-wide v4

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    .line 1427
    .end local v2    # "position":J
    :cond_0
    return-wide v2
.end method

.method public release()V
    .locals 3

    .prologue
    .line 1330
    invoke-direct {p0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->isLogVerbose()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1331
    const-string v1, "release"

    invoke-direct {p0, v1}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->logd(Ljava/lang/String;)V

    .line 1334
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->isReleased()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1354
    :goto_0
    return-void

    .line 1337
    :cond_1
    invoke-direct {p0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->isLogVerbose()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1338
    const-string v1, "Releasing"

    invoke-direct {p0, v1}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->logd(Ljava/lang/String;)V

    .line 1341
    :cond_2
    iget-boolean v1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mIsPlaybackRequested:Z

    if-eqz v1, :cond_3

    iget-boolean v1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mIsCurrent:Z

    if-nez v1, :cond_3

    .line 1342
    iget-object v1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mHandler:Landroid/os/Handler;

    const/16 v2, 0xb

    invoke-static {v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    .line 1343
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1346
    .end local v0    # "msg":Landroid/os/Message;
    :cond_3
    iget-object v1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mItemStatusReceiver:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$ItemStatusReceiver;

    if-eqz v1, :cond_4

    .line 1347
    iget-object v1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mItemStatusReceiver:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$ItemStatusReceiver;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1348
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mItemStatusReceiver:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$ItemStatusReceiver;

    .line 1350
    :cond_4
    invoke-direct {p0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->notifyIfSongPlayed()V

    .line 1351
    invoke-static {p0}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    .line 1352
    sget-object v1, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->RELEASED:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    invoke-direct {p0, v1}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->setState(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;)V

    .line 1353
    iget-object v1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mRouteChecker:Lcom/google/android/music/utils/RouteChecker;

    invoke-virtual {v1}, Lcom/google/android/music/utils/RouteChecker;->onDestroy()V

    goto :goto_0
.end method

.method public seek(J)J
    .locals 5
    .param p1, "position"    # J

    .prologue
    .line 1443
    invoke-direct {p0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->isLogVerbose()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1444
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Seek to position "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " requested."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->logv(Ljava/lang/String;)V

    .line 1447
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mState:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    invoke-virtual {v1}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->canBePlayed()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mState:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    invoke-virtual {v1}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1448
    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->updatePosition(J)V

    .line 1452
    iget-object v1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mState:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    invoke-virtual {v1}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1453
    sget-object v1, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->PAUSED:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    invoke-direct {p0, v1}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->setState(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;)V

    .line 1454
    invoke-direct {p0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->notifyTrackPlayStateChanged()V

    .line 1457
    :cond_2
    iget-boolean v1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mIsPlaybackRequested:Z

    if-eqz v1, :cond_3

    .line 1458
    iget-object v1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x8

    invoke-static {v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    .line 1459
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1463
    .end local v0    # "msg":Landroid/os/Message;
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->position()J

    move-result-wide v2

    return-wide v2
.end method

.method public setAsCurrentPlayer()V
    .locals 1

    .prologue
    .line 1201
    invoke-direct {p0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->isLogVerbose()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1202
    const-string v0, "Setting this player as the current one."

    invoke-direct {p0, v0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->logv(Ljava/lang/String;)V

    .line 1204
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mIsCurrent:Z

    .line 1205
    return-void
.end method

.method public setAudioSessionId(I)V
    .locals 0
    .param p1, "fxsession"    # I

    .prologue
    .line 1416
    iput p1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mAudioSessionId:I

    .line 1417
    return-void
.end method

.method public setDataSource(Lcom/google/android/music/download/ContentIdentifier;JJZZLcom/google/android/music/store/MusicFile;Lcom/google/android/music/store/ContainerDescriptor;Lcom/google/android/music/playback/AsyncMediaPlayer$AsyncCommandCallback;)V
    .locals 10
    .param p1, "songId"    # Lcom/google/android/music/download/ContentIdentifier;
    .param p2, "positionMs"    # J
    .param p4, "durationMs"    # J
    .param p6, "isCurrentPlayer"    # Z
    .param p7, "fromUserAction"    # Z
    .param p8, "musicFile"    # Lcom/google/android/music/store/MusicFile;
    .param p9, "containerDescriptor"    # Lcom/google/android/music/store/ContainerDescriptor;
    .param p10, "cb"    # Lcom/google/android/music/playback/AsyncMediaPlayer$AsyncCommandCallback;

    .prologue
    .line 1097
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/music/download/ContentIdentifier;->isSharedDomain()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1098
    const/16 v6, 0xf

    iput v6, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mDownloadError:I

    .line 1099
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Shared track is not playable on remote device: songId="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->logd(Ljava/lang/String;)V

    .line 1100
    const/4 v6, 0x0

    move-object/from16 v0, p10

    invoke-interface {v0, v6}, Lcom/google/android/music/playback/AsyncMediaPlayer$AsyncCommandCallback;->onFailure(Z)V

    .line 1181
    :goto_0
    return-void

    .line 1105
    :cond_0
    if-eqz p1, :cond_1

    if-nez p8, :cond_2

    .line 1106
    :cond_1
    const-string v6, "Invalid arguments: songId=%s musicFile=%s"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object p1, v7, v8

    const/4 v8, 0x1

    aput-object p8, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->logw(Ljava/lang/String;)V

    .line 1107
    const/4 v6, 0x1

    move-object/from16 v0, p10

    invoke-interface {v0, v6}, Lcom/google/android/music/playback/AsyncMediaPlayer$AsyncCommandCallback;->onFailure(Z)V

    goto :goto_0

    .line 1112
    :cond_2
    invoke-virtual {p1}, Lcom/google/android/music/download/ContentIdentifier;->isCacheable()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-virtual/range {p8 .. p8}, Lcom/google/android/music/store/MusicFile;->getIsCloudFile()Z

    move-result v6

    if-nez v6, :cond_4

    .line 1113
    const/16 v6, 0xe

    iput v6, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mDownloadError:I

    .line 1114
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Sideloaded track is not playable on remote device: songId="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->logd(Ljava/lang/String;)V

    .line 1115
    if-eqz p7, :cond_3

    .line 1116
    const/4 v6, 0x0

    move-object/from16 v0, p10

    invoke-interface {v0, v6}, Lcom/google/android/music/playback/AsyncMediaPlayer$AsyncCommandCallback;->onFailure(Z)V

    goto :goto_0

    .line 1118
    :cond_3
    const/4 v6, 0x1

    move-object/from16 v0, p10

    invoke-interface {v0, v6}, Lcom/google/android/music/playback/AsyncMediaPlayer$AsyncCommandCallback;->onFailure(Z)V

    goto :goto_0

    .line 1125
    :cond_4
    invoke-virtual {p1}, Lcom/google/android/music/download/ContentIdentifier;->isCacheable()Z

    move-result v6

    if-eqz v6, :cond_5

    invoke-virtual/range {p8 .. p8}, Lcom/google/android/music/store/MusicFile;->getIsCloudFile()Z

    move-result v6

    if-nez v6, :cond_6

    .line 1126
    :cond_5
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Track is not playable on remote device: songId="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->logw(Ljava/lang/String;)V

    .line 1127
    const/4 v6, 0x1

    move-object/from16 v0, p10

    invoke-interface {v0, v6}, Lcom/google/android/music/playback/AsyncMediaPlayer$AsyncCommandCallback;->onFailure(Z)V

    goto :goto_0

    .line 1131
    :cond_6
    iput-object p1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mCurrentSongId:Lcom/google/android/music/download/ContentIdentifier;

    .line 1132
    invoke-direct {p0, p2, p3}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->updatePosition(J)V

    .line 1133
    move-object/from16 v0, p10

    iput-object v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mCallback:Lcom/google/android/music/playback/AsyncMediaPlayer$AsyncCommandCallback;

    .line 1134
    move/from16 v0, p7

    iput-boolean v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mFromUserAction:Z

    .line 1135
    move/from16 v0, p6

    iput-boolean v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mIsCurrent:Z

    .line 1136
    move-object/from16 v0, p9

    iput-object v0, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    .line 1138
    invoke-direct {p0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->isLogVerbose()Z

    move-result v6

    if-eqz v6, :cond_7

    .line 1139
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "setDataSource: duration="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->logd(Ljava/lang/String;)V

    .line 1143
    :cond_7
    iget-object v6, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mContext:Landroid/content/Context;

    invoke-static {v6}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v5

    .line 1144
    .local v5, "store":Lcom/google/android/music/store/Store;
    invoke-virtual {v5}, Lcom/google/android/music/store/Store;->beginRead()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 1145
    .local v2, "db":Landroid/database/sqlite/SQLiteDatabase;
    new-instance v6, Lcom/google/android/music/store/MusicFile;

    invoke-direct {v6}, Lcom/google/android/music/store/MusicFile;-><init>()V

    iput-object v6, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mMusicFile:Lcom/google/android/music/store/MusicFile;

    .line 1148
    :try_start_0
    iget-object v6, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mMusicFile:Lcom/google/android/music/store/MusicFile;

    invoke-virtual/range {p8 .. p8}, Lcom/google/android/music/store/MusicFile;->getLocalId()J

    move-result-wide v8

    invoke-virtual {v6, v2, v8, v9}, Lcom/google/android/music/store/MusicFile;->load(Landroid/database/sqlite/SQLiteDatabase;J)V
    :try_end_0
    .catch Lcom/google/android/music/store/DataNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1154
    invoke-virtual {v5, v2}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1157
    iget-object v6, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mContext:Landroid/content/Context;

    iget-object v7, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mMusicFile:Lcom/google/android/music/store/MusicFile;

    move/from16 v0, p7

    invoke-static {v6, v0, v7}, Lcom/google/android/music/cast/CastUtils;->generateMplayUrl(Landroid/content/Context;ZLcom/google/android/music/store/MusicFile;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mUrl:Ljava/lang/String;

    .line 1158
    iget-object v6, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mUrl:Ljava/lang/String;

    if-nez v6, :cond_8

    .line 1159
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Failed to generate URL for songId="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->logw(Ljava/lang/String;)V

    .line 1160
    const/4 v6, 0x1

    move-object/from16 v0, p10

    invoke-interface {v0, v6}, Lcom/google/android/music/playback/AsyncMediaPlayer$AsyncCommandCallback;->onFailure(Z)V

    goto/16 :goto_0

    .line 1149
    :catch_0
    move-exception v3

    .line 1150
    .local v3, "e":Lcom/google/android/music/store/DataNotFoundException;
    :try_start_1
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Failed to load MusicFile for songId="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6, v3}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->logw(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1151
    const/4 v6, 0x1

    move-object/from16 v0, p10

    invoke-interface {v0, v6}, Lcom/google/android/music/playback/AsyncMediaPlayer$AsyncCommandCallback;->onFailure(Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1154
    invoke-virtual {v5, v2}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    goto/16 :goto_0

    .end local v3    # "e":Lcom/google/android/music/store/DataNotFoundException;
    :catchall_0
    move-exception v6

    invoke-virtual {v5, v2}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    throw v6

    .line 1164
    :cond_8
    sget-object v6, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->DATA_SOURCE_SET:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    invoke-direct {p0, v6}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->setState(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;)V

    .line 1166
    invoke-direct {p0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->isLogVerbose()Z

    move-result v6

    if-eqz v6, :cond_9

    .line 1167
    const-string v6, "Setting data source.  Will play %s mPrequeueItems=%s mSessionId=%s"

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    iget-object v9, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mUrl:Ljava/lang/String;

    aput-object v9, v7, v8

    const/4 v8, 0x1

    iget-boolean v9, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mPrequeueItems:Z

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x2

    iget-object v9, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mSessionManager:Lcom/google/android/music/cast/CastSessionManager;

    invoke-virtual {v9}, Lcom/google/android/music/cast/CastSessionManager;->getSessionId()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->logv(Ljava/lang/String;)V

    .line 1172
    :cond_9
    iget-boolean v6, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mIsCurrent:Z

    if-nez v6, :cond_b

    iget-boolean v6, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mPrequeueItems:Z

    if-eqz v6, :cond_b

    iget-object v6, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mSessionManager:Lcom/google/android/music/cast/CastSessionManager;

    invoke-virtual {v6}, Lcom/google/android/music/cast/CastSessionManager;->getSessionId()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_b

    .line 1173
    invoke-direct {p0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->isLogVerbose()Z

    move-result v6

    if-eqz v6, :cond_a

    .line 1174
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Requesting queued playback for "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mUrl:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->logv(Ljava/lang/String;)V

    .line 1176
    :cond_a
    iget-object v6, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mHandler:Landroid/os/Handler;

    const/4 v7, 0x3

    invoke-static {v6, v7}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v4

    .line 1177
    .local v4, "msg":Landroid/os/Message;
    iget-object v6, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mHandler:Landroid/os/Handler;

    invoke-virtual {v6, v4}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1180
    .end local v4    # "msg":Landroid/os/Message;
    :cond_b
    invoke-interface/range {p10 .. p10}, Lcom/google/android/music/playback/AsyncMediaPlayer$AsyncCommandCallback;->onSuccess()V

    goto/16 :goto_0
.end method

.method public setNextPlayer(Lcom/google/android/music/playback/AsyncMediaPlayer;)V
    .locals 3
    .param p1, "player"    # Lcom/google/android/music/playback/AsyncMediaPlayer;

    .prologue
    .line 1185
    instance-of v1, p1, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    if-eqz v1, :cond_1

    .line 1186
    check-cast p1, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    .end local p1    # "player":Lcom/google/android/music/playback/AsyncMediaPlayer;
    iput-object p1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mNextPlayer:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    .line 1188
    invoke-direct {p0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->isLogVerbose()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1189
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Next player set ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mNextPlayer:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;

    iget-object v2, v2, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mItemStatusCategory:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->logv(Ljava/lang/String;)V

    .line 1192
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mPrequeueItems:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mSessionManager:Lcom/google/android/music/cast/CastSessionManager;

    invoke-virtual {v1}, Lcom/google/android/music/cast/CastSessionManager;->getSessionId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1193
    iget-object v1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mHandler:Landroid/os/Handler;

    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 1194
    .local v0, "message":Landroid/os/Message;
    iget-object v1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1197
    .end local v0    # "message":Landroid/os/Message;
    :cond_1
    return-void
.end method

.method public setVolume(F)V
    .locals 4
    .param p1, "vol"    # F

    .prologue
    .line 1468
    iget-object v1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x9

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 1469
    .local v0, "message":Landroid/os/Message;
    iget-object v1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1470
    return-void
.end method

.method public start()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 1239
    invoke-direct {p0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->isLogVerbose()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1240
    const-string v2, "start"

    invoke-direct {p0, v2}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->logd(Ljava/lang/String;)V

    .line 1243
    :cond_0
    iget-object v2, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mState:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    invoke-virtual {v2}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->isPlaying()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1244
    const-string v2, "Ignoring request to start playback -- already playing."

    invoke-direct {p0, v2}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->logw(Ljava/lang/String;)V

    .line 1295
    :goto_0
    return-void

    .line 1248
    :cond_1
    iget-object v2, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mState:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    invoke-virtual {v2}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->isPaused()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1249
    iget-object v2, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mHandler:Landroid/os/Handler;

    const/4 v3, 0x5

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 1250
    .local v0, "message":Landroid/os/Message;
    iget-object v2, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 1254
    .end local v0    # "message":Landroid/os/Message;
    :cond_2
    iget-object v2, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mState:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    invoke-virtual {v2}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->canBePlayed()Z

    move-result v2

    if-nez v2, :cond_4

    .line 1255
    const-string v2, "Attempting to start canceled or unplayable item.Clearing state and starting over."

    invoke-direct {p0, v2}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->logw(Ljava/lang/String;)V

    .line 1257
    sget-object v2, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->NONE:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    invoke-direct {p0, v2}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->setState(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;)V

    .line 1258
    iget-object v2, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mItemStatusReceiver:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$ItemStatusReceiver;

    if-eqz v2, :cond_3

    .line 1259
    iget-object v2, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mItemStatusReceiver:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$ItemStatusReceiver;

    invoke-virtual {v2, v3}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1260
    iput-object v5, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mItemStatusReceiver:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$ItemStatusReceiver;

    .line 1262
    :cond_3
    iput-object v5, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mItemId:Ljava/lang/String;

    .line 1263
    const-wide/16 v2, 0x0

    invoke-direct {p0, v2, v3}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->updatePosition(J)V

    .line 1267
    :cond_4
    invoke-direct {p0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->notifyOpenStarted()V

    .line 1268
    iget-object v2, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mMusicFile:Lcom/google/android/music/store/MusicFile;

    if-nez v2, :cond_5

    .line 1269
    const-string v2, "No data source set!"

    invoke-direct {p0, v2}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->logw(Ljava/lang/String;)V

    .line 1270
    sget-object v2, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->ERROR:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    invoke-direct {p0, v2}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->setState(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;)V

    .line 1271
    invoke-direct {p0, v4}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->notifyFailure(Z)V

    goto :goto_0

    .line 1274
    :cond_5
    iget-object v2, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mUrl:Ljava/lang/String;

    if-nez v2, :cond_6

    .line 1275
    const-string v2, "Could not generate url for remote playback."

    invoke-direct {p0, v2}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->logw(Ljava/lang/String;)V

    .line 1276
    sget-object v2, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->ERROR:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    invoke-direct {p0, v2}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->setState(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;)V

    .line 1277
    invoke-direct {p0, v4}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->notifyFailure(Z)V

    goto :goto_0

    .line 1282
    :cond_6
    iget-object v2, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mItemId:Ljava/lang/String;

    if-nez v2, :cond_8

    .line 1283
    invoke-direct {p0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->isLogVerbose()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 1284
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Requesting remote playback of "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mUrl:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->logd(Ljava/lang/String;)V

    .line 1286
    :cond_7
    iget-object v2, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mHandler:Landroid/os/Handler;

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v1

    .line 1287
    .local v1, "msg":Landroid/os/Message;
    iget-object v2, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 1289
    .end local v1    # "msg":Landroid/os/Message;
    :cond_8
    iget-object v2, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mState:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    invoke-virtual {v2}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->isPlaying()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 1290
    const-string v2, "start() request ignored -- already playing."

    invoke-direct {p0, v2}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->logd(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1292
    :cond_9
    const-string v2, "start() request ignored -- already queued."

    invoke-direct {p0, v2}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->logd(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public stop()V
    .locals 4

    .prologue
    .line 1309
    invoke-direct {p0}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->isLogVerbose()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1310
    const-string v1, "stop"

    invoke-direct {p0, v1}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->logd(Ljava/lang/String;)V

    .line 1313
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mItemId:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 1314
    const-string v1, "Cannot stop -- have no queue id or have no item id."

    invoke-direct {p0, v1}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->logw(Ljava/lang/String;)V

    .line 1326
    :goto_0
    return-void

    .line 1317
    :cond_1
    iget-object v1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mState:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    invoke-virtual {v1}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->canBePlayed()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mState:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    invoke-virtual {v1}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->isPlaying()Z

    move-result v1

    if-nez v1, :cond_2

    .line 1318
    const-string v1, "Cannot stop -- not playable."

    invoke-direct {p0, v1}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->logw(Ljava/lang/String;)V

    goto :goto_0

    .line 1322
    :cond_2
    sget-object v1, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;->CANCELED:Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;

    invoke-direct {p0, v1}, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->setState(Lcom/google/android/music/cast/RemoteAsyncMediaPlayer$State;)V

    .line 1324
    iget-object v1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mUrl:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 1325
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/google/android/music/cast/RemoteAsyncMediaPlayer;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method
