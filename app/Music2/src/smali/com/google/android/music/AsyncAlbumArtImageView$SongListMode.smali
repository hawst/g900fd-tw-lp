.class Lcom/google/android/music/AsyncAlbumArtImageView$SongListMode;
.super Lcom/google/android/music/AsyncAlbumArtImageView$Mode;
.source "AsyncAlbumArtImageView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/AsyncAlbumArtImageView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SongListMode"
.end annotation


# instance fields
.field public songlist:Lcom/google/android/music/medialist/SongList;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 652
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/music/AsyncAlbumArtImageView$Mode;-><init>(Lcom/google/android/music/AsyncAlbumArtImageView$1;)V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/music/AsyncAlbumArtImageView$SongListMode;)V
    .locals 1
    .param p1, "other"    # Lcom/google/android/music/AsyncAlbumArtImageView$SongListMode;

    .prologue
    .line 654
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/music/AsyncAlbumArtImageView$Mode;-><init>(Lcom/google/android/music/AsyncAlbumArtImageView$1;)V

    .line 655
    iget-object v0, p1, Lcom/google/android/music/AsyncAlbumArtImageView$SongListMode;->songlist:Lcom/google/android/music/medialist/SongList;

    iput-object v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView$SongListMode;->songlist:Lcom/google/android/music/medialist/SongList;

    .line 656
    return-void
.end method


# virtual methods
.method public copy()Lcom/google/android/music/AsyncAlbumArtImageView$Mode;
    .locals 1

    .prologue
    .line 700
    new-instance v0, Lcom/google/android/music/AsyncAlbumArtImageView$SongListMode;

    invoke-direct {v0, p0}, Lcom/google/android/music/AsyncAlbumArtImageView$SongListMode;-><init>(Lcom/google/android/music/AsyncAlbumArtImageView$SongListMode;)V

    return-object v0
.end method

.method public createBitmap(Landroid/content/Context;IILcom/google/android/music/download/artwork/AlbumIdSink;Lcom/google/android/music/download/artwork/RemoteUrlSink;)Landroid/graphics/Bitmap;
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "missingAlbumSink"    # Lcom/google/android/music/download/artwork/AlbumIdSink;
    .param p5, "missingUrlSink"    # Lcom/google/android/music/download/artwork/RemoteUrlSink;

    .prologue
    .line 672
    iget-object v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView$SongListMode;->songlist:Lcom/google/android/music/medialist/SongList;

    instance-of v0, v0, Lcom/google/android/music/medialist/ExternalSongList;

    if-eqz v0, :cond_0

    .line 673
    iget-object v8, p0, Lcom/google/android/music/AsyncAlbumArtImageView$SongListMode;->songlist:Lcom/google/android/music/medialist/SongList;

    check-cast v8, Lcom/google/android/music/medialist/ExternalSongList;

    .line 674
    .local v8, "externalSongList":Lcom/google/android/music/medialist/ExternalSongList;
    invoke-virtual {v8, p1}, Lcom/google/android/music/medialist/ExternalSongList;->getAlbumArtUrl(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 675
    .local v1, "url":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 676
    invoke-virtual {p0}, Lcom/google/android/music/AsyncAlbumArtImageView$SongListMode;->cropToSquare()Z

    move-result v5

    move-object v0, p1

    move v2, p2

    move v3, p3

    move-object v4, p5

    # invokes: Lcom/google/android/music/AsyncAlbumArtImageView;->getBitmapForRemoteUrlOrDefault(Landroid/content/Context;Ljava/lang/String;IILcom/google/android/music/download/artwork/RemoteUrlSink;Z)Landroid/graphics/Bitmap;
    invoke-static/range {v0 .. v5}, Lcom/google/android/music/AsyncAlbumArtImageView;->access$100(Landroid/content/Context;Ljava/lang/String;IILcom/google/android/music/download/artwork/RemoteUrlSink;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 681
    .end local v1    # "url":Ljava/lang/String;
    .end local v8    # "externalSongList":Lcom/google/android/music/medialist/ExternalSongList;
    :goto_0
    return-object v0

    :cond_0
    iget-object v2, p0, Lcom/google/android/music/AsyncAlbumArtImageView$SongListMode;->songlist:Lcom/google/android/music/medialist/SongList;

    const/4 v7, 0x0

    move-object v3, p1

    move v4, p2

    move v5, p3

    move-object v6, p4

    invoke-virtual/range {v2 .. v7}, Lcom/google/android/music/medialist/SongList;->getImage(Landroid/content/Context;IILcom/google/android/music/download/artwork/AlbumIdSink;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    .line 691
    instance-of v1, p1, Lcom/google/android/music/AsyncAlbumArtImageView$SongListMode;

    if-nez v1, :cond_0

    .line 692
    const/4 v1, 0x0

    .line 695
    :goto_0
    return v1

    :cond_0
    move-object v0, p1

    .line 694
    check-cast v0, Lcom/google/android/music/AsyncAlbumArtImageView$SongListMode;

    .line 695
    .local v0, "o":Lcom/google/android/music/AsyncAlbumArtImageView$SongListMode;
    iget-object v1, p0, Lcom/google/android/music/AsyncAlbumArtImageView$SongListMode;->songlist:Lcom/google/android/music/medialist/SongList;

    iget-object v2, v0, Lcom/google/android/music/AsyncAlbumArtImageView$SongListMode;->songlist:Lcom/google/android/music/medialist/SongList;

    invoke-virtual {v1, v2}, Lcom/google/android/music/medialist/SongList;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public getExternalUrl(Landroid/content/Context;)Ljava/lang/String;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 660
    iget-object v1, p0, Lcom/google/android/music/AsyncAlbumArtImageView$SongListMode;->songlist:Lcom/google/android/music/medialist/SongList;

    instance-of v1, v1, Lcom/google/android/music/medialist/ExternalSongList;

    if-eqz v1, :cond_0

    .line 661
    iget-object v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView$SongListMode;->songlist:Lcom/google/android/music/medialist/SongList;

    check-cast v0, Lcom/google/android/music/medialist/ExternalSongList;

    .line 662
    .local v0, "externalSongList":Lcom/google/android/music/medialist/ExternalSongList;
    invoke-virtual {v0, p1}, Lcom/google/android/music/medialist/ExternalSongList;->getAlbumArtUrl(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 664
    .end local v0    # "externalSongList":Lcom/google/android/music/medialist/ExternalSongList;
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 686
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SongListMode<"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/AsyncAlbumArtImageView$SongListMode;->songlist:Lcom/google/android/music/medialist/SongList;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
