.class public Lcom/google/android/music/download/keepon/KeeponSchedulingServiceConnection;
.super Lcom/google/android/music/utils/SafeServiceConnection;
.source "KeeponSchedulingServiceConnection.java"


# instance fields
.field private volatile mKeeponCallbackManager:Lcom/google/android/music/download/keepon/IKeeponCallbackManager;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/google/android/music/utils/SafeServiceConnection;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 1
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 18
    invoke-static {p2}, Lcom/google/android/music/download/keepon/IKeeponCallbackManager$Stub;->asInterface(Landroid/os/IBinder;)Lcom/google/android/music/download/keepon/IKeeponCallbackManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/download/keepon/KeeponSchedulingServiceConnection;->mKeeponCallbackManager:Lcom/google/android/music/download/keepon/IKeeponCallbackManager;

    .line 20
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 1
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 23
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/download/keepon/KeeponSchedulingServiceConnection;->mKeeponCallbackManager:Lcom/google/android/music/download/keepon/IKeeponCallbackManager;

    .line 24
    return-void
.end method
