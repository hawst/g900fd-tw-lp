.class Lcom/google/android/music/quiz/ArtistQuizActivity$FinishQuizOnClickListener;
.super Ljava/lang/Object;
.source "ArtistQuizActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/quiz/ArtistQuizActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FinishQuizOnClickListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/quiz/ArtistQuizActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/music/quiz/ArtistQuizActivity;)V
    .locals 0

    .prologue
    .line 340
    iput-object p1, p0, Lcom/google/android/music/quiz/ArtistQuizActivity$FinishQuizOnClickListener;->this$0:Lcom/google/android/music/quiz/ArtistQuizActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/music/quiz/ArtistQuizActivity;Lcom/google/android/music/quiz/ArtistQuizActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/music/quiz/ArtistQuizActivity;
    .param p2, "x1"    # Lcom/google/android/music/quiz/ArtistQuizActivity$1;

    .prologue
    .line 340
    invoke-direct {p0, p1}, Lcom/google/android/music/quiz/ArtistQuizActivity$FinishQuizOnClickListener;-><init>(Lcom/google/android/music/quiz/ArtistQuizActivity;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v1, 0x0

    .line 344
    iget-object v0, p0, Lcom/google/android/music/quiz/ArtistQuizActivity$FinishQuizOnClickListener;->this$0:Lcom/google/android/music/quiz/ArtistQuizActivity;

    # setter for: Lcom/google/android/music/quiz/ArtistQuizActivity;->mShowMoreClicked:Z
    invoke-static {v0, v1}, Lcom/google/android/music/quiz/ArtistQuizActivity;->access$1002(Lcom/google/android/music/quiz/ArtistQuizActivity;Z)Z

    .line 345
    iget-object v0, p0, Lcom/google/android/music/quiz/ArtistQuizActivity$FinishQuizOnClickListener;->this$0:Lcom/google/android/music/quiz/ArtistQuizActivity;

    invoke-virtual {v0, v1}, Lcom/google/android/music/quiz/ArtistQuizActivity;->setLoadingProgressVisibility(I)V

    .line 346
    iget-object v0, p0, Lcom/google/android/music/quiz/ArtistQuizActivity$FinishQuizOnClickListener;->this$0:Lcom/google/android/music/quiz/ArtistQuizActivity;

    invoke-virtual {v0}, Lcom/google/android/music/quiz/ArtistQuizActivity;->setPrioritizeRecommendations()V

    .line 347
    iget-object v0, p0, Lcom/google/android/music/quiz/ArtistQuizActivity$FinishQuizOnClickListener;->this$0:Lcom/google/android/music/quiz/ArtistQuizActivity;

    # invokes: Lcom/google/android/music/quiz/ArtistQuizActivity;->getUserQuizResults()V
    invoke-static {v0}, Lcom/google/android/music/quiz/ArtistQuizActivity;->access$1200(Lcom/google/android/music/quiz/ArtistQuizActivity;)V

    .line 348
    return-void
.end method
