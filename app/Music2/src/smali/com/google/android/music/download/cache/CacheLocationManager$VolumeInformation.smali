.class public final Lcom/google/android/music/download/cache/CacheLocationManager$VolumeInformation;
.super Ljava/lang/Object;
.source "CacheLocationManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/download/cache/CacheLocationManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "VolumeInformation"
.end annotation


# instance fields
.field private mFreeSpace:J

.field private mTotalSpace:J

.field private mVolumeId:Ljava/util/UUID;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 642
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$302(Lcom/google/android/music/download/cache/CacheLocationManager$VolumeInformation;J)J
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/download/cache/CacheLocationManager$VolumeInformation;
    .param p1, "x1"    # J

    .prologue
    .line 642
    iput-wide p1, p0, Lcom/google/android/music/download/cache/CacheLocationManager$VolumeInformation;->mFreeSpace:J

    return-wide p1
.end method

.method static synthetic access$402(Lcom/google/android/music/download/cache/CacheLocationManager$VolumeInformation;J)J
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/download/cache/CacheLocationManager$VolumeInformation;
    .param p1, "x1"    # J

    .prologue
    .line 642
    iput-wide p1, p0, Lcom/google/android/music/download/cache/CacheLocationManager$VolumeInformation;->mTotalSpace:J

    return-wide p1
.end method

.method static synthetic access$502(Lcom/google/android/music/download/cache/CacheLocationManager$VolumeInformation;Ljava/util/UUID;)Ljava/util/UUID;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/download/cache/CacheLocationManager$VolumeInformation;
    .param p1, "x1"    # Ljava/util/UUID;

    .prologue
    .line 642
    iput-object p1, p0, Lcom/google/android/music/download/cache/CacheLocationManager$VolumeInformation;->mVolumeId:Ljava/util/UUID;

    return-object p1
.end method


# virtual methods
.method public getFreeSpace()J
    .locals 2

    .prologue
    .line 660
    iget-wide v0, p0, Lcom/google/android/music/download/cache/CacheLocationManager$VolumeInformation;->mFreeSpace:J

    return-wide v0
.end method

.method public getFreeSpaceInMegaBytes()J
    .locals 3

    .prologue
    .line 667
    iget-wide v0, p0, Lcom/google/android/music/download/cache/CacheLocationManager$VolumeInformation;->mFreeSpace:J

    const/16 v2, 0x14

    shr-long/2addr v0, v2

    return-wide v0
.end method

.method public getTotalSpace()J
    .locals 2

    .prologue
    .line 674
    iget-wide v0, p0, Lcom/google/android/music/download/cache/CacheLocationManager$VolumeInformation;->mTotalSpace:J

    return-wide v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 649
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "VolumeInformation{mFreeSpace="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/music/download/cache/CacheLocationManager$VolumeInformation;->mFreeSpace:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mTotalSpace="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/music/download/cache/CacheLocationManager$VolumeInformation;->mTotalSpace:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mVolumeId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/download/cache/CacheLocationManager$VolumeInformation;->mVolumeId:Ljava/util/UUID;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
