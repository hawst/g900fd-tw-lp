.class Lcom/google/android/music/playback/MusicPlaybackService$DelayedStopHandler;
.super Landroid/os/Handler;
.source "MusicPlaybackService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/playback/MusicPlaybackService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DelayedStopHandler"
.end annotation


# instance fields
.field private final mServiceRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/google/android/music/playback/MusicPlaybackService;",
            ">;"
        }
    .end annotation
.end field

.field private volatile mStopped:Z


# direct methods
.method public constructor <init>(Lcom/google/android/music/playback/MusicPlaybackService;)V
    .locals 1
    .param p1, "service"    # Lcom/google/android/music/playback/MusicPlaybackService;

    .prologue
    .line 2242
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 2240
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$DelayedStopHandler;->mStopped:Z

    .line 2243
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$DelayedStopHandler;->mServiceRef:Ljava/lang/ref/WeakReference;

    .line 2244
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 2248
    iget-boolean v1, p0, Lcom/google/android/music/playback/MusicPlaybackService$DelayedStopHandler;->mStopped:Z

    if-eqz v1, :cond_1

    .line 2269
    :cond_0
    :goto_0
    return-void

    .line 2252
    :cond_1
    iget-object v1, p0, Lcom/google/android/music/playback/MusicPlaybackService$DelayedStopHandler;->mServiceRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/MusicPlaybackService;

    .line 2253
    .local v0, "service":Lcom/google/android/music/playback/MusicPlaybackService;
    if-eqz v0, :cond_0

    .line 2258
    # getter for: Lcom/google/android/music/playback/MusicPlaybackService;->mServiceInUse:Z
    invoke-static {v0}, Lcom/google/android/music/playback/MusicPlaybackService;->access$2200(Lcom/google/android/music/playback/MusicPlaybackService;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-boolean v1, v0, Lcom/google/android/music/playback/MusicPlaybackService;->mIsNotificationShowing:Z

    if-nez v1, :cond_0

    # getter for: Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentPlayback:Ljava/util/concurrent/atomic/AtomicReference;
    invoke-static {v0}, Lcom/google/android/music/playback/MusicPlaybackService;->access$200(Lcom/google/android/music/playback/MusicPlaybackService;)Ljava/util/concurrent/atomic/AtomicReference;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/playback/DevicePlayback;

    invoke-virtual {v1}, Lcom/google/android/music/playback/DevicePlayback;->getState()Lcom/google/android/music/playback/DevicePlayback$State;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/playback/DevicePlayback$State;->playingOrWillPlay()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2263
    # getter for: Lcom/google/android/music/playback/MusicPlaybackService;->mCurrentPlayback:Ljava/util/concurrent/atomic/AtomicReference;
    invoke-static {v0}, Lcom/google/android/music/playback/MusicPlaybackService;->access$200(Lcom/google/android/music/playback/MusicPlaybackService;)Ljava/util/concurrent/atomic/AtomicReference;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/playback/DevicePlayback;

    invoke-virtual {v1}, Lcom/google/android/music/playback/DevicePlayback;->saveState()V

    .line 2265
    # getter for: Lcom/google/android/music/playback/MusicPlaybackService;->mServiceStartId:I
    invoke-static {v0}, Lcom/google/android/music/playback/MusicPlaybackService;->access$2300(Lcom/google/android/music/playback/MusicPlaybackService;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/music/playback/MusicPlaybackService;->stopSelfResult(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2266
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/music/playback/MusicPlaybackService$DelayedStopHandler;->mStopped:Z

    .line 2267
    # invokes: Lcom/google/android/music/playback/MusicPlaybackService;->clearDelayedStops()V
    invoke-static {v0}, Lcom/google/android/music/playback/MusicPlaybackService;->access$2400(Lcom/google/android/music/playback/MusicPlaybackService;)V

    goto :goto_0
.end method

.method public isStopped()Z
    .locals 1

    .prologue
    .line 2272
    iget-boolean v0, p0, Lcom/google/android/music/playback/MusicPlaybackService$DelayedStopHandler;->mStopped:Z

    return v0
.end method
