.class Lcom/google/android/music/playback/LocalAsyncMediaPlayer$3;
.super Ljava/lang/Object;
.source "LocalAsyncMediaPlayer.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnErrorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/playback/LocalAsyncMediaPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/playback/LocalAsyncMediaPlayer;


# direct methods
.method constructor <init>(Lcom/google/android/music/playback/LocalAsyncMediaPlayer;)V
    .locals 0

    .prologue
    .line 897
    iput-object p1, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer$3;->this$0:Lcom/google/android/music/playback/LocalAsyncMediaPlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 5
    .param p1, "mp"    # Landroid/media/MediaPlayer;
    .param p2, "what"    # I
    .param p3, "extra"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 899
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "AsyncMediaPlayer("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer$3;->this$0:Lcom/google/android/music/playback/LocalAsyncMediaPlayer;

    # getter for: Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mCurrentSong:Lcom/google/android/music/download/ContentIdentifier;
    invoke-static {v4}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->access$600(Lcom/google/android/music/playback/LocalAsyncMediaPlayer;)Lcom/google/android/music/download/ContentIdentifier;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") OnError "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/music/utils/MusicUtils;->debugLog(Ljava/lang/Object;)V

    .line 901
    iget-object v3, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer$3;->this$0:Lcom/google/android/music/playback/LocalAsyncMediaPlayer;

    # getter for: Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mIsCurrentPlayer:Z
    invoke-static {v3}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->access$400(Lcom/google/android/music/playback/LocalAsyncMediaPlayer;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 902
    iget-object v3, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer$3;->this$0:Lcom/google/android/music/playback/LocalAsyncMediaPlayer;

    # getter for: Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mServiceHooks:Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;
    invoke-static {v3}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->access$500(Lcom/google/android/music/playback/LocalAsyncMediaPlayer;)Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;->notifyOpenComplete()V

    .line 903
    iget-object v3, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer$3;->this$0:Lcom/google/android/music/playback/LocalAsyncMediaPlayer;

    invoke-virtual {v3}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->isPlaying()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 905
    iget-object v3, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer$3;->this$0:Lcom/google/android/music/playback/LocalAsyncMediaPlayer;

    # invokes: Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->notifyFailure(Z)V
    invoke-static {v3, v2}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->access$700(Lcom/google/android/music/playback/LocalAsyncMediaPlayer;Z)V

    .line 913
    :cond_0
    iget-object v3, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer$3;->this$0:Lcom/google/android/music/playback/LocalAsyncMediaPlayer;

    const/16 v4, 0xe

    # invokes: Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->removeStatus(I)V
    invoke-static {v3, v4}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->access$200(Lcom/google/android/music/playback/LocalAsyncMediaPlayer;I)V

    .line 914
    iget-object v3, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer$3;->this$0:Lcom/google/android/music/playback/LocalAsyncMediaPlayer;

    invoke-virtual {v3}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->getAudioSessionId()I

    move-result v0

    .line 915
    .local v0, "audioSessionId":I
    iget-object v3, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer$3;->this$0:Lcom/google/android/music/playback/LocalAsyncMediaPlayer;

    # getter for: Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mMediaPlayer:Lcom/google/android/music/playback/CompatMediaPlayer;
    invoke-static {v3}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->access$000(Lcom/google/android/music/playback/LocalAsyncMediaPlayer;)Lcom/google/android/music/playback/CompatMediaPlayer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/music/playback/CompatMediaPlayer;->release()V

    .line 919
    iget-object v3, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer$3;->this$0:Lcom/google/android/music/playback/LocalAsyncMediaPlayer;

    new-instance v4, Lcom/google/android/music/playback/CompatMediaPlayer;

    invoke-direct {v4}, Lcom/google/android/music/playback/CompatMediaPlayer;-><init>()V

    # setter for: Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mMediaPlayer:Lcom/google/android/music/playback/CompatMediaPlayer;
    invoke-static {v3, v4}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->access$002(Lcom/google/android/music/playback/LocalAsyncMediaPlayer;Lcom/google/android/music/playback/CompatMediaPlayer;)Lcom/google/android/music/playback/CompatMediaPlayer;

    .line 920
    iget-object v3, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer$3;->this$0:Lcom/google/android/music/playback/LocalAsyncMediaPlayer;

    # getter for: Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mMediaPlayer:Lcom/google/android/music/playback/CompatMediaPlayer;
    invoke-static {v3}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->access$000(Lcom/google/android/music/playback/LocalAsyncMediaPlayer;)Lcom/google/android/music/playback/CompatMediaPlayer;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer$3;->this$0:Lcom/google/android/music/playback/LocalAsyncMediaPlayer;

    # getter for: Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->access$800(Lcom/google/android/music/playback/LocalAsyncMediaPlayer;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v3, v4, v1}, Lcom/google/android/music/playback/CompatMediaPlayer;->setWakeMode(Landroid/content/Context;I)V

    .line 922
    iget-object v3, p0, Lcom/google/android/music/playback/LocalAsyncMediaPlayer$3;->this$0:Lcom/google/android/music/playback/LocalAsyncMediaPlayer;

    invoke-virtual {v3, v0}, Lcom/google/android/music/playback/LocalAsyncMediaPlayer;->setAudioSessionId(I)V

    .line 924
    packed-switch p2, :pswitch_data_0

    .line 928
    const-string v1, "MultiPlayer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v2

    .line 931
    :pswitch_0
    return v1

    .line 924
    nop

    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_0
    .end packed-switch
.end method
