.class public Lcom/google/android/music/playback/AudioManagerCompat;
.super Ljava/lang/Object;
.source "AudioManagerCompat.java"


# static fields
.field private static sRegisterRemoteControlClientMethod:Ljava/lang/reflect/Method;

.field private static sRemoteControlAPIsExist:Z

.field private static sUnregisterRemoteControlClient:Ljava/lang/reflect/Method;


# instance fields
.field private final mAudioManager:Landroid/media/AudioManager;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 22
    sput-boolean v3, Lcom/google/android/music/playback/AudioManagerCompat;->sRemoteControlAPIsExist:Z

    .line 30
    invoke-static {}, Lcom/google/android/music/preferences/MusicPreferences;->isICSOrGreater()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 32
    :try_start_0
    const-class v3, Lcom/google/android/music/playback/AudioManagerCompat;

    invoke-virtual {v3}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    .line 33
    .local v0, "classLoader":Ljava/lang/ClassLoader;
    invoke-static {v0}, Lcom/google/android/music/playback/RemoteControlClientCompat;->getActualRemoteControlClientClass(Ljava/lang/ClassLoader;)Ljava/lang/Class;

    move-result-object v2

    .line 35
    .local v2, "sRemoteControlClientClass":Ljava/lang/Class;
    const-class v3, Landroid/media/AudioManager;

    const-string v4, "registerRemoteControlClient"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Class;

    const/4 v6, 0x0

    aput-object v2, v5, v6

    invoke-virtual {v3, v4, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    sput-object v3, Lcom/google/android/music/playback/AudioManagerCompat;->sRegisterRemoteControlClientMethod:Ljava/lang/reflect/Method;

    .line 37
    const-class v3, Landroid/media/AudioManager;

    const-string v4, "unregisterRemoteControlClient"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Class;

    const/4 v6, 0x0

    aput-object v2, v5, v6

    invoke-virtual {v3, v4, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    sput-object v3, Lcom/google/android/music/playback/AudioManagerCompat;->sUnregisterRemoteControlClient:Ljava/lang/reflect/Method;

    .line 40
    const/4 v3, 0x1

    sput-boolean v3, Lcom/google/android/music/playback/AudioManagerCompat;->sRemoteControlAPIsExist:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 44
    .end local v2    # "sRemoteControlClientClass":Ljava/lang/Class;
    :goto_0
    sget-boolean v3, Lcom/google/android/music/playback/AudioManagerCompat;->sRemoteControlAPIsExist:Z

    if-nez v3, :cond_0

    .line 45
    new-instance v3, Ljava/lang/RuntimeException;

    const-string v4, "ICS APIs for RemoteControlClient don\'t exist"

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 41
    :catch_0
    move-exception v1

    .line 42
    .local v1, "e":Ljava/lang/Exception;
    const-string v3, "AudioManagerCompat"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Could not get ICS info: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 50
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_0
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    const-string v0, "audio"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/google/android/music/playback/AudioManagerCompat;->mAudioManager:Landroid/media/AudioManager;

    .line 54
    return-void
.end method

.method public static getAudioManagerCompat(Landroid/content/Context;)Lcom/google/android/music/playback/AudioManagerCompat;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 57
    new-instance v0, Lcom/google/android/music/playback/AudioManagerCompat;

    invoke-direct {v0, p0}, Lcom/google/android/music/playback/AudioManagerCompat;-><init>(Landroid/content/Context;)V

    return-object v0
.end method


# virtual methods
.method public registerRemoteControlClient(Lcom/google/android/music/playback/RemoteControlClientCompat;)V
    .locals 6
    .param p1, "remoteControlClient"    # Lcom/google/android/music/playback/RemoteControlClientCompat;

    .prologue
    .line 61
    sget-boolean v1, Lcom/google/android/music/playback/AudioManagerCompat;->sRemoteControlAPIsExist:Z

    if-nez v1, :cond_0

    .line 71
    :goto_0
    return-void

    .line 66
    :cond_0
    :try_start_0
    sget-object v1, Lcom/google/android/music/playback/AudioManagerCompat;->sRegisterRemoteControlClientMethod:Ljava/lang/reflect/Method;

    iget-object v2, p0, Lcom/google/android/music/playback/AudioManagerCompat;->mAudioManager:Landroid/media/AudioManager;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p1}, Lcom/google/android/music/playback/RemoteControlClientCompat;->getActualRemoteControlClientObject()Ljava/lang/Object;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 68
    :catch_0
    move-exception v0

    .line 69
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "AudioManagerCompat"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public unregisterRemoteControlClient(Lcom/google/android/music/playback/RemoteControlClientCompat;)V
    .locals 6
    .param p1, "remoteControlClient"    # Lcom/google/android/music/playback/RemoteControlClientCompat;

    .prologue
    .line 75
    sget-boolean v1, Lcom/google/android/music/playback/AudioManagerCompat;->sRemoteControlAPIsExist:Z

    if-nez v1, :cond_0

    .line 85
    :goto_0
    return-void

    .line 80
    :cond_0
    :try_start_0
    sget-object v1, Lcom/google/android/music/playback/AudioManagerCompat;->sUnregisterRemoteControlClient:Ljava/lang/reflect/Method;

    iget-object v2, p0, Lcom/google/android/music/playback/AudioManagerCompat;->mAudioManager:Landroid/media/AudioManager;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p1}, Lcom/google/android/music/playback/RemoteControlClientCompat;->getActualRemoteControlClientObject()Ljava/lang/Object;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 82
    :catch_0
    move-exception v0

    .line 83
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "AudioManagerCompat"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
