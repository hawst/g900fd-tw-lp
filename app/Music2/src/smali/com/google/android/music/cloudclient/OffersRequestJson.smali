.class public Lcom/google/android/music/cloudclient/OffersRequestJson;
.super Lcom/google/api/client/json/GenericJson;
.source "OffersRequestJson.java"


# instance fields
.field public mCouponCode:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "couponCode"
    .end annotation
.end field

.field public mCouponType:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "couponType"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    return-void
.end method

.method public static serialize(Ljava/lang/String;Ljava/lang/String;)[B
    .locals 2
    .param p0, "couponCode"    # Ljava/lang/String;
    .param p1, "couponType"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 24
    new-instance v0, Lcom/google/android/music/cloudclient/OffersRequestJson;

    invoke-direct {v0}, Lcom/google/android/music/cloudclient/OffersRequestJson;-><init>()V

    .line 27
    .local v0, "request":Lcom/google/android/music/cloudclient/OffersRequestJson;
    if-nez p0, :cond_0

    const-string p0, ""

    .end local p0    # "couponCode":Ljava/lang/String;
    :cond_0
    iput-object p0, v0, Lcom/google/android/music/cloudclient/OffersRequestJson;->mCouponCode:Ljava/lang/String;

    .line 28
    if-nez p1, :cond_1

    const-string p1, ""

    .end local p1    # "couponType":Ljava/lang/String;
    :cond_1
    iput-object p1, v0, Lcom/google/android/music/cloudclient/OffersRequestJson;->mCouponType:Ljava/lang/String;

    .line 30
    invoke-static {v0}, Lcom/google/android/music/cloudclient/JsonUtils;->toJsonByteArray(Lcom/google/api/client/json/GenericJson;)[B

    move-result-object v1

    return-object v1
.end method
