.class Lcom/google/android/music/net/NetworkMonitor$NetworkMonitorBinder;
.super Lcom/google/android/music/net/INetworkMonitor$Stub;
.source "NetworkMonitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/net/NetworkMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "NetworkMonitorBinder"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/net/NetworkMonitor;


# direct methods
.method private constructor <init>(Lcom/google/android/music/net/NetworkMonitor;)V
    .locals 0

    .prologue
    .line 490
    iput-object p1, p0, Lcom/google/android/music/net/NetworkMonitor$NetworkMonitorBinder;->this$0:Lcom/google/android/music/net/NetworkMonitor;

    invoke-direct {p0}, Lcom/google/android/music/net/INetworkMonitor$Stub;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/music/net/NetworkMonitor;Lcom/google/android/music/net/NetworkMonitor$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/music/net/NetworkMonitor;
    .param p2, "x1"    # Lcom/google/android/music/net/NetworkMonitor$1;

    .prologue
    .line 490
    invoke-direct {p0, p1}, Lcom/google/android/music/net/NetworkMonitor$NetworkMonitorBinder;-><init>(Lcom/google/android/music/net/NetworkMonitor;)V

    return-void
.end method


# virtual methods
.method public hasHighSpeedConnection()Z
    .locals 1

    .prologue
    .line 557
    iget-object v0, p0, Lcom/google/android/music/net/NetworkMonitor$NetworkMonitorBinder;->this$0:Lcom/google/android/music/net/NetworkMonitor;

    # getter for: Lcom/google/android/music/net/NetworkMonitor;->mUnmeteredWifiOrEthernetConnected:Z
    invoke-static {v0}, Lcom/google/android/music/net/NetworkMonitor;->access$700(Lcom/google/android/music/net/NetworkMonitor;)Z

    move-result v0

    return v0
.end method

.method public hasMobileOrMeteredConnection()Z
    .locals 1

    .prologue
    .line 553
    iget-object v0, p0, Lcom/google/android/music/net/NetworkMonitor$NetworkMonitorBinder;->this$0:Lcom/google/android/music/net/NetworkMonitor;

    # getter for: Lcom/google/android/music/net/NetworkMonitor;->mMobileOrMeteredConnected:Z
    invoke-static {v0}, Lcom/google/android/music/net/NetworkMonitor;->access$600(Lcom/google/android/music/net/NetworkMonitor;)Z

    move-result v0

    return v0
.end method

.method public hasWifiConnection()Z
    .locals 1

    .prologue
    .line 549
    iget-object v0, p0, Lcom/google/android/music/net/NetworkMonitor$NetworkMonitorBinder;->this$0:Lcom/google/android/music/net/NetworkMonitor;

    # getter for: Lcom/google/android/music/net/NetworkMonitor;->mWifiConnected:Z
    invoke-static {v0}, Lcom/google/android/music/net/NetworkMonitor;->access$2100(Lcom/google/android/music/net/NetworkMonitor;)Z

    move-result v0

    return v0
.end method

.method public isConnected()Z
    .locals 1

    .prologue
    .line 545
    iget-object v0, p0, Lcom/google/android/music/net/NetworkMonitor$NetworkMonitorBinder;->this$0:Lcom/google/android/music/net/NetworkMonitor;

    # getter for: Lcom/google/android/music/net/NetworkMonitor;->mMobileOrMeteredConnected:Z
    invoke-static {v0}, Lcom/google/android/music/net/NetworkMonitor;->access$600(Lcom/google/android/music/net/NetworkMonitor;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/net/NetworkMonitor$NetworkMonitorBinder;->this$0:Lcom/google/android/music/net/NetworkMonitor;

    # getter for: Lcom/google/android/music/net/NetworkMonitor;->mUnmeteredWifiOrEthernetConnected:Z
    invoke-static {v0}, Lcom/google/android/music/net/NetworkMonitor;->access$700(Lcom/google/android/music/net/NetworkMonitor;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isDownloadingAvailable()Z
    .locals 1

    .prologue
    .line 565
    iget-object v0, p0, Lcom/google/android/music/net/NetworkMonitor$NetworkMonitorBinder;->this$0:Lcom/google/android/music/net/NetworkMonitor;

    # getter for: Lcom/google/android/music/net/NetworkMonitor;->mIsDownloadingAvailable:Z
    invoke-static {v0}, Lcom/google/android/music/net/NetworkMonitor;->access$1600(Lcom/google/android/music/net/NetworkMonitor;)Z

    move-result v0

    return v0
.end method

.method public isStreamingAvailable()Z
    .locals 1

    .prologue
    .line 561
    iget-object v0, p0, Lcom/google/android/music/net/NetworkMonitor$NetworkMonitorBinder;->this$0:Lcom/google/android/music/net/NetworkMonitor;

    # getter for: Lcom/google/android/music/net/NetworkMonitor;->mIsStreamingAvailable:Z
    invoke-static {v0}, Lcom/google/android/music/net/NetworkMonitor;->access$1300(Lcom/google/android/music/net/NetworkMonitor;)Z

    move-result v0

    return v0
.end method

.method public registerDownloadabilityChangeListener(Lcom/google/android/music/net/IDownloadabilityChangeListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/google/android/music/net/IDownloadabilityChangeListener;

    .prologue
    .line 516
    iget-object v1, p0, Lcom/google/android/music/net/NetworkMonitor$NetworkMonitorBinder;->this$0:Lcom/google/android/music/net/NetworkMonitor;

    # getter for: Lcom/google/android/music/net/NetworkMonitor;->mDownloadabilityListeners:Landroid/os/RemoteCallbackList;
    invoke-static {v1}, Lcom/google/android/music/net/NetworkMonitor;->access$1900(Lcom/google/android/music/net/NetworkMonitor;)Landroid/os/RemoteCallbackList;

    move-result-object v2

    monitor-enter v2

    .line 517
    :try_start_0
    iget-object v1, p0, Lcom/google/android/music/net/NetworkMonitor$NetworkMonitorBinder;->this$0:Lcom/google/android/music/net/NetworkMonitor;

    # getter for: Lcom/google/android/music/net/NetworkMonitor;->mDownloadabilityListeners:Landroid/os/RemoteCallbackList;
    invoke-static {v1}, Lcom/google/android/music/net/NetworkMonitor;->access$1900(Lcom/google/android/music/net/NetworkMonitor;)Landroid/os/RemoteCallbackList;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;)Z

    .line 518
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 520
    :try_start_1
    iget-object v1, p0, Lcom/google/android/music/net/NetworkMonitor$NetworkMonitorBinder;->this$0:Lcom/google/android/music/net/NetworkMonitor;

    # getter for: Lcom/google/android/music/net/NetworkMonitor;->mIsDownloadingAvailable:Z
    invoke-static {v1}, Lcom/google/android/music/net/NetworkMonitor;->access$1600(Lcom/google/android/music/net/NetworkMonitor;)Z

    move-result v1

    invoke-interface {p1, v1}, Lcom/google/android/music/net/IDownloadabilityChangeListener;->onDownloadabilityChanged(Z)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    .line 524
    :goto_0
    return-void

    .line 518
    :catchall_0
    move-exception v1

    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 521
    :catch_0
    move-exception v0

    .line 522
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "NetworkMonitor"

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public registerNetworkChangeListener(Lcom/google/android/music/net/INetworkChangeListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/google/android/music/net/INetworkChangeListener;

    .prologue
    .line 527
    iget-object v1, p0, Lcom/google/android/music/net/NetworkMonitor$NetworkMonitorBinder;->this$0:Lcom/google/android/music/net/NetworkMonitor;

    # getter for: Lcom/google/android/music/net/NetworkMonitor;->mNetworkChangeListeners:Landroid/os/RemoteCallbackList;
    invoke-static {v1}, Lcom/google/android/music/net/NetworkMonitor;->access$2000(Lcom/google/android/music/net/NetworkMonitor;)Landroid/os/RemoteCallbackList;

    move-result-object v2

    monitor-enter v2

    .line 528
    :try_start_0
    iget-object v1, p0, Lcom/google/android/music/net/NetworkMonitor$NetworkMonitorBinder;->this$0:Lcom/google/android/music/net/NetworkMonitor;

    # getter for: Lcom/google/android/music/net/NetworkMonitor;->mNetworkChangeListeners:Landroid/os/RemoteCallbackList;
    invoke-static {v1}, Lcom/google/android/music/net/NetworkMonitor;->access$2000(Lcom/google/android/music/net/NetworkMonitor;)Landroid/os/RemoteCallbackList;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;)Z

    .line 529
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 531
    :try_start_1
    iget-object v1, p0, Lcom/google/android/music/net/NetworkMonitor$NetworkMonitorBinder;->this$0:Lcom/google/android/music/net/NetworkMonitor;

    # getter for: Lcom/google/android/music/net/NetworkMonitor;->mMobileOrMeteredConnected:Z
    invoke-static {v1}, Lcom/google/android/music/net/NetworkMonitor;->access$600(Lcom/google/android/music/net/NetworkMonitor;)Z

    move-result v1

    iget-object v2, p0, Lcom/google/android/music/net/NetworkMonitor$NetworkMonitorBinder;->this$0:Lcom/google/android/music/net/NetworkMonitor;

    # getter for: Lcom/google/android/music/net/NetworkMonitor;->mUnmeteredWifiOrEthernetConnected:Z
    invoke-static {v2}, Lcom/google/android/music/net/NetworkMonitor;->access$700(Lcom/google/android/music/net/NetworkMonitor;)Z

    move-result v2

    invoke-interface {p1, v1, v2}, Lcom/google/android/music/net/INetworkChangeListener;->onNetworkChanged(ZZ)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    .line 536
    :goto_0
    return-void

    .line 529
    :catchall_0
    move-exception v1

    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 533
    :catch_0
    move-exception v0

    .line 534
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "NetworkMonitor"

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public registerStreamabilityChangeListener(Lcom/google/android/music/net/IStreamabilityChangeListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/google/android/music/net/IStreamabilityChangeListener;

    .prologue
    .line 499
    iget-object v1, p0, Lcom/google/android/music/net/NetworkMonitor$NetworkMonitorBinder;->this$0:Lcom/google/android/music/net/NetworkMonitor;

    # getter for: Lcom/google/android/music/net/NetworkMonitor;->mStreamabilityListeners:Landroid/os/RemoteCallbackList;
    invoke-static {v1}, Lcom/google/android/music/net/NetworkMonitor;->access$1800(Lcom/google/android/music/net/NetworkMonitor;)Landroid/os/RemoteCallbackList;

    move-result-object v2

    monitor-enter v2

    .line 500
    :try_start_0
    iget-object v1, p0, Lcom/google/android/music/net/NetworkMonitor$NetworkMonitorBinder;->this$0:Lcom/google/android/music/net/NetworkMonitor;

    # getter for: Lcom/google/android/music/net/NetworkMonitor;->mStreamabilityListeners:Landroid/os/RemoteCallbackList;
    invoke-static {v1}, Lcom/google/android/music/net/NetworkMonitor;->access$1800(Lcom/google/android/music/net/NetworkMonitor;)Landroid/os/RemoteCallbackList;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;)Z

    .line 501
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 503
    :try_start_1
    iget-object v1, p0, Lcom/google/android/music/net/NetworkMonitor$NetworkMonitorBinder;->this$0:Lcom/google/android/music/net/NetworkMonitor;

    # getter for: Lcom/google/android/music/net/NetworkMonitor;->mIsStreamingAvailable:Z
    invoke-static {v1}, Lcom/google/android/music/net/NetworkMonitor;->access$1300(Lcom/google/android/music/net/NetworkMonitor;)Z

    move-result v1

    invoke-interface {p1, v1}, Lcom/google/android/music/net/IStreamabilityChangeListener;->onStreamabilityChanged(Z)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    .line 507
    :goto_0
    return-void

    .line 501
    :catchall_0
    move-exception v1

    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 504
    :catch_0
    move-exception v0

    .line 505
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "NetworkMonitor"

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public unregisterDownloadabilityChangeListener(Lcom/google/android/music/net/IDownloadabilityChangeListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/google/android/music/net/IDownloadabilityChangeListener;

    .prologue
    .line 510
    iget-object v0, p0, Lcom/google/android/music/net/NetworkMonitor$NetworkMonitorBinder;->this$0:Lcom/google/android/music/net/NetworkMonitor;

    # getter for: Lcom/google/android/music/net/NetworkMonitor;->mDownloadabilityListeners:Landroid/os/RemoteCallbackList;
    invoke-static {v0}, Lcom/google/android/music/net/NetworkMonitor;->access$1900(Lcom/google/android/music/net/NetworkMonitor;)Landroid/os/RemoteCallbackList;

    move-result-object v1

    monitor-enter v1

    .line 511
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/net/NetworkMonitor$NetworkMonitorBinder;->this$0:Lcom/google/android/music/net/NetworkMonitor;

    # getter for: Lcom/google/android/music/net/NetworkMonitor;->mDownloadabilityListeners:Landroid/os/RemoteCallbackList;
    invoke-static {v0}, Lcom/google/android/music/net/NetworkMonitor;->access$1900(Lcom/google/android/music/net/NetworkMonitor;)Landroid/os/RemoteCallbackList;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z

    .line 512
    monitor-exit v1

    .line 513
    return-void

    .line 512
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public unregisterNetworkChangeListener(Lcom/google/android/music/net/INetworkChangeListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/google/android/music/net/INetworkChangeListener;

    .prologue
    .line 539
    iget-object v0, p0, Lcom/google/android/music/net/NetworkMonitor$NetworkMonitorBinder;->this$0:Lcom/google/android/music/net/NetworkMonitor;

    # getter for: Lcom/google/android/music/net/NetworkMonitor;->mNetworkChangeListeners:Landroid/os/RemoteCallbackList;
    invoke-static {v0}, Lcom/google/android/music/net/NetworkMonitor;->access$2000(Lcom/google/android/music/net/NetworkMonitor;)Landroid/os/RemoteCallbackList;

    move-result-object v1

    monitor-enter v1

    .line 540
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/net/NetworkMonitor$NetworkMonitorBinder;->this$0:Lcom/google/android/music/net/NetworkMonitor;

    # getter for: Lcom/google/android/music/net/NetworkMonitor;->mNetworkChangeListeners:Landroid/os/RemoteCallbackList;
    invoke-static {v0}, Lcom/google/android/music/net/NetworkMonitor;->access$2000(Lcom/google/android/music/net/NetworkMonitor;)Landroid/os/RemoteCallbackList;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z

    .line 541
    monitor-exit v1

    .line 542
    return-void

    .line 541
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public unregisterStreamabilityChangeListener(Lcom/google/android/music/net/IStreamabilityChangeListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/google/android/music/net/IStreamabilityChangeListener;

    .prologue
    .line 493
    iget-object v0, p0, Lcom/google/android/music/net/NetworkMonitor$NetworkMonitorBinder;->this$0:Lcom/google/android/music/net/NetworkMonitor;

    # getter for: Lcom/google/android/music/net/NetworkMonitor;->mStreamabilityListeners:Landroid/os/RemoteCallbackList;
    invoke-static {v0}, Lcom/google/android/music/net/NetworkMonitor;->access$1800(Lcom/google/android/music/net/NetworkMonitor;)Landroid/os/RemoteCallbackList;

    move-result-object v1

    monitor-enter v1

    .line 494
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/net/NetworkMonitor$NetworkMonitorBinder;->this$0:Lcom/google/android/music/net/NetworkMonitor;

    # getter for: Lcom/google/android/music/net/NetworkMonitor;->mStreamabilityListeners:Landroid/os/RemoteCallbackList;
    invoke-static {v0}, Lcom/google/android/music/net/NetworkMonitor;->access$1800(Lcom/google/android/music/net/NetworkMonitor;)Landroid/os/RemoteCallbackList;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z

    .line 495
    monitor-exit v1

    .line 496
    return-void

    .line 495
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
