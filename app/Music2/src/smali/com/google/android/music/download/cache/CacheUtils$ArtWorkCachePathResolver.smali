.class public Lcom/google/android/music/download/cache/CacheUtils$ArtWorkCachePathResolver;
.super Lcom/google/android/music/download/cache/CacheUtils$ArtworkPathResolver;
.source "CacheUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/download/cache/CacheUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ArtWorkCachePathResolver"
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 179
    invoke-direct {p0}, Lcom/google/android/music/download/cache/CacheUtils$ArtworkPathResolver;-><init>()V

    .line 180
    invoke-static {p1, v1}, Lcom/google/android/music/download/cache/CacheUtils;->getExternalCacheDirectory(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/download/cache/CacheUtils$ArtWorkCachePathResolver;->mExternalCacheDirectory:Ljava/io/File;

    .line 181
    invoke-static {p1, v1}, Lcom/google/android/music/download/cache/CacheUtils;->getInternalCacheDirectory(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/download/cache/CacheUtils$ArtWorkCachePathResolver;->mInternalCacheDirectory:Ljava/io/File;

    .line 182
    return-void
.end method
