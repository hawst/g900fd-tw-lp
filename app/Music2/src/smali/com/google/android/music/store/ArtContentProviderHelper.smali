.class Lcom/google/android/music/store/ArtContentProviderHelper;
.super Ljava/lang/Object;
.source "ArtContentProviderHelper.java"


# static fields
.field private static final LOGV:Z


# instance fields
.field private final mAppContext:Landroid/content/Context;

.field private final mStore:Lcom/google/android/music/store/Store;

.field private final mUriMatcher:Landroid/content/UriMatcher;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->CONTENT_PROVIDER:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/store/ArtContentProviderHelper;->LOGV:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/content/UriMatcher;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "uriMatcher"    # Landroid/content/UriMatcher;

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/store/ArtContentProviderHelper;->mAppContext:Landroid/content/Context;

    .line 35
    invoke-static {p1}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/store/ArtContentProviderHelper;->mStore:Lcom/google/android/music/store/Store;

    .line 36
    iput-object p2, p0, Lcom/google/android/music/store/ArtContentProviderHelper;->mUriMatcher:Landroid/content/UriMatcher;

    .line 37
    return-void
.end method

.method private getArtFileDescriptor(Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    .locals 12
    .param p1, "remoteLocation"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 209
    iget-object v7, p0, Lcom/google/android/music/store/ArtContentProviderHelper;->mAppContext:Landroid/content/Context;

    invoke-static {v7}, Lcom/google/android/music/utils/MusicUtils;->getStreamingAccount(Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v7

    if-eqz v7, :cond_3

    const/4 v2, 0x1

    .line 211
    .local v2, "hasStreaming":Z
    :goto_0
    const/4 v3, 0x0

    .line 212
    .local v3, "locationPair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Integer;>;"
    if-eqz v2, :cond_0

    .line 214
    iget-object v7, p0, Lcom/google/android/music/store/ArtContentProviderHelper;->mStore:Lcom/google/android/music/store/Store;

    invoke-virtual {v7, p1}, Lcom/google/android/music/store/Store;->getArtwork(Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v3

    .line 217
    :cond_0
    new-instance v6, Lcom/google/android/music/download/cache/CacheUtils$ArtWorkCachePathResolver;

    iget-object v7, p0, Lcom/google/android/music/store/ArtContentProviderHelper;->mAppContext:Landroid/content/Context;

    invoke-direct {v6, v7}, Lcom/google/android/music/download/cache/CacheUtils$ArtWorkCachePathResolver;-><init>(Landroid/content/Context;)V

    .line 218
    .local v6, "resolver":Lcom/google/android/music/download/cache/CacheUtils$ArtworkPathResolver;
    const/4 v0, 0x0

    .line 220
    .local v0, "fd":Landroid/os/ParcelFileDescriptor;
    if-nez v3, :cond_4

    const/4 v1, 0x0

    .line 222
    .local v1, "fileLocation":Ljava/io/File;
    :goto_1
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v8

    const-wide/16 v10, 0x0

    cmp-long v7, v8, v10

    if-nez v7, :cond_5

    .line 224
    :cond_1
    if-eqz p1, :cond_2

    invoke-static {p1}, Lcom/google/android/music/download/cache/CacheUtils;->isRemoteLocationSideLoaded(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 226
    const-string v7, "mediastore:"

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {p1, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    .line 228
    .local v4, "mediaStoreAlbumId":Ljava/lang/String;
    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    invoke-static {v8, v9}, Lcom/google/android/music/store/MusicContent$AlbumArt;->getMediaStoreAlbumArt(J)Landroid/net/Uri;

    move-result-object v5

    .line 230
    .local v5, "mediaStoreUri":Landroid/net/Uri;
    if-eqz v5, :cond_2

    .line 232
    :try_start_0
    iget-object v7, p0, Lcom/google/android/music/store/ArtContentProviderHelper;->mAppContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string v8, "r"

    invoke-virtual {v7, v5, v8}, Landroid/content/ContentResolver;->openFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 244
    .end local v4    # "mediaStoreAlbumId":Ljava/lang/String;
    .end local v5    # "mediaStoreUri":Landroid/net/Uri;
    :cond_2
    :goto_2
    return-object v0

    .line 209
    .end local v0    # "fd":Landroid/os/ParcelFileDescriptor;
    .end local v1    # "fileLocation":Ljava/io/File;
    .end local v2    # "hasStreaming":Z
    .end local v3    # "locationPair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Integer;>;"
    .end local v6    # "resolver":Lcom/google/android/music/download/cache/CacheUtils$ArtworkPathResolver;
    :cond_3
    const/4 v2, 0x0

    goto :goto_0

    .line 220
    .restart local v0    # "fd":Landroid/os/ParcelFileDescriptor;
    .restart local v2    # "hasStreaming":Z
    .restart local v3    # "locationPair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Integer;>;"
    .restart local v6    # "resolver":Lcom/google/android/music/download/cache/CacheUtils$ArtworkPathResolver;
    :cond_4
    iget-object v7, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v7, Ljava/lang/String;

    iget-object v8, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    invoke-virtual {v6, v7, v8}, Lcom/google/android/music/download/cache/CacheUtils$ArtworkPathResolver;->resolveArtworkPath(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v1

    goto :goto_1

    .line 241
    .restart local v1    # "fileLocation":Ljava/io/File;
    :cond_5
    const/high16 v7, 0x10000000

    invoke-static {v1, v7}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    goto :goto_2

    .line 234
    .restart local v4    # "mediaStoreAlbumId":Ljava/lang/String;
    .restart local v5    # "mediaStoreUri":Landroid/net/Uri;
    :catch_0
    move-exception v7

    goto :goto_2
.end method

.method private getFauxAlbumArt(JII)Landroid/os/ParcelFileDescriptor;
    .locals 13
    .param p1, "albumId"    # J
    .param p3, "width"    # I
    .param p4, "height"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 150
    const/4 v8, 0x0

    .line 151
    .local v8, "albumName":Ljava/lang/String;
    const/4 v9, 0x0

    .line 153
    .local v9, "artistName":Ljava/lang/String;
    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "album_name"

    aput-object v1, v2, v0

    const/4 v0, 0x1

    const-string v1, "album_artist"

    aput-object v1, v2, v0

    .line 157
    .local v2, "cols":[Ljava/lang/String;
    iget-object v0, p0, Lcom/google/android/music/store/ArtContentProviderHelper;->mAppContext:Landroid/content/Context;

    invoke-static {p1, p2}, Lcom/google/android/music/store/MusicContent$Albums;->getAlbumsUri(J)Landroid/net/Uri;

    move-result-object v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 160
    .local v12, "c":Landroid/database/Cursor;
    if-eqz v12, :cond_0

    :try_start_0
    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 161
    const/4 v0, 0x0

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 162
    const/4 v0, 0x1

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v9

    .line 165
    :cond_0
    invoke-static {v12}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 168
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x9

    if-lt v0, v1, :cond_1

    .line 169
    iget-object v4, p0, Lcom/google/android/music/store/ArtContentProviderHelper;->mAppContext:Landroid/content/Context;

    const/4 v5, 0x0

    move-wide v6, p1

    move/from16 v10, p3

    move/from16 v11, p4

    invoke-static/range {v4 .. v11}, Lcom/google/android/music/utils/AlbumArtUtils;->getStaticFauxArtPipe(Landroid/content/Context;IJLjava/lang/String;Ljava/lang/String;II)Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    .line 172
    :goto_0
    return-object v0

    .line 165
    :catchall_0
    move-exception v0

    invoke-static {v12}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0

    .line 172
    :cond_1
    const/4 v5, 0x0

    move-object v4, p0

    move-wide v6, p1

    move/from16 v10, p3

    move/from16 v11, p4

    invoke-direct/range {v4 .. v11}, Lcom/google/android/music/store/ArtContentProviderHelper;->getStaticFauxArt(IJLjava/lang/String;Ljava/lang/String;II)Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    goto :goto_0
.end method

.method private getStaticFauxArt(IJII)Landroid/os/ParcelFileDescriptor;
    .locals 8
    .param p1, "style"    # I
    .param p2, "id"    # J
    .param p4, "width"    # I
    .param p5, "height"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 179
    move-object v0, p0

    move v1, p1

    move-wide v2, p2

    move-object v5, v4

    move v6, p4

    move v7, p5

    invoke-direct/range {v0 .. v7}, Lcom/google/android/music/store/ArtContentProviderHelper;->getStaticFauxArt(IJLjava/lang/String;Ljava/lang/String;II)Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    return-object v0
.end method

.method private getStaticFauxArt(IJLjava/lang/String;Ljava/lang/String;II)Landroid/os/ParcelFileDescriptor;
    .locals 10
    .param p1, "style"    # I
    .param p2, "id"    # J
    .param p4, "mainLabel"    # Ljava/lang/String;
    .param p5, "subLabel"    # Ljava/lang/String;
    .param p6, "width"    # I
    .param p7, "height"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 184
    iget-object v0, p0, Lcom/google/android/music/store/ArtContentProviderHelper;->mAppContext:Landroid/content/Context;

    move v1, p1

    move-wide v2, p2

    move-object v4, p4

    move-object v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    invoke-static/range {v0 .. v7}, Lcom/google/android/music/utils/AlbumArtUtils;->getStaticFauxArtFile(Landroid/content/Context;IJLjava/lang/String;Ljava/lang/String;II)Ljava/io/File;

    move-result-object v8

    .line 186
    .local v8, "f":Ljava/io/File;
    const/high16 v0, 0x10000000

    invoke-static {v8, v0}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    return-object v0
.end method

.method private handleUrlArt(Landroid/content/Context;Lcom/google/android/music/store/Store;Ljava/lang/String;Lcom/google/android/music/art/ArtLoader$DownloadMode;)Landroid/os/ParcelFileDescriptor;
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "store"    # Lcom/google/android/music/store/Store;
    .param p3, "url"    # Ljava/lang/String;
    .param p4, "mode"    # Lcom/google/android/music/art/ArtLoader$DownloadMode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 131
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 132
    const/4 v0, 0x0

    .line 145
    :cond_0
    :goto_0
    return-object v0

    .line 135
    :cond_1
    const/4 v0, 0x0

    .line 136
    .local v0, "fd":Landroid/os/ParcelFileDescriptor;
    invoke-direct {p0, p3}, Lcom/google/android/music/store/ArtContentProviderHelper;->getArtFileDescriptor(Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    .line 138
    if-nez v0, :cond_0

    sget-object v3, Lcom/google/android/music/art/ArtLoader$DownloadMode;->ASYNC_DOWNLOAD:Lcom/google/android/music/art/ArtLoader$DownloadMode;

    if-ne p4, v3, :cond_0

    .line 139
    invoke-static {p1}, Lcom/google/android/music/utils/MusicUtils;->getStreamingAccount(Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v3

    if-eqz v3, :cond_2

    move v1, v2

    .line 140
    .local v1, "hasStreaming":Z
    :goto_1
    if-eqz v1, :cond_0

    .line 141
    invoke-static {p1, p3, v2}, Lcom/google/android/music/download/artwork/ArtDownloadUtils;->startArtDownload(Landroid/content/Context;Ljava/lang/String;Z)V

    goto :goto_0

    .line 139
    .end local v1    # "hasStreaming":Z
    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private static parseIntegerParameter(Landroid/net/Uri;Ljava/lang/String;I)I
    .locals 5
    .param p0, "uri"    # Landroid/net/Uri;
    .param p1, "paramName"    # Ljava/lang/String;
    .param p2, "defaultValue"    # I

    .prologue
    .line 190
    invoke-virtual {p0, p1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 191
    .local v1, "paramValue":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 193
    :try_start_0
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result p2

    .line 198
    .end local p2    # "defaultValue":I
    :cond_0
    :goto_0
    return p2

    .line 194
    .restart local p2    # "defaultValue":I
    :catch_0
    move-exception v0

    .line 195
    .local v0, "e":Ljava/lang/NumberFormatException;
    const-string v2, "ArtContentProviderHelper"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "could not parse param \""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\" from URI: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public openArtFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    .locals 33
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "mode"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 40
    const/16 v28, 0x0

    .line 46
    .local v28, "fd":Landroid/os/ParcelFileDescriptor;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/store/ArtContentProviderHelper;->mAppContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v29

    .line 47
    .local v29, "store":Lcom/google/android/music/store/Store;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/store/ArtContentProviderHelper;->mUriMatcher:Landroid/content/UriMatcher;

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v31

    .line 48
    .local v31, "uriMatch":I
    const-string v4, "w"

    const/4 v5, -0x1

    move-object/from16 v0, p1

    invoke-static {v0, v4, v5}, Lcom/google/android/music/store/ArtContentProviderHelper;->parseIntegerParameter(Landroid/net/Uri;Ljava/lang/String;I)I

    move-result v14

    .line 49
    .local v14, "width":I
    const-string v4, "h"

    const/4 v5, -0x1

    move-object/from16 v0, p1

    invoke-static {v0, v4, v5}, Lcom/google/android/music/store/ArtContentProviderHelper;->parseIntegerParameter(Landroid/net/Uri;Ljava/lang/String;I)I

    move-result v15

    .line 51
    .local v15, "height":I
    const/16 v4, 0x320

    move/from16 v0, v31

    if-eq v0, v4, :cond_0

    const/16 v4, 0x323

    move/from16 v0, v31

    if-eq v0, v4, :cond_0

    const/16 v4, 0x325

    move/from16 v0, v31

    if-ne v0, v4, :cond_5

    .line 54
    :cond_0
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v30

    .line 55
    .local v30, "strAlbumId":Ljava/lang/String;
    invoke-static/range {v30 .. v30}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v22

    .line 56
    .local v22, "albumId":J
    const-wide/16 v4, 0x0

    cmp-long v4, v22, v4

    if-gtz v4, :cond_2

    .line 58
    sget-boolean v4, Lcom/google/android/music/store/ArtContentProviderHelper;->LOGV:Z

    if-eqz v4, :cond_1

    const-string v4, "ArtContentProviderHelper"

    const-string v5, "Unknown album art requested"

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    .end local v22    # "albumId":J
    .end local v30    # "strAlbumId":Ljava/lang/String;
    :cond_1
    :goto_0
    if-nez v28, :cond_c

    .line 119
    new-instance v4, Ljava/io/FileNotFoundException;

    invoke-direct {v4}, Ljava/io/FileNotFoundException;-><init>()V

    throw v4

    .line 60
    .restart local v22    # "albumId":J
    .restart local v30    # "strAlbumId":Ljava/lang/String;
    :cond_2
    move-object/from16 v0, v29

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/Store;->getArtLocationForAlbum(J)Ljava/lang/String;

    move-result-object v25

    .line 61
    .local v25, "artUrl":Ljava/lang/String;
    invoke-static/range {p1 .. p1}, Lcom/google/android/music/store/MusicContent$UrlArt;->getMode(Landroid/net/Uri;)Lcom/google/android/music/art/ArtLoader$DownloadMode;

    move-result-object v27

    .line 63
    .local v27, "downloadMode":Lcom/google/android/music/art/ArtLoader$DownloadMode;
    sget-object v4, Lcom/google/android/music/art/ArtLoader$DownloadMode;->SYNC_DOWNLOAD:Lcom/google/android/music/art/ArtLoader$DownloadMode;

    move-object/from16 v0, v27

    if-ne v0, v4, :cond_3

    .line 64
    new-instance v4, Ljava/lang/UnsupportedOperationException;

    const-string v5, "sync downloads not supported"

    invoke-direct {v4, v5}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 67
    :cond_3
    if-nez v27, :cond_4

    .line 69
    sget-object v27, Lcom/google/android/music/art/ArtLoader$DownloadMode;->ASYNC_DOWNLOAD:Lcom/google/android/music/art/ArtLoader$DownloadMode;

    .line 72
    :cond_4
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/store/ArtContentProviderHelper;->mAppContext:Landroid/content/Context;

    move-object/from16 v0, p0

    move-object/from16 v1, v29

    move-object/from16 v2, v25

    move-object/from16 v3, v27

    invoke-direct {v0, v4, v1, v2, v3}, Lcom/google/android/music/store/ArtContentProviderHelper;->handleUrlArt(Landroid/content/Context;Lcom/google/android/music/store/Store;Ljava/lang/String;Lcom/google/android/music/art/ArtLoader$DownloadMode;)Landroid/os/ParcelFileDescriptor;

    move-result-object v28

    .line 73
    if-nez v28, :cond_1

    .line 74
    const/16 v4, 0x323

    move/from16 v0, v31

    if-ne v0, v4, :cond_1

    .line 76
    move-object/from16 v0, p0

    move-wide/from16 v1, v22

    invoke-direct {v0, v1, v2, v14, v15}, Lcom/google/android/music/store/ArtContentProviderHelper;->getFauxAlbumArt(JII)Landroid/os/ParcelFileDescriptor;

    move-result-object v28

    goto :goto_0

    .line 80
    .end local v22    # "albumId":J
    .end local v25    # "artUrl":Ljava/lang/String;
    .end local v27    # "downloadMode":Lcom/google/android/music/art/ArtLoader$DownloadMode;
    .end local v30    # "strAlbumId":Ljava/lang/String;
    :cond_5
    const/16 v4, 0x324

    move/from16 v0, v31

    if-ne v0, v4, :cond_6

    .line 81
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v22

    .line 82
    .restart local v22    # "albumId":J
    move-object/from16 v0, p0

    move-wide/from16 v1, v22

    invoke-direct {v0, v1, v2, v14, v15}, Lcom/google/android/music/store/ArtContentProviderHelper;->getFauxAlbumArt(JII)Landroid/os/ParcelFileDescriptor;

    move-result-object v28

    .line 83
    goto :goto_0

    .end local v22    # "albumId":J
    :cond_6
    const/16 v4, 0x322

    move/from16 v0, v31

    if-ne v0, v4, :cond_8

    .line 84
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v10

    .line 85
    .local v10, "id":J
    const/4 v4, 0x2

    new-array v6, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "_id"

    aput-object v5, v6, v4

    const/4 v4, 0x1

    const-string v5, "playlist_name"

    aput-object v5, v6, v4

    .line 89
    .local v6, "projection":[Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/store/ArtContentProviderHelper;->mAppContext:Landroid/content/Context;

    invoke-static {v10, v11}, Lcom/google/android/music/store/MusicContent$Playlists;->getPlaylistUri(J)Landroid/net/Uri;

    move-result-object v5

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-static/range {v4 .. v9}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v26

    .line 91
    .local v26, "cursor":Landroid/database/Cursor;
    const/4 v12, 0x0

    .line 93
    .local v12, "playlistName":Ljava/lang/String;
    :try_start_0
    invoke-interface/range {v26 .. v26}, Landroid/database/Cursor;->moveToFirst()Z

    .line 94
    const/4 v4, 0x1

    move-object/from16 v0, v26

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v12

    .line 96
    invoke-static/range {v26 .. v26}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 98
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x9

    if-lt v4, v5, :cond_7

    .line 99
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/music/store/ArtContentProviderHelper;->mAppContext:Landroid/content/Context;

    const/4 v9, 0x1

    const/4 v13, 0x0

    invoke-static/range {v8 .. v15}, Lcom/google/android/music/utils/AlbumArtUtils;->getStaticFauxArtPipe(Landroid/content/Context;IJLjava/lang/String;Ljava/lang/String;II)Landroid/os/ParcelFileDescriptor;

    move-result-object v28

    goto/16 :goto_0

    .line 96
    :catchall_0
    move-exception v4

    invoke-static/range {v26 .. v26}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v4

    .line 102
    :cond_7
    const/4 v9, 0x1

    const/4 v13, 0x0

    move-object/from16 v8, p0

    invoke-direct/range {v8 .. v15}, Lcom/google/android/music/store/ArtContentProviderHelper;->getStaticFauxArt(IJLjava/lang/String;Ljava/lang/String;II)Landroid/os/ParcelFileDescriptor;

    move-result-object v28

    goto/16 :goto_0

    .line 105
    .end local v6    # "projection":[Ljava/lang/String;
    .end local v10    # "id":J
    .end local v12    # "playlistName":Ljava/lang/String;
    .end local v26    # "cursor":Landroid/database/Cursor;
    :cond_8
    const/16 v4, 0x321

    move/from16 v0, v31

    if-ne v0, v4, :cond_9

    .line 106
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v10

    .line 107
    .restart local v10    # "id":J
    const/16 v17, 0x5

    move-object/from16 v16, p0

    move-wide/from16 v18, v10

    move/from16 v20, v14

    move/from16 v21, v15

    invoke-direct/range {v16 .. v21}, Lcom/google/android/music/store/ArtContentProviderHelper;->getStaticFauxArt(IJII)Landroid/os/ParcelFileDescriptor;

    move-result-object v28

    .line 108
    goto/16 :goto_0

    .end local v10    # "id":J
    :cond_9
    const/16 v4, 0x326

    move/from16 v0, v31

    if-ne v0, v4, :cond_a

    .line 109
    invoke-static/range {p1 .. p1}, Lcom/google/android/music/store/MusicContent$UrlArt;->getRemoteUrl(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v32

    .line 110
    .local v32, "url":Ljava/lang/String;
    invoke-static/range {p1 .. p1}, Lcom/google/android/music/store/MusicContent$UrlArt;->getMode(Landroid/net/Uri;)Lcom/google/android/music/art/ArtLoader$DownloadMode;

    move-result-object v24

    .line 111
    .local v24, "artLoaderMode":Lcom/google/android/music/art/ArtLoader$DownloadMode;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/store/ArtContentProviderHelper;->mAppContext:Landroid/content/Context;

    move-object/from16 v0, p0

    move-object/from16 v1, v29

    move-object/from16 v2, v32

    move-object/from16 v3, v24

    invoke-direct {v0, v4, v1, v2, v3}, Lcom/google/android/music/store/ArtContentProviderHelper;->handleUrlArt(Landroid/content/Context;Lcom/google/android/music/store/Store;Ljava/lang/String;Lcom/google/android/music/art/ArtLoader$DownloadMode;)Landroid/os/ParcelFileDescriptor;

    move-result-object v28

    .line 112
    goto/16 :goto_0

    .end local v24    # "artLoaderMode":Lcom/google/android/music/art/ArtLoader$DownloadMode;
    .end local v32    # "url":Ljava/lang/String;
    :cond_a
    const/16 v4, 0x834

    move/from16 v0, v31

    if-ne v0, v4, :cond_b

    .line 113
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/music/store/ArtContentProviderHelper;->mAppContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/google/android/music/utils/AlbumArtUtils;->getMusicUserContentPromoteNautilusPipe(Landroid/content/Context;)Landroid/os/ParcelFileDescriptor;

    move-result-object v28

    goto/16 :goto_0

    .line 115
    :cond_b
    const-string v4, "ArtContentProviderHelper"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Unrecognized openFile request: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 121
    :cond_c
    invoke-virtual/range {v28 .. v28}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/FileDescriptor;->valid()Z

    move-result v4

    if-nez v4, :cond_d

    .line 122
    const-string v4, "ArtContentProviderHelper"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Invalid file descriptor for "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 123
    new-instance v4, Ljava/io/FileNotFoundException;

    invoke-direct {v4}, Ljava/io/FileNotFoundException;-><init>()V

    throw v4

    .line 125
    :cond_d
    return-object v28
.end method
