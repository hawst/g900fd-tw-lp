.class public Lcom/google/android/music/cloudclient/GetSituationsRequest;
.super Lcom/google/api/client/json/GenericJson;
.source "GetSituationsRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/cloudclient/GetSituationsRequest$RequestSignals;
    }
.end annotation


# instance fields
.field public mRequestSignals:Lcom/google/android/music/cloudclient/GetSituationsRequest$RequestSignals;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "requestSignals"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    .line 19
    return-void
.end method

.method public static serialize(I)[B
    .locals 2
    .param p0, "timeZoneOffsetSecs"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 25
    new-instance v0, Lcom/google/android/music/cloudclient/GetSituationsRequest;

    invoke-direct {v0}, Lcom/google/android/music/cloudclient/GetSituationsRequest;-><init>()V

    .line 26
    .local v0, "request":Lcom/google/android/music/cloudclient/GetSituationsRequest;
    new-instance v1, Lcom/google/android/music/cloudclient/GetSituationsRequest$RequestSignals;

    invoke-direct {v1}, Lcom/google/android/music/cloudclient/GetSituationsRequest$RequestSignals;-><init>()V

    iput-object v1, v0, Lcom/google/android/music/cloudclient/GetSituationsRequest;->mRequestSignals:Lcom/google/android/music/cloudclient/GetSituationsRequest$RequestSignals;

    .line 27
    iget-object v1, v0, Lcom/google/android/music/cloudclient/GetSituationsRequest;->mRequestSignals:Lcom/google/android/music/cloudclient/GetSituationsRequest$RequestSignals;

    iput p0, v1, Lcom/google/android/music/cloudclient/GetSituationsRequest$RequestSignals;->mTimeZoneOffsetSecs:I

    .line 28
    invoke-static {v0}, Lcom/google/android/music/cloudclient/JsonUtils;->toJsonByteArray(Lcom/google/api/client/json/GenericJson;)[B

    move-result-object v1

    return-object v1
.end method
