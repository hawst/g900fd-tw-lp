.class public Lcom/google/android/music/sync/google/model/BatchMutateResponse$MutateResponse;
.super Ljava/lang/Object;
.source "BatchMutateResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/sync/google/model/BatchMutateResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MutateResponse"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/sync/google/model/BatchMutateResponse$MutateResponse$ResponseCode;
    }
.end annotation


# instance fields
.field public mId:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "id"
    .end annotation
.end field

.field public mResponseCode:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "response_code"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    return-void
.end method


# virtual methods
.method public throwExceptionFromResponseCode()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/sync/api/ConflictException;,
            Lcom/google/android/music/sync/api/BadRequestException;
        }
    .end annotation

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/music/sync/google/model/BatchMutateResponse$MutateResponse;->mResponseCode:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 44
    new-instance v0, Lcom/google/android/music/store/InvalidDataException;

    const-string v1, "Response code is null"

    invoke-direct {v0, v1}, Lcom/google/android/music/store/InvalidDataException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 47
    :cond_0
    sget-object v0, Lcom/google/android/music/sync/google/model/BatchMutateResponse$MutateResponse$ResponseCode;->CONFLICT:Lcom/google/android/music/sync/google/model/BatchMutateResponse$MutateResponse$ResponseCode;

    invoke-virtual {v0}, Lcom/google/android/music/sync/google/model/BatchMutateResponse$MutateResponse$ResponseCode;->name()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/sync/google/model/BatchMutateResponse$MutateResponse;->mResponseCode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 48
    new-instance v0, Lcom/google/android/music/sync/api/ConflictException;

    invoke-direct {v0}, Lcom/google/android/music/sync/api/ConflictException;-><init>()V

    throw v0

    .line 51
    :cond_1
    sget-object v0, Lcom/google/android/music/sync/google/model/BatchMutateResponse$MutateResponse$ResponseCode;->INVALID_REQUEST:Lcom/google/android/music/sync/google/model/BatchMutateResponse$MutateResponse$ResponseCode;

    invoke-virtual {v0}, Lcom/google/android/music/sync/google/model/BatchMutateResponse$MutateResponse$ResponseCode;->name()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/sync/google/model/BatchMutateResponse$MutateResponse;->mResponseCode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 52
    new-instance v0, Lcom/google/android/music/sync/api/BadRequestException;

    const-string v1, "INVALID_REQUEST"

    invoke-direct {v0, v1}, Lcom/google/android/music/sync/api/BadRequestException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 55
    :cond_2
    sget-object v0, Lcom/google/android/music/sync/google/model/BatchMutateResponse$MutateResponse$ResponseCode;->TOO_MANY_ITEMS:Lcom/google/android/music/sync/google/model/BatchMutateResponse$MutateResponse$ResponseCode;

    invoke-virtual {v0}, Lcom/google/android/music/sync/google/model/BatchMutateResponse$MutateResponse$ResponseCode;->name()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/sync/google/model/BatchMutateResponse$MutateResponse;->mResponseCode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 56
    new-instance v0, Lcom/google/android/music/sync/api/BadRequestException;

    const-string v1, "TOO_MANY_ITEMS"

    invoke-direct {v0, v1}, Lcom/google/android/music/sync/api/BadRequestException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 58
    :cond_3
    return-void
.end method
