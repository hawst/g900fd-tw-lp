.class public Lcom/google/android/music/xdi/XdiContentProvider;
.super Landroid/content/ContentProvider;
.source "XdiContentProvider.java"


# static fields
.field static final BASE_URI:Landroid/net/Uri;

.field static final DEFAULT_BROWSE_HEADERS_PROJECTION:[Ljava/lang/String;

.field static final DEFAULT_BROWSE_HEADER_ID_PROJECTION:[Ljava/lang/String;

.field static final DEFAULT_DETAIL_ACTIONS_PROJECTION:[Ljava/lang/String;

.field static final DEFAULT_DETAIL_BLURB_PROJECTION:[Ljava/lang/String;

.field static final DEFAULT_DETAIL_CHILDREN_PROJECTION:[Ljava/lang/String;

.field static final DEFAULT_DETAIL_ITEM_PROJECTION:[Ljava/lang/String;

.field static final DEFAULT_DETAIL_SECTIONS_PROJECTION:[Ljava/lang/String;

.field static final DEFAULT_LAUNCHER_CLUSTER_PROJECTION:[Ljava/lang/String;

.field static final DEFAULT_LAUNCHER_ROOT_PROJECTION:[Ljava/lang/String;

.field static final DEFAULT_SEARCH_RESULTS_PROJECTION:[Ljava/lang/String;

.field private static final LOGV:Z

.field static final META_PROJECTION:[Ljava/lang/String;

.field private static final sUriMatcher:Landroid/content/UriMatcher;


# instance fields
.field private mMusicContentObserver:Landroid/database/ContentObserver;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x3

    const/4 v3, 0x2

    .line 33
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->XDI:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/xdi/XdiContentProvider;->LOGV:Z

    .line 37
    const-string v0, "content://com.google.android.music.xdi"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/xdi/XdiContentProvider;->BASE_URI:Landroid/net/Uri;

    .line 283
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v5

    const-string v1, "name"

    aput-object v1, v0, v6

    const-string v1, "importance"

    aput-object v1, v0, v3

    const-string v1, "display_name"

    aput-object v1, v0, v4

    const-string v1, "visible_count"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "cache_time_ms"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "image_crop_allowed"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "browse_items_uri"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "intent_uri"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "notification_text"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/xdi/XdiContentProvider;->DEFAULT_LAUNCHER_ROOT_PROJECTION:[Ljava/lang/String;

    .line 297
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v5

    const-string v1, "parent_id"

    aput-object v1, v0, v6

    const-string v1, "image_uri"

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/music/xdi/XdiContentProvider;->DEFAULT_LAUNCHER_CLUSTER_PROJECTION:[Ljava/lang/String;

    .line 304
    const/16 v0, 0xc

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v5

    const-string v1, "name"

    aput-object v1, v0, v6

    const-string v1, "display_name"

    aput-object v1, v0, v3

    const-string v1, "background_image_uri"

    aput-object v1, v0, v4

    const-string v1, "bg_image_uri"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "icon_uri"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "default_item_width"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "default_item_height"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "color_hint"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "badge_uri"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "items_uri"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "items_per_page"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/xdi/XdiContentProvider;->DEFAULT_BROWSE_HEADERS_PROJECTION:[Ljava/lang/String;

    .line 320
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v5

    const-string v1, "parent_id"

    aput-object v1, v0, v6

    const-string v1, "display_name"

    aput-object v1, v0, v3

    const-string v1, "display_description"

    aput-object v1, v0, v4

    const-string v1, "image_uri"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "width"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "height"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "intent_uri"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/xdi/XdiContentProvider;->DEFAULT_BROWSE_HEADER_ID_PROJECTION:[Ljava/lang/String;

    .line 341
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v5

    const-string v1, "foreground_image_uri"

    aput-object v1, v0, v6

    const-string v1, "background_image_uri"

    aput-object v1, v0, v3

    const-string v1, "badge_uri"

    aput-object v1, v0, v4

    const-string v1, "color_hint"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "text_color_hint"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/xdi/XdiContentProvider;->DEFAULT_DETAIL_ITEM_PROJECTION:[Ljava/lang/String;

    .line 351
    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v5

    const-string v1, "name"

    aput-object v1, v0, v6

    const-string v1, "display_header"

    aput-object v1, v0, v3

    const-string v1, "display_name"

    aput-object v1, v0, v4

    const-string v1, "display_subname"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "section_type"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "blob_content"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "content_uri"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "user_rating_custom"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "user_rating"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "user_rating_count"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/xdi/XdiContentProvider;->DEFAULT_DETAIL_SECTIONS_PROJECTION:[Ljava/lang/String;

    .line 366
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v5

    const-string v1, "display_name"

    aput-object v1, v0, v6

    const-string v1, "display_subname"

    aput-object v1, v0, v3

    const-string v1, "display_description"

    aput-object v1, v0, v4

    const-string v1, "display_category"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "user_rating_count"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "user_rating"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "user_rating_custom"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "action_uri"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "display_date"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/xdi/XdiContentProvider;->DEFAULT_DETAIL_BLURB_PROJECTION:[Ljava/lang/String;

    .line 380
    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v5

    const-string v1, "display_name"

    aput-object v1, v0, v6

    const-string v1, "display_subname"

    aput-object v1, v0, v3

    const-string v1, "display_description"

    aput-object v1, v0, v4

    const-string v1, "display_number"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "image_uri"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "item_display_type"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "user_rating_count"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "user_rating"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "action_uri"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "display_length"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/xdi/XdiContentProvider;->DEFAULT_DETAIL_CHILDREN_PROJECTION:[Ljava/lang/String;

    .line 395
    new-array v0, v7, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v5

    const-string v1, "display_name"

    aput-object v1, v0, v6

    const-string v1, "display_subname"

    aput-object v1, v0, v3

    const-string v1, "intent_uri"

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/music/xdi/XdiContentProvider;->DEFAULT_DETAIL_ACTIONS_PROJECTION:[Ljava/lang/String;

    .line 403
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v5

    const-string v1, "display_name"

    aput-object v1, v0, v6

    const-string v1, "results_uri"

    aput-object v1, v0, v3

    const-string v1, "default_item_width"

    aput-object v1, v0, v4

    const-string v1, "default_item_height"

    aput-object v1, v0, v7

    sput-object v0, Lcom/google/android/music/xdi/XdiContentProvider;->DEFAULT_SEARCH_RESULTS_PROJECTION:[Ljava/lang/String;

    .line 412
    new-array v0, v7, [Ljava/lang/String;

    const-string v1, "background_image_uri"

    aput-object v1, v0, v5

    const-string v1, "badge_uri"

    aput-object v1, v0, v6

    const-string v1, "color_hint"

    aput-object v1, v0, v3

    const-string v1, "activity_title"

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/music/xdi/XdiContentProvider;->META_PROJECTION:[Ljava/lang/String;

    .line 419
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    sput-object v0, Lcom/google/android/music/xdi/XdiContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    .line 422
    sget-object v0, Lcom/google/android/music/xdi/XdiContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.xdi"

    const-string v2, "launcher"

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 423
    sget-object v0, Lcom/google/android/music/xdi/XdiContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.xdi"

    const-string v2, "launcher/items/"

    invoke-virtual {v0, v1, v2, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 424
    sget-object v0, Lcom/google/android/music/xdi/XdiContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.xdi"

    const-string v2, "launcher/items/#"

    invoke-virtual {v0, v1, v2, v7}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 426
    sget-object v0, Lcom/google/android/music/xdi/XdiContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.xdi"

    const-string v2, "browse/headers"

    const/4 v3, 0x5

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 427
    sget-object v0, Lcom/google/android/music/xdi/XdiContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.xdi"

    const-string v2, "browse/#"

    const/4 v3, 0x6

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 429
    sget-object v0, Lcom/google/android/music/xdi/XdiContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.xdi"

    const-string v2, "mymusic/headers"

    const/4 v3, 0x7

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 430
    sget-object v0, Lcom/google/android/music/xdi/XdiContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.xdi"

    const-string v2, "mymusic/#"

    const/16 v3, 0x8

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 431
    sget-object v0, Lcom/google/android/music/xdi/XdiContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.xdi"

    const-string v2, "myartists"

    const/16 v3, 0x24

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 432
    sget-object v0, Lcom/google/android/music/xdi/XdiContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.xdi"

    const-string v2, "myartists/#"

    const/16 v3, 0x25

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 433
    sget-object v0, Lcom/google/android/music/xdi/XdiContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.xdi"

    const-string v2, "mygenres/headers"

    const/16 v3, 0x9

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 434
    sget-object v0, Lcom/google/android/music/xdi/XdiContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.xdi"

    const-string v2, "mygenres/#"

    const/16 v3, 0xa

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 435
    sget-object v0, Lcom/google/android/music/xdi/XdiContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.xdi"

    const-string v2, "myalbums"

    const/16 v3, 0x26

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 436
    sget-object v0, Lcom/google/android/music/xdi/XdiContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.xdi"

    const-string v2, "myalbums/#"

    const/16 v3, 0x27

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 437
    sget-object v0, Lcom/google/android/music/xdi/XdiContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.xdi"

    const-string v2, "radio/headers"

    const/16 v3, 0x20

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 438
    sget-object v0, Lcom/google/android/music/xdi/XdiContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.xdi"

    const-string v2, "radio/#"

    const/16 v3, 0x21

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 439
    sget-object v0, Lcom/google/android/music/xdi/XdiContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.xdi"

    const-string v2, "instantmixes/headers"

    const/16 v3, 0x22

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 441
    sget-object v0, Lcom/google/android/music/xdi/XdiContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.xdi"

    const-string v2, "instantmixes/#"

    const/16 v3, 0x23

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 444
    sget-object v0, Lcom/google/android/music/xdi/XdiContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.xdi"

    const-string v2, "details/albums/*"

    const/16 v3, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 445
    sget-object v0, Lcom/google/android/music/xdi/XdiContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.xdi"

    const-string v2, "details/albums/*/sections"

    const/16 v3, 0x65

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 446
    sget-object v0, Lcom/google/android/music/xdi/XdiContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.xdi"

    const-string v2, "details/albums/*/section/album"

    const/16 v3, 0x66

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 448
    sget-object v0, Lcom/google/android/music/xdi/XdiContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.xdi"

    const-string v2, "details/albums/*/section/tracks"

    const/16 v3, 0x67

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 450
    sget-object v0, Lcom/google/android/music/xdi/XdiContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.xdi"

    const-string v2, "details/albums/*/section/album/actions"

    const/16 v3, 0x68

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 453
    sget-object v0, Lcom/google/android/music/xdi/XdiContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.xdi"

    const-string v2, "details/playlists/#"

    const/16 v3, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 454
    sget-object v0, Lcom/google/android/music/xdi/XdiContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.xdi"

    const-string v2, "details/playlists/#/sections"

    const/16 v3, 0xc9

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 456
    sget-object v0, Lcom/google/android/music/xdi/XdiContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.xdi"

    const-string v2, "details/playlists/#/section/tracks"

    const/16 v3, 0xcb

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 458
    sget-object v0, Lcom/google/android/music/xdi/XdiContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.xdi"

    const-string v2, "details/playlists/#/section/playlist"

    const/16 v3, 0xca

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 460
    sget-object v0, Lcom/google/android/music/xdi/XdiContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.xdi"

    const-string v2, "details/playlists/#/section/playlist/actions"

    const/16 v3, 0xcc

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 463
    sget-object v0, Lcom/google/android/music/xdi/XdiContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.xdi"

    const-string v2, "details/artists/*"

    const/16 v3, 0x12c

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 464
    sget-object v0, Lcom/google/android/music/xdi/XdiContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.xdi"

    const-string v2, "details/artists/*/sections"

    const/16 v3, 0x12d

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 465
    sget-object v0, Lcom/google/android/music/xdi/XdiContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.xdi"

    const-string v2, "details/artists/*/section/artist"

    const/16 v3, 0x12e

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 467
    sget-object v0, Lcom/google/android/music/xdi/XdiContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.xdi"

    const-string v2, "details/artists/*/section/albums"

    const/16 v3, 0x12f

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 469
    sget-object v0, Lcom/google/android/music/xdi/XdiContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.xdi"

    const-string v2, "details/artists/*/section/related"

    const/16 v3, 0x130

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 471
    sget-object v0, Lcom/google/android/music/xdi/XdiContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.xdi"

    const-string v2, "details/artists/*/section/artist/actions"

    const/16 v3, 0x131

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 473
    sget-object v0, Lcom/google/android/music/xdi/XdiContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.xdi"

    const-string v2, "details/artists/*/topsongs"

    const/16 v3, 0x132

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 474
    sget-object v0, Lcom/google/android/music/xdi/XdiContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.xdi"

    const-string v2, "details/artists/*/lockersongs"

    const/16 v3, 0x133

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 477
    sget-object v0, Lcom/google/android/music/xdi/XdiContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.xdi"

    const-string v2, "search"

    const/16 v3, 0xb

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 478
    sget-object v0, Lcom/google/android/music/xdi/XdiContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.xdi"

    const-string v2, "search/headers/#"

    const/16 v3, 0xc

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 480
    sget-object v0, Lcom/google/android/music/xdi/XdiContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.xdi"

    const-string v2, "explore/headers"

    const/16 v3, 0xd

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 481
    sget-object v0, Lcom/google/android/music/xdi/XdiContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.xdi"

    const-string v2, "explore/#"

    const/16 v3, 0xe

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 482
    sget-object v0, Lcom/google/android/music/xdi/XdiContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.xdi"

    const-string v2, "explore/featured/headers"

    const/16 v3, 0xf

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 483
    sget-object v0, Lcom/google/android/music/xdi/XdiContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.xdi"

    const-string v2, "explore/featured/*/*"

    const/16 v3, 0x10

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 484
    sget-object v0, Lcom/google/android/music/xdi/XdiContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.xdi"

    const-string v2, "explore/recommendations/headers"

    const/16 v3, 0x11

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 486
    sget-object v0, Lcom/google/android/music/xdi/XdiContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.xdi"

    const-string v2, "explore/recommendations/*/*"

    const/16 v3, 0x12

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 488
    sget-object v0, Lcom/google/android/music/xdi/XdiContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.xdi"

    const-string v2, "explore/newreleases/headers"

    const/16 v3, 0x13

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 490
    sget-object v0, Lcom/google/android/music/xdi/XdiContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.xdi"

    const-string v2, "explore/newreleases/*/*"

    const/16 v3, 0x14

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 492
    sget-object v0, Lcom/google/android/music/xdi/XdiContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.xdi"

    const-string v2, "explore/genre/radio/items/*/*/*"

    const/16 v3, 0x1f

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 493
    sget-object v0, Lcom/google/android/music/xdi/XdiContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.xdi"

    const-string v2, "explore/genres/*/*/headers"

    const/16 v3, 0x15

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 494
    sget-object v0, Lcom/google/android/music/xdi/XdiContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.xdi"

    const-string v2, "explore/genres/*/*"

    const/16 v3, 0x16

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 495
    sget-object v0, Lcom/google/android/music/xdi/XdiContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.xdi"

    const-string v2, "explore/genre/featured/items/*/*/*"

    const/16 v3, 0x1a

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 497
    sget-object v0, Lcom/google/android/music/xdi/XdiContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.xdi"

    const-string v2, "explore/genre/newreleases/items/*/*/*"

    const/16 v3, 0x1c

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 499
    sget-object v0, Lcom/google/android/music/xdi/XdiContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.xdi"

    const-string v2, "explore/genre/featured/*/headers"

    const/16 v3, 0x19

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 501
    sget-object v0, Lcom/google/android/music/xdi/XdiContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.xdi"

    const-string v2, "explore/genre/newreleases/*/headers"

    const/16 v3, 0x1b

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 503
    sget-object v0, Lcom/google/android/music/xdi/XdiContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.xdi"

    const-string v2, "explore/genre/items/*/*"

    const/16 v3, 0x18

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 504
    sget-object v0, Lcom/google/android/music/xdi/XdiContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.xdi"

    const-string v2, "explore/genre/*/*/*/headers"

    const/16 v3, 0x17

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 506
    sget-object v0, Lcom/google/android/music/xdi/XdiContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.xdi"

    const-string v2, "meta/#"

    const/16 v3, 0x1d

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 507
    sget-object v0, Lcom/google/android/music/xdi/XdiContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.music.xdi"

    const-string v2, "metatitle/*"

    const/16 v3, 0x1e

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 508
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    return-void
.end method

.method public static notifyChange(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 1082
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    .line 1083
    return-void
.end method

.method public static notifyChangeToPano(Landroid/content/Context;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1062
    sget-object v0, Lcom/google/android/music/xdi/XdiContentProvider;->BASE_URI:Landroid/net/Uri;

    invoke-static {p0, v0}, Lcom/google/android/music/xdi/XdiContentProvider;->notifyChange(Landroid/content/Context;Landroid/net/Uri;)V

    .line 1063
    invoke-static {p0}, Lcom/google/android/music/xdi/XdiContentProvider;->notifyClustersDataChanged(Landroid/content/Context;)V

    .line 1064
    invoke-static {p0}, Lcom/google/android/music/xdi/XdiContentProvider;->notifyHeadersDataChanged(Landroid/content/Context;)V

    .line 1065
    return-void
.end method

.method public static notifyClustersDataChanged(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1069
    sget-object v0, Lcom/google/android/music/xdi/XdiContentProvider;->BASE_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "launcher"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/music/xdi/XdiContentProvider;->notifyChange(Landroid/content/Context;Landroid/net/Uri;)V

    .line 1071
    sget-object v0, Lcom/google/android/music/xdi/XdiContentProvider;->BASE_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "launcher/items/"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/music/xdi/XdiContentProvider;->notifyChange(Landroid/content/Context;Landroid/net/Uri;)V

    .line 1073
    return-void
.end method

.method public static notifyHeadersDataChanged(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1077
    sget-object v0, Lcom/google/android/music/xdi/XdiContentProvider;->BASE_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "browse"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/music/xdi/XdiContentProvider;->notifyChange(Landroid/content/Context;Landroid/net/Uri;)V

    .line 1079
    return-void
.end method

.method private queryImpl(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 36
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 584
    sget-object v6, Lcom/google/android/music/xdi/XdiContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    move-object/from16 v0, p1

    invoke-virtual {v6, v0}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v32

    .line 585
    .local v32, "match":I
    const-string v6, "MusicXdi"

    const/4 v7, 0x2

    invoke-static {v6, v7}, Lcom/google/android/music/log/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 586
    const-string v6, "MusicXdi"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Queried: "

    invoke-virtual {v7, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v12, "; match: "

    invoke-virtual {v7, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move/from16 v0, v32

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v12, "; projection: "

    invoke-virtual {v7, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static/range {p2 .. p2}, Ljava/util/Arrays;->deepToString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v7, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 589
    :cond_0
    const/4 v5, 0x0

    .line 591
    .local v5, "c":Landroid/database/Cursor;
    sparse-switch v32, :sswitch_data_0

    .line 1026
    sget-boolean v6, Lcom/google/android/music/xdi/XdiContentProvider;->LOGV:Z

    if-eqz v6, :cond_1

    .line 1027
    const-string v6, "MusicXdi"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Skipping unmatched URI: "

    invoke-virtual {v7, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v7, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1032
    :cond_1
    :goto_0
    const-string v6, "MusicXdi"

    const/4 v7, 0x2

    invoke-static {v6, v7}, Lcom/google/android/music/log/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1033
    const-string v6, "MusicXdi"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Cursor: "

    invoke-virtual {v7, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {v5}, Landroid/database/DatabaseUtils;->dumpCursorToString(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v7, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 1035
    :cond_2
    return-object v5

    .line 594
    :sswitch_0
    if-nez p2, :cond_3

    sget-object p2, Lcom/google/android/music/xdi/XdiContentProvider;->DEFAULT_LAUNCHER_ROOT_PROJECTION:[Ljava/lang/String;

    .line 595
    :cond_3
    new-instance v5, Lcom/google/android/music/xdi/LauncherRootCursor;

    .end local v5    # "c":Landroid/database/Cursor;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/xdi/XdiContentProvider;->getContext()Landroid/content/Context;

    move-result-object v6

    move-object/from16 v0, p2

    invoke-direct {v5, v6, v0}, Lcom/google/android/music/xdi/LauncherRootCursor;-><init>(Landroid/content/Context;[Ljava/lang/String;)V

    .line 596
    .restart local v5    # "c":Landroid/database/Cursor;
    goto :goto_0

    .line 600
    :sswitch_1
    if-nez p2, :cond_4

    sget-object p2, Lcom/google/android/music/xdi/XdiContentProvider;->DEFAULT_LAUNCHER_CLUSTER_PROJECTION:[Ljava/lang/String;

    .line 601
    :cond_4
    new-instance v5, Lcom/google/android/music/xdi/LauncherClustersCursor;

    .end local v5    # "c":Landroid/database/Cursor;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/xdi/XdiContentProvider;->getContext()Landroid/content/Context;

    move-result-object v6

    move-object/from16 v0, p2

    invoke-direct {v5, v6, v0}, Lcom/google/android/music/xdi/LauncherClustersCursor;-><init>(Landroid/content/Context;[Ljava/lang/String;)V

    .line 602
    .restart local v5    # "c":Landroid/database/Cursor;
    goto :goto_0

    .line 606
    :sswitch_2
    if-nez p2, :cond_5

    sget-object p2, Lcom/google/android/music/xdi/XdiContentProvider;->DEFAULT_LAUNCHER_CLUSTER_PROJECTION:[Ljava/lang/String;

    .line 607
    :cond_5
    invoke-static/range {p1 .. p1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v30

    .line 608
    .local v30, "clusterId":J
    new-instance v5, Lcom/google/android/music/xdi/LauncherClustersCursor;

    .end local v5    # "c":Landroid/database/Cursor;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/xdi/XdiContentProvider;->getContext()Landroid/content/Context;

    move-result-object v6

    move-object/from16 v0, p2

    move-wide/from16 v1, v30

    invoke-direct {v5, v6, v0, v1, v2}, Lcom/google/android/music/xdi/LauncherClustersCursor;-><init>(Landroid/content/Context;[Ljava/lang/String;J)V

    .line 609
    .restart local v5    # "c":Landroid/database/Cursor;
    goto :goto_0

    .line 613
    .end local v30    # "clusterId":J
    :sswitch_3
    if-nez p2, :cond_6

    sget-object p2, Lcom/google/android/music/xdi/XdiContentProvider;->DEFAULT_BROWSE_HEADERS_PROJECTION:[Ljava/lang/String;

    .line 614
    :cond_6
    new-instance v5, Lcom/google/android/music/xdi/RootHeadersCursor;

    .end local v5    # "c":Landroid/database/Cursor;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/xdi/XdiContentProvider;->getContext()Landroid/content/Context;

    move-result-object v6

    move-object/from16 v0, p2

    invoke-direct {v5, v6, v0}, Lcom/google/android/music/xdi/RootHeadersCursor;-><init>(Landroid/content/Context;[Ljava/lang/String;)V

    .line 615
    .restart local v5    # "c":Landroid/database/Cursor;
    goto :goto_0

    .line 619
    :sswitch_4
    if-nez p2, :cond_7

    sget-object p2, Lcom/google/android/music/xdi/XdiContentProvider;->DEFAULT_BROWSE_HEADER_ID_PROJECTION:[Ljava/lang/String;

    .line 620
    :cond_7
    invoke-static/range {p1 .. p1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v8

    .line 621
    .local v8, "headerId":J
    new-instance v5, Lcom/google/android/music/xdi/RootItemsCursor;

    .end local v5    # "c":Landroid/database/Cursor;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/xdi/XdiContentProvider;->getContext()Landroid/content/Context;

    move-result-object v6

    move-object/from16 v0, p2

    invoke-direct {v5, v6, v0, v8, v9}, Lcom/google/android/music/xdi/RootItemsCursor;-><init>(Landroid/content/Context;[Ljava/lang/String;J)V

    .line 622
    .restart local v5    # "c":Landroid/database/Cursor;
    goto :goto_0

    .line 626
    .end local v8    # "headerId":J
    :sswitch_5
    if-nez p2, :cond_8

    sget-object p2, Lcom/google/android/music/xdi/XdiContentProvider;->DEFAULT_BROWSE_HEADERS_PROJECTION:[Ljava/lang/String;

    .line 627
    :cond_8
    new-instance v5, Lcom/google/android/music/xdi/MyMusicHeadersCursor;

    .end local v5    # "c":Landroid/database/Cursor;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/xdi/XdiContentProvider;->getContext()Landroid/content/Context;

    move-result-object v6

    move-object/from16 v0, p2

    invoke-direct {v5, v6, v0}, Lcom/google/android/music/xdi/MyMusicHeadersCursor;-><init>(Landroid/content/Context;[Ljava/lang/String;)V

    .line 628
    .restart local v5    # "c":Landroid/database/Cursor;
    goto/16 :goto_0

    .line 632
    :sswitch_6
    const-string v6, "start_index"

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 633
    .local v10, "startIndex":Ljava/lang/String;
    const-string v6, "fetch_count"

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 634
    .local v11, "fetchCount":Ljava/lang/String;
    if-nez p2, :cond_9

    sget-object p2, Lcom/google/android/music/xdi/XdiContentProvider;->DEFAULT_BROWSE_HEADER_ID_PROJECTION:[Ljava/lang/String;

    .line 635
    :cond_9
    invoke-static/range {p1 .. p1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v8

    .line 636
    .restart local v8    # "headerId":J
    new-instance v5, Lcom/google/android/music/xdi/MyMusicItemsCursor;

    .end local v5    # "c":Landroid/database/Cursor;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/xdi/XdiContentProvider;->getContext()Landroid/content/Context;

    move-result-object v6

    move-object/from16 v7, p2

    invoke-direct/range {v5 .. v11}, Lcom/google/android/music/xdi/MyMusicItemsCursor;-><init>(Landroid/content/Context;[Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)V

    .line 638
    .restart local v5    # "c":Landroid/database/Cursor;
    goto/16 :goto_0

    .line 642
    .end local v8    # "headerId":J
    .end local v10    # "startIndex":Ljava/lang/String;
    .end local v11    # "fetchCount":Ljava/lang/String;
    :sswitch_7
    if-nez p2, :cond_a

    sget-object p2, Lcom/google/android/music/xdi/XdiContentProvider;->DEFAULT_BROWSE_HEADERS_PROJECTION:[Ljava/lang/String;

    .line 643
    :cond_a
    new-instance v5, Lcom/google/android/music/xdi/RadioHeadersCursor;

    .end local v5    # "c":Landroid/database/Cursor;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/xdi/XdiContentProvider;->getContext()Landroid/content/Context;

    move-result-object v6

    move-object/from16 v0, p2

    invoke-direct {v5, v6, v0}, Lcom/google/android/music/xdi/RadioHeadersCursor;-><init>(Landroid/content/Context;[Ljava/lang/String;)V

    .line 644
    .restart local v5    # "c":Landroid/database/Cursor;
    goto/16 :goto_0

    .line 649
    :sswitch_8
    if-nez p2, :cond_b

    sget-object p2, Lcom/google/android/music/xdi/XdiContentProvider;->DEFAULT_BROWSE_HEADER_ID_PROJECTION:[Ljava/lang/String;

    .line 650
    :cond_b
    invoke-static/range {p1 .. p1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v8

    .line 651
    .restart local v8    # "headerId":J
    new-instance v5, Lcom/google/android/music/xdi/RadioItemsCursor;

    .end local v5    # "c":Landroid/database/Cursor;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/xdi/XdiContentProvider;->getContext()Landroid/content/Context;

    move-result-object v6

    move-object/from16 v0, p2

    invoke-direct {v5, v6, v0, v8, v9}, Lcom/google/android/music/xdi/RadioItemsCursor;-><init>(Landroid/content/Context;[Ljava/lang/String;J)V

    .line 652
    .restart local v5    # "c":Landroid/database/Cursor;
    goto/16 :goto_0

    .line 656
    .end local v8    # "headerId":J
    :sswitch_9
    if-nez p2, :cond_c

    sget-object p2, Lcom/google/android/music/xdi/XdiContentProvider;->DEFAULT_BROWSE_HEADERS_PROJECTION:[Ljava/lang/String;

    .line 657
    :cond_c
    new-instance v5, Lcom/google/android/music/xdi/InstantMixesHeadersCursor;

    .end local v5    # "c":Landroid/database/Cursor;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/xdi/XdiContentProvider;->getContext()Landroid/content/Context;

    move-result-object v6

    move-object/from16 v0, p2

    invoke-direct {v5, v6, v0}, Lcom/google/android/music/xdi/InstantMixesHeadersCursor;-><init>(Landroid/content/Context;[Ljava/lang/String;)V

    .line 658
    .restart local v5    # "c":Landroid/database/Cursor;
    goto/16 :goto_0

    .line 662
    :sswitch_a
    if-nez p2, :cond_d

    sget-object p2, Lcom/google/android/music/xdi/XdiContentProvider;->DEFAULT_BROWSE_HEADERS_PROJECTION:[Ljava/lang/String;

    .line 663
    :cond_d
    new-instance v5, Lcom/google/android/music/xdi/MyArtistsHeadersCursor;

    .end local v5    # "c":Landroid/database/Cursor;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/xdi/XdiContentProvider;->getContext()Landroid/content/Context;

    move-result-object v6

    move-object/from16 v0, p2

    invoke-direct {v5, v6, v0}, Lcom/google/android/music/xdi/MyArtistsHeadersCursor;-><init>(Landroid/content/Context;[Ljava/lang/String;)V

    .line 664
    .restart local v5    # "c":Landroid/database/Cursor;
    goto/16 :goto_0

    .line 668
    :sswitch_b
    if-nez p2, :cond_e

    sget-object p2, Lcom/google/android/music/xdi/XdiContentProvider;->DEFAULT_BROWSE_HEADER_ID_PROJECTION:[Ljava/lang/String;

    .line 669
    :cond_e
    new-instance v5, Lcom/google/android/music/xdi/MyArtistsItemsCursor;

    .end local v5    # "c":Landroid/database/Cursor;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/xdi/XdiContentProvider;->getContext()Landroid/content/Context;

    move-result-object v6

    move-object/from16 v0, p2

    invoke-direct {v5, v6, v0}, Lcom/google/android/music/xdi/MyArtistsItemsCursor;-><init>(Landroid/content/Context;[Ljava/lang/String;)V

    .line 670
    .restart local v5    # "c":Landroid/database/Cursor;
    goto/16 :goto_0

    .line 674
    :sswitch_c
    if-nez p2, :cond_f

    sget-object p2, Lcom/google/android/music/xdi/XdiContentProvider;->DEFAULT_BROWSE_HEADERS_PROJECTION:[Ljava/lang/String;

    .line 675
    :cond_f
    new-instance v5, Lcom/google/android/music/xdi/MyGenresHeadersCursor;

    .end local v5    # "c":Landroid/database/Cursor;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/xdi/XdiContentProvider;->getContext()Landroid/content/Context;

    move-result-object v6

    move-object/from16 v0, p2

    invoke-direct {v5, v6, v0}, Lcom/google/android/music/xdi/MyGenresHeadersCursor;-><init>(Landroid/content/Context;[Ljava/lang/String;)V

    .line 676
    .restart local v5    # "c":Landroid/database/Cursor;
    goto/16 :goto_0

    .line 680
    :sswitch_d
    if-nez p2, :cond_10

    sget-object p2, Lcom/google/android/music/xdi/XdiContentProvider;->DEFAULT_BROWSE_HEADER_ID_PROJECTION:[Ljava/lang/String;

    .line 681
    :cond_10
    invoke-static/range {p1 .. p1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v8

    .line 682
    .restart local v8    # "headerId":J
    new-instance v5, Lcom/google/android/music/xdi/MyGenresItemsCursor;

    .end local v5    # "c":Landroid/database/Cursor;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/xdi/XdiContentProvider;->getContext()Landroid/content/Context;

    move-result-object v6

    move-object/from16 v0, p2

    invoke-direct {v5, v6, v0, v8, v9}, Lcom/google/android/music/xdi/MyGenresItemsCursor;-><init>(Landroid/content/Context;[Ljava/lang/String;J)V

    .line 683
    .restart local v5    # "c":Landroid/database/Cursor;
    goto/16 :goto_0

    .line 687
    .end local v8    # "headerId":J
    :sswitch_e
    if-nez p2, :cond_11

    sget-object p2, Lcom/google/android/music/xdi/XdiContentProvider;->DEFAULT_BROWSE_HEADERS_PROJECTION:[Ljava/lang/String;

    .line 688
    :cond_11
    new-instance v5, Lcom/google/android/music/xdi/MyAlbumsHeadersCursor;

    .end local v5    # "c":Landroid/database/Cursor;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/xdi/XdiContentProvider;->getContext()Landroid/content/Context;

    move-result-object v6

    move-object/from16 v0, p2

    invoke-direct {v5, v6, v0}, Lcom/google/android/music/xdi/MyAlbumsHeadersCursor;-><init>(Landroid/content/Context;[Ljava/lang/String;)V

    .line 689
    .restart local v5    # "c":Landroid/database/Cursor;
    goto/16 :goto_0

    .line 693
    :sswitch_f
    if-nez p2, :cond_12

    sget-object p2, Lcom/google/android/music/xdi/XdiContentProvider;->DEFAULT_BROWSE_HEADER_ID_PROJECTION:[Ljava/lang/String;

    .line 694
    :cond_12
    new-instance v5, Lcom/google/android/music/xdi/MyAlbumsItemsCursor;

    .end local v5    # "c":Landroid/database/Cursor;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/xdi/XdiContentProvider;->getContext()Landroid/content/Context;

    move-result-object v6

    move-object/from16 v0, p2

    invoke-direct {v5, v6, v0}, Lcom/google/android/music/xdi/MyAlbumsItemsCursor;-><init>(Landroid/content/Context;[Ljava/lang/String;)V

    .line 695
    .restart local v5    # "c":Landroid/database/Cursor;
    goto/16 :goto_0

    .line 699
    :sswitch_10
    if-nez p2, :cond_13

    sget-object p2, Lcom/google/android/music/xdi/XdiContentProvider;->DEFAULT_SEARCH_RESULTS_PROJECTION:[Ljava/lang/String;

    .line 700
    :cond_13
    const-string v6, "q"

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 701
    .local v15, "query":Ljava/lang/String;
    new-instance v5, Lcom/google/android/music/xdi/SearchHeadersCursor;

    .end local v5    # "c":Landroid/database/Cursor;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/xdi/XdiContentProvider;->getContext()Landroid/content/Context;

    move-result-object v6

    move-object/from16 v0, p2

    invoke-direct {v5, v6, v0, v15}, Lcom/google/android/music/xdi/SearchHeadersCursor;-><init>(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;)V

    .line 702
    .restart local v5    # "c":Landroid/database/Cursor;
    goto/16 :goto_0

    .line 706
    .end local v15    # "query":Ljava/lang/String;
    :sswitch_11
    if-nez p2, :cond_14

    sget-object p2, Lcom/google/android/music/xdi/XdiContentProvider;->DEFAULT_BROWSE_HEADER_ID_PROJECTION:[Ljava/lang/String;

    .line 707
    :cond_14
    invoke-static/range {p1 .. p1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v8

    .line 708
    .restart local v8    # "headerId":J
    const-string v6, "q"

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 709
    .restart local v15    # "query":Ljava/lang/String;
    new-instance v5, Lcom/google/android/music/xdi/SearchHeaderItemsCursor;

    .end local v5    # "c":Landroid/database/Cursor;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/xdi/XdiContentProvider;->getContext()Landroid/content/Context;

    move-result-object v13

    move-object v12, v5

    move-object/from16 v14, p2

    move-wide/from16 v16, v8

    invoke-direct/range {v12 .. v17}, Lcom/google/android/music/xdi/SearchHeaderItemsCursor;-><init>(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;J)V

    .line 710
    .restart local v5    # "c":Landroid/database/Cursor;
    goto/16 :goto_0

    .line 714
    .end local v8    # "headerId":J
    .end local v15    # "query":Ljava/lang/String;
    :sswitch_12
    if-nez p2, :cond_15

    sget-object p2, Lcom/google/android/music/xdi/XdiContentProvider;->DEFAULT_BROWSE_HEADERS_PROJECTION:[Ljava/lang/String;

    .line 715
    :cond_15
    new-instance v5, Lcom/google/android/music/xdi/ExploreHeadersCursor;

    .end local v5    # "c":Landroid/database/Cursor;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/xdi/XdiContentProvider;->getContext()Landroid/content/Context;

    move-result-object v6

    move-object/from16 v0, p2

    invoke-direct {v5, v6, v0}, Lcom/google/android/music/xdi/ExploreHeadersCursor;-><init>(Landroid/content/Context;[Ljava/lang/String;)V

    .line 716
    .restart local v5    # "c":Landroid/database/Cursor;
    goto/16 :goto_0

    .line 720
    :sswitch_13
    if-nez p2, :cond_16

    sget-object p2, Lcom/google/android/music/xdi/XdiContentProvider;->DEFAULT_BROWSE_HEADER_ID_PROJECTION:[Ljava/lang/String;

    .line 721
    :cond_16
    invoke-static/range {p1 .. p1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v8

    .line 722
    .restart local v8    # "headerId":J
    new-instance v5, Lcom/google/android/music/xdi/ExploreItemsCursor;

    .end local v5    # "c":Landroid/database/Cursor;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/xdi/XdiContentProvider;->getContext()Landroid/content/Context;

    move-result-object v18

    const/16 v22, 0x0

    move-object/from16 v17, v5

    move-object/from16 v19, p2

    move-wide/from16 v20, v8

    invoke-direct/range {v17 .. v22}, Lcom/google/android/music/xdi/ExploreItemsCursor;-><init>(Landroid/content/Context;[Ljava/lang/String;JLjava/lang/String;)V

    .line 723
    .restart local v5    # "c":Landroid/database/Cursor;
    goto/16 :goto_0

    .line 727
    .end local v8    # "headerId":J
    :sswitch_14
    if-nez p2, :cond_17

    sget-object p2, Lcom/google/android/music/xdi/XdiContentProvider;->DEFAULT_BROWSE_HEADERS_PROJECTION:[Ljava/lang/String;

    .line 728
    :cond_17
    new-instance v5, Lcom/google/android/music/xdi/ExploreFeaturedHeadersCursor;

    .end local v5    # "c":Landroid/database/Cursor;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/xdi/XdiContentProvider;->getContext()Landroid/content/Context;

    move-result-object v6

    const/4 v7, 0x0

    move-object/from16 v0, p2

    invoke-direct {v5, v6, v0, v7}, Lcom/google/android/music/xdi/ExploreFeaturedHeadersCursor;-><init>(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;)V

    .line 729
    .restart local v5    # "c":Landroid/database/Cursor;
    goto/16 :goto_0

    .line 733
    :sswitch_15
    if-nez p2, :cond_18

    sget-object p2, Lcom/google/android/music/xdi/XdiContentProvider;->DEFAULT_BROWSE_HEADERS_PROJECTION:[Ljava/lang/String;

    .line 734
    :cond_18
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v33

    .line 735
    .local v33, "pathSegments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface/range {v33 .. v33}, Ljava/util/List;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x2

    move-object/from16 v0, v33

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Ljava/lang/String;

    .line 736
    .local v26, "genreId":Ljava/lang/String;
    new-instance v5, Lcom/google/android/music/xdi/ExploreFeaturedHeadersCursor;

    .end local v5    # "c":Landroid/database/Cursor;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/xdi/XdiContentProvider;->getContext()Landroid/content/Context;

    move-result-object v6

    move-object/from16 v0, p2

    move-object/from16 v1, v26

    invoke-direct {v5, v6, v0, v1}, Lcom/google/android/music/xdi/ExploreFeaturedHeadersCursor;-><init>(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;)V

    .line 737
    .restart local v5    # "c":Landroid/database/Cursor;
    goto/16 :goto_0

    .line 741
    .end local v26    # "genreId":Ljava/lang/String;
    .end local v33    # "pathSegments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :sswitch_16
    if-nez p2, :cond_19

    sget-object p2, Lcom/google/android/music/xdi/XdiContentProvider;->DEFAULT_BROWSE_HEADER_ID_PROJECTION:[Ljava/lang/String;

    .line 742
    :cond_19
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v33

    .line 743
    .restart local v33    # "pathSegments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface/range {v33 .. v33}, Ljava/util/List;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x2

    move-object/from16 v0, v33

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v20

    .line 744
    .local v20, "groupId":J
    invoke-interface/range {v33 .. v33}, Ljava/util/List;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    move-object/from16 v0, v33

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v22

    .line 745
    .local v22, "groupType":I
    new-instance v5, Lcom/google/android/music/xdi/ExploreFeaturedItemsCursor;

    .end local v5    # "c":Landroid/database/Cursor;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/xdi/XdiContentProvider;->getContext()Landroid/content/Context;

    move-result-object v18

    const/16 v23, 0x0

    move-object/from16 v17, v5

    move-object/from16 v19, p2

    invoke-direct/range {v17 .. v23}, Lcom/google/android/music/xdi/ExploreFeaturedItemsCursor;-><init>(Landroid/content/Context;[Ljava/lang/String;JILjava/lang/String;)V

    .line 747
    .restart local v5    # "c":Landroid/database/Cursor;
    goto/16 :goto_0

    .line 751
    .end local v20    # "groupId":J
    .end local v22    # "groupType":I
    .end local v33    # "pathSegments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :sswitch_17
    if-nez p2, :cond_1a

    sget-object p2, Lcom/google/android/music/xdi/XdiContentProvider;->DEFAULT_BROWSE_HEADERS_PROJECTION:[Ljava/lang/String;

    .line 752
    :cond_1a
    new-instance v5, Lcom/google/android/music/xdi/ExploreRecommendationsHeadersCursor;

    .end local v5    # "c":Landroid/database/Cursor;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/xdi/XdiContentProvider;->getContext()Landroid/content/Context;

    move-result-object v6

    move-object/from16 v0, p2

    invoke-direct {v5, v6, v0}, Lcom/google/android/music/xdi/ExploreRecommendationsHeadersCursor;-><init>(Landroid/content/Context;[Ljava/lang/String;)V

    .line 753
    .restart local v5    # "c":Landroid/database/Cursor;
    goto/16 :goto_0

    .line 757
    :sswitch_18
    if-nez p2, :cond_1b

    sget-object p2, Lcom/google/android/music/xdi/XdiContentProvider;->DEFAULT_BROWSE_HEADER_ID_PROJECTION:[Ljava/lang/String;

    .line 758
    :cond_1b
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v33

    .line 759
    .restart local v33    # "pathSegments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface/range {v33 .. v33}, Ljava/util/List;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x2

    move-object/from16 v0, v33

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v20

    .line 760
    .restart local v20    # "groupId":J
    invoke-interface/range {v33 .. v33}, Ljava/util/List;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    move-object/from16 v0, v33

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v22

    .line 761
    .restart local v22    # "groupType":I
    new-instance v5, Lcom/google/android/music/xdi/ExploreRecommendationsItemsCursor;

    .end local v5    # "c":Landroid/database/Cursor;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/xdi/XdiContentProvider;->getContext()Landroid/content/Context;

    move-result-object v18

    move-object/from16 v17, v5

    move-object/from16 v19, p2

    invoke-direct/range {v17 .. v22}, Lcom/google/android/music/xdi/ExploreRecommendationsItemsCursor;-><init>(Landroid/content/Context;[Ljava/lang/String;JI)V

    .line 763
    .restart local v5    # "c":Landroid/database/Cursor;
    goto/16 :goto_0

    .line 767
    .end local v20    # "groupId":J
    .end local v22    # "groupType":I
    .end local v33    # "pathSegments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :sswitch_19
    if-nez p2, :cond_1c

    sget-object p2, Lcom/google/android/music/xdi/XdiContentProvider;->DEFAULT_BROWSE_HEADERS_PROJECTION:[Ljava/lang/String;

    .line 768
    :cond_1c
    new-instance v5, Lcom/google/android/music/xdi/ExploreNewReleasesHeadersCursor;

    .end local v5    # "c":Landroid/database/Cursor;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/xdi/XdiContentProvider;->getContext()Landroid/content/Context;

    move-result-object v6

    const/4 v7, 0x0

    move-object/from16 v0, p2

    invoke-direct {v5, v6, v0, v7}, Lcom/google/android/music/xdi/ExploreNewReleasesHeadersCursor;-><init>(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;)V

    .line 769
    .restart local v5    # "c":Landroid/database/Cursor;
    goto/16 :goto_0

    .line 773
    :sswitch_1a
    if-nez p2, :cond_1d

    sget-object p2, Lcom/google/android/music/xdi/XdiContentProvider;->DEFAULT_BROWSE_HEADERS_PROJECTION:[Ljava/lang/String;

    .line 774
    :cond_1d
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v33

    .line 775
    .restart local v33    # "pathSegments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface/range {v33 .. v33}, Ljava/util/List;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x2

    move-object/from16 v0, v33

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Ljava/lang/String;

    .line 776
    .restart local v26    # "genreId":Ljava/lang/String;
    new-instance v5, Lcom/google/android/music/xdi/ExploreNewReleasesHeadersCursor;

    .end local v5    # "c":Landroid/database/Cursor;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/xdi/XdiContentProvider;->getContext()Landroid/content/Context;

    move-result-object v6

    move-object/from16 v0, p2

    move-object/from16 v1, v26

    invoke-direct {v5, v6, v0, v1}, Lcom/google/android/music/xdi/ExploreNewReleasesHeadersCursor;-><init>(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;)V

    .line 777
    .restart local v5    # "c":Landroid/database/Cursor;
    goto/16 :goto_0

    .line 781
    .end local v26    # "genreId":Ljava/lang/String;
    .end local v33    # "pathSegments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :sswitch_1b
    if-nez p2, :cond_1e

    sget-object p2, Lcom/google/android/music/xdi/XdiContentProvider;->DEFAULT_BROWSE_HEADER_ID_PROJECTION:[Ljava/lang/String;

    .line 782
    :cond_1e
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v33

    .line 783
    .restart local v33    # "pathSegments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface/range {v33 .. v33}, Ljava/util/List;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x2

    move-object/from16 v0, v33

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v20

    .line 784
    .restart local v20    # "groupId":J
    invoke-interface/range {v33 .. v33}, Ljava/util/List;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    move-object/from16 v0, v33

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v22

    .line 785
    .restart local v22    # "groupType":I
    new-instance v5, Lcom/google/android/music/xdi/ExploreNewReleasesItemsCursor;

    .end local v5    # "c":Landroid/database/Cursor;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/xdi/XdiContentProvider;->getContext()Landroid/content/Context;

    move-result-object v18

    const/16 v23, 0x0

    move-object/from16 v17, v5

    move-object/from16 v19, p2

    invoke-direct/range {v17 .. v23}, Lcom/google/android/music/xdi/ExploreNewReleasesItemsCursor;-><init>(Landroid/content/Context;[Ljava/lang/String;JILjava/lang/String;)V

    .line 787
    .restart local v5    # "c":Landroid/database/Cursor;
    goto/16 :goto_0

    .line 791
    .end local v20    # "groupId":J
    .end local v22    # "groupType":I
    .end local v33    # "pathSegments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :sswitch_1c
    if-nez p2, :cond_1f

    sget-object p2, Lcom/google/android/music/xdi/XdiContentProvider;->DEFAULT_BROWSE_HEADERS_PROJECTION:[Ljava/lang/String;

    .line 792
    :cond_1f
    new-instance v5, Lcom/google/android/music/xdi/ExploreGenresHeadersCursor;

    .end local v5    # "c":Landroid/database/Cursor;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/xdi/XdiContentProvider;->getContext()Landroid/content/Context;

    move-result-object v6

    const/4 v7, 0x0

    move-object/from16 v0, p2

    invoke-direct {v5, v6, v0, v7}, Lcom/google/android/music/xdi/ExploreGenresHeadersCursor;-><init>(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;)V

    .line 793
    .restart local v5    # "c":Landroid/database/Cursor;
    goto/16 :goto_0

    .line 797
    :sswitch_1d
    if-nez p2, :cond_20

    sget-object p2, Lcom/google/android/music/xdi/XdiContentProvider;->DEFAULT_BROWSE_HEADER_ID_PROJECTION:[Ljava/lang/String;

    .line 798
    :cond_20
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v33

    .line 799
    .restart local v33    # "pathSegments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface/range {v33 .. v33}, Ljava/util/List;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x2

    move-object/from16 v0, v33

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Ljava/lang/String;

    .line 800
    .restart local v26    # "genreId":Ljava/lang/String;
    invoke-interface/range {v33 .. v33}, Ljava/util/List;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    move-object/from16 v0, v33

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Ljava/lang/String;

    .line 801
    .local v27, "genreName":Ljava/lang/String;
    new-instance v5, Lcom/google/android/music/xdi/ExploreGenresItemsCursor;

    .end local v5    # "c":Landroid/database/Cursor;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/xdi/XdiContentProvider;->getContext()Landroid/content/Context;

    move-result-object v6

    move-object/from16 v0, p2

    move-object/from16 v1, v26

    move-object/from16 v2, v27

    invoke-direct {v5, v6, v0, v1, v2}, Lcom/google/android/music/xdi/ExploreGenresItemsCursor;-><init>(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 802
    .restart local v5    # "c":Landroid/database/Cursor;
    goto/16 :goto_0

    .line 806
    .end local v26    # "genreId":Ljava/lang/String;
    .end local v27    # "genreName":Ljava/lang/String;
    .end local v33    # "pathSegments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :sswitch_1e
    if-nez p2, :cond_21

    sget-object p2, Lcom/google/android/music/xdi/XdiContentProvider;->DEFAULT_BROWSE_HEADERS_PROJECTION:[Ljava/lang/String;

    .line 807
    :cond_21
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v33

    .line 808
    .restart local v33    # "pathSegments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface/range {v33 .. v33}, Ljava/util/List;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x4

    move-object/from16 v0, v33

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Ljava/lang/String;

    .line 809
    .restart local v26    # "genreId":Ljava/lang/String;
    invoke-interface/range {v33 .. v33}, Ljava/util/List;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x3

    move-object/from16 v0, v33

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Ljava/lang/String;

    .line 810
    .restart local v27    # "genreName":Ljava/lang/String;
    invoke-interface/range {v33 .. v33}, Ljava/util/List;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x2

    move-object/from16 v0, v33

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v28

    check-cast v28, Ljava/lang/String;

    .line 811
    .local v28, "parentGenreId":Ljava/lang/String;
    const-string v6, "null"

    move-object/from16 v0, v28

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_22

    .line 812
    const/16 v28, 0x0

    .line 814
    :cond_22
    new-instance v5, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;

    .end local v5    # "c":Landroid/database/Cursor;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/xdi/XdiContentProvider;->getContext()Landroid/content/Context;

    move-result-object v24

    move-object/from16 v23, v5

    move-object/from16 v25, p2

    invoke-direct/range {v23 .. v28}, Lcom/google/android/music/xdi/ExploreGenreHeadersCursor;-><init>(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 816
    .restart local v5    # "c":Landroid/database/Cursor;
    goto/16 :goto_0

    .line 820
    .end local v26    # "genreId":Ljava/lang/String;
    .end local v27    # "genreName":Ljava/lang/String;
    .end local v28    # "parentGenreId":Ljava/lang/String;
    .end local v33    # "pathSegments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :sswitch_1f
    if-nez p2, :cond_23

    sget-object p2, Lcom/google/android/music/xdi/XdiContentProvider;->DEFAULT_BROWSE_HEADER_ID_PROJECTION:[Ljava/lang/String;

    .line 821
    :cond_23
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v33

    .line 822
    .restart local v33    # "pathSegments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface/range {v33 .. v33}, Ljava/util/List;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x2

    move-object/from16 v0, v33

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Ljava/lang/String;

    .line 823
    .restart local v26    # "genreId":Ljava/lang/String;
    invoke-interface/range {v33 .. v33}, Ljava/util/List;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    move-object/from16 v0, v33

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v28

    check-cast v28, Ljava/lang/String;

    .line 824
    .restart local v28    # "parentGenreId":Ljava/lang/String;
    const-string v6, "null"

    move-object/from16 v0, v28

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_24

    .line 825
    const/16 v28, 0x0

    .line 827
    :cond_24
    new-instance v5, Lcom/google/android/music/xdi/ExploreGenreItemsCursor;

    .end local v5    # "c":Landroid/database/Cursor;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/xdi/XdiContentProvider;->getContext()Landroid/content/Context;

    move-result-object v6

    move-object/from16 v0, p2

    move-object/from16 v1, v26

    move-object/from16 v2, v28

    invoke-direct {v5, v6, v0, v1, v2}, Lcom/google/android/music/xdi/ExploreGenreItemsCursor;-><init>(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 828
    .restart local v5    # "c":Landroid/database/Cursor;
    goto/16 :goto_0

    .line 832
    .end local v26    # "genreId":Ljava/lang/String;
    .end local v28    # "parentGenreId":Ljava/lang/String;
    .end local v33    # "pathSegments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :sswitch_20
    if-nez p2, :cond_25

    sget-object p2, Lcom/google/android/music/xdi/XdiContentProvider;->DEFAULT_BROWSE_HEADER_ID_PROJECTION:[Ljava/lang/String;

    .line 833
    :cond_25
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v33

    .line 834
    .restart local v33    # "pathSegments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface/range {v33 .. v33}, Ljava/util/List;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x3

    move-object/from16 v0, v33

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v20

    .line 835
    .restart local v20    # "groupId":J
    invoke-interface/range {v33 .. v33}, Ljava/util/List;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x2

    move-object/from16 v0, v33

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v22

    .line 836
    .restart local v22    # "groupType":I
    invoke-interface/range {v33 .. v33}, Ljava/util/List;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    move-object/from16 v0, v33

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Ljava/lang/String;

    .line 837
    .restart local v26    # "genreId":Ljava/lang/String;
    new-instance v5, Lcom/google/android/music/xdi/ExploreFeaturedItemsCursor;

    .end local v5    # "c":Landroid/database/Cursor;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/xdi/XdiContentProvider;->getContext()Landroid/content/Context;

    move-result-object v18

    move-object/from16 v17, v5

    move-object/from16 v19, p2

    move-object/from16 v23, v26

    invoke-direct/range {v17 .. v23}, Lcom/google/android/music/xdi/ExploreFeaturedItemsCursor;-><init>(Landroid/content/Context;[Ljava/lang/String;JILjava/lang/String;)V

    .line 839
    .restart local v5    # "c":Landroid/database/Cursor;
    goto/16 :goto_0

    .line 843
    .end local v20    # "groupId":J
    .end local v22    # "groupType":I
    .end local v26    # "genreId":Ljava/lang/String;
    .end local v33    # "pathSegments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :sswitch_21
    if-nez p2, :cond_26

    sget-object p2, Lcom/google/android/music/xdi/XdiContentProvider;->DEFAULT_BROWSE_HEADER_ID_PROJECTION:[Ljava/lang/String;

    .line 844
    :cond_26
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v33

    .line 845
    .restart local v33    # "pathSegments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface/range {v33 .. v33}, Ljava/util/List;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x3

    move-object/from16 v0, v33

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v20

    .line 846
    .restart local v20    # "groupId":J
    invoke-interface/range {v33 .. v33}, Ljava/util/List;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x2

    move-object/from16 v0, v33

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v22

    .line 847
    .restart local v22    # "groupType":I
    invoke-interface/range {v33 .. v33}, Ljava/util/List;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    move-object/from16 v0, v33

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Ljava/lang/String;

    .line 848
    .restart local v26    # "genreId":Ljava/lang/String;
    new-instance v5, Lcom/google/android/music/xdi/ExploreNewReleasesItemsCursor;

    .end local v5    # "c":Landroid/database/Cursor;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/xdi/XdiContentProvider;->getContext()Landroid/content/Context;

    move-result-object v18

    move-object/from16 v17, v5

    move-object/from16 v19, p2

    move-object/from16 v23, v26

    invoke-direct/range {v17 .. v23}, Lcom/google/android/music/xdi/ExploreNewReleasesItemsCursor;-><init>(Landroid/content/Context;[Ljava/lang/String;JILjava/lang/String;)V

    .line 850
    .restart local v5    # "c":Landroid/database/Cursor;
    goto/16 :goto_0

    .line 854
    .end local v20    # "groupId":J
    .end local v22    # "groupType":I
    .end local v26    # "genreId":Ljava/lang/String;
    .end local v33    # "pathSegments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :sswitch_22
    if-nez p2, :cond_27

    sget-object p2, Lcom/google/android/music/xdi/XdiContentProvider;->DEFAULT_BROWSE_HEADER_ID_PROJECTION:[Ljava/lang/String;

    .line 855
    :cond_27
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v33

    .line 856
    .restart local v33    # "pathSegments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface/range {v33 .. v33}, Ljava/util/List;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x3

    move-object/from16 v0, v33

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Ljava/lang/String;

    .line 857
    .restart local v26    # "genreId":Ljava/lang/String;
    invoke-interface/range {v33 .. v33}, Ljava/util/List;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x2

    move-object/from16 v0, v33

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Ljava/lang/String;

    .line 858
    .restart local v27    # "genreName":Ljava/lang/String;
    invoke-interface/range {v33 .. v33}, Ljava/util/List;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    move-object/from16 v0, v33

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v28

    check-cast v28, Ljava/lang/String;

    .line 859
    .restart local v28    # "parentGenreId":Ljava/lang/String;
    const-string v6, "null"

    move-object/from16 v0, v28

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_28

    .line 860
    const/16 v28, 0x0

    .line 862
    :cond_28
    new-instance v5, Lcom/google/android/music/xdi/ExploreGenreRadioItemsCursor;

    .end local v5    # "c":Landroid/database/Cursor;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/xdi/XdiContentProvider;->getContext()Landroid/content/Context;

    move-result-object v24

    move-object/from16 v23, v5

    move-object/from16 v25, p2

    invoke-direct/range {v23 .. v28}, Lcom/google/android/music/xdi/ExploreGenreRadioItemsCursor;-><init>(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 864
    .restart local v5    # "c":Landroid/database/Cursor;
    goto/16 :goto_0

    .line 868
    .end local v26    # "genreId":Ljava/lang/String;
    .end local v27    # "genreName":Ljava/lang/String;
    .end local v28    # "parentGenreId":Ljava/lang/String;
    .end local v33    # "pathSegments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :sswitch_23
    if-nez p2, :cond_29

    sget-object p2, Lcom/google/android/music/xdi/XdiContentProvider;->DEFAULT_DETAIL_ITEM_PROJECTION:[Ljava/lang/String;

    .line 869
    :cond_29
    new-instance v5, Lcom/google/android/music/xdi/DetailAlbumCursor;

    .end local v5    # "c":Landroid/database/Cursor;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/xdi/XdiContentProvider;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p2

    invoke-direct {v5, v6, v0, v7}, Lcom/google/android/music/xdi/DetailAlbumCursor;-><init>(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;)V

    .line 870
    .restart local v5    # "c":Landroid/database/Cursor;
    goto/16 :goto_0

    .line 874
    :sswitch_24
    if-nez p2, :cond_2a

    sget-object p2, Lcom/google/android/music/xdi/XdiContentProvider;->DEFAULT_DETAIL_SECTIONS_PROJECTION:[Ljava/lang/String;

    .line 875
    :cond_2a
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v33

    .line 876
    .restart local v33    # "pathSegments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface/range {v33 .. v33}, Ljava/util/List;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x2

    move-object/from16 v0, v33

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 877
    .local v4, "albumId":Ljava/lang/String;
    new-instance v5, Lcom/google/android/music/xdi/DetailAlbumSectionsCursor;

    .end local v5    # "c":Landroid/database/Cursor;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/xdi/XdiContentProvider;->getContext()Landroid/content/Context;

    move-result-object v6

    move-object/from16 v0, p2

    invoke-direct {v5, v6, v0, v4}, Lcom/google/android/music/xdi/DetailAlbumSectionsCursor;-><init>(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;)V

    .line 878
    .restart local v5    # "c":Landroid/database/Cursor;
    goto/16 :goto_0

    .line 882
    .end local v4    # "albumId":Ljava/lang/String;
    .end local v33    # "pathSegments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :sswitch_25
    if-nez p2, :cond_2b

    sget-object p2, Lcom/google/android/music/xdi/XdiContentProvider;->DEFAULT_DETAIL_BLURB_PROJECTION:[Ljava/lang/String;

    .line 883
    :cond_2b
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v33

    .line 884
    .restart local v33    # "pathSegments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface/range {v33 .. v33}, Ljava/util/List;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x3

    move-object/from16 v0, v33

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 885
    .restart local v4    # "albumId":Ljava/lang/String;
    new-instance v5, Lcom/google/android/music/xdi/DetailAlbumInfoCursor;

    .end local v5    # "c":Landroid/database/Cursor;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/xdi/XdiContentProvider;->getContext()Landroid/content/Context;

    move-result-object v6

    move-object/from16 v0, p2

    invoke-direct {v5, v6, v0, v4}, Lcom/google/android/music/xdi/DetailAlbumInfoCursor;-><init>(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;)V

    .line 886
    .restart local v5    # "c":Landroid/database/Cursor;
    goto/16 :goto_0

    .line 890
    .end local v4    # "albumId":Ljava/lang/String;
    .end local v33    # "pathSegments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :sswitch_26
    if-nez p2, :cond_2c

    sget-object p2, Lcom/google/android/music/xdi/XdiContentProvider;->DEFAULT_DETAIL_CHILDREN_PROJECTION:[Ljava/lang/String;

    .line 891
    :cond_2c
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v33

    .line 892
    .restart local v33    # "pathSegments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface/range {v33 .. v33}, Ljava/util/List;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x3

    move-object/from16 v0, v33

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 893
    .restart local v4    # "albumId":Ljava/lang/String;
    new-instance v5, Lcom/google/android/music/xdi/DetailAlbumTracklistCursor;

    .end local v5    # "c":Landroid/database/Cursor;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/xdi/XdiContentProvider;->getContext()Landroid/content/Context;

    move-result-object v6

    move-object/from16 v0, p2

    invoke-direct {v5, v6, v0, v4}, Lcom/google/android/music/xdi/DetailAlbumTracklistCursor;-><init>(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;)V

    .line 894
    .restart local v5    # "c":Landroid/database/Cursor;
    goto/16 :goto_0

    .line 898
    .end local v4    # "albumId":Ljava/lang/String;
    .end local v33    # "pathSegments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :sswitch_27
    if-nez p2, :cond_2d

    sget-object p2, Lcom/google/android/music/xdi/XdiContentProvider;->DEFAULT_DETAIL_ACTIONS_PROJECTION:[Ljava/lang/String;

    .line 899
    :cond_2d
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v33

    .line 900
    .restart local v33    # "pathSegments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface/range {v33 .. v33}, Ljava/util/List;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x4

    move-object/from16 v0, v33

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 901
    .restart local v4    # "albumId":Ljava/lang/String;
    new-instance v5, Lcom/google/android/music/xdi/DetailAlbumActionsCursor;

    .end local v5    # "c":Landroid/database/Cursor;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/xdi/XdiContentProvider;->getContext()Landroid/content/Context;

    move-result-object v6

    move-object/from16 v0, p2

    invoke-direct {v5, v6, v0, v4}, Lcom/google/android/music/xdi/DetailAlbumActionsCursor;-><init>(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;)V

    .line 902
    .restart local v5    # "c":Landroid/database/Cursor;
    goto/16 :goto_0

    .line 907
    .end local v4    # "albumId":Ljava/lang/String;
    .end local v33    # "pathSegments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :sswitch_28
    if-nez p2, :cond_2e

    sget-object p2, Lcom/google/android/music/xdi/XdiContentProvider;->DEFAULT_DETAIL_ITEM_PROJECTION:[Ljava/lang/String;

    .line 908
    :cond_2e
    invoke-static/range {p1 .. p1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v34

    .line 909
    .local v34, "playlistId":J
    new-instance v5, Lcom/google/android/music/xdi/DetailPlaylistCursor;

    .end local v5    # "c":Landroid/database/Cursor;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/xdi/XdiContentProvider;->getContext()Landroid/content/Context;

    move-result-object v6

    move-object/from16 v0, p2

    move-wide/from16 v1, v34

    invoke-direct {v5, v6, v0, v1, v2}, Lcom/google/android/music/xdi/DetailPlaylistCursor;-><init>(Landroid/content/Context;[Ljava/lang/String;J)V

    .line 910
    .restart local v5    # "c":Landroid/database/Cursor;
    goto/16 :goto_0

    .line 914
    .end local v34    # "playlistId":J
    :sswitch_29
    if-nez p2, :cond_2f

    sget-object p2, Lcom/google/android/music/xdi/XdiContentProvider;->DEFAULT_DETAIL_SECTIONS_PROJECTION:[Ljava/lang/String;

    .line 915
    :cond_2f
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v33

    .line 916
    .restart local v33    # "pathSegments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface/range {v33 .. v33}, Ljava/util/List;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x2

    move-object/from16 v0, v33

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Ljava/lang/String;

    .line 917
    .local v29, "idString":Ljava/lang/String;
    invoke-static/range {v29 .. v29}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v34

    .line 918
    .restart local v34    # "playlistId":J
    new-instance v5, Lcom/google/android/music/xdi/DetailPlaylistSectionsCursor;

    .end local v5    # "c":Landroid/database/Cursor;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/xdi/XdiContentProvider;->getContext()Landroid/content/Context;

    move-result-object v6

    move-object/from16 v0, p2

    move-wide/from16 v1, v34

    invoke-direct {v5, v6, v0, v1, v2}, Lcom/google/android/music/xdi/DetailPlaylistSectionsCursor;-><init>(Landroid/content/Context;[Ljava/lang/String;J)V

    .line 919
    .restart local v5    # "c":Landroid/database/Cursor;
    goto/16 :goto_0

    .line 923
    .end local v29    # "idString":Ljava/lang/String;
    .end local v33    # "pathSegments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v34    # "playlistId":J
    :sswitch_2a
    if-nez p2, :cond_30

    sget-object p2, Lcom/google/android/music/xdi/XdiContentProvider;->DEFAULT_DETAIL_BLURB_PROJECTION:[Ljava/lang/String;

    .line 924
    :cond_30
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v33

    .line 925
    .restart local v33    # "pathSegments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface/range {v33 .. v33}, Ljava/util/List;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x3

    move-object/from16 v0, v33

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Ljava/lang/String;

    .line 926
    .restart local v29    # "idString":Ljava/lang/String;
    invoke-static/range {v29 .. v29}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v34

    .line 927
    .restart local v34    # "playlistId":J
    new-instance v5, Lcom/google/android/music/xdi/DetailPlaylistInfoCursor;

    .end local v5    # "c":Landroid/database/Cursor;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/xdi/XdiContentProvider;->getContext()Landroid/content/Context;

    move-result-object v6

    move-object/from16 v0, p2

    move-wide/from16 v1, v34

    invoke-direct {v5, v6, v0, v1, v2}, Lcom/google/android/music/xdi/DetailPlaylistInfoCursor;-><init>(Landroid/content/Context;[Ljava/lang/String;J)V

    .line 928
    .restart local v5    # "c":Landroid/database/Cursor;
    goto/16 :goto_0

    .line 932
    .end local v29    # "idString":Ljava/lang/String;
    .end local v33    # "pathSegments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v34    # "playlistId":J
    :sswitch_2b
    if-nez p2, :cond_31

    sget-object p2, Lcom/google/android/music/xdi/XdiContentProvider;->DEFAULT_DETAIL_CHILDREN_PROJECTION:[Ljava/lang/String;

    .line 933
    :cond_31
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v33

    .line 934
    .restart local v33    # "pathSegments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface/range {v33 .. v33}, Ljava/util/List;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x3

    move-object/from16 v0, v33

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Ljava/lang/String;

    .line 935
    .restart local v29    # "idString":Ljava/lang/String;
    invoke-static/range {v29 .. v29}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v34

    .line 936
    .restart local v34    # "playlistId":J
    new-instance v5, Lcom/google/android/music/xdi/DetailPlaylistTracklistCursor;

    .end local v5    # "c":Landroid/database/Cursor;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/xdi/XdiContentProvider;->getContext()Landroid/content/Context;

    move-result-object v6

    move-object/from16 v0, p2

    move-wide/from16 v1, v34

    invoke-direct {v5, v6, v0, v1, v2}, Lcom/google/android/music/xdi/DetailPlaylistTracklistCursor;-><init>(Landroid/content/Context;[Ljava/lang/String;J)V

    .line 937
    .restart local v5    # "c":Landroid/database/Cursor;
    goto/16 :goto_0

    .line 941
    .end local v29    # "idString":Ljava/lang/String;
    .end local v33    # "pathSegments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v34    # "playlistId":J
    :sswitch_2c
    if-nez p2, :cond_32

    sget-object p2, Lcom/google/android/music/xdi/XdiContentProvider;->DEFAULT_DETAIL_ACTIONS_PROJECTION:[Ljava/lang/String;

    .line 942
    :cond_32
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v33

    .line 943
    .restart local v33    # "pathSegments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface/range {v33 .. v33}, Ljava/util/List;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x4

    move-object/from16 v0, v33

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Ljava/lang/String;

    .line 944
    .restart local v29    # "idString":Ljava/lang/String;
    invoke-static/range {v29 .. v29}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v34

    .line 945
    .restart local v34    # "playlistId":J
    new-instance v5, Lcom/google/android/music/xdi/DetailPlaylistActionsCursor;

    .end local v5    # "c":Landroid/database/Cursor;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/xdi/XdiContentProvider;->getContext()Landroid/content/Context;

    move-result-object v6

    move-object/from16 v0, p2

    move-wide/from16 v1, v34

    invoke-direct {v5, v6, v0, v1, v2}, Lcom/google/android/music/xdi/DetailPlaylistActionsCursor;-><init>(Landroid/content/Context;[Ljava/lang/String;J)V

    .line 946
    .restart local v5    # "c":Landroid/database/Cursor;
    goto/16 :goto_0

    .line 951
    .end local v29    # "idString":Ljava/lang/String;
    .end local v33    # "pathSegments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v34    # "playlistId":J
    :sswitch_2d
    if-nez p2, :cond_33

    sget-object p2, Lcom/google/android/music/xdi/XdiContentProvider;->DEFAULT_DETAIL_ITEM_PROJECTION:[Ljava/lang/String;

    .line 952
    :cond_33
    new-instance v5, Lcom/google/android/music/xdi/DetailArtistCursor;

    .end local v5    # "c":Landroid/database/Cursor;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/xdi/XdiContentProvider;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p2

    invoke-direct {v5, v6, v0, v7}, Lcom/google/android/music/xdi/DetailArtistCursor;-><init>(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;)V

    .line 953
    .restart local v5    # "c":Landroid/database/Cursor;
    goto/16 :goto_0

    .line 957
    :sswitch_2e
    if-nez p2, :cond_34

    sget-object p2, Lcom/google/android/music/xdi/XdiContentProvider;->DEFAULT_DETAIL_SECTIONS_PROJECTION:[Ljava/lang/String;

    .line 958
    :cond_34
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v33

    .line 959
    .restart local v33    # "pathSegments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface/range {v33 .. v33}, Ljava/util/List;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x2

    move-object/from16 v0, v33

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Ljava/lang/String;

    .line 960
    .restart local v29    # "idString":Ljava/lang/String;
    new-instance v5, Lcom/google/android/music/xdi/DetailArtistSectionsCursor;

    .end local v5    # "c":Landroid/database/Cursor;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/xdi/XdiContentProvider;->getContext()Landroid/content/Context;

    move-result-object v6

    move-object/from16 v0, p2

    move-object/from16 v1, v29

    invoke-direct {v5, v6, v0, v1}, Lcom/google/android/music/xdi/DetailArtistSectionsCursor;-><init>(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;)V

    .line 961
    .restart local v5    # "c":Landroid/database/Cursor;
    goto/16 :goto_0

    .line 965
    .end local v29    # "idString":Ljava/lang/String;
    .end local v33    # "pathSegments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :sswitch_2f
    if-nez p2, :cond_35

    sget-object p2, Lcom/google/android/music/xdi/XdiContentProvider;->DEFAULT_DETAIL_BLURB_PROJECTION:[Ljava/lang/String;

    .line 966
    :cond_35
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v33

    .line 967
    .restart local v33    # "pathSegments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface/range {v33 .. v33}, Ljava/util/List;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x3

    move-object/from16 v0, v33

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Ljava/lang/String;

    .line 968
    .restart local v29    # "idString":Ljava/lang/String;
    new-instance v5, Lcom/google/android/music/xdi/DetailArtistInfoCursor;

    .end local v5    # "c":Landroid/database/Cursor;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/xdi/XdiContentProvider;->getContext()Landroid/content/Context;

    move-result-object v6

    move-object/from16 v0, p2

    move-object/from16 v1, v29

    invoke-direct {v5, v6, v0, v1}, Lcom/google/android/music/xdi/DetailArtistInfoCursor;-><init>(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;)V

    .line 969
    .restart local v5    # "c":Landroid/database/Cursor;
    goto/16 :goto_0

    .line 973
    .end local v29    # "idString":Ljava/lang/String;
    .end local v33    # "pathSegments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :sswitch_30
    if-nez p2, :cond_36

    sget-object p2, Lcom/google/android/music/xdi/XdiContentProvider;->DEFAULT_DETAIL_CHILDREN_PROJECTION:[Ljava/lang/String;

    .line 974
    :cond_36
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v33

    .line 975
    .restart local v33    # "pathSegments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface/range {v33 .. v33}, Ljava/util/List;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x3

    move-object/from16 v0, v33

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Ljava/lang/String;

    .line 976
    .restart local v29    # "idString":Ljava/lang/String;
    new-instance v5, Lcom/google/android/music/xdi/DetailArtistAlbumsCursor;

    .end local v5    # "c":Landroid/database/Cursor;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/xdi/XdiContentProvider;->getContext()Landroid/content/Context;

    move-result-object v6

    move-object/from16 v0, p2

    move-object/from16 v1, v29

    invoke-direct {v5, v6, v0, v1}, Lcom/google/android/music/xdi/DetailArtistAlbumsCursor;-><init>(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;)V

    .line 977
    .restart local v5    # "c":Landroid/database/Cursor;
    goto/16 :goto_0

    .line 981
    .end local v29    # "idString":Ljava/lang/String;
    .end local v33    # "pathSegments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :sswitch_31
    if-nez p2, :cond_37

    sget-object p2, Lcom/google/android/music/xdi/XdiContentProvider;->DEFAULT_DETAIL_CHILDREN_PROJECTION:[Ljava/lang/String;

    .line 982
    :cond_37
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v33

    .line 983
    .restart local v33    # "pathSegments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface/range {v33 .. v33}, Ljava/util/List;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x3

    move-object/from16 v0, v33

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Ljava/lang/String;

    .line 984
    .restart local v29    # "idString":Ljava/lang/String;
    new-instance v5, Lcom/google/android/music/xdi/DetailArtistRelatedArtistsCursor;

    .end local v5    # "c":Landroid/database/Cursor;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/xdi/XdiContentProvider;->getContext()Landroid/content/Context;

    move-result-object v6

    move-object/from16 v0, p2

    move-object/from16 v1, v29

    invoke-direct {v5, v6, v0, v1}, Lcom/google/android/music/xdi/DetailArtistRelatedArtistsCursor;-><init>(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;)V

    .line 985
    .restart local v5    # "c":Landroid/database/Cursor;
    goto/16 :goto_0

    .line 989
    .end local v29    # "idString":Ljava/lang/String;
    .end local v33    # "pathSegments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :sswitch_32
    if-nez p2, :cond_38

    sget-object p2, Lcom/google/android/music/xdi/XdiContentProvider;->DEFAULT_DETAIL_ACTIONS_PROJECTION:[Ljava/lang/String;

    .line 990
    :cond_38
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v33

    .line 991
    .restart local v33    # "pathSegments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface/range {v33 .. v33}, Ljava/util/List;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x4

    move-object/from16 v0, v33

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Ljava/lang/String;

    .line 992
    .restart local v29    # "idString":Ljava/lang/String;
    new-instance v5, Lcom/google/android/music/xdi/DetailArtistActionsCursor;

    .end local v5    # "c":Landroid/database/Cursor;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/xdi/XdiContentProvider;->getContext()Landroid/content/Context;

    move-result-object v6

    const/4 v7, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v29

    invoke-direct {v5, v6, v0, v1, v7}, Lcom/google/android/music/xdi/DetailArtistActionsCursor;-><init>(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;Z)V

    .line 994
    .restart local v5    # "c":Landroid/database/Cursor;
    goto/16 :goto_0

    .line 998
    .end local v29    # "idString":Ljava/lang/String;
    .end local v33    # "pathSegments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :sswitch_33
    if-nez p2, :cond_39

    sget-object p2, Lcom/google/android/music/xdi/XdiContentProvider;->DEFAULT_DETAIL_CHILDREN_PROJECTION:[Ljava/lang/String;

    .line 999
    :cond_39
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v33

    .line 1000
    .restart local v33    # "pathSegments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface/range {v33 .. v33}, Ljava/util/List;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x2

    move-object/from16 v0, v33

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Ljava/lang/String;

    .line 1001
    .restart local v29    # "idString":Ljava/lang/String;
    new-instance v5, Lcom/google/android/music/xdi/DetailArtistTopSongsCursor;

    .end local v5    # "c":Landroid/database/Cursor;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/xdi/XdiContentProvider;->getContext()Landroid/content/Context;

    move-result-object v6

    move-object/from16 v0, p2

    move-object/from16 v1, v29

    invoke-direct {v5, v6, v0, v1}, Lcom/google/android/music/xdi/DetailArtistTopSongsCursor;-><init>(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;)V

    .line 1002
    .restart local v5    # "c":Landroid/database/Cursor;
    goto/16 :goto_0

    .line 1006
    .end local v29    # "idString":Ljava/lang/String;
    .end local v33    # "pathSegments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :sswitch_34
    if-nez p2, :cond_3a

    sget-object p2, Lcom/google/android/music/xdi/XdiContentProvider;->DEFAULT_DETAIL_CHILDREN_PROJECTION:[Ljava/lang/String;

    .line 1007
    :cond_3a
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v33

    .line 1008
    .restart local v33    # "pathSegments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface/range {v33 .. v33}, Ljava/util/List;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x2

    move-object/from16 v0, v33

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Ljava/lang/String;

    .line 1009
    .restart local v29    # "idString":Ljava/lang/String;
    new-instance v5, Lcom/google/android/music/xdi/DetailArtistLockerSongsCursor;

    .end local v5    # "c":Landroid/database/Cursor;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/xdi/XdiContentProvider;->getContext()Landroid/content/Context;

    move-result-object v6

    move-object/from16 v0, p2

    move-object/from16 v1, v29

    invoke-direct {v5, v6, v0, v1}, Lcom/google/android/music/xdi/DetailArtistLockerSongsCursor;-><init>(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;)V

    .line 1010
    .restart local v5    # "c":Landroid/database/Cursor;
    goto/16 :goto_0

    .line 1014
    .end local v29    # "idString":Ljava/lang/String;
    .end local v33    # "pathSegments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :sswitch_35
    if-nez p2, :cond_3b

    sget-object p2, Lcom/google/android/music/xdi/XdiContentProvider;->META_PROJECTION:[Ljava/lang/String;

    .line 1015
    :cond_3b
    new-instance v5, Lcom/google/android/music/xdi/MetaIdCursor;

    .end local v5    # "c":Landroid/database/Cursor;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/xdi/XdiContentProvider;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static/range {p1 .. p1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v12

    move-object/from16 v0, p2

    invoke-direct {v5, v6, v0, v12, v13}, Lcom/google/android/music/xdi/MetaIdCursor;-><init>(Landroid/content/Context;[Ljava/lang/String;J)V

    .line 1016
    .restart local v5    # "c":Landroid/database/Cursor;
    goto/16 :goto_0

    .line 1020
    :sswitch_36
    if-nez p2, :cond_3c

    sget-object p2, Lcom/google/android/music/xdi/XdiContentProvider;->META_PROJECTION:[Ljava/lang/String;

    .line 1021
    :cond_3c
    new-instance v5, Lcom/google/android/music/xdi/MetaTitleCursor;

    .end local v5    # "c":Landroid/database/Cursor;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/xdi/XdiContentProvider;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p2

    invoke-direct {v5, v6, v0, v7}, Lcom/google/android/music/xdi/MetaTitleCursor;-><init>(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;)V

    .line 1022
    .restart local v5    # "c":Landroid/database/Cursor;
    goto/16 :goto_0

    .line 591
    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x3 -> :sswitch_1
        0x4 -> :sswitch_2
        0x5 -> :sswitch_3
        0x6 -> :sswitch_4
        0x7 -> :sswitch_5
        0x8 -> :sswitch_6
        0x9 -> :sswitch_c
        0xa -> :sswitch_d
        0xb -> :sswitch_10
        0xc -> :sswitch_11
        0xd -> :sswitch_12
        0xe -> :sswitch_13
        0xf -> :sswitch_14
        0x10 -> :sswitch_16
        0x11 -> :sswitch_17
        0x12 -> :sswitch_18
        0x13 -> :sswitch_19
        0x14 -> :sswitch_1b
        0x15 -> :sswitch_1c
        0x16 -> :sswitch_1d
        0x17 -> :sswitch_1e
        0x18 -> :sswitch_1f
        0x19 -> :sswitch_15
        0x1a -> :sswitch_20
        0x1b -> :sswitch_1a
        0x1c -> :sswitch_21
        0x1d -> :sswitch_35
        0x1e -> :sswitch_36
        0x1f -> :sswitch_22
        0x20 -> :sswitch_7
        0x21 -> :sswitch_8
        0x22 -> :sswitch_9
        0x23 -> :sswitch_8
        0x24 -> :sswitch_a
        0x25 -> :sswitch_b
        0x26 -> :sswitch_e
        0x27 -> :sswitch_f
        0x64 -> :sswitch_23
        0x65 -> :sswitch_24
        0x66 -> :sswitch_25
        0x67 -> :sswitch_26
        0x68 -> :sswitch_27
        0xc8 -> :sswitch_28
        0xc9 -> :sswitch_29
        0xca -> :sswitch_2a
        0xcb -> :sswitch_2b
        0xcc -> :sswitch_2c
        0x12c -> :sswitch_2d
        0x12d -> :sswitch_2e
        0x12e -> :sswitch_2f
        0x12f -> :sswitch_30
        0x130 -> :sswitch_31
        0x131 -> :sswitch_32
        0x132 -> :sswitch_33
        0x133 -> :sswitch_34
    .end sparse-switch
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 515
    sget-boolean v0, Lcom/google/android/music/xdi/XdiContentProvider;->LOGV:Z

    if-eqz v0, :cond_0

    .line 516
    const-string v0, "MusicXdi"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Delete not supported.  URI: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 518
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 540
    const/4 v0, 0x0

    return-object v0
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 531
    sget-boolean v0, Lcom/google/android/music/xdi/XdiContentProvider;->LOGV:Z

    if-eqz v0, :cond_0

    .line 532
    const-string v0, "MusicXdi"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Insert not supported.  URI: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 534
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()Z
    .locals 1

    .prologue
    .line 547
    invoke-virtual {p0}, Lcom/google/android/music/xdi/XdiContentProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/music/xdi/XdiUtils;->isXdiEnvironment(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 551
    invoke-virtual {p0}, Lcom/google/android/music/xdi/XdiContentProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/music/tutorial/SignupStatus;->launchVerificationCheck(Landroid/content/Context;)V

    .line 555
    invoke-virtual {p0}, Lcom/google/android/music/xdi/XdiContentProvider;->registerMusicContentObserver()V

    .line 558
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 571
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v0

    .line 573
    .local v0, "token":J
    :try_start_0
    invoke-direct/range {p0 .. p5}, Lcom/google/android/music/xdi/XdiContentProvider;->queryImpl(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 575
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    return-object v2

    :catchall_0
    move-exception v2

    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v2
.end method

.method registerMusicContentObserver()V
    .locals 4

    .prologue
    .line 1041
    iget-object v0, p0, Lcom/google/android/music/xdi/XdiContentProvider;->mMusicContentObserver:Landroid/database/ContentObserver;

    if-nez v0, :cond_0

    .line 1042
    new-instance v0, Lcom/google/android/music/xdi/XdiContentProvider$1;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/google/android/music/xdi/XdiContentProvider$1;-><init>(Lcom/google/android/music/xdi/XdiContentProvider;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/google/android/music/xdi/XdiContentProvider;->mMusicContentObserver:Landroid/database/ContentObserver;

    .line 1048
    invoke-virtual {p0}, Lcom/google/android/music/xdi/XdiContentProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/music/store/MusicContent;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/music/xdi/XdiContentProvider;->mMusicContentObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 1051
    :cond_0
    return-void
.end method

.method public shutdown()V
    .locals 0

    .prologue
    .line 564
    invoke-virtual {p0}, Lcom/google/android/music/xdi/XdiContentProvider;->unregisterMusicContentObserver()V

    .line 565
    return-void
.end method

.method unregisterMusicContentObserver()V
    .locals 2

    .prologue
    .line 1054
    iget-object v0, p0, Lcom/google/android/music/xdi/XdiContentProvider;->mMusicContentObserver:Landroid/database/ContentObserver;

    if-eqz v0, :cond_0

    .line 1055
    invoke-virtual {p0}, Lcom/google/android/music/xdi/XdiContentProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/xdi/XdiContentProvider;->mMusicContentObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 1056
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/xdi/XdiContentProvider;->mMusicContentObserver:Landroid/database/ContentObserver;

    .line 1058
    :cond_0
    return-void
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 523
    sget-boolean v0, Lcom/google/android/music/xdi/XdiContentProvider;->LOGV:Z

    if-eqz v0, :cond_0

    .line 524
    const-string v0, "MusicXdi"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Update not supported.  URI: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 526
    :cond_0
    const/4 v0, 0x0

    return v0
.end method
