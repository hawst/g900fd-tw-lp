.class public Lcom/google/android/music/sync/google/ClientSyncState;
.super Ljava/lang/Object;
.source "ClientSyncState.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/sync/google/ClientSyncState$1;,
        Lcom/google/android/music/sync/google/ClientSyncState$Helpers;,
        Lcom/google/android/music/sync/google/ClientSyncState$Builder;
    }
.end annotation


# static fields
.field private static final type:Lcom/google/common/io/protocol/ProtoBufType;


# instance fields
.field final mEtagPlaylist:Ljava/lang/String;

.field final mEtagPlaylistEntry:Ljava/lang/String;

.field final mEtagTrack:Ljava/lang/String;

.field final mRemoteAccount:Ljava/lang/Integer;

.field final mRemoteBlacklistItemVersion:Ljava/lang/Long;

.field final mRemotePlaylistVersion:Ljava/lang/Long;

.field final mRemotePlentryVersion:Ljava/lang/Long;

.field final mRemoteRadioStationVersion:Ljava/lang/Long;

.field final mRemoteTrackVersion:Ljava/lang/Long;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/16 v5, 0x224

    const/16 v4, 0x213

    const/4 v3, 0x0

    .line 24
    new-instance v0, Lcom/google/common/io/protocol/ProtoBufType;

    const-string v1, "MusicMetadataSyncState"

    invoke-direct {v0, v1}, Lcom/google/common/io/protocol/ProtoBufType;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/music/sync/google/ClientSyncState;->type:Lcom/google/common/io/protocol/ProtoBufType;

    .line 40
    sget-object v0, Lcom/google/android/music/sync/google/ClientSyncState;->type:Lcom/google/common/io/protocol/ProtoBufType;

    const/16 v1, 0x215

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/common/io/protocol/ProtoBufType;

    .line 41
    sget-object v0, Lcom/google/android/music/sync/google/ClientSyncState;->type:Lcom/google/common/io/protocol/ProtoBufType;

    const/4 v1, 0x2

    invoke-virtual {v0, v4, v1, v3}, Lcom/google/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/common/io/protocol/ProtoBufType;

    .line 43
    sget-object v0, Lcom/google/android/music/sync/google/ClientSyncState;->type:Lcom/google/common/io/protocol/ProtoBufType;

    const/4 v1, 0x3

    invoke-virtual {v0, v4, v1, v3}, Lcom/google/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/common/io/protocol/ProtoBufType;

    .line 45
    sget-object v0, Lcom/google/android/music/sync/google/ClientSyncState;->type:Lcom/google/common/io/protocol/ProtoBufType;

    const/4 v1, 0x4

    invoke-virtual {v0, v4, v1, v3}, Lcom/google/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/common/io/protocol/ProtoBufType;

    .line 47
    sget-object v0, Lcom/google/android/music/sync/google/ClientSyncState;->type:Lcom/google/common/io/protocol/ProtoBufType;

    const/4 v1, 0x5

    invoke-virtual {v0, v5, v1, v3}, Lcom/google/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/common/io/protocol/ProtoBufType;

    .line 49
    sget-object v0, Lcom/google/android/music/sync/google/ClientSyncState;->type:Lcom/google/common/io/protocol/ProtoBufType;

    const/4 v1, 0x6

    invoke-virtual {v0, v5, v1, v3}, Lcom/google/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/common/io/protocol/ProtoBufType;

    .line 51
    sget-object v0, Lcom/google/android/music/sync/google/ClientSyncState;->type:Lcom/google/common/io/protocol/ProtoBufType;

    const/4 v1, 0x7

    invoke-virtual {v0, v5, v1, v3}, Lcom/google/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/common/io/protocol/ProtoBufType;

    .line 53
    sget-object v0, Lcom/google/android/music/sync/google/ClientSyncState;->type:Lcom/google/common/io/protocol/ProtoBufType;

    const/16 v1, 0x8

    invoke-virtual {v0, v4, v1, v3}, Lcom/google/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/common/io/protocol/ProtoBufType;

    .line 55
    sget-object v0, Lcom/google/android/music/sync/google/ClientSyncState;->type:Lcom/google/common/io/protocol/ProtoBufType;

    const/16 v1, 0x9

    invoke-virtual {v0, v4, v1, v3}, Lcom/google/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/common/io/protocol/ProtoBufType;

    .line 58
    return-void
.end method

.method private constructor <init>(Ljava/lang/Integer;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "remoteAccount"    # Ljava/lang/Integer;
    .param p2, "remoteTrackVersion"    # Ljava/lang/Long;
    .param p3, "remotePlaylistVersion"    # Ljava/lang/Long;
    .param p4, "remotePlentryVersion"    # Ljava/lang/Long;
    .param p5, "radioStationVersion"    # Ljava/lang/Long;
    .param p6, "blacklistItemVersion"    # Ljava/lang/Long;
    .param p7, "etagTrack"    # Ljava/lang/String;
    .param p8, "etagPlaylist"    # Ljava/lang/String;
    .param p9, "etagPlaylistEntry"    # Ljava/lang/String;

    .prologue
    .line 258
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 259
    iput-object p1, p0, Lcom/google/android/music/sync/google/ClientSyncState;->mRemoteAccount:Ljava/lang/Integer;

    .line 260
    iput-object p2, p0, Lcom/google/android/music/sync/google/ClientSyncState;->mRemoteTrackVersion:Ljava/lang/Long;

    .line 261
    iput-object p3, p0, Lcom/google/android/music/sync/google/ClientSyncState;->mRemotePlaylistVersion:Ljava/lang/Long;

    .line 262
    iput-object p4, p0, Lcom/google/android/music/sync/google/ClientSyncState;->mRemotePlentryVersion:Ljava/lang/Long;

    .line 263
    iput-object p5, p0, Lcom/google/android/music/sync/google/ClientSyncState;->mRemoteRadioStationVersion:Ljava/lang/Long;

    .line 264
    iput-object p6, p0, Lcom/google/android/music/sync/google/ClientSyncState;->mRemoteBlacklistItemVersion:Ljava/lang/Long;

    .line 265
    iput-object p7, p0, Lcom/google/android/music/sync/google/ClientSyncState;->mEtagTrack:Ljava/lang/String;

    .line 266
    iput-object p8, p0, Lcom/google/android/music/sync/google/ClientSyncState;->mEtagPlaylist:Ljava/lang/String;

    .line 267
    iput-object p9, p0, Lcom/google/android/music/sync/google/ClientSyncState;->mEtagPlaylistEntry:Ljava/lang/String;

    .line 269
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/Integer;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/music/sync/google/ClientSyncState$1;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Integer;
    .param p2, "x1"    # Ljava/lang/Long;
    .param p3, "x2"    # Ljava/lang/Long;
    .param p4, "x3"    # Ljava/lang/Long;
    .param p5, "x4"    # Ljava/lang/Long;
    .param p6, "x5"    # Ljava/lang/Long;
    .param p7, "x6"    # Ljava/lang/String;
    .param p8, "x7"    # Ljava/lang/String;
    .param p9, "x8"    # Ljava/lang/String;
    .param p10, "x9"    # Lcom/google/android/music/sync/google/ClientSyncState$1;

    .prologue
    .line 23
    invoke-direct/range {p0 .. p9}, Lcom/google/android/music/sync/google/ClientSyncState;-><init>(Ljava/lang/Integer;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static newBuilder()Lcom/google/android/music/sync/google/ClientSyncState$Builder;
    .locals 2

    .prologue
    .line 272
    new-instance v0, Lcom/google/android/music/sync/google/ClientSyncState$Builder;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/music/sync/google/ClientSyncState$Builder;-><init>(Lcom/google/android/music/sync/google/ClientSyncState$1;)V

    return-object v0
.end method

.method public static newBuilder(Lcom/google/android/music/sync/google/ClientSyncState;)Lcom/google/android/music/sync/google/ClientSyncState$Builder;
    .locals 2
    .param p0, "clone"    # Lcom/google/android/music/sync/google/ClientSyncState;

    .prologue
    .line 276
    new-instance v0, Lcom/google/android/music/sync/google/ClientSyncState$Builder;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/music/sync/google/ClientSyncState$Builder;-><init>(Lcom/google/android/music/sync/google/ClientSyncState;Lcom/google/android/music/sync/google/ClientSyncState$1;)V

    return-object v0
.end method

.method public static parseFrom([B)Lcom/google/android/music/sync/google/ClientSyncState;
    .locals 14
    .param p0, "b"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/sync/common/ProviderException;
        }
    .end annotation

    .prologue
    .line 280
    new-instance v11, Lcom/google/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/android/music/sync/google/ClientSyncState;->type:Lcom/google/common/io/protocol/ProtoBufType;

    invoke-direct {v11, v0}, Lcom/google/common/io/protocol/ProtoBuf;-><init>(Lcom/google/common/io/protocol/ProtoBufType;)V

    .line 282
    .local v11, "item":Lcom/google/common/io/protocol/ProtoBuf;
    :try_start_0
    invoke-virtual {v11, p0}, Lcom/google/common/io/protocol/ProtoBuf;->parse([B)Lcom/google/common/io/protocol/ProtoBuf;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 286
    const/4 v0, 0x1

    invoke-virtual {v11, v0}, Lcom/google/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {v11, v0}, Lcom/google/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 288
    .local v1, "remoteAccount":Ljava/lang/Integer;
    :goto_0
    const/4 v0, 0x2

    invoke-virtual {v11, v0}, Lcom/google/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {v11, v0}, Lcom/google/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 290
    .local v2, "remoteTrackVersion":Ljava/lang/Long;
    :goto_1
    const/4 v0, 0x3

    invoke-virtual {v11, v0}, Lcom/google/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {v11, v0}, Lcom/google/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    .line 292
    .local v3, "remotePlaylistVersion":Ljava/lang/Long;
    :goto_2
    const/4 v0, 0x4

    invoke-virtual {v11, v0}, Lcom/google/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {v11, v0}, Lcom/google/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    .line 294
    .local v4, "remotePlentryVersion":Ljava/lang/Long;
    :goto_3
    const/16 v0, 0x8

    invoke-virtual {v11, v0}, Lcom/google/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_4

    const/16 v0, 0x8

    invoke-virtual {v11, v0}, Lcom/google/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    .line 296
    .local v5, "remoteRadioStationVersion":Ljava/lang/Long;
    :goto_4
    const/16 v0, 0x9

    invoke-virtual {v11, v0}, Lcom/google/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_5

    const/16 v0, 0x9

    invoke-virtual {v11, v0}, Lcom/google/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    .line 298
    .local v6, "remoteBlacklistItemVersion":Ljava/lang/Long;
    :goto_5
    const/4 v0, 0x5

    invoke-virtual {v11, v0}, Lcom/google/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x5

    invoke-virtual {v11, v0}, Lcom/google/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 300
    .local v7, "etagTrack":Ljava/lang/String;
    :goto_6
    const/4 v0, 0x6

    invoke-virtual {v11, v0}, Lcom/google/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_7

    const/4 v0, 0x6

    invoke-virtual {v11, v0}, Lcom/google/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 302
    .local v8, "etagPlaylist":Ljava/lang/String;
    :goto_7
    const/4 v0, 0x7

    invoke-virtual {v11, v0}, Lcom/google/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_8

    const/4 v0, 0x7

    invoke-virtual {v11, v0}, Lcom/google/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 304
    .local v9, "etagPlaylistEntry":Ljava/lang/String;
    :goto_8
    new-instance v0, Lcom/google/android/music/sync/google/ClientSyncState;

    invoke-direct/range {v0 .. v9}, Lcom/google/android/music/sync/google/ClientSyncState;-><init>(Ljava/lang/Integer;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    .line 283
    .end local v1    # "remoteAccount":Ljava/lang/Integer;
    .end local v2    # "remoteTrackVersion":Ljava/lang/Long;
    .end local v3    # "remotePlaylistVersion":Ljava/lang/Long;
    .end local v4    # "remotePlentryVersion":Ljava/lang/Long;
    .end local v5    # "remoteRadioStationVersion":Ljava/lang/Long;
    .end local v6    # "remoteBlacklistItemVersion":Ljava/lang/Long;
    .end local v7    # "etagTrack":Ljava/lang/String;
    .end local v8    # "etagPlaylist":Ljava/lang/String;
    .end local v9    # "etagPlaylistEntry":Ljava/lang/String;
    :catch_0
    move-exception v10

    .line 284
    .local v10, "e":Ljava/io/IOException;
    new-instance v0, Lcom/google/android/music/sync/common/ProviderException;

    invoke-direct {v0, v10}, Lcom/google/android/music/sync/common/ProviderException;-><init>(Ljava/lang/Throwable;)V

    throw v0

    .line 286
    .end local v10    # "e":Ljava/io/IOException;
    :cond_0
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 288
    .restart local v1    # "remoteAccount":Ljava/lang/Integer;
    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    .line 290
    .restart local v2    # "remoteTrackVersion":Ljava/lang/Long;
    :cond_2
    const/4 v3, 0x0

    goto :goto_2

    .line 292
    .restart local v3    # "remotePlaylistVersion":Ljava/lang/Long;
    :cond_3
    const/4 v4, 0x0

    goto :goto_3

    .line 294
    .restart local v4    # "remotePlentryVersion":Ljava/lang/Long;
    :cond_4
    const/4 v5, 0x0

    goto :goto_4

    .line 296
    .restart local v5    # "remoteRadioStationVersion":Ljava/lang/Long;
    :cond_5
    const/4 v6, 0x0

    goto :goto_5

    .line 298
    .restart local v6    # "remoteBlacklistItemVersion":Ljava/lang/Long;
    :cond_6
    const/4 v7, 0x0

    goto :goto_6

    .line 300
    .restart local v7    # "etagTrack":Ljava/lang/String;
    :cond_7
    const/4 v8, 0x0

    goto :goto_7

    .line 302
    .restart local v8    # "etagPlaylist":Ljava/lang/String;
    :cond_8
    const/4 v9, 0x0

    goto :goto_8
.end method


# virtual methods
.method public getRemoteAccount()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 375
    iget-object v0, p0, Lcom/google/android/music/sync/google/ClientSyncState;->mRemoteAccount:Ljava/lang/Integer;

    return-object v0
.end method

.method public getRemotePlaylistVersion()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 359
    iget-object v0, p0, Lcom/google/android/music/sync/google/ClientSyncState;->mRemotePlaylistVersion:Ljava/lang/Long;

    return-object v0
.end method

.method public getRemotePlentryVersion()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 363
    iget-object v0, p0, Lcom/google/android/music/sync/google/ClientSyncState;->mRemotePlentryVersion:Ljava/lang/Long;

    return-object v0
.end method

.method public getRemoteTrackVersion()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 355
    iget-object v0, p0, Lcom/google/android/music/sync/google/ClientSyncState;->mRemoteTrackVersion:Ljava/lang/Long;

    return-object v0
.end method

.method public toBytes()[B
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/sync/common/ProviderException;
        }
    .end annotation

    .prologue
    .line 310
    new-instance v1, Lcom/google/common/io/protocol/ProtoBuf;

    sget-object v2, Lcom/google/android/music/sync/google/ClientSyncState;->type:Lcom/google/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v2}, Lcom/google/common/io/protocol/ProtoBuf;-><init>(Lcom/google/common/io/protocol/ProtoBufType;)V

    .line 311
    .local v1, "item":Lcom/google/common/io/protocol/ProtoBuf;
    iget-object v2, p0, Lcom/google/android/music/sync/google/ClientSyncState;->mRemoteAccount:Ljava/lang/Integer;

    if-eqz v2, :cond_0

    .line 312
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/music/sync/google/ClientSyncState;->mRemoteAccount:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/google/common/io/protocol/ProtoBuf;->addInt(II)V

    .line 315
    :cond_0
    iget-object v2, p0, Lcom/google/android/music/sync/google/ClientSyncState;->mRemoteTrackVersion:Ljava/lang/Long;

    if-eqz v2, :cond_1

    .line 316
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/music/sync/google/ClientSyncState;->mRemoteTrackVersion:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v1, v2, v4, v5}, Lcom/google/common/io/protocol/ProtoBuf;->addLong(IJ)V

    .line 319
    :cond_1
    iget-object v2, p0, Lcom/google/android/music/sync/google/ClientSyncState;->mRemotePlaylistVersion:Ljava/lang/Long;

    if-eqz v2, :cond_2

    .line 320
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/android/music/sync/google/ClientSyncState;->mRemotePlaylistVersion:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v1, v2, v4, v5}, Lcom/google/common/io/protocol/ProtoBuf;->addLong(IJ)V

    .line 323
    :cond_2
    iget-object v2, p0, Lcom/google/android/music/sync/google/ClientSyncState;->mRemotePlentryVersion:Ljava/lang/Long;

    if-eqz v2, :cond_3

    .line 324
    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/android/music/sync/google/ClientSyncState;->mRemotePlentryVersion:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v1, v2, v4, v5}, Lcom/google/common/io/protocol/ProtoBuf;->addLong(IJ)V

    .line 327
    :cond_3
    iget-object v2, p0, Lcom/google/android/music/sync/google/ClientSyncState;->mRemoteRadioStationVersion:Ljava/lang/Long;

    if-eqz v2, :cond_4

    .line 328
    const/16 v2, 0x8

    iget-object v3, p0, Lcom/google/android/music/sync/google/ClientSyncState;->mRemoteRadioStationVersion:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v1, v2, v4, v5}, Lcom/google/common/io/protocol/ProtoBuf;->addLong(IJ)V

    .line 331
    :cond_4
    iget-object v2, p0, Lcom/google/android/music/sync/google/ClientSyncState;->mRemoteBlacklistItemVersion:Ljava/lang/Long;

    if-eqz v2, :cond_5

    .line 332
    const/16 v2, 0x9

    iget-object v3, p0, Lcom/google/android/music/sync/google/ClientSyncState;->mRemoteBlacklistItemVersion:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v1, v2, v4, v5}, Lcom/google/common/io/protocol/ProtoBuf;->addLong(IJ)V

    .line 335
    :cond_5
    iget-object v2, p0, Lcom/google/android/music/sync/google/ClientSyncState;->mEtagTrack:Ljava/lang/String;

    if-eqz v2, :cond_6

    .line 336
    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/android/music/sync/google/ClientSyncState;->mEtagTrack:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/google/common/io/protocol/ProtoBuf;->addString(ILjava/lang/String;)V

    .line 339
    :cond_6
    iget-object v2, p0, Lcom/google/android/music/sync/google/ClientSyncState;->mEtagPlaylist:Ljava/lang/String;

    if-eqz v2, :cond_7

    .line 340
    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/android/music/sync/google/ClientSyncState;->mEtagPlaylist:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/google/common/io/protocol/ProtoBuf;->addString(ILjava/lang/String;)V

    .line 343
    :cond_7
    iget-object v2, p0, Lcom/google/android/music/sync/google/ClientSyncState;->mEtagPlaylistEntry:Ljava/lang/String;

    if-eqz v2, :cond_8

    .line 344
    const/4 v2, 0x7

    iget-object v3, p0, Lcom/google/android/music/sync/google/ClientSyncState;->mEtagPlaylistEntry:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/google/common/io/protocol/ProtoBuf;->addString(ILjava/lang/String;)V

    .line 348
    :cond_8
    :try_start_0
    invoke-virtual {v1}, Lcom/google/common/io/protocol/ProtoBuf;->toByteArray()[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    return-object v2

    .line 349
    :catch_0
    move-exception v0

    .line 350
    .local v0, "e":Ljava/io/IOException;
    new-instance v2, Lcom/google/android/music/sync/common/ProviderException;

    invoke-direct {v2, v0}, Lcom/google/android/music/sync/common/ProviderException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 380
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "remoteAccount:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/sync/google/ClientSyncState;->mRemoteAccount:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; remoteTrackVersion: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/sync/google/ClientSyncState;->mRemoteTrackVersion:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; remotePlaylistVersion: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/sync/google/ClientSyncState;->mRemotePlaylistVersion:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; remotePlaylistEntryVersion: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/sync/google/ClientSyncState;->mRemotePlentryVersion:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; remoteRadioStationVersion: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/sync/google/ClientSyncState;->mRemoteRadioStationVersion:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; remoteBlacklistItemVersion: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/sync/google/ClientSyncState;->mRemoteBlacklistItemVersion:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; etagTrack: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/sync/google/ClientSyncState;->mEtagTrack:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; etagPlaylist: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/sync/google/ClientSyncState;->mEtagPlaylist:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; etagPlaylistEntry: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/sync/google/ClientSyncState;->mEtagPlaylistEntry:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
