.class Lcom/google/android/music/ui/BaseActivity$3;
.super Landroid/support/v4/widget/DrawerLayout$SimpleDrawerListener;
.source "BaseActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/BaseActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/BaseActivity;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/BaseActivity;)V
    .locals 0

    .prologue
    .line 237
    iput-object p1, p0, Lcom/google/android/music/ui/BaseActivity$3;->this$0:Lcom/google/android/music/ui/BaseActivity;

    invoke-direct {p0}, Landroid/support/v4/widget/DrawerLayout$SimpleDrawerListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onDrawerClosed(Landroid/view/View;)V
    .locals 3
    .param p1, "drawerView"    # Landroid/view/View;

    .prologue
    .line 240
    iget-object v1, p0, Lcom/google/android/music/ui/BaseActivity$3;->this$0:Lcom/google/android/music/ui/BaseActivity;

    invoke-virtual {v1}, Lcom/google/android/music/ui/BaseActivity;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    .line 241
    .local v0, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->wasSidePannelClosedOnce()Z

    move-result v1

    if-nez v1, :cond_0

    .line 242
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->setSidePannelWasClosedOnce(Z)V

    .line 245
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/ui/BaseActivity$3;->this$0:Lcom/google/android/music/ui/BaseActivity;

    # invokes: Lcom/google/android/music/ui/BaseActivity;->restoreActionBarTitle()V
    invoke-static {v1}, Lcom/google/android/music/ui/BaseActivity;->access$200(Lcom/google/android/music/ui/BaseActivity;)V

    .line 246
    iget-object v1, p0, Lcom/google/android/music/ui/BaseActivity$3;->this$0:Lcom/google/android/music/ui/BaseActivity;

    iget-object v2, p0, Lcom/google/android/music/ui/BaseActivity$3;->this$0:Lcom/google/android/music/ui/BaseActivity;

    # getter for: Lcom/google/android/music/ui/BaseActivity;->mActionBarAlpha:I
    invoke-static {v2}, Lcom/google/android/music/ui/BaseActivity;->access$300(Lcom/google/android/music/ui/BaseActivity;)I

    move-result v2

    # invokes: Lcom/google/android/music/ui/BaseActivity;->setActionBarAlphaInternal(I)V
    invoke-static {v1, v2}, Lcom/google/android/music/ui/BaseActivity;->access$400(Lcom/google/android/music/ui/BaseActivity;I)V

    .line 247
    return-void
.end method

.method public onDrawerOpened(Landroid/view/View;)V
    .locals 3
    .param p1, "drawerView"    # Landroid/view/View;

    .prologue
    .line 252
    iget-object v0, p0, Lcom/google/android/music/ui/BaseActivity$3;->this$0:Lcom/google/android/music/ui/BaseActivity;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/music/ui/BaseActivity;->mDrawerAutoHide:Z
    invoke-static {v0, v1}, Lcom/google/android/music/ui/BaseActivity;->access$502(Lcom/google/android/music/ui/BaseActivity;Z)Z

    .line 253
    iget-object v0, p0, Lcom/google/android/music/ui/BaseActivity$3;->this$0:Lcom/google/android/music/ui/BaseActivity;

    const v1, 0x7f0b00e3

    const/4 v2, 0x1

    # invokes: Lcom/google/android/music/ui/BaseActivity;->setActionBarTitleInternal(IZ)V
    invoke-static {v0, v1, v2}, Lcom/google/android/music/ui/BaseActivity;->access$600(Lcom/google/android/music/ui/BaseActivity;IZ)V

    .line 254
    iget-object v0, p0, Lcom/google/android/music/ui/BaseActivity$3;->this$0:Lcom/google/android/music/ui/BaseActivity;

    const/16 v1, 0xff

    # invokes: Lcom/google/android/music/ui/BaseActivity;->setActionBarAlphaInternal(I)V
    invoke-static {v0, v1}, Lcom/google/android/music/ui/BaseActivity;->access$400(Lcom/google/android/music/ui/BaseActivity;I)V

    .line 255
    return-void
.end method

.method public onDrawerSlide(Landroid/view/View;F)V
    .locals 3
    .param p1, "drawerView"    # Landroid/view/View;
    .param p2, "slideOffset"    # F

    .prologue
    .line 259
    iget-object v1, p0, Lcom/google/android/music/ui/BaseActivity$3;->this$0:Lcom/google/android/music/ui/BaseActivity;

    # getter for: Lcom/google/android/music/ui/BaseActivity;->mActionBarAlpha:I
    invoke-static {v1}, Lcom/google/android/music/ui/BaseActivity;->access$300(Lcom/google/android/music/ui/BaseActivity;)I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/google/android/music/ui/BaseActivity$3;->this$0:Lcom/google/android/music/ui/BaseActivity;

    # getter for: Lcom/google/android/music/ui/BaseActivity;->mActionBarAlpha:I
    invoke-static {v2}, Lcom/google/android/music/ui/BaseActivity;->access$300(Lcom/google/android/music/ui/BaseActivity;)I

    move-result v2

    rsub-int v2, v2, 0xff

    int-to-float v2, v2

    mul-float/2addr v2, p2

    add-float/2addr v1, v2

    float-to-int v0, v1

    .line 261
    .local v0, "newAlpha":I
    iget-object v1, p0, Lcom/google/android/music/ui/BaseActivity$3;->this$0:Lcom/google/android/music/ui/BaseActivity;

    # invokes: Lcom/google/android/music/ui/BaseActivity;->setActionBarAlphaInternal(I)V
    invoke-static {v1, v0}, Lcom/google/android/music/ui/BaseActivity;->access$400(Lcom/google/android/music/ui/BaseActivity;I)V

    .line 262
    return-void
.end method

.method public onDrawerStateChanged(I)V
    .locals 1
    .param p1, "newState"    # I

    .prologue
    .line 266
    iget-object v0, p0, Lcom/google/android/music/ui/BaseActivity$3;->this$0:Lcom/google/android/music/ui/BaseActivity;

    invoke-virtual {v0}, Lcom/google/android/music/ui/BaseActivity;->supportInvalidateOptionsMenu()V

    .line 267
    return-void
.end method
