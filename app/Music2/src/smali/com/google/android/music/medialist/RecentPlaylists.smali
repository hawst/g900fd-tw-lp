.class public Lcom/google/android/music/medialist/RecentPlaylists;
.super Lcom/google/android/music/medialist/PlaylistsList;
.source "RecentPlaylists.java"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/music/medialist/RecentPlaylists;",
            ">;"
        }
    .end annotation
.end field

.field private static final LIMIT:S = -0x1s


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    new-instance v0, Lcom/google/android/music/medialist/RecentPlaylists$1;

    invoke-direct {v0}, Lcom/google/android/music/medialist/RecentPlaylists$1;-><init>()V

    sput-object v0, Lcom/google/android/music/medialist/RecentPlaylists;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/google/android/music/medialist/PlaylistsList;-><init>()V

    .line 21
    return-void
.end method


# virtual methods
.method public getContentUri(Landroid/content/Context;)Landroid/net/Uri;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 25
    const/4 v0, -0x1

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/music/store/MusicContent$Playlists;->getRecentPlaylistUri(IZ)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method
