.class public Lcom/google/android/music/ui/QueueTrackListAdapter;
.super Lcom/google/android/music/ui/TrackListAdapter;
.source "QueueTrackListAdapter.java"


# static fields
.field private static mFadeAmount:F


# instance fields
.field private mEnabled:Z


# direct methods
.method public constructor <init>(Lcom/google/android/music/ui/MusicFragment;ZLcom/google/android/music/medialist/MediaList;)V
    .locals 3
    .param p1, "musicFragment"    # Lcom/google/android/music/ui/MusicFragment;
    .param p2, "leftPadding"    # Z
    .param p3, "medialist"    # Lcom/google/android/music/medialist/MediaList;

    .prologue
    .line 42
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/music/ui/TrackListAdapter;-><init>(Lcom/google/android/music/ui/MusicFragment;ZLcom/google/android/music/medialist/MediaList;)V

    .line 28
    invoke-virtual {p0}, Lcom/google/android/music/ui/QueueTrackListAdapter;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 29
    .local v0, "res":Landroid/content/res/Resources;
    const v1, 0x7f0d0011

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x42c80000    # 100.0f

    div-float/2addr v1, v2

    sput v1, Lcom/google/android/music/ui/QueueTrackListAdapter;->mFadeAmount:F

    .line 31
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/music/ui/QueueTrackListAdapter;->mEnabled:Z

    .line 43
    const v1, 0x7f04008c

    invoke-virtual {p0, v1}, Lcom/google/android/music/ui/QueueTrackListAdapter;->setViewResource(I)V

    .line 44
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 55
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/music/ui/TrackListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 58
    .local v0, "v":Landroid/view/View;
    iget-boolean v1, p0, Lcom/google/android/music/ui/QueueTrackListAdapter;->mEnabled:Z

    if-nez v1, :cond_0

    .line 59
    sget v1, Lcom/google/android/music/ui/QueueTrackListAdapter;->mFadeAmount:F

    invoke-static {v0, v1}, Lcom/google/android/music/utils/ViewUtils;->setAlpha(Landroid/view/View;F)V

    .line 61
    :cond_0
    return-object v0
.end method

.method public setEnabled(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 48
    iget-boolean v0, p0, Lcom/google/android/music/ui/QueueTrackListAdapter;->mEnabled:Z

    if-ne p1, v0, :cond_0

    .line 51
    :goto_0
    return-void

    .line 49
    :cond_0
    iput-boolean p1, p0, Lcom/google/android/music/ui/QueueTrackListAdapter;->mEnabled:Z

    .line 50
    invoke-virtual {p0}, Lcom/google/android/music/ui/QueueTrackListAdapter;->notifyDataSetChanged()V

    goto :goto_0
.end method
