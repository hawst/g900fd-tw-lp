.class public abstract Lcom/google/android/music/art/ArtTypeHandler;
.super Ljava/lang/Object;
.source "ArtTypeHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/art/ArtTypeHandler$1;,
        Lcom/google/android/music/art/ArtTypeHandler$PlaylistArtType;
    }
.end annotation


# static fields
.field static final ALBUM_ID_COLUMNS:[Ljava/lang/String;

.field static final GENRE_ALBUM_COLUMNS:[Ljava/lang/String;

.field static final LOGV:Z

.field static final NAUTILUS_ART_COLUMNS:[Ljava/lang/String;

.field static final RADIO_COLUMNS:[Ljava/lang/String;

.field static final XAUDIO_ALBUM_ART_COLUMNS:[Ljava/lang/String;


# instance fields
.field protected final mAppContext:Landroid/content/Context;

.field protected final mResolver:Lcom/google/android/music/art/ArtResolver2;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 42
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->ART_RESOLVER:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/art/ArtTypeHandler;->LOGV:Z

    .line 56
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "AlbumArtLocation"

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/music/art/ArtTypeHandler;->XAUDIO_ALBUM_ART_COLUMNS:[Ljava/lang/String;

    .line 60
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "artworkUrl"

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/music/art/ArtTypeHandler;->NAUTILUS_ART_COLUMNS:[Ljava/lang/String;

    .line 64
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "radio_art"

    aput-object v1, v0, v3

    const-string v1, "radio_seed_source_type"

    aput-object v1, v0, v4

    const/4 v1, 0x2

    const-string v2, "radio_seed_source_id"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/art/ArtTypeHandler;->RADIO_COLUMNS:[Ljava/lang/String;

    .line 73
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "album_id"

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/music/art/ArtTypeHandler;->ALBUM_ID_COLUMNS:[Ljava/lang/String;

    .line 76
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "album_id"

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/music/art/ArtTypeHandler;->GENRE_ALBUM_COLUMNS:[Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Lcom/google/android/music/art/ArtResolver2;Landroid/content/Context;)V
    .locals 1
    .param p1, "resolver"    # Lcom/google/android/music/art/ArtResolver2;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    iput-object p1, p0, Lcom/google/android/music/art/ArtTypeHandler;->mResolver:Lcom/google/android/music/art/ArtResolver2;

    .line 85
    invoke-virtual {p2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/art/ArtTypeHandler;->mAppContext:Landroid/content/Context;

    .line 86
    return-void
.end method

.method private lookupPlaylistRadioFallbackId(Ljava/lang/String;)J
    .locals 10
    .param p1, "seedSourceId"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 330
    const-wide/16 v8, -0x1

    .line 331
    .local v8, "playlistId":J
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "playlist_id"

    aput-object v0, v2, v3

    .line 335
    .local v2, "listCols":[Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/music/store/MusicContent$Playlists;->getPlaylistUriByRemoteSourceId(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 337
    .local v1, "listUri":Landroid/net/Uri;
    const/4 v6, 0x0

    .line 339
    .local v6, "c":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/art/ArtTypeHandler;->mAppContext:Landroid/content/Context;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 340
    sget-boolean v0, Lcom/google/android/music/art/ArtTypeHandler;->LOGV:Z

    if-eqz v0, :cond_0

    .line 341
    const-string v0, "ArtTypeHandler"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "lookupPlaylistRadioFallbackId seedSourceId="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", results="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 344
    :cond_0
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 345
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v8

    .line 348
    :cond_1
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 350
    return-wide v8

    .line 348
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method


# virtual methods
.method protected final addArtByAlbumId(Lcom/google/android/music/art/ArtRequest2;J)V
    .locals 10
    .param p1, "request"    # Lcom/google/android/music/art/ArtRequest2;
    .param p2, "albumId"    # J

    .prologue
    .line 146
    invoke-static {p2, p3}, Lcom/google/android/music/store/MusicContent$Albums;->getAudioInAlbumUri(J)Landroid/net/Uri;

    move-result-object v1

    .line 147
    .local v1, "tracksUri":Landroid/net/Uri;
    const/4 v8, 0x0

    .line 148
    .local v8, "c":Landroid/database/Cursor;
    const/4 v6, 0x0

    .line 150
    .local v6, "albumArtUrl":Ljava/lang/String;
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/art/ArtTypeHandler;->mAppContext:Landroid/content/Context;

    sget-object v2, Lcom/google/android/music/art/ArtTypeHandler;->XAUDIO_ALBUM_ART_COLUMNS:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 152
    :cond_0
    :goto_0
    if-eqz v8, :cond_1

    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    if-nez v6, :cond_1

    .line 153
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 154
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v6

    goto :goto_0

    .line 158
    :cond_1
    invoke-static {v8}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 160
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 161
    new-instance v7, Lcom/google/android/music/art/ArtResolver2$ArtUrl;

    const/4 v0, 0x1

    invoke-direct {v7, v6, v0}, Lcom/google/android/music/art/ArtResolver2$ArtUrl;-><init>(Ljava/lang/String;Z)V

    .line 162
    .local v7, "artUrl":Lcom/google/android/music/art/ArtResolver2$ArtUrl;
    invoke-virtual {p1, v7}, Lcom/google/android/music/art/ArtRequest2;->addNeededItem(Lcom/google/android/music/art/ArtResolver2$ArtUrl;)V

    .line 167
    .end local v7    # "artUrl":Lcom/google/android/music/art/ArtResolver2$ArtUrl;
    :goto_1
    return-void

    .line 158
    :catchall_0
    move-exception v0

    invoke-static {v8}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0

    .line 164
    :cond_2
    const-string v0, "ArtTypeHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unable to resolve album id \'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\' to a URL. No art will be "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "shown"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method protected final addArtByArtistId(Lcom/google/android/music/art/ArtRequest2;JLjava/lang/String;)V
    .locals 14
    .param p1, "request"    # Lcom/google/android/music/art/ArtRequest2;
    .param p2, "artistId"    # J
    .param p4, "artistMetajamId"    # Ljava/lang/String;

    .prologue
    .line 178
    invoke-static/range {p4 .. p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-wide/16 v4, 0x0

    cmp-long v2, p2, v4

    if-gez v2, :cond_0

    .line 179
    const-string v2, "ArtTypeHandler"

    const-string v4, "artistId and metajamId are both empty. Unable to look up artist art."

    invoke-static {v2, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 213
    :goto_0
    return-void

    .line 183
    :cond_0
    invoke-static/range {p4 .. p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 184
    invoke-static/range {p4 .. p4}, Lcom/google/android/music/store/MusicContent$Artists;->getNautilusArtistsUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 188
    .local v3, "artistUri":Landroid/net/Uri;
    :goto_1
    const/4 v12, 0x0

    .line 189
    .local v12, "c":Landroid/database/Cursor;
    const/4 v10, 0x0

    .line 191
    .local v10, "artUri":Ljava/lang/String;
    :try_start_0
    iget-object v2, p0, Lcom/google/android/music/art/ArtTypeHandler;->mAppContext:Landroid/content/Context;

    sget-object v4, Lcom/google/android/music/art/ArtTypeHandler;->NAUTILUS_ART_COLUMNS:[Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 193
    if-eqz v12, :cond_1

    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 194
    const/4 v2, 0x0

    invoke-interface {v12, v2}, Landroid/database/Cursor;->isNull(I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_3

    const/4 v10, 0x0

    .line 197
    :cond_1
    :goto_2
    invoke-static {v12}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 199
    if-eqz v10, :cond_4

    .line 200
    new-instance v11, Lcom/google/android/music/art/ArtResolver2$ArtUrl;

    const/4 v2, 0x0

    invoke-direct {v11, v10, v2}, Lcom/google/android/music/art/ArtResolver2$ArtUrl;-><init>(Ljava/lang/String;Z)V

    .line 201
    .local v11, "artUrl":Lcom/google/android/music/art/ArtResolver2$ArtUrl;
    invoke-virtual {p1, v11}, Lcom/google/android/music/art/ArtRequest2;->addNeededItem(Lcom/google/android/music/art/ArtResolver2$ArtUrl;)V

    goto :goto_0

    .line 186
    .end local v3    # "artistUri":Landroid/net/Uri;
    .end local v10    # "artUri":Ljava/lang/String;
    .end local v11    # "artUrl":Lcom/google/android/music/art/ArtResolver2$ArtUrl;
    .end local v12    # "c":Landroid/database/Cursor;
    :cond_2
    invoke-static/range {p2 .. p3}, Lcom/google/android/music/store/MusicContent$Artists;->getArtistsUri(J)Landroid/net/Uri;

    move-result-object v3

    .restart local v3    # "artistUri":Landroid/net/Uri;
    goto :goto_1

    .line 194
    .restart local v10    # "artUri":Ljava/lang/String;
    .restart local v12    # "c":Landroid/database/Cursor;
    :cond_3
    const/4 v2, 0x0

    :try_start_1
    invoke-interface {v12, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v10

    goto :goto_2

    .line 197
    :catchall_0
    move-exception v2

    invoke-static {v12}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v2

    .line 203
    :cond_4
    const-wide/16 v4, 0x0

    cmp-long v2, p2, v4

    if-gez v2, :cond_5

    .line 206
    const-string v2, "ArtTypeHandler"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "No artUri available and artist ID is fake. Unable to look up art for artist"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 209
    :cond_5
    invoke-static/range {p2 .. p3}, Lcom/google/android/music/store/MusicContent$Artists;->getAlbumsByArtistsUri(J)Landroid/net/Uri;

    move-result-object v3

    .line 210
    const/4 v7, 0x1

    sget-object v8, Lcom/google/android/music/art/ArtTypeHandler;->NAUTILUS_ART_COLUMNS:[Ljava/lang/String;

    const/4 v9, 0x1

    move-object v4, p0

    move-object v5, p1

    move-object v6, v3

    invoke-virtual/range {v4 .. v9}, Lcom/google/android/music/art/ArtTypeHandler;->addUrlsFromUri(Lcom/google/android/music/art/ArtRequest2;Landroid/net/Uri;Z[Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method protected final addMultiUrlStringItems(Lcom/google/android/music/art/ArtRequest2;Ljava/lang/String;Z)I
    .locals 7
    .param p1, "request"    # Lcom/google/android/music/art/ArtRequest2;
    .param p2, "multiUrl"    # Ljava/lang/String;
    .param p3, "useAlbumArtPipeline"    # Z

    .prologue
    .line 131
    invoke-static {p2}, Lcom/google/android/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 132
    invoke-static {p2}, Lcom/google/android/music/utils/MusicUtils;->decodeStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/google/android/music/art/ArtTypeHandler;->enforceCompositeItemLimit([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 133
    .local v5, "urls":[Ljava/lang/String;
    move-object v0, v5

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v4, v0, v2

    .line 134
    .local v4, "url":Ljava/lang/String;
    new-instance v1, Lcom/google/android/music/art/ArtResolver2$ArtUrl;

    invoke-direct {v1, v4, p3}, Lcom/google/android/music/art/ArtResolver2$ArtUrl;-><init>(Ljava/lang/String;Z)V

    .line 135
    .local v1, "artUrl":Lcom/google/android/music/art/ArtResolver2$ArtUrl;
    invoke-virtual {p1, v1}, Lcom/google/android/music/art/ArtRequest2;->addNeededItem(Lcom/google/android/music/art/ArtResolver2$ArtUrl;)V

    .line 133
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 137
    .end local v1    # "artUrl":Lcom/google/android/music/art/ArtResolver2$ArtUrl;
    .end local v4    # "url":Ljava/lang/String;
    :cond_0
    array-length v6, v5

    return v6
.end method

.method protected final addUrlsFromUri(Lcom/google/android/music/art/ArtRequest2;Landroid/net/Uri;Z[Ljava/lang/String;Z)V
    .locals 12
    .param p1, "request"    # Lcom/google/android/music/art/ArtRequest2;
    .param p2, "uri"    # Landroid/net/Uri;
    .param p3, "includeExternal"    # Z
    .param p4, "projection"    # [Ljava/lang/String;
    .param p5, "useAlbumPipeline"    # Z

    .prologue
    .line 228
    const/4 v11, 0x0

    .line 229
    .local v11, "c":Landroid/database/Cursor;
    invoke-static {}, Lcom/google/android/play/utils/collections/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v10

    .line 231
    .local v10, "artUrls":Ljava/util/Set;, "Ljava/util/Set<Lcom/google/android/music/art/ArtResolver2$ArtUrl;>;"
    :try_start_0
    iget-object v1, p0, Lcom/google/android/music/art/ArtTypeHandler;->mAppContext:Landroid/content/Context;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v2, p2

    move-object/from16 v3, p4

    move v8, p3

    invoke-static/range {v1 .. v8}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZZ)Landroid/database/Cursor;

    move-result-object v11

    .line 233
    if-eqz v11, :cond_1

    .line 234
    :cond_0
    :goto_0
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 235
    const/4 v1, 0x0

    invoke-interface {v11, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 236
    new-instance v9, Lcom/google/android/music/art/ArtResolver2$ArtUrl;

    const/4 v1, 0x0

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    move/from16 v0, p5

    invoke-direct {v9, v1, v0}, Lcom/google/android/music/art/ArtResolver2$ArtUrl;-><init>(Ljava/lang/String;Z)V

    .line 238
    .local v9, "artUrl":Lcom/google/android/music/art/ArtResolver2$ArtUrl;
    invoke-interface {v10, v9}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 243
    .end local v9    # "artUrl":Lcom/google/android/music/art/ArtResolver2$ArtUrl;
    :catchall_0
    move-exception v1

    invoke-static {v11}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v1

    :cond_1
    invoke-static {v11}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 245
    invoke-virtual {p0, v10}, Lcom/google/android/music/art/ArtTypeHandler;->enforceCompositeItemLimit(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v10

    .line 246
    invoke-virtual {p1, v10}, Lcom/google/android/music/art/ArtRequest2;->addNeededItems(Ljava/util/Collection;)V

    .line 247
    return-void
.end method

.method protected enforceCompositeItemLimit(Ljava/util/Set;)Ljava/util/Set;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/music/art/ArtResolver2$ArtUrl;",
            ">;)",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/music/art/ArtResolver2$ArtUrl;",
            ">;"
        }
    .end annotation

    .prologue
    .local p1, "items":Ljava/util/Set;, "Ljava/util/Set<Lcom/google/android/music/art/ArtResolver2$ArtUrl;>;"
    const/4 v3, 0x5

    .line 371
    invoke-static {p1}, Lcom/google/android/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 372
    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v0

    if-le v0, v3, :cond_1

    .line 373
    sget-boolean v0, Lcom/google/android/music/art/ArtTypeHandler;->LOGV:Z

    if-eqz v0, :cond_0

    .line 374
    const-string v0, "ArtTypeHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Enforcing item limit on items from "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 377
    :cond_0
    invoke-static {p1, v3}, Lcom/google/common/collect/Iterables;->limit(Ljava/lang/Iterable;I)Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/collect/ImmutableSet;->copyOf(Ljava/lang/Iterable;)Lcom/google/common/collect/ImmutableSet;

    move-result-object p1

    .line 379
    .end local p1    # "items":Ljava/util/Set;, "Ljava/util/Set<Lcom/google/android/music/art/ArtResolver2$ArtUrl;>;"
    :cond_1
    return-object p1
.end method

.method protected enforceCompositeItemLimit([Ljava/lang/String;)[Ljava/lang/String;
    .locals 4
    .param p1, "items"    # [Ljava/lang/String;

    .prologue
    const/4 v3, 0x5

    .line 389
    invoke-static {p1}, Lcom/google/android/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 390
    array-length v0, p1

    if-le v0, v3, :cond_1

    .line 391
    sget-boolean v0, Lcom/google/android/music/art/ArtTypeHandler;->LOGV:Z

    if-eqz v0, :cond_0

    .line 392
    const-string v0, "ArtTypeHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Enforcing item limit on items from "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    array-length v2, p1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 395
    :cond_0
    const/4 v0, 0x0

    invoke-static {p1, v0, v3}, Ljava/util/Arrays;->copyOfRange([Ljava/lang/Object;II)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 397
    :goto_0
    return-object v0

    :cond_1
    move-object v0, p1

    goto :goto_0
.end method

.method public final fillNeededItems(Lcom/google/android/music/art/ArtRequest2;)V
    .locals 1
    .param p1, "request"    # Lcom/google/android/music/art/ArtRequest2;

    .prologue
    .line 111
    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lcom/google/android/music/art/ArtRequest2;->externalChangeState(I)V

    .line 112
    invoke-virtual {p0, p1}, Lcom/google/android/music/art/ArtTypeHandler;->fillNeededItemsImpl(Lcom/google/android/music/art/ArtRequest2;)V

    .line 113
    return-void
.end method

.method protected abstract fillNeededItemsImpl(Lcom/google/android/music/art/ArtRequest2;)V
.end method

.method protected final genreLookup(Lcom/google/android/music/art/ArtRequest2;J)V
    .locals 6
    .param p1, "request"    # Lcom/google/android/music/art/ArtRequest2;
    .param p2, "genreId"    # J

    .prologue
    const/4 v3, 0x1

    .line 361
    invoke-static {p2, p3}, Lcom/google/android/music/store/MusicContent$Genres;->getAlbumsOfGenreUri(J)Landroid/net/Uri;

    move-result-object v2

    .line 362
    .local v2, "lookupUri":Landroid/net/Uri;
    sget-object v4, Lcom/google/android/music/art/ArtTypeHandler;->NAUTILUS_ART_COLUMNS:[Ljava/lang/String;

    move-object v0, p0

    move-object v1, p1

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/music/art/ArtTypeHandler;->addUrlsFromUri(Lcom/google/android/music/art/ArtRequest2;Landroid/net/Uri;Z[Ljava/lang/String;Z)V

    .line 363
    return-void
.end method

.method public abstract getPostProcessor(Lcom/google/android/music/art/ArtDescriptor;)Lcom/google/android/music/art/ArtPostProcessor;
.end method

.method protected final localRadioLookup(Lcom/google/android/music/art/ArtRequest2;J)V
    .locals 22
    .param p1, "request"    # Lcom/google/android/music/art/ArtRequest2;
    .param p2, "stationId"    # J

    .prologue
    .line 290
    invoke-static/range {p2 .. p3}, Lcom/google/android/music/store/MusicContent$RadioStations;->getRadioStationUri(J)Landroid/net/Uri;

    move-result-object v3

    .line 291
    .local v3, "uri":Landroid/net/Uri;
    const/4 v13, 0x0

    .line 292
    .local v13, "c":Landroid/database/Cursor;
    invoke-static {}, Lcom/google/android/play/utils/collections/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v20

    .line 294
    .local v20, "urls":Ljava/util/Set;, "Ljava/util/Set<Lcom/google/android/music/art/ArtResolver2$ArtUrl;>;"
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/art/ArtTypeHandler;->mAppContext:Landroid/content/Context;

    sget-object v4, Lcom/google/android/music/art/ArtTypeHandler;->RADIO_COLUMNS:[Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    .line 295
    if-eqz v13, :cond_3

    invoke-interface {v13}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 296
    const/4 v2, 0x0

    invoke-interface {v13, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v16, ""

    .line 298
    .local v16, "multiUrls":Ljava/lang/String;
    :goto_0
    const/4 v2, 0x1

    invoke-interface {v13, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v17

    .line 299
    .local v17, "seedType":I
    const/4 v2, 0x2

    invoke-interface {v13, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 300
    .local v10, "remoteSeedId":Ljava/lang/String;
    invoke-static/range {v16 .. v16}, Lcom/google/android/music/utils/MusicUtils;->decodeStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/music/art/ArtTypeHandler;->enforceCompositeItemLimit([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v19

    .line 302
    .local v19, "urlArray":[Ljava/lang/String;
    const/16 v2, 0x8

    move/from16 v0, v17

    if-ne v0, v2, :cond_1

    move-object/from16 v0, v19

    array-length v2, v0

    if-nez v2, :cond_1

    .line 304
    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/google/android/music/art/ArtTypeHandler;->lookupPlaylistRadioFallbackId(Ljava/lang/String;)J

    move-result-wide v8

    .line 305
    .local v8, "playlistId":J
    const-wide/16 v4, -0x1

    cmp-long v2, v8, v4

    if-eqz v2, :cond_3

    .line 306
    sget-object v7, Lcom/google/android/music/art/ArtTypeHandler$PlaylistArtType;->USER:Lcom/google/android/music/art/ArtTypeHandler$PlaylistArtType;

    move-object/from16 v5, p0

    move-object/from16 v6, p1

    invoke-virtual/range {v5 .. v10}, Lcom/google/android/music/art/ArtTypeHandler;->playlistLookup(Lcom/google/android/music/art/ArtRequest2;Lcom/google/android/music/art/ArtTypeHandler$PlaylistArtType;JLjava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 318
    invoke-static {v13}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 320
    .end local v8    # "playlistId":J
    .end local v10    # "remoteSeedId":Ljava/lang/String;
    .end local v16    # "multiUrls":Ljava/lang/String;
    .end local v17    # "seedType":I
    .end local v19    # "urlArray":[Ljava/lang/String;
    :goto_1
    return-void

    .line 296
    :cond_0
    const/4 v2, 0x0

    :try_start_1
    invoke-interface {v13, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    goto :goto_0

    .line 310
    .restart local v10    # "remoteSeedId":Ljava/lang/String;
    .restart local v16    # "multiUrls":Ljava/lang/String;
    .restart local v17    # "seedType":I
    .restart local v19    # "urlArray":[Ljava/lang/String;
    :cond_1
    move-object/from16 v11, v19

    .local v11, "arr$":[Ljava/lang/String;
    array-length v15, v11

    .local v15, "len$":I
    const/4 v14, 0x0

    .local v14, "i$":I
    :goto_2
    if-ge v14, v15, :cond_2

    aget-object v18, v11, v14

    .line 311
    .local v18, "url":Ljava/lang/String;
    new-instance v12, Lcom/google/android/music/art/ArtResolver2$ArtUrl;

    const/4 v2, 0x1

    move-object/from16 v0, v18

    invoke-direct {v12, v0, v2}, Lcom/google/android/music/art/ArtResolver2$ArtUrl;-><init>(Ljava/lang/String;Z)V

    .line 312
    .local v12, "artUrl":Lcom/google/android/music/art/ArtResolver2$ArtUrl;
    move-object/from16 v0, v20

    invoke-interface {v0, v12}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 310
    add-int/lit8 v14, v14, 0x1

    goto :goto_2

    .line 314
    .end local v12    # "artUrl":Lcom/google/android/music/art/ArtResolver2$ArtUrl;
    .end local v18    # "url":Ljava/lang/String;
    :cond_2
    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/google/android/music/art/ArtRequest2;->addNeededItems(Ljava/util/Collection;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 318
    .end local v10    # "remoteSeedId":Ljava/lang/String;
    .end local v11    # "arr$":[Ljava/lang/String;
    .end local v14    # "i$":I
    .end local v15    # "len$":I
    .end local v16    # "multiUrls":Ljava/lang/String;
    .end local v17    # "seedType":I
    .end local v19    # "urlArray":[Ljava/lang/String;
    :cond_3
    invoke-static {v13}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto :goto_1

    :catchall_0
    move-exception v2

    invoke-static {v13}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v2
.end method

.method protected final playlistLookup(Lcom/google/android/music/art/ArtRequest2;Lcom/google/android/music/art/ArtTypeHandler$PlaylistArtType;JLjava/lang/String;)V
    .locals 7
    .param p1, "request"    # Lcom/google/android/music/art/ArtRequest2;
    .param p2, "artType"    # Lcom/google/android/music/art/ArtTypeHandler$PlaylistArtType;
    .param p3, "playlistId"    # J
    .param p5, "extId"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    .line 261
    sget-boolean v0, Lcom/google/android/music/art/ArtTypeHandler;->LOGV:Z

    if-eqz v0, :cond_0

    .line 262
    const-string v0, "ArtTypeHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "playlistLookup: playlistId="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ", "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "extId=\""

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "\" artType="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 266
    :cond_0
    sget-object v0, Lcom/google/android/music/art/ArtTypeHandler$1;->$SwitchMap$com$google$android$music$art$ArtTypeHandler$PlaylistArtType:[I

    invoke-virtual {p2}, Lcom/google/android/music/art/ArtTypeHandler$PlaylistArtType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 275
    invoke-static {p3, p4}, Lcom/google/android/music/store/MusicContent$Playlists$Members;->getPlaylistItemsUri(J)Landroid/net/Uri;

    move-result-object v2

    .line 278
    .local v2, "itemsUri":Landroid/net/Uri;
    :goto_0
    sget-object v4, Lcom/google/android/music/art/ArtTypeHandler;->XAUDIO_ALBUM_ART_COLUMNS:[Ljava/lang/String;

    move-object v0, p0

    move-object v1, p1

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/music/art/ArtTypeHandler;->addUrlsFromUri(Lcom/google/android/music/art/ArtRequest2;Landroid/net/Uri;Z[Ljava/lang/String;Z)V

    .line 280
    return-void

    .line 268
    .end local v2    # "itemsUri":Landroid/net/Uri;
    :pswitch_0
    invoke-static {p5}, Lcom/google/android/music/store/MusicContent$SharedWithMePlaylist$Members;->getUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 269
    .restart local v2    # "itemsUri":Landroid/net/Uri;
    goto :goto_0

    .line 271
    .end local v2    # "itemsUri":Landroid/net/Uri;
    :pswitch_1
    const/4 v0, 0x0

    invoke-static {p3, p4, v0}, Lcom/google/android/music/store/MusicContent$AutoPlaylists$Members;->getAutoPlaylistItemsUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 273
    .restart local v2    # "itemsUri":Landroid/net/Uri;
    goto :goto_0

    .line 266
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
