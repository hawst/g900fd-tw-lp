.class public Lcom/google/android/music/ui/MyLibraryFragment;
.super Lcom/google/android/music/ui/SubFragmentsPagerFragment;
.source "MyLibraryFragment.java"


# direct methods
.method public constructor <init>()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 11
    invoke-direct {p0}, Lcom/google/android/music/ui/SubFragmentsPagerFragment;-><init>()V

    .line 12
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 13
    .local v0, "tabs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/music/ui/FragmentTabInfo;>;"
    new-instance v1, Lcom/google/android/music/ui/FragmentTabInfo;

    const-string v2, "genres"

    const v3, 0x7f0b00a0

    const-class v4, Lcom/google/android/music/ui/GenreGridFragment;

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/music/ui/FragmentTabInfo;-><init>(Ljava/lang/String;ILjava/lang/Class;Z)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 15
    new-instance v1, Lcom/google/android/music/ui/FragmentTabInfo;

    const-string v2, "artists"

    const v3, 0x7f0b009e

    const-class v4, Lcom/google/android/music/ui/ArtistGridFragment;

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/music/ui/FragmentTabInfo;-><init>(Ljava/lang/String;ILjava/lang/Class;Z)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 17
    new-instance v1, Lcom/google/android/music/ui/FragmentTabInfo;

    const-string v2, "albums"

    const v3, 0x7f0b009f

    const-class v4, Lcom/google/android/music/ui/AllAlbumsFragment;

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/music/ui/FragmentTabInfo;-><init>(Ljava/lang/String;ILjava/lang/Class;Z)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 19
    new-instance v1, Lcom/google/android/music/ui/FragmentTabInfo;

    const-string v2, "songs"

    const v3, 0x7f0b00a1

    const-class v4, Lcom/google/android/music/ui/AllTracksFragment;

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/music/ui/FragmentTabInfo;-><init>(Ljava/lang/String;ILjava/lang/Class;Z)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 21
    const-string v1, "artists"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/music/ui/MyLibraryFragment;->init(Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 22
    return-void
.end method
