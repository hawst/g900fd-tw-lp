.class Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$GoToArtist;
.super Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$UIThreadMenuEntry;
.source "DocumentMenuHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GoToArtist"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;Landroid/content/res/Resources;)V
    .locals 2
    .param p2, "res"    # Landroid/content/res/Resources;

    .prologue
    .line 1149
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$GoToArtist;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    .line 1150
    const v0, 0x7f0e0029

    const v1, 0x7f0b0244

    invoke-direct {p0, p2, v0, v1}, Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$UIThreadMenuEntry;-><init>(Landroid/content/res/Resources;II)V

    .line 1151
    return-void
.end method


# virtual methods
.method public onActionSelected()V
    .locals 5

    .prologue
    .line 1155
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$GoToArtist;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v0

    sget-object v1, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ALBUM:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$GoToArtist;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v0

    sget-object v1, Lcom/google/android/music/ui/cardlib/model/Document$Type;->TRACK:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$GoToArtist;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v0

    sget-object v1, Lcom/google/android/music/ui/cardlib/model/Document$Type;->RADIO:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-ne v0, v1, :cond_2

    .line 1158
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$GoToArtist;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/model/Document;->isNautilus()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1159
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$GoToArtist;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mContext:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$GoToArtist;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v1, v1, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v1}, Lcom/google/android/music/ui/cardlib/model/Document;->getArtistMetajamId()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$GoToArtist;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v2, v2, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v2}, Lcom/google/android/music/ui/cardlib/model/Document;->getArtistName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/music/ui/ArtistPageActivity;->showNautilusArtist(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 1168
    :goto_0
    return-void

    .line 1162
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$GoToArtist;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mContext:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$GoToArtist;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v1, v1, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v1}, Lcom/google/android/music/ui/cardlib/model/Document;->getArtistId()J

    move-result-wide v2

    iget-object v1, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$GoToArtist;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v1, v1, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v1}, Lcom/google/android/music/ui/cardlib/model/Document;->getArtistName()Ljava/lang/String;

    move-result-object v1

    const/4 v4, 0x1

    invoke-static {v0, v2, v3, v1, v4}, Lcom/google/android/music/ui/ArtistPageActivity;->showArtist(Landroid/content/Context;JLjava/lang/String;Z)V

    goto :goto_0

    .line 1166
    :cond_2
    const-string v0, "DocumentMenuHandler"

    const-string v1, "Unexpected doc type"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
