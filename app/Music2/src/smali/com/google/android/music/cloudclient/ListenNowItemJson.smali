.class public Lcom/google/android/music/cloudclient/ListenNowItemJson;
.super Lcom/google/api/client/json/GenericJson;
.source "ListenNowItemJson.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/cloudclient/ListenNowItemJson$ListenNowItemIdJson;
    }
.end annotation


# instance fields
.field public imageRefs:Ljava/util/List;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "images"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/cloudclient/ImageRefJson;",
            ">;"
        }
    .end annotation
.end field

.field public mAlbum:Lcom/google/android/music/cloudclient/ListenNowAlbumJson;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "album"
    .end annotation
.end field

.field public mPlaylist:Lcom/google/android/music/cloudclient/ListenNowPlaylistJson;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "playlist"
    .end annotation
.end field

.field public mRadioStation:Lcom/google/android/music/cloudclient/ListenNowRadioStationJson;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "radio_station"
    .end annotation
.end field

.field public mSuggestionReason:I
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "suggestion_reason"
    .end annotation
.end field

.field public mSuggestionText:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "suggestion_text"
    .end annotation
.end field

.field public mType:I
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "type"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    .line 41
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/cloudclient/ListenNowItemJson;->imageRefs:Ljava/util/List;

    .line 44
    return-void
.end method
