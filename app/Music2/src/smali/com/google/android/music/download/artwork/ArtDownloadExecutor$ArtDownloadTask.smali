.class Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;
.super Ljava/lang/Object;
.source "ArtDownloadExecutor.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/download/artwork/ArtDownloadExecutor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ArtDownloadTask"
.end annotation


# static fields
.field private static final RELATIVE_CACHE_DIRECTORY:Ljava/io/File;

.field private static SUB_FOLDER:Ljava/lang/String;


# instance fields
.field private final LOGV:Z

.field private mArtworkExists:Z

.field private final mCacheDir:Ljava/io/File;

.field private final mContext:Landroid/content/Context;

.field private mDownloadSuccessful:Z

.field private final mEventLogger:Lcom/google/android/music/eventlog/MusicEventLogger;

.field private final mIsPrefetch:Z

.field private mLocalLocationFullPath:Ljava/io/File;

.field private mLocalLocationRelativePath:Ljava/lang/String;

.field private final mRemoteUrl:Ljava/lang/String;

.field private final mStorageType:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 262
    invoke-static {}, Lcom/google/android/music/download/artwork/ArtDownloadUtils;->getArtSubFolder()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;->SUB_FOLDER:Ljava/lang/String;

    .line 273
    new-instance v0, Ljava/io/File;

    # getter for: Lcom/google/android/music/download/artwork/ArtDownloadExecutor;->ARTWORK_CACHE_DIRECTORY:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/music/download/artwork/ArtDownloadExecutor;->access$200()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;->SUB_FOLDER:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;->RELATIVE_CACHE_DIRECTORY:Ljava/io/File;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/io/File;ILjava/lang/String;Z)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cacheDir"    # Ljava/io/File;
    .param p3, "storageType"    # I
    .param p4, "remoteUrl"    # Ljava/lang/String;
    .param p5, "isPrefetch"    # Z

    .prologue
    .line 291
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 264
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->DOWNLOAD:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;->LOGV:Z

    .line 280
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;->mDownloadSuccessful:Z

    .line 286
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;->mArtworkExists:Z

    .line 292
    iput-object p1, p0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;->mContext:Landroid/content/Context;

    .line 293
    iput-object p2, p0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;->mCacheDir:Ljava/io/File;

    .line 294
    iput p3, p0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;->mStorageType:I

    .line 295
    iget-object v0, p0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/music/eventlog/MusicEventLogger;->getInstance(Landroid/content/Context;)Lcom/google/android/music/eventlog/MusicEventLogger;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;->mEventLogger:Lcom/google/android/music/eventlog/MusicEventLogger;

    .line 296
    iput-object p4, p0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;->mRemoteUrl:Ljava/lang/String;

    .line 297
    iput-boolean p5, p0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;->mIsPrefetch:Z

    .line 298
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;)Ljava/io/File;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;

    .prologue
    .line 253
    iget-object v0, p0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;->mLocalLocationFullPath:Ljava/io/File;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;

    .prologue
    .line 253
    iget-boolean v0, p0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;->mArtworkExists:Z

    return v0
.end method

.method private getLocalLocationRelativePath()Ljava/lang/String;
    .locals 3

    .prologue
    .line 326
    iget-object v1, p0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;->mRemoteUrl:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/music/store/Store;->getFileNameFromRemoteArtUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 328
    .local v0, "fileName":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    sget-object v2, Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;->RELATIVE_CACHE_DIRECTORY:Ljava/io/File;

    invoke-direct {v1, v2, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private getRemoteLocation()Ljava/lang/String;
    .locals 1

    .prologue
    .line 322
    iget-object v0, p0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;->mRemoteUrl:Ljava/lang/String;

    return-object v0
.end method

.method private logHttpRequest(Ljava/lang/String;)V
    .locals 1
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 421
    iget-object v0, p0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;->mEventLogger:Lcom/google/android/music/eventlog/MusicEventLogger;

    invoke-virtual {v0, p1}, Lcom/google/android/music/eventlog/MusicEventLogger;->logArtDownload(Ljava/lang/String;)V

    .line 422
    return-void
.end method

.method private logMissingRemoteLocation()V
    .locals 2

    .prologue
    .line 332
    const-string v0, "ArtDownloadTask"

    const-string v1, "Empty remote url."

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 333
    return-void
.end method

.method private streamFromCloud(Ljava/lang/String;)V
    .locals 14
    .param p1, "remoteLocation"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 373
    iget-object v9, p0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;->mLocalLocationFullPath:Ljava/io/File;

    invoke-virtual {v9}, Ljava/io/File;->length()J

    move-result-wide v10

    const-wide/16 v12, 0x0

    cmp-long v9, v10, v12

    if-lez v9, :cond_0

    .line 374
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;->mDownloadSuccessful:Z

    .line 414
    :goto_0
    return-void

    .line 379
    :cond_0
    invoke-static {p1}, Lcom/google/android/music/utils/IOUtils;->rewriteArtUri(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 380
    .local v0, "albumArtUrl":Ljava/lang/String;
    iget-boolean v9, p0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;->LOGV:Z

    if-eqz v9, :cond_1

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_1

    .line 381
    const-string v9, "ArtDownloadTask"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Converted "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " to "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 383
    :cond_1
    iget-object v9, p0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;->mContext:Landroid/content/Context;

    invoke-static {v9}, Lcom/google/android/music/cloudclient/MusicRequest;->getSharedHttpClient(Landroid/content/Context;)Lcom/google/android/music/cloudclient/MusicHttpClient;

    move-result-object v3

    .line 384
    .local v3, "httpClient":Lcom/google/android/music/cloudclient/MusicHttpClient;
    new-instance v4, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v4, v0}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 385
    .local v4, "httpMethod":Lorg/apache/http/client/methods/HttpGet;
    const/4 v8, 0x0

    .line 389
    .local v8, "response":Lorg/apache/http/HttpResponse;
    iget-object v9, p0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;->mLocalLocationFullPath:Ljava/io/File;

    invoke-virtual {v9}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v9

    invoke-virtual {v9}, Ljava/io/File;->mkdirs()Z

    .line 390
    new-instance v6, Ljava/io/FileOutputStream;

    iget-object v9, p0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;->mLocalLocationFullPath:Ljava/io/File;

    invoke-direct {v6, v9}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 391
    .local v6, "out":Ljava/io/FileOutputStream;
    const/4 v5, 0x0

    .line 393
    .local v5, "is":Ljava/io/InputStream;
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;->logHttpRequest(Ljava/lang/String;)V

    .line 394
    invoke-virtual {v3, v4}, Lcom/google/android/music/cloudclient/MusicHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v8

    .line 395
    invoke-interface {v8}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v9

    invoke-interface {v9}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v5

    .line 397
    const/16 v9, 0x2000

    new-array v1, v9, [B

    .line 398
    .local v1, "buff":[B
    const/4 v7, 0x0

    .line 399
    .local v7, "read":I
    :goto_1
    invoke-virtual {v5, v1}, Ljava/io/InputStream;->read([B)I

    move-result v7

    const/4 v9, -0x1

    if-eq v7, v9, :cond_2

    .line 400
    const/4 v9, 0x0

    invoke-virtual {v6, v1, v9, v7}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 404
    .end local v1    # "buff":[B
    .end local v7    # "read":I
    :catchall_0
    move-exception v9

    invoke-static {v5}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    .line 406
    :try_start_1
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->flush()V

    .line 407
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v10

    invoke-virtual {v10}, Ljava/io/FileDescriptor;->sync()V

    .line 408
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 412
    :goto_2
    invoke-static {v4, v8}, Lcom/google/android/music/cloudclient/MusicRequest;->releaseResponse(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/HttpResponse;)V

    throw v9

    .line 402
    .restart local v1    # "buff":[B
    .restart local v7    # "read":I
    :cond_2
    const/4 v9, 0x1

    :try_start_2
    iput-boolean v9, p0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;->mDownloadSuccessful:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 404
    invoke-static {v5}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    .line 406
    :try_start_3
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->flush()V

    .line 407
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v9

    invoke-virtual {v9}, Ljava/io/FileDescriptor;->sync()V

    .line 408
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    .line 412
    :goto_3
    invoke-static {v4, v8}, Lcom/google/android/music/cloudclient/MusicRequest;->releaseResponse(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/HttpResponse;)V

    goto/16 :goto_0

    .line 409
    :catch_0
    move-exception v2

    .line 410
    .local v2, "e":Ljava/io/IOException;
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;->mDownloadSuccessful:Z

    goto :goto_3

    .line 409
    .end local v1    # "buff":[B
    .end local v2    # "e":Ljava/io/IOException;
    .end local v7    # "read":I
    :catch_1
    move-exception v2

    .line 410
    .restart local v2    # "e":Ljava/io/IOException;
    const/4 v10, 0x0

    iput-boolean v10, p0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;->mDownloadSuccessful:Z

    goto :goto_2
.end method


# virtual methods
.method public afterExecute(Lcom/google/android/music/download/artwork/ArtDownloadExecutor$DownloadListener;)V
    .locals 6
    .param p1, "listener"    # Lcom/google/android/music/download/artwork/ArtDownloadExecutor$DownloadListener;

    .prologue
    .line 336
    invoke-virtual {p0}, Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;->wasDownloadSuccessful()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 337
    iget-object v1, p0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v0

    .line 338
    .local v0, "store":Lcom/google/android/music/store/Store;
    iget-object v1, p0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;->mLocalLocationFullPath:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v4

    .line 339
    .local v4, "fileLength":J
    iget-object v1, p0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;->mRemoteUrl:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;->mLocalLocationRelativePath:Ljava/lang/String;

    iget v3, p0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;->mStorageType:I

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/music/store/Store;->saveArtwork(Ljava/lang/String;Ljava/lang/String;IJ)V

    .line 345
    .end local v0    # "store":Lcom/google/android/music/store/Store;
    .end local v4    # "fileLength":J
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;->mRemoteUrl:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;->wasDownloadSuccessful()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;->mLocalLocationFullPath:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_1
    invoke-interface {p1, v2, v1}, Lcom/google/android/music/download/artwork/ArtDownloadExecutor$DownloadListener;->onDownloadArtworkFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 349
    return-void

    .line 340
    :cond_1
    iget-object v1, p0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;->mLocalLocationFullPath:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 342
    iget-object v1, p0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;->mLocalLocationFullPath:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    goto :goto_0

    .line 345
    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public getLocalLocationAbsolutePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 314
    iget-object v0, p0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;->mLocalLocationFullPath:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTaskId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 318
    iget-object v0, p0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;->mRemoteUrl:Ljava/lang/String;

    return-object v0
.end method

.method public initLocalLocation()Ljava/lang/String;
    .locals 3

    .prologue
    .line 305
    invoke-direct {p0}, Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;->getLocalLocationRelativePath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;->mLocalLocationRelativePath:Ljava/lang/String;

    .line 306
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;->mCacheDir:Ljava/io/File;

    iget-object v2, p0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;->mLocalLocationRelativePath:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;->mLocalLocationFullPath:Ljava/io/File;

    .line 307
    invoke-virtual {p0}, Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;->getLocalLocationAbsolutePath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public run()V
    .locals 4

    .prologue
    .line 353
    invoke-direct {p0}, Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;->getRemoteLocation()Ljava/lang/String;

    move-result-object v1

    .line 354
    .local v1, "remoteLocation":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 355
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;->mArtworkExists:Z

    .line 358
    invoke-direct {p0}, Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;->logMissingRemoteLocation()V

    .line 368
    :goto_0
    return-void

    .line 364
    :cond_0
    :try_start_0
    invoke-direct {p0, v1}, Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;->streamFromCloud(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 365
    :catch_0
    move-exception v0

    .line 366
    .local v0, "e":Ljava/io/IOException;
    const-string v2, "ArtDownloadTask"

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public wasDownloadSuccessful()Z
    .locals 1

    .prologue
    .line 417
    iget-boolean v0, p0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor$ArtDownloadTask;->mDownloadSuccessful:Z

    return v0
.end method
