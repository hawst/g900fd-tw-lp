.class Lcom/google/android/music/ui/MaterialContainerHeaderBg$2;
.super Ljava/lang/Object;
.source "MaterialContainerHeaderBg.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/MaterialContainerHeaderBg;->setupArtistArtSlideShow(Landroid/database/Cursor;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/MaterialContainerHeaderBg;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/MaterialContainerHeaderBg;)V
    .locals 0

    .prologue
    .line 334
    iput-object p1, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg$2;->this$0:Lcom/google/android/music/ui/MaterialContainerHeaderBg;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 337
    iget-object v2, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg$2;->this$0:Lcom/google/android/music/ui/MaterialContainerHeaderBg;

    # getter for: Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mArtistArtUrls:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->access$500(Lcom/google/android/music/ui/MaterialContainerHeaderBg;)Ljava/util/ArrayList;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg$2;->this$0:Lcom/google/android/music/ui/MaterialContainerHeaderBg;

    # getter for: Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mArtistArtUrls:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->access$500(Lcom/google/android/music/ui/MaterialContainerHeaderBg;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 339
    :cond_0
    const-string v2, "MaterialHeaderBg"

    const-string v3, "mSlideRefreshRunnable: the artist url list is empty"

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 358
    :goto_0
    return-void

    .line 343
    :cond_1
    # getter for: Lcom/google/android/music/ui/MaterialContainerHeaderBg;->LOGV:Z
    invoke-static {}, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->access$600()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 344
    const-string v2, "MaterialHeaderBg"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Slide refresh. Showing first slide="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg$2;->this$0:Lcom/google/android/music/ui/MaterialContainerHeaderBg;

    # getter for: Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mShowingFirstSlide:Z
    invoke-static {v4}, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->access$400(Lcom/google/android/music/ui/MaterialContainerHeaderBg;)Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 349
    :cond_2
    iget-object v2, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg$2;->this$0:Lcom/google/android/music/ui/MaterialContainerHeaderBg;

    # getter for: Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mShowingFirstSlide:Z
    invoke-static {v2}, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->access$400(Lcom/google/android/music/ui/MaterialContainerHeaderBg;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg$2;->this$0:Lcom/google/android/music/ui/MaterialContainerHeaderBg;

    # getter for: Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mArtistArtHolderBottom:Lcom/google/android/music/ui/MaterialContainerHeaderBg$ArtistArtHolder;
    invoke-static {v2}, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->access$700(Lcom/google/android/music/ui/MaterialContainerHeaderBg;)Lcom/google/android/music/ui/MaterialContainerHeaderBg$ArtistArtHolder;

    move-result-object v0

    .line 351
    .local v0, "nextArtist":Lcom/google/android/music/ui/MaterialContainerHeaderBg$ArtistArtHolder;
    :goto_1
    invoke-virtual {v0}, Lcom/google/android/music/ui/MaterialContainerHeaderBg$ArtistArtHolder;->clearArtistArt()V

    .line 352
    iget-object v2, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg$2;->this$0:Lcom/google/android/music/ui/MaterialContainerHeaderBg;

    # invokes: Lcom/google/android/music/ui/MaterialContainerHeaderBg;->getNextArtistUrl()Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->access$900(Lcom/google/android/music/ui/MaterialContainerHeaderBg;)Ljava/lang/String;

    move-result-object v1

    .line 357
    .local v1, "nextUrl":Ljava/lang/String;
    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/MaterialContainerHeaderBg$ArtistArtHolder;->requestBitmap(Ljava/lang/String;)V

    goto :goto_0

    .line 349
    .end local v0    # "nextArtist":Lcom/google/android/music/ui/MaterialContainerHeaderBg$ArtistArtHolder;
    .end local v1    # "nextUrl":Ljava/lang/String;
    :cond_3
    iget-object v2, p0, Lcom/google/android/music/ui/MaterialContainerHeaderBg$2;->this$0:Lcom/google/android/music/ui/MaterialContainerHeaderBg;

    # getter for: Lcom/google/android/music/ui/MaterialContainerHeaderBg;->mArtistArtHolderTop:Lcom/google/android/music/ui/MaterialContainerHeaderBg$ArtistArtHolder;
    invoke-static {v2}, Lcom/google/android/music/ui/MaterialContainerHeaderBg;->access$800(Lcom/google/android/music/ui/MaterialContainerHeaderBg;)Lcom/google/android/music/ui/MaterialContainerHeaderBg$ArtistArtHolder;

    move-result-object v0

    goto :goto_1
.end method
