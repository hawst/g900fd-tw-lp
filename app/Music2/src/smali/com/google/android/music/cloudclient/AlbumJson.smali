.class public Lcom/google/android/music/cloudclient/AlbumJson;
.super Lcom/google/api/client/json/GenericJson;
.source "AlbumJson.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/cloudclient/AlbumJson$DescriptionAttribution;
    }
.end annotation


# instance fields
.field public mAlbumArtRef:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "albumArtRef"
    .end annotation
.end field

.field public mAlbumArtist:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "albumArtist"
    .end annotation
.end field

.field public mAlbumId:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "albumId"
    .end annotation
.end field

.field public mArtist:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "artist"
    .end annotation
.end field

.field public mArtistId:Ljava/util/List;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "artistId"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public mCreationTimestamp:J
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "creationTimestamp"
    .end annotation
.end field

.field public mDescription:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "description"
    .end annotation
.end field

.field public mDescriptionAttribution:Lcom/google/android/music/cloudclient/AlbumJson$DescriptionAttribution;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "description_attribution"
    .end annotation
.end field

.field public mIsCompilation:Z
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "isCompilation"
    .end annotation
.end field

.field public mLastTimePlayed:J
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "lastTimePlayed"
    .end annotation
.end field

.field public mName:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "name"
    .end annotation
.end field

.field public mTrackCount:I
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "trackCount"
    .end annotation
.end field

.field public mTracks:Ljava/util/List;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "tracks"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/sync/google/model/Track;",
            ">;"
        }
    .end annotation
.end field

.field public mYear:I
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "year"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    .line 45
    return-void
.end method
