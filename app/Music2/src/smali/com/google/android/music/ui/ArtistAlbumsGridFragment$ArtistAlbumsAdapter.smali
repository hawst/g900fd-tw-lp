.class final Lcom/google/android/music/ui/ArtistAlbumsGridFragment$ArtistAlbumsAdapter;
.super Lcom/google/android/music/ui/AlbumsAdapter;
.source "ArtistAlbumsGridFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/ArtistAlbumsGridFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "ArtistAlbumsAdapter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/ArtistAlbumsGridFragment;


# direct methods
.method protected constructor <init>(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;Lcom/google/android/music/ui/AlbumGridFragment;Landroid/database/Cursor;Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;)V
    .locals 0
    .param p2, "fragment"    # Lcom/google/android/music/ui/AlbumGridFragment;
    .param p3, "cursor"    # Landroid/database/Cursor;
    .param p4, "cardsContextMenuDelegate"    # Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;

    .prologue
    .line 511
    iput-object p1, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$ArtistAlbumsAdapter;->this$0:Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    .line 512
    invoke-direct {p0, p2, p3, p4}, Lcom/google/android/music/ui/AlbumsAdapter;-><init>(Lcom/google/android/music/ui/AlbumGridFragment;Landroid/database/Cursor;Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;)V

    .line 513
    invoke-virtual {p0}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$ArtistAlbumsAdapter;->init()V

    .line 514
    return-void
.end method

.method protected constructor <init>(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;Lcom/google/android/music/ui/AlbumGridFragment;Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;)V
    .locals 0
    .param p2, "fragment"    # Lcom/google/android/music/ui/AlbumGridFragment;
    .param p3, "cardsContextMenuDelegate"    # Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;

    .prologue
    .line 505
    iput-object p1, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$ArtistAlbumsAdapter;->this$0:Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    .line 506
    invoke-direct {p0, p2, p3}, Lcom/google/android/music/ui/AlbumsAdapter;-><init>(Lcom/google/android/music/ui/MusicFragment;Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;)V

    .line 507
    invoke-virtual {p0}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$ArtistAlbumsAdapter;->init()V

    .line 508
    return-void
.end method


# virtual methods
.method public getFakeView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 526
    invoke-static {p2}, Lcom/google/android/music/utils/MusicUtils;->getDocument(Landroid/view/View;)Lcom/google/android/music/ui/cardlib/model/Document;

    move-result-object v0

    .line 527
    .local v0, "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    sget-object v2, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ALL_SONGS_ARTIST:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    invoke-virtual {v0, v2}, Lcom/google/android/music/ui/cardlib/model/Document;->setType(Lcom/google/android/music/ui/cardlib/model/Document$Type;)V

    .line 528
    iget-object v2, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$ArtistAlbumsAdapter;->this$0:Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    # getter for: Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mArtistId:J
    invoke-static {v2}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->access$400(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/music/ui/cardlib/model/Document;->setId(J)V

    .line 529
    iget-object v2, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$ArtistAlbumsAdapter;->this$0:Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    # getter for: Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mArtistId:J
    invoke-static {v2}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->access$400(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/music/ui/cardlib/model/Document;->setArtistId(J)V

    .line 530
    iget-object v2, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$ArtistAlbumsAdapter;->this$0:Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    # getter for: Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mArtistName:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->access$200(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/music/ui/cardlib/model/Document;->setArtistName(Ljava/lang/String;)V

    .line 531
    iget-object v2, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$ArtistAlbumsAdapter;->this$0:Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    # getter for: Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->mArtistName:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->access$200(Lcom/google/android/music/ui/ArtistAlbumsGridFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/music/ui/cardlib/model/Document;->setTitle(Ljava/lang/String;)V

    .line 532
    invoke-virtual {p0}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$ArtistAlbumsAdapter;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0b0055

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/music/ui/cardlib/model/Document;->setSubTitle(Ljava/lang/String;)V

    move-object v1, p2

    .line 534
    check-cast v1, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    .line 535
    .local v1, "pcv":Lcom/google/android/music/ui/cardlib/layout/PlayCardView;
    iget-object v2, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$ArtistAlbumsAdapter;->mCardsContextMenuDelegate:Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;

    invoke-virtual {v1, v0, v2}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->bind(Lcom/google/android/music/ui/cardlib/model/Document;Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;)V

    .line 536
    invoke-virtual {v1, v0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->setTag(Ljava/lang/Object;)V

    .line 537
    invoke-virtual {p2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 538
    return-object v1
.end method

.method protected init()V
    .locals 1

    .prologue
    .line 518
    invoke-super {p0}, Lcom/google/android/music/ui/AlbumsAdapter;->init()V

    .line 519
    iget-object v0, p0, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$ArtistAlbumsAdapter;->this$0:Lcom/google/android/music/ui/ArtistAlbumsGridFragment;

    invoke-virtual {v0}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->isTabletMusicExperience()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 520
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/ArtistAlbumsGridFragment$ArtistAlbumsAdapter;->setDisableFake(Z)V

    .line 522
    :cond_0
    return-void
.end method
