.class public Lcom/google/android/music/GPlusShareActivity;
.super Landroid/app/Activity;
.source "GPlusShareActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/GPlusShareActivity$AsyncShareWorker;
    }
.end annotation


# static fields
.field private static final LOGV:Z


# instance fields
.field private mCanceled:Z

.field private mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

.field private mShareWorker:Lcom/google/android/music/GPlusShareActivity$AsyncShareWorker;

.field private mStoreId:Ljava/lang/String;

.field private mStreamingAccount:Landroid/accounts/Account;

.field private mTextView:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->UI:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/GPlusShareActivity;->LOGV:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 69
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/GPlusShareActivity;->mCanceled:Z

    .line 159
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/GPlusShareActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/GPlusShareActivity;

    .prologue
    .line 39
    iget-boolean v0, p0, Lcom/google/android/music/GPlusShareActivity;->mCanceled:Z

    return v0
.end method

.method static synthetic access$002(Lcom/google/android/music/GPlusShareActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/GPlusShareActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 39
    iput-boolean p1, p0, Lcom/google/android/music/GPlusShareActivity;->mCanceled:Z

    return p1
.end method

.method static synthetic access$100(Lcom/google/android/music/GPlusShareActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/GPlusShareActivity;

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/google/android/music/GPlusShareActivity;->transitionToError()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/music/GPlusShareActivity;Lcom/google/android/music/SharePreviewResponse;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/GPlusShareActivity;
    .param p1, "x1"    # Lcom/google/android/music/SharePreviewResponse;

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/google/android/music/GPlusShareActivity;->launchShare(Lcom/google/android/music/SharePreviewResponse;)V

    return-void
.end method

.method static synthetic access$300()Z
    .locals 1

    .prologue
    .line 39
    sget-boolean v0, Lcom/google/android/music/GPlusShareActivity;->LOGV:Z

    return v0
.end method

.method static synthetic access$400(Lcom/google/android/music/GPlusShareActivity;)Lcom/google/android/music/preferences/MusicPreferences;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/GPlusShareActivity;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/music/GPlusShareActivity;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/music/GPlusShareActivity;)Landroid/accounts/Account;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/GPlusShareActivity;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/music/GPlusShareActivity;->mStreamingAccount:Landroid/accounts/Account;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/music/GPlusShareActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/GPlusShareActivity;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/music/GPlusShareActivity;->mStoreId:Ljava/lang/String;

    return-object v0
.end method

.method public static isSharingSupported(Landroid/content/Context;)Z
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    .line 242
    new-instance v0, Landroid/content/Intent;

    const-string v4, "com.google.android.apps.plus.SHARE_GOOGLE"

    invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 243
    .local v0, "fakeIntent":Landroid/content/Intent;
    const-string v4, "https://music.google.com/music/playpreview"

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 244
    const-string v4, "authAccount"

    const-string v5, "deadbeef@non-existent-email.com"

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 245
    const-string v4, "com.google.android.apps.plus.VERSION"

    const-string v5, "1.00"

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 247
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 248
    .local v1, "pm":Landroid/content/pm/PackageManager;
    invoke-virtual {v1, v0, v3}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v2

    .line 249
    .local v2, "resolutions":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    if-eqz v4, :cond_0

    const/4 v3, 0x1

    :cond_0
    return v3
.end method

.method private launchShare(Lcom/google/android/music/SharePreviewResponse;)V
    .locals 4
    .param p1, "sharePreview"    # Lcom/google/android/music/SharePreviewResponse;

    .prologue
    .line 117
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.apps.plus.SHARE_GOOGLE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 118
    .local v0, "shareIntent":Landroid/content/Intent;
    iget-object v1, p1, Lcom/google/android/music/SharePreviewResponse;->mUrl:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 119
    const-string v1, "authAccount"

    iget-object v2, p0, Lcom/google/android/music/GPlusShareActivity;->mStreamingAccount:Landroid/accounts/Account;

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 120
    const-string v1, "com.google.android.apps.plus.VERSION"

    const-string v2, "1.00"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 121
    const-string v1, "com.google.android.apps.plus.EXTERNAL_ID"

    iget-object v2, p1, Lcom/google/android/music/SharePreviewResponse;->mExternalId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 125
    const-string v1, "com.google.android.apps.plus.FOOTER"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 126
    const/high16 v1, 0x80000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 128
    sget-boolean v1, Lcom/google/android/music/GPlusShareActivity;->LOGV:Z

    if-eqz v1, :cond_0

    .line 129
    const-string v1, "GPlusShareMusic"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Launching intent: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " Extras: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/Bundle;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 134
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/music/GPlusShareActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 135
    invoke-virtual {p0}, Lcom/google/android/music/GPlusShareActivity;->finish()V

    .line 136
    return-void
.end method

.method public static share(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "storeId"    # Ljava/lang/String;
    .param p2, "title"    # Ljava/lang/String;

    .prologue
    .line 253
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/music/GPlusShareActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 254
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "title"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 255
    const-string v1, "storeId"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 256
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 257
    return-void
.end method

.method private transitionToError()V
    .locals 2

    .prologue
    .line 112
    const v0, 0x7f0e00df

    invoke-virtual {p0, v0}, Lcom/google/android/music/GPlusShareActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 113
    iget-object v0, p0, Lcom/google/android/music/GPlusShareActivity;->mTextView:Landroid/widget/TextView;

    const v1, 0x7f0b01e2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 114
    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 0
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 140
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    .line 141
    invoke-virtual {p0}, Lcom/google/android/music/GPlusShareActivity;->finish()V

    .line 142
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 153
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/GPlusShareActivity;->mCanceled:Z

    .line 154
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    .line 155
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 9
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v8, 0x1

    .line 73
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 75
    invoke-static {p0, p0}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/music/GPlusShareActivity;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    .line 77
    iget-object v3, p0, Lcom/google/android/music/GPlusShareActivity;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    invoke-virtual {v3}, Lcom/google/android/music/preferences/MusicPreferences;->getStreamingAccount()Landroid/accounts/Account;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/music/GPlusShareActivity;->mStreamingAccount:Landroid/accounts/Account;

    .line 78
    iget-object v3, p0, Lcom/google/android/music/GPlusShareActivity;->mStreamingAccount:Landroid/accounts/Account;

    if-nez v3, :cond_0

    .line 79
    const-string v3, "GPlusShareMusic"

    const-string v4, "Asked to share a song/album, but not streaming account selected"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    invoke-virtual {p0}, Lcom/google/android/music/GPlusShareActivity;->finish()V

    .line 105
    :goto_0
    return-void

    .line 83
    :cond_0
    const v3, 0x7f040109

    invoke-virtual {p0, v3}, Lcom/google/android/music/GPlusShareActivity;->setContentView(I)V

    .line 85
    invoke-virtual {p0}, Lcom/google/android/music/GPlusShareActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 86
    .local v2, "intent":Landroid/content/Intent;
    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 87
    .local v1, "extras":Landroid/os/Bundle;
    const-string v3, "storeId"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/music/GPlusShareActivity;->mStoreId:Ljava/lang/String;

    .line 89
    const v3, 0x7f0e007d

    invoke-virtual {p0, v3}, Lcom/google/android/music/GPlusShareActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/google/android/music/GPlusShareActivity;->mTextView:Landroid/widget/TextView;

    .line 90
    iget-object v3, p0, Lcom/google/android/music/GPlusShareActivity;->mTextView:Landroid/widget/TextView;

    const v4, 0x7f0b01e0

    new-array v5, v8, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v7, "title"

    invoke-virtual {v1, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {p0, v4, v5}, Lcom/google/android/music/GPlusShareActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 92
    const v3, 0x7f0e0290

    invoke-virtual {p0, v3}, Lcom/google/android/music/GPlusShareActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 93
    .local v0, "cancelButton":Landroid/widget/Button;
    if-eqz v0, :cond_1

    .line 94
    new-instance v3, Lcom/google/android/music/GPlusShareActivity$1;

    invoke-direct {v3, p0}, Lcom/google/android/music/GPlusShareActivity$1;-><init>(Lcom/google/android/music/GPlusShareActivity;)V

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 103
    :cond_1
    new-instance v3, Lcom/google/android/music/GPlusShareActivity$AsyncShareWorker;

    invoke-direct {v3, p0}, Lcom/google/android/music/GPlusShareActivity$AsyncShareWorker;-><init>(Lcom/google/android/music/GPlusShareActivity;)V

    iput-object v3, p0, Lcom/google/android/music/GPlusShareActivity;->mShareWorker:Lcom/google/android/music/GPlusShareActivity$AsyncShareWorker;

    .line 104
    iget-object v3, p0, Lcom/google/android/music/GPlusShareActivity;->mShareWorker:Lcom/google/android/music/GPlusShareActivity$AsyncShareWorker;

    invoke-virtual {v3, v8}, Lcom/google/android/music/GPlusShareActivity$AsyncShareWorker;->sendEmptyMessage(I)Z

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 146
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 147
    invoke-static {p0}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    .line 148
    iget-object v0, p0, Lcom/google/android/music/GPlusShareActivity;->mShareWorker:Lcom/google/android/music/GPlusShareActivity$AsyncShareWorker;

    invoke-virtual {v0}, Lcom/google/android/music/GPlusShareActivity$AsyncShareWorker;->quit()V

    .line 149
    return-void
.end method
