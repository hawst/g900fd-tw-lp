.class public Lcom/google/android/music/dial/SonosDialDevice;
.super Lcom/google/android/music/dial/DialDevice;
.source "SonosDialDevice.java"


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/support/v7/media/MediaRouter$RouteInfo;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "routeInfo"    # Landroid/support/v7/media/MediaRouter$RouteInfo;

    .prologue
    .line 16
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/dial/DialDevice;-><init>(Landroid/content/Context;Landroid/support/v7/media/MediaRouter$RouteInfo;)V

    .line 17
    return-void
.end method

.method public static supportsRemoteDevice(Lcom/google/android/music/dial/RemoteDeviceInfo;)Z
    .locals 4
    .param p0, "device"    # Lcom/google/android/music/dial/RemoteDeviceInfo;

    .prologue
    const/4 v3, 0x1

    .line 27
    invoke-virtual {p0}, Lcom/google/android/music/dial/RemoteDeviceInfo;->getDeviceType()Ljava/lang/String;

    move-result-object v0

    .line 28
    .local v0, "deviceType":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string v2, "urn:sonos-com:service:SonosGroup:1"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 35
    :cond_0
    return v3
.end method


# virtual methods
.method public handleMessage(Ljava/lang/String;Lcom/google/android/music/dial/model/GenericDialEvent;)Z
    .locals 1
    .param p1, "routeId"    # Ljava/lang/String;
    .param p2, "message"    # Lcom/google/android/music/dial/model/GenericDialEvent;

    .prologue
    .line 41
    const/4 v0, 0x0

    return v0
.end method
