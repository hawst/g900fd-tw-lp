.class Lcom/google/android/music/AsyncAlbumArtImageView$ExternalRadioMode;
.super Lcom/google/android/music/AsyncAlbumArtImageView$MultiUrlCompositeMode;
.source "AsyncAlbumArtImageView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/AsyncAlbumArtImageView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ExternalRadioMode"
.end annotation


# instance fields
.field mRadioOverlayIsLocal:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 419
    invoke-direct {p0}, Lcom/google/android/music/AsyncAlbumArtImageView$MultiUrlCompositeMode;-><init>()V

    .line 416
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView$ExternalRadioMode;->mRadioOverlayIsLocal:Z

    .line 420
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView$ExternalRadioMode;->mDrawRadioOverlay:Z

    .line 421
    return-void
.end method

.method public constructor <init>(Lcom/google/android/music/AsyncAlbumArtImageView$ExternalRadioMode;)V
    .locals 1
    .param p1, "other"    # Lcom/google/android/music/AsyncAlbumArtImageView$ExternalRadioMode;

    .prologue
    .line 424
    invoke-direct {p0, p1}, Lcom/google/android/music/AsyncAlbumArtImageView$MultiUrlCompositeMode;-><init>(Lcom/google/android/music/AsyncAlbumArtImageView$MultiUrlCompositeMode;)V

    .line 416
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView$ExternalRadioMode;->mRadioOverlayIsLocal:Z

    .line 426
    iget-boolean v0, p1, Lcom/google/android/music/AsyncAlbumArtImageView$ExternalRadioMode;->mDrawRadioOverlay:Z

    iput-boolean v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView$ExternalRadioMode;->mDrawRadioOverlay:Z

    .line 427
    return-void
.end method


# virtual methods
.method public copy()Lcom/google/android/music/AsyncAlbumArtImageView$Mode;
    .locals 1

    .prologue
    .line 507
    new-instance v0, Lcom/google/android/music/AsyncAlbumArtImageView$ExternalRadioMode;

    invoke-direct {v0, p0}, Lcom/google/android/music/AsyncAlbumArtImageView$ExternalRadioMode;-><init>(Lcom/google/android/music/AsyncAlbumArtImageView$ExternalRadioMode;)V

    return-object v0
.end method

.method public createBitmap(Landroid/content/Context;IILcom/google/android/music/download/artwork/AlbumIdSink;Lcom/google/android/music/download/artwork/RemoteUrlSink;)Landroid/graphics/Bitmap;
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "missingAlbumSink"    # Lcom/google/android/music/download/artwork/AlbumIdSink;
    .param p5, "missingUrlSink"    # Lcom/google/android/music/download/artwork/RemoteUrlSink;

    .prologue
    .line 442
    iget-object v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView$ExternalRadioMode;->mUrls:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/music/utils/MusicUtils;->decodeStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v11

    .line 443
    .local v11, "urls":[Ljava/lang/String;
    const/4 v8, 0x0

    .line 444
    .local v8, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v11, :cond_0

    array-length v0, v11

    if-nez v0, :cond_1

    .line 446
    :cond_0
    invoke-static {}, Lcom/google/android/music/utils/AlbumArtUtils;->getPreferredConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v0

    invoke-static {p2, p3, v0}, Lcom/google/android/music/utils/AlbumArtUtils;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 448
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView$ExternalRadioMode;->mRadioOverlayIsLocal:Z

    .line 474
    :goto_0
    if-nez v8, :cond_4

    .line 475
    const/4 v0, 0x0

    .line 484
    :goto_1
    return-object v0

    .line 449
    :cond_1
    array-length v0, v11

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 451
    const/4 v0, 0x0

    aget-object v1, v11, v0

    const/4 v5, 0x1

    move-object v0, p1

    move v2, p2

    move v3, p3

    move-object/from16 v4, p5

    # invokes: Lcom/google/android/music/AsyncAlbumArtImageView;->getBitmapForRemoteUrlOrDefault(Landroid/content/Context;Ljava/lang/String;IILcom/google/android/music/download/artwork/RemoteUrlSink;Z)Landroid/graphics/Bitmap;
    invoke-static/range {v0 .. v5}, Lcom/google/android/music/AsyncAlbumArtImageView;->access$100(Landroid/content/Context;Ljava/lang/String;IILcom/google/android/music/download/artwork/RemoteUrlSink;Z)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 453
    if-eqz v8, :cond_2

    .line 454
    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    int-to-float v1, v1

    div-float v6, v0, v1

    .line 455
    .local v6, "aspect":F
    int-to-float v0, p2

    int-to-float v1, p3

    div-float v10, v0, v1

    .line 456
    .local v10, "requestedAspect":F
    sub-float v0, v10, v6

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v7

    .line 460
    .local v7, "aspectError":F
    const v0, 0x3c23d70a    # 0.01f

    cmpl-float v0, v7, v0

    if-lez v0, :cond_2

    .line 461
    invoke-static {v8, p2, p3}, Lcom/google/android/music/art/ArtRenderingUtils;->sliceBitmapSectionAndScale(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v9

    .line 463
    .local v9, "cropped":Landroid/graphics/Bitmap;
    invoke-static {v8}, Lcom/google/android/music/utils/AlbumArtUtils;->recycleBitmap(Landroid/graphics/Bitmap;)V

    .line 464
    move-object v8, v9

    .line 467
    .end local v6    # "aspect":F
    .end local v7    # "aspectError":F
    .end local v9    # "cropped":Landroid/graphics/Bitmap;
    .end local v10    # "requestedAspect":F
    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView$ExternalRadioMode;->mRadioOverlayIsLocal:Z

    goto :goto_0

    .line 470
    :cond_3
    invoke-super/range {p0 .. p5}, Lcom/google/android/music/AsyncAlbumArtImageView$MultiUrlCompositeMode;->createBitmap(Landroid/content/Context;IILcom/google/android/music/download/artwork/AlbumIdSink;Lcom/google/android/music/download/artwork/RemoteUrlSink;)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 472
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView$ExternalRadioMode;->mRadioOverlayIsLocal:Z

    goto :goto_0

    .line 478
    :cond_4
    iget-boolean v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView$ExternalRadioMode;->mDrawRadioOverlay:Z

    if-eqz v0, :cond_6

    iget-boolean v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView$ExternalRadioMode;->mRadioOverlayIsLocal:Z

    if-eqz v0, :cond_6

    .line 479
    invoke-virtual {v8}, Landroid/graphics/Bitmap;->isMutable()Z

    move-result v0

    if-nez v0, :cond_5

    .line 480
    invoke-static {}, Lcom/google/android/music/utils/AlbumArtUtils;->getPreferredConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v8, v0, v1}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 482
    :cond_5
    invoke-static {p1, v8, p2, p3}, Lcom/google/android/music/utils/AlbumArtUtils;->drawFauxRadioArtOverlay(Landroid/content/Context;Landroid/graphics/Bitmap;II)Z

    :cond_6
    move-object v0, v8

    .line 484
    goto :goto_1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    .line 495
    instance-of v0, p1, Lcom/google/android/music/AsyncAlbumArtImageView$ExternalRadioMode;

    if-nez v0, :cond_0

    .line 496
    const/4 v0, 0x0

    .line 498
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/music/AsyncAlbumArtImageView$MultiUrlCompositeMode;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public equals(Ljava/lang/String;)Z
    .locals 1
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 502
    iget-object v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView$ExternalRadioMode;->mUrls:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/google/android/music/AsyncAlbumArtImageView$ExternalRadioMode;->isEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isCacheable()Z
    .locals 1

    .prologue
    .line 436
    iget-boolean v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView$ExternalRadioMode;->mRadioOverlayIsLocal:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView$ExternalRadioMode;->mDrawRadioOverlay:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 489
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ExternalRadioMode<urls: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/AsyncAlbumArtImageView$ExternalRadioMode;->mUrls:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public turnOffRadioOverlay()V
    .locals 1

    .prologue
    .line 430
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView$ExternalRadioMode;->mDrawRadioOverlay:Z

    .line 431
    return-void
.end method
