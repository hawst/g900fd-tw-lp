.class abstract Lcom/google/android/music/leanback/bitmap/LoaderBitmapGettersGetter;
.super Ljava/lang/Object;
.source "LoaderBitmapGettersGetter.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Lcom/google/android/music/leanback/bitmap/BitmapGettersGetter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Lcom/google/android/music/leanback/bitmap/BitmapGettersGetter;"
    }
.end annotation


# static fields
.field protected static final TAG:Ljava/lang/String;


# instance fields
.field private mBitmapGetters:[Lcom/google/android/music/leanback/bitmap/BitmapGetter;

.field protected final mContext:Landroid/content/Context;

.field private mCursor:Landroid/database/Cursor;

.field protected final mLoaderId:I

.field protected final mLoaderManager:Landroid/support/v4/app/LoaderManager;

.field protected final mSeed:J

.field private final mSempahore:Ljava/util/concurrent/Semaphore;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->LEANBACK:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-virtual {v0}, Lcom/google/android/music/utils/DebugUtils$MusicTag;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/leanback/bitmap/LoaderBitmapGettersGetter;->TAG:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Landroid/support/v4/app/LoaderManager;IJ)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "loaderManager"    # Landroid/support/v4/app/LoaderManager;
    .param p3, "loaderId"    # I
    .param p4, "seed"    # J

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    new-instance v0, Ljava/util/concurrent/Semaphore;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/music/leanback/bitmap/LoaderBitmapGettersGetter;->mSempahore:Ljava/util/concurrent/Semaphore;

    .line 33
    iput-object p1, p0, Lcom/google/android/music/leanback/bitmap/LoaderBitmapGettersGetter;->mContext:Landroid/content/Context;

    .line 34
    iput-object p2, p0, Lcom/google/android/music/leanback/bitmap/LoaderBitmapGettersGetter;->mLoaderManager:Landroid/support/v4/app/LoaderManager;

    .line 35
    iput p3, p0, Lcom/google/android/music/leanback/bitmap/LoaderBitmapGettersGetter;->mLoaderId:I

    .line 36
    iput-wide p4, p0, Lcom/google/android/music/leanback/bitmap/LoaderBitmapGettersGetter;->mSeed:J

    .line 37
    return-void
.end method

.method private getBitmapGetters(Landroid/database/Cursor;)[Lcom/google/android/music/leanback/bitmap/BitmapGetter;
    .locals 6
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 53
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 55
    .local v0, "bitmapGettersList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/music/leanback/bitmap/BitmapGetter;>;"
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/music/leanback/bitmap/LoaderBitmapGettersGetter;->onCursorLoaded()V

    .line 56
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 58
    :cond_0
    invoke-virtual {p0, p1, v0}, Lcom/google/android/music/leanback/bitmap/LoaderBitmapGettersGetter;->addBitmapGetters(Landroid/database/Cursor;Ljava/util/ArrayList;)V

    .line 59
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    const/16 v3, 0x32

    if-lt v2, v3, :cond_0

    .line 65
    :cond_1
    :goto_0
    new-instance v2, Ljava/util/Random;

    iget-wide v4, p0, Lcom/google/android/music/leanback/bitmap/LoaderBitmapGettersGetter;->mSeed:J

    invoke-direct {v2, v4, v5}, Ljava/util/Random;-><init>(J)V

    invoke-static {v0, v2}, Ljava/util/Collections;->shuffle(Ljava/util/List;Ljava/util/Random;)V

    .line 66
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v2, v2, [Lcom/google/android/music/leanback/bitmap/BitmapGetter;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lcom/google/android/music/leanback/bitmap/BitmapGetter;

    return-object v2

    .line 61
    :catch_0
    move-exception v1

    .line 62
    .local v1, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/google/android/music/leanback/bitmap/LoaderBitmapGettersGetter;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to traverse cursor: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method protected abstract addBitmapGetters(Landroid/database/Cursor;Ljava/util/ArrayList;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/music/leanback/bitmap/BitmapGetter;",
            ">;)V"
        }
    .end annotation
.end method

.method public getBitmapGetters()[Lcom/google/android/music/leanback/bitmap/BitmapGetter;
    .locals 2

    .prologue
    .line 72
    :try_start_0
    iget-object v1, p0, Lcom/google/android/music/leanback/bitmap/LoaderBitmapGettersGetter;->mSempahore:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->acquire()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 76
    :goto_0
    iget-object v1, p0, Lcom/google/android/music/leanback/bitmap/LoaderBitmapGettersGetter;->mSempahore:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    .line 77
    iget-object v1, p0, Lcom/google/android/music/leanback/bitmap/LoaderBitmapGettersGetter;->mCursor:Landroid/database/Cursor;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/music/leanback/bitmap/LoaderBitmapGettersGetter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_0

    .line 78
    iget-object v1, p0, Lcom/google/android/music/leanback/bitmap/LoaderBitmapGettersGetter;->mCursor:Landroid/database/Cursor;

    invoke-direct {p0, v1}, Lcom/google/android/music/leanback/bitmap/LoaderBitmapGettersGetter;->getBitmapGetters(Landroid/database/Cursor;)[Lcom/google/android/music/leanback/bitmap/BitmapGetter;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/leanback/bitmap/LoaderBitmapGettersGetter;->mBitmapGetters:[Lcom/google/android/music/leanback/bitmap/BitmapGetter;

    .line 80
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/leanback/bitmap/LoaderBitmapGettersGetter;->mBitmapGetters:[Lcom/google/android/music/leanback/bitmap/BitmapGetter;

    return-object v1

    .line 73
    :catch_0
    move-exception v0

    .line 74
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method

.method protected abstract getCursorLoader()Landroid/support/v4/content/Loader;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 1
    .param p1, "id"    # I
    .param p2, "args"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 85
    invoke-virtual {p0}, Lcom/google/android/music/leanback/bitmap/LoaderBitmapGettersGetter;->getCursorLoader()Landroid/support/v4/content/Loader;

    move-result-object v0

    return-object v0
.end method

.method protected onCursorLoaded()V
    .locals 0

    .prologue
    .line 50
    return-void
.end method

.method public onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 1
    .param p2, "data"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 90
    .local p1, "loader":Landroid/support/v4/content/Loader;, "Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    iput-object p2, p0, Lcom/google/android/music/leanback/bitmap/LoaderBitmapGettersGetter;->mCursor:Landroid/database/Cursor;

    .line 91
    iget-object v0, p0, Lcom/google/android/music/leanback/bitmap/LoaderBitmapGettersGetter;->mSempahore:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    .line 92
    return-void
.end method

.method public bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Landroid/support/v4/content/Loader;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 19
    check-cast p2, Landroid/database/Cursor;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/music/leanback/bitmap/LoaderBitmapGettersGetter;->onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 96
    .local p1, "loader":Landroid/support/v4/content/Loader;, "Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    iget-object v0, p0, Lcom/google/android/music/leanback/bitmap/LoaderBitmapGettersGetter;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 97
    iget-object v0, p0, Lcom/google/android/music/leanback/bitmap/LoaderBitmapGettersGetter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 99
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/leanback/bitmap/LoaderBitmapGettersGetter;->mCursor:Landroid/database/Cursor;

    .line 100
    return-void
.end method

.method public startLoading()V
    .locals 3

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/music/leanback/bitmap/LoaderBitmapGettersGetter;->mLoaderManager:Landroid/support/v4/app/LoaderManager;

    iget v1, p0, Lcom/google/android/music/leanback/bitmap/LoaderBitmapGettersGetter;->mLoaderId:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/LoaderManager;->destroyLoader(I)V

    .line 41
    iget-object v0, p0, Lcom/google/android/music/leanback/bitmap/LoaderBitmapGettersGetter;->mLoaderManager:Landroid/support/v4/app/LoaderManager;

    iget v1, p0, Lcom/google/android/music/leanback/bitmap/LoaderBitmapGettersGetter;->mLoaderId:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 42
    return-void
.end method
