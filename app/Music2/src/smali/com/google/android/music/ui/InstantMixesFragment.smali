.class public Lcom/google/android/music/ui/InstantMixesFragment;
.super Lcom/google/android/music/ui/SubFragmentsPagerFragment;
.source "InstantMixesFragment.java"


# direct methods
.method public constructor <init>()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 19
    invoke-direct {p0}, Lcom/google/android/music/ui/SubFragmentsPagerFragment;-><init>()V

    .line 20
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 21
    .local v0, "tabs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/music/ui/FragmentTabInfo;>;"
    new-instance v1, Lcom/google/android/music/ui/FragmentTabInfo;

    const-string v2, "instant_mixes"

    const v3, 0x7f0b00ae

    const-class v4, Lcom/google/android/music/ui/MyRadioFragment;

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/music/ui/FragmentTabInfo;-><init>(Ljava/lang/String;ILjava/lang/Class;Z)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 23
    new-instance v1, Lcom/google/android/music/ui/FragmentTabInfo;

    const-string v2, "recommended"

    const v3, 0x7f0b00ad

    const-class v4, Lcom/google/android/music/ui/RecommendedRadioFragment;

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/music/ui/FragmentTabInfo;-><init>(Ljava/lang/String;ILjava/lang/Class;Z)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 25
    const-string v1, "instant_mixes"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/music/ui/InstantMixesFragment;->init(Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 26
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedState"    # Landroid/os/Bundle;

    .prologue
    .line 30
    invoke-super {p0, p1}, Lcom/google/android/music/ui/SubFragmentsPagerFragment;->onCreate(Landroid/os/Bundle;)V

    .line 31
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/InstantMixesFragment;->setHasOptionsMenu(Z)V

    .line 32
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 3
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "inflater"    # Landroid/view/MenuInflater;

    .prologue
    .line 36
    invoke-virtual {p0}, Lcom/google/android/music/ui/InstantMixesFragment;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    .line 37
    .local v0, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->showStartRadioButtonsInActionBar()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 38
    const v1, 0x7f140003

    invoke-virtual {p2, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 39
    const v1, 0x7f0e02a3

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    const v2, 0x7f0b025b

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    .line 41
    :cond_0
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 45
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 50
    invoke-super {p0, p1}, Lcom/google/android/music/ui/SubFragmentsPagerFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 47
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/music/ui/InstantMixesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/music/ui/SearchActivity;->showCreateRadioSearch(Landroid/content/Context;)V

    .line 48
    const/4 v0, 0x1

    goto :goto_0

    .line 45
    nop

    :pswitch_data_0
    .packed-switch 0x7f0e02a3
        :pswitch_0
    .end packed-switch
.end method
