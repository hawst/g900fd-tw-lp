.class public Lcom/google/android/music/cloudclient/ListenNowPlaylistJson;
.super Lcom/google/api/client/json/GenericJson;
.source "ListenNowPlaylistJson.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/cloudclient/ListenNowPlaylistJson$PlaylistIdJson;
    }
.end annotation


# instance fields
.field public mDescription:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "description"
    .end annotation
.end field

.field public mEditorArtwork:Lcom/google/android/music/cloudclient/ImageRefJson;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "playlist_creator_art_ref"
    .end annotation
.end field

.field public mId:Lcom/google/android/music/cloudclient/ListenNowPlaylistJson$PlaylistIdJson;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "id"
    .end annotation
.end field

.field public mName:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "title"
    .end annotation
.end field

.field public mOwner:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "owner"
    .end annotation
.end field

.field public mOwnerProfileImage:Lcom/google/android/music/cloudclient/ImageRefJson;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "owner_profile_photo"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    .line 25
    return-void
.end method
