.class public abstract Lcom/google/android/music/ui/FakeNthCardMediaListCardAdapter;
.super Lcom/google/android/music/ui/MediaListCardAdapter;
.source "FakeNthCardMediaListCardAdapter.java"


# instance fields
.field private mDisableFake:Z

.field private final mFakeItemIndex:I

.field private final mFakeItemLayout:I

.field private final mLayoutId:I

.field private mLayoutInflater:Landroid/view/LayoutInflater;

.field private mShowFakeEvenIfEmpty:Z


# direct methods
.method protected constructor <init>(Lcom/google/android/music/ui/MusicFragment;III)V
    .locals 2
    .param p1, "musicFragment"    # Lcom/google/android/music/ui/MusicFragment;
    .param p2, "layout"    # I
    .param p3, "fakeLayout"    # I
    .param p4, "fakeItemIndex"    # I

    .prologue
    const/4 v0, 0x0

    .line 33
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/ui/MediaListCardAdapter;-><init>(Lcom/google/android/music/ui/MusicFragment;I)V

    .line 22
    iput-boolean v0, p0, Lcom/google/android/music/ui/FakeNthCardMediaListCardAdapter;->mShowFakeEvenIfEmpty:Z

    .line 23
    iput-boolean v0, p0, Lcom/google/android/music/ui/FakeNthCardMediaListCardAdapter;->mDisableFake:Z

    .line 34
    iput p3, p0, Lcom/google/android/music/ui/FakeNthCardMediaListCardAdapter;->mFakeItemLayout:I

    .line 35
    iput p4, p0, Lcom/google/android/music/ui/FakeNthCardMediaListCardAdapter;->mFakeItemIndex:I

    .line 36
    iput p2, p0, Lcom/google/android/music/ui/FakeNthCardMediaListCardAdapter;->mLayoutId:I

    .line 37
    invoke-interface {p1}, Lcom/google/android/music/ui/MusicFragment;->getFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->getLayoutInflater(Landroid/os/Bundle;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/FakeNthCardMediaListCardAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    .line 38
    return-void
.end method

.method protected constructor <init>(Lcom/google/android/music/ui/MusicFragment;IIILandroid/database/Cursor;)V
    .locals 2
    .param p1, "musicFragment"    # Lcom/google/android/music/ui/MusicFragment;
    .param p2, "layout"    # I
    .param p3, "fakeLayout"    # I
    .param p4, "fakeItemIndex"    # I
    .param p5, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v0, 0x0

    .line 51
    invoke-direct {p0, p1, p2, p5}, Lcom/google/android/music/ui/MediaListCardAdapter;-><init>(Lcom/google/android/music/ui/MusicFragment;ILandroid/database/Cursor;)V

    .line 22
    iput-boolean v0, p0, Lcom/google/android/music/ui/FakeNthCardMediaListCardAdapter;->mShowFakeEvenIfEmpty:Z

    .line 23
    iput-boolean v0, p0, Lcom/google/android/music/ui/FakeNthCardMediaListCardAdapter;->mDisableFake:Z

    .line 52
    iput p3, p0, Lcom/google/android/music/ui/FakeNthCardMediaListCardAdapter;->mFakeItemLayout:I

    .line 53
    iput p4, p0, Lcom/google/android/music/ui/FakeNthCardMediaListCardAdapter;->mFakeItemIndex:I

    .line 54
    iput p2, p0, Lcom/google/android/music/ui/FakeNthCardMediaListCardAdapter;->mLayoutId:I

    .line 55
    invoke-interface {p1}, Lcom/google/android/music/ui/MusicFragment;->getFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->getLayoutInflater(Landroid/os/Bundle;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/FakeNthCardMediaListCardAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    .line 56
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 2

    .prologue
    .line 124
    invoke-super {p0}, Lcom/google/android/music/ui/MediaListCardAdapter;->getCount()I

    move-result v0

    .line 125
    .local v0, "count":I
    iget-boolean v1, p0, Lcom/google/android/music/ui/FakeNthCardMediaListCardAdapter;->mDisableFake:Z

    if-nez v1, :cond_1

    if-gtz v0, :cond_0

    iget-boolean v1, p0, Lcom/google/android/music/ui/FakeNthCardMediaListCardAdapter;->mShowFakeEvenIfEmpty:Z

    if-eqz v1, :cond_1

    .line 126
    :cond_0
    add-int/lit8 v0, v0, 0x1

    .line 128
    :cond_1
    return v0
.end method

.method protected getFakeItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 132
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getFakeItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 136
    const-wide/high16 v0, -0x8000000000000000L

    return-wide v0
.end method

.method public abstract getFakeView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 111
    iget-boolean v0, p0, Lcom/google/android/music/ui/FakeNthCardMediaListCardAdapter;->mDisableFake:Z

    if-eqz v0, :cond_0

    invoke-super {p0, p1}, Lcom/google/android/music/ui/MediaListCardAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    .line 117
    :goto_0
    return-object v0

    .line 112
    :cond_0
    iget v0, p0, Lcom/google/android/music/ui/FakeNthCardMediaListCardAdapter;->mFakeItemIndex:I

    if-ge p1, v0, :cond_1

    .line 113
    invoke-super {p0, p1}, Lcom/google/android/music/ui/MediaListCardAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 114
    :cond_1
    iget v0, p0, Lcom/google/android/music/ui/FakeNthCardMediaListCardAdapter;->mFakeItemIndex:I

    if-ne v0, p1, :cond_2

    .line 115
    invoke-virtual {p0, p1}, Lcom/google/android/music/ui/FakeNthCardMediaListCardAdapter;->getFakeItem(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 117
    :cond_2
    add-int/lit8 v0, p1, -0x1

    invoke-super {p0, v0}, Lcom/google/android/music/ui/MediaListCardAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 99
    iget-boolean v0, p0, Lcom/google/android/music/ui/FakeNthCardMediaListCardAdapter;->mDisableFake:Z

    if-eqz v0, :cond_0

    invoke-super {p0, p1}, Lcom/google/android/music/ui/MediaListCardAdapter;->getItemId(I)J

    move-result-wide v0

    .line 105
    :goto_0
    return-wide v0

    .line 100
    :cond_0
    iget v0, p0, Lcom/google/android/music/ui/FakeNthCardMediaListCardAdapter;->mFakeItemIndex:I

    if-ge p1, v0, :cond_1

    .line 101
    invoke-super {p0, p1}, Lcom/google/android/music/ui/MediaListCardAdapter;->getItemId(I)J

    move-result-wide v0

    goto :goto_0

    .line 102
    :cond_1
    iget v0, p0, Lcom/google/android/music/ui/FakeNthCardMediaListCardAdapter;->mFakeItemIndex:I

    if-ne v0, p1, :cond_2

    .line 103
    invoke-virtual {p0, p1}, Lcom/google/android/music/ui/FakeNthCardMediaListCardAdapter;->getFakeItemId(I)J

    move-result-wide v0

    goto :goto_0

    .line 105
    :cond_2
    add-int/lit8 v0, p1, -0x1

    invoke-super {p0, v0}, Lcom/google/android/music/ui/MediaListCardAdapter;->getItemId(I)J

    move-result-wide v0

    goto :goto_0
.end method

.method public getItemViewType(I)I
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 66
    iget-boolean v0, p0, Lcom/google/android/music/ui/FakeNthCardMediaListCardAdapter;->mDisableFake:Z

    if-eqz v0, :cond_0

    invoke-super {p0, p1}, Lcom/google/android/music/ui/MediaListCardAdapter;->getItemViewType(I)I

    move-result v0

    .line 70
    :goto_0
    return v0

    .line 67
    :cond_0
    iget v0, p0, Lcom/google/android/music/ui/FakeNthCardMediaListCardAdapter;->mFakeItemIndex:I

    if-ne p1, v0, :cond_1

    iget v0, p0, Lcom/google/android/music/ui/FakeNthCardMediaListCardAdapter;->mFakeItemLayout:I

    iget v1, p0, Lcom/google/android/music/ui/FakeNthCardMediaListCardAdapter;->mLayoutId:I

    if-eq v0, v1, :cond_1

    .line 68
    const/4 v0, 0x1

    goto :goto_0

    .line 70
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 75
    iget-boolean v2, p0, Lcom/google/android/music/ui/FakeNthCardMediaListCardAdapter;->mDisableFake:Z

    if-eqz v2, :cond_0

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/music/ui/MediaListCardAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 94
    :goto_0
    return-object v1

    .line 77
    :cond_0
    iget v0, p0, Lcom/google/android/music/ui/FakeNthCardMediaListCardAdapter;->mFakeItemIndex:I

    .line 78
    .local v0, "fakeIndex":I
    iget v2, p0, Lcom/google/android/music/ui/FakeNthCardMediaListCardAdapter;->mFakeItemIndex:I

    invoke-super {p0}, Lcom/google/android/music/ui/MediaListCardAdapter;->getCount()I

    move-result v3

    if-le v2, v3, :cond_1

    .line 82
    const/4 v0, 0x0

    .line 84
    :cond_1
    if-ge p1, v0, :cond_2

    .line 85
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/music/ui/MediaListCardAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .local v1, "v":Landroid/view/View;
    goto :goto_0

    .line 86
    .end local v1    # "v":Landroid/view/View;
    :cond_2
    if-ne v0, p1, :cond_4

    .line 87
    if-nez p2, :cond_3

    .line 88
    invoke-virtual {p0}, Lcom/google/android/music/ui/FakeNthCardMediaListCardAdapter;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {p0, v2, p3}, Lcom/google/android/music/ui/FakeNthCardMediaListCardAdapter;->newFakeView(Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 90
    :cond_3
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/music/ui/FakeNthCardMediaListCardAdapter;->getFakeView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .restart local v1    # "v":Landroid/view/View;
    goto :goto_0

    .line 92
    .end local v1    # "v":Landroid/view/View;
    :cond_4
    add-int/lit8 v2, p1, -0x1

    invoke-super {p0, v2, p2, p3}, Lcom/google/android/music/ui/MediaListCardAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .restart local v1    # "v":Landroid/view/View;
    goto :goto_0
.end method

.method public getViewTypeCount()I
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 60
    iget-boolean v1, p0, Lcom/google/android/music/ui/FakeNthCardMediaListCardAdapter;->mDisableFake:Z

    if-eqz v1, :cond_1

    .line 61
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget v1, p0, Lcom/google/android/music/ui/FakeNthCardMediaListCardAdapter;->mFakeItemLayout:I

    iget v2, p0, Lcom/google/android/music/ui/FakeNthCardMediaListCardAdapter;->mLayoutId:I

    if-ne v1, v2, :cond_0

    const/4 v0, 0x2

    goto :goto_0
.end method

.method protected newFakeView(Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 140
    iget-object v0, p0, Lcom/google/android/music/ui/FakeNthCardMediaListCardAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    iget v1, p0, Lcom/google/android/music/ui/FakeNthCardMediaListCardAdapter;->mFakeItemLayout:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public setDisableFake(Z)V
    .locals 1
    .param p1, "disableFake"    # Z

    .prologue
    .line 150
    iget-boolean v0, p0, Lcom/google/android/music/ui/FakeNthCardMediaListCardAdapter;->mDisableFake:Z

    if-ne v0, p1, :cond_0

    .line 153
    :goto_0
    return-void

    .line 151
    :cond_0
    iput-boolean p1, p0, Lcom/google/android/music/ui/FakeNthCardMediaListCardAdapter;->mDisableFake:Z

    .line 152
    invoke-virtual {p0}, Lcom/google/android/music/ui/FakeNthCardMediaListCardAdapter;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method public setShowFakeEvenIfEmpty(Z)V
    .locals 1
    .param p1, "showFakeEvenIfEmpty"    # Z

    .prologue
    .line 144
    iget-boolean v0, p0, Lcom/google/android/music/ui/FakeNthCardMediaListCardAdapter;->mShowFakeEvenIfEmpty:Z

    if-ne p1, v0, :cond_0

    .line 147
    :goto_0
    return-void

    .line 145
    :cond_0
    iput-boolean p1, p0, Lcom/google/android/music/ui/FakeNthCardMediaListCardAdapter;->mShowFakeEvenIfEmpty:Z

    .line 146
    invoke-virtual {p0}, Lcom/google/android/music/ui/FakeNthCardMediaListCardAdapter;->notifyDataSetChanged()V

    goto :goto_0
.end method
