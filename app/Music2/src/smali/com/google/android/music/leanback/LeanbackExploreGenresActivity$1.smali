.class Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$1;
.super Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;
.source "LeanbackExploreGenresActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;->getListRowsAdapter()Landroid/support/v17/leanback/widget/ObjectAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;


# direct methods
.method constructor <init>(Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;Landroid/support/v17/leanback/widget/Presenter;)V
    .locals 0
    .param p2, "x0"    # Landroid/support/v17/leanback/widget/Presenter;

    .prologue
    .line 122
    iput-object p1, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$1;->this$0:Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;

    invoke-direct {p0, p2}, Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;-><init>(Landroid/support/v17/leanback/widget/Presenter;)V

    return-void
.end method

.method private getFeaturedGroupsBaseLoaderId(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 163
    invoke-direct {p0, p1}, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$1;->getFeaturedLoaderId(I)I

    move-result v0

    add-int/lit8 v0, v0, -0x3

    return v0
.end method

.method private getFeaturedLoaderId(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 151
    mul-int/lit16 v0, p1, 0x3e8

    rsub-int/lit8 v0, v0, -0x1

    return v0
.end method

.method private getNewReleasesImageLoaderId(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 159
    invoke-direct {p0, p1}, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$1;->getFeaturedLoaderId(I)I

    move-result v0

    add-int/lit8 v0, v0, -0x2

    return v0
.end method

.method private getNewReleasesLoaderId(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 155
    invoke-direct {p0, p1}, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$1;->getFeaturedLoaderId(I)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    return v0
.end method


# virtual methods
.method protected bind(Landroid/database/Cursor;)Ljava/lang/Object;
    .locals 18
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 125
    # getter for: Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;->access$000()Ljava/lang/String;

    move-result-object v2

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "List rows binding for position: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->getPosition()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 128
    .local v3, "genreNautilusId":Ljava/lang/String;
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 129
    .local v4, "genreName":Ljava/lang/String;
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    .line 131
    .local v14, "subgenreCount":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$1;->this$0:Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;

    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->getPosition()I

    move-result v5

    # invokes: Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;->getRadioItem(Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/music/leanback/Item;
    invoke-static {v2, v3, v4, v5}, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;->access$100(Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/music/leanback/Item;

    move-result-object v13

    .line 132
    .local v13, "radioItem":Lcom/google/android/music/leanback/Item;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$1;->this$0:Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;

    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->getPosition()I

    move-result v5

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$1;->getFeaturedGroupsBaseLoaderId(I)I

    move-result v5

    const-wide/16 v10, -0xab7

    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->getPosition()I

    move-result v6

    mul-int/lit16 v6, v6, 0x3e8

    int-to-long v0, v6

    move-wide/from16 v16, v0

    add-long v6, v10, v16

    # invokes: Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;->getFeaturedAdapter(Ljava/lang/String;Ljava/lang/String;IJ)Landroid/support/v17/leanback/widget/CursorObjectAdapter;
    invoke-static/range {v2 .. v7}, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;->access$200(Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;Ljava/lang/String;Ljava/lang/String;IJ)Landroid/support/v17/leanback/widget/CursorObjectAdapter;

    move-result-object v8

    .line 135
    .local v8, "featuredAdapter":Landroid/support/v17/leanback/widget/CursorObjectAdapter;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$1;->this$0:Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;

    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->getPosition()I

    move-result v5

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$1;->getNewReleasesImageLoaderId(I)I

    move-result v5

    const-wide/32 v10, 0x86e8

    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->getPosition()I

    move-result v6

    int-to-long v0, v6

    move-wide/from16 v16, v0

    add-long v6, v10, v16

    # invokes: Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;->getNewReleasesAdapter(Ljava/lang/String;Ljava/lang/String;IJ)Landroid/support/v17/leanback/widget/CursorObjectAdapter;
    invoke-static/range {v2 .. v7}, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;->access$300(Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;Ljava/lang/String;Ljava/lang/String;IJ)Landroid/support/v17/leanback/widget/CursorObjectAdapter;

    move-result-object v9

    .line 138
    .local v9, "newReleasesAdapter":Landroid/support/v17/leanback/widget/CursorObjectAdapter;
    new-instance v7, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$1;->this$0:Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;

    invoke-virtual {v2}, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;->getItemPresenter()Lcom/google/android/music/leanback/ItemPresenter;

    move-result-object v2

    invoke-direct {v7, v2, v13, v8, v9}, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;-><init>(Lcom/google/android/music/leanback/ItemPresenter;Lcom/google/android/music/leanback/Item;Landroid/support/v17/leanback/widget/ObjectAdapter;Landroid/support/v17/leanback/widget/ObjectAdapter;)V

    .line 140
    .local v7, "genreItemsObjectAdapter":Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;
    if-lez v14, :cond_0

    .line 141
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$1;->this$0:Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;

    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->getPosition()I

    move-result v5

    # invokes: Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;->prepareSubgenresItem(Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;Ljava/lang/String;Ljava/lang/String;I)V
    invoke-static {v2, v7, v3, v4, v5}, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;->access$400(Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$GenreItemsObjectAdapter;Ljava/lang/String;Ljava/lang/String;I)V

    .line 144
    :cond_0
    new-instance v5, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$ExploreGenresListRow;

    new-instance v6, Landroid/support/v17/leanback/widget/HeaderItem;

    const/4 v2, 0x0

    invoke-direct {v6, v4, v2}, Landroid/support/v17/leanback/widget/HeaderItem;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->getPosition()I

    move-result v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$1;->getFeaturedLoaderId(I)I

    move-result v10

    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->getPosition()I

    move-result v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$1;->getNewReleasesLoaderId(I)I

    move-result v11

    move-object v12, v3

    invoke-direct/range {v5 .. v12}, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$ExploreGenresListRow;-><init>(Landroid/support/v17/leanback/widget/HeaderItem;Landroid/support/v17/leanback/widget/ObjectAdapter;Landroid/support/v17/leanback/widget/CursorObjectAdapter;Landroid/support/v17/leanback/widget/CursorObjectAdapter;IILjava/lang/String;)V

    return-object v5
.end method
