.class public Lcom/google/android/music/download/artwork/ArtDownloadService;
.super Landroid/app/Service;
.source "ArtDownloadService.java"

# interfaces
.implements Lcom/google/android/music/download/artwork/ArtDownloadExecutor$DownloadListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/download/artwork/ArtDownloadService$RequestHandler;,
        Lcom/google/android/music/download/artwork/ArtDownloadService$IncomingMessengerHandler;
    }
.end annotation


# static fields
.field static final CACHED_ART_PROJECTION:[Ljava/lang/String;

.field private static final LOGV:Z


# instance fields
.field private mArtExecutor:Lcom/google/android/music/download/artwork/ArtDownloadExecutor;

.field private mClients:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Landroid/os/Messenger;",
            ">;"
        }
    .end annotation
.end field

.field private final mFileSystem:Lcom/google/android/music/download/cache/FileSystem;

.field private final mIncomingMessenger:Landroid/os/Messenger;

.field private final mNetworkChangeListener:Lcom/google/android/music/net/INetworkChangeListener;

.field private mNetworkMonitorServiceConnection:Lcom/google/android/music/net/NetworkMonitorServiceConnection;

.field private mOrphanedFilesScavenger:Ljava/lang/Thread;

.field private mPreferences:Landroid/content/SharedPreferences;

.field private mRequestHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 67
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->DOWNLOAD:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/download/artwork/ArtDownloadService;->LOGV:Z

    .line 168
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "RemoteLocation"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "LocalLocation"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "LocalLocationStorageType"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/download/artwork/ArtDownloadService;->CACHED_ART_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 174
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 134
    iput-object v2, p0, Lcom/google/android/music/download/artwork/ArtDownloadService;->mArtExecutor:Lcom/google/android/music/download/artwork/ArtDownloadExecutor;

    .line 143
    new-instance v0, Landroid/os/Messenger;

    new-instance v1, Lcom/google/android/music/download/artwork/ArtDownloadService$IncomingMessengerHandler;

    invoke-direct {v1, p0, v2}, Lcom/google/android/music/download/artwork/ArtDownloadService$IncomingMessengerHandler;-><init>(Lcom/google/android/music/download/artwork/ArtDownloadService;Lcom/google/android/music/download/artwork/ArtDownloadService$1;)V

    invoke-direct {v0, v1}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/google/android/music/download/artwork/ArtDownloadService;->mIncomingMessenger:Landroid/os/Messenger;

    .line 677
    new-instance v0, Lcom/google/android/music/download/artwork/ArtDownloadService$4;

    invoke-direct {v0, p0}, Lcom/google/android/music/download/artwork/ArtDownloadService$4;-><init>(Lcom/google/android/music/download/artwork/ArtDownloadService;)V

    iput-object v0, p0, Lcom/google/android/music/download/artwork/ArtDownloadService;->mNetworkChangeListener:Lcom/google/android/music/net/INetworkChangeListener;

    .line 175
    new-instance v0, Lcom/google/android/music/download/cache/FileSystemImpl;

    invoke-direct {v0, v2}, Lcom/google/android/music/download/cache/FileSystemImpl;-><init>(Lcom/google/android/music/download/cache/FilteredFileDeleter;)V

    iput-object v0, p0, Lcom/google/android/music/download/artwork/ArtDownloadService;->mFileSystem:Lcom/google/android/music/download/cache/FileSystem;

    .line 176
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/music/download/artwork/ArtDownloadService;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/download/artwork/ArtDownloadService;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/google/android/music/download/artwork/ArtDownloadService;->clearOrphanedFilesAndStartAsyncDownloads()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/music/download/artwork/ArtDownloadService;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/download/artwork/ArtDownloadService;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/google/android/music/download/artwork/ArtDownloadService;->notifyServiceIdle()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/music/download/artwork/ArtDownloadService;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/download/artwork/ArtDownloadService;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/google/android/music/download/artwork/ArtDownloadService;->shutdownArtExecutorNow()V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/music/download/artwork/ArtDownloadService;)Ljava/util/Set;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/download/artwork/ArtDownloadService;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/music/download/artwork/ArtDownloadService;->mClients:Ljava/util/Set;

    return-object v0
.end method

.method private adjustCacheSize(J)V
    .locals 11
    .param p1, "increment"    # J

    .prologue
    .line 198
    iget-object v6, p0, Lcom/google/android/music/download/artwork/ArtDownloadService;->mPreferences:Landroid/content/SharedPreferences;

    monitor-enter v6

    .line 199
    :try_start_0
    iget-object v1, p0, Lcom/google/android/music/download/artwork/ArtDownloadService;->mPreferences:Landroid/content/SharedPreferences;

    const-string v7, "AlbumArtCacheSize"

    const-wide/16 v8, 0x0

    invoke-interface {v1, v7, v8, v9}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    .line 200
    .local v4, "size":J
    add-long v2, v4, p1

    .line 204
    .local v2, "newSize":J
    iget-object v1, p0, Lcom/google/android/music/download/artwork/ArtDownloadService;->mPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v0

    .line 206
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    :try_start_1
    const-string v1, "AlbumArtCacheSize"

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 208
    :try_start_2
    invoke-static {v0}, Lcom/google/android/music/SharedPreferencesCompat;->apply(Landroid/content/SharedPreferences$Editor;)V

    .line 210
    monitor-exit v6

    .line 211
    return-void

    .line 208
    :catchall_0
    move-exception v1

    invoke-static {v0}, Lcom/google/android/music/SharedPreferencesCompat;->apply(Landroid/content/SharedPreferences$Editor;)V

    throw v1

    .line 210
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v2    # "newSize":J
    .end local v4    # "size":J
    :catchall_1
    move-exception v1

    monitor-exit v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v1
.end method

.method private static clearOrphanedArtFiles(Lcom/google/android/music/download/artwork/ArtDownloadService;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Lcom/google/android/music/utils/DbUtils$CursorHelper;Ljava/io/File;Ljava/io/File;Lcom/google/android/music/download/cache/CacheUtils$ArtworkPathResolver;)J
    .locals 25
    .param p0, "service"    # Lcom/google/android/music/download/artwork/ArtDownloadService;
    .param p1, "cacheTable"    # Ljava/lang/String;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "idColumnName"    # Ljava/lang/String;
    .param p5, "externalCacheDir"    # Ljava/io/File;
    .param p6, "internalCacheDir"    # Ljava/io/File;
    .param p7, "resolver"    # Lcom/google/android/music/download/cache/CacheUtils$ArtworkPathResolver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/music/download/artwork/ArtDownloadService;",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/google/android/music/utils/DbUtils$CursorHelper",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/io/File;",
            "Ljava/io/File;",
            "Lcom/google/android/music/download/cache/CacheUtils$ArtworkPathResolver;",
            ")J"
        }
    .end annotation

    .prologue
    .line 235
    .local p4, "helper":Lcom/google/android/music/utils/DbUtils$CursorHelper;, "Lcom/google/android/music/utils/DbUtils$CursorHelper<Ljava/lang/String;>;"
    invoke-static/range {p0 .. p0}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v22

    .line 236
    .local v22, "store":Lcom/google/android/music/store/Store;
    invoke-virtual/range {v22 .. v22}, Lcom/google/android/music/store/Store;->beginRead()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    .line 237
    .local v4, "readDB":Landroid/database/sqlite/SQLiteDatabase;
    const/16 v24, 0x0

    .line 238
    .local v24, "writeDB":Landroid/database/sqlite/SQLiteDatabase;
    const-wide/16 v12, 0x0

    .line 239
    .local v12, "cacheSize":J
    const/16 v23, 0x0

    .line 242
    .local v23, "success":Z
    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v5, p1

    move-object/from16 v6, p2

    :try_start_0
    invoke-virtual/range {v4 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v17

    .line 243
    .local v17, "localFiles":Landroid/database/Cursor;
    if-nez v17, :cond_2

    .line 244
    const-wide/16 v6, -0x1

    .line 301
    if-eqz v4, :cond_0

    .line 302
    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 304
    :cond_0
    if-eqz v24, :cond_1

    .line 305
    move-object/from16 v0, v22

    move-object/from16 v1, v24

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    .line 308
    :cond_1
    :goto_0
    return-wide v6

    .line 246
    :cond_2
    :try_start_1
    new-instance v16, Ljava/util/HashMap;

    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->getCount()I

    move-result v5

    move-object/from16 v0, v16

    invoke-direct {v0, v5}, Ljava/util/HashMap;-><init>(I)V

    .line 247
    .local v16, "knownFiles":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v18, Ljava/util/HashSet;

    invoke-direct/range {v18 .. v18}, Ljava/util/HashSet;-><init>()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 249
    .local v18, "noLocalIds":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    :cond_3
    :goto_1
    :try_start_2
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-eqz v5, :cond_a

    .line 250
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v5

    if-eqz v5, :cond_5

    .line 251
    const-wide/16 v6, -0x1

    .line 276
    :try_start_3
    invoke-static/range {v17 .. v17}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 301
    if-eqz v4, :cond_4

    .line 302
    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 304
    :cond_4
    if-eqz v24, :cond_1

    .line 305
    move-object/from16 v0, v22

    move-object/from16 v1, v24

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    goto :goto_0

    .line 254
    :cond_5
    const/4 v5, 0x0

    :try_start_4
    move-object/from16 v0, p4

    move-object/from16 v1, v17

    invoke-interface {v0, v1, v5}, Lcom/google/android/music/utils/DbUtils$CursorHelper;->getValue(Landroid/database/Cursor;I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/String;

    .line 255
    .local v15, "id":Ljava/lang/String;
    const/4 v5, 0x1

    move-object/from16 v0, v17

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    .line 256
    .local v19, "path":Ljava/lang/String;
    const/4 v5, 0x2

    move-object/from16 v0, v17

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v21

    .line 257
    .local v21, "storageType":I
    move-object/from16 v0, p7

    move-object/from16 v1, v19

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/download/cache/CacheUtils$ArtworkPathResolver;->resolveArtworkPath(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v14

    .line 259
    .local v14, "f":Ljava/io/File;
    if-eqz v14, :cond_9

    .line 260
    invoke-virtual {v14}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_8

    .line 261
    if-eqz v15, :cond_3

    .line 262
    move-object/from16 v0, v18

    invoke-virtual {v0, v15}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 276
    .end local v14    # "f":Ljava/io/File;
    .end local v15    # "id":Ljava/lang/String;
    .end local v19    # "path":Ljava/lang/String;
    .end local v21    # "storageType":I
    :catchall_0
    move-exception v5

    :try_start_5
    invoke-static/range {v17 .. v17}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v5
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 301
    .end local v16    # "knownFiles":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v17    # "localFiles":Landroid/database/Cursor;
    .end local v18    # "noLocalIds":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    :catchall_1
    move-exception v5

    if-eqz v4, :cond_6

    .line 302
    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 304
    :cond_6
    if-eqz v24, :cond_7

    .line 305
    move-object/from16 v0, v22

    move-object/from16 v1, v24

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    :cond_7
    throw v5

    .line 265
    .restart local v14    # "f":Ljava/io/File;
    .restart local v15    # "id":Ljava/lang/String;
    .restart local v16    # "knownFiles":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .restart local v17    # "localFiles":Landroid/database/Cursor;
    .restart local v18    # "noLocalIds":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    .restart local v19    # "path":Ljava/lang/String;
    .restart local v21    # "storageType":I
    :cond_8
    :try_start_6
    invoke-virtual {v14}, Ljava/io/File;->length()J

    move-result-wide v6

    add-long/2addr v12, v6

    .line 266
    invoke-virtual {v14}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v16

    invoke-virtual {v0, v5, v15}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 269
    :cond_9
    sget-boolean v5, Lcom/google/android/music/download/artwork/ArtDownloadService;->LOGV:Z

    if-eqz v5, :cond_3

    .line 270
    const-string v5, "ArtDownloadService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Ignoring path: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " from validation as the storage is missing"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_1

    .line 276
    .end local v14    # "f":Ljava/io/File;
    .end local v15    # "id":Ljava/lang/String;
    .end local v19    # "path":Ljava/lang/String;
    .end local v21    # "storageType":I
    :cond_a
    :try_start_7
    invoke-static/range {v17 .. v17}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 278
    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 279
    const/4 v4, 0x0

    .line 282
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    move-object/from16 v2, p5

    invoke-static {v0, v1, v2}, Lcom/google/android/music/download/artwork/ArtDownloadService;->validateLocalFiles(Lcom/google/android/music/download/artwork/ArtDownloadService;Ljava/util/HashMap;Ljava/io/File;)V

    .line 284
    if-eqz p6, :cond_c

    if-eqz p5, :cond_b

    invoke-virtual/range {p5 .. p6}, Ljava/io/File;->compareTo(Ljava/io/File;)I

    move-result v5

    if-eqz v5, :cond_c

    .line 286
    :cond_b
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    move-object/from16 v2, p6

    invoke-static {v0, v1, v2}, Lcom/google/android/music/download/artwork/ArtDownloadService;->validateLocalFiles(Lcom/google/android/music/download/artwork/ArtDownloadService;Ljava/util/HashMap;Ljava/io/File;)V

    .line 289
    :cond_c
    invoke-virtual/range {v18 .. v18}, Ljava/util/HashSet;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_d

    invoke-virtual/range {v16 .. v16}, Ljava/util/HashMap;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_e

    .line 291
    :cond_d
    invoke-virtual/range {v16 .. v16}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v5

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 292
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    .line 293
    .local v20, "sb":Ljava/lang/StringBuilder;
    move-object/from16 v0, v20

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 294
    move-object/from16 v0, p4

    move-object/from16 v1, v20

    move-object/from16 v2, v18

    invoke-interface {v0, v1, v2}, Lcom/google/android/music/utils/DbUtils$CursorHelper;->appendIN(Ljava/lang/StringBuilder;Ljava/util/Collection;)V

    .line 295
    invoke-virtual/range {v22 .. v22}, Lcom/google/android/music/store/Store;->beginWriteTxn()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v24

    .line 296
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    move-object/from16 v0, v24

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 298
    const/16 v23, 0x1

    .line 301
    .end local v20    # "sb":Ljava/lang/StringBuilder;
    :cond_e
    if-eqz v4, :cond_f

    .line 302
    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 304
    :cond_f
    if-eqz v24, :cond_10

    .line 305
    move-object/from16 v0, v22

    move-object/from16 v1, v24

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    :cond_10
    move-wide v6, v12

    .line 308
    goto/16 :goto_0
.end method

.method private clearOrphanedFiles()V
    .locals 11

    .prologue
    .line 221
    invoke-static {}, Lcom/google/android/music/download/artwork/ArtDownloadUtils;->getArtCacheDirectory()Ljava/lang/String;

    move-result-object v10

    .line 222
    .local v10, "artTopDir":Ljava/lang/String;
    const-string v1, "ARTWORK_CACHE"

    sget-object v2, Lcom/google/android/music/download/artwork/ArtDownloadService;->CACHED_ART_PROJECTION:[Ljava/lang/String;

    const-string v3, "RemoteLocation"

    new-instance v4, Lcom/google/android/music/utils/DbUtils$StringCursorHelper;

    invoke-direct {v4}, Lcom/google/android/music/utils/DbUtils$StringCursorHelper;-><init>()V

    invoke-static {p0, v10}, Lcom/google/android/music/download/cache/CacheUtils;->getExternalCacheDirectory(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v5

    invoke-static {p0, v10}, Lcom/google/android/music/download/cache/CacheUtils;->getInternalCacheDirectory(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v6

    new-instance v7, Lcom/google/android/music/download/cache/CacheUtils$ArtWorkCachePathResolver;

    invoke-direct {v7, p0}, Lcom/google/android/music/download/cache/CacheUtils$ArtWorkCachePathResolver;-><init>(Landroid/content/Context;)V

    move-object v0, p0

    invoke-static/range {v0 .. v7}, Lcom/google/android/music/download/artwork/ArtDownloadService;->clearOrphanedArtFiles(Lcom/google/android/music/download/artwork/ArtDownloadService;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Lcom/google/android/music/utils/DbUtils$CursorHelper;Ljava/io/File;Ljava/io/File;Lcom/google/android/music/download/cache/CacheUtils$ArtworkPathResolver;)J

    move-result-wide v8

    .line 229
    .local v8, "artSize":J
    invoke-direct {p0, v8, v9}, Lcom/google/android/music/download/artwork/ArtDownloadService;->setCacheSize(J)V

    .line 230
    return-void
.end method

.method private clearOrphanedFilesAndStartAsyncDownloads()V
    .locals 0

    .prologue
    .line 214
    invoke-direct {p0}, Lcom/google/android/music/download/artwork/ArtDownloadService;->clearOrphanedFiles()V

    .line 215
    invoke-direct {p0}, Lcom/google/android/music/download/artwork/ArtDownloadService;->startAsyncDownloads()V

    .line 216
    return-void
.end method

.method private clearOrphanedFilesAsync()V
    .locals 2

    .prologue
    .line 361
    invoke-direct {p0}, Lcom/google/android/music/download/artwork/ArtDownloadService;->destroyOrphanedFilesScavenger()V

    .line 362
    new-instance v0, Lcom/google/android/music/download/artwork/ArtDownloadService$1;

    const-string v1, "OrphanedFilesScavenger"

    invoke-direct {v0, p0, v1}, Lcom/google/android/music/download/artwork/ArtDownloadService$1;-><init>(Lcom/google/android/music/download/artwork/ArtDownloadService;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/music/download/artwork/ArtDownloadService;->mOrphanedFilesScavenger:Ljava/lang/Thread;

    .line 369
    iget-object v0, p0, Lcom/google/android/music/download/artwork/ArtDownloadService;->mOrphanedFilesScavenger:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 370
    return-void
.end method

.method private deleteFileIfPossible(Ljava/lang/String;)Z
    .locals 1
    .param p1, "absolutePath"    # Ljava/lang/String;

    .prologue
    .line 627
    monitor-enter p0

    .line 628
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/download/artwork/ArtDownloadService;->mArtExecutor:Lcom/google/android/music/download/artwork/ArtDownloadExecutor;

    if-eqz v0, :cond_0

    .line 629
    iget-object v0, p0, Lcom/google/android/music/download/artwork/ArtDownloadService;->mArtExecutor:Lcom/google/android/music/download/artwork/ArtDownloadExecutor;

    invoke-virtual {v0, p1}, Lcom/google/android/music/download/artwork/ArtDownloadExecutor;->deleteFileIfPossible(Ljava/lang/String;)Z

    move-result v0

    monitor-exit p0

    .line 631
    :goto_0
    return v0

    :cond_0
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v0

    monitor-exit p0

    goto :goto_0

    .line 633
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private destroyOrphanedFilesScavenger()V
    .locals 1

    .prologue
    .line 373
    iget-object v0, p0, Lcom/google/android/music/download/artwork/ArtDownloadService;->mOrphanedFilesScavenger:Ljava/lang/Thread;

    if-eqz v0, :cond_0

    .line 374
    iget-object v0, p0, Lcom/google/android/music/download/artwork/ArtDownloadService;->mOrphanedFilesScavenger:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 375
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/download/artwork/ArtDownloadService;->mOrphanedFilesScavenger:Ljava/lang/Thread;

    .line 377
    :cond_0
    return-void
.end method

.method public static getCacheLimit(Landroid/content/Context;)J
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 478
    new-instance v0, Lcom/google/android/music/download/cache/FileSystemImpl;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/music/download/cache/FileSystemImpl;-><init>(Lcom/google/android/music/download/cache/FilteredFileDeleter;)V

    .line 479
    .local v0, "fs":Lcom/google/android/music/download/cache/FileSystem;
    const/4 v1, 0x5

    invoke-static {p0, v0, v1}, Lcom/google/android/music/download/artwork/ArtDownloadService;->percentageToBytesDiskSpace(Landroid/content/Context;Lcom/google/android/music/download/cache/FileSystem;I)J

    move-result-wide v2

    return-wide v2
.end method

.method private static getCacheRootDir(Landroid/content/Context;)Ljava/io/File;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 433
    invoke-virtual {p0}, Landroid/content/Context;->getExternalCacheDir()Ljava/io/File;

    move-result-object v0

    .line 434
    .local v0, "cacheRootDir":Ljava/io/File;
    if-nez v0, :cond_0

    .line 435
    invoke-virtual {p0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v0

    .line 437
    :cond_0
    return-object v0
.end method

.method private notifyArtDownloadFailed(Ljava/lang/String;)V
    .locals 1
    .param p1, "remoteLocation"    # Ljava/lang/String;

    .prologue
    .line 718
    const/4 v0, 0x4

    invoke-direct {p0, p1, v0}, Lcom/google/android/music/download/artwork/ArtDownloadService;->notifyClients(Ljava/lang/String;I)V

    .line 719
    return-void
.end method

.method private notifyArtDownloadSucceeded(Ljava/lang/String;)V
    .locals 1
    .param p1, "remoteLocation"    # Ljava/lang/String;

    .prologue
    .line 714
    const/4 v0, 0x2

    invoke-direct {p0, p1, v0}, Lcom/google/android/music/download/artwork/ArtDownloadService;->notifyClients(Ljava/lang/String;I)V

    .line 715
    return-void
.end method

.method private notifyClients(Ljava/lang/String;I)V
    .locals 11
    .param p1, "remoteLocation"    # Ljava/lang/String;
    .param p2, "msgWhat"    # I

    .prologue
    .line 722
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 723
    .local v0, "deadClients":Ljava/util/List;, "Ljava/util/List<Landroid/os/Messenger;>;"
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 724
    .local v2, "extras":Landroid/os/Bundle;
    if-eqz p1, :cond_0

    .line 725
    const-string v6, "remoteUrl"

    invoke-virtual {v2, v6, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 727
    :cond_0
    sget-boolean v6, Lcom/google/android/music/download/artwork/ArtDownloadService;->LOGV:Z

    if-eqz v6, :cond_1

    .line 728
    const-string v6, "ArtDownloadService"

    const-string v7, "sending notifyClients with what=%d to %d clients"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    iget-object v10, p0, Lcom/google/android/music/download/artwork/ArtDownloadService;->mClients:Ljava/util/Set;

    invoke-interface {v10}, Ljava/util/Set;->size()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 731
    :cond_1
    iget-object v6, p0, Lcom/google/android/music/download/artwork/ArtDownloadService;->mClients:Ljava/util/Set;

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/os/Messenger;

    .line 732
    .local v5, "messenger":Landroid/os/Messenger;
    const/4 v6, 0x0

    invoke-static {v6, p2, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v4

    .line 734
    .local v4, "message":Landroid/os/Message;
    :try_start_0
    invoke-virtual {v5, v4}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 735
    :catch_0
    move-exception v1

    .line 736
    .local v1, "e":Landroid/os/RemoteException;
    sget-boolean v6, Lcom/google/android/music/download/artwork/ArtDownloadService;->LOGV:Z

    if-eqz v6, :cond_2

    .line 737
    const-string v6, "ArtDownloadService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Removing dead client "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7, v1}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 739
    :cond_2
    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 743
    .end local v1    # "e":Landroid/os/RemoteException;
    .end local v4    # "message":Landroid/os/Message;
    .end local v5    # "messenger":Landroid/os/Messenger;
    :cond_3
    iget-object v6, p0, Lcom/google/android/music/download/artwork/ArtDownloadService;->mClients:Ljava/util/Set;

    invoke-interface {v6, v0}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 744
    return-void
.end method

.method private notifyServiceIdle()V
    .locals 2

    .prologue
    .line 747
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/download/artwork/ArtDownloadService;->notifyClients(Ljava/lang/String;I)V

    .line 748
    return-void
.end method

.method private static percentageToBytesDiskSpace(Landroid/content/Context;Lcom/google/android/music/download/cache/FileSystem;I)J
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "fs"    # Lcom/google/android/music/download/cache/FileSystem;
    .param p2, "percentage"    # I

    .prologue
    .line 457
    invoke-static {p0}, Lcom/google/android/music/download/artwork/ArtDownloadService;->getCacheRootDir(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    .line 458
    .local v0, "cacheRootDir":Ljava/io/File;
    invoke-interface {p1, v0}, Lcom/google/android/music/download/cache/FileSystem;->getTotalSpace(Ljava/io/File;)J

    move-result-wide v2

    .line 459
    .local v2, "totalSpace":J
    int-to-long v4, p2

    mul-long/2addr v4, v2

    const-wide/16 v6, 0x64

    div-long/2addr v4, v6

    return-wide v4
.end method

.method private queueArtRequestImp(Ljava/lang/String;Z)V
    .locals 1
    .param p1, "remoteUrl"    # Ljava/lang/String;
    .param p2, "isPrefetch"    # Z

    .prologue
    .line 613
    monitor-enter p0

    .line 614
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/download/artwork/ArtDownloadService;->mArtExecutor:Lcom/google/android/music/download/artwork/ArtDownloadExecutor;

    if-nez v0, :cond_0

    .line 615
    new-instance v0, Lcom/google/android/music/download/artwork/ArtDownloadExecutor;

    invoke-direct {v0, p0, p0}, Lcom/google/android/music/download/artwork/ArtDownloadExecutor;-><init>(Landroid/content/Context;Lcom/google/android/music/download/artwork/ArtDownloadExecutor$DownloadListener;)V

    iput-object v0, p0, Lcom/google/android/music/download/artwork/ArtDownloadService;->mArtExecutor:Lcom/google/android/music/download/artwork/ArtDownloadExecutor;

    .line 618
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/download/artwork/ArtDownloadService;->mArtExecutor:Lcom/google/android/music/download/artwork/ArtDownloadExecutor;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/music/download/artwork/ArtDownloadExecutor;->addDownload(Ljava/lang/String;Z)V

    .line 619
    monitor-exit p0

    .line 620
    return-void

    .line 619
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private setCacheSize(J)V
    .locals 5
    .param p1, "size"    # J

    .prologue
    .line 186
    const-string v1, "ArtDownloadService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Setting cache size "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    iget-object v2, p0, Lcom/google/android/music/download/artwork/ArtDownloadService;->mPreferences:Landroid/content/SharedPreferences;

    monitor-enter v2

    .line 188
    :try_start_0
    iget-object v1, p0, Lcom/google/android/music/download/artwork/ArtDownloadService;->mPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v0

    .line 190
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    :try_start_1
    const-string v1, "AlbumArtCacheSize"

    invoke-interface {v0, v1, p1, p2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 192
    :try_start_2
    invoke-static {v0}, Lcom/google/android/music/SharedPreferencesCompat;->apply(Landroid/content/SharedPreferences$Editor;)V

    .line 194
    monitor-exit v2

    .line 195
    return-void

    .line 192
    :catchall_0
    move-exception v1

    invoke-static {v0}, Lcom/google/android/music/SharedPreferencesCompat;->apply(Landroid/content/SharedPreferences$Editor;)V

    throw v1

    .line 194
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    :catchall_1
    move-exception v1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v1
.end method

.method private declared-synchronized shutdownArtExecutorNow()V
    .locals 1

    .prologue
    .line 390
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/download/artwork/ArtDownloadService;->mArtExecutor:Lcom/google/android/music/download/artwork/ArtDownloadExecutor;

    if-eqz v0, :cond_0

    .line 391
    iget-object v0, p0, Lcom/google/android/music/download/artwork/ArtDownloadService;->mArtExecutor:Lcom/google/android/music/download/artwork/ArtDownloadExecutor;

    invoke-virtual {v0}, Lcom/google/android/music/download/artwork/ArtDownloadExecutor;->shutdownNow()Ljava/util/List;

    .line 392
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/download/artwork/ArtDownloadService;->mArtExecutor:Lcom/google/android/music/download/artwork/ArtDownloadExecutor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 394
    :cond_0
    monitor-exit p0

    return-void

    .line 390
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private startAsyncDownloads()V
    .locals 2

    .prologue
    .line 416
    sget-object v0, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    new-instance v1, Lcom/google/android/music/download/artwork/ArtDownloadService$2;

    invoke-direct {v1, p0}, Lcom/google/android/music/download/artwork/ArtDownloadService$2;-><init>(Lcom/google/android/music/download/artwork/ArtDownloadService;)V

    invoke-static {v0, v1}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsync(Lcom/google/android/music/utils/LoggableHandler;Ljava/lang/Runnable;)V

    .line 422
    return-void
.end method

.method private static validateLocalFiles(Lcom/google/android/music/download/artwork/ArtDownloadService;Ljava/util/HashMap;Ljava/io/File;)V
    .locals 9
    .param p0, "service"    # Lcom/google/android/music/download/artwork/ArtDownloadService;
    .param p2, "fileOrDir"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/music/download/artwork/ArtDownloadService;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/io/File;",
            ")V"
        }
    .end annotation

    .prologue
    .line 313
    .local p1, "knownFiles":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/io/File;->exists()Z

    move-result v6

    if-nez v6, :cond_1

    .line 338
    :cond_0
    :goto_0
    return-void

    .line 316
    :cond_1
    invoke-virtual {p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    .line 317
    .local v0, "absolutePath":Ljava/lang/String;
    invoke-virtual {p2}, Ljava/io/File;->isFile()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 318
    const-string v6, ".nomedia"

    invoke-virtual {p2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 319
    invoke-virtual {p1, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 320
    invoke-virtual {p1, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 322
    :cond_2
    invoke-direct {p0, v0}, Lcom/google/android/music/download/artwork/ArtDownloadService;->deleteFileIfPossible(Ljava/lang/String;)Z

    goto :goto_0

    .line 329
    :cond_3
    invoke-virtual {p2}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v3

    .line 330
    .local v3, "files":[Ljava/io/File;
    if-nez v3, :cond_4

    .line 331
    const-string v6, "ArtDownloadService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Neither file nor directory: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 333
    :cond_4
    move-object v1, v3

    .local v1, "arr$":[Ljava/io/File;
    array-length v5, v1

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_1
    if-ge v4, v5, :cond_0

    aget-object v2, v1, v4

    .line 334
    .local v2, "file":Ljava/io/File;
    invoke-static {p0, p1, v2}, Lcom/google/android/music/download/artwork/ArtDownloadService;->validateLocalFiles(Lcom/google/android/music/download/artwork/ArtDownloadService;Ljava/util/HashMap;Ljava/io/File;)V

    .line 333
    add-int/lit8 v4, v4, 0x1

    goto :goto_1
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 342
    iget-object v0, p0, Lcom/google/android/music/download/artwork/ArtDownloadService;->mIncomingMessenger:Landroid/os/Messenger;

    invoke-virtual {v0}, Landroid/os/Messenger;->getBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public onCreate()V
    .locals 3

    .prologue
    .line 347
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 348
    const-string v1, "ArtDownload"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/google/android/music/download/artwork/ArtDownloadService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/download/artwork/ArtDownloadService;->mPreferences:Landroid/content/SharedPreferences;

    .line 349
    new-instance v1, Lcom/google/android/music/net/NetworkMonitorServiceConnection;

    iget-object v2, p0, Lcom/google/android/music/download/artwork/ArtDownloadService;->mNetworkChangeListener:Lcom/google/android/music/net/INetworkChangeListener;

    invoke-direct {v1, v2}, Lcom/google/android/music/net/NetworkMonitorServiceConnection;-><init>(Lcom/google/android/music/net/INetworkChangeListener;)V

    iput-object v1, p0, Lcom/google/android/music/download/artwork/ArtDownloadService;->mNetworkMonitorServiceConnection:Lcom/google/android/music/net/NetworkMonitorServiceConnection;

    .line 351
    iget-object v1, p0, Lcom/google/android/music/download/artwork/ArtDownloadService;->mNetworkMonitorServiceConnection:Lcom/google/android/music/net/NetworkMonitorServiceConnection;

    invoke-virtual {v1, p0}, Lcom/google/android/music/net/NetworkMonitorServiceConnection;->bindToService(Landroid/content/Context;)V

    .line 352
    invoke-direct {p0}, Lcom/google/android/music/download/artwork/ArtDownloadService;->clearOrphanedFilesAsync()V

    .line 354
    new-instance v1, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v1}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v1, p0, Lcom/google/android/music/download/artwork/ArtDownloadService;->mClients:Ljava/util/Set;

    .line 355
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "RequestHandlerThread"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 356
    .local v0, "thread":Landroid/os/HandlerThread;
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 357
    new-instance v1, Lcom/google/android/music/download/artwork/ArtDownloadService$RequestHandler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/google/android/music/download/artwork/ArtDownloadService$RequestHandler;-><init>(Lcom/google/android/music/download/artwork/ArtDownloadService;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/google/android/music/download/artwork/ArtDownloadService;->mRequestHandler:Landroid/os/Handler;

    .line 358
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 381
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 382
    invoke-direct {p0}, Lcom/google/android/music/download/artwork/ArtDownloadService;->destroyOrphanedFilesScavenger()V

    .line 383
    iget-object v0, p0, Lcom/google/android/music/download/artwork/ArtDownloadService;->mNetworkMonitorServiceConnection:Lcom/google/android/music/net/NetworkMonitorServiceConnection;

    invoke-virtual {v0, p0}, Lcom/google/android/music/net/NetworkMonitorServiceConnection;->unbindFromService(Landroid/content/Context;)V

    .line 384
    invoke-direct {p0}, Lcom/google/android/music/download/artwork/ArtDownloadService;->shutdownArtExecutorNow()V

    .line 385
    iget-object v0, p0, Lcom/google/android/music/download/artwork/ArtDownloadService;->mRequestHandler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 386
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/download/artwork/ArtDownloadService;->mRequestHandler:Landroid/os/Handler;

    .line 387
    return-void
.end method

.method public onDownloadArtworkFile(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "remoteLocation"    # Ljava/lang/String;
    .param p2, "localLocation"    # Ljava/lang/String;

    .prologue
    .line 657
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/download/artwork/ArtDownloadService;->adjustCacheSize(J)V

    .line 658
    if-eqz p2, :cond_0

    .line 659
    invoke-direct {p0, p1}, Lcom/google/android/music/download/artwork/ArtDownloadService;->notifyArtDownloadSucceeded(Ljava/lang/String;)V

    .line 663
    :goto_0
    return-void

    .line 661
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/music/download/artwork/ArtDownloadService;->notifyArtDownloadFailed(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onDownloadQueueCompleted()V
    .locals 0

    .prologue
    .line 674
    invoke-direct {p0}, Lcom/google/android/music/download/artwork/ArtDownloadService;->notifyServiceIdle()V

    .line 675
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 398
    if-eqz p1, :cond_0

    const-string v1, "com.google.android.music.SYNC_COMPLETE"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 399
    invoke-direct {p0}, Lcom/google/android/music/download/artwork/ArtDownloadService;->clearOrphanedFilesAsync()V

    .line 412
    :goto_0
    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    move-result v1

    return v1

    .line 400
    :cond_0
    if-eqz p1, :cond_2

    const-string v1, "com.google.android.music.ACTION_REMOTE_ART_REQUESTED"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 402
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 403
    .local v0, "extras":Landroid/os/Bundle;
    if-eqz v0, :cond_1

    .line 404
    iget-object v1, p0, Lcom/google/android/music/download/artwork/ArtDownloadService;->mRequestHandler:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    .line 406
    :cond_1
    const-string v1, "ArtDownloadService"

    const-string v2, "Art request should have extras"

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 409
    .end local v0    # "extras":Landroid/os/Bundle;
    :cond_2
    invoke-direct {p0}, Lcom/google/android/music/download/artwork/ArtDownloadService;->startAsyncDownloads()V

    goto :goto_0
.end method

.method public queueArtRequest(Ljava/lang/String;Z)V
    .locals 4
    .param p1, "remoteUrl"    # Ljava/lang/String;
    .param p2, "isPrefetch"    # Z

    .prologue
    .line 588
    iget-object v2, p0, Lcom/google/android/music/download/artwork/ArtDownloadService;->mNetworkMonitorServiceConnection:Lcom/google/android/music/net/NetworkMonitorServiceConnection;

    invoke-virtual {v2}, Lcom/google/android/music/net/NetworkMonitorServiceConnection;->getNetworkMonitor()Lcom/google/android/music/net/INetworkMonitor;

    move-result-object v1

    .line 589
    .local v1, "networkMonitor":Lcom/google/android/music/net/INetworkMonitor;
    if-nez v1, :cond_0

    .line 590
    iget-object v2, p0, Lcom/google/android/music/download/artwork/ArtDownloadService;->mNetworkMonitorServiceConnection:Lcom/google/android/music/net/NetworkMonitorServiceConnection;

    new-instance v3, Lcom/google/android/music/download/artwork/ArtDownloadService$3;

    invoke-direct {v3, p0, p1, p2}, Lcom/google/android/music/download/artwork/ArtDownloadService$3;-><init>(Lcom/google/android/music/download/artwork/ArtDownloadService;Ljava/lang/String;Z)V

    invoke-virtual {v2, v3}, Lcom/google/android/music/net/NetworkMonitorServiceConnection;->addRunOnServiceConnected(Ljava/lang/Runnable;)V

    .line 609
    :goto_0
    return-void

    .line 599
    :cond_0
    :try_start_0
    invoke-interface {v1}, Lcom/google/android/music/net/INetworkMonitor;->isConnected()Z

    move-result v2

    if-nez v2, :cond_1

    .line 600
    invoke-virtual {p0}, Lcom/google/android/music/download/artwork/ArtDownloadService;->stopSelf()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 603
    :catch_0
    move-exception v0

    .line 604
    .local v0, "e":Landroid/os/RemoteException;
    const-string v2, "ArtDownloadService"

    const-string v3, "queueArtRequest: remote call to NetworkMonitor failed"

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 608
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/download/artwork/ArtDownloadService;->queueArtRequestImp(Ljava/lang/String;Z)V

    goto :goto_0
.end method
