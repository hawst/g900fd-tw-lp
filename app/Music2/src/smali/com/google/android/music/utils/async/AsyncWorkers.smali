.class public Lcom/google/android/music/utils/async/AsyncWorkers;
.super Ljava/lang/Object;
.source "AsyncWorkers.java"


# static fields
.field public static final sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

.field public static final sUIBackgroundWorker:Lcom/google/android/music/utils/LoggableHandler;

.field private static final sUniqueMessageTypes:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/google/android/music/utils/LoggableHandler;",
            "Ljava/util/concurrent/atomic/AtomicInteger;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 22
    new-instance v0, Lcom/google/android/music/utils/LoggableHandler;

    const-string v1, "UIAsyncWorker"

    invoke-direct {v0, v1}, Lcom/google/android/music/utils/LoggableHandler;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/music/utils/async/AsyncWorkers;->sUIBackgroundWorker:Lcom/google/android/music/utils/LoggableHandler;

    .line 27
    new-instance v0, Lcom/google/android/music/utils/LoggableHandler;

    const-string v1, "BGAsyncWorker"

    invoke-direct {v0, v1}, Lcom/google/android/music/utils/LoggableHandler;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    .line 30
    new-instance v0, Ljava/util/HashMap;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    sput-object v0, Lcom/google/android/music/utils/async/AsyncWorkers;->sUniqueMessageTypes:Ljava/util/HashMap;

    return-void
.end method

.method public static declared-synchronized getUniqueMessageType(Lcom/google/android/music/utils/LoggableHandler;)I
    .locals 3
    .param p0, "worker"    # Lcom/google/android/music/utils/LoggableHandler;

    .prologue
    .line 69
    const-class v2, Lcom/google/android/music/utils/async/AsyncWorkers;

    monitor-enter v2

    :try_start_0
    sget-object v1, Lcom/google/android/music/utils/async/AsyncWorkers;->sUniqueMessageTypes:Ljava/util/HashMap;

    invoke-virtual {v1, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/atomic/AtomicInteger;

    .line 70
    .local v0, "i":Ljava/util/concurrent/atomic/AtomicInteger;
    if-nez v0, :cond_0

    .line 71
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    .end local v0    # "i":Ljava/util/concurrent/atomic/AtomicInteger;
    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    .line 72
    .restart local v0    # "i":Ljava/util/concurrent/atomic/AtomicInteger;
    sget-object v1, Lcom/google/android/music/utils/async/AsyncWorkers;->sUniqueMessageTypes:Ljava/util/HashMap;

    invoke-virtual {v1, p0, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    :cond_0
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    monitor-exit v2

    return v1

    .line 69
    .end local v0    # "i":Ljava/util/concurrent/atomic/AtomicInteger;
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method

.method public static runAsync(Lcom/google/android/music/utils/LoggableHandler;ILjava/lang/Runnable;Z)V
    .locals 2
    .param p0, "worker"    # Lcom/google/android/music/utils/LoggableHandler;
    .param p1, "msgType"    # I
    .param p2, "r"    # Ljava/lang/Runnable;
    .param p3, "clearPrevious"    # Z

    .prologue
    .line 44
    if-eqz p3, :cond_0

    .line 45
    invoke-virtual {p0, p1}, Lcom/google/android/music/utils/LoggableHandler;->removeMessages(I)V

    .line 47
    :cond_0
    new-instance v1, Lcom/google/android/music/utils/async/TraceableRunnable;

    invoke-direct {v1, p2}, Lcom/google/android/music/utils/async/TraceableRunnable;-><init>(Ljava/lang/Runnable;)V

    invoke-static {p0, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;Ljava/lang/Runnable;)Landroid/os/Message;

    move-result-object v0

    .line 48
    .local v0, "msg":Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->what:I

    .line 49
    invoke-virtual {p0, v0}, Lcom/google/android/music/utils/LoggableHandler;->sendMessage(Landroid/os/Message;)Z

    .line 50
    return-void
.end method

.method public static runAsync(Lcom/google/android/music/utils/LoggableHandler;Ljava/lang/Runnable;)V
    .locals 1
    .param p0, "worker"    # Lcom/google/android/music/utils/LoggableHandler;
    .param p1, "r"    # Ljava/lang/Runnable;

    .prologue
    .line 38
    new-instance v0, Lcom/google/android/music/utils/async/TraceableRunnable;

    invoke-direct {v0, p1}, Lcom/google/android/music/utils/async/TraceableRunnable;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {p0, v0}, Lcom/google/android/music/utils/LoggableHandler;->post(Ljava/lang/Runnable;)Z

    .line 39
    return-void
.end method

.method public static runAsyncDelayed(Lcom/google/android/music/utils/LoggableHandler;Ljava/lang/Runnable;I)V
    .locals 4
    .param p0, "worker"    # Lcom/google/android/music/utils/LoggableHandler;
    .param p1, "r"    # Ljava/lang/Runnable;
    .param p2, "delay"    # I

    .prologue
    .line 53
    new-instance v0, Lcom/google/android/music/utils/async/TraceableRunnable;

    invoke-direct {v0, p1}, Lcom/google/android/music/utils/async/TraceableRunnable;-><init>(Ljava/lang/Runnable;)V

    int-to-long v2, p2

    invoke-virtual {p0, v0, v2, v3}, Lcom/google/android/music/utils/LoggableHandler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 54
    return-void
.end method

.method public static runAsyncWithCallback(Lcom/google/android/music/utils/LoggableHandler;Lcom/google/android/music/utils/async/AsyncRunner;)V
    .locals 2
    .param p0, "worker"    # Lcom/google/android/music/utils/LoggableHandler;
    .param p1, "runner"    # Lcom/google/android/music/utils/async/AsyncRunner;

    .prologue
    .line 58
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 60
    .local v0, "callbackHandler":Landroid/os/Handler;
    new-instance v1, Lcom/google/android/music/utils/async/CallbackRunnable;

    invoke-direct {v1, v0, p1}, Lcom/google/android/music/utils/async/CallbackRunnable;-><init>(Landroid/os/Handler;Lcom/google/android/music/utils/async/AsyncRunner;)V

    invoke-virtual {p0, v1}, Lcom/google/android/music/utils/LoggableHandler;->post(Ljava/lang/Runnable;)Z

    .line 61
    return-void
.end method
