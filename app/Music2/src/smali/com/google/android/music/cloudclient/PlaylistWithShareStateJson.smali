.class public Lcom/google/android/music/cloudclient/PlaylistWithShareStateJson;
.super Lcom/google/android/music/sync/google/model/SyncablePlaylist;
.source "PlaylistWithShareStateJson.java"


# instance fields
.field public mShareState:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "shareState"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/google/android/music/sync/google/model/SyncablePlaylist;-><init>()V

    .line 23
    const-string v0, "PRIVATE"

    iput-object v0, p0, Lcom/google/android/music/cloudclient/PlaylistWithShareStateJson;->mShareState:Ljava/lang/String;

    return-void
.end method

.method public static localShareStateToRemote(I)Ljava/lang/String;
    .locals 3
    .param p0, "localState"    # I

    .prologue
    .line 32
    const/4 v0, 0x2

    if-ne p0, v0, :cond_0

    .line 33
    const-string v0, "PUBLIC"

    .line 35
    :goto_0
    return-object v0

    .line 34
    :cond_0
    const/4 v0, 0x3

    if-ne p0, v0, :cond_1

    .line 35
    const-string v0, "PRIVATE"

    goto :goto_0

    .line 37
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
