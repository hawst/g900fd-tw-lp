.class Lcom/google/android/music/quiz/GenreQuizActivity$OnGenreSelectedListner;
.super Ljava/lang/Object;
.source "GenreQuizActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/quiz/GenreQuizActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "OnGenreSelectedListner"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/quiz/GenreQuizActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/music/quiz/GenreQuizActivity;)V
    .locals 0

    .prologue
    .line 213
    iput-object p1, p0, Lcom/google/android/music/quiz/GenreQuizActivity$OnGenreSelectedListner;->this$0:Lcom/google/android/music/quiz/GenreQuizActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/music/quiz/GenreQuizActivity;Lcom/google/android/music/quiz/GenreQuizActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/music/quiz/GenreQuizActivity;
    .param p2, "x1"    # Lcom/google/android/music/quiz/GenreQuizActivity$1;

    .prologue
    .line 213
    invoke-direct {p0, p1}, Lcom/google/android/music/quiz/GenreQuizActivity$OnGenreSelectedListner;-><init>(Lcom/google/android/music/quiz/GenreQuizActivity;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 217
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/quiz/GenreQuizActivity$GenreEntryViewHolder;

    iget-object v1, v2, Lcom/google/android/music/quiz/GenreQuizActivity$GenreEntryViewHolder;->mGenreId:Ljava/lang/String;

    .line 218
    .local v1, "selectedId":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/quiz/GenreQuizActivity$GenreEntryViewHolder;

    iget-object v0, v2, Lcom/google/android/music/quiz/GenreQuizActivity$GenreEntryViewHolder;->mArt:Lcom/google/android/music/widgets/ArtImageCheckbox;

    .line 219
    .local v0, "mArt":Lcom/google/android/music/widgets/ArtImageCheckbox;
    iget-object v2, p0, Lcom/google/android/music/quiz/GenreQuizActivity$OnGenreSelectedListner;->this$0:Lcom/google/android/music/quiz/GenreQuizActivity;

    # getter for: Lcom/google/android/music/quiz/GenreQuizActivity;->mSelectedGenreIds:Ljava/util/HashSet;
    invoke-static {v2}, Lcom/google/android/music/quiz/GenreQuizActivity;->access$600(Lcom/google/android/music/quiz/GenreQuizActivity;)Ljava/util/HashSet;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 220
    iget-object v2, p0, Lcom/google/android/music/quiz/GenreQuizActivity$OnGenreSelectedListner;->this$0:Lcom/google/android/music/quiz/GenreQuizActivity;

    # getter for: Lcom/google/android/music/quiz/GenreQuizActivity;->mSelectedGenreIds:Ljava/util/HashSet;
    invoke-static {v2}, Lcom/google/android/music/quiz/GenreQuizActivity;->access$600(Lcom/google/android/music/quiz/GenreQuizActivity;)Ljava/util/HashSet;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 221
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/google/android/music/widgets/ArtImageCheckbox;->setChecked(Z)V

    .line 226
    :goto_0
    return-void

    .line 223
    :cond_0
    iget-object v2, p0, Lcom/google/android/music/quiz/GenreQuizActivity$OnGenreSelectedListner;->this$0:Lcom/google/android/music/quiz/GenreQuizActivity;

    # getter for: Lcom/google/android/music/quiz/GenreQuizActivity;->mSelectedGenreIds:Ljava/util/HashSet;
    invoke-static {v2}, Lcom/google/android/music/quiz/GenreQuizActivity;->access$600(Lcom/google/android/music/quiz/GenreQuizActivity;)Ljava/util/HashSet;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 224
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/google/android/music/widgets/ArtImageCheckbox;->setChecked(Z)V

    goto :goto_0
.end method
