.class Lcom/google/android/music/leanback/LeanbackMyGenresActivity$1$1;
.super Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;
.source "LeanbackMyGenresActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/leanback/LeanbackMyGenresActivity$1;->bind(Landroid/database/Cursor;)Ljava/lang/Object;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/music/leanback/LeanbackMyGenresActivity$1;


# direct methods
.method constructor <init>(Lcom/google/android/music/leanback/LeanbackMyGenresActivity$1;Landroid/support/v17/leanback/widget/Presenter;)V
    .locals 0
    .param p2, "x0"    # Landroid/support/v17/leanback/widget/Presenter;

    .prologue
    .line 35
    iput-object p1, p0, Lcom/google/android/music/leanback/LeanbackMyGenresActivity$1$1;->this$1:Lcom/google/android/music/leanback/LeanbackMyGenresActivity$1;

    invoke-direct {p0, p2}, Lcom/google/android/music/leanback/LeanbackCursorObjectAdapter;-><init>(Landroid/support/v17/leanback/widget/Presenter;)V

    return-void
.end method


# virtual methods
.method protected bind(Landroid/database/Cursor;)Ljava/lang/Object;
    .locals 8
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 38
    iget-object v6, p0, Lcom/google/android/music/leanback/LeanbackMyGenresActivity$1$1;->this$1:Lcom/google/android/music/leanback/LeanbackMyGenresActivity$1;

    iget-object v3, v6, Lcom/google/android/music/leanback/LeanbackMyGenresActivity$1;->this$0:Lcom/google/android/music/leanback/LeanbackMyGenresActivity;

    .line 39
    .local v3, "context":Landroid/content/Context;
    const/4 v6, 0x0

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 40
    .local v0, "albumId":J
    const/4 v6, 0x1

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 41
    .local v5, "name":Ljava/lang/String;
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 42
    iget-object v6, p0, Lcom/google/android/music/leanback/LeanbackMyGenresActivity$1$1;->this$1:Lcom/google/android/music/leanback/LeanbackMyGenresActivity$1;

    iget-object v6, v6, Lcom/google/android/music/leanback/LeanbackMyGenresActivity$1;->this$0:Lcom/google/android/music/leanback/LeanbackMyGenresActivity;

    const v7, 0x7f0b00c5

    invoke-virtual {v6, v7}, Lcom/google/android/music/leanback/LeanbackMyGenresActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 44
    :cond_0
    const/4 v6, 0x2

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 45
    .local v2, "artist":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 46
    iget-object v6, p0, Lcom/google/android/music/leanback/LeanbackMyGenresActivity$1$1;->this$1:Lcom/google/android/music/leanback/LeanbackMyGenresActivity$1;

    iget-object v6, v6, Lcom/google/android/music/leanback/LeanbackMyGenresActivity$1;->this$0:Lcom/google/android/music/leanback/LeanbackMyGenresActivity;

    const v7, 0x7f0b00c4

    invoke-virtual {v6, v7}, Lcom/google/android/music/leanback/LeanbackMyGenresActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 49
    :cond_1
    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Lcom/google/android/music/leanback/LeanbackUtils;->getAlbumDetailsIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v4

    .line 52
    .local v4, "intent":Landroid/content/Intent;
    new-instance v6, Lcom/google/android/music/leanback/Item$Builder;

    invoke-direct {v6}, Lcom/google/android/music/leanback/Item$Builder;-><init>()V

    invoke-virtual {v6, v5}, Lcom/google/android/music/leanback/Item$Builder;->title(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v6

    invoke-virtual {v6, v2}, Lcom/google/android/music/leanback/Item$Builder;->description(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v6

    new-instance v7, Lcom/google/android/music/leanback/LeanbackMyGenresActivity$1$1$1;

    invoke-direct {v7, p0, v0, v1, v3}, Lcom/google/android/music/leanback/LeanbackMyGenresActivity$1$1$1;-><init>(Lcom/google/android/music/leanback/LeanbackMyGenresActivity$1$1;JLandroid/content/Context;)V

    invoke-virtual {v6, v7}, Lcom/google/android/music/leanback/Item$Builder;->iconUri(Lcom/google/android/music/leanback/Item$StringGetter;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v6

    invoke-virtual {v6, v4}, Lcom/google/android/music/leanback/Item$Builder;->intent(Landroid/content/Intent;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v6

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/google/android/music/leanback/Item$Builder;->id(Ljava/lang/String;)Lcom/google/android/music/leanback/Item$Builder;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/music/leanback/Item$Builder;->build()Lcom/google/android/music/leanback/Item;

    move-result-object v6

    return-object v6
.end method
