.class public abstract Lcom/google/android/music/ui/MediaListGridFragment;
.super Lcom/google/android/music/ui/GridFragment;
.source "MediaListGridFragment.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/music/ui/GridFragment;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field private mActivityCreated:Z

.field private mAdapter:Lcom/google/android/music/ui/MediaListCursorAdapter;

.field private mAsync:Z

.field protected final mCardsContextMenuDelegate:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$DocumentContextMenuDelegate;

.field private mClearedFromOnStop:Z

.field private mDataSetObserver:Landroid/database/DataSetObserver;

.field private mIsDataSetObserverRegistered:Z

.field private mMediaList:Lcom/google/android/music/medialist/MediaList;

.field private mProjection:[Ljava/lang/String;

.field private mShowBigCard:Z


# direct methods
.method protected constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 69
    invoke-direct {p0}, Lcom/google/android/music/ui/GridFragment;-><init>()V

    .line 40
    iput-boolean v1, p0, Lcom/google/android/music/ui/MediaListGridFragment;->mIsDataSetObserverRegistered:Z

    .line 41
    new-instance v0, Lcom/google/android/music/ui/MediaListGridFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/MediaListGridFragment$1;-><init>(Lcom/google/android/music/ui/MediaListGridFragment;)V

    iput-object v0, p0, Lcom/google/android/music/ui/MediaListGridFragment;->mDataSetObserver:Landroid/database/DataSetObserver;

    .line 51
    iput-boolean v1, p0, Lcom/google/android/music/ui/MediaListGridFragment;->mActivityCreated:Z

    .line 56
    iput-boolean v1, p0, Lcom/google/android/music/ui/MediaListGridFragment;->mClearedFromOnStop:Z

    .line 63
    new-instance v0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$DocumentContextMenuDelegate;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$DocumentContextMenuDelegate;-><init>(Lcom/google/android/music/ui/MusicFragment;)V

    iput-object v0, p0, Lcom/google/android/music/ui/MediaListGridFragment;->mCardsContextMenuDelegate:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$DocumentContextMenuDelegate;

    .line 70
    return-void
.end method

.method protected constructor <init>(Lcom/google/android/music/medialist/MediaList;[Ljava/lang/String;Z)V
    .locals 2
    .param p1, "mediaList"    # Lcom/google/android/music/medialist/MediaList;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "async"    # Z

    .prologue
    const/4 v1, 0x0

    .line 82
    invoke-direct {p0}, Lcom/google/android/music/ui/GridFragment;-><init>()V

    .line 40
    iput-boolean v1, p0, Lcom/google/android/music/ui/MediaListGridFragment;->mIsDataSetObserverRegistered:Z

    .line 41
    new-instance v0, Lcom/google/android/music/ui/MediaListGridFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/MediaListGridFragment$1;-><init>(Lcom/google/android/music/ui/MediaListGridFragment;)V

    iput-object v0, p0, Lcom/google/android/music/ui/MediaListGridFragment;->mDataSetObserver:Landroid/database/DataSetObserver;

    .line 51
    iput-boolean v1, p0, Lcom/google/android/music/ui/MediaListGridFragment;->mActivityCreated:Z

    .line 56
    iput-boolean v1, p0, Lcom/google/android/music/ui/MediaListGridFragment;->mClearedFromOnStop:Z

    .line 63
    new-instance v0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$DocumentContextMenuDelegate;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$DocumentContextMenuDelegate;-><init>(Lcom/google/android/music/ui/MusicFragment;)V

    iput-object v0, p0, Lcom/google/android/music/ui/MediaListGridFragment;->mCardsContextMenuDelegate:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$DocumentContextMenuDelegate;

    .line 83
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/music/ui/MediaListGridFragment;->init(Lcom/google/android/music/medialist/MediaList;[Ljava/lang/String;Z)V

    .line 84
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/ui/MediaListGridFragment;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/MediaListGridFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/google/android/music/ui/MediaListGridFragment;->updateEmptyScreenVisibility(Z)V

    return-void
.end method

.method private getGridAdapter(Lcom/google/android/music/ui/MediaListCursorAdapter;)Landroid/widget/ListAdapter;
    .locals 5
    .param p1, "listAdapter"    # Lcom/google/android/music/ui/MediaListCursorAdapter;

    .prologue
    .line 260
    invoke-virtual {p0}, Lcom/google/android/music/ui/MediaListGridFragment;->getScreenColumns()I

    move-result v1

    .line 263
    .local v1, "numColumns":I
    iget-boolean v3, p0, Lcom/google/android/music/ui/MediaListGridFragment;->mShowBigCard:Z

    if-eqz v3, :cond_0

    .line 265
    invoke-virtual {p0}, Lcom/google/android/music/ui/MediaListGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0d000e

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    .line 266
    .local v2, "numRows":I
    new-instance v0, Lcom/google/android/music/ui/GridAdapterWrapperWithHeader;

    invoke-virtual {p0}, Lcom/google/android/music/ui/MediaListGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-direct {v0, v3, p1, v1, v2}, Lcom/google/android/music/ui/GridAdapterWrapperWithHeader;-><init>(Landroid/content/Context;Landroid/widget/ListAdapter;II)V

    .line 273
    .end local v2    # "numRows":I
    .local v0, "gridAdapter":Landroid/widget/ListAdapter;
    :goto_0
    return-object v0

    .line 270
    .end local v0    # "gridAdapter":Landroid/widget/ListAdapter;
    :cond_0
    new-instance v0, Lcom/google/android/music/ui/GridAdapterWrapper;

    invoke-virtual {p0}, Lcom/google/android/music/ui/MediaListGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-direct {v0, v3, p1, v1}, Lcom/google/android/music/ui/GridAdapterWrapper;-><init>(Landroid/content/Context;Landroid/widget/ListAdapter;I)V

    .restart local v0    # "gridAdapter":Landroid/widget/ListAdapter;
    goto :goto_0
.end method

.method private initFromArgs()Z
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 157
    invoke-virtual {p0}, Lcom/google/android/music/ui/MediaListGridFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 158
    .local v0, "args":Landroid/os/Bundle;
    if-nez v0, :cond_1

    .line 178
    :cond_0
    :goto_0
    return v4

    .line 161
    :cond_1
    const-string v5, "list"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 162
    .local v1, "embrio":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 165
    invoke-static {v1}, Lcom/google/android/music/medialist/MediaList;->thaw(Ljava/lang/String;)Lcom/google/android/music/medialist/MediaList;

    move-result-object v2

    .line 166
    .local v2, "list":Lcom/google/android/music/medialist/MediaList;
    if-eqz v2, :cond_0

    .line 169
    const-string v5, "proj"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 170
    .local v3, "proj":[Ljava/lang/String;
    if-eqz v3, :cond_0

    array-length v5, v3

    if-eqz v5, :cond_0

    .line 173
    iput-object v2, p0, Lcom/google/android/music/ui/MediaListGridFragment;->mMediaList:Lcom/google/android/music/medialist/MediaList;

    .line 174
    iput-object v3, p0, Lcom/google/android/music/ui/MediaListGridFragment;->mProjection:[Ljava/lang/String;

    .line 175
    const-string v4, "async"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    iput-boolean v4, p0, Lcom/google/android/music/ui/MediaListGridFragment;->mAsync:Z

    .line 176
    const-string v4, "bigcard"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    iput-boolean v4, p0, Lcom/google/android/music/ui/MediaListGridFragment;->mShowBigCard:Z

    .line 178
    const/4 v4, 0x1

    goto :goto_0
.end method

.method private initializeGridView()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 212
    invoke-virtual {p0}, Lcom/google/android/music/ui/MediaListGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 213
    .local v0, "activity":Landroid/support/v4/app/FragmentActivity;
    iget-boolean v5, p0, Lcom/google/android/music/ui/MediaListGridFragment;->mAsync:Z

    if-eqz v5, :cond_1

    .line 216
    iget-object v5, p0, Lcom/google/android/music/ui/MediaListGridFragment;->mMediaList:Lcom/google/android/music/medialist/MediaList;

    iget-object v6, p0, Lcom/google/android/music/ui/MediaListGridFragment;->mProjection:[Ljava/lang/String;

    invoke-virtual {v5, v0, v6, v7}, Lcom/google/android/music/medialist/MediaList;->getCursor(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/music/medialist/MediaList$MediaCursor;

    move-result-object v2

    .line 217
    .local v2, "asyncCursor":Landroid/database/Cursor;
    invoke-virtual {p0, v2}, Lcom/google/android/music/ui/MediaListGridFragment;->newAsyncCursorAdapter(Landroid/database/Cursor;)Lcom/google/android/music/ui/MediaListCursorAdapter;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/music/ui/MediaListGridFragment;->mAdapter:Lcom/google/android/music/ui/MediaListCursorAdapter;

    .line 218
    iget-object v5, p0, Lcom/google/android/music/ui/MediaListGridFragment;->mAdapter:Lcom/google/android/music/ui/MediaListCursorAdapter;

    invoke-direct {p0, v5}, Lcom/google/android/music/ui/MediaListGridFragment;->getGridAdapter(Lcom/google/android/music/ui/MediaListCursorAdapter;)Landroid/widget/ListAdapter;

    move-result-object v3

    .line 219
    .local v3, "gridAdapter":Landroid/widget/ListAdapter;
    invoke-virtual {p0, v3}, Lcom/google/android/music/ui/MediaListGridFragment;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 228
    .end local v2    # "asyncCursor":Landroid/database/Cursor;
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/music/ui/MediaListGridFragment;->getListAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    .line 229
    .local v1, "adapter":Landroid/widget/ListAdapter;
    if-eqz v1, :cond_0

    iget-boolean v5, p0, Lcom/google/android/music/ui/MediaListGridFragment;->mIsDataSetObserverRegistered:Z

    if-nez v5, :cond_0

    .line 230
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/google/android/music/ui/MediaListGridFragment;->mIsDataSetObserverRegistered:Z

    .line 231
    iget-object v5, p0, Lcom/google/android/music/ui/MediaListGridFragment;->mDataSetObserver:Landroid/database/DataSetObserver;

    invoke-interface {v1, v5}, Landroid/widget/ListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 233
    :cond_0
    return-void

    .line 221
    .end local v1    # "adapter":Landroid/widget/ListAdapter;
    .end local v3    # "gridAdapter":Landroid/widget/ListAdapter;
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/music/ui/MediaListGridFragment;->newLoaderCursorAdapter()Lcom/google/android/music/ui/MediaListCursorAdapter;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/music/ui/MediaListGridFragment;->mAdapter:Lcom/google/android/music/ui/MediaListCursorAdapter;

    .line 222
    iget-object v5, p0, Lcom/google/android/music/ui/MediaListGridFragment;->mAdapter:Lcom/google/android/music/ui/MediaListCursorAdapter;

    invoke-direct {p0, v5}, Lcom/google/android/music/ui/MediaListGridFragment;->getGridAdapter(Lcom/google/android/music/ui/MediaListCursorAdapter;)Landroid/widget/ListAdapter;

    move-result-object v3

    .line 223
    .restart local v3    # "gridAdapter":Landroid/widget/ListAdapter;
    invoke-virtual {p0, v3}, Lcom/google/android/music/ui/MediaListGridFragment;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 224
    invoke-virtual {p0}, Lcom/google/android/music/ui/MediaListGridFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v4

    .line 225
    .local v4, "lm":Landroid/support/v4/app/LoaderManager;
    const/4 v5, 0x0

    invoke-virtual {v4, v5, v7, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    goto :goto_0
.end method

.method private updateEmptyScreenVisibility(Z)V
    .locals 2
    .param p1, "visible"    # Z

    .prologue
    .line 359
    invoke-virtual {p0, p1}, Lcom/google/android/music/ui/MediaListGridFragment;->setEmptyScreenVisible(Z)V

    .line 360
    invoke-virtual {p0}, Lcom/google/android/music/ui/MediaListGridFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    .line 361
    .local v0, "lv":Landroid/widget/ListView;
    if-eqz p1, :cond_0

    .line 362
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setFastScrollEnabled(Z)V

    .line 366
    :goto_0
    invoke-virtual {v0}, Landroid/widget/ListView;->invalidate()V

    .line 367
    return-void

    .line 364
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/music/ui/MediaListGridFragment;->getSupportsFastScroll()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setFastScrollEnabled(Z)V

    goto :goto_0
.end method


# virtual methods
.method public getFragmentMediaList()Lcom/google/android/music/medialist/MediaList;
    .locals 1

    .prologue
    .line 191
    iget-object v0, p0, Lcom/google/android/music/ui/MediaListGridFragment;->mMediaList:Lcom/google/android/music/medialist/MediaList;

    return-object v0
.end method

.method protected getMediaList()Lcom/google/android/music/medialist/MediaList;
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lcom/google/android/music/ui/MediaListGridFragment;->mMediaList:Lcom/google/android/music/medialist/MediaList;

    return-object v0
.end method

.method protected getSupportsFastScroll()Z
    .locals 1

    .prologue
    .line 91
    const/4 v0, 0x0

    return v0
.end method

.method protected init(Lcom/google/android/music/medialist/MediaList;[Ljava/lang/String;Z)V
    .locals 1
    .param p1, "mediaList"    # Lcom/google/android/music/medialist/MediaList;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "async"    # Z

    .prologue
    .line 107
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/google/android/music/ui/MediaListGridFragment;->init(Lcom/google/android/music/medialist/MediaList;[Ljava/lang/String;ZZ)V

    .line 108
    return-void
.end method

.method protected init(Lcom/google/android/music/medialist/MediaList;[Ljava/lang/String;ZZ)V
    .locals 2
    .param p1, "mediaList"    # Lcom/google/android/music/medialist/MediaList;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "async"    # Z
    .param p4, "showBigCard"    # Z

    .prologue
    .line 117
    iget-object v0, p0, Lcom/google/android/music/ui/MediaListGridFragment;->mMediaList:Lcom/google/android/music/medialist/MediaList;

    if-eqz v0, :cond_0

    .line 118
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Already initialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 121
    :cond_0
    iput-object p1, p0, Lcom/google/android/music/ui/MediaListGridFragment;->mMediaList:Lcom/google/android/music/medialist/MediaList;

    .line 122
    iput-object p2, p0, Lcom/google/android/music/ui/MediaListGridFragment;->mProjection:[Ljava/lang/String;

    .line 123
    iput-boolean p3, p0, Lcom/google/android/music/ui/MediaListGridFragment;->mAsync:Z

    .line 124
    iput-boolean p4, p0, Lcom/google/android/music/ui/MediaListGridFragment;->mShowBigCard:Z

    .line 126
    iget-boolean v0, p0, Lcom/google/android/music/ui/MediaListGridFragment;->mActivityCreated:Z

    if-eqz v0, :cond_1

    .line 128
    invoke-direct {p0}, Lcom/google/android/music/ui/MediaListGridFragment;->initializeGridView()V

    .line 130
    :cond_1
    return-void
.end method

.method protected isClearedFromOnStop()Z
    .locals 1

    .prologue
    .line 355
    iget-boolean v0, p0, Lcom/google/android/music/ui/MediaListGridFragment;->mClearedFromOnStop:Z

    return v0
.end method

.method protected isInitialized()Z
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lcom/google/android/music/ui/MediaListGridFragment;->mMediaList:Lcom/google/android/music/medialist/MediaList;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method abstract newAsyncCursorAdapter(Landroid/database/Cursor;)Lcom/google/android/music/ui/MediaListCursorAdapter;
.end method

.method abstract newLoaderCursorAdapter()Lcom/google/android/music/ui/MediaListCursorAdapter;
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 249
    invoke-super {p0, p1}, Lcom/google/android/music/ui/GridFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 251
    invoke-virtual {p0}, Lcom/google/android/music/ui/MediaListGridFragment;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 253
    invoke-direct {p0}, Lcom/google/android/music/ui/MediaListGridFragment;->initializeGridView()V

    .line 255
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/ui/MediaListGridFragment;->mActivityCreated:Z

    .line 256
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 96
    invoke-super {p0, p1}, Lcom/google/android/music/ui/GridFragment;->onCreate(Landroid/os/Bundle;)V

    .line 97
    invoke-virtual {p0}, Lcom/google/android/music/ui/MediaListGridFragment;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 98
    invoke-direct {p0}, Lcom/google/android/music/ui/MediaListGridFragment;->initFromArgs()Z

    .line 100
    :cond_0
    return-void
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 4
    .param p1, "id"    # I
    .param p2, "args"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 322
    iget-boolean v0, p0, Lcom/google/android/music/ui/MediaListGridFragment;->mAsync:Z

    if-eqz v0, :cond_0

    .line 323
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Attempt to use loader with async cursor"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 325
    :cond_0
    new-instance v0, Lcom/google/android/music/ui/MediaListCursorLoader;

    invoke-virtual {p0}, Lcom/google/android/music/ui/MediaListGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/MediaListGridFragment;->mMediaList:Lcom/google/android/music/medialist/MediaList;

    iget-object v3, p0, Lcom/google/android/music/ui/MediaListGridFragment;->mProjection:[Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/music/ui/MediaListCursorLoader;-><init>(Landroid/content/Context;Lcom/google/android/music/medialist/MediaList;[Ljava/lang/String;)V

    return-object v0
.end method

.method public onDestroyView()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 296
    iget-boolean v0, p0, Lcom/google/android/music/ui/MediaListGridFragment;->mIsDataSetObserverRegistered:Z

    if-eqz v0, :cond_0

    .line 297
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/ui/MediaListGridFragment;->mIsDataSetObserverRegistered:Z

    .line 298
    invoke-virtual {p0}, Lcom/google/android/music/ui/MediaListGridFragment;->getListAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/ui/MediaListGridFragment;->mDataSetObserver:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 303
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/music/ui/MediaListGridFragment;->mAsync:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/music/ui/MediaListGridFragment;->mAdapter:Lcom/google/android/music/ui/MediaListCursorAdapter;

    if-eqz v0, :cond_1

    .line 304
    iget-object v0, p0, Lcom/google/android/music/ui/MediaListGridFragment;->mAdapter:Lcom/google/android/music/ui/MediaListCursorAdapter;

    invoke-virtual {v0, v2}, Lcom/google/android/music/ui/MediaListCursorAdapter;->changeCursor(Landroid/database/Cursor;)V

    .line 312
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/music/ui/MediaListGridFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 315
    iget-object v0, p0, Lcom/google/android/music/ui/MediaListGridFragment;->mCardsContextMenuDelegate:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$DocumentContextMenuDelegate;

    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$DocumentContextMenuDelegate;->dismissContextMenu()V

    .line 316
    invoke-super {p0}, Lcom/google/android/music/ui/GridFragment;->onDestroyView()V

    .line 317
    return-void
.end method

.method public onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 2
    .param p2, "data"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 331
    .local p1, "loader":Landroid/support/v4/content/Loader;, "Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    iget-boolean v0, p0, Lcom/google/android/music/ui/MediaListGridFragment;->mAsync:Z

    if-eqz v0, :cond_0

    .line 332
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Attempt to use loader with async cursor"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 336
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/ui/MediaListGridFragment;->mAdapter:Lcom/google/android/music/ui/MediaListCursorAdapter;

    invoke-virtual {v0, p2}, Lcom/google/android/music/ui/MediaListCursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 337
    iget-object v0, p0, Lcom/google/android/music/ui/MediaListGridFragment;->mAdapter:Lcom/google/android/music/ui/MediaListCursorAdapter;

    invoke-virtual {v0}, Lcom/google/android/music/ui/MediaListCursorAdapter;->getCount()I

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/music/ui/MediaListGridFragment;->updateEmptyScreenVisibility(Z)V

    .line 338
    return-void

    .line 337
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Landroid/support/v4/content/Loader;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 26
    check-cast p2, Landroid/database/Cursor;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/music/ui/MediaListGridFragment;->onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 342
    .local p1, "loader":Landroid/support/v4/content/Loader;, "Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    iget-boolean v0, p0, Lcom/google/android/music/ui/MediaListGridFragment;->mAsync:Z

    if-eqz v0, :cond_0

    .line 343
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Attempt to use loader with async cursor"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 348
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/ui/MediaListGridFragment;->mAdapter:Lcom/google/android/music/ui/MediaListCursorAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/MediaListCursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 349
    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 278
    invoke-super {p0}, Lcom/google/android/music/ui/GridFragment;->onStart()V

    .line 279
    iget-boolean v0, p0, Lcom/google/android/music/ui/MediaListGridFragment;->mClearedFromOnStop:Z

    if-eqz v0, :cond_0

    .line 281
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/ui/MediaListGridFragment;->mClearedFromOnStop:Z

    .line 282
    invoke-virtual {p0}, Lcom/google/android/music/ui/MediaListGridFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->invalidateViews()V

    .line 284
    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 288
    invoke-super {p0}, Lcom/google/android/music/ui/GridFragment;->onStop()V

    .line 289
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/ui/MediaListGridFragment;->mClearedFromOnStop:Z

    .line 290
    invoke-virtual {p0}, Lcom/google/android/music/ui/MediaListGridFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/music/ui/cardlib/utils/Utils;->clearPlayCardThumbnails(Landroid/view/ViewGroup;)V

    .line 291
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 237
    invoke-super {p0, p1, p2}, Lcom/google/android/music/ui/GridFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 240
    invoke-virtual {p0}, Lcom/google/android/music/ui/MediaListGridFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    .line 241
    .local v0, "lv":Landroid/widget/ListView;
    invoke-virtual {p0}, Lcom/google/android/music/ui/MediaListGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f00c5

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 243
    .local v1, "padding":I
    invoke-virtual {v0, v1, v1, v1, v1}, Landroid/widget/ListView;->setPadding(IIII)V

    .line 244
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setClipToPadding(Z)V

    .line 245
    return-void
.end method

.method protected saveMediaListAsArguments()V
    .locals 3

    .prologue
    .line 142
    iget-object v1, p0, Lcom/google/android/music/ui/MediaListGridFragment;->mMediaList:Lcom/google/android/music/medialist/MediaList;

    if-nez v1, :cond_0

    .line 154
    :goto_0
    return-void

    .line 144
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/music/ui/MediaListGridFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 145
    .local v0, "args":Landroid/os/Bundle;
    if-nez v0, :cond_1

    .line 146
    new-instance v0, Landroid/os/Bundle;

    .end local v0    # "args":Landroid/os/Bundle;
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 149
    .restart local v0    # "args":Landroid/os/Bundle;
    :cond_1
    const-string v1, "list"

    iget-object v2, p0, Lcom/google/android/music/ui/MediaListGridFragment;->mMediaList:Lcom/google/android/music/medialist/MediaList;

    invoke-virtual {v2}, Lcom/google/android/music/medialist/MediaList;->freeze()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    const-string v1, "proj"

    iget-object v2, p0, Lcom/google/android/music/ui/MediaListGridFragment;->mProjection:[Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 151
    const-string v1, "async"

    iget-boolean v2, p0, Lcom/google/android/music/ui/MediaListGridFragment;->mAsync:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 152
    const-string v1, "bigcard"

    iget-boolean v2, p0, Lcom/google/android/music/ui/MediaListGridFragment;->mShowBigCard:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 153
    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/MediaListGridFragment;->setArguments(Landroid/os/Bundle;)V

    goto :goto_0
.end method
