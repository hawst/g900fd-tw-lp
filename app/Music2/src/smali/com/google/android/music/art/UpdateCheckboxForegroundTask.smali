.class public Lcom/google/android/music/art/UpdateCheckboxForegroundTask;
.super Lcom/google/android/music/art/UpdateForegroundTask;
.source "UpdateCheckboxForegroundTask.java"


# instance fields
.field private final mOverlayColor:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/music/art/UpdateForegroundTask$Callback;Lcom/google/android/music/art/ArtRequest;ZIIII)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "owner"    # Lcom/google/android/music/art/UpdateForegroundTask$Callback;
    .param p4, "cropToCircle"    # Z
    .param p5, "overlayColor"    # I
    .param p6, "defaultArtId"    # I
    .param p7, "width"    # I
    .param p8, "height"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/music/art/UpdateForegroundTask$Callback;",
            "Lcom/google/android/music/art/ArtRequest",
            "<*>;ZIIII)V"
        }
    .end annotation

    .prologue
    .line 28
    .local p3, "artRequest":Lcom/google/android/music/art/ArtRequest;, "Lcom/google/android/music/art/ArtRequest<*>;"
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p6

    move v6, p7

    move/from16 v7, p8

    invoke-direct/range {v0 .. v7}, Lcom/google/android/music/art/UpdateForegroundTask;-><init>(Landroid/content/Context;Lcom/google/android/music/art/UpdateForegroundTask$Callback;Lcom/google/android/music/art/ArtRequest;ZIII)V

    .line 29
    iput p5, p0, Lcom/google/android/music/art/UpdateCheckboxForegroundTask;->mOverlayColor:I

    .line 30
    return-void
.end method

.method private createStateListDrawable(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)Landroid/graphics/drawable/StateListDrawable;
    .locals 6
    .param p1, "background"    # Landroid/graphics/Bitmap;
    .param p2, "overlay"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v5, 0x0

    .line 95
    new-instance v2, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v2}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 97
    .local v2, "drawable":Landroid/graphics/drawable/StateListDrawable;
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v0, p1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    .line 98
    .local v0, "bgDrawable":Landroid/graphics/drawable/BitmapDrawable;
    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v1, p2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    .line 99
    .local v1, "checkedDrawable":Landroid/graphics/drawable/BitmapDrawable;
    iget v3, p0, Lcom/google/android/music/art/UpdateCheckboxForegroundTask;->mWidth:I

    iget v4, p0, Lcom/google/android/music/art/UpdateCheckboxForegroundTask;->mHeight:I

    invoke-virtual {v2, v5, v5, v3, v4}, Landroid/graphics/drawable/StateListDrawable;->setBounds(IIII)V

    .line 100
    const/4 v3, 0x2

    new-array v3, v3, [I

    fill-array-data v3, :array_0

    invoke-virtual {v2, v3, v1}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 102
    sget-object v3, Landroid/util/StateSet;->WILD_CARD:[I

    invoke-virtual {v2, v3, v0}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 103
    return-object v2

    .line 100
    :array_0
    .array-data 4
        0x10100a0
        0x101009e
    .end array-data
.end method

.method private makeDrawable(Landroid/graphics/Bitmap;I)Landroid/graphics/drawable/StateListDrawable;
    .locals 18
    .param p1, "art"    # Landroid/graphics/Bitmap;
    .param p2, "color"    # I

    .prologue
    .line 56
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/music/art/UpdateCheckboxForegroundTask;->mWidth:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/music/art/UpdateCheckboxForegroundTask;->mHeight:I

    move-object/from16 v0, p1

    invoke-static {v0, v2, v3}, Lcom/google/android/music/art/ArtRenderingUtils;->sliceBitmapSectionAndScale(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 59
    .local v7, "background":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/music/art/UpdateCheckboxForegroundTask;->mWidth:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/music/art/UpdateCheckboxForegroundTask;->mHeight:I

    sget-object v4, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v15

    .line 60
    .local v15, "overlay":Landroid/graphics/Bitmap;
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v15}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 61
    .local v1, "canvas":Landroid/graphics/Canvas;
    new-instance v6, Landroid/graphics/Paint;

    invoke-direct {v6}, Landroid/graphics/Paint;-><init>()V

    .line 62
    .local v6, "paint":Landroid/graphics/Paint;
    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, v7, v2, v3, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 64
    new-instance v6, Landroid/graphics/Paint;

    .end local v6    # "paint":Landroid/graphics/Paint;
    invoke-direct {v6}, Landroid/graphics/Paint;-><init>()V

    .line 65
    .restart local v6    # "paint":Landroid/graphics/Paint;
    move/from16 v0, p2

    invoke-virtual {v6, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 66
    const/16 v2, 0xe6

    invoke-virtual {v6, v2}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 67
    const/4 v2, 0x0

    invoke-virtual {v6, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 68
    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v6, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 69
    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v5

    int-to-float v5, v5

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 70
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/music/art/UpdateCheckboxForegroundTask;->mCropToCircle:Z

    if-eqz v2, :cond_1

    const/16 v9, 0x12e

    .line 72
    .local v9, "checkmarkAsset":I
    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/art/UpdateCheckboxForegroundTask;->mContext:Landroid/content/Context;

    invoke-static {v2, v9}, Lcom/google/android/music/art/StaticArt;->getArt(Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 74
    .local v8, "checkmark":Landroid/graphics/Bitmap;
    invoke-virtual {v15}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    sub-int v13, v2, v3

    .line 75
    .local v13, "dh":I
    invoke-virtual {v15}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    sub-int v14, v2, v3

    .line 76
    .local v14, "dw":I
    new-instance v12, Landroid/graphics/Rect;

    div-int/lit8 v2, v14, 0x2

    div-int/lit8 v3, v13, 0x2

    invoke-virtual {v15}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    div-int/lit8 v5, v14, 0x2

    sub-int/2addr v4, v5

    invoke-virtual {v15}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    div-int/lit8 v17, v13, 0x2

    sub-int v5, v5, v17

    invoke-direct {v12, v2, v3, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 78
    .local v12, "destRect":Landroid/graphics/Rect;
    new-instance v16, Landroid/graphics/Rect;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    move-object/from16 v0, v16

    invoke-direct {v0, v2, v3, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 79
    .local v16, "srcRect":Landroid/graphics/Rect;
    const/4 v2, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v1, v8, v0, v12, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 82
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/music/art/UpdateCheckboxForegroundTask;->mCropToCircle:Z

    if-eqz v2, :cond_0

    .line 83
    invoke-static {v7}, Lcom/google/android/music/art/ArtRenderingUtils;->cropToCircle(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v10

    .line 84
    .local v10, "croppedBackground":Landroid/graphics/Bitmap;
    invoke-virtual {v7}, Landroid/graphics/Bitmap;->recycle()V

    .line 85
    move-object v7, v10

    .line 86
    invoke-static {v15}, Lcom/google/android/music/art/ArtRenderingUtils;->cropToCircle(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v11

    .line 87
    .local v11, "croppedOverlay":Landroid/graphics/Bitmap;
    invoke-virtual {v15}, Landroid/graphics/Bitmap;->recycle()V

    .line 88
    move-object v15, v11

    .line 90
    .end local v10    # "croppedBackground":Landroid/graphics/Bitmap;
    .end local v11    # "croppedOverlay":Landroid/graphics/Bitmap;
    :cond_0
    move-object/from16 v0, p0

    invoke-direct {v0, v7, v15}, Lcom/google/android/music/art/UpdateCheckboxForegroundTask;->createStateListDrawable(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v2

    return-object v2

    .line 70
    .end local v8    # "checkmark":Landroid/graphics/Bitmap;
    .end local v9    # "checkmarkAsset":I
    .end local v12    # "destRect":Landroid/graphics/Rect;
    .end local v13    # "dh":I
    .end local v14    # "dw":I
    .end local v16    # "srcRect":Landroid/graphics/Rect;
    :cond_1
    const/16 v9, 0x12d

    goto :goto_0
.end method


# virtual methods
.method protected renderForeground()Landroid/graphics/drawable/Drawable;
    .locals 4

    .prologue
    .line 34
    const/4 v1, 0x0

    .line 35
    .local v1, "result":Landroid/graphics/Bitmap;
    iget-object v2, p0, Lcom/google/android/music/art/UpdateCheckboxForegroundTask;->mArtRequest:Lcom/google/android/music/art/ArtRequest;

    iget-object v2, v2, Lcom/google/android/music/art/ArtRequest;->mImages:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 36
    iget-object v2, p0, Lcom/google/android/music/art/UpdateCheckboxForegroundTask;->mArtRequest:Lcom/google/android/music/art/ArtRequest;

    iget-object v2, v2, Lcom/google/android/music/art/ArtRequest;->mImages:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "result":Landroid/graphics/Bitmap;
    check-cast v1, Landroid/graphics/Bitmap;

    .line 42
    .restart local v1    # "result":Landroid/graphics/Bitmap;
    :cond_0
    :goto_0
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v2

    if-nez v2, :cond_2

    .line 43
    iget v2, p0, Lcom/google/android/music/art/UpdateCheckboxForegroundTask;->mOverlayColor:I

    invoke-direct {p0, v1, v2}, Lcom/google/android/music/art/UpdateCheckboxForegroundTask;->makeDrawable(Landroid/graphics/Bitmap;I)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v0

    .line 46
    :goto_1
    return-object v0

    .line 38
    :cond_1
    iget v2, p0, Lcom/google/android/music/art/UpdateCheckboxForegroundTask;->mDefaultArtId:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    .line 39
    iget-object v2, p0, Lcom/google/android/music/art/UpdateCheckboxForegroundTask;->mContext:Landroid/content/Context;

    iget v3, p0, Lcom/google/android/music/art/UpdateCheckboxForegroundTask;->mDefaultArtId:I

    invoke-static {v2, v3}, Lcom/google/android/music/art/StaticArt;->getArt(Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object v1

    goto :goto_0

    .line 46
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method
