.class Lcom/google/android/music/ui/ModifyPlaylistFragment$2;
.super Ljava/lang/Object;
.source "ModifyPlaylistFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/ModifyPlaylistFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/ModifyPlaylistFragment;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/ModifyPlaylistFragment;)V
    .locals 0

    .prologue
    .line 257
    iput-object p1, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment$2;->this$0:Lcom/google/android/music/ui/ModifyPlaylistFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 1
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 261
    iget-object v0, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment$2;->this$0:Lcom/google/android/music/ui/ModifyPlaylistFragment;

    # getter for: Lcom/google/android/music/ui/ModifyPlaylistFragment;->mIsCreate:Z
    invoke-static {v0}, Lcom/google/android/music/ui/ModifyPlaylistFragment;->access$400(Lcom/google/android/music/ui/ModifyPlaylistFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 262
    iget-object v0, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment$2;->this$0:Lcom/google/android/music/ui/ModifyPlaylistFragment;

    # invokes: Lcom/google/android/music/ui/ModifyPlaylistFragment;->handleCreate()V
    invoke-static {v0}, Lcom/google/android/music/ui/ModifyPlaylistFragment;->access$500(Lcom/google/android/music/ui/ModifyPlaylistFragment;)V

    .line 266
    :goto_0
    iget-object v0, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment$2;->this$0:Lcom/google/android/music/ui/ModifyPlaylistFragment;

    invoke-virtual {v0}, Lcom/google/android/music/ui/ModifyPlaylistFragment;->dismiss()V

    .line 267
    return-void

    .line 264
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/ui/ModifyPlaylistFragment$2;->this$0:Lcom/google/android/music/ui/ModifyPlaylistFragment;

    # invokes: Lcom/google/android/music/ui/ModifyPlaylistFragment;->handleEdit()V
    invoke-static {v0}, Lcom/google/android/music/ui/ModifyPlaylistFragment;->access$600(Lcom/google/android/music/ui/ModifyPlaylistFragment;)V

    goto :goto_0
.end method
