.class Lcom/google/android/music/quiz/GenreQuizActivity$2;
.super Ljava/lang/Object;
.source "GenreQuizActivity.java"

# interfaces
.implements Lcom/google/android/music/utils/async/AsyncRunner;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/quiz/GenreQuizActivity;->fetchGenreList()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field mLocalResponse:Lcom/google/android/music/cloudclient/QuizGenresResponseJson;

.field final synthetic this$0:Lcom/google/android/music/quiz/GenreQuizActivity;

.field final synthetic val$client:Lcom/google/android/music/cloudclient/MusicCloud;


# direct methods
.method constructor <init>(Lcom/google/android/music/quiz/GenreQuizActivity;Lcom/google/android/music/cloudclient/MusicCloud;)V
    .locals 0

    .prologue
    .line 102
    iput-object p1, p0, Lcom/google/android/music/quiz/GenreQuizActivity$2;->this$0:Lcom/google/android/music/quiz/GenreQuizActivity;

    iput-object p2, p0, Lcom/google/android/music/quiz/GenreQuizActivity$2;->val$client:Lcom/google/android/music/cloudclient/MusicCloud;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public backgroundTask()V
    .locals 3

    .prologue
    .line 109
    :try_start_0
    iget-object v1, p0, Lcom/google/android/music/quiz/GenreQuizActivity$2;->val$client:Lcom/google/android/music/cloudclient/MusicCloud;

    invoke-interface {v1}, Lcom/google/android/music/cloudclient/MusicCloud;->getUserQuizGenresList()Lcom/google/android/music/cloudclient/QuizGenresResponseJson;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/quiz/GenreQuizActivity$2;->mLocalResponse:Lcom/google/android/music/cloudclient/QuizGenresResponseJson;

    .line 110
    iget-object v1, p0, Lcom/google/android/music/quiz/GenreQuizActivity$2;->mLocalResponse:Lcom/google/android/music/cloudclient/QuizGenresResponseJson;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/music/quiz/GenreQuizActivity$2;->mLocalResponse:Lcom/google/android/music/cloudclient/QuizGenresResponseJson;

    iget-object v1, v1, Lcom/google/android/music/cloudclient/QuizGenresResponseJson;->mGenres:Ljava/util/List;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/music/quiz/GenreQuizActivity$2;->mLocalResponse:Lcom/google/android/music/cloudclient/QuizGenresResponseJson;

    iget-object v1, v1, Lcom/google/android/music/cloudclient/QuizGenresResponseJson;->mGenres:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/music/quiz/GenreQuizActivity$2;->mLocalResponse:Lcom/google/android/music/cloudclient/QuizGenresResponseJson;

    iget-object v1, v1, Lcom/google/android/music/cloudclient/QuizGenresResponseJson;->mGenres:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/cloudclient/QuizGenreJson;

    iget-object v1, v1, Lcom/google/android/music/cloudclient/QuizGenreJson;->mType:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 113
    :cond_0
    const-string v1, "MusicQuiz"

    const-string v2, "Incomplete data"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 121
    :cond_1
    :goto_0
    return-void

    .line 116
    :catch_0
    move-exception v0

    .line 117
    .local v0, "e":Ljava/lang/InterruptedException;
    const-string v1, "MusicQuiz"

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 118
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catch_1
    move-exception v0

    .line 119
    .local v0, "e":Ljava/io/IOException;
    const-string v1, "MusicQuiz"

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public taskCompleted()V
    .locals 2

    .prologue
    .line 125
    iget-object v0, p0, Lcom/google/android/music/quiz/GenreQuizActivity$2;->this$0:Lcom/google/android/music/quiz/GenreQuizActivity;

    invoke-virtual {v0}, Lcom/google/android/music/quiz/GenreQuizActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/quiz/GenreQuizActivity$2;->this$0:Lcom/google/android/music/quiz/GenreQuizActivity;

    invoke-virtual {v0}, Lcom/google/android/music/quiz/GenreQuizActivity;->isActivityDestroyed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 130
    :cond_0
    :goto_0
    return-void

    .line 128
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/quiz/GenreQuizActivity$2;->this$0:Lcom/google/android/music/quiz/GenreQuizActivity;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/music/quiz/GenreQuizActivity;->setLoadingProgressVisibility(I)V

    .line 129
    iget-object v0, p0, Lcom/google/android/music/quiz/GenreQuizActivity$2;->this$0:Lcom/google/android/music/quiz/GenreQuizActivity;

    iget-object v1, p0, Lcom/google/android/music/quiz/GenreQuizActivity$2;->mLocalResponse:Lcom/google/android/music/cloudclient/QuizGenresResponseJson;

    # invokes: Lcom/google/android/music/quiz/GenreQuizActivity;->initAdapter(Lcom/google/android/music/cloudclient/QuizGenresResponseJson;)V
    invoke-static {v0, v1}, Lcom/google/android/music/quiz/GenreQuizActivity;->access$100(Lcom/google/android/music/quiz/GenreQuizActivity;Lcom/google/android/music/cloudclient/QuizGenresResponseJson;)V

    goto :goto_0
.end method
