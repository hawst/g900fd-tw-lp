.class Lcom/google/android/music/KeepOnView$4;
.super Ljava/lang/Object;
.source "KeepOnView.java"

# interfaces
.implements Lcom/google/android/music/utils/async/AsyncRunner;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/KeepOnView;->setDocumentType()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

.field private final mList:Lcom/google/android/music/medialist/SongList;

.field final synthetic this$0:Lcom/google/android/music/KeepOnView;


# direct methods
.method constructor <init>(Lcom/google/android/music/KeepOnView;)V
    .locals 1

    .prologue
    .line 296
    iput-object p1, p0, Lcom/google/android/music/KeepOnView$4;->this$0:Lcom/google/android/music/KeepOnView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 298
    iget-object v0, p0, Lcom/google/android/music/KeepOnView$4;->this$0:Lcom/google/android/music/KeepOnView;

    # getter for: Lcom/google/android/music/KeepOnView;->mSongList:Lcom/google/android/music/medialist/SongList;
    invoke-static {v0}, Lcom/google/android/music/KeepOnView;->access$100(Lcom/google/android/music/KeepOnView;)Lcom/google/android/music/medialist/SongList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/KeepOnView$4;->mList:Lcom/google/android/music/medialist/SongList;

    .line 299
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/KeepOnView$4;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    return-void
.end method


# virtual methods
.method public backgroundTask()V
    .locals 2

    .prologue
    .line 306
    iget-object v0, p0, Lcom/google/android/music/KeepOnView$4;->mList:Lcom/google/android/music/medialist/SongList;

    if-eqz v0, :cond_0

    .line 307
    iget-object v0, p0, Lcom/google/android/music/KeepOnView$4;->this$0:Lcom/google/android/music/KeepOnView;

    invoke-virtual {v0}, Lcom/google/android/music/KeepOnView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/KeepOnView$4;->mList:Lcom/google/android/music/medialist/SongList;

    invoke-static {v0, v1}, Lcom/google/android/music/ui/cardlib/model/Document;->fromSongList(Landroid/content/Context;Lcom/google/android/music/medialist/SongList;)Lcom/google/android/music/ui/cardlib/model/Document;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/KeepOnView$4;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    .line 309
    :cond_0
    return-void
.end method

.method public taskCompleted()V
    .locals 2

    .prologue
    .line 313
    iget-object v0, p0, Lcom/google/android/music/KeepOnView$4;->mList:Lcom/google/android/music/medialist/SongList;

    iget-object v1, p0, Lcom/google/android/music/KeepOnView$4;->this$0:Lcom/google/android/music/KeepOnView;

    # getter for: Lcom/google/android/music/KeepOnView;->mSongList:Lcom/google/android/music/medialist/SongList;
    invoke-static {v1}, Lcom/google/android/music/KeepOnView;->access$100(Lcom/google/android/music/KeepOnView;)Lcom/google/android/music/medialist/SongList;

    move-result-object v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/music/KeepOnView$4;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    if-eqz v0, :cond_0

    .line 314
    iget-object v0, p0, Lcom/google/android/music/KeepOnView$4;->this$0:Lcom/google/android/music/KeepOnView;

    iget-object v1, p0, Lcom/google/android/music/KeepOnView$4;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v1}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v1

    # setter for: Lcom/google/android/music/KeepOnView;->mDocumentType:Lcom/google/android/music/ui/cardlib/model/Document$Type;
    invoke-static {v0, v1}, Lcom/google/android/music/KeepOnView;->access$302(Lcom/google/android/music/KeepOnView;Lcom/google/android/music/ui/cardlib/model/Document$Type;)Lcom/google/android/music/ui/cardlib/model/Document$Type;

    .line 316
    :cond_0
    return-void
.end method
