.class public Lcom/google/android/music/ui/HomeActivity;
.super Lcom/google/android/music/ui/BaseActivity;
.source "HomeActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/ui/HomeActivity$5;,
        Lcom/google/android/music/ui/HomeActivity$Screen;
    }
.end annotation


# static fields
.field private static final DEFAULT_SCREEN:Lcom/google/android/music/ui/HomeActivity$Screen;


# instance fields
.field private mActivityStarted:Z

.field private mContentObserver:Landroid/database/ContentObserver;

.field private mContentObserverRegistered:Z

.field private mCurrentScreen:Lcom/google/android/music/ui/HomeActivity$Screen;

.field private mNautilusStatus:Lcom/google/android/music/NautilusStatus;

.field private mSyncObserver:Landroid/content/SyncStatusObserver;

.field private mSyncObserverHandle:Ljava/lang/Object;

.field private mTargetScreen:Lcom/google/android/music/ui/HomeActivity$Screen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lcom/google/android/music/ui/HomeActivity$Screen;->MAINSTAGE:Lcom/google/android/music/ui/HomeActivity$Screen;

    sput-object v0, Lcom/google/android/music/ui/HomeActivity;->DEFAULT_SCREEN:Lcom/google/android/music/ui/HomeActivity$Screen;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 43
    invoke-direct {p0}, Lcom/google/android/music/ui/BaseActivity;-><init>()V

    .line 49
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/ui/HomeActivity;->mSyncObserverHandle:Ljava/lang/Object;

    .line 287
    iput-boolean v1, p0, Lcom/google/android/music/ui/HomeActivity;->mContentObserverRegistered:Z

    .line 288
    iput-boolean v1, p0, Lcom/google/android/music/ui/HomeActivity;->mActivityStarted:Z

    .line 537
    new-instance v0, Lcom/google/android/music/ui/HomeActivity$2;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/google/android/music/ui/HomeActivity$2;-><init>(Lcom/google/android/music/ui/HomeActivity;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/google/android/music/ui/HomeActivity;->mContentObserver:Landroid/database/ContentObserver;

    .line 544
    new-instance v0, Lcom/google/android/music/ui/HomeActivity$3;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/HomeActivity$3;-><init>(Lcom/google/android/music/ui/HomeActivity;)V

    iput-object v0, p0, Lcom/google/android/music/ui/HomeActivity;->mSyncObserver:Landroid/content/SyncStatusObserver;

    return-void
.end method

.method static synthetic access$000()Lcom/google/android/music/ui/HomeActivity$Screen;
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lcom/google/android/music/ui/HomeActivity;->DEFAULT_SCREEN:Lcom/google/android/music/ui/HomeActivity$Screen;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/music/ui/HomeActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/HomeActivity;

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/google/android/music/ui/HomeActivity;->updateHasAudioAndSyncState()V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/music/ui/HomeActivity;Lcom/google/android/music/ui/HomeActivity$Screen;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/HomeActivity;
    .param p1, "x1"    # Lcom/google/android/music/ui/HomeActivity$Screen;

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lcom/google/android/music/ui/HomeActivity;->showSingleFragmentScreen(Lcom/google/android/music/ui/HomeActivity$Screen;)V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/music/ui/HomeActivity;)Lcom/google/android/music/NautilusStatus;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/HomeActivity;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/music/ui/HomeActivity;->mNautilusStatus:Lcom/google/android/music/NautilusStatus;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/music/ui/HomeActivity;)Lcom/google/android/music/ui/HomeActivity$Screen;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/HomeActivity;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/music/ui/HomeActivity;->mTargetScreen:Lcom/google/android/music/ui/HomeActivity$Screen;

    return-object v0
.end method

.method public static createHomeScreenIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 612
    invoke-static {p0}, Lcom/google/android/music/ui/HomeActivity;->newHomeActivityIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 613
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 614
    return-object v0
.end method

.method public static createHomeScreenIntent(Landroid/content/Context;Lcom/google/android/music/ui/HomeActivity$Screen;)Landroid/content/Intent;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "target"    # Lcom/google/android/music/ui/HomeActivity$Screen;

    .prologue
    .line 618
    invoke-static {p0}, Lcom/google/android/music/ui/HomeActivity;->createHomeScreenIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "music:home:screen"

    invoke-virtual {p1}, Lcom/google/android/music/ui/HomeActivity$Screen;->getTag()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static newHomeActivityIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 606
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 607
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.android.music.activitymanagement.TopLevelActivity"

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    .line 608
    return-object v0
.end method

.method private prepareScreenChange(Lcom/google/android/music/ui/HomeActivity$Screen;)V
    .locals 3
    .param p1, "newScreen"    # Lcom/google/android/music/ui/HomeActivity$Screen;

    .prologue
    const/4 v2, 0x1

    .line 528
    invoke-virtual {p0}, Lcom/google/android/music/ui/HomeActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    .line 529
    .local v0, "actionBar":Landroid/support/v7/app/ActionBar;
    invoke-virtual {v0, v2}, Landroid/support/v7/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 530
    iput-object p1, p0, Lcom/google/android/music/ui/HomeActivity;->mCurrentScreen:Lcom/google/android/music/ui/HomeActivity$Screen;

    .line 531
    invoke-virtual {p0}, Lcom/google/android/music/ui/HomeActivity;->getTVMenuView()Landroid/widget/ListView;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 532
    invoke-virtual {p0}, Lcom/google/android/music/ui/HomeActivity;->getTVMenuView()Landroid/widget/ListView;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/google/android/music/ui/TvHomeMenu;->selectScreen(Lcom/google/android/music/ui/HomeActivity$Screen;Landroid/widget/ListView;)V

    .line 534
    :cond_0
    # getter for: Lcom/google/android/music/ui/HomeActivity$Screen;->mTitleResId:I
    invoke-static {p1}, Lcom/google/android/music/ui/HomeActivity$Screen;->access$200(Lcom/google/android/music/ui/HomeActivity$Screen;)I

    move-result v1

    invoke-virtual {p0, v1, v2}, Lcom/google/android/music/ui/HomeActivity;->setActionBarTitle(IZ)V

    .line 535
    return-void
.end method

.method private registerMusicEventListeners()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 393
    iget-boolean v0, p0, Lcom/google/android/music/ui/HomeActivity;->mContentObserverRegistered:Z

    if-nez v0, :cond_0

    .line 394
    invoke-virtual {p0}, Lcom/google/android/music/ui/HomeActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/music/store/MusicContent;->CONTENT_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/music/ui/HomeActivity;->mContentObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 396
    iput-boolean v3, p0, Lcom/google/android/music/ui/HomeActivity;->mContentObserverRegistered:Z

    .line 398
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/ui/HomeActivity;->mSyncObserverHandle:Ljava/lang/Object;

    if-nez v0, :cond_1

    .line 399
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/music/ui/HomeActivity;->mSyncObserver:Landroid/content/SyncStatusObserver;

    invoke-static {v0, v1}, Landroid/content/ContentResolver;->addStatusChangeListener(ILandroid/content/SyncStatusObserver;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/HomeActivity;->mSyncObserverHandle:Ljava/lang/Object;

    .line 402
    :cond_1
    invoke-direct {p0}, Lcom/google/android/music/ui/HomeActivity;->updateHasAudioAndSyncState()V

    .line 403
    return-void
.end method

.method private showSingleFragmentScreen(Lcom/google/android/music/ui/HomeActivity$Screen;)V
    .locals 5
    .param p1, "screen"    # Lcom/google/android/music/ui/HomeActivity$Screen;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 500
    invoke-virtual {p0}, Lcom/google/android/music/ui/HomeActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 501
    .local v0, "fm":Landroid/support/v4/app/FragmentManager;
    invoke-virtual {p1}, Lcom/google/android/music/ui/HomeActivity$Screen;->getTag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    if-nez v1, :cond_1

    .line 502
    invoke-direct {p0, p1}, Lcom/google/android/music/ui/HomeActivity;->prepareScreenChange(Lcom/google/android/music/ui/HomeActivity$Screen;)V

    .line 503
    # invokes: Lcom/google/android/music/ui/HomeActivity$Screen;->createFragment(Landroid/content/Context;)Landroid/support/v4/app/Fragment;
    invoke-static {p1, p0}, Lcom/google/android/music/ui/HomeActivity$Screen;->access$100(Lcom/google/android/music/ui/HomeActivity$Screen;Landroid/content/Context;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/music/ui/HomeActivity$Screen;->getTag()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v4, v2}, Lcom/google/android/music/ui/HomeActivity;->replaceContent(Landroid/support/v4/app/Fragment;ZLjava/lang/String;)V

    .line 507
    sget-object v1, Lcom/google/android/music/ui/HomeActivity$Screen;->NO_CONTENT:Lcom/google/android/music/ui/HomeActivity$Screen;

    if-ne p1, v1, :cond_0

    .line 508
    invoke-virtual {p0, v3}, Lcom/google/android/music/ui/HomeActivity;->setEmptyScreenShowing(Z)V

    .line 517
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/music/ui/HomeActivity;->supportInvalidateOptionsMenu()V

    .line 524
    :goto_1
    iget-object v1, p0, Lcom/google/android/music/ui/HomeActivity;->mTargetScreen:Lcom/google/android/music/ui/HomeActivity$Screen;

    # getter for: Lcom/google/android/music/ui/HomeActivity$Screen;->mTitleResId:I
    invoke-static {v1}, Lcom/google/android/music/ui/HomeActivity$Screen;->access$200(Lcom/google/android/music/ui/HomeActivity$Screen;)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/music/ui/HomeActivity;->setTitle(I)V

    .line 525
    return-void

    .line 510
    :cond_0
    invoke-virtual {p0, v4}, Lcom/google/android/music/ui/HomeActivity;->setEmptyScreenShowing(Z)V

    .line 511
    invoke-virtual {p0, v3}, Lcom/google/android/music/ui/HomeActivity;->enableSideDrawer(Z)V

    .line 512
    invoke-virtual {p0}, Lcom/google/android/music/ui/HomeActivity;->showDrawerIfRequested()V

    .line 513
    invoke-virtual {p0}, Lcom/google/android/music/ui/HomeActivity;->autoHideDrawerIfRequested()V

    .line 514
    invoke-virtual {p0}, Lcom/google/android/music/ui/HomeActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/support/v7/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    goto :goto_0

    .line 520
    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/music/ui/HomeActivity;->prepareScreenChange(Lcom/google/android/music/ui/HomeActivity$Screen;)V

    goto :goto_1
.end method

.method private updateHasAudioAndSyncState()V
    .locals 1

    .prologue
    .line 569
    new-instance v0, Lcom/google/android/music/ui/HomeActivity$4;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/HomeActivity$4;-><init>(Lcom/google/android/music/ui/HomeActivity;)V

    invoke-static {v0}, Lcom/google/android/music/utils/MusicUtils;->runAsyncWithCallback(Lcom/google/android/music/utils/async/AsyncRunner;)V

    .line 603
    return-void
.end method

.method private updateUIStateIfNeeded(Lcom/google/android/music/NautilusStatus;)V
    .locals 2
    .param p1, "newStatus"    # Lcom/google/android/music/NautilusStatus;

    .prologue
    .line 363
    iget-boolean v0, p0, Lcom/google/android/music/ui/HomeActivity;->mActivityStarted:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/ui/HomeActivity;->mNautilusStatus:Lcom/google/android/music/NautilusStatus;

    if-ne v0, p1, :cond_1

    .line 383
    :cond_0
    :goto_0
    return-void

    .line 372
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/ui/HomeActivity;->mCurrentScreen:Lcom/google/android/music/ui/HomeActivity$Screen;

    sget-object v1, Lcom/google/android/music/ui/HomeActivity$Screen;->EXPLORE:Lcom/google/android/music/ui/HomeActivity$Screen;

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lcom/google/android/music/ui/HomeActivity;->mCurrentScreen:Lcom/google/android/music/ui/HomeActivity$Screen;

    sget-object v1, Lcom/google/android/music/ui/HomeActivity$Screen;->INSTANT_MIXES:Lcom/google/android/music/ui/HomeActivity$Screen;

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lcom/google/android/music/ui/HomeActivity;->mCurrentScreen:Lcom/google/android/music/ui/HomeActivity$Screen;

    sget-object v1, Lcom/google/android/music/ui/HomeActivity$Screen;->RADIO:Lcom/google/android/music/ui/HomeActivity$Screen;

    if-ne v0, v1, :cond_3

    .line 375
    :cond_2
    sget-object v0, Lcom/google/android/music/ui/HomeActivity$Screen;->MAINSTAGE:Lcom/google/android/music/ui/HomeActivity$Screen;

    iput-object v0, p0, Lcom/google/android/music/ui/HomeActivity;->mTargetScreen:Lcom/google/android/music/ui/HomeActivity$Screen;

    .line 378
    :cond_3
    iput-object p1, p0, Lcom/google/android/music/ui/HomeActivity;->mNautilusStatus:Lcom/google/android/music/NautilusStatus;

    .line 379
    invoke-direct {p0}, Lcom/google/android/music/ui/HomeActivity;->registerMusicEventListeners()V

    .line 380
    iget-object v0, p0, Lcom/google/android/music/ui/HomeActivity;->mNautilusStatus:Lcom/google/android/music/NautilusStatus;

    sget-object v1, Lcom/google/android/music/NautilusStatus;->GOT_NAUTILUS:Lcom/google/android/music/NautilusStatus;

    if-ne v0, v1, :cond_0

    .line 381
    iget-object v0, p0, Lcom/google/android/music/ui/HomeActivity;->mTargetScreen:Lcom/google/android/music/ui/HomeActivity$Screen;

    invoke-direct {p0, v0}, Lcom/google/android/music/ui/HomeActivity;->showSingleFragmentScreen(Lcom/google/android/music/ui/HomeActivity$Screen;)V

    goto :goto_0
.end method


# virtual methods
.method public onActivityReenter(ILandroid/content/Intent;)V
    .locals 1
    .param p1, "resultCode"    # I
    .param p2, "data"    # Landroid/content/Intent;

    .prologue
    .line 346
    const/4 v0, -0x2

    if-ne p1, v0, :cond_0

    .line 347
    invoke-virtual {p0}, Lcom/google/android/music/ui/HomeActivity;->postponeEnterTransition()V

    .line 348
    new-instance v0, Lcom/google/android/music/ui/HomeActivity$1;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/HomeActivity$1;-><init>(Lcom/google/android/music/ui/HomeActivity;)V

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/HomeActivity;->setExitSharedElementCallback(Landroid/support/v4/app/SharedElementCallback;)V

    .line 358
    invoke-virtual {p0}, Lcom/google/android/music/ui/HomeActivity;->startPostponedEnterTransition()V

    .line 360
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 292
    invoke-super {p0, p1}, Lcom/google/android/music/ui/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 294
    invoke-virtual {p0}, Lcom/google/android/music/ui/HomeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 295
    .local v0, "launchIntent":Landroid/content/Intent;
    invoke-static {p0}, Lcom/google/android/music/leanback/LeanbackUtils;->launchLeanbackInterfaceIfNeeded(Landroid/app/Activity;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 342
    :cond_0
    :goto_0
    return-void

    .line 298
    :cond_1
    if-eqz v0, :cond_3

    const-string v5, "android.intent.action.MAIN"

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    const-string v5, "allowTutorial"

    const/4 v6, 0x0

    invoke-virtual {v0, v5, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 303
    :cond_2
    invoke-static {p0}, Lcom/google/android/music/xdi/XdiUtils;->launchXdiInterfaceIfNeeded(Landroid/app/Activity;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 314
    invoke-virtual {p0}, Lcom/google/android/music/ui/HomeActivity;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v5

    invoke-static {p0, v5}, Lcom/google/android/music/tutorial/TutorialUtils;->launchTutorialOnStartupIfNeeded(Landroid/app/Activity;Lcom/google/android/music/preferences/MusicPreferences;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 320
    :cond_3
    if-eqz p1, :cond_4

    .line 321
    move-object v2, p1

    .line 328
    .local v2, "options":Landroid/os/Bundle;
    :goto_1
    if-nez v2, :cond_6

    const/4 v4, 0x0

    .line 331
    .local v4, "screenTag":Ljava/lang/String;
    :goto_2
    sget-object v5, Lcom/google/android/music/ui/HomeActivity;->DEFAULT_SCREEN:Lcom/google/android/music/ui/HomeActivity$Screen;

    invoke-static {v4, v5}, Lcom/google/android/music/ui/HomeActivity$Screen;->fromTag(Ljava/lang/String;Lcom/google/android/music/ui/HomeActivity$Screen;)Lcom/google/android/music/ui/HomeActivity$Screen;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/music/ui/HomeActivity;->mTargetScreen:Lcom/google/android/music/ui/HomeActivity$Screen;

    .line 333
    invoke-virtual {p0}, Lcom/google/android/music/ui/HomeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const-string v6, "metajamIdDestination"

    invoke-virtual {v5, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 335
    .local v1, "metajamIdDestination":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/android/music/ui/HomeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const-string v6, "playlistShareTokenDestination"

    invoke-virtual {v5, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 337
    .local v3, "playlistShareTokenDestination":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_7

    .line 338
    invoke-static {p0, v1}, Lcom/google/android/music/ui/AppNavigation;->openMetajamItem(Landroid/app/Activity;Ljava/lang/String;)V

    goto :goto_0

    .line 322
    .end local v1    # "metajamIdDestination":Ljava/lang/String;
    .end local v2    # "options":Landroid/os/Bundle;
    .end local v3    # "playlistShareTokenDestination":Ljava/lang/String;
    .end local v4    # "screenTag":Ljava/lang/String;
    :cond_4
    if-eqz v0, :cond_5

    .line 323
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    .restart local v2    # "options":Landroid/os/Bundle;
    goto :goto_1

    .line 325
    .end local v2    # "options":Landroid/os/Bundle;
    :cond_5
    const/4 v2, 0x0

    .restart local v2    # "options":Landroid/os/Bundle;
    goto :goto_1

    .line 328
    :cond_6
    const-string v5, "music:home:screen"

    invoke-virtual {v2, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto :goto_2

    .line 339
    .restart local v1    # "metajamIdDestination":Ljava/lang/String;
    .restart local v3    # "playlistShareTokenDestination":Ljava/lang/String;
    .restart local v4    # "screenTag":Ljava/lang/String;
    :cond_7
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 340
    invoke-static {p0, v3}, Lcom/google/android/music/ui/AppNavigation;->openPlaylist(Landroid/app/Activity;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 435
    invoke-super {p0}, Lcom/google/android/music/ui/BaseActivity;->onDestroy()V

    .line 436
    return-void
.end method

.method public onNewIntent(Landroid/content/Intent;)V
    .locals 5
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 460
    invoke-super {p0, p1}, Lcom/google/android/music/ui/BaseActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 461
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 462
    .local v0, "extras":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 463
    const-string v4, "music:home:screen"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 464
    .local v3, "screenTag":Ljava/lang/String;
    sget-object v4, Lcom/google/android/music/ui/HomeActivity;->DEFAULT_SCREEN:Lcom/google/android/music/ui/HomeActivity$Screen;

    invoke-static {v3, v4}, Lcom/google/android/music/ui/HomeActivity$Screen;->fromTag(Ljava/lang/String;Lcom/google/android/music/ui/HomeActivity$Screen;)Lcom/google/android/music/ui/HomeActivity$Screen;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/music/ui/HomeActivity;->mTargetScreen:Lcom/google/android/music/ui/HomeActivity$Screen;

    .line 466
    iget-object v4, p0, Lcom/google/android/music/ui/HomeActivity;->mTargetScreen:Lcom/google/android/music/ui/HomeActivity$Screen;

    invoke-direct {p0, v4}, Lcom/google/android/music/ui/HomeActivity;->showSingleFragmentScreen(Lcom/google/android/music/ui/HomeActivity$Screen;)V

    .line 468
    const-string v4, "metajamIdDestination"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 470
    .local v1, "metajamIdDestination":Ljava/lang/String;
    const-string v4, "playlistShareTokenDestination"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 472
    .local v2, "playlistShareTokenDestination":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 473
    invoke-static {p0, v1}, Lcom/google/android/music/ui/AppNavigation;->openMetajamItem(Landroid/app/Activity;Ljava/lang/String;)V

    .line 479
    .end local v1    # "metajamIdDestination":Ljava/lang/String;
    .end local v2    # "playlistShareTokenDestination":Ljava/lang/String;
    .end local v3    # "screenTag":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 474
    .restart local v1    # "metajamIdDestination":Ljava/lang/String;
    .restart local v2    # "playlistShareTokenDestination":Ljava/lang/String;
    .restart local v3    # "screenTag":Ljava/lang/String;
    :cond_1
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 475
    invoke-static {p0, v2}, Lcom/google/android/music/ui/AppNavigation;->openPlaylist(Landroid/app/Activity;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onNewNautilusStatus(Lcom/google/android/music/NautilusStatus;)V
    .locals 0
    .param p1, "newStatus"    # Lcom/google/android/music/NautilusStatus;

    .prologue
    .line 445
    invoke-super {p0, p1}, Lcom/google/android/music/ui/BaseActivity;->onNewNautilusStatus(Lcom/google/android/music/NautilusStatus;)V

    .line 447
    invoke-direct {p0, p1}, Lcom/google/android/music/ui/HomeActivity;->updateUIStateIfNeeded(Lcom/google/android/music/NautilusStatus;)V

    .line 448
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 483
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 495
    invoke-super {p0, p1}, Lcom/google/android/music/ui/BaseActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 488
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/music/ui/HomeActivity;->isSideDrawerOpen()Z

    move-result v0

    if-nez v0, :cond_0

    .line 489
    invoke-virtual {p0}, Lcom/google/android/music/ui/HomeActivity;->openSideDrawer()V

    .line 490
    const/4 v0, 0x1

    goto :goto_0

    .line 492
    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/music/ui/BaseActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0

    .line 483
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 416
    invoke-super {p0}, Lcom/google/android/music/ui/BaseActivity;->onPause()V

    .line 417
    iget-boolean v0, p0, Lcom/google/android/music/ui/HomeActivity;->mContentObserverRegistered:Z

    if-eqz v0, :cond_0

    .line 418
    invoke-virtual {p0}, Lcom/google/android/music/ui/HomeActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/ui/HomeActivity;->mContentObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 419
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/ui/HomeActivity;->mContentObserverRegistered:Z

    .line 421
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/ui/HomeActivity;->mSyncObserverHandle:Ljava/lang/Object;

    if-eqz v0, :cond_1

    .line 422
    iget-object v0, p0, Lcom/google/android/music/ui/HomeActivity;->mSyncObserverHandle:Ljava/lang/Object;

    invoke-static {v0}, Landroid/content/ContentResolver;->removeStatusChangeListener(Ljava/lang/Object;)V

    .line 423
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/ui/HomeActivity;->mSyncObserverHandle:Ljava/lang/Object;

    .line 425
    :cond_1
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 407
    invoke-super {p0}, Lcom/google/android/music/ui/BaseActivity;->onResume()V

    .line 411
    invoke-direct {p0}, Lcom/google/android/music/ui/HomeActivity;->registerMusicEventListeners()V

    .line 412
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 452
    invoke-super {p0, p1}, Lcom/google/android/music/ui/BaseActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 453
    iget-object v0, p0, Lcom/google/android/music/ui/HomeActivity;->mCurrentScreen:Lcom/google/android/music/ui/HomeActivity$Screen;

    if-eqz v0, :cond_0

    .line 454
    const-string v0, "music:home:screen"

    iget-object v1, p0, Lcom/google/android/music/ui/HomeActivity;->mCurrentScreen:Lcom/google/android/music/ui/HomeActivity$Screen;

    invoke-virtual {v1}, Lcom/google/android/music/ui/HomeActivity$Screen;->getTag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 456
    :cond_0
    return-void
.end method

.method protected onStart()V
    .locals 1

    .prologue
    .line 387
    invoke-super {p0}, Lcom/google/android/music/ui/BaseActivity;->onStart()V

    .line 388
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/ui/HomeActivity;->mActivityStarted:Z

    .line 389
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->getNautilusStatus()Lcom/google/android/music/NautilusStatus;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/music/ui/HomeActivity;->updateUIStateIfNeeded(Lcom/google/android/music/NautilusStatus;)V

    .line 390
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 429
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/ui/HomeActivity;->mActivityStarted:Z

    .line 430
    invoke-super {p0}, Lcom/google/android/music/ui/BaseActivity;->onStop()V

    .line 431
    return-void
.end method

.method protected useActionBarHamburger()Z
    .locals 1

    .prologue
    .line 440
    const/4 v0, 0x1

    return v0
.end method
