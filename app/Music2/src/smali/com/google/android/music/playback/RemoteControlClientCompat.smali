.class public Lcom/google/android/music/playback/RemoteControlClientCompat;
.super Ljava/lang/Object;
.source "RemoteControlClientCompat.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/playback/RemoteControlClientCompat$MetadataEditorCompat;,
        Lcom/google/android/music/playback/RemoteControlClientCompat$OnMetadataUpdateListener;,
        Lcom/google/android/music/playback/RemoteControlClientCompat$OnGetPlaybackPositionListener;,
        Lcom/google/android/music/playback/RemoteControlClientCompat$OnPlaybackPositionUpdateListener;
    }
.end annotation


# static fields
.field public static FLAG_KEY_MEDIA_FAST_FORWARD:I

.field public static FLAG_KEY_MEDIA_NEXT:I

.field public static FLAG_KEY_MEDIA_PAUSE:I

.field public static FLAG_KEY_MEDIA_PLAY:I

.field public static FLAG_KEY_MEDIA_PLAY_PAUSE:I

.field public static FLAG_KEY_MEDIA_POSITION_UPDATE:I

.field public static FLAG_KEY_MEDIA_PREVIOUS:I

.field public static FLAG_KEY_MEDIA_RATING:I

.field public static FLAG_KEY_MEDIA_REWIND:I

.field public static FLAG_KEY_MEDIA_STOP:I

.field public static PLAYSTATE_BUFFERING:I

.field public static PLAYSTATE_ERROR:I

.field public static PLAYSTATE_FAST_FORWARDING:I

.field public static PLAYSTATE_PAUSED:I

.field public static PLAYSTATE_PLAYING:I

.field public static PLAYSTATE_REWINDING:I

.field public static PLAYSTATE_SKIPPING_BACKWARDS:I

.field public static PLAYSTATE_SKIPPING_FORWARDS:I

.field public static PLAYSTATE_STOPPED:I

.field private static sHasRemoteControlAPIs:Z

.field private static sOnGetPlaybackPositionListenerInterface:Ljava/lang/Class;

.field private static sOnMetadataUpdateListenerInterface:Ljava/lang/Class;

.field private static sOnPlaybackPositionUpdateListenerInterface:Ljava/lang/Class;

.field private static sRCCEditMetadataMethod:Ljava/lang/reflect/Method;

.field private static sRCCSetMetadataUpdateListener:Ljava/lang/reflect/Method;

.field private static sRCCSetOnGetPlaybackPositionListener:Ljava/lang/reflect/Method;

.field private static sRCCSetPlayStateMethodJBMR2Plus:Ljava/lang/reflect/Method;

.field private static sRCCSetPlayStateMethodPreJBMR2:Ljava/lang/reflect/Method;

.field private static sRCCSetPlaybackPositionUpdateListener:Ljava/lang/reflect/Method;

.field private static sRCCSetTransportControlFlags:Ljava/lang/reflect/Method;

.field private static sRemoteControlClientClass:Ljava/lang/Class;


# instance fields
.field private mActualRemoteControlClient:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .prologue
    const/4 v8, 0x3

    const/4 v12, 0x2

    const/4 v11, 0x0

    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 72
    sput v9, Lcom/google/android/music/playback/RemoteControlClientCompat;->PLAYSTATE_STOPPED:I

    .line 73
    sput v12, Lcom/google/android/music/playback/RemoteControlClientCompat;->PLAYSTATE_PAUSED:I

    .line 74
    sput v8, Lcom/google/android/music/playback/RemoteControlClientCompat;->PLAYSTATE_PLAYING:I

    .line 75
    const/4 v8, 0x4

    sput v8, Lcom/google/android/music/playback/RemoteControlClientCompat;->PLAYSTATE_FAST_FORWARDING:I

    .line 76
    const/4 v8, 0x5

    sput v8, Lcom/google/android/music/playback/RemoteControlClientCompat;->PLAYSTATE_REWINDING:I

    .line 77
    const/4 v8, 0x6

    sput v8, Lcom/google/android/music/playback/RemoteControlClientCompat;->PLAYSTATE_SKIPPING_FORWARDS:I

    .line 78
    const/4 v8, 0x7

    sput v8, Lcom/google/android/music/playback/RemoteControlClientCompat;->PLAYSTATE_SKIPPING_BACKWARDS:I

    .line 79
    const/16 v8, 0x8

    sput v8, Lcom/google/android/music/playback/RemoteControlClientCompat;->PLAYSTATE_BUFFERING:I

    .line 80
    const/16 v8, 0x9

    sput v8, Lcom/google/android/music/playback/RemoteControlClientCompat;->PLAYSTATE_ERROR:I

    .line 82
    sput v9, Lcom/google/android/music/playback/RemoteControlClientCompat;->FLAG_KEY_MEDIA_PREVIOUS:I

    .line 83
    sput v12, Lcom/google/android/music/playback/RemoteControlClientCompat;->FLAG_KEY_MEDIA_REWIND:I

    .line 84
    const/4 v8, 0x4

    sput v8, Lcom/google/android/music/playback/RemoteControlClientCompat;->FLAG_KEY_MEDIA_PLAY:I

    .line 85
    const/16 v8, 0x8

    sput v8, Lcom/google/android/music/playback/RemoteControlClientCompat;->FLAG_KEY_MEDIA_PLAY_PAUSE:I

    .line 86
    const/16 v8, 0x10

    sput v8, Lcom/google/android/music/playback/RemoteControlClientCompat;->FLAG_KEY_MEDIA_PAUSE:I

    .line 87
    const/16 v8, 0x20

    sput v8, Lcom/google/android/music/playback/RemoteControlClientCompat;->FLAG_KEY_MEDIA_STOP:I

    .line 88
    const/16 v8, 0x40

    sput v8, Lcom/google/android/music/playback/RemoteControlClientCompat;->FLAG_KEY_MEDIA_FAST_FORWARD:I

    .line 89
    const/16 v8, 0x80

    sput v8, Lcom/google/android/music/playback/RemoteControlClientCompat;->FLAG_KEY_MEDIA_NEXT:I

    .line 90
    const/16 v8, 0x100

    sput v8, Lcom/google/android/music/playback/RemoteControlClientCompat;->FLAG_KEY_MEDIA_POSITION_UPDATE:I

    .line 91
    const/16 v8, 0x200

    sput v8, Lcom/google/android/music/playback/RemoteControlClientCompat;->FLAG_KEY_MEDIA_RATING:I

    .line 94
    sput-object v10, Lcom/google/android/music/playback/RemoteControlClientCompat;->sOnPlaybackPositionUpdateListenerInterface:Ljava/lang/Class;

    .line 95
    sput-object v10, Lcom/google/android/music/playback/RemoteControlClientCompat;->sOnGetPlaybackPositionListenerInterface:Ljava/lang/Class;

    .line 96
    sput-object v10, Lcom/google/android/music/playback/RemoteControlClientCompat;->sOnMetadataUpdateListenerInterface:Ljava/lang/Class;

    .line 101
    sput-object v10, Lcom/google/android/music/playback/RemoteControlClientCompat;->sRCCSetPlayStateMethodJBMR2Plus:Ljava/lang/reflect/Method;

    .line 103
    sput-object v10, Lcom/google/android/music/playback/RemoteControlClientCompat;->sRCCSetPlaybackPositionUpdateListener:Ljava/lang/reflect/Method;

    .line 104
    sput-object v10, Lcom/google/android/music/playback/RemoteControlClientCompat;->sRCCSetOnGetPlaybackPositionListener:Ljava/lang/reflect/Method;

    .line 105
    sput-object v10, Lcom/google/android/music/playback/RemoteControlClientCompat;->sRCCSetMetadataUpdateListener:Ljava/lang/reflect/Method;

    .line 108
    sput-boolean v11, Lcom/google/android/music/playback/RemoteControlClientCompat;->sHasRemoteControlAPIs:Z

    .line 111
    invoke-static {}, Lcom/google/android/music/preferences/MusicPreferences;->isICSOrGreater()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 113
    :try_start_0
    const-class v8, Lcom/google/android/music/playback/RemoteControlClientCompat;

    invoke-virtual {v8}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    .line 114
    .local v1, "classLoader":Ljava/lang/ClassLoader;
    invoke-static {v1}, Lcom/google/android/music/playback/RemoteControlClientCompat;->getActualRemoteControlClientClass(Ljava/lang/ClassLoader;)Ljava/lang/Class;

    move-result-object v8

    sput-object v8, Lcom/google/android/music/playback/RemoteControlClientCompat;->sRemoteControlClientClass:Ljava/lang/Class;

    .line 117
    const-class v8, Lcom/google/android/music/playback/RemoteControlClientCompat;

    invoke-virtual {v8}, Ljava/lang/Class;->getFields()[Ljava/lang/reflect/Field;

    move-result-object v0

    .local v0, "arr$":[Ljava/lang/reflect/Field;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_1

    aget-object v3, v0, v4
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_5

    .line 119
    .local v3, "field":Ljava/lang/reflect/Field;
    :try_start_1
    sget-object v8, Lcom/google/android/music/playback/RemoteControlClientCompat;->sRemoteControlClientClass:Ljava/lang/Class;

    invoke-virtual {v3}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v6

    .line 120
    .local v6, "realField":Ljava/lang/reflect/Field;
    const/4 v8, 0x0

    invoke-virtual {v6, v8}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    .line 121
    .local v7, "realValue":Ljava/lang/Object;
    const/4 v8, 0x0

    invoke-virtual {v3, v8, v7}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/NoSuchFieldException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_5

    .line 117
    .end local v6    # "realField":Ljava/lang/reflect/Field;
    .end local v7    # "realValue":Ljava/lang/Object;
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 122
    :catch_0
    move-exception v2

    .line 123
    .local v2, "e":Ljava/lang/NoSuchFieldException;
    :try_start_2
    const-string v8, "RemoteControlCompat"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Could not get real field: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v3}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/ClassNotFoundException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/NoSuchMethodException; {:try_start_2 .. :try_end_2} :catch_5

    goto :goto_1

    .line 172
    .end local v0    # "arr$":[Ljava/lang/reflect/Field;
    .end local v2    # "e":Ljava/lang/NoSuchFieldException;
    .end local v3    # "field":Ljava/lang/reflect/Field;
    .end local v4    # "i$":I
    .end local v5    # "len$":I
    :catch_1
    move-exception v2

    .line 173
    .local v2, "e":Ljava/lang/ClassNotFoundException;
    const-string v8, "RemoteControlCompat"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Could not find class. "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v2}, Ljava/lang/ClassNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 180
    .end local v2    # "e":Ljava/lang/ClassNotFoundException;
    :cond_0
    :goto_2
    return-void

    .line 124
    .restart local v0    # "arr$":[Ljava/lang/reflect/Field;
    .restart local v3    # "field":Ljava/lang/reflect/Field;
    .restart local v4    # "i$":I
    .restart local v5    # "len$":I
    :catch_2
    move-exception v2

    .line 125
    .local v2, "e":Ljava/lang/IllegalArgumentException;
    :try_start_3
    const-string v8, "RemoteControlCompat"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Error trying to pull field value for: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v3}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v2}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/ClassNotFoundException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/lang/NoSuchMethodException; {:try_start_3 .. :try_end_3} :catch_5

    goto :goto_1

    .line 174
    .end local v0    # "arr$":[Ljava/lang/reflect/Field;
    .end local v2    # "e":Ljava/lang/IllegalArgumentException;
    .end local v3    # "field":Ljava/lang/reflect/Field;
    .end local v4    # "i$":I
    .end local v5    # "len$":I
    :catch_3
    move-exception v2

    .line 175
    .local v2, "e":Ljava/lang/SecurityException;
    const-string v8, "RemoteControlCompat"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Security Exception: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v2}, Ljava/lang/SecurityException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    .line 127
    .end local v2    # "e":Ljava/lang/SecurityException;
    .restart local v0    # "arr$":[Ljava/lang/reflect/Field;
    .restart local v3    # "field":Ljava/lang/reflect/Field;
    .restart local v4    # "i$":I
    .restart local v5    # "len$":I
    :catch_4
    move-exception v2

    .line 128
    .local v2, "e":Ljava/lang/IllegalAccessException;
    :try_start_4
    const-string v8, "RemoteControlCompat"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Error trying to pull field value for: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v3}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v2}, Ljava/lang/IllegalAccessException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catch Ljava/lang/ClassNotFoundException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_4 .. :try_end_4} :catch_3
    .catch Ljava/lang/NoSuchMethodException; {:try_start_4 .. :try_end_4} :catch_5

    goto/16 :goto_1

    .line 176
    .end local v0    # "arr$":[Ljava/lang/reflect/Field;
    .end local v2    # "e":Ljava/lang/IllegalAccessException;
    .end local v3    # "field":Ljava/lang/reflect/Field;
    .end local v4    # "i$":I
    .end local v5    # "len$":I
    :catch_5
    move-exception v2

    .line 177
    .local v2, "e":Ljava/lang/NoSuchMethodException;
    const-string v8, "RemoteControlCompat"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "No Such Method Exception: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v2}, Ljava/lang/NoSuchMethodException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_2

    .line 134
    .end local v2    # "e":Ljava/lang/NoSuchMethodException;
    .restart local v0    # "arr$":[Ljava/lang/reflect/Field;
    .restart local v4    # "i$":I
    .restart local v5    # "len$":I
    :cond_1
    :try_start_5
    sget-object v8, Lcom/google/android/music/playback/RemoteControlClientCompat;->sRemoteControlClientClass:Ljava/lang/Class;

    const-string v9, "editMetadata"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Class;

    const/4 v11, 0x0

    sget-object v12, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v12, v10, v11

    invoke-virtual {v8, v9, v10}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v8

    sput-object v8, Lcom/google/android/music/playback/RemoteControlClientCompat;->sRCCEditMetadataMethod:Ljava/lang/reflect/Method;

    .line 136
    sget-object v8, Lcom/google/android/music/playback/RemoteControlClientCompat;->sRemoteControlClientClass:Ljava/lang/Class;

    const-string v9, "setPlaybackState"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Class;

    const/4 v11, 0x0

    sget-object v12, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v12, v10, v11

    invoke-virtual {v8, v9, v10}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v8

    sput-object v8, Lcom/google/android/music/playback/RemoteControlClientCompat;->sRCCSetPlayStateMethodPreJBMR2:Ljava/lang/reflect/Method;

    .line 139
    invoke-static {}, Lcom/google/android/music/preferences/MusicPreferences;->isJellyBeanMR2OrGreater()Z

    move-result v8

    if-nez v8, :cond_2

    invoke-static {}, Lcom/google/android/music/preferences/MusicPreferences;->isGlass()Z

    move-result v8

    if-eqz v8, :cond_3

    .line 140
    :cond_2
    sget-object v8, Lcom/google/android/music/playback/RemoteControlClientCompat;->sRemoteControlClientClass:Ljava/lang/Class;

    const-string v9, "setPlaybackState"

    const/4 v10, 0x3

    new-array v10, v10, [Ljava/lang/Class;

    const/4 v11, 0x0

    sget-object v12, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v12, v10, v11

    const/4 v11, 0x1

    sget-object v12, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    aput-object v12, v10, v11

    const/4 v11, 0x2

    sget-object v12, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    aput-object v12, v10, v11

    invoke-virtual {v8, v9, v10}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v8

    sput-object v8, Lcom/google/android/music/playback/RemoteControlClientCompat;->sRCCSetPlayStateMethodJBMR2Plus:Ljava/lang/reflect/Method;

    .line 143
    const-string v8, "android.media.RemoteControlClient$OnPlaybackPositionUpdateListener"

    invoke-static {v8}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v8

    sput-object v8, Lcom/google/android/music/playback/RemoteControlClientCompat;->sOnPlaybackPositionUpdateListenerInterface:Ljava/lang/Class;

    .line 146
    const-string v8, "android.media.RemoteControlClient$OnGetPlaybackPositionListener"

    invoke-static {v8}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v8

    sput-object v8, Lcom/google/android/music/playback/RemoteControlClientCompat;->sOnGetPlaybackPositionListenerInterface:Ljava/lang/Class;

    .line 149
    sget-object v8, Lcom/google/android/music/playback/RemoteControlClientCompat;->sRemoteControlClientClass:Ljava/lang/Class;

    const-string v9, "setPlaybackPositionUpdateListener"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Class;

    const/4 v11, 0x0

    sget-object v12, Lcom/google/android/music/playback/RemoteControlClientCompat;->sOnPlaybackPositionUpdateListenerInterface:Ljava/lang/Class;

    aput-object v12, v10, v11

    invoke-virtual {v8, v9, v10}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v8

    sput-object v8, Lcom/google/android/music/playback/RemoteControlClientCompat;->sRCCSetPlaybackPositionUpdateListener:Ljava/lang/reflect/Method;

    .line 153
    sget-object v8, Lcom/google/android/music/playback/RemoteControlClientCompat;->sRemoteControlClientClass:Ljava/lang/Class;

    const-string v9, "setOnGetPlaybackPositionListener"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Class;

    const/4 v11, 0x0

    sget-object v12, Lcom/google/android/music/playback/RemoteControlClientCompat;->sOnGetPlaybackPositionListenerInterface:Ljava/lang/Class;

    aput-object v12, v10, v11

    invoke-virtual {v8, v9, v10}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v8

    sput-object v8, Lcom/google/android/music/playback/RemoteControlClientCompat;->sRCCSetOnGetPlaybackPositionListener:Ljava/lang/reflect/Method;

    .line 159
    :cond_3
    invoke-static {}, Lcom/google/android/music/preferences/MusicPreferences;->isKitKatOrGreater()Z

    move-result v8

    if-eqz v8, :cond_4

    .line 160
    const-string v8, "android.media.RemoteControlClient$OnMetadataUpdateListener"

    invoke-static {v8}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v8

    sput-object v8, Lcom/google/android/music/playback/RemoteControlClientCompat;->sOnMetadataUpdateListenerInterface:Ljava/lang/Class;

    .line 163
    sget-object v8, Lcom/google/android/music/playback/RemoteControlClientCompat;->sRemoteControlClientClass:Ljava/lang/Class;

    const-string v9, "setMetadataUpdateListener"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Class;

    const/4 v11, 0x0

    sget-object v12, Lcom/google/android/music/playback/RemoteControlClientCompat;->sOnMetadataUpdateListenerInterface:Ljava/lang/Class;

    aput-object v12, v10, v11

    invoke-virtual {v8, v9, v10}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v8

    sput-object v8, Lcom/google/android/music/playback/RemoteControlClientCompat;->sRCCSetMetadataUpdateListener:Ljava/lang/reflect/Method;

    .line 168
    :cond_4
    sget-object v8, Lcom/google/android/music/playback/RemoteControlClientCompat;->sRemoteControlClientClass:Ljava/lang/Class;

    const-string v9, "setTransportControlFlags"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Class;

    const/4 v11, 0x0

    sget-object v12, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v12, v10, v11

    invoke-virtual {v8, v9, v10}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v8

    sput-object v8, Lcom/google/android/music/playback/RemoteControlClientCompat;->sRCCSetTransportControlFlags:Ljava/lang/reflect/Method;

    .line 171
    const/4 v8, 0x1

    sput-boolean v8, Lcom/google/android/music/playback/RemoteControlClientCompat;->sHasRemoteControlAPIs:Z
    :try_end_5
    .catch Ljava/lang/ClassNotFoundException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_5 .. :try_end_5} :catch_3
    .catch Ljava/lang/NoSuchMethodException; {:try_start_5 .. :try_end_5} :catch_5

    goto/16 :goto_2
.end method

.method public constructor <init>(Landroid/app/PendingIntent;Landroid/os/Looper;)V
    .locals 5
    .param p1, "pendingIntent"    # Landroid/app/PendingIntent;
    .param p2, "looper"    # Landroid/os/Looper;

    .prologue
    .line 203
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 204
    sget-boolean v1, Lcom/google/android/music/playback/RemoteControlClientCompat;->sHasRemoteControlAPIs:Z

    if-nez v1, :cond_0

    .line 215
    :goto_0
    return-void

    .line 209
    :cond_0
    :try_start_0
    sget-object v1, Lcom/google/android/music/playback/RemoteControlClientCompat;->sRemoteControlClientClass:Ljava/lang/Class;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    const-class v4, Landroid/app/PendingIntent;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-class v4, Landroid/os/Looper;

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/playback/RemoteControlClientCompat;->mActualRemoteControlClient:Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 212
    :catch_0
    move-exception v0

    .line 213
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "RemoteControlCompat"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error creating new instance of "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/google/android/music/playback/RemoteControlClientCompat;->sRemoteControlClientClass:Ljava/lang/Class;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method static synthetic access$000()Z
    .locals 1

    .prologue
    .line 19
    sget-boolean v0, Lcom/google/android/music/playback/RemoteControlClientCompat;->sHasRemoteControlAPIs:Z

    return v0
.end method

.method public static getActualRemoteControlClientClass(Ljava/lang/ClassLoader;)Ljava/lang/Class;
    .locals 1
    .param p0, "classLoader"    # Ljava/lang/ClassLoader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 184
    const-string v0, "android.media.RemoteControlClient"

    invoke-virtual {p0, v0}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public editMetadata(Z)Lcom/google/android/music/playback/RemoteControlClientCompat$MetadataEditorCompat;
    .locals 7
    .param p1, "startEmpty"    # Z

    .prologue
    .line 341
    sget-boolean v2, Lcom/google/android/music/playback/RemoteControlClientCompat;->sHasRemoteControlAPIs:Z

    if-eqz v2, :cond_0

    .line 343
    :try_start_0
    sget-object v2, Lcom/google/android/music/playback/RemoteControlClientCompat;->sRCCEditMetadataMethod:Ljava/lang/reflect/Method;

    iget-object v3, p0, Lcom/google/android/music/playback/RemoteControlClientCompat;->mActualRemoteControlClient:Ljava/lang/Object;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 351
    :goto_0
    new-instance v2, Lcom/google/android/music/playback/RemoteControlClientCompat$MetadataEditorCompat;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v1, v3}, Lcom/google/android/music/playback/RemoteControlClientCompat$MetadataEditorCompat;-><init>(Lcom/google/android/music/playback/RemoteControlClientCompat;Ljava/lang/Object;Lcom/google/android/music/playback/RemoteControlClientCompat$1;)V

    return-object v2

    .line 345
    :catch_0
    move-exception v0

    .line 346
    .local v0, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2

    .line 349
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    const/4 v1, 0x0

    .local v1, "metadataEditor":Ljava/lang/Object;
    goto :goto_0
.end method

.method public final getActualRemoteControlClientObject()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 483
    iget-object v0, p0, Lcom/google/android/music/playback/RemoteControlClientCompat;->mActualRemoteControlClient:Ljava/lang/Object;

    return-object v0
.end method

.method public setOnGetPlaybackPositionListener(Lcom/google/android/music/playback/RemoteControlClientCompat$OnGetPlaybackPositionListener;)V
    .locals 6
    .param p1, "l"    # Lcom/google/android/music/playback/RemoteControlClientCompat$OnGetPlaybackPositionListener;

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 422
    sget-object v2, Lcom/google/android/music/playback/RemoteControlClientCompat;->sRCCSetPlayStateMethodJBMR2Plus:Ljava/lang/reflect/Method;

    if-eqz v2, :cond_0

    sget-object v2, Lcom/google/android/music/playback/RemoteControlClientCompat;->sOnGetPlaybackPositionListenerInterface:Ljava/lang/Class;

    if-eqz v2, :cond_0

    .line 424
    sget-object v2, Lcom/google/android/music/playback/RemoteControlClientCompat;->sOnGetPlaybackPositionListenerInterface:Ljava/lang/Class;

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    new-array v3, v3, [Ljava/lang/Class;

    sget-object v4, Lcom/google/android/music/playback/RemoteControlClientCompat;->sOnGetPlaybackPositionListenerInterface:Ljava/lang/Class;

    aput-object v4, v3, v5

    new-instance v4, Lcom/google/android/music/playback/RemoteControlClientCompat$2;

    invoke-direct {v4, p0, p1}, Lcom/google/android/music/playback/RemoteControlClientCompat$2;-><init>(Lcom/google/android/music/playback/RemoteControlClientCompat;Lcom/google/android/music/playback/RemoteControlClientCompat$OnGetPlaybackPositionListener;)V

    invoke-static {v2, v3, v4}, Ljava/lang/reflect/Proxy;->newProxyInstance(Ljava/lang/ClassLoader;[Ljava/lang/Class;Ljava/lang/reflect/InvocationHandler;)Ljava/lang/Object;

    move-result-object v1

    .line 442
    .local v1, "onGetPlaybackPositionListenerWrapper":Ljava/lang/Object;
    :try_start_0
    sget-object v2, Lcom/google/android/music/playback/RemoteControlClientCompat;->sRCCSetOnGetPlaybackPositionListener:Ljava/lang/reflect/Method;

    iget-object v3, p0, Lcom/google/android/music/playback/RemoteControlClientCompat;->mActualRemoteControlClient:Ljava/lang/Object;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    invoke-virtual {v2, v3, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 448
    .end local v1    # "onGetPlaybackPositionListenerWrapper":Ljava/lang/Object;
    :cond_0
    return-void

    .line 444
    .restart local v1    # "onGetPlaybackPositionListenerWrapper":Ljava/lang/Object;
    :catch_0
    move-exception v0

    .line 445
    .local v0, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method

.method public setOnMetadataUpdateListener(Lcom/google/android/music/playback/RemoteControlClientCompat$OnMetadataUpdateListener;)V
    .locals 6
    .param p1, "l"    # Lcom/google/android/music/playback/RemoteControlClientCompat$OnMetadataUpdateListener;

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 452
    sget-object v2, Lcom/google/android/music/playback/RemoteControlClientCompat;->sRCCSetMetadataUpdateListener:Ljava/lang/reflect/Method;

    if-eqz v2, :cond_0

    sget-object v2, Lcom/google/android/music/playback/RemoteControlClientCompat;->sOnMetadataUpdateListenerInterface:Ljava/lang/Class;

    if-eqz v2, :cond_0

    .line 455
    sget-object v2, Lcom/google/android/music/playback/RemoteControlClientCompat;->sOnMetadataUpdateListenerInterface:Ljava/lang/Class;

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    new-array v3, v3, [Ljava/lang/Class;

    sget-object v4, Lcom/google/android/music/playback/RemoteControlClientCompat;->sOnMetadataUpdateListenerInterface:Ljava/lang/Class;

    aput-object v4, v3, v5

    new-instance v4, Lcom/google/android/music/playback/RemoteControlClientCompat$3;

    invoke-direct {v4, p0, p1}, Lcom/google/android/music/playback/RemoteControlClientCompat$3;-><init>(Lcom/google/android/music/playback/RemoteControlClientCompat;Lcom/google/android/music/playback/RemoteControlClientCompat$OnMetadataUpdateListener;)V

    invoke-static {v2, v3, v4}, Ljava/lang/reflect/Proxy;->newProxyInstance(Ljava/lang/ClassLoader;[Ljava/lang/Class;Ljava/lang/reflect/InvocationHandler;)Ljava/lang/Object;

    move-result-object v1

    .line 474
    .local v1, "onUpdateMetadataListenerWrapper":Ljava/lang/Object;
    :try_start_0
    sget-object v2, Lcom/google/android/music/playback/RemoteControlClientCompat;->sRCCSetMetadataUpdateListener:Ljava/lang/reflect/Method;

    iget-object v3, p0, Lcom/google/android/music/playback/RemoteControlClientCompat;->mActualRemoteControlClient:Ljava/lang/Object;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    invoke-virtual {v2, v3, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 480
    .end local v1    # "onUpdateMetadataListenerWrapper":Ljava/lang/Object;
    :cond_0
    return-void

    .line 476
    .restart local v1    # "onUpdateMetadataListenerWrapper":Ljava/lang/Object;
    :catch_0
    move-exception v0

    .line 477
    .local v0, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method

.method public setPlaybackPositionUpdateListener(Lcom/google/android/music/playback/RemoteControlClientCompat$OnPlaybackPositionUpdateListener;)V
    .locals 6
    .param p1, "l"    # Lcom/google/android/music/playback/RemoteControlClientCompat$OnPlaybackPositionUpdateListener;

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 391
    sget-object v2, Lcom/google/android/music/playback/RemoteControlClientCompat;->sRCCSetPlayStateMethodJBMR2Plus:Ljava/lang/reflect/Method;

    if-eqz v2, :cond_0

    sget-object v2, Lcom/google/android/music/playback/RemoteControlClientCompat;->sOnPlaybackPositionUpdateListenerInterface:Ljava/lang/Class;

    if-eqz v2, :cond_0

    .line 393
    sget-object v2, Lcom/google/android/music/playback/RemoteControlClientCompat;->sOnPlaybackPositionUpdateListenerInterface:Ljava/lang/Class;

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    new-array v3, v3, [Ljava/lang/Class;

    sget-object v4, Lcom/google/android/music/playback/RemoteControlClientCompat;->sOnPlaybackPositionUpdateListenerInterface:Ljava/lang/Class;

    aput-object v4, v3, v5

    new-instance v4, Lcom/google/android/music/playback/RemoteControlClientCompat$1;

    invoke-direct {v4, p0, p1}, Lcom/google/android/music/playback/RemoteControlClientCompat$1;-><init>(Lcom/google/android/music/playback/RemoteControlClientCompat;Lcom/google/android/music/playback/RemoteControlClientCompat$OnPlaybackPositionUpdateListener;)V

    invoke-static {v2, v3, v4}, Ljava/lang/reflect/Proxy;->newProxyInstance(Ljava/lang/ClassLoader;[Ljava/lang/Class;Ljava/lang/reflect/InvocationHandler;)Ljava/lang/Object;

    move-result-object v1

    .line 412
    .local v1, "onPlaybackPostiionUpdateListenerWrapper":Ljava/lang/Object;
    :try_start_0
    sget-object v2, Lcom/google/android/music/playback/RemoteControlClientCompat;->sRCCSetPlaybackPositionUpdateListener:Ljava/lang/reflect/Method;

    iget-object v3, p0, Lcom/google/android/music/playback/RemoteControlClientCompat;->mActualRemoteControlClient:Ljava/lang/Object;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    invoke-virtual {v2, v3, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 418
    .end local v1    # "onPlaybackPostiionUpdateListenerWrapper":Ljava/lang/Object;
    :cond_0
    return-void

    .line 414
    .restart local v1    # "onPlaybackPostiionUpdateListenerWrapper":Ljava/lang/Object;
    :catch_0
    move-exception v0

    .line 415
    .local v0, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method

.method public setPlaybackState(IJF)V
    .locals 6
    .param p1, "state"    # I
    .param p2, "position"    # J
    .param p4, "speed"    # F

    .prologue
    .line 365
    sget-boolean v1, Lcom/google/android/music/playback/RemoteControlClientCompat;->sHasRemoteControlAPIs:Z

    if-eqz v1, :cond_0

    .line 367
    :try_start_0
    sget-object v1, Lcom/google/android/music/playback/RemoteControlClientCompat;->sRCCSetPlayStateMethodJBMR2Plus:Ljava/lang/reflect/Method;

    if-eqz v1, :cond_1

    .line 368
    sget-object v1, Lcom/google/android/music/playback/RemoteControlClientCompat;->sRCCSetPlayStateMethodJBMR2Plus:Ljava/lang/reflect/Method;

    iget-object v2, p0, Lcom/google/android/music/playback/RemoteControlClientCompat;->mActualRemoteControlClient:Ljava/lang/Object;

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    invoke-static {p4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 377
    :cond_0
    :goto_0
    return-void

    .line 371
    :cond_1
    sget-object v1, Lcom/google/android/music/playback/RemoteControlClientCompat;->sRCCSetPlayStateMethodPreJBMR2:Ljava/lang/reflect/Method;

    iget-object v2, p0, Lcom/google/android/music/playback/RemoteControlClientCompat;->mActualRemoteControlClient:Ljava/lang/Object;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 373
    :catch_0
    move-exception v0

    .line 374
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public setTransportControlFlags(I)V
    .locals 6
    .param p1, "flags"    # I

    .prologue
    .line 380
    sget-boolean v1, Lcom/google/android/music/playback/RemoteControlClientCompat;->sHasRemoteControlAPIs:Z

    if-eqz v1, :cond_0

    .line 382
    :try_start_0
    sget-object v1, Lcom/google/android/music/playback/RemoteControlClientCompat;->sRCCSetTransportControlFlags:Ljava/lang/reflect/Method;

    iget-object v2, p0, Lcom/google/android/music/playback/RemoteControlClientCompat;->mActualRemoteControlClient:Ljava/lang/Object;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 387
    :cond_0
    return-void

    .line 383
    :catch_0
    move-exception v0

    .line 384
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
