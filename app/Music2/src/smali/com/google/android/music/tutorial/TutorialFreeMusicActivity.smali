.class public Lcom/google/android/music/tutorial/TutorialFreeMusicActivity;
.super Landroid/app/Activity;
.source "TutorialFreeMusicActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private mTracker:Lcom/google/android/music/eventlog/MusicEventLogger;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method public freeMusicOnClick()V
    .locals 2

    .prologue
    .line 72
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 73
    .local v0, "i":Landroid/content/Intent;
    const v1, 0x7f0b012f

    invoke-virtual {p0, v1}, Lcom/google/android/music/tutorial/TutorialFreeMusicActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 74
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 75
    const v1, 0x8000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 76
    const/high16 v1, 0x80000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 78
    invoke-virtual {p0, v0}, Lcom/google/android/music/tutorial/TutorialFreeMusicActivity;->startActivity(Landroid/content/Intent;)V

    .line 79
    invoke-virtual {p0}, Lcom/google/android/music/tutorial/TutorialFreeMusicActivity;->finish()V

    .line 80
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 56
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 64
    :goto_0
    return-void

    .line 58
    :sswitch_0
    invoke-virtual {p0}, Lcom/google/android/music/tutorial/TutorialFreeMusicActivity;->skipOnClick()V

    goto :goto_0

    .line 61
    :sswitch_1
    invoke-virtual {p0}, Lcom/google/android/music/tutorial/TutorialFreeMusicActivity;->freeMusicOnClick()V

    goto :goto_0

    .line 56
    :sswitch_data_0
    .sparse-switch
        0x7f0e0253 -> :sswitch_0
        0x7f0e0280 -> :sswitch_1
    .end sparse-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    .line 32
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 34
    invoke-static {p0}, Lcom/google/android/music/eventlog/MusicEventLogger;->getInstance(Landroid/content/Context;)Lcom/google/android/music/eventlog/MusicEventLogger;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/tutorial/TutorialFreeMusicActivity;->mTracker:Lcom/google/android/music/eventlog/MusicEventLogger;

    .line 36
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/google/android/music/tutorial/TutorialFreeMusicActivity;->requestWindowFeature(I)Z

    .line 37
    const v1, 0x7f0400ff

    invoke-virtual {p0, v1}, Lcom/google/android/music/tutorial/TutorialFreeMusicActivity;->setContentView(I)V

    .line 39
    const v1, 0x7f0e0280

    invoke-virtual {p0, v1}, Lcom/google/android/music/tutorial/TutorialFreeMusicActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 40
    const v1, 0x7f0e0253

    invoke-virtual {p0, v1}, Lcom/google/android/music/tutorial/TutorialFreeMusicActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 41
    .local v0, "skipButton":Landroid/widget/Button;
    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    .line 42
    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 43
    return-void
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 51
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 52
    invoke-static {p0}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    .line 53
    return-void
.end method

.method public skipOnClick()V
    .locals 0

    .prologue
    .line 67
    invoke-virtual {p0}, Lcom/google/android/music/tutorial/TutorialFreeMusicActivity;->finish()V

    .line 68
    return-void
.end method
