.class public Lcom/google/android/music/ui/cardlib/model/MoreClusterClickHandler;
.super Ljava/lang/Object;
.source "MoreClusterClickHandler.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final mActivity:Landroid/app/Activity;

.field private final mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

.field private final mDocList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/music/ui/cardlib/model/Document;",
            ">;"
        }
    .end annotation
.end field

.field private final mIsEmulateRadio:Z

.field private final mPlayAll:Z

.field private final mTitle:Ljava/lang/String;

.field private final mType:Lcom/google/android/music/ui/cardlib/model/Document$Type;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Ljava/lang/String;Ljava/util/ArrayList;Lcom/google/android/music/ui/cardlib/model/Document$Type;ZZLcom/google/android/music/store/ContainerDescriptor;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "title"    # Ljava/lang/String;
    .param p4, "type"    # Lcom/google/android/music/ui/cardlib/model/Document$Type;
    .param p5, "isEmulateRadio"    # Z
    .param p6, "playAll"    # Z
    .param p7, "containerDescriptor"    # Lcom/google/android/music/store/ContainerDescriptor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/music/ui/cardlib/model/Document;",
            ">;",
            "Lcom/google/android/music/ui/cardlib/model/Document$Type;",
            "ZZ",
            "Lcom/google/android/music/store/ContainerDescriptor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 42
    .local p3, "docList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/music/ui/cardlib/model/Document;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/model/MoreClusterClickHandler;->mActivity:Landroid/app/Activity;

    .line 44
    iput-object p2, p0, Lcom/google/android/music/ui/cardlib/model/MoreClusterClickHandler;->mTitle:Ljava/lang/String;

    .line 45
    iput-object p3, p0, Lcom/google/android/music/ui/cardlib/model/MoreClusterClickHandler;->mDocList:Ljava/util/ArrayList;

    .line 46
    iput-object p4, p0, Lcom/google/android/music/ui/cardlib/model/MoreClusterClickHandler;->mType:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    .line 47
    iput-boolean p5, p0, Lcom/google/android/music/ui/cardlib/model/MoreClusterClickHandler;->mIsEmulateRadio:Z

    .line 48
    iput-boolean p6, p0, Lcom/google/android/music/ui/cardlib/model/MoreClusterClickHandler;->mPlayAll:Z

    .line 49
    iput-object p7, p0, Lcom/google/android/music/ui/cardlib/model/MoreClusterClickHandler;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    .line 50
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/MoreClusterClickHandler;->mDocList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/MoreClusterClickHandler;->mDocList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 55
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/MoreClusterClickHandler;->mActivity:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/music/ui/cardlib/model/MoreClusterClickHandler;->mTitle:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/MoreClusterClickHandler;->mDocList:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/android/music/ui/cardlib/model/MoreClusterClickHandler;->mType:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    iget-boolean v4, p0, Lcom/google/android/music/ui/cardlib/model/MoreClusterClickHandler;->mIsEmulateRadio:Z

    iget-boolean v5, p0, Lcom/google/android/music/ui/cardlib/model/MoreClusterClickHandler;->mPlayAll:Z

    iget-object v6, p0, Lcom/google/android/music/ui/cardlib/model/MoreClusterClickHandler;->mContainerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    invoke-static/range {v0 .. v6}, Lcom/google/android/music/ui/DocumentListActivity;->buildStartIntent(Landroid/content/Context;Ljava/lang/String;Ljava/util/ArrayList;Lcom/google/android/music/ui/cardlib/model/Document$Type;ZZLcom/google/android/music/store/ContainerDescriptor;)Landroid/content/Intent;

    move-result-object v7

    .line 58
    .local v7, "intent":Landroid/content/Intent;
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/MoreClusterClickHandler;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0, v7}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 60
    iget-object v0, p0, Lcom/google/android/music/ui/cardlib/model/MoreClusterClickHandler;->mActivity:Landroid/app/Activity;

    const v1, 0x7f05001e

    const v2, 0x7f05001f

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->overridePendingTransition(II)V

    .line 62
    .end local v7    # "intent":Landroid/content/Intent;
    :cond_0
    return-void
.end method
