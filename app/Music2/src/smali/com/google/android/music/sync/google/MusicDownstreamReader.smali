.class Lcom/google/android/music/sync/google/MusicDownstreamReader;
.super Lcom/google/android/music/sync/common/DownstreamReader;
.source "MusicDownstreamReader.java"


# instance fields
.field private mProtocolState:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private mRemoteAccount:Ljava/lang/String;

.field private final mStore:Lcom/google/android/music/store/Store;


# direct methods
.method public constructor <init>(Lcom/google/android/music/sync/common/AbstractSyncAdapter$DownstreamFetchQueue;ILandroid/content/Context;Ljava/util/Map;Ljava/lang/String;)V
    .locals 2
    .param p1, "fetchQueue"    # Lcom/google/android/music/sync/common/AbstractSyncAdapter$DownstreamFetchQueue;
    .param p2, "queueCapacity"    # I
    .param p3, "context"    # Landroid/content/Context;
    .param p5, "logTag"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/music/sync/common/AbstractSyncAdapter$DownstreamFetchQueue;",
            "I",
            "Landroid/content/Context;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 49
    .local p4, "protocolState":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-direct {p0, p1, p2, p5}, Lcom/google/android/music/sync/common/DownstreamReader;-><init>(Lcom/google/android/music/sync/common/AbstractSyncAdapter$DownstreamFetchQueue;ILjava/lang/String;)V

    .line 50
    invoke-static {p3}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/sync/google/MusicDownstreamReader;->mStore:Lcom/google/android/music/store/Store;

    .line 51
    const-class v0, Ljava/lang/Integer;

    const-string v1, "remote_account"

    invoke-interface {p4, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/sync/google/MusicDownstreamReader;->mRemoteAccount:Ljava/lang/String;

    .line 53
    iput-object p4, p0, Lcom/google/android/music/sync/google/MusicDownstreamReader;->mProtocolState:Ljava/util/Map;

    .line 54
    return-void
.end method


# virtual methods
.method public processServerEntity(Lcom/google/android/music/sync/common/QueueableSyncEntity;)V
    .locals 25
    .param p1, "serverEntity"    # Lcom/google/android/music/sync/common/QueueableSyncEntity;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/sync/common/HardSyncException;,
            Lcom/google/android/music/sync/common/SoftSyncException;
        }
    .end annotation

    .prologue
    .line 59
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/sync/google/MusicDownstreamReader;->mStore:Lcom/google/android/music/store/Store;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/music/store/Store;->beginRead()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 60
    .local v3, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v5, 0x0

    .line 61
    .local v5, "entityFromClient":Lcom/google/android/music/sync/common/QueueableSyncEntity;
    const/4 v8, 0x0

    .line 63
    .local v8, "nautilusTrackFromServer":Lcom/google/android/music/sync/google/model/Track;
    :try_start_0
    move-object/from16 v0, p1

    instance-of v0, v0, Lcom/google/android/music/sync/google/model/Track;

    move/from16 v20, v0

    if-eqz v20, :cond_4

    .line 64
    const-class v20, Lcom/google/android/music/sync/google/model/Track;

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/google/android/music/sync/google/model/Track;

    .line 65
    .local v15, "serverTrack":Lcom/google/android/music/sync/google/model/Track;
    iget-object v13, v15, Lcom/google/android/music/sync/google/model/Track;->mRemoteId:Ljava/lang/String;

    .line 67
    .local v13, "remoteId":Ljava/lang/String;
    const-string v20, "MusicSyncAdapter"

    const/16 v21, 0x2

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v20

    if-eqz v20, :cond_0

    .line 68
    const-string v20, "MusicSyncAdapter"

    const-string v21, "Reader has server track (remoteId=%s"

    const/16 v22, 0x1

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    aput-object v13, v22, v23

    invoke-static/range {v21 .. v22}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 71
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/sync/google/MusicDownstreamReader;->mRemoteAccount:Ljava/lang/String;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-static {v3, v0, v13, v1}, Lcom/google/android/music/store/MusicFile;->readMusicFile(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/music/store/MusicFile;)Lcom/google/android/music/store/MusicFile;

    move-result-object v7

    .line 72
    .local v7, "musicFile":Lcom/google/android/music/store/MusicFile;
    if-eqz v7, :cond_1

    .line 73
    invoke-static {v7}, Lcom/google/android/music/sync/google/model/Track;->parse(Lcom/google/android/music/store/MusicFile;)Lcom/google/android/music/sync/google/model/Track;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v5

    .line 162
    .end local v7    # "musicFile":Lcom/google/android/music/store/MusicFile;
    .end local v13    # "remoteId":Ljava/lang/String;
    .end local v15    # "serverTrack":Lcom/google/android/music/sync/google/model/Track;
    :cond_1
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/sync/google/MusicDownstreamReader;->mStore:Lcom/google/android/music/store/Store;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 167
    if-eqz v8, :cond_2

    .line 175
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/sync/google/MusicDownstreamReader;->mMergeQueue:Lcom/google/android/music/sync/common/AbstractSyncAdapter$DownstreamMergeQueue;

    move-object/from16 v20, v0

    new-instance v21, Lcom/google/android/music/sync/common/AbstractSyncAdapter$DownstreamMergeQueueEntry;

    const/16 v22, 0x0

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-direct {v0, v8, v1}, Lcom/google/android/music/sync/common/AbstractSyncAdapter$DownstreamMergeQueueEntry;-><init>(Lcom/google/android/music/sync/common/QueueableSyncEntity;Lcom/google/android/music/sync/common/QueueableSyncEntity;)V

    invoke-virtual/range {v20 .. v21}, Lcom/google/android/music/sync/common/AbstractSyncAdapter$DownstreamMergeQueue;->put(Ljava/lang/Object;)V

    .line 177
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/sync/google/MusicDownstreamReader;->mMergeQueue:Lcom/google/android/music/sync/common/AbstractSyncAdapter$DownstreamMergeQueue;

    move-object/from16 v20, v0

    new-instance v21, Lcom/google/android/music/sync/common/AbstractSyncAdapter$DownstreamMergeQueueEntry;

    move-object/from16 v0, v21

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v5}, Lcom/google/android/music/sync/common/AbstractSyncAdapter$DownstreamMergeQueueEntry;-><init>(Lcom/google/android/music/sync/common/QueueableSyncEntity;Lcom/google/android/music/sync/common/QueueableSyncEntity;)V

    invoke-virtual/range {v20 .. v21}, Lcom/google/android/music/sync/common/AbstractSyncAdapter$DownstreamMergeQueue;->put(Ljava/lang/Object;)V
    :try_end_1
    .catch Lcom/google/android/music/sync/common/ClosableBlockingQueue$QueueClosedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    .line 186
    :cond_3
    :goto_1
    return-void

    .line 75
    :cond_4
    :try_start_2
    move-object/from16 v0, p1

    instance-of v0, v0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;

    move/from16 v20, v0

    if-eqz v20, :cond_8

    .line 76
    const-class v20, Lcom/google/android/music/sync/google/model/SyncablePlaylist;

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/google/android/music/sync/google/model/SyncablePlaylist;

    move-object/from16 v0, v20

    iget-object v13, v0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mRemoteId:Ljava/lang/String;

    .line 77
    .restart local v13    # "remoteId":Ljava/lang/String;
    const-string v20, "MusicSyncAdapter"

    const/16 v21, 0x2

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v20

    if-eqz v20, :cond_5

    .line 78
    const-string v20, "MusicSyncAdapter"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Reader has server playlist "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/sync/google/MusicDownstreamReader;->mRemoteAccount:Ljava/lang/String;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-static {v3, v0, v13, v1}, Lcom/google/android/music/store/PlayList;->readPlayList(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/music/store/PlayList;)Lcom/google/android/music/store/PlayList;

    move-result-object v10

    .line 81
    .local v10, "playList":Lcom/google/android/music/store/PlayList;
    if-nez v10, :cond_6

    .line 85
    const-class v20, Lcom/google/android/music/sync/google/model/SyncablePlaylist;

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/google/android/music/sync/google/model/SyncablePlaylist;

    .line 86
    .local v19, "syncablePlaylist":Lcom/google/android/music/sync/google/model/SyncablePlaylist;
    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mShareToken:Ljava/lang/String;

    move-object/from16 v16, v0

    .line 87
    .local v16, "shareToken":Ljava/lang/String;
    invoke-static/range {v16 .. v16}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v20

    if-nez v20, :cond_7

    .line 88
    const/16 v20, 0x0

    move-object/from16 v0, v16

    move-object/from16 v1, v20

    invoke-static {v3, v0, v1}, Lcom/google/android/music/store/PlayList;->readPlayList(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Lcom/google/android/music/store/PlayList;)Lcom/google/android/music/store/PlayList;

    move-result-object v10

    .line 94
    .end local v16    # "shareToken":Ljava/lang/String;
    .end local v19    # "syncablePlaylist":Lcom/google/android/music/sync/google/model/SyncablePlaylist;
    :cond_6
    :goto_2
    if-eqz v10, :cond_1

    .line 95
    invoke-static {v10}, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->parse(Lcom/google/android/music/store/PlayList;)Lcom/google/android/music/sync/google/model/SyncablePlaylist;

    move-result-object v5

    goto/16 :goto_0

    .line 90
    .restart local v16    # "shareToken":Ljava/lang/String;
    .restart local v19    # "syncablePlaylist":Lcom/google/android/music/sync/google/model/SyncablePlaylist;
    :cond_7
    const-string v20, "MusicSyncAdapter"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Missing share token for playlist: "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 162
    .end local v10    # "playList":Lcom/google/android/music/store/PlayList;
    .end local v13    # "remoteId":Ljava/lang/String;
    .end local v16    # "shareToken":Ljava/lang/String;
    .end local v19    # "syncablePlaylist":Lcom/google/android/music/sync/google/model/SyncablePlaylist;
    :catchall_0
    move-exception v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/sync/google/MusicDownstreamReader;->mStore:Lcom/google/android/music/store/Store;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    throw v20

    .line 97
    :cond_8
    :try_start_3
    move-object/from16 v0, p1

    instance-of v0, v0, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;

    move/from16 v20, v0

    if-eqz v20, :cond_d

    .line 98
    move-object/from16 v0, p1

    check-cast v0, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;

    move-object v11, v0

    .line 99
    .local v11, "playlistEntry":Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;
    iget-object v13, v11, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mRemoteId:Ljava/lang/String;

    .line 100
    .restart local v13    # "remoteId":Ljava/lang/String;
    const-string v20, "MusicSyncAdapter"

    const/16 v21, 0x2

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v20

    if-eqz v20, :cond_9

    .line 101
    const-string v20, "MusicSyncAdapter"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Reader has server plentry "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/sync/google/MusicDownstreamReader;->mRemoteAccount:Ljava/lang/String;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-static {v3, v0, v13, v1}, Lcom/google/android/music/store/PlayList$Item;->readItem(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/music/store/PlayList$Item;)Lcom/google/android/music/store/PlayList$Item;

    move-result-object v6

    .line 105
    .local v6, "item":Lcom/google/android/music/store/PlayList$Item;
    if-eqz v6, :cond_a

    .line 106
    invoke-virtual {v11}, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->getSource()I

    move-result v20

    invoke-static/range {v20 .. v20}, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->convertServerSourceToClientSourceType(I)I

    move-result v18

    .line 108
    .local v18, "sourceType":I
    iget-object v0, v11, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mTrackId:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move/from16 v1, v18

    invoke-static {v0, v1, v6}, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->parse(Ljava/lang/String;ILcom/google/android/music/store/PlayList$Item;)Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;

    move-result-object v5

    .line 112
    .end local v18    # "sourceType":I
    :cond_a
    invoke-virtual {v11}, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->isDeleted()Z

    move-result v20

    if-nez v20, :cond_b

    .line 113
    invoke-virtual {v11}, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->getTrack()Lcom/google/android/music/sync/google/model/Track;

    move-result-object v8

    .line 115
    :cond_b
    invoke-virtual {v11}, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->getSource()I

    move-result v17

    .line 116
    .local v17, "source":I
    const/16 v20, 0x2

    move/from16 v0, v17

    move/from16 v1, v20

    if-ne v0, v1, :cond_1

    if-eqz v8, :cond_1

    .line 119
    invoke-virtual {v8}, Lcom/google/android/music/sync/google/model/Track;->getNormalizedNautilusId()Ljava/lang/String;

    move-result-object v9

    .line 120
    .local v9, "nid":Ljava/lang/String;
    const-string v20, "MusicSyncAdapter"

    const/16 v21, 0x2

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v20

    if-eqz v20, :cond_c

    .line 121
    const-string v20, "MusicSyncAdapter"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "nautilusTrackFromServer="

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 124
    :cond_c
    const-string v20, "MusicSyncAdapter"

    const/16 v21, 0x2

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v20

    if-eqz v20, :cond_1

    .line 125
    const-string v20, "MusicSyncAdapter"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Reader has server track with nid="

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 129
    .end local v6    # "item":Lcom/google/android/music/store/PlayList$Item;
    .end local v9    # "nid":Ljava/lang/String;
    .end local v11    # "playlistEntry":Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;
    .end local v13    # "remoteId":Ljava/lang/String;
    .end local v17    # "source":I
    :cond_d
    move-object/from16 v0, p1

    instance-of v0, v0, Lcom/google/android/music/sync/google/model/SyncableRadioStation;

    move/from16 v20, v0

    if-eqz v20, :cond_12

    .line 130
    const-class v20, Lcom/google/android/music/sync/google/model/SyncableRadioStation;

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/google/android/music/sync/google/model/SyncableRadioStation;

    move-object/from16 v0, v20

    iget-object v13, v0, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mRemoteId:Ljava/lang/String;

    .line 131
    .restart local v13    # "remoteId":Ljava/lang/String;
    const-string v20, "MusicSyncAdapter"

    const/16 v21, 0x2

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v20

    if-eqz v20, :cond_e

    .line 132
    const-string v20, "MusicSyncAdapter"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Reader has server radio station "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 134
    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/sync/google/MusicDownstreamReader;->mRemoteAccount:Ljava/lang/String;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-static {v3, v0, v13, v1}, Lcom/google/android/music/store/RadioStation;->read(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/music/store/RadioStation;)Lcom/google/android/music/store/RadioStation;

    move-result-object v12

    .line 135
    .local v12, "radioStation":Lcom/google/android/music/store/RadioStation;
    if-nez v12, :cond_10

    .line 139
    const-class v20, Lcom/google/android/music/sync/google/model/SyncableRadioStation;

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/google/android/music/sync/google/model/SyncableRadioStation;

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mSeed:Lcom/google/android/music/sync/google/model/RadioSeed;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/music/sync/google/model/RadioSeed;->getSourceIdAndType()Landroid/util/Pair;

    move-result-object v14

    .line 142
    .local v14, "seed":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Integer;>;"
    iget-object v0, v14, Landroid/util/Pair;->first:Ljava/lang/Object;

    move-object/from16 v20, v0

    check-cast v20, Ljava/lang/CharSequence;

    invoke-static/range {v20 .. v20}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v20

    if-nez v20, :cond_f

    iget-object v0, v14, Landroid/util/Pair;->second:Ljava/lang/Object;

    move-object/from16 v20, v0

    if-nez v20, :cond_11

    .line 143
    :cond_f
    const-string v20, "MusicSyncAdapter"

    const-string v21, "Ignoring radio station %s because of bad seed data: seedId=%s seedType=%s"

    const/16 v22, 0x3

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    aput-object v13, v22, v23

    const/16 v23, 0x1

    iget-object v0, v14, Landroid/util/Pair;->first:Ljava/lang/Object;

    move-object/from16 v24, v0

    aput-object v24, v22, v23

    const/16 v23, 0x2

    iget-object v0, v14, Landroid/util/Pair;->second:Ljava/lang/Object;

    move-object/from16 v24, v0

    aput-object v24, v22, v23

    invoke-static/range {v21 .. v22}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 152
    .end local v14    # "seed":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Integer;>;"
    :cond_10
    :goto_3
    if-eqz v12, :cond_1

    .line 153
    invoke-static {v12}, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->parse(Lcom/google/android/music/store/RadioStation;)Lcom/google/android/music/sync/google/model/SyncableRadioStation;

    move-result-object v5

    goto/16 :goto_0

    .line 148
    .restart local v14    # "seed":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Integer;>;"
    :cond_11
    iget-object v0, v14, Landroid/util/Pair;->first:Ljava/lang/Object;

    move-object/from16 v20, v0

    check-cast v20, Ljava/lang/String;

    iget-object v0, v14, Landroid/util/Pair;->second:Ljava/lang/Object;

    move-object/from16 v21, v0

    check-cast v21, Ljava/lang/Integer;

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Integer;->intValue()I

    move-result v21

    const/16 v22, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v21

    move-object/from16 v2, v22

    invoke-static {v3, v0, v1, v2}, Lcom/google/android/music/store/RadioStation;->readRecommendedStation(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;ILcom/google/android/music/store/RadioStation;)Lcom/google/android/music/store/RadioStation;

    move-result-object v12

    goto :goto_3

    .line 155
    .end local v12    # "radioStation":Lcom/google/android/music/store/RadioStation;
    .end local v13    # "remoteId":Ljava/lang/String;
    .end local v14    # "seed":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Integer;>;"
    :cond_12
    move-object/from16 v0, p1

    instance-of v0, v0, Lcom/google/android/music/sync/google/model/SyncableBlacklistItem;

    move/from16 v20, v0

    if-nez v20, :cond_1

    .line 158
    new-instance v20, Lcom/google/android/music/sync/common/HardSyncException;

    const-string v21, "Received a downstream server entity that is of unknown type.  Fatal error."

    invoke-direct/range {v20 .. v21}, Lcom/google/android/music/sync/common/HardSyncException;-><init>(Ljava/lang/String;)V

    throw v20
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 178
    :catch_0
    move-exception v4

    .line 179
    .local v4, "e":Lcom/google/android/music/sync/common/ClosableBlockingQueue$QueueClosedException;
    const-string v20, "MusicSyncAdapter"

    const/16 v21, 0x2

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v20

    if-eqz v20, :cond_3

    .line 180
    const-string v20, "MusicSyncAdapter"

    const-string v21, "Failed to put an entry into the merge queue.  Bailing."

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 183
    .end local v4    # "e":Lcom/google/android/music/sync/common/ClosableBlockingQueue$QueueClosedException;
    :catch_1
    move-exception v4

    .line 184
    .local v4, "e":Ljava/lang/InterruptedException;
    new-instance v20, Lcom/google/android/music/sync/common/SoftSyncException;

    const-string v21, "Interrupted while putting into merge queue: "

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-direct {v0, v1, v4}, Lcom/google/android/music/sync/common/SoftSyncException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v20
.end method
