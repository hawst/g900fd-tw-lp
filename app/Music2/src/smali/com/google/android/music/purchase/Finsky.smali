.class public Lcom/google/android/music/purchase/Finsky;
.super Ljava/lang/Object;
.source "Finsky.java"


# direct methods
.method public static doesSupportNautilusCancelation(Landroid/content/Context;Lcom/google/android/music/preferences/MusicPreferences;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "prefs"    # Lcom/google/android/music/preferences/MusicPreferences;

    .prologue
    .line 229
    invoke-static {p0}, Lcom/google/android/music/purchase/Finsky;->getPlayStoreVersion(Landroid/content/Context;)I

    move-result v0

    const v1, 0x4c7e850

    if-le v0, v1, :cond_0

    invoke-static {p0, p1}, Lcom/google/android/music/purchase/Finsky;->isDirectPurchaseAvailable(Landroid/content/Context;Lcom/google/android/music/preferences/MusicPreferences;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getBuyAlbumUri(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "albumCompactMetajamId"    # Ljava/lang/String;
    .param p2, "trackCompactMetajamId"    # Ljava/lang/String;

    .prologue
    .line 145
    const-string v1, "https://play.google.com/store/music/"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 146
    .local v0, "builder":Landroid/net/Uri$Builder;
    const-string v1, "album"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 148
    const-string v1, "id"

    invoke-virtual {v0, v1, p1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 151
    if-eqz p2, :cond_0

    .line 152
    const-string v1, "pcampaignid"

    const-string v2, "android_music_buy_track"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 153
    const-string v1, "tid"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "song-"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 158
    :goto_0
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    return-object v1

    .line 155
    :cond_0
    const-string v1, "pcampaignid"

    const-string v2, "android_music_buy_album"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_0
.end method

.method public static getBuyArtistUri(Landroid/content/Context;Ljava/lang/String;)Landroid/net/Uri;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "artistCompactMetajamId"    # Ljava/lang/String;

    .prologue
    .line 168
    const-string v1, "https://play.google.com/store/music/"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 169
    .local v0, "builder":Landroid/net/Uri$Builder;
    const-string v1, "artist"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 170
    const-string v1, "pcampaignid"

    const-string v2, "android_music_buy_artist"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 171
    const-string v1, "id"

    invoke-virtual {v0, v1, p1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 173
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    return-object v1
.end method

.method public static getBuyTrackUri(Landroid/content/Context;Ljava/lang/String;)Landroid/net/Uri;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "trackCompactMetajamId"    # Ljava/lang/String;

    .prologue
    .line 191
    const-string v1, "market://details"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 192
    .local v0, "builder":Landroid/net/Uri$Builder;
    const-string v1, "id"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "song-"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 193
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    return-object v1
.end method

.method private static getPlayStoreVersion(Landroid/content/Context;)I
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 283
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 286
    .local v2, "manager":Landroid/content/pm/PackageManager;
    :try_start_0
    const-string v3, "com.android.vending"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 287
    .local v1, "info":Landroid/content/pm/PackageInfo;
    iget v3, v1, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 290
    .end local v1    # "info":Landroid/content/pm/PackageInfo;
    :goto_0
    return v3

    .line 288
    :catch_0
    move-exception v0

    .line 289
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v3, "MusicFinsky"

    const-string v4, "Failed to get store app version"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 290
    const/4 v3, -0x1

    goto :goto_0
.end method

.method public static isDirectPurchaseAvailable(Landroid/content/Context;Lcom/google/android/music/preferences/MusicPreferences;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "prefs"    # Lcom/google/android/music/preferences/MusicPreferences;

    .prologue
    const/4 v1, 0x0

    .line 224
    invoke-static {p1}, Lcom/google/android/music/purchase/Finsky;->makeDirectPurchaseIntent(Lcom/google/android/music/preferences/MusicPreferences;)Landroid/content/Intent;

    move-result-object v0

    .line 225
    .local v0, "i":Landroid/content/Intent;
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method public static isPlayStoreAvailable(Landroid/content/Context;Lcom/google/android/music/preferences/MusicPreferences;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "prefs"    # Lcom/google/android/music/preferences/MusicPreferences;

    .prologue
    const/4 v1, 0x0

    .line 315
    invoke-static {p1}, Lcom/google/android/music/download/IntentConstants;->getMusicStoreIntent(Lcom/google/android/music/preferences/MusicPreferences;)Landroid/content/Intent;

    move-result-object v0

    .line 316
    .local v0, "shopIntent":Landroid/content/Intent;
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 317
    const/4 v1, 0x1

    .line 319
    :cond_0
    return v1
.end method

.method private static makeCancelNautilusIntent()Landroid/content/Intent;
    .locals 2

    .prologue
    .line 273
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 274
    .local v0, "i":Landroid/content/Intent;
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 275
    const v1, 0x8000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 276
    const/high16 v1, 0x80000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 277
    const-string v1, "https://market.android.com/details?id=com.google.android.music&rdid=com.google.android.music&rdot=1"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 278
    const-string v1, "com.android.vending"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 279
    return-object v0
.end method

.method private static makeDirectPurchaseIntent(Lcom/google/android/music/preferences/MusicPreferences;)Landroid/content/Intent;
    .locals 4
    .param p0, "prefs"    # Lcom/google/android/music/preferences/MusicPreferences;

    .prologue
    .line 197
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.android.vending.billing.PURCHASE"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 198
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "android.intent.category.DEFAULT"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 199
    const-string v2, "com.android.vending"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 200
    const-string v2, "backend"

    const/4 v3, 0x2

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 201
    const-string v2, "offer_type"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 202
    invoke-virtual {p0}, Lcom/google/android/music/preferences/MusicPreferences;->getSyncAccount()Landroid/accounts/Account;

    move-result-object v0

    .line 203
    .local v0, "account":Landroid/accounts/Account;
    if-eqz v0, :cond_0

    .line 204
    const-string v2, "authAccount"

    iget-object v3, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 206
    :cond_0
    return-object v1
.end method

.method private static makeDirectPurchaseIntent(Lcom/google/android/music/preferences/MusicPreferences;ILjava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 3
    .param p0, "prefs"    # Lcom/google/android/music/preferences/MusicPreferences;
    .param p1, "docType"    # I
    .param p2, "docId"    # Ljava/lang/String;
    .param p3, "fullDocId"    # Ljava/lang/String;

    .prologue
    .line 211
    invoke-static {p0}, Lcom/google/android/music/purchase/Finsky;->makeDirectPurchaseIntent(Lcom/google/android/music/preferences/MusicPreferences;)Landroid/content/Intent;

    move-result-object v0

    .line 212
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "document_type"

    const/16 v2, 0xf

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 213
    const-string v1, "backend_docid"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 214
    const-string v1, "full_docid"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 215
    return-object v0
.end method

.method private static makeNautilusPurchaseIntent(Lcom/google/android/music/preferences/MusicPreferences;Ljava/lang/String;)Landroid/content/Intent;
    .locals 3
    .param p0, "prefs"    # Lcom/google/android/music/preferences/MusicPreferences;
    .param p1, "docId"    # Ljava/lang/String;

    .prologue
    .line 219
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "music-subscription_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 220
    .local v0, "fullDocId":Ljava/lang/String;
    const/16 v1, 0xf

    invoke-static {p0, v1, p1, v0}, Lcom/google/android/music/purchase/Finsky;->makeDirectPurchaseIntent(Lcom/google/android/music/preferences/MusicPreferences;ILjava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    return-object v1
.end method

.method public static startBuyAlbumActivity(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "albumMetajamId"    # Ljava/lang/String;

    .prologue
    .line 358
    const/4 v2, 0x0

    invoke-static {p0, p1, v2}, Lcom/google/android/music/purchase/Finsky;->getBuyAlbumUri(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 359
    .local v1, "uri":Landroid/net/Uri;
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v0, v2, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 360
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "com.android.vending"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 361
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 362
    return-void
.end method

.method public static startBuyArtistActivity(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "artistMetajamId"    # Ljava/lang/String;

    .prologue
    .line 371
    invoke-static {p0, p1}, Lcom/google/android/music/purchase/Finsky;->getBuyArtistUri(Landroid/content/Context;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 372
    .local v1, "uri":Landroid/net/Uri;
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v0, v2, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 373
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "com.android.vending"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 374
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 375
    return-void
.end method

.method public static startBuyTrackActivity(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "trackMetajamId"    # Ljava/lang/String;

    .prologue
    .line 330
    invoke-static {p0, p1}, Lcom/google/android/music/purchase/Finsky;->getBuyTrackUri(Landroid/content/Context;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 331
    .local v1, "uri":Landroid/net/Uri;
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v0, v2, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 332
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "com.android.vending"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 333
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 334
    return-void
.end method

.method public static startBuyTrackActivity(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "trackMetajamId"    # Ljava/lang/String;
    .param p2, "albumMetajamId"    # Ljava/lang/String;

    .prologue
    .line 345
    invoke-static {p0, p2, p1}, Lcom/google/android/music/purchase/Finsky;->getBuyAlbumUri(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 346
    .local v1, "uri":Landroid/net/Uri;
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v0, v2, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 347
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "com.android.vending"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 348
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 349
    return-void
.end method

.method public static startCancelNautilusActivity(Landroid/app/Activity;)Z
    .locals 3
    .param p0, "activity"    # Landroid/app/Activity;

    .prologue
    .line 264
    :try_start_0
    invoke-static {}, Lcom/google/android/music/purchase/Finsky;->makeCancelNautilusIntent()Landroid/content/Intent;

    move-result-object v1

    .line 265
    .local v1, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 266
    const/4 v2, 0x1

    .line 268
    .end local v1    # "intent":Landroid/content/Intent;
    :goto_0
    return v2

    .line 267
    :catch_0
    move-exception v0

    .line 268
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static startNautilusPurchaseActivityForResult(Landroid/app/Activity;Lcom/google/android/music/preferences/MusicPreferences;ILjava/lang/String;)Z
    .locals 3
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "prefs"    # Lcom/google/android/music/preferences/MusicPreferences;
    .param p2, "requestCode"    # I
    .param p3, "docId"    # Ljava/lang/String;

    .prologue
    .line 245
    :try_start_0
    invoke-static {p1, p3}, Lcom/google/android/music/purchase/Finsky;->makeNautilusPurchaseIntent(Lcom/google/android/music/preferences/MusicPreferences;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 246
    .local v1, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v1, p2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 247
    const/4 v2, 0x1

    .line 249
    .end local v1    # "intent":Landroid/content/Intent;
    :goto_0
    return v2

    .line 248
    :catch_0
    move-exception v0

    .line 249
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const/4 v2, 0x0

    goto :goto_0
.end method
