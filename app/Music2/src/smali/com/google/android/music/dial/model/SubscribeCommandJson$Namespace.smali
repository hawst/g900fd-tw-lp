.class public Lcom/google/android/music/dial/model/SubscribeCommandJson$Namespace;
.super Lcom/google/api/client/json/GenericJson;
.source "SubscribeCommandJson.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/dial/model/SubscribeCommandJson;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Namespace"
.end annotation


# instance fields
.field public mGroupId:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "groupId"
    .end annotation
.end field

.field public mMaxVersion:I
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "maxVersion"
    .end annotation
.end field

.field public mMinVersion:I
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "minVersion"
    .end annotation
.end field

.field public mName:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "name"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    .line 44
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;IILjava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "minVersion"    # I
    .param p3, "maxVersion"    # I
    .param p4, "groupId"    # Ljava/lang/String;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/google/android/music/dial/model/SubscribeCommandJson$Namespace;->mName:Ljava/lang/String;

    .line 35
    iput p2, p0, Lcom/google/android/music/dial/model/SubscribeCommandJson$Namespace;->mMinVersion:I

    .line 36
    iput p3, p0, Lcom/google/android/music/dial/model/SubscribeCommandJson$Namespace;->mMaxVersion:I

    .line 37
    iput-object p4, p0, Lcom/google/android/music/dial/model/SubscribeCommandJson$Namespace;->mGroupId:Ljava/lang/String;

    .line 38
    return-void
.end method
