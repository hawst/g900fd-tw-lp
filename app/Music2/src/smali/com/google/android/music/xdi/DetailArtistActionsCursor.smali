.class Lcom/google/android/music/xdi/DetailArtistActionsCursor;
.super Landroid/database/MatrixCursor;
.source "DetailArtistActionsCursor.java"


# static fields
.field private static final PROJECTION_ARTISTS:[Ljava/lang/String;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 35
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "artist"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "artworkUrl"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/xdi/DetailArtistActionsCursor;->PROJECTION_ARTISTS:[Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "artistId"    # Ljava/lang/String;
    .param p4, "showArtistDetailsAction"    # Z

    .prologue
    .line 49
    invoke-direct {p0, p2}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 50
    iput-object p1, p0, Lcom/google/android/music/xdi/DetailArtistActionsCursor;->mContext:Landroid/content/Context;

    .line 51
    new-instance v0, Lcom/google/android/music/xdi/ProjectionMap;

    invoke-direct {v0, p2}, Lcom/google/android/music/xdi/ProjectionMap;-><init>([Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/music/xdi/DetailArtistActionsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    .line 53
    invoke-direct {p0, p3, p4}, Lcom/google/android/music/xdi/DetailArtistActionsCursor;->addActions(Ljava/lang/String;Z)V

    .line 54
    return-void
.end method

.method private addActions(Ljava/lang/String;Z)V
    .locals 7
    .param p1, "artistId"    # Ljava/lang/String;
    .param p2, "showArtistDetailsAction"    # Z

    .prologue
    .line 57
    iget-object v5, p0, Lcom/google/android/music/xdi/DetailArtistActionsCursor;->mContext:Landroid/content/Context;

    sget-object v6, Lcom/google/android/music/xdi/DetailArtistActionsCursor;->PROJECTION_ARTISTS:[Ljava/lang/String;

    invoke-static {v5, p1, v6}, Lcom/google/android/music/xdi/XdiUtils;->getArtistCursor(Landroid/content/Context;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .line 59
    .local v3, "cursor":Landroid/database/Cursor;
    if-nez v3, :cond_0

    .line 86
    :goto_0
    return-void

    .line 64
    :cond_0
    :try_start_0
    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 65
    const/4 v5, 0x0

    invoke-interface {v3, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 66
    .local v4, "name":Ljava/lang/String;
    const/4 v2, 0x0

    .line 67
    .local v2, "artUri":Ljava/lang/String;
    const/4 v5, 0x1

    invoke-interface {v3, v5}, Landroid/database/Cursor;->isNull(I)Z

    move-result v5

    if-nez v5, :cond_1

    .line 68
    const/4 v5, 0x1

    invoke-interface {v3, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 70
    :cond_1
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 71
    iget-object v5, p0, Lcom/google/android/music/xdi/DetailArtistActionsCursor;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultArtistArtUri(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 74
    :cond_2
    const/4 v0, 0x0

    .line 76
    .local v0, "actionId":I
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "actionId":I
    .local v1, "actionId":I
    invoke-direct {p0, p1, v4, v0}, Lcom/google/android/music/xdi/DetailArtistActionsCursor;->addShuffleAllSongsAction(Ljava/lang/String;Ljava/lang/String;I)V

    .line 77
    add-int/lit8 v0, v1, 0x1

    .end local v1    # "actionId":I
    .restart local v0    # "actionId":I
    invoke-direct {p0, p1, v4, v1}, Lcom/google/android/music/xdi/DetailArtistActionsCursor;->addStartRadioAction(Ljava/lang/String;Ljava/lang/String;I)V

    .line 78
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "actionId":I
    .restart local v1    # "actionId":I
    invoke-direct {p0, p1, v4, v0}, Lcom/google/android/music/xdi/DetailArtistActionsCursor;->addPlayTopSongsAction(Ljava/lang/String;Ljava/lang/String;I)V

    .line 79
    if-eqz p2, :cond_3

    .line 80
    add-int/lit8 v0, v1, 0x1

    .end local v1    # "actionId":I
    .restart local v0    # "actionId":I
    invoke-direct {p0, p1, v4, v1}, Lcom/google/android/music/xdi/DetailArtistActionsCursor;->addShowArtistDetailAction(Ljava/lang/String;Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 84
    .end local v0    # "actionId":I
    .end local v2    # "artUri":Ljava/lang/String;
    .end local v4    # "name":Ljava/lang/String;
    :cond_3
    invoke-static {v3}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto :goto_0

    :catchall_0
    move-exception v5

    invoke-static {v3}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v5
.end method

.method private addPlayTopSongsAction(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 16
    .param p1, "artistId"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "actionId"    # I

    .prologue
    .line 112
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/music/xdi/DetailArtistActionsCursor;->mContext:Landroid/content/Context;

    invoke-static {v13}, Lcom/google/android/music/xdi/XdiUtils;->isNautilusEnabled(Landroid/content/Context;)Z

    move-result v13

    if-nez v13, :cond_1

    .line 159
    :cond_0
    :goto_0
    return-void

    .line 115
    :cond_1
    invoke-static/range {p1 .. p1}, Lcom/google/android/music/utils/MusicUtils;->isNautilusId(Ljava/lang/String;)Z

    move-result v8

    .line 118
    .local v8, "isNautilusId":Z
    const-wide/16 v2, 0x0

    .line 119
    .local v2, "artistIdLong":J
    if-eqz v8, :cond_2

    .line 120
    move-object/from16 v4, p1

    .line 132
    .local v4, "artistMetajamId":Ljava/lang/String;
    :goto_1
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-nez v13, :cond_0

    .line 136
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/music/xdi/DetailArtistActionsCursor;->mContext:Landroid/content/Context;

    invoke-static {v13, v4}, Lcom/google/android/music/xdi/XdiUtils;->getTopSongsByArtist(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v10

    .line 137
    .local v10, "songDocs":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/ui/cardlib/model/Document;>;"
    invoke-interface {v10}, Ljava/util/List;->isEmpty()Z

    move-result v13

    if-eqz v13, :cond_3

    .line 138
    const-string v13, "MusicXdi"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "No top songs found for nautilus artist "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 123
    .end local v4    # "artistMetajamId":Ljava/lang/String;
    .end local v10    # "songDocs":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/ui/cardlib/model/Document;>;"
    :cond_2
    :try_start_0
    invoke-static/range {p1 .. p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 129
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/music/xdi/DetailArtistActionsCursor;->mContext:Landroid/content/Context;

    invoke-static {v13, v2, v3}, Lcom/google/android/music/xdi/XdiUtils;->getArtistMetajamId(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v4

    .restart local v4    # "artistMetajamId":Ljava/lang/String;
    goto :goto_1

    .line 124
    .end local v4    # "artistMetajamId":Ljava/lang/String;
    :catch_0
    move-exception v6

    .line 125
    .local v6, "e":Ljava/lang/NumberFormatException;
    const-string v13, "MusicXdi"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Error converting artist id to long: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 141
    .end local v6    # "e":Ljava/lang/NumberFormatException;
    .restart local v4    # "artistMetajamId":Ljava/lang/String;
    .restart local v10    # "songDocs":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/ui/cardlib/model/Document;>;"
    :cond_3
    invoke-static {v10}, Lcom/google/android/music/xdi/XdiUtils;->extractTrackMetajamIds(Ljava/util/List;)Ljava/util/List;

    move-result-object v12

    .line 142
    .local v12, "songIdsList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v13

    new-array v11, v13, [Ljava/lang/String;

    .line 143
    .local v11, "songIds":[Ljava/lang/String;
    invoke-interface {v12, v11}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 145
    invoke-static {}, Lcom/google/android/music/store/ContainerDescriptor;->newUnknownContainerDescriptor()Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v5

    .line 147
    .local v5, "descriptor":Lcom/google/android/music/store/ContainerDescriptor;
    new-instance v13, Lcom/google/android/music/medialist/NautilusSelectedSongList;

    invoke-direct {v13, v5, v11}, Lcom/google/android/music/medialist/NautilusSelectedSongList;-><init>(Lcom/google/android/music/store/ContainerDescriptor;[Ljava/lang/String;)V

    invoke-virtual {v13}, Lcom/google/android/music/medialist/NautilusSelectedSongList;->freeze()Ljava/lang/String;

    move-result-object v9

    .line 149
    .local v9, "selectedSongList":Ljava/lang/String;
    invoke-static {}, Lcom/google/android/music/xdi/XdiUtils;->newPanoPlayIntent()Landroid/content/Intent;

    move-result-object v13

    const-string v14, "container"

    const/16 v15, 0xc

    invoke-virtual {v13, v14, v15}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v13

    const-string v14, "song_list"

    invoke-virtual {v13, v14, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v13

    const-string v14, "offset"

    const/4 v15, 0x0

    invoke-virtual {v13, v14, v15}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v7

    .line 154
    .local v7, "intent":Landroid/content/Intent;
    const-string v13, "name"

    move-object/from16 v0, p2

    invoke-virtual {v7, v13, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 155
    const-string v13, "play_mode"

    const-string v14, "play"

    invoke-virtual {v7, v13, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 157
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/music/xdi/DetailArtistActionsCursor;->mContext:Landroid/content/Context;

    const v14, 0x7f0b02d5

    invoke-virtual {v13, v14}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/music/xdi/DetailArtistActionsCursor;->mContext:Landroid/content/Context;

    const v15, 0x7f0b02d7

    invoke-virtual {v14, v15}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    move/from16 v1, p3

    invoke-direct {v0, v13, v14, v1, v7}, Lcom/google/android/music/xdi/DetailArtistActionsCursor;->addRow(Ljava/lang/String;Ljava/lang/String;ILandroid/content/Intent;)V

    goto/16 :goto_0
.end method

.method private addRow(Ljava/lang/String;Ljava/lang/String;ILandroid/content/Intent;)V
    .locals 4
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "subName"    # Ljava/lang/String;
    .param p3, "actionId"    # I
    .param p4, "intent"    # Landroid/content/Intent;

    .prologue
    .line 190
    iget-object v1, p0, Lcom/google/android/music/xdi/DetailArtistActionsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    invoke-virtual {v1}, Lcom/google/android/music/xdi/ProjectionMap;->size()I

    move-result v1

    new-array v0, v1, [Ljava/lang/Object;

    .line 191
    .local v0, "values":[Ljava/lang/Object;
    iget-object v1, p0, Lcom/google/android/music/xdi/DetailArtistActionsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v2, "_id"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 192
    iget-object v1, p0, Lcom/google/android/music/xdi/DetailArtistActionsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v2, "display_name"

    invoke-virtual {v1, v0, v2, p1}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 193
    iget-object v1, p0, Lcom/google/android/music/xdi/DetailArtistActionsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v2, "display_subname"

    invoke-virtual {v1, v0, v2, p2}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 194
    iget-object v1, p0, Lcom/google/android/music/xdi/DetailArtistActionsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v2, "intent_uri"

    const/4 v3, 0x1

    invoke-virtual {p4, v3}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 196
    invoke-virtual {p0, v0}, Lcom/google/android/music/xdi/DetailArtistActionsCursor;->addRow([Ljava/lang/Object;)V

    .line 197
    return-void
.end method

.method private addShowArtistDetailAction(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 4
    .param p1, "artistId"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "actionId"    # I

    .prologue
    .line 183
    invoke-static {p1}, Lcom/google/android/music/xdi/XdiUtils;->getArtistDetailsIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 185
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/google/android/music/xdi/DetailArtistActionsCursor;->mContext:Landroid/content/Context;

    const v2, 0x7f0b02d4

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/xdi/DetailArtistActionsCursor;->mContext:Landroid/content/Context;

    const v3, 0x7f0b02d9

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v2, p3, v0}, Lcom/google/android/music/xdi/DetailArtistActionsCursor;->addRow(Ljava/lang/String;Ljava/lang/String;ILandroid/content/Intent;)V

    .line 187
    return-void
.end method

.method private addShuffleAllSongsAction(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 5
    .param p1, "artistId"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "actionId"    # I

    .prologue
    .line 162
    invoke-static {p1}, Lcom/google/android/music/utils/MusicUtils;->isNautilusId(Ljava/lang/String;)Z

    move-result v1

    .line 164
    .local v1, "isNautilusArtistId":Z
    invoke-static {}, Lcom/google/android/music/xdi/XdiUtils;->newPanoPlayIntent()Landroid/content/Intent;

    move-result-object v0

    .line 165
    .local v0, "intent":Landroid/content/Intent;
    if-eqz v1, :cond_0

    .line 166
    const-string v2, "container"

    const/16 v3, 0x11

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 168
    const-string v2, "id_string"

    invoke-virtual {v0, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 174
    :goto_0
    const-string v2, "name"

    invoke-virtual {v0, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 175
    const-string v2, "play_mode"

    const-string v3, "shuffle"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 177
    iget-object v2, p0, Lcom/google/android/music/xdi/DetailArtistActionsCursor;->mContext:Landroid/content/Context;

    const v3, 0x7f0b0056

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/music/xdi/DetailArtistActionsCursor;->mContext:Landroid/content/Context;

    const v4, 0x7f0b02d8

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v2, v3, p3, v0}, Lcom/google/android/music/xdi/DetailArtistActionsCursor;->addRow(Ljava/lang/String;Ljava/lang/String;ILandroid/content/Intent;)V

    .line 179
    return-void

    .line 170
    :cond_0
    const-string v2, "container"

    const/16 v3, 0x10

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 172
    const-string v2, "id"

    invoke-static {p1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    goto :goto_0
.end method

.method private addStartRadioAction(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 5
    .param p1, "artistId"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "actionId"    # I

    .prologue
    .line 89
    invoke-static {p1}, Lcom/google/android/music/utils/MusicUtils;->isNautilusId(Ljava/lang/String;)Z

    move-result v2

    .line 91
    .local v2, "isNautilusArtistId":Z
    invoke-static {}, Lcom/google/android/music/xdi/XdiUtils;->newPanoPlayIntent()Landroid/content/Intent;

    move-result-object v1

    .line 92
    .local v1, "intent":Landroid/content/Intent;
    if-eqz v2, :cond_0

    .line 93
    const-string v3, "container"

    const/16 v4, 0x8

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 95
    const-string v3, "id_string"

    invoke-virtual {v1, v3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 101
    :goto_0
    const-string v3, "name"

    invoke-virtual {v1, v3, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 103
    iget-object v3, p0, Lcom/google/android/music/xdi/DetailArtistActionsCursor;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/google/android/music/xdi/XdiUtils;->isNautilusEnabled(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/music/xdi/DetailArtistActionsCursor;->mContext:Landroid/content/Context;

    const v4, 0x7f0b023a

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 106
    .local v0, "actionTitle":Ljava/lang/String;
    :goto_1
    iget-object v3, p0, Lcom/google/android/music/xdi/DetailArtistActionsCursor;->mContext:Landroid/content/Context;

    const v4, 0x7f0b02d6

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v0, v3, p3, v1}, Lcom/google/android/music/xdi/DetailArtistActionsCursor;->addRow(Ljava/lang/String;Ljava/lang/String;ILandroid/content/Intent;)V

    .line 107
    return-void

    .line 97
    .end local v0    # "actionTitle":Ljava/lang/String;
    :cond_0
    const-string v3, "container"

    const/16 v4, 0xd

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 99
    const-string v3, "id"

    invoke-static {p1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    goto :goto_0

    .line 103
    :cond_1
    iget-object v3, p0, Lcom/google/android/music/xdi/DetailArtistActionsCursor;->mContext:Landroid/content/Context;

    const v4, 0x7f0b023b

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method
