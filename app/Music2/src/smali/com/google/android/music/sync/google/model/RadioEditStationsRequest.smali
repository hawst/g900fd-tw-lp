.class public Lcom/google/android/music/sync/google/model/RadioEditStationsRequest;
.super Lcom/google/api/client/json/GenericJson;
.source "RadioEditStationsRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/sync/google/model/RadioEditStationsRequest$UserFeedParams;,
        Lcom/google/android/music/sync/google/model/RadioEditStationsRequest$MutateRadioStationRequest;
    }
.end annotation


# instance fields
.field public mMutableRadioStationRequests:Ljava/util/List;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "mutations"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/sync/google/model/RadioEditStationsRequest$MutateRadioStationRequest;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    .line 44
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/sync/google/model/RadioEditStationsRequest;->mMutableRadioStationRequests:Ljava/util/List;

    return-void
.end method

.method public static serialize(Lcom/google/android/music/sync/google/model/SyncableRadioStation;ZII)[B
    .locals 3
    .param p0, "syncableRadioStation"    # Lcom/google/android/music/sync/google/model/SyncableRadioStation;
    .param p1, "includeFeed"    # Z
    .param p2, "maxEntries"    # I
    .param p3, "contentFilter"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 56
    new-instance v1, Lcom/google/android/music/sync/google/model/RadioEditStationsRequest;

    invoke-direct {v1}, Lcom/google/android/music/sync/google/model/RadioEditStationsRequest;-><init>()V

    .line 58
    .local v1, "request":Lcom/google/android/music/sync/google/model/RadioEditStationsRequest;
    new-instance v0, Lcom/google/android/music/sync/google/model/RadioEditStationsRequest$MutateRadioStationRequest;

    invoke-direct {v0}, Lcom/google/android/music/sync/google/model/RadioEditStationsRequest$MutateRadioStationRequest;-><init>()V

    .line 59
    .local v0, "mutation":Lcom/google/android/music/sync/google/model/RadioEditStationsRequest$MutateRadioStationRequest;
    iput-object p0, v0, Lcom/google/android/music/sync/google/model/RadioEditStationsRequest$MutateRadioStationRequest;->mCreateRadioStation:Lcom/google/android/music/sync/google/model/SyncableRadioStation;

    .line 60
    iput-boolean p1, v0, Lcom/google/android/music/sync/google/model/RadioEditStationsRequest$MutateRadioStationRequest;->mIncludeFeed:Z

    .line 61
    iput p2, v0, Lcom/google/android/music/sync/google/model/RadioEditStationsRequest$MutateRadioStationRequest;->mNumEntries:I

    .line 62
    new-instance v2, Lcom/google/android/music/sync/google/model/RadioEditStationsRequest$UserFeedParams;

    invoke-direct {v2}, Lcom/google/android/music/sync/google/model/RadioEditStationsRequest$UserFeedParams;-><init>()V

    iput-object v2, v0, Lcom/google/android/music/sync/google/model/RadioEditStationsRequest$MutateRadioStationRequest;->mUserFeedParams:Lcom/google/android/music/sync/google/model/RadioEditStationsRequest$UserFeedParams;

    .line 63
    iget-object v2, v0, Lcom/google/android/music/sync/google/model/RadioEditStationsRequest$MutateRadioStationRequest;->mUserFeedParams:Lcom/google/android/music/sync/google/model/RadioEditStationsRequest$UserFeedParams;

    iput p3, v2, Lcom/google/android/music/sync/google/model/RadioEditStationsRequest$UserFeedParams;->mContentFilter:I

    .line 65
    iget-object v2, v1, Lcom/google/android/music/sync/google/model/RadioEditStationsRequest;->mMutableRadioStationRequests:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 67
    invoke-static {v1}, Lcom/google/android/music/cloudclient/JsonUtils;->toJsonByteArray(Lcom/google/api/client/json/GenericJson;)[B

    move-result-object v2

    return-object v2
.end method


# virtual methods
.method public addRequest(Lcom/google/android/music/sync/google/model/RadioEditStationsRequest$MutateRadioStationRequest;)V
    .locals 1
    .param p1, "request"    # Lcom/google/android/music/sync/google/model/RadioEditStationsRequest$MutateRadioStationRequest;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/music/sync/google/model/RadioEditStationsRequest;->mMutableRadioStationRequests:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 51
    return-void
.end method
