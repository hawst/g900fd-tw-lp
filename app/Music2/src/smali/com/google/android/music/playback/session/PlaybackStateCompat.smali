.class public final Lcom/google/android/music/playback/session/PlaybackStateCompat;
.super Ljava/lang/Object;
.source "PlaybackStateCompat.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/playback/session/PlaybackStateCompat$CustomAction;,
        Lcom/google/android/music/playback/session/PlaybackStateCompat$Builder;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/music/playback/session/PlaybackStateCompat;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mActions:J

.field private final mActiveItemId:J

.field private final mBufferedPosition:J

.field private mCustomActions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/playback/session/PlaybackStateCompat$CustomAction;",
            ">;"
        }
    .end annotation
.end field

.field private final mErrorMessage:Ljava/lang/CharSequence;

.field private final mPosition:J

.field private final mSpeed:F

.field private final mState:I

.field private mStateObj:Ljava/lang/Object;

.field private final mUpdateTime:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 407
    new-instance v0, Lcom/google/android/music/playback/session/PlaybackStateCompat$1;

    invoke-direct {v0}, Lcom/google/android/music/playback/session/PlaybackStateCompat$1;-><init>()V

    sput-object v0, Lcom/google/android/music/playback/session/PlaybackStateCompat;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(IJJFJJLjava/util/List;JLjava/lang/CharSequence;)V
    .locals 6
    .param p1, "state"    # I
    .param p2, "position"    # J
    .param p4, "updateTime"    # J
    .param p6, "speed"    # F
    .param p7, "bufferedPosition"    # J
    .param p9, "transportControls"    # J
    .param p12, "activeItemId"    # J
    .param p14, "error"    # Ljava/lang/CharSequence;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IJJFJJ",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/playback/session/PlaybackStateCompat$CustomAction;",
            ">;J",
            "Ljava/lang/CharSequence;",
            ")V"
        }
    .end annotation

    .prologue
    .line 216
    .local p11, "customActions":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/playback/session/PlaybackStateCompat$CustomAction;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 217
    iput p1, p0, Lcom/google/android/music/playback/session/PlaybackStateCompat;->mState:I

    .line 218
    iput-wide p2, p0, Lcom/google/android/music/playback/session/PlaybackStateCompat;->mPosition:J

    .line 219
    iput p6, p0, Lcom/google/android/music/playback/session/PlaybackStateCompat;->mSpeed:F

    .line 220
    iput-wide p4, p0, Lcom/google/android/music/playback/session/PlaybackStateCompat;->mUpdateTime:J

    .line 221
    iput-wide p7, p0, Lcom/google/android/music/playback/session/PlaybackStateCompat;->mBufferedPosition:J

    .line 222
    iput-wide p9, p0, Lcom/google/android/music/playback/session/PlaybackStateCompat;->mActions:J

    .line 223
    new-instance v2, Ljava/util/ArrayList;

    move-object/from16 v0, p11

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/android/music/playback/session/PlaybackStateCompat;->mCustomActions:Ljava/util/List;

    .line 224
    move-wide/from16 v0, p12

    iput-wide v0, p0, Lcom/google/android/music/playback/session/PlaybackStateCompat;->mActiveItemId:J

    .line 225
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/google/android/music/playback/session/PlaybackStateCompat;->mErrorMessage:Ljava/lang/CharSequence;

    .line 226
    return-void
.end method

.method synthetic constructor <init>(IJJFJJLjava/util/List;JLjava/lang/CharSequence;Lcom/google/android/music/playback/session/PlaybackStateCompat$1;)V
    .locals 0
    .param p1, "x0"    # I
    .param p2, "x1"    # J
    .param p4, "x2"    # J
    .param p6, "x3"    # F
    .param p7, "x4"    # J
    .param p9, "x5"    # J
    .param p11, "x6"    # Ljava/util/List;
    .param p12, "x7"    # J
    .param p14, "x8"    # Ljava/lang/CharSequence;
    .param p15, "x9"    # Lcom/google/android/music/playback/session/PlaybackStateCompat$1;

    .prologue
    .line 21
    invoke-direct/range {p0 .. p14}, Lcom/google/android/music/playback/session/PlaybackStateCompat;-><init>(IJJFJJLjava/util/List;JLjava/lang/CharSequence;)V

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 228
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 229
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/music/playback/session/PlaybackStateCompat;->mState:I

    .line 230
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/music/playback/session/PlaybackStateCompat;->mPosition:J

    .line 231
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/google/android/music/playback/session/PlaybackStateCompat;->mSpeed:F

    .line 232
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/music/playback/session/PlaybackStateCompat;->mUpdateTime:J

    .line 233
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/music/playback/session/PlaybackStateCompat;->mBufferedPosition:J

    .line 234
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/music/playback/session/PlaybackStateCompat;->mActions:J

    .line 235
    sget-object v0, Lcom/google/android/music/playback/session/PlaybackStateCompat$CustomAction;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/playback/session/PlaybackStateCompat;->mCustomActions:Ljava/util/List;

    .line 236
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/music/playback/session/PlaybackStateCompat;->mActiveItemId:J

    .line 237
    sget-object v0, Landroid/text/TextUtils;->CHAR_SEQUENCE_CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    iput-object v0, p0, Lcom/google/android/music/playback/session/PlaybackStateCompat;->mErrorMessage:Ljava/lang/CharSequence;

    .line 238
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/google/android/music/playback/session/PlaybackStateCompat$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/google/android/music/playback/session/PlaybackStateCompat$1;

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lcom/google/android/music/playback/session/PlaybackStateCompat;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 258
    const/4 v0, 0x0

    return v0
.end method

.method public getActions()J
    .locals 2

    .prologue
    .line 333
    iget-wide v0, p0, Lcom/google/android/music/playback/session/PlaybackStateCompat;->mActions:J

    return-wide v0
.end method

.method public getPlaybackSpeed()F
    .locals 1

    .prologue
    .line 314
    iget v0, p0, Lcom/google/android/music/playback/session/PlaybackStateCompat;->mSpeed:F

    return v0
.end method

.method public getPlaybackState()Ljava/lang/Object;
    .locals 15

    .prologue
    .line 398
    iget-object v0, p0, Lcom/google/android/music/playback/session/PlaybackStateCompat;->mStateObj:Ljava/lang/Object;

    if-nez v0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-ge v0, v1, :cond_1

    .line 399
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/playback/session/PlaybackStateCompat;->mStateObj:Ljava/lang/Object;

    .line 404
    :goto_0
    return-object v0

    .line 402
    :cond_1
    iget v1, p0, Lcom/google/android/music/playback/session/PlaybackStateCompat;->mState:I

    iget-wide v2, p0, Lcom/google/android/music/playback/session/PlaybackStateCompat;->mPosition:J

    iget-wide v4, p0, Lcom/google/android/music/playback/session/PlaybackStateCompat;->mUpdateTime:J

    iget v6, p0, Lcom/google/android/music/playback/session/PlaybackStateCompat;->mSpeed:F

    iget-wide v7, p0, Lcom/google/android/music/playback/session/PlaybackStateCompat;->mBufferedPosition:J

    iget-wide v9, p0, Lcom/google/android/music/playback/session/PlaybackStateCompat;->mActions:J

    iget-object v11, p0, Lcom/google/android/music/playback/session/PlaybackStateCompat;->mCustomActions:Ljava/util/List;

    iget-wide v12, p0, Lcom/google/android/music/playback/session/PlaybackStateCompat;->mActiveItemId:J

    iget-object v14, p0, Lcom/google/android/music/playback/session/PlaybackStateCompat;->mErrorMessage:Ljava/lang/CharSequence;

    invoke-static/range {v1 .. v14}, Lcom/google/android/music/playback/session/PlaybackStateCompatApi21;->newInstance(IJJFJJLjava/util/List;JLjava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/playback/session/PlaybackStateCompat;->mStateObj:Ljava/lang/Object;

    .line 404
    iget-object v0, p0, Lcom/google/android/music/playback/session/PlaybackStateCompat;->mStateObj:Ljava/lang/Object;

    goto :goto_0
.end method

.method public getPosition()J
    .locals 2

    .prologue
    .line 294
    iget-wide v0, p0, Lcom/google/android/music/playback/session/PlaybackStateCompat;->mPosition:J

    return-wide v0
.end method

.method public getState()I
    .locals 1

    .prologue
    .line 287
    iget v0, p0, Lcom/google/android/music/playback/session/PlaybackStateCompat;->mState:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 242
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "PlaybackState {"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 243
    .local v0, "bob":Ljava/lang/StringBuilder;
    const-string v1, "state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/music/playback/session/PlaybackStateCompat;->mState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 244
    const-string v1, ", position="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/music/playback/session/PlaybackStateCompat;->mPosition:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 245
    const-string v1, ", buffered position="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/music/playback/session/PlaybackStateCompat;->mBufferedPosition:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 246
    const-string v1, ", speed="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/music/playback/session/PlaybackStateCompat;->mSpeed:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 247
    const-string v1, ", updated="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/music/playback/session/PlaybackStateCompat;->mUpdateTime:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 248
    const-string v1, ", actions="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/music/playback/session/PlaybackStateCompat;->mActions:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 249
    const-string v1, ", custom actions="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/playback/session/PlaybackStateCompat;->mCustomActions:Ljava/util/List;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 250
    const-string v1, ", active item id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/music/playback/session/PlaybackStateCompat;->mActiveItemId:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 251
    const-string v1, ", error="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/playback/session/PlaybackStateCompat;->mErrorMessage:Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 252
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 253
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 263
    iget v0, p0, Lcom/google/android/music/playback/session/PlaybackStateCompat;->mState:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 264
    iget-wide v0, p0, Lcom/google/android/music/playback/session/PlaybackStateCompat;->mPosition:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 265
    iget v0, p0, Lcom/google/android/music/playback/session/PlaybackStateCompat;->mSpeed:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 266
    iget-wide v0, p0, Lcom/google/android/music/playback/session/PlaybackStateCompat;->mUpdateTime:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 267
    iget-wide v0, p0, Lcom/google/android/music/playback/session/PlaybackStateCompat;->mBufferedPosition:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 268
    iget-wide v0, p0, Lcom/google/android/music/playback/session/PlaybackStateCompat;->mActions:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 269
    iget-object v0, p0, Lcom/google/android/music/playback/session/PlaybackStateCompat;->mCustomActions:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 270
    iget-wide v0, p0, Lcom/google/android/music/playback/session/PlaybackStateCompat;->mActiveItemId:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 271
    iget-object v0, p0, Lcom/google/android/music/playback/session/PlaybackStateCompat;->mErrorMessage:Ljava/lang/CharSequence;

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Landroid/text/TextUtils;->writeToParcel(Ljava/lang/CharSequence;Landroid/os/Parcel;I)V

    .line 272
    return-void
.end method
