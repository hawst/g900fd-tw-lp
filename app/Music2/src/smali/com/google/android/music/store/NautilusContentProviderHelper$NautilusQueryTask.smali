.class Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryTask;
.super Ljava/lang/Object;
.source "NautilusContentProviderHelper.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/store/NautilusContentProviderHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "NautilusQueryTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryResult;",
        ">;"
    }
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field private mNautilusId:Ljava/lang/String;

.field private mProjection:[Ljava/lang/String;

.field private mUri:Landroid/net/Uri;

.field private mUriType:I


# direct methods
.method public constructor <init>(Landroid/content/Context;ILandroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "type"    # I
    .param p3, "uri"    # Landroid/net/Uri;
    .param p4, "nid"    # Ljava/lang/String;
    .param p5, "projection"    # [Ljava/lang/String;

    .prologue
    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 91
    iput-object p1, p0, Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryTask;->mContext:Landroid/content/Context;

    .line 92
    iput p2, p0, Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryTask;->mUriType:I

    .line 93
    iput-object p3, p0, Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryTask;->mUri:Landroid/net/Uri;

    .line 94
    iput-object p4, p0, Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryTask;->mNautilusId:Ljava/lang/String;

    .line 95
    iput-object p5, p0, Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryTask;->mProjection:[Ljava/lang/String;

    .line 96
    return-void
.end method


# virtual methods
.method public call()Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryResult;
    .locals 23
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 100
    new-instance v12, Lcom/google/android/music/store/TagNormalizer;

    invoke-direct {v12}, Lcom/google/android/music/store/TagNormalizer;-><init>()V

    .line 101
    .local v12, "normalizer":Lcom/google/android/music/store/TagNormalizer;
    new-instance v5, Landroid/database/MatrixCursor;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryTask;->mProjection:[Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-direct {v5, v0}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 102
    .local v5, "cursor":Landroid/database/MatrixCursor;
    const-wide/16 v10, -0x1

    .line 104
    .local v10, "localId":J
    new-instance v6, Ljava/util/LinkedList;

    invoke-direct {v6}, Ljava/util/LinkedList;-><init>()V

    .line 106
    .local v6, "excludeList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryTask;->mUriType:I

    move/from16 v20, v0

    sparse-switch v20, :sswitch_data_0

    .line 289
    :cond_0
    :goto_0
    if-eqz v5, :cond_1

    .line 292
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryTask;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryTask;->mUri:Landroid/net/Uri;

    move-object/from16 v21, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v5, v0, v1}, Landroid/database/MatrixCursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    .line 293
    invoke-virtual {v5}, Landroid/database/MatrixCursor;->getCount()I

    .line 295
    :cond_1
    new-instance v20, Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryResult;

    move-object/from16 v0, v20

    invoke-direct {v0, v5, v10, v11, v6}, Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryResult;-><init>(Landroid/database/Cursor;JLjava/util/List;)V

    return-object v20

    .line 108
    :sswitch_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryTask;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryTask;->mNautilusId:Ljava/lang/String;

    move-object/from16 v21, v0

    # invokes: Lcom/google/android/music/store/NautilusContentProviderHelper;->getTrack(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/music/sync/google/model/Track;
    invoke-static/range {v20 .. v21}, Lcom/google/android/music/store/NautilusContentProviderHelper;->access$000(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/music/sync/google/model/Track;

    move-result-object v17

    .line 109
    .local v17, "track":Lcom/google/android/music/sync/google/model/Track;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryTask;->mProjection:[Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-static {v0, v1}, Lcom/google/android/music/store/ProjectionUtils;->project(Lcom/google/android/music/cloudclient/TrackJson;[Ljava/lang/String;)[Ljava/lang/Object;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v5, v0}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 110
    move-object/from16 v0, v17

    invoke-static {v12, v0}, Lcom/google/android/music/store/NautilusContentProviderHelper;->generateLocalId(Lcom/google/android/music/store/TagNormalizer;Lcom/google/android/music/sync/google/model/Track;)J

    move-result-wide v10

    .line 111
    goto :goto_0

    .line 114
    .end local v17    # "track":Lcom/google/android/music/sync/google/model/Track;
    :sswitch_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryTask;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryTask;->mNautilusId:Ljava/lang/String;

    move-object/from16 v21, v0

    # invokes: Lcom/google/android/music/store/NautilusContentProviderHelper;->getAlbum(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/music/cloudclient/AlbumJson;
    invoke-static/range {v20 .. v21}, Lcom/google/android/music/store/NautilusContentProviderHelper;->access$100(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/music/cloudclient/AlbumJson;

    move-result-object v2

    .line 115
    .local v2, "album":Lcom/google/android/music/cloudclient/AlbumJson;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryTask;->mProjection:[Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/google/android/music/store/ProjectionUtils;->isHasDifferentArtistProjection([Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_5

    .line 117
    const/4 v7, 0x0

    .line 118
    .local v7, "hasDifferent":Z
    const/4 v13, 0x0

    .line 119
    .local v13, "prevArtist":Ljava/lang/String;
    iget-object v0, v2, Lcom/google/android/music/cloudclient/AlbumJson;->mTracks:Ljava/util/List;

    move-object/from16 v20, v0

    invoke-interface/range {v20 .. v20}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-eqz v20, :cond_2

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/google/android/music/sync/google/model/Track;

    .line 121
    .restart local v17    # "track":Lcom/google/android/music/sync/google/model/Track;
    if-eqz v13, :cond_3

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/google/android/music/sync/google/model/Track;->mArtist:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v13, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-nez v20, :cond_3

    .line 122
    const/4 v7, 0x1

    .line 127
    .end local v17    # "track":Lcom/google/android/music/sync/google/model/Track;
    :cond_2
    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    if-eqz v7, :cond_4

    const/16 v20, 0x1

    :goto_2
    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    aput-object v20, v21, v22

    move-object/from16 v0, v21

    invoke-virtual {v5, v0}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 131
    .end local v7    # "hasDifferent":Z
    .end local v8    # "i$":Ljava/util/Iterator;
    .end local v13    # "prevArtist":Ljava/lang/String;
    :goto_3
    invoke-static {v12, v2}, Lcom/google/android/music/store/NautilusContentProviderHelper;->generateLocalId(Lcom/google/android/music/store/TagNormalizer;Lcom/google/android/music/cloudclient/AlbumJson;)J

    move-result-wide v10

    .line 132
    goto/16 :goto_0

    .line 125
    .restart local v7    # "hasDifferent":Z
    .restart local v8    # "i$":Ljava/util/Iterator;
    .restart local v13    # "prevArtist":Ljava/lang/String;
    .restart local v17    # "track":Lcom/google/android/music/sync/google/model/Track;
    :cond_3
    move-object/from16 v0, v17

    iget-object v13, v0, Lcom/google/android/music/sync/google/model/Track;->mArtist:Ljava/lang/String;

    .line 126
    goto :goto_1

    .line 127
    .end local v17    # "track":Lcom/google/android/music/sync/google/model/Track;
    :cond_4
    const/16 v20, 0x0

    goto :goto_2

    .line 129
    .end local v7    # "hasDifferent":Z
    .end local v8    # "i$":Ljava/util/Iterator;
    .end local v13    # "prevArtist":Ljava/lang/String;
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryTask;->mProjection:[Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-static {v2, v0}, Lcom/google/android/music/store/ProjectionUtils;->project(Lcom/google/android/music/cloudclient/AlbumJson;[Ljava/lang/String;)[Ljava/lang/Object;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v5, v0}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto :goto_3

    .line 135
    .end local v2    # "album":Lcom/google/android/music/cloudclient/AlbumJson;
    :sswitch_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryTask;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryTask;->mNautilusId:Ljava/lang/String;

    move-object/from16 v21, v0

    # invokes: Lcom/google/android/music/store/NautilusContentProviderHelper;->getAlbum(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/music/cloudclient/AlbumJson;
    invoke-static/range {v20 .. v21}, Lcom/google/android/music/store/NautilusContentProviderHelper;->access$100(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/music/cloudclient/AlbumJson;

    move-result-object v2

    .line 136
    .restart local v2    # "album":Lcom/google/android/music/cloudclient/AlbumJson;
    if-eqz v2, :cond_0

    iget-object v0, v2, Lcom/google/android/music/cloudclient/AlbumJson;->mTracks:Ljava/util/List;

    move-object/from16 v20, v0

    if-eqz v20, :cond_0

    .line 137
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryTask;->mProjection:[Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/google/android/music/store/MusicContentProvider;->hasCount([Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_6

    .line 139
    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    iget-object v0, v2, Lcom/google/android/music/cloudclient/AlbumJson;->mTracks:Ljava/util/List;

    move-object/from16 v22, v0

    invoke-interface/range {v22 .. v22}, Ljava/util/List;->size()I

    move-result v22

    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    aput-object v22, v20, v21

    move-object/from16 v0, v20

    invoke-virtual {v5, v0}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 140
    iget-object v0, v2, Lcom/google/android/music/cloudclient/AlbumJson;->mTracks:Ljava/util/List;

    move-object/from16 v20, v0

    invoke-interface/range {v20 .. v20}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .restart local v8    # "i$":Ljava/util/Iterator;
    :goto_4
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-eqz v20, :cond_7

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/google/android/music/sync/google/model/Track;

    .line 141
    .restart local v17    # "track":Lcom/google/android/music/sync/google/model/Track;
    move-object/from16 v0, v17

    invoke-static {v12, v0}, Lcom/google/android/music/store/NautilusContentProviderHelper;->generateLocalId(Lcom/google/android/music/store/TagNormalizer;Lcom/google/android/music/sync/google/model/Track;)J

    move-result-wide v20

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 145
    .end local v8    # "i$":Ljava/util/Iterator;
    .end local v17    # "track":Lcom/google/android/music/sync/google/model/Track;
    :cond_6
    iget-object v0, v2, Lcom/google/android/music/cloudclient/AlbumJson;->mTracks:Ljava/util/List;

    move-object/from16 v20, v0

    invoke-interface/range {v20 .. v20}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .restart local v8    # "i$":Ljava/util/Iterator;
    :goto_5
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-eqz v20, :cond_7

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/google/android/music/sync/google/model/Track;

    .line 146
    .restart local v17    # "track":Lcom/google/android/music/sync/google/model/Track;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryTask;->mProjection:[Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-static {v0, v1}, Lcom/google/android/music/store/ProjectionUtils;->project(Lcom/google/android/music/cloudclient/TrackJson;[Ljava/lang/String;)[Ljava/lang/Object;

    move-result-object v15

    .line 147
    .local v15, "row":[Ljava/lang/Object;
    invoke-virtual {v5, v15}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 148
    move-object/from16 v0, v17

    invoke-static {v12, v0}, Lcom/google/android/music/store/NautilusContentProviderHelper;->generateLocalId(Lcom/google/android/music/store/TagNormalizer;Lcom/google/android/music/sync/google/model/Track;)J

    move-result-wide v20

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 151
    .end local v15    # "row":[Ljava/lang/Object;
    .end local v17    # "track":Lcom/google/android/music/sync/google/model/Track;
    :cond_7
    invoke-static {v12, v2}, Lcom/google/android/music/store/NautilusContentProviderHelper;->generateLocalId(Lcom/google/android/music/store/TagNormalizer;Lcom/google/android/music/cloudclient/AlbumJson;)J

    move-result-wide v10

    goto/16 :goto_0

    .line 156
    .end local v2    # "album":Lcom/google/android/music/cloudclient/AlbumJson;
    .end local v8    # "i$":Ljava/util/Iterator;
    :sswitch_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryTask;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryTask;->mNautilusId:Ljava/lang/String;

    move-object/from16 v21, v0

    # invokes: Lcom/google/android/music/store/NautilusContentProviderHelper;->getArtist(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/music/cloudclient/ArtistJson;
    invoke-static/range {v20 .. v21}, Lcom/google/android/music/store/NautilusContentProviderHelper;->access$200(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/music/cloudclient/ArtistJson;

    move-result-object v3

    .line 157
    .local v3, "artist":Lcom/google/android/music/cloudclient/ArtistJson;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryTask;->mProjection:[Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-static {v3, v0}, Lcom/google/android/music/store/ProjectionUtils;->project(Lcom/google/android/music/cloudclient/ArtistJson;[Ljava/lang/String;)[Ljava/lang/Object;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v5, v0}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 158
    invoke-static {v12, v3}, Lcom/google/android/music/store/NautilusContentProviderHelper;->generateLocalId(Lcom/google/android/music/store/TagNormalizer;Lcom/google/android/music/cloudclient/ArtistJson;)J

    move-result-wide v10

    .line 159
    goto/16 :goto_0

    .line 162
    .end local v3    # "artist":Lcom/google/android/music/cloudclient/ArtistJson;
    :sswitch_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryTask;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryTask;->mNautilusId:Ljava/lang/String;

    move-object/from16 v21, v0

    # invokes: Lcom/google/android/music/store/NautilusContentProviderHelper;->getArtist(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/music/cloudclient/ArtistJson;
    invoke-static/range {v20 .. v21}, Lcom/google/android/music/store/NautilusContentProviderHelper;->access$200(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/music/cloudclient/ArtistJson;

    move-result-object v3

    .line 163
    .restart local v3    # "artist":Lcom/google/android/music/cloudclient/ArtistJson;
    if-eqz v3, :cond_0

    iget-object v0, v3, Lcom/google/android/music/cloudclient/ArtistJson;->mAlbums:Ljava/util/List;

    move-object/from16 v20, v0

    if-eqz v20, :cond_0

    .line 164
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryTask;->mProjection:[Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/google/android/music/store/MusicContentProvider;->hasCount([Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_c

    .line 166
    const/4 v4, 0x0

    .line 167
    .local v4, "count":I
    iget-object v0, v3, Lcom/google/android/music/cloudclient/ArtistJson;->mAlbums:Ljava/util/List;

    move-object/from16 v20, v0

    invoke-interface/range {v20 .. v20}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .restart local v8    # "i$":Ljava/util/Iterator;
    :cond_8
    :goto_6
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-eqz v20, :cond_a

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/cloudclient/AlbumJson;

    .line 168
    .restart local v2    # "album":Lcom/google/android/music/cloudclient/AlbumJson;
    iget-object v0, v2, Lcom/google/android/music/cloudclient/AlbumJson;->mTracks:Ljava/util/List;

    move-object/from16 v18, v0

    .line 169
    .local v18, "tracks":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/sync/google/model/Track;>;"
    if-nez v18, :cond_9

    .line 173
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryTask;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    iget-object v0, v2, Lcom/google/android/music/cloudclient/AlbumJson;->mAlbumId:Ljava/lang/String;

    move-object/from16 v21, v0

    # invokes: Lcom/google/android/music/store/NautilusContentProviderHelper;->getAlbum(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/music/cloudclient/AlbumJson;
    invoke-static/range {v20 .. v21}, Lcom/google/android/music/store/NautilusContentProviderHelper;->access$100(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/music/cloudclient/AlbumJson;

    move-result-object v2

    .line 174
    if-eqz v2, :cond_9

    .line 175
    iget-object v0, v2, Lcom/google/android/music/cloudclient/AlbumJson;->mTracks:Ljava/util/List;

    move-object/from16 v18, v0

    .line 179
    :cond_9
    if-eqz v18, :cond_8

    .line 180
    invoke-interface/range {v18 .. v18}, Ljava/util/List;->size()I

    move-result v20

    add-int v4, v4, v20

    goto :goto_6

    .line 183
    .end local v2    # "album":Lcom/google/android/music/cloudclient/AlbumJson;
    .end local v18    # "tracks":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/sync/google/model/Track;>;"
    :cond_a
    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    aput-object v22, v20, v21

    move-object/from16 v0, v20

    invoke-virtual {v5, v0}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 208
    .end local v4    # "count":I
    .end local v8    # "i$":Ljava/util/Iterator;
    :cond_b
    invoke-static {v12, v3}, Lcom/google/android/music/store/NautilusContentProviderHelper;->generateLocalId(Lcom/google/android/music/store/TagNormalizer;Lcom/google/android/music/cloudclient/ArtistJson;)J

    move-result-wide v10

    goto/16 :goto_0

    .line 187
    :cond_c
    iget-object v0, v3, Lcom/google/android/music/cloudclient/ArtistJson;->mAlbums:Ljava/util/List;

    move-object/from16 v20, v0

    invoke-interface/range {v20 .. v20}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_d
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-eqz v20, :cond_b

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/cloudclient/AlbumJson;

    .line 188
    .restart local v2    # "album":Lcom/google/android/music/cloudclient/AlbumJson;
    iget-object v0, v2, Lcom/google/android/music/cloudclient/AlbumJson;->mTracks:Ljava/util/List;

    move-object/from16 v18, v0

    .line 189
    .restart local v18    # "tracks":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/sync/google/model/Track;>;"
    if-nez v18, :cond_e

    .line 193
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryTask;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    iget-object v0, v2, Lcom/google/android/music/cloudclient/AlbumJson;->mAlbumId:Ljava/lang/String;

    move-object/from16 v21, v0

    # invokes: Lcom/google/android/music/store/NautilusContentProviderHelper;->getAlbum(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/music/cloudclient/AlbumJson;
    invoke-static/range {v20 .. v21}, Lcom/google/android/music/store/NautilusContentProviderHelper;->access$100(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/music/cloudclient/AlbumJson;

    move-result-object v2

    .line 194
    if-eqz v2, :cond_e

    .line 195
    iget-object v0, v2, Lcom/google/android/music/cloudclient/AlbumJson;->mTracks:Ljava/util/List;

    move-object/from16 v18, v0

    .line 199
    :cond_e
    if-eqz v18, :cond_d

    .line 200
    invoke-interface/range {v18 .. v18}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .local v9, "i$":Ljava/util/Iterator;
    :goto_7
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-eqz v20, :cond_d

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/google/android/music/sync/google/model/Track;

    .line 201
    .restart local v17    # "track":Lcom/google/android/music/sync/google/model/Track;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryTask;->mProjection:[Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-static {v0, v1}, Lcom/google/android/music/store/ProjectionUtils;->project(Lcom/google/android/music/cloudclient/TrackJson;[Ljava/lang/String;)[Ljava/lang/Object;

    move-result-object v15

    .line 202
    .restart local v15    # "row":[Ljava/lang/Object;
    invoke-virtual {v5, v15}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 203
    move-object/from16 v0, v17

    invoke-static {v12, v0}, Lcom/google/android/music/store/NautilusContentProviderHelper;->generateLocalId(Lcom/google/android/music/store/TagNormalizer;Lcom/google/android/music/sync/google/model/Track;)J

    move-result-wide v20

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_7

    .line 213
    .end local v2    # "album":Lcom/google/android/music/cloudclient/AlbumJson;
    .end local v3    # "artist":Lcom/google/android/music/cloudclient/ArtistJson;
    .end local v9    # "i$":Ljava/util/Iterator;
    .end local v15    # "row":[Ljava/lang/Object;
    .end local v17    # "track":Lcom/google/android/music/sync/google/model/Track;
    .end local v18    # "tracks":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/sync/google/model/Track;>;"
    :sswitch_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryTask;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryTask;->mNautilusId:Ljava/lang/String;

    move-object/from16 v21, v0

    # invokes: Lcom/google/android/music/store/NautilusContentProviderHelper;->getArtist(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/music/cloudclient/ArtistJson;
    invoke-static/range {v20 .. v21}, Lcom/google/android/music/store/NautilusContentProviderHelper;->access$200(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/music/cloudclient/ArtistJson;

    move-result-object v3

    .line 214
    .restart local v3    # "artist":Lcom/google/android/music/cloudclient/ArtistJson;
    if-eqz v3, :cond_0

    iget-object v0, v3, Lcom/google/android/music/cloudclient/ArtistJson;->mAlbums:Ljava/util/List;

    move-object/from16 v20, v0

    if-eqz v20, :cond_0

    .line 215
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryTask;->mProjection:[Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/google/android/music/store/MusicContentProvider;->hasCount([Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_f

    .line 217
    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    iget-object v0, v3, Lcom/google/android/music/cloudclient/ArtistJson;->mAlbums:Ljava/util/List;

    move-object/from16 v22, v0

    invoke-interface/range {v22 .. v22}, Ljava/util/List;->size()I

    move-result v22

    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    aput-object v22, v20, v21

    move-object/from16 v0, v20

    invoke-virtual {v5, v0}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 218
    iget-object v0, v3, Lcom/google/android/music/cloudclient/ArtistJson;->mAlbums:Ljava/util/List;

    move-object/from16 v20, v0

    invoke-interface/range {v20 .. v20}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .restart local v8    # "i$":Ljava/util/Iterator;
    :goto_8
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-eqz v20, :cond_10

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/cloudclient/AlbumJson;

    .line 219
    .restart local v2    # "album":Lcom/google/android/music/cloudclient/AlbumJson;
    invoke-static {v12, v2}, Lcom/google/android/music/store/NautilusContentProviderHelper;->generateLocalId(Lcom/google/android/music/store/TagNormalizer;Lcom/google/android/music/cloudclient/AlbumJson;)J

    move-result-wide v20

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_8

    .line 223
    .end local v2    # "album":Lcom/google/android/music/cloudclient/AlbumJson;
    .end local v8    # "i$":Ljava/util/Iterator;
    :cond_f
    iget-object v0, v3, Lcom/google/android/music/cloudclient/ArtistJson;->mAlbums:Ljava/util/List;

    move-object/from16 v20, v0

    invoke-interface/range {v20 .. v20}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .restart local v8    # "i$":Ljava/util/Iterator;
    :goto_9
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-eqz v20, :cond_10

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/cloudclient/AlbumJson;

    .line 224
    .restart local v2    # "album":Lcom/google/android/music/cloudclient/AlbumJson;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryTask;->mProjection:[Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-static {v2, v0}, Lcom/google/android/music/store/ProjectionUtils;->project(Lcom/google/android/music/cloudclient/AlbumJson;[Ljava/lang/String;)[Ljava/lang/Object;

    move-result-object v15

    .line 225
    .restart local v15    # "row":[Ljava/lang/Object;
    invoke-virtual {v5, v15}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 226
    invoke-static {v12, v2}, Lcom/google/android/music/store/NautilusContentProviderHelper;->generateLocalId(Lcom/google/android/music/store/TagNormalizer;Lcom/google/android/music/cloudclient/AlbumJson;)J

    move-result-wide v20

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_9

    .line 229
    .end local v2    # "album":Lcom/google/android/music/cloudclient/AlbumJson;
    .end local v15    # "row":[Ljava/lang/Object;
    :cond_10
    invoke-static {v12, v3}, Lcom/google/android/music/store/NautilusContentProviderHelper;->generateLocalId(Lcom/google/android/music/store/TagNormalizer;Lcom/google/android/music/cloudclient/ArtistJson;)J

    move-result-wide v10

    goto/16 :goto_0

    .line 234
    .end local v3    # "artist":Lcom/google/android/music/cloudclient/ArtistJson;
    .end local v8    # "i$":Ljava/util/Iterator;
    :sswitch_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryTask;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryTask;->mNautilusId:Ljava/lang/String;

    move-object/from16 v21, v0

    # invokes: Lcom/google/android/music/store/NautilusContentProviderHelper;->getArtist(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/music/cloudclient/ArtistJson;
    invoke-static/range {v20 .. v21}, Lcom/google/android/music/store/NautilusContentProviderHelper;->access$200(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/music/cloudclient/ArtistJson;

    move-result-object v3

    .line 235
    .restart local v3    # "artist":Lcom/google/android/music/cloudclient/ArtistJson;
    if-eqz v3, :cond_0

    iget-object v0, v3, Lcom/google/android/music/cloudclient/ArtistJson;->mRelatedArtists:Ljava/util/List;

    move-object/from16 v20, v0

    if-eqz v20, :cond_0

    .line 236
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryTask;->mProjection:[Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/google/android/music/store/MusicContentProvider;->hasCount([Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_12

    .line 238
    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    iget-object v0, v3, Lcom/google/android/music/cloudclient/ArtistJson;->mRelatedArtists:Ljava/util/List;

    move-object/from16 v22, v0

    invoke-interface/range {v22 .. v22}, Ljava/util/List;->size()I

    move-result v22

    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    aput-object v22, v20, v21

    move-object/from16 v0, v20

    invoke-virtual {v5, v0}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 246
    :cond_11
    invoke-static {v12, v3}, Lcom/google/android/music/store/NautilusContentProviderHelper;->generateLocalId(Lcom/google/android/music/store/TagNormalizer;Lcom/google/android/music/cloudclient/ArtistJson;)J

    move-result-wide v10

    goto/16 :goto_0

    .line 241
    :cond_12
    iget-object v0, v3, Lcom/google/android/music/cloudclient/ArtistJson;->mRelatedArtists:Ljava/util/List;

    move-object/from16 v20, v0

    invoke-interface/range {v20 .. v20}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .restart local v8    # "i$":Ljava/util/Iterator;
    :goto_a
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-eqz v20, :cond_11

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/google/android/music/cloudclient/ArtistJson;

    .line 242
    .local v14, "relArtist":Lcom/google/android/music/cloudclient/ArtistJson;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryTask;->mProjection:[Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-static {v14, v0}, Lcom/google/android/music/store/ProjectionUtils;->project(Lcom/google/android/music/cloudclient/ArtistJson;[Ljava/lang/String;)[Ljava/lang/Object;

    move-result-object v15

    .line 243
    .restart local v15    # "row":[Ljava/lang/Object;
    invoke-virtual {v5, v15}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto :goto_a

    .line 251
    .end local v3    # "artist":Lcom/google/android/music/cloudclient/ArtistJson;
    .end local v8    # "i$":Ljava/util/Iterator;
    .end local v14    # "relArtist":Lcom/google/android/music/cloudclient/ArtistJson;
    .end local v15    # "row":[Ljava/lang/Object;
    :sswitch_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryTask;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryTask;->mNautilusId:Ljava/lang/String;

    move-object/from16 v21, v0

    # invokes: Lcom/google/android/music/store/NautilusContentProviderHelper;->getArtist(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/music/cloudclient/ArtistJson;
    invoke-static/range {v20 .. v21}, Lcom/google/android/music/store/NautilusContentProviderHelper;->access$200(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/music/cloudclient/ArtistJson;

    move-result-object v3

    .line 252
    .restart local v3    # "artist":Lcom/google/android/music/cloudclient/ArtistJson;
    const-string v20, "true"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryTask;->mUri:Landroid/net/Uri;

    move-object/from16 v21, v0

    const-string v22, "hasVideo"

    invoke-virtual/range {v21 .. v22}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    .line 254
    .local v16, "showTrackWithVideo":Z
    if-eqz v3, :cond_0

    iget-object v0, v3, Lcom/google/android/music/cloudclient/ArtistJson;->mTopTracks:Ljava/util/List;

    move-object/from16 v20, v0

    if-eqz v20, :cond_0

    .line 255
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryTask;->mProjection:[Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/google/android/music/store/MusicContentProvider;->hasCount([Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_14

    .line 257
    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    iget-object v0, v3, Lcom/google/android/music/cloudclient/ArtistJson;->mTopTracks:Ljava/util/List;

    move-object/from16 v22, v0

    invoke-interface/range {v22 .. v22}, Ljava/util/List;->size()I

    move-result v22

    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    aput-object v22, v20, v21

    move-object/from16 v0, v20

    invoke-virtual {v5, v0}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 272
    :cond_13
    invoke-static {v12, v3}, Lcom/google/android/music/store/NautilusContentProviderHelper;->generateLocalId(Lcom/google/android/music/store/TagNormalizer;Lcom/google/android/music/cloudclient/ArtistJson;)J

    move-result-wide v10

    goto/16 :goto_0

    .line 260
    :cond_14
    iget-object v0, v3, Lcom/google/android/music/cloudclient/ArtistJson;->mTopTracks:Ljava/util/List;

    move-object/from16 v20, v0

    invoke-interface/range {v20 .. v20}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .restart local v8    # "i$":Ljava/util/Iterator;
    :cond_15
    :goto_b
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-eqz v20, :cond_13

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/google/android/music/sync/google/model/Track;

    .line 261
    .restart local v17    # "track":Lcom/google/android/music/sync/google/model/Track;
    if-eqz v16, :cond_16

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/google/android/music/sync/google/model/Track;->mPrimaryVideo:Lcom/google/android/music/cloudclient/TrackJson$PrimaryVideo;

    move-object/from16 v20, v0

    if-eqz v20, :cond_15

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/google/android/music/sync/google/model/Track;->mPrimaryVideo:Lcom/google/android/music/cloudclient/TrackJson$PrimaryVideo;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/google/android/music/cloudclient/TrackJson$PrimaryVideo;->mId:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v20

    if-nez v20, :cond_15

    .line 265
    :cond_16
    if-eqz v16, :cond_17

    .line 267
    :cond_17
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryTask;->mProjection:[Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-static {v0, v1}, Lcom/google/android/music/store/ProjectionUtils;->project(Lcom/google/android/music/cloudclient/TrackJson;[Ljava/lang/String;)[Ljava/lang/Object;

    move-result-object v15

    .line 268
    .restart local v15    # "row":[Ljava/lang/Object;
    invoke-virtual {v5, v15}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto :goto_b

    .line 277
    .end local v3    # "artist":Lcom/google/android/music/cloudclient/ArtistJson;
    .end local v8    # "i$":Ljava/util/Iterator;
    .end local v15    # "row":[Ljava/lang/Object;
    .end local v16    # "showTrackWithVideo":Z
    .end local v17    # "track":Lcom/google/android/music/sync/google/model/Track;
    :sswitch_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryTask;->mProjection:[Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    array-length v0, v0

    move/from16 v20, v0

    const/16 v21, 0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-le v0, v1, :cond_18

    .line 278
    const-string v20, "NautilusContentProvider"

    const-string v21, "Only one column is supported for querying artist art url."

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 281
    :cond_18
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryTask;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryTask;->mNautilusId:Ljava/lang/String;

    move-object/from16 v21, v0

    # invokes: Lcom/google/android/music/store/NautilusContentProviderHelper;->getArtistArtUrl(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    invoke-static/range {v20 .. v21}, Lcom/google/android/music/store/NautilusContentProviderHelper;->access$300(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 282
    .local v19, "url":Ljava/lang/String;
    invoke-static/range {v19 .. v19}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v20

    if-nez v20, :cond_0

    .line 283
    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    aput-object v19, v20, v21

    move-object/from16 v0, v20

    invoke-virtual {v5, v0}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 106
    :sswitch_data_0
    .sparse-switch
        0x12d -> :sswitch_0
        0x12e -> :sswitch_2
        0x12f -> :sswitch_4
        0x191 -> :sswitch_1
        0x1f5 -> :sswitch_3
        0x1f6 -> :sswitch_5
        0x1f7 -> :sswitch_7
        0x1f8 -> :sswitch_6
        0x1f9 -> :sswitch_8
    .end sparse-switch
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 81
    invoke-virtual {p0}, Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryTask;->call()Lcom/google/android/music/store/NautilusContentProviderHelper$NautilusQueryResult;

    move-result-object v0

    return-object v0
.end method
