.class Lcom/google/android/music/ui/GenreRadioStationsFragment$GenreRadioStationsAdapter;
.super Lcom/google/android/music/ui/FakeNthCardMediaListCardAdapter;
.source "GenreRadioStationsFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/GenreRadioStationsFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GenreRadioStationsAdapter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/GenreRadioStationsFragment;


# direct methods
.method protected constructor <init>(Lcom/google/android/music/ui/GenreRadioStationsFragment;Lcom/google/android/music/ui/GenreRadioStationsFragment;)V
    .locals 3
    .param p2, "musicFragment"    # Lcom/google/android/music/ui/GenreRadioStationsFragment;

    .prologue
    .line 95
    iput-object p1, p0, Lcom/google/android/music/ui/GenreRadioStationsFragment$GenreRadioStationsAdapter;->this$0:Lcom/google/android/music/ui/GenreRadioStationsFragment;

    .line 96
    # getter for: Lcom/google/android/music/ui/GenreRadioStationsFragment;->mTileMetadata:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;
    invoke-static {p1}, Lcom/google/android/music/ui/GenreRadioStationsFragment;->access$000(Lcom/google/android/music/ui/GenreRadioStationsFragment;)Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->getLayoutId()I

    move-result v0

    # getter for: Lcom/google/android/music/ui/GenreRadioStationsFragment;->mTileMetadata:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;
    invoke-static {p1}, Lcom/google/android/music/ui/GenreRadioStationsFragment;->access$000(Lcom/google/android/music/ui/GenreRadioStationsFragment;)Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->getLayoutId()I

    move-result v1

    const/4 v2, 0x0

    invoke-direct {p0, p2, v0, v1, v2}, Lcom/google/android/music/ui/FakeNthCardMediaListCardAdapter;-><init>(Lcom/google/android/music/ui/MusicFragment;III)V

    .line 97
    return-void
.end method

.method protected constructor <init>(Lcom/google/android/music/ui/GenreRadioStationsFragment;Lcom/google/android/music/ui/GenreRadioStationsFragment;Landroid/database/Cursor;)V
    .locals 6
    .param p2, "musicFragment"    # Lcom/google/android/music/ui/GenreRadioStationsFragment;
    .param p3, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 100
    iput-object p1, p0, Lcom/google/android/music/ui/GenreRadioStationsFragment$GenreRadioStationsAdapter;->this$0:Lcom/google/android/music/ui/GenreRadioStationsFragment;

    .line 101
    # getter for: Lcom/google/android/music/ui/GenreRadioStationsFragment;->mTileMetadata:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;
    invoke-static {p1}, Lcom/google/android/music/ui/GenreRadioStationsFragment;->access$000(Lcom/google/android/music/ui/GenreRadioStationsFragment;)Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->getLayoutId()I

    move-result v2

    # getter for: Lcom/google/android/music/ui/GenreRadioStationsFragment;->mTileMetadata:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;
    invoke-static {p1}, Lcom/google/android/music/ui/GenreRadioStationsFragment;->access$000(Lcom/google/android/music/ui/GenreRadioStationsFragment;)Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->getLayoutId()I

    move-result v3

    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/music/ui/FakeNthCardMediaListCardAdapter;-><init>(Lcom/google/android/music/ui/MusicFragment;IIILandroid/database/Cursor;)V

    .line 103
    return-void
.end method


# virtual methods
.method protected bindViewToLoadingItem(Landroid/view/View;Landroid/content/Context;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 145
    const v1, 0x1020014

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 146
    .local v0, "tv":Landroid/widget/TextView;
    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 147
    return-void
.end method

.method protected bindViewToMediaListItem(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;J)V
    .locals 7
    .param p1, "view"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "cursor"    # Landroid/database/Cursor;
    .param p4, "itemId"    # J

    .prologue
    const/4 v6, 0x1

    .line 125
    invoke-interface {p3, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 126
    .local v3, "genreNautilusId":Ljava/lang/String;
    const/4 v5, 0x2

    invoke-interface {p3, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 127
    .local v2, "genreName":Ljava/lang/String;
    const/4 v5, 0x3

    invoke-interface {p3, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 128
    .local v1, "genreArtUrls":Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/music/utils/MusicUtils;->getDocument(Landroid/view/View;)Lcom/google/android/music/ui/cardlib/model/Document;

    move-result-object v0

    .line 130
    .local v0, "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    sget-object v5, Lcom/google/android/music/ui/cardlib/model/Document$Type;->GENRE:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    invoke-virtual {v0, v5}, Lcom/google/android/music/ui/cardlib/model/Document;->setType(Lcom/google/android/music/ui/cardlib/model/Document$Type;)V

    .line 131
    invoke-virtual {v0, v3}, Lcom/google/android/music/ui/cardlib/model/Document;->setGenreId(Ljava/lang/String;)V

    .line 132
    invoke-virtual {v0, v2}, Lcom/google/android/music/ui/cardlib/model/Document;->setTitle(Ljava/lang/String;)V

    .line 133
    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/cardlib/model/Document;->setArtUrl(Ljava/lang/String;)V

    .line 134
    invoke-virtual {v0, v6}, Lcom/google/android/music/ui/cardlib/model/Document;->setIsEmulatedRadio(Z)V

    .line 136
    instance-of v5, p1, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    if-eqz v5, :cond_0

    move-object v4, p1

    .line 137
    check-cast v4, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    .line 138
    .local v4, "pcv":Lcom/google/android/music/ui/cardlib/layout/PlayCardView;
    iget-object v5, p0, Lcom/google/android/music/ui/GenreRadioStationsFragment$GenreRadioStationsAdapter;->this$0:Lcom/google/android/music/ui/GenreRadioStationsFragment;

    iget-object v5, v5, Lcom/google/android/music/ui/GenreRadioStationsFragment;->mCardsContextMenuDelegate:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$DocumentContextMenuDelegate;

    invoke-virtual {v4, v0, v5}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->bind(Lcom/google/android/music/ui/cardlib/model/Document;Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;)V

    .line 139
    const/high16 v5, 0x3f800000    # 1.0f

    invoke-virtual {v4, v5}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->setThumbnailAspectRatio(F)V

    .line 141
    .end local v4    # "pcv":Lcom/google/android/music/ui/cardlib/layout/PlayCardView;
    :cond_0
    return-void
.end method

.method public getFakeView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v7, 0x1

    .line 107
    invoke-static {p2}, Lcom/google/android/music/utils/MusicUtils;->getDocument(Landroid/view/View;)Lcom/google/android/music/ui/cardlib/model/Document;

    move-result-object v0

    .line 108
    .local v0, "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    sget-object v2, Lcom/google/android/music/ui/cardlib/model/Document$Type;->GENRE:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    invoke-virtual {v0, v2}, Lcom/google/android/music/ui/cardlib/model/Document;->setType(Lcom/google/android/music/ui/cardlib/model/Document$Type;)V

    .line 109
    iget-object v2, p0, Lcom/google/android/music/ui/GenreRadioStationsFragment$GenreRadioStationsAdapter;->this$0:Lcom/google/android/music/ui/GenreRadioStationsFragment;

    # getter for: Lcom/google/android/music/ui/GenreRadioStationsFragment;->mParentGenre:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/music/ui/GenreRadioStationsFragment;->access$100(Lcom/google/android/music/ui/GenreRadioStationsFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/music/ui/cardlib/model/Document;->setGenreId(Ljava/lang/String;)V

    .line 110
    iget-object v2, p0, Lcom/google/android/music/ui/GenreRadioStationsFragment$GenreRadioStationsAdapter;->this$0:Lcom/google/android/music/ui/GenreRadioStationsFragment;

    const v3, 0x7f0b02ce

    new-array v4, v7, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/android/music/ui/GenreRadioStationsFragment$GenreRadioStationsAdapter;->this$0:Lcom/google/android/music/ui/GenreRadioStationsFragment;

    # getter for: Lcom/google/android/music/ui/GenreRadioStationsFragment;->mParentName:Ljava/lang/String;
    invoke-static {v6}, Lcom/google/android/music/ui/GenreRadioStationsFragment;->access$200(Lcom/google/android/music/ui/GenreRadioStationsFragment;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Lcom/google/android/music/ui/GenreRadioStationsFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/music/ui/cardlib/model/Document;->setTitle(Ljava/lang/String;)V

    .line 111
    invoke-virtual {v0, v7}, Lcom/google/android/music/ui/cardlib/model/Document;->setIsEmulatedRadio(Z)V

    .line 112
    iget-object v2, p0, Lcom/google/android/music/ui/GenreRadioStationsFragment$GenreRadioStationsAdapter;->this$0:Lcom/google/android/music/ui/GenreRadioStationsFragment;

    # getter for: Lcom/google/android/music/ui/GenreRadioStationsFragment;->mParentArt:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/music/ui/GenreRadioStationsFragment;->access$300(Lcom/google/android/music/ui/GenreRadioStationsFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/music/ui/cardlib/model/Document;->setArtUrl(Ljava/lang/String;)V

    .line 113
    instance-of v2, p2, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    if-eqz v2, :cond_0

    move-object v1, p2

    .line 114
    check-cast v1, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    .line 115
    .local v1, "pcv":Lcom/google/android/music/ui/cardlib/layout/PlayCardView;
    iget-object v2, p0, Lcom/google/android/music/ui/GenreRadioStationsFragment$GenreRadioStationsAdapter;->this$0:Lcom/google/android/music/ui/GenreRadioStationsFragment;

    iget-object v2, v2, Lcom/google/android/music/ui/GenreRadioStationsFragment;->mCardsContextMenuDelegate:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$DocumentContextMenuDelegate;

    invoke-virtual {v1, v0, v2}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->bind(Lcom/google/android/music/ui/cardlib/model/Document;Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;)V

    .line 116
    iget-object v2, p0, Lcom/google/android/music/ui/GenreRadioStationsFragment$GenreRadioStationsAdapter;->this$0:Lcom/google/android/music/ui/GenreRadioStationsFragment;

    # getter for: Lcom/google/android/music/ui/GenreRadioStationsFragment;->mTileMetadata:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;
    invoke-static {v2}, Lcom/google/android/music/ui/GenreRadioStationsFragment;->access$000(Lcom/google/android/music/ui/GenreRadioStationsFragment;)Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->getThumbnailAspectRatio()F

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;->setThumbnailAspectRatio(F)V

    .line 118
    .end local v1    # "pcv":Lcom/google/android/music/ui/cardlib/layout/PlayCardView;
    :cond_0
    invoke-virtual {p2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 119
    return-object p2
.end method
