.class public Lcom/google/android/music/ui/GenreRadioActivity;
.super Lcom/google/android/music/ui/BaseActivity;
.source "GenreRadioActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/google/android/music/ui/BaseActivity;-><init>()V

    return-void
.end method

.method public static buildStartIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "genreId"    # Ljava/lang/String;
    .param p2, "genreName"    # Ljava/lang/String;
    .param p3, "artUrls"    # Ljava/lang/String;

    .prologue
    .line 27
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 28
    :cond_0
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Genre name and its NautilusId are needed to display its sub genres. "

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 31
    :cond_1
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/google/android/music/ui/GenreRadioActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 32
    .local v1, "intent":Landroid/content/Intent;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 33
    .local v0, "extra":Landroid/os/Bundle;
    const-string v2, "nautilusId"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 34
    const-string v2, "name"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    const-string v2, "artUrls"

    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 37
    return-object v1
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedState"    # Landroid/os/Bundle;

    .prologue
    .line 13
    invoke-super {p0, p1}, Lcom/google/android/music/ui/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 14
    invoke-virtual {p0}, Lcom/google/android/music/ui/GenreRadioActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 15
    .local v0, "extra":Landroid/os/Bundle;
    const-string v3, "name"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 16
    .local v2, "genreName":Ljava/lang/String;
    invoke-virtual {p0, v2}, Lcom/google/android/music/ui/GenreRadioActivity;->setActionBarTitle(Ljava/lang/String;)V

    .line 18
    invoke-virtual {p0}, Lcom/google/android/music/ui/GenreRadioActivity;->getContent()Landroid/support/v4/app/Fragment;

    move-result-object v3

    if-nez v3, :cond_0

    .line 19
    new-instance v1, Lcom/google/android/music/ui/GenreRadioStationsFragment;

    invoke-direct {v1}, Lcom/google/android/music/ui/GenreRadioStationsFragment;-><init>()V

    .line 20
    .local v1, "fragment":Lcom/google/android/music/ui/GenreRadioStationsFragment;
    invoke-virtual {v1, v0}, Lcom/google/android/music/ui/GenreRadioStationsFragment;->setArguments(Landroid/os/Bundle;)V

    .line 21
    const/4 v3, 0x0

    invoke-virtual {p0, v1, v3}, Lcom/google/android/music/ui/GenreRadioActivity;->replaceContent(Landroid/support/v4/app/Fragment;Z)V

    .line 23
    .end local v1    # "fragment":Lcom/google/android/music/ui/GenreRadioStationsFragment;
    :cond_0
    return-void
.end method
