.class public Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;
.super Lcom/google/android/music/ui/BaseFragment;
.source "NowPlayingHeaderPageFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private mAlbumId:J

.field private mAlbumName:Ljava/lang/String;

.field private mAlbumSmall:Lcom/google/android/music/AsyncAlbumArtImageView;

.field private mArtUrl:Ljava/lang/String;

.field private mArtistName:Ljava/lang/String;

.field private mArtistNameView:Landroid/widget/TextView;

.field private mBufferingProgress:Landroid/widget/ProgressBar;

.field private mHeader:Landroid/view/View;

.field private mMetadataView:Landroid/view/View;

.field private mNowPlayingDrawerListener:Landroid/content/BroadcastReceiver;

.field private mQueuePosition:I

.field private mRootView:Landroid/view/View;

.field private mStatusListener:Landroid/content/BroadcastReceiver;

.field private mTitle:Ljava/lang/String;

.field private mTrackNameView:Lcom/google/android/music/widgets/MarqueeTextView;

.field private mVideoIcon:Landroid/widget/ImageView;

.field private mVideoId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/google/android/music/ui/BaseFragment;-><init>()V

    .line 61
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;->mQueuePosition:I

    .line 69
    new-instance v0, Lcom/google/android/music/ui/NowPlayingHeaderPageFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/NowPlayingHeaderPageFragment$1;-><init>(Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;)V

    iput-object v0, p0, Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;->mStatusListener:Landroid/content/BroadcastReceiver;

    .line 89
    new-instance v0, Lcom/google/android/music/ui/NowPlayingHeaderPageFragment$2;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/NowPlayingHeaderPageFragment$2;-><init>(Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;)V

    iput-object v0, p0, Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;->mNowPlayingDrawerListener:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;

    .prologue
    .line 33
    iget v0, p0, Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;->mQueuePosition:I

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;Landroid/content/Intent;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;
    .param p1, "x1"    # Landroid/content/Intent;

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;->updateStreaming(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;->updateMetadataMarquee()V

    return-void
.end method

.method private findViewById(I)Landroid/view/View;
    .locals 1
    .param p1, "id"    # I

    .prologue
    .line 205
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;->mRootView:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private initializeView()V
    .locals 1

    .prologue
    .line 192
    const v0, 0x7f0e01d3

    invoke-direct {p0, v0}, Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;->mHeader:Landroid/view/View;

    .line 193
    const v0, 0x7f0e01d0

    invoke-direct {p0, v0}, Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/AsyncAlbumArtImageView;

    iput-object v0, p0, Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;->mAlbumSmall:Lcom/google/android/music/AsyncAlbumArtImageView;

    .line 194
    const v0, 0x7f0e01c1

    invoke-direct {p0, v0}, Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/widgets/MarqueeTextView;

    iput-object v0, p0, Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;->mTrackNameView:Lcom/google/android/music/widgets/MarqueeTextView;

    .line 195
    const v0, 0x7f0e01d4

    invoke-direct {p0, v0}, Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;->mArtistNameView:Landroid/widget/TextView;

    .line 196
    const v0, 0x7f0e01d2

    invoke-direct {p0, v0}, Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;->mBufferingProgress:Landroid/widget/ProgressBar;

    .line 197
    const v0, 0x7f0e01d1

    invoke-direct {p0, v0}, Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;->mVideoIcon:Landroid/widget/ImageView;

    .line 198
    const v0, 0x7f0e01cf

    invoke-direct {p0, v0}, Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;->mMetadataView:Landroid/view/View;

    .line 201
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;->mMetadataView:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 202
    return-void
.end method

.method public static newInstance(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;
    .locals 4
    .param p0, "position"    # I
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "albumName"    # Ljava/lang/String;
    .param p3, "artistName"    # Ljava/lang/String;
    .param p4, "albumId"    # J
    .param p6, "artUrl"    # Ljava/lang/String;
    .param p7, "videoId"    # Ljava/lang/String;

    .prologue
    .line 114
    new-instance v1, Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;

    invoke-direct {v1}, Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;-><init>()V

    .line 115
    .local v1, "fragment":Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 116
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "position"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 117
    const-string v2, "title"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    const-string v2, "albumName"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    const-string v2, "artistName"

    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    const-string v2, "albumId"

    invoke-virtual {v0, v2, p4, p5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 121
    const-string v2, "artUrl"

    invoke-virtual {v0, v2, p6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    const-string v2, "videoId"

    invoke-virtual {v0, v2, p7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    invoke-virtual {v1, v0}, Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;->setArguments(Landroid/os/Bundle;)V

    .line 124
    return-object v1
.end method

.method private updateMetadataMarquee()V
    .locals 4

    .prologue
    .line 237
    invoke-virtual {p0}, Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;->getBottomDrawer()Lcom/google/android/music/widgets/ExpandingScrollView;

    move-result-object v0

    .line 238
    .local v0, "drawer":Lcom/google/android/music/widgets/ExpandingScrollView;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/music/widgets/ExpandingScrollView;->getExpandingState()Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    move-result-object v2

    sget-object v3, Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;->FULLY_EXPANDED:Lcom/google/android/music/widgets/ExpandingScrollView$ExpandingState;

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    .line 242
    .local v1, "expanded":Z
    :goto_0
    iget-object v2, p0, Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;->mTrackNameView:Lcom/google/android/music/widgets/MarqueeTextView;

    invoke-virtual {v2, v1}, Lcom/google/android/music/widgets/MarqueeTextView;->doMarquee(Z)V

    .line 243
    return-void

    .line 238
    .end local v1    # "expanded":Z
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private updateStreaming(Landroid/content/Intent;)V
    .locals 6
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 217
    const-string v3, "inErrorState"

    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 219
    .local v0, "isInErrorState":Z
    const-string v3, "streaming"

    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    .line 221
    .local v2, "isStreaming":Z
    const-string v3, "preparing"

    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 224
    .local v1, "isPreparing":Z
    if-eqz v0, :cond_0

    .line 225
    iget-object v3, p0, Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;->mAlbumSmall:Lcom/google/android/music/AsyncAlbumArtImageView;

    invoke-virtual {v3, v4}, Lcom/google/android/music/AsyncAlbumArtImageView;->setAvailable(Z)V

    .line 226
    iget-object v3, p0, Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;->mBufferingProgress:Landroid/widget/ProgressBar;

    invoke-virtual {v3, v5}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 234
    :goto_0
    return-void

    .line 227
    :cond_0
    if-eqz v1, :cond_1

    if-eqz v2, :cond_1

    .line 228
    iget-object v3, p0, Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;->mAlbumSmall:Lcom/google/android/music/AsyncAlbumArtImageView;

    invoke-virtual {v3, v4}, Lcom/google/android/music/AsyncAlbumArtImageView;->setAvailable(Z)V

    .line 229
    iget-object v3, p0, Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;->mBufferingProgress:Landroid/widget/ProgressBar;

    invoke-virtual {v3, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0

    .line 231
    :cond_1
    iget-object v3, p0, Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;->mAlbumSmall:Lcom/google/android/music/AsyncAlbumArtImageView;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/google/android/music/AsyncAlbumArtImageView;->setAvailable(Z)V

    .line 232
    iget-object v3, p0, Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;->mBufferingProgress:Landroid/widget/ProgressBar;

    invoke-virtual {v3, v5}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0
.end method

.method private updateTrackInfo()V
    .locals 2

    .prologue
    .line 209
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;->mTrackNameView:Lcom/google/android/music/widgets/MarqueeTextView;

    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;->mTitle:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/music/widgets/MarqueeTextView;->setText(Ljava/lang/CharSequence;)V

    .line 210
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;->mArtistNameView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;->mArtistName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 211
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;->mVideoIcon:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 212
    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;->mVideoIcon:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;->mVideoId:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 214
    :cond_0
    return-void

    .line 212
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 247
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 248
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.google.android.music.nowplaying.HEADER_CLICKED"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 249
    invoke-virtual {p0}, Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 250
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 130
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/music/ui/BaseFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    .line 131
    const v3, 0x7f04008a

    const/4 v4, 0x0

    invoke-virtual {p1, v3, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;->mRootView:Landroid/view/View;

    .line 133
    new-instance v2, Landroid/content/IntentFilter;

    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    .line 134
    .local v2, "playbackStatusFilter":Landroid/content/IntentFilter;
    const-string v3, "com.android.music.playstatechanged"

    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 135
    const-string v3, "com.android.music.metachanged"

    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 136
    const-string v3, "com.android.music.asyncopencomplete"

    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 137
    const-string v3, "com.android.music.asyncopenstart"

    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 138
    const-string v3, "com.android.music.playbackfailed"

    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 139
    invoke-virtual {p0}, Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;->mStatusListener:Landroid/content/BroadcastReceiver;

    invoke-virtual {v3, v4, v2}, Landroid/support/v4/app/FragmentActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 141
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 142
    .local v1, "drawerStateFilter":Landroid/content/IntentFilter;
    const-string v3, "com.google.android.music.nowplaying.DRAWER_STATE_CHANGED_ACTION"

    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 143
    invoke-virtual {p0}, Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-static {v3}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;->mNowPlayingDrawerListener:Landroid/content/BroadcastReceiver;

    invoke-virtual {v3, v4, v1}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 146
    invoke-virtual {p0}, Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 147
    .local v0, "args":Landroid/os/Bundle;
    const-string v3, "position"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;->mQueuePosition:I

    .line 148
    const-string v3, "title"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;->mTitle:Ljava/lang/String;

    .line 149
    const-string v3, "artistName"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;->mArtistName:Ljava/lang/String;

    .line 150
    const-string v3, "albumName"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;->mAlbumName:Ljava/lang/String;

    .line 151
    const-string v3, "albumId"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;->mAlbumId:J

    .line 152
    const-string v3, "artUrl"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;->mArtUrl:Ljava/lang/String;

    .line 153
    invoke-virtual {p0}, Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/music/preferences/MusicPreferences;->isYouTubeAvailable(Landroid/app/Activity;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 154
    const-string v3, "videoId"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;->mVideoId:Ljava/lang/String;

    .line 157
    :cond_0
    invoke-direct {p0}, Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;->initializeView()V

    .line 158
    invoke-direct {p0}, Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;->updateTrackInfo()V

    .line 160
    iget-object v3, p0, Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;->mRootView:Landroid/view/View;

    return-object v3
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 185
    invoke-virtual {p0}, Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;->mStatusListener:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 186
    invoke-virtual {p0}, Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;->mNowPlayingDrawerListener:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 188
    invoke-super {p0}, Lcom/google/android/music/ui/BaseFragment;->onDestroyView()V

    .line 189
    return-void
.end method

.method public onStart()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 165
    invoke-super {p0}, Lcom/google/android/music/ui/BaseFragment;->onStart()V

    .line 166
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;->mArtUrl:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 167
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;->mAlbumSmall:Lcom/google/android/music/AsyncAlbumArtImageView;

    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;->mArtUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/music/AsyncAlbumArtImageView;->setExternalAlbumArt(Ljava/lang/String;)V

    .line 171
    :goto_0
    invoke-direct {p0}, Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;->updateMetadataMarquee()V

    .line 172
    return-void

    .line 169
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;->mAlbumSmall:Lcom/google/android/music/AsyncAlbumArtImageView;

    iget-wide v2, p0, Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;->mAlbumId:J

    invoke-virtual {v0, v2, v3, v1, v1}, Lcom/google/android/music/AsyncAlbumArtImageView;->setAlbumId(JLjava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 176
    invoke-super {p0}, Lcom/google/android/music/ui/BaseFragment;->onStop()V

    .line 177
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingHeaderPageFragment;->mAlbumSmall:Lcom/google/android/music/AsyncAlbumArtImageView;

    invoke-virtual {v0}, Lcom/google/android/music/AsyncAlbumArtImageView;->clearArtwork()V

    .line 178
    return-void
.end method
