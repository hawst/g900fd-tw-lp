.class Lcom/google/android/music/playback/session/MediaSessionCompat$MediaSessionImplPreL;
.super Ljava/lang/Object;
.source "MediaSessionCompat.java"

# interfaces
.implements Lcom/google/android/music/playback/session/MediaSessionCompat$MediaSessionImpl;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/playback/session/MediaSessionCompat;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "MediaSessionImplPreL"
.end annotation


# instance fields
.field private mActive:Z

.field private final mContext:Landroid/content/Context;

.field private final mLooper:Landroid/os/Looper;

.field private final mMediaButtonReceiver:Landroid/content/ComponentName;

.field private final mPendingIntent:Landroid/app/PendingIntent;

.field private final mRccObj:Lcom/google/android/music/playback/RemoteControlClientCompat;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/app/PendingIntent;Landroid/os/Looper;Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/app/PendingIntent;
    .param p3, "looper"    # Landroid/os/Looper;
    .param p4, "mediaButtonReciever"    # Landroid/content/ComponentName;

    .prologue
    .line 666
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 662
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/playback/session/MediaSessionCompat$MediaSessionImplPreL;->mActive:Z

    .line 667
    iput-object p2, p0, Lcom/google/android/music/playback/session/MediaSessionCompat$MediaSessionImplPreL;->mPendingIntent:Landroid/app/PendingIntent;

    .line 668
    iput-object p3, p0, Lcom/google/android/music/playback/session/MediaSessionCompat$MediaSessionImplPreL;->mLooper:Landroid/os/Looper;

    .line 669
    iput-object p1, p0, Lcom/google/android/music/playback/session/MediaSessionCompat$MediaSessionImplPreL;->mContext:Landroid/content/Context;

    .line 670
    iget-object v0, p0, Lcom/google/android/music/playback/session/MediaSessionCompat$MediaSessionImplPreL;->mPendingIntent:Landroid/app/PendingIntent;

    iget-object v1, p0, Lcom/google/android/music/playback/session/MediaSessionCompat$MediaSessionImplPreL;->mLooper:Landroid/os/Looper;

    invoke-static {v0, v1}, Lcom/google/android/music/playback/session/MediaSessionCompatPreL;->createRccCompat(Landroid/app/PendingIntent;Landroid/os/Looper;)Lcom/google/android/music/playback/RemoteControlClientCompat;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/playback/session/MediaSessionCompat$MediaSessionImplPreL;->mRccObj:Lcom/google/android/music/playback/RemoteControlClientCompat;

    .line 671
    iput-object p4, p0, Lcom/google/android/music/playback/session/MediaSessionCompat$MediaSessionImplPreL;->mMediaButtonReceiver:Landroid/content/ComponentName;

    .line 672
    return-void
.end method


# virtual methods
.method public getMediaSession()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 776
    const/4 v0, 0x0

    return-object v0
.end method

.method public getRemoteControlClient()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 708
    iget-object v0, p0, Lcom/google/android/music/playback/session/MediaSessionCompat$MediaSessionImplPreL;->mRccObj:Lcom/google/android/music/playback/RemoteControlClientCompat;

    if-eqz v0, :cond_0

    .line 709
    iget-object v0, p0, Lcom/google/android/music/playback/session/MediaSessionCompat$MediaSessionImplPreL;->mRccObj:Lcom/google/android/music/playback/RemoteControlClientCompat;

    invoke-virtual {v0}, Lcom/google/android/music/playback/RemoteControlClientCompat;->getActualRemoteControlClientObject()Ljava/lang/Object;

    move-result-object v0

    .line 711
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSessionToken()Lcom/google/android/music/playback/session/MediaSessionCompat$Token;
    .locals 1

    .prologue
    .line 747
    const/4 v0, 0x0

    return-object v0
.end method

.method public release()V
    .locals 2

    .prologue
    .line 687
    iget-object v0, p0, Lcom/google/android/music/playback/session/MediaSessionCompat$MediaSessionImplPreL;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/music/playback/session/MediaSessionCompat$MediaSessionImplPreL;->mRccObj:Lcom/google/android/music/playback/RemoteControlClientCompat;

    invoke-static {v0, v1}, Lcom/google/android/music/playback/session/MediaSessionCompatPreL;->release(Landroid/content/Context;Ljava/lang/Object;)V

    .line 688
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/playback/session/MediaSessionCompat$MediaSessionImplPreL;->mActive:Z

    .line 689
    return-void
.end method

.method public setActive(Z)V
    .locals 4
    .param p1, "active"    # Z

    .prologue
    .line 681
    iget-object v0, p0, Lcom/google/android/music/playback/session/MediaSessionCompat$MediaSessionImplPreL;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/music/playback/session/MediaSessionCompat$MediaSessionImplPreL;->mRccObj:Lcom/google/android/music/playback/RemoteControlClientCompat;

    iget-object v2, p0, Lcom/google/android/music/playback/session/MediaSessionCompat$MediaSessionImplPreL;->mMediaButtonReceiver:Landroid/content/ComponentName;

    iget-boolean v3, p0, Lcom/google/android/music/playback/session/MediaSessionCompat$MediaSessionImplPreL;->mActive:Z

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/music/playback/session/MediaSessionCompatPreL;->setActive(Landroid/content/Context;Ljava/lang/Object;Landroid/content/ComponentName;Z)V

    .line 682
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/playback/session/MediaSessionCompat$MediaSessionImplPreL;->mActive:Z

    .line 683
    return-void
.end method

.method public setCallback(Lcom/google/android/music/playback/session/MediaSessionCompat$Callback;Landroid/os/Handler;)V
    .locals 2
    .param p1, "callback"    # Lcom/google/android/music/playback/session/MediaSessionCompat$Callback;
    .param p2, "handler"    # Landroid/os/Handler;

    .prologue
    .line 676
    iget-object v0, p0, Lcom/google/android/music/playback/session/MediaSessionCompat$MediaSessionImplPreL;->mRccObj:Lcom/google/android/music/playback/RemoteControlClientCompat;

    iget-object v1, p1, Lcom/google/android/music/playback/session/MediaSessionCompat$Callback;->mCallbackObj:Ljava/lang/Object;

    invoke-static {v0, v1, p2}, Lcom/google/android/music/playback/session/MediaSessionCompatPreL;->setCallback(Ljava/lang/Object;Ljava/lang/Object;Landroid/os/Handler;)V

    .line 677
    return-void
.end method

.method public setExtras(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 769
    return-void
.end method

.method public setFlags(I)V
    .locals 0
    .param p1, "flags"    # I

    .prologue
    .line 719
    return-void
.end method

.method public setMetadata(Lcom/google/android/music/playback/session/MediaMetadataCompat;)V
    .locals 1
    .param p1, "metadata"    # Lcom/google/android/music/playback/session/MediaMetadataCompat;

    .prologue
    .line 698
    iget-object v0, p0, Lcom/google/android/music/playback/session/MediaSessionCompat$MediaSessionImplPreL;->mRccObj:Lcom/google/android/music/playback/RemoteControlClientCompat;

    invoke-static {v0, p1}, Lcom/google/android/music/playback/session/MediaSessionCompatPreL;->setMetadata(Ljava/lang/Object;Lcom/google/android/music/playback/session/MediaMetadataCompat;)V

    .line 699
    return-void
.end method

.method public setPlaybackState(Lcom/google/android/music/playback/session/PlaybackStateCompat;)V
    .locals 1
    .param p1, "state"    # Lcom/google/android/music/playback/session/PlaybackStateCompat;

    .prologue
    .line 693
    iget-object v0, p0, Lcom/google/android/music/playback/session/MediaSessionCompat$MediaSessionImplPreL;->mRccObj:Lcom/google/android/music/playback/RemoteControlClientCompat;

    invoke-static {v0, p1}, Lcom/google/android/music/playback/session/MediaSessionCompatPreL;->setPlaybackState(Ljava/lang/Object;Lcom/google/android/music/playback/session/PlaybackStateCompat;)V

    .line 694
    return-void
.end method

.method public setQueue(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/playback/session/MediaSessionCompat$QueueItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 755
    .local p1, "queue":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/playback/session/MediaSessionCompat$QueueItem;>;"
    return-void
.end method

.method public setQueueTitle(Ljava/lang/CharSequence;)V
    .locals 0
    .param p1, "title"    # Ljava/lang/CharSequence;

    .prologue
    .line 762
    return-void
.end method

.method public setSessionActivity(Landroid/app/PendingIntent;)V
    .locals 0
    .param p1, "pi"    # Landroid/app/PendingIntent;

    .prologue
    .line 783
    return-void
.end method
