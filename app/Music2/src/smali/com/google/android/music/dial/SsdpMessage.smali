.class public Lcom/google/android/music/dial/SsdpMessage;
.super Ljava/lang/Object;
.source "SsdpMessage.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/dial/SsdpMessage$1;,
        Lcom/google/android/music/dial/SsdpMessage$Builder;,
        Lcom/google/android/music/dial/SsdpMessage$MessageType;
    }
.end annotation


# instance fields
.field private final mHeaders:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mType:Lcom/google/android/music/dial/SsdpMessage$MessageType;


# direct methods
.method private constructor <init>(Lcom/google/android/music/dial/SsdpMessage$MessageType;Ljava/util/LinkedHashMap;)V
    .locals 0
    .param p1, "type"    # Lcom/google/android/music/dial/SsdpMessage$MessageType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/music/dial/SsdpMessage$MessageType;",
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 106
    .local p2, "headers":Ljava/util/LinkedHashMap;, "Ljava/util/LinkedHashMap<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 107
    iput-object p1, p0, Lcom/google/android/music/dial/SsdpMessage;->mType:Lcom/google/android/music/dial/SsdpMessage$MessageType;

    .line 108
    iput-object p2, p0, Lcom/google/android/music/dial/SsdpMessage;->mHeaders:Ljava/util/LinkedHashMap;

    .line 109
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/music/dial/SsdpMessage$MessageType;Ljava/util/LinkedHashMap;Lcom/google/android/music/dial/SsdpMessage$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/music/dial/SsdpMessage$MessageType;
    .param p2, "x1"    # Ljava/util/LinkedHashMap;
    .param p3, "x2"    # Lcom/google/android/music/dial/SsdpMessage$1;

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/dial/SsdpMessage;-><init>(Lcom/google/android/music/dial/SsdpMessage$MessageType;Ljava/util/LinkedHashMap;)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/google/android/music/dial/SsdpMessage;
    .locals 12
    .param p0, "rawMessage"    # Ljava/lang/String;

    .prologue
    const/4 v11, 0x2

    const/4 v8, 0x0

    .line 169
    new-instance v6, Ljava/io/StringReader;

    invoke-direct {v6, p0}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    .line 170
    .local v6, "stringReader":Ljava/io/StringReader;
    new-instance v0, Ljava/io/BufferedReader;

    invoke-direct {v0, v6}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 171
    .local v0, "bufferedReader":Ljava/io/BufferedReader;
    const/4 v5, 0x0

    .line 174
    .local v5, "startLine":Ljava/lang/String;
    :try_start_0
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    .line 180
    if-nez v5, :cond_0

    .line 201
    :goto_0
    return-object v8

    .line 175
    :catch_0
    move-exception v2

    .line 176
    .local v2, "e":Ljava/io/IOException;
    const-string v9, "SsdpReader"

    const-string v10, "Error reading line"

    invoke-static {v9, v10, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 184
    .end local v2    # "e":Ljava/io/IOException;
    :cond_0
    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/google/android/music/dial/SsdpMessage$MessageType;->fromString(Ljava/lang/String;)Lcom/google/android/music/dial/SsdpMessage$MessageType;

    move-result-object v7

    .line 185
    .local v7, "type":Lcom/google/android/music/dial/SsdpMessage$MessageType;
    new-instance v1, Lcom/google/android/music/dial/SsdpMessage$Builder;

    invoke-direct {v1, v7}, Lcom/google/android/music/dial/SsdpMessage$Builder;-><init>(Lcom/google/android/music/dial/SsdpMessage$MessageType;)V

    .line 186
    .local v1, "builder":Lcom/google/android/music/dial/SsdpMessage$Builder;
    const/4 v3, 0x0

    .line 189
    .local v3, "line":Ljava/lang/String;
    :cond_1
    :goto_1
    :try_start_1
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 190
    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    .line 191
    const-string v9, ":"

    const/4 v10, 0x2

    invoke-virtual {v3, v9, v10}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v4

    .line 192
    .local v4, "nameValue":[Ljava/lang/String;
    array-length v9, v4

    if-ne v9, v11, :cond_1

    .line 193
    const/4 v9, 0x0

    aget-object v9, v4, v9

    const/4 v10, 0x1

    aget-object v10, v4, v10

    invoke-virtual {v1, v9, v10}, Lcom/google/android/music/dial/SsdpMessage$Builder;->withHeader(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/music/dial/SsdpMessage$Builder;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 196
    .end local v4    # "nameValue":[Ljava/lang/String;
    :catch_1
    move-exception v2

    .line 197
    .restart local v2    # "e":Ljava/io/IOException;
    const-string v9, "SsdpReader"

    const-string v10, "Error reading line"

    invoke-static {v9, v10, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 201
    .end local v2    # "e":Ljava/io/IOException;
    :cond_2
    invoke-virtual {v1}, Lcom/google/android/music/dial/SsdpMessage$Builder;->build()Lcom/google/android/music/dial/SsdpMessage;

    move-result-object v8

    goto :goto_0
.end method


# virtual methods
.method public getFirstHeaderValue(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 133
    invoke-virtual {p0, p1}, Lcom/google/android/music/dial/SsdpMessage;->getHeaderValues(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 134
    .local v0, "values":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 135
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 137
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getHeaderValues(Ljava/lang/String;)Ljava/util/List;
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 120
    iget-object v0, p0, Lcom/google/android/music/dial/SsdpMessage;->mHeaders:Ljava/util/LinkedHashMap;

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public getMessageType()Lcom/google/android/music/dial/SsdpMessage$MessageType;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/google/android/music/dial/SsdpMessage;->mType:Lcom/google/android/music/dial/SsdpMessage$MessageType;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 8

    .prologue
    .line 147
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 148
    .local v3, "sb":Ljava/lang/StringBuilder;
    iget-object v6, p0, Lcom/google/android/music/dial/SsdpMessage;->mType:Lcom/google/android/music/dial/SsdpMessage$MessageType;

    invoke-virtual {v6}, Lcom/google/android/music/dial/SsdpMessage$MessageType;->getStartLine()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\r\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 149
    iget-object v6, p0, Lcom/google/android/music/dial/SsdpMessage;->mHeaders:Ljava/util/LinkedHashMap;

    invoke-virtual {v6}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 150
    .local v2, "name":Ljava/lang/String;
    iget-object v6, p0, Lcom/google/android/music/dial/SsdpMessage;->mHeaders:Ljava/util/LinkedHashMap;

    invoke-virtual {v6, v2}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/List;

    .line 151
    .local v5, "values":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v5, :cond_0

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v6

    if-eqz v6, :cond_0

    .line 154
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 155
    .local v4, "value":Ljava/lang/String;
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ": "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\r\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 158
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "name":Ljava/lang/String;
    .end local v4    # "value":Ljava/lang/String;
    .end local v5    # "values":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_1
    const-string v6, "\r\n"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 159
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    return-object v6
.end method
