.class Lcom/google/android/music/dial/RemoteDeviceManager$1;
.super Ljava/lang/Object;
.source "RemoteDeviceManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/dial/RemoteDeviceManager;->handleSsdpMessage(Lcom/google/android/music/dial/SsdpMessage;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/dial/RemoteDeviceManager;

.field final synthetic val$message:Lcom/google/android/music/dial/SsdpMessage;

.field final synthetic val$usn:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/music/dial/RemoteDeviceManager;Ljava/lang/String;Lcom/google/android/music/dial/SsdpMessage;)V
    .locals 0

    .prologue
    .line 518
    iput-object p1, p0, Lcom/google/android/music/dial/RemoteDeviceManager$1;->this$0:Lcom/google/android/music/dial/RemoteDeviceManager;

    iput-object p2, p0, Lcom/google/android/music/dial/RemoteDeviceManager$1;->val$usn:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/music/dial/RemoteDeviceManager$1;->val$message:Lcom/google/android/music/dial/SsdpMessage;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 521
    iget-object v1, p0, Lcom/google/android/music/dial/RemoteDeviceManager$1;->val$usn:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/music/dial/RemoteDeviceManager$1;->val$message:Lcom/google/android/music/dial/SsdpMessage;

    # invokes: Lcom/google/android/music/dial/RemoteDeviceManager;->remoteDeviceInfoFromSsdpMessage(Ljava/lang/String;Lcom/google/android/music/dial/SsdpMessage;)Lcom/google/android/music/dial/RemoteDeviceInfo;
    invoke-static {v1, v2}, Lcom/google/android/music/dial/RemoteDeviceManager;->access$000(Ljava/lang/String;Lcom/google/android/music/dial/SsdpMessage;)Lcom/google/android/music/dial/RemoteDeviceInfo;

    move-result-object v0

    .line 522
    .local v0, "device":Lcom/google/android/music/dial/RemoteDeviceInfo;
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/music/dial/RemoteDeviceManager$1;->this$0:Lcom/google/android/music/dial/RemoteDeviceManager;

    # invokes: Lcom/google/android/music/dial/RemoteDeviceManager;->validRemoteDeviceInfo(Lcom/google/android/music/dial/RemoteDeviceInfo;)Z
    invoke-static {v1, v0}, Lcom/google/android/music/dial/RemoteDeviceManager;->access$100(Lcom/google/android/music/dial/RemoteDeviceManager;Lcom/google/android/music/dial/RemoteDeviceInfo;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 523
    iget-object v1, p0, Lcom/google/android/music/dial/RemoteDeviceManager$1;->this$0:Lcom/google/android/music/dial/RemoteDeviceManager;

    # getter for: Lcom/google/android/music/dial/RemoteDeviceManager;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/google/android/music/dial/RemoteDeviceManager;->access$200(Lcom/google/android/music/dial/RemoteDeviceManager;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/dial/RemoteDeviceManager$1;->this$0:Lcom/google/android/music/dial/RemoteDeviceManager;

    # getter for: Lcom/google/android/music/dial/RemoteDeviceManager;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/google/android/music/dial/RemoteDeviceManager;->access$200(Lcom/google/android/music/dial/RemoteDeviceManager;)Landroid/os/Handler;

    move-result-object v2

    const/16 v3, 0x8

    invoke-virtual {v2, v3, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 526
    :cond_0
    return-void
.end method
