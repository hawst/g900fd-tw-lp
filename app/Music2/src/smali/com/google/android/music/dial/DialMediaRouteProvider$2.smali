.class Lcom/google/android/music/dial/DialMediaRouteProvider$2;
.super Ljava/lang/Object;
.source "DialMediaRouteProvider.java"

# interfaces
.implements Lcom/google/android/music/dial/RemoteDeviceManager$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/dial/DialMediaRouteProvider;->createRemoteDeviceManager()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/dial/DialMediaRouteProvider;


# direct methods
.method constructor <init>(Lcom/google/android/music/dial/DialMediaRouteProvider;)V
    .locals 0

    .prologue
    .line 191
    iput-object p1, p0, Lcom/google/android/music/dial/DialMediaRouteProvider$2;->this$0:Lcom/google/android/music/dial/DialMediaRouteProvider;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDeviceOffline(Lcom/google/android/music/dial/RemoteDeviceInfo;)V
    .locals 4
    .param p1, "deviceInfo"    # Lcom/google/android/music/dial/RemoteDeviceInfo;

    .prologue
    .line 209
    # getter for: Lcom/google/android/music/dial/DialMediaRouteProvider;->LOGV:Z
    invoke-static {}, Lcom/google/android/music/dial/DialMediaRouteProvider;->access$000()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 210
    const-string v0, "DialMediaRouteProvider"

    const-string v1, "onDeviceOffline %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 212
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/dial/DialMediaRouteProvider$2;->this$0:Lcom/google/android/music/dial/DialMediaRouteProvider;

    # invokes: Lcom/google/android/music/dial/DialMediaRouteProvider;->publishRoutes()V
    invoke-static {v0}, Lcom/google/android/music/dial/DialMediaRouteProvider;->access$200(Lcom/google/android/music/dial/DialMediaRouteProvider;)V

    .line 213
    return-void
.end method

.method public onDeviceOnline(Lcom/google/android/music/dial/RemoteDeviceInfo;)V
    .locals 4
    .param p1, "deviceInfo"    # Lcom/google/android/music/dial/RemoteDeviceInfo;

    .prologue
    .line 201
    # getter for: Lcom/google/android/music/dial/DialMediaRouteProvider;->LOGV:Z
    invoke-static {}, Lcom/google/android/music/dial/DialMediaRouteProvider;->access$000()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 202
    const-string v0, "DialMediaRouteProvider"

    const-string v1, "onDeviceOnline %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 204
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/dial/DialMediaRouteProvider$2;->this$0:Lcom/google/android/music/dial/DialMediaRouteProvider;

    # invokes: Lcom/google/android/music/dial/DialMediaRouteProvider;->publishRoutes()V
    invoke-static {v0}, Lcom/google/android/music/dial/DialMediaRouteProvider;->access$200(Lcom/google/android/music/dial/DialMediaRouteProvider;)V

    .line 205
    return-void
.end method

.method public onScanError(Lcom/google/android/music/dial/RemoteDeviceManager$Listener$ScanError;)V
    .locals 5
    .param p1, "error"    # Lcom/google/android/music/dial/RemoteDeviceManager$Listener$ScanError;

    .prologue
    .line 194
    # getter for: Lcom/google/android/music/dial/DialMediaRouteProvider;->LOGV:Z
    invoke-static {}, Lcom/google/android/music/dial/DialMediaRouteProvider;->access$000()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 195
    const-string v0, "DialMediaRouteProvider"

    const-string v1, "onScanError error: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p1}, Lcom/google/android/music/dial/RemoteDeviceManager$Listener$ScanError;->name()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    :cond_0
    return-void
.end method
