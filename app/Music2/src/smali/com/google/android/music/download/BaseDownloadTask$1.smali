.class Lcom/google/android/music/download/BaseDownloadTask$1;
.super Lcom/google/android/music/net/IStreamabilityChangeListener$Stub;
.source "BaseDownloadTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/download/BaseDownloadTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/download/BaseDownloadTask;


# direct methods
.method constructor <init>(Lcom/google/android/music/download/BaseDownloadTask;)V
    .locals 0

    .prologue
    .line 354
    .local p0, "this":Lcom/google/android/music/download/BaseDownloadTask$1;, "Lcom/google/android/music/download/BaseDownloadTask.1;"
    iput-object p1, p0, Lcom/google/android/music/download/BaseDownloadTask$1;->this$0:Lcom/google/android/music/download/BaseDownloadTask;

    invoke-direct {p0}, Lcom/google/android/music/net/IStreamabilityChangeListener$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onStreamabilityChanged(Z)V
    .locals 4
    .param p1, "isStreamable"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 358
    .local p0, "this":Lcom/google/android/music/download/BaseDownloadTask$1;, "Lcom/google/android/music/download/BaseDownloadTask.1;"
    iget-object v0, p0, Lcom/google/android/music/download/BaseDownloadTask$1;->this$0:Lcom/google/android/music/download/BaseDownloadTask;

    # getter for: Lcom/google/android/music/download/BaseDownloadTask;->mStreamingEnabled:Ljava/util/concurrent/atomic/AtomicBoolean;
    invoke-static {v0}, Lcom/google/android/music/download/BaseDownloadTask;->access$000(Lcom/google/android/music/download/BaseDownloadTask;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eq v0, p1, :cond_0

    .line 359
    iget-object v0, p0, Lcom/google/android/music/download/BaseDownloadTask$1;->this$0:Lcom/google/android/music/download/BaseDownloadTask;

    # getter for: Lcom/google/android/music/download/BaseDownloadTask;->mLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/google/android/music/download/BaseDownloadTask;->access$100(Lcom/google/android/music/download/BaseDownloadTask;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 360
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/download/BaseDownloadTask$1;->this$0:Lcom/google/android/music/download/BaseDownloadTask;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    # setter for: Lcom/google/android/music/download/BaseDownloadTask;->mStreamingEnabledChangedTime:J
    invoke-static {v0, v2, v3}, Lcom/google/android/music/download/BaseDownloadTask;->access$202(Lcom/google/android/music/download/BaseDownloadTask;J)J

    .line 361
    iget-object v0, p0, Lcom/google/android/music/download/BaseDownloadTask$1;->this$0:Lcom/google/android/music/download/BaseDownloadTask;

    # getter for: Lcom/google/android/music/download/BaseDownloadTask;->mStreamingEnabled:Ljava/util/concurrent/atomic/AtomicBoolean;
    invoke-static {v0}, Lcom/google/android/music/download/BaseDownloadTask;->access$000(Lcom/google/android/music/download/BaseDownloadTask;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 362
    iget-object v0, p0, Lcom/google/android/music/download/BaseDownloadTask$1;->this$0:Lcom/google/android/music/download/BaseDownloadTask;

    # getter for: Lcom/google/android/music/download/BaseDownloadTask;->mLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/google/android/music/download/BaseDownloadTask;->access$100(Lcom/google/android/music/download/BaseDownloadTask;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 363
    monitor-exit v1

    .line 365
    :cond_0
    return-void

    .line 363
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
