.class public Lcom/google/android/music/medialist/AllAlbumsList;
.super Lcom/google/android/music/medialist/AlbumList;
.source "AllAlbumsList.java"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/music/medialist/AllAlbumsList;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    new-instance v0, Lcom/google/android/music/medialist/AllAlbumsList$1;

    invoke-direct {v0}, Lcom/google/android/music/medialist/AllAlbumsList$1;-><init>()V

    sput-object v0, Lcom/google/android/music/medialist/AllAlbumsList;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 15
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/music/medialist/AlbumList;-><init>(Z)V

    .line 16
    return-void
.end method


# virtual methods
.method public getContentUri(Landroid/content/Context;)Landroid/net/Uri;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 20
    invoke-static {}, Lcom/google/android/music/store/MusicContent$Albums;->getAllAlbumsUri()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method
