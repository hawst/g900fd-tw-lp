.class public Lcom/google/android/music/ui/MainstageDocumentHelper;
.super Ljava/lang/Object;
.source "MainstageDocumentHelper.java"


# static fields
.field public static final MAINSTAGE_PROJECTION:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 17
    const/16 v0, 0x1d

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "album_id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "album_name"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "playlist_id"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "playlist_name"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "playlist_type"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "playlist_share_token"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "playlist_art_url"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "playlist_owner_name"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "playlist_owner_profile_photo_url"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "album_artist_id"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "album_artist"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "radio_id"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "radio_name"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "radio_art"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "reason"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "StoreAlbumId"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "artworkUrl"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "hasLocal"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "ArtistMetajamId"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "radio_seed_source_type"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "radio_seed_source_id"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "radio_source_id"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "reason_text"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "mainstage_description"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "album_artist_profile_image"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "playlist_editor_artwork"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "radio_highlight_color"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "radio_profile_image"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/ui/MainstageDocumentHelper;->MAINSTAGE_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public static buildMainstageDocument(Landroid/content/Context;Lcom/google/android/music/ui/cardlib/model/Document;Landroid/database/Cursor;)Lcom/google/android/music/ui/cardlib/model/Document;
    .locals 30
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "doc"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p2, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 82
    if-nez p2, :cond_0

    const/16 p1, 0x0

    .line 211
    .end local p1    # "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    :goto_0
    return-object p1

    .line 85
    .restart local p1    # "doc":Lcom/google/android/music/ui/cardlib/model/Document;
    :cond_0
    const/16 v27, 0x1

    move-object/from16 v0, p2

    move/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v27

    if-nez v27, :cond_4

    .line 86
    const/16 v27, 0x2

    move-object/from16 v0, p2

    move/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 87
    .local v9, "albumName":Ljava/lang/String;
    const/16 v27, 0x1

    move-object/from16 v0, p2

    move/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 88
    .local v6, "albumId":J
    const/16 v27, 0xa

    move-object/from16 v0, p2

    move/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    .line 89
    .local v12, "artistId":J
    const/16 v27, 0xb

    move-object/from16 v0, p2

    move/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 90
    .local v4, "albumArtistName":Ljava/lang/String;
    sget-object v27, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ALBUM:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/cardlib/model/Document;->setType(Lcom/google/android/music/ui/cardlib/model/Document$Type;)V

    .line 91
    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v7}, Lcom/google/android/music/ui/cardlib/model/Document;->setId(J)V

    .line 92
    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Lcom/google/android/music/ui/cardlib/model/Document;->setTitle(Ljava/lang/String;)V

    .line 93
    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lcom/google/android/music/ui/cardlib/model/Document;->setSubTitle(Ljava/lang/String;)V

    .line 94
    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v7}, Lcom/google/android/music/ui/cardlib/model/Document;->setAlbumId(J)V

    .line 95
    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Lcom/google/android/music/ui/cardlib/model/Document;->setAlbumName(Ljava/lang/String;)V

    .line 96
    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lcom/google/android/music/ui/cardlib/model/Document;->setArtistName(Ljava/lang/String;)V

    .line 97
    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v13}, Lcom/google/android/music/ui/cardlib/model/Document;->setArtistId(J)V

    .line 98
    const/16 v27, 0x10

    move-object/from16 v0, p2

    move/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v27

    if-nez v27, :cond_1

    .line 99
    const/16 v27, 0x10

    move-object/from16 v0, p2

    move/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/cardlib/model/Document;->setAlbumMetajamId(Ljava/lang/String;)V

    .line 101
    :cond_1
    const/16 v27, 0x13

    move-object/from16 v0, p2

    move/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v27

    if-nez v27, :cond_2

    .line 102
    const/16 v27, 0x13

    move-object/from16 v0, p2

    move/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/cardlib/model/Document;->setArtistMetajamId(Ljava/lang/String;)V

    .line 198
    .end local v4    # "albumArtistName":Ljava/lang/String;
    .end local v6    # "albumId":J
    .end local v9    # "albumName":Ljava/lang/String;
    .end local v12    # "artistId":J
    :cond_2
    :goto_1
    const/16 v27, 0xf

    move-object/from16 v0, p2

    move/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v24

    .line 199
    .local v24, "reasonType":I
    const/16 v27, 0x12

    move-object/from16 v0, p2

    move/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v27

    if-eqz v27, :cond_11

    const/16 v27, 0x1

    :goto_2
    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/cardlib/model/Document;->setHasLocal(Z)V

    .line 200
    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/cardlib/model/Document;->setMainstageReason(I)V

    .line 201
    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->getPosition()I

    move-result v27

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/cardlib/model/Document;->setPosition(I)V

    .line 202
    const/16 v27, 0x18

    move-object/from16 v0, p2

    move/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/cardlib/model/Document;->setDescription(Ljava/lang/String;)V

    .line 205
    const/16 v27, 0x17

    move-object/from16 v0, p2

    move/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v23

    .line 206
    .local v23, "reason":Ljava/lang/String;
    invoke-static/range {v23 .. v23}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v27

    if-eqz v27, :cond_3

    .line 207
    invoke-static/range {p0 .. p1}, Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;->getReasonString(Landroid/content/Context;Lcom/google/android/music/ui/cardlib/model/Document;)Ljava/lang/String;

    move-result-object v23

    .line 209
    :cond_3
    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/cardlib/model/Document;->setReason1(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 104
    .end local v23    # "reason":Ljava/lang/String;
    .end local v24    # "reasonType":I
    :cond_4
    const/16 v27, 0x3

    move-object/from16 v0, p2

    move/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v27

    if-nez v27, :cond_7

    .line 105
    const/16 v27, 0x3

    move-object/from16 v0, p2

    move/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v16

    .line 106
    .local v16, "playlistId":J
    const/16 v27, 0x4

    move-object/from16 v0, p2

    move/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v18

    .line 107
    .local v18, "playlistName":Ljava/lang/String;
    move-object/from16 v0, p1

    move-wide/from16 v1, v16

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/ui/cardlib/model/Document;->setId(J)V

    .line 108
    sget-object v27, Lcom/google/android/music/ui/cardlib/model/Document$Type;->PLAYLIST:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/cardlib/model/Document;->setType(Lcom/google/android/music/ui/cardlib/model/Document$Type;)V

    .line 109
    const/16 v27, 0x5

    move-object/from16 v0, p2

    move/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v19

    .line 110
    .local v19, "playlistType":I
    const/16 v27, 0x8

    move-object/from16 v0, p2

    move/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 111
    .local v11, "ownerName":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-static {v0, v1, v2}, Lcom/google/android/music/utils/LabelUtils;->getPlaylistPrimaryLabel(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v26

    .line 114
    .local v26, "title":Ljava/lang/String;
    const/16 v27, 0x47

    move/from16 v0, v19

    move/from16 v1, v27

    if-eq v0, v1, :cond_5

    const/16 v27, 0x46

    move/from16 v0, v19

    move/from16 v1, v27

    if-eq v0, v1, :cond_5

    const/16 v27, 0x33

    move/from16 v0, v19

    move/from16 v1, v27

    if-ne v0, v1, :cond_6

    :cond_5
    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v27

    if-nez v27, :cond_6

    .line 118
    const v27, 0x7f0b0102

    const/16 v28, 0x1

    move/from16 v0, v28

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v28, v0

    const/16 v29, 0x0

    aput-object v11, v28, v29

    move-object/from16 v0, p0

    move/from16 v1, v27

    move-object/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v25

    .line 125
    .local v25, "subtitle":Ljava/lang/String;
    :goto_3
    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/cardlib/model/Document;->setTitle(Ljava/lang/String;)V

    .line 126
    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/cardlib/model/Document;->setPlaylistName(Ljava/lang/String;)V

    .line 127
    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/cardlib/model/Document;->setSubTitle(Ljava/lang/String;)V

    .line 128
    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/cardlib/model/Document;->setPlaylistType(I)V

    .line 129
    const/16 v27, 0x6

    move-object/from16 v0, p2

    move/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/cardlib/model/Document;->setPlaylistShareToken(Ljava/lang/String;)V

    .line 130
    const/16 v27, 0x7

    move-object/from16 v0, p2

    move/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/cardlib/model/Document;->setArtUrl(Ljava/lang/String;)V

    .line 131
    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Lcom/google/android/music/ui/cardlib/model/Document;->setPlaylistOwnerName(Ljava/lang/String;)V

    .line 132
    const/16 v27, 0x9

    move-object/from16 v0, p2

    move/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/cardlib/model/Document;->setProfilePhotoUrl(Ljava/lang/String;)V

    .line 134
    const/16 v27, 0x1a

    move-object/from16 v0, p2

    move/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/cardlib/model/Document;->setPlaylistEditorArtwork(Ljava/lang/String;)V

    .line 136
    const/16 v27, 0x15

    move-object/from16 v0, p2

    move/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v27

    if-nez v27, :cond_2

    .line 137
    const/16 v27, 0x15

    move-object/from16 v0, p2

    move/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/cardlib/model/Document;->setRadioSeedId(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 120
    .end local v25    # "subtitle":Ljava/lang/String;
    :cond_6
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v27

    move-object/from16 v0, p0

    move-object/from16 v1, v27

    move/from16 v2, v19

    invoke-static {v0, v1, v2}, Lcom/google/android/music/utils/LabelUtils;->getPlaylistSecondaryLabel(Landroid/content/Context;Lcom/google/android/music/preferences/MusicPreferences;I)Ljava/lang/String;

    move-result-object v25

    .restart local v25    # "subtitle":Ljava/lang/String;
    goto/16 :goto_3

    .line 139
    .end local v11    # "ownerName":Ljava/lang/String;
    .end local v16    # "playlistId":J
    .end local v18    # "playlistName":Ljava/lang/String;
    .end local v19    # "playlistType":I
    .end local v25    # "subtitle":Ljava/lang/String;
    .end local v26    # "title":Ljava/lang/String;
    :cond_7
    const/16 v27, 0xc

    move-object/from16 v0, p2

    move/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v27

    if-nez v27, :cond_d

    .line 140
    const/16 v27, 0xc

    move-object/from16 v0, p2

    move/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 141
    .local v14, "id":J
    const/16 v27, 0xd

    move-object/from16 v0, p2

    move/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v26

    .line 142
    .restart local v26    # "title":Ljava/lang/String;
    const/16 v27, 0xe

    move-object/from16 v0, p2

    move/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 143
    .local v10, "artUrl":Ljava/lang/String;
    sget-object v27, Lcom/google/android/music/ui/cardlib/model/Document$Type;->RADIO:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/cardlib/model/Document;->setType(Lcom/google/android/music/ui/cardlib/model/Document$Type;)V

    .line 144
    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v15}, Lcom/google/android/music/ui/cardlib/model/Document;->setId(J)V

    .line 145
    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/cardlib/model/Document;->setTitle(Ljava/lang/String;)V

    .line 146
    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Lcom/google/android/music/ui/cardlib/model/Document;->setArtUrl(Ljava/lang/String;)V

    .line 147
    const/16 v27, 0x15

    move-object/from16 v0, p2

    move/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v27

    if-eqz v27, :cond_9

    const/16 v21, 0x0

    .line 149
    .local v21, "radioSeedId":Ljava/lang/String;
    :goto_4
    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/cardlib/model/Document;->setRadioSeedId(Ljava/lang/String;)V

    .line 150
    const/16 v27, 0x14

    move-object/from16 v0, p2

    move/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v27

    if-eqz v27, :cond_a

    const/16 v22, -0x1

    .line 152
    .local v22, "radioSeedType":I
    :goto_5
    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/cardlib/model/Document;->setRadioSeedType(I)V

    .line 153
    const/16 v27, 0x16

    move-object/from16 v0, p2

    move/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v27

    if-eqz v27, :cond_b

    const/16 v20, 0x0

    .line 155
    .local v20, "radioRemoteId":Ljava/lang/String;
    :goto_6
    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/cardlib/model/Document;->setRadioRemoteId(Ljava/lang/String;)V

    .line 156
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Lcom/google/android/music/preferences/MusicPreferences;->isNautilusEnabled()Z

    move-result v27

    if-eqz v27, :cond_c

    .line 157
    const v27, 0x7f0b0264

    move-object/from16 v0, p0

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/cardlib/model/Document;->setSubTitle(Ljava/lang/String;)V

    .line 161
    :goto_7
    const/16 v27, 0x1b

    move-object/from16 v0, p2

    move/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/cardlib/model/Document;->setRadioHighlightColor(Ljava/lang/String;)V

    .line 162
    const/16 v27, 0x1c

    move-object/from16 v0, p2

    move/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/cardlib/model/Document;->setProfilePhotoUrl(Ljava/lang/String;)V

    .line 166
    const/16 v27, 0x4

    move/from16 v0, v22

    move/from16 v1, v27

    if-eq v0, v1, :cond_8

    const/16 v27, 0x7

    move/from16 v0, v22

    move/from16 v1, v27

    if-ne v0, v1, :cond_2

    .line 168
    :cond_8
    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Lcom/google/android/music/ui/cardlib/model/Document;->setProfilePhotoUrl(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 147
    .end local v20    # "radioRemoteId":Ljava/lang/String;
    .end local v21    # "radioSeedId":Ljava/lang/String;
    .end local v22    # "radioSeedType":I
    :cond_9
    const/16 v27, 0x15

    move-object/from16 v0, p2

    move/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v21

    goto/16 :goto_4

    .line 150
    .restart local v21    # "radioSeedId":Ljava/lang/String;
    :cond_a
    const/16 v27, 0x14

    move-object/from16 v0, p2

    move/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v22

    goto/16 :goto_5

    .line 153
    .restart local v22    # "radioSeedType":I
    :cond_b
    const/16 v27, 0x16

    move-object/from16 v0, p2

    move/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v20

    goto/16 :goto_6

    .line 159
    .restart local v20    # "radioRemoteId":Ljava/lang/String;
    :cond_c
    const v27, 0x7f0b0265

    move-object/from16 v0, p0

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/cardlib/model/Document;->setSubTitle(Ljava/lang/String;)V

    goto :goto_7

    .line 171
    .end local v10    # "artUrl":Ljava/lang/String;
    .end local v14    # "id":J
    .end local v20    # "radioRemoteId":Ljava/lang/String;
    .end local v21    # "radioSeedId":Ljava/lang/String;
    .end local v22    # "radioSeedType":I
    .end local v26    # "title":Ljava/lang/String;
    :cond_d
    const/16 v27, 0x10

    move-object/from16 v0, p2

    move/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v27

    if-nez v27, :cond_10

    .line 176
    const/16 v27, 0x10

    move-object/from16 v0, p2

    move/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 177
    .local v8, "albumMetajamId":Ljava/lang/String;
    const/16 v27, 0x2

    move-object/from16 v0, p2

    move/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 178
    .restart local v9    # "albumName":Ljava/lang/String;
    const/16 v27, 0xb

    move-object/from16 v0, p2

    move/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 179
    .restart local v4    # "albumArtistName":Ljava/lang/String;
    const/16 v27, 0x19

    move-object/from16 v0, p2

    move/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 180
    .local v5, "albumArtistProfileImage":Ljava/lang/String;
    const/16 v27, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/cardlib/model/Document;->setIsNautilus(Z)V

    .line 181
    sget-object v27, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ALBUM:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/cardlib/model/Document;->setType(Lcom/google/android/music/ui/cardlib/model/Document$Type;)V

    .line 182
    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Lcom/google/android/music/ui/cardlib/model/Document;->setAlbumMetajamId(Ljava/lang/String;)V

    .line 183
    const/16 v27, 0x11

    move-object/from16 v0, p2

    move/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v27

    if-nez v27, :cond_e

    .line 184
    const/16 v27, 0x11

    move-object/from16 v0, p2

    move/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/cardlib/model/Document;->setArtUrl(Ljava/lang/String;)V

    .line 186
    :cond_e
    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Lcom/google/android/music/ui/cardlib/model/Document;->setAlbumName(Ljava/lang/String;)V

    .line 187
    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Lcom/google/android/music/ui/cardlib/model/Document;->setTitle(Ljava/lang/String;)V

    .line 188
    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lcom/google/android/music/ui/cardlib/model/Document;->setSubTitle(Ljava/lang/String;)V

    .line 189
    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lcom/google/android/music/ui/cardlib/model/Document;->setArtistName(Ljava/lang/String;)V

    .line 190
    const/16 v27, 0x13

    move-object/from16 v0, p2

    move/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v27

    if-nez v27, :cond_f

    .line 191
    const/16 v27, 0x13

    move-object/from16 v0, p2

    move/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/cardlib/model/Document;->setArtistMetajamId(Ljava/lang/String;)V

    .line 193
    :cond_f
    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/google/android/music/ui/cardlib/model/Document;->setProfilePhotoUrl(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 195
    .end local v4    # "albumArtistName":Ljava/lang/String;
    .end local v5    # "albumArtistProfileImage":Ljava/lang/String;
    .end local v8    # "albumMetajamId":Ljava/lang/String;
    .end local v9    # "albumName":Ljava/lang/String;
    :cond_10
    const-string v27, "MainstageDocumentHelper"

    const-string v28, "Unexpected mainstage item"

    new-instance v29, Ljava/lang/Exception;

    invoke-direct/range {v29 .. v29}, Ljava/lang/Exception;-><init>()V

    invoke-static/range {v27 .. v29}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_1

    .line 199
    .restart local v24    # "reasonType":I
    :cond_11
    const/16 v27, 0x0

    goto/16 :goto_2
.end method
