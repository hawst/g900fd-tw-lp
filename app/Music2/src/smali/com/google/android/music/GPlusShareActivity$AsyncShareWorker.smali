.class Lcom/google/android/music/GPlusShareActivity$AsyncShareWorker;
.super Lcom/google/android/music/utils/LoggableHandler;
.source "GPlusShareActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/GPlusShareActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AsyncShareWorker"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/GPlusShareActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/music/GPlusShareActivity;)V
    .locals 1

    .prologue
    .line 161
    iput-object p1, p0, Lcom/google/android/music/GPlusShareActivity$AsyncShareWorker;->this$0:Lcom/google/android/music/GPlusShareActivity;

    .line 162
    const-string v0, "GPlusShareMusic"

    invoke-direct {p0, v0}, Lcom/google/android/music/utils/LoggableHandler;-><init>(Ljava/lang/String;)V

    .line 163
    return-void
.end method

.method private createShareUrl()Lcom/google/android/music/SharePreviewResponse;
    .locals 22

    .prologue
    .line 191
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/music/GPlusShareActivity$AsyncShareWorker;->this$0:Lcom/google/android/music/GPlusShareActivity;

    .line 192
    .local v6, "context":Landroid/content/Context;
    new-instance v12, Lcom/google/android/music/sync/google/MusicAuthInfo;

    invoke-direct {v12, v6}, Lcom/google/android/music/sync/google/MusicAuthInfo;-><init>(Landroid/content/Context;)V

    .line 193
    .local v12, "musicAuthInfo":Lcom/google/android/music/sync/google/MusicAuthInfo;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/GPlusShareActivity$AsyncShareWorker;->this$0:Lcom/google/android/music/GPlusShareActivity;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/music/GPlusShareActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v17

    const-string v18, "android_id"

    const-wide/16 v20, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move-wide/from16 v2, v20

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gsf/Gservices;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v18

    invoke-static/range {v18 .. v19}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    .line 195
    .local v4, "androidId":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/GPlusShareActivity$AsyncShareWorker;->this$0:Lcom/google/android/music/GPlusShareActivity;

    move-object/from16 v17, v0

    # getter for: Lcom/google/android/music/GPlusShareActivity;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;
    invoke-static/range {v17 .. v17}, Lcom/google/android/music/GPlusShareActivity;->access$400(Lcom/google/android/music/GPlusShareActivity;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/music/preferences/MusicPreferences;->getLoggingId()Ljava/lang/String;

    move-result-object v11

    .line 197
    .local v11, "loggingId":Ljava/lang/String;
    invoke-static {v6}, Lcom/google/android/music/download/DownloadUtils;->getUserAgent(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v17

    const/16 v18, 0x1

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-static {v6, v0, v1}, Lcom/google/android/music/GoogleHttpClientFactory;->createGoogleHttpClient(Landroid/content/Context;Ljava/lang/String;Z)Lcom/google/android/music/cloudclient/MusicHttpClient;

    move-result-object v9

    .line 200
    .local v9, "httpClient":Lcom/google/android/music/cloudclient/MusicHttpClient;
    :try_start_0
    sget-object v18, Lcom/google/android/music/utils/DebugUtils;->HTTP_TAG:Ljava/lang/String;

    invoke-static {}, Lcom/google/android/music/utils/DebugUtils;->isAutoLogAll()Z

    move-result v17

    if-eqz v17, :cond_1

    const/16 v17, 0x3

    :goto_0
    move-object/from16 v0, v18

    move/from16 v1, v17

    invoke-virtual {v9, v0, v1}, Lcom/google/android/music/cloudclient/MusicHttpClient;->enableCurlLogging(Ljava/lang/String;I)V

    .line 203
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/GPlusShareActivity$AsyncShareWorker;->this$0:Lcom/google/android/music/GPlusShareActivity;

    move-object/from16 v17, v0

    # getter for: Lcom/google/android/music/GPlusShareActivity;->mStreamingAccount:Landroid/accounts/Account;
    invoke-static/range {v17 .. v17}, Lcom/google/android/music/GPlusShareActivity;->access$500(Lcom/google/android/music/GPlusShareActivity;)Landroid/accounts/Account;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v12, v0}, Lcom/google/android/music/sync/google/MusicAuthInfo;->getAuthToken(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v5

    .line 204
    .local v5, "authToken":Ljava/lang/String;
    new-instance v8, Lorg/apache/http/client/methods/HttpGet;

    const-string v17, "https://music.google.com/music/sharepreview?storeId=%s&source=music-mobile&u=0"

    const/16 v18, 0x1

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/GPlusShareActivity$AsyncShareWorker;->this$0:Lcom/google/android/music/GPlusShareActivity;

    move-object/from16 v20, v0

    # getter for: Lcom/google/android/music/GPlusShareActivity;->mStoreId:Ljava/lang/String;
    invoke-static/range {v20 .. v20}, Lcom/google/android/music/GPlusShareActivity;->access$600(Lcom/google/android/music/GPlusShareActivity;)Ljava/lang/String;

    move-result-object v20

    aput-object v20, v18, v19

    invoke-static/range {v17 .. v18}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-direct {v8, v0}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 205
    .local v8, "get":Lorg/apache/http/client/methods/HttpGet;
    const-string v17, "Authorization"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "GoogleLogin auth="

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v8, v0, v1}, Lorg/apache/http/client/methods/HttpGet;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    const-string v17, "X-Device-Logging-ID"

    move-object/from16 v0, v17

    invoke-virtual {v8, v0, v11}, Lorg/apache/http/client/methods/HttpGet;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 207
    const-string v17, "X-Device-ID"

    move-object/from16 v0, v17

    invoke-virtual {v8, v0, v4}, Lorg/apache/http/client/methods/HttpGet;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 209
    invoke-virtual {v9, v8}, Lcom/google/android/music/cloudclient/MusicHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v14

    .line 210
    .local v14, "response":Lorg/apache/http/HttpResponse;
    invoke-interface {v14}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v16

    .line 211
    .local v16, "status":I
    const/16 v17, 0xc8

    move/from16 v0, v16

    move/from16 v1, v17

    if-lt v0, v1, :cond_2

    const/16 v17, 0x12c

    move/from16 v0, v16

    move/from16 v1, v17

    if-ge v0, v1, :cond_2

    .line 212
    invoke-interface {v14}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/google/android/music/SharePreviewResponse;->parseFromJsonInputStream(Ljava/io/InputStream;)Lcom/google/android/music/SharePreviewResponse;

    move-result-object v15

    .line 215
    .local v15, "sharePreview":Lcom/google/android/music/SharePreviewResponse;
    # getter for: Lcom/google/android/music/GPlusShareActivity;->LOGV:Z
    invoke-static {}, Lcom/google/android/music/GPlusShareActivity;->access$300()Z

    move-result v17

    if-eqz v17, :cond_0

    .line 216
    const-string v17, "GPlusShareMusic"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Got share url: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    iget-object v0, v15, Lcom/google/android/music/SharePreviewResponse;->mUrl:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 234
    :cond_0
    invoke-virtual {v9}, Lcom/google/android/music/cloudclient/MusicHttpClient;->close()V

    .line 236
    .end local v5    # "authToken":Ljava/lang/String;
    .end local v8    # "get":Lorg/apache/http/client/methods/HttpGet;
    .end local v14    # "response":Lorg/apache/http/HttpResponse;
    .end local v15    # "sharePreview":Lcom/google/android/music/SharePreviewResponse;
    .end local v16    # "status":I
    :goto_1
    return-object v15

    .line 200
    :cond_1
    const/16 v17, 0x2

    goto/16 :goto_0

    .line 220
    .restart local v5    # "authToken":Ljava/lang/String;
    .restart local v8    # "get":Lorg/apache/http/client/methods/HttpGet;
    .restart local v14    # "response":Lorg/apache/http/HttpResponse;
    .restart local v16    # "status":I
    :cond_2
    :try_start_1
    const-string v17, "GPlusShareMusic"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Got invalid response from server: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 221
    new-instance v13, Ljava/io/BufferedReader;

    new-instance v17, Ljava/io/InputStreamReader;

    invoke-interface {v14}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v18

    invoke-direct/range {v17 .. v18}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    move-object/from16 v0, v17

    invoke-direct {v13, v0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 224
    .local v13, "reader":Ljava/io/BufferedReader;
    :goto_2
    invoke-virtual {v13}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v10

    .local v10, "line":Ljava/lang/String;
    if-eqz v10, :cond_3

    .line 225
    const-string v17, "GPlusShareMusic"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Response: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/accounts/AuthenticatorException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 229
    .end local v5    # "authToken":Ljava/lang/String;
    .end local v8    # "get":Lorg/apache/http/client/methods/HttpGet;
    .end local v10    # "line":Ljava/lang/String;
    .end local v13    # "reader":Ljava/io/BufferedReader;
    .end local v14    # "response":Lorg/apache/http/HttpResponse;
    .end local v16    # "status":I
    :catch_0
    move-exception v7

    .line 230
    .local v7, "e":Landroid/accounts/AuthenticatorException;
    :try_start_2
    const-string v17, "GPlusShareMusic"

    invoke-virtual {v7}, Landroid/accounts/AuthenticatorException;->getMessage()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-static {v0, v1, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 234
    invoke-virtual {v9}, Lcom/google/android/music/cloudclient/MusicHttpClient;->close()V

    .line 236
    .end local v7    # "e":Landroid/accounts/AuthenticatorException;
    :goto_3
    const/4 v15, 0x0

    goto :goto_1

    .line 227
    .restart local v5    # "authToken":Ljava/lang/String;
    .restart local v8    # "get":Lorg/apache/http/client/methods/HttpGet;
    .restart local v10    # "line":Ljava/lang/String;
    .restart local v13    # "reader":Ljava/io/BufferedReader;
    .restart local v14    # "response":Lorg/apache/http/HttpResponse;
    .restart local v16    # "status":I
    :cond_3
    :try_start_3
    invoke-virtual {v13}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Landroid/accounts/AuthenticatorException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 234
    invoke-virtual {v9}, Lcom/google/android/music/cloudclient/MusicHttpClient;->close()V

    goto :goto_3

    .line 231
    .end local v5    # "authToken":Ljava/lang/String;
    .end local v8    # "get":Lorg/apache/http/client/methods/HttpGet;
    .end local v10    # "line":Ljava/lang/String;
    .end local v13    # "reader":Ljava/io/BufferedReader;
    .end local v14    # "response":Lorg/apache/http/HttpResponse;
    .end local v16    # "status":I
    :catch_1
    move-exception v7

    .line 232
    .local v7, "e":Ljava/io/IOException;
    :try_start_4
    const-string v17, "GPlusShareMusic"

    invoke-virtual {v7}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-static {v0, v1, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 234
    invoke-virtual {v9}, Lcom/google/android/music/cloudclient/MusicHttpClient;->close()V

    goto :goto_3

    .end local v7    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v17

    invoke-virtual {v9}, Lcom/google/android/music/cloudclient/MusicHttpClient;->close()V

    throw v17
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 167
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 186
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown message type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Landroid/os/Message;->what:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 169
    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/music/GPlusShareActivity$AsyncShareWorker;->createShareUrl()Lcom/google/android/music/SharePreviewResponse;

    move-result-object v0

    .line 170
    .local v0, "shareUrl":Lcom/google/android/music/SharePreviewResponse;
    iget-object v1, p0, Lcom/google/android/music/GPlusShareActivity$AsyncShareWorker;->this$0:Lcom/google/android/music/GPlusShareActivity;

    new-instance v2, Lcom/google/android/music/GPlusShareActivity$AsyncShareWorker$1;

    invoke-direct {v2, p0, v0}, Lcom/google/android/music/GPlusShareActivity$AsyncShareWorker$1;-><init>(Lcom/google/android/music/GPlusShareActivity$AsyncShareWorker;Lcom/google/android/music/SharePreviewResponse;)V

    invoke-virtual {v1, v2}, Lcom/google/android/music/GPlusShareActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 188
    return-void

    .line 167
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
