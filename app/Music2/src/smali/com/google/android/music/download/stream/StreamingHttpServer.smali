.class public Lcom/google/android/music/download/stream/StreamingHttpServer;
.super Ljava/lang/Object;
.source "StreamingHttpServer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/download/stream/StreamingHttpServer$WorkerThread;,
        Lcom/google/android/music/download/stream/StreamingHttpServer$RequestAcceptorThread;
    }
.end annotation


# static fields
.field private static final LOGV:Z


# instance fields
.field private mAcceptor:Lcom/google/android/music/download/stream/StreamingHttpServer$RequestAcceptorThread;

.field private final mAllowedStreams:Lcom/google/android/music/download/stream/AllowedStreams;

.field protected mParams:Lorg/apache/http/params/HttpParams;

.field protected mServerSocket:Ljava/net/ServerSocket;

.field private mShutdown:Z

.field private mWorkers:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/google/android/music/download/stream/StreamingHttpServer$WorkerThread;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->DOWNLOAD:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/download/stream/StreamingHttpServer;->LOGV:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-boolean v3, p0, Lcom/google/android/music/download/stream/StreamingHttpServer;->mShutdown:Z

    .line 56
    new-instance v0, Lcom/google/android/music/download/stream/AllowedStreams;

    invoke-direct {v0}, Lcom/google/android/music/download/stream/AllowedStreams;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/download/stream/StreamingHttpServer;->mAllowedStreams:Lcom/google/android/music/download/stream/AllowedStreams;

    .line 58
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/download/stream/StreamingHttpServer;->mWorkers:Ljava/util/LinkedList;

    .line 61
    new-instance v0, Lorg/apache/http/params/BasicHttpParams;

    invoke-direct {v0}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/download/stream/StreamingHttpServer;->mParams:Lorg/apache/http/params/HttpParams;

    .line 62
    iget-object v0, p0, Lcom/google/android/music/download/stream/StreamingHttpServer;->mParams:Lorg/apache/http/params/HttpParams;

    const-string v1, "http.connection.stalecheck"

    invoke-interface {v0, v1, v3}, Lorg/apache/http/params/HttpParams;->setBooleanParameter(Ljava/lang/String;Z)Lorg/apache/http/params/HttpParams;

    move-result-object v0

    const-string v1, "http.tcp.nodelay"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lorg/apache/http/params/HttpParams;->setBooleanParameter(Ljava/lang/String;Z)Lorg/apache/http/params/HttpParams;

    move-result-object v0

    const-string v1, "http.socket.timeout"

    const/16 v2, 0x2710

    invoke-interface {v0, v1, v2}, Lorg/apache/http/params/HttpParams;->setIntParameter(Ljava/lang/String;I)Lorg/apache/http/params/HttpParams;

    move-result-object v0

    const-string v1, "http.socket.buffer-size"

    const/16 v2, 0x2000

    invoke-interface {v0, v1, v2}, Lorg/apache/http/params/HttpParams;->setIntParameter(Ljava/lang/String;I)Lorg/apache/http/params/HttpParams;

    .line 67
    invoke-direct {p0, v3}, Lcom/google/android/music/download/stream/StreamingHttpServer;->bind(I)V

    .line 68
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/music/download/stream/StreamingHttpServer;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/download/stream/StreamingHttpServer;

    .prologue
    .line 41
    iget-boolean v0, p0, Lcom/google/android/music/download/stream/StreamingHttpServer;->mShutdown:Z

    return v0
.end method

.method static synthetic access$200(Lcom/google/android/music/download/stream/StreamingHttpServer;)Ljava/util/LinkedList;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/download/stream/StreamingHttpServer;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/music/download/stream/StreamingHttpServer;->mWorkers:Ljava/util/LinkedList;

    return-object v0
.end method

.method static synthetic access$300()Z
    .locals 1

    .prologue
    .line 41
    sget-boolean v0, Lcom/google/android/music/download/stream/StreamingHttpServer;->LOGV:Z

    return v0
.end method

.method static synthetic access$400(Lcom/google/android/music/download/stream/StreamingHttpServer;)Lcom/google/android/music/download/stream/AllowedStreams;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/download/stream/StreamingHttpServer;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/music/download/stream/StreamingHttpServer;->mAllowedStreams:Lcom/google/android/music/download/stream/AllowedStreams;

    return-object v0
.end method

.method private bind(I)V
    .locals 3
    .param p1, "port"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 79
    const-string v1, "localhost"

    const/4 v2, 0x4

    new-array v2, v2, [B

    fill-array-data v2, :array_0

    invoke-static {v1, v2}, Ljava/net/InetAddress;->getByAddress(Ljava/lang/String;[B)Ljava/net/InetAddress;

    move-result-object v0

    .line 82
    .local v0, "loopbackAddress":Ljava/net/InetAddress;
    new-instance v1, Ljava/net/InetSocketAddress;

    invoke-direct {v1, v0, p1}, Ljava/net/InetSocketAddress;-><init>(Ljava/net/InetAddress;I)V

    invoke-direct {p0, v1}, Lcom/google/android/music/download/stream/StreamingHttpServer;->bind(Ljava/net/InetSocketAddress;)V

    .line 83
    return-void

    .line 79
    nop

    :array_0
    .array-data 1
        0x7ft
        0x0t
        0x0t
        0x1t
    .end array-data
.end method

.method private bind(Ljava/net/InetSocketAddress;)V
    .locals 3
    .param p1, "addr"    # Ljava/net/InetSocketAddress;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 86
    new-instance v0, Ljava/net/ServerSocket;

    invoke-direct {v0}, Ljava/net/ServerSocket;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/download/stream/StreamingHttpServer;->mServerSocket:Ljava/net/ServerSocket;

    .line 87
    iget-object v0, p0, Lcom/google/android/music/download/stream/StreamingHttpServer;->mServerSocket:Ljava/net/ServerSocket;

    invoke-virtual {v0, p1}, Ljava/net/ServerSocket;->bind(Ljava/net/SocketAddress;)V

    .line 88
    sget-boolean v0, Lcom/google/android/music/download/stream/StreamingHttpServer;->LOGV:Z

    if-eqz v0, :cond_0

    const-string v0, "StreamingHttpServer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Bound to port: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/download/stream/StreamingHttpServer;->mServerSocket:Ljava/net/ServerSocket;

    invoke-virtual {v2}, Ljava/net/ServerSocket;->getLocalPort()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/download/stream/StreamingHttpServer;->mAcceptor:Lcom/google/android/music/download/stream/StreamingHttpServer$RequestAcceptorThread;

    if-eqz v0, :cond_1

    .line 91
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Should never bind to a socket twice"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 93
    :cond_1
    new-instance v0, Lcom/google/android/music/download/stream/StreamingHttpServer$RequestAcceptorThread;

    invoke-direct {v0, p0}, Lcom/google/android/music/download/stream/StreamingHttpServer$RequestAcceptorThread;-><init>(Lcom/google/android/music/download/stream/StreamingHttpServer;)V

    iput-object v0, p0, Lcom/google/android/music/download/stream/StreamingHttpServer;->mAcceptor:Lcom/google/android/music/download/stream/StreamingHttpServer$RequestAcceptorThread;

    .line 94
    iget-object v0, p0, Lcom/google/android/music/download/stream/StreamingHttpServer;->mAcceptor:Lcom/google/android/music/download/stream/StreamingHttpServer$RequestAcceptorThread;

    invoke-virtual {v0}, Lcom/google/android/music/download/stream/StreamingHttpServer$RequestAcceptorThread;->start()V

    .line 95
    return-void
.end method

.method private generateUri(Lcom/google/android/music/download/stream/StreamingContent;)Ljava/lang/String;
    .locals 4
    .param p1, "content"    # Lcom/google/android/music/download/stream/StreamingContent;

    .prologue
    .line 106
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 107
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string v1, "http://127.0.0.1:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/android/music/download/stream/StreamingHttpServer;->getPort()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/music/download/stream/StreamingContent;->getSecureId()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 111
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private getPort()I
    .locals 2

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/android/music/download/stream/StreamingHttpServer;->mServerSocket:Ljava/net/ServerSocket;

    if-nez v0, :cond_0

    .line 99
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Socket not bound"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 102
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/download/stream/StreamingHttpServer;->mServerSocket:Ljava/net/ServerSocket;

    invoke-virtual {v0}, Ljava/net/ServerSocket;->getLocalPort()I

    move-result v0

    return v0
.end method


# virtual methods
.method public serveStream(Lcom/google/android/music/download/stream/StreamingContent;Lcom/google/android/music/download/stream/StreamingContent;Lcom/google/android/music/download/stream/StreamingContent;)Ljava/lang/String;
    .locals 1
    .param p1, "streamingContent"    # Lcom/google/android/music/download/stream/StreamingContent;
    .param p2, "current"    # Lcom/google/android/music/download/stream/StreamingContent;
    .param p3, "next"    # Lcom/google/android/music/download/stream/StreamingContent;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/music/download/stream/StreamingHttpServer;->mAllowedStreams:Lcom/google/android/music/download/stream/AllowedStreams;

    invoke-virtual {v0, p2, p3}, Lcom/google/android/music/download/stream/AllowedStreams;->setAllowedStreams(Lcom/google/android/music/download/stream/StreamingContent;Lcom/google/android/music/download/stream/StreamingContent;)V

    .line 73
    iget-object v0, p0, Lcom/google/android/music/download/stream/StreamingHttpServer;->mAcceptor:Lcom/google/android/music/download/stream/StreamingHttpServer$RequestAcceptorThread;

    # invokes: Lcom/google/android/music/download/stream/StreamingHttpServer$RequestAcceptorThread;->shutdownOldWorkers()V
    invoke-static {v0}, Lcom/google/android/music/download/stream/StreamingHttpServer$RequestAcceptorThread;->access$000(Lcom/google/android/music/download/stream/StreamingHttpServer$RequestAcceptorThread;)V

    .line 74
    invoke-direct {p0, p1}, Lcom/google/android/music/download/stream/StreamingHttpServer;->generateUri(Lcom/google/android/music/download/stream/StreamingContent;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public shutdown()V
    .locals 4

    .prologue
    .line 115
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/music/download/stream/StreamingHttpServer;->mShutdown:Z

    .line 117
    :try_start_0
    iget-object v2, p0, Lcom/google/android/music/download/stream/StreamingHttpServer;->mServerSocket:Ljava/net/ServerSocket;

    invoke-virtual {v2}, Ljava/net/ServerSocket;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 121
    :goto_0
    iget-object v3, p0, Lcom/google/android/music/download/stream/StreamingHttpServer;->mWorkers:Ljava/util/LinkedList;

    monitor-enter v3

    .line 122
    :try_start_1
    iget-object v2, p0, Lcom/google/android/music/download/stream/StreamingHttpServer;->mWorkers:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/download/stream/StreamingHttpServer$WorkerThread;

    .line 123
    .local v1, "wt":Lcom/google/android/music/download/stream/StreamingHttpServer$WorkerThread;
    invoke-virtual {v1}, Lcom/google/android/music/download/stream/StreamingHttpServer$WorkerThread;->shutdown()V

    goto :goto_1

    .line 125
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "wt":Lcom/google/android/music/download/stream/StreamingHttpServer$WorkerThread;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_2
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 126
    return-void

    .line 118
    .end local v0    # "i$":Ljava/util/Iterator;
    :catch_0
    move-exception v2

    goto :goto_0
.end method
