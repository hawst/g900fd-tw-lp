.class public abstract Lcom/google/android/music/widgets/ScrollableViewGroup;
.super Landroid/view/ViewGroup;
.source "ScrollableViewGroup.java"


# static fields
.field private static final INTERPOLATOR:Landroid/view/animation/Interpolator;


# instance fields
.field private flingVelocity:F

.field private flingable:Z

.field protected isBeingDragged:Z

.field private lastPosition:[F

.field private final limits:[I

.field private maximumVelocity:I

.field private minimumVelocity:I

.field private receivedDown:Z

.field private scrollEnabled:Z

.field protected scroller:Landroid/widget/Scroller;

.field private touchSlop:I

.field private velocityTracker:Landroid/view/VelocityTracker;

.field private vertical:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    new-instance v0, Lcom/google/android/music/widgets/ScrollableViewGroup$1;

    invoke-direct {v0}, Lcom/google/android/music/widgets/ScrollableViewGroup$1;-><init>()V

    sput-object v0, Lcom/google/android/music/widgets/ScrollableViewGroup;->INTERPOLATOR:Landroid/view/animation/Interpolator;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 60
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 32
    new-array v1, v4, [F

    fill-array-data v1, :array_0

    iput-object v1, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->lastPosition:[F

    .line 33
    new-array v1, v4, [I

    fill-array-data v1, :array_1

    iput-object v1, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->limits:[I

    .line 37
    const/4 v1, 0x0

    iput v1, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->flingVelocity:F

    .line 39
    iput-boolean v3, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->vertical:Z

    .line 40
    iput-boolean v3, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->flingable:Z

    .line 41
    iput-boolean v2, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->isBeingDragged:Z

    .line 42
    iput-boolean v3, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->scrollEnabled:Z

    .line 43
    iput-boolean v2, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->receivedDown:Z

    .line 72
    invoke-virtual {p0}, Lcom/google/android/music/widgets/ScrollableViewGroup;->getContext()Landroid/content/Context;

    move-result-object p1

    .line 73
    invoke-virtual {p0, v2}, Lcom/google/android/music/widgets/ScrollableViewGroup;->setFocusable(Z)V

    .line 75
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 76
    .local v0, "configuration":Landroid/view/ViewConfiguration;
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    iput v1, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->touchSlop:I

    .line 77
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v1

    iput v1, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->minimumVelocity:I

    .line 78
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v1

    iput v1, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->maximumVelocity:I

    .line 79
    new-instance v1, Landroid/widget/Scroller;

    sget-object v2, Lcom/google/android/music/widgets/ScrollableViewGroup;->INTERPOLATOR:Landroid/view/animation/Interpolator;

    invoke-direct {v1, p1, v2}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V

    iput-object v1, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->scroller:Landroid/widget/Scroller;

    .line 61
    return-void

    .line 32
    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data

    .line 33
    :array_1
    .array-data 4
        -0x7fffffff
        0x7fffffff
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 64
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 32
    new-array v1, v4, [F

    fill-array-data v1, :array_0

    iput-object v1, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->lastPosition:[F

    .line 33
    new-array v1, v4, [I

    fill-array-data v1, :array_1

    iput-object v1, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->limits:[I

    .line 37
    const/4 v1, 0x0

    iput v1, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->flingVelocity:F

    .line 39
    iput-boolean v3, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->vertical:Z

    .line 40
    iput-boolean v3, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->flingable:Z

    .line 41
    iput-boolean v2, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->isBeingDragged:Z

    .line 42
    iput-boolean v3, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->scrollEnabled:Z

    .line 43
    iput-boolean v2, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->receivedDown:Z

    .line 72
    invoke-virtual {p0}, Lcom/google/android/music/widgets/ScrollableViewGroup;->getContext()Landroid/content/Context;

    move-result-object p1

    .line 73
    invoke-virtual {p0, v2}, Lcom/google/android/music/widgets/ScrollableViewGroup;->setFocusable(Z)V

    .line 75
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 76
    .local v0, "configuration":Landroid/view/ViewConfiguration;
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    iput v1, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->touchSlop:I

    .line 77
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v1

    iput v1, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->minimumVelocity:I

    .line 78
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v1

    iput v1, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->maximumVelocity:I

    .line 79
    new-instance v1, Landroid/widget/Scroller;

    sget-object v2, Lcom/google/android/music/widgets/ScrollableViewGroup;->INTERPOLATOR:Landroid/view/animation/Interpolator;

    invoke-direct {v1, p1, v2}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V

    iput-object v1, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->scroller:Landroid/widget/Scroller;

    .line 65
    return-void

    .line 32
    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data

    .line 33
    :array_1
    .array-data 4
        -0x7fffffff
        0x7fffffff
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defState"    # I

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 68
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 32
    new-array v1, v4, [F

    fill-array-data v1, :array_0

    iput-object v1, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->lastPosition:[F

    .line 33
    new-array v1, v4, [I

    fill-array-data v1, :array_1

    iput-object v1, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->limits:[I

    .line 37
    const/4 v1, 0x0

    iput v1, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->flingVelocity:F

    .line 39
    iput-boolean v3, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->vertical:Z

    .line 40
    iput-boolean v3, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->flingable:Z

    .line 41
    iput-boolean v2, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->isBeingDragged:Z

    .line 42
    iput-boolean v3, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->scrollEnabled:Z

    .line 43
    iput-boolean v2, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->receivedDown:Z

    .line 72
    invoke-virtual {p0}, Lcom/google/android/music/widgets/ScrollableViewGroup;->getContext()Landroid/content/Context;

    move-result-object p1

    .line 73
    invoke-virtual {p0, v2}, Lcom/google/android/music/widgets/ScrollableViewGroup;->setFocusable(Z)V

    .line 75
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 76
    .local v0, "configuration":Landroid/view/ViewConfiguration;
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    iput v1, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->touchSlop:I

    .line 77
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v1

    iput v1, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->minimumVelocity:I

    .line 78
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v1

    iput v1, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->maximumVelocity:I

    .line 79
    new-instance v1, Landroid/widget/Scroller;

    sget-object v2, Lcom/google/android/music/widgets/ScrollableViewGroup;->INTERPOLATOR:Landroid/view/animation/Interpolator;

    invoke-direct {v1, p1, v2}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V

    iput-object v1, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->scroller:Landroid/widget/Scroller;

    .line 69
    return-void

    .line 32
    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data

    .line 33
    :array_1
    .array-data 4
        -0x7fffffff
        0x7fffffff
    .end array-data
.end method

.method private clampToScrollLimits(I)I
    .locals 3
    .param p1, "value"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 321
    iget-object v0, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->limits:[I

    aget v0, v0, v1

    if-ge p1, v0, :cond_1

    .line 322
    iget-object v0, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->limits:[I

    aget p1, v0, v1

    .line 327
    .end local p1    # "value":I
    :cond_0
    :goto_0
    return p1

    .line 324
    .restart local p1    # "value":I
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->limits:[I

    aget v0, v0, v2

    if-le p1, v0, :cond_0

    .line 325
    iget-object v0, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->limits:[I

    aget p1, v0, v2

    goto :goto_0
.end method

.method private fling(F)V
    .locals 13
    .param p1, "velocity"    # F

    .prologue
    const/4 v6, 0x1

    const/4 v3, 0x0

    .line 309
    iput p1, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->flingVelocity:F

    .line 310
    invoke-virtual {p0}, Lcom/google/android/music/widgets/ScrollableViewGroup;->getScrollX()I

    move-result v1

    .line 311
    .local v1, "x":I
    invoke-virtual {p0}, Lcom/google/android/music/widgets/ScrollableViewGroup;->getScrollY()I

    move-result v2

    .line 312
    .local v2, "y":I
    iget-boolean v0, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->vertical:Z

    if-eqz v0, :cond_0

    .line 313
    iget-object v0, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->scroller:Landroid/widget/Scroller;

    float-to-int v4, p1

    iget-object v5, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->limits:[I

    aget v7, v5, v3

    iget-object v5, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->limits:[I

    aget v8, v5, v6

    move v5, v3

    move v6, v3

    invoke-virtual/range {v0 .. v8}, Landroid/widget/Scroller;->fling(IIIIIIII)V

    .line 317
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/music/widgets/ScrollableViewGroup;->invalidate()V

    .line 318
    return-void

    .line 315
    :cond_0
    iget-object v4, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->scroller:Landroid/widget/Scroller;

    float-to-int v7, p1

    iget-object v0, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->limits:[I

    aget v9, v0, v3

    iget-object v0, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->limits:[I

    aget v10, v0, v6

    move v5, v1

    move v6, v2

    move v8, v3

    move v11, v3

    move v12, v3

    invoke-virtual/range {v4 .. v12}, Landroid/widget/Scroller;->fling(IIIIIIII)V

    goto :goto_0
.end method

.method private getCurrentVelocity()F
    .locals 3

    .prologue
    .line 301
    iget-object v0, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->velocityTracker:Landroid/view/VelocityTracker;

    const/16 v1, 0x3e8

    iget v2, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->maximumVelocity:I

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    .line 302
    iget-boolean v0, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->vertical:Z

    if-eqz v0, :cond_0

    .line 303
    iget-object v0, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->velocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->getYVelocity()F

    move-result v0

    .line 305
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->velocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->getXVelocity()F

    move-result v0

    goto :goto_0
.end method

.method private motionShouldStartDrag(Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 230
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    iget-object v7, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->lastPosition:[F

    aget v7, v7, v4

    sub-float v0, v6, v7

    .line 231
    .local v0, "deltaX":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    iget-object v7, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->lastPosition:[F

    aget v7, v7, v5

    sub-float v1, v6, v7

    .line 232
    .local v1, "deltaY":F
    iget v6, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->touchSlop:I

    int-to-float v6, v6

    cmpl-float v6, v0, v6

    if-gtz v6, :cond_0

    iget v6, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->touchSlop:I

    neg-int v6, v6

    int-to-float v6, v6

    cmpg-float v6, v0, v6

    if-gez v6, :cond_3

    :cond_0
    move v2, v5

    .line 233
    .local v2, "draggedX":Z
    :goto_0
    iget v6, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->touchSlop:I

    int-to-float v6, v6

    cmpl-float v6, v1, v6

    if-gtz v6, :cond_1

    iget v6, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->touchSlop:I

    neg-int v6, v6

    int-to-float v6, v6

    cmpg-float v6, v1, v6

    if-gez v6, :cond_4

    :cond_1
    move v3, v5

    .line 234
    .local v3, "draggedY":Z
    :goto_1
    iget-boolean v6, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->vertical:Z

    if-eqz v6, :cond_6

    .line 235
    if-eqz v3, :cond_5

    if-nez v2, :cond_5

    .line 237
    :cond_2
    :goto_2
    return v5

    .end local v2    # "draggedX":Z
    .end local v3    # "draggedY":Z
    :cond_3
    move v2, v4

    .line 232
    goto :goto_0

    .restart local v2    # "draggedX":Z
    :cond_4
    move v3, v4

    .line 233
    goto :goto_1

    .restart local v3    # "draggedY":Z
    :cond_5
    move v5, v4

    .line 235
    goto :goto_2

    .line 237
    :cond_6
    if-eqz v2, :cond_7

    if-eqz v3, :cond_2

    :cond_7
    move v5, v4

    goto :goto_2
.end method

.method private shouldStartDrag(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 107
    iget-boolean v2, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->scrollEnabled:Z

    if-nez v2, :cond_1

    .line 140
    :cond_0
    :goto_0
    return v0

    .line 111
    :cond_1
    iget-boolean v2, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->isBeingDragged:Z

    if-eqz v2, :cond_2

    .line 112
    iput-boolean v0, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->isBeingDragged:Z

    goto :goto_0

    .line 116
    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 120
    :pswitch_1
    invoke-virtual {p0, p1}, Lcom/google/android/music/widgets/ScrollableViewGroup;->updatePosition(Landroid/view/MotionEvent;)V

    .line 121
    iget-object v2, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->scroller:Landroid/widget/Scroller;

    invoke-virtual {v2}, Landroid/widget/Scroller;->isFinished()Z

    move-result v2

    if-nez v2, :cond_3

    .line 122
    invoke-direct {p0}, Lcom/google/android/music/widgets/ScrollableViewGroup;->startDrag()V

    move v0, v1

    .line 123
    goto :goto_0

    .line 125
    :cond_3
    iput-boolean v1, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->receivedDown:Z

    goto :goto_0

    .line 132
    :pswitch_2
    invoke-direct {p0, p1}, Lcom/google/android/music/widgets/ScrollableViewGroup;->motionShouldStartDrag(Landroid/view/MotionEvent;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 133
    invoke-virtual {p0, p1}, Lcom/google/android/music/widgets/ScrollableViewGroup;->updatePosition(Landroid/view/MotionEvent;)V

    .line 134
    invoke-direct {p0}, Lcom/google/android/music/widgets/ScrollableViewGroup;->startDrag()V

    move v0, v1

    .line 135
    goto :goto_0

    .line 116
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private startDrag()V
    .locals 1

    .prologue
    .line 202
    iget-boolean v0, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->isBeingDragged:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->scroller:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 203
    invoke-virtual {p0}, Lcom/google/android/music/widgets/ScrollableViewGroup;->dragStarted()V

    .line 205
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->isBeingDragged:Z

    .line 206
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->flingVelocity:F

    .line 207
    iget-object v0, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->scroller:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->abortAnimation()V

    .line 208
    return-void
.end method

.method private stopDrag(Z)V
    .locals 3
    .param p1, "cancelled"    # Z

    .prologue
    const/4 v2, 0x0

    .line 211
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->isBeingDragged:Z

    .line 212
    if-nez p1, :cond_3

    iget-boolean v1, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->flingable:Z

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lcom/google/android/music/widgets/ScrollableViewGroup;->getChildCount()I

    move-result v1

    if-lez v1, :cond_3

    .line 213
    invoke-direct {p0}, Lcom/google/android/music/widgets/ScrollableViewGroup;->getCurrentVelocity()F

    move-result v0

    .line 214
    .local v0, "velocity":F
    iget v1, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->minimumVelocity:I

    int-to-float v1, v1

    cmpl-float v1, v0, v1

    if-gtz v1, :cond_0

    iget v1, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->minimumVelocity:I

    neg-int v1, v1

    int-to-float v1, v1

    cmpg-float v1, v0, v1

    if-gez v1, :cond_2

    .line 215
    :cond_0
    neg-float v1, v0

    invoke-direct {p0, v1}, Lcom/google/android/music/widgets/ScrollableViewGroup;->fling(F)V

    .line 223
    .end local v0    # "velocity":F
    :goto_0
    iget-boolean v1, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->flingable:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->velocityTracker:Landroid/view/VelocityTracker;

    if-eqz v1, :cond_1

    .line 224
    iget-object v1, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->velocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v1}, Landroid/view/VelocityTracker;->recycle()V

    .line 225
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->velocityTracker:Landroid/view/VelocityTracker;

    .line 227
    :cond_1
    return-void

    .line 217
    .restart local v0    # "velocity":F
    :cond_2
    invoke-virtual {p0, v2}, Lcom/google/android/music/widgets/ScrollableViewGroup;->onMoveFinished(F)V

    goto :goto_0

    .line 220
    .end local v0    # "velocity":F
    :cond_3
    invoke-virtual {p0, v2}, Lcom/google/android/music/widgets/ScrollableViewGroup;->onMoveFinished(F)V

    goto :goto_0
.end method

.method private updatePositionAndComputeDelta(Landroid/view/MotionEvent;)F
    .locals 3
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 241
    iget-boolean v2, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->vertical:Z

    if-eqz v2, :cond_0

    const/4 v0, 0x1

    .line 242
    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->lastPosition:[F

    aget v1, v2, v0

    .line 243
    .local v1, "position":F
    invoke-virtual {p0, p1}, Lcom/google/android/music/widgets/ScrollableViewGroup;->updatePosition(Landroid/view/MotionEvent;)V

    .line 244
    iget-object v2, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->lastPosition:[F

    aget v2, v2, v0

    sub-float v2, v1, v2

    return v2

    .line 241
    .end local v0    # "i":I
    .end local v1    # "position":F
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public computeScroll()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 274
    iget-object v2, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->scroller:Landroid/widget/Scroller;

    invoke-virtual {v2}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 276
    iget-boolean v2, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->vertical:Z

    if-eqz v2, :cond_2

    .line 277
    iget-object v2, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->scroller:Landroid/widget/Scroller;

    invoke-virtual {v2}, Landroid/widget/Scroller;->getCurrY()I

    move-result v0

    .line 278
    .local v0, "current":I
    invoke-virtual {p0, v4, v0}, Lcom/google/android/music/widgets/ScrollableViewGroup;->scrollTo(II)V

    .line 283
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/music/widgets/ScrollableViewGroup;->invalidate()V

    .line 285
    iget v2, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->flingVelocity:F

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_0

    .line 286
    iget v2, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->flingVelocity:F

    invoke-virtual {p0, v2}, Lcom/google/android/music/widgets/ScrollableViewGroup;->onMoveFinished(F)V

    .line 287
    iput v3, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->flingVelocity:F

    .line 290
    :cond_0
    iget-boolean v2, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->vertical:Z

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->scroller:Landroid/widget/Scroller;

    invoke-virtual {v2}, Landroid/widget/Scroller;->getFinalY()I

    move-result v1

    .line 291
    .local v1, "target":I
    :goto_1
    if-ne v0, v1, :cond_1

    .line 292
    iget-object v2, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->scroller:Landroid/widget/Scroller;

    invoke-virtual {v2}, Landroid/widget/Scroller;->abortAnimation()V

    .line 293
    iget-boolean v2, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->isBeingDragged:Z

    if-nez v2, :cond_1

    .line 294
    invoke-virtual {p0}, Lcom/google/android/music/widgets/ScrollableViewGroup;->dragEnded()V

    .line 298
    .end local v0    # "current":I
    .end local v1    # "target":I
    :cond_1
    return-void

    .line 280
    :cond_2
    iget-object v2, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->scroller:Landroid/widget/Scroller;

    invoke-virtual {v2}, Landroid/widget/Scroller;->getCurrX()I

    move-result v0

    .line 281
    .restart local v0    # "current":I
    invoke-virtual {p0, v0, v4}, Lcom/google/android/music/widgets/ScrollableViewGroup;->scrollTo(II)V

    goto :goto_0

    .line 290
    :cond_3
    iget-object v2, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->scroller:Landroid/widget/Scroller;

    invoke-virtual {v2}, Landroid/widget/Scroller;->getFinalX()I

    move-result v1

    goto :goto_1
.end method

.method dragEnded()V
    .locals 0

    .prologue
    .line 351
    return-void
.end method

.method dragStarted()V
    .locals 0

    .prologue
    .line 348
    return-void
.end method

.method public getScroll()I
    .locals 1

    .prologue
    .line 248
    iget-boolean v0, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->vertical:Z

    if-eqz v0, :cond_0

    .line 249
    invoke-virtual {p0}, Lcom/google/android/music/widgets/ScrollableViewGroup;->getScrollY()I

    move-result v0

    .line 251
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/music/widgets/ScrollableViewGroup;->getScrollX()I

    move-result v0

    goto :goto_0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 103
    invoke-direct {p0, p1}, Lcom/google/android/music/widgets/ScrollableViewGroup;->shouldStartDrag(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method protected onMoveFinished(F)V
    .locals 0
    .param p1, "velocity"    # F

    .prologue
    .line 262
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 148
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 150
    .local v0, "action":I
    iget-boolean v2, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->flingable:Z

    if-eqz v2, :cond_1

    .line 151
    iget-object v2, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->velocityTracker:Landroid/view/VelocityTracker;

    if-nez v2, :cond_0

    .line 152
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->velocityTracker:Landroid/view/VelocityTracker;

    .line 154
    :cond_0
    iget-object v2, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->velocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v2, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 157
    :cond_1
    iget-boolean v2, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->isBeingDragged:Z

    if-nez v2, :cond_4

    .line 158
    invoke-direct {p0, p1}, Lcom/google/android/music/widgets/ScrollableViewGroup;->shouldStartDrag(Landroid/view/MotionEvent;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 183
    :cond_2
    :goto_0
    return v3

    .line 162
    :cond_3
    if-ne v0, v3, :cond_2

    iget-boolean v2, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->receivedDown:Z

    if-eqz v2, :cond_2

    .line 163
    iput-boolean v4, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->receivedDown:Z

    .line 164
    invoke-virtual {p0}, Lcom/google/android/music/widgets/ScrollableViewGroup;->performClick()Z

    move-result v3

    goto :goto_0

    .line 169
    :cond_4
    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 178
    :pswitch_0
    const/4 v2, 0x3

    if-ne v0, v2, :cond_5

    move v2, v3

    :goto_1
    invoke-direct {p0, v2}, Lcom/google/android/music/widgets/ScrollableViewGroup;->stopDrag(Z)V

    .line 179
    iput-boolean v4, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->receivedDown:Z

    goto :goto_0

    .line 171
    :pswitch_1
    invoke-direct {p0, p1}, Lcom/google/android/music/widgets/ScrollableViewGroup;->updatePositionAndComputeDelta(Landroid/view/MotionEvent;)F

    move-result v1

    .line 172
    .local v1, "delta":F
    invoke-virtual {p0}, Lcom/google/android/music/widgets/ScrollableViewGroup;->getScroll()I

    move-result v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v5

    add-int/2addr v2, v5

    invoke-virtual {p0, v2}, Lcom/google/android/music/widgets/ScrollableViewGroup;->scrollTo(I)V

    .line 173
    iput-boolean v4, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->receivedDown:Z

    goto :goto_0

    .end local v1    # "delta":F
    :cond_5
    move v2, v4

    .line 178
    goto :goto_1

    .line 169
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected scrollTo(I)V
    .locals 2
    .param p1, "position"    # I

    .prologue
    const/4 v1, 0x0

    .line 255
    iget-boolean v0, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->vertical:Z

    if-eqz v0, :cond_0

    .line 256
    invoke-direct {p0, p1}, Lcom/google/android/music/widgets/ScrollableViewGroup;->clampToScrollLimits(I)I

    move-result v0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/music/widgets/ScrollableViewGroup;->scrollTo(II)V

    .line 260
    :goto_0
    return-void

    .line 258
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/music/widgets/ScrollableViewGroup;->clampToScrollLimits(I)I

    move-result v0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/music/widgets/ScrollableViewGroup;->scrollTo(II)V

    goto :goto_0
.end method

.method public setFlingable(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 91
    iput-boolean p1, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->flingable:Z

    .line 92
    return-void
.end method

.method public setScrollEnabled(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 95
    iput-boolean p1, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->scrollEnabled:Z

    .line 96
    return-void
.end method

.method public setScrollLimits(II)V
    .locals 2
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    .line 265
    iget-object v0, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->limits:[I

    const/4 v1, 0x0

    aput p1, v0, v1

    .line 266
    iget-object v0, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->limits:[I

    const/4 v1, 0x1

    aput p2, v0, v1

    .line 267
    return-void
.end method

.method public setVertical(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 83
    iput-boolean p1, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->vertical:Z

    .line 84
    return-void
.end method

.method public showContextMenuForChild(Landroid/view/View;)Z
    .locals 1
    .param p1, "originalView"    # Landroid/view/View;

    .prologue
    .line 192
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/music/widgets/ScrollableViewGroup;->requestDisallowInterceptTouchEvent(Z)V

    .line 193
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->showContextMenuForChild(Landroid/view/View;)Z

    move-result v0

    return v0
.end method

.method public smoothScrollTo(I)V
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 331
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/google/android/music/widgets/ScrollableViewGroup;->smoothScrollTo(IZ)V

    .line 332
    return-void
.end method

.method public smoothScrollTo(IZ)V
    .locals 12
    .param p1, "position"    # I
    .param p2, "clamp"    # Z

    .prologue
    const/16 v5, 0x1f4

    const/4 v1, 0x0

    .line 335
    if-eqz p2, :cond_0

    .line 336
    invoke-direct {p0, p1}, Lcom/google/android/music/widgets/ScrollableViewGroup;->clampToScrollLimits(I)I

    move-result p1

    .line 338
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/music/widgets/ScrollableViewGroup;->getScroll()I

    move-result v0

    sub-int v4, p1, v0

    .line 339
    .local v4, "delta":I
    iget-boolean v0, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->vertical:Z

    if-eqz v0, :cond_1

    .line 340
    iget-object v0, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->scroller:Landroid/widget/Scroller;

    invoke-virtual {p0}, Lcom/google/android/music/widgets/ScrollableViewGroup;->getScrollY()I

    move-result v2

    move v3, v1

    invoke-virtual/range {v0 .. v5}, Landroid/widget/Scroller;->startScroll(IIIII)V

    .line 344
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/music/widgets/ScrollableViewGroup;->invalidate()V

    .line 345
    return-void

    .line 342
    :cond_1
    iget-object v6, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->scroller:Landroid/widget/Scroller;

    invoke-virtual {p0}, Lcom/google/android/music/widgets/ScrollableViewGroup;->getScrollX()I

    move-result v7

    move v8, v1

    move v9, v4

    move v10, v1

    move v11, v5

    invoke-virtual/range {v6 .. v11}, Landroid/widget/Scroller;->startScroll(IIIII)V

    goto :goto_0
.end method

.method protected updatePosition(Landroid/view/MotionEvent;)V
    .locals 3
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 197
    iget-object v0, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->lastPosition:[F

    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    aput v2, v0, v1

    .line 198
    iget-object v0, p0, Lcom/google/android/music/widgets/ScrollableViewGroup;->lastPosition:[F

    const/4 v1, 0x1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    aput v2, v0, v1

    .line 199
    return-void
.end method
