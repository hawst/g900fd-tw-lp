.class final Lcom/google/android/music/medialist/ArtistAlbumList$1;
.super Ljava/lang/Object;
.source "ArtistAlbumList.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/medialist/ArtistAlbumList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/android/music/medialist/ArtistAlbumList;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/google/android/music/medialist/ArtistAlbumList;
    .locals 5
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v1, 0x1

    .line 75
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    .line 76
    .local v2, "id":J
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 77
    .local v0, "artistName":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-ne v4, v1, :cond_0

    .line 78
    .local v1, "filter":Z
    :goto_0
    new-instance v4, Lcom/google/android/music/medialist/ArtistAlbumList;

    invoke-direct {v4, v2, v3, v0, v1}, Lcom/google/android/music/medialist/ArtistAlbumList;-><init>(JLjava/lang/String;Z)V

    return-object v4

    .line 77
    .end local v1    # "filter":Z
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Landroid/os/Parcel;

    .prologue
    .line 73
    invoke-virtual {p0, p1}, Lcom/google/android/music/medialist/ArtistAlbumList$1;->createFromParcel(Landroid/os/Parcel;)Lcom/google/android/music/medialist/ArtistAlbumList;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/google/android/music/medialist/ArtistAlbumList;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 82
    new-array v0, p1, [Lcom/google/android/music/medialist/ArtistAlbumList;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 73
    invoke-virtual {p0, p1}, Lcom/google/android/music/medialist/ArtistAlbumList$1;->newArray(I)[Lcom/google/android/music/medialist/ArtistAlbumList;

    move-result-object v0

    return-object v0
.end method
