.class public final Lcom/google/android/music/leanback/bitmap/BitmapFactory;
.super Ljava/lang/Object;
.source "BitmapFactory.java"


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static sImageCardHeightPixels:I

.field private static sImageCardWideHeightPixels:I

.field private static sImageCardWideWidthPixels:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 29
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->LEANBACK:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-virtual {v0}, Lcom/google/android/music/utils/DebugUtils$MusicTag;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/leanback/bitmap/BitmapFactory;->TAG:Ljava/lang/String;

    .line 30
    sput v1, Lcom/google/android/music/leanback/bitmap/BitmapFactory;->sImageCardHeightPixels:I

    .line 31
    sput v1, Lcom/google/android/music/leanback/bitmap/BitmapFactory;->sImageCardWideHeightPixels:I

    .line 32
    sput v1, Lcom/google/android/music/leanback/bitmap/BitmapFactory;->sImageCardWideWidthPixels:I

    return-void
.end method

.method public static darkenBitmap(Landroid/content/Context;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "source"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v6, 0x0

    .line 204
    const-string v3, "Darkening bitmap on Main Thread!"

    invoke-static {p0, v3}, Lcom/google/android/music/utils/MusicUtils;->checkMainThread(Landroid/content/Context;Ljava/lang/String;)V

    .line 205
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    .line 206
    .local v2, "paint":Landroid/graphics/Paint;
    new-instance v1, Landroid/graphics/LightingColorFilter;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c00bd

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0c00be

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-direct {v1, v3, v4}, Landroid/graphics/LightingColorFilter;-><init>(II)V

    .line 209
    .local v1, "filter":Landroid/graphics/ColorFilter;
    invoke-virtual {v2, v1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 210
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, p1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 211
    .local v0, "canvas":Landroid/graphics/Canvas;
    invoke-virtual {v0, p1, v6, v6, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 212
    return-object p1
.end method

.method static getBitmap(Landroid/content/Context;[Lcom/google/android/music/leanback/bitmap/BitmapGetter;IIZ)Landroid/util/Pair;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "bitmapGetters"    # [Lcom/google/android/music/leanback/bitmap/BitmapGetter;
    .param p2, "targetWidth"    # I
    .param p3, "targetHeight"    # I
    .param p4, "cropToSquare"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "[",
            "Lcom/google/android/music/leanback/bitmap/BitmapGetter;",
            "IIZ)",
            "Landroid/util/Pair",
            "<",
            "Landroid/graphics/Bitmap;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 132
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/leanback/bitmap/BitmapFactory;->getBitmap(Landroid/content/Context;[Lcom/google/android/music/leanback/bitmap/BitmapGetter;IIZZ)Landroid/util/Pair;

    move-result-object v0

    return-object v0
.end method

.method static getBitmap(Landroid/content/Context;[Lcom/google/android/music/leanback/bitmap/BitmapGetter;IIZZ)Landroid/util/Pair;
    .locals 15
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "bitmapGetters"    # [Lcom/google/android/music/leanback/bitmap/BitmapGetter;
    .param p2, "targetWidth"    # I
    .param p3, "targetHeight"    # I
    .param p4, "cropToSquare"    # Z
    .param p5, "withFiltering"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "[",
            "Lcom/google/android/music/leanback/bitmap/BitmapGetter;",
            "IIZZ)",
            "Landroid/util/Pair",
            "<",
            "Landroid/graphics/Bitmap;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 138
    if-nez p1, :cond_0

    .line 139
    new-instance v11, Landroid/util/Pair;

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-static {v13}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v13

    invoke-direct {v11, v12, v13}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 176
    :goto_0
    return-object v11

    .line 141
    :cond_0
    const/4 v3, 0x1

    .line 142
    .local v3, "allBitmapsReady":Z
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 143
    .local v6, "bitmaps":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/Bitmap;>;"
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_1
    move-object/from16 v0, p1

    array-length v11, v0

    if-ge v10, v11, :cond_4

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v11

    const/4 v12, 0x4

    if-ge v11, v12, :cond_4

    .line 144
    aget-object v11, p1, v10

    if-nez v11, :cond_2

    .line 143
    :cond_1
    :goto_2
    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    .line 147
    :cond_2
    aget-object v11, p1, v10

    move/from16 v0, p2

    move/from16 v1, p3

    move/from16 v2, p4

    invoke-interface {v11, p0, v0, v1, v2}, Lcom/google/android/music/leanback/bitmap/BitmapGetter;->getBitmap(Landroid/content/Context;IIZ)Landroid/util/Pair;

    move-result-object v5

    .line 149
    .local v5, "bitmapPair":Landroid/util/Pair;, "Landroid/util/Pair<Landroid/graphics/Bitmap;Ljava/lang/Boolean;>;"
    iget-object v11, v5, Landroid/util/Pair;->first:Ljava/lang/Object;

    if-nez v11, :cond_3

    iget-object v11, v5, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v11, Ljava/lang/Boolean;

    invoke-virtual {v11}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v11

    if-eqz v11, :cond_1

    .line 150
    :cond_3
    iget-object v11, v5, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-virtual {v6, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 151
    iget-object v11, v5, Landroid/util/Pair;->first:Ljava/lang/Object;

    if-nez v11, :cond_1

    .line 152
    const/4 v3, 0x0

    goto :goto_2

    .line 156
    .end local v5    # "bitmapPair":Landroid/util/Pair;, "Landroid/util/Pair<Landroid/graphics/Bitmap;Ljava/lang/Boolean;>;"
    :cond_4
    if-nez v3, :cond_5

    .line 157
    new-instance v11, Landroid/util/Pair;

    const/4 v12, 0x0

    const/4 v13, 0x1

    invoke-static {v13}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v13

    invoke-direct {v11, v12, v13}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 159
    :cond_5
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v11

    const/4 v12, 0x4

    if-lt v11, v12, :cond_6

    .line 160
    div-int/lit8 v9, p2, 0x2

    .line 161
    .local v9, "halfWidth":I
    div-int/lit8 v8, p3, 0x2

    .line 162
    .local v8, "halfHeight":I
    sget-object v11, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    move/from16 v0, p2

    move/from16 v1, p3

    invoke-static {v0, v1, v11}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 163
    .local v4, "bitmap":Landroid/graphics/Bitmap;
    new-instance v7, Landroid/graphics/Canvas;

    invoke-direct {v7, v4}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 164
    .local v7, "canvas":Landroid/graphics/Canvas;
    const/4 v11, 0x0

    invoke-virtual {v6, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/graphics/Bitmap;

    move/from16 v0, p5

    invoke-static {p0, v9, v8, v11, v0}, Lcom/google/android/music/leanback/bitmap/BitmapFactory;->resizeBitmap(Landroid/content/Context;IILandroid/graphics/Bitmap;Z)Landroid/graphics/Bitmap;

    move-result-object v11

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-virtual {v7, v11, v12, v13, v14}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 166
    const/4 v11, 0x1

    invoke-virtual {v6, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/graphics/Bitmap;

    move/from16 v0, p5

    invoke-static {p0, v9, v8, v11, v0}, Lcom/google/android/music/leanback/bitmap/BitmapFactory;->resizeBitmap(Landroid/content/Context;IILandroid/graphics/Bitmap;Z)Landroid/graphics/Bitmap;

    move-result-object v11

    int-to-float v12, v9

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-virtual {v7, v11, v12, v13, v14}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 168
    const/4 v11, 0x2

    invoke-virtual {v6, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/graphics/Bitmap;

    move/from16 v0, p5

    invoke-static {p0, v9, v8, v11, v0}, Lcom/google/android/music/leanback/bitmap/BitmapFactory;->resizeBitmap(Landroid/content/Context;IILandroid/graphics/Bitmap;Z)Landroid/graphics/Bitmap;

    move-result-object v11

    const/4 v12, 0x0

    int-to-float v13, v8

    const/4 v14, 0x0

    invoke-virtual {v7, v11, v12, v13, v14}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 170
    const/4 v11, 0x3

    invoke-virtual {v6, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/graphics/Bitmap;

    move/from16 v0, p5

    invoke-static {p0, v9, v8, v11, v0}, Lcom/google/android/music/leanback/bitmap/BitmapFactory;->resizeBitmap(Landroid/content/Context;IILandroid/graphics/Bitmap;Z)Landroid/graphics/Bitmap;

    move-result-object v11

    int-to-float v12, v9

    int-to-float v13, v8

    const/4 v14, 0x0

    invoke-virtual {v7, v11, v12, v13, v14}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 172
    new-instance v11, Landroid/util/Pair;

    const/4 v12, 0x1

    invoke-static {v12}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v12

    invoke-direct {v11, v4, v12}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 173
    .end local v4    # "bitmap":Landroid/graphics/Bitmap;
    .end local v7    # "canvas":Landroid/graphics/Canvas;
    .end local v8    # "halfHeight":I
    .end local v9    # "halfWidth":I
    :cond_6
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v11

    if-lez v11, :cond_7

    .line 174
    new-instance v11, Landroid/util/Pair;

    const/4 v12, 0x0

    invoke-virtual {v6, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    const/4 v13, 0x1

    invoke-static {v13}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v13

    invoke-direct {v11, v12, v13}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 176
    :cond_7
    new-instance v11, Landroid/util/Pair;

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-static {v13}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v13

    invoke-direct {v11, v12, v13}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_0
.end method

.method public static getBitmap(Landroid/content/Context;Lcom/google/android/music/leanback/bitmap/BitmapGettersGetter;IIZLcom/google/android/music/leanback/bitmap/BitmapListener;)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "bitmapGettersGetter"    # Lcom/google/android/music/leanback/bitmap/BitmapGettersGetter;
    .param p2, "targetWidth"    # I
    .param p3, "targetHeight"    # I
    .param p4, "cropToSquare"    # Z
    .param p5, "bitmapListener"    # Lcom/google/android/music/leanback/bitmap/BitmapListener;

    .prologue
    .line 76
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v6, p5

    invoke-static/range {v0 .. v6}, Lcom/google/android/music/leanback/bitmap/BitmapFactory;->getBitmap(Landroid/content/Context;Lcom/google/android/music/leanback/bitmap/BitmapGettersGetter;IIZZLcom/google/android/music/leanback/bitmap/BitmapListener;)V

    .line 78
    return-void
.end method

.method public static getBitmap(Landroid/content/Context;Lcom/google/android/music/leanback/bitmap/BitmapGettersGetter;IIZZLcom/google/android/music/leanback/bitmap/BitmapListener;)V
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "bitmapGettersGetter"    # Lcom/google/android/music/leanback/bitmap/BitmapGettersGetter;
    .param p2, "targetWidth"    # I
    .param p3, "targetHeight"    # I
    .param p4, "cropToSquare"    # Z
    .param p5, "withFiltering"    # Z
    .param p6, "bitmapListener"    # Lcom/google/android/music/leanback/bitmap/BitmapListener;

    .prologue
    .line 84
    if-nez p1, :cond_0

    .line 85
    const/4 v1, 0x0

    invoke-interface {p6, v1}, Lcom/google/android/music/leanback/bitmap/BitmapListener;->onBitmapLoaded(Landroid/graphics/Bitmap;)V

    .line 115
    :goto_0
    return-void

    .line 88
    :cond_0
    new-instance v8, Lcom/google/android/music/leanback/bitmap/RetryBitmapListener;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v8, v1, p6}, Lcom/google/android/music/leanback/bitmap/RetryBitmapListener;-><init>(Landroid/os/Handler;Lcom/google/android/music/leanback/bitmap/BitmapListener;)V

    .line 91
    .local v8, "retryBitmapListener":Lcom/google/android/music/leanback/bitmap/RetryBitmapListener;
    new-instance v0, Lcom/google/android/music/leanback/bitmap/BitmapFactory$1;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v8}, Lcom/google/android/music/leanback/bitmap/BitmapFactory$1;-><init>(Landroid/content/Context;Lcom/google/android/music/leanback/bitmap/BitmapGettersGetter;IIZZLcom/google/android/music/leanback/bitmap/BitmapListener;Lcom/google/android/music/leanback/bitmap/RetryBitmapListener;)V

    .line 113
    .local v0, "asyncRunner":Lcom/google/android/music/utils/async/AsyncRunner;
    invoke-virtual {v8, v0}, Lcom/google/android/music/leanback/bitmap/RetryBitmapListener;->setAsyncRunner(Lcom/google/android/music/utils/async/AsyncRunner;)V

    .line 114
    invoke-static {v0}, Lcom/google/android/music/utils/MusicUtils;->runAsyncWithCallback(Lcom/google/android/music/utils/async/AsyncRunner;)V

    goto :goto_0
.end method

.method public static getBitmap(Landroid/content/Context;Lcom/google/android/music/leanback/bitmap/BitmapGettersGetter;ZZLcom/google/android/music/leanback/bitmap/BitmapListener;)V
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "bitmapGettersGetter"    # Lcom/google/android/music/leanback/bitmap/BitmapGettersGetter;
    .param p2, "wide"    # Z
    .param p3, "cropToSquare"    # Z
    .param p4, "bitmapListener"    # Lcom/google/android/music/leanback/bitmap/BitmapListener;

    .prologue
    const/4 v5, 0x0

    .line 60
    if-nez p1, :cond_0

    .line 61
    const/4 v0, 0x0

    invoke-interface {p4, v0}, Lcom/google/android/music/leanback/bitmap/BitmapListener;->onBitmapLoaded(Landroid/graphics/Bitmap;)V

    .line 70
    :goto_0
    return-void

    .line 64
    :cond_0
    if-nez p3, :cond_1

    if-eqz p2, :cond_1

    const/4 v7, 0x1

    .line 65
    .local v7, "useWideDimensions":Z
    :goto_1
    if-eqz v7, :cond_2

    invoke-static {p0}, Lcom/google/android/music/leanback/bitmap/BitmapFactory;->getWideCardImageHeightPixels(Landroid/content/Context;)I

    move-result v3

    .line 67
    .local v3, "targetHeight":I
    :goto_2
    if-eqz v7, :cond_3

    invoke-static {p0}, Lcom/google/android/music/leanback/bitmap/BitmapFactory;->getWideCardImageWidthPixels(Landroid/content/Context;)I

    move-result v2

    .local v2, "targetWidth":I
    :goto_3
    move-object v0, p0

    move-object v1, p1

    move v4, p3

    move-object v6, p4

    .line 68
    invoke-static/range {v0 .. v6}, Lcom/google/android/music/leanback/bitmap/BitmapFactory;->getBitmap(Landroid/content/Context;Lcom/google/android/music/leanback/bitmap/BitmapGettersGetter;IIZZLcom/google/android/music/leanback/bitmap/BitmapListener;)V

    goto :goto_0

    .end local v2    # "targetWidth":I
    .end local v3    # "targetHeight":I
    .end local v7    # "useWideDimensions":Z
    :cond_1
    move v7, v5

    .line 64
    goto :goto_1

    .line 65
    .restart local v7    # "useWideDimensions":Z
    :cond_2
    invoke-static {p0}, Lcom/google/android/music/leanback/bitmap/BitmapFactory;->getCardImageHeightPixels(Landroid/content/Context;)I

    move-result v3

    goto :goto_2

    .restart local v3    # "targetHeight":I
    :cond_3
    move v2, v3

    .line 67
    goto :goto_3
.end method

.method public static getCardImageHeightPixels(Landroid/content/Context;)I
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 35
    sget v0, Lcom/google/android/music/leanback/bitmap/BitmapFactory;->sImageCardHeightPixels:I

    if-gtz v0, :cond_0

    .line 36
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f016a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/google/android/music/leanback/bitmap/BitmapFactory;->sImageCardHeightPixels:I

    .line 39
    :cond_0
    sget v0, Lcom/google/android/music/leanback/bitmap/BitmapFactory;->sImageCardHeightPixels:I

    return v0
.end method

.method public static getWideCardImageHeightPixels(Landroid/content/Context;)I
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 43
    sget v0, Lcom/google/android/music/leanback/bitmap/BitmapFactory;->sImageCardWideHeightPixels:I

    if-gtz v0, :cond_0

    .line 44
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f016b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/google/android/music/leanback/bitmap/BitmapFactory;->sImageCardWideHeightPixels:I

    .line 47
    :cond_0
    sget v0, Lcom/google/android/music/leanback/bitmap/BitmapFactory;->sImageCardWideHeightPixels:I

    return v0
.end method

.method public static getWideCardImageWidthPixels(Landroid/content/Context;)I
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 51
    sget v0, Lcom/google/android/music/leanback/bitmap/BitmapFactory;->sImageCardWideWidthPixels:I

    if-gtz v0, :cond_0

    .line 52
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f016c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/google/android/music/leanback/bitmap/BitmapFactory;->sImageCardWideWidthPixels:I

    .line 55
    :cond_0
    sget v0, Lcom/google/android/music/leanback/bitmap/BitmapFactory;->sImageCardWideWidthPixels:I

    return v0
.end method

.method public static resizeBitmap(Landroid/content/Context;IILandroid/graphics/Bitmap;Z)Landroid/graphics/Bitmap;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "targetWidth"    # I
    .param p2, "targetHeight"    # I
    .param p3, "bitmap"    # Landroid/graphics/Bitmap;
    .param p4, "withFiltering"    # Z

    .prologue
    .line 192
    const-string v0, "Resizing bitmap on Main Thread!"

    invoke-static {p0, v0}, Lcom/google/android/music/utils/MusicUtils;->checkMainThread(Landroid/content/Context;Ljava/lang/String;)V

    .line 193
    if-eqz p4, :cond_0

    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    :goto_0
    invoke-static {p3, p1, p2, v0}, Lcom/google/android/music/art/ArtRenderingUtils;->sliceBitmapSectionAndScale(Landroid/graphics/Bitmap;IILandroid/graphics/Paint;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
