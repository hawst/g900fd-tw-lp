.class Lcom/google/android/music/ui/BaseTrackListView$3;
.super Ljava/lang/Object;
.source "BaseTrackListView.java"

# interfaces
.implements Lcom/mobeta/android/dslv/DragSortListView$RemoveListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/BaseTrackListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/BaseTrackListView;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/BaseTrackListView;)V
    .locals 0

    .prologue
    .line 87
    iput-object p1, p0, Lcom/google/android/music/ui/BaseTrackListView$3;->this$0:Lcom/google/android/music/ui/BaseTrackListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public remove(I)V
    .locals 8
    .param p1, "position"    # I

    .prologue
    .line 90
    iget-object v4, p0, Lcom/google/android/music/ui/BaseTrackListView$3;->this$0:Lcom/google/android/music/ui/BaseTrackListView;

    # getter for: Lcom/google/android/music/ui/BaseTrackListView;->mAdapter:Lcom/google/android/music/ui/TrackListAdapter;
    invoke-static {v4}, Lcom/google/android/music/ui/BaseTrackListView;->access$000(Lcom/google/android/music/ui/BaseTrackListView;)Lcom/google/android/music/ui/TrackListAdapter;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 91
    iget-object v4, p0, Lcom/google/android/music/ui/BaseTrackListView$3;->this$0:Lcom/google/android/music/ui/BaseTrackListView;

    # getter for: Lcom/google/android/music/ui/BaseTrackListView;->mAdapter:Lcom/google/android/music/ui/TrackListAdapter;
    invoke-static {v4}, Lcom/google/android/music/ui/BaseTrackListView;->access$000(Lcom/google/android/music/ui/BaseTrackListView;)Lcom/google/android/music/ui/TrackListAdapter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/music/ui/TrackListAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 92
    .local v0, "c":Landroid/database/Cursor;
    if-eqz v0, :cond_1

    .line 95
    const-string v4, "_id"

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    .line 98
    .local v1, "idIdx":I
    iget-object v4, p0, Lcom/google/android/music/ui/BaseTrackListView$3;->this$0:Lcom/google/android/music/ui/BaseTrackListView;

    # getter for: Lcom/google/android/music/ui/BaseTrackListView;->mAdapter:Lcom/google/android/music/ui/TrackListAdapter;
    invoke-static {v4}, Lcom/google/android/music/ui/BaseTrackListView;->access$000(Lcom/google/android/music/ui/BaseTrackListView;)Lcom/google/android/music/ui/TrackListAdapter;

    move-result-object v4

    invoke-virtual {v4, p1}, Lcom/google/android/music/ui/TrackListAdapter;->getItemTempPosition(I)I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v4

    if-nez v4, :cond_0

    .line 99
    const-string v4, "BaseTrackListView"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed to move item. Invalid \"remove\" position: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ". Cursor size:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    :cond_0
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 110
    .local v2, "itemToRemoveId":J
    iget-object v4, p0, Lcom/google/android/music/ui/BaseTrackListView$3;->this$0:Lcom/google/android/music/ui/BaseTrackListView;

    # getter for: Lcom/google/android/music/ui/BaseTrackListView;->mAdapter:Lcom/google/android/music/ui/TrackListAdapter;
    invoke-static {v4}, Lcom/google/android/music/ui/BaseTrackListView;->access$000(Lcom/google/android/music/ui/BaseTrackListView;)Lcom/google/android/music/ui/TrackListAdapter;

    move-result-object v4

    invoke-virtual {v4, p1}, Lcom/google/android/music/ui/TrackListAdapter;->removeItemTemp(I)V

    .line 111
    iget-object v4, p0, Lcom/google/android/music/ui/BaseTrackListView$3;->this$0:Lcom/google/android/music/ui/BaseTrackListView;

    invoke-virtual {v4}, Lcom/google/android/music/ui/BaseTrackListView;->invalidateViews()V

    .line 113
    iget-object v4, p0, Lcom/google/android/music/ui/BaseTrackListView$3;->this$0:Lcom/google/android/music/ui/BaseTrackListView;

    # getter for: Lcom/google/android/music/ui/BaseTrackListView;->mUndoEnabled:Z
    invoke-static {v4}, Lcom/google/android/music/ui/BaseTrackListView;->access$200(Lcom/google/android/music/ui/BaseTrackListView;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 114
    iget-object v4, p0, Lcom/google/android/music/ui/BaseTrackListView$3;->this$0:Lcom/google/android/music/ui/BaseTrackListView;

    # getter for: Lcom/google/android/music/ui/BaseTrackListView;->mUndoDialog:Lcom/google/android/music/ui/dialogs/UndoDialog;
    invoke-static {v4}, Lcom/google/android/music/ui/BaseTrackListView;->access$300(Lcom/google/android/music/ui/BaseTrackListView;)Lcom/google/android/music/ui/dialogs/UndoDialog;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Lcom/google/android/music/ui/dialogs/UndoDialog;->show(J)V

    .line 120
    .end local v0    # "c":Landroid/database/Cursor;
    .end local v1    # "idIdx":I
    .end local v2    # "itemToRemoveId":J
    :cond_1
    :goto_0
    return-void

    .line 116
    .restart local v0    # "c":Landroid/database/Cursor;
    .restart local v1    # "idIdx":I
    .restart local v2    # "itemToRemoveId":J
    :cond_2
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    const/4 v5, 0x1

    iget-object v6, p0, Lcom/google/android/music/ui/BaseTrackListView$3;->this$0:Lcom/google/android/music/ui/BaseTrackListView;

    # getter for: Lcom/google/android/music/ui/BaseTrackListView;->mSongList:Lcom/google/android/music/medialist/SongList;
    invoke-static {v6}, Lcom/google/android/music/ui/BaseTrackListView;->access$100(Lcom/google/android/music/ui/BaseTrackListView;)Lcom/google/android/music/medialist/SongList;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/music/ui/BaseTrackListView$3;->this$0:Lcom/google/android/music/ui/BaseTrackListView;

    invoke-virtual {v7}, Lcom/google/android/music/ui/BaseTrackListView;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v4, v5, v6, v7}, Lcom/google/android/music/utils/MusicUtils;->performDelete(Ljava/lang/Long;ZLcom/google/android/music/medialist/SongList;Landroid/content/Context;)V

    goto :goto_0
.end method
