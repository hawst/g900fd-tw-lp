.class Lcom/google/android/music/xdi/RootItemsCursor$MyMusicCursor;
.super Landroid/database/MatrixCursor;
.source "RootItemsCursor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/xdi/RootItemsCursor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MyMusicCursor"
.end annotation


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 74
    sget-object v1, Lcom/google/android/music/xdi/XdiContentProvider;->DEFAULT_BROWSE_HEADER_ID_PROJECTION:[Ljava/lang/String;

    invoke-direct {p0, v1}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 76
    new-instance v3, Lcom/google/android/music/xdi/ProjectionMap;

    sget-object v1, Lcom/google/android/music/xdi/XdiContentProvider;->DEFAULT_BROWSE_HEADER_ID_PROJECTION:[Ljava/lang/String;

    invoke-direct {v3, v1}, Lcom/google/android/music/xdi/ProjectionMap;-><init>([Ljava/lang/String;)V

    .line 79
    .local v3, "projectionMap":Lcom/google/android/music/xdi/ProjectionMap;
    const/4 v0, 0x0

    .line 80
    .local v0, "id":I
    add-int/lit8 v10, v0, 0x1

    .end local v0    # "id":I
    .local v10, "id":I
    int-to-long v4, v0

    invoke-direct {p0, p1, v3, v4, v5}, Lcom/google/android/music/xdi/RootItemsCursor$MyMusicCursor;->addShuffleAllRow(Landroid/content/Context;Lcom/google/android/music/xdi/ProjectionMap;J)V

    .line 81
    add-int/lit8 v0, v10, 0x1

    .end local v10    # "id":I
    .restart local v0    # "id":I
    int-to-long v4, v10

    const v6, 0x7f0b009e

    const v7, 0x7f02003a

    const-string v8, "myartists"

    const/4 v9, 0x1

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v1 .. v9}, Lcom/google/android/music/xdi/RootItemsCursor$MyMusicCursor;->addRow(Landroid/content/Context;Lcom/google/android/music/xdi/ProjectionMap;JIILjava/lang/String;Z)V

    .line 84
    add-int/lit8 v10, v0, 0x1

    .end local v0    # "id":I
    .restart local v10    # "id":I
    int-to-long v4, v0

    const v6, 0x7f0b00a0

    const v7, 0x7f02003a

    const-string v8, "mygenres"

    const/4 v9, 0x0

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v1 .. v9}, Lcom/google/android/music/xdi/RootItemsCursor$MyMusicCursor;->addRow(Landroid/content/Context;Lcom/google/android/music/xdi/ProjectionMap;JIILjava/lang/String;Z)V

    .line 86
    add-int/lit8 v0, v10, 0x1

    .end local v10    # "id":I
    .restart local v0    # "id":I
    int-to-long v4, v10

    const v6, 0x7f0b009f

    const v7, 0x7f02003a

    const-string v8, "myalbums"

    const/4 v9, 0x1

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v1 .. v9}, Lcom/google/android/music/xdi/RootItemsCursor$MyMusicCursor;->addRow(Landroid/content/Context;Lcom/google/android/music/xdi/ProjectionMap;JIILjava/lang/String;Z)V

    .line 88
    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;Lcom/google/android/music/xdi/RootItemsCursor$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/content/Context;
    .param p2, "x1"    # Lcom/google/android/music/xdi/RootItemsCursor$1;

    .prologue
    .line 72
    invoke-direct {p0, p1}, Lcom/google/android/music/xdi/RootItemsCursor$MyMusicCursor;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method private addRow(Landroid/content/Context;Lcom/google/android/music/xdi/ProjectionMap;JIILjava/lang/String;Z)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "projectionMap"    # Lcom/google/android/music/xdi/ProjectionMap;
    .param p3, "id"    # J
    .param p5, "nameResId"    # I
    .param p6, "imageResId"    # I
    .param p7, "path"    # Ljava/lang/String;
    .param p8, "isGridIntent"    # Z

    .prologue
    .line 112
    sget-object v4, Lcom/google/android/music/xdi/XdiContentProvider;->DEFAULT_BROWSE_HEADER_ID_PROJECTION:[Ljava/lang/String;

    array-length v4, v4

    new-array v3, v4, [Ljava/lang/Object;

    .line 114
    .local v3, "values":[Ljava/lang/Object;
    invoke-virtual {p1, p5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 115
    .local v2, "name":Ljava/lang/String;
    invoke-static {p1, p6}, Lcom/google/android/music/xdi/UriUtils;->getAndroidResourceUri(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    .line 118
    .local v0, "imageUri":Ljava/lang/String;
    if-eqz p8, :cond_0

    .line 119
    sget-object v4, Lcom/google/android/music/xdi/XdiContentProvider;->BASE_URI:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v4

    invoke-virtual {v4, p7}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v4, v5}, Lcom/google/android/music/xdi/XdiContract;->getGridIntent(Landroid/net/Uri;I)Landroid/content/Intent;

    move-result-object v1

    .line 127
    .local v1, "intent":Landroid/content/Intent;
    :goto_0
    const-string v4, "_id"

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {p2, v3, v4, v5}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 128
    const-string v4, "display_name"

    invoke-virtual {p2, v3, v4, v2}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 129
    const-string v4, "image_uri"

    invoke-virtual {p2, v3, v4, v0}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 130
    const-string v4, "intent_uri"

    const/4 v5, 0x1

    invoke-virtual {v1, v5}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p2, v3, v4, v5}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 133
    invoke-virtual {p0, v3}, Lcom/google/android/music/xdi/RootItemsCursor$MyMusicCursor;->addRow([Ljava/lang/Object;)V

    .line 134
    return-void

    .line 123
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_0
    sget-object v4, Lcom/google/android/music/xdi/XdiContentProvider;->BASE_URI:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v4

    invoke-virtual {v4, p7}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v4

    invoke-static {v4, v2}, Lcom/google/android/music/xdi/XdiContract;->getBrowseIntentByName(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .restart local v1    # "intent":Landroid/content/Intent;
    goto :goto_0
.end method

.method private addShuffleAllRow(Landroid/content/Context;Lcom/google/android/music/xdi/ProjectionMap;J)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "projectionMap"    # Lcom/google/android/music/xdi/ProjectionMap;
    .param p3, "id"    # J

    .prologue
    .line 91
    sget-object v4, Lcom/google/android/music/xdi/XdiContentProvider;->DEFAULT_BROWSE_HEADER_ID_PROJECTION:[Ljava/lang/String;

    array-length v4, v4

    new-array v3, v4, [Ljava/lang/Object;

    .line 93
    .local v3, "values":[Ljava/lang/Object;
    const v4, 0x7f0b0054

    invoke-virtual {p1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 94
    .local v2, "name":Ljava/lang/String;
    const v4, 0x7f02003a

    invoke-static {p1, v4}, Lcom/google/android/music/xdi/UriUtils;->getAndroidResourceUri(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    .line 97
    .local v0, "imageUri":Ljava/lang/String;
    invoke-static {}, Lcom/google/android/music/xdi/XdiUtils;->newPanoPlayIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "container"

    const/16 v6, 0x19

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    .line 101
    .local v1, "intent":Landroid/content/Intent;
    const-string v4, "_id"

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {p2, v3, v4, v5}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 102
    const-string v4, "display_name"

    invoke-virtual {p2, v3, v4, v2}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 103
    const-string v4, "image_uri"

    invoke-virtual {p2, v3, v4, v0}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 104
    const-string v4, "intent_uri"

    const/4 v5, 0x1

    invoke-virtual {v1, v5}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p2, v3, v4, v5}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 107
    invoke-virtual {p0, v3}, Lcom/google/android/music/xdi/RootItemsCursor$MyMusicCursor;->addRow([Ljava/lang/Object;)V

    .line 108
    return-void
.end method
