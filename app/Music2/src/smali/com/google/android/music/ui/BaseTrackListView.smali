.class public Lcom/google/android/music/ui/BaseTrackListView;
.super Lcom/mobeta/android/dslv/DragSortListView;
.source "BaseTrackListView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/ui/BaseTrackListView$CustomDSController;,
        Lcom/google/android/music/ui/BaseTrackListView$OnContentChangedCallback;
    }
.end annotation


# instance fields
.field private mAbsoluteY:I

.field private mAdapter:Lcom/google/android/music/ui/TrackListAdapter;

.field private mEditable:Z

.field private mNowPlayingReceiver:Landroid/content/BroadcastReceiver;

.field private mOnDrop:Lcom/mobeta/android/dslv/DragSortListView$DropListener;

.field private mOnRemove:Lcom/mobeta/android/dslv/DragSortListView$RemoveListener;

.field private mOnUndoClick:Lcom/google/android/music/ui/dialogs/UndoDialog$UndoClickListener;

.field private mPositionToCenter:I

.field private mScrollToNowPlaying:Z

.field private mSongList:Lcom/google/android/music/medialist/SongList;

.field private mUndoDialog:Lcom/google/android/music/ui/dialogs/UndoDialog;

.field private mUndoEnabled:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 141
    invoke-direct {p0, p1, p2}, Lcom/mobeta/android/dslv/DragSortListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 47
    iput v1, p0, Lcom/google/android/music/ui/BaseTrackListView;->mAbsoluteY:I

    .line 49
    new-instance v0, Lcom/google/android/music/ui/BaseTrackListView$1;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/BaseTrackListView$1;-><init>(Lcom/google/android/music/ui/BaseTrackListView;)V

    iput-object v0, p0, Lcom/google/android/music/ui/BaseTrackListView;->mNowPlayingReceiver:Landroid/content/BroadcastReceiver;

    .line 59
    new-instance v0, Lcom/google/android/music/ui/BaseTrackListView$2;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/BaseTrackListView$2;-><init>(Lcom/google/android/music/ui/BaseTrackListView;)V

    iput-object v0, p0, Lcom/google/android/music/ui/BaseTrackListView;->mOnDrop:Lcom/mobeta/android/dslv/DragSortListView$DropListener;

    .line 86
    new-instance v0, Lcom/google/android/music/ui/BaseTrackListView$3;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/BaseTrackListView$3;-><init>(Lcom/google/android/music/ui/BaseTrackListView;)V

    iput-object v0, p0, Lcom/google/android/music/ui/BaseTrackListView;->mOnRemove:Lcom/mobeta/android/dslv/DragSortListView$RemoveListener;

    .line 123
    new-instance v0, Lcom/google/android/music/ui/BaseTrackListView$4;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/BaseTrackListView$4;-><init>(Lcom/google/android/music/ui/BaseTrackListView;)V

    iput-object v0, p0, Lcom/google/android/music/ui/BaseTrackListView;->mOnUndoClick:Lcom/google/android/music/ui/dialogs/UndoDialog$UndoClickListener;

    .line 136
    iput v1, p0, Lcom/google/android/music/ui/BaseTrackListView;->mPositionToCenter:I

    .line 142
    invoke-virtual {p0, v2}, Lcom/google/android/music/ui/BaseTrackListView;->setDragEnabled(Z)V

    .line 143
    invoke-virtual {p0, v2}, Lcom/google/android/music/ui/BaseTrackListView;->setVerticalFadingEdgeEnabled(Z)V

    .line 144
    invoke-virtual {p0, v3}, Lcom/google/android/music/ui/BaseTrackListView;->setFastScrollEnabled(Z)V

    .line 145
    new-instance v0, Lcom/google/android/music/ui/dialogs/UndoDialog;

    iget-object v1, p0, Lcom/google/android/music/ui/BaseTrackListView;->mOnUndoClick:Lcom/google/android/music/ui/dialogs/UndoDialog$UndoClickListener;

    invoke-direct {v0, v1, p1}, Lcom/google/android/music/ui/dialogs/UndoDialog;-><init>(Lcom/google/android/music/ui/dialogs/UndoDialog$UndoClickListener;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/music/ui/BaseTrackListView;->mUndoDialog:Lcom/google/android/music/ui/dialogs/UndoDialog;

    .line 146
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "music_enable_undo_removal"

    invoke-static {v0, v1, v3}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/music/ui/BaseTrackListView;->mUndoEnabled:Z

    .line 149
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/ui/BaseTrackListView;)Lcom/google/android/music/ui/TrackListAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/BaseTrackListView;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/music/ui/BaseTrackListView;->mAdapter:Lcom/google/android/music/ui/TrackListAdapter;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/music/ui/BaseTrackListView;)Lcom/google/android/music/medialist/SongList;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/BaseTrackListView;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/music/ui/BaseTrackListView;->mSongList:Lcom/google/android/music/medialist/SongList;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/music/ui/BaseTrackListView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/BaseTrackListView;

    .prologue
    .line 44
    iget-boolean v0, p0, Lcom/google/android/music/ui/BaseTrackListView;->mUndoEnabled:Z

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/music/ui/BaseTrackListView;)Lcom/google/android/music/ui/dialogs/UndoDialog;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/BaseTrackListView;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/music/ui/BaseTrackListView;->mUndoDialog:Lcom/google/android/music/ui/dialogs/UndoDialog;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/music/ui/BaseTrackListView;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/ui/BaseTrackListView;

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/google/android/music/ui/BaseTrackListView;->doScroll()V

    return-void
.end method

.method private disableDragDrop()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 351
    invoke-virtual {p0, v1}, Lcom/google/android/music/ui/BaseTrackListView;->setDragEnabled(Z)V

    .line 352
    iget-object v0, p0, Lcom/google/android/music/ui/BaseTrackListView;->mAdapter:Lcom/google/android/music/ui/TrackListAdapter;

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/TrackListAdapter;->setEditMode(Z)V

    .line 353
    return-void
.end method

.method private doScroll()V
    .locals 7

    .prologue
    const/4 v5, -0x1

    .line 359
    iget-boolean v4, p0, Lcom/google/android/music/ui/BaseTrackListView;->mScrollToNowPlaying:Z

    if-nez v4, :cond_0

    iget v4, p0, Lcom/google/android/music/ui/BaseTrackListView;->mPositionToCenter:I

    if-eq v4, v5, :cond_1

    .line 360
    :cond_0
    iget-object v4, p0, Lcom/google/android/music/ui/BaseTrackListView;->mAdapter:Lcom/google/android/music/ui/TrackListAdapter;

    if-nez v4, :cond_2

    .line 391
    :cond_1
    :goto_0
    return-void

    .line 364
    :cond_2
    const/4 v3, 0x0

    .line 365
    .local v3, "scrollToPosition":I
    const/4 v2, 0x0

    .line 366
    .local v2, "offset":I
    :try_start_0
    iget v4, p0, Lcom/google/android/music/ui/BaseTrackListView;->mPositionToCenter:I

    if-eq v4, v5, :cond_3

    .line 367
    iget v3, p0, Lcom/google/android/music/ui/BaseTrackListView;->mPositionToCenter:I

    .line 368
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseTrackListView;->getHeight()I

    move-result v4

    div-int/lit8 v2, v4, 0x2

    .line 377
    :goto_1
    iget-object v4, p0, Lcom/google/android/music/ui/BaseTrackListView;->mAdapter:Lcom/google/android/music/ui/TrackListAdapter;

    invoke-virtual {v4}, Lcom/google/android/music/ui/TrackListAdapter;->getCount()I

    move-result v0

    .line 378
    .local v0, "adapterCount":I
    if-le v3, v0, :cond_5

    .line 379
    const-string v4, "BaseTrackListView"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Need to scroll to: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " which is larger than the current list size: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 387
    .end local v0    # "adapterCount":I
    :catch_0
    move-exception v1

    .line 388
    .local v1, "e":Landroid/os/RemoteException;
    const-string v4, "BaseTrackListView"

    invoke-virtual {v1}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 370
    .end local v1    # "e":Landroid/os/RemoteException;
    :cond_3
    :try_start_1
    sget-object v4, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    if-nez v4, :cond_4

    .line 371
    const-string v4, "BaseTrackListView"

    const-string v5, "Playback service not ready."

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 374
    :cond_4
    sget-object v4, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    invoke-interface {v4}, Lcom/google/android/music/playback/IMusicPlaybackService;->getQueuePosition()I

    move-result v3

    goto :goto_1

    .line 383
    .restart local v0    # "adapterCount":I
    :cond_5
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseTrackListView;->getHeaderViewsCount()I

    move-result v4

    add-int/2addr v4, v3

    invoke-virtual {p0, v4, v2}, Lcom/google/android/music/ui/BaseTrackListView;->setSelectionFromTop(II)V

    .line 385
    const/4 v4, -0x1

    iput v4, p0, Lcom/google/android/music/ui/BaseTrackListView;->mPositionToCenter:I

    .line 386
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/google/android/music/ui/BaseTrackListView;->mScrollToNowPlaying:Z
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method private enableDragDrop()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 338
    invoke-virtual {p0, v3}, Lcom/google/android/music/ui/BaseTrackListView;->setDragEnabled(Z)V

    .line 339
    iget-object v1, p0, Lcom/google/android/music/ui/BaseTrackListView;->mOnDrop:Lcom/mobeta/android/dslv/DragSortListView$DropListener;

    invoke-virtual {p0, v1}, Lcom/google/android/music/ui/BaseTrackListView;->setDropListener(Lcom/mobeta/android/dslv/DragSortListView$DropListener;)V

    .line 340
    iget-object v1, p0, Lcom/google/android/music/ui/BaseTrackListView;->mOnRemove:Lcom/mobeta/android/dslv/DragSortListView$RemoveListener;

    invoke-virtual {p0, v1}, Lcom/google/android/music/ui/BaseTrackListView;->setRemoveListener(Lcom/mobeta/android/dslv/DragSortListView$RemoveListener;)V

    .line 341
    const/high16 v1, 0x40a00000    # 5.0f

    invoke-virtual {p0, v1}, Lcom/google/android/music/ui/BaseTrackListView;->setMaxScrollSpeed(F)V

    .line 343
    new-instance v0, Lcom/google/android/music/ui/BaseTrackListView$CustomDSController;

    iget-object v1, p0, Lcom/google/android/music/ui/BaseTrackListView;->mUndoDialog:Lcom/google/android/music/ui/dialogs/UndoDialog;

    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseTrackListView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/music/ui/BaseTrackListView$CustomDSController;-><init>(Lcom/mobeta/android/dslv/DragSortListView;Lcom/google/android/music/ui/dialogs/UndoDialog;Landroid/content/Context;)V

    .line 344
    .local v0, "controller":Lcom/google/android/music/ui/BaseTrackListView$CustomDSController;
    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/BaseTrackListView;->setFloatViewManager(Lcom/mobeta/android/dslv/DragSortListView$FloatViewManager;)V

    .line 345
    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/BaseTrackListView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 347
    iget-object v1, p0, Lcom/google/android/music/ui/BaseTrackListView;->mAdapter:Lcom/google/android/music/ui/TrackListAdapter;

    invoke-virtual {v1, v3}, Lcom/google/android/music/ui/TrackListAdapter;->setEditMode(Z)V

    .line 348
    return-void
.end method

.method private findTrackListAdapter(Landroid/widget/ListAdapter;)Lcom/google/android/music/ui/TrackListAdapter;
    .locals 1
    .param p1, "adapter"    # Landroid/widget/ListAdapter;

    .prologue
    .line 327
    instance-of v0, p1, Lcom/google/android/music/ui/TrackListAdapter;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 328
    check-cast v0, Lcom/google/android/music/ui/TrackListAdapter;

    .line 334
    :goto_0
    return-object v0

    .line 329
    :cond_0
    instance-of v0, p1, Landroid/widget/HeaderViewListAdapter;

    if-eqz v0, :cond_1

    .line 331
    check-cast p1, Landroid/widget/HeaderViewListAdapter;

    .end local p1    # "adapter":Landroid/widget/ListAdapter;
    invoke-virtual {p1}, Landroid/widget/HeaderViewListAdapter;->getWrappedAdapter()Landroid/widget/ListAdapter;

    move-result-object p1

    .line 332
    .restart local p1    # "adapter":Landroid/widget/ListAdapter;
    invoke-direct {p0, p1}, Lcom/google/android/music/ui/BaseTrackListView;->findTrackListAdapter(Landroid/widget/ListAdapter;)Lcom/google/android/music/ui/TrackListAdapter;

    move-result-object v0

    goto :goto_0

    .line 334
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getTag(Landroid/view/View;)Ljava/lang/Object;
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 308
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    .line 309
    .local v2, "tag":Ljava/lang/Object;
    if-nez v2, :cond_1

    instance-of v5, p1, Landroid/view/ViewGroup;

    if-eqz v5, :cond_1

    move-object v4, p1

    .line 310
    check-cast v4, Landroid/view/ViewGroup;

    .line 311
    .local v4, "vg":Landroid/view/ViewGroup;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v4}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v5

    if-ge v1, v5, :cond_1

    .line 312
    invoke-virtual {v4, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 313
    .local v0, "child":Landroid/view/View;
    invoke-direct {p0, v0}, Lcom/google/android/music/ui/BaseTrackListView;->getTag(Landroid/view/View;)Ljava/lang/Object;

    move-result-object v2

    .line 314
    if-eqz v2, :cond_0

    move-object v3, v2

    .line 319
    .end local v0    # "child":Landroid/view/View;
    .end local v1    # "i":I
    .end local v2    # "tag":Ljava/lang/Object;
    .end local v4    # "vg":Landroid/view/ViewGroup;
    .local v3, "tag":Ljava/lang/Object;
    :goto_1
    return-object v3

    .line 311
    .end local v3    # "tag":Ljava/lang/Object;
    .restart local v0    # "child":Landroid/view/View;
    .restart local v1    # "i":I
    .restart local v2    # "tag":Ljava/lang/Object;
    .restart local v4    # "vg":Landroid/view/ViewGroup;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .end local v0    # "child":Landroid/view/View;
    .end local v1    # "i":I
    .end local v4    # "vg":Landroid/view/ViewGroup;
    :cond_1
    move-object v3, v2

    .line 319
    .end local v2    # "tag":Ljava/lang/Object;
    .restart local v3    # "tag":Ljava/lang/Object;
    goto :goto_1
.end method


# virtual methods
.method public getAbsoluteY()I
    .locals 1

    .prologue
    .line 404
    iget v0, p0, Lcom/google/android/music/ui/BaseTrackListView;->mAbsoluteY:I

    return v0
.end method

.method handleItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 6
    .param p1, "l"    # Landroid/widget/ListView;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    const/4 v5, 0x1

    .line 269
    invoke-virtual {p1}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result v0

    .line 270
    .local v0, "headerCount":I
    if-ge p3, v0, :cond_0

    .line 301
    :goto_0
    return-void

    .line 272
    :cond_0
    iget-object v3, p0, Lcom/google/android/music/ui/BaseTrackListView;->mSongList:Lcom/google/android/music/medialist/SongList;

    if-nez v3, :cond_1

    .line 273
    const-string v3, "BaseTrackListView"

    const-string v4, "The SongList is null."

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 277
    :cond_1
    invoke-direct {p0, p2}, Lcom/google/android/music/ui/BaseTrackListView;->getTag(Landroid/view/View;)Ljava/lang/Object;

    move-result-object v1

    .line 278
    .local v1, "tag":Ljava/lang/Object;
    instance-of v3, v1, Lcom/google/android/music/ui/TrackListAdapter$ViewHolder;

    if-eqz v3, :cond_4

    move-object v2, v1

    .line 279
    check-cast v2, Lcom/google/android/music/ui/TrackListAdapter$ViewHolder;

    .line 280
    .local v2, "vh":Lcom/google/android/music/ui/TrackListAdapter$ViewHolder;
    iget-boolean v3, v2, Lcom/google/android/music/ui/TrackListAdapter$ViewHolder;->isAvailable:Z

    if-eqz v3, :cond_2

    .line 281
    iget-object v3, p0, Lcom/google/android/music/ui/BaseTrackListView;->mSongList:Lcom/google/android/music/medialist/SongList;

    sub-int v4, p3, v0

    invoke-static {v3, v4}, Lcom/google/android/music/utils/MusicUtils;->playMediaList(Lcom/google/android/music/medialist/SongList;I)V

    .line 298
    .end local v2    # "vh":Lcom/google/android/music/ui/TrackListAdapter$ViewHolder;
    :goto_1
    iget-object v3, p0, Lcom/google/android/music/ui/BaseTrackListView;->mAdapter:Lcom/google/android/music/ui/TrackListAdapter;

    invoke-virtual {v3}, Lcom/google/android/music/ui/TrackListAdapter;->clearHighlight()V

    .line 300
    iget-object v3, p0, Lcom/google/android/music/ui/BaseTrackListView;->mUndoDialog:Lcom/google/android/music/ui/dialogs/UndoDialog;

    invoke-virtual {v3}, Lcom/google/android/music/ui/dialogs/UndoDialog;->dismiss()V

    goto :goto_0

    .line 284
    .restart local v2    # "vh":Lcom/google/android/music/ui/TrackListAdapter$ViewHolder;
    :cond_2
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/music/preferences/MusicPreferences;->isStreamOnlyOnWifi()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 285
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseTrackListView;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0b016a

    invoke-static {v3, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_1

    .line 288
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseTrackListView;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0b016b

    invoke-static {v3, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_1

    .line 293
    .end local v2    # "vh":Lcom/google/android/music/ui/TrackListAdapter$ViewHolder;
    :cond_4
    const-string v3, "BaseTrackListView"

    const-string v4, "The tag on the view wasn\'t a ViewHolder. The underlying adapter probably is not TrackListAdapter."

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 295
    iget-object v3, p0, Lcom/google/android/music/ui/BaseTrackListView;->mSongList:Lcom/google/android/music/medialist/SongList;

    sub-int v4, p3, v0

    invoke-static {v3, v4}, Lcom/google/android/music/utils/MusicUtils;->playMediaList(Lcom/google/android/music/medialist/SongList;I)V

    goto :goto_1
.end method

.method public onAttachedToWindow()V
    .locals 3

    .prologue
    .line 153
    invoke-super {p0}, Lcom/mobeta/android/dslv/DragSortListView;->onAttachedToWindow()V

    .line 154
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 155
    .local v0, "f":Landroid/content/IntentFilter;
    const-string v1, "com.android.music.metachanged"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 156
    const-string v1, "com.android.music.queuechanged"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 157
    const-string v1, "com.google.android.music.mix.playbackmodechanged"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 158
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseTrackListView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/BaseTrackListView;->mNowPlayingReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 159
    iget-object v1, p0, Lcom/google/android/music/ui/BaseTrackListView;->mAdapter:Lcom/google/android/music/ui/TrackListAdapter;

    if-eqz v1, :cond_0

    .line 160
    iget-object v1, p0, Lcom/google/android/music/ui/BaseTrackListView;->mAdapter:Lcom/google/android/music/ui/TrackListAdapter;

    invoke-virtual {v1}, Lcom/google/android/music/ui/TrackListAdapter;->updatePlaybackState()V

    .line 162
    :cond_0
    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 167
    iget-object v0, p0, Lcom/google/android/music/ui/BaseTrackListView;->mUndoDialog:Lcom/google/android/music/ui/dialogs/UndoDialog;

    invoke-virtual {v0}, Lcom/google/android/music/ui/dialogs/UndoDialog;->dismiss()V

    .line 168
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseTrackListView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/ui/BaseTrackListView;->mNowPlayingReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 169
    invoke-super {p0}, Lcom/mobeta/android/dslv/DragSortListView;->onDetachedFromWindow()V

    .line 170
    return-void
.end method

.method public scrollToNowPlaying()V
    .locals 1

    .prologue
    .line 198
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/ui/BaseTrackListView;->mScrollToNowPlaying:Z

    .line 199
    invoke-direct {p0}, Lcom/google/android/music/ui/BaseTrackListView;->doScroll()V

    .line 200
    return-void
.end method

.method public scrollToPositionAsCentered(I)V
    .locals 0
    .param p1, "positionToCenter"    # I

    .prologue
    .line 207
    iput p1, p0, Lcom/google/android/music/ui/BaseTrackListView;->mPositionToCenter:I

    .line 208
    invoke-direct {p0}, Lcom/google/android/music/ui/BaseTrackListView;->doScroll()V

    .line 209
    return-void
.end method

.method public setAbsoluteY(I)V
    .locals 0
    .param p1, "y"    # I

    .prologue
    .line 400
    iput p1, p0, Lcom/google/android/music/ui/BaseTrackListView;->mAbsoluteY:I

    .line 401
    return-void
.end method

.method public bridge synthetic setAdapter(Landroid/widget/Adapter;)V
    .locals 0
    .param p1, "x0"    # Landroid/widget/Adapter;

    .prologue
    .line 44
    check-cast p1, Landroid/widget/ListAdapter;

    .end local p1    # "x0":Landroid/widget/Adapter;
    invoke-virtual {p0, p1}, Lcom/google/android/music/ui/BaseTrackListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method public setAdapter(Landroid/widget/ListAdapter;)V
    .locals 2
    .param p1, "adapter"    # Landroid/widget/ListAdapter;

    .prologue
    .line 174
    invoke-super {p0, p1}, Lcom/mobeta/android/dslv/DragSortListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 175
    if-nez p1, :cond_0

    .line 177
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/ui/BaseTrackListView;->mAdapter:Lcom/google/android/music/ui/TrackListAdapter;

    .line 192
    :goto_0
    return-void

    .line 181
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/music/ui/BaseTrackListView;->findTrackListAdapter(Landroid/widget/ListAdapter;)Lcom/google/android/music/ui/TrackListAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/BaseTrackListView;->mAdapter:Lcom/google/android/music/ui/TrackListAdapter;

    .line 182
    iget-object v0, p0, Lcom/google/android/music/ui/BaseTrackListView;->mAdapter:Lcom/google/android/music/ui/TrackListAdapter;

    if-nez v0, :cond_1

    .line 183
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The adapter must be a TrackListAdapter"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 186
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/ui/BaseTrackListView;->mAdapter:Lcom/google/android/music/ui/TrackListAdapter;

    new-instance v1, Lcom/google/android/music/ui/BaseTrackListView$5;

    invoke-direct {v1, p0}, Lcom/google/android/music/ui/BaseTrackListView$5;-><init>(Lcom/google/android/music/ui/BaseTrackListView;)V

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/TrackListAdapter;->setOnContentChangedCallback(Lcom/google/android/music/ui/BaseTrackListView$OnContentChangedCallback;)V

    goto :goto_0
.end method

.method public setSongList(Lcom/google/android/music/medialist/SongList;)V
    .locals 2
    .param p1, "songlist"    # Lcom/google/android/music/medialist/SongList;

    .prologue
    .line 222
    iput-object p1, p0, Lcom/google/android/music/ui/BaseTrackListView;->mSongList:Lcom/google/android/music/medialist/SongList;

    .line 224
    iget-object v0, p0, Lcom/google/android/music/ui/BaseTrackListView;->mUndoDialog:Lcom/google/android/music/ui/dialogs/UndoDialog;

    invoke-virtual {v0}, Lcom/google/android/music/ui/dialogs/UndoDialog;->dismiss()V

    .line 226
    iget-object v0, p0, Lcom/google/android/music/ui/BaseTrackListView;->mUndoDialog:Lcom/google/android/music/ui/dialogs/UndoDialog;

    invoke-virtual {v0, p1}, Lcom/google/android/music/ui/dialogs/UndoDialog;->setSongList(Lcom/google/android/music/medialist/SongList;)V

    .line 228
    iget-object v0, p0, Lcom/google/android/music/ui/BaseTrackListView;->mSongList:Lcom/google/android/music/medialist/SongList;

    invoke-virtual {v0}, Lcom/google/android/music/medialist/SongList;->isEditable()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/music/ui/BaseTrackListView;->mEditable:Z

    .line 229
    iget-boolean v0, p0, Lcom/google/android/music/ui/BaseTrackListView;->mEditable:Z

    if-eqz v0, :cond_0

    .line 230
    invoke-direct {p0}, Lcom/google/android/music/ui/BaseTrackListView;->enableDragDrop()V

    .line 235
    :goto_0
    iget-object v0, p0, Lcom/google/android/music/ui/BaseTrackListView;->mAdapter:Lcom/google/android/music/ui/TrackListAdapter;

    iget-object v1, p0, Lcom/google/android/music/ui/BaseTrackListView;->mSongList:Lcom/google/android/music/medialist/SongList;

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/TrackListAdapter;->setSongList(Lcom/google/android/music/medialist/SongList;)V

    .line 238
    new-instance v0, Lcom/google/android/music/ui/BaseTrackListView$6;

    invoke-direct {v0, p0}, Lcom/google/android/music/ui/BaseTrackListView$6;-><init>(Lcom/google/android/music/ui/BaseTrackListView;)V

    invoke-static {v0}, Lcom/google/android/music/utils/MusicUtils;->runAsyncWithCallback(Lcom/google/android/music/utils/async/AsyncRunner;)V

    .line 251
    return-void

    .line 232
    :cond_0
    invoke-direct {p0}, Lcom/google/android/music/ui/BaseTrackListView;->disableDragDrop()V

    goto :goto_0
.end method

.method public showTrackArtist(Z)V
    .locals 2
    .param p1, "show"    # Z

    .prologue
    .line 259
    iget-object v0, p0, Lcom/google/android/music/ui/BaseTrackListView;->mAdapter:Lcom/google/android/music/ui/TrackListAdapter;

    if-nez v0, :cond_0

    .line 260
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The adapter wasn\'t set"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 262
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/ui/BaseTrackListView;->mAdapter:Lcom/google/android/music/ui/TrackListAdapter;

    invoke-virtual {v0, p1}, Lcom/google/android/music/ui/TrackListAdapter;->showTrackArtist(Z)V

    .line 263
    return-void
.end method
