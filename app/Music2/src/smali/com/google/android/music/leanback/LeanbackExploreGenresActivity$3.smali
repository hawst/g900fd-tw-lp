.class Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$3;
.super Ljava/lang/Object;
.source "LeanbackExploreGenresActivity.java"

# interfaces
.implements Lcom/google/android/music/leanback/Item$StringGetter;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;->getArtUriGetterForGenre(Landroid/content/Context;Ljava/lang/String;J)Lcom/google/android/music/leanback/Item$StringGetter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mArtUri:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$genreId:Ljava/lang/String;

.field final synthetic val$seed:J


# direct methods
.method constructor <init>(Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;Landroid/content/Context;Ljava/lang/String;J)V
    .locals 0

    .prologue
    .line 211
    iput-object p1, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$3;->this$0:Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;

    iput-object p2, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$3;->val$context:Landroid/content/Context;

    iput-object p3, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$3;->val$genreId:Ljava/lang/String;

    iput-wide p4, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$3;->val$seed:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 216
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$3;->mArtUri:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 217
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$3;->mArtUri:Ljava/lang/String;

    .line 225
    :goto_0
    return-object v1

    .line 219
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$3;->val$context:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$3;->val$genreId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$3;->this$0:Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;

    # getter for: Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;->mGenreId:Ljava/lang/String;
    invoke-static {v3}, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;->access$500(Lcom/google/android/music/leanback/LeanbackExploreGenresActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/google/android/music/utils/MusicUtils;->getArtUrlsForGenreRadio(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$3;->mArtUri:Ljava/lang/String;

    .line 220
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$3;->mArtUri:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/music/utils/MusicUtils;->decodeStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 221
    .local v0, "arts":[Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 222
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    new-instance v2, Ljava/util/Random;

    iget-wide v4, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$3;->val$seed:J

    invoke-direct {v2, v4, v5}, Ljava/util/Random;-><init>(J)V

    invoke-static {v1, v2}, Ljava/util/Collections;->shuffle(Ljava/util/List;Ljava/util/Random;)V

    .line 223
    invoke-static {v0}, Lcom/google/android/music/utils/MusicUtils;->encodeStringArray([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$3;->mArtUri:Ljava/lang/String;

    .line 225
    :cond_1
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackExploreGenresActivity$3;->mArtUri:Ljava/lang/String;

    goto :goto_0
.end method
