.class public Lcom/google/android/music/cloudclient/OfferJson;
.super Lcom/google/api/client/json/GenericJson;
.source "OfferJson.java"


# instance fields
.field public mDescription:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "description"
    .end annotation
.end field

.field public mFopLess:Z
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "fop_less"
    .end annotation
.end field

.field public mHasFreeTrialPeriod:Z
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "has_trial"
    .end annotation
.end field

.field public mStoreDocId:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "offer_id"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    return-void
.end method
