.class public Lcom/google/android/music/activities/CreateMixActivity;
.super Landroid/app/Activity;
.source "CreateMixActivity.java"


# static fields
.field private static final LOGV:Z


# instance fields
.field private mAsyncWorker:Lcom/google/android/music/mix/AsyncMixCreatorWorker;

.field private mDivider:Landroid/view/View;

.field private mIflRadio:Z

.field private mMediaController:Landroid/widget/MediaController;

.field private volatile mMixCreated:Z

.field mMixCreationCallbacks:Lcom/google/android/music/mix/AsyncMixCreatorWorker$MixCreationCallbacks;

.field private mOnCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

.field private mOnErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

.field private mOnPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

.field private mText:Landroid/widget/TextView;

.field private mVideoView:Landroid/widget/VideoView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->UI:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/activities/CreateMixActivity;->LOGV:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 42
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/activities/CreateMixActivity;->mMixCreated:Z

    .line 54
    new-instance v0, Lcom/google/android/music/activities/CreateMixActivity$1;

    invoke-direct {v0, p0}, Lcom/google/android/music/activities/CreateMixActivity$1;-><init>(Lcom/google/android/music/activities/CreateMixActivity;)V

    iput-object v0, p0, Lcom/google/android/music/activities/CreateMixActivity;->mMixCreationCallbacks:Lcom/google/android/music/mix/AsyncMixCreatorWorker$MixCreationCallbacks;

    .line 114
    new-instance v0, Lcom/google/android/music/activities/CreateMixActivity$2;

    invoke-direct {v0, p0}, Lcom/google/android/music/activities/CreateMixActivity$2;-><init>(Lcom/google/android/music/activities/CreateMixActivity;)V

    iput-object v0, p0, Lcom/google/android/music/activities/CreateMixActivity;->mOnPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    .line 127
    new-instance v0, Lcom/google/android/music/activities/CreateMixActivity$3;

    invoke-direct {v0, p0}, Lcom/google/android/music/activities/CreateMixActivity$3;-><init>(Lcom/google/android/music/activities/CreateMixActivity;)V

    iput-object v0, p0, Lcom/google/android/music/activities/CreateMixActivity;->mOnCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    .line 137
    new-instance v0, Lcom/google/android/music/activities/CreateMixActivity$4;

    invoke-direct {v0, p0}, Lcom/google/android/music/activities/CreateMixActivity$4;-><init>(Lcom/google/android/music/activities/CreateMixActivity;)V

    iput-object v0, p0, Lcom/google/android/music/activities/CreateMixActivity;->mOnErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    return-void
.end method

.method static synthetic access$002(Lcom/google/android/music/activities/CreateMixActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/activities/CreateMixActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 34
    iput-boolean p1, p0, Lcom/google/android/music/activities/CreateMixActivity;->mMixCreated:Z

    return p1
.end method

.method static synthetic access$100(Lcom/google/android/music/activities/CreateMixActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/activities/CreateMixActivity;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/music/activities/CreateMixActivity;->mText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/music/activities/CreateMixActivity;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/activities/CreateMixActivity;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/music/activities/CreateMixActivity;->mDivider:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/music/activities/CreateMixActivity;)Landroid/widget/MediaController;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/activities/CreateMixActivity;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/music/activities/CreateMixActivity;->mMediaController:Landroid/widget/MediaController;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/music/activities/CreateMixActivity;)Landroid/widget/VideoView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/activities/CreateMixActivity;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/music/activities/CreateMixActivity;->mVideoView:Landroid/widget/VideoView;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/music/activities/CreateMixActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/activities/CreateMixActivity;

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/google/android/music/activities/CreateMixActivity;->showFallbackImage()V

    return-void
.end method

.method private showFallbackImage()V
    .locals 3

    .prologue
    .line 227
    const v1, 0x7f0e00fb

    invoke-virtual {p0, v1}, Lcom/google/android/music/activities/CreateMixActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 228
    .local v0, "fallback":Landroid/widget/ImageView;
    iget-object v1, p0, Lcom/google/android/music/activities/CreateMixActivity;->mVideoView:Landroid/widget/VideoView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/VideoView;->setVisibility(I)V

    .line 229
    iget-boolean v1, p0, Lcom/google/android/music/activities/CreateMixActivity;->mIflRadio:Z

    if-eqz v1, :cond_0

    const v1, 0x7f0200e1

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 231
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 232
    return-void

    .line 229
    :cond_0
    const v1, 0x7f02013d

    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 12
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v11, -0x1

    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 149
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 156
    invoke-static {p0}, Lcom/google/android/music/cast/CastUtils;->showMrpVolumeDialog(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-static {p0}, Lcom/google/android/music/cast/CastUtils;->isDefaultRouteSelected(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 157
    :cond_0
    const/4 v6, 0x3

    invoke-virtual {p0, v6}, Lcom/google/android/music/activities/CreateMixActivity;->setVolumeControlStream(I)V

    .line 161
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/music/activities/CreateMixActivity;->getIntent()Landroid/content/Intent;

    move-result-object v6

    const-string v9, "mixType"

    invoke-virtual {v6, v9, v11}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 162
    .local v2, "mixType":I
    invoke-virtual {p0}, Lcom/google/android/music/activities/CreateMixActivity;->getIntent()Landroid/content/Intent;

    move-result-object v6

    const-string v9, "name"

    invoke-virtual {v6, v9}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 164
    .local v3, "stationTitle":Ljava/lang/String;
    if-nez v3, :cond_2

    .line 165
    sget-object v6, Lcom/google/android/music/mix/MixDescriptor$Type;->LUCKY_RADIO:Lcom/google/android/music/mix/MixDescriptor$Type;

    invoke-virtual {v6}, Lcom/google/android/music/mix/MixDescriptor$Type;->ordinal()I

    move-result v6

    if-ne v2, v6, :cond_3

    .line 166
    const v6, 0x7f0b02ed

    invoke-virtual {p0, v6}, Lcom/google/android/music/activities/CreateMixActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 173
    :cond_2
    :goto_0
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/music/preferences/MusicPreferences;->isNautilusEnabled()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 174
    const v6, 0x7f0b01d1

    new-array v9, v7, [Ljava/lang/Object;

    aput-object v3, v9, v8

    invoke-virtual {p0, v6, v9}, Lcom/google/android/music/activities/CreateMixActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/google/android/music/activities/CreateMixActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 180
    :goto_1
    invoke-virtual {p0, v8}, Lcom/google/android/music/activities/CreateMixActivity;->setResult(I)V

    .line 182
    const v6, 0x7f040028

    invoke-virtual {p0, v6}, Lcom/google/android/music/activities/CreateMixActivity;->setContentView(I)V

    .line 184
    const v6, 0x7f0e007d

    invoke-virtual {p0, v6}, Lcom/google/android/music/activities/CreateMixActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/google/android/music/activities/CreateMixActivity;->mText:Landroid/widget/TextView;

    .line 185
    const v6, 0x7f0e00f9

    invoke-virtual {p0, v6}, Lcom/google/android/music/activities/CreateMixActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/music/activities/CreateMixActivity;->mDivider:Landroid/view/View;

    .line 188
    new-instance v6, Lcom/google/android/music/mix/AsyncMixCreatorWorker;

    invoke-virtual {p0}, Lcom/google/android/music/activities/CreateMixActivity;->getIntent()Landroid/content/Intent;

    move-result-object v9

    iget-object v10, p0, Lcom/google/android/music/activities/CreateMixActivity;->mMixCreationCallbacks:Lcom/google/android/music/mix/AsyncMixCreatorWorker$MixCreationCallbacks;

    invoke-direct {v6, p0, v9, v10}, Lcom/google/android/music/mix/AsyncMixCreatorWorker;-><init>(Landroid/content/Context;Landroid/content/Intent;Lcom/google/android/music/mix/AsyncMixCreatorWorker$MixCreationCallbacks;)V

    iput-object v6, p0, Lcom/google/android/music/activities/CreateMixActivity;->mAsyncWorker:Lcom/google/android/music/mix/AsyncMixCreatorWorker;

    .line 189
    iget-object v6, p0, Lcom/google/android/music/activities/CreateMixActivity;->mAsyncWorker:Lcom/google/android/music/mix/AsyncMixCreatorWorker;

    invoke-virtual {v6}, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->start()V

    .line 192
    invoke-virtual {p0}, Lcom/google/android/music/activities/CreateMixActivity;->getIntent()Landroid/content/Intent;

    move-result-object v6

    const-string v9, "mixType"

    invoke-virtual {v6, v9, v11}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    sget-object v9, Lcom/google/android/music/mix/MixDescriptor$Type;->LUCKY_RADIO:Lcom/google/android/music/mix/MixDescriptor$Type;

    invoke-virtual {v9}, Lcom/google/android/music/mix/MixDescriptor$Type;->ordinal()I

    move-result v9

    if-ne v6, v9, :cond_5

    move v6, v7

    :goto_2
    iput-boolean v6, p0, Lcom/google/android/music/activities/CreateMixActivity;->mIflRadio:Z

    .line 198
    invoke-virtual {p0}, Lcom/google/android/music/activities/CreateMixActivity;->getWindow()Landroid/view/Window;

    move-result-object v6

    const/4 v9, -0x3

    invoke-virtual {v6, v9}, Landroid/view/Window;->setFormat(I)V

    .line 199
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    .line 200
    .local v0, "builder":Landroid/net/Uri$Builder;
    const-string v6, "android.resource"

    invoke-virtual {v0, v6}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 201
    invoke-virtual {p0}, Lcom/google/android/music/activities/CreateMixActivity;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 202
    iget-boolean v6, p0, Lcom/google/android/music/activities/CreateMixActivity;->mIflRadio:Z

    if-eqz v6, :cond_6

    const/high16 v4, 0x7f090000

    .line 203
    .local v4, "videoId":I
    :goto_3
    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 204
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v5

    .line 205
    .local v5, "videoUri":Landroid/net/Uri;
    new-instance v6, Landroid/widget/MediaController;

    invoke-direct {v6, p0}, Landroid/widget/MediaController;-><init>(Landroid/content/Context;)V

    iput-object v6, p0, Lcom/google/android/music/activities/CreateMixActivity;->mMediaController:Landroid/widget/MediaController;

    .line 206
    const v6, 0x7f0e00fc

    invoke-virtual {p0, v6}, Lcom/google/android/music/activities/CreateMixActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/VideoView;

    iput-object v6, p0, Lcom/google/android/music/activities/CreateMixActivity;->mVideoView:Landroid/widget/VideoView;

    .line 207
    iget-object v6, p0, Lcom/google/android/music/activities/CreateMixActivity;->mVideoView:Landroid/widget/VideoView;

    iget-object v9, p0, Lcom/google/android/music/activities/CreateMixActivity;->mMediaController:Landroid/widget/MediaController;

    invoke-virtual {v6, v9}, Landroid/widget/VideoView;->setMediaController(Landroid/widget/MediaController;)V

    .line 208
    iget-object v6, p0, Lcom/google/android/music/activities/CreateMixActivity;->mVideoView:Landroid/widget/VideoView;

    iget-object v9, p0, Lcom/google/android/music/activities/CreateMixActivity;->mOnPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    invoke-virtual {v6, v9}, Landroid/widget/VideoView;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 209
    iget-object v6, p0, Lcom/google/android/music/activities/CreateMixActivity;->mVideoView:Landroid/widget/VideoView;

    iget-object v9, p0, Lcom/google/android/music/activities/CreateMixActivity;->mOnCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    invoke-virtual {v6, v9}, Landroid/widget/VideoView;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 210
    iget-object v6, p0, Lcom/google/android/music/activities/CreateMixActivity;->mVideoView:Landroid/widget/VideoView;

    iget-object v9, p0, Lcom/google/android/music/activities/CreateMixActivity;->mOnErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    invoke-virtual {v6, v9}, Landroid/widget/VideoView;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 211
    iget-object v6, p0, Lcom/google/android/music/activities/CreateMixActivity;->mVideoView:Landroid/widget/VideoView;

    invoke-virtual {v6, v5}, Landroid/widget/VideoView;->setVideoURI(Landroid/net/Uri;)V

    .line 216
    iget-object v6, p0, Lcom/google/android/music/activities/CreateMixActivity;->mVideoView:Landroid/widget/VideoView;

    invoke-virtual {v6, v7}, Landroid/widget/VideoView;->setZOrderOnTop(Z)V

    .line 217
    iget-boolean v6, p0, Lcom/google/android/music/activities/CreateMixActivity;->mIflRadio:Z

    if-eqz v6, :cond_7

    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/music/preferences/MusicPreferences;->isRadioAnimationEnabled()Z

    move-result v6

    if-eqz v6, :cond_7

    move v1, v7

    .line 219
    .local v1, "isVideoEnabled":Z
    :goto_4
    if-eqz v1, :cond_8

    .line 220
    iget-object v6, p0, Lcom/google/android/music/activities/CreateMixActivity;->mVideoView:Landroid/widget/VideoView;

    invoke-virtual {v6}, Landroid/widget/VideoView;->start()V

    .line 224
    :goto_5
    return-void

    .line 168
    .end local v0    # "builder":Landroid/net/Uri$Builder;
    .end local v1    # "isVideoEnabled":Z
    .end local v4    # "videoId":I
    .end local v5    # "videoUri":Landroid/net/Uri;
    :cond_3
    const-string v3, ""

    goto/16 :goto_0

    .line 176
    :cond_4
    const v6, 0x7f0b01d3

    new-array v9, v7, [Ljava/lang/Object;

    aput-object v3, v9, v8

    invoke-virtual {p0, v6, v9}, Lcom/google/android/music/activities/CreateMixActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/google/android/music/activities/CreateMixActivity;->setTitle(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    :cond_5
    move v6, v8

    .line 192
    goto/16 :goto_2

    .line 202
    .restart local v0    # "builder":Landroid/net/Uri$Builder;
    :cond_6
    const v4, 0x7f090001

    goto :goto_3

    .restart local v4    # "videoId":I
    .restart local v5    # "videoUri":Landroid/net/Uri;
    :cond_7
    move v1, v8

    .line 217
    goto :goto_4

    .line 222
    .restart local v1    # "isVideoEnabled":Z
    :cond_8
    invoke-direct {p0}, Lcom/google/android/music/activities/CreateMixActivity;->showFallbackImage()V

    goto :goto_5
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    .line 254
    iget-boolean v1, p0, Lcom/google/android/music/activities/CreateMixActivity;->mMixCreated:Z

    if-nez v1, :cond_0

    .line 256
    :try_start_0
    sget-object v1, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    if-eqz v1, :cond_0

    .line 257
    sget-object v1, Lcom/google/android/music/utils/MusicUtils;->sService:Lcom/google/android/music/playback/IMusicPlaybackService;

    invoke-interface {v1}, Lcom/google/android/music/playback/IMusicPlaybackService;->cancelMix()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 263
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/google/android/music/activities/CreateMixActivity;->mAsyncWorker:Lcom/google/android/music/mix/AsyncMixCreatorWorker;

    invoke-virtual {v1}, Lcom/google/android/music/mix/AsyncMixCreatorWorker;->quit()V

    .line 264
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 265
    return-void

    .line 259
    :catch_0
    move-exception v0

    .line 260
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "CreateMixActivity"

    const-string v2, "MusicPlaybackService could not cancel the mix"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 242
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 243
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/ui/UIStateManager;->onPause()V

    .line 244
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 236
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 237
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/ui/UIStateManager;->onResume()V

    .line 238
    return-void
.end method
