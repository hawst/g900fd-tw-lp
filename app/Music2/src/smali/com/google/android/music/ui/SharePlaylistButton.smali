.class public Lcom/google/android/music/ui/SharePlaylistButton;
.super Lcom/google/android/music/ui/BaseActionButton;
.source "SharePlaylistButton.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 18
    const v0, 0x7f0b0238

    const v1, 0x7f020153

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/android/music/ui/BaseActionButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 19
    return-void
.end method


# virtual methods
.method protected handleAction(Landroid/content/Context;Lcom/google/android/music/medialist/MediaList;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "medialist"    # Lcom/google/android/music/medialist/MediaList;

    .prologue
    .line 24
    invoke-virtual {p0}, Lcom/google/android/music/ui/SharePlaylistButton;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "Not implemented yet."

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 25
    return-void
.end method
