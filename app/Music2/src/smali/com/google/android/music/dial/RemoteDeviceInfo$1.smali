.class final Lcom/google/android/music/dial/RemoteDeviceInfo$1;
.super Ljava/lang/Object;
.source "RemoteDeviceInfo.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/dial/RemoteDeviceInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/android/music/dial/RemoteDeviceInfo;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 206
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/google/android/music/dial/RemoteDeviceInfo;
    .locals 5
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    const/4 v3, 0x1

    .line 210
    new-instance v2, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;

    invoke-direct {v2}, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->setDeviceId(Ljava/lang/String;)Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->setUniqueDeviceName(Ljava/lang/String;)Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->setDeviceType(Ljava/lang/String;)Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->setFriendlyName(Ljava/lang/String;)Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->setManufacturer(Ljava/lang/String;)Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;

    move-result-object v4

    const-class v2, Landroid/net/Uri;

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/net/Uri;

    invoke-virtual {v4, v2}, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->setManufacturerUrl(Landroid/net/Uri;)Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->setModelDescription(Ljava/lang/String;)Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->setModelName(Ljava/lang/String;)Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->setModelNumber(Ljava/lang/String;)Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->setSerialNumber(Ljava/lang/String;)Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;

    move-result-object v4

    const-class v2, Landroid/net/Uri;

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/net/Uri;

    invoke-virtual {v4, v2}, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->setApplicationUrl(Landroid/net/Uri;)Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->setGroupId(Ljava/lang/String;)Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->setGroupName(Ljava/lang/String;)Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;

    move-result-object v4

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-ne v2, v3, :cond_0

    move v2, v3

    :goto_0
    invoke-virtual {v4, v2}, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->setIsGroupCoordinator(Z)Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->setHouseholdId(Ljava/lang/String;)Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;

    move-result-object v0

    .line 227
    .local v0, "builder":Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v1

    .line 228
    .local v1, "remoteServices":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/dial/RemoteService;>;"
    const-class v2, Lcom/google/android/music/dial/RemoteService;

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    .line 229
    invoke-virtual {v0, v1}, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->setRemoteServices(Ljava/util/List;)Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;

    .line 230
    invoke-virtual {v0}, Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;->build()Lcom/google/android/music/dial/RemoteDeviceInfo;

    move-result-object v2

    return-object v2

    .line 210
    .end local v0    # "builder":Lcom/google/android/music/dial/RemoteDeviceInfo$Builder;
    .end local v1    # "remoteServices":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/dial/RemoteService;>;"
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Landroid/os/Parcel;

    .prologue
    .line 206
    invoke-virtual {p0, p1}, Lcom/google/android/music/dial/RemoteDeviceInfo$1;->createFromParcel(Landroid/os/Parcel;)Lcom/google/android/music/dial/RemoteDeviceInfo;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/google/android/music/dial/RemoteDeviceInfo;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 235
    new-array v0, p1, [Lcom/google/android/music/dial/RemoteDeviceInfo;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 206
    invoke-virtual {p0, p1}, Lcom/google/android/music/dial/RemoteDeviceInfo$1;->newArray(I)[Lcom/google/android/music/dial/RemoteDeviceInfo;

    move-result-object v0

    return-object v0
.end method
