.class public abstract Lcom/google/android/music/medialist/NautilusSongList;
.super Lcom/google/android/music/medialist/ExternalSongList;
.source "NautilusSongList.java"

# interfaces
.implements Lcom/google/android/music/medialist/NautilusMediaList;


# static fields
.field protected static final TAG:Ljava/lang/String; = "NautilusSongList"


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 22
    sget-object v0, Lcom/google/android/music/download/ContentIdentifier$Domain;->NAUTILUS:Lcom/google/android/music/download/ContentIdentifier$Domain;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/music/medialist/ExternalSongList;-><init>(Lcom/google/android/music/download/ContentIdentifier$Domain;Z)V

    .line 24
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/music/medialist/NautilusSongList;->setFlag(I)V

    .line 25
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/android/music/medialist/NautilusSongList;->setFlag(I)V

    .line 26
    return-void
.end method


# virtual methods
.method public appendToPlaylist(Landroid/content/Context;J)I
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "playlistId"    # J

    .prologue
    const/4 v3, 0x0

    .line 78
    invoke-virtual {p0, p1, v3}, Lcom/google/android/music/medialist/NautilusSongList;->addToStore(Landroid/content/Context;Z)[J

    move-result-object v1

    .line 79
    .local v1, "localIds":[J
    if-eqz v1, :cond_0

    array-length v4, v1

    if-lez v4, :cond_0

    .line 81
    invoke-static {}, Lcom/google/android/music/store/ContainerDescriptor;->newUnknownContainerDescriptor()Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v0

    .line 82
    .local v0, "descriptor":Lcom/google/android/music/store/ContainerDescriptor;
    new-instance v2, Lcom/google/android/music/medialist/SelectedSongList;

    invoke-direct {v2, v0, v1}, Lcom/google/android/music/medialist/SelectedSongList;-><init>(Lcom/google/android/music/store/ContainerDescriptor;[J)V

    .line 83
    .local v2, "songs":Lcom/google/android/music/medialist/SelectedSongList;
    invoke-virtual {v2, p1, p2, p3}, Lcom/google/android/music/medialist/SelectedSongList;->appendToPlaylist(Landroid/content/Context;J)I

    move-result v3

    .line 85
    .end local v0    # "descriptor":Lcom/google/android/music/store/ContainerDescriptor;
    .end local v2    # "songs":Lcom/google/android/music/medialist/SelectedSongList;
    :cond_0
    return v3
.end method

.method public containsLocalItems(Landroid/content/Context;)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 36
    const/4 v0, 0x0

    return v0
.end method

.method public containsRemoteItems(Landroid/content/Context;)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 30
    const/4 v0, 0x1

    return v0
.end method

.method public isAddToStoreSupported()Z
    .locals 1

    .prologue
    .line 67
    const/4 v0, 0x1

    return v0
.end method

.method public isAllInLibrary(Landroid/content/Context;)Z
    .locals 11
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v10, 0x1

    const/4 v3, 0x0

    const/4 v7, 0x0

    .line 44
    new-array v2, v10, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v7

    .line 47
    .local v2, "cols":[Ljava/lang/String;
    invoke-virtual {p0, p1}, Lcom/google/android/music/medialist/NautilusSongList;->getFullContentUri(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v1

    move-object v0, p1

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 48
    .local v6, "c":Landroid/database/Cursor;
    if-eqz v6, :cond_2

    .line 50
    :cond_0
    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 51
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 52
    .local v8, "localId":J
    invoke-static {v8, v9}, Lcom/google/android/music/store/ProjectionUtils;->isFauxNautilusId(J)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 59
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    move v0, v7

    .line 62
    .end local v8    # "localId":J
    :goto_0
    return v0

    .line 59
    :cond_1
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    move v0, v10

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0

    :cond_2
    move v0, v7

    .line 62
    goto :goto_0
.end method

.method public supportsAppendToPlaylist()Z
    .locals 1

    .prologue
    .line 72
    const/4 v0, 0x1

    return v0
.end method
