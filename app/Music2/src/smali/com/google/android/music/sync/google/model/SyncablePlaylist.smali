.class public Lcom/google/android/music/sync/google/model/SyncablePlaylist;
.super Lcom/google/api/client/json/GenericJson;
.source "SyncablePlaylist.java"

# interfaces
.implements Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;


# instance fields
.field public mArtUrls:Ljava/util/List;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "albumArtRef"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/cloudclient/TrackJson$ImageRef;",
            ">;"
        }
    .end annotation
.end field

.field public mClientId:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "clientId"
    .end annotation
.end field

.field public mCreationTimestamp:J
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "creationTimestamp"
    .end annotation
.end field

.field public mDescription:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "description"
    .end annotation
.end field

.field public mIsDeleted:Z
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "deleted"
    .end annotation
.end field

.field public mLastModifiedTimestamp:J
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "lastModifiedTimestamp"
    .end annotation
.end field

.field public mLocalId:J

.field public mName:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "name"
    .end annotation
.end field

.field public mOwnerName:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "ownerName"
    .end annotation
.end field

.field public mOwnerProfilePhotoUrl:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "ownerProfilePhotoUrl"
    .end annotation
.end field

.field private mPlayList:Lcom/google/android/music/store/PlayList;

.field public mRemoteId:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "id"
    .end annotation
.end field

.field public mShareToken:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "shareToken"
    .end annotation
.end field

.field public mType:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "type"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    .line 32
    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    .line 43
    iput-wide v2, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mCreationTimestamp:J

    .line 46
    iput-wide v2, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mLastModifiedTimestamp:J

    .line 58
    const-string v0, "USER_GENERATED"

    iput-object v0, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mType:Ljava/lang/String;

    .line 76
    iput-wide v2, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mLocalId:J

    return-void
.end method

.method public static parse(Lcom/google/android/music/store/PlayList;)Lcom/google/android/music/sync/google/model/SyncablePlaylist;
    .locals 1
    .param p0, "playList"    # Lcom/google/android/music/store/PlayList;

    .prologue
    .line 198
    new-instance v0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;

    invoke-direct {v0}, Lcom/google/android/music/sync/google/model/SyncablePlaylist;-><init>()V

    .line 199
    .local v0, "syncable":Lcom/google/android/music/sync/google/model/SyncablePlaylist;
    invoke-static {v0, p0}, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->populateSyncablePlaylist(Lcom/google/android/music/sync/google/model/SyncablePlaylist;Lcom/google/android/music/store/PlayList;)Lcom/google/android/music/sync/google/model/SyncablePlaylist;

    .line 200
    return-object v0
.end method

.method public static parseAndSetShareState(Lcom/google/android/music/store/PlayList;I)Lcom/google/android/music/cloudclient/PlaylistWithShareStateJson;
    .locals 2
    .param p0, "playList"    # Lcom/google/android/music/store/PlayList;
    .param p1, "shareState"    # I

    .prologue
    .line 191
    new-instance v0, Lcom/google/android/music/cloudclient/PlaylistWithShareStateJson;

    invoke-direct {v0}, Lcom/google/android/music/cloudclient/PlaylistWithShareStateJson;-><init>()V

    .line 192
    .local v0, "syncable":Lcom/google/android/music/cloudclient/PlaylistWithShareStateJson;
    invoke-static {p1}, Lcom/google/android/music/cloudclient/PlaylistWithShareStateJson;->localShareStateToRemote(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/music/cloudclient/PlaylistWithShareStateJson;->mShareState:Ljava/lang/String;

    .line 193
    invoke-static {v0, p0}, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->populateSyncablePlaylist(Lcom/google/android/music/sync/google/model/SyncablePlaylist;Lcom/google/android/music/store/PlayList;)Lcom/google/android/music/sync/google/model/SyncablePlaylist;

    .line 194
    return-object v0
.end method

.method public static parseFromJsonInputStream(Ljava/io/InputStream;)Lcom/google/android/music/sync/google/model/SyncablePlaylist;
    .locals 5
    .param p0, "in"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 120
    :try_start_0
    sget-object v2, Lcom/google/api/client/json/Json;->JSON_FACTORY:Lorg/codehaus/jackson/JsonFactory;

    invoke-virtual {v2, p0}, Lorg/codehaus/jackson/JsonFactory;->createJsonParser(Ljava/io/InputStream;)Lorg/codehaus/jackson/JsonParser;

    move-result-object v1

    .line 121
    .local v1, "parser":Lorg/codehaus/jackson/JsonParser;
    invoke-virtual {v1}, Lorg/codehaus/jackson/JsonParser;->nextToken()Lorg/codehaus/jackson/JsonToken;

    .line 122
    const-class v2, Lcom/google/android/music/sync/google/model/SyncablePlaylist;

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/google/api/client/json/Json;->parse(Lorg/codehaus/jackson/JsonParser;Ljava/lang/Class;Lcom/google/api/client/json/CustomizeJsonParser;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/sync/google/model/SyncablePlaylist;
    :try_end_0
    .catch Lorg/codehaus/jackson/JsonParseException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v2

    .line 123
    .end local v1    # "parser":Lorg/codehaus/jackson/JsonParser;
    :catch_0
    move-exception v0

    .line 124
    .local v0, "je":Lorg/codehaus/jackson/JsonParseException;
    new-instance v2, Ljava/io/IOException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to parse playlist: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lorg/codehaus/jackson/JsonParseException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method private static populateSyncablePlaylist(Lcom/google/android/music/sync/google/model/SyncablePlaylist;Lcom/google/android/music/store/PlayList;)Lcom/google/android/music/sync/google/model/SyncablePlaylist;
    .locals 8
    .param p0, "syncable"    # Lcom/google/android/music/sync/google/model/SyncablePlaylist;
    .param p1, "playList"    # Lcom/google/android/music/store/PlayList;

    .prologue
    .line 205
    iput-object p1, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mPlayList:Lcom/google/android/music/store/PlayList;

    .line 206
    invoke-virtual {p1}, Lcom/google/android/music/store/PlayList;->getSourceId()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mRemoteId:Ljava/lang/String;

    .line 207
    invoke-virtual {p1}, Lcom/google/android/music/store/PlayList;->getId()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mLocalId:J

    .line 208
    invoke-virtual {p1}, Lcom/google/android/music/store/PlayList;->getName()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_0

    .line 209
    invoke-virtual {p1}, Lcom/google/android/music/store/PlayList;->getName()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mName:Ljava/lang/String;

    .line 211
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/music/store/PlayList;->getType()I

    move-result v6

    const/4 v7, 0x1

    if-ne v6, v7, :cond_1

    .line 212
    const-string v6, "MAGIC"

    iput-object v6, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mType:Ljava/lang/String;

    .line 222
    :goto_0
    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/music/store/PlayList;->getSourceVersion()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mLastModifiedTimestamp:J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 226
    :goto_1
    invoke-virtual {p1}, Lcom/google/android/music/store/PlayList;->getShareToken()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mShareToken:Ljava/lang/String;

    .line 227
    invoke-virtual {p1}, Lcom/google/android/music/store/PlayList;->getOwnerName()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mOwnerName:Ljava/lang/String;

    .line 228
    invoke-virtual {p1}, Lcom/google/android/music/store/PlayList;->getDescription()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mDescription:Ljava/lang/String;

    .line 230
    invoke-virtual {p1}, Lcom/google/android/music/store/PlayList;->getArtworkLocation()Ljava/lang/String;

    move-result-object v1

    .line 231
    .local v1, "encodedUrls":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_5

    .line 232
    invoke-static {v1}, Lcom/google/android/music/utils/MusicUtils;->decodeStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 233
    .local v5, "urls":[Ljava/lang/String;
    array-length v6, v5

    new-array v4, v6, [Lcom/google/android/music/cloudclient/TrackJson$ImageRef;

    .line 234
    .local v4, "imageRefs":[Lcom/google/android/music/cloudclient/TrackJson$ImageRef;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_2
    array-length v6, v5

    if-ge v2, v6, :cond_4

    .line 235
    new-instance v3, Lcom/google/android/music/cloudclient/TrackJson$ImageRef;

    invoke-direct {v3}, Lcom/google/android/music/cloudclient/TrackJson$ImageRef;-><init>()V

    .line 236
    .local v3, "imageRef":Lcom/google/android/music/cloudclient/TrackJson$ImageRef;
    aget-object v6, v5, v2

    iput-object v6, v3, Lcom/google/android/music/cloudclient/TrackJson$ImageRef;->mUrl:Ljava/lang/String;

    .line 237
    aput-object v3, v4, v2

    .line 234
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 213
    .end local v1    # "encodedUrls":Ljava/lang/String;
    .end local v2    # "i":I
    .end local v3    # "imageRef":Lcom/google/android/music/cloudclient/TrackJson$ImageRef;
    .end local v4    # "imageRefs":[Lcom/google/android/music/cloudclient/TrackJson$ImageRef;
    .end local v5    # "urls":[Ljava/lang/String;
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/music/store/PlayList;->getType()I

    move-result v6

    const/16 v7, 0x47

    if-ne v6, v7, :cond_2

    .line 214
    const-string v6, "SHARED"

    iput-object v6, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mType:Ljava/lang/String;

    goto :goto_0

    .line 215
    :cond_2
    invoke-virtual {p1}, Lcom/google/android/music/store/PlayList;->getType()I

    move-result v6

    const/16 v7, 0x50

    if-ne v6, v7, :cond_3

    .line 216
    const-string v6, "AUTO"

    iput-object v6, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mType:Ljava/lang/String;

    goto :goto_0

    .line 218
    :cond_3
    const-string v6, "USER_GENERATED"

    iput-object v6, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mType:Ljava/lang/String;

    goto :goto_0

    .line 223
    :catch_0
    move-exception v0

    .line 224
    .local v0, "e":Ljava/lang/NumberFormatException;
    const-wide/16 v6, 0x0

    iput-wide v6, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mLastModifiedTimestamp:J

    goto :goto_1

    .line 239
    .end local v0    # "e":Ljava/lang/NumberFormatException;
    .restart local v1    # "encodedUrls":Ljava/lang/String;
    .restart local v2    # "i":I
    .restart local v4    # "imageRefs":[Lcom/google/android/music/cloudclient/TrackJson$ImageRef;
    .restart local v5    # "urls":[Ljava/lang/String;
    :cond_4
    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mArtUrls:Ljava/util/List;

    .line 241
    .end local v2    # "i":I
    .end local v4    # "imageRefs":[Lcom/google/android/music/cloudclient/TrackJson$ImageRef;
    .end local v5    # "urls":[Ljava/lang/String;
    :cond_5
    invoke-virtual {p1}, Lcom/google/android/music/store/PlayList;->getOwnerProfilePhotoUrl()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mOwnerProfilePhotoUrl:Ljava/lang/String;

    .line 243
    return-object p0
.end method


# virtual methods
.method public formatAsPlayList(Lcom/google/android/music/store/PlayList;)Lcom/google/android/music/store/PlayList;
    .locals 4
    .param p1, "playlist"    # Lcom/google/android/music/store/PlayList;

    .prologue
    .line 129
    if-nez p1, :cond_0

    .line 130
    new-instance p1, Lcom/google/android/music/store/PlayList;

    .end local p1    # "playlist":Lcom/google/android/music/store/PlayList;
    invoke-direct {p1}, Lcom/google/android/music/store/PlayList;-><init>()V

    .line 132
    .restart local p1    # "playlist":Lcom/google/android/music/store/PlayList;
    :cond_0
    iget-object v2, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mRemoteId:Ljava/lang/String;

    invoke-virtual {p1, v2}, Lcom/google/android/music/store/PlayList;->setSourceId(Ljava/lang/String;)V

    .line 133
    iget-object v2, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mName:Ljava/lang/String;

    invoke-virtual {p1, v2}, Lcom/google/android/music/store/PlayList;->setName(Ljava/lang/String;)V

    .line 134
    iget-wide v2, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mLastModifiedTimestamp:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/google/android/music/store/PlayList;->setSourceVersion(Ljava/lang/String;)V

    .line 135
    const-string v2, "MAGIC"

    iget-object v3, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mType:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 136
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, Lcom/google/android/music/store/PlayList;->setType(I)V

    .line 144
    :goto_0
    iget-object v2, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mShareToken:Ljava/lang/String;

    invoke-virtual {p1, v2}, Lcom/google/android/music/store/PlayList;->setShareToken(Ljava/lang/String;)V

    .line 145
    iget-object v2, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mOwnerName:Ljava/lang/String;

    invoke-virtual {p1, v2}, Lcom/google/android/music/store/PlayList;->setOwnerName(Ljava/lang/String;)V

    .line 146
    iget-object v2, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mDescription:Ljava/lang/String;

    invoke-virtual {p1, v2}, Lcom/google/android/music/store/PlayList;->setDescription(Ljava/lang/String;)V

    .line 147
    iget-object v2, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mArtUrls:Ljava/util/List;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mArtUrls:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_5

    .line 148
    iget-object v2, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mArtUrls:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    new-array v1, v2, [Ljava/lang/String;

    .line 149
    .local v1, "urls":[Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    array-length v2, v1

    if-ge v0, v2, :cond_4

    .line 150
    iget-object v2, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mArtUrls:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/music/cloudclient/TrackJson$ImageRef;

    iget-object v2, v2, Lcom/google/android/music/cloudclient/TrackJson$ImageRef;->mUrl:Ljava/lang/String;

    aput-object v2, v1, v0

    .line 149
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 137
    .end local v0    # "i":I
    .end local v1    # "urls":[Ljava/lang/String;
    :cond_1
    const-string v2, "SHARED"

    iget-object v3, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mType:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 138
    const/16 v2, 0x47

    invoke-virtual {p1, v2}, Lcom/google/android/music/store/PlayList;->setType(I)V

    goto :goto_0

    .line 139
    :cond_2
    const-string v2, "AUTO"

    iget-object v3, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mType:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 140
    const/16 v2, 0x50

    invoke-virtual {p1, v2}, Lcom/google/android/music/store/PlayList;->setType(I)V

    goto :goto_0

    .line 142
    :cond_3
    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Lcom/google/android/music/store/PlayList;->setType(I)V

    goto :goto_0

    .line 152
    .restart local v0    # "i":I
    .restart local v1    # "urls":[Ljava/lang/String;
    :cond_4
    invoke-static {v1}, Lcom/google/android/music/utils/MusicUtils;->encodeStringArray([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/google/android/music/store/PlayList;->setArtworkLocation(Ljava/lang/String;)V

    .line 154
    .end local v0    # "i":I
    .end local v1    # "urls":[Ljava/lang/String;
    :cond_5
    iget-object v2, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mOwnerProfilePhotoUrl:Ljava/lang/String;

    invoke-virtual {p1, v2}, Lcom/google/android/music/store/PlayList;->setOwnerProfilePhotoUrl(Ljava/lang/String;)V

    .line 156
    return-object p1
.end method

.method public getBatchMutationUrl(Landroid/content/Context;)Lcom/google/android/music/sync/api/MusicUrl;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 278
    invoke-static {}, Lcom/google/android/music/sync/api/MusicUrl;->forPlaylistsBatchMutation()Lcom/google/android/music/sync/api/MusicUrl;

    move-result-object v0

    return-object v0
.end method

.method public getCreationTimestamp()J
    .locals 2

    .prologue
    .line 302
    iget-wide v0, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mCreationTimestamp:J

    return-wide v0
.end method

.method public getEncapsulatedPlayList()Lcom/google/android/music/store/PlayList;
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mPlayList:Lcom/google/android/music/store/PlayList;

    return-object v0
.end method

.method public getFeedUrl(Landroid/content/Context;)Lcom/google/android/music/sync/api/MusicUrl;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 270
    invoke-static {}, Lcom/google/android/music/sync/api/MusicUrl;->forPlaylistsFeed()Lcom/google/android/music/sync/api/MusicUrl;

    move-result-object v0

    return-object v0
.end method

.method public getFeedUrlAsPost(Landroid/content/Context;)Lcom/google/android/music/sync/api/MusicUrl;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 274
    invoke-static {}, Lcom/google/android/music/sync/api/MusicUrl;->forPlaylistsFeedAsPost()Lcom/google/android/music/sync/api/MusicUrl;

    move-result-object v0

    return-object v0
.end method

.method public getLastModifiedTimestamp()J
    .locals 2

    .prologue
    .line 294
    iget-wide v0, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mLastModifiedTimestamp:J

    return-wide v0
.end method

.method public getLocalId()J
    .locals 2

    .prologue
    .line 282
    iget-wide v0, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mLocalId:J

    return-wide v0
.end method

.method public getRemoteId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 286
    iget-object v0, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mRemoteId:Ljava/lang/String;

    return-object v0
.end method

.method public getUrl(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/music/sync/api/MusicUrl;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "id"    # Ljava/lang/String;

    .prologue
    .line 266
    invoke-static {p2}, Lcom/google/android/music/sync/api/MusicUrl;->forPlaylist(Ljava/lang/String;)Lcom/google/android/music/sync/api/MusicUrl;

    move-result-object v0

    return-object v0
.end method

.method public isDeleted()Z
    .locals 1

    .prologue
    .line 310
    iget-boolean v0, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mIsDeleted:Z

    return v0
.end method

.method public isInsert()Z
    .locals 1

    .prologue
    .line 318
    iget-boolean v0, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mIsDeleted:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mRemoteId:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isUpdate()Z
    .locals 1

    .prologue
    .line 322
    iget-boolean v0, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mIsDeleted:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mRemoteId:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public serializeAsJson()[B
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/store/InvalidDataException;
        }
    .end annotation

    .prologue
    .line 98
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 100
    .local v0, "byteStream":Ljava/io/ByteArrayOutputStream;
    :try_start_0
    sget-object v4, Lcom/google/api/client/json/Json;->JSON_FACTORY:Lorg/codehaus/jackson/JsonFactory;

    sget-object v5, Lorg/codehaus/jackson/JsonEncoding;->UTF8:Lorg/codehaus/jackson/JsonEncoding;

    invoke-virtual {v4, v0, v5}, Lorg/codehaus/jackson/JsonFactory;->createJsonGenerator(Ljava/io/OutputStream;Lorg/codehaus/jackson/JsonEncoding;)Lorg/codehaus/jackson/JsonGenerator;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 103
    .local v2, "generator":Lorg/codehaus/jackson/JsonGenerator;
    :try_start_1
    invoke-static {v2, p0}, Lcom/google/api/client/json/Json;->serialize(Lorg/codehaus/jackson/JsonGenerator;Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 105
    :try_start_2
    invoke-virtual {v2}, Lorg/codehaus/jackson/JsonGenerator;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 111
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    .line 112
    .local v3, "serialization":[B
    const-string v4, "MusicSyncAdapter"

    const/4 v5, 0x2

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 113
    const-string v4, "MusicSyncAdapter"

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    :cond_0
    return-object v3

    .line 105
    .end local v3    # "serialization":[B
    :catchall_0
    move-exception v4

    :try_start_3
    invoke-virtual {v2}, Lorg/codehaus/jackson/JsonGenerator;->close()V

    throw v4
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    .line 107
    .end local v2    # "generator":Lorg/codehaus/jackson/JsonGenerator;
    :catch_0
    move-exception v1

    .line 108
    .local v1, "e":Ljava/io/IOException;
    const-string v4, "MusicSyncAdapter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unable to serialize a playlist as JSON: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ": "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 109
    new-instance v4, Lcom/google/android/music/store/InvalidDataException;

    const-string v5, "Unable to serialize playlist for upstream sync."

    invoke-direct {v4, v5, v1}, Lcom/google/android/music/store/InvalidDataException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4
.end method

.method public setCreationTimestamp(J)V
    .locals 1
    .param p1, "time"    # J

    .prologue
    .line 306
    iput-wide p1, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mCreationTimestamp:J

    .line 307
    return-void
.end method

.method public setIsDeleted(Z)V
    .locals 0
    .param p1, "isDeleted"    # Z

    .prologue
    .line 314
    iput-boolean p1, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mIsDeleted:Z

    .line 315
    return-void
.end method

.method public setLastModifiedTimestamp(J)V
    .locals 1
    .param p1, "time"    # J

    .prologue
    .line 298
    iput-wide p1, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mLastModifiedTimestamp:J

    .line 299
    return-void
.end method

.method public setRemoteId(Ljava/lang/String;)V
    .locals 0
    .param p1, "remoteId"    # Ljava/lang/String;

    .prologue
    .line 290
    iput-object p1, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mRemoteId:Ljava/lang/String;

    .line 291
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 248
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 249
    .local v0, "s":Ljava/lang/StringBuffer;
    const-string v1, "; localid:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mLocalId:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "; remoteid:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mRemoteId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "; ctime:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mCreationTimestamp:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "; mtime:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mLastModifiedTimestamp:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "; isDeleted: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mIsDeleted:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "; name: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "; clientId: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mClientId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "; type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "; shareToken: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mShareToken:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "; ownerName: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mOwnerName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "; description: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mDescription:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "; artUrls: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mArtUrls:Ljava/util/List;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "; ownerProfilePhotoUrl: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mOwnerProfilePhotoUrl:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 262
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public validateForUpstreamDelete()V
    .locals 2

    .prologue
    .line 176
    iget-object v0, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mRemoteId:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 177
    new-instance v0, Lcom/google/android/music/store/InvalidDataException;

    const-string v1, "Invalid playlist for upstream delete."

    invoke-direct {v0, v1}, Lcom/google/android/music/store/InvalidDataException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 179
    :cond_0
    return-void
.end method

.method public validateForUpstreamInsert()V
    .locals 2

    .prologue
    .line 164
    iget-object v0, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mName:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 165
    new-instance v0, Lcom/google/android/music/store/InvalidDataException;

    const-string v1, "Invalid playlist for upstream insert."

    invoke-direct {v0, v1}, Lcom/google/android/music/store/InvalidDataException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 167
    :cond_0
    return-void
.end method

.method public validateForUpstreamUpdate()V
    .locals 2

    .prologue
    .line 170
    iget-object v0, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mName:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mRemoteId:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 171
    :cond_0
    new-instance v0, Lcom/google/android/music/store/InvalidDataException;

    const-string v1, "Invalid playlist for upstream update."

    invoke-direct {v0, v1}, Lcom/google/android/music/store/InvalidDataException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 173
    :cond_1
    return-void
.end method

.method public wipeAllFields()V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    const/4 v1, 0x0

    .line 81
    iput-object v1, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mRemoteId:Ljava/lang/String;

    .line 82
    iput-wide v2, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mCreationTimestamp:J

    .line 83
    iput-wide v2, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mLastModifiedTimestamp:J

    .line 84
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mIsDeleted:Z

    .line 85
    iput-object v1, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mName:Ljava/lang/String;

    .line 86
    iput-object v1, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mPlayList:Lcom/google/android/music/store/PlayList;

    .line 87
    iput-wide v2, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mLocalId:J

    .line 88
    iput-object v1, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mClientId:Ljava/lang/String;

    .line 89
    const-string v0, "USER_GENERATED"

    iput-object v0, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mType:Ljava/lang/String;

    .line 90
    iput-object v1, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mShareToken:Ljava/lang/String;

    .line 91
    iput-object v1, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mOwnerName:Ljava/lang/String;

    .line 92
    iput-object v1, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mDescription:Ljava/lang/String;

    .line 93
    iput-object v1, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mArtUrls:Ljava/util/List;

    .line 94
    iput-object v1, p0, Lcom/google/android/music/sync/google/model/SyncablePlaylist;->mOwnerProfilePhotoUrl:Ljava/lang/String;

    .line 95
    return-void
.end method
