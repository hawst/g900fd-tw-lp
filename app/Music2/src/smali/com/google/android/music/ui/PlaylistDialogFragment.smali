.class public Lcom/google/android/music/ui/PlaylistDialogFragment;
.super Landroid/support/v4/app/DialogFragment;
.source "PlaylistDialogFragment.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/ui/PlaylistDialogFragment$HeaderListAdapter;,
        Lcom/google/android/music/ui/PlaylistDialogFragment$PlayListInfo;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v4/app/DialogFragment;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# static fields
.field private static final SECTION_TITLES:[I


# instance fields
.field private mAdapter:Lcom/google/android/music/ui/MultiAdaptersListAdapter;

.field private final mExcludeFollowedPlaylists:Z

.field private mExcludedPlaylistId:J

.field private final mItemLayoutId:I

.field private mListView:Landroid/widget/ListView;

.field private mOnItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

.field private mShowNewPlaylist:Z

.field private final mTitleResourceId:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 57
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/music/ui/PlaylistDialogFragment;->SECTION_TITLES:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x7f0b00d0
        0x7f0b022b
        0x7f0b022c
    .end array-data
.end method

.method public constructor <init>(IZ)V
    .locals 1
    .param p1, "titleId"    # I
    .param p2, "excludeFollowedPlaylists"    # Z

    .prologue
    .line 252
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    .line 63
    const v0, 0x7f040019

    iput v0, p0, Lcom/google/android/music/ui/PlaylistDialogFragment;->mItemLayoutId:I

    .line 71
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/music/ui/PlaylistDialogFragment;->mShowNewPlaylist:Z

    .line 253
    iput p1, p0, Lcom/google/android/music/ui/PlaylistDialogFragment;->mTitleResourceId:I

    .line 254
    iput-boolean p2, p0, Lcom/google/android/music/ui/PlaylistDialogFragment;->mExcludeFollowedPlaylists:Z

    .line 255
    return-void
.end method

.method public static createArgs(Lcom/google/android/music/medialist/SongList;J)Landroid/os/Bundle;
    .locals 3
    .param p0, "songlist"    # Lcom/google/android/music/medialist/SongList;
    .param p1, "excludedPlaylistId"    # J

    .prologue
    .line 77
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 78
    .local v0, "result":Landroid/os/Bundle;
    const-string v1, "songList"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 79
    const-string v1, "excludePlaylist"

    invoke-virtual {v0, v1, p1, p2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 80
    return-object v0
.end method

.method private startLoading()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 192
    invoke-virtual {p0}, Lcom/google/android/music/ui/PlaylistDialogFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v1

    .line 193
    .local v1, "lm":Landroid/support/v4/app/LoaderManager;
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    const/4 v2, 0x3

    if-ge v0, v2, :cond_1

    .line 194
    invoke-virtual {v1, v0}, Landroid/support/v4/app/LoaderManager;->getLoader(I)Landroid/support/v4/content/Loader;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 195
    invoke-virtual {v1, v0, v3, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 193
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 197
    :cond_0
    invoke-virtual {v1, v0, v3, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    goto :goto_1

    .line 200
    :cond_1
    return-void
.end method


# virtual methods
.method public getAdapterItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 211
    iget-object v0, p0, Lcom/google/android/music/ui/PlaylistDialogFragment;->mAdapter:Lcom/google/android/music/ui/MultiAdaptersListAdapter;

    invoke-virtual {v0, p1}, Lcom/google/android/music/ui/MultiAdaptersListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 15
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 96
    invoke-virtual {p0}, Lcom/google/android/music/ui/PlaylistDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v10

    invoke-virtual {v10}, Landroid/support/v4/app/FragmentActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v7

    .line 97
    .local v7, "inflater":Landroid/view/LayoutInflater;
    const v10, 0x7f040017

    const/4 v11, 0x0

    invoke-virtual {v7, v10, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 98
    .local v4, "childView":Landroid/view/View;
    const v10, 0x102000a

    invoke-virtual {v4, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/ListView;

    iput-object v10, p0, Lcom/google/android/music/ui/PlaylistDialogFragment;->mListView:Landroid/widget/ListView;

    .line 99
    iget-object v10, p0, Lcom/google/android/music/ui/PlaylistDialogFragment;->mListView:Landroid/widget/ListView;

    iget-object v11, p0, Lcom/google/android/music/ui/PlaylistDialogFragment;->mOnItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v10, v11}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 101
    invoke-virtual {p0}, Lcom/google/android/music/ui/PlaylistDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    .line 102
    .local v2, "args":Landroid/os/Bundle;
    if-eqz v2, :cond_0

    .line 103
    const-string v10, "excludePlaylist"

    const-wide/16 v12, -0x1

    invoke-virtual {v2, v10, v12, v13}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v10

    iput-wide v10, p0, Lcom/google/android/music/ui/PlaylistDialogFragment;->mExcludedPlaylistId:J

    .line 106
    :cond_0
    new-instance v10, Lcom/google/android/music/ui/MultiAdaptersListAdapter;

    const/4 v11, 0x3

    invoke-direct {v10, v11}, Lcom/google/android/music/ui/MultiAdaptersListAdapter;-><init>(I)V

    iput-object v10, p0, Lcom/google/android/music/ui/PlaylistDialogFragment;->mAdapter:Lcom/google/android/music/ui/MultiAdaptersListAdapter;

    .line 107
    const/4 v6, 0x0

    .local v6, "ii":I
    :goto_0
    const/4 v10, 0x3

    if-ge v6, v10, :cond_1

    .line 108
    iget-object v10, p0, Lcom/google/android/music/ui/PlaylistDialogFragment;->mAdapter:Lcom/google/android/music/ui/MultiAdaptersListAdapter;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Lcom/google/android/music/ui/MultiAdaptersListAdapter;->addSection(Lcom/google/android/music/ui/MultiAdaptersListAdapter$HeaderAdapter;)V

    .line 107
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 112
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/music/ui/PlaylistDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    .line 113
    .local v5, "context":Landroid/content/Context;
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 114
    .local v8, "oneItemlist":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/ui/PlaylistDialogFragment$PlayListInfo;>;"
    iget-boolean v10, p0, Lcom/google/android/music/ui/PlaylistDialogFragment;->mShowNewPlaylist:Z

    if-eqz v10, :cond_2

    .line 115
    new-instance v10, Lcom/google/android/music/ui/PlaylistDialogFragment$PlayListInfo;

    const-wide/16 v12, -0x1

    sget-object v11, Lcom/google/android/music/ui/PlaylistDialogFragment;->SECTION_TITLES:[I

    const/4 v14, 0x0

    aget v11, v11, v14

    invoke-virtual {v5, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v12, v13, v11}, Lcom/google/android/music/ui/PlaylistDialogFragment$PlayListInfo;-><init>(JLjava/lang/String;)V

    invoke-interface {v8, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 118
    :cond_2
    new-instance v1, Lcom/google/android/music/ui/PlaylistDialogFragment$HeaderListAdapter;

    const/4 v10, 0x0

    const v11, 0x7f040019

    invoke-direct {v1, v5, v10, v11, v8}, Lcom/google/android/music/ui/PlaylistDialogFragment$HeaderListAdapter;-><init>(Landroid/content/Context;Ljava/lang/String;ILjava/util/List;)V

    .line 120
    .local v1, "adapter":Lcom/google/android/music/ui/MultiAdaptersListAdapter$HeaderAdapter;
    iget-object v10, p0, Lcom/google/android/music/ui/PlaylistDialogFragment;->mAdapter:Lcom/google/android/music/ui/MultiAdaptersListAdapter;

    const/4 v11, 0x0

    invoke-virtual {v10, v11, v1}, Lcom/google/android/music/ui/MultiAdaptersListAdapter;->setSection(ILcom/google/android/music/ui/MultiAdaptersListAdapter$HeaderAdapter;)V

    .line 123
    invoke-direct {p0}, Lcom/google/android/music/ui/PlaylistDialogFragment;->startLoading()V

    .line 125
    invoke-virtual {p0}, Lcom/google/android/music/ui/PlaylistDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 126
    .local v0, "activity":Landroid/support/v4/app/FragmentActivity;
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 127
    .local v3, "builder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {p0}, Lcom/google/android/music/ui/PlaylistDialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    .line 128
    .local v9, "res":Landroid/content/res/Resources;
    iget v10, p0, Lcom/google/android/music/ui/PlaylistDialogFragment;->mTitleResourceId:I

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v10

    invoke-virtual {v3, v10}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 129
    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual {v3, v10, v11}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 130
    const/high16 v10, 0x1040000

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v10

    const/4 v11, 0x0

    invoke-virtual {v3, v10, v11}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 131
    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 132
    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v10

    return-object v10
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 7
    .param p1, "id"    # I
    .param p2, "args"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 138
    packed-switch p1, :pswitch_data_0

    .line 148
    const-string v0, "AddToPlaylistFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onCreateLoader undexpected id: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    :goto_0
    return-object v4

    .line 141
    :pswitch_0
    const/4 v0, 0x5

    iget-boolean v1, p0, Lcom/google/android/music/ui/PlaylistDialogFragment;->mExcludeFollowedPlaylists:Z

    invoke-static {v0, v1}, Lcom/google/android/music/store/MusicContent$Playlists;->getRecentPlaylistUri(IZ)Landroid/net/Uri;

    move-result-object v2

    .line 152
    .local v2, "uri":Landroid/net/Uri;
    :goto_1
    new-instance v0, Landroid/support/v4/content/CursorLoader;

    invoke-virtual {p0}, Lcom/google/android/music/ui/PlaylistDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget-object v3, Lcom/google/android/music/ui/PlaylistClustersFragment;->CURSOR_COLUMNS:[Ljava/lang/String;

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    move-object v4, v0

    goto :goto_0

    .line 145
    .end local v2    # "uri":Landroid/net/Uri;
    :pswitch_1
    iget-boolean v0, p0, Lcom/google/android/music/ui/PlaylistDialogFragment;->mExcludeFollowedPlaylists:Z

    invoke-static {v0}, Lcom/google/android/music/store/MusicContent$Playlists;->getPlaylistsUri(Z)Landroid/net/Uri;

    move-result-object v2

    .line 146
    .restart local v2    # "uri":Landroid/net/Uri;
    goto :goto_1

    .line 138
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 13
    .param p2, "data"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, "loader":Landroid/support/v4/content/Loader;, "Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    const/4 v12, 0x1

    .line 159
    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v8

    .line 160
    .local v8, "loaderId":I
    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v2

    .line 161
    .local v2, "elemCount":I
    invoke-static {v2}, Lcom/google/android/music/ui/cardlib/utils/Lists;->newArrayList(I)Ljava/util/ArrayList;

    move-result-object v7

    .line 162
    .local v7, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/music/ui/PlaylistDialogFragment$PlayListInfo;>;"
    if-ne v8, v12, :cond_1

    const/4 v9, 0x4

    .line 163
    .local v9, "maxCount":I
    :goto_0
    const/4 v1, 0x0

    .line 164
    .local v1, "addedCount":I
    :cond_0
    :goto_1
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v10

    if-eqz v10, :cond_2

    if-ge v1, v9, :cond_2

    .line 165
    const/4 v10, 0x0

    invoke-interface {p2, v10}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 167
    .local v4, "id":J
    iget-wide v10, p0, Lcom/google/android/music/ui/PlaylistDialogFragment;->mExcludedPlaylistId:J

    cmp-long v10, v4, v10

    if-eqz v10, :cond_0

    .line 168
    new-instance v6, Lcom/google/android/music/ui/PlaylistDialogFragment$PlayListInfo;

    invoke-interface {p2, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v6, v4, v5, v10}, Lcom/google/android/music/ui/PlaylistDialogFragment$PlayListInfo;-><init>(JLjava/lang/String;)V

    .line 170
    .local v6, "info":Lcom/google/android/music/ui/PlaylistDialogFragment$PlayListInfo;
    invoke-virtual {v7, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 171
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .end local v1    # "addedCount":I
    .end local v4    # "id":J
    .end local v6    # "info":Lcom/google/android/music/ui/PlaylistDialogFragment$PlayListInfo;
    .end local v9    # "maxCount":I
    :cond_1
    move v9, v2

    .line 162
    goto :goto_0

    .line 175
    .restart local v1    # "addedCount":I
    .restart local v9    # "maxCount":I
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/music/ui/PlaylistDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v10

    sget-object v11, Lcom/google/android/music/ui/PlaylistDialogFragment;->SECTION_TITLES:[I

    aget v11, v11, v8

    invoke-virtual {v10, v11}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 176
    .local v3, "header":Ljava/lang/String;
    new-instance v0, Lcom/google/android/music/ui/PlaylistDialogFragment$HeaderListAdapter;

    invoke-virtual {p0}, Lcom/google/android/music/ui/PlaylistDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v10

    const v11, 0x7f040019

    invoke-direct {v0, v10, v3, v11, v7}, Lcom/google/android/music/ui/PlaylistDialogFragment$HeaderListAdapter;-><init>(Landroid/content/Context;Ljava/lang/String;ILjava/util/List;)V

    .line 178
    .local v0, "adapter":Lcom/google/android/music/ui/MultiAdaptersListAdapter$HeaderAdapter;
    iget-object v10, p0, Lcom/google/android/music/ui/PlaylistDialogFragment;->mAdapter:Lcom/google/android/music/ui/MultiAdaptersListAdapter;

    invoke-virtual {v10, v8, v0}, Lcom/google/android/music/ui/MultiAdaptersListAdapter;->setSection(ILcom/google/android/music/ui/MultiAdaptersListAdapter$HeaderAdapter;)V

    .line 180
    iget-object v10, p0, Lcom/google/android/music/ui/PlaylistDialogFragment;->mAdapter:Lcom/google/android/music/ui/MultiAdaptersListAdapter;

    invoke-virtual {v10}, Lcom/google/android/music/ui/MultiAdaptersListAdapter;->hasAllAdapters()Z

    move-result v10

    if-eqz v10, :cond_3

    .line 181
    iget-object v10, p0, Lcom/google/android/music/ui/PlaylistDialogFragment;->mListView:Landroid/widget/ListView;

    iget-object v11, p0, Lcom/google/android/music/ui/PlaylistDialogFragment;->mAdapter:Lcom/google/android/music/ui/MultiAdaptersListAdapter;

    invoke-virtual {v10, v11}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 184
    :cond_3
    iget-object v10, p0, Lcom/google/android/music/ui/PlaylistDialogFragment;->mAdapter:Lcom/google/android/music/ui/MultiAdaptersListAdapter;

    invoke-virtual {v10}, Lcom/google/android/music/ui/MultiAdaptersListAdapter;->notifyDataSetChanged()V

    .line 185
    return-void
.end method

.method public bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Landroid/support/v4/content/Loader;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 46
    check-cast p2, Landroid/database/Cursor;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/music/ui/PlaylistDialogFragment;->onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 189
    .local p1, "arg0":Landroid/support/v4/content/Loader;, "Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    return-void
.end method

.method public setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V
    .locals 0
    .param p1, "onItemClickListener"    # Landroid/widget/AdapterView$OnItemClickListener;

    .prologue
    .line 203
    iput-object p1, p0, Lcom/google/android/music/ui/PlaylistDialogFragment;->mOnItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    .line 204
    return-void
.end method

.method public setShowNewPlaylist(Z)V
    .locals 0
    .param p1, "showNewPlaylist"    # Z

    .prologue
    .line 207
    iput-boolean p1, p0, Lcom/google/android/music/ui/PlaylistDialogFragment;->mShowNewPlaylist:Z

    .line 208
    return-void
.end method
