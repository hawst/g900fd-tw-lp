.class public final enum Lcom/google/android/music/playback/DevicePlayback$State;
.super Ljava/lang/Enum;
.source "DevicePlayback.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/playback/DevicePlayback;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "State"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/music/playback/DevicePlayback$State;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/music/playback/DevicePlayback$State;

.field public static final enum NO_PLAYLIST:Lcom/google/android/music/playback/DevicePlayback$State;

.field public static final enum PAUSED:Lcom/google/android/music/playback/DevicePlayback$State;

.field public static final enum PLAYING:Lcom/google/android/music/playback/DevicePlayback$State;

.field public static final enum SWITCHING_TRACKS:Lcom/google/android/music/playback/DevicePlayback$State;

.field public static final enum TRANSIENT_PAUSE:Lcom/google/android/music/playback/DevicePlayback$State;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 59
    new-instance v0, Lcom/google/android/music/playback/DevicePlayback$State;

    const-string v1, "NO_PLAYLIST"

    invoke-direct {v0, v1, v2}, Lcom/google/android/music/playback/DevicePlayback$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/playback/DevicePlayback$State;->NO_PLAYLIST:Lcom/google/android/music/playback/DevicePlayback$State;

    .line 60
    new-instance v0, Lcom/google/android/music/playback/DevicePlayback$State;

    const-string v1, "PAUSED"

    invoke-direct {v0, v1, v3}, Lcom/google/android/music/playback/DevicePlayback$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/playback/DevicePlayback$State;->PAUSED:Lcom/google/android/music/playback/DevicePlayback$State;

    .line 61
    new-instance v0, Lcom/google/android/music/playback/DevicePlayback$State;

    const-string v1, "TRANSIENT_PAUSE"

    invoke-direct {v0, v1, v4}, Lcom/google/android/music/playback/DevicePlayback$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/playback/DevicePlayback$State;->TRANSIENT_PAUSE:Lcom/google/android/music/playback/DevicePlayback$State;

    .line 62
    new-instance v0, Lcom/google/android/music/playback/DevicePlayback$State;

    const-string v1, "SWITCHING_TRACKS"

    invoke-direct {v0, v1, v5}, Lcom/google/android/music/playback/DevicePlayback$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/playback/DevicePlayback$State;->SWITCHING_TRACKS:Lcom/google/android/music/playback/DevicePlayback$State;

    .line 63
    new-instance v0, Lcom/google/android/music/playback/DevicePlayback$State;

    const-string v1, "PLAYING"

    invoke-direct {v0, v1, v6}, Lcom/google/android/music/playback/DevicePlayback$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/playback/DevicePlayback$State;->PLAYING:Lcom/google/android/music/playback/DevicePlayback$State;

    .line 58
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/android/music/playback/DevicePlayback$State;

    sget-object v1, Lcom/google/android/music/playback/DevicePlayback$State;->NO_PLAYLIST:Lcom/google/android/music/playback/DevicePlayback$State;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/music/playback/DevicePlayback$State;->PAUSED:Lcom/google/android/music/playback/DevicePlayback$State;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/music/playback/DevicePlayback$State;->TRANSIENT_PAUSE:Lcom/google/android/music/playback/DevicePlayback$State;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/music/playback/DevicePlayback$State;->SWITCHING_TRACKS:Lcom/google/android/music/playback/DevicePlayback$State;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/music/playback/DevicePlayback$State;->PLAYING:Lcom/google/android/music/playback/DevicePlayback$State;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/android/music/playback/DevicePlayback$State;->$VALUES:[Lcom/google/android/music/playback/DevicePlayback$State;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 58
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/music/playback/DevicePlayback$State;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 58
    const-class v0, Lcom/google/android/music/playback/DevicePlayback$State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/playback/DevicePlayback$State;

    return-object v0
.end method

.method public static values()[Lcom/google/android/music/playback/DevicePlayback$State;
    .locals 1

    .prologue
    .line 58
    sget-object v0, Lcom/google/android/music/playback/DevicePlayback$State;->$VALUES:[Lcom/google/android/music/playback/DevicePlayback$State;

    invoke-virtual {v0}, [Lcom/google/android/music/playback/DevicePlayback$State;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/music/playback/DevicePlayback$State;

    return-object v0
.end method


# virtual methods
.method playingOrWillPlay()Z
    .locals 2

    .prologue
    .line 66
    sget-object v0, Lcom/google/android/music/playback/DevicePlayback$1;->$SwitchMap$com$google$android$music$playback$DevicePlayback$State:[I

    invoke-virtual {p0}, Lcom/google/android/music/playback/DevicePlayback$State;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 72
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 70
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 66
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
