.class Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard$1;
.super Ljava/lang/Object;
.source "MaterialMainstagePlayCard.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->bind(Lcom/google/android/music/ui/cardlib/model/Document;Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;)V
    .locals 0

    .prologue
    .line 348
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard$1;->this$0:Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 352
    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 353
    .local v1, "options":Landroid/graphics/BitmapFactory$Options;
    const/16 v2, 0x80

    iput v2, v1, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 354
    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard$1;->this$0:Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;

    invoke-virtual {v2}, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020179

    invoke-static {v2, v3, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 356
    .local v0, "art":Landroid/graphics/Bitmap;
    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard$1;->this$0:Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;

    iget-object v2, v2, Lcom/google/android/music/ui/cardlib/layout/MaterialMainstagePlayCard;->mAvatarView:Lcom/google/android/music/art/SimpleArtView;

    invoke-virtual {v2, v0}, Lcom/google/android/music/art/SimpleArtView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 357
    return-void
.end method
