.class public Lcom/google/android/music/cloudclient/AlbumJson$DescriptionAttribution;
.super Lcom/google/api/client/json/GenericJson;
.source "AlbumJson.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/cloudclient/AlbumJson;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DescriptionAttribution"
.end annotation


# instance fields
.field public mLicenseTitle:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "license_title"
    .end annotation
.end field

.field public mLicenseUrl:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "license_url"
    .end annotation
.end field

.field public mSourceTitle:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "source_title"
    .end annotation
.end field

.field public mSourceUrl:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "source_url"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    return-void
.end method
