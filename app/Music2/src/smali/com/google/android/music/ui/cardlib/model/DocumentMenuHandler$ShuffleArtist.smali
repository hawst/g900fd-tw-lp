.class Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$ShuffleArtist;
.super Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$AsyncMenuEntry;
.source "DocumentMenuHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ShuffleArtist"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;Landroid/content/res/Resources;)V
    .locals 3
    .param p2, "res"    # Landroid/content/res/Resources;

    .prologue
    .line 838
    iput-object p1, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$ShuffleArtist;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    .line 839
    const v1, 0x7f0e0033

    const v2, 0x7f0b0239

    # invokes: Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->isDownloadedOnlyMode()Z
    invoke-static {p1}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->access$300(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, p2, v1, v2, v0}, Lcom/google/android/music/ui/cardlib/PlayCardMenuHandler$AsyncMenuEntry;-><init>(Landroid/content/res/Resources;IIZ)V

    .line 841
    return-void

    .line 839
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onActionSelected()V
    .locals 4

    .prologue
    .line 849
    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$ShuffleArtist;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    # invokes: Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->isDownloadedOnlyMode()Z
    invoke-static {v2}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->access$300(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 850
    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$ShuffleArtist;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    # invokes: Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->showNotAvailableToast()V
    invoke-static {v2}, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->access$400(Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;)V

    .line 861
    :goto_0
    return-void

    .line 854
    :cond_0
    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$ShuffleArtist;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v2, v2, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    invoke-virtual {v2}, Lcom/google/android/music/ui/cardlib/model/Document;->getType()Lcom/google/android/music/ui/cardlib/model/Document$Type;

    move-result-object v2

    sget-object v3, Lcom/google/android/music/ui/cardlib/model/Document$Type;->ARTIST:Lcom/google/android/music/ui/cardlib/model/Document$Type;

    if-ne v2, v3, :cond_1

    .line 855
    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$ShuffleArtist;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v2, v2, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mDoc:Lcom/google/android/music/ui/cardlib/model/Document;

    iget-object v3, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$ShuffleArtist;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v3, v3, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mContext:Landroid/app/Activity;

    invoke-virtual {v2, v3}, Lcom/google/android/music/ui/cardlib/model/Document;->getSongList(Landroid/content/Context;)Lcom/google/android/music/medialist/SongList;

    move-result-object v1

    .line 856
    .local v1, "songlist":Lcom/google/android/music/medialist/SongList;
    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$ShuffleArtist;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v2, v2, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mContext:Landroid/app/Activity;

    invoke-static {v2}, Lcom/google/android/music/ui/UIStateManager;->getInstance(Landroid/content/Context;)Lcom/google/android/music/ui/UIStateManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    .line 857
    .local v0, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    iget-object v2, p0, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler$ShuffleArtist;->this$0:Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;

    iget-object v2, v2, Lcom/google/android/music/ui/cardlib/model/DocumentMenuHandler;->mContext:Landroid/app/Activity;

    invoke-static {v2, v1, v0}, Lcom/google/android/music/utils/MusicUtils;->playArtistShuffle(Landroid/content/Context;Lcom/google/android/music/medialist/SongList;Lcom/google/android/music/preferences/MusicPreferences;)V

    goto :goto_0

    .line 859
    .end local v0    # "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    .end local v1    # "songlist":Lcom/google/android/music/medialist/SongList;
    :cond_1
    const-string v2, "DocumentMenuHandler"

    const-string v3, "Unexpected doc type"

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
