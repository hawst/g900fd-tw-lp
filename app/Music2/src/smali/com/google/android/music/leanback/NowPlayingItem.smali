.class Lcom/google/android/music/leanback/NowPlayingItem;
.super Lcom/google/android/music/leanback/Item;
.source "NowPlayingItem.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/leanback/NowPlayingItem$Listener;
    }
.end annotation


# static fields
.field private static final LOGV:Z

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mAlbumId:J

.field private mAlbumName:Ljava/lang/String;

.field private mArtistName:Ljava/lang/String;

.field private mBackgroundImageMessageHandler:Lcom/google/android/music/leanback/BackgroundImageMessageHandler;

.field private final mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mCurrentTrackId:J

.field private mCurrentTrackName:Ljava/lang/String;

.field private mData:Landroid/os/Bundle;

.field private final mDataLock:Ljava/lang/Object;

.field private final mHandler:Landroid/os/Handler;

.field private mIsSelected:Z

.field private final mLeanbackItemActivity:Lcom/google/android/music/leanback/LeanbackItemActivity;

.field private final mListener:Lcom/google/android/music/leanback/NowPlayingItem$Listener;

.field private final mLoaderId:I

.field private final mNowPlayingBitmapGetter:Lcom/google/android/music/leanback/bitmap/NowPlayingBitmapGetter;

.field private mPlaybackServiceConnection:Landroid/content/ServiceConnection;

.field private mPlaybackServiceToken:Lcom/google/android/music/utils/MusicUtils$ServiceToken;

.field private final mRelevantKeys:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mService:Lcom/google/android/music/playback/IMusicPlaybackService;

.field private mTrackName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->LEANBACK:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/leanback/NowPlayingItem;->LOGV:Z

    .line 46
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->LEANBACK:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-virtual {v0}, Lcom/google/android/music/utils/DebugUtils$MusicTag;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/leanback/NowPlayingItem;->TAG:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Lcom/google/android/music/leanback/LeanbackItemActivity;ILcom/google/android/music/leanback/NowPlayingItem$Listener;)V
    .locals 2
    .param p1, "leanbackItemActivity"    # Lcom/google/android/music/leanback/LeanbackItemActivity;
    .param p2, "loaderId"    # I
    .param p3, "listener"    # Lcom/google/android/music/leanback/NowPlayingItem$Listener;

    .prologue
    .line 86
    invoke-direct {p0}, Lcom/google/android/music/leanback/Item;-><init>()V

    .line 60
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/leanback/NowPlayingItem;->mDataLock:Ljava/lang/Object;

    .line 73
    new-instance v0, Lcom/google/android/music/leanback/NowPlayingItem$1;

    invoke-direct {v0, p0}, Lcom/google/android/music/leanback/NowPlayingItem$1;-><init>(Lcom/google/android/music/leanback/NowPlayingItem;)V

    iput-object v0, p0, Lcom/google/android/music/leanback/NowPlayingItem;->mPlaybackServiceConnection:Landroid/content/ServiceConnection;

    .line 153
    new-instance v0, Lcom/google/android/music/leanback/NowPlayingItem$2;

    invoke-direct {v0, p0}, Lcom/google/android/music/leanback/NowPlayingItem$2;-><init>(Lcom/google/android/music/leanback/NowPlayingItem;)V

    iput-object v0, p0, Lcom/google/android/music/leanback/NowPlayingItem;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 170
    new-instance v0, Lcom/google/android/music/leanback/NowPlayingItem$3;

    invoke-direct {v0, p0}, Lcom/google/android/music/leanback/NowPlayingItem$3;-><init>(Lcom/google/android/music/leanback/NowPlayingItem;)V

    iput-object v0, p0, Lcom/google/android/music/leanback/NowPlayingItem;->mHandler:Landroid/os/Handler;

    .line 87
    iput-object p1, p0, Lcom/google/android/music/leanback/NowPlayingItem;->mLeanbackItemActivity:Lcom/google/android/music/leanback/LeanbackItemActivity;

    .line 88
    iput p2, p0, Lcom/google/android/music/leanback/NowPlayingItem;->mLoaderId:I

    .line 89
    iput-object p3, p0, Lcom/google/android/music/leanback/NowPlayingItem;->mListener:Lcom/google/android/music/leanback/NowPlayingItem$Listener;

    .line 90
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/leanback/NowPlayingItem;->mIsSelected:Z

    .line 91
    new-instance v0, Lcom/google/android/music/leanback/bitmap/NowPlayingBitmapGetter;

    invoke-direct {v0}, Lcom/google/android/music/leanback/bitmap/NowPlayingBitmapGetter;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/leanback/NowPlayingItem;->mNowPlayingBitmapGetter:Lcom/google/android/music/leanback/bitmap/NowPlayingBitmapGetter;

    .line 92
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/leanback/NowPlayingItem;->mRelevantKeys:Ljava/util/HashSet;

    .line 93
    iget-object v0, p0, Lcom/google/android/music/leanback/NowPlayingItem;->mRelevantKeys:Ljava/util/HashSet;

    const-string v1, "currentSongLoaded"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 94
    iget-object v0, p0, Lcom/google/android/music/leanback/NowPlayingItem;->mRelevantKeys:Ljava/util/HashSet;

    const-string v1, "id"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 95
    iget-object v0, p0, Lcom/google/android/music/leanback/NowPlayingItem;->mRelevantKeys:Ljava/util/HashSet;

    const-string v1, "domain"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 96
    iget-object v0, p0, Lcom/google/android/music/leanback/NowPlayingItem;->mRelevantKeys:Ljava/util/HashSet;

    const-string v1, "artist"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 97
    iget-object v0, p0, Lcom/google/android/music/leanback/NowPlayingItem;->mRelevantKeys:Ljava/util/HashSet;

    const-string v1, "album"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 98
    iget-object v0, p0, Lcom/google/android/music/leanback/NowPlayingItem;->mRelevantKeys:Ljava/util/HashSet;

    const-string v1, "albumId"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 99
    iget-object v0, p0, Lcom/google/android/music/leanback/NowPlayingItem;->mRelevantKeys:Ljava/util/HashSet;

    const-string v1, "albumArtFromService"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 100
    iget-object v0, p0, Lcom/google/android/music/leanback/NowPlayingItem;->mRelevantKeys:Ljava/util/HashSet;

    const-string v1, "externalAlbumArtUrl"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 101
    iget-object v0, p0, Lcom/google/android/music/leanback/NowPlayingItem;->mRelevantKeys:Ljava/util/HashSet;

    const-string v1, "albumArtResourceId"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 102
    iget-object v0, p0, Lcom/google/android/music/leanback/NowPlayingItem;->mRelevantKeys:Ljava/util/HashSet;

    const-string v1, "track"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 103
    return-void
.end method

.method static synthetic access$002(Lcom/google/android/music/leanback/NowPlayingItem;Lcom/google/android/music/playback/IMusicPlaybackService;)Lcom/google/android/music/playback/IMusicPlaybackService;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/leanback/NowPlayingItem;
    .param p1, "x1"    # Lcom/google/android/music/playback/IMusicPlaybackService;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/google/android/music/leanback/NowPlayingItem;->mService:Lcom/google/android/music/playback/IMusicPlaybackService;

    return-object p1
.end method

.method static synthetic access$100(Lcom/google/android/music/leanback/NowPlayingItem;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/leanback/NowPlayingItem;

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/google/android/music/leanback/NowPlayingItem;->updateTrackInfo()V

    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/music/leanback/NowPlayingItem;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/leanback/NowPlayingItem;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/music/leanback/NowPlayingItem;->mAlbumName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/google/android/music/leanback/NowPlayingItem;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/leanback/NowPlayingItem;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/google/android/music/leanback/NowPlayingItem;->mAlbumName:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/google/android/music/leanback/NowPlayingItem;)J
    .locals 2
    .param p0, "x0"    # Lcom/google/android/music/leanback/NowPlayingItem;

    .prologue
    .line 43
    iget-wide v0, p0, Lcom/google/android/music/leanback/NowPlayingItem;->mAlbumId:J

    return-wide v0
.end method

.method static synthetic access$1102(Lcom/google/android/music/leanback/NowPlayingItem;J)J
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/leanback/NowPlayingItem;
    .param p1, "x1"    # J

    .prologue
    .line 43
    iput-wide p1, p0, Lcom/google/android/music/leanback/NowPlayingItem;->mAlbumId:J

    return-wide p1
.end method

.method static synthetic access$1200(Lcom/google/android/music/leanback/NowPlayingItem;)Lcom/google/android/music/leanback/bitmap/NowPlayingBitmapGetter;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/leanback/NowPlayingItem;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/music/leanback/NowPlayingItem;->mNowPlayingBitmapGetter:Lcom/google/android/music/leanback/bitmap/NowPlayingBitmapGetter;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/google/android/music/leanback/NowPlayingItem;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/leanback/NowPlayingItem;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/music/leanback/NowPlayingItem;->mTrackName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1302(Lcom/google/android/music/leanback/NowPlayingItem;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/leanback/NowPlayingItem;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/google/android/music/leanback/NowPlayingItem;->mTrackName:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1400(Lcom/google/android/music/leanback/NowPlayingItem;JLjava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/leanback/NowPlayingItem;
    .param p1, "x1"    # J
    .param p3, "x2"    # Ljava/lang/String;

    .prologue
    .line 43
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/music/leanback/NowPlayingItem;->handleSongId(JLjava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1500(Lcom/google/android/music/leanback/NowPlayingItem;)Lcom/google/android/music/leanback/NowPlayingItem$Listener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/leanback/NowPlayingItem;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/music/leanback/NowPlayingItem;->mListener:Lcom/google/android/music/leanback/NowPlayingItem$Listener;

    return-object v0
.end method

.method static synthetic access$200()Z
    .locals 1

    .prologue
    .line 43
    sget-boolean v0, Lcom/google/android/music/leanback/NowPlayingItem;->LOGV:Z

    return v0
.end method

.method static synthetic access$300()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lcom/google/android/music/leanback/NowPlayingItem;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/music/leanback/NowPlayingItem;Landroid/content/Intent;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/leanback/NowPlayingItem;
    .param p1, "x1"    # Landroid/content/Intent;

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lcom/google/android/music/leanback/NowPlayingItem;->updateTrackInfo(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/music/leanback/NowPlayingItem;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/leanback/NowPlayingItem;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/music/leanback/NowPlayingItem;->mDataLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/music/leanback/NowPlayingItem;)Landroid/os/Bundle;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/leanback/NowPlayingItem;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/music/leanback/NowPlayingItem;->mData:Landroid/os/Bundle;

    return-object v0
.end method

.method static synthetic access$602(Lcom/google/android/music/leanback/NowPlayingItem;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/leanback/NowPlayingItem;
    .param p1, "x1"    # Landroid/os/Bundle;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/google/android/music/leanback/NowPlayingItem;->mData:Landroid/os/Bundle;

    return-object p1
.end method

.method static synthetic access$700(Lcom/google/android/music/leanback/NowPlayingItem;)Ljava/util/HashSet;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/leanback/NowPlayingItem;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/music/leanback/NowPlayingItem;->mRelevantKeys:Ljava/util/HashSet;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/music/leanback/NowPlayingItem;)Lcom/google/android/music/leanback/LeanbackItemActivity;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/leanback/NowPlayingItem;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/music/leanback/NowPlayingItem;->mLeanbackItemActivity:Lcom/google/android/music/leanback/LeanbackItemActivity;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/music/leanback/NowPlayingItem;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/leanback/NowPlayingItem;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/music/leanback/NowPlayingItem;->mArtistName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$902(Lcom/google/android/music/leanback/NowPlayingItem;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/leanback/NowPlayingItem;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/google/android/music/leanback/NowPlayingItem;->mArtistName:Ljava/lang/String;

    return-object p1
.end method

.method private handleSongId(JLjava/lang/String;)Z
    .locals 7
    .param p1, "id"    # J
    .param p3, "trackName"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 269
    iget-wide v4, p0, Lcom/google/android/music/leanback/NowPlayingItem;->mCurrentTrackId:J

    cmp-long v3, p1, v4

    if-nez v3, :cond_2

    .line 270
    if-nez p3, :cond_1

    .line 271
    iget-object v3, p0, Lcom/google/android/music/leanback/NowPlayingItem;->mCurrentTrackName:Ljava/lang/String;

    if-nez v3, :cond_2

    .line 288
    :cond_0
    :goto_0
    return v2

    .line 274
    :cond_1
    iget-object v3, p0, Lcom/google/android/music/leanback/NowPlayingItem;->mCurrentTrackName:Ljava/lang/String;

    invoke-virtual {p3, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 278
    :cond_2
    iput-wide p1, p0, Lcom/google/android/music/leanback/NowPlayingItem;->mCurrentTrackId:J

    .line 279
    iput-object p3, p0, Lcom/google/android/music/leanback/NowPlayingItem;->mCurrentTrackName:Ljava/lang/String;

    .line 280
    sget-object v2, Lcom/google/android/music/leanback/NowPlayingItem;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "New Track: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " id: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 282
    iget-boolean v2, p0, Lcom/google/android/music/leanback/NowPlayingItem;->mIsSelected:Z

    if-eqz v2, :cond_3

    .line 283
    invoke-static {p1, p2, p3}, Lcom/google/android/music/store/ContainerDescriptor;->newSingleSongDescriptor(JLjava/lang/String;)Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v0

    .line 285
    .local v0, "descriptor":Lcom/google/android/music/store/ContainerDescriptor;
    new-instance v1, Lcom/google/android/music/medialist/SingleSongList;

    invoke-direct {v1, v0, p1, p2, p3}, Lcom/google/android/music/medialist/SingleSongList;-><init>(Lcom/google/android/music/store/ContainerDescriptor;JLjava/lang/String;)V

    .line 286
    .local v1, "songList":Lcom/google/android/music/medialist/SongList;
    iget-object v2, p0, Lcom/google/android/music/leanback/NowPlayingItem;->mLeanbackItemActivity:Lcom/google/android/music/leanback/LeanbackItemActivity;

    iget v3, p0, Lcom/google/android/music/leanback/NowPlayingItem;->mLoaderId:I

    invoke-static {v2, v1, v3}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->loadArtistArtwork(Lcom/google/android/music/leanback/LeanbackItemActivity;Lcom/google/android/music/medialist/SongList;I)V

    .line 288
    .end local v0    # "descriptor":Lcom/google/android/music/store/ContainerDescriptor;
    .end local v1    # "songList":Lcom/google/android/music/medialist/SongList;
    :cond_3
    const/4 v2, 0x1

    goto :goto_0
.end method

.method private updateTrackInfo()V
    .locals 3

    .prologue
    .line 329
    iget-object v0, p0, Lcom/google/android/music/leanback/NowPlayingItem;->mService:Lcom/google/android/music/playback/IMusicPlaybackService;

    if-nez v0, :cond_0

    .line 335
    :goto_0
    return-void

    .line 333
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.music.metachanged"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/music/leanback/NowPlayingItem;->mService:Lcom/google/android/music/playback/IMusicPlaybackService;

    iget-object v2, p0, Lcom/google/android/music/leanback/NowPlayingItem;->mLeanbackItemActivity:Lcom/google/android/music/leanback/LeanbackItemActivity;

    invoke-static {v0, v1, v2}, Lcom/google/android/music/playback/MusicPlaybackService;->populateExtras(Landroid/content/Intent;Lcom/google/android/music/playback/IMusicPlaybackService;Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/music/leanback/NowPlayingItem;->updateTrackInfo(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private updateTrackInfo(Landroid/content/Intent;)V
    .locals 4
    .param p1, "sourceIntent"    # Landroid/content/Intent;

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x2

    .line 338
    iget-object v2, p0, Lcom/google/android/music/leanback/NowPlayingItem;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v3, v1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 339
    .local v0, "numsg":Landroid/os/Message;
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    :cond_0
    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 340
    iget-object v1, p0, Lcom/google/android/music/leanback/NowPlayingItem;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 341
    iget-object v1, p0, Lcom/google/android/music/leanback/NowPlayingItem;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 342
    return-void
.end method


# virtual methods
.method public cleanup()V
    .locals 2

    .prologue
    .line 320
    iget-object v0, p0, Lcom/google/android/music/leanback/NowPlayingItem;->mLeanbackItemActivity:Lcom/google/android/music/leanback/LeanbackItemActivity;

    iget-object v1, p0, Lcom/google/android/music/leanback/NowPlayingItem;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Lcom/google/android/music/leanback/LeanbackItemActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 321
    iget-object v0, p0, Lcom/google/android/music/leanback/NowPlayingItem;->mPlaybackServiceToken:Lcom/google/android/music/utils/MusicUtils$ServiceToken;

    if-eqz v0, :cond_0

    .line 322
    iget-object v0, p0, Lcom/google/android/music/leanback/NowPlayingItem;->mPlaybackServiceToken:Lcom/google/android/music/utils/MusicUtils$ServiceToken;

    invoke-static {v0}, Lcom/google/android/music/utils/MusicUtils;->unbindFromService(Lcom/google/android/music/utils/MusicUtils$ServiceToken;)V

    .line 324
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/leanback/NowPlayingItem;->mService:Lcom/google/android/music/playback/IMusicPlaybackService;

    .line 325
    iget-object v0, p0, Lcom/google/android/music/leanback/NowPlayingItem;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 326
    return-void
.end method

.method public getBitmapGettersGetter()Lcom/google/android/music/leanback/bitmap/BitmapGettersGetter;
    .locals 4

    .prologue
    .line 150
    new-instance v0, Lcom/google/android/music/leanback/bitmap/ConstantBitmapGettersGetter;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/music/leanback/bitmap/BitmapGetter;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/music/leanback/NowPlayingItem;->mNowPlayingBitmapGetter:Lcom/google/android/music/leanback/bitmap/NowPlayingBitmapGetter;

    aput-object v3, v1, v2

    invoke-direct {v0, v1}, Lcom/google/android/music/leanback/bitmap/ConstantBitmapGettersGetter;-><init>([Lcom/google/android/music/leanback/bitmap/BitmapGetter;)V

    return-object v0
.end method

.method getDescriptionGetter()Lcom/google/android/music/leanback/Item$StringGetter;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 118
    const/4 v1, 0x0

    .line 119
    .local v1, "artistName":Ljava/lang/String;
    const/4 v0, 0x0

    .line 120
    .local v0, "albumName":Ljava/lang/String;
    iget-object v3, p0, Lcom/google/android/music/leanback/NowPlayingItem;->mDataLock:Ljava/lang/Object;

    monitor-enter v3

    .line 121
    :try_start_0
    iget-object v4, p0, Lcom/google/android/music/leanback/NowPlayingItem;->mData:Landroid/os/Bundle;

    if-eqz v4, :cond_0

    .line 122
    iget-object v1, p0, Lcom/google/android/music/leanback/NowPlayingItem;->mArtistName:Ljava/lang/String;

    .line 123
    iget-object v0, p0, Lcom/google/android/music/leanback/NowPlayingItem;->mAlbumName:Ljava/lang/String;

    .line 125
    :cond_0
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 127
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 128
    if-eqz v1, :cond_1

    new-instance v2, Lcom/google/android/music/leanback/Item$ConstantStringGetter;

    invoke-direct {v2, v1}, Lcom/google/android/music/leanback/Item$ConstantStringGetter;-><init>(Ljava/lang/String;)V

    .line 132
    :cond_1
    :goto_0
    return-object v2

    .line 125
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    .line 129
    :cond_2
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 130
    if-eqz v0, :cond_1

    new-instance v2, Lcom/google/android/music/leanback/Item$ConstantStringGetter;

    invoke-direct {v2, v0}, Lcom/google/android/music/leanback/Item$ConstantStringGetter;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 132
    :cond_3
    new-instance v2, Lcom/google/android/music/leanback/Item$ConstantStringGetter;

    iget-object v3, p0, Lcom/google/android/music/leanback/NowPlayingItem;->mLeanbackItemActivity:Lcom/google/android/music/leanback/LeanbackItemActivity;

    const v4, 0x7f0b0373

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    const/4 v6, 0x1

    aput-object v1, v5, v6

    invoke-virtual {v3, v4, v5}, Lcom/google/android/music/leanback/LeanbackItemActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/android/music/leanback/Item$ConstantStringGetter;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method getIntentGetter()Lcom/google/android/music/leanback/Item$IntentGetter;
    .locals 2

    .prologue
    .line 139
    new-instance v0, Lcom/google/android/music/leanback/Item$ConstantIntentGetter;

    iget-object v1, p0, Lcom/google/android/music/leanback/NowPlayingItem;->mLeanbackItemActivity:Lcom/google/android/music/leanback/LeanbackItemActivity;

    invoke-static {v1}, Lcom/google/android/music/leanback/LeanbackUtils;->newNowPlayingPlayIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/music/leanback/Item$ConstantIntentGetter;-><init>(Landroid/content/Intent;)V

    return-object v0
.end method

.method getOverlayResourceId()I
    .locals 1

    .prologue
    .line 145
    const v0, 0x7f0200cc

    return v0
.end method

.method getTitle()Ljava/lang/String;
    .locals 3

    .prologue
    .line 107
    const/4 v0, 0x0

    .line 108
    .local v0, "trackName":Ljava/lang/String;
    iget-object v2, p0, Lcom/google/android/music/leanback/NowPlayingItem;->mDataLock:Ljava/lang/Object;

    monitor-enter v2

    .line 109
    :try_start_0
    iget-object v1, p0, Lcom/google/android/music/leanback/NowPlayingItem;->mData:Landroid/os/Bundle;

    if-eqz v1, :cond_0

    .line 110
    iget-object v0, p0, Lcom/google/android/music/leanback/NowPlayingItem;->mTrackName:Ljava/lang/String;

    .line 112
    :cond_0
    monitor-exit v2

    .line 113
    return-object v0

    .line 112
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public init(Lcom/google/android/music/leanback/BackgroundImageMessageHandler;)V
    .locals 3
    .param p1, "backgroundImageMessageHandler"    # Lcom/google/android/music/leanback/BackgroundImageMessageHandler;

    .prologue
    .line 307
    iput-object p1, p0, Lcom/google/android/music/leanback/NowPlayingItem;->mBackgroundImageMessageHandler:Lcom/google/android/music/leanback/BackgroundImageMessageHandler;

    .line 308
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 309
    .local v0, "f":Landroid/content/IntentFilter;
    const-string v1, "com.android.music.metachanged"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 310
    const-string v1, "com.android.music.asyncopencomplete"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 311
    const-string v1, "com.android.music.asyncopenstart"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 312
    const-string v1, "com.android.music.playbackfailed"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 313
    const-string v1, "com.android.music.playstatechanged"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 314
    iget-object v1, p0, Lcom/google/android/music/leanback/NowPlayingItem;->mLeanbackItemActivity:Lcom/google/android/music/leanback/LeanbackItemActivity;

    iget-object v2, p0, Lcom/google/android/music/leanback/NowPlayingItem;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Lcom/google/android/music/leanback/LeanbackItemActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 315
    iget-object v1, p0, Lcom/google/android/music/leanback/NowPlayingItem;->mLeanbackItemActivity:Lcom/google/android/music/leanback/LeanbackItemActivity;

    iget-object v2, p0, Lcom/google/android/music/leanback/NowPlayingItem;->mPlaybackServiceConnection:Landroid/content/ServiceConnection;

    invoke-static {v1, v2}, Lcom/google/android/music/utils/MusicUtils;->bindToService(Landroid/app/Activity;Landroid/content/ServiceConnection;)Lcom/google/android/music/utils/MusicUtils$ServiceToken;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/music/leanback/NowPlayingItem;->mPlaybackServiceToken:Lcom/google/android/music/utils/MusicUtils$ServiceToken;

    .line 317
    return-void
.end method

.method public onItemSelected(Ljava/lang/Object;)Z
    .locals 6
    .param p1, "item"    # Ljava/lang/Object;

    .prologue
    .line 291
    if-ne p1, p0, :cond_1

    const/4 v2, 0x1

    :goto_0
    iput-boolean v2, p0, Lcom/google/android/music/leanback/NowPlayingItem;->mIsSelected:Z

    .line 292
    iget-boolean v2, p0, Lcom/google/android/music/leanback/NowPlayingItem;->mIsSelected:Z

    if-eqz v2, :cond_0

    .line 293
    iget-object v2, p0, Lcom/google/android/music/leanback/NowPlayingItem;->mCurrentTrackName:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-wide v2, p0, Lcom/google/android/music/leanback/NowPlayingItem;->mCurrentTrackId:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_2

    .line 294
    iget-wide v2, p0, Lcom/google/android/music/leanback/NowPlayingItem;->mCurrentTrackId:J

    iget-object v4, p0, Lcom/google/android/music/leanback/NowPlayingItem;->mCurrentTrackName:Ljava/lang/String;

    invoke-static {v2, v3, v4}, Lcom/google/android/music/store/ContainerDescriptor;->newSingleSongDescriptor(JLjava/lang/String;)Lcom/google/android/music/store/ContainerDescriptor;

    move-result-object v0

    .line 296
    .local v0, "descriptor":Lcom/google/android/music/store/ContainerDescriptor;
    new-instance v1, Lcom/google/android/music/medialist/SingleSongList;

    iget-wide v2, p0, Lcom/google/android/music/leanback/NowPlayingItem;->mCurrentTrackId:J

    iget-object v4, p0, Lcom/google/android/music/leanback/NowPlayingItem;->mCurrentTrackName:Ljava/lang/String;

    invoke-direct {v1, v0, v2, v3, v4}, Lcom/google/android/music/medialist/SingleSongList;-><init>(Lcom/google/android/music/store/ContainerDescriptor;JLjava/lang/String;)V

    .line 298
    .local v1, "songList":Lcom/google/android/music/medialist/SongList;
    iget-object v2, p0, Lcom/google/android/music/leanback/NowPlayingItem;->mLeanbackItemActivity:Lcom/google/android/music/leanback/LeanbackItemActivity;

    iget v3, p0, Lcom/google/android/music/leanback/NowPlayingItem;->mLoaderId:I

    invoke-static {v2, v1, v3}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->loadArtistArtwork(Lcom/google/android/music/leanback/LeanbackItemActivity;Lcom/google/android/music/medialist/SongList;I)V

    .line 303
    .end local v0    # "descriptor":Lcom/google/android/music/store/ContainerDescriptor;
    .end local v1    # "songList":Lcom/google/android/music/medialist/SongList;
    :cond_0
    :goto_1
    iget-boolean v2, p0, Lcom/google/android/music/leanback/NowPlayingItem;->mIsSelected:Z

    return v2

    .line 291
    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    .line 300
    :cond_2
    iget-object v2, p0, Lcom/google/android/music/leanback/NowPlayingItem;->mBackgroundImageMessageHandler:Lcom/google/android/music/leanback/BackgroundImageMessageHandler;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/android/music/leanback/BackgroundImageMessageHandler;->postItemSelectedMessage(Ljava/lang/Object;)V

    goto :goto_1
.end method
