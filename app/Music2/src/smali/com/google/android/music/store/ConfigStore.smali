.class public Lcom/google/android/music/store/ConfigStore;
.super Lcom/google/android/music/store/BaseStore;
.source "ConfigStore.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/store/ConfigStore$DatabaseHelper;
    }
.end annotation


# static fields
.field static final LOGV:Z

.field private static sInstance:Lcom/google/android/music/store/ConfigStore;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDbOpener:Lcom/google/android/music/store/ConfigStore$DatabaseHelper;

.field private final mDowngraded:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    new-instance v0, Lcom/google/android/music/store/ConfigStore;

    invoke-direct {v0}, Lcom/google/android/music/store/ConfigStore;-><init>()V

    sput-object v0, Lcom/google/android/music/store/ConfigStore;->sInstance:Lcom/google/android/music/store/ConfigStore;

    .line 29
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->STORE:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/store/ConfigStore;->LOGV:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/google/android/music/store/BaseStore;-><init>()V

    .line 32
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/music/store/ConfigStore;->mDowngraded:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 104
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/music/store/ConfigStore;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/store/ConfigStore;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/google/android/music/store/ConfigStore;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private downgrade(Ljava/lang/String;Z)Landroid/database/sqlite/SQLiteDatabase;
    .locals 5
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "readable"    # Z

    .prologue
    .line 81
    iget-object v2, p0, Lcom/google/android/music/store/ConfigStore;->mDowngraded:Ljava/util/concurrent/atomic/AtomicBoolean;

    monitor-enter v2

    .line 83
    :try_start_0
    iget-object v1, p0, Lcom/google/android/music/store/ConfigStore;->mDowngraded:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    if-nez v1, :cond_0

    .line 84
    iget-object v1, p0, Lcom/google/android/music/store/ConfigStore;->mDbOpener:Lcom/google/android/music/store/ConfigStore$DatabaseHelper;

    invoke-virtual {v1}, Lcom/google/android/music/store/ConfigStore$DatabaseHelper;->close()V

    .line 85
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 86
    .local v0, "dbFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 87
    const-string v1, "ConfigStore"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Sucessfully deleted old database file at "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    :goto_0
    iget-object v1, p0, Lcom/google/android/music/store/ConfigStore;->mDowngraded:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 96
    .end local v0    # "dbFile":Ljava/io/File;
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/store/ConfigStore;->mDbOpener:Lcom/google/android/music/store/ConfigStore$DatabaseHelper;

    invoke-virtual {v1}, Lcom/google/android/music/store/ConfigStore$DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    monitor-exit v2

    return-object v1

    .line 89
    .restart local v0    # "dbFile":Ljava/io/File;
    :cond_1
    const-string v1, "ConfigStore"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to delete old database file at "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 97
    .end local v0    # "dbFile":Ljava/io/File;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/google/android/music/store/ConfigStore;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 43
    sget-object v0, Lcom/google/android/music/store/ConfigStore;->sInstance:Lcom/google/android/music/store/ConfigStore;

    invoke-direct {v0, p0}, Lcom/google/android/music/store/ConfigStore;->init(Landroid/content/Context;)V

    .line 44
    sget-object v0, Lcom/google/android/music/store/ConfigStore;->sInstance:Lcom/google/android/music/store/ConfigStore;

    return-object v0
.end method

.method private declared-synchronized init(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 51
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/store/ConfigStore;->mDbOpener:Lcom/google/android/music/store/ConfigStore$DatabaseHelper;

    if-nez v0, :cond_0

    .line 52
    iput-object p1, p0, Lcom/google/android/music/store/ConfigStore;->mContext:Landroid/content/Context;

    .line 53
    const-string v0, "ConfigStore"

    const-string v1, "Config Database version: 1"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    new-instance v0, Lcom/google/android/music/store/ConfigStore$DatabaseHelper;

    invoke-direct {v0, p0}, Lcom/google/android/music/store/ConfigStore$DatabaseHelper;-><init>(Lcom/google/android/music/store/ConfigStore;)V

    iput-object v0, p0, Lcom/google/android/music/store/ConfigStore;->mDbOpener:Lcom/google/android/music/store/ConfigStore$DatabaseHelper;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 56
    :cond_0
    monitor-exit p0

    return-void

    .line 51
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method protected getDatabase(Z)Landroid/database/sqlite/SQLiteDatabase;
    .locals 3
    .param p1, "readable"    # Z

    .prologue
    .line 64
    :try_start_0
    iget-object v1, p0, Lcom/google/android/music/store/ConfigStore;->mDbOpener:Lcom/google/android/music/store/ConfigStore$DatabaseHelper;

    invoke-virtual {v1}, Lcom/google/android/music/store/ConfigStore$DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catch Lcom/google/android/music/store/DowngradeException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    .line 73
    :goto_0
    return-object v1

    .line 65
    :catch_0
    move-exception v0

    .line 66
    .local v0, "e":Lcom/google/android/music/store/DowngradeException;
    invoke-virtual {v0}, Lcom/google/android/music/store/DowngradeException;->getFilepath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1, p1}, Lcom/google/android/music/store/ConfigStore;->downgrade(Ljava/lang/String;Z)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    goto :goto_0

    .line 67
    .end local v0    # "e":Lcom/google/android/music/store/DowngradeException;
    :catch_1
    move-exception v0

    .line 71
    .local v0, "e":Landroid/database/sqlite/SQLiteException;
    iget-object v1, p0, Lcom/google/android/music/store/ConfigStore;->mDbOpener:Lcom/google/android/music/store/ConfigStore$DatabaseHelper;

    # getter for: Lcom/google/android/music/store/ConfigStore$DatabaseHelper;->mDBPath:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/music/store/ConfigStore$DatabaseHelper;->access$000(Lcom/google/android/music/store/ConfigStore$DatabaseHelper;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 72
    const-string v1, "ConfigStore"

    const-string v2, "Error trying to open the DB"

    invoke-static {v1, v2, v0}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 73
    iget-object v1, p0, Lcom/google/android/music/store/ConfigStore;->mDbOpener:Lcom/google/android/music/store/ConfigStore$DatabaseHelper;

    # getter for: Lcom/google/android/music/store/ConfigStore$DatabaseHelper;->mDBPath:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/music/store/ConfigStore$DatabaseHelper;->access$000(Lcom/google/android/music/store/ConfigStore$DatabaseHelper;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1, p1}, Lcom/google/android/music/store/ConfigStore;->downgrade(Ljava/lang/String;Z)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    goto :goto_0

    .line 75
    :cond_0
    throw v0
.end method
