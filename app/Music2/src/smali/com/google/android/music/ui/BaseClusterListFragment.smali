.class public abstract Lcom/google/android/music/ui/BaseClusterListFragment;
.super Lcom/google/android/music/ui/BaseListFragment;
.source "BaseClusterListFragment.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Lcom/google/android/music/ui/ClusterListAdapter$ClusterBuilder;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/music/ui/BaseListFragment;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Lcom/google/android/music/ui/ClusterListAdapter$ClusterBuilder;"
    }
.end annotation


# instance fields
.field private mAdapter:Lcom/google/android/music/ui/ClusterListAdapter;

.field private mLoadedStateMask:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/google/android/music/ui/BaseListFragment;-><init>()V

    .line 35
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/music/ui/BaseClusterListFragment;->mLoadedStateMask:J

    .line 39
    return-void
.end method


# virtual methods
.method public final buildCluster(ILandroid/database/Cursor;)Lcom/google/android/music/ui/Cluster;
    .locals 1
    .param p1, "id"    # I
    .param p2, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 135
    invoke-virtual {p0, p1, p2}, Lcom/google/android/music/ui/BaseClusterListFragment;->createCluster(ILandroid/database/Cursor;)Lcom/google/android/music/ui/Cluster;

    move-result-object v0

    return-object v0
.end method

.method protected abstract createCluster(ILandroid/database/Cursor;)Lcom/google/android/music/ui/Cluster;
.end method

.method protected abstract getCardsContextMenuDelegate()Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;
.end method

.method protected abstract getClusterContentUri(I)Landroid/net/Uri;
.end method

.method protected abstract getClusterProjection(I)[Ljava/lang/String;
.end method

.method protected abstract getNumberOfClusters()I
.end method

.method protected getScreenColumns()I
    .locals 2

    .prologue
    .line 62
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseClusterListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseClusterListFragment;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/utils/ViewUtils;->getScreenColumnCount(Landroid/content/res/Resources;Lcom/google/android/music/preferences/MusicPreferences;)I

    move-result v0

    return v0
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 7
    .param p1, "id"    # I
    .param p2, "args"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 140
    invoke-virtual {p0, p1}, Lcom/google/android/music/ui/BaseClusterListFragment;->getClusterContentUri(I)Landroid/net/Uri;

    move-result-object v2

    .line 141
    .local v2, "uri":Landroid/net/Uri;
    invoke-virtual {p0, p1}, Lcom/google/android/music/ui/BaseClusterListFragment;->getClusterProjection(I)[Ljava/lang/String;

    move-result-object v3

    .line 142
    .local v3, "projection":[Ljava/lang/String;
    new-instance v0, Landroid/support/v4/content/CursorLoader;

    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseClusterListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 6
    .param p2, "data"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, "loader":Landroid/support/v4/content/Loader;, "Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    const/4 v0, 0x1

    .line 147
    iget-object v1, p0, Lcom/google/android/music/ui/BaseClusterListFragment;->mAdapter:Lcom/google/android/music/ui/ClusterListAdapter;

    invoke-virtual {v1, p1, p2}, Lcom/google/android/music/ui/ClusterListAdapter;->swapCursor(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    .line 148
    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/BaseClusterListFragment;->setListShown(Z)V

    .line 149
    if-eqz p2, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-nez v1, :cond_1

    .line 150
    :cond_0
    iget-wide v2, p0, Lcom/google/android/music/ui/BaseClusterListFragment;->mLoadedStateMask:J

    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v1

    shl-int v1, v0, v1

    xor-int/lit8 v1, v1, -0x1

    int-to-long v4, v1

    and-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/android/music/ui/BaseClusterListFragment;->mLoadedStateMask:J

    .line 154
    :goto_0
    iget-wide v2, p0, Lcom/google/android/music/ui/BaseClusterListFragment;->mLoadedStateMask:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_2

    :goto_1
    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/BaseClusterListFragment;->setEmptyScreenVisible(Z)V

    .line 155
    return-void

    .line 152
    :cond_1
    iget-wide v2, p0, Lcom/google/android/music/ui/BaseClusterListFragment;->mLoadedStateMask:J

    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v1

    shl-int v1, v0, v1

    int-to-long v4, v1

    or-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/android/music/ui/BaseClusterListFragment;->mLoadedStateMask:J

    goto :goto_0

    .line 154
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Landroid/support/v4/content/Loader;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 32
    check-cast p2, Landroid/database/Cursor;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/music/ui/BaseClusterListFragment;->onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 162
    .local p1, "loader":Landroid/support/v4/content/Loader;, "Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    iget-object v0, p0, Lcom/google/android/music/ui/BaseClusterListFragment;->mAdapter:Lcom/google/android/music/ui/ClusterListAdapter;

    invoke-virtual {v0, p1}, Lcom/google/android/music/ui/ClusterListAdapter;->nullCursor(Landroid/support/v4/content/Loader;)V

    .line 163
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/music/ui/BaseClusterListFragment;->mLoadedStateMask:J

    .line 164
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 43
    invoke-super {p0, p1, p2}, Lcom/google/android/music/ui/BaseListFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 46
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseClusterListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    .line 47
    .local v0, "lv":Landroid/widget/ListView;
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseClusterListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f00c5

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 49
    .local v1, "padding":I
    invoke-virtual {v0, v1, v1, v1, v1}, Landroid/widget/ListView;->setPadding(IIII)V

    .line 50
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setClipToPadding(Z)V

    .line 51
    return-void
.end method

.method protected startLoading()V
    .locals 1

    .prologue
    .line 98
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/BaseClusterListFragment;->startLoading(Z)V

    .line 99
    return-void
.end method

.method protected startLoading(Z)V
    .locals 10
    .param p1, "restore"    # Z

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 109
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseClusterListFragment;->getNumberOfClusters()I

    move-result v0

    .line 110
    .local v0, "howManyClusters":I
    new-instance v3, Lcom/google/android/music/ui/ClusterListAdapter;

    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseClusterListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseClusterListFragment;->getCardsContextMenuDelegate()Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;

    move-result-object v7

    invoke-direct {v3, v6, p0, v7, v0}, Lcom/google/android/music/ui/ClusterListAdapter;-><init>(Landroid/content/Context;Lcom/google/android/music/ui/ClusterListAdapter$ClusterBuilder;Lcom/google/android/music/ui/cardlib/layout/PlayCardView$ContextMenuDelegate;I)V

    iput-object v3, p0, Lcom/google/android/music/ui/BaseClusterListFragment;->mAdapter:Lcom/google/android/music/ui/ClusterListAdapter;

    .line 112
    iget-object v3, p0, Lcom/google/android/music/ui/BaseClusterListFragment;->mAdapter:Lcom/google/android/music/ui/ClusterListAdapter;

    invoke-virtual {p0, v3}, Lcom/google/android/music/ui/BaseClusterListFragment;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 113
    const/4 v3, 0x0

    invoke-virtual {p0, v3}, Lcom/google/android/music/ui/BaseClusterListFragment;->setListShown(Z)V

    .line 114
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseClusterListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v3

    invoke-virtual {v3, v9}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 115
    const-wide/16 v4, 0x0

    .line 116
    .local v4, "newState":J
    if-lez v0, :cond_1

    .line 117
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseClusterListFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v2

    .line 118
    .local v2, "lm":Landroid/support/v4/app/LoaderManager;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_2

    .line 119
    invoke-virtual {v2, v1}, Landroid/support/v4/app/LoaderManager;->getLoader(I)Landroid/support/v4/content/Loader;

    move-result-object v3

    if-eqz v3, :cond_0

    if-nez p1, :cond_0

    .line 120
    invoke-virtual {v2, v1, v9, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 124
    :goto_1
    shl-int v3, v8, v1

    int-to-long v6, v3

    or-long/2addr v4, v6

    .line 118
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 122
    :cond_0
    invoke-virtual {v2, v1, v9, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    goto :goto_1

    .line 127
    .end local v1    # "i":I
    .end local v2    # "lm":Landroid/support/v4/app/LoaderManager;
    :cond_1
    invoke-virtual {p0, v8}, Lcom/google/android/music/ui/BaseClusterListFragment;->setListShown(Z)V

    .line 128
    invoke-virtual {p0, v8}, Lcom/google/android/music/ui/BaseClusterListFragment;->setEmptyScreenVisible(Z)V

    .line 130
    :cond_2
    iput-wide v4, p0, Lcom/google/android/music/ui/BaseClusterListFragment;->mLoadedStateMask:J

    .line 131
    return-void
.end method
