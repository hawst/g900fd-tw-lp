.class Lcom/google/android/music/leanback/ItemPresenter$1;
.super Ljava/lang/Object;
.source "ItemPresenter.java"

# interfaces
.implements Lcom/google/android/music/utils/async/AsyncRunner;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/leanback/ItemPresenter;->onBindViewHolder(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;Ljava/lang/Object;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mDescription:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/music/leanback/ItemPresenter;

.field final synthetic val$imageCardView:Lcom/google/android/music/leanback/ImageCardView;

.field final synthetic val$item:Lcom/google/android/music/leanback/Item;


# direct methods
.method constructor <init>(Lcom/google/android/music/leanback/ItemPresenter;Lcom/google/android/music/leanback/Item;Lcom/google/android/music/leanback/ImageCardView;)V
    .locals 0

    .prologue
    .line 161
    iput-object p1, p0, Lcom/google/android/music/leanback/ItemPresenter$1;->this$0:Lcom/google/android/music/leanback/ItemPresenter;

    iput-object p2, p0, Lcom/google/android/music/leanback/ItemPresenter$1;->val$item:Lcom/google/android/music/leanback/Item;

    iput-object p3, p0, Lcom/google/android/music/leanback/ItemPresenter$1;->val$imageCardView:Lcom/google/android/music/leanback/ImageCardView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public backgroundTask()V
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/google/android/music/leanback/ItemPresenter$1;->val$item:Lcom/google/android/music/leanback/Item;

    invoke-virtual {v0}, Lcom/google/android/music/leanback/Item;->getDescriptionGetter()Lcom/google/android/music/leanback/Item$StringGetter;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/music/leanback/Item$StringGetter;->getString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/leanback/ItemPresenter$1;->mDescription:Ljava/lang/String;

    .line 167
    return-void
.end method

.method public taskCompleted()V
    .locals 2

    .prologue
    .line 171
    iget-object v0, p0, Lcom/google/android/music/leanback/ItemPresenter$1;->val$imageCardView:Lcom/google/android/music/leanback/ImageCardView;

    const v1, 0x7f0e015f

    invoke-virtual {v0, v1}, Lcom/google/android/music/leanback/ImageCardView;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/leanback/ItemPresenter$1;->val$item:Lcom/google/android/music/leanback/Item;

    if-eq v0, v1, :cond_0

    .line 176
    :goto_0
    return-void

    .line 174
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/leanback/ItemPresenter$1;->val$imageCardView:Lcom/google/android/music/leanback/ImageCardView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/music/leanback/ImageCardView;->setForceKeepContent(Z)V

    .line 175
    iget-object v0, p0, Lcom/google/android/music/leanback/ItemPresenter$1;->val$imageCardView:Lcom/google/android/music/leanback/ImageCardView;

    iget-object v1, p0, Lcom/google/android/music/leanback/ItemPresenter$1;->mDescription:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/music/leanback/ImageCardView;->setContentText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
