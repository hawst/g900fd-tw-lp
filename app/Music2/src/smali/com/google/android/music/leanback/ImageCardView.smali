.class public Lcom/google/android/music/leanback/ImageCardView;
.super Landroid/support/v17/leanback/widget/BaseCardView;
.source "ImageCardView.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mContentView:Landroid/widget/TextView;

.field private final mContentViewHeight:I

.field private final mContentViewMetrics:Landroid/graphics/Paint$FontMetricsInt;

.field private final mImageView:Landroid/widget/ImageView;

.field private final mInfoArea:Landroid/view/View;

.field private final mInfoBottomPadding:I

.field private final mInfoTitleBaseline:I

.field private mKeepContent:Z

.field private final mTitleView:Landroid/widget/TextView;

.field private final mTitleViewHeight:I

.field private final mTitleViewMetrics:Landroid/graphics/Paint$FontMetricsInt;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->LEANBACK:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-virtual {v0}, Lcom/google/android/music/utils/DebugUtils$MusicTag;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/leanback/ImageCardView;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 45
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/music/leanback/ImageCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 49
    const v0, 0x7f0100f8

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/music/leanback/ImageCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 50
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 53
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v17/leanback/widget/BaseCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 55
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0f018a

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    iput v5, p0, Lcom/google/android/music/leanback/ImageCardView;->mInfoTitleBaseline:I

    .line 57
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0f018b

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    iput v5, p0, Lcom/google/android/music/leanback/ImageCardView;->mInfoBottomPadding:I

    .line 60
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 61
    .local v2, "inflater":Landroid/view/LayoutInflater;
    const v5, 0x7f04005a

    invoke-virtual {v2, v5, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 63
    .local v4, "v":Landroid/view/View;
    const v5, 0x7f0e0139

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    iput-object v5, p0, Lcom/google/android/music/leanback/ImageCardView;->mImageView:Landroid/widget/ImageView;

    .line 64
    iget-object v5, p0, Lcom/google/android/music/leanback/ImageCardView;->mImageView:Landroid/widget/ImageView;

    const/4 v6, 0x4

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 65
    const v5, 0x7f0e013a

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/music/leanback/ImageCardView;->mInfoArea:Landroid/view/View;

    .line 66
    const v5, 0x7f0e013b

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/google/android/music/leanback/ImageCardView;->mTitleView:Landroid/widget/TextView;

    .line 67
    const v5, 0x7f0e013c

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/google/android/music/leanback/ImageCardView;->mContentView:Landroid/widget/TextView;

    .line 69
    iget-object v5, p0, Lcom/google/android/music/leanback/ImageCardView;->mInfoArea:Landroid/view/View;

    if-eqz v5, :cond_0

    .line 70
    sget-object v5, Landroid/support/v17/leanback/R$styleable;->lbImageCardView:[I

    invoke-virtual {p1, p2, v5, p3, v7}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 73
    .local v0, "a":Landroid/content/res/TypedArray;
    const/4 v5, 0x0

    :try_start_0
    invoke-virtual {v0, v5}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/google/android/music/leanback/ImageCardView;->setInfoAreaBackground(Landroid/graphics/drawable/Drawable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 76
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 79
    .end local v0    # "a":Landroid/content/res/TypedArray;
    :cond_0
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3, v8}, Landroid/graphics/Paint;-><init>(I)V

    .line 80
    .local v3, "titlePaint":Landroid/graphics/Paint;
    iget-object v5, p0, Lcom/google/android/music/leanback/ImageCardView;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getTextSize()F

    move-result v5

    invoke-virtual {v3, v5}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 81
    iget-object v5, p0, Lcom/google/android/music/leanback/ImageCardView;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 82
    invoke-virtual {v3}, Landroid/graphics/Paint;->getFontMetricsInt()Landroid/graphics/Paint$FontMetricsInt;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/music/leanback/ImageCardView;->mTitleViewMetrics:Landroid/graphics/Paint$FontMetricsInt;

    .line 83
    invoke-virtual {v3}, Landroid/graphics/Paint;->getFontSpacing()F

    move-result v5

    float-to-int v5, v5

    iput v5, p0, Lcom/google/android/music/leanback/ImageCardView;->mTitleViewHeight:I

    .line 85
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1, v8}, Landroid/graphics/Paint;-><init>(I)V

    .line 86
    .local v1, "contentPaint":Landroid/graphics/Paint;
    iget-object v5, p0, Lcom/google/android/music/leanback/ImageCardView;->mContentView:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getTextSize()F

    move-result v5

    invoke-virtual {v1, v5}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 87
    iget-object v5, p0, Lcom/google/android/music/leanback/ImageCardView;->mContentView:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 88
    invoke-virtual {v1}, Landroid/graphics/Paint;->getFontMetricsInt()Landroid/graphics/Paint$FontMetricsInt;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/music/leanback/ImageCardView;->mContentViewMetrics:Landroid/graphics/Paint$FontMetricsInt;

    .line 89
    invoke-virtual {v1}, Landroid/graphics/Paint;->getFontSpacing()F

    move-result v5

    float-to-int v5, v5

    iput v5, p0, Lcom/google/android/music/leanback/ImageCardView;->mContentViewHeight:I

    .line 90
    return-void

    .line 76
    .end local v1    # "contentPaint":Landroid/graphics/Paint;
    .end local v3    # "titlePaint":Landroid/graphics/Paint;
    .restart local v0    # "a":Landroid/content/res/TypedArray;
    :catchall_0
    move-exception v5

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    throw v5
.end method

.method private fadeIn(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 200
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setAlpha(F)V

    .line 201
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v2, 0x10e0000

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 203
    return-void
.end method

.method private updateTextViewPadding()V
    .locals 12

    .prologue
    const/4 v10, 0x0

    .line 206
    iget-object v11, p0, Lcom/google/android/music/leanback/ImageCardView;->mContentView:Landroid/widget/TextView;

    invoke-virtual {v11}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v11

    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_0

    iget-boolean v11, p0, Lcom/google/android/music/leanback/ImageCardView;->mKeepContent:Z

    if-nez v11, :cond_0

    const/4 v2, 0x1

    .line 207
    .local v2, "contentEmpty":Z
    :goto_0
    iget-object v11, p0, Lcom/google/android/music/leanback/ImageCardView;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v11}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v11

    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    .line 208
    .local v7, "titleEmpty":Z
    if-eqz v7, :cond_2

    .line 209
    iget-object v11, p0, Lcom/google/android/music/leanback/ImageCardView;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v11, v10, v10, v10, v10}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 210
    if-eqz v2, :cond_1

    .line 211
    iget-object v11, p0, Lcom/google/android/music/leanback/ImageCardView;->mContentView:Landroid/widget/TextView;

    invoke-virtual {v11, v10, v10, v10, v10}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 240
    :goto_1
    return-void

    .end local v2    # "contentEmpty":Z
    .end local v7    # "titleEmpty":Z
    :cond_0
    move v2, v10

    .line 206
    goto :goto_0

    .line 213
    .restart local v2    # "contentEmpty":Z
    .restart local v7    # "titleEmpty":Z
    :cond_1
    iget v3, p0, Lcom/google/android/music/leanback/ImageCardView;->mContentViewHeight:I

    .line 214
    .local v3, "contentHeight":I
    iget-object v11, p0, Lcom/google/android/music/leanback/ImageCardView;->mContentViewMetrics:Landroid/graphics/Paint$FontMetricsInt;

    iget v1, v11, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    .line 215
    .local v1, "contentDescent":I
    iget v11, p0, Lcom/google/android/music/leanback/ImageCardView;->mTitleViewHeight:I

    sub-int/2addr v11, v3

    add-int v4, v11, v1

    .line 216
    .local v4, "contentTopPadding":I
    iget v11, p0, Lcom/google/android/music/leanback/ImageCardView;->mInfoBottomPadding:I

    sub-int v0, v11, v1

    .line 217
    .local v0, "contentBottomPadding":I
    iget-object v11, p0, Lcom/google/android/music/leanback/ImageCardView;->mContentView:Landroid/widget/TextView;

    invoke-virtual {v11, v10, v4, v10, v0}, Landroid/widget/TextView;->setPadding(IIII)V

    goto :goto_1

    .line 220
    .end local v0    # "contentBottomPadding":I
    .end local v1    # "contentDescent":I
    .end local v3    # "contentHeight":I
    .end local v4    # "contentTopPadding":I
    :cond_2
    if-eqz v2, :cond_3

    .line 221
    iget v8, p0, Lcom/google/android/music/leanback/ImageCardView;->mTitleViewHeight:I

    .line 222
    .local v8, "titleHeight":I
    iget-object v11, p0, Lcom/google/android/music/leanback/ImageCardView;->mTitleViewMetrics:Landroid/graphics/Paint$FontMetricsInt;

    iget v6, v11, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    .line 223
    .local v6, "titleDescent":I
    iget v11, p0, Lcom/google/android/music/leanback/ImageCardView;->mInfoTitleBaseline:I

    sub-int/2addr v11, v8

    add-int v9, v11, v6

    .line 224
    .local v9, "titleTopPadding":I
    iget v11, p0, Lcom/google/android/music/leanback/ImageCardView;->mInfoBottomPadding:I

    sub-int v5, v11, v6

    .line 225
    .local v5, "titleBottomPadding":I
    iget-object v11, p0, Lcom/google/android/music/leanback/ImageCardView;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v11, v10, v9, v10, v5}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 226
    iget-object v11, p0, Lcom/google/android/music/leanback/ImageCardView;->mContentView:Landroid/widget/TextView;

    invoke-virtual {v11, v10, v10, v10, v10}, Landroid/widget/TextView;->setPadding(IIII)V

    goto :goto_1

    .line 228
    .end local v5    # "titleBottomPadding":I
    .end local v6    # "titleDescent":I
    .end local v8    # "titleHeight":I
    .end local v9    # "titleTopPadding":I
    :cond_3
    iget v8, p0, Lcom/google/android/music/leanback/ImageCardView;->mTitleViewHeight:I

    .line 229
    .restart local v8    # "titleHeight":I
    iget-object v11, p0, Lcom/google/android/music/leanback/ImageCardView;->mTitleViewMetrics:Landroid/graphics/Paint$FontMetricsInt;

    iget v6, v11, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    .line 230
    .restart local v6    # "titleDescent":I
    iget v11, p0, Lcom/google/android/music/leanback/ImageCardView;->mInfoTitleBaseline:I

    sub-int/2addr v11, v8

    add-int v9, v11, v6

    .line 231
    .restart local v9    # "titleTopPadding":I
    const/4 v5, 0x0

    .line 232
    .restart local v5    # "titleBottomPadding":I
    iget-object v11, p0, Lcom/google/android/music/leanback/ImageCardView;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v11, v10, v9, v10, v5}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 233
    iget v3, p0, Lcom/google/android/music/leanback/ImageCardView;->mContentViewHeight:I

    .line 234
    .restart local v3    # "contentHeight":I
    iget-object v11, p0, Lcom/google/android/music/leanback/ImageCardView;->mContentViewMetrics:Landroid/graphics/Paint$FontMetricsInt;

    iget v1, v11, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    .line 235
    .restart local v1    # "contentDescent":I
    iget v11, p0, Lcom/google/android/music/leanback/ImageCardView;->mTitleViewHeight:I

    sub-int/2addr v11, v6

    sub-int/2addr v11, v3

    add-int v4, v11, v1

    .line 236
    .restart local v4    # "contentTopPadding":I
    iget v11, p0, Lcom/google/android/music/leanback/ImageCardView;->mInfoBottomPadding:I

    sub-int v0, v11, v1

    .line 237
    .restart local v0    # "contentBottomPadding":I
    iget-object v11, p0, Lcom/google/android/music/leanback/ImageCardView;->mContentView:Landroid/widget/TextView;

    invoke-virtual {v11, v10, v4, v10, v0}, Landroid/widget/TextView;->setPadding(IIII)V

    goto :goto_1
.end method


# virtual methods
.method public final getMainImageView()Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/android/music/leanback/ImageCardView;->mImageView:Landroid/widget/ImageView;

    return-object v0
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 245
    iget-object v0, p0, Lcom/google/android/music/leanback/ImageCardView;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 246
    iget-object v0, p0, Lcom/google/android/music/leanback/ImageCardView;->mImageView:Landroid/widget/ImageView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 247
    invoke-super {p0}, Landroid/support/v17/leanback/widget/BaseCardView;->onDetachedFromWindow()V

    .line 248
    return-void
.end method

.method public setContentText(Ljava/lang/CharSequence;)V
    .locals 2
    .param p1, "text"    # Ljava/lang/CharSequence;

    .prologue
    .line 177
    iget-object v0, p0, Lcom/google/android/music/leanback/ImageCardView;->mContentView:Landroid/widget/TextView;

    if-nez v0, :cond_0

    .line 185
    :goto_0
    return-void

    .line 181
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/leanback/ImageCardView;->mContentView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 182
    iget-object v1, p0, Lcom/google/android/music/leanback/ImageCardView;->mContentView:Landroid/widget/TextView;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/music/leanback/ImageCardView;->mKeepContent:Z

    if-nez v0, :cond_1

    const/16 v0, 0x8

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 184
    invoke-direct {p0}, Lcom/google/android/music/leanback/ImageCardView;->updateTextViewPadding()V

    goto :goto_0

    .line 182
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public setForceKeepContent(Z)V
    .locals 0
    .param p1, "keepContent"    # Z

    .prologue
    .line 196
    iput-boolean p1, p0, Lcom/google/android/music/leanback/ImageCardView;->mKeepContent:Z

    .line 197
    return-void
.end method

.method public setInfoAreaBackground(Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1, "drawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 155
    iget-object v0, p0, Lcom/google/android/music/leanback/ImageCardView;->mInfoArea:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 156
    iget-object v0, p0, Lcom/google/android/music/leanback/ImageCardView;->mInfoArea:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 158
    :cond_0
    return-void
.end method

.method public setInfoAreaBackgroundColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 161
    iget-object v0, p0, Lcom/google/android/music/leanback/ImageCardView;->mInfoArea:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 162
    iget-object v0, p0, Lcom/google/android/music/leanback/ImageCardView;->mInfoArea:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 164
    :cond_0
    return-void
.end method

.method public setMainImage(Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1, "drawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 112
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/google/android/music/leanback/ImageCardView;->setMainImage(Landroid/graphics/drawable/Drawable;Z)V

    .line 113
    return-void
.end method

.method public setMainImage(Landroid/graphics/drawable/Drawable;Z)V
    .locals 3
    .param p1, "drawable"    # Landroid/graphics/drawable/Drawable;
    .param p2, "fade"    # Z

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 119
    iget-object v0, p0, Lcom/google/android/music/leanback/ImageCardView;->mImageView:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    .line 137
    :goto_0
    return-void

    .line 123
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/leanback/ImageCardView;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 124
    if-nez p1, :cond_1

    .line 125
    iget-object v0, p0, Lcom/google/android/music/leanback/ImageCardView;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 126
    iget-object v0, p0, Lcom/google/android/music/leanback/ImageCardView;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 127
    iget-object v0, p0, Lcom/google/android/music/leanback/ImageCardView;->mImageView:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 129
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/leanback/ImageCardView;->mImageView:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 130
    if-eqz p2, :cond_2

    .line 131
    iget-object v0, p0, Lcom/google/android/music/leanback/ImageCardView;->mImageView:Landroid/widget/ImageView;

    invoke-direct {p0, v0}, Lcom/google/android/music/leanback/ImageCardView;->fadeIn(Landroid/view/View;)V

    goto :goto_0

    .line 133
    :cond_2
    iget-object v0, p0, Lcom/google/android/music/leanback/ImageCardView;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 134
    iget-object v0, p0, Lcom/google/android/music/leanback/ImageCardView;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setAlpha(F)V

    goto :goto_0
.end method

.method public setMainImageAdjustViewBounds(Z)V
    .locals 1
    .param p1, "adjustViewBounds"    # Z

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/android/music/leanback/ImageCardView;->mImageView:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 98
    iget-object v0, p0, Lcom/google/android/music/leanback/ImageCardView;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setAdjustViewBounds(Z)V

    .line 100
    :cond_0
    return-void
.end method

.method public setMainImageDimensions(II)V
    .locals 2
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 140
    iget-object v1, p0, Lcom/google/android/music/leanback/ImageCardView;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 141
    .local v0, "lp":Landroid/view/ViewGroup$LayoutParams;
    iput p1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 142
    iput p2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 143
    iget-object v1, p0, Lcom/google/android/music/leanback/ImageCardView;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 144
    return-void
.end method

.method public setMainImageScaleType(Landroid/widget/ImageView$ScaleType;)V
    .locals 1
    .param p1, "scaleType"    # Landroid/widget/ImageView$ScaleType;

    .prologue
    .line 103
    iget-object v0, p0, Lcom/google/android/music/leanback/ImageCardView;->mImageView:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 104
    iget-object v0, p0, Lcom/google/android/music/leanback/ImageCardView;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 106
    :cond_0
    return-void
.end method

.method public setTitleText(Ljava/lang/CharSequence;)V
    .locals 2
    .param p1, "text"    # Ljava/lang/CharSequence;

    .prologue
    .line 167
    iget-object v0, p0, Lcom/google/android/music/leanback/ImageCardView;->mTitleView:Landroid/widget/TextView;

    if-nez v0, :cond_0

    .line 174
    :goto_0
    return-void

    .line 171
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/leanback/ImageCardView;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 172
    iget-object v1, p0, Lcom/google/android/music/leanback/ImageCardView;->mTitleView:Landroid/widget/TextView;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0x8

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 173
    invoke-direct {p0}, Lcom/google/android/music/leanback/ImageCardView;->updateTextViewPadding()V

    goto :goto_0

    .line 172
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
