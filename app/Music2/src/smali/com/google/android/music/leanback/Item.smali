.class public Lcom/google/android/music/leanback/Item;
.super Ljava/lang/Object;
.source "Item.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/leanback/Item$Builder;,
        Lcom/google/android/music/leanback/Item$ConstantStringGetter;,
        Lcom/google/android/music/leanback/Item$StringGetter;,
        Lcom/google/android/music/leanback/Item$ConstantIntentGetter;,
        Lcom/google/android/music/leanback/Item$IntentGetter;
    }
.end annotation


# instance fields
.field private final mBitmapGettersGetter:Lcom/google/android/music/leanback/bitmap/BitmapGettersGetter;

.field private final mDarkenBackground:Z

.field private final mDescriptionGetter:Lcom/google/android/music/leanback/Item$StringGetter;

.field private final mIconResourceId:I

.field private final mIconUriGetter:Lcom/google/android/music/leanback/Item$StringGetter;

.field private final mId:Ljava/lang/String;

.field private final mIntentGetter:Lcom/google/android/music/leanback/Item$IntentGetter;

.field private final mOverlayResourceId:I

.field private final mSecondaryId:J

.field private final mTitle:Ljava/lang/String;

.field private final mWide:Z


# direct methods
.method protected constructor <init>()V
    .locals 13

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 192
    const/4 v8, 0x1

    const-wide/16 v10, 0x0

    move-object v0, p0

    move-object v2, v1

    move-object v3, v1

    move-object v5, v1

    move-object v6, v1

    move v7, v4

    move v9, v4

    move-object v12, v1

    invoke-direct/range {v0 .. v12}, Lcom/google/android/music/leanback/Item;-><init>(Ljava/lang/String;Lcom/google/android/music/leanback/Item$StringGetter;Lcom/google/android/music/leanback/Item$StringGetter;ILcom/google/android/music/leanback/Item$IntentGetter;Ljava/lang/String;ZZIJLcom/google/android/music/leanback/bitmap/BitmapGettersGetter;)V

    .line 193
    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;Lcom/google/android/music/leanback/Item$StringGetter;Lcom/google/android/music/leanback/Item$StringGetter;ILcom/google/android/music/leanback/Item$IntentGetter;Ljava/lang/String;ZZIJLcom/google/android/music/leanback/bitmap/BitmapGettersGetter;)V
    .locals 0
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "descriptionGetter"    # Lcom/google/android/music/leanback/Item$StringGetter;
    .param p3, "iconUriGetter"    # Lcom/google/android/music/leanback/Item$StringGetter;
    .param p4, "iconResourceId"    # I
    .param p5, "intentGetter"    # Lcom/google/android/music/leanback/Item$IntentGetter;
    .param p6, "id"    # Ljava/lang/String;
    .param p7, "wide"    # Z
    .param p8, "darkenBackground"    # Z
    .param p9, "overlayResourceId"    # I
    .param p10, "secondaryId"    # J
    .param p12, "bitmapGettersGetter"    # Lcom/google/android/music/leanback/bitmap/BitmapGettersGetter;

    .prologue
    .line 198
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 199
    iput-object p1, p0, Lcom/google/android/music/leanback/Item;->mTitle:Ljava/lang/String;

    .line 200
    iput-object p2, p0, Lcom/google/android/music/leanback/Item;->mDescriptionGetter:Lcom/google/android/music/leanback/Item$StringGetter;

    .line 201
    iput-object p3, p0, Lcom/google/android/music/leanback/Item;->mIconUriGetter:Lcom/google/android/music/leanback/Item$StringGetter;

    .line 202
    iput p4, p0, Lcom/google/android/music/leanback/Item;->mIconResourceId:I

    .line 203
    iput-object p5, p0, Lcom/google/android/music/leanback/Item;->mIntentGetter:Lcom/google/android/music/leanback/Item$IntentGetter;

    .line 204
    iput-object p6, p0, Lcom/google/android/music/leanback/Item;->mId:Ljava/lang/String;

    .line 205
    iput-boolean p7, p0, Lcom/google/android/music/leanback/Item;->mWide:Z

    .line 206
    iput-boolean p8, p0, Lcom/google/android/music/leanback/Item;->mDarkenBackground:Z

    .line 207
    iput p9, p0, Lcom/google/android/music/leanback/Item;->mOverlayResourceId:I

    .line 208
    iput-wide p10, p0, Lcom/google/android/music/leanback/Item;->mSecondaryId:J

    .line 209
    iput-object p12, p0, Lcom/google/android/music/leanback/Item;->mBitmapGettersGetter:Lcom/google/android/music/leanback/bitmap/BitmapGettersGetter;

    .line 210
    return-void
.end method


# virtual methods
.method public getBitmapGettersGetter()Lcom/google/android/music/leanback/bitmap/BitmapGettersGetter;
    .locals 1

    .prologue
    .line 288
    iget-object v0, p0, Lcom/google/android/music/leanback/Item;->mBitmapGettersGetter:Lcom/google/android/music/leanback/bitmap/BitmapGettersGetter;

    return-object v0
.end method

.method getDarkenBackground()Z
    .locals 1

    .prologue
    .line 269
    iget-boolean v0, p0, Lcom/google/android/music/leanback/Item;->mDarkenBackground:Z

    return v0
.end method

.method getDescriptionGetter()Lcom/google/android/music/leanback/Item$StringGetter;
    .locals 1

    .prologue
    .line 225
    iget-object v0, p0, Lcom/google/android/music/leanback/Item;->mDescriptionGetter:Lcom/google/android/music/leanback/Item$StringGetter;

    return-object v0
.end method

.method getIconResourceId()I
    .locals 1

    .prologue
    .line 241
    iget v0, p0, Lcom/google/android/music/leanback/Item;->mIconResourceId:I

    return v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 255
    iget-object v0, p0, Lcom/google/android/music/leanback/Item;->mId:Ljava/lang/String;

    return-object v0
.end method

.method getIntentGetter()Lcom/google/android/music/leanback/Item$IntentGetter;
    .locals 1

    .prologue
    .line 248
    iget-object v0, p0, Lcom/google/android/music/leanback/Item;->mIntentGetter:Lcom/google/android/music/leanback/Item$IntentGetter;

    return-object v0
.end method

.method getOverlayResourceId()I
    .locals 1

    .prologue
    .line 277
    iget v0, p0, Lcom/google/android/music/leanback/Item;->mOverlayResourceId:I

    return v0
.end method

.method getSecondaryId()J
    .locals 2

    .prologue
    .line 284
    iget-wide v0, p0, Lcom/google/android/music/leanback/Item;->mSecondaryId:J

    return-wide v0
.end method

.method getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/google/android/music/leanback/Item;->mTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getWide()Z
    .locals 1

    .prologue
    .line 262
    iget-boolean v0, p0, Lcom/google/android/music/leanback/Item;->mWide:Z

    return v0
.end method

.method onClicked(Landroid/content/Context;Landroid/view/View;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "view"    # Landroid/view/View;

    .prologue
    .line 295
    invoke-virtual {p0}, Lcom/google/android/music/leanback/Item;->getIntentGetter()Lcom/google/android/music/leanback/Item$IntentGetter;

    move-result-object v2

    .line 296
    .local v2, "intentGetter":Lcom/google/android/music/leanback/Item$IntentGetter;
    if-eqz v2, :cond_0

    .line 297
    invoke-interface {v2}, Lcom/google/android/music/leanback/Item$IntentGetter;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 298
    .local v1, "intent":Landroid/content/Intent;
    const/4 v0, 0x0

    .line 299
    .local v0, "bundle":Landroid/os/Bundle;
    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;Landroid/os/Bundle;)V

    .line 301
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_0
    return-void
.end method
