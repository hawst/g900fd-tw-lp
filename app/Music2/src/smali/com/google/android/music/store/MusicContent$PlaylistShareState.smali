.class public final Lcom/google/android/music/store/MusicContent$PlaylistShareState;
.super Ljava/lang/Object;
.source "MusicContent.java"

# interfaces
.implements Landroid/provider/BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/store/MusicContent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PlaylistShareState"
.end annotation


# static fields
.field public static final CONTENT_URI:Landroid/net/Uri;

.field public static STATE:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 3522
    const-string v0, "state"

    sput-object v0, Lcom/google/android/music/store/MusicContent$PlaylistShareState;->STATE:Ljava/lang/String;

    .line 3529
    sget-object v0, Lcom/google/android/music/store/MusicContent;->CONTENT_URI:Landroid/net/Uri;

    const-string v1, "playlist_share_state"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/store/MusicContent$PlaylistShareState;->CONTENT_URI:Landroid/net/Uri;

    return-void
.end method

.method public static getPlaylistShareState(Landroid/content/Context;J)I
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "playlistId"    # J

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 3567
    invoke-static {p1, p2}, Lcom/google/android/music/store/MusicContent$PlaylistShareState;->getUri(J)Landroid/net/Uri;

    move-result-object v1

    .line 3568
    .local v1, "uri":Landroid/net/Uri;
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    sget-object v0, Lcom/google/android/music/store/MusicContent$PlaylistShareState;->STATE:Ljava/lang/String;

    aput-object v0, v2, v4

    .local v2, "projection":[Ljava/lang/String;
    move-object v0, p0

    move-object v4, v3

    move-object v5, v3

    .line 3571
    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 3572
    .local v6, "c":Landroid/database/Cursor;
    const/4 v7, 0x1

    .line 3574
    .local v7, "sharedValue":I
    if-eqz v6, :cond_0

    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3575
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v7

    .line 3578
    :cond_0
    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 3580
    return v7

    .line 3578
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method public static getUri(J)Landroid/net/Uri;
    .locals 2
    .param p0, "playlistId"    # J

    .prologue
    .line 3545
    sget-object v0, Lcom/google/android/music/store/MusicContent$PlaylistShareState;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {p0, p1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static setShareState(Landroid/content/ContentResolver;JZ)I
    .locals 5
    .param p0, "resolver"    # Landroid/content/ContentResolver;
    .param p1, "playlistId"    # J
    .param p3, "isShared"    # Z

    .prologue
    const/4 v4, 0x0

    .line 3553
    new-instance v1, Landroid/content/ContentValues;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Landroid/content/ContentValues;-><init>(I)V

    .line 3554
    .local v1, "values":Landroid/content/ContentValues;
    sget-object v3, Lcom/google/android/music/store/MusicContent$PlaylistShareState;->STATE:Ljava/lang/String;

    if-eqz p3, :cond_0

    const/4 v2, 0x2

    :goto_0
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3555
    invoke-static {p1, p2}, Lcom/google/android/music/store/MusicContent$PlaylistShareState;->getUri(J)Landroid/net/Uri;

    move-result-object v0

    .line 3556
    .local v0, "uri":Landroid/net/Uri;
    invoke-virtual {p0, v0, v1, v4, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    return v2

    .line 3554
    .end local v0    # "uri":Landroid/net/Uri;
    :cond_0
    const/4 v2, 0x3

    goto :goto_0
.end method
