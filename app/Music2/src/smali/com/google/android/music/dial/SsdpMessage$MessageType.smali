.class public final enum Lcom/google/android/music/dial/SsdpMessage$MessageType;
.super Ljava/lang/Enum;
.source "SsdpMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/dial/SsdpMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "MessageType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/music/dial/SsdpMessage$MessageType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/music/dial/SsdpMessage$MessageType;

.field public static final enum MSEARCH:Lcom/google/android/music/dial/SsdpMessage$MessageType;

.field public static final enum NOTIFY:Lcom/google/android/music/dial/SsdpMessage$MessageType;

.field public static final enum REPLY:Lcom/google/android/music/dial/SsdpMessage$MessageType;


# instance fields
.field private final mStartLine:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 52
    new-instance v0, Lcom/google/android/music/dial/SsdpMessage$MessageType;

    const-string v1, "REPLY"

    const-string v2, "HTTP/1.1 200 OK"

    invoke-direct {v0, v1, v3, v2}, Lcom/google/android/music/dial/SsdpMessage$MessageType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/music/dial/SsdpMessage$MessageType;->REPLY:Lcom/google/android/music/dial/SsdpMessage$MessageType;

    .line 53
    new-instance v0, Lcom/google/android/music/dial/SsdpMessage$MessageType;

    const-string v1, "MSEARCH"

    const-string v2, "M-SEARCH * HTTP/1.1"

    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/music/dial/SsdpMessage$MessageType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/music/dial/SsdpMessage$MessageType;->MSEARCH:Lcom/google/android/music/dial/SsdpMessage$MessageType;

    .line 54
    new-instance v0, Lcom/google/android/music/dial/SsdpMessage$MessageType;

    const-string v1, "NOTIFY"

    const-string v2, "NOTIFY * HTTP/1.1"

    invoke-direct {v0, v1, v5, v2}, Lcom/google/android/music/dial/SsdpMessage$MessageType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/music/dial/SsdpMessage$MessageType;->NOTIFY:Lcom/google/android/music/dial/SsdpMessage$MessageType;

    .line 51
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/music/dial/SsdpMessage$MessageType;

    sget-object v1, Lcom/google/android/music/dial/SsdpMessage$MessageType;->REPLY:Lcom/google/android/music/dial/SsdpMessage$MessageType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/music/dial/SsdpMessage$MessageType;->MSEARCH:Lcom/google/android/music/dial/SsdpMessage$MessageType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/music/dial/SsdpMessage$MessageType;->NOTIFY:Lcom/google/android/music/dial/SsdpMessage$MessageType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/music/dial/SsdpMessage$MessageType;->$VALUES:[Lcom/google/android/music/dial/SsdpMessage$MessageType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "startLine"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 58
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 59
    iput-object p3, p0, Lcom/google/android/music/dial/SsdpMessage$MessageType;->mStartLine:Ljava/lang/String;

    .line 60
    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/google/android/music/dial/SsdpMessage$MessageType;
    .locals 5
    .param p0, "startLine"    # Ljava/lang/String;

    .prologue
    .line 67
    invoke-static {}, Lcom/google/android/music/dial/SsdpMessage$MessageType;->values()[Lcom/google/android/music/dial/SsdpMessage$MessageType;

    move-result-object v0

    .local v0, "arr$":[Lcom/google/android/music/dial/SsdpMessage$MessageType;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 68
    .local v3, "mt":Lcom/google/android/music/dial/SsdpMessage$MessageType;
    iget-object v4, v3, Lcom/google/android/music/dial/SsdpMessage$MessageType;->mStartLine:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 72
    .end local v3    # "mt":Lcom/google/android/music/dial/SsdpMessage$MessageType;
    :goto_1
    return-object v3

    .line 67
    .restart local v3    # "mt":Lcom/google/android/music/dial/SsdpMessage$MessageType;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 72
    .end local v3    # "mt":Lcom/google/android/music/dial/SsdpMessage$MessageType;
    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/music/dial/SsdpMessage$MessageType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 51
    const-class v0, Lcom/google/android/music/dial/SsdpMessage$MessageType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/dial/SsdpMessage$MessageType;

    return-object v0
.end method

.method public static values()[Lcom/google/android/music/dial/SsdpMessage$MessageType;
    .locals 1

    .prologue
    .line 51
    sget-object v0, Lcom/google/android/music/dial/SsdpMessage$MessageType;->$VALUES:[Lcom/google/android/music/dial/SsdpMessage$MessageType;

    invoke-virtual {v0}, [Lcom/google/android/music/dial/SsdpMessage$MessageType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/music/dial/SsdpMessage$MessageType;

    return-object v0
.end method


# virtual methods
.method public getStartLine()Ljava/lang/String;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/music/dial/SsdpMessage$MessageType;->mStartLine:Ljava/lang/String;

    return-object v0
.end method
