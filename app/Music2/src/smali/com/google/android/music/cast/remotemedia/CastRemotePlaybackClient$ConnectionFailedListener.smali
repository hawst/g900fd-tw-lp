.class Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionFailedListener;
.super Ljava/lang/Object;
.source "CastRemotePlaybackClient.java"

# interfaces
.implements Lcom/google/android/gms/common/api/GoogleApiClient$OnConnectionFailedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ConnectionFailedListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;


# direct methods
.method private constructor <init>(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)V
    .locals 0

    .prologue
    .line 641
    iput-object p1, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionFailedListener;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;
    .param p2, "x1"    # Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$1;

    .prologue
    .line 641
    invoke-direct {p0, p1}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionFailedListener;-><init>(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)V

    return-void
.end method


# virtual methods
.method public onConnectionFailed(Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 2
    .param p1, "result"    # Lcom/google/android/gms/common/ConnectionResult;

    .prologue
    .line 644
    const-string v0, "MusicCastRemote"

    const-string v1, "onConnectionFailed"

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 645
    iget-object v0, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionFailedListener;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    # getter for: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->mServiceHooks:Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;
    invoke-static {v0}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$1000(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/music/playback/LocalDevicePlayback$ServiceHooks;->notifyMediaRouteInvalidated()V

    .line 646
    iget-object v0, p0, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient$ConnectionFailedListener;->this$0:Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;

    # invokes: Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->tearDown()V
    invoke-static {v0}, Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;->access$1100(Lcom/google/android/music/cast/remotemedia/CastRemotePlaybackClient;)V

    .line 647
    return-void
.end method
