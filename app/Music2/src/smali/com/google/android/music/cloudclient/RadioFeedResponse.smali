.class public Lcom/google/android/music/cloudclient/RadioFeedResponse;
.super Lcom/google/api/client/json/GenericJson;
.source "RadioFeedResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/cloudclient/RadioFeedResponse$RadioData;
    }
.end annotation


# instance fields
.field public mRadioData:Lcom/google/android/music/cloudclient/RadioFeedResponse$RadioData;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "data"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    .line 17
    return-void
.end method


# virtual methods
.method public getFirstStationTracks()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/sync/google/model/Track;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 32
    iget-object v0, p0, Lcom/google/android/music/cloudclient/RadioFeedResponse;->mRadioData:Lcom/google/android/music/cloudclient/RadioFeedResponse$RadioData;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/cloudclient/RadioFeedResponse;->mRadioData:Lcom/google/android/music/cloudclient/RadioFeedResponse$RadioData;

    iget-object v0, v0, Lcom/google/android/music/cloudclient/RadioFeedResponse$RadioData;->mRadioStations:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/cloudclient/RadioFeedResponse;->mRadioData:Lcom/google/android/music/cloudclient/RadioFeedResponse$RadioData;

    iget-object v0, v0, Lcom/google/android/music/cloudclient/RadioFeedResponse$RadioData;->mRadioStations:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/music/cloudclient/RadioFeedResponse;->mRadioData:Lcom/google/android/music/cloudclient/RadioFeedResponse$RadioData;

    iget-object v0, v0, Lcom/google/android/music/cloudclient/RadioFeedResponse$RadioData;->mRadioStations:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 36
    iget-object v0, p0, Lcom/google/android/music/cloudclient/RadioFeedResponse;->mRadioData:Lcom/google/android/music/cloudclient/RadioFeedResponse$RadioData;

    iget-object v0, v0, Lcom/google/android/music/cloudclient/RadioFeedResponse$RadioData;->mRadioStations:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/sync/google/model/SyncableRadioStation;

    iget-object v0, v0, Lcom/google/android/music/sync/google/model/SyncableRadioStation;->mTracks:Ljava/util/List;

    .line 38
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
