.class public Lcom/google/android/music/cloudclient/CloudQueueManager;
.super Ljava/lang/Object;
.source "CloudQueueManager.java"


# static fields
.field static final LOGV:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->CLOUD_CLIENT:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/cloudclient/CloudQueueManager;->LOGV:Z

    return-void
.end method

.method public static clearCloudQueue(Landroid/content/Context;)Z
    .locals 10
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v6, 0x0

    .line 93
    invoke-static {p0}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v5

    .line 94
    .local v5, "store":Lcom/google/android/music/store/Store;
    new-instance v3, Lcom/google/android/music/cloudclient/SetCloudQueueRequestJson;

    invoke-direct {v3}, Lcom/google/android/music/cloudclient/SetCloudQueueRequestJson;-><init>()V

    .line 96
    .local v3, "request":Lcom/google/android/music/cloudclient/SetCloudQueueRequestJson;
    new-instance v2, Lcom/google/android/music/cloudclient/CloudQueueFlatQueueJson;

    invoke-direct {v2}, Lcom/google/android/music/cloudclient/CloudQueueFlatQueueJson;-><init>()V

    .line 97
    .local v2, "queue":Lcom/google/android/music/cloudclient/CloudQueueFlatQueueJson;
    iput-object v2, v3, Lcom/google/android/music/cloudclient/SetCloudQueueRequestJson;->mQueue:Lcom/google/android/music/cloudclient/CloudQueueFlatQueueJson;

    .line 98
    invoke-virtual {v5}, Lcom/google/android/music/store/Store;->getCurrentCloudQueueVersion()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    iput-object v7, v2, Lcom/google/android/music/cloudclient/CloudQueueFlatQueueJson;->mVersion:Ljava/lang/Long;

    .line 99
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v7

    iput-object v7, v2, Lcom/google/android/music/cloudclient/CloudQueueFlatQueueJson;->mContainers:Ljava/util/List;

    .line 100
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v7

    iput-object v7, v2, Lcom/google/android/music/cloudclient/CloudQueueFlatQueueJson;->mTracks:Ljava/util/List;

    .line 101
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v7

    iput-object v7, v2, Lcom/google/android/music/cloudclient/CloudQueueFlatQueueJson;->mItems:Ljava/util/List;

    .line 102
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v7

    iput-object v7, v2, Lcom/google/android/music/cloudclient/CloudQueueFlatQueueJson;->mItemIdsInUnshuffledOrder:Ljava/util/List;

    .line 104
    new-instance v0, Lcom/google/android/music/cloudclient/MusicCloudImpl;

    invoke-direct {v0, p0}, Lcom/google/android/music/cloudclient/MusicCloudImpl;-><init>(Landroid/content/Context;)V

    .line 107
    .local v0, "cloudClient":Lcom/google/android/music/cloudclient/MusicCloud;
    :try_start_0
    invoke-interface {v0, v3}, Lcom/google/android/music/cloudclient/MusicCloud;->setCloudQueue(Lcom/google/android/music/cloudclient/SetCloudQueueRequestJson;)Lcom/google/android/music/cloudclient/SetCloudQueueResponseJson;

    move-result-object v4

    .line 108
    .local v4, "response":Lcom/google/android/music/cloudclient/SetCloudQueueResponseJson;
    sget-boolean v7, Lcom/google/android/music/cloudclient/CloudQueueManager;->LOGV:Z

    if-eqz v7, :cond_0

    .line 109
    const-string v7, "CloudQueueManager"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Clear cloud queue returned new version: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, v4, Lcom/google/android/music/cloudclient/SetCloudQueueResponseJson;->mVersion:Ljava/lang/Long;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 111
    :cond_0
    const/4 v6, 0x1

    .line 117
    .end local v4    # "response":Lcom/google/android/music/cloudclient/SetCloudQueueResponseJson;
    :goto_0
    return v6

    .line 112
    :catch_0
    move-exception v1

    .line 113
    .local v1, "e":Ljava/lang/InterruptedException;
    const-string v7, "CloudQueueManager"

    invoke-virtual {v1}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 115
    .end local v1    # "e":Ljava/lang/InterruptedException;
    :catch_1
    move-exception v1

    .line 116
    .local v1, "e":Ljava/io/IOException;
    const-string v7, "CloudQueueManager"

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public static copyLocalQueueToCloudQueue(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 419
    invoke-static {p0}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v0

    .line 420
    .local v0, "store":Lcom/google/android/music/store/Store;
    invoke-virtual {v0}, Lcom/google/android/music/store/Store;->copyLocalQueueToCloudQueue()Z

    move-result v1

    return v1
.end method

.method public static getCloudQueue(Landroid/content/Context;)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 189
    invoke-static {p0}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v3

    .line 190
    .local v3, "store":Lcom/google/android/music/store/Store;
    new-instance v0, Lcom/google/android/music/cloudclient/MusicCloudImpl;

    invoke-direct {v0, p0}, Lcom/google/android/music/cloudclient/MusicCloudImpl;-><init>(Landroid/content/Context;)V

    .line 193
    .local v0, "cloudClient":Lcom/google/android/music/cloudclient/MusicCloud;
    :try_start_0
    invoke-interface {v0}, Lcom/google/android/music/cloudclient/MusicCloud;->getCloudQueue()Lcom/google/android/music/cloudclient/GetCloudQueueResponseJson;

    move-result-object v2

    .line 195
    .local v2, "response":Lcom/google/android/music/cloudclient/GetCloudQueueResponseJson;
    sget-boolean v4, Lcom/google/android/music/cloudclient/CloudQueueManager;->LOGV:Z

    if-eqz v4, :cond_0

    .line 196
    const-string v4, "CloudQueueManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Get Cloud Queue returned queue: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v2, Lcom/google/android/music/cloudclient/GetCloudQueueResponseJson;->mQueue:Lcom/google/android/music/cloudclient/CloudQueueFlatQueueJson;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 199
    :cond_0
    iget-object v4, v2, Lcom/google/android/music/cloudclient/GetCloudQueueResponseJson;->mQueue:Lcom/google/android/music/cloudclient/CloudQueueFlatQueueJson;

    if-nez v4, :cond_1

    .line 200
    const-string v4, "CloudQueueManager"

    const-string v5, "Cannot update local copy of cloud queue, since GetCloudQueueResponse returned a null queue."

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 243
    .end local v2    # "response":Lcom/google/android/music/cloudclient/GetCloudQueueResponseJson;
    :goto_0
    return-void

    .line 205
    .restart local v2    # "response":Lcom/google/android/music/cloudclient/GetCloudQueueResponseJson;
    :cond_1
    iget-object v4, v2, Lcom/google/android/music/cloudclient/GetCloudQueueResponseJson;->mQueue:Lcom/google/android/music/cloudclient/CloudQueueFlatQueueJson;

    iget-object v4, v4, Lcom/google/android/music/cloudclient/CloudQueueFlatQueueJson;->mItems:Ljava/util/List;

    if-nez v4, :cond_2

    .line 206
    const-string v4, "CloudQueueManager"

    const-string v5, "Cannot update local copy of cloud queue, since GetCloudQueueResponse returned a null list of items."

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 238
    .end local v2    # "response":Lcom/google/android/music/cloudclient/GetCloudQueueResponseJson;
    :catch_0
    move-exception v1

    .line 239
    .local v1, "e":Ljava/lang/InterruptedException;
    const-string v4, "CloudQueueManager"

    invoke-virtual {v1}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 211
    .end local v1    # "e":Ljava/lang/InterruptedException;
    .restart local v2    # "response":Lcom/google/android/music/cloudclient/GetCloudQueueResponseJson;
    :cond_2
    :try_start_1
    iget-object v4, v2, Lcom/google/android/music/cloudclient/GetCloudQueueResponseJson;->mQueue:Lcom/google/android/music/cloudclient/CloudQueueFlatQueueJson;

    iget-object v4, v4, Lcom/google/android/music/cloudclient/CloudQueueFlatQueueJson;->mItemIdsInUnshuffledOrder:Ljava/util/List;

    if-nez v4, :cond_3

    .line 212
    const-string v4, "CloudQueueManager"

    const-string v5, "Cannot update local copy of cloud queue, since GetCloudQueueResponse returned a null list of unshuffled item id orderings."

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 240
    .end local v2    # "response":Lcom/google/android/music/cloudclient/GetCloudQueueResponseJson;
    :catch_1
    move-exception v1

    .line 241
    .local v1, "e":Ljava/io/IOException;
    const-string v4, "CloudQueueManager"

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 217
    .end local v1    # "e":Ljava/io/IOException;
    .restart local v2    # "response":Lcom/google/android/music/cloudclient/GetCloudQueueResponseJson;
    :cond_3
    :try_start_2
    iget-object v4, v2, Lcom/google/android/music/cloudclient/GetCloudQueueResponseJson;->mQueue:Lcom/google/android/music/cloudclient/CloudQueueFlatQueueJson;

    iget-object v4, v4, Lcom/google/android/music/cloudclient/CloudQueueFlatQueueJson;->mTracks:Ljava/util/List;

    if-nez v4, :cond_4

    .line 218
    const-string v4, "CloudQueueManager"

    const-string v5, "Cannot update local copy of cloud queue, since GetCloudQueueResponse returned a null list of tracks."

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 223
    :cond_4
    iget-object v4, v2, Lcom/google/android/music/cloudclient/GetCloudQueueResponseJson;->mQueue:Lcom/google/android/music/cloudclient/CloudQueueFlatQueueJson;

    iget-object v4, v4, Lcom/google/android/music/cloudclient/CloudQueueFlatQueueJson;->mContainers:Ljava/util/List;

    if-nez v4, :cond_5

    .line 224
    const-string v4, "CloudQueueManager"

    const-string v5, "Cannot update local copy of cloud queue, since GetCloudQueueResponse returned a null list of containers."

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 229
    :cond_5
    iget-object v4, v2, Lcom/google/android/music/cloudclient/GetCloudQueueResponseJson;->mQueue:Lcom/google/android/music/cloudclient/CloudQueueFlatQueueJson;

    iget-object v4, v4, Lcom/google/android/music/cloudclient/CloudQueueFlatQueueJson;->mSettings:Lcom/google/android/music/cloudclient/CloudQueuePlaySettingsJson;

    if-nez v4, :cond_6

    .line 230
    const-string v4, "CloudQueueManager"

    const-string v5, "Cannot update local copy of cloud queue, since GetCloudQueueResponse returned null play settings."

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 235
    :cond_6
    iget-object v4, v2, Lcom/google/android/music/cloudclient/GetCloudQueueResponseJson;->mQueue:Lcom/google/android/music/cloudclient/CloudQueueFlatQueueJson;

    invoke-static {p0}, Lcom/google/android/music/cloudclient/CloudQueueManager;->getSourceAccount(Landroid/content/Context;)I

    move-result v5

    invoke-virtual {v3, v4, v5}, Lcom/google/android/music/store/Store;->setCloudQueue(Lcom/google/android/music/cloudclient/CloudQueueFlatQueueJson;I)V

    .line 236
    iget-object v4, v2, Lcom/google/android/music/cloudclient/GetCloudQueueResponseJson;->mQueue:Lcom/google/android/music/cloudclient/CloudQueueFlatQueueJson;

    iget-object v4, v4, Lcom/google/android/music/cloudclient/CloudQueueFlatQueueJson;->mSettings:Lcom/google/android/music/cloudclient/CloudQueuePlaySettingsJson;

    invoke-static {v3, v4}, Lcom/google/android/music/cloudclient/CloudQueueManager;->updatePlayModeState(Lcom/google/android/music/store/Store;Lcom/google/android/music/cloudclient/CloudQueuePlaySettingsJson;)V

    .line 237
    iget-object v4, v2, Lcom/google/android/music/cloudclient/GetCloudQueueResponseJson;->mQueue:Lcom/google/android/music/cloudclient/CloudQueueFlatQueueJson;

    iget-object v4, v4, Lcom/google/android/music/cloudclient/CloudQueueFlatQueueJson;->mSettings:Lcom/google/android/music/cloudclient/CloudQueuePlaySettingsJson;

    iget v4, v4, Lcom/google/android/music/cloudclient/CloudQueuePlaySettingsJson;->mContentFilter:I

    invoke-static {p0, v4}, Lcom/google/android/music/cloudclient/CloudQueueManager;->setContentFilter(Landroid/content/Context;I)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0
.end method

.method private static getContentFilter(Landroid/content/Context;)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 485
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    .line 486
    .local v1, "refObject":Ljava/lang/Object;
    invoke-static {p0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    .line 488
    .local v0, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    :try_start_0
    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->getContentFilter()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    .line 490
    invoke-static {v1}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    return v2

    :catchall_0
    move-exception v2

    invoke-static {v1}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    throw v2
.end method

.method private static getCurrentPlayMode(Lcom/google/android/music/store/Store;)I
    .locals 10
    .param p0, "store"    # Lcom/google/android/music/store/Store;

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 427
    const-wide/16 v6, 0x0

    const-wide/16 v8, 0x1

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {p0, v6, v7, v8}, Lcom/google/android/music/store/Store;->getCloudQueueContainers(JLjava/lang/Long;)Ljava/util/List;

    move-result-object v2

    .line 429
    .local v2, "containers":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/cloudclient/CloudQueueContainerJson;>;"
    if-eqz v2, :cond_1

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_1

    .line 430
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/cloudclient/CloudQueueContainerJson;

    .line 431
    .local v0, "container":Lcom/google/android/music/cloudclient/CloudQueueContainerJson;
    iget v1, v0, Lcom/google/android/music/cloudclient/CloudQueueContainerJson;->mType:I

    .line 432
    .local v1, "containerType":I
    const/16 v6, 0xe

    if-ne v1, v6, :cond_1

    .line 433
    const/4 v4, 0x4

    .line 446
    .end local v0    # "container":Lcom/google/android/music/cloudclient/CloudQueueContainerJson;
    .end local v1    # "containerType":I
    :cond_0
    :goto_0
    return v4

    .line 437
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/music/store/Store;->getCloudQueueRepeatMode()I

    move-result v3

    .line 438
    .local v3, "repeatMode":I
    if-eqz v3, :cond_0

    .line 440
    if-ne v3, v4, :cond_2

    move v4, v5

    .line 441
    goto :goto_0

    .line 442
    :cond_2
    if-ne v3, v5, :cond_3

    .line 443
    const/4 v4, 0x3

    goto :goto_0

    .line 445
    :cond_3
    const-string v4, "CloudQueueManager"

    const-string v5, "Couldn\'t deduce current play mode."

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 446
    const/4 v4, 0x0

    goto :goto_0
.end method

.method private static getCurrentPlayingContainerKey(Lcom/google/android/music/store/Store;)Ljava/lang/String;
    .locals 6
    .param p0, "store"    # Lcom/google/android/music/store/Store;

    .prologue
    .line 451
    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x1

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {p0, v2, v3, v4}, Lcom/google/android/music/store/Store;->getCloudQueueContainers(JLjava/lang/Long;)Ljava/util/List;

    move-result-object v1

    .line 453
    .local v1, "containers":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/cloudclient/CloudQueueContainerJson;>;"
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 454
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/cloudclient/CloudQueueContainerJson;

    .line 455
    .local v0, "container":Lcom/google/android/music/cloudclient/CloudQueueContainerJson;
    iget-object v2, v0, Lcom/google/android/music/cloudclient/CloudQueueContainerJson;->mKey:Ljava/lang/String;

    .line 458
    .end local v0    # "container":Lcom/google/android/music/cloudclient/CloudQueueContainerJson;
    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private static getSourceAccount(Landroid/content/Context;)I
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 246
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    .line 248
    .local v1, "owner":Ljava/lang/Object;
    invoke-static {p0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    .line 251
    .local v0, "musicPreferences":Lcom/google/android/music/preferences/MusicPreferences;
    :try_start_0
    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->getStreamingAccount()Landroid/accounts/Account;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 253
    .local v2, "streamingAccount":Landroid/accounts/Account;
    invoke-static {v1}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    .line 256
    if-nez v2, :cond_0

    .line 257
    const/4 v3, 0x0

    .line 260
    :goto_0
    return v3

    .line 253
    .end local v2    # "streamingAccount":Landroid/accounts/Account;
    :catchall_0
    move-exception v3

    invoke-static {v1}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    throw v3

    .line 260
    .restart local v2    # "streamingAccount":Landroid/accounts/Account;
    :cond_0
    invoke-static {v2}, Lcom/google/android/music/store/Store;->computeAccountHash(Landroid/accounts/Account;)I

    move-result v3

    goto :goto_0
.end method

.method public static setCloudQueue(Landroid/content/Context;)V
    .locals 13
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v6, 0x0

    const-wide/16 v2, 0x0

    .line 36
    invoke-static {p0}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v1

    .line 37
    .local v1, "store":Lcom/google/android/music/store/Store;
    new-instance v11, Lcom/google/android/music/cloudclient/SetCloudQueueRequestJson;

    invoke-direct {v11}, Lcom/google/android/music/cloudclient/SetCloudQueueRequestJson;-><init>()V

    .line 38
    .local v11, "request":Lcom/google/android/music/cloudclient/SetCloudQueueRequestJson;
    invoke-virtual {v1}, Lcom/google/android/music/store/Store;->getCurrentCloudQueueVersion()J

    move-result-wide v8

    .line 40
    .local v8, "currentCloudQueueVersion":J
    new-instance v10, Lcom/google/android/music/cloudclient/CloudQueueFlatQueueJson;

    invoke-direct {v10}, Lcom/google/android/music/cloudclient/CloudQueueFlatQueueJson;-><init>()V

    .line 41
    .local v10, "queue":Lcom/google/android/music/cloudclient/CloudQueueFlatQueueJson;
    iput-object v10, v11, Lcom/google/android/music/cloudclient/SetCloudQueueRequestJson;->mQueue:Lcom/google/android/music/cloudclient/CloudQueueFlatQueueJson;

    .line 42
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    iput-object v4, v10, Lcom/google/android/music/cloudclient/CloudQueueFlatQueueJson;->mVersion:Ljava/lang/Long;

    .line 43
    invoke-virtual {v1, v2, v3, v6}, Lcom/google/android/music/store/Store;->getCloudQueueContainers(JLjava/lang/Long;)Ljava/util/List;

    move-result-object v4

    iput-object v4, v10, Lcom/google/android/music/cloudclient/CloudQueueFlatQueueJson;->mContainers:Ljava/util/List;

    .line 44
    invoke-virtual {v1, v2, v3, v6}, Lcom/google/android/music/store/Store;->getCloudQueueTracks(JLjava/lang/Long;)Ljava/util/List;

    move-result-object v4

    iput-object v4, v10, Lcom/google/android/music/cloudclient/CloudQueueFlatQueueJson;->mTracks:Ljava/util/List;

    move-wide v4, v2

    .line 45
    invoke-virtual/range {v1 .. v6}, Lcom/google/android/music/store/Store;->getCloudQueueItems(JJLjava/lang/Long;)Ljava/util/List;

    move-result-object v2

    iput-object v2, v10, Lcom/google/android/music/cloudclient/CloudQueueFlatQueueJson;->mItems:Ljava/util/List;

    .line 48
    new-instance v2, Lcom/google/android/music/cloudclient/CloudQueuePlaySettingsJson;

    invoke-direct {v2}, Lcom/google/android/music/cloudclient/CloudQueuePlaySettingsJson;-><init>()V

    iput-object v2, v10, Lcom/google/android/music/cloudclient/CloudQueueFlatQueueJson;->mSettings:Lcom/google/android/music/cloudclient/CloudQueuePlaySettingsJson;

    .line 49
    iget-object v3, v10, Lcom/google/android/music/cloudclient/CloudQueueFlatQueueJson;->mSettings:Lcom/google/android/music/cloudclient/CloudQueuePlaySettingsJson;

    invoke-virtual {v1}, Lcom/google/android/music/store/Store;->getCloudQueueShuffleMode()I

    move-result v2

    if-nez v2, :cond_2

    const/4 v2, 0x0

    :goto_0
    iput-boolean v2, v3, Lcom/google/android/music/cloudclient/CloudQueuePlaySettingsJson;->mShuffled:Z

    .line 51
    iget-object v2, v10, Lcom/google/android/music/cloudclient/CloudQueueFlatQueueJson;->mSettings:Lcom/google/android/music/cloudclient/CloudQueuePlaySettingsJson;

    invoke-static {v1}, Lcom/google/android/music/cloudclient/CloudQueueManager;->getCurrentPlayMode(Lcom/google/android/music/store/Store;)I

    move-result v3

    iput v3, v2, Lcom/google/android/music/cloudclient/CloudQueuePlaySettingsJson;->mMode:I

    .line 53
    iget-object v2, v10, Lcom/google/android/music/cloudclient/CloudQueueFlatQueueJson;->mSettings:Lcom/google/android/music/cloudclient/CloudQueuePlaySettingsJson;

    iget-boolean v2, v2, Lcom/google/android/music/cloudclient/CloudQueuePlaySettingsJson;->mShuffled:Z

    if-eqz v2, :cond_3

    .line 54
    invoke-virtual {v1}, Lcom/google/android/music/store/Store;->getCloudQueueItemIdsSortedByItemUnshuffledOrder()Ljava/util/List;

    move-result-object v2

    iput-object v2, v10, Lcom/google/android/music/cloudclient/CloudQueueFlatQueueJson;->mItemIdsInUnshuffledOrder:Ljava/util/List;

    .line 56
    invoke-virtual {v1}, Lcom/google/android/music/store/Store;->getCloudQueueItemIdsSortedByItemOrder()Ljava/util/List;

    move-result-object v2

    iput-object v2, v10, Lcom/google/android/music/cloudclient/CloudQueueFlatQueueJson;->mItemIdsInShuffledOrder:Ljava/util/List;

    .line 61
    :goto_1
    iget-object v2, v10, Lcom/google/android/music/cloudclient/CloudQueueFlatQueueJson;->mSettings:Lcom/google/android/music/cloudclient/CloudQueuePlaySettingsJson;

    iget v2, v2, Lcom/google/android/music/cloudclient/CloudQueuePlaySettingsJson;->mMode:I

    const/4 v3, 0x4

    if-ne v2, v3, :cond_0

    .line 62
    iget-object v2, v10, Lcom/google/android/music/cloudclient/CloudQueueFlatQueueJson;->mSettings:Lcom/google/android/music/cloudclient/CloudQueuePlaySettingsJson;

    invoke-static {v1}, Lcom/google/android/music/cloudclient/CloudQueueManager;->getCurrentPlayingContainerKey(Lcom/google/android/music/store/Store;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/music/cloudclient/CloudQueuePlaySettingsJson;->mRadioContainerKey:Ljava/lang/String;

    .line 65
    :cond_0
    iget-object v2, v10, Lcom/google/android/music/cloudclient/CloudQueueFlatQueueJson;->mSettings:Lcom/google/android/music/cloudclient/CloudQueuePlaySettingsJson;

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/music/cloudclient/CloudQueuePlaySettingsJson;->mVersion:Ljava/lang/Long;

    .line 66
    iget-object v2, v10, Lcom/google/android/music/cloudclient/CloudQueueFlatQueueJson;->mSettings:Lcom/google/android/music/cloudclient/CloudQueuePlaySettingsJson;

    invoke-static {p0}, Lcom/google/android/music/cloudclient/CloudQueueManager;->getContentFilter(Landroid/content/Context;)I

    move-result v3

    iput v3, v2, Lcom/google/android/music/cloudclient/CloudQueuePlaySettingsJson;->mContentFilter:I

    .line 68
    new-instance v0, Lcom/google/android/music/cloudclient/MusicCloudImpl;

    invoke-direct {v0, p0}, Lcom/google/android/music/cloudclient/MusicCloudImpl;-><init>(Landroid/content/Context;)V

    .line 71
    .local v0, "cloudClient":Lcom/google/android/music/cloudclient/MusicCloud;
    :try_start_0
    invoke-interface {v0, v11}, Lcom/google/android/music/cloudclient/MusicCloud;->setCloudQueue(Lcom/google/android/music/cloudclient/SetCloudQueueRequestJson;)Lcom/google/android/music/cloudclient/SetCloudQueueResponseJson;

    move-result-object v12

    .line 73
    .local v12, "response":Lcom/google/android/music/cloudclient/SetCloudQueueResponseJson;
    sget-boolean v2, Lcom/google/android/music/cloudclient/CloudQueueManager;->LOGV:Z

    if-eqz v2, :cond_1

    .line 74
    const-string v2, "CloudQueueManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Cloud Queue Server reports new version: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v12, Lcom/google/android/music/cloudclient/SetCloudQueueResponseJson;->mVersion:Ljava/lang/Long;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 76
    :cond_1
    iget-object v2, v12, Lcom/google/android/music/cloudclient/SetCloudQueueResponseJson;->mVersion:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/google/android/music/store/Store;->updateCloudQueueItemVersions(J)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    .line 82
    .end local v12    # "response":Lcom/google/android/music/cloudclient/SetCloudQueueResponseJson;
    :goto_2
    return-void

    .line 49
    .end local v0    # "cloudClient":Lcom/google/android/music/cloudclient/MusicCloud;
    :cond_2
    const/4 v2, 0x1

    goto :goto_0

    .line 58
    :cond_3
    invoke-virtual {v1}, Lcom/google/android/music/store/Store;->getCloudQueueItemIdsSortedByItemOrder()Ljava/util/List;

    move-result-object v2

    iput-object v2, v10, Lcom/google/android/music/cloudclient/CloudQueueFlatQueueJson;->mItemIdsInUnshuffledOrder:Ljava/util/List;

    goto :goto_1

    .line 77
    .restart local v0    # "cloudClient":Lcom/google/android/music/cloudclient/MusicCloud;
    :catch_0
    move-exception v7

    .line 78
    .local v7, "e":Ljava/io/IOException;
    const-string v2, "CloudQueueManager"

    invoke-virtual {v7}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    .line 79
    .end local v7    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v7

    .line 80
    .local v7, "e":Ljava/lang/InterruptedException;
    const-string v2, "CloudQueueManager"

    invoke-virtual {v7}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2
.end method

.method private static setContentFilter(Landroid/content/Context;I)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "contentFilter"    # I

    .prologue
    .line 495
    if-nez p1, :cond_0

    .line 496
    const-string v2, "CloudQueueManager"

    const-string v3, "Cannot set content filter to unknown value."

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 507
    :goto_0
    return-void

    .line 500
    :cond_0
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    .line 501
    .local v1, "refObject":Ljava/lang/Object;
    invoke-static {p0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    .line 503
    .local v0, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    :try_start_0
    invoke-virtual {v0, p1}, Lcom/google/android/music/preferences/MusicPreferences;->setContentFilter(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 505
    invoke-static {v1}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    goto :goto_0

    :catchall_0
    move-exception v2

    invoke-static {v1}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    throw v2
.end method

.method public static setRepeatShuffleMode(Landroid/content/Context;)V
    .locals 10
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 373
    invoke-static {p0}, Lcom/google/android/music/utils/MusicUtils;->isDifferentialSyncEnabled(Landroid/content/Context;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 374
    invoke-static {p0}, Lcom/google/android/music/cloudclient/CloudQueueManager;->setCloudQueue(Landroid/content/Context;)V

    .line 410
    :goto_0
    return-void

    .line 378
    :cond_0
    invoke-static {p0}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v6

    .line 379
    .local v6, "store":Lcom/google/android/music/store/Store;
    new-instance v0, Lcom/google/android/music/cloudclient/MusicCloudImpl;

    invoke-direct {v0, p0}, Lcom/google/android/music/cloudclient/MusicCloudImpl;-><init>(Landroid/content/Context;)V

    .line 380
    .local v0, "cloudClient":Lcom/google/android/music/cloudclient/MusicCloud;
    invoke-virtual {v6}, Lcom/google/android/music/store/Store;->getCurrentCloudQueueVersion()J

    move-result-wide v2

    .line 382
    .local v2, "currentCloudQueueVersion":J
    new-instance v4, Lcom/google/android/music/cloudclient/UpdateCloudQueueRequestJson;

    invoke-direct {v4}, Lcom/google/android/music/cloudclient/UpdateCloudQueueRequestJson;-><init>()V

    .line 383
    .local v4, "request":Lcom/google/android/music/cloudclient/UpdateCloudQueueRequestJson;
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v7

    iput-object v7, v4, Lcom/google/android/music/cloudclient/UpdateCloudQueueRequestJson;->mContainers:Ljava/util/List;

    .line 384
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v7

    iput-object v7, v4, Lcom/google/android/music/cloudclient/UpdateCloudQueueRequestJson;->mTracks:Ljava/util/List;

    .line 385
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v7

    iput-object v7, v4, Lcom/google/android/music/cloudclient/UpdateCloudQueueRequestJson;->mItems:Ljava/util/List;

    .line 386
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    iput-object v7, v4, Lcom/google/android/music/cloudclient/UpdateCloudQueueRequestJson;->mSenderVersion:Ljava/lang/Long;

    .line 388
    new-instance v7, Lcom/google/android/music/cloudclient/CloudQueuePlaySettingsJson;

    invoke-direct {v7}, Lcom/google/android/music/cloudclient/CloudQueuePlaySettingsJson;-><init>()V

    iput-object v7, v4, Lcom/google/android/music/cloudclient/UpdateCloudQueueRequestJson;->mSettings:Lcom/google/android/music/cloudclient/CloudQueuePlaySettingsJson;

    .line 389
    iget-object v8, v4, Lcom/google/android/music/cloudclient/UpdateCloudQueueRequestJson;->mSettings:Lcom/google/android/music/cloudclient/CloudQueuePlaySettingsJson;

    invoke-virtual {v6}, Lcom/google/android/music/store/Store;->getCloudQueueShuffleMode()I

    move-result v7

    if-nez v7, :cond_3

    const/4 v7, 0x0

    :goto_1
    iput-boolean v7, v8, Lcom/google/android/music/cloudclient/CloudQueuePlaySettingsJson;->mShuffled:Z

    .line 390
    iget-object v7, v4, Lcom/google/android/music/cloudclient/UpdateCloudQueueRequestJson;->mSettings:Lcom/google/android/music/cloudclient/CloudQueuePlaySettingsJson;

    invoke-static {v6}, Lcom/google/android/music/cloudclient/CloudQueueManager;->getCurrentPlayMode(Lcom/google/android/music/store/Store;)I

    move-result v8

    iput v8, v7, Lcom/google/android/music/cloudclient/CloudQueuePlaySettingsJson;->mMode:I

    .line 392
    iget-object v7, v4, Lcom/google/android/music/cloudclient/UpdateCloudQueueRequestJson;->mSettings:Lcom/google/android/music/cloudclient/CloudQueuePlaySettingsJson;

    iget v7, v7, Lcom/google/android/music/cloudclient/CloudQueuePlaySettingsJson;->mMode:I

    const/4 v8, 0x4

    if-ne v7, v8, :cond_1

    .line 393
    iget-object v7, v4, Lcom/google/android/music/cloudclient/UpdateCloudQueueRequestJson;->mSettings:Lcom/google/android/music/cloudclient/CloudQueuePlaySettingsJson;

    invoke-static {v6}, Lcom/google/android/music/cloudclient/CloudQueueManager;->getCurrentPlayingContainerKey(Lcom/google/android/music/store/Store;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v7, Lcom/google/android/music/cloudclient/CloudQueuePlaySettingsJson;->mRadioContainerKey:Ljava/lang/String;

    .line 396
    :cond_1
    iget-object v7, v4, Lcom/google/android/music/cloudclient/UpdateCloudQueueRequestJson;->mSettings:Lcom/google/android/music/cloudclient/CloudQueuePlaySettingsJson;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    iput-object v8, v7, Lcom/google/android/music/cloudclient/CloudQueuePlaySettingsJson;->mVersion:Ljava/lang/Long;

    .line 397
    iget-object v7, v4, Lcom/google/android/music/cloudclient/UpdateCloudQueueRequestJson;->mSettings:Lcom/google/android/music/cloudclient/CloudQueuePlaySettingsJson;

    invoke-static {p0}, Lcom/google/android/music/cloudclient/CloudQueueManager;->getContentFilter(Landroid/content/Context;)I

    move-result v8

    iput v8, v7, Lcom/google/android/music/cloudclient/CloudQueuePlaySettingsJson;->mContentFilter:I

    .line 400
    :try_start_0
    invoke-interface {v0, v4}, Lcom/google/android/music/cloudclient/MusicCloud;->updateCloudQueue(Lcom/google/android/music/cloudclient/UpdateCloudQueueRequestJson;)Lcom/google/android/music/cloudclient/UpdateCloudQueueResponseJson;

    move-result-object v5

    .line 401
    .local v5, "response":Lcom/google/android/music/cloudclient/UpdateCloudQueueResponseJson;
    sget-boolean v7, Lcom/google/android/music/cloudclient/CloudQueueManager;->LOGV:Z

    if-eqz v7, :cond_2

    .line 402
    const-string v7, "CloudQueueManager"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Update cloud queue returned version: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, v5, Lcom/google/android/music/cloudclient/UpdateCloudQueueResponseJson;->mVersion:Ljava/lang/Long;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 404
    :cond_2
    iget-object v7, v5, Lcom/google/android/music/cloudclient/UpdateCloudQueueResponseJson;->mVersion:Ljava/lang/Long;

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Lcom/google/android/music/store/Store;->setLatestCloudQueueVersion(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto/16 :goto_0

    .line 405
    .end local v5    # "response":Lcom/google/android/music/cloudclient/UpdateCloudQueueResponseJson;
    :catch_0
    move-exception v1

    .line 406
    .local v1, "e":Ljava/lang/InterruptedException;
    const-string v7, "CloudQueueManager"

    invoke-virtual {v1}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0

    .line 389
    .end local v1    # "e":Ljava/lang/InterruptedException;
    :cond_3
    const/4 v7, 0x1

    goto :goto_1

    .line 407
    :catch_1
    move-exception v1

    .line 408
    .local v1, "e":Ljava/io/IOException;
    const-string v7, "CloudQueueManager"

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0
.end method

.method public static syncCloudQueue(Landroid/content/Context;)V
    .locals 10
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 126
    invoke-static {p0}, Lcom/google/android/music/utils/MusicUtils;->isDifferentialSyncEnabled(Landroid/content/Context;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 127
    invoke-static {p0}, Lcom/google/android/music/cloudclient/CloudQueueManager;->getCloudQueue(Landroid/content/Context;)V

    .line 179
    :cond_0
    :goto_0
    return-void

    .line 131
    :cond_1
    invoke-static {p0}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v6

    .line 132
    .local v6, "store":Lcom/google/android/music/store/Store;
    new-instance v0, Lcom/google/android/music/cloudclient/MusicCloudImpl;

    invoke-direct {v0, p0}, Lcom/google/android/music/cloudclient/MusicCloudImpl;-><init>(Landroid/content/Context;)V

    .line 134
    .local v0, "cloudClient":Lcom/google/android/music/cloudclient/MusicCloud;
    new-instance v3, Lcom/google/android/music/cloudclient/SyncCloudQueueRequestJson;

    invoke-direct {v3}, Lcom/google/android/music/cloudclient/SyncCloudQueueRequestJson;-><init>()V

    .line 135
    .local v3, "request":Lcom/google/android/music/cloudclient/SyncCloudQueueRequestJson;
    invoke-virtual {v6}, Lcom/google/android/music/store/Store;->getCurrentCloudQueueVersion()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    iput-object v7, v3, Lcom/google/android/music/cloudclient/SyncCloudQueueRequestJson;->mVersion:Ljava/lang/Long;

    .line 138
    :try_start_0
    invoke-interface {v0, v3}, Lcom/google/android/music/cloudclient/MusicCloud;->syncCloudQueue(Lcom/google/android/music/cloudclient/SyncCloudQueueRequestJson;)Lcom/google/android/music/cloudclient/SyncCloudQueueResponseJson;

    move-result-object v4

    .line 140
    .local v4, "response":Lcom/google/android/music/cloudclient/SyncCloudQueueResponseJson;
    sget-boolean v7, Lcom/google/android/music/cloudclient/CloudQueueManager;->LOGV:Z

    if-eqz v7, :cond_2

    .line 141
    const-string v7, "CloudQueueManager"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Sync cloud queue returned version: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, v4, Lcom/google/android/music/cloudclient/SyncCloudQueueResponseJson;->mVersion:Ljava/lang/Long;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 142
    const-string v7, "CloudQueueManager"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Sync cloud queue returned items: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, v4, Lcom/google/android/music/cloudclient/SyncCloudQueueResponseJson;->mItems:Ljava/util/List;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 145
    :cond_2
    iget-object v7, v3, Lcom/google/android/music/cloudclient/SyncCloudQueueRequestJson;->mVersion:Ljava/lang/Long;

    iget-object v8, v4, Lcom/google/android/music/cloudclient/SyncCloudQueueResponseJson;->mVersion:Ljava/lang/Long;

    invoke-virtual {v7, v8}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 146
    sget-boolean v7, Lcom/google/android/music/cloudclient/CloudQueueManager;->LOGV:Z

    if-eqz v7, :cond_0

    .line 147
    const-string v7, "CloudQueueManager"

    const-string v8, "Already at current version, no diffs to apply."

    invoke-static {v7, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 171
    .end local v4    # "response":Lcom/google/android/music/cloudclient/SyncCloudQueueResponseJson;
    :catch_0
    move-exception v1

    .line 172
    .local v1, "e":Ljava/lang/InterruptedException;
    const-string v7, "CloudQueueManager"

    invoke-virtual {v1}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 152
    .end local v1    # "e":Ljava/lang/InterruptedException;
    .restart local v4    # "response":Lcom/google/android/music/cloudclient/SyncCloudQueueResponseJson;
    :cond_3
    :try_start_1
    invoke-static {p0}, Lcom/google/android/music/cloudclient/CloudQueueManager;->getSourceAccount(Landroid/content/Context;)I

    move-result v5

    .line 153
    .local v5, "sourceAccount":I
    new-instance v2, Lcom/google/android/music/cloudclient/CloudQueueFlatQueueJson;

    invoke-direct {v2}, Lcom/google/android/music/cloudclient/CloudQueueFlatQueueJson;-><init>()V

    .line 154
    .local v2, "queue":Lcom/google/android/music/cloudclient/CloudQueueFlatQueueJson;
    iget-object v7, v4, Lcom/google/android/music/cloudclient/SyncCloudQueueResponseJson;->mTracks:Ljava/util/List;

    iput-object v7, v2, Lcom/google/android/music/cloudclient/CloudQueueFlatQueueJson;->mTracks:Ljava/util/List;

    .line 155
    iget-object v7, v4, Lcom/google/android/music/cloudclient/SyncCloudQueueResponseJson;->mContainers:Ljava/util/List;

    iput-object v7, v2, Lcom/google/android/music/cloudclient/CloudQueueFlatQueueJson;->mContainers:Ljava/util/List;

    .line 156
    iget-object v7, v4, Lcom/google/android/music/cloudclient/SyncCloudQueueResponseJson;->mItems:Ljava/util/List;

    iput-object v7, v2, Lcom/google/android/music/cloudclient/CloudQueueFlatQueueJson;->mItems:Ljava/util/List;

    .line 157
    iget-object v7, v4, Lcom/google/android/music/cloudclient/SyncCloudQueueResponseJson;->mSettings:Lcom/google/android/music/cloudclient/CloudQueuePlaySettingsJson;

    iput-object v7, v2, Lcom/google/android/music/cloudclient/CloudQueueFlatQueueJson;->mSettings:Lcom/google/android/music/cloudclient/CloudQueuePlaySettingsJson;

    .line 159
    iget-boolean v7, v4, Lcom/google/android/music/cloudclient/SyncCloudQueueResponseJson;->mIncremental:Z

    if-eqz v7, :cond_5

    .line 160
    iget-object v7, v4, Lcom/google/android/music/cloudclient/SyncCloudQueueResponseJson;->mItemIdsToDelete:Ljava/util/List;

    invoke-virtual {v6, v2, v5, v7}, Lcom/google/android/music/store/Store;->updateCloudQueue(Lcom/google/android/music/cloudclient/CloudQueueFlatQueueJson;ILjava/util/List;)V

    .line 165
    :goto_1
    iget-object v7, v4, Lcom/google/android/music/cloudclient/SyncCloudQueueResponseJson;->mSettings:Lcom/google/android/music/cloudclient/CloudQueuePlaySettingsJson;

    if-eqz v7, :cond_4

    .line 166
    iget-object v7, v4, Lcom/google/android/music/cloudclient/SyncCloudQueueResponseJson;->mSettings:Lcom/google/android/music/cloudclient/CloudQueuePlaySettingsJson;

    invoke-static {v6, v7}, Lcom/google/android/music/cloudclient/CloudQueueManager;->updatePlayModeState(Lcom/google/android/music/store/Store;Lcom/google/android/music/cloudclient/CloudQueuePlaySettingsJson;)V

    .line 167
    iget-object v7, v4, Lcom/google/android/music/cloudclient/SyncCloudQueueResponseJson;->mSettings:Lcom/google/android/music/cloudclient/CloudQueuePlaySettingsJson;

    iget v7, v7, Lcom/google/android/music/cloudclient/CloudQueuePlaySettingsJson;->mContentFilter:I

    invoke-static {p0, v7}, Lcom/google/android/music/cloudclient/CloudQueueManager;->setContentFilter(Landroid/content/Context;I)V

    .line 170
    :cond_4
    iget-object v7, v4, Lcom/google/android/music/cloudclient/SyncCloudQueueResponseJson;->mVersion:Ljava/lang/Long;

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Lcom/google/android/music/store/Store;->setLatestCloudQueueVersion(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    .line 173
    .end local v2    # "queue":Lcom/google/android/music/cloudclient/CloudQueueFlatQueueJson;
    .end local v4    # "response":Lcom/google/android/music/cloudclient/SyncCloudQueueResponseJson;
    .end local v5    # "sourceAccount":I
    :catch_1
    move-exception v1

    .line 176
    .local v1, "e":Ljava/io/IOException;
    const-string v7, "CloudQueueManager"

    const-string v8, "Unable to sync with cloud queue, so getting entire queue."

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 177
    invoke-static {p0}, Lcom/google/android/music/cloudclient/CloudQueueManager;->getCloudQueue(Landroid/content/Context;)V

    goto/16 :goto_0

    .line 162
    .end local v1    # "e":Ljava/io/IOException;
    .restart local v2    # "queue":Lcom/google/android/music/cloudclient/CloudQueueFlatQueueJson;
    .restart local v4    # "response":Lcom/google/android/music/cloudclient/SyncCloudQueueResponseJson;
    .restart local v5    # "sourceAccount":I
    :cond_5
    :try_start_2
    invoke-virtual {v6, v2, v5}, Lcom/google/android/music/store/Store;->setCloudQueue(Lcom/google/android/music/cloudclient/CloudQueueFlatQueueJson;I)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1
.end method

.method public static updateCloudQueue(Landroid/content/Context;)V
    .locals 10
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v6, 0x0

    .line 264
    invoke-static {p0}, Lcom/google/android/music/utils/MusicUtils;->isDifferentialSyncEnabled(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 265
    invoke-static {p0}, Lcom/google/android/music/cloudclient/CloudQueueManager;->setCloudQueue(Landroid/content/Context;)V

    .line 291
    :cond_0
    :goto_0
    return-void

    .line 269
    :cond_1
    invoke-static {p0}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v1

    .line 270
    .local v1, "store":Lcom/google/android/music/store/Store;
    new-instance v0, Lcom/google/android/music/cloudclient/MusicCloudImpl;

    invoke-direct {v0, p0}, Lcom/google/android/music/cloudclient/MusicCloudImpl;-><init>(Landroid/content/Context;)V

    .line 272
    .local v0, "cloudClient":Lcom/google/android/music/cloudclient/MusicCloud;
    invoke-virtual {v1}, Lcom/google/android/music/store/Store;->getCurrentCloudQueueVersion()J

    move-result-wide v2

    .line 273
    .local v2, "latestVersion":J
    new-instance v8, Lcom/google/android/music/cloudclient/UpdateCloudQueueRequestJson;

    invoke-direct {v8}, Lcom/google/android/music/cloudclient/UpdateCloudQueueRequestJson;-><init>()V

    .line 274
    .local v8, "request":Lcom/google/android/music/cloudclient/UpdateCloudQueueRequestJson;
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    iput-object v4, v8, Lcom/google/android/music/cloudclient/UpdateCloudQueueRequestJson;->mSenderVersion:Ljava/lang/Long;

    .line 275
    invoke-virtual {v1, v2, v3, v6}, Lcom/google/android/music/store/Store;->getCloudQueueContainers(JLjava/lang/Long;)Ljava/util/List;

    move-result-object v4

    iput-object v4, v8, Lcom/google/android/music/cloudclient/UpdateCloudQueueRequestJson;->mContainers:Ljava/util/List;

    .line 276
    invoke-virtual {v1, v2, v3, v6}, Lcom/google/android/music/store/Store;->getCloudQueueTracks(JLjava/lang/Long;)Ljava/util/List;

    move-result-object v4

    iput-object v4, v8, Lcom/google/android/music/cloudclient/UpdateCloudQueueRequestJson;->mTracks:Ljava/util/List;

    .line 277
    const-wide/16 v4, 0x0

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/music/store/Store;->getCloudQueueItems(JJLjava/lang/Long;)Ljava/util/List;

    move-result-object v4

    iput-object v4, v8, Lcom/google/android/music/cloudclient/UpdateCloudQueueRequestJson;->mItems:Ljava/util/List;

    .line 281
    :try_start_0
    invoke-interface {v0, v8}, Lcom/google/android/music/cloudclient/MusicCloud;->updateCloudQueue(Lcom/google/android/music/cloudclient/UpdateCloudQueueRequestJson;)Lcom/google/android/music/cloudclient/UpdateCloudQueueResponseJson;

    move-result-object v9

    .line 283
    .local v9, "response":Lcom/google/android/music/cloudclient/UpdateCloudQueueResponseJson;
    sget-boolean v4, Lcom/google/android/music/cloudclient/CloudQueueManager;->LOGV:Z

    if-eqz v4, :cond_0

    .line 284
    const-string v4, "CloudQueueManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Update Cloud Queue returned new version: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v9, Lcom/google/android/music/cloudclient/UpdateCloudQueueResponseJson;->mVersion:Ljava/lang/Long;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 286
    .end local v9    # "response":Lcom/google/android/music/cloudclient/UpdateCloudQueueResponseJson;
    :catch_0
    move-exception v7

    .line 287
    .local v7, "e":Ljava/lang/InterruptedException;
    const-string v4, "CloudQueueManager"

    invoke-virtual {v7}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 288
    .end local v7    # "e":Ljava/lang/InterruptedException;
    :catch_1
    move-exception v7

    .line 289
    .local v7, "e":Ljava/io/IOException;
    const-string v4, "CloudQueueManager"

    invoke-virtual {v7}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public static updateCloudQueueVersionForUpdatedItems(Landroid/content/Context;JJ)V
    .locals 11
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "fromPos"    # J
    .param p3, "toPos"    # J

    .prologue
    .line 306
    invoke-static {p0}, Lcom/google/android/music/utils/MusicUtils;->isDifferentialSyncEnabled(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 307
    invoke-static {p0}, Lcom/google/android/music/cloudclient/CloudQueueManager;->setCloudQueue(Landroid/content/Context;)V

    .line 333
    :goto_0
    return-void

    .line 311
    :cond_0
    invoke-static {p0}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v1

    .line 312
    .local v1, "store":Lcom/google/android/music/store/Store;
    new-instance v8, Lcom/google/android/music/cloudclient/UpdateCloudQueueRequestJson;

    invoke-direct {v8}, Lcom/google/android/music/cloudclient/UpdateCloudQueueRequestJson;-><init>()V

    .line 314
    .local v8, "request":Lcom/google/android/music/cloudclient/UpdateCloudQueueRequestJson;
    invoke-static {p1, p2, p3, p4}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    .line 315
    .local v4, "targetOrder":J
    const-wide/16 v2, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/music/store/Store;->getCloudQueueItems(JJLjava/lang/Long;)Ljava/util/List;

    move-result-object v2

    iput-object v2, v8, Lcom/google/android/music/cloudclient/UpdateCloudQueueRequestJson;->mItems:Ljava/util/List;

    .line 316
    invoke-virtual {v1}, Lcom/google/android/music/store/Store;->getCurrentCloudQueueVersion()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v8, Lcom/google/android/music/cloudclient/UpdateCloudQueueRequestJson;->mSenderVersion:Ljava/lang/Long;

    .line 318
    new-instance v0, Lcom/google/android/music/cloudclient/MusicCloudImpl;

    invoke-direct {v0, p0}, Lcom/google/android/music/cloudclient/MusicCloudImpl;-><init>(Landroid/content/Context;)V

    .line 321
    .local v0, "cloudClient":Lcom/google/android/music/cloudclient/MusicCloud;
    :try_start_0
    invoke-interface {v0, v8}, Lcom/google/android/music/cloudclient/MusicCloud;->updateCloudQueue(Lcom/google/android/music/cloudclient/UpdateCloudQueueRequestJson;)Lcom/google/android/music/cloudclient/UpdateCloudQueueResponseJson;

    move-result-object v9

    .line 323
    .local v9, "response":Lcom/google/android/music/cloudclient/UpdateCloudQueueResponseJson;
    sget-boolean v2, Lcom/google/android/music/cloudclient/CloudQueueManager;->LOGV:Z

    if-eqz v2, :cond_1

    .line 324
    const-string v2, "CloudQueueManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Update cloud queue returned version: "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v6, v9, Lcom/google/android/music/cloudclient/UpdateCloudQueueResponseJson;->mVersion:Ljava/lang/Long;

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 326
    :cond_1
    iget-object v2, v9, Lcom/google/android/music/cloudclient/UpdateCloudQueueResponseJson;->mVersion:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1, v4, v5, v2, v3}, Lcom/google/android/music/store/Store;->caqUpdateCloudQueueVersionForShiftedItems(JJ)V

    .line 327
    iget-object v2, v9, Lcom/google/android/music/cloudclient/UpdateCloudQueueResponseJson;->mVersion:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/google/android/music/store/Store;->setLatestCloudQueueVersion(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 328
    .end local v9    # "response":Lcom/google/android/music/cloudclient/UpdateCloudQueueResponseJson;
    :catch_0
    move-exception v7

    .line 329
    .local v7, "e":Ljava/lang/InterruptedException;
    const-string v2, "CloudQueueManager"

    invoke-virtual {v7}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 330
    .end local v7    # "e":Ljava/lang/InterruptedException;
    :catch_1
    move-exception v7

    .line 331
    .local v7, "e":Ljava/io/IOException;
    const-string v2, "CloudQueueManager"

    invoke-virtual {v7}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private static updatePlayModeState(Lcom/google/android/music/store/Store;Lcom/google/android/music/cloudclient/CloudQueuePlaySettingsJson;)V
    .locals 4
    .param p0, "store"    # Lcom/google/android/music/store/Store;
    .param p1, "playSettings"    # Lcom/google/android/music/cloudclient/CloudQueuePlaySettingsJson;

    .prologue
    const/4 v2, 0x1

    .line 463
    iget-boolean v3, p1, Lcom/google/android/music/cloudclient/CloudQueuePlaySettingsJson;->mShuffled:Z

    if-eqz v3, :cond_0

    move v1, v2

    .line 465
    .local v1, "targetShuffleMode":I
    :goto_0
    invoke-virtual {p0, v1}, Lcom/google/android/music/store/Store;->setCloudQueueShuffleMode(I)V

    .line 468
    iget v3, p1, Lcom/google/android/music/cloudclient/CloudQueuePlaySettingsJson;->mMode:I

    if-ne v3, v2, :cond_1

    .line 469
    const/4 v0, 0x0

    .line 481
    .local v0, "targetRepeatMode":I
    :goto_1
    invoke-virtual {p0, v0}, Lcom/google/android/music/store/Store;->setCloudQueueRepeatMode(I)V

    .line 482
    .end local v0    # "targetRepeatMode":I
    :goto_2
    return-void

    .line 463
    .end local v1    # "targetShuffleMode":I
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 470
    .restart local v1    # "targetShuffleMode":I
    :cond_1
    iget v2, p1, Lcom/google/android/music/cloudclient/CloudQueuePlaySettingsJson;->mMode:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    .line 471
    const/4 v0, 0x1

    .restart local v0    # "targetRepeatMode":I
    goto :goto_1

    .line 472
    .end local v0    # "targetRepeatMode":I
    :cond_2
    iget v2, p1, Lcom/google/android/music/cloudclient/CloudQueuePlaySettingsJson;->mMode:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_3

    .line 473
    const/4 v0, 0x2

    .restart local v0    # "targetRepeatMode":I
    goto :goto_1

    .line 474
    .end local v0    # "targetRepeatMode":I
    :cond_3
    iget v2, p1, Lcom/google/android/music/cloudclient/CloudQueuePlaySettingsJson;->mMode:I

    const/4 v3, 0x4

    if-ne v2, v3, :cond_4

    .line 475
    const/4 v0, 0x0

    .restart local v0    # "targetRepeatMode":I
    goto :goto_1

    .line 477
    .end local v0    # "targetRepeatMode":I
    :cond_4
    const-string v2, "CloudQueueManager"

    const-string v3, "Cloud Queue Server is reporting an unknown play mode, so not overriding local play mode."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method
