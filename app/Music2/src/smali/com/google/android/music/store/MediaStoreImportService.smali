.class public Lcom/google/android/music/store/MediaStoreImportService;
.super Landroid/app/Service;
.source "MediaStoreImportService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/store/MediaStoreImportService$Receiver;,
        Lcom/google/android/music/store/MediaStoreImportService$LocalBinder;
    }
.end annotation


# static fields
.field private static final LOGV:Z


# instance fields
.field private final mBinder:Landroid/os/IBinder;

.field private mContentChangeProcessor:Ljava/lang/Runnable;

.field private final mContentObserver:Landroid/database/ContentObserver;

.field private mDelayedImport:Ljava/lang/Runnable;

.field private mFirstChangeTimeSinceLastImport:J

.field private final mImportPending:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private mLastImportTime:J

.field private final mWorker:Lcom/google/android/music/utils/LoggableHandler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->STORE_IMPORTER:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/store/MediaStoreImportService;->LOGV:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 34
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 45
    new-instance v0, Lcom/google/android/music/store/MediaStoreImportService$LocalBinder;

    invoke-direct {v0, p0}, Lcom/google/android/music/store/MediaStoreImportService$LocalBinder;-><init>(Lcom/google/android/music/store/MediaStoreImportService;)V

    iput-object v0, p0, Lcom/google/android/music/store/MediaStoreImportService;->mBinder:Landroid/os/IBinder;

    .line 47
    new-instance v0, Lcom/google/android/music/utils/LoggableHandler;

    const-string v1, "MediaStoreImportService"

    invoke-direct {v0, v1}, Lcom/google/android/music/utils/LoggableHandler;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/music/store/MediaStoreImportService;->mWorker:Lcom/google/android/music/utils/LoggableHandler;

    .line 67
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/music/store/MediaStoreImportService;->mImportPending:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 69
    iput-wide v2, p0, Lcom/google/android/music/store/MediaStoreImportService;->mLastImportTime:J

    .line 70
    iput-wide v2, p0, Lcom/google/android/music/store/MediaStoreImportService;->mFirstChangeTimeSinceLastImport:J

    .line 87
    new-instance v0, Lcom/google/android/music/store/MediaStoreImportService$1;

    invoke-direct {v0, p0}, Lcom/google/android/music/store/MediaStoreImportService$1;-><init>(Lcom/google/android/music/store/MediaStoreImportService;)V

    iput-object v0, p0, Lcom/google/android/music/store/MediaStoreImportService;->mDelayedImport:Ljava/lang/Runnable;

    .line 100
    new-instance v0, Lcom/google/android/music/store/MediaStoreImportService$2;

    invoke-direct {v0, p0}, Lcom/google/android/music/store/MediaStoreImportService$2;-><init>(Lcom/google/android/music/store/MediaStoreImportService;)V

    iput-object v0, p0, Lcom/google/android/music/store/MediaStoreImportService;->mContentChangeProcessor:Ljava/lang/Runnable;

    .line 124
    new-instance v0, Lcom/google/android/music/store/MediaStoreImportService$3;

    iget-object v1, p0, Lcom/google/android/music/store/MediaStoreImportService;->mWorker:Lcom/google/android/music/utils/LoggableHandler;

    invoke-direct {v0, p0, v1}, Lcom/google/android/music/store/MediaStoreImportService$3;-><init>(Lcom/google/android/music/store/MediaStoreImportService;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/google/android/music/store/MediaStoreImportService;->mContentObserver:Landroid/database/ContentObserver;

    .line 205
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/store/MediaStoreImportService;)J
    .locals 2
    .param p0, "x0"    # Lcom/google/android/music/store/MediaStoreImportService;

    .prologue
    .line 34
    iget-wide v0, p0, Lcom/google/android/music/store/MediaStoreImportService;->mLastImportTime:J

    return-wide v0
.end method

.method static synthetic access$100(Lcom/google/android/music/store/MediaStoreImportService;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/store/MediaStoreImportService;

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/google/android/music/store/MediaStoreImportService;->importMediaStore()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/music/store/MediaStoreImportService;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/store/MediaStoreImportService;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/music/store/MediaStoreImportService;->mDelayedImport:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/music/store/MediaStoreImportService;)Lcom/google/android/music/utils/LoggableHandler;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/store/MediaStoreImportService;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/music/store/MediaStoreImportService;->mWorker:Lcom/google/android/music/utils/LoggableHandler;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/music/store/MediaStoreImportService;)J
    .locals 2
    .param p0, "x0"    # Lcom/google/android/music/store/MediaStoreImportService;

    .prologue
    .line 34
    iget-wide v0, p0, Lcom/google/android/music/store/MediaStoreImportService;->mFirstChangeTimeSinceLastImport:J

    return-wide v0
.end method

.method static synthetic access$402(Lcom/google/android/music/store/MediaStoreImportService;J)J
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/store/MediaStoreImportService;
    .param p1, "x1"    # J

    .prologue
    .line 34
    iput-wide p1, p0, Lcom/google/android/music/store/MediaStoreImportService;->mFirstChangeTimeSinceLastImport:J

    return-wide p1
.end method

.method static synthetic access$500()Z
    .locals 1

    .prologue
    .line 34
    sget-boolean v0, Lcom/google/android/music/store/MediaStoreImportService;->LOGV:Z

    return v0
.end method

.method static synthetic access$600(Lcom/google/android/music/store/MediaStoreImportService;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/store/MediaStoreImportService;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/music/store/MediaStoreImportService;->mContentChangeProcessor:Ljava/lang/Runnable;

    return-object v0
.end method

.method private importMediaStore()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 73
    iget-object v0, p0, Lcom/google/android/music/store/MediaStoreImportService;->mImportPending:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 75
    :try_start_0
    invoke-static {p0}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/music/store/Store;->importMediaStore(Landroid/content/Context;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 77
    iget-object v0, p0, Lcom/google/android/music/store/MediaStoreImportService;->mImportPending:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 78
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.music.IMPORT_COMPLETE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/google/android/music/store/MediaStoreImportService;->sendBroadcast(Landroid/content/Intent;)V

    .line 83
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/music/store/MediaStoreImportService;->mLastImportTime:J

    .line 84
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/music/store/MediaStoreImportService;->mFirstChangeTimeSinceLastImport:J

    .line 85
    return-void

    .line 77
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/music/store/MediaStoreImportService;->mImportPending:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 78
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.google.android.music.IMPORT_COMPLETE"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lcom/google/android/music/store/MediaStoreImportService;->sendBroadcast(Landroid/content/Intent;)V

    throw v0
.end method

.method private sendStatusBroadcast(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 262
    const/4 v0, 0x0

    .line 263
    .local v0, "action":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/music/store/MediaStoreImportService;->mImportPending:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 264
    const-string v0, "com.google.android.music.IMPORT_PENDING"

    .line 269
    :goto_0
    if-eqz v0, :cond_0

    .line 270
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 272
    :cond_0
    return-void

    .line 266
    :cond_1
    const-string v0, "com.google.android.music.IMPORT_COMPLETE"

    goto :goto_0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v3, 0x1

    .line 172
    sget-boolean v0, Lcom/google/android/music/store/MediaStoreImportService;->LOGV:Z

    if-eqz v0, :cond_0

    const-string v0, "MediaStoreImportService"

    const-string v1, "onBind"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 175
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/store/MediaStoreImportService;->mWorker:Lcom/google/android/music/utils/LoggableHandler;

    iget-object v1, p0, Lcom/google/android/music/store/MediaStoreImportService;->mContentChangeProcessor:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/google/android/music/utils/LoggableHandler;->post(Ljava/lang/Runnable;)Z

    .line 177
    invoke-virtual {p0}, Lcom/google/android/music/store/MediaStoreImportService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/music/store/MediaStoreImportService;->mContentObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 179
    invoke-virtual {p0}, Lcom/google/android/music/store/MediaStoreImportService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Audio$Playlists;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/music/store/MediaStoreImportService;->mContentObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 181
    iget-object v0, p0, Lcom/google/android/music/store/MediaStoreImportService;->mBinder:Landroid/os/IBinder;

    return-object v0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 193
    sget-boolean v0, Lcom/google/android/music/store/MediaStoreImportService;->LOGV:Z

    if-eqz v0, :cond_0

    const-string v0, "MediaStoreImportService"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 194
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/music/store/MediaStoreImportService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/store/MediaStoreImportService;->mContentObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 195
    iget-object v0, p0, Lcom/google/android/music/store/MediaStoreImportService;->mWorker:Lcom/google/android/music/utils/LoggableHandler;

    invoke-virtual {v0}, Lcom/google/android/music/utils/LoggableHandler;->quit()V

    .line 196
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 197
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 134
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 135
    .local v0, "action":Ljava/lang/String;
    :goto_0
    sget-boolean v1, Lcom/google/android/music/store/MediaStoreImportService;->LOGV:Z

    if-eqz v1, :cond_0

    const-string v1, "MediaStoreImportService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Handle action: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 136
    :cond_0
    const-string v1, "MediaStoreImportService.import"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 137
    iget-object v1, p0, Lcom/google/android/music/store/MediaStoreImportService;->mWorker:Lcom/google/android/music/utils/LoggableHandler;

    new-instance v2, Lcom/google/android/music/store/MediaStoreImportService$4;

    invoke-direct {v2, p0, p3}, Lcom/google/android/music/store/MediaStoreImportService$4;-><init>(Lcom/google/android/music/store/MediaStoreImportService;I)V

    invoke-virtual {v1, v2}, Lcom/google/android/music/utils/LoggableHandler;->post(Ljava/lang/Runnable;)Z

    .line 167
    :goto_1
    const/4 v1, 0x3

    return v1

    .line 134
    .end local v0    # "action":Ljava/lang/String;
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 144
    .restart local v0    # "action":Ljava/lang/String;
    :cond_2
    const-string v1, "MediaStoreImportService.status"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 145
    invoke-direct {p0, p0}, Lcom/google/android/music/store/MediaStoreImportService;->sendStatusBroadcast(Landroid/content/Context;)V

    .line 146
    iget-object v1, p0, Lcom/google/android/music/store/MediaStoreImportService;->mWorker:Lcom/google/android/music/utils/LoggableHandler;

    new-instance v2, Lcom/google/android/music/store/MediaStoreImportService$5;

    invoke-direct {v2, p0, p3}, Lcom/google/android/music/store/MediaStoreImportService$5;-><init>(Lcom/google/android/music/store/MediaStoreImportService;I)V

    invoke-virtual {v1, v2}, Lcom/google/android/music/utils/LoggableHandler;->post(Ljava/lang/Runnable;)Z

    goto :goto_1

    .line 151
    :cond_3
    const-string v1, "MediaStoreImportService.import_pending"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 152
    iget-object v1, p0, Lcom/google/android/music/store/MediaStoreImportService;->mImportPending:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 153
    iget-object v1, p0, Lcom/google/android/music/store/MediaStoreImportService;->mWorker:Lcom/google/android/music/utils/LoggableHandler;

    new-instance v2, Lcom/google/android/music/store/MediaStoreImportService$6;

    invoke-direct {v2, p0, p3}, Lcom/google/android/music/store/MediaStoreImportService$6;-><init>(Lcom/google/android/music/store/MediaStoreImportService;I)V

    invoke-virtual {v1, v2}, Lcom/google/android/music/utils/LoggableHandler;->post(Ljava/lang/Runnable;)Z

    goto :goto_1

    .line 159
    :cond_4
    const-string v1, "MediaStoreImportService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unexpected action requested: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 160
    iget-object v1, p0, Lcom/google/android/music/store/MediaStoreImportService;->mWorker:Lcom/google/android/music/utils/LoggableHandler;

    new-instance v2, Lcom/google/android/music/store/MediaStoreImportService$7;

    invoke-direct {v2, p0, p3}, Lcom/google/android/music/store/MediaStoreImportService$7;-><init>(Lcom/google/android/music/store/MediaStoreImportService;I)V

    invoke-virtual {v1, v2}, Lcom/google/android/music/utils/LoggableHandler;->post(Ljava/lang/Runnable;)Z

    goto :goto_1
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 186
    sget-boolean v0, Lcom/google/android/music/store/MediaStoreImportService;->LOGV:Z

    if-eqz v0, :cond_0

    const-string v0, "MediaStoreImportService"

    const-string v1, "onUnbind"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 187
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/music/store/MediaStoreImportService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/store/MediaStoreImportService;->mContentObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 188
    invoke-super {p0, p1}, Landroid/app/Service;->onUnbind(Landroid/content/Intent;)Z

    move-result v0

    return v0
.end method
