.class public Lcom/google/android/music/cloudclient/GetCloudQueueResponseJson;
.super Lcom/google/api/client/json/GenericJson;
.source "GetCloudQueueResponseJson.java"


# instance fields
.field public mQueue:Lcom/google/android/music/cloudclient/CloudQueueFlatQueueJson;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "queue"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 18
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "queue: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/music/cloudclient/GetCloudQueueResponseJson;->mQueue:Lcom/google/android/music/cloudclient/CloudQueueFlatQueueJson;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
