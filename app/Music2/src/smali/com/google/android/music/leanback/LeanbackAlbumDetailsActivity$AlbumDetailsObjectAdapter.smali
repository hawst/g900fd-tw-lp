.class Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;
.super Landroid/support/v17/leanback/widget/ObjectAdapter;
.source "LeanbackAlbumDetailsActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "AlbumDetailsObjectAdapter"
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mDetailsObjectAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

.field private mExtraDataLoaded:Z

.field private mSize:I

.field private final mSongFooterRow:Lcom/google/android/music/leanback/SongFooterRow;

.field private mSongListObjectAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

.field private mSongListObjectAdapterStartIndex:I

.field private final mSongTitleRow:Lcom/google/android/music/leanback/SongTitleRow;

.field final synthetic this$0:Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;


# direct methods
.method constructor <init>(Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;Landroid/content/Context;Landroid/support/v17/leanback/widget/PresenterSelector;Landroid/support/v17/leanback/widget/ObjectAdapter;Lcom/google/android/music/leanback/SongTitleRow;Lcom/google/android/music/leanback/SongFooterRow;)V
    .locals 2
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "presenterSelector"    # Landroid/support/v17/leanback/widget/PresenterSelector;
    .param p4, "detailsObjectAdapter"    # Landroid/support/v17/leanback/widget/ObjectAdapter;
    .param p5, "songTitleRow"    # Lcom/google/android/music/leanback/SongTitleRow;
    .param p6, "songFooterRow"    # Lcom/google/android/music/leanback/SongFooterRow;

    .prologue
    .line 299
    iput-object p1, p0, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;->this$0:Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;

    .line 300
    invoke-direct {p0, p3}, Landroid/support/v17/leanback/widget/ObjectAdapter;-><init>(Landroid/support/v17/leanback/widget/PresenterSelector;)V

    .line 295
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;->mExtraDataLoaded:Z

    .line 301
    iput-object p2, p0, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;->mContext:Landroid/content/Context;

    .line 302
    iput-object p4, p0, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;->mDetailsObjectAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    .line 303
    iput-object p5, p0, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;->mSongTitleRow:Lcom/google/android/music/leanback/SongTitleRow;

    .line 304
    iput-object p6, p0, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;->mSongFooterRow:Lcom/google/android/music/leanback/SongFooterRow;

    .line 305
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;->mDetailsObjectAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    new-instance v1, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter$1;

    invoke-direct {v1, p0, p1}, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter$1;-><init>(Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;)V

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/ObjectAdapter;->registerObserver(Landroid/support/v17/leanback/widget/ObjectAdapter$DataObserver;)V

    .line 335
    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;->recalculateSize()V

    .line 336
    return-void
.end method

.method static synthetic access$1100(Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;II)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 287
    invoke-virtual {p0, p1, p2}, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;->notifyItemRangeChanged(II)V

    return-void
.end method

.method static synthetic access$1300(Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;

    .prologue
    .line 287
    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;->notifyChanged()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;

    .prologue
    .line 287
    invoke-direct {p0}, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;->recalculateSize()V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;

    .prologue
    .line 287
    invoke-virtual {p0}, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;->notifyChanged()V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;II)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 287
    invoke-virtual {p0, p1, p2}, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;->notifyItemRangeChanged(II)V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;II)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 287
    invoke-virtual {p0, p1, p2}, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;->notifyItemRangeInserted(II)V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;II)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 287
    invoke-virtual {p0, p1, p2}, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;->notifyItemRangeRemoved(II)V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;

    .prologue
    .line 287
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;II)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 287
    invoke-virtual {p0, p1, p2}, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;->notifyItemRangeChanged(II)V

    return-void
.end method

.method private recalculateSize()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 365
    iget-object v3, p0, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;->mDetailsObjectAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    invoke-virtual {v3}, Landroid/support/v17/leanback/widget/ObjectAdapter;->size()I

    move-result v3

    invoke-static {v3, v7}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 366
    .local v2, "size":I
    if-lez v2, :cond_0

    iget-boolean v3, p0, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;->mExtraDataLoaded:Z

    if-nez v3, :cond_0

    .line 367
    iget-object v3, p0, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;->mDetailsObjectAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    invoke-virtual {v3, v6}, Landroid/support/v17/leanback/widget/ObjectAdapter;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v17/leanback/widget/DetailsOverviewRow;

    .line 369
    .local v0, "detailsOverviewRow":Landroid/support/v17/leanback/widget/DetailsOverviewRow;
    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/DetailsOverviewRow;->getItem()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/leanback/Item;

    .line 370
    .local v1, "item":Lcom/google/android/music/leanback/Item;
    iget-object v3, p0, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Lcom/google/android/music/leanback/Item;->getBitmapGettersGetter()Lcom/google/android/music/leanback/bitmap/BitmapGettersGetter;

    move-result-object v4

    new-instance v5, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter$2;

    invoke-direct {v5, p0, v0}, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter$2;-><init>(Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;Landroid/support/v17/leanback/widget/DetailsOverviewRow;)V

    invoke-static {v3, v4, v6, v6, v5}, Lcom/google/android/music/leanback/bitmap/BitmapFactory;->getBitmap(Landroid/content/Context;Lcom/google/android/music/leanback/bitmap/BitmapGettersGetter;ZZLcom/google/android/music/leanback/bitmap/BitmapListener;)V

    .line 378
    new-instance v3, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter$3;

    invoke-direct {v3, p0, v1, v0}, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter$3;-><init>(Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;Lcom/google/android/music/leanback/Item;Landroid/support/v17/leanback/widget/DetailsOverviewRow;)V

    invoke-static {v3}, Lcom/google/android/music/utils/MusicUtils;->runAsyncWithCallback(Lcom/google/android/music/utils/async/AsyncRunner;)V

    .line 394
    iget-object v3, p0, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;->this$0:Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;

    invoke-virtual {v1}, Lcom/google/android/music/leanback/Item;->getId()Ljava/lang/String;

    move-result-object v4

    # invokes: Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;->createSongList(Ljava/lang/String;Lcom/google/android/music/leanback/Item;)Landroid/support/v17/leanback/widget/ObjectAdapter;
    invoke-static {v3, v4, v1}, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;->access$1200(Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity;Ljava/lang/String;Lcom/google/android/music/leanback/Item;)Landroid/support/v17/leanback/widget/ObjectAdapter;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;->mSongListObjectAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    .line 395
    iget-object v3, p0, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;->mSongListObjectAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    new-instance v4, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter$4;

    invoke-direct {v4, p0}, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter$4;-><init>(Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;)V

    invoke-virtual {v3, v4}, Landroid/support/v17/leanback/widget/ObjectAdapter;->registerObserver(Landroid/support/v17/leanback/widget/ObjectAdapter$DataObserver;)V

    .line 402
    iput-boolean v7, p0, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;->mExtraDataLoaded:Z

    .line 404
    .end local v0    # "detailsOverviewRow":Landroid/support/v17/leanback/widget/DetailsOverviewRow;
    .end local v1    # "item":Lcom/google/android/music/leanback/Item;
    :cond_0
    iget-object v3, p0, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;->mSongListObjectAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;->mSongListObjectAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    invoke-virtual {v3}, Landroid/support/v17/leanback/widget/ObjectAdapter;->size()I

    move-result v3

    if-lez v3, :cond_1

    .line 405
    add-int/lit8 v2, v2, 0x1

    .line 406
    iput v2, p0, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;->mSongListObjectAdapterStartIndex:I

    .line 407
    iget-object v3, p0, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;->mSongListObjectAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    invoke-virtual {v3}, Landroid/support/v17/leanback/widget/ObjectAdapter;->size()I

    move-result v3

    add-int/2addr v2, v3

    .line 408
    add-int/lit8 v2, v2, 0x1

    .line 412
    :goto_0
    iput v2, p0, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;->mSize:I

    .line 413
    return-void

    .line 410
    :cond_1
    const/4 v3, -0x1

    iput v3, p0, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;->mSongListObjectAdapterStartIndex:I

    goto :goto_0
.end method


# virtual methods
.method public get(I)Ljava/lang/Object;
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 345
    iget v0, p0, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;->mSize:I

    if-ge p1, v0, :cond_3

    .line 346
    iget v0, p0, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;->mSongListObjectAdapterStartIndex:I

    if-ltz v0, :cond_2

    .line 347
    iget v0, p0, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;->mSize:I

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_0

    .line 348
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;->mSongFooterRow:Lcom/google/android/music/leanback/SongFooterRow;

    .line 358
    :goto_0
    return-object v0

    .line 350
    :cond_0
    iget v0, p0, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;->mSongListObjectAdapterStartIndex:I

    if-lt p1, v0, :cond_1

    .line 351
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;->mSongListObjectAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    iget v1, p0, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;->mSongListObjectAdapterStartIndex:I

    sub-int v1, p1, v1

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/ObjectAdapter;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 354
    :cond_1
    iget v0, p0, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;->mSongListObjectAdapterStartIndex:I

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_2

    .line 355
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;->mSongTitleRow:Lcom/google/android/music/leanback/SongTitleRow;

    goto :goto_0

    .line 358
    :cond_2
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;->mDetailsObjectAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    invoke-virtual {v0, p1}, Landroid/support/v17/leanback/widget/ObjectAdapter;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 360
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Tried to get unexpected position "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". Size is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;->mSize:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 340
    iget v0, p0, Lcom/google/android/music/leanback/LeanbackAlbumDetailsActivity$AlbumDetailsObjectAdapter;->mSize:I

    return v0
.end method
