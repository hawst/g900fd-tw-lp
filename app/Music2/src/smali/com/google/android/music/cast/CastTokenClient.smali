.class public interface abstract Lcom/google/android/music/cast/CastTokenClient;
.super Ljava/lang/Object;
.source "CastTokenClient.java"


# virtual methods
.method public abstract clearCachedCastToken(Ljava/lang/String;)V
.end method

.method public abstract getCachedCastToken(Ljava/lang/String;)Ljava/lang/String;
.end method

.method public abstract getCastToken(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
.end method

.method public abstract getCastToken(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;
.end method

.method public abstract hasCachedCastToken(Ljava/lang/String;)Z
.end method

.method public abstract release()V
.end method
