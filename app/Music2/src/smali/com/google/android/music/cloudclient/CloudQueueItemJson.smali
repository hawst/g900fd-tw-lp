.class public Lcom/google/android/music/cloudclient/CloudQueueItemJson;
.super Lcom/google/api/client/json/GenericJson;
.source "CloudQueueItemJson.java"


# instance fields
.field public mContainerKey:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "containerKey"
    .end annotation
.end field

.field public mId:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "id"
    .end annotation
.end field

.field public mTrackId:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "trackId"
    .end annotation
.end field

.field public mTrackNid:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "trackNid"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    return-void
.end method
