.class public final Lcom/google/android/music/store/MusicContent$Playlists$Members;
.super Ljava/lang/Object;
.source "MusicContent.java"

# interfaces
.implements Lcom/google/android/music/store/MusicContent$PlaylistMemberColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/store/MusicContent$Playlists;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Members"
.end annotation


# direct methods
.method public static getPlaylistItemUri(JJ)Landroid/net/Uri;
    .locals 2
    .param p0, "playlistId"    # J
    .param p2, "itemId"    # J

    .prologue
    .line 1243
    invoke-static {p0, p1}, Lcom/google/android/music/store/MusicContent$Playlists$Members;->getPlaylistItemsUri(J)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0, p2, p3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static getPlaylistItemsCount(Landroid/content/Context;JZ)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "playlistId"    # J
    .param p3, "shouldFilter"    # Z

    .prologue
    .line 1251
    invoke-static {p1, p2}, Lcom/google/android/music/store/MusicContent$Playlists$Members;->getPlaylistItemsUri(J)Landroid/net/Uri;

    move-result-object v0

    .line 1252
    .local v0, "uri":Landroid/net/Uri;
    const/4 v1, 0x1

    invoke-static {p0, v0, p3, v1}, Lcom/google/android/music/store/MusicContent;->getCount(Landroid/content/Context;Landroid/net/Uri;ZZ)I

    move-result v1

    return v1
.end method

.method public static getPlaylistItemsUri(J)Landroid/net/Uri;
    .locals 2
    .param p0, "playlistId"    # J

    .prologue
    .line 1239
    invoke-static {p0, p1}, Lcom/google/android/music/store/MusicContent$Playlists;->getPlaylistUri(J)Landroid/net/Uri;

    move-result-object v0

    const-string v1, "members"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method
