.class Lcom/google/android/music/ui/UIStateManager$7;
.super Lcom/google/android/music/utils/SafeServiceConnection;
.source "UIStateManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/UIStateManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/UIStateManager;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/UIStateManager;)V
    .locals 0

    .prologue
    .line 214
    iput-object p1, p0, Lcom/google/android/music/ui/UIStateManager$7;->this$0:Lcom/google/android/music/ui/UIStateManager;

    invoke-direct {p0}, Lcom/google/android/music/utils/SafeServiceConnection;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 217
    iget-object v0, p0, Lcom/google/android/music/ui/UIStateManager$7;->this$0:Lcom/google/android/music/ui/UIStateManager;

    invoke-static {p2}, Lcom/google/android/music/download/cache/ICacheManager$Stub;->asInterface(Landroid/os/IBinder;)Lcom/google/android/music/download/cache/ICacheManager;

    move-result-object v1

    # setter for: Lcom/google/android/music/ui/UIStateManager;->mCacheManager:Lcom/google/android/music/download/cache/ICacheManager;
    invoke-static {v0, v1}, Lcom/google/android/music/ui/UIStateManager;->access$1102(Lcom/google/android/music/ui/UIStateManager;Lcom/google/android/music/download/cache/ICacheManager;)Lcom/google/android/music/download/cache/ICacheManager;

    .line 218
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 222
    iget-object v0, p0, Lcom/google/android/music/ui/UIStateManager$7;->this$0:Lcom/google/android/music/ui/UIStateManager;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/music/ui/UIStateManager;->mCacheManager:Lcom/google/android/music/download/cache/ICacheManager;
    invoke-static {v0, v1}, Lcom/google/android/music/ui/UIStateManager;->access$1102(Lcom/google/android/music/ui/UIStateManager;Lcom/google/android/music/download/cache/ICacheManager;)Lcom/google/android/music/download/cache/ICacheManager;

    .line 223
    return-void
.end method
