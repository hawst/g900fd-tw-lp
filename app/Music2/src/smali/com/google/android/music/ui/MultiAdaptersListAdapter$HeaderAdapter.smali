.class public interface abstract Lcom/google/android/music/ui/MultiAdaptersListAdapter$HeaderAdapter;
.super Ljava/lang/Object;
.source "MultiAdaptersListAdapter.java"

# interfaces
.implements Landroid/widget/Adapter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/MultiAdaptersListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "HeaderAdapter"
.end annotation


# virtual methods
.method public abstract addLayoutIds(Ljava/util/Set;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract getHeader()Ljava/lang/String;
.end method

.method public abstract getItemLayoutId(I)I
.end method
