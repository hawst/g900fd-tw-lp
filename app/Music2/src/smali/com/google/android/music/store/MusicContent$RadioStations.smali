.class public final Lcom/google/android/music/store/MusicContent$RadioStations;
.super Ljava/lang/Object;
.source "MusicContent.java"

# interfaces
.implements Landroid/provider/BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/store/MusicContent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RadioStations"
.end annotation


# static fields
.field public static final CONTENT_URI:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 3292
    sget-object v0, Lcom/google/android/music/store/MusicContent;->CONTENT_URI:Landroid/net/Uri;

    const-string v1, "radio_stations"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/store/MusicContent$RadioStations;->CONTENT_URI:Landroid/net/Uri;

    return-void
.end method

.method public static getMixDescriptorSeedType(I)Lcom/google/android/music/mix/MixDescriptor$Type;
    .locals 3
    .param p0, "radioSeedType"    # I

    .prologue
    .line 3318
    packed-switch p0, :pswitch_data_0

    .line 3338
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Bad radio seed type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3320
    :pswitch_0
    sget-object v0, Lcom/google/android/music/mix/MixDescriptor$Type;->ALBUM_SEED:Lcom/google/android/music/mix/MixDescriptor$Type;

    .line 3335
    :goto_0
    return-object v0

    .line 3322
    :pswitch_1
    sget-object v0, Lcom/google/android/music/mix/MixDescriptor$Type;->ARTIST_SEED:Lcom/google/android/music/mix/MixDescriptor$Type;

    goto :goto_0

    .line 3324
    :pswitch_2
    sget-object v0, Lcom/google/android/music/mix/MixDescriptor$Type;->ARTIST_SHUFFLE_SEED:Lcom/google/android/music/mix/MixDescriptor$Type;

    goto :goto_0

    .line 3326
    :pswitch_3
    sget-object v0, Lcom/google/android/music/mix/MixDescriptor$Type;->GENRE_SEED:Lcom/google/android/music/mix/MixDescriptor$Type;

    goto :goto_0

    .line 3329
    :pswitch_4
    sget-object v0, Lcom/google/android/music/mix/MixDescriptor$Type;->TRACK_SEED:Lcom/google/android/music/mix/MixDescriptor$Type;

    goto :goto_0

    .line 3331
    :pswitch_5
    sget-object v0, Lcom/google/android/music/mix/MixDescriptor$Type;->LUCKY_RADIO:Lcom/google/android/music/mix/MixDescriptor$Type;

    goto :goto_0

    .line 3333
    :pswitch_6
    sget-object v0, Lcom/google/android/music/mix/MixDescriptor$Type;->PLAYLIST_SEED:Lcom/google/android/music/mix/MixDescriptor$Type;

    goto :goto_0

    .line 3335
    :pswitch_7
    sget-object v0, Lcom/google/android/music/mix/MixDescriptor$Type;->CURATED_SEED:Lcom/google/android/music/mix/MixDescriptor$Type;

    goto :goto_0

    .line 3318
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_4
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_5
        :pswitch_2
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public static getRadioStationUri(J)Landroid/net/Uri;
    .locals 2
    .param p0, "radioStationId"    # J

    .prologue
    .line 3307
    sget-object v0, Lcom/google/android/music/store/MusicContent$RadioStations;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0, p0, p1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method
