.class public Lcom/google/android/music/ui/ShuffleButton;
.super Lcom/google/android/music/ui/BaseActionButton;
.source "ShuffleButton.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 20
    const v0, 0x7f0b0237

    const v1, 0x7f020130

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/android/music/ui/BaseActionButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 21
    return-void
.end method


# virtual methods
.method protected handleAction(Landroid/content/Context;Lcom/google/android/music/medialist/MediaList;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "medialist"    # Lcom/google/android/music/medialist/MediaList;

    .prologue
    .line 25
    instance-of v0, p2, Lcom/google/android/music/medialist/SongList;

    if-eqz v0, :cond_0

    .line 26
    check-cast p2, Lcom/google/android/music/medialist/SongList;

    .end local p2    # "medialist":Lcom/google/android/music/medialist/MediaList;
    invoke-static {p2}, Lcom/google/android/music/utils/MusicUtils;->shuffleSongs(Lcom/google/android/music/medialist/SongList;)V

    .line 30
    :goto_0
    return-void

    .line 28
    .restart local p2    # "medialist":Lcom/google/android/music/medialist/MediaList;
    :cond_0
    const-string v0, "ActionButton"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid MediaList: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
