.class Lcom/google/android/music/xdi/DetailAlbumInfoCursor;
.super Landroid/database/MatrixCursor;
.source "DetailAlbumInfoCursor.java"


# static fields
.field private static final PROJECTION_ALBUMS:[Ljava/lang/String;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 27
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "album_name"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "album_artist"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "album_year"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/xdi/DetailAlbumInfoCursor;->PROJECTION_ALBUMS:[Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "albumId"    # Ljava/lang/String;

    .prologue
    .line 42
    invoke-direct {p0, p2}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 43
    iput-object p1, p0, Lcom/google/android/music/xdi/DetailAlbumInfoCursor;->mContext:Landroid/content/Context;

    .line 44
    new-instance v0, Lcom/google/android/music/xdi/ProjectionMap;

    invoke-direct {v0, p2}, Lcom/google/android/music/xdi/ProjectionMap;-><init>([Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/music/xdi/DetailAlbumInfoCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    .line 46
    invoke-direct {p0, p3}, Lcom/google/android/music/xdi/DetailAlbumInfoCursor;->addAlbum(Ljava/lang/String;)V

    .line 47
    return-void
.end method

.method private addAlbum(Ljava/lang/String;)V
    .locals 14
    .param p1, "albumId"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x0

    .line 50
    iget-object v9, p0, Lcom/google/android/music/xdi/DetailAlbumInfoCursor;->mContext:Landroid/content/Context;

    sget-object v10, Lcom/google/android/music/xdi/DetailAlbumInfoCursor;->PROJECTION_ALBUMS:[Ljava/lang/String;

    invoke-static {v9, p1, v10}, Lcom/google/android/music/xdi/XdiUtils;->getAlbumCursor(Landroid/content/Context;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .line 52
    .local v3, "cursor":Landroid/database/Cursor;
    if-nez v3, :cond_0

    .line 90
    :goto_0
    return-void

    .line 57
    :cond_0
    :try_start_0
    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v9

    if-eqz v9, :cond_2

    .line 58
    iget-object v9, p0, Lcom/google/android/music/xdi/DetailAlbumInfoCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    invoke-virtual {v9}, Lcom/google/android/music/xdi/ProjectionMap;->size()I

    move-result v9

    new-array v7, v9, [Ljava/lang/Object;

    .line 59
    .local v7, "values":[Ljava/lang/Object;
    const/4 v9, 0x0

    invoke-interface {v3, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 60
    .local v4, "name":Ljava/lang/String;
    const/4 v9, 0x1

    invoke-interface {v3, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 61
    .local v1, "artist":Ljava/lang/String;
    iget-object v9, p0, Lcom/google/android/music/xdi/DetailAlbumInfoCursor;->mContext:Landroid/content/Context;

    invoke-static {v9, p1}, Lcom/google/android/music/xdi/XdiUtils;->getAlbumSongCount(Landroid/content/Context;Ljava/lang/String;)I

    move-result v5

    .line 62
    .local v5, "songCount":I
    const/4 v9, 0x2

    invoke-interface {v3, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 63
    .local v0, "albumYear":I
    iget-object v9, p0, Lcom/google/android/music/xdi/DetailAlbumInfoCursor;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const/high16 v10, 0x7f120000

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-virtual {v9, v10, v5, v11}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 66
    .local v6, "songCountString":Ljava/lang/String;
    sget-object v9, Lcom/google/android/music/xdi/XdiContentProvider;->BASE_URI:Landroid/net/Uri;

    invoke-virtual {v9}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v9

    const-string v10, "details/albums"

    invoke-virtual {v9, v10}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v9

    invoke-virtual {v9, p1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v9

    const-string v10, "section"

    invoke-virtual {v9, v10}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v9

    const-string v10, "album"

    invoke-virtual {v9, v10}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v9

    const-string v10, "actions"

    invoke-virtual {v9, v10}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v9

    invoke-virtual {v9}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    .line 74
    .local v2, "contentUri":Landroid/net/Uri;
    iget-object v9, p0, Lcom/google/android/music/xdi/DetailAlbumInfoCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v10, "_id"

    invoke-interface {v3}, Landroid/database/Cursor;->getPosition()I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v9, v7, v10, v11}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 75
    iget-object v9, p0, Lcom/google/android/music/xdi/DetailAlbumInfoCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v10, "display_name"

    invoke-virtual {v9, v7, v10, v4}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 76
    iget-object v9, p0, Lcom/google/android/music/xdi/DetailAlbumInfoCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v10, "display_subname"

    invoke-virtual {v9, v7, v10, v1}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 77
    iget-object v9, p0, Lcom/google/android/music/xdi/DetailAlbumInfoCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v10, "display_description"

    invoke-virtual {v9, v7, v10, v6}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 79
    iget-object v9, p0, Lcom/google/android/music/xdi/DetailAlbumInfoCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v10, "display_category"

    const/4 v11, 0x0

    invoke-virtual {v9, v7, v10, v11}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 80
    iget-object v9, p0, Lcom/google/android/music/xdi/DetailAlbumInfoCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v10, "display_date"

    if-lez v0, :cond_1

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    :cond_1
    invoke-virtual {v9, v7, v10, v8}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 82
    iget-object v8, p0, Lcom/google/android/music/xdi/DetailAlbumInfoCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v9, "action_uri"

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v7, v9, v10}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 85
    invoke-virtual {p0, v7}, Lcom/google/android/music/xdi/DetailAlbumInfoCursor;->addRow([Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 88
    .end local v0    # "albumYear":I
    .end local v1    # "artist":Ljava/lang/String;
    .end local v2    # "contentUri":Landroid/net/Uri;
    .end local v4    # "name":Ljava/lang/String;
    .end local v5    # "songCount":I
    .end local v6    # "songCountString":Ljava/lang/String;
    .end local v7    # "values":[Ljava/lang/Object;
    :cond_2
    invoke-static {v3}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto/16 :goto_0

    :catchall_0
    move-exception v8

    invoke-static {v3}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v8
.end method
