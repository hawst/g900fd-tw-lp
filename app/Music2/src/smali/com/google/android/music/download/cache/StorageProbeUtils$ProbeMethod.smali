.class public final enum Lcom/google/android/music/download/cache/StorageProbeUtils$ProbeMethod;
.super Ljava/lang/Enum;
.source "StorageProbeUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/download/cache/StorageProbeUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ProbeMethod"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/music/download/cache/StorageProbeUtils$ProbeMethod;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/music/download/cache/StorageProbeUtils$ProbeMethod;

.field public static final enum CONTEXT_APIS:Lcom/google/android/music/download/cache/StorageProbeUtils$ProbeMethod;

.field public static final enum KITKAT_API:Lcom/google/android/music/download/cache/StorageProbeUtils$ProbeMethod;

.field public static final enum MOUNT_SERVICE:Lcom/google/android/music/download/cache/StorageProbeUtils$ProbeMethod;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 37
    new-instance v0, Lcom/google/android/music/download/cache/StorageProbeUtils$ProbeMethod;

    const-string v1, "MOUNT_SERVICE"

    invoke-direct {v0, v1, v2}, Lcom/google/android/music/download/cache/StorageProbeUtils$ProbeMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/download/cache/StorageProbeUtils$ProbeMethod;->MOUNT_SERVICE:Lcom/google/android/music/download/cache/StorageProbeUtils$ProbeMethod;

    .line 38
    new-instance v0, Lcom/google/android/music/download/cache/StorageProbeUtils$ProbeMethod;

    const-string v1, "KITKAT_API"

    invoke-direct {v0, v1, v3}, Lcom/google/android/music/download/cache/StorageProbeUtils$ProbeMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/download/cache/StorageProbeUtils$ProbeMethod;->KITKAT_API:Lcom/google/android/music/download/cache/StorageProbeUtils$ProbeMethod;

    .line 39
    new-instance v0, Lcom/google/android/music/download/cache/StorageProbeUtils$ProbeMethod;

    const-string v1, "CONTEXT_APIS"

    invoke-direct {v0, v1, v4}, Lcom/google/android/music/download/cache/StorageProbeUtils$ProbeMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/music/download/cache/StorageProbeUtils$ProbeMethod;->CONTEXT_APIS:Lcom/google/android/music/download/cache/StorageProbeUtils$ProbeMethod;

    .line 36
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/music/download/cache/StorageProbeUtils$ProbeMethod;

    sget-object v1, Lcom/google/android/music/download/cache/StorageProbeUtils$ProbeMethod;->MOUNT_SERVICE:Lcom/google/android/music/download/cache/StorageProbeUtils$ProbeMethod;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/music/download/cache/StorageProbeUtils$ProbeMethod;->KITKAT_API:Lcom/google/android/music/download/cache/StorageProbeUtils$ProbeMethod;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/music/download/cache/StorageProbeUtils$ProbeMethod;->CONTEXT_APIS:Lcom/google/android/music/download/cache/StorageProbeUtils$ProbeMethod;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/music/download/cache/StorageProbeUtils$ProbeMethod;->$VALUES:[Lcom/google/android/music/download/cache/StorageProbeUtils$ProbeMethod;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/music/download/cache/StorageProbeUtils$ProbeMethod;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 36
    const-class v0, Lcom/google/android/music/download/cache/StorageProbeUtils$ProbeMethod;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/download/cache/StorageProbeUtils$ProbeMethod;

    return-object v0
.end method

.method public static values()[Lcom/google/android/music/download/cache/StorageProbeUtils$ProbeMethod;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lcom/google/android/music/download/cache/StorageProbeUtils$ProbeMethod;->$VALUES:[Lcom/google/android/music/download/cache/StorageProbeUtils$ProbeMethod;

    invoke-virtual {v0}, [Lcom/google/android/music/download/cache/StorageProbeUtils$ProbeMethod;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/music/download/cache/StorageProbeUtils$ProbeMethod;

    return-object v0
.end method


# virtual methods
.method public getMountPoints(Landroid/content/Context;)Ljava/util/Set;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/music/download/cache/StorageProbeUtils$DiscoveredLocation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 43
    sget-object v1, Lcom/google/android/music/download/cache/StorageProbeUtils$1;->$SwitchMap$com$google$android$music$download$cache$StorageProbeUtils$ProbeMethod:[I

    invoke-virtual {p0}, Lcom/google/android/music/download/cache/StorageProbeUtils$ProbeMethod;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 50
    # invokes: Lcom/google/android/music/download/cache/StorageProbeUtils;->getLocationsWithMountService(Landroid/content/Context;)Ljava/util/Set;
    invoke-static {p1}, Lcom/google/android/music/download/cache/StorageProbeUtils;->access$000(Landroid/content/Context;)Ljava/util/Set;

    move-result-object v0

    .line 53
    .local v0, "mountPoints":Ljava/util/Set;, "Ljava/util/Set<Lcom/google/android/music/download/cache/StorageProbeUtils$DiscoveredLocation;>;"
    :goto_0
    return-object v0

    .line 45
    .end local v0    # "mountPoints":Ljava/util/Set;, "Ljava/util/Set<Lcom/google/android/music/download/cache/StorageProbeUtils$DiscoveredLocation;>;"
    :pswitch_0
    invoke-static {p1}, Lcom/google/android/music/download/cache/StorageProbeUtils;->getLocationsWithKitKatApis(Landroid/content/Context;)Ljava/util/Set;

    move-result-object v0

    .line 46
    .restart local v0    # "mountPoints":Ljava/util/Set;, "Ljava/util/Set<Lcom/google/android/music/download/cache/StorageProbeUtils$DiscoveredLocation;>;"
    goto :goto_0

    .line 43
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
