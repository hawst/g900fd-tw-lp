.class public Lcom/google/android/music/download/stream/StreamingContent;
.super Ljava/lang/Object;
.source "StreamingContent.java"


# static fields
.field private static final LOGV:Z


# instance fields
.field private mCompleted:J

.field private final mContext:Landroid/content/Context;

.field private mDownloadProgress:Lcom/google/android/music/download/TrackDownloadProgress;

.field private final mDownloadRequest:Lcom/google/android/music/download/TrackDownloadRequest;

.field private mExtraChunkSize:J

.field private mFilepath:Ljava/lang/String;

.field private mLastWaitLog:J

.field private final mSecureId:J

.field private volatile mStartReadPoint:J

.field private mWaitingContentTypeAllowed:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->DOWNLOAD:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/download/stream/StreamingContent;->LOGV:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/music/download/TrackDownloadRequest;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "request"    # Lcom/google/android/music/download/TrackDownloadRequest;

    .prologue
    const-wide/16 v2, 0x0

    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput-wide v2, p0, Lcom/google/android/music/download/stream/StreamingContent;->mCompleted:J

    .line 58
    iput-wide v2, p0, Lcom/google/android/music/download/stream/StreamingContent;->mStartReadPoint:J

    .line 59
    iput-wide v2, p0, Lcom/google/android/music/download/stream/StreamingContent;->mLastWaitLog:J

    .line 60
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/download/stream/StreamingContent;->mWaitingContentTypeAllowed:Z

    .line 63
    iput-wide v2, p0, Lcom/google/android/music/download/stream/StreamingContent;->mExtraChunkSize:J

    .line 69
    iput-object p1, p0, Lcom/google/android/music/download/stream/StreamingContent;->mContext:Landroid/content/Context;

    .line 70
    iput-object p2, p0, Lcom/google/android/music/download/stream/StreamingContent;->mDownloadRequest:Lcom/google/android/music/download/TrackDownloadRequest;

    .line 71
    invoke-virtual {p2}, Lcom/google/android/music/download/TrackDownloadRequest;->getFileLocation()Lcom/google/android/music/download/cache/FileLocation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/download/cache/FileLocation;->getFullPath()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/download/stream/StreamingContent;->mFilepath:Ljava/lang/String;

    .line 73
    invoke-static {}, Lcom/google/android/music/download/cp/CpUtils;->getRandom()Ljava/security/SecureRandom;

    move-result-object v0

    invoke-virtual {v0}, Ljava/security/SecureRandom;->nextLong()J

    move-result-wide v0

    const-wide v2, 0x7fffffffffffffffL

    and-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/music/download/stream/StreamingContent;->mSecureId:J

    .line 74
    return-void
.end method

.method public static contentListToString(Ljava/lang/String;Ljava/util/List;)Ljava/lang/String;
    .locals 4
    .param p0, "msg"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/download/stream/StreamingContent;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 414
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/download/stream/StreamingContent;>;"
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 415
    .local v0, "builder":Ljava/lang/StringBuilder;
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 416
    const-string v3, "=["

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 417
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/download/stream/StreamingContent;

    .line 418
    .local v1, "content":Lcom/google/android/music/download/stream/StreamingContent;
    invoke-virtual {v1}, Lcom/google/android/music/download/stream/StreamingContent;->getId()Lcom/google/android/music/download/ContentIdentifier;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 419
    const-string v3, ", "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 421
    .end local v1    # "content":Lcom/google/android/music/download/stream/StreamingContent;
    :cond_0
    const-string v3, "]"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 422
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method


# virtual methods
.method public clearFileIfNotSavable(Lcom/google/android/music/download/cache/ICacheManager;)V
    .locals 5
    .param p1, "cacheManager"    # Lcom/google/android/music/download/cache/ICacheManager;

    .prologue
    .line 130
    if-nez p1, :cond_1

    .line 131
    const-string v2, "StreamingContent"

    const-string v3, "cacheManager is null"

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    :cond_0
    :goto_0
    return-void

    .line 134
    :cond_1
    const/4 v1, 0x0

    .line 135
    .local v1, "shouldDelete":Z
    monitor-enter p0

    .line 137
    :try_start_0
    iget-object v2, p0, Lcom/google/android/music/download/stream/StreamingContent;->mDownloadRequest:Lcom/google/android/music/download/TrackDownloadRequest;

    invoke-virtual {v2}, Lcom/google/android/music/download/TrackDownloadRequest;->getId()Lcom/google/android/music/download/ContentIdentifier;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/music/download/ContentIdentifier;->isCacheable()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/music/download/stream/StreamingContent;->mDownloadProgress:Lcom/google/android/music/download/TrackDownloadProgress;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/music/download/stream/StreamingContent;->mDownloadProgress:Lcom/google/android/music/download/TrackDownloadProgress;

    invoke-virtual {v2}, Lcom/google/android/music/download/TrackDownloadProgress;->isSavable()Z

    move-result v2

    if-nez v2, :cond_3

    .line 139
    :cond_2
    const/4 v1, 0x1

    .line 141
    :cond_3
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 142
    if-eqz v1, :cond_0

    .line 144
    :try_start_1
    sget-boolean v2, Lcom/google/android/music/download/stream/StreamingContent;->LOGV:Z

    if-eqz v2, :cond_4

    .line 145
    const-string v2, "StreamingContent"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "clearFileIfNotSavable: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/music/download/stream/StreamingContent;->mDownloadRequest:Lcom/google/android/music/download/TrackDownloadRequest;

    invoke-virtual {v4}, Lcom/google/android/music/download/TrackDownloadRequest;->getFileLocation()Lcom/google/android/music/download/cache/FileLocation;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/music/download/cache/FileLocation;->getFullPath()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    :cond_4
    iget-object v2, p0, Lcom/google/android/music/download/stream/StreamingContent;->mDownloadRequest:Lcom/google/android/music/download/TrackDownloadRequest;

    invoke-interface {p1, v2}, Lcom/google/android/music/download/cache/ICacheManager;->requestDelete(Lcom/google/android/music/download/DownloadRequest;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 149
    :catch_0
    move-exception v0

    .line 150
    .local v0, "e":Landroid/os/RemoteException;
    const-string v2, "StreamingContent"

    const-string v3, "Failed to call request delete"

    invoke-static {v2, v3, v0}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 141
    .end local v0    # "e":Landroid/os/RemoteException;
    :catchall_0
    move-exception v2

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2
.end method

.method public getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 396
    iget-object v0, p0, Lcom/google/android/music/download/stream/StreamingContent;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public getDownloadRequest()Lcom/google/android/music/download/TrackDownloadRequest;
    .locals 1

    .prologue
    .line 382
    iget-object v0, p0, Lcom/google/android/music/download/stream/StreamingContent;->mDownloadRequest:Lcom/google/android/music/download/TrackDownloadRequest;

    return-object v0
.end method

.method public getId()Lcom/google/android/music/download/ContentIdentifier;
    .locals 1

    .prologue
    .line 375
    iget-object v0, p0, Lcom/google/android/music/download/stream/StreamingContent;->mDownloadRequest:Lcom/google/android/music/download/TrackDownloadRequest;

    invoke-virtual {v0}, Lcom/google/android/music/download/TrackDownloadRequest;->getId()Lcom/google/android/music/download/ContentIdentifier;

    move-result-object v0

    return-object v0
.end method

.method getSecureId()J
    .locals 2

    .prologue
    .line 407
    iget-wide v0, p0, Lcom/google/android/music/download/stream/StreamingContent;->mSecureId:J

    return-wide v0
.end method

.method public getStartReadPoint()J
    .locals 2

    .prologue
    .line 403
    iget-wide v0, p0, Lcom/google/android/music/download/stream/StreamingContent;->mStartReadPoint:J

    return-wide v0
.end method

.method public declared-synchronized getStreamingInput(J)Lcom/google/android/music/download/stream/StreamingInput;
    .locals 13
    .param p1, "offset"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 162
    monitor-enter p0

    const/4 v6, 0x0

    .line 163
    .local v6, "location":Ljava/io/File;
    :try_start_0
    iget-object v9, p0, Lcom/google/android/music/download/stream/StreamingContent;->mFilepath:Ljava/lang/String;

    if-eqz v9, :cond_0

    .line 164
    new-instance v7, Ljava/io/File;

    iget-object v9, p0, Lcom/google/android/music/download/stream/StreamingContent;->mFilepath:Ljava/lang/String;

    invoke-direct {v7, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .end local v6    # "location":Ljava/io/File;
    .local v7, "location":Ljava/io/File;
    move-object v6, v7

    .line 168
    .end local v7    # "location":Ljava/io/File;
    .restart local v6    # "location":Ljava/io/File;
    :goto_0
    if-nez v6, :cond_1

    .line 169
    const/4 v9, 0x0

    .line 196
    :goto_1
    monitor-exit p0

    return-object v9

    .line 166
    :cond_0
    :try_start_1
    iget-object v9, p0, Lcom/google/android/music/download/stream/StreamingContent;->mDownloadRequest:Lcom/google/android/music/download/TrackDownloadRequest;

    invoke-virtual {v9}, Lcom/google/android/music/download/TrackDownloadRequest;->getFileLocation()Lcom/google/android/music/download/cache/FileLocation;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/android/music/download/cache/FileLocation;->getFullPath()Ljava/io/File;

    move-result-object v6

    goto :goto_0

    .line 173
    :cond_1
    const/4 v0, 0x0

    .line 174
    .local v0, "cpData":[B
    iget-object v9, p0, Lcom/google/android/music/download/stream/StreamingContent;->mDownloadRequest:Lcom/google/android/music/download/TrackDownloadRequest;

    invoke-virtual {v9}, Lcom/google/android/music/download/TrackDownloadRequest;->getId()Lcom/google/android/music/download/ContentIdentifier;

    move-result-object v4

    .line 175
    .local v4, "id":Lcom/google/android/music/download/ContentIdentifier;
    invoke-virtual {v4}, Lcom/google/android/music/download/ContentIdentifier;->isCacheable()Z

    move-result v9

    if-eqz v9, :cond_2

    .line 176
    iget-object v9, p0, Lcom/google/android/music/download/stream/StreamingContent;->mContext:Landroid/content/Context;

    invoke-static {v9}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v8

    .line 177
    .local v8, "store":Lcom/google/android/music/store/Store;
    invoke-virtual {v4}, Lcom/google/android/music/download/ContentIdentifier;->getId()J

    move-result-wide v10

    const/4 v9, 0x0

    invoke-virtual {v8, v10, v11, v9}, Lcom/google/android/music/store/Store;->getCpData(JZ)[B

    move-result-object v0

    .line 180
    .end local v8    # "store":Lcom/google/android/music/store/Store;
    :cond_2
    if-nez v0, :cond_3

    .line 181
    new-instance v3, Ljava/io/RandomAccessFile;

    const-string v9, "r"

    invoke-direct {v3, v6, v9}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 182
    .local v3, "file":Ljava/io/RandomAccessFile;
    invoke-virtual {v3, p1, p2}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 183
    new-instance v9, Lcom/google/android/music/download/stream/FileStreamingInput;

    invoke-direct {v9, v3}, Lcom/google/android/music/download/stream/FileStreamingInput;-><init>(Ljava/io/RandomAccessFile;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 162
    .end local v0    # "cpData":[B
    .end local v3    # "file":Ljava/io/RandomAccessFile;
    .end local v4    # "id":Lcom/google/android/music/download/ContentIdentifier;
    :catchall_0
    move-exception v9

    monitor-exit p0

    throw v9

    .line 185
    .restart local v0    # "cpData":[B
    .restart local v4    # "id":Lcom/google/android/music/download/ContentIdentifier;
    :cond_3
    :try_start_2
    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, v6}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 188
    .local v3, "file":Ljava/io/FileInputStream;
    :try_start_3
    new-instance v1, Lcom/google/android/music/download/cp/CpInputStream;

    invoke-direct {v1, v3, v0}, Lcom/google/android/music/download/cp/CpInputStream;-><init>(Ljava/io/InputStream;[B)V
    :try_end_3
    .catch Lcom/google/android/music/download/cp/UnrecognizedDataCpException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 193
    .local v1, "cpInput":Lcom/google/android/music/download/cp/CpInputStream;
    :try_start_4
    new-instance v5, Lcom/google/android/music/io/ChunkedInputStreamAdapter;

    invoke-direct {v5, v1}, Lcom/google/android/music/io/ChunkedInputStreamAdapter;-><init>(Lcom/google/android/music/io/ChunkedInputStream;)V

    .line 194
    .local v5, "in":Lcom/google/android/music/io/ChunkedInputStreamAdapter;
    invoke-virtual {v5, p1, p2}, Lcom/google/android/music/io/ChunkedInputStreamAdapter;->skip(J)J

    .line 195
    invoke-virtual {v1}, Lcom/google/android/music/download/cp/CpInputStream;->getChunkSize()I

    move-result v9

    int-to-long v10, v9

    iput-wide v10, p0, Lcom/google/android/music/download/stream/StreamingContent;->mExtraChunkSize:J

    .line 196
    new-instance v9, Lcom/google/android/music/download/stream/StreamingInputAdapter;

    invoke-direct {v9, v5}, Lcom/google/android/music/download/stream/StreamingInputAdapter;-><init>(Ljava/io/InputStream;)V

    goto :goto_1

    .line 189
    .end local v1    # "cpInput":Lcom/google/android/music/download/cp/CpInputStream;
    .end local v5    # "in":Lcom/google/android/music/io/ChunkedInputStreamAdapter;
    :catch_0
    move-exception v2

    .line 190
    .local v2, "e":Lcom/google/android/music/download/cp/UnrecognizedDataCpException;
    const-string v9, "StreamingContent"

    const-string v10, "Invalid CP file"

    invoke-static {v9, v10, v2}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 191
    new-instance v9, Ljava/io/FileNotFoundException;

    const-string v10, "No valid cp file is found"

    invoke-direct {v9, v10}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v9
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
.end method

.method public hasId(Lcom/google/android/music/download/ContentIdentifier;)Z
    .locals 1
    .param p1, "id"    # Lcom/google/android/music/download/ContentIdentifier;

    .prologue
    .line 361
    iget-object v0, p0, Lcom/google/android/music/download/stream/StreamingContent;->mDownloadRequest:Lcom/google/android/music/download/TrackDownloadRequest;

    invoke-virtual {v0}, Lcom/google/android/music/download/TrackDownloadRequest;->getId()Lcom/google/android/music/download/ContentIdentifier;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/music/download/ContentIdentifier;->equals(Lcom/google/android/music/download/ContentIdentifier;)Z

    move-result v0

    return v0
.end method

.method public hasRequest(Lcom/google/android/music/download/TrackDownloadRequest;)Z
    .locals 1
    .param p1, "request"    # Lcom/google/android/music/download/TrackDownloadRequest;

    .prologue
    .line 368
    iget-object v0, p0, Lcom/google/android/music/download/stream/StreamingContent;->mDownloadRequest:Lcom/google/android/music/download/TrackDownloadRequest;

    invoke-virtual {v0, p1}, Lcom/google/android/music/download/TrackDownloadRequest;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public declared-synchronized isCompleted()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 217
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/music/download/stream/StreamingContent;->mDownloadProgress:Lcom/google/android/music/download/TrackDownloadProgress;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_1

    .line 221
    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/google/android/music/download/stream/StreamingContent;->mDownloadProgress:Lcom/google/android/music/download/TrackDownloadProgress;

    invoke-virtual {v1}, Lcom/google/android/music/download/TrackDownloadProgress;->getState()Lcom/google/android/music/download/DownloadState$State;

    move-result-object v1

    sget-object v2, Lcom/google/android/music/download/DownloadState$State;->COMPLETED:Lcom/google/android/music/download/DownloadState$State;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    .line 217
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized isFinished()Z
    .locals 1

    .prologue
    .line 206
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/download/stream/StreamingContent;->mDownloadProgress:Lcom/google/android/music/download/TrackDownloadProgress;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 207
    const/4 v0, 0x0

    .line 210
    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/music/download/stream/StreamingContent;->mDownloadProgress:Lcom/google/android/music/download/TrackDownloadProgress;

    invoke-virtual {v0}, Lcom/google/android/music/download/TrackDownloadProgress;->getState()Lcom/google/android/music/download/DownloadState$State;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/download/DownloadState$State;->isFinished()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_0

    .line 206
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public isMyProgress(Lcom/google/android/music/download/TrackDownloadProgress;)Z
    .locals 1
    .param p1, "progress"    # Lcom/google/android/music/download/TrackDownloadProgress;

    .prologue
    .line 389
    iget-object v0, p0, Lcom/google/android/music/download/stream/StreamingContent;->mDownloadRequest:Lcom/google/android/music/download/TrackDownloadRequest;

    invoke-virtual {v0, p1}, Lcom/google/android/music/download/TrackDownloadRequest;->isMyProgress(Lcom/google/android/music/download/TrackDownloadProgress;)Z

    move-result v0

    return v0
.end method

.method public declared-synchronized notifyDownloadProgress(Lcom/google/android/music/download/TrackDownloadProgress;Lcom/google/android/music/download/cache/ICacheManager;)V
    .locals 8
    .param p1, "progress"    # Lcom/google/android/music/download/TrackDownloadProgress;
    .param p2, "cacheManager"    # Lcom/google/android/music/download/cache/ICacheManager;

    .prologue
    .line 88
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/music/download/stream/StreamingContent;->mDownloadRequest:Lcom/google/android/music/download/TrackDownloadRequest;

    invoke-virtual {v1, p1}, Lcom/google/android/music/download/TrackDownloadRequest;->isMyProgress(Lcom/google/android/music/download/TrackDownloadProgress;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 89
    sget-boolean v1, Lcom/google/android/music/download/stream/StreamingContent;->LOGV:Z

    if-eqz v1, :cond_0

    .line 90
    const-string v1, "StreamingContent"

    const-string v2, "notifyDownloadProgress: progress not mine, ignoring %s %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/music/download/stream/StreamingContent;->mDownloadRequest:Lcom/google/android/music/download/TrackDownloadRequest;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object p1, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 123
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 96
    :cond_1
    :try_start_1
    iput-object p1, p0, Lcom/google/android/music/download/stream/StreamingContent;->mDownloadProgress:Lcom/google/android/music/download/TrackDownloadProgress;

    .line 97
    invoke-virtual {p1}, Lcom/google/android/music/download/TrackDownloadProgress;->getCompletedBytes()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/music/download/stream/StreamingContent;->mCompleted:J

    .line 99
    iget-object v1, p0, Lcom/google/android/music/download/stream/StreamingContent;->mDownloadProgress:Lcom/google/android/music/download/TrackDownloadProgress;

    invoke-virtual {v1}, Lcom/google/android/music/download/TrackDownloadProgress;->isSavable()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    if-eqz v1, :cond_3

    .line 101
    const/4 v7, 0x0

    .line 102
    .local v7, "newName":Ljava/lang/String;
    if-eqz p2, :cond_4

    .line 103
    :try_start_2
    sget-boolean v1, Lcom/google/android/music/download/stream/StreamingContent;->LOGV:Z

    if-eqz v1, :cond_2

    .line 104
    const-string v1, "StreamingContent"

    const-string v2, "storeInCache: %s %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/music/download/stream/StreamingContent;->mDownloadRequest:Lcom/google/android/music/download/TrackDownloadRequest;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object p1, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    :cond_2
    iget-object v2, p0, Lcom/google/android/music/download/stream/StreamingContent;->mDownloadRequest:Lcom/google/android/music/download/TrackDownloadRequest;

    iget-object v1, p0, Lcom/google/android/music/download/stream/StreamingContent;->mDownloadProgress:Lcom/google/android/music/download/TrackDownloadProgress;

    invoke-virtual {v1}, Lcom/google/android/music/download/TrackDownloadProgress;->getHttpContentType()Ljava/lang/String;

    move-result-object v3

    iget-object v1, p0, Lcom/google/android/music/download/stream/StreamingContent;->mDownloadProgress:Lcom/google/android/music/download/TrackDownloadProgress;

    invoke-virtual {v1}, Lcom/google/android/music/download/TrackDownloadProgress;->getDownloadByteLength()J

    move-result-wide v4

    iget-object v1, p0, Lcom/google/android/music/download/stream/StreamingContent;->mDownloadProgress:Lcom/google/android/music/download/TrackDownloadProgress;

    invoke-virtual {v1}, Lcom/google/android/music/download/TrackDownloadProgress;->getStreamFidelity()I

    move-result v6

    move-object v1, p2

    invoke-interface/range {v1 .. v6}, Lcom/google/android/music/download/cache/ICacheManager;->storeInCache(Lcom/google/android/music/download/DownloadRequest;Ljava/lang/String;JI)Ljava/lang/String;

    move-result-object v7

    .line 113
    :goto_1
    if-nez v7, :cond_5

    .line 114
    const-string v1, "StreamingContent"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to store request in the cache"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/music/download/stream/StreamingContent;->mDownloadRequest:Lcom/google/android/music/download/TrackDownloadRequest;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 122
    .end local v7    # "newName":Ljava/lang/String;
    :cond_3
    :goto_2
    :try_start_3
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 88
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 111
    .restart local v7    # "newName":Ljava/lang/String;
    :cond_4
    :try_start_4
    const-string v1, "StreamingContent"

    const-string v2, "cacheManager is null"

    invoke-static {v1, v2}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 118
    :catch_0
    move-exception v0

    .line 119
    .local v0, "e":Landroid/os/RemoteException;
    :try_start_5
    const-string v1, "StreamingContent"

    const-string v2, "Failed to call store cache"

    invoke-static {v1, v2, v0}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_2

    .line 116
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_5
    :try_start_6
    iput-object v7, p0, Lcom/google/android/music/download/stream/StreamingContent;->mFilepath:Ljava/lang/String;
    :try_end_6
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_2
.end method

.method public declared-synchronized setWaitingContentTypeAllowed(Z)V
    .locals 5
    .param p1, "waitingAllowed"    # Z

    .prologue
    .line 237
    monitor-enter p0

    :try_start_0
    sget-boolean v0, Lcom/google/android/music/download/stream/StreamingContent;->LOGV:Z

    if-eqz v0, :cond_0

    .line 238
    const-string v0, "StreamingContent"

    const-string v1, "setWaitingContentTypeAllowed: %s request=%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/music/download/stream/StreamingContent;->mDownloadRequest:Lcom/google/android/music/download/TrackDownloadRequest;

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    :cond_0
    iput-boolean p1, p0, Lcom/google/android/music/download/stream/StreamingContent;->mWaitingContentTypeAllowed:Z

    .line 242
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 243
    monitor-exit p0

    return-void

    .line 237
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized shouldFilter(Ljava/lang/String;)Z
    .locals 1
    .param p1, "fullPathName"    # Ljava/lang/String;

    .prologue
    .line 354
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/download/stream/StreamingContent;->mFilepath:Ljava/lang/String;

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/music/download/stream/StreamingContent;->mDownloadRequest:Lcom/google/android/music/download/TrackDownloadRequest;

    invoke-virtual {v0}, Lcom/google/android/music/download/TrackDownloadRequest;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized waitForContentType()Ljava/lang/String;
    .locals 14
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 251
    monitor-enter p0

    :try_start_0
    sget-boolean v8, Lcom/google/android/music/download/stream/StreamingContent;->LOGV:Z

    if-eqz v8, :cond_0

    .line 252
    const-string v8, "StreamingContent"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "waitForContentType: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/google/android/music/download/stream/StreamingContent;->mDownloadRequest:Lcom/google/android/music/download/TrackDownloadRequest;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    :cond_0
    :goto_0
    iget-object v8, p0, Lcom/google/android/music/download/stream/StreamingContent;->mDownloadProgress:Lcom/google/android/music/download/TrackDownloadProgress;

    if-eqz v8, :cond_1

    iget-object v8, p0, Lcom/google/android/music/download/stream/StreamingContent;->mDownloadProgress:Lcom/google/android/music/download/TrackDownloadProgress;

    invoke-virtual {v8}, Lcom/google/android/music/download/TrackDownloadProgress;->getHttpContentType()Ljava/lang/String;

    move-result-object v8

    if-nez v8, :cond_5

    invoke-virtual {p0}, Lcom/google/android/music/download/stream/StreamingContent;->isFinished()Z

    move-result v8

    if-nez v8, :cond_5

    .line 257
    :cond_1
    iget-boolean v8, p0, Lcom/google/android/music/download/stream/StreamingContent;->mWaitingContentTypeAllowed:Z

    if-nez v8, :cond_4

    .line 258
    sget-boolean v8, Lcom/google/android/music/download/stream/StreamingContent;->LOGV:Z

    if-eqz v8, :cond_2

    .line 259
    const-string v8, "StreamingContent"

    const-string v9, "waitForContentType: streaming not allowed request=%s"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    iget-object v12, p0, Lcom/google/android/music/download/stream/StreamingContent;->mDownloadRequest:Lcom/google/android/music/download/TrackDownloadRequest;

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_2
    move-object v0, v5

    .line 320
    :cond_3
    :goto_1
    monitor-exit p0

    return-object v0

    .line 264
    :cond_4
    :try_start_1
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 251
    :catchall_0
    move-exception v5

    monitor-exit p0

    throw v5

    .line 267
    :cond_5
    :try_start_2
    iget-object v8, p0, Lcom/google/android/music/download/stream/StreamingContent;->mDownloadProgress:Lcom/google/android/music/download/TrackDownloadProgress;

    invoke-virtual {v8}, Lcom/google/android/music/download/TrackDownloadProgress;->getHttpContentType()Ljava/lang/String;

    move-result-object v0

    .line 268
    .local v0, "contentType":Ljava/lang/String;
    if-nez v0, :cond_3

    .line 272
    iget-object v8, p0, Lcom/google/android/music/download/stream/StreamingContent;->mDownloadProgress:Lcom/google/android/music/download/TrackDownloadProgress;

    invoke-virtual {v8}, Lcom/google/android/music/download/TrackDownloadProgress;->getState()Lcom/google/android/music/download/DownloadState$State;

    move-result-object v8

    sget-object v9, Lcom/google/android/music/download/DownloadState$State;->COMPLETED:Lcom/google/android/music/download/DownloadState$State;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eq v8, v9, :cond_6

    move-object v0, v5

    .line 273
    goto :goto_1

    .line 276
    :cond_6
    const/4 v4, 0x0

    .line 278
    .local v4, "musicFile":Lcom/google/android/music/store/MusicFile;
    :try_start_3
    iget-object v8, p0, Lcom/google/android/music/download/stream/StreamingContent;->mContext:Landroid/content/Context;

    invoke-static {v8}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v8

    const/4 v9, 0x0

    iget-object v10, p0, Lcom/google/android/music/download/stream/StreamingContent;->mDownloadRequest:Lcom/google/android/music/download/TrackDownloadRequest;

    invoke-virtual {v10}, Lcom/google/android/music/download/TrackDownloadRequest;->getId()Lcom/google/android/music/download/ContentIdentifier;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/android/music/download/ContentIdentifier;->getId()J

    move-result-wide v10

    invoke-static {v8, v9, v10, v11}, Lcom/google/android/music/store/MusicFile;->getSummaryMusicFile(Lcom/google/android/music/store/Store;Lcom/google/android/music/store/MusicFile;J)Lcom/google/android/music/store/MusicFile;
    :try_end_3
    .catch Lcom/google/android/music/store/DataNotFoundException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v4

    .line 285
    if-nez v4, :cond_7

    .line 286
    :try_start_4
    const-string v8, "StreamingContent"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Failed to load music file for "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/google/android/music/download/stream/StreamingContent;->mDownloadRequest:Lcom/google/android/music/download/TrackDownloadRequest;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v5

    .line 287
    goto :goto_1

    .line 280
    :catch_0
    move-exception v1

    .line 281
    .local v1, "e":Lcom/google/android/music/store/DataNotFoundException;
    const-string v8, "StreamingContent"

    const-string v9, "Failed to load track data: "

    invoke-static {v8, v9, v1}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v5

    .line 282
    goto :goto_1

    .line 290
    .end local v1    # "e":Lcom/google/android/music/store/DataNotFoundException;
    :cond_7
    iget-object v8, p0, Lcom/google/android/music/download/stream/StreamingContent;->mContext:Landroid/content/Context;

    invoke-static {v8, v4}, Lcom/google/android/music/download/cache/CacheUtils;->resolveMusicPath(Landroid/content/Context;Lcom/google/android/music/store/MusicFile;)Ljava/io/File;

    move-result-object v3

    .line 291
    .local v3, "location":Ljava/io/File;
    invoke-static {v3}, Lcom/google/android/music/utils/IOUtils;->getFileExtension(Ljava/io/File;)Ljava/lang/String;

    move-result-object v2

    .line 292
    .local v2, "extension":Ljava/lang/String;
    if-nez v2, :cond_8

    .line 293
    const-string v8, "StreamingContent"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Failed to parse file extension for location: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v5

    .line 294
    goto/16 :goto_1

    .line 297
    :cond_8
    sget-object v5, Lcom/google/android/music/download/DownloadUtils;->ExtensionToMimeMap:Lcom/google/common/collect/ImmutableMap;

    invoke-virtual {v5, v2}, Lcom/google/common/collect/ImmutableMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "contentType":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .line 298
    .restart local v0    # "contentType":Ljava/lang/String;
    invoke-virtual {v4}, Lcom/google/android/music/store/MusicFile;->getLocalCopySize()J

    move-result-wide v8

    iput-wide v8, p0, Lcom/google/android/music/download/stream/StreamingContent;->mCompleted:J

    .line 304
    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/music/download/stream/StreamingContent;->mFilepath:Ljava/lang/String;

    .line 305
    iget-object v5, p0, Lcom/google/android/music/download/stream/StreamingContent;->mDownloadProgress:Lcom/google/android/music/download/TrackDownloadProgress;

    invoke-virtual {v5}, Lcom/google/android/music/download/TrackDownloadProgress;->getSeekMs()J

    move-result-wide v6

    .line 306
    .local v6, "seekMs":J
    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-eqz v5, :cond_9

    .line 308
    iget-wide v8, p0, Lcom/google/android/music/download/stream/StreamingContent;->mCompleted:J

    long-to-float v5, v8

    long-to-float v8, v6

    mul-float/2addr v5, v8

    invoke-virtual {v4}, Lcom/google/android/music/store/MusicFile;->getDurationInMilliSec()J

    move-result-wide v8

    long-to-float v8, v8

    div-float/2addr v5, v8

    float-to-long v8, v5

    iput-wide v8, p0, Lcom/google/android/music/download/stream/StreamingContent;->mStartReadPoint:J

    .line 311
    :cond_9
    sget-boolean v5, Lcom/google/android/music/download/stream/StreamingContent;->LOGV:Z

    if-eqz v5, :cond_a

    .line 312
    const-string v5, "StreamingContent"

    const-string v8, "contentType=%s completed=%d fileName=%s"

    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v0, v9, v10

    const/4 v10, 0x1

    iget-wide v12, p0, Lcom/google/android/music/download/stream/StreamingContent;->mCompleted:J

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x2

    iget-object v11, p0, Lcom/google/android/music/download/stream/StreamingContent;->mFilepath:Ljava/lang/String;

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 316
    :cond_a
    sget-boolean v5, Lcom/google/android/music/download/stream/StreamingContent;->LOGV:Z

    if-eqz v5, :cond_3

    .line 317
    const-string v5, "StreamingContent"

    const-string v8, "contentType: %s for request %s"

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v0, v9, v10

    const/4 v10, 0x1

    iget-object v11, p0, Lcom/google/android/music/download/stream/StreamingContent;->mDownloadRequest:Lcom/google/android/music/download/TrackDownloadRequest;

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8}, Lcom/google/android/music/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_1
.end method

.method public declared-synchronized waitForData(J)V
    .locals 7
    .param p1, "amount"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 335
    monitor-enter p0

    :goto_0
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/music/download/stream/StreamingContent;->isFinished()Z

    move-result v2

    if-nez v2, :cond_1

    iget-wide v2, p0, Lcom/google/android/music/download/stream/StreamingContent;->mCompleted:J

    iget-wide v4, p0, Lcom/google/android/music/download/stream/StreamingContent;->mExtraChunkSize:J

    add-long/2addr v4, p1

    cmp-long v2, v2, v4

    if-gez v2, :cond_1

    .line 336
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 337
    .local v0, "uptimeMs":J
    sget-boolean v2, Lcom/google/android/music/download/stream/StreamingContent;->LOGV:Z

    if-eqz v2, :cond_0

    iget-wide v2, p0, Lcom/google/android/music/download/stream/StreamingContent;->mLastWaitLog:J

    const-wide/16 v4, 0x2710

    add-long/2addr v2, v4

    cmp-long v2, v2, v0

    if-gez v2, :cond_0

    .line 338
    iput-wide v0, p0, Lcom/google/android/music/download/stream/StreamingContent;->mLastWaitLog:J

    .line 339
    const-string v2, "StreamingContent"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "waiting for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " bytes in file: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/music/download/stream/StreamingContent;->mFilepath:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 342
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 335
    .end local v0    # "uptimeMs":J
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 344
    :cond_1
    monitor-exit p0

    return-void
.end method
