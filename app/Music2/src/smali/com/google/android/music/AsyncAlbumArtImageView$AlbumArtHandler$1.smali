.class Lcom/google/android/music/AsyncAlbumArtImageView$AlbumArtHandler$1;
.super Ljava/lang/Object;
.source "AsyncAlbumArtImageView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/AsyncAlbumArtImageView$AlbumArtHandler;->renderBitmap(Lcom/google/android/music/AsyncAlbumArtImageView;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/AsyncAlbumArtImageView$AlbumArtHandler;

.field final synthetic val$bitmap:Landroid/graphics/Bitmap;

.field final synthetic val$imageView:Lcom/google/android/music/AsyncAlbumArtImageView;

.field final synthetic val$missingUrls:Lcom/google/android/music/download/artwork/ArtIdRecorder$RemoteArtUrlRecorder;

.field final synthetic val$mode:Lcom/google/android/music/AsyncAlbumArtImageView$Mode;


# direct methods
.method constructor <init>(Lcom/google/android/music/AsyncAlbumArtImageView$AlbumArtHandler;Lcom/google/android/music/AsyncAlbumArtImageView;Lcom/google/android/music/AsyncAlbumArtImageView$Mode;Lcom/google/android/music/download/artwork/ArtIdRecorder$RemoteArtUrlRecorder;Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 2026
    iput-object p1, p0, Lcom/google/android/music/AsyncAlbumArtImageView$AlbumArtHandler$1;->this$0:Lcom/google/android/music/AsyncAlbumArtImageView$AlbumArtHandler;

    iput-object p2, p0, Lcom/google/android/music/AsyncAlbumArtImageView$AlbumArtHandler$1;->val$imageView:Lcom/google/android/music/AsyncAlbumArtImageView;

    iput-object p3, p0, Lcom/google/android/music/AsyncAlbumArtImageView$AlbumArtHandler$1;->val$mode:Lcom/google/android/music/AsyncAlbumArtImageView$Mode;

    iput-object p4, p0, Lcom/google/android/music/AsyncAlbumArtImageView$AlbumArtHandler$1;->val$missingUrls:Lcom/google/android/music/download/artwork/ArtIdRecorder$RemoteArtUrlRecorder;

    iput-object p5, p0, Lcom/google/android/music/AsyncAlbumArtImageView$AlbumArtHandler$1;->val$bitmap:Landroid/graphics/Bitmap;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 2029
    iget-object v1, p0, Lcom/google/android/music/AsyncAlbumArtImageView$AlbumArtHandler$1;->val$imageView:Lcom/google/android/music/AsyncAlbumArtImageView;

    monitor-enter v1

    .line 2030
    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView$AlbumArtHandler$1;->val$mode:Lcom/google/android/music/AsyncAlbumArtImageView$Mode;

    iget-object v2, p0, Lcom/google/android/music/AsyncAlbumArtImageView$AlbumArtHandler$1;->val$imageView:Lcom/google/android/music/AsyncAlbumArtImageView;

    # getter for: Lcom/google/android/music/AsyncAlbumArtImageView;->mMode:Lcom/google/android/music/AsyncAlbumArtImageView$Mode;
    invoke-static {v2}, Lcom/google/android/music/AsyncAlbumArtImageView;->access$900(Lcom/google/android/music/AsyncAlbumArtImageView;)Lcom/google/android/music/AsyncAlbumArtImageView$Mode;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2031
    iget-object v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView$AlbumArtHandler$1;->val$imageView:Lcom/google/android/music/AsyncAlbumArtImageView;

    iget-object v2, p0, Lcom/google/android/music/AsyncAlbumArtImageView$AlbumArtHandler$1;->val$missingUrls:Lcom/google/android/music/download/artwork/ArtIdRecorder$RemoteArtUrlRecorder;

    # invokes: Lcom/google/android/music/AsyncAlbumArtImageView;->requestMissingArt(Lcom/google/android/music/download/artwork/ArtIdRecorder$RemoteArtUrlRecorder;)V
    invoke-static {v0, v2}, Lcom/google/android/music/AsyncAlbumArtImageView;->access$1200(Lcom/google/android/music/AsyncAlbumArtImageView;Lcom/google/android/music/download/artwork/ArtIdRecorder$RemoteArtUrlRecorder;)V

    .line 2032
    iget-object v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView$AlbumArtHandler$1;->val$bitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 2033
    iget-object v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView$AlbumArtHandler$1;->val$imageView:Lcom/google/android/music/AsyncAlbumArtImageView;

    iget-object v2, p0, Lcom/google/android/music/AsyncAlbumArtImageView$AlbumArtHandler$1;->val$bitmap:Landroid/graphics/Bitmap;

    const/4 v3, 0x0

    # invokes: Lcom/google/android/music/AsyncAlbumArtImageView;->setAlbumImage(Landroid/graphics/Bitmap;Z)V
    invoke-static {v0, v2, v3}, Lcom/google/android/music/AsyncAlbumArtImageView;->access$1300(Lcom/google/android/music/AsyncAlbumArtImageView;Landroid/graphics/Bitmap;Z)V

    .line 2039
    :cond_0
    :goto_0
    monitor-exit v1

    .line 2040
    return-void

    .line 2037
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/AsyncAlbumArtImageView$AlbumArtHandler$1;->val$bitmap:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/google/android/music/utils/AlbumArtUtils;->recycleBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 2039
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
