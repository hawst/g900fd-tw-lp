.class Lcom/google/android/music/ui/MaterialContainerHeaderView$2;
.super Ljava/lang/Object;
.source "MaterialContainerHeaderView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/MaterialContainerHeaderView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/MaterialContainerHeaderView;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/MaterialContainerHeaderView;)V
    .locals 0

    .prologue
    .line 66
    iput-object p1, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView$2;->this$0:Lcom/google/android/music/ui/MaterialContainerHeaderView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 69
    iget-object v1, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView$2;->this$0:Lcom/google/android/music/ui/MaterialContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/MaterialContainerHeaderView;->mContainerDocument:Lcom/google/android/music/ui/cardlib/model/Document;
    invoke-static {v1}, Lcom/google/android/music/ui/MaterialContainerHeaderView;->access$100(Lcom/google/android/music/ui/MaterialContainerHeaderView;)Lcom/google/android/music/ui/cardlib/model/Document;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 70
    new-instance v0, Lcom/google/android/music/ui/ScreenMenuHandler;

    iget-object v1, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView$2;->this$0:Lcom/google/android/music/ui/MaterialContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/MaterialContainerHeaderView;->mFragment:Lcom/google/android/music/ui/BaseListFragment;
    invoke-static {v1}, Lcom/google/android/music/ui/MaterialContainerHeaderView;->access$200(Lcom/google/android/music/ui/MaterialContainerHeaderView;)Lcom/google/android/music/ui/BaseListFragment;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/ui/MaterialContainerHeaderView$2;->this$0:Lcom/google/android/music/ui/MaterialContainerHeaderView;

    # getter for: Lcom/google/android/music/ui/MaterialContainerHeaderView;->mContainerDocument:Lcom/google/android/music/ui/cardlib/model/Document;
    invoke-static {v2}, Lcom/google/android/music/ui/MaterialContainerHeaderView;->access$100(Lcom/google/android/music/ui/MaterialContainerHeaderView;)Lcom/google/android/music/ui/cardlib/model/Document;

    move-result-object v2

    sget-object v3, Lcom/google/android/music/ui/ScreenMenuHandler$ScreenMenuType;->TRACK_CONTAINER:Lcom/google/android/music/ui/ScreenMenuHandler$ScreenMenuType;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/music/ui/ScreenMenuHandler;-><init>(Lcom/google/android/music/ui/MusicFragment;Lcom/google/android/music/ui/cardlib/model/Document;Lcom/google/android/music/ui/ScreenMenuHandler$ScreenMenuType;)V

    .line 73
    .local v0, "menuHandler":Lcom/google/android/music/ui/ScreenMenuHandler;
    invoke-virtual {v0, p1}, Lcom/google/android/music/ui/ScreenMenuHandler;->showPopupMenu(Landroid/view/View;)V

    .line 98
    .end local v0    # "menuHandler":Lcom/google/android/music/ui/ScreenMenuHandler;
    :goto_0
    return-void

    .line 76
    :cond_0
    new-instance v1, Lcom/google/android/music/ui/MaterialContainerHeaderView$2$1;

    invoke-direct {v1, p0, p1}, Lcom/google/android/music/ui/MaterialContainerHeaderView$2$1;-><init>(Lcom/google/android/music/ui/MaterialContainerHeaderView$2;Landroid/view/View;)V

    invoke-static {v1}, Lcom/google/android/music/utils/MusicUtils;->runAsyncWithCallback(Lcom/google/android/music/utils/async/AsyncRunner;)V

    goto :goto_0
.end method
