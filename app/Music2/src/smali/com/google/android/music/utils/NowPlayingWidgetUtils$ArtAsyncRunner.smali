.class Lcom/google/android/music/utils/NowPlayingWidgetUtils$ArtAsyncRunner;
.super Ljava/lang/Object;
.source "NowPlayingWidgetUtils.java"

# interfaces
.implements Lcom/google/android/music/utils/async/AsyncRunner;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/utils/NowPlayingWidgetUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ArtAsyncRunner"
.end annotation


# instance fields
.field private mArtBitmap:Landroid/graphics/Bitmap;

.field private final mArtDimen:I

.field private mArtResourceId:I

.field private mContext:Landroid/content/Context;

.field private final mMetadata:Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData;

.field private final mViews:Landroid/widget/RemoteViews;

.field private final mWidgetId:I


# direct methods
.method public constructor <init>(Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData;IILandroid/widget/RemoteViews;Landroid/content/Context;)V
    .locals 1
    .param p1, "metadata"    # Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData;
    .param p2, "artDimen"    # I
    .param p3, "widgetId"    # I
    .param p4, "views"    # Landroid/widget/RemoteViews;
    .param p5, "context"    # Landroid/content/Context;

    .prologue
    .line 476
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 471
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/music/utils/NowPlayingWidgetUtils$ArtAsyncRunner;->mArtResourceId:I

    .line 477
    iput-object p1, p0, Lcom/google/android/music/utils/NowPlayingWidgetUtils$ArtAsyncRunner;->mMetadata:Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData;

    .line 478
    iput p2, p0, Lcom/google/android/music/utils/NowPlayingWidgetUtils$ArtAsyncRunner;->mArtDimen:I

    .line 479
    iput-object p4, p0, Lcom/google/android/music/utils/NowPlayingWidgetUtils$ArtAsyncRunner;->mViews:Landroid/widget/RemoteViews;

    .line 480
    iput p3, p0, Lcom/google/android/music/utils/NowPlayingWidgetUtils$ArtAsyncRunner;->mWidgetId:I

    .line 481
    iput-object p5, p0, Lcom/google/android/music/utils/NowPlayingWidgetUtils$ArtAsyncRunner;->mContext:Landroid/content/Context;

    .line 482
    return-void
.end method


# virtual methods
.method public backgroundTask()V
    .locals 11

    .prologue
    const/4 v4, -0x1

    const/4 v9, 0x0

    const/4 v6, 0x1

    .line 486
    iget-object v0, p0, Lcom/google/android/music/utils/NowPlayingWidgetUtils$ArtAsyncRunner;->mMetadata:Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData;

    iget-object v0, v0, Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData;->art:Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData$Art;

    iget-boolean v0, v0, Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData$Art;->fromService:Z

    if-eqz v0, :cond_0

    .line 487
    iget-object v1, p0, Lcom/google/android/music/utils/NowPlayingWidgetUtils$ArtAsyncRunner;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/google/android/music/utils/NowPlayingWidgetUtils$ArtAsyncRunner;->mMetadata:Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData;

    iget-wide v2, v0, Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData;->albumId:J

    iget v4, p0, Lcom/google/android/music/utils/NowPlayingWidgetUtils$ArtAsyncRunner;->mArtDimen:I

    iget v5, p0, Lcom/google/android/music/utils/NowPlayingWidgetUtils$ArtAsyncRunner;->mArtDimen:I

    iget-object v0, p0, Lcom/google/android/music/utils/NowPlayingWidgetUtils$ArtAsyncRunner;->mMetadata:Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData;

    iget-object v7, v0, Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData;->album:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/music/utils/NowPlayingWidgetUtils$ArtAsyncRunner;->mMetadata:Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData;

    iget-object v8, v0, Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData;->artist:Ljava/lang/String;

    move v10, v6

    invoke-static/range {v1 .. v10}, Lcom/google/android/music/utils/AlbumArtUtils;->getArtwork(Landroid/content/Context;JIIZLjava/lang/String;Ljava/lang/String;Lcom/google/android/music/download/artwork/AlbumIdSink;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/utils/NowPlayingWidgetUtils$ArtAsyncRunner;->mArtBitmap:Landroid/graphics/Bitmap;

    .line 501
    :goto_0
    return-void

    .line 490
    :cond_0
    iget-object v0, p0, Lcom/google/android/music/utils/NowPlayingWidgetUtils$ArtAsyncRunner;->mMetadata:Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData;

    iget-object v0, v0, Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData;->art:Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData$Art;

    iget-object v0, v0, Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData$Art;->url:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 491
    iget-object v2, p0, Lcom/google/android/music/utils/NowPlayingWidgetUtils$ArtAsyncRunner;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/google/android/music/utils/NowPlayingWidgetUtils$ArtAsyncRunner;->mMetadata:Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData;

    iget-object v0, v0, Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData;->art:Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData$Art;

    iget-object v3, v0, Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData$Art;->url:Ljava/lang/String;

    iget v4, p0, Lcom/google/android/music/utils/NowPlayingWidgetUtils$ArtAsyncRunner;->mArtDimen:I

    iget v5, p0, Lcom/google/android/music/utils/NowPlayingWidgetUtils$ArtAsyncRunner;->mArtDimen:I

    move v7, v6

    move v8, v6

    invoke-static/range {v2 .. v8}, Lcom/google/android/music/utils/AlbumArtUtils;->getExternalAlbumArtBitmap(Landroid/content/Context;Ljava/lang/String;IIZZZ)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/utils/NowPlayingWidgetUtils$ArtAsyncRunner;->mArtBitmap:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 493
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/utils/NowPlayingWidgetUtils$ArtAsyncRunner;->mMetadata:Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData;

    iget-object v0, v0, Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData;->art:Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData$Art;

    iget v0, v0, Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData$Art;->resourceId:I

    if-eq v0, v4, :cond_2

    .line 494
    iget-object v0, p0, Lcom/google/android/music/utils/NowPlayingWidgetUtils$ArtAsyncRunner;->mMetadata:Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData;

    iget-object v0, v0, Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData;->art:Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData$Art;

    iget v0, v0, Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData$Art;->resourceId:I

    iput v0, p0, Lcom/google/android/music/utils/NowPlayingWidgetUtils$ArtAsyncRunner;->mArtResourceId:I

    goto :goto_0

    .line 495
    :cond_2
    iget-object v0, p0, Lcom/google/android/music/utils/NowPlayingWidgetUtils$ArtAsyncRunner;->mMetadata:Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData;

    iget-wide v0, v0, Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData;->albumId:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_3

    .line 496
    iget-object v1, p0, Lcom/google/android/music/utils/NowPlayingWidgetUtils$ArtAsyncRunner;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/google/android/music/utils/NowPlayingWidgetUtils$ArtAsyncRunner;->mMetadata:Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData;

    iget-wide v2, v0, Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData;->albumId:J

    iget v4, p0, Lcom/google/android/music/utils/NowPlayingWidgetUtils$ArtAsyncRunner;->mArtDimen:I

    iget v5, p0, Lcom/google/android/music/utils/NowPlayingWidgetUtils$ArtAsyncRunner;->mArtDimen:I

    move-object v7, v9

    move-object v8, v9

    move v10, v6

    invoke-static/range {v1 .. v10}, Lcom/google/android/music/utils/AlbumArtUtils;->getArtwork(Landroid/content/Context;JIIZLjava/lang/String;Ljava/lang/String;Lcom/google/android/music/download/artwork/AlbumIdSink;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/utils/NowPlayingWidgetUtils$ArtAsyncRunner;->mArtBitmap:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 499
    :cond_3
    iput v4, p0, Lcom/google/android/music/utils/NowPlayingWidgetUtils$ArtAsyncRunner;->mArtResourceId:I

    goto :goto_0
.end method

.method public taskCompleted()V
    .locals 3

    .prologue
    const v2, 0x7f0e01bd

    .line 505
    iget-object v0, p0, Lcom/google/android/music/utils/NowPlayingWidgetUtils$ArtAsyncRunner;->mViews:Landroid/widget/RemoteViews;

    if-eqz v0, :cond_0

    .line 506
    iget-object v0, p0, Lcom/google/android/music/utils/NowPlayingWidgetUtils$ArtAsyncRunner;->mArtBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 507
    iget-object v0, p0, Lcom/google/android/music/utils/NowPlayingWidgetUtils$ArtAsyncRunner;->mViews:Landroid/widget/RemoteViews;

    iget-object v1, p0, Lcom/google/android/music/utils/NowPlayingWidgetUtils$ArtAsyncRunner;->mArtBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v2, v1}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    .line 508
    iget-object v0, p0, Lcom/google/android/music/utils/NowPlayingWidgetUtils$ArtAsyncRunner;->mViews:Landroid/widget/RemoteViews;

    iget-object v1, p0, Lcom/google/android/music/utils/NowPlayingWidgetUtils$ArtAsyncRunner;->mMetadata:Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData;

    iget-object v1, v1, Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData;->state:Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData$State;

    iget-object v1, v1, Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData$State;->launchIntent:Landroid/app/PendingIntent;

    invoke-virtual {v0, v2, v1}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 515
    :goto_0
    iget-object v0, p0, Lcom/google/android/music/utils/NowPlayingWidgetUtils$ArtAsyncRunner;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v0

    iget v1, p0, Lcom/google/android/music/utils/NowPlayingWidgetUtils$ArtAsyncRunner;->mWidgetId:I

    iget-object v2, p0, Lcom/google/android/music/utils/NowPlayingWidgetUtils$ArtAsyncRunner;->mViews:Landroid/widget/RemoteViews;

    invoke-virtual {v0, v1, v2}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    .line 517
    :cond_0
    return-void

    .line 509
    :cond_1
    iget v0, p0, Lcom/google/android/music/utils/NowPlayingWidgetUtils$ArtAsyncRunner;->mArtResourceId:I

    if-lez v0, :cond_2

    .line 510
    iget-object v0, p0, Lcom/google/android/music/utils/NowPlayingWidgetUtils$ArtAsyncRunner;->mViews:Landroid/widget/RemoteViews;

    iget v1, p0, Lcom/google/android/music/utils/NowPlayingWidgetUtils$ArtAsyncRunner;->mArtResourceId:I

    invoke-virtual {v0, v2, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 511
    iget-object v0, p0, Lcom/google/android/music/utils/NowPlayingWidgetUtils$ArtAsyncRunner;->mViews:Landroid/widget/RemoteViews;

    iget-object v1, p0, Lcom/google/android/music/utils/NowPlayingWidgetUtils$ArtAsyncRunner;->mMetadata:Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData;

    iget-object v1, v1, Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData;->state:Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData$State;

    iget-object v1, v1, Lcom/google/android/music/utils/NowPlayingWidgetUtils$MetaData$State;->launchIntent:Landroid/app/PendingIntent;

    invoke-virtual {v0, v2, v1}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    goto :goto_0

    .line 513
    :cond_2
    const-string v0, "MusicWidgetUtils"

    const-string v1, "Failed to set album art for the remote views"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
