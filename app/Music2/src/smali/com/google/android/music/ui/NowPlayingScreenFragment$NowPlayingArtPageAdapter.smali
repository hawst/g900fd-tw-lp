.class Lcom/google/android/music/ui/NowPlayingScreenFragment$NowPlayingArtPageAdapter;
.super Landroid/support/v4/app/FragmentStatePagerAdapter;
.source "NowPlayingScreenFragment.java"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/NowPlayingScreenFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "NowPlayingArtPageAdapter"
.end annotation


# instance fields
.field private mPager:Landroid/support/v4/view/ViewPager;

.field final synthetic this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/music/ui/NowPlayingScreenFragment;Landroid/support/v4/view/ViewPager;)V
    .locals 1
    .param p2, "pager"    # Landroid/support/v4/view/ViewPager;

    .prologue
    .line 1863
    iput-object p1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$NowPlayingArtPageAdapter;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    .line 1864
    invoke-virtual {p1}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/support/v4/app/FragmentStatePagerAdapter;-><init>(Landroid/support/v4/app/FragmentManager;)V

    .line 1865
    iput-object p2, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$NowPlayingArtPageAdapter;->mPager:Landroid/support/v4/view/ViewPager;

    .line 1866
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$NowPlayingArtPageAdapter;->mPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p0}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 1867
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$NowPlayingArtPageAdapter;->mPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p0}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 1868
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 1929
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$NowPlayingArtPageAdapter;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # invokes: Lcom/google/android/music/ui/NowPlayingScreenFragment;->getPageCount()I
    invoke-static {v0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$3000(Lcom/google/android/music/ui/NowPlayingScreenFragment;)I

    move-result v0

    return v0
.end method

.method public getItem(I)Landroid/support/v4/app/Fragment;
    .locals 31
    .param p1, "position"    # I

    .prologue
    .line 1936
    invoke-static {}, Lcom/google/android/music/utils/MusicUtils;->assertUiThread()V

    .line 1938
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/music/ui/NowPlayingScreenFragment$NowPlayingArtPageAdapter;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # getter for: Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueCursor:Landroid/database/Cursor;
    invoke-static {v1}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$2900(Lcom/google/android/music/ui/NowPlayingScreenFragment;)Landroid/database/Cursor;

    move-result-object v1

    if-eqz v1, :cond_6

    .line 1939
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/music/ui/NowPlayingScreenFragment$NowPlayingArtPageAdapter;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # getter for: Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueCursor:Landroid/database/Cursor;
    invoke-static {v1}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$2900(Lcom/google/android/music/ui/NowPlayingScreenFragment;)Landroid/database/Cursor;

    move-result-object v1

    invoke-interface {v1}, Landroid/database/Cursor;->getPosition()I

    move-result v27

    .line 1941
    .local v27, "oldPos":I
    :try_start_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/music/ui/NowPlayingScreenFragment$NowPlayingArtPageAdapter;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # getter for: Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueCursor:Landroid/database/Cursor;
    invoke-static {v1}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$2900(Lcom/google/android/music/ui/NowPlayingScreenFragment;)Landroid/database/Cursor;

    move-result-object v1

    move/from16 v0, p1

    invoke-interface {v1, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1942
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/music/ui/NowPlayingScreenFragment$NowPlayingArtPageAdapter;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # getter for: Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueCursor:Landroid/database/Cursor;
    invoke-static {v1}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$2900(Lcom/google/android/music/ui/NowPlayingScreenFragment;)Landroid/database/Cursor;

    move-result-object v1

    const-string v13, "audio_id"

    invoke-interface {v1, v13}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v26

    .line 1944
    .local v26, "musicIdIdx":I
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/music/ui/NowPlayingScreenFragment$NowPlayingArtPageAdapter;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # getter for: Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueCursor:Landroid/database/Cursor;
    invoke-static {v1}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$2900(Lcom/google/android/music/ui/NowPlayingScreenFragment;)Landroid/database/Cursor;

    move-result-object v1

    const-string v13, "title"

    invoke-interface {v1, v13}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v28

    .line 1946
    .local v28, "titleIdx":I
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/music/ui/NowPlayingScreenFragment$NowPlayingArtPageAdapter;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # getter for: Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueCursor:Landroid/database/Cursor;
    invoke-static {v1}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$2900(Lcom/google/android/music/ui/NowPlayingScreenFragment;)Landroid/database/Cursor;

    move-result-object v1

    const-string v13, "album_id"

    invoke-interface {v1, v13}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v12

    .line 1948
    .local v12, "albumIdIdx":I
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/music/ui/NowPlayingScreenFragment$NowPlayingArtPageAdapter;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # getter for: Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueCursor:Landroid/database/Cursor;
    invoke-static {v1}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$2900(Lcom/google/android/music/ui/NowPlayingScreenFragment;)Landroid/database/Cursor;

    move-result-object v1

    const-string v13, "album"

    invoke-interface {v1, v13}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v24

    .line 1950
    .local v24, "albumNameIdx":I
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/music/ui/NowPlayingScreenFragment$NowPlayingArtPageAdapter;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # getter for: Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueCursor:Landroid/database/Cursor;
    invoke-static {v1}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$2900(Lcom/google/android/music/ui/NowPlayingScreenFragment;)Landroid/database/Cursor;

    move-result-object v1

    const-string v13, "artist"

    invoke-interface {v1, v13}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v25

    .line 1952
    .local v25, "artistNameIdx":I
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/music/ui/NowPlayingScreenFragment$NowPlayingArtPageAdapter;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # getter for: Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueCursor:Landroid/database/Cursor;
    invoke-static {v1}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$2900(Lcom/google/android/music/ui/NowPlayingScreenFragment;)Landroid/database/Cursor;

    move-result-object v1

    const-string v13, "Vid"

    invoke-interface {v1, v13}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v29

    .line 1954
    .local v29, "videoIdIdx":I
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/music/ui/NowPlayingScreenFragment$NowPlayingArtPageAdapter;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # getter for: Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueCursor:Landroid/database/Cursor;
    invoke-static {v1}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$2900(Lcom/google/android/music/ui/NowPlayingScreenFragment;)Landroid/database/Cursor;

    move-result-object v1

    const-string v13, "VThumbnailUrl"

    invoke-interface {v1, v13}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v30

    .line 1957
    .local v30, "videoThumbnailIdx":I
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/music/ui/NowPlayingScreenFragment$NowPlayingArtPageAdapter;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # getter for: Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueCursor:Landroid/database/Cursor;
    invoke-static {v1}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$2900(Lcom/google/android/music/ui/NowPlayingScreenFragment;)Landroid/database/Cursor;

    move-result-object v1

    move/from16 v0, v28

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1958
    .local v2, "title":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/music/ui/NowPlayingScreenFragment$NowPlayingArtPageAdapter;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # getter for: Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueCursor:Landroid/database/Cursor;
    invoke-static {v1}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$2900(Lcom/google/android/music/ui/NowPlayingScreenFragment;)Landroid/database/Cursor;

    move-result-object v1

    invoke-interface {v1, v12}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    .line 1959
    .local v5, "albumId":J
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/music/ui/NowPlayingScreenFragment$NowPlayingArtPageAdapter;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # getter for: Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueCursor:Landroid/database/Cursor;
    invoke-static {v1}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$2900(Lcom/google/android/music/ui/NowPlayingScreenFragment;)Landroid/database/Cursor;

    move-result-object v1

    move/from16 v0, v24

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1960
    .local v3, "albumName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/music/ui/NowPlayingScreenFragment$NowPlayingArtPageAdapter;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # getter for: Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueCursor:Landroid/database/Cursor;
    invoke-static {v1}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$2900(Lcom/google/android/music/ui/NowPlayingScreenFragment;)Landroid/database/Cursor;

    move-result-object v1

    move/from16 v0, v25

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 1961
    .local v4, "artistName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/music/ui/NowPlayingScreenFragment$NowPlayingArtPageAdapter;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # getter for: Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueCursor:Landroid/database/Cursor;
    invoke-static {v1}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$2900(Lcom/google/android/music/ui/NowPlayingScreenFragment;)Landroid/database/Cursor;

    move-result-object v1

    move/from16 v0, v26

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 1962
    .local v8, "musicId":J
    const/4 v10, 0x0

    .line 1963
    .local v10, "videoId":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/music/ui/NowPlayingScreenFragment$NowPlayingArtPageAdapter;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # getter for: Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueCursor:Landroid/database/Cursor;
    invoke-static {v1}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$2900(Lcom/google/android/music/ui/NowPlayingScreenFragment;)Landroid/database/Cursor;

    move-result-object v1

    move/from16 v0, v29

    invoke-interface {v1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1964
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/music/ui/NowPlayingScreenFragment$NowPlayingArtPageAdapter;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # getter for: Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueCursor:Landroid/database/Cursor;
    invoke-static {v1}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$2900(Lcom/google/android/music/ui/NowPlayingScreenFragment;)Landroid/database/Cursor;

    move-result-object v1

    move/from16 v0, v29

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 1967
    :cond_0
    const/4 v11, 0x0

    .line 1968
    .local v11, "videoThumbnailUrl":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/music/ui/NowPlayingScreenFragment$NowPlayingArtPageAdapter;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # getter for: Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueCursor:Landroid/database/Cursor;
    invoke-static {v1}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$2900(Lcom/google/android/music/ui/NowPlayingScreenFragment;)Landroid/database/Cursor;

    move-result-object v1

    move/from16 v0, v30

    invoke-interface {v1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1969
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/music/ui/NowPlayingScreenFragment$NowPlayingArtPageAdapter;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # getter for: Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueCursor:Landroid/database/Cursor;
    invoke-static {v1}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$2900(Lcom/google/android/music/ui/NowPlayingScreenFragment;)Landroid/database/Cursor;

    move-result-object v1

    move/from16 v0, v30

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 1972
    :cond_1
    invoke-static {v3}, Lcom/google/android/music/utils/MusicUtils;->isUnknown(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1973
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/music/ui/NowPlayingScreenFragment$NowPlayingArtPageAdapter;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    invoke-virtual {v1}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v13, 0x7f0b00c5

    invoke-virtual {v1, v13}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1975
    :cond_2
    invoke-static {v4}, Lcom/google/android/music/utils/MusicUtils;->isUnknown(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1976
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/music/ui/NowPlayingScreenFragment$NowPlayingArtPageAdapter;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    invoke-virtual {v1}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v13, 0x7f0b00c4

    invoke-virtual {v1, v13}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 1979
    :cond_3
    const/4 v7, 0x0

    .line 1980
    .local v7, "artUrl":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/music/ui/NowPlayingScreenFragment$NowPlayingArtPageAdapter;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # getter for: Lcom/google/android/music/ui/NowPlayingScreenFragment;->mSongList:Lcom/google/android/music/medialist/SongList;
    invoke-static {v1}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$3100(Lcom/google/android/music/ui/NowPlayingScreenFragment;)Lcom/google/android/music/medialist/SongList;

    move-result-object v1

    instance-of v1, v1, Lcom/google/android/music/medialist/SharedSongList;

    if-eqz v1, :cond_4

    .line 1981
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/music/ui/NowPlayingScreenFragment$NowPlayingArtPageAdapter;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # getter for: Lcom/google/android/music/ui/NowPlayingScreenFragment;->mSongList:Lcom/google/android/music/medialist/SongList;
    invoke-static {v1}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$3100(Lcom/google/android/music/ui/NowPlayingScreenFragment;)Lcom/google/android/music/medialist/SongList;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/medialist/SharedSongList;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/music/ui/NowPlayingScreenFragment$NowPlayingArtPageAdapter;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    invoke-virtual {v13}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v13

    invoke-virtual {v1, v13}, Lcom/google/android/music/medialist/SharedSongList;->getAlbumArtUrl(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    :cond_4
    move/from16 v1, p1

    .line 1988
    invoke-static/range {v1 .. v11}, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->newInstance(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;JLjava/lang/String;Ljava/lang/String;)Lcom/google/android/music/ui/NowPlayingArtPageFragment;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 1993
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/music/ui/NowPlayingScreenFragment$NowPlayingArtPageAdapter;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # getter for: Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueCursor:Landroid/database/Cursor;
    invoke-static {v13}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$2900(Lcom/google/android/music/ui/NowPlayingScreenFragment;)Landroid/database/Cursor;

    move-result-object v13

    move/from16 v0, v27

    invoke-interface {v13, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 1998
    .end local v2    # "title":Ljava/lang/String;
    .end local v3    # "albumName":Ljava/lang/String;
    .end local v4    # "artistName":Ljava/lang/String;
    .end local v5    # "albumId":J
    .end local v7    # "artUrl":Ljava/lang/String;
    .end local v8    # "musicId":J
    .end local v10    # "videoId":Ljava/lang/String;
    .end local v11    # "videoThumbnailUrl":Ljava/lang/String;
    .end local v12    # "albumIdIdx":I
    .end local v24    # "albumNameIdx":I
    .end local v25    # "artistNameIdx":I
    .end local v26    # "musicIdIdx":I
    .end local v27    # "oldPos":I
    .end local v28    # "titleIdx":I
    .end local v29    # "videoIdIdx":I
    .end local v30    # "videoThumbnailIdx":I
    :goto_0
    return-object v1

    .line 1993
    .restart local v27    # "oldPos":I
    :cond_5
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/music/ui/NowPlayingScreenFragment$NowPlayingArtPageAdapter;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # getter for: Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueCursor:Landroid/database/Cursor;
    invoke-static {v1}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$2900(Lcom/google/android/music/ui/NowPlayingScreenFragment;)Landroid/database/Cursor;

    move-result-object v1

    move/from16 v0, v27

    invoke-interface {v1, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 1996
    .end local v27    # "oldPos":I
    :cond_6
    const-string v1, "NowPlayingFragment"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Could not extract metadata for queue position "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move/from16 v0, p1

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v1, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1998
    const-string v14, ""

    const-string v15, ""

    const-string v16, ""

    const-wide/16 v17, 0x0

    const-string v19, ""

    const-wide/16 v20, 0x0

    const-string v22, ""

    const-string v23, ""

    move/from16 v13, p1

    invoke-static/range {v13 .. v23}, Lcom/google/android/music/ui/NowPlayingArtPageFragment;->newInstance(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;JLjava/lang/String;Ljava/lang/String;)Lcom/google/android/music/ui/NowPlayingArtPageFragment;

    move-result-object v1

    goto :goto_0

    .line 1993
    .restart local v27    # "oldPos":I
    :catchall_0
    move-exception v1

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/music/ui/NowPlayingScreenFragment$NowPlayingArtPageAdapter;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # getter for: Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueCursor:Landroid/database/Cursor;
    invoke-static {v13}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$2900(Lcom/google/android/music/ui/NowPlayingScreenFragment;)Landroid/database/Cursor;

    move-result-object v13

    move/from16 v0, v27

    invoke-interface {v13, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    throw v1
.end method

.method public getItemPosition(Ljava/lang/Object;)I
    .locals 2
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 1879
    iget-object v1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$NowPlayingArtPageAdapter;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # getter for: Lcom/google/android/music/ui/NowPlayingScreenFragment;->mQueueCursor:Landroid/database/Cursor;
    invoke-static {v1}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$2900(Lcom/google/android/music/ui/NowPlayingScreenFragment;)Landroid/database/Cursor;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v0, -0x2

    .line 1924
    :goto_0
    return v0

    .line 1880
    :cond_0
    const/4 v0, -0x2

    .line 1924
    .local v0, "result":I
    goto :goto_0
.end method

.method public onPageScrollStateChanged(I)V
    .locals 0
    .param p1, "state"    # I

    .prologue
    .line 2003
    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 0
    .param p1, "position"    # I
    .param p2, "positionOffset"    # F
    .param p3, "positionOffsetPixels"    # I

    .prologue
    .line 2007
    return-void
.end method

.method public onPageSelected(I)V
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 2011
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$NowPlayingArtPageAdapter;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # invokes: Lcom/google/android/music/ui/NowPlayingScreenFragment;->handlePageSelected(I)V
    invoke-static {v0, p1}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$3200(Lcom/google/android/music/ui/NowPlayingScreenFragment;I)V

    .line 2012
    return-void
.end method

.method public saveState()Landroid/os/Parcelable;
    .locals 1

    .prologue
    .line 1874
    const/4 v0, 0x0

    return-object v0
.end method
