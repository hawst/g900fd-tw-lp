.class public Lcom/google/android/music/log/LogFile;
.super Ljava/lang/Object;
.source "LogFile.java"


# static fields
.field private static final LOGV:Z


# instance fields
.field private mLogDir:Ljava/io/File;

.field private mLogFileName:Ljava/lang/String;

.field private mLogRecord:Ljava/util/logging/LogRecord;

.field private mLogger:Ljava/util/logging/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->LOG_FILE:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/music/log/LogFile;->LOGV:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/io/File;Ljava/lang/String;)V
    .locals 7
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "dir"    # Ljava/io/File;
    .param p3, "fileName"    # Ljava/lang/String;

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    new-instance v3, Ljava/util/logging/LogRecord;

    sget-object v4, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v5, ""

    invoke-direct {v3, v4, v5}, Ljava/util/logging/LogRecord;-><init>(Ljava/util/logging/Level;Ljava/lang/String;)V

    iput-object v3, p0, Lcom/google/android/music/log/LogFile;->mLogRecord:Ljava/util/logging/LogRecord;

    .line 38
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_0

    if-nez p3, :cond_1

    .line 39
    :cond_0
    const-string v3, "LogFile"

    const-string v4, "Invalid configuration provided"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 40
    new-instance v3, Ljava/lang/IllegalArgumentException;

    invoke-direct {v3}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v3

    .line 43
    :cond_1
    iput-object p2, p0, Lcom/google/android/music/log/LogFile;->mLogDir:Ljava/io/File;

    .line 44
    iput-object p3, p0, Lcom/google/android/music/log/LogFile;->mLogFileName:Ljava/lang/String;

    .line 46
    new-instance v2, Ljava/io/File;

    iget-object v3, p0, Lcom/google/android/music/log/LogFile;->mLogDir:Ljava/io/File;

    iget-object v4, p0, Lcom/google/android/music/log/LogFile;->mLogFileName:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 47
    .local v2, "path":Ljava/io/File;
    invoke-static {p1}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/music/log/LogFile;->mLogger:Ljava/util/logging/Logger;

    .line 50
    :try_start_0
    new-instance v1, Ljava/util/logging/FileHandler;

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    const/high16 v4, 0x100000

    const/4 v5, 0x2

    const/4 v6, 0x1

    invoke-direct {v1, v3, v4, v5, v6}, Ljava/util/logging/FileHandler;-><init>(Ljava/lang/String;IIZ)V

    .line 52
    .local v1, "handler":Ljava/util/logging/Handler;
    sget-boolean v3, Lcom/google/android/music/log/LogFile;->LOGV:Z

    if-eqz v3, :cond_2

    .line 53
    const-string v3, "LogFile"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "path="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 55
    :cond_2
    new-instance v3, Lcom/google/android/music/log/CustomFormatter;

    invoke-direct {v3}, Lcom/google/android/music/log/CustomFormatter;-><init>()V

    invoke-virtual {v1, v3}, Ljava/util/logging/Handler;->setFormatter(Ljava/util/logging/Formatter;)V

    .line 56
    iget-object v3, p0, Lcom/google/android/music/log/LogFile;->mLogger:Ljava/util/logging/Logger;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/logging/Logger;->setUseParentHandlers(Z)V

    .line 57
    iget-object v3, p0, Lcom/google/android/music/log/LogFile;->mLogger:Ljava/util/logging/Logger;

    invoke-virtual {v3, v1}, Ljava/util/logging/Logger;->addHandler(Ljava/util/logging/Handler;)V

    .line 58
    iget-object v3, p0, Lcom/google/android/music/log/LogFile;->mLogger:Ljava/util/logging/Logger;

    sget-object v4, Ljava/util/logging/Level;->ALL:Ljava/util/logging/Level;

    invoke-virtual {v3, v4}, Ljava/util/logging/Logger;->setLevel(Ljava/util/logging/Level;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 63
    .end local v1    # "handler":Ljava/util/logging/Handler;
    :goto_0
    return-void

    .line 60
    :catch_0
    move-exception v0

    .line 61
    .local v0, "e":Ljava/io/IOException;
    const-string v3, "LogFile"

    const-string v4, "Exception: "

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private dumpLogFile(Ljava/io/File;Ljava/io/PrintWriter;)V
    .locals 7
    .param p1, "file"    # Ljava/io/File;
    .param p2, "writer"    # Ljava/io/PrintWriter;

    .prologue
    .line 184
    const/4 v2, 0x0

    .line 185
    .local v2, "reader":Ljava/io/InputStreamReader;
    invoke-virtual {p2}, Ljava/io/PrintWriter;->println()V

    .line 186
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ":"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 187
    invoke-virtual {p2}, Ljava/io/PrintWriter;->println()V

    .line 189
    :try_start_0
    new-instance v3, Ljava/io/FileReader;

    invoke-direct {v3, p1}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 190
    .end local v2    # "reader":Ljava/io/InputStreamReader;
    .local v3, "reader":Ljava/io/InputStreamReader;
    const/16 v5, 0x2000

    :try_start_1
    new-array v0, v5, [C

    .line 192
    .local v0, "buf":[C
    :goto_0
    invoke-virtual {v3, v0}, Ljava/io/InputStreamReader;->read([C)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v4

    .line 193
    .local v4, "ret":I
    const/4 v5, -0x1

    if-ne v4, v5, :cond_0

    .line 201
    invoke-static {v3}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    move-object v2, v3

    .line 203
    .end local v0    # "buf":[C
    .end local v3    # "reader":Ljava/io/InputStreamReader;
    .end local v4    # "ret":I
    .restart local v2    # "reader":Ljava/io/InputStreamReader;
    :goto_1
    return-void

    .line 196
    .end local v2    # "reader":Ljava/io/InputStreamReader;
    .restart local v0    # "buf":[C
    .restart local v3    # "reader":Ljava/io/InputStreamReader;
    .restart local v4    # "ret":I
    :cond_0
    const/4 v5, 0x0

    :try_start_2
    invoke-virtual {p2, v0, v5, v4}, Ljava/io/PrintWriter;->write([CII)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    .line 198
    .end local v0    # "buf":[C
    .end local v4    # "ret":I
    :catch_0
    move-exception v1

    move-object v2, v3

    .line 199
    .end local v3    # "reader":Ljava/io/InputStreamReader;
    .local v1, "e":Ljava/io/IOException;
    .restart local v2    # "reader":Ljava/io/InputStreamReader;
    :goto_2
    :try_start_3
    const-string v5, "LogFile"

    const-string v6, "Exception: "

    invoke-static {v5, v6, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 201
    invoke-static {v2}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    goto :goto_1

    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v5

    :goto_3
    invoke-static {v2}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    throw v5

    .end local v2    # "reader":Ljava/io/InputStreamReader;
    .restart local v3    # "reader":Ljava/io/InputStreamReader;
    :catchall_1
    move-exception v5

    move-object v2, v3

    .end local v3    # "reader":Ljava/io/InputStreamReader;
    .restart local v2    # "reader":Ljava/io/InputStreamReader;
    goto :goto_3

    .line 198
    :catch_1
    move-exception v1

    goto :goto_2
.end method

.method private declared-synchronized log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "level"    # Ljava/util/logging/Level;
    .param p2, "tag"    # Ljava/lang/String;
    .param p3, "msg"    # Ljava/lang/String;

    .prologue
    .line 135
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/log/LogFile;->mLogger:Ljava/util/logging/Logger;

    if-nez v0, :cond_0

    .line 136
    const-string v0, "LogFile"

    const-string v1, "File logger not configured"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 144
    :goto_0
    monitor-exit p0

    return-void

    .line 139
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/music/log/LogFile;->mLogRecord:Ljava/util/logging/LogRecord;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/logging/LogRecord;->setMillis(J)V

    .line 140
    iget-object v0, p0, Lcom/google/android/music/log/LogFile;->mLogRecord:Ljava/util/logging/LogRecord;

    invoke-virtual {v0, p1}, Ljava/util/logging/LogRecord;->setLevel(Ljava/util/logging/Level;)V

    .line 141
    iget-object v0, p0, Lcom/google/android/music/log/LogFile;->mLogRecord:Ljava/util/logging/LogRecord;

    const-string v1, "%s: %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    const/4 v3, 0x1

    aput-object p3, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/logging/LogRecord;->setMessage(Ljava/lang/String;)V

    .line 142
    iget-object v0, p0, Lcom/google/android/music/log/LogFile;->mLogRecord:Ljava/util/logging/LogRecord;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/logging/LogRecord;->setThrown(Ljava/lang/Throwable;)V

    .line 143
    iget-object v0, p0, Lcom/google/android/music/log/LogFile;->mLogger:Ljava/util/logging/Logger;

    iget-object v1, p0, Lcom/google/android/music/log/LogFile;->mLogRecord:Ljava/util/logging/LogRecord;

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/LogRecord;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 135
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 4
    .param p1, "level"    # Ljava/util/logging/Level;
    .param p2, "tag"    # Ljava/lang/String;
    .param p3, "msg"    # Ljava/lang/String;
    .param p4, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 147
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/log/LogFile;->mLogger:Ljava/util/logging/Logger;

    if-nez v0, :cond_0

    .line 148
    const-string v0, "LogFile"

    const-string v1, "File logger not configured"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 156
    :goto_0
    monitor-exit p0

    return-void

    .line 151
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/music/log/LogFile;->mLogRecord:Ljava/util/logging/LogRecord;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/logging/LogRecord;->setMillis(J)V

    .line 152
    iget-object v0, p0, Lcom/google/android/music/log/LogFile;->mLogRecord:Ljava/util/logging/LogRecord;

    invoke-virtual {v0, p1}, Ljava/util/logging/LogRecord;->setLevel(Ljava/util/logging/Level;)V

    .line 153
    iget-object v0, p0, Lcom/google/android/music/log/LogFile;->mLogRecord:Ljava/util/logging/LogRecord;

    const-string v1, "%s: %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    const/4 v3, 0x1

    aput-object p3, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/logging/LogRecord;->setMessage(Ljava/lang/String;)V

    .line 154
    iget-object v0, p0, Lcom/google/android/music/log/LogFile;->mLogRecord:Ljava/util/logging/LogRecord;

    invoke-virtual {v0, p4}, Ljava/util/logging/LogRecord;->setThrown(Ljava/lang/Throwable;)V

    .line 155
    iget-object v0, p0, Lcom/google/android/music/log/LogFile;->mLogger:Ljava/util/logging/Logger;

    iget-object v1, p0, Lcom/google/android/music/log/LogFile;->mLogRecord:Ljava/util/logging/LogRecord;

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/LogRecord;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 147
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public d(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "msg"    # Ljava/lang/String;

    .prologue
    .line 75
    sget-object v0, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    invoke-direct {p0, v0, p1, p2}, Lcom/google/android/music/log/LogFile;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    return-void
.end method

.method public d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "msg"    # Ljava/lang/String;
    .param p3, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 79
    sget-object v0, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    invoke-direct {p0, v0, p1, p2, p3}, Lcom/google/android/music/log/LogFile;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 80
    return-void
.end method

.method public declared-synchronized dump(Ljava/io/PrintWriter;)V
    .locals 5
    .param p1, "writer"    # Ljava/io/PrintWriter;

    .prologue
    .line 171
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/music/log/LogFile;->mLogDir:Ljava/io/File;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/music/log/LogFile;->mLogFileName:Ljava/lang/String;

    if-nez v2, :cond_2

    .line 172
    :cond_0
    const-string v2, "LogFile"

    const-string v3, "The file or directory of the log file is unknown"

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 181
    :cond_1
    monitor-exit p0

    return-void

    .line 175
    :cond_2
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/4 v2, 0x2

    if-ge v1, v2, :cond_1

    .line 176
    :try_start_1
    new-instance v0, Ljava/io/File;

    iget-object v2, p0, Lcom/google/android/music/log/LogFile;->mLogDir:Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/google/android/music/log/LogFile;->mLogFileName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 177
    .local v0, "f":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 178
    invoke-direct {p0, v0, p1}, Lcom/google/android/music/log/LogFile;->dumpLogFile(Ljava/io/File;Ljava/io/PrintWriter;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 175
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 171
    .end local v0    # "f":Ljava/io/File;
    .end local v1    # "i":I
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public e(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "msg"    # Ljava/lang/String;

    .prologue
    .line 103
    sget-object v0, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-direct {p0, v0, p1, p2}, Lcom/google/android/music/log/LogFile;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    return-void
.end method

.method public e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "msg"    # Ljava/lang/String;
    .param p3, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 107
    sget-object v0, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-direct {p0, v0, p1, p2, p3}, Lcom/google/android/music/log/LogFile;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 108
    return-void
.end method

.method public f(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "msg"    # Ljava/lang/String;

    .prologue
    .line 123
    sget-object v0, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    invoke-direct {p0, v0, p1, p2}, Lcom/google/android/music/log/LogFile;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    return-void
.end method

.method public f(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "msg"    # Ljava/lang/String;
    .param p3, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 131
    sget-object v0, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    invoke-direct {p0, v0, p1, p2, p3}, Lcom/google/android/music/log/LogFile;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 132
    return-void
.end method

.method public i(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "msg"    # Ljava/lang/String;

    .prologue
    .line 83
    sget-object v0, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    invoke-direct {p0, v0, p1, p2}, Lcom/google/android/music/log/LogFile;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    return-void
.end method

.method public i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "msg"    # Ljava/lang/String;
    .param p3, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 87
    sget-object v0, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    invoke-direct {p0, v0, p1, p2, p3}, Lcom/google/android/music/log/LogFile;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 88
    return-void
.end method

.method public v(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "msg"    # Ljava/lang/String;

    .prologue
    .line 66
    sget-object v0, Ljava/util/logging/Level;->FINEST:Ljava/util/logging/Level;

    invoke-direct {p0, v0, p1, p2}, Lcom/google/android/music/log/LogFile;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    return-void
.end method

.method public v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "msg"    # Ljava/lang/String;
    .param p3, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 70
    invoke-static {p1, p2, p3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 71
    sget-object v0, Ljava/util/logging/Level;->FINEST:Ljava/util/logging/Level;

    invoke-direct {p0, v0, p1, p2, p3}, Lcom/google/android/music/log/LogFile;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 72
    return-void
.end method

.method public w(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "msg"    # Ljava/lang/String;

    .prologue
    .line 91
    sget-object v0, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    invoke-direct {p0, v0, p1, p2}, Lcom/google/android/music/log/LogFile;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    return-void
.end method

.method public w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "msg"    # Ljava/lang/String;
    .param p3, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 95
    sget-object v0, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    invoke-direct {p0, v0, p1, p2, p3}, Lcom/google/android/music/log/LogFile;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 96
    return-void
.end method

.method public wtf(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "msg"    # Ljava/lang/String;

    .prologue
    .line 111
    sget-object v0, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-direct {p0, v0, p1, p2}, Lcom/google/android/music/log/LogFile;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    return-void
.end method

.method public wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "msg"    # Ljava/lang/String;
    .param p3, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 119
    sget-object v0, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-direct {p0, v0, p1, p2, p3}, Lcom/google/android/music/log/LogFile;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 120
    return-void
.end method
