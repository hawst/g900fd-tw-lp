.class Lcom/google/android/music/ui/CreateArtistShuffleActivity$6;
.super Ljava/lang/Object;
.source "CreateArtistShuffleActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/CreateArtistShuffleActivity;->onFailure()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/CreateArtistShuffleActivity;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/CreateArtistShuffleActivity;)V
    .locals 0

    .prologue
    .line 225
    iput-object p1, p0, Lcom/google/android/music/ui/CreateArtistShuffleActivity$6;->this$0:Lcom/google/android/music/ui/CreateArtistShuffleActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 228
    iget-object v1, p0, Lcom/google/android/music/ui/CreateArtistShuffleActivity$6;->this$0:Lcom/google/android/music/ui/CreateArtistShuffleActivity;

    invoke-virtual {v1}, Lcom/google/android/music/ui/CreateArtistShuffleActivity;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/music/ui/CreateArtistShuffleActivity$6;->this$0:Lcom/google/android/music/ui/CreateArtistShuffleActivity;

    # getter for: Lcom/google/android/music/ui/CreateArtistShuffleActivity;->mDestroyed:Z
    invoke-static {v1}, Lcom/google/android/music/ui/CreateArtistShuffleActivity;->access$400(Lcom/google/android/music/ui/CreateArtistShuffleActivity;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 229
    iget-object v1, p0, Lcom/google/android/music/ui/CreateArtistShuffleActivity$6;->this$0:Lcom/google/android/music/ui/CreateArtistShuffleActivity;

    const v2, 0x7f0b01d8

    invoke-virtual {v1, v2}, Lcom/google/android/music/ui/CreateArtistShuffleActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 230
    .local v0, "errorMessage":Ljava/lang/CharSequence;
    iget-object v1, p0, Lcom/google/android/music/ui/CreateArtistShuffleActivity$6;->this$0:Lcom/google/android/music/ui/CreateArtistShuffleActivity;

    # getter for: Lcom/google/android/music/ui/CreateArtistShuffleActivity;->mText:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/google/android/music/ui/CreateArtistShuffleActivity;->access$500(Lcom/google/android/music/ui/CreateArtistShuffleActivity;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 231
    iget-object v1, p0, Lcom/google/android/music/ui/CreateArtistShuffleActivity$6;->this$0:Lcom/google/android/music/ui/CreateArtistShuffleActivity;

    # getter for: Lcom/google/android/music/ui/CreateArtistShuffleActivity;->mOkButton:Landroid/widget/Button;
    invoke-static {v1}, Lcom/google/android/music/ui/CreateArtistShuffleActivity;->access$600(Lcom/google/android/music/ui/CreateArtistShuffleActivity;)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 232
    iget-object v1, p0, Lcom/google/android/music/ui/CreateArtistShuffleActivity$6;->this$0:Lcom/google/android/music/ui/CreateArtistShuffleActivity;

    # getter for: Lcom/google/android/music/ui/CreateArtistShuffleActivity;->mDivider:Landroid/view/View;
    invoke-static {v1}, Lcom/google/android/music/ui/CreateArtistShuffleActivity;->access$700(Lcom/google/android/music/ui/CreateArtistShuffleActivity;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 234
    .end local v0    # "errorMessage":Ljava/lang/CharSequence;
    :cond_0
    return-void
.end method
