.class public Lcom/google/android/music/sync/google/MusicSyncAdapter;
.super Lcom/google/android/music/sync/common/AbstractSyncAdapter;
.source "MusicSyncAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/sync/google/MusicSyncAdapter$2;,
        Lcom/google/android/music/sync/google/MusicSyncAdapter$MusicSyncAdapterBuilder;,
        Lcom/google/android/music/sync/google/MusicSyncAdapter$AllFeedsDownstreamState;,
        Lcom/google/android/music/sync/google/MusicSyncAdapter$TickledDownstreamState;,
        Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState;
    }
.end annotation


# instance fields
.field private ALARM_NAUTILUS_EXPIRATION_ACTION:Ljava/lang/String;

.field private final LOGV:Z

.field private mInitialBlacklistItemVersion:J

.field private mInitialPlaylistEntryVersion:J

.field private mInitialPlaylistVersion:J

.field private mInitialRadioStationVersion:J

.field private mInitialTrackVersion:J

.field private mMusicApiClient:Lcom/google/android/music/sync/api/MusicApiClient;

.field private mSyncingNotification:Landroid/app/Notification;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 384
    invoke-direct {p0, p1}, Lcom/google/android/music/sync/common/AbstractSyncAdapter;-><init>(Landroid/content/Context;)V

    .line 169
    const-string v0, "com.google.android.music.sync.EXP_ALARM"

    iput-object v0, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->ALARM_NAUTILUS_EXPIRATION_ACTION:Ljava/lang/String;

    .line 385
    const-string v0, "MusicSyncAdapter"

    iput-object v0, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mTag:Ljava/lang/String;

    .line 386
    sget-object v0, Lcom/google/android/music/utils/DebugUtils$MusicTag;->SYNC:Lcom/google/android/music/utils/DebugUtils$MusicTag;

    invoke-static {v0}, Lcom/google/android/music/utils/DebugUtils;->isLoggable(Lcom/google/android/music/utils/DebugUtils$MusicTag;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->LOGV:Z

    .line 387
    return-void
.end method

.method static synthetic access$102(Lcom/google/android/music/sync/google/MusicSyncAdapter;Lcom/google/android/music/sync/api/MusicApiClient;)Lcom/google/android/music/sync/api/MusicApiClient;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/sync/google/MusicSyncAdapter;
    .param p1, "x1"    # Lcom/google/android/music/sync/api/MusicApiClient;

    .prologue
    .line 87
    iput-object p1, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mMusicApiClient:Lcom/google/android/music/sync/api/MusicApiClient;

    return-object p1
.end method

.method private createAlbumIntent(JLjava/lang/String;Ljava/lang/String;)Landroid/app/PendingIntent;
    .locals 7
    .param p1, "albumId"    # J
    .param p3, "albumName"    # Ljava/lang/String;
    .param p4, "artistName"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 1056
    new-instance v1, Lcom/google/android/music/medialist/AlbumSongList;

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v1 .. v6}, Lcom/google/android/music/medialist/AlbumSongList;-><init>(JLjava/lang/String;Ljava/lang/String;Z)V

    .line 1057
    .local v1, "songList":Lcom/google/android/music/medialist/AlbumSongList;
    iget-object v2, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mContext:Landroid/content/Context;

    invoke-static {v2, v1}, Lcom/google/android/music/ui/AppNavigation;->getShowSonglistIntent(Landroid/content/Context;Lcom/google/android/music/medialist/SongList;)Landroid/content/Intent;

    move-result-object v0

    .line 1058
    .local v0, "i":Landroid/content/Intent;
    iget-object v2, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mContext:Landroid/content/Context;

    invoke-static {v2, v6, v0, v6}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    return-object v2
.end method

.method private createFreeAndPurchasedIntent()Landroid/app/PendingIntent;
    .locals 7

    .prologue
    .line 1043
    new-instance v3, Ljava/lang/Object;

    invoke-direct {v3}, Ljava/lang/Object;-><init>()V

    .line 1044
    .local v3, "prefsHolder":Ljava/lang/Object;
    iget-object v4, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mContext:Landroid/content/Context;

    invoke-static {v4, v3}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v2

    .line 1046
    .local v2, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    const-wide/16 v4, -0x3

    const/4 v6, 0x0

    :try_start_0
    invoke-static {v4, v5, v6, v2}, Lcom/google/android/music/medialist/AutoPlaylistSongList;->getAutoPlaylist(JZLcom/google/android/music/preferences/MusicPreferences;)Lcom/google/android/music/medialist/AutoPlaylistSongList;

    move-result-object v1

    .line 1048
    .local v1, "playlist":Lcom/google/android/music/medialist/AutoPlaylistSongList;
    iget-object v4, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mContext:Landroid/content/Context;

    invoke-static {v4, v1}, Lcom/google/android/music/ui/AppNavigation;->getShowSonglistIntent(Landroid/content/Context;Lcom/google/android/music/medialist/SongList;)Landroid/content/Intent;

    move-result-object v0

    .line 1049
    .local v0, "i":Landroid/content/Intent;
    iget-object v4, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mContext:Landroid/content/Context;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-static {v4, v5, v0, v6}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v4

    .line 1051
    invoke-static {v3}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    return-object v4

    .end local v0    # "i":Landroid/content/Intent;
    .end local v1    # "playlist":Lcom/google/android/music/medialist/AutoPlaylistSongList;
    :catchall_0
    move-exception v4

    invoke-static {v3}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    throw v4
.end method

.method private getChangesFromServerAsDom(Landroid/accounts/Account;Lcom/google/android/music/sync/common/AbstractSyncAdapter$DownstreamFetchQueue;Ljava/util/HashMap;)Z
    .locals 21
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "fetchQueue"    # Lcom/google/android/music/sync/common/AbstractSyncAdapter$DownstreamFetchQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            "Lcom/google/android/music/sync/common/AbstractSyncAdapter$DownstreamFetchQueue;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/AuthenticatorException;,
            Lcom/google/android/music/sync/common/HardSyncException;,
            Lcom/google/android/music/sync/common/SoftSyncException;
        }
    .end annotation

    .prologue
    .line 813
    .local p3, "protocolState":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    const-class v2, Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState;

    const-string v3, "downstream_state"

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState;

    .line 815
    .local v11, "downstreamState":Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState;
    const-class v2, Ljava/lang/String;

    const-string v3, "continuation"

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 816
    .local v8, "continuationToken":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->LOGV:Z

    if-eqz v2, :cond_0

    .line 817
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mTag:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Fetcher: Getting changes from server for "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v11}, Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState;->getNextFeedToSync()Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, " with continuation token "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 821
    :cond_0
    const-string v2, "is_manual_sync"

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v13

    .line 822
    .local v13, "isManualSync":Z
    const/16 v18, 0x0

    .line 824
    .local v18, "result":Lcom/google/android/music/sync/api/MusicApiClient$GetResult;, "Lcom/google/android/music/sync/api/MusicApiClient$GetResult<+Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;>;"
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/sync/google/MusicSyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "music_downstream_page_size"

    const/16 v6, 0xfa

    invoke-static {v2, v3, v6}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v4

    .line 826
    .local v4, "pageSize":I
    invoke-virtual {v11}, Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState;->getNextFeedToSync()Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;

    move-result-object v15

    .line 827
    .local v15, "nextFeed":Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;
    sget-object v2, Lcom/google/android/music/sync/google/MusicSyncAdapter$2;->$SwitchMap$com$google$android$music$sync$google$MusicSyncAdapter$DownstreamState$Feed:[I

    invoke-virtual {v15}, Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 916
    new-instance v2, Ljava/io/IOException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unknown feed type for downstream sync: "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catch Lcom/google/android/music/sync/common/SyncHttpException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/google/android/music/sync/api/BadRequestException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/google/android/music/sync/api/ForbiddenException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/google/android/music/sync/api/ResourceNotFoundException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Lcom/google/android/music/sync/api/ServiceUnavailableException; {:try_start_0 .. :try_end_0} :catch_6
    .catch Lcom/google/android/music/sync/api/NotModifiedException; {:try_start_0 .. :try_end_0} :catch_8

    .line 919
    .end local v4    # "pageSize":I
    .end local v15    # "nextFeed":Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;
    :catch_0
    move-exception v12

    .line 920
    .local v12, "e":Lcom/google/android/music/sync/common/SyncHttpException;
    const-string v2, "MusicSyncAdapter"

    invoke-virtual {v12}, Lcom/google/android/music/sync/common/SyncHttpException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v12}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 921
    new-instance v2, Lcom/google/android/music/sync/common/HardSyncException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Http code "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v12}, Lcom/google/android/music/sync/common/SyncHttpException;->getStatusCode()I

    move-result v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, " on fetch"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v12}, Lcom/google/android/music/sync/common/HardSyncException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    .line 829
    .end local v12    # "e":Lcom/google/android/music/sync/common/SyncHttpException;
    .restart local v4    # "pageSize":I
    .restart local v15    # "nextFeed":Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;
    :pswitch_0
    :try_start_1
    const-class v2, Ljava/lang/String;

    const-string v3, "etag_track"

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 830
    .local v5, "etag":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mMusicApiClient:Lcom/google/android/music/sync/api/MusicApiClient;

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mInitialTrackVersion:J

    move-object/from16 v3, p1

    move-object/from16 v9, p3

    invoke-interface/range {v2 .. v9}, Lcom/google/android/music/sync/api/MusicApiClient;->getTracks(Landroid/accounts/Account;ILjava/lang/String;JLjava/lang/String;Ljava/util/HashMap;)Lcom/google/android/music/sync/api/MusicApiClient$GetResult;

    move-result-object v18

    .line 832
    move-object/from16 v0, v18

    iget-object v2, v0, Lcom/google/android/music/sync/api/MusicApiClient$GetResult;->mEtag:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 833
    const-string v2, "etag_track"

    move-object/from16 v0, v18

    iget-object v3, v0, Lcom/google/android/music/sync/api/MusicApiClient$GetResult;->mEtag:Ljava/lang/String;

    move-object/from16 v0, p3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Lcom/google/android/music/sync/common/SyncHttpException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lcom/google/android/music/sync/api/BadRequestException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Lcom/google/android/music/sync/api/ForbiddenException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Lcom/google/android/music/sync/api/ResourceNotFoundException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Lcom/google/android/music/sync/api/ServiceUnavailableException; {:try_start_1 .. :try_end_1} :catch_6
    .catch Lcom/google/android/music/sync/api/NotModifiedException; {:try_start_1 .. :try_end_1} :catch_8

    .line 939
    .end local v4    # "pageSize":I
    .end local v5    # "etag":Ljava/lang/String;
    .end local v15    # "nextFeed":Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;
    :cond_1
    :goto_0
    const-string v2, "continuation"

    move-object/from16 v0, v18

    iget-object v3, v0, Lcom/google/android/music/sync/api/MusicApiClient$GetResult;->mContinuationToken:Ljava/lang/String;

    move-object/from16 v0, p3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 940
    move-object/from16 v0, v18

    iget-object v2, v0, Lcom/google/android/music/sync/api/MusicApiClient$GetResult;->mItems:Ljava/util/Iterator;

    if-nez v2, :cond_5

    .line 941
    const-string v2, "MusicSyncAdapter"

    const/4 v3, 0x2

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 942
    const-string v2, "MusicSyncAdapter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "No mutations found for feed "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v11}, Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState;->getNextFeedToSync()Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 965
    :cond_2
    :goto_1
    move-object/from16 v0, v18

    iget-object v2, v0, Lcom/google/android/music/sync/api/MusicApiClient$GetResult;->mContinuationToken:Ljava/lang/String;

    if-nez v2, :cond_9

    .line 966
    invoke-virtual {v11}, Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState;->onDoneWithFeed()V

    .line 967
    const-string v2, "continuation"

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 968
    invoke-virtual {v11}, Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState;->getNextFeedToSync()Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;

    move-result-object v2

    if-eqz v2, :cond_8

    const/4 v2, 0x1

    .line 970
    :goto_2
    return v2

    .line 838
    .restart local v4    # "pageSize":I
    .restart local v15    # "nextFeed":Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;
    :pswitch_1
    :try_start_2
    const-class v2, Ljava/lang/String;

    const-string v3, "etag_playlist"

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 839
    .restart local v5    # "etag":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mMusicApiClient:Lcom/google/android/music/sync/api/MusicApiClient;

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mInitialPlaylistVersion:J

    move-object/from16 v3, p1

    move-object/from16 v9, p3

    invoke-interface/range {v2 .. v9}, Lcom/google/android/music/sync/api/MusicApiClient;->getPlaylists(Landroid/accounts/Account;ILjava/lang/String;JLjava/lang/String;Ljava/util/HashMap;)Lcom/google/android/music/sync/api/MusicApiClient$GetResult;

    move-result-object v18

    .line 841
    move-object/from16 v0, v18

    iget-object v2, v0, Lcom/google/android/music/sync/api/MusicApiClient$GetResult;->mEtag:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 842
    const-string v2, "etag_playlist"

    move-object/from16 v0, v18

    iget-object v3, v0, Lcom/google/android/music/sync/api/MusicApiClient$GetResult;->mEtag:Ljava/lang/String;

    move-object/from16 v0, p3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catch Lcom/google/android/music/sync/common/SyncHttpException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lcom/google/android/music/sync/api/BadRequestException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Lcom/google/android/music/sync/api/ForbiddenException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Lcom/google/android/music/sync/api/ResourceNotFoundException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Lcom/google/android/music/sync/api/ServiceUnavailableException; {:try_start_2 .. :try_end_2} :catch_6
    .catch Lcom/google/android/music/sync/api/NotModifiedException; {:try_start_2 .. :try_end_2} :catch_8

    goto :goto_0

    .line 922
    .end local v4    # "pageSize":I
    .end local v5    # "etag":Ljava/lang/String;
    .end local v15    # "nextFeed":Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;
    :catch_1
    move-exception v12

    .line 923
    .local v12, "e":Ljava/io/IOException;
    new-instance v2, Lcom/google/android/music/sync/common/SoftSyncException;

    invoke-direct {v2, v12}, Lcom/google/android/music/sync/common/SoftSyncException;-><init>(Ljava/lang/Throwable;)V

    throw v2

    .line 849
    .end local v12    # "e":Ljava/io/IOException;
    .restart local v4    # "pageSize":I
    .restart local v15    # "nextFeed":Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;
    :pswitch_2
    :try_start_3
    new-instance v17, Ljava/lang/Object;

    invoke-direct/range {v17 .. v17}, Ljava/lang/Object;-><init>()V

    .line 850
    .local v17, "prefsHolder":Ljava/lang/Object;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v0, v17

    invoke-static {v2, v0}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;
    :try_end_3
    .catch Lcom/google/android/music/sync/common/SyncHttpException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Lcom/google/android/music/sync/api/BadRequestException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Lcom/google/android/music/sync/api/ForbiddenException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Lcom/google/android/music/sync/api/ResourceNotFoundException; {:try_start_3 .. :try_end_3} :catch_4
    .catch Lcom/google/android/music/sync/api/ServiceUnavailableException; {:try_start_3 .. :try_end_3} :catch_6
    .catch Lcom/google/android/music/sync/api/NotModifiedException; {:try_start_3 .. :try_end_3} :catch_8

    move-result-object v16

    .line 853
    .local v16, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    :try_start_4
    invoke-virtual/range {v16 .. v16}, Lcom/google/android/music/preferences/MusicPreferences;->isNautilusEnabled()Z

    move-result v14

    .line 854
    .local v14, "isNautilusEnabled":Z
    if-eqz v14, :cond_3

    .line 855
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v2, v0, v3}, Lcom/google/android/music/store/SoundSearchPlaylistHelper;->updateSoundSearchPlaylist(Landroid/content/Context;Landroid/accounts/Account;Lcom/google/android/music/store/Store;)J
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 859
    :cond_3
    :try_start_5
    invoke-static/range {v17 .. v17}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    .line 862
    new-instance v19, Lcom/google/android/music/sync/api/MusicApiClient$GetResult;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, v19

    invoke-direct {v0, v2, v3, v6}, Lcom/google/android/music/sync/api/MusicApiClient$GetResult;-><init>(Ljava/util/Iterator;Ljava/lang/String;Ljava/lang/String;)V

    .end local v18    # "result":Lcom/google/android/music/sync/api/MusicApiClient$GetResult;, "Lcom/google/android/music/sync/api/MusicApiClient$GetResult<+Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;>;"
    .local v19, "result":Lcom/google/android/music/sync/api/MusicApiClient$GetResult;, "Lcom/google/android/music/sync/api/MusicApiClient$GetResult<+Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;>;"
    move-object/from16 v18, v19

    .line 864
    .end local v19    # "result":Lcom/google/android/music/sync/api/MusicApiClient$GetResult;, "Lcom/google/android/music/sync/api/MusicApiClient$GetResult<+Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;>;"
    .restart local v18    # "result":Lcom/google/android/music/sync/api/MusicApiClient$GetResult;, "Lcom/google/android/music/sync/api/MusicApiClient$GetResult<+Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;>;"
    goto/16 :goto_0

    .line 859
    .end local v14    # "isNautilusEnabled":Z
    :catchall_0
    move-exception v2

    invoke-static/range {v17 .. v17}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    throw v2
    :try_end_5
    .catch Lcom/google/android/music/sync/common/SyncHttpException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Lcom/google/android/music/sync/api/BadRequestException; {:try_start_5 .. :try_end_5} :catch_2
    .catch Lcom/google/android/music/sync/api/ForbiddenException; {:try_start_5 .. :try_end_5} :catch_3
    .catch Lcom/google/android/music/sync/api/ResourceNotFoundException; {:try_start_5 .. :try_end_5} :catch_4
    .catch Lcom/google/android/music/sync/api/ServiceUnavailableException; {:try_start_5 .. :try_end_5} :catch_6
    .catch Lcom/google/android/music/sync/api/NotModifiedException; {:try_start_5 .. :try_end_5} :catch_8

    .line 924
    .end local v4    # "pageSize":I
    .end local v15    # "nextFeed":Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;
    .end local v16    # "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    .end local v17    # "prefsHolder":Ljava/lang/Object;
    :catch_2
    move-exception v12

    .line 925
    .local v12, "e":Lcom/google/android/music/sync/api/BadRequestException;
    new-instance v2, Lcom/google/android/music/sync/common/HardSyncException;

    invoke-direct {v2, v12}, Lcom/google/android/music/sync/common/HardSyncException;-><init>(Ljava/lang/Throwable;)V

    throw v2

    .line 866
    .end local v12    # "e":Lcom/google/android/music/sync/api/BadRequestException;
    .restart local v4    # "pageSize":I
    .restart local v15    # "nextFeed":Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;
    :pswitch_3
    :try_start_6
    const-class v2, Ljava/lang/String;

    const-string v3, "etag_playlist_entry"

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 867
    .restart local v5    # "etag":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mMusicApiClient:Lcom/google/android/music/sync/api/MusicApiClient;

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mInitialPlaylistEntryVersion:J

    move-object/from16 v3, p1

    move-object/from16 v9, p3

    invoke-interface/range {v2 .. v9}, Lcom/google/android/music/sync/api/MusicApiClient;->getPlaylistEntries(Landroid/accounts/Account;ILjava/lang/String;JLjava/lang/String;Ljava/util/HashMap;)Lcom/google/android/music/sync/api/MusicApiClient$GetResult;

    move-result-object v18

    .line 869
    move-object/from16 v0, v18

    iget-object v2, v0, Lcom/google/android/music/sync/api/MusicApiClient$GetResult;->mEtag:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 870
    const-string v2, "etag_playlist_entry"

    move-object/from16 v0, v18

    iget-object v3, v0, Lcom/google/android/music/sync/api/MusicApiClient$GetResult;->mEtag:Ljava/lang/String;

    move-object/from16 v0, p3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_6
    .catch Lcom/google/android/music/sync/common/SyncHttpException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Lcom/google/android/music/sync/api/BadRequestException; {:try_start_6 .. :try_end_6} :catch_2
    .catch Lcom/google/android/music/sync/api/ForbiddenException; {:try_start_6 .. :try_end_6} :catch_3
    .catch Lcom/google/android/music/sync/api/ResourceNotFoundException; {:try_start_6 .. :try_end_6} :catch_4
    .catch Lcom/google/android/music/sync/api/ServiceUnavailableException; {:try_start_6 .. :try_end_6} :catch_6
    .catch Lcom/google/android/music/sync/api/NotModifiedException; {:try_start_6 .. :try_end_6} :catch_8

    goto/16 :goto_0

    .line 926
    .end local v4    # "pageSize":I
    .end local v5    # "etag":Ljava/lang/String;
    .end local v15    # "nextFeed":Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;
    :catch_3
    move-exception v12

    .line 927
    .local v12, "e":Lcom/google/android/music/sync/api/ForbiddenException;
    new-instance v2, Lcom/google/android/music/sync/common/HardSyncException;

    invoke-direct {v2, v12}, Lcom/google/android/music/sync/common/HardSyncException;-><init>(Ljava/lang/Throwable;)V

    throw v2

    .line 875
    .end local v12    # "e":Lcom/google/android/music/sync/api/ForbiddenException;
    .restart local v4    # "pageSize":I
    .restart local v15    # "nextFeed":Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;
    :pswitch_4
    const/4 v5, 0x0

    .line 876
    .restart local v5    # "etag":Ljava/lang/String;
    :try_start_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mMusicApiClient:Lcom/google/android/music/sync/api/MusicApiClient;

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mInitialRadioStationVersion:J

    move-object/from16 v3, p1

    move-object/from16 v9, p3

    invoke-interface/range {v2 .. v9}, Lcom/google/android/music/sync/api/MusicApiClient;->getRadioStations(Landroid/accounts/Account;ILjava/lang/String;JLjava/lang/String;Ljava/util/HashMap;)Lcom/google/android/music/sync/api/MusicApiClient$GetResult;

    move-result-object v18

    .line 878
    goto/16 :goto_0

    .line 882
    .end local v5    # "etag":Ljava/lang/String;
    :pswitch_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mMusicApiClient:Lcom/google/android/music/sync/api/MusicApiClient;

    move-object/from16 v0, p1

    move-object/from16 v1, p3

    invoke-interface {v2, v0, v13, v1}, Lcom/google/android/music/sync/api/MusicApiClient;->getConfig(Landroid/accounts/Account;ZLjava/util/HashMap;)Lcom/google/android/music/sync/api/MusicApiClient$GetResult;

    move-result-object v18

    .line 883
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/google/android/music/sync/google/MusicSyncAdapter;->processConfigResult(Lcom/google/android/music/sync/api/MusicApiClient$GetResult;)V
    :try_end_7
    .catch Lcom/google/android/music/sync/common/SyncHttpException; {:try_start_7 .. :try_end_7} :catch_0
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1
    .catch Lcom/google/android/music/sync/api/BadRequestException; {:try_start_7 .. :try_end_7} :catch_2
    .catch Lcom/google/android/music/sync/api/ForbiddenException; {:try_start_7 .. :try_end_7} :catch_3
    .catch Lcom/google/android/music/sync/api/ResourceNotFoundException; {:try_start_7 .. :try_end_7} :catch_4
    .catch Lcom/google/android/music/sync/api/ServiceUnavailableException; {:try_start_7 .. :try_end_7} :catch_6
    .catch Lcom/google/android/music/sync/api/NotModifiedException; {:try_start_7 .. :try_end_7} :catch_8

    goto/16 :goto_0

    .line 928
    .end local v4    # "pageSize":I
    .end local v15    # "nextFeed":Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;
    :catch_4
    move-exception v12

    .line 929
    .local v12, "e":Lcom/google/android/music/sync/api/ResourceNotFoundException;
    new-instance v2, Lcom/google/android/music/sync/common/HardSyncException;

    invoke-direct {v2, v12}, Lcom/google/android/music/sync/common/HardSyncException;-><init>(Ljava/lang/Throwable;)V

    throw v2

    .line 887
    .end local v12    # "e":Lcom/google/android/music/sync/api/ResourceNotFoundException;
    .restart local v4    # "pageSize":I
    .restart local v15    # "nextFeed":Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;
    :pswitch_6
    const/4 v5, 0x0

    .line 891
    .restart local v5    # "etag":Ljava/lang/String;
    :try_start_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mMusicApiClient:Lcom/google/android/music/sync/api/MusicApiClient;

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mInitialBlacklistItemVersion:J

    move-object/from16 v3, p1

    move-object/from16 v9, p3

    invoke-interface/range {v2 .. v9}, Lcom/google/android/music/sync/api/MusicApiClient;->getBlacklistItems(Landroid/accounts/Account;ILjava/lang/String;JLjava/lang/String;Ljava/util/HashMap;)Lcom/google/android/music/sync/api/MusicApiClient$GetResult;
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_5
    .catch Lcom/google/android/music/sync/common/SyncHttpException; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1
    .catch Lcom/google/android/music/sync/api/BadRequestException; {:try_start_8 .. :try_end_8} :catch_2
    .catch Lcom/google/android/music/sync/api/ForbiddenException; {:try_start_8 .. :try_end_8} :catch_3
    .catch Lcom/google/android/music/sync/api/ResourceNotFoundException; {:try_start_8 .. :try_end_8} :catch_4
    .catch Lcom/google/android/music/sync/api/ServiceUnavailableException; {:try_start_8 .. :try_end_8} :catch_6
    .catch Lcom/google/android/music/sync/api/NotModifiedException; {:try_start_8 .. :try_end_8} :catch_8

    move-result-object v18

    goto/16 :goto_0

    .line 893
    :catch_5
    move-exception v12

    .line 894
    .local v12, "e":Ljava/lang/Exception;
    :try_start_9
    const-string v2, "MusicSyncAdapter"

    const-string v3, "Exception when getting blacklist items"

    invoke-static {v2, v3, v12}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 895
    new-instance v2, Lcom/google/android/music/sync/api/NotModifiedException;

    invoke-direct {v2}, Lcom/google/android/music/sync/api/NotModifiedException;-><init>()V

    throw v2
    :try_end_9
    .catch Lcom/google/android/music/sync/common/SyncHttpException; {:try_start_9 .. :try_end_9} :catch_0
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_1
    .catch Lcom/google/android/music/sync/api/BadRequestException; {:try_start_9 .. :try_end_9} :catch_2
    .catch Lcom/google/android/music/sync/api/ForbiddenException; {:try_start_9 .. :try_end_9} :catch_3
    .catch Lcom/google/android/music/sync/api/ResourceNotFoundException; {:try_start_9 .. :try_end_9} :catch_4
    .catch Lcom/google/android/music/sync/api/ServiceUnavailableException; {:try_start_9 .. :try_end_9} :catch_6
    .catch Lcom/google/android/music/sync/api/NotModifiedException; {:try_start_9 .. :try_end_9} :catch_8

    .line 930
    .end local v4    # "pageSize":I
    .end local v5    # "etag":Ljava/lang/String;
    .end local v12    # "e":Ljava/lang/Exception;
    .end local v15    # "nextFeed":Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;
    :catch_6
    move-exception v12

    .line 931
    .local v12, "e":Lcom/google/android/music/sync/api/ServiceUnavailableException;
    new-instance v20, Lcom/google/android/music/sync/common/SoftSyncException;

    move-object/from16 v0, v20

    invoke-direct {v0, v12}, Lcom/google/android/music/sync/common/SoftSyncException;-><init>(Ljava/lang/Throwable;)V

    .line 932
    .local v20, "se":Lcom/google/android/music/sync/common/SoftSyncException;
    invoke-virtual {v12}, Lcom/google/android/music/sync/api/ServiceUnavailableException;->getRetryAfter()J

    move-result-wide v2

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v3}, Lcom/google/android/music/sync/common/SoftSyncException;->setRetryAfter(J)V

    .line 933
    throw v20

    .line 901
    .end local v12    # "e":Lcom/google/android/music/sync/api/ServiceUnavailableException;
    .end local v20    # "se":Lcom/google/android/music/sync/common/SoftSyncException;
    .restart local v4    # "pageSize":I
    .restart local v15    # "nextFeed":Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;
    :pswitch_7
    :try_start_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/music/utils/MusicUtils;->isCloudQueueModeEnabled(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 905
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/music/cloudclient/CloudQueueManager;->syncCloudQueue(Landroid/content/Context;)V

    .line 906
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/music/store/QueueUtils;->notifyChange(Landroid/content/Context;)V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_7
    .catch Lcom/google/android/music/sync/common/SyncHttpException; {:try_start_a .. :try_end_a} :catch_0
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_1
    .catch Lcom/google/android/music/sync/api/BadRequestException; {:try_start_a .. :try_end_a} :catch_2
    .catch Lcom/google/android/music/sync/api/ForbiddenException; {:try_start_a .. :try_end_a} :catch_3
    .catch Lcom/google/android/music/sync/api/ResourceNotFoundException; {:try_start_a .. :try_end_a} :catch_4
    .catch Lcom/google/android/music/sync/api/ServiceUnavailableException; {:try_start_a .. :try_end_a} :catch_6
    .catch Lcom/google/android/music/sync/api/NotModifiedException; {:try_start_a .. :try_end_a} :catch_8

    .line 911
    :cond_4
    :goto_3
    :try_start_b
    new-instance v19, Lcom/google/android/music/sync/api/MusicApiClient$GetResult;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, v19

    invoke-direct {v0, v2, v3, v6}, Lcom/google/android/music/sync/api/MusicApiClient$GetResult;-><init>(Ljava/util/Iterator;Ljava/lang/String;Ljava/lang/String;)V

    .end local v18    # "result":Lcom/google/android/music/sync/api/MusicApiClient$GetResult;, "Lcom/google/android/music/sync/api/MusicApiClient$GetResult<+Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;>;"
    .restart local v19    # "result":Lcom/google/android/music/sync/api/MusicApiClient$GetResult;, "Lcom/google/android/music/sync/api/MusicApiClient$GetResult<+Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;>;"
    move-object/from16 v18, v19

    .line 913
    .end local v19    # "result":Lcom/google/android/music/sync/api/MusicApiClient$GetResult;, "Lcom/google/android/music/sync/api/MusicApiClient$GetResult<+Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;>;"
    .restart local v18    # "result":Lcom/google/android/music/sync/api/MusicApiClient$GetResult;, "Lcom/google/android/music/sync/api/MusicApiClient$GetResult<+Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;>;"
    goto/16 :goto_0

    .line 908
    :catch_7
    move-exception v12

    .line 909
    .local v12, "e":Ljava/lang/Exception;
    const-string v2, "MusicSyncAdapter"

    const-string v3, "Exception when syncing cloud queue"

    invoke-static {v2, v3, v12}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_b
    .catch Lcom/google/android/music/sync/common/SyncHttpException; {:try_start_b .. :try_end_b} :catch_0
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_1
    .catch Lcom/google/android/music/sync/api/BadRequestException; {:try_start_b .. :try_end_b} :catch_2
    .catch Lcom/google/android/music/sync/api/ForbiddenException; {:try_start_b .. :try_end_b} :catch_3
    .catch Lcom/google/android/music/sync/api/ResourceNotFoundException; {:try_start_b .. :try_end_b} :catch_4
    .catch Lcom/google/android/music/sync/api/ServiceUnavailableException; {:try_start_b .. :try_end_b} :catch_6
    .catch Lcom/google/android/music/sync/api/NotModifiedException; {:try_start_b .. :try_end_b} :catch_8

    goto :goto_3

    .line 934
    .end local v4    # "pageSize":I
    .end local v12    # "e":Ljava/lang/Exception;
    .end local v15    # "nextFeed":Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;
    :catch_8
    move-exception v12

    .line 936
    .local v12, "e":Lcom/google/android/music/sync/api/NotModifiedException;
    new-instance v18, Lcom/google/android/music/sync/api/MusicApiClient$GetResult;

    .end local v18    # "result":Lcom/google/android/music/sync/api/MusicApiClient$GetResult;, "Lcom/google/android/music/sync/api/MusicApiClient$GetResult<+Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;>;"
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, v18

    invoke-direct {v0, v2, v3, v6}, Lcom/google/android/music/sync/api/MusicApiClient$GetResult;-><init>(Ljava/util/Iterator;Ljava/lang/String;Ljava/lang/String;)V

    .restart local v18    # "result":Lcom/google/android/music/sync/api/MusicApiClient$GetResult;, "Lcom/google/android/music/sync/api/MusicApiClient$GetResult<+Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;>;"
    goto/16 :goto_0

    .line 945
    .end local v12    # "e":Lcom/google/android/music/sync/api/NotModifiedException;
    :cond_5
    const/4 v10, 0x0

    .line 946
    .local v10, "count":I
    :cond_6
    :goto_4
    move-object/from16 v0, v18

    iget-object v2, v0, Lcom/google/android/music/sync/api/MusicApiClient$GetResult;->mItems:Ljava/util/Iterator;

    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 947
    add-int/lit8 v10, v10, 0x1

    .line 949
    :try_start_c
    move-object/from16 v0, v18

    iget-object v2, v0, Lcom/google/android/music/sync/api/MusicApiClient$GetResult;->mItems:Ljava/util/Iterator;

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Lcom/google/android/music/sync/common/AbstractSyncAdapter$DownstreamFetchQueue;->put(Ljava/lang/Object;)V
    :try_end_c
    .catch Lcom/google/android/music/sync/common/ClosableBlockingQueue$QueueClosedException; {:try_start_c .. :try_end_c} :catch_9
    .catch Ljava/lang/InterruptedException; {:try_start_c .. :try_end_c} :catch_a

    goto :goto_4

    .line 950
    :catch_9
    move-exception v12

    .line 951
    .local v12, "e":Lcom/google/android/music/sync/common/ClosableBlockingQueue$QueueClosedException;
    const-string v2, "MusicSyncAdapter"

    const/4 v3, 0x2

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 952
    const-string v2, "MusicSyncAdapter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, "The reader has killed the fetch queue, so there\'s "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, "no point in having the fetcher continue."

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 954
    const/4 v2, 0x0

    goto/16 :goto_2

    .line 956
    .end local v12    # "e":Lcom/google/android/music/sync/common/ClosableBlockingQueue$QueueClosedException;
    :catch_a
    move-exception v12

    .line 957
    .local v12, "e":Ljava/lang/InterruptedException;
    new-instance v2, Lcom/google/android/music/sync/common/SoftSyncException;

    invoke-direct {v2, v12}, Lcom/google/android/music/sync/common/SoftSyncException;-><init>(Ljava/lang/Throwable;)V

    throw v2

    .line 960
    .end local v12    # "e":Ljava/lang/InterruptedException;
    :cond_7
    const-string v2, "MusicSyncAdapter"

    const/4 v3, 0x2

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 961
    const-string v2, "MusicSyncAdapter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Fetcher: "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, " "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v11}, Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState;->getNextFeedToSync()Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, "  mutation(s) found."

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 968
    .end local v10    # "count":I
    :cond_8
    const/4 v2, 0x0

    goto/16 :goto_2

    .line 970
    :cond_9
    const/4 v2, 0x1

    goto/16 :goto_2

    .line 827
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_5
        :pswitch_2
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method private maybeUpdateSubscribedFeeds(Landroid/accounts/Account;)V
    .locals 20
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 527
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/sync/google/MusicSyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    .line 528
    .local v10, "cr":Landroid/content/ContentResolver;
    new-instance v18, Ljava/util/HashSet;

    invoke-direct/range {v18 .. v18}, Ljava/util/HashSet;-><init>()V

    .line 529
    .local v18, "newFeeds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const-string v2, "track-update"

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 530
    const-string v2, "playlist-update"

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 531
    const-string v2, "playlist-entry-update"

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 532
    const-string v2, "sound-search-update"

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 533
    const-string v2, "music_sync_radio"

    const/4 v3, 0x1

    invoke-static {v10, v2, v3}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 535
    const-string v2, "radio-station-update"

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 537
    :cond_0
    const-string v2, "music_sync_config"

    const/4 v3, 0x1

    invoke-static {v10, v2, v3}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 539
    const-string v2, "config-update"

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 541
    :cond_1
    const-string v2, "dismissed-item-list-update"

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 542
    const-string v2, "cloud-queue-update"

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 544
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v12

    .line 545
    .local v12, "existingFeeds":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Long;>;"
    const/4 v2, 0x2

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "_id"

    aput-object v3, v4, v2

    const/4 v2, 0x1

    const-string v3, "feed"

    aput-object v3, v4, v2

    .line 546
    .local v4, "projection":[Ljava/lang/String;
    const-string v19, "_sync_account=? AND _sync_account_type=? AND authority=?"

    .line 549
    .local v19, "where":Ljava/lang/String;
    const/4 v2, 0x3

    new-array v6, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    move-object/from16 v0, p1

    iget-object v3, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v3, v6, v2

    const/4 v2, 0x1

    move-object/from16 v0, p1

    iget-object v3, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    aput-object v3, v6, v2

    const/4 v2, 0x2

    const-string v3, "com.google.android.music.MusicContent"

    aput-object v3, v6, v2

    .line 551
    .local v6, "values":[Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/sync/google/MusicSyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v2

    sget-object v3, Lcom/google/android/gsf/SubscribedFeeds$Feeds;->CONTENT_URI:Landroid/net/Uri;

    const-string v5, "_sync_account=? AND _sync_account_type=? AND authority=?"

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 553
    .local v8, "c":Landroid/database/Cursor;
    if-nez v8, :cond_3

    .line 554
    const-string v2, "MusicSyncAdapter"

    const-string v3, "Can\'t find sync subscription feeds."

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 590
    :cond_2
    return-void

    .line 558
    :cond_3
    :goto_0
    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 559
    const/4 v2, 0x0

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v16

    .line 560
    .local v16, "id":J
    const/4 v2, 0x1

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 561
    .local v13, "feed":Ljava/lang/String;
    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v12, v13, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 564
    .end local v13    # "feed":Ljava/lang/String;
    .end local v16    # "id":J
    :catchall_0
    move-exception v2

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v2

    :cond_4
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 567
    invoke-interface/range {v18 .. v18}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v14

    .local v14, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/String;

    .line 569
    .local v15, "newFeed":Ljava/lang/String;
    invoke-virtual {v12, v15}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 571
    new-instance v9, Landroid/content/ContentValues;

    invoke-direct {v9}, Landroid/content/ContentValues;-><init>()V

    .line 572
    .local v9, "contentValues":Landroid/content/ContentValues;
    const-string v2, "_sync_account"

    move-object/from16 v0, p1

    iget-object v3, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v9, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 573
    const-string v2, "_sync_account_type"

    move-object/from16 v0, p1

    iget-object v3, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v9, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 574
    const-string v2, "feed"

    invoke-virtual {v9, v2, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 575
    const-string v2, "service"

    const-string v3, "sj"

    invoke-virtual {v9, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 576
    const-string v2, "authority"

    const-string v3, "com.google.android.music.MusicContent"

    invoke-virtual {v9, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 577
    sget-object v2, Lcom/google/android/gsf/SubscribedFeeds$Feeds;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v10, v2, v9}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    goto :goto_1

    .line 581
    .end local v9    # "contentValues":Landroid/content/ContentValues;
    :cond_5
    invoke-virtual {v12, v15}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 585
    .end local v15    # "newFeed":Ljava/lang/String;
    :cond_6
    invoke-virtual {v12}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :goto_2
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/util/Map$Entry;

    .line 586
    .local v11, "existing":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Long;>;"
    invoke-interface {v11}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    .line 587
    .restart local v16    # "id":J
    sget-object v2, Lcom/google/android/gsf/SubscribedFeeds$Feeds;->CONTENT_URI:Landroid/net/Uri;

    move-wide/from16 v0, v16

    invoke-static {v2, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v5, 0x0

    invoke-virtual {v10, v2, v3, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_2
.end method

.method public static newBuilder()Lcom/google/android/music/sync/google/MusicSyncAdapter$MusicSyncAdapterBuilder;
    .locals 1

    .prologue
    .line 390
    new-instance v0, Lcom/google/android/music/sync/google/MusicSyncAdapter$MusicSyncAdapterBuilder;

    invoke-direct {v0}, Lcom/google/android/music/sync/google/MusicSyncAdapter$MusicSyncAdapterBuilder;-><init>()V

    return-object v0
.end method

.method private processConfigResult(Lcom/google/android/music/preferences/MusicPreferences;Lcom/google/android/music/sync/api/MusicApiClient$GetResult;)V
    .locals 33
    .param p1, "preferences"    # Lcom/google/android/music/preferences/MusicPreferences;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/music/preferences/MusicPreferences;",
            "Lcom/google/android/music/sync/api/MusicApiClient$GetResult",
            "<+",
            "Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1139
    .local p2, "configResult":Lcom/google/android/music/sync/api/MusicApiClient$GetResult;, "Lcom/google/android/music/sync/api/MusicApiClient$GetResult<+Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;>;"
    invoke-static {}, Lcom/google/android/music/utils/ConfigUtils;->isNautilusEnabled()Z

    move-result v16

    .line 1140
    .local v16, "isNautilusUser":Z
    move/from16 v17, v16

    .line 1141
    .local v17, "isNautilusUserNewConfig":Z
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/preferences/MusicPreferences;->isValidAccount()Z

    move-result v19

    .line 1142
    .local v19, "isUserValid":Z
    move/from16 v20, v19

    .line 1143
    .local v20, "isUserValidNewConfig":Z
    invoke-static {}, Lcom/google/android/music/utils/ConfigUtils;->isNautilusPurchaseAvailable()Z

    move-result v12

    .line 1144
    .local v12, "isNautilusAvailable":Z
    invoke-static {}, Lcom/google/android/music/utils/ConfigUtils;->isNautilusFreeTrialAvailable()Z

    move-result v14

    .line 1145
    .local v14, "isNautilusFreeTrialAvailable":Z
    move v13, v12

    .line 1146
    .local v13, "isNautilusAvailableNewConfig":Z
    move v15, v14

    .line 1147
    .local v15, "isNautilusFreeTrialAvailableNewConfig":Z
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/preferences/MusicPreferences;->getClearNautilusContent()Z

    move-result v25

    .line 1148
    .local v25, "shouldCleanup":Z
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/preferences/MusicPreferences;->getResetSyncState()Z

    move-result v26

    .line 1149
    .local v26, "shouldResetSyncVersions":Z
    new-instance v5, Lcom/google/android/music/store/ConfigItem;

    invoke-direct {v5}, Lcom/google/android/music/store/ConfigItem;-><init>()V

    .line 1150
    .local v5, "config":Lcom/google/android/music/store/ConfigItem;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v28, v0

    invoke-static/range {v28 .. v28}, Lcom/google/android/music/store/ConfigStore;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/ConfigStore;

    move-result-object v8

    .line 1151
    .local v8, "configStore":Lcom/google/android/music/store/ConfigStore;
    invoke-virtual {v8}, Lcom/google/android/music/store/ConfigStore;->beginWriteTxn()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v6

    .line 1152
    .local v6, "configDB":Landroid/database/sqlite/SQLiteDatabase;
    const/16 v18, 0x0

    .line 1153
    .local v18, "isTxnSuccessful":Z
    const-wide/16 v10, 0x0

    .line 1154
    .local v10, "expTime":J
    invoke-static {v6}, Lcom/google/android/music/store/ConfigItem;->compileInsertStatement(Landroid/database/sqlite/SQLiteDatabase;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v9

    .line 1156
    .local v9, "insertStatement":Landroid/database/sqlite/SQLiteStatement;
    :try_start_0
    invoke-static {v6}, Lcom/google/android/music/store/ConfigItem;->deleteAllServerSettings(Landroid/database/sqlite/SQLiteDatabase;)I

    .line 1157
    :cond_0
    :goto_0
    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/google/android/music/sync/api/MusicApiClient$GetResult;->mItems:Ljava/util/Iterator;

    move-object/from16 v28, v0

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v28

    if-eqz v28, :cond_9

    .line 1158
    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/google/android/music/sync/api/MusicApiClient$GetResult;->mItems:Ljava/util/Iterator;

    move-object/from16 v28, v0

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/music/sync/google/model/ConfigEntry;

    .line 1159
    .local v7, "configEntry":Lcom/google/android/music/sync/google/model/ConfigEntry;
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->LOGV:Z

    move/from16 v28, v0

    if-eqz v28, :cond_1

    .line 1160
    const-string v28, "MusicSyncAdapter"

    const-string v29, "Config entry key=%s value=%s"

    const/16 v30, 0x2

    move/from16 v0, v30

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v30, v0

    const/16 v31, 0x0

    invoke-virtual {v7}, Lcom/google/android/music/sync/google/model/ConfigEntry;->getKey()Ljava/lang/String;

    move-result-object v32

    aput-object v32, v30, v31

    const/16 v31, 0x1

    invoke-virtual {v7}, Lcom/google/android/music/sync/google/model/ConfigEntry;->getValue()Ljava/lang/String;

    move-result-object v32

    aput-object v32, v30, v31

    invoke-static/range {v29 .. v30}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v28 .. v29}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 1163
    :cond_1
    invoke-virtual {v5}, Lcom/google/android/music/store/ConfigItem;->reset()V

    .line 1164
    invoke-virtual {v7}, Lcom/google/android/music/sync/google/model/ConfigEntry;->getKey()Ljava/lang/String;

    move-result-object v22

    .line 1165
    .local v22, "name":Ljava/lang/String;
    invoke-virtual {v7}, Lcom/google/android/music/sync/google/model/ConfigEntry;->getValue()Ljava/lang/String;

    move-result-object v27

    .line 1166
    .local v27, "value":Ljava/lang/String;
    invoke-static/range {v22 .. v22}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v28

    if-nez v28, :cond_8

    .line 1169
    move-object/from16 v0, v22

    invoke-virtual {v5, v0}, Lcom/google/android/music/store/ConfigItem;->setName(Ljava/lang/String;)V

    .line 1170
    move-object/from16 v0, v27

    invoke-virtual {v5, v0}, Lcom/google/android/music/store/ConfigItem;->setValue(Ljava/lang/String;)V

    .line 1171
    const/16 v28, 0x1

    move/from16 v0, v28

    invoke-virtual {v5, v0}, Lcom/google/android/music/store/ConfigItem;->setType(I)V

    .line 1172
    invoke-virtual {v5, v9}, Lcom/google/android/music/store/ConfigItem;->insert(Landroid/database/sqlite/SQLiteStatement;)J

    .line 1174
    const-string v28, "isNautilusUser"

    move-object/from16 v0, v22

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_2

    .line 1175
    const-string v28, "true"

    move-object/from16 v0, v28

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    goto :goto_0

    .line 1176
    :cond_2
    const-string v28, "nautilusExpirationTimeMs"

    move-object/from16 v0, v22

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_3

    .line 1177
    invoke-static/range {v27 .. v27}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    goto/16 :goto_0

    .line 1178
    :cond_3
    const-string v28, "nextConfigSyncDelayInSeconds"

    move-object/from16 v0, v22

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_5

    .line 1179
    invoke-static/range {v27 .. v27}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Long;->longValue()J

    move-result-wide v28

    move-object/from16 v0, p0

    move-wide/from16 v1, v28

    invoke-direct {v0, v1, v2}, Lcom/google/android/music/sync/google/MusicSyncAdapter;->scheduleAdditionalSync(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_0

    .line 1253
    .end local v7    # "configEntry":Lcom/google/android/music/sync/google/model/ConfigEntry;
    .end local v22    # "name":Ljava/lang/String;
    .end local v27    # "value":Ljava/lang/String;
    :catchall_0
    move-exception v28

    move/from16 v0, v18

    invoke-virtual {v8, v6, v0}, Lcom/google/android/music/store/ConfigStore;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    .line 1254
    invoke-static {v9}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/sqlite/SQLiteProgram;)V

    .line 1255
    if-eqz v18, :cond_4

    .line 1256
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v29, v0

    invoke-virtual/range {v29 .. v29}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v29

    sget-object v30, Lcom/google/android/music/store/ConfigContent;->SERVER_SETTINGS_URI:Landroid/net/Uri;

    const/16 v31, 0x0

    const/16 v32, 0x0

    invoke-virtual/range {v29 .. v32}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    :cond_4
    throw v28

    .line 1180
    .restart local v7    # "configEntry":Lcom/google/android/music/sync/google/model/ConfigEntry;
    .restart local v22    # "name":Ljava/lang/String;
    .restart local v27    # "value":Ljava/lang/String;
    :cond_5
    :try_start_1
    const-string v28, "isNautilusAvailable"

    move-object/from16 v0, v22

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_6

    .line 1181
    const-string v28, "true"

    move-object/from16 v0, v28

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    goto/16 :goto_0

    .line 1182
    :cond_6
    const-string v28, "isTrAvailable"

    move-object/from16 v0, v22

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_7

    .line 1183
    const-string v28, "true"

    move-object/from16 v0, v28

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    goto/16 :goto_0

    .line 1184
    :cond_7
    const-string v28, "isAnyServiceAvailable"

    move-object/from16 v0, v22

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_0

    .line 1185
    const-string v28, "true"

    move-object/from16 v0, v28

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    goto/16 :goto_0

    .line 1188
    :cond_8
    const-string v28, "MusicSyncAdapter"

    const-string v29, "Empty config value"

    invoke-static/range {v28 .. v29}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1194
    .end local v7    # "configEntry":Lcom/google/android/music/sync/google/model/ConfigEntry;
    .end local v22    # "name":Ljava/lang/String;
    .end local v27    # "value":Ljava/lang/String;
    :cond_9
    if-nez v19, :cond_16

    if-eqz v20, :cond_16

    .line 1195
    const/16 v28, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->setIsValidAccount(Z)V

    .line 1196
    const-string v28, "MusicSyncAdapter"

    const-string v29, "Service became available to invalid account."

    invoke-static/range {v28 .. v29}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1197
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/preferences/MusicPreferences;->getSyncAccount()Landroid/accounts/Account;

    move-result-object v28

    const-string v29, "com.google.android.music.MusicContent"

    new-instance v30, Landroid/os/Bundle;

    invoke-direct/range {v30 .. v30}, Landroid/os/Bundle;-><init>()V

    invoke-static/range {v28 .. v30}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1207
    :cond_a
    :goto_1
    if-eqz v16, :cond_17

    if-nez v17, :cond_17

    .line 1208
    const-string v28, "MusicSyncAdapter"

    const-string v29, "User lost Nautilus."

    invoke-static/range {v28 .. v29}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1209
    const/16 v25, 0x1

    .line 1210
    const/16 v28, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->setClearNautilusContent(Z)V

    .line 1220
    :cond_b
    :goto_2
    if-nez v17, :cond_f

    if-eqz v20, :cond_f

    .line 1224
    invoke-static {}, Lcom/google/android/music/utils/ConfigUtils;->hasIsNautilusAvailableBeenSet()Z

    move-result v28

    if-nez v28, :cond_c

    invoke-static {}, Lcom/google/android/music/utils/ConfigUtils;->hasIsNautilusFreeTrialAvailableBeenSet()Z

    move-result v28

    if-eqz v28, :cond_f

    .line 1228
    :cond_c
    if-nez v12, :cond_d

    if-eqz v14, :cond_18

    :cond_d
    const/16 v24, 0x1

    .line 1230
    .local v24, "prevNautilusAvailable":Z
    :goto_3
    if-nez v13, :cond_e

    if-eqz v15, :cond_19

    :cond_e
    const/16 v23, 0x1

    .line 1232
    .local v23, "newNautilusAvailable":Z
    :goto_4
    if-nez v24, :cond_f

    if-eqz v23, :cond_f

    .line 1233
    const/16 v28, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->setTutorialViewed(Z)V

    .line 1238
    .end local v23    # "newNautilusAvailable":Z
    .end local v24    # "prevNautilusAvailable":Z
    :cond_f
    if-nez v14, :cond_1a

    if-eqz v15, :cond_1a

    .line 1239
    const-string v28, "MusicSyncAdapter"

    const-string v29, "Nautilus trial became available."

    invoke-static/range {v28 .. v29}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1244
    :cond_10
    :goto_5
    if-nez v12, :cond_1b

    if-eqz v13, :cond_1b

    .line 1245
    const-string v28, "MusicSyncAdapter"

    const-string v29, "Nautilus purchase became available."

    invoke-static/range {v28 .. v29}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1250
    :cond_11
    :goto_6
    const/16 v18, 0x1

    .line 1253
    move/from16 v0, v18

    invoke-virtual {v8, v6, v0}, Lcom/google/android/music/store/ConfigStore;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    .line 1254
    invoke-static {v9}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/sqlite/SQLiteProgram;)V

    .line 1255
    if-eqz v18, :cond_12

    .line 1256
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v28

    sget-object v29, Lcom/google/android/music/store/ConfigContent;->SERVER_SETTINGS_URI:Landroid/net/Uri;

    const/16 v30, 0x0

    const/16 v31, 0x0

    invoke-virtual/range {v28 .. v31}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    .line 1261
    :cond_12
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v28, v0

    invoke-static/range {v28 .. v28}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v21

    .line 1262
    .local v21, "musicStore":Lcom/google/android/music/store/Store;
    if-eqz v25, :cond_13

    .line 1263
    invoke-virtual/range {v21 .. v21}, Lcom/google/android/music/store/Store;->deleteAllNautilusContent()Z

    move-result v28

    if-eqz v28, :cond_13

    .line 1264
    const/16 v25, 0x0

    .line 1265
    const/16 v28, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->setClearNautilusContent(Z)V

    .line 1269
    :cond_13
    if-eqz v26, :cond_14

    .line 1270
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/preferences/MusicPreferences;->getSyncAccount()Landroid/accounts/Account;

    move-result-object v4

    .line 1271
    .local v4, "account":Landroid/accounts/Account;
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/google/android/music/sync/google/MusicSyncAdapter;->resetSync(Landroid/accounts/Account;)Z

    move-result v28

    if-eqz v28, :cond_14

    .line 1272
    const/16 v26, 0x0

    .line 1273
    const/16 v28, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->setResetSyncState(Z)V

    .line 1274
    const/16 v28, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->setShowSyncNotification(Z)V

    .line 1278
    .end local v4    # "account":Landroid/accounts/Account;
    :cond_14
    if-eqz v17, :cond_15

    .line 1279
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/music/preferences/MusicPreferences;->updateNautilusTimestamp()V

    .line 1280
    move-object/from16 v0, p0

    invoke-direct {v0, v10, v11}, Lcom/google/android/music/sync/google/MusicSyncAdapter;->scheduleNautilusExpirationSync(J)V

    .line 1285
    :cond_15
    return-void

    .line 1199
    .end local v21    # "musicStore":Lcom/google/android/music/store/Store;
    :cond_16
    if-eqz v19, :cond_a

    if-nez v20, :cond_a

    .line 1204
    :try_start_2
    const-string v28, "MusicSyncAdapter"

    const-string v29, "Service became unavailable to valid account."

    invoke-static/range {v28 .. v29}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1211
    :cond_17
    if-nez v16, :cond_b

    if-eqz v17, :cond_b

    .line 1212
    const-string v28, "MusicSyncAdapter"

    const-string v29, "User gained Nautilus."

    invoke-static/range {v28 .. v29}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1215
    const/16 v26, 0x1

    .line 1216
    const/16 v28, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->setResetSyncState(Z)V

    .line 1217
    const/16 v28, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->setShowSyncNotification(Z)V

    goto/16 :goto_2

    .line 1228
    :cond_18
    const/16 v24, 0x0

    goto/16 :goto_3

    .line 1230
    .restart local v24    # "prevNautilusAvailable":Z
    :cond_19
    const/16 v23, 0x0

    goto/16 :goto_4

    .line 1240
    .end local v24    # "prevNautilusAvailable":Z
    :cond_1a
    if-eqz v14, :cond_10

    if-nez v15, :cond_10

    .line 1241
    const-string v28, "MusicSyncAdapter"

    const-string v29, "Nautilus trial became unavailable."

    invoke-static/range {v28 .. v29}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5

    .line 1246
    :cond_1b
    if-eqz v12, :cond_11

    if-nez v13, :cond_11

    .line 1247
    const-string v28, "MusicSyncAdapter"

    const-string v29, "Nautilus purchase became unavailable."

    invoke-static/range {v28 .. v29}, Lcom/google/android/music/log/Log;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_6
.end method

.method private processConfigResult(Lcom/google/android/music/sync/api/MusicApiClient$GetResult;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/music/sync/api/MusicApiClient$GetResult",
            "<+",
            "Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1094
    .local p1, "configResult":Lcom/google/android/music/sync/api/MusicApiClient$GetResult;, "Lcom/google/android/music/sync/api/MusicApiClient$GetResult<+Lcom/google/android/music/sync/google/model/MusicQueueableSyncEntity;>;"
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    .line 1095
    .local v1, "prefsHolder":Ljava/lang/Object;
    iget-object v2, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mContext:Landroid/content/Context;

    invoke-static {v2, v1}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    .line 1097
    .local v0, "preferences":Lcom/google/android/music/preferences/MusicPreferences;
    :try_start_0
    invoke-direct {p0, v0, p1}, Lcom/google/android/music/sync/google/MusicSyncAdapter;->processConfigResult(Lcom/google/android/music/preferences/MusicPreferences;Lcom/google/android/music/sync/api/MusicApiClient$GetResult;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1099
    invoke-static {v1}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    .line 1101
    return-void

    .line 1099
    :catchall_0
    move-exception v2

    invoke-static {v1}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    throw v2
.end method

.method private resetSync(Landroid/accounts/Account;)Z
    .locals 4
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 1288
    iget-object v2, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v0

    .line 1289
    .local v0, "musicStore":Lcom/google/android/music/store/Store;
    invoke-virtual {v0}, Lcom/google/android/music/store/Store;->resetSyncState()Z

    move-result v1

    .line 1290
    .local v1, "ret":Z
    const-string v2, "com.google.android.music.MusicContent"

    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    invoke-static {p1, v2, v3}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1291
    return v1
.end method

.method private scheduleAdditionalSync(J)V
    .locals 9
    .param p1, "nextConfigSyncDelayInSeconds"    # J

    .prologue
    const/4 v5, 0x0

    .line 1326
    const-wide/16 v2, 0x0

    cmp-long v2, p1, v2

    if-gtz v2, :cond_0

    .line 1343
    :goto_0
    return-void

    .line 1329
    :cond_0
    const-wide/32 v2, 0x2a300

    cmp-long v2, p1, v2

    if-lez v2, :cond_1

    .line 1332
    const-string v2, "MusicSyncAdapter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "nextConfigSyncDelayInSeconds from server config response too large: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/music/log/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1337
    :cond_1
    iget-object v2, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mContext:Landroid/content/Context;

    const-string v3, "alarm"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/AlarmManager;

    move-object v0, v2

    check-cast v0, Landroid/app/AlarmManager;

    .line 1339
    .local v0, "alarmManager":Landroid/app/AlarmManager;
    iget-object v2, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mContext:Landroid/content/Context;

    new-instance v3, Landroid/content/Intent;

    const-string v4, "com.google.android.music.sync.SERVER_REQUEST_CONFIG_ALARM"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {v2, v5, v3, v5}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 1341
    .local v1, "intent":Landroid/app/PendingIntent;
    const/4 v2, 0x1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    mul-long/2addr v6, p1

    add-long/2addr v4, v6

    invoke-virtual {v0, v2, v4, v5, v1}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    goto :goto_0
.end method

.method private scheduleNautilusExpirationSync(J)V
    .locals 11
    .param p1, "expirationTime"    # J

    .prologue
    const-wide/16 v8, 0x3c

    const/4 v10, 0x0

    .line 1296
    const-wide/16 v6, 0x0

    cmp-long v6, p1, v6

    if-lez v6, :cond_0

    .line 1298
    iget-object v6, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    sget-object v7, Lcom/google/android/music/MusicGservicesKeys;->MUSIC_EXPIRATION_SYNC_ALARM_MINUTES:Ljava/lang/String;

    invoke-static {v6, v7, v8, v9}, Lcom/google/android/gsf/Gservices;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v6

    mul-long/2addr v6, v8

    const-wide/16 v8, 0x3e8

    mul-long v2, v6, v8

    .line 1302
    .local v2, "delayMs":J
    add-long v4, p1, v2

    .line 1304
    .local v4, "time":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    cmp-long v6, v4, v6

    if-lez v6, :cond_0

    .line 1305
    iget-object v6, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mContext:Landroid/content/Context;

    const-string v7, "alarm"

    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/app/AlarmManager;

    move-object v0, v6

    check-cast v0, Landroid/app/AlarmManager;

    .line 1308
    .local v0, "alarmManager":Landroid/app/AlarmManager;
    iget-object v6, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mContext:Landroid/content/Context;

    new-instance v7, Landroid/content/Intent;

    iget-object v8, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->ALARM_NAUTILUS_EXPIRATION_ACTION:Ljava/lang/String;

    invoke-direct {v7, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {v6, v10, v7, v10}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 1312
    .local v1, "intent":Landroid/app/PendingIntent;
    const/4 v6, 0x1

    invoke-virtual {v0, v6, v4, v5, v1}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 1315
    .end local v0    # "alarmManager":Landroid/app/AlarmManager;
    .end local v1    # "intent":Landroid/app/PendingIntent;
    .end local v2    # "delayMs":J
    .end local v4    # "time":J
    :cond_0
    return-void
.end method

.method private sendNotificationIfNecessary(Landroid/accounts/Account;Ljava/util/Map;)V
    .locals 17
    .param p1, "account"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 996
    .local p2, "protocolState":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v12, "new_purchased_count"

    move-object/from16 v0, p2

    invoke-interface {v0, v12}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v12}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v8

    .line 999
    .local v8, "newTrackCount":I
    if-nez v8, :cond_0

    .line 1039
    :goto_0
    return-void

    .line 1002
    :cond_0
    const-string v12, "new_purchased_albumId"

    move-object/from16 v0, p2

    invoke-interface {v0, v12}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/Long;

    invoke-virtual {v12}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 1003
    .local v2, "albumId":J
    const-string v12, "new_purchased_album_name"

    move-object/from16 v0, p2

    invoke-interface {v0, v12}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 1004
    .local v4, "albumname":Ljava/lang/String;
    const-string v12, "new_purchased_artist_name"

    move-object/from16 v0, p2

    invoke-interface {v0, v12}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 1005
    .local v5, "artistName":Ljava/lang/String;
    const-string v12, "new_purchased_song_title"

    move-object/from16 v0, p2

    invoke-interface {v0, v12}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    .line 1010
    .local v11, "songName":Ljava/lang/String;
    const/4 v12, 0x1

    if-ne v8, v12, :cond_1

    .line 1011
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mContext:Landroid/content/Context;

    const v13, 0x7f0b01c6

    const/4 v14, 0x2

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    aput-object v11, v14, v15

    const/4 v15, 0x1

    aput-object v5, v14, v15

    invoke-virtual {v12, v13, v14}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    .line 1013
    .local v10, "notificationTitle":Ljava/lang/String;
    invoke-direct/range {p0 .. p0}, Lcom/google/android/music/sync/google/MusicSyncAdapter;->createFreeAndPurchasedIntent()Landroid/app/PendingIntent;

    move-result-object v6

    .line 1026
    .local v6, "contentIntent":Landroid/app/PendingIntent;
    :goto_1
    new-instance v7, Landroid/app/Notification;

    invoke-direct {v7}, Landroid/app/Notification;-><init>()V

    .line 1027
    .local v7, "mNotification":Landroid/app/Notification;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    iput-wide v12, v7, Landroid/app/Notification;->when:J

    .line 1028
    const/16 v12, 0x18

    iput v12, v7, Landroid/app/Notification;->flags:I

    .line 1030
    const v12, 0x7f02025d

    iput v12, v7, Landroid/app/Notification;->icon:I

    .line 1031
    const/4 v12, 0x0

    iput v12, v7, Landroid/app/Notification;->defaults:I

    .line 1033
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mContext:Landroid/content/Context;

    const v14, 0x7f0b01c9

    invoke-virtual {v13, v14}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v7, v12, v10, v13, v6}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 1036
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mContext:Landroid/content/Context;

    const-string v13, "notification"

    invoke-virtual {v12, v13}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/app/NotificationManager;

    .line 1038
    .local v9, "notificationManager":Landroid/app/NotificationManager;
    const/16 v12, 0x19

    invoke-virtual {v9, v12, v7}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto/16 :goto_0

    .line 1014
    .end local v6    # "contentIntent":Landroid/app/PendingIntent;
    .end local v7    # "mNotification":Landroid/app/Notification;
    .end local v9    # "notificationManager":Landroid/app/NotificationManager;
    .end local v10    # "notificationTitle":Ljava/lang/String;
    :cond_1
    const-string v12, "new_purchased_same_album"

    move-object/from16 v0, p2

    invoke-interface {v0, v12}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/Boolean;

    invoke-virtual {v12}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v12

    if-eqz v12, :cond_2

    .line 1015
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mContext:Landroid/content/Context;

    const v13, 0x7f0b01c7

    const/4 v14, 0x2

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    aput-object v4, v14, v15

    const/4 v15, 0x1

    aput-object v5, v14, v15

    invoke-virtual {v12, v13, v14}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    .line 1017
    .restart local v10    # "notificationTitle":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/google/android/music/sync/google/MusicSyncAdapter;->createAlbumIntent(JLjava/lang/String;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v6

    .restart local v6    # "contentIntent":Landroid/app/PendingIntent;
    goto :goto_1

    .line 1021
    .end local v6    # "contentIntent":Landroid/app/PendingIntent;
    .end local v10    # "notificationTitle":Ljava/lang/String;
    :cond_2
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mContext:Landroid/content/Context;

    const v13, 0x7f0b01c8

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-virtual {v12, v13, v14}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    .line 1023
    .restart local v10    # "notificationTitle":Ljava/lang/String;
    invoke-direct/range {p0 .. p0}, Lcom/google/android/music/sync/google/MusicSyncAdapter;->createFreeAndPurchasedIntent()Landroid/app/PendingIntent;

    move-result-object v6

    .restart local v6    # "contentIntent":Landroid/app/PendingIntent;
    goto :goto_1
.end method

.method private showSyncingNotification()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 508
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mSyncingNotification:Landroid/app/Notification;

    if-eqz v3, :cond_0

    .line 524
    :goto_0
    return-void

    .line 509
    :cond_0
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mContext:Landroid/content/Context;

    const-string v4, "notification"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/NotificationManager;

    .line 511
    .local v1, "nm":Landroid/app/NotificationManager;
    iget-object v3, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b011b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 512
    .local v2, "text":Ljava/lang/CharSequence;
    new-instance v3, Landroid/support/v4/app/NotificationCompat$Builder;

    iget-object v4, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Landroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    const v4, 0x7f02025c

    invoke-virtual {v3, v4}, Landroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/google/android/music/ui/AppNavigation;->getHomeScreenPendingIntent(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {v3, v4}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/support/v4/app/NotificationCompat$Builder;->setOngoing(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/support/v4/app/NotificationCompat$Builder;->setAutoCancel(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v3

    invoke-virtual {v3, v5, v5, v6}, Landroid/support/v4/app/NotificationCompat$Builder;->setProgress(IIZ)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/support/v4/app/NotificationCompat$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    .line 522
    .local v0, "builder":Landroid/support/v4/app/NotificationCompat$Builder;
    invoke-virtual {v0}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mSyncingNotification:Landroid/app/Notification;

    .line 523
    const/16 v3, 0x65

    iget-object v4, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mSyncingNotification:Landroid/app/Notification;

    invoke-virtual {v1, v3, v4}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto :goto_0
.end method


# virtual methods
.method protected createDownstreamMerger(Lcom/google/android/music/sync/common/AbstractSyncAdapter$DownstreamMergeQueue;Landroid/content/Context;Ljava/util/Map;)Lcom/google/android/music/sync/common/DownstreamMerger;
    .locals 6
    .param p1, "mergeQueue"    # Lcom/google/android/music/sync/common/AbstractSyncAdapter$DownstreamMergeQueue;
    .param p2, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/music/sync/common/AbstractSyncAdapter$DownstreamMergeQueue;",
            "Landroid/content/Context;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)",
            "Lcom/google/android/music/sync/common/DownstreamMerger;"
        }
    .end annotation

    .prologue
    .line 428
    .local p3, "protocolState":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "music_merge_block_size"

    const/16 v3, 0x64

    invoke-static {v0, v1, v3}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    .line 430
    .local v2, "blockSize":I
    new-instance v0, Lcom/google/android/music/sync/google/MusicDownstreamMerger;

    iget-object v5, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mTag:Ljava/lang/String;

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/music/sync/google/MusicDownstreamMerger;-><init>(Lcom/google/android/music/sync/common/AbstractSyncAdapter$DownstreamMergeQueue;ILandroid/content/Context;Ljava/util/Map;Ljava/lang/String;)V

    return-object v0
.end method

.method protected createDownstreamReader(Lcom/google/android/music/sync/common/AbstractSyncAdapter$DownstreamFetchQueue;ILandroid/content/Context;Ljava/util/Map;)Lcom/google/android/music/sync/common/DownstreamReader;
    .locals 6
    .param p1, "fetchQueue"    # Lcom/google/android/music/sync/common/AbstractSyncAdapter$DownstreamFetchQueue;
    .param p2, "queueCapacity"    # I
    .param p3, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/music/sync/common/AbstractSyncAdapter$DownstreamFetchQueue;",
            "I",
            "Landroid/content/Context;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)",
            "Lcom/google/android/music/sync/common/DownstreamReader;"
        }
    .end annotation

    .prologue
    .line 422
    .local p4, "protocolState":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    new-instance v0, Lcom/google/android/music/sync/google/MusicDownstreamReader;

    iget-object v5, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mTag:Ljava/lang/String;

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/music/sync/google/MusicDownstreamReader;-><init>(Lcom/google/android/music/sync/common/AbstractSyncAdapter$DownstreamFetchQueue;ILandroid/content/Context;Ljava/util/Map;Ljava/lang/String;)V

    return-object v0
.end method

.method protected createUpstreamReader(Lcom/google/android/music/sync/common/AbstractSyncAdapter$UpstreamQueue;Landroid/content/Context;Ljava/util/Map;)Lcom/google/android/music/sync/common/UpstreamReader;
    .locals 6
    .param p1, "upstreamQueue"    # Lcom/google/android/music/sync/common/AbstractSyncAdapter$UpstreamQueue;
    .param p2, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/music/sync/common/AbstractSyncAdapter$UpstreamQueue;",
            "Landroid/content/Context;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)",
            "Lcom/google/android/music/sync/common/UpstreamReader;"
        }
    .end annotation

    .prologue
    .line 436
    .local p3, "protocolState":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-static {p2}, Lcom/google/android/music/utils/MusicUtils;->areUpstreamTrackDeletesEnabled(Landroid/content/Context;)Z

    move-result v5

    .line 437
    .local v5, "enableTrackDeletes":Z
    if-nez v5, :cond_0

    .line 438
    const-string v0, "MusicSyncAdapter"

    const-string v1, "Upstream track deletions have been disabled."

    invoke-static {v0, v1}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 440
    :cond_0
    new-instance v0, Lcom/google/android/music/sync/google/MusicUpstreamReader;

    iget-object v4, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mTag:Ljava/lang/String;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/music/sync/google/MusicUpstreamReader;-><init>(Lcom/google/android/music/sync/common/AbstractSyncAdapter$UpstreamQueue;Landroid/content/Context;Ljava/util/Map;Ljava/lang/String;Z)V

    return-object v0
.end method

.method protected createUpstreamSender(Lcom/google/android/music/sync/common/AbstractSyncAdapter$UpstreamQueue;Landroid/content/Context;Ljava/util/Map;)Lcom/google/android/music/sync/common/UpstreamSender;
    .locals 8
    .param p1, "upstreamQueue"    # Lcom/google/android/music/sync/common/AbstractSyncAdapter$UpstreamQueue;
    .param p2, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/music/sync/common/AbstractSyncAdapter$UpstreamQueue;",
            "Landroid/content/Context;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)",
            "Lcom/google/android/music/sync/common/UpstreamSender;"
        }
    .end annotation

    .prologue
    .line 447
    .local p3, "protocolState":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "music_upstream_page_size"

    const/16 v3, 0xfa

    invoke-static {v0, v1, v3}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    .line 450
    .local v2, "upstreamBlockSize":I
    invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "music_enable_track_stats_upsync"

    const/4 v3, 0x0

    invoke-static {v0, v1, v3}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v7

    .line 452
    .local v7, "enableTrackStatsUpSync":Z
    new-instance v0, Lcom/google/android/music/sync/google/MusicUpstreamSender;

    iget-object v5, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mTag:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mMusicApiClient:Lcom/google/android/music/sync/api/MusicApiClient;

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v7}, Lcom/google/android/music/sync/google/MusicUpstreamSender;-><init>(Lcom/google/android/music/sync/common/AbstractSyncAdapter$UpstreamQueue;ILandroid/content/Context;Ljava/util/Map;Ljava/lang/String;Lcom/google/android/music/sync/api/MusicApiClient;Z)V

    return-object v0
.end method

.method protected fetchDataFromServer(Landroid/accounts/Account;Lcom/google/android/music/sync/common/AbstractSyncAdapter$DownstreamFetchQueue;Ljava/util/HashMap;)Z
    .locals 1
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "fetchQueue"    # Lcom/google/android/music/sync/common/AbstractSyncAdapter$DownstreamFetchQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            "Lcom/google/android/music/sync/common/AbstractSyncAdapter$DownstreamFetchQueue;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/AuthenticatorException;,
            Lcom/google/android/music/sync/common/HardSyncException;,
            Lcom/google/android/music/sync/common/SoftSyncException;
        }
    .end annotation

    .prologue
    .line 803
    .local p3, "protocolState":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/music/sync/google/MusicSyncAdapter;->getChangesFromServerAsDom(Landroid/accounts/Account;Lcom/google/android/music/sync/common/AbstractSyncAdapter$DownstreamFetchQueue;Ljava/util/HashMap;)Z

    move-result v0

    return v0
.end method

.method protected getStatsString(Ljava/lang/StringBuffer;Landroid/content/SyncResult;)V
    .locals 6
    .param p1, "sb"    # Ljava/lang/StringBuffer;
    .param p2, "result"    # Landroid/content/SyncResult;

    .prologue
    const-wide/16 v4, 0x0

    .line 1074
    iget-object v0, p2, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v0, v0, Landroid/content/SyncStats;->numUpdates:J

    cmp-long v0, v0, v4

    if-lez v0, :cond_0

    .line 1075
    const-string v0, "u"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-object v1, p2, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v1, Landroid/content/SyncStats;->numUpdates:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    .line 1077
    :cond_0
    iget-object v0, p2, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v0, v0, Landroid/content/SyncStats;->numInserts:J

    cmp-long v0, v0, v4

    if-lez v0, :cond_1

    .line 1078
    const-string v0, "i"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-object v1, p2, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v1, Landroid/content/SyncStats;->numInserts:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    .line 1080
    :cond_1
    iget-object v0, p2, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v0, v0, Landroid/content/SyncStats;->numDeletes:J

    cmp-long v0, v0, v4

    if-lez v0, :cond_2

    .line 1081
    const-string v0, "d"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-object v1, p2, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v1, Landroid/content/SyncStats;->numDeletes:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    .line 1083
    :cond_2
    iget-object v0, p2, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v0, v0, Landroid/content/SyncStats;->numEntries:J

    cmp-long v0, v0, v4

    if-lez v0, :cond_3

    .line 1084
    const-string v0, "n"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-object v1, p2, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v1, Landroid/content/SyncStats;->numEntries:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    .line 1086
    :cond_3
    iget-object v0, p2, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v0, v0, Landroid/content/SyncStats;->numSkippedEntries:J

    cmp-long v0, v0, v4

    if-lez v0, :cond_4

    .line 1087
    const-string v0, "k"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-object v1, p2, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v1, Landroid/content/SyncStats;->numSkippedEntries:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    .line 1089
    :cond_4
    invoke-virtual {p2}, Landroid/content/SyncResult;->toDebugString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1090
    return-void
.end method

.method protected onDownstreamComplete(Landroid/accounts/Account;Ljava/util/HashMap;)V
    .locals 5
    .param p1, "account"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 979
    .local p2, "protocolState":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    iget-object v1, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/music/store/MediaStoreImporter;->updateLocalMusicBasedOnRemoteContentAsync(Landroid/content/Context;)V

    .line 980
    iget-object v1, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/music/store/RecentItemsManager;->updateRecentItemsAsync(Landroid/content/Context;)V

    .line 981
    iget-object v1, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/music/store/BlacklistItemsManager;->cleanupDuplicateItemsAsync(Landroid/content/Context;)V

    .line 983
    const-string v1, "is_manual_sync"

    invoke-virtual {p2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 986
    .local v0, "isManualSync":Z
    iget-object v1, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mContext:Landroid/content/Context;

    invoke-static {p1, v1}, Lcom/google/android/music/sync/google/MusicPeriodicUpdater;->checkAndEnablePeriodicUpdate(Landroid/accounts/Account;Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    if-eqz v0, :cond_1

    .line 987
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mContext:Landroid/content/Context;

    invoke-static {p1, v1}, Lcom/google/android/music/sync/google/MusicPeriodicUpdater;->performPeriodicUpdate(Landroid/accounts/Account;Landroid/content/Context;)V

    .line 990
    :cond_1
    iget-object v1, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/google/android/music/store/MusicContent;->CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    .line 992
    return-void
.end method

.method protected onDownstreamStart(Landroid/accounts/Account;Ljava/util/HashMap;)V
    .locals 0
    .param p1, "account"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 792
    .local p2, "protocolState":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    return-void
.end method

.method protected onLogSyncDetails(JJLandroid/content/SyncResult;)V
    .locals 5
    .param p1, "bytesSent"    # J
    .param p3, "bytesReceived"    # J
    .param p5, "result"    # Landroid/content/SyncResult;

    .prologue
    .line 1064
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 1065
    .local v0, "sb":Ljava/lang/StringBuffer;
    invoke-virtual {p0, v0, p5}, Lcom/google/android/music/sync/google/MusicSyncAdapter;->getStatsString(Ljava/lang/StringBuffer;Landroid/content/SyncResult;)V

    .line 1066
    const v1, 0x318f9

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "com.google.android.music"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    .line 1068
    return-void
.end method

.method protected onSyncEnd(Landroid/accounts/Account;Landroid/content/Context;Ljava/util/Map;Z)V
    .locals 8
    .param p1, "syncAccount"    # Landroid/accounts/Account;
    .param p2, "context"    # Landroid/content/Context;
    .param p4, "success"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            "Landroid/content/Context;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .local p3, "protocolState":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    const/4 v5, 0x0

    .line 463
    const/4 v0, 0x0

    .line 464
    .local v0, "accountMismatch":Z
    iget-object v6, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mContext:Landroid/content/Context;

    invoke-static {v6, p0}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v3

    .line 467
    .local v3, "preferences":Lcom/google/android/music/preferences/MusicPreferences;
    :try_start_0
    invoke-virtual {v3}, Lcom/google/android/music/preferences/MusicPreferences;->getSyncAccount()Landroid/accounts/Account;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 469
    .local v1, "configuredAccount":Landroid/accounts/Account;
    invoke-static {p0}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    .line 472
    if-nez v1, :cond_5

    .line 473
    const-string v6, "MusicSyncAdapter"

    const-string v7, "Just synced account has been removed"

    invoke-static {v6, v7}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 474
    const/4 v0, 0x1

    .line 480
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    .line 482
    if-nez v1, :cond_6

    move v4, v5

    .line 485
    .local v4, "requestSync":Z
    :goto_1
    sget-object v6, Lcom/google/android/music/utils/async/AsyncWorkers;->sBackendServiceWorker:Lcom/google/android/music/utils/LoggableHandler;

    new-instance v7, Lcom/google/android/music/sync/google/MusicSyncAdapter$1;

    invoke-direct {v7, p0, p2, v4}, Lcom/google/android/music/sync/google/MusicSyncAdapter$1;-><init>(Lcom/google/android/music/sync/google/MusicSyncAdapter;Landroid/content/Context;Z)V

    invoke-static {v6, v7}, Lcom/google/android/music/utils/async/AsyncWorkers;->runAsync(Lcom/google/android/music/utils/LoggableHandler;Ljava/lang/Runnable;)V

    .line 491
    .end local v4    # "requestSync":Z
    :cond_1
    new-instance v6, Landroid/content/Intent;

    const-string v7, "com.google.android.music.SYNC_COMPLETE"

    invoke-direct {v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2, v6}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 494
    invoke-virtual {v3}, Lcom/google/android/music/preferences/MusicPreferences;->getShowSyncNotification()Z

    move-result v6

    if-nez v6, :cond_2

    iget-object v6, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mSyncingNotification:Landroid/app/Notification;

    if-eqz v6, :cond_3

    .line 495
    :cond_2
    const-string v6, "notification"

    invoke-virtual {p2, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/NotificationManager;

    .line 497
    .local v2, "nm":Landroid/app/NotificationManager;
    const/16 v6, 0x65

    invoke-virtual {v2, v6}, Landroid/app/NotificationManager;->cancel(I)V

    .line 498
    const/4 v6, 0x0

    iput-object v6, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mSyncingNotification:Landroid/app/Notification;

    .line 499
    invoke-virtual {v3, v5}, Lcom/google/android/music/preferences/MusicPreferences;->setShowSyncNotification(Z)V

    .line 502
    .end local v2    # "nm":Landroid/app/NotificationManager;
    :cond_3
    if-nez v0, :cond_4

    if-eqz p4, :cond_4

    .line 503
    invoke-direct {p0, p1, p3}, Lcom/google/android/music/sync/google/MusicSyncAdapter;->sendNotificationIfNecessary(Landroid/accounts/Account;Ljava/util/Map;)V

    .line 505
    :cond_4
    return-void

    .line 469
    .end local v1    # "configuredAccount":Landroid/accounts/Account;
    :catchall_0
    move-exception v5

    invoke-static {p0}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    throw v5

    .line 475
    .restart local v1    # "configuredAccount":Landroid/accounts/Account;
    :cond_5
    invoke-virtual {p1, v1}, Landroid/accounts/Account;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 476
    const-string v6, "MusicSyncAdapter"

    const-string v7, "Streaming account has changed"

    invoke-static {v6, v7}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 477
    const/4 v0, 0x1

    goto :goto_0

    .line 482
    :cond_6
    const/4 v4, 0x1

    goto :goto_1
.end method

.method protected onSyncStart(Landroid/accounts/Account;Landroid/content/Context;Ljava/util/Map;Landroid/os/Bundle;)V
    .locals 14
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "context"    # Landroid/content/Context;
    .param p4, "extras"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            "Landroid/content/Context;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Landroid/os/Bundle;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/music/sync/common/HardSyncException;,
            Lcom/google/android/music/sync/common/SoftSyncException;
        }
    .end annotation

    .prologue
    .line 596
    .local p3, "protocolState":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    move-object/from16 v0, p2

    invoke-static {v0, p0}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v7

    .line 598
    .local v7, "preferences":Lcom/google/android/music/preferences/MusicPreferences;
    invoke-direct {p0, p1}, Lcom/google/android/music/sync/google/MusicSyncAdapter;->maybeUpdateSubscribedFeeds(Landroid/accounts/Account;)V

    .line 600
    invoke-virtual {v7}, Lcom/google/android/music/preferences/MusicPreferences;->getSyncAccount()Landroid/accounts/Account;

    move-result-object v2

    .line 601
    .local v2, "configuredAccount":Landroid/accounts/Account;
    invoke-virtual {v7}, Lcom/google/android/music/preferences/MusicPreferences;->isValidAccount()Z

    move-result v10

    if-nez v10, :cond_1

    const/4 v6, 0x1

    .line 602
    .local v6, "isSyncingInvalidAccount":Z
    :goto_0
    invoke-virtual {v7}, Lcom/google/android/music/preferences/MusicPreferences;->getShowSyncNotification()Z

    move-result v10

    if-eqz v10, :cond_0

    .line 603
    invoke-direct {p0}, Lcom/google/android/music/sync/google/MusicSyncAdapter;->showSyncingNotification()V

    .line 605
    :cond_0
    invoke-static {p0}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    .line 609
    const-string v10, "check_syncable_account"

    const/4 v11, 0x1

    move-object/from16 v0, p4

    invoke-virtual {v0, v10, v11}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v10

    if-eqz v10, :cond_2

    invoke-virtual {p1, v2}, Landroid/accounts/Account;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_2

    .line 615
    const-string v10, "com.google.android.music.MusicContent"

    const/4 v11, 0x0

    invoke-static {p1, v10, v11}, Landroid/content/ContentResolver;->setSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;Z)V

    .line 616
    const-string v10, "com.google.android.music.MusicContent"

    const/4 v11, 0x0

    invoke-static {p1, v10, v11}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    .line 617
    const-string v10, "MusicSyncAdapter"

    const-string v11, "Sync is requested with wrong account. Ignoring..."

    invoke-static {v10, v11}, Lcom/google/android/music/log/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 618
    new-instance v10, Lcom/google/android/music/sync/common/HardSyncException;

    const-string v11, "Sync requested for unexpected account."

    invoke-direct {v10, v11}, Lcom/google/android/music/sync/common/HardSyncException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 601
    .end local v6    # "isSyncingInvalidAccount":Z
    :cond_1
    const/4 v6, 0x0

    goto :goto_0

    .line 621
    .restart local v6    # "isSyncingInvalidAccount":Z
    :cond_2
    iget-boolean v10, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->LOGV:Z

    if-eqz v10, :cond_3

    .line 622
    iget-object v10, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mTag:Ljava/lang/String;

    const-string v11, "Reading client state from db."

    invoke-static {v10, v11}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 624
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/music/sync/google/MusicSyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v10

    invoke-static {v10}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v8

    .line 626
    .local v8, "store":Lcom/google/android/music/store/Store;
    invoke-virtual {v8}, Lcom/google/android/music/store/Store;->beginRead()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 627
    .local v3, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v9, 0x0

    .line 629
    .local v9, "syncState":Lcom/google/android/music/sync/google/ClientSyncState;
    :try_start_0
    invoke-static {v3, p1}, Lcom/google/android/music/sync/google/ClientSyncState$Helpers;->get(Landroid/database/sqlite/SQLiteDatabase;Landroid/accounts/Account;)Lcom/google/android/music/sync/google/ClientSyncState;
    :try_end_0
    .catch Lcom/google/android/music/sync/common/ProviderException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v9

    .line 633
    invoke-virtual {v8, v3}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 635
    if-nez v9, :cond_5

    .line 636
    iget-boolean v10, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->LOGV:Z

    if-eqz v10, :cond_4

    .line 637
    iget-object v10, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mTag:Ljava/lang/String;

    const-string v11, "There is no sync state for this account."

    invoke-static {v10, v11}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 640
    :cond_4
    :try_start_1
    invoke-virtual {v8}, Lcom/google/android/music/store/Store;->beginWriteTxn()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 641
    invoke-static {}, Lcom/google/android/music/sync/google/ClientSyncState;->newBuilder()Lcom/google/android/music/sync/google/ClientSyncState$Builder;

    move-result-object v10

    const-wide/16 v12, 0x0

    invoke-virtual {v10, v12, v13}, Lcom/google/android/music/sync/google/ClientSyncState$Builder;->setRemoteTrackVersion(J)Lcom/google/android/music/sync/google/ClientSyncState$Builder;

    move-result-object v10

    const-wide/16 v12, 0x0

    invoke-virtual {v10, v12, v13}, Lcom/google/android/music/sync/google/ClientSyncState$Builder;->setRemotePlaylistVersion(J)Lcom/google/android/music/sync/google/ClientSyncState$Builder;

    move-result-object v10

    const-wide/16 v12, 0x0

    invoke-virtual {v10, v12, v13}, Lcom/google/android/music/sync/google/ClientSyncState$Builder;->setRemotePlentryVersion(J)Lcom/google/android/music/sync/google/ClientSyncState$Builder;

    move-result-object v10

    const-wide/16 v12, 0x0

    invoke-virtual {v10, v12, v13}, Lcom/google/android/music/sync/google/ClientSyncState$Builder;->setRemoteRadioStationVersion(J)Lcom/google/android/music/sync/google/ClientSyncState$Builder;

    move-result-object v10

    const-wide/16 v12, 0x0

    invoke-virtual {v10, v12, v13}, Lcom/google/android/music/sync/google/ClientSyncState$Builder;->setRemoteBlacklistItemVersion(J)Lcom/google/android/music/sync/google/ClientSyncState$Builder;

    move-result-object v10

    invoke-static {p1}, Lcom/google/android/music/store/Store;->computeAccountHash(Landroid/accounts/Account;)I

    move-result v11

    invoke-virtual {v10, v11}, Lcom/google/android/music/sync/google/ClientSyncState$Builder;->setRemoteAccount(I)Lcom/google/android/music/sync/google/ClientSyncState$Builder;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/android/music/sync/google/ClientSyncState$Builder;->build()Lcom/google/android/music/sync/google/ClientSyncState;

    move-result-object v9

    .line 649
    invoke-static {v3, p1, v9}, Lcom/google/android/music/sync/google/ClientSyncState$Helpers;->set(Landroid/database/sqlite/SQLiteDatabase;Landroid/accounts/Account;Lcom/google/android/music/sync/google/ClientSyncState;)V
    :try_end_1
    .catch Lcom/google/android/music/sync/common/ProviderException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 653
    const/4 v10, 0x1

    invoke-virtual {v8, v3, v10}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    .line 656
    :cond_5
    iget-boolean v10, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->LOGV:Z

    if-eqz v10, :cond_6

    .line 657
    iget-object v10, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mTag:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Current sync state:"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v9}, Lcom/google/android/music/sync/google/ClientSyncState;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 659
    :cond_6
    iget-object v10, v9, Lcom/google/android/music/sync/google/ClientSyncState;->mRemoteTrackVersion:Ljava/lang/Long;

    if-nez v10, :cond_b

    const-wide/16 v10, 0x0

    :goto_1
    iput-wide v10, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mInitialTrackVersion:J

    .line 661
    iget-object v10, v9, Lcom/google/android/music/sync/google/ClientSyncState;->mRemotePlaylistVersion:Ljava/lang/Long;

    if-nez v10, :cond_c

    const-wide/16 v10, 0x0

    :goto_2
    iput-wide v10, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mInitialPlaylistVersion:J

    .line 663
    iget-object v10, v9, Lcom/google/android/music/sync/google/ClientSyncState;->mRemotePlentryVersion:Ljava/lang/Long;

    if-nez v10, :cond_d

    const-wide/16 v10, 0x0

    :goto_3
    iput-wide v10, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mInitialPlaylistEntryVersion:J

    .line 665
    iget-object v10, v9, Lcom/google/android/music/sync/google/ClientSyncState;->mRemoteRadioStationVersion:Ljava/lang/Long;

    if-nez v10, :cond_e

    const-wide/16 v10, 0x0

    :goto_4
    iput-wide v10, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mInitialRadioStationVersion:J

    .line 667
    iget-object v10, v9, Lcom/google/android/music/sync/google/ClientSyncState;->mRemoteBlacklistItemVersion:Ljava/lang/Long;

    if-nez v10, :cond_f

    const-wide/16 v10, 0x0

    :goto_5
    iput-wide v10, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mInitialBlacklistItemVersion:J

    .line 670
    const-string v10, "merger_track_version"

    iget-wide v12, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mInitialTrackVersion:J

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    move-object/from16 v0, p3

    invoke-interface {v0, v10, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 671
    const-string v10, "merger_playlist_version"

    iget-wide v12, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mInitialPlaylistVersion:J

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    move-object/from16 v0, p3

    invoke-interface {v0, v10, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 672
    const-string v10, "merger_plentry_version"

    iget-wide v12, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mInitialPlaylistEntryVersion:J

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    move-object/from16 v0, p3

    invoke-interface {v0, v10, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 673
    const-string v10, "merger_radio_station_version"

    iget-wide v12, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mInitialRadioStationVersion:J

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    move-object/from16 v0, p3

    invoke-interface {v0, v10, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 674
    const-string v10, "merger_blacklist_item_version"

    iget-wide v12, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mInitialBlacklistItemVersion:J

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    move-object/from16 v0, p3

    invoke-interface {v0, v10, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 676
    iget-object v10, v9, Lcom/google/android/music/sync/google/ClientSyncState;->mEtagTrack:Ljava/lang/String;

    if-eqz v10, :cond_7

    .line 677
    const-string v10, "etag_track"

    iget-object v11, v9, Lcom/google/android/music/sync/google/ClientSyncState;->mEtagTrack:Ljava/lang/String;

    move-object/from16 v0, p3

    invoke-interface {v0, v10, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 679
    :cond_7
    iget-object v10, v9, Lcom/google/android/music/sync/google/ClientSyncState;->mEtagPlaylist:Ljava/lang/String;

    if-eqz v10, :cond_8

    .line 680
    const-string v10, "etag_playlist"

    iget-object v11, v9, Lcom/google/android/music/sync/google/ClientSyncState;->mEtagPlaylist:Ljava/lang/String;

    move-object/from16 v0, p3

    invoke-interface {v0, v10, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 682
    :cond_8
    iget-object v10, v9, Lcom/google/android/music/sync/google/ClientSyncState;->mEtagPlaylistEntry:Ljava/lang/String;

    if-eqz v10, :cond_9

    .line 683
    const-string v10, "etag_playlist_entry"

    iget-object v11, v9, Lcom/google/android/music/sync/google/ClientSyncState;->mEtagPlaylistEntry:Ljava/lang/String;

    move-object/from16 v0, p3

    invoke-interface {v0, v10, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 685
    :cond_9
    const-string v10, "remote_account"

    iget-object v11, v9, Lcom/google/android/music/sync/google/ClientSyncState;->mRemoteAccount:Ljava/lang/Integer;

    move-object/from16 v0, p3

    invoke-interface {v0, v10, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 686
    const-string v10, "account"

    move-object/from16 v0, p3

    invoke-interface {v0, v10, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 688
    const-string v10, "feed"

    move-object/from16 v0, p4

    invoke-virtual {v0, v10}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_22

    .line 689
    const-string v10, "feed"

    move-object/from16 v0, p4

    invoke-virtual {v0, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 690
    .local v5, "feed":Ljava/lang/String;
    const-string v10, "track-update"

    invoke-virtual {v10, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_10

    .line 691
    iget-boolean v10, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->LOGV:Z

    if-eqz v10, :cond_a

    .line 692
    iget-object v10, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mTag:Ljava/lang/String;

    const-string v11, "Sync manager specified track feed type."

    invoke-static {v10, v11}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 694
    :cond_a
    const-string v10, "downstream_state"

    new-instance v11, Lcom/google/android/music/sync/google/MusicSyncAdapter$TickledDownstreamState;

    sget-object v12, Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;->TRACKS:Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;

    invoke-direct {v11, v12}, Lcom/google/android/music/sync/google/MusicSyncAdapter$TickledDownstreamState;-><init>(Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;)V

    move-object/from16 v0, p3

    invoke-interface {v0, v10, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 781
    .end local v5    # "feed":Ljava/lang/String;
    :goto_6
    const-string v10, "new_purchased_count"

    new-instance v11, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v12, 0x0

    invoke-direct {v11, v12}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    move-object/from16 v0, p3

    invoke-interface {v0, v10, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 782
    const-string v10, "new_purchased_same_album"

    const/4 v11, 0x1

    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    move-object/from16 v0, p3

    invoke-interface {v0, v10, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 783
    const-string v10, "is_manual_sync"

    const-string v11, "force"

    const/4 v12, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v11, v12}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v11

    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    move-object/from16 v0, p3

    invoke-interface {v0, v10, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 785
    return-void

    .line 630
    :catch_0
    move-exception v4

    .line 631
    .local v4, "e":Lcom/google/android/music/sync/common/ProviderException;
    :try_start_2
    new-instance v10, Lcom/google/android/music/sync/common/SoftSyncException;

    const-string v11, "Unable to read sync state: "

    invoke-direct {v10, v11, v4}, Lcom/google/android/music/sync/common/SoftSyncException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v10
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 633
    .end local v4    # "e":Lcom/google/android/music/sync/common/ProviderException;
    :catchall_0
    move-exception v10

    invoke-virtual {v8, v3}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    throw v10

    .line 650
    :catch_1
    move-exception v4

    .line 651
    .restart local v4    # "e":Lcom/google/android/music/sync/common/ProviderException;
    :try_start_3
    new-instance v10, Lcom/google/android/music/sync/common/SoftSyncException;

    const-string v11, "Unable to write sync state: "

    invoke-direct {v10, v11, v4}, Lcom/google/android/music/sync/common/SoftSyncException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v10
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 653
    .end local v4    # "e":Lcom/google/android/music/sync/common/ProviderException;
    :catchall_1
    move-exception v10

    const/4 v11, 0x1

    invoke-virtual {v8, v3, v11}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    throw v10

    .line 659
    :cond_b
    iget-object v10, v9, Lcom/google/android/music/sync/google/ClientSyncState;->mRemoteTrackVersion:Ljava/lang/Long;

    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    goto/16 :goto_1

    .line 661
    :cond_c
    iget-object v10, v9, Lcom/google/android/music/sync/google/ClientSyncState;->mRemotePlaylistVersion:Ljava/lang/Long;

    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    goto/16 :goto_2

    .line 663
    :cond_d
    iget-object v10, v9, Lcom/google/android/music/sync/google/ClientSyncState;->mRemotePlentryVersion:Ljava/lang/Long;

    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    goto/16 :goto_3

    .line 665
    :cond_e
    iget-object v10, v9, Lcom/google/android/music/sync/google/ClientSyncState;->mRemoteRadioStationVersion:Ljava/lang/Long;

    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    goto/16 :goto_4

    .line 667
    :cond_f
    iget-object v10, v9, Lcom/google/android/music/sync/google/ClientSyncState;->mRemoteBlacklistItemVersion:Ljava/lang/Long;

    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    goto/16 :goto_5

    .line 696
    .restart local v5    # "feed":Ljava/lang/String;
    :cond_10
    const-string v10, "playlist-update"

    invoke-virtual {v10, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_12

    .line 697
    iget-boolean v10, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->LOGV:Z

    if-eqz v10, :cond_11

    .line 698
    iget-object v10, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mTag:Ljava/lang/String;

    const-string v11, "Sync manager specified playlist feed type."

    invoke-static {v10, v11}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 700
    :cond_11
    const-string v10, "downstream_state"

    new-instance v11, Lcom/google/android/music/sync/google/MusicSyncAdapter$TickledDownstreamState;

    sget-object v12, Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;->PLAYLISTS:Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;

    invoke-direct {v11, v12}, Lcom/google/android/music/sync/google/MusicSyncAdapter$TickledDownstreamState;-><init>(Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;)V

    move-object/from16 v0, p3

    invoke-interface {v0, v10, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_6

    .line 702
    :cond_12
    const-string v10, "playlist-entry-update"

    invoke-virtual {v10, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_14

    .line 703
    iget-boolean v10, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->LOGV:Z

    if-eqz v10, :cond_13

    .line 704
    iget-object v10, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mTag:Ljava/lang/String;

    const-string v11, "Sync manager specified playlist-entry feed type."

    invoke-static {v10, v11}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 706
    :cond_13
    const-string v10, "downstream_state"

    new-instance v11, Lcom/google/android/music/sync/google/MusicSyncAdapter$TickledDownstreamState;

    sget-object v12, Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;->PLENTRIES:Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;

    invoke-direct {v11, v12}, Lcom/google/android/music/sync/google/MusicSyncAdapter$TickledDownstreamState;-><init>(Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;)V

    move-object/from16 v0, p3

    invoke-interface {v0, v10, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_6

    .line 708
    :cond_14
    const-string v10, "radio-station-update"

    invoke-virtual {v10, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_16

    .line 709
    iget-boolean v10, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->LOGV:Z

    if-eqz v10, :cond_15

    .line 710
    iget-object v10, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mTag:Ljava/lang/String;

    const-string v11, "Sync manager specified radio-station feed type."

    invoke-static {v10, v11}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 712
    :cond_15
    const-string v10, "downstream_state"

    new-instance v11, Lcom/google/android/music/sync/google/MusicSyncAdapter$TickledDownstreamState;

    sget-object v12, Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;->RADIO_STATIONS:Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;

    invoke-direct {v11, v12}, Lcom/google/android/music/sync/google/MusicSyncAdapter$TickledDownstreamState;-><init>(Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;)V

    move-object/from16 v0, p3

    invoke-interface {v0, v10, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_6

    .line 714
    :cond_16
    const-string v10, "config-update"

    invoke-virtual {v10, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_18

    .line 715
    iget-boolean v10, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->LOGV:Z

    if-eqz v10, :cond_17

    .line 716
    iget-object v10, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mTag:Ljava/lang/String;

    const-string v11, "Sync manager specified config feed type."

    invoke-static {v10, v11}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 718
    :cond_17
    const-string v10, "downstream_state"

    new-instance v11, Lcom/google/android/music/sync/google/MusicSyncAdapter$TickledDownstreamState;

    sget-object v12, Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;->CONFIG:Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;

    invoke-direct {v11, v12}, Lcom/google/android/music/sync/google/MusicSyncAdapter$TickledDownstreamState;-><init>(Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;)V

    move-object/from16 v0, p3

    invoke-interface {v0, v10, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_6

    .line 720
    :cond_18
    const-string v10, "sound-search-update"

    invoke-virtual {v10, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1a

    .line 721
    iget-boolean v10, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->LOGV:Z

    if-eqz v10, :cond_19

    .line 722
    iget-object v10, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mTag:Ljava/lang/String;

    const-string v11, "Sync manager specified sound-search playlist feed type."

    invoke-static {v10, v11}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 724
    :cond_19
    const-string v10, "downstream_state"

    new-instance v11, Lcom/google/android/music/sync/google/MusicSyncAdapter$TickledDownstreamState;

    sget-object v12, Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;->SOUND_SEARCH:Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;

    invoke-direct {v11, v12}, Lcom/google/android/music/sync/google/MusicSyncAdapter$TickledDownstreamState;-><init>(Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;)V

    move-object/from16 v0, p3

    invoke-interface {v0, v10, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_6

    .line 726
    :cond_1a
    const-string v10, "dismissed-item-list-update"

    invoke-virtual {v10, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1c

    .line 727
    iget-boolean v10, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->LOGV:Z

    if-eqz v10, :cond_1b

    .line 728
    iget-object v10, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mTag:Ljava/lang/String;

    const-string v11, "Sync manager specified blacklist-item feed type."

    invoke-static {v10, v11}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 730
    :cond_1b
    const-string v10, "downstream_state"

    new-instance v11, Lcom/google/android/music/sync/google/MusicSyncAdapter$TickledDownstreamState;

    sget-object v12, Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;->BLACKLIST_ITEMS:Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;

    invoke-direct {v11, v12}, Lcom/google/android/music/sync/google/MusicSyncAdapter$TickledDownstreamState;-><init>(Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;)V

    move-object/from16 v0, p3

    invoke-interface {v0, v10, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_6

    .line 732
    :cond_1c
    const-string v10, "cloud-queue-update"

    invoke-virtual {v10, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1e

    .line 733
    iget-boolean v10, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->LOGV:Z

    if-eqz v10, :cond_1d

    .line 734
    iget-object v10, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mTag:Ljava/lang/String;

    const-string v11, "Sync manager specified cloud queue feed type."

    invoke-static {v10, v11}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 736
    :cond_1d
    const-string v10, "downstream_state"

    new-instance v11, Lcom/google/android/music/sync/google/MusicSyncAdapter$TickledDownstreamState;

    sget-object v12, Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;->CLOUD_QUEUE:Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;

    invoke-direct {v11, v12}, Lcom/google/android/music/sync/google/MusicSyncAdapter$TickledDownstreamState;-><init>(Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;)V

    move-object/from16 v0, p3

    invoke-interface {v0, v10, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_6

    .line 739
    :cond_1e
    iget-boolean v10, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->LOGV:Z

    if-eqz v10, :cond_1f

    .line 740
    iget-object v10, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mTag:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Sync manager specified an unknown feed type "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ". Syncing all feeds."

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 744
    :cond_1f
    if-eqz v6, :cond_21

    .line 745
    iget-boolean v10, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->LOGV:Z

    if-eqz v10, :cond_20

    .line 746
    iget-object v10, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mTag:Ljava/lang/String;

    const-string v11, "Will only sync config because we have an invalid account."

    invoke-static {v10, v11}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 748
    :cond_20
    const-string v10, "downstream_state"

    new-instance v11, Lcom/google/android/music/sync/google/MusicSyncAdapter$TickledDownstreamState;

    sget-object v12, Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;->CONFIG:Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;

    invoke-direct {v11, v12}, Lcom/google/android/music/sync/google/MusicSyncAdapter$TickledDownstreamState;-><init>(Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;)V

    move-object/from16 v0, p3

    invoke-interface {v0, v10, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_6

    .line 752
    :cond_21
    const-string v10, "downstream_state"

    new-instance v11, Lcom/google/android/music/sync/google/MusicSyncAdapter$AllFeedsDownstreamState;

    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v12

    invoke-direct {v11, v12}, Lcom/google/android/music/sync/google/MusicSyncAdapter$AllFeedsDownstreamState;-><init>(Landroid/content/ContentResolver;)V

    move-object/from16 v0, p3

    invoke-interface {v0, v10, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_6

    .line 756
    .end local v5    # "feed":Ljava/lang/String;
    :cond_22
    const-string v10, "configAlarm"

    move-object/from16 v0, p4

    invoke-virtual {v0, v10}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_24

    .line 758
    iget-boolean v10, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->LOGV:Z

    if-eqz v10, :cond_23

    .line 759
    iget-object v10, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mTag:Ljava/lang/String;

    const-string v11, "Sync manager specified configAlarm feed type."

    invoke-static {v10, v11}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 761
    :cond_23
    const-string v10, "downstream_state"

    new-instance v11, Lcom/google/android/music/sync/google/MusicSyncAdapter$TickledDownstreamState;

    sget-object v12, Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;->CONFIG_ALARM:Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;

    invoke-direct {v11, v12}, Lcom/google/android/music/sync/google/MusicSyncAdapter$TickledDownstreamState;-><init>(Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;)V

    move-object/from16 v0, p3

    invoke-interface {v0, v10, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_6

    .line 765
    :cond_24
    iget-boolean v10, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->LOGV:Z

    if-eqz v10, :cond_25

    .line 766
    iget-object v10, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mTag:Ljava/lang/String;

    const-string v11, "Sync manager provided no specific feed type.  Syncing all feeds."

    invoke-static {v10, v11}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 769
    :cond_25
    if-eqz v6, :cond_27

    .line 770
    iget-boolean v10, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->LOGV:Z

    if-eqz v10, :cond_26

    .line 771
    iget-object v10, p0, Lcom/google/android/music/sync/google/MusicSyncAdapter;->mTag:Ljava/lang/String;

    const-string v11, "Will only sync config because we have an invalid account."

    invoke-static {v10, v11}, Lcom/google/android/music/log/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 773
    :cond_26
    const-string v10, "downstream_state"

    new-instance v11, Lcom/google/android/music/sync/google/MusicSyncAdapter$TickledDownstreamState;

    sget-object v12, Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;->CONFIG:Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;

    invoke-direct {v11, v12}, Lcom/google/android/music/sync/google/MusicSyncAdapter$TickledDownstreamState;-><init>(Lcom/google/android/music/sync/google/MusicSyncAdapter$DownstreamState$Feed;)V

    move-object/from16 v0, p3

    invoke-interface {v0, v10, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_6

    .line 776
    :cond_27
    const-string v10, "downstream_state"

    new-instance v11, Lcom/google/android/music/sync/google/MusicSyncAdapter$AllFeedsDownstreamState;

    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v12

    invoke-direct {v11, v12}, Lcom/google/android/music/sync/google/MusicSyncAdapter$AllFeedsDownstreamState;-><init>(Landroid/content/ContentResolver;)V

    move-object/from16 v0, p3

    invoke-interface {v0, v10, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_6
.end method
