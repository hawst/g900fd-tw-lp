.class public Lcom/google/android/music/tutorial/SignupStatus$SignupCheckService;
.super Landroid/app/IntentService;
.source "SignupStatus.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/tutorial/SignupStatus;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SignupCheckService"
.end annotation


# instance fields
.field private mMarketCatalogService:Lcom/google/android/finsky/services/IMarketCatalogService;

.field private mMarketCatalogServiceBound:Z

.field private mMarketServiceSafeConnection:Lcom/google/android/music/utils/SafeServiceConnection;

.field private mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

.field private mNetworkMonitorServiceConnection:Lcom/google/android/music/net/NetworkMonitorServiceConnection;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 158
    const-string v0, "SignupCheckService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 135
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/tutorial/SignupStatus$SignupCheckService;->mMarketCatalogService:Lcom/google/android/finsky/services/IMarketCatalogService;

    .line 137
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/tutorial/SignupStatus$SignupCheckService;->mMarketCatalogServiceBound:Z

    .line 143
    new-instance v0, Lcom/google/android/music/tutorial/SignupStatus$SignupCheckService$1;

    invoke-direct {v0, p0}, Lcom/google/android/music/tutorial/SignupStatus$SignupCheckService$1;-><init>(Lcom/google/android/music/tutorial/SignupStatus$SignupCheckService;)V

    iput-object v0, p0, Lcom/google/android/music/tutorial/SignupStatus$SignupCheckService;->mMarketServiceSafeConnection:Lcom/google/android/music/utils/SafeServiceConnection;

    .line 159
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/tutorial/SignupStatus$SignupCheckService;Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/tutorial/SignupStatus$SignupCheckService;
    .param p1, "x1"    # Landroid/content/ComponentName;
    .param p2, "x2"    # Landroid/os/IBinder;

    .prologue
    .line 123
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/tutorial/SignupStatus$SignupCheckService;->onServiceConnectedImp(Landroid/content/ComponentName;Landroid/os/IBinder;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/music/tutorial/SignupStatus$SignupCheckService;Landroid/content/ComponentName;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/tutorial/SignupStatus$SignupCheckService;
    .param p1, "x1"    # Landroid/content/ComponentName;

    .prologue
    .line 123
    invoke-direct {p0, p1}, Lcom/google/android/music/tutorial/SignupStatus$SignupCheckService;->onServiceDisconnectedImp(Landroid/content/ComponentName;)V

    return-void
.end method

.method private autoSelectAccountWithVerification(Landroid/content/SharedPreferences;Lcom/google/android/music/preferences/MusicPreferences;)V
    .locals 2
    .param p1, "sharedPreferences"    # Landroid/content/SharedPreferences;
    .param p2, "preferences"    # Lcom/google/android/music/preferences/MusicPreferences;

    .prologue
    .line 259
    invoke-direct {p0}, Lcom/google/android/music/tutorial/SignupStatus$SignupCheckService;->getSoleAccount()Landroid/accounts/Account;

    move-result-object v0

    .line 260
    .local v0, "soleAccount":Landroid/accounts/Account;
    if-nez v0, :cond_1

    .line 272
    :cond_0
    :goto_0
    return-void

    .line 264
    :cond_1
    # invokes: Lcom/google/android/music/tutorial/SignupStatus;->isAccountVerified(Landroid/content/Context;Landroid/content/SharedPreferences;Landroid/accounts/Account;)Z
    invoke-static {p0, p1, v0}, Lcom/google/android/music/tutorial/SignupStatus;->access$500(Landroid/content/Context;Landroid/content/SharedPreferences;Landroid/accounts/Account;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 267
    invoke-virtual {p2, v0}, Lcom/google/android/music/preferences/MusicPreferences;->autoSelectStreamingAccount(Landroid/accounts/Account;)Z

    goto :goto_0
.end method

.method private autoSelectAccountWithoutVerification(Lcom/google/android/music/preferences/MusicPreferences;)V
    .locals 1
    .param p1, "preferences"    # Lcom/google/android/music/preferences/MusicPreferences;

    .prologue
    .line 275
    invoke-direct {p0}, Lcom/google/android/music/tutorial/SignupStatus$SignupCheckService;->getSoleAccount()Landroid/accounts/Account;

    move-result-object v0

    .line 276
    .local v0, "soleAccount":Landroid/accounts/Account;
    if-nez v0, :cond_0

    .line 287
    :goto_0
    return-void

    .line 283
    :cond_0
    invoke-virtual {p1, v0}, Lcom/google/android/music/preferences/MusicPreferences;->autoSelectStreamingAccount(Landroid/accounts/Account;)Z

    goto :goto_0
.end method

.method private checkStoreAvailable()V
    .locals 14

    .prologue
    .line 290
    iget-object v9, p0, Lcom/google/android/music/tutorial/SignupStatus$SignupCheckService;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    invoke-virtual {v9}, Lcom/google/android/music/preferences/MusicPreferences;->getStoreAvailableLastChecked()J

    move-result-wide v6

    .line 291
    .local v6, "lastChecked":J
    const-wide/32 v10, 0xf731400

    add-long/2addr v10, v6

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    cmp-long v9, v10, v12

    if-lez v9, :cond_0

    .line 318
    :goto_0
    return-void

    .line 296
    :cond_0
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/music/tutorial/SignupStatus$SignupCheckService;->connectToMarketService()Z

    move-result v9

    if-nez v9, :cond_1

    .line 297
    const/4 v9, 0x0

    invoke-direct {p0, v9}, Lcom/google/android/music/tutorial/SignupStatus$SignupCheckService;->markStoreAvailability(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 316
    invoke-direct {p0}, Lcom/google/android/music/tutorial/SignupStatus$SignupCheckService;->disconnectFromMarketService()V

    goto :goto_0

    .line 301
    :cond_1
    :try_start_1
    invoke-static {p0}, Lcom/google/android/music/tutorial/SignupStatus;->getAllAccounts(Landroid/content/Context;)[Landroid/accounts/Account;

    move-result-object v1

    .line 302
    .local v1, "accounts":[Landroid/accounts/Account;
    const/4 v8, 0x0

    .line 303
    .local v8, "storeAvailable":Z
    move-object v2, v1

    .local v2, "arr$":[Landroid/accounts/Account;
    array-length v5, v2

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_1
    if-ge v4, v5, :cond_2

    aget-object v0, v2, v4

    .line 304
    .local v0, "account":Landroid/accounts/Account;
    iget-object v9, p0, Lcom/google/android/music/tutorial/SignupStatus$SignupCheckService;->mMarketCatalogService:Lcom/google/android/finsky/services/IMarketCatalogService;

    iget-object v10, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    const/4 v11, 0x2

    invoke-interface {v9, v10, v11}, Lcom/google/android/finsky/services/IMarketCatalogService;->isBackendEnabled(Ljava/lang/String;I)Z

    move-result v8

    .line 306
    if-eqz v8, :cond_3

    .line 310
    .end local v0    # "account":Landroid/accounts/Account;
    :cond_2
    invoke-direct {p0, v8}, Lcom/google/android/music/tutorial/SignupStatus$SignupCheckService;->markStoreAvailability(Z)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 316
    .end local v1    # "accounts":[Landroid/accounts/Account;
    .end local v2    # "arr$":[Landroid/accounts/Account;
    .end local v4    # "i$":I
    .end local v5    # "len$":I
    .end local v8    # "storeAvailable":Z
    :goto_2
    invoke-direct {p0}, Lcom/google/android/music/tutorial/SignupStatus$SignupCheckService;->disconnectFromMarketService()V

    goto :goto_0

    .line 303
    .restart local v0    # "account":Landroid/accounts/Account;
    .restart local v1    # "accounts":[Landroid/accounts/Account;
    .restart local v2    # "arr$":[Landroid/accounts/Account;
    .restart local v4    # "i$":I
    .restart local v5    # "len$":I
    .restart local v8    # "storeAvailable":Z
    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 311
    .end local v0    # "account":Landroid/accounts/Account;
    .end local v1    # "accounts":[Landroid/accounts/Account;
    .end local v2    # "arr$":[Landroid/accounts/Account;
    .end local v4    # "i$":I
    .end local v5    # "len$":I
    .end local v8    # "storeAvailable":Z
    :catch_0
    move-exception v3

    .line 312
    .local v3, "e":Landroid/os/RemoteException;
    :try_start_2
    const-string v9, "SignupStatus"

    invoke-virtual {v3}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 313
    const/4 v9, 0x0

    invoke-direct {p0, v9}, Lcom/google/android/music/tutorial/SignupStatus$SignupCheckService;->markStoreAvailability(Z)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 316
    .end local v3    # "e":Landroid/os/RemoteException;
    :catchall_0
    move-exception v9

    invoke-direct {p0}, Lcom/google/android/music/tutorial/SignupStatus$SignupCheckService;->disconnectFromMarketService()V

    throw v9
.end method

.method private connectToMarketService()Z
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 329
    invoke-direct {p0, p0}, Lcom/google/android/music/tutorial/SignupStatus$SignupCheckService;->getMarketServiceIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    .line 330
    .local v1, "marketServiceIntent":Landroid/content/Intent;
    if-nez v1, :cond_0

    .line 331
    const-string v3, "SignupStatus"

    const-string v4, "Could not find market service"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 354
    :goto_0
    return v2

    .line 334
    :cond_0
    iget-object v4, p0, Lcom/google/android/music/tutorial/SignupStatus$SignupCheckService;->mMarketServiceSafeConnection:Lcom/google/android/music/utils/SafeServiceConnection;

    invoke-virtual {v4, p0, v1, v3}, Lcom/google/android/music/utils/SafeServiceConnection;->bindService(Landroid/content/Context;Landroid/content/Intent;I)Z

    move-result v4

    if-nez v4, :cond_1

    .line 336
    const-string v3, "SignupStatus"

    const-string v4, "Could not find market service"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 339
    :cond_1
    iput-boolean v3, p0, Lcom/google/android/music/tutorial/SignupStatus$SignupCheckService;->mMarketCatalogServiceBound:Z

    .line 340
    monitor-enter p0

    .line 341
    :try_start_0
    iget-object v4, p0, Lcom/google/android/music/tutorial/SignupStatus$SignupCheckService;->mMarketCatalogService:Lcom/google/android/finsky/services/IMarketCatalogService;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v4, :cond_2

    .line 343
    const-wide/16 v4, 0x2710

    :try_start_1
    invoke-virtual {p0, v4, v5}, Ljava/lang/Object;->wait(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 348
    :cond_2
    :goto_1
    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 349
    iget-object v4, p0, Lcom/google/android/music/tutorial/SignupStatus$SignupCheckService;->mMarketCatalogService:Lcom/google/android/finsky/services/IMarketCatalogService;

    if-nez v4, :cond_3

    .line 350
    const-string v3, "SignupStatus"

    const-string v4, "Could not connect to market service"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 344
    :catch_0
    move-exception v0

    .line 345
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_3
    const-string v4, "SignupStatus"

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 348
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v2

    :cond_3
    move v2, v3

    .line 354
    goto :goto_0
.end method

.method private disableNetworkRecevier()V
    .locals 4

    .prologue
    .line 239
    invoke-virtual {p0}, Lcom/google/android/music/tutorial/SignupStatus$SignupCheckService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    new-instance v1, Landroid/content/ComponentName;

    const-class v2, Lcom/google/android/music/tutorial/SignupStatus$NetworkChangedReceiver;

    invoke-direct {v1, p0, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v2, 0x2

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 242
    return-void
.end method

.method private disconnectFromMarketService()V
    .locals 1

    .prologue
    .line 363
    iget-boolean v0, p0, Lcom/google/android/music/tutorial/SignupStatus$SignupCheckService;->mMarketCatalogServiceBound:Z

    if-eqz v0, :cond_0

    .line 364
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/music/tutorial/SignupStatus$SignupCheckService;->mMarketCatalogServiceBound:Z

    .line 365
    iget-object v0, p0, Lcom/google/android/music/tutorial/SignupStatus$SignupCheckService;->mMarketServiceSafeConnection:Lcom/google/android/music/utils/SafeServiceConnection;

    invoke-virtual {v0, p0}, Lcom/google/android/music/utils/SafeServiceConnection;->unbindService(Landroid/content/Context;)V

    .line 367
    :cond_0
    return-void
.end method

.method private enableNetworkReceiver()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 233
    invoke-virtual {p0}, Lcom/google/android/music/tutorial/SignupStatus$SignupCheckService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    new-instance v1, Landroid/content/ComponentName;

    const-class v2, Lcom/google/android/music/tutorial/SignupStatus$NetworkChangedReceiver;

    invoke-direct {v1, p0, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1, v3, v3}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 236
    return-void
.end method

.method private getMarketServiceIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 358
    const-string v0, "com.google.android.finsky.services.IMarketCatalogService.BIND"

    const-string v1, "com.android.vending"

    invoke-static {p1, v0, v1}, Lcom/google/android/music/utils/SystemUtils;->getExplicitServiceIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private getSoleAccount()Landroid/accounts/Account;
    .locals 3

    .prologue
    .line 250
    invoke-static {p0}, Lcom/google/android/music/tutorial/SignupStatus;->getAllAccounts(Landroid/content/Context;)[Landroid/accounts/Account;

    move-result-object v0

    .line 251
    .local v0, "accounts":[Landroid/accounts/Account;
    if-eqz v0, :cond_0

    array-length v1, v0

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    .line 252
    :cond_0
    const/4 v1, 0x0

    .line 254
    :goto_0
    return-object v1

    :cond_1
    const/4 v1, 0x0

    aget-object v1, v0, v1

    goto :goto_0
.end method

.method private markStoreAvailability(Z)V
    .locals 4
    .param p1, "available"    # Z

    .prologue
    .line 321
    iget-object v0, p0, Lcom/google/android/music/tutorial/SignupStatus$SignupCheckService;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    invoke-virtual {v0, p1}, Lcom/google/android/music/preferences/MusicPreferences;->setStoreAvailabilityVerified(Z)V

    .line 322
    iget-object v0, p0, Lcom/google/android/music/tutorial/SignupStatus$SignupCheckService;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/music/preferences/MusicPreferences;->setStoreAvailableLastChecked(J)V

    .line 323
    sget-boolean v0, Lcom/google/android/music/utils/DebugUtils;->IS_DEBUG_BUILD:Z

    if-eqz v0, :cond_0

    .line 324
    const-string v0, "SignupStatus"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "store availability: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 326
    :cond_0
    return-void
.end method

.method private onServiceConnectedImp(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 1
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 370
    monitor-enter p0

    .line 371
    :try_start_0
    invoke-static {p2}, Lcom/google/android/finsky/services/IMarketCatalogService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/google/android/finsky/services/IMarketCatalogService;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/tutorial/SignupStatus$SignupCheckService;->mMarketCatalogService:Lcom/google/android/finsky/services/IMarketCatalogService;

    .line 372
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 373
    monitor-exit p0

    .line 374
    return-void

    .line 373
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private onServiceDisconnectedImp(Landroid/content/ComponentName;)V
    .locals 1
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 377
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/tutorial/SignupStatus$SignupCheckService;->mMarketCatalogService:Lcom/google/android/finsky/services/IMarketCatalogService;

    .line 378
    return-void
.end method


# virtual methods
.method public onCreate()V
    .locals 1

    .prologue
    .line 163
    invoke-super {p0}, Landroid/app/IntentService;->onCreate()V

    .line 164
    invoke-static {p0, p0}, Lcom/google/android/music/preferences/MusicPreferences;->getMusicPreferences(Landroid/content/Context;Ljava/lang/Object;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/tutorial/SignupStatus$SignupCheckService;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    .line 165
    new-instance v0, Lcom/google/android/music/net/NetworkMonitorServiceConnection;

    invoke-direct {v0}, Lcom/google/android/music/net/NetworkMonitorServiceConnection;-><init>()V

    iput-object v0, p0, Lcom/google/android/music/tutorial/SignupStatus$SignupCheckService;->mNetworkMonitorServiceConnection:Lcom/google/android/music/net/NetworkMonitorServiceConnection;

    .line 166
    iget-object v0, p0, Lcom/google/android/music/tutorial/SignupStatus$SignupCheckService;->mNetworkMonitorServiceConnection:Lcom/google/android/music/net/NetworkMonitorServiceConnection;

    invoke-virtual {v0, p0}, Lcom/google/android/music/net/NetworkMonitorServiceConnection;->bindToService(Landroid/content/Context;)V

    .line 167
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 171
    invoke-super {p0}, Landroid/app/IntentService;->onDestroy()V

    .line 172
    invoke-static {p0}, Lcom/google/android/music/preferences/MusicPreferences;->releaseMusicPreferences(Ljava/lang/Object;)V

    .line 173
    iget-object v0, p0, Lcom/google/android/music/tutorial/SignupStatus$SignupCheckService;->mNetworkMonitorServiceConnection:Lcom/google/android/music/net/NetworkMonitorServiceConnection;

    invoke-virtual {v0, p0}, Lcom/google/android/music/net/NetworkMonitorServiceConnection;->unbindFromService(Landroid/content/Context;)V

    .line 174
    return-void
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 9
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v8, 0x2

    .line 178
    # invokes: Lcom/google/android/music/tutorial/SignupStatus;->getSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;
    invoke-static {p0}, Lcom/google/android/music/tutorial/SignupStatus;->access$200(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v4

    .line 180
    .local v4, "sharedPreferences":Landroid/content/SharedPreferences;
    iget-object v5, p0, Lcom/google/android/music/tutorial/SignupStatus$SignupCheckService;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    invoke-virtual {v5}, Lcom/google/android/music/preferences/MusicPreferences;->waitForFullyLoaded()Z

    move-result v5

    if-nez v5, :cond_1

    .line 181
    const-string v5, "SignupStatus"

    const-string v6, "MusicPreferences never loaded."

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 230
    :cond_0
    :goto_0
    return-void

    .line 185
    :cond_1
    iget-object v5, p0, Lcom/google/android/music/tutorial/SignupStatus$SignupCheckService;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    invoke-virtual {v5}, Lcom/google/android/music/preferences/MusicPreferences;->validateStreamingAccount()V

    .line 187
    # invokes: Lcom/google/android/music/tutorial/SignupStatus;->isBackgroundDataEnabled(Landroid/content/Context;)Z
    invoke-static {p0}, Lcom/google/android/music/tutorial/SignupStatus;->access$300(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 188
    invoke-direct {p0}, Lcom/google/android/music/tutorial/SignupStatus$SignupCheckService;->enableNetworkReceiver()V

    goto :goto_0

    .line 191
    :cond_2
    iget-object v5, p0, Lcom/google/android/music/tutorial/SignupStatus$SignupCheckService;->mNetworkMonitorServiceConnection:Lcom/google/android/music/net/NetworkMonitorServiceConnection;

    const-wide/16 v6, 0x2710

    invoke-virtual {v5, v6, v7}, Lcom/google/android/music/net/NetworkMonitorServiceConnection;->waitForServiceConnection(J)Z

    move-result v5

    if-nez v5, :cond_3

    .line 192
    const-string v5, "SignupStatus"

    const-string v6, "NetworkMonitor service connection never came up"

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 198
    :cond_3
    :try_start_0
    iget-object v5, p0, Lcom/google/android/music/tutorial/SignupStatus$SignupCheckService;->mNetworkMonitorServiceConnection:Lcom/google/android/music/net/NetworkMonitorServiceConnection;

    invoke-virtual {v5}, Lcom/google/android/music/net/NetworkMonitorServiceConnection;->getNetworkMonitor()Lcom/google/android/music/net/INetworkMonitor;

    move-result-object v3

    .line 200
    .local v3, "networkMonitor":Lcom/google/android/music/net/INetworkMonitor;
    if-eqz v3, :cond_4

    invoke-interface {v3}, Lcom/google/android/music/net/INetworkMonitor;->isConnected()Z

    move-result v5

    if-nez v5, :cond_5

    .line 201
    :cond_4
    const-string v5, "SignupStatus"

    const-string v6, "No connection available, not continuing signup check"

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 202
    invoke-direct {p0}, Lcom/google/android/music/tutorial/SignupStatus$SignupCheckService;->enableNetworkReceiver()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 205
    .end local v3    # "networkMonitor":Lcom/google/android/music/net/INetworkMonitor;
    :catch_0
    move-exception v0

    .line 206
    .local v0, "e":Landroid/os/RemoteException;
    const-string v5, "SignupStatus"

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 210
    .end local v0    # "e":Landroid/os/RemoteException;
    .restart local v3    # "networkMonitor":Lcom/google/android/music/net/INetworkMonitor;
    :cond_5
    invoke-direct {p0}, Lcom/google/android/music/tutorial/SignupStatus$SignupCheckService;->disableNetworkRecevier()V

    .line 212
    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 215
    .local v1, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-direct {p0}, Lcom/google/android/music/tutorial/SignupStatus$SignupCheckService;->checkStoreAvailable()V

    .line 216
    iget-object v5, p0, Lcom/google/android/music/tutorial/SignupStatus$SignupCheckService;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    invoke-virtual {v5}, Lcom/google/android/music/preferences/MusicPreferences;->isStoreAvailablilityVerified()Z

    move-result v2

    .line 217
    .local v2, "isStoreAvailable":Z
    if-eqz v2, :cond_8

    .line 218
    const-string v5, "status"

    invoke-interface {v1, v5, v8}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 222
    :cond_6
    :goto_1
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 225
    invoke-static {p0}, Lcom/google/android/music/xdi/XdiUtils;->isXdiEnvironment(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_7

    invoke-static {p0}, Lcom/google/android/music/leanback/LeanbackUtils;->isLeanbackEnvironment(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 226
    :cond_7
    iget-object v5, p0, Lcom/google/android/music/tutorial/SignupStatus$SignupCheckService;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    invoke-direct {p0, v5}, Lcom/google/android/music/tutorial/SignupStatus$SignupCheckService;->autoSelectAccountWithoutVerification(Lcom/google/android/music/preferences/MusicPreferences;)V

    goto :goto_0

    .line 219
    :cond_8
    # invokes: Lcom/google/android/music/tutorial/SignupStatus;->getVerifiedStatus(Landroid/content/SharedPreferences;)I
    invoke-static {v4}, Lcom/google/android/music/tutorial/SignupStatus;->access$400(Landroid/content/SharedPreferences;)I

    move-result v5

    if-ne v5, v8, :cond_6

    .line 220
    const-string v5, "status"

    invoke-interface {v1, v5}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_1

    .line 227
    :cond_9
    if-eqz v2, :cond_0

    .line 228
    iget-object v5, p0, Lcom/google/android/music/tutorial/SignupStatus$SignupCheckService;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;

    invoke-direct {p0, v4, v5}, Lcom/google/android/music/tutorial/SignupStatus$SignupCheckService;->autoSelectAccountWithVerification(Landroid/content/SharedPreferences;Lcom/google/android/music/preferences/MusicPreferences;)V

    goto/16 :goto_0
.end method
