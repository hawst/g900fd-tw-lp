.class Lcom/google/android/music/xdi/MetaTitleCursor;
.super Landroid/database/MatrixCursor;
.source "MetaTitleCursor.java"


# direct methods
.method constructor <init>(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "title"    # Ljava/lang/String;

    .prologue
    .line 32
    invoke-direct {p0, p2}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 33
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/music/xdi/MetaTitleCursor;->extractDataForTitle(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;)V

    .line 34
    return-void
.end method

.method private extractDataForTitle(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "title"    # Ljava/lang/String;

    .prologue
    .line 37
    new-instance v0, Lcom/google/android/music/xdi/ProjectionMap;

    invoke-direct {v0, p2}, Lcom/google/android/music/xdi/ProjectionMap;-><init>([Ljava/lang/String;)V

    .line 38
    .local v0, "projectionMap":Lcom/google/android/music/xdi/ProjectionMap;
    array-length v2, p2

    new-array v1, v2, [Ljava/lang/Object;

    .line 39
    .local v1, "values":[Ljava/lang/Object;
    const-string v2, "activity_title"

    invoke-virtual {v0, v1, v2, p3}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 40
    const-string v2, "background_image_uri"

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 41
    const-string v2, "badge_uri"

    invoke-static {p1}, Lcom/google/android/music/xdi/XdiUtils;->getDefaultBadgeUri(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 43
    const-string v2, "color_hint"

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c00a6

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 45
    invoke-virtual {p0, v1}, Lcom/google/android/music/xdi/MetaTitleCursor;->addRow([Ljava/lang/Object;)V

    .line 46
    return-void
.end method
