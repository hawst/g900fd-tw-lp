.class public Lcom/google/android/music/preferences/MusicPreferenceService$MusicPreferenceServiceBinder;
.super Landroid/app/Service;
.source "MusicPreferenceService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/preferences/MusicPreferenceService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MusicPreferenceServiceBinder"
.end annotation


# instance fields
.field private mMusicPreferenceService:Landroid/os/Binder;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 164
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 166
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/music/preferences/MusicPreferenceService$MusicPreferenceServiceBinder;->mMusicPreferenceService:Landroid/os/Binder;

    return-void
.end method


# virtual methods
.method public declared-synchronized onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 170
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/music/preferences/MusicPreferenceService$MusicPreferenceServiceBinder;->mMusicPreferenceService:Landroid/os/Binder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 175
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 177
    iget-object v0, p0, Lcom/google/android/music/preferences/MusicPreferenceService$MusicPreferenceServiceBinder;->mMusicPreferenceService:Landroid/os/Binder;

    if-nez v0, :cond_0

    .line 178
    new-instance v0, Lcom/google/android/music/preferences/MusicPreferenceService;

    invoke-direct {v0, p0}, Lcom/google/android/music/preferences/MusicPreferenceService;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/music/preferences/MusicPreferenceService$MusicPreferenceServiceBinder;->mMusicPreferenceService:Landroid/os/Binder;

    .line 180
    :cond_0
    return-void
.end method
