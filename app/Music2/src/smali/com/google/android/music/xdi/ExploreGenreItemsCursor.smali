.class Lcom/google/android/music/xdi/ExploreGenreItemsCursor;
.super Lcom/google/android/music/xdi/XdiCursor;
.source "ExploreGenreItemsCursor.java"


# instance fields
.field private final mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;


# direct methods
.method constructor <init>(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "genreId"    # Ljava/lang/String;
    .param p4, "parentGenreId"    # Ljava/lang/String;

    .prologue
    .line 27
    invoke-static {p1, p3, p4}, Lcom/google/android/music/xdi/ExploreGenreItemsCursor;->getCursorForHeader(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/music/xdi/XdiCursor;-><init>(Landroid/content/Context;[Ljava/lang/String;Landroid/database/Cursor;)V

    .line 28
    new-instance v0, Lcom/google/android/music/xdi/ProjectionMap;

    invoke-direct {v0, p2}, Lcom/google/android/music/xdi/ProjectionMap;-><init>([Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/music/xdi/ExploreGenreItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    .line 29
    return-void
.end method

.method private static getCursorForHeader(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "genreId"    # Ljava/lang/String;
    .param p2, "parentGenreId"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 34
    if-nez p2, :cond_0

    .line 35
    invoke-static {p1}, Lcom/google/android/music/store/MusicContent$Explore;->getGenresUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 39
    .local v1, "queryUri":Landroid/net/Uri;
    :goto_0
    sget-object v2, Lcom/google/android/music/xdi/MusicProjections;->PROJECTION_GENRES:[Ljava/lang/String;

    move-object v0, p0

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0

    .line 37
    .end local v1    # "queryUri":Landroid/net/Uri;
    :cond_0
    invoke-static {p2, p1}, Lcom/google/android/music/store/MusicContent$Explore;->getGenresUriWithSubgenre(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .restart local v1    # "queryUri":Landroid/net/Uri;
    goto :goto_0
.end method


# virtual methods
.method protected extractDataForCurrentRow([Ljava/lang/Object;)Z
    .locals 13
    .param p1, "values"    # [Ljava/lang/Object;

    .prologue
    const/4 v10, 0x0

    const/4 v12, 0x0

    const/4 v11, 0x1

    .line 45
    invoke-virtual {p0}, Lcom/google/android/music/xdi/ExploreGenreItemsCursor;->getWrappedCursor()Landroid/database/Cursor;

    move-result-object v1

    .line 46
    .local v1, "cursor":Landroid/database/Cursor;
    invoke-virtual {p0}, Lcom/google/android/music/xdi/ExploreGenreItemsCursor;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 47
    .local v0, "context":Landroid/content/Context;
    invoke-interface {v1, v10}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 48
    .local v2, "id":J
    invoke-interface {v1, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 49
    .local v7, "subGenreId":Ljava/lang/String;
    const/4 v8, 0x3

    invoke-interface {v1, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 50
    .local v6, "parentGenreId":Ljava/lang/String;
    const/4 v8, 0x2

    invoke-interface {v1, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 52
    .local v5, "name":Ljava/lang/String;
    sget-object v8, Lcom/google/android/music/xdi/XdiContentProvider;->BASE_URI:Landroid/net/Uri;

    invoke-virtual {v8}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v8

    const-string v9, "explore/genre"

    invoke-virtual {v8, v9}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v8

    invoke-virtual {v8, v7}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v8

    invoke-virtual {v8, v5}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v8

    invoke-static {v6}, Lcom/google/android/music/xdi/XdiUtils;->getGenreIdForIntentPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v8

    invoke-virtual {v8}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v8

    invoke-static {v8, v10}, Lcom/google/android/music/xdi/XdiContract;->getBrowseIntent(Landroid/net/Uri;I)Landroid/content/Intent;

    move-result-object v4

    .line 59
    .local v4, "intent":Landroid/content/Intent;
    const-string v8, "meta_uri"

    invoke-static {v5}, Lcom/google/android/music/xdi/XdiUtils;->getMetaTitleUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    invoke-virtual {v9}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 61
    iget-object v8, p0, Lcom/google/android/music/xdi/ExploreGenreItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v9, "_id"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v8, p1, v9, v10}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 62
    iget-object v8, p0, Lcom/google/android/music/xdi/ExploreGenreItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v9, "display_name"

    invoke-virtual {v8, p1, v9, v5}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 63
    iget-object v8, p0, Lcom/google/android/music/xdi/ExploreGenreItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v9, "display_description"

    invoke-virtual {v8, p1, v9, v12}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 64
    iget-object v8, p0, Lcom/google/android/music/xdi/ExploreGenreItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v9, "image_uri"

    invoke-static {v0, v7, v6}, Lcom/google/android/music/xdi/XdiUtils;->getGenreArtUri(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, p1, v9, v10}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 66
    iget-object v8, p0, Lcom/google/android/music/xdi/ExploreGenreItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v9, "width"

    invoke-virtual {v8, p1, v9, v12}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 67
    iget-object v8, p0, Lcom/google/android/music/xdi/ExploreGenreItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v9, "height"

    invoke-virtual {v8, p1, v9, v12}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 68
    iget-object v8, p0, Lcom/google/android/music/xdi/ExploreGenreItemsCursor;->mProjectionMap:Lcom/google/android/music/xdi/ProjectionMap;

    const-string v9, "intent_uri"

    invoke-virtual {v4, v11}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, p1, v9, v10}, Lcom/google/android/music/xdi/ProjectionMap;->writeValueToArray([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 71
    return v11
.end method
