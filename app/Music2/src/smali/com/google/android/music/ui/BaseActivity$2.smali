.class Lcom/google/android/music/ui/BaseActivity$2;
.super Ljava/lang/Object;
.source "BaseActivity.java"

# interfaces
.implements Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerContentClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/BaseActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/BaseActivity;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/BaseActivity;)V
    .locals 0

    .prologue
    .line 186
    iput-object p1, p0, Lcom/google/android/music/ui/BaseActivity$2;->this$0:Lcom/google/android/music/ui/BaseActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAccountListToggleButtonClicked(Z)V
    .locals 0
    .param p1, "isListExpanded"    # Z

    .prologue
    .line 220
    return-void
.end method

.method public onCurrentAccountClicked(ZLcom/google/android/finsky/protos/DocumentV2$DocV2;)Z
    .locals 1
    .param p1, "isLoaded"    # Z
    .param p2, "profileDoc"    # Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .prologue
    .line 190
    const/4 v0, 0x0

    return v0
.end method

.method public onDownloadToggleClicked(Z)V
    .locals 3
    .param p1, "isDownloadOnly"    # Z

    .prologue
    .line 224
    iget-object v1, p0, Lcom/google/android/music/ui/BaseActivity$2;->this$0:Lcom/google/android/music/ui/BaseActivity;

    invoke-virtual {v1}, Lcom/google/android/music/ui/BaseActivity;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    .line 225
    .local v0, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    if-eqz p1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v0, v1}, Lcom/google/android/music/preferences/MusicPreferences;->setDisplayOptions(I)V

    .line 227
    iget-object v1, p0, Lcom/google/android/music/ui/BaseActivity$2;->this$0:Lcom/google/android/music/ui/BaseActivity;

    const/4 v2, -0x2

    invoke-virtual {v1, v2}, Lcom/google/android/music/ui/BaseActivity;->setResult(I)V

    .line 228
    return-void

    .line 225
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onPrimaryActionClicked(Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;)Z
    .locals 1
    .param p1, "primaryAction"    # Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;

    .prologue
    .line 204
    iget-object v0, p1, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;->actionSelectedRunnable:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 205
    const/4 v0, 0x1

    return v0
.end method

.method public onSecondaryAccountClicked(Ljava/lang/String;)Z
    .locals 1
    .param p1, "accountName"    # Ljava/lang/String;

    .prologue
    .line 195
    iget-object v0, p0, Lcom/google/android/music/ui/BaseActivity$2;->this$0:Lcom/google/android/music/ui/BaseActivity;

    # invokes: Lcom/google/android/music/ui/BaseActivity;->showAccountSwitchWarningDialog(Ljava/lang/String;)V
    invoke-static {v0, p1}, Lcom/google/android/music/ui/BaseActivity;->access$100(Lcom/google/android/music/ui/BaseActivity;Ljava/lang/String;)V

    .line 196
    const/4 v0, 0x1

    return v0
.end method

.method public onSecondaryActionClicked(Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;)Z
    .locals 1
    .param p1, "secondaryAction"    # Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;

    .prologue
    .line 211
    iget-object v0, p1, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;->actionSelectedRunnable:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 214
    const/4 v0, 0x1

    return v0
.end method
