.class Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$6$1;
.super Lcom/google/android/music/leanback/SongRow;
.source "LeanbackArtistDetailsActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$6;->bind(Landroid/database/Cursor;)Ljava/lang/Object;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$6;

.field final synthetic val$position:I


# direct methods
.method constructor <init>(Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$6;Landroid/content/Context;Lcom/google/android/music/ui/cardlib/model/Document;ILandroid/content/Intent;Lcom/google/android/music/leanback/Item;ZI)V
    .locals 7
    .param p2, "x0"    # Landroid/content/Context;
    .param p3, "x1"    # Lcom/google/android/music/ui/cardlib/model/Document;
    .param p4, "x2"    # I
    .param p5, "x3"    # Landroid/content/Intent;
    .param p6, "x4"    # Lcom/google/android/music/leanback/Item;
    .param p7, "x5"    # Z

    .prologue
    .line 333
    iput-object p1, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$6$1;->this$1:Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$6;

    iput p8, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$6$1;->val$position:I

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move v3, p4

    move-object v4, p5

    move-object v5, p6

    move v6, p7

    invoke-direct/range {v0 .. v6}, Lcom/google/android/music/leanback/SongRow;-><init>(Landroid/content/Context;Lcom/google/android/music/ui/cardlib/model/Document;ILandroid/content/Intent;Lcom/google/android/music/leanback/Item;Z)V

    return-void
.end method


# virtual methods
.method onClicked()V
    .locals 4

    .prologue
    .line 336
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$6$1;->this$1:Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$6;

    iget-object v1, v1, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$6;->this$0:Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;

    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$6$1;->this$1:Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$6;

    iget-object v2, v2, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$6;->this$0:Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;

    # getter for: Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;->mPlaySongsSongList:Lcom/google/android/music/medialist/SongList;
    invoke-static {v2}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;->access$200(Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;)Lcom/google/android/music/medialist/SongList;

    move-result-object v2

    iget v3, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$6$1;->val$position:I

    # invokes: Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;->getPlaySongsIntent(Lcom/google/android/music/medialist/SongList;I)Landroid/content/Intent;
    invoke-static {v1, v2, v3}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;->access$300(Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;Lcom/google/android/music/medialist/SongList;I)Landroid/content/Intent;

    move-result-object v0

    .line 337
    .local v0, "intent":Landroid/content/Intent;
    if-eqz v0, :cond_0

    .line 338
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$6$1;->this$1:Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$6;

    iget-object v1, v1, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity$6;->this$0:Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;

    invoke-virtual {v1, v0}, Lcom/google/android/music/leanback/LeanbackArtistDetailsActivity;->startActivity(Landroid/content/Intent;)V

    .line 340
    :cond_0
    return-void
.end method
