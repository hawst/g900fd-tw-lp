.class public Lcom/google/android/music/leanback/SongRowPresenter;
.super Landroid/support/v17/leanback/widget/RowPresenter;
.source "SongRowPresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/leanback/SongRowPresenter$Listener;
    }
.end annotation


# instance fields
.field private final mListener:Lcom/google/android/music/leanback/SongRowPresenter$Listener;


# direct methods
.method constructor <init>(Lcom/google/android/music/leanback/SongRowPresenter$Listener;)V
    .locals 1
    .param p1, "listener"    # Lcom/google/android/music/leanback/SongRowPresenter$Listener;

    .prologue
    .line 31
    invoke-direct {p0}, Landroid/support/v17/leanback/widget/RowPresenter;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/google/android/music/leanback/SongRowPresenter;->mListener:Lcom/google/android/music/leanback/SongRowPresenter$Listener;

    .line 33
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/music/leanback/SongRowPresenter;->setHeaderPresenter(Landroid/support/v17/leanback/widget/RowHeaderPresenter;)V

    .line 34
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/music/leanback/SongRowPresenter;->setSelectEffectEnabled(Z)V

    .line 35
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/music/leanback/SongRowPresenter;)Lcom/google/android/music/leanback/SongRowPresenter$Listener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/leanback/SongRowPresenter;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/google/android/music/leanback/SongRowPresenter;->mListener:Lcom/google/android/music/leanback/SongRowPresenter$Listener;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/music/leanback/SongRowPresenter;Landroid/view/View;Landroid/view/View;Landroid/view/View;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/music/leanback/SongRowPresenter;
    .param p1, "x1"    # Landroid/view/View;
    .param p2, "x2"    # Landroid/view/View;
    .param p3, "x3"    # Landroid/view/View;

    .prologue
    .line 22
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/music/leanback/SongRowPresenter;->updateSelector(Landroid/view/View;Landroid/view/View;Landroid/view/View;)V

    return-void
.end method

.method private getTrackNumberString(I)Ljava/lang/String;
    .locals 3
    .param p1, "trackNumber"    # I

    .prologue
    .line 188
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    .line 189
    .local v0, "baseString":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x2

    if-ge v1, v2, :cond_0

    .line 190
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "0"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 192
    :cond_0
    return-object v0
.end method

.method private updateSelector(Landroid/view/View;Landroid/view/View;Landroid/view/View;)V
    .locals 12
    .param p1, "selectorView"    # Landroid/view/View;
    .param p2, "songDetailsView"    # Landroid/view/View;
    .param p3, "songRadioView"    # Landroid/view/View;

    .prologue
    const/4 v5, 0x0

    .line 137
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x10e0000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v6

    .line 139
    .local v6, "animationDuration":I
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f017e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    .line 141
    .local v8, "roundRectRadius":I
    new-instance v7, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v7}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    .line 143
    .local v7, "interpolator":Landroid/view/animation/DecelerateInterpolator;
    invoke-virtual {p2}, Landroid/view/View;->isFocused()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p3}, Landroid/view/View;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 145
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 147
    .local v2, "lp":Landroid/view/ViewGroup$LayoutParams;
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {p2}, Landroid/view/View;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x7f0f017c

    :goto_0
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 153
    .local v3, "targetWidth":I
    invoke-virtual {p2}, Landroid/view/View;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p2}, Landroid/view/View;->getLeft()I

    move-result v0

    sub-int/2addr v0, v8

    int-to-float v9, v0

    .line 159
    .local v9, "targetTranslationX":F
    :goto_1
    invoke-virtual {p1}, Landroid/view/View;->getAlpha()F

    move-result v0

    cmpl-float v0, v0, v5

    if-nez v0, :cond_1

    .line 160
    invoke-virtual {p1, v9}, Landroid/view/View;->setTranslationX(F)V

    .line 161
    iput v3, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 162
    invoke-virtual {p1}, Landroid/view/View;->requestLayout()V

    .line 166
    :cond_1
    iget v0, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    sub-int/2addr v0, v3

    int-to-float v4, v0

    .line 167
    .local v4, "deltaWidth":F
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 168
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v10

    new-instance v0, Lcom/google/android/music/leanback/SongRowPresenter$5;

    move-object v1, p0

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/music/leanback/SongRowPresenter$5;-><init>(Lcom/google/android/music/leanback/SongRowPresenter;Landroid/view/ViewGroup$LayoutParams;IFLandroid/view/View;)V

    invoke-virtual {v10, v0}, Landroid/view/ViewPropertyAnimator;->setUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    int-to-long v10, v6

    invoke-virtual {v0, v10, v11}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 185
    .end local v2    # "lp":Landroid/view/ViewGroup$LayoutParams;
    .end local v3    # "targetWidth":I
    .end local v4    # "deltaWidth":F
    .end local v9    # "targetTranslationX":F
    :goto_2
    return-void

    .line 147
    .restart local v2    # "lp":Landroid/view/ViewGroup$LayoutParams;
    :cond_2
    const v0, 0x7f0f017d

    goto :goto_0

    .line 153
    .restart local v3    # "targetWidth":I
    :cond_3
    invoke-virtual {p3}, Landroid/view/View;->getLeft()I

    move-result v0

    invoke-virtual {p3}, Landroid/view/View;->getWidth()I

    move-result v1

    sub-int/2addr v1, v3

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    int-to-float v9, v0

    goto :goto_1

    .line 181
    .end local v2    # "lp":Landroid/view/ViewGroup$LayoutParams;
    .end local v3    # "targetWidth":I
    :cond_4
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 182
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    int-to-long v10, v6

    invoke-virtual {v0, v10, v11}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    goto :goto_2
.end method


# virtual methods
.method public createRowViewHolder(Landroid/view/ViewGroup;)Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;
    .locals 4
    .param p1, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 39
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "layout_inflater"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 41
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v2, 0x7f04005e

    const/4 v3, 0x0

    invoke-virtual {v0, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 42
    .local v1, "v":Landroid/view/View;
    new-instance v2, Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;

    invoke-direct {v2, v1}, Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;-><init>(Landroid/view/View;)V

    return-object v2
.end method

.method protected onBindRowViewHolder(Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;Ljava/lang/Object;)V
    .locals 22
    .param p1, "viewHolder"    # Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;
    .param p2, "item"    # Ljava/lang/Object;

    .prologue
    .line 47
    invoke-super/range {p0 .. p2}, Landroid/support/v17/leanback/widget/RowPresenter;->onBindRowViewHolder(Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;Ljava/lang/Object;)V

    move-object/from16 v11, p2

    .line 49
    check-cast v11, Lcom/google/android/music/leanback/SongRow;

    .line 51
    .local v11, "songRow":Lcom/google/android/music/leanback/SongRow;
    invoke-virtual {v11}, Lcom/google/android/music/leanback/SongRow;->getDocument()Lcom/google/android/music/ui/cardlib/model/Document;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/music/ui/cardlib/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v12

    .line 52
    .local v12, "title":Ljava/lang/String;
    invoke-virtual {v11}, Lcom/google/android/music/leanback/SongRow;->getDocument()Lcom/google/android/music/ui/cardlib/model/Document;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/music/ui/cardlib/model/Document;->getArtistName()Ljava/lang/String;

    move-result-object v2

    .line 53
    .local v2, "artist":Ljava/lang/String;
    invoke-virtual {v11}, Lcom/google/android/music/leanback/SongRow;->getDocument()Lcom/google/android/music/ui/cardlib/model/Document;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/music/ui/cardlib/model/Document;->getDuration()J

    move-result-wide v18

    const-wide/16 v20, 0x3e8

    div-long v4, v18, v20

    .line 54
    .local v4, "duration":J
    invoke-virtual {v11}, Lcom/google/android/music/leanback/SongRow;->getPosition()I

    move-result v7

    .line 56
    .local v7, "position":I
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;->view:Landroid/view/View;

    move-object/from16 v17, v0

    const v18, 0x7f0e0166

    invoke-virtual/range {v17 .. v18}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v18

    if-nez v7, :cond_1

    const/16 v17, 0x8

    :goto_0
    move-object/from16 v0, v18

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 59
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;->view:Landroid/view/View;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-static {v0, v4, v5}, Lcom/google/android/music/utils/MusicUtils;->makeTimeStringSync(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v6

    .line 61
    .local v6, "durationString":Ljava/lang/String;
    invoke-virtual {v11}, Lcom/google/android/music/leanback/SongRow;->isPlaying()Z

    move-result v17

    if-eqz v17, :cond_2

    .line 62
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;->view:Landroid/view/View;

    move-object/from16 v17, v0

    const v18, 0x7f0e016b

    invoke-virtual/range {v17 .. v18}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v17

    const/16 v18, 0x0

    invoke-virtual/range {v17 .. v18}, Landroid/view/View;->setVisibility(I)V

    .line 63
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;->view:Landroid/view/View;

    move-object/from16 v17, v0

    const v18, 0x7f0e016c

    invoke-virtual/range {v17 .. v18}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v17

    const/16 v18, 0x8

    invoke-virtual/range {v17 .. v18}, Landroid/view/View;->setVisibility(I)V

    .line 70
    :goto_1
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;->view:Landroid/view/View;

    move-object/from16 v17, v0

    const v18, 0x7f0e0167

    invoke-virtual/range {v17 .. v18}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 71
    .local v3, "background":Landroid/view/View;
    invoke-virtual {v11}, Lcom/google/android/music/leanback/SongRow;->isLast()Z

    move-result v17

    if-eqz v17, :cond_3

    .line 72
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;->view:Landroid/view/View;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v17

    const v18, 0x7f0201c7

    invoke-virtual/range {v17 .. v18}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 79
    :goto_2
    sget v17, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v18, 0x15

    move/from16 v0, v17

    move/from16 v1, v18

    if-lt v0, v1, :cond_0

    .line 80
    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v3, v0}, Landroid/view/View;->setClipToOutline(Z)V

    .line 82
    :cond_0
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;->view:Landroid/view/View;

    move-object/from16 v17, v0

    const v18, 0x7f0e016d

    invoke-virtual/range {v17 .. v18}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/TextView;

    .line 83
    .local v16, "trackTitleView":Landroid/widget/TextView;
    move-object/from16 v0, v16

    invoke-virtual {v0, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 85
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;->view:Landroid/view/View;

    move-object/from16 v17, v0

    const v18, 0x7f0e016e

    invoke-virtual/range {v17 .. v18}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/TextView;

    .line 86
    .local v13, "trackArtistView":Landroid/widget/TextView;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v17

    if-nez v17, :cond_4

    .line 87
    invoke-virtual {v13}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v17

    const v18, 0x7f0b0371

    const/16 v19, 0x1

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    aput-object v2, v19, v20

    invoke-virtual/range {v17 .. v19}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 93
    :goto_3
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;->view:Landroid/view/View;

    move-object/from16 v17, v0

    const v18, 0x7f0e016f

    invoke-virtual/range {v17 .. v18}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/TextView;

    .line 94
    .local v14, "trackDurationView":Landroid/widget/TextView;
    invoke-virtual {v14, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 96
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;->view:Landroid/view/View;

    move-object/from16 v17, v0

    const v18, 0x7f0e0168

    invoke-virtual/range {v17 .. v18}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    .line 97
    .local v8, "selectorView":Landroid/view/View;
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;->view:Landroid/view/View;

    move-object/from16 v17, v0

    const v18, 0x7f0e016a

    invoke-virtual/range {v17 .. v18}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    .line 98
    .local v9, "songDetailsView":Landroid/view/View;
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;->view:Landroid/view/View;

    move-object/from16 v17, v0

    const v18, 0x7f0e0170

    invoke-virtual/range {v17 .. v18}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    .line 100
    .local v10, "songRadioView":Landroid/view/View;
    new-instance v17, Lcom/google/android/music/leanback/SongRowPresenter$1;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v11}, Lcom/google/android/music/leanback/SongRowPresenter$1;-><init>(Lcom/google/android/music/leanback/SongRowPresenter;Lcom/google/android/music/leanback/SongRow;)V

    move-object/from16 v0, v17

    invoke-virtual {v9, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 106
    new-instance v17, Lcom/google/android/music/leanback/SongRowPresenter$2;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v11}, Lcom/google/android/music/leanback/SongRowPresenter$2;-><init>(Lcom/google/android/music/leanback/SongRowPresenter;Lcom/google/android/music/leanback/SongRow;)V

    move-object/from16 v0, v17

    invoke-virtual {v10, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 113
    new-instance v17, Lcom/google/android/music/leanback/SongRowPresenter$3;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v8, v9, v10}, Lcom/google/android/music/leanback/SongRowPresenter$3;-><init>(Lcom/google/android/music/leanback/SongRowPresenter;Landroid/view/View;Landroid/view/View;Landroid/view/View;)V

    move-object/from16 v0, v17

    invoke-virtual {v9, v0}, Landroid/view/View;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 119
    new-instance v17, Lcom/google/android/music/leanback/SongRowPresenter$4;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v8, v9, v10}, Lcom/google/android/music/leanback/SongRowPresenter$4;-><init>(Lcom/google/android/music/leanback/SongRowPresenter;Landroid/view/View;Landroid/view/View;Landroid/view/View;)V

    move-object/from16 v0, v17

    invoke-virtual {v10, v0}, Landroid/view/View;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 125
    return-void

    .line 56
    .end local v3    # "background":Landroid/view/View;
    .end local v6    # "durationString":Ljava/lang/String;
    .end local v8    # "selectorView":Landroid/view/View;
    .end local v9    # "songDetailsView":Landroid/view/View;
    .end local v10    # "songRadioView":Landroid/view/View;
    .end local v13    # "trackArtistView":Landroid/widget/TextView;
    .end local v14    # "trackDurationView":Landroid/widget/TextView;
    .end local v16    # "trackTitleView":Landroid/widget/TextView;
    :cond_1
    const/16 v17, 0x0

    goto/16 :goto_0

    .line 65
    .restart local v6    # "durationString":Ljava/lang/String;
    :cond_2
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;->view:Landroid/view/View;

    move-object/from16 v17, v0

    const v18, 0x7f0e016c

    invoke-virtual/range {v17 .. v18}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/widget/TextView;

    .line 66
    .local v15, "trackNumberView":Landroid/widget/TextView;
    add-int/lit8 v17, v7, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/google/android/music/leanback/SongRowPresenter;->getTrackNumberString(I)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 67
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v15, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 68
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;->view:Landroid/view/View;

    move-object/from16 v17, v0

    const v18, 0x7f0e016b

    invoke-virtual/range {v17 .. v18}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v17

    const/16 v18, 0x8

    invoke-virtual/range {v17 .. v18}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1

    .line 75
    .end local v15    # "trackNumberView":Landroid/widget/TextView;
    .restart local v3    # "background":Landroid/view/View;
    :cond_3
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;->view:Landroid/view/View;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v17

    const v18, 0x7f0201c8

    invoke-virtual/range {v17 .. v18}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_2

    .line 90
    .restart local v13    # "trackArtistView":Landroid/widget/TextView;
    .restart local v16    # "trackTitleView":Landroid/widget/TextView;
    :cond_4
    const/16 v17, 0x0

    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3
.end method
