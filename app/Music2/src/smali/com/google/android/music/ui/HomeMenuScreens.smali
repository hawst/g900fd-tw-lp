.class public Lcom/google/android/music/ui/HomeMenuScreens;
.super Ljava/lang/Object;
.source "HomeMenuScreens.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/ui/HomeMenuScreens$1;,
        Lcom/google/android/music/ui/HomeMenuScreens$PlayDrawerScreenShimRunnable;
    }
.end annotation


# static fields
.field public static final FREE_ITEM_SCREENS:[Lcom/google/android/music/ui/HomeActivity$Screen;

.field public static final PAID_ITEM_SCREENS:[Lcom/google/android/music/ui/HomeActivity$Screen;

.field public static final SIDELOADED_ITEM_SCREENS:[Lcom/google/android/music/ui/HomeActivity$Screen;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 22
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/google/android/music/ui/HomeActivity$Screen;

    sget-object v1, Lcom/google/android/music/ui/HomeActivity$Screen;->MAINSTAGE:Lcom/google/android/music/ui/HomeActivity$Screen;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/music/ui/HomeActivity$Screen;->MY_LIBRARY:Lcom/google/android/music/ui/HomeActivity$Screen;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/music/ui/HomeActivity$Screen;->PLAYLISTS:Lcom/google/android/music/ui/HomeActivity$Screen;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/music/ui/HomeActivity$Screen;->RADIO:Lcom/google/android/music/ui/HomeActivity$Screen;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/music/ui/HomeActivity$Screen;->EXPLORE:Lcom/google/android/music/ui/HomeActivity$Screen;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/music/ui/HomeActivity$Screen;->SETTINGS:Lcom/google/android/music/ui/HomeActivity$Screen;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/music/ui/HomeActivity$Screen;->HELP:Lcom/google/android/music/ui/HomeActivity$Screen;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/music/ui/HomeActivity$Screen;->FEEDBACK:Lcom/google/android/music/ui/HomeActivity$Screen;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/ui/HomeMenuScreens;->PAID_ITEM_SCREENS:[Lcom/google/android/music/ui/HomeActivity$Screen;

    .line 33
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/google/android/music/ui/HomeActivity$Screen;

    sget-object v1, Lcom/google/android/music/ui/HomeActivity$Screen;->MAINSTAGE:Lcom/google/android/music/ui/HomeActivity$Screen;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/music/ui/HomeActivity$Screen;->MY_LIBRARY:Lcom/google/android/music/ui/HomeActivity$Screen;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/music/ui/HomeActivity$Screen;->PLAYLISTS:Lcom/google/android/music/ui/HomeActivity$Screen;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/music/ui/HomeActivity$Screen;->INSTANT_MIXES:Lcom/google/android/music/ui/HomeActivity$Screen;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/music/ui/HomeActivity$Screen;->SHOP:Lcom/google/android/music/ui/HomeActivity$Screen;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/music/ui/HomeActivity$Screen;->SETTINGS:Lcom/google/android/music/ui/HomeActivity$Screen;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/music/ui/HomeActivity$Screen;->HELP:Lcom/google/android/music/ui/HomeActivity$Screen;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/music/ui/HomeActivity$Screen;->FEEDBACK:Lcom/google/android/music/ui/HomeActivity$Screen;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/ui/HomeMenuScreens;->FREE_ITEM_SCREENS:[Lcom/google/android/music/ui/HomeActivity$Screen;

    .line 45
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/google/android/music/ui/HomeActivity$Screen;

    sget-object v1, Lcom/google/android/music/ui/HomeActivity$Screen;->MAINSTAGE:Lcom/google/android/music/ui/HomeActivity$Screen;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/music/ui/HomeActivity$Screen;->MY_LIBRARY:Lcom/google/android/music/ui/HomeActivity$Screen;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/music/ui/HomeActivity$Screen;->PLAYLISTS:Lcom/google/android/music/ui/HomeActivity$Screen;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/music/ui/HomeActivity$Screen;->SETTINGS:Lcom/google/android/music/ui/HomeActivity$Screen;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/music/ui/HomeActivity$Screen;->HELP:Lcom/google/android/music/ui/HomeActivity$Screen;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/music/ui/HomeActivity$Screen;->FEEDBACK:Lcom/google/android/music/ui/HomeActivity$Screen;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/ui/HomeMenuScreens;->SIDELOADED_ITEM_SCREENS:[Lcom/google/android/music/ui/HomeActivity$Screen;

    return-void
.end method

.method public static configureDrawer(Lcom/google/android/play/drawer/PlayDrawerLayout;Lcom/google/android/music/ui/BaseActivity;Lcom/google/android/music/ui/HomeActivity$Screen;)V
    .locals 7
    .param p0, "layout"    # Lcom/google/android/play/drawer/PlayDrawerLayout;
    .param p1, "activity"    # Lcom/google/android/music/ui/BaseActivity;
    .param p2, "activeScreen"    # Lcom/google/android/music/ui/HomeActivity$Screen;

    .prologue
    .line 125
    invoke-virtual {p1}, Lcom/google/android/music/ui/BaseActivity;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->getSeletectedAccountForDisplay()Ljava/lang/String;

    move-result-object v1

    .line 127
    .local v1, "account":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 128
    const/4 v0, 0x0

    new-array v2, v0, [Landroid/accounts/Account;

    .line 129
    .local v2, "accounts":[Landroid/accounts/Account;
    const-string v1, ""

    .line 133
    :goto_0
    invoke-static {}, Lcom/google/android/music/ui/HomeMenuScreens;->getMenuScreens()[Lcom/google/android/music/ui/HomeActivity$Screen;

    move-result-object v6

    .line 134
    .local v6, "screens":[Lcom/google/android/music/ui/HomeActivity$Screen;
    invoke-static {p1, v6, p2}, Lcom/google/android/music/ui/HomeMenuScreens;->getPrimaryActions(Lcom/google/android/music/ui/BaseActivity;[Lcom/google/android/music/ui/HomeActivity$Screen;Lcom/google/android/music/ui/HomeActivity$Screen;)Ljava/util/List;

    move-result-object v3

    invoke-static {p1}, Lcom/google/android/music/ui/HomeMenuScreens;->getDownloadSwitchConfig(Lcom/google/android/music/ui/BaseActivity;)Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerDownloadSwitchConfig;

    move-result-object v4

    invoke-static {p1, v6}, Lcom/google/android/music/ui/HomeMenuScreens;->getSecondaryActions(Lcom/google/android/music/ui/BaseActivity;[Lcom/google/android/music/ui/HomeActivity$Screen;)Ljava/util/List;

    move-result-object v5

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/play/drawer/PlayDrawerLayout;->updateContent(Ljava/lang/String;[Landroid/accounts/Account;Ljava/util/List;Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerDownloadSwitchConfig;Ljava/util/List;)V

    .line 138
    return-void

    .line 131
    .end local v2    # "accounts":[Landroid/accounts/Account;
    .end local v6    # "screens":[Lcom/google/android/music/ui/HomeActivity$Screen;
    :cond_0
    invoke-static {p1}, Lcom/google/android/music/utils/MusicUtils;->getAvailableAccounts(Landroid/content/Context;)[Landroid/accounts/Account;

    move-result-object v2

    .restart local v2    # "accounts":[Landroid/accounts/Account;
    goto :goto_0
.end method

.method private static getDownloadSwitchConfig(Lcom/google/android/music/ui/BaseActivity;)Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerDownloadSwitchConfig;
    .locals 8
    .param p0, "parent"    # Lcom/google/android/music/ui/BaseActivity;

    .prologue
    const/4 v0, 0x0

    .line 104
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v6

    .line 107
    .local v6, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    invoke-virtual {v6}, Lcom/google/android/music/preferences/MusicPreferences;->isTvMusic()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 112
    :cond_0
    :goto_0
    return-object v0

    .line 108
    :cond_1
    invoke-virtual {v6}, Lcom/google/android/music/preferences/MusicPreferences;->hasStreamingAccount()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 109
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    .line 110
    .local v7, "res":Landroid/content/res/Resources;
    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->getPreferences()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->getDisplayOptions()I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v5, 0x1

    .line 112
    .local v5, "downloadOnly":Z
    :goto_1
    new-instance v0, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerDownloadSwitchConfig;

    const v1, 0x7f0b008f

    invoke-virtual {v7, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0c007d

    invoke-virtual {v7, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    const v3, 0x7f0201f2

    const v4, 0x7f0201f1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerDownloadSwitchConfig;-><init>(Ljava/lang/String;IIIZ)V

    goto :goto_0

    .line 110
    .end local v5    # "downloadOnly":Z
    :cond_2
    const/4 v5, 0x0

    goto :goto_1
.end method

.method public static getMenuScreens()[Lcom/google/android/music/ui/HomeActivity$Screen;
    .locals 2

    .prologue
    .line 55
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/ui/UIStateManager;->getPrefs()Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v0

    .line 56
    .local v0, "prefs":Lcom/google/android/music/preferences/MusicPreferences;
    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->isNautilusEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 57
    sget-object v1, Lcom/google/android/music/ui/HomeMenuScreens;->PAID_ITEM_SCREENS:[Lcom/google/android/music/ui/HomeActivity$Screen;

    .line 61
    :goto_0
    return-object v1

    .line 58
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/music/preferences/MusicPreferences;->hasStreamingAccount()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 59
    sget-object v1, Lcom/google/android/music/ui/HomeMenuScreens;->FREE_ITEM_SCREENS:[Lcom/google/android/music/ui/HomeActivity$Screen;

    goto :goto_0

    .line 61
    :cond_1
    sget-object v1, Lcom/google/android/music/ui/HomeMenuScreens;->SIDELOADED_ITEM_SCREENS:[Lcom/google/android/music/ui/HomeActivity$Screen;

    goto :goto_0
.end method

.method private static getPrimaryActions(Lcom/google/android/music/ui/BaseActivity;[Lcom/google/android/music/ui/HomeActivity$Screen;Lcom/google/android/music/ui/HomeActivity$Screen;)Ljava/util/List;
    .locals 15
    .param p0, "parent"    # Lcom/google/android/music/ui/BaseActivity;
    .param p1, "screens"    # [Lcom/google/android/music/ui/HomeActivity$Screen;
    .param p2, "activeScreen"    # Lcom/google/android/music/ui/HomeActivity$Screen;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/music/ui/BaseActivity;",
            "[",
            "Lcom/google/android/music/ui/HomeActivity$Screen;",
            "Lcom/google/android/music/ui/HomeActivity$Screen;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;",
            ">;"
        }
    .end annotation

    .prologue
    .line 67
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 69
    .local v9, "actions":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;>;"
    move-object/from16 v10, p1

    .local v10, "arr$":[Lcom/google/android/music/ui/HomeActivity$Screen;
    array-length v12, v10

    .local v12, "len$":I
    const/4 v11, 0x0

    .local v11, "i$":I
    :goto_0
    if-ge v11, v12, :cond_1

    aget-object v13, v10, v11

    .line 70
    .local v13, "screen":Lcom/google/android/music/ui/HomeActivity$Screen;
    invoke-virtual {v13}, Lcom/google/android/music/ui/HomeActivity$Screen;->isPrimary()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 71
    new-instance v1, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;

    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v13, v2}, Lcom/google/android/music/ui/HomeActivity$Screen;->getTitle(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v13}, Lcom/google/android/music/ui/HomeActivity$Screen;->getIconResourceId()I

    move-result v3

    invoke-virtual {v13}, Lcom/google/android/music/ui/HomeActivity$Screen;->getSelectedIconResourceId()I

    move-result v4

    const v5, 0x7f0c013f

    move-object/from16 v0, p2

    invoke-virtual {v13, v0}, Lcom/google/android/music/ui/HomeActivity$Screen;->equals(Ljava/lang/Object;)Z

    move-result v6

    invoke-virtual {v13}, Lcom/google/android/music/ui/HomeActivity$Screen;->isInDownloadOnly()Z

    move-result v7

    new-instance v8, Lcom/google/android/music/ui/HomeMenuScreens$PlayDrawerScreenShimRunnable;

    const/4 v14, 0x0

    invoke-direct {v8, v13, p0, v14}, Lcom/google/android/music/ui/HomeMenuScreens$PlayDrawerScreenShimRunnable;-><init>(Lcom/google/android/music/ui/HomeActivity$Screen;Lcom/google/android/music/ui/BaseActivity;Lcom/google/android/music/ui/HomeMenuScreens$1;)V

    invoke-direct/range {v1 .. v8}, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;-><init>(Ljava/lang/String;IIIZZLjava/lang/Runnable;)V

    .line 80
    .local v1, "action":Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;
    invoke-interface {v9, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 69
    .end local v1    # "action":Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;
    :cond_0
    add-int/lit8 v11, v11, 0x1

    goto :goto_0

    .line 83
    .end local v13    # "screen":Lcom/google/android/music/ui/HomeActivity$Screen;
    :cond_1
    return-object v9
.end method

.method private static getSecondaryActions(Lcom/google/android/music/ui/BaseActivity;[Lcom/google/android/music/ui/HomeActivity$Screen;)Ljava/util/List;
    .locals 9
    .param p0, "parent"    # Lcom/google/android/music/ui/BaseActivity;
    .param p1, "screens"    # [Lcom/google/android/music/ui/HomeActivity$Screen;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/music/ui/BaseActivity;",
            "[",
            "Lcom/google/android/music/ui/HomeActivity$Screen;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;",
            ">;"
        }
    .end annotation

    .prologue
    .line 88
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 90
    .local v1, "actions":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;>;"
    move-object v2, p1

    .local v2, "arr$":[Lcom/google/android/music/ui/HomeActivity$Screen;
    array-length v4, v2

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v5, v2, v3

    .line 91
    .local v5, "screen":Lcom/google/android/music/ui/HomeActivity$Screen;
    invoke-virtual {v5}, Lcom/google/android/music/ui/HomeActivity$Screen;->isSecondary()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 92
    new-instance v0, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;

    invoke-virtual {p0}, Lcom/google/android/music/ui/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/music/ui/HomeActivity$Screen;->getTitle(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Lcom/google/android/music/ui/HomeMenuScreens$PlayDrawerScreenShimRunnable;

    const/4 v8, 0x0

    invoke-direct {v7, v5, p0, v8}, Lcom/google/android/music/ui/HomeMenuScreens$PlayDrawerScreenShimRunnable;-><init>(Lcom/google/android/music/ui/HomeActivity$Screen;Lcom/google/android/music/ui/BaseActivity;Lcom/google/android/music/ui/HomeMenuScreens$1;)V

    invoke-direct {v0, v6, v7}, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;-><init>(Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 96
    .local v0, "action":Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 90
    .end local v0    # "action":Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 99
    .end local v5    # "screen":Lcom/google/android/music/ui/HomeActivity$Screen;
    :cond_1
    return-object v1
.end method
