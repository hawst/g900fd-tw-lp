.class public Lcom/google/android/music/ui/GenreGridFragment;
.super Lcom/google/android/music/ui/MediaListGridFragment;
.source "GenreGridFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/ui/GenreGridFragment$1;,
        Lcom/google/android/music/ui/GenreGridFragment$GenresAdapter;
    }
.end annotation


# static fields
.field public static final PROJECTION:[Ljava/lang/String;


# instance fields
.field private mTileMetadata:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 29
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "name"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "hasLocal"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/ui/GenreGridFragment;->PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/google/android/music/ui/MediaListGridFragment;-><init>()V

    .line 78
    return-void
.end method

.method static synthetic access$200(Lcom/google/android/music/ui/GenreGridFragment;)Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/music/ui/GenreGridFragment;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/music/ui/GenreGridFragment;->mTileMetadata:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    return-object v0
.end method


# virtual methods
.method protected getSupportsFastScroll()Z
    .locals 1

    .prologue
    .line 75
    const/4 v0, 0x1

    return v0
.end method

.method protected initEmptyScreen()V
    .locals 1

    .prologue
    .line 137
    invoke-super {p0}, Lcom/google/android/music/ui/MediaListGridFragment;->initEmptyScreen()V

    .line 138
    invoke-static {}, Lcom/google/android/music/ui/UIStateManager;->getInstance()Lcom/google/android/music/ui/UIStateManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/music/ui/UIStateManager;->isDownloadedOnlyMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 139
    const v0, 0x7f0b02a4

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/GenreGridFragment;->setEmptyScreenText(I)V

    .line 140
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/GenreGridFragment;->setEmptyScreenLearnMoreVisible(Z)V

    .line 146
    :goto_0
    return-void

    .line 142
    :cond_0
    const v0, 0x7f0b02a5

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/GenreGridFragment;->setEmptyScreenText(I)V

    .line 143
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/GenreGridFragment;->setEmptyScreenLearnMoreVisible(Z)V

    goto :goto_0
.end method

.method newAsyncCursorAdapter(Landroid/database/Cursor;)Lcom/google/android/music/ui/MediaListCursorAdapter;
    .locals 2
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    .line 60
    new-instance v0, Lcom/google/android/music/ui/GenreGridFragment$GenresAdapter;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p0, p1, v1}, Lcom/google/android/music/ui/GenreGridFragment$GenresAdapter;-><init>(Lcom/google/android/music/ui/GenreGridFragment;Lcom/google/android/music/ui/GenreGridFragment;Landroid/database/Cursor;Lcom/google/android/music/ui/GenreGridFragment$1;)V

    return-object v0
.end method

.method newLoaderCursorAdapter()Lcom/google/android/music/ui/MediaListCursorAdapter;
    .locals 2

    .prologue
    .line 55
    new-instance v0, Lcom/google/android/music/ui/GenreGridFragment$GenresAdapter;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p0, v1}, Lcom/google/android/music/ui/GenreGridFragment$GenresAdapter;-><init>(Lcom/google/android/music/ui/GenreGridFragment;Lcom/google/android/music/ui/GenreGridFragment;Lcom/google/android/music/ui/GenreGridFragment$1;)V

    return-object v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 43
    invoke-virtual {p0}, Lcom/google/android/music/ui/GenreGridFragment;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 45
    sget-object v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata;->CARD_SMALL_2x1:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    iput-object v0, p0, Lcom/google/android/music/ui/GenreGridFragment;->mTileMetadata:Lcom/google/android/music/ui/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    .line 46
    new-instance v0, Lcom/google/android/music/medialist/AllGenreList;

    invoke-direct {v0}, Lcom/google/android/music/medialist/AllGenreList;-><init>()V

    sget-object v1, Lcom/google/android/music/ui/GenreGridFragment;->PROJECTION:[Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/music/ui/GenreGridFragment;->init(Lcom/google/android/music/medialist/MediaList;[Ljava/lang/String;Z)V

    .line 50
    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/music/ui/MediaListGridFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 51
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 65
    invoke-super {p0, p1, p2}, Lcom/google/android/music/ui/MediaListGridFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 66
    invoke-virtual {p0}, Lcom/google/android/music/ui/GenreGridFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/music/ui/GenreGridFragment;->getSupportsFastScroll()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setFastScrollEnabled(Z)V

    .line 67
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 68
    invoke-virtual {p0}, Lcom/google/android/music/ui/GenreGridFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    const/high16 v1, 0x1000000

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setScrollBarStyle(I)V

    .line 69
    invoke-virtual {p0}, Lcom/google/android/music/ui/GenreGridFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setFastScrollAlwaysVisible(Z)V

    .line 71
    :cond_0
    return-void
.end method
