.class Lcom/google/android/music/ui/TopChartsExploreFragment$1;
.super Ljava/lang/Object;
.source "TopChartsExploreFragment.java"

# interfaces
.implements Lcom/google/android/music/utils/async/AsyncRunner;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/TopChartsExploreFragment;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mArtUrls:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/music/ui/TopChartsExploreFragment;

.field final synthetic val$appContext:Landroid/content/Context;

.field final synthetic val$genreId:Ljava/lang/String;

.field final synthetic val$name:Ljava/lang/String;

.field final synthetic val$parentGenreId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/TopChartsExploreFragment;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 114
    iput-object p1, p0, Lcom/google/android/music/ui/TopChartsExploreFragment$1;->this$0:Lcom/google/android/music/ui/TopChartsExploreFragment;

    iput-object p2, p0, Lcom/google/android/music/ui/TopChartsExploreFragment$1;->val$appContext:Landroid/content/Context;

    iput-object p3, p0, Lcom/google/android/music/ui/TopChartsExploreFragment$1;->val$genreId:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/music/ui/TopChartsExploreFragment$1;->val$parentGenreId:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/music/ui/TopChartsExploreFragment$1;->val$name:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public backgroundTask()V
    .locals 3

    .prologue
    .line 119
    iget-object v0, p0, Lcom/google/android/music/ui/TopChartsExploreFragment$1;->this$0:Lcom/google/android/music/ui/TopChartsExploreFragment;

    invoke-virtual {v0}, Lcom/google/android/music/ui/TopChartsExploreFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 120
    iget-object v0, p0, Lcom/google/android/music/ui/TopChartsExploreFragment$1;->val$appContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/music/ui/TopChartsExploreFragment$1;->val$genreId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/music/ui/TopChartsExploreFragment$1;->val$parentGenreId:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/music/utils/MusicUtils;->getArtUrlsForGenreRadio(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/ui/TopChartsExploreFragment$1;->mArtUrls:Ljava/lang/String;

    .line 123
    :cond_0
    return-void
.end method

.method public taskCompleted()V
    .locals 4

    .prologue
    .line 127
    iget-object v1, p0, Lcom/google/android/music/ui/TopChartsExploreFragment$1;->this$0:Lcom/google/android/music/ui/TopChartsExploreFragment;

    invoke-virtual {v1}, Lcom/google/android/music/ui/TopChartsExploreFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 128
    .local v0, "activity":Landroid/app/Activity;
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/music/ui/TopChartsExploreFragment$1;->mArtUrls:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 129
    iget-object v1, p0, Lcom/google/android/music/ui/TopChartsExploreFragment$1;->val$name:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/music/ui/TopChartsExploreFragment$1;->val$genreId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/music/ui/TopChartsExploreFragment$1;->mArtUrls:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/music/utils/MusicUtils;->playGenreRadio(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    :cond_0
    return-void
.end method
