.class public Lcom/google/android/music/store/PlayList;
.super Lcom/google/android/music/store/Syncable;
.source "PlayList.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/store/PlayList$Item;
    }
.end annotation


# static fields
.field private static final COUNT_COLUMNS:[Ljava/lang/String;

.field private static final FULL_PROJECTION:[Ljava/lang/String;

.field private static final LISTITEMS_MUSICID_COLUMN:[Ljava/lang/String;

.field private static final MUSIC_ID_COLUMN:[Ljava/lang/String;

.field private static final PLAYLIST_TOMBSTONE_PROJECTION:[Ljava/lang/String;

.field private static TOMBSTONE_PROJECTION_INDEX_ID:I

.field private static TOMBSTONE_PROJECTION_SOURCE_ID:I

.field private static TOMBSTONE_PROJECTION_SOURCE_VERSION:I


# instance fields
.field private mArtworkLocation:Ljava/lang/String;

.field private mDescription:Ljava/lang/String;

.field private mEditorArtworkUrl:Ljava/lang/String;

.field private mId:J

.field private mMediaStoreId:J

.field private mName:Ljava/lang/String;

.field private mOwnerName:Ljava/lang/String;

.field private mOwnerProfilePhotoUrl:Ljava/lang/String;

.field private mShareToken:Ljava/lang/String;

.field private mType:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 50
    const/16 v0, 0xf

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "LISTS.Id"

    aput-object v1, v0, v3

    const-string v1, "LISTS.MediaStoreId"

    aput-object v1, v0, v4

    const-string v1, "LISTS.Name"

    aput-object v1, v0, v5

    const-string v1, "LISTS.SourceAccount"

    aput-object v1, v0, v6

    const/4 v1, 0x4

    const-string v2, "LISTS._sync_version"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "LISTS.SourceId"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "LISTS._sync_dirty"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "LISTS.ListType"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "LISTS.ListArtworkLocation"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "LISTS.ShareToken"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "LISTS.OwnerName"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "LISTS.Description"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "LISTS.OwnerProfilePhotoUrl"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "LISTS.NameSort"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "LISTS.EditorArtworkUrl"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/store/PlayList;->FULL_PROJECTION:[Ljava/lang/String;

    .line 84
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "Id"

    aput-object v1, v0, v3

    const-string v1, "SourceId"

    aput-object v1, v0, v4

    const-string v1, "_sync_version"

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/music/store/PlayList;->PLAYLIST_TOMBSTONE_PROJECTION:[Ljava/lang/String;

    .line 90
    sput v3, Lcom/google/android/music/store/PlayList;->TOMBSTONE_PROJECTION_INDEX_ID:I

    .line 91
    sput v4, Lcom/google/android/music/store/PlayList;->TOMBSTONE_PROJECTION_SOURCE_ID:I

    .line 92
    sput v5, Lcom/google/android/music/store/PlayList;->TOMBSTONE_PROJECTION_SOURCE_VERSION:I

    .line 162
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "Id"

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/music/store/PlayList;->MUSIC_ID_COLUMN:[Ljava/lang/String;

    .line 165
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "MusicId"

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/music/store/PlayList;->LISTITEMS_MUSICID_COLUMN:[Ljava/lang/String;

    .line 169
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "count(1)"

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/music/store/PlayList;->COUNT_COLUMNS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 94
    invoke-direct {p0}, Lcom/google/android/music/store/Syncable;-><init>()V

    .line 31
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/music/store/PlayList;->mType:I

    .line 95
    return-void
.end method

.method static synthetic access$400()I
    .locals 1

    .prologue
    .line 26
    sget v0, Lcom/google/android/music/store/PlayList;->TOMBSTONE_PROJECTION_SOURCE_ID:I

    return v0
.end method

.method static synthetic access$500()I
    .locals 1

    .prologue
    .line 26
    sget v0, Lcom/google/android/music/store/PlayList;->TOMBSTONE_PROJECTION_SOURCE_VERSION:I

    return v0
.end method

.method static appendAlbum(Landroid/database/sqlite/SQLiteDatabase;JJ)I
    .locals 9
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "playlistId"    # J
    .param p3, "albumId"    # J

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    const/4 v0, 0x1

    .line 1145
    const-string v1, "MUSIC"

    sget-object v2, Lcom/google/android/music/store/PlayList;->MUSIC_ID_COLUMN:[Ljava/lang/String;

    const-string v3, "MUSIC.AlbumId=?"

    new-array v4, v0, [Ljava/lang/String;

    invoke-static {p3, p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    const-string v5, "SongId"

    const-string v7, "DiscNumber, TrackNumber, CanonicalName"

    move-object v0, p0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4

    .line 1156
    .local v4, "c":Landroid/database/Cursor;
    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x1

    move-object v1, p0

    move-wide v2, p1

    :try_start_0
    invoke-static/range {v1 .. v8}, Lcom/google/android/music/store/PlayList;->appendItems(Landroid/database/sqlite/SQLiteDatabase;JLandroid/database/Cursor;ZLjava/lang/Integer;ZZ)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 1159
    invoke-static {v4}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    return v0

    :catchall_0
    move-exception v0

    invoke-static {v4}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method static appendArtist(Landroid/database/sqlite/SQLiteDatabase;JJ)I
    .locals 11
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "playlistId"    # J
    .param p3, "artistId"    # J

    .prologue
    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v5, 0x1

    .line 1173
    invoke-static {p3, p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v9

    .line 1174
    .local v9, "artistIdString":Ljava/lang/String;
    const-string v1, "MUSIC"

    sget-object v2, Lcom/google/android/music/store/PlayList;->MUSIC_ID_COLUMN:[Ljava/lang/String;

    const-string v3, "AlbumArtistId=? OR ArtistId=?"

    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    aput-object v9, v4, v7

    aput-object v9, v4, v5

    const-string v5, "SongId"

    const-string v7, "CanonicalName"

    move-object v0, p0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4

    .line 1185
    .local v4, "c":Landroid/database/Cursor;
    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x1

    move-object v1, p0

    move-wide v2, p1

    :try_start_0
    invoke-static/range {v1 .. v8}, Lcom/google/android/music/store/PlayList;->appendItems(Landroid/database/sqlite/SQLiteDatabase;JLandroid/database/Cursor;ZLjava/lang/Integer;ZZ)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 1187
    invoke-static {v4}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    return v0

    :catchall_0
    move-exception v0

    invoke-static {v4}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method static appendGenre(Landroid/database/sqlite/SQLiteDatabase;JJ)I
    .locals 9
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "playlistId"    # J
    .param p3, "genreId"    # J

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    const/4 v0, 0x1

    .line 1200
    const-string v1, "MUSIC"

    sget-object v2, Lcom/google/android/music/store/PlayList;->MUSIC_ID_COLUMN:[Ljava/lang/String;

    const-string v3, "GenreId=?"

    new-array v4, v0, [Ljava/lang/String;

    invoke-static {p3, p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    const-string v5, "SongId"

    const-string v7, "CanonicalName"

    move-object v0, p0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4

    .line 1211
    .local v4, "c":Landroid/database/Cursor;
    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x1

    move-object v1, p0

    move-wide v2, p1

    :try_start_0
    invoke-static/range {v1 .. v8}, Lcom/google/android/music/store/PlayList;->appendItems(Landroid/database/sqlite/SQLiteDatabase;JLandroid/database/Cursor;ZLjava/lang/Integer;ZZ)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 1213
    invoke-static {v4}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    return v0

    :catchall_0
    move-exception v0

    invoke-static {v4}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method static appendGenre(Landroid/database/sqlite/SQLiteDatabase;JJJ)I
    .locals 9
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "playlistId"    # J
    .param p3, "genreId"    # J
    .param p5, "albumId"    # J

    .prologue
    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v5, 0x1

    .line 1225
    const-string v1, "MUSIC"

    sget-object v2, Lcom/google/android/music/store/PlayList;->MUSIC_ID_COLUMN:[Ljava/lang/String;

    const-string v3, "GenreId=? AND AlbumId=?"

    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    invoke-static {p3, p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v7

    invoke-static {p5, p6}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    const-string v5, "SongId"

    const-string v7, "DiscNumber, TrackNumber, CanonicalName"

    move-object v0, p0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4

    .line 1236
    .local v4, "c":Landroid/database/Cursor;
    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x1

    move-object v1, p0

    move-wide v2, p1

    :try_start_0
    invoke-static/range {v1 .. v8}, Lcom/google/android/music/store/PlayList;->appendItems(Landroid/database/sqlite/SQLiteDatabase;JLandroid/database/Cursor;ZLjava/lang/Integer;ZZ)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 1238
    invoke-static {v4}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    return v0

    :catchall_0
    move-exception v0

    invoke-static {v4}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method static appendItem(Landroid/database/sqlite/SQLiteDatabase;JJ)J
    .locals 7
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "playlistId"    # J
    .param p3, "musicId"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 1135
    const/4 v6, 0x1

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    invoke-static/range {v1 .. v6}, Lcom/google/android/music/store/PlayList;->appendItem(Landroid/database/sqlite/SQLiteDatabase;JJZ)J

    move-result-wide v0

    return-wide v0
.end method

.method static appendItem(Landroid/database/sqlite/SQLiteDatabase;JJZ)J
    .locals 11
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "playlistId"    # J
    .param p3, "musicId"    # J
    .param p5, "canResetPositions"    # Z

    .prologue
    .line 1093
    invoke-static {p0, p1, p2}, Lcom/google/android/music/store/PlayList;->getItemCount(Landroid/database/sqlite/SQLiteDatabase;J)I

    move-result v1

    const/16 v2, 0x3e8

    if-lt v1, v2, :cond_0

    .line 1094
    const-wide/16 v2, 0x0

    .line 1124
    :goto_0
    return-wide v2

    .line 1097
    :cond_0
    new-instance v7, Lcom/google/android/music/store/PlayList$Item;

    invoke-direct {v7}, Lcom/google/android/music/store/PlayList$Item;-><init>()V

    .line 1098
    .local v7, "newItem":Lcom/google/android/music/store/PlayList$Item;
    invoke-virtual {v7, p3, p4}, Lcom/google/android/music/store/PlayList$Item;->setMusicId(J)V

    .line 1099
    invoke-virtual {v7, p1, p2}, Lcom/google/android/music/store/PlayList$Item;->setListId(J)V

    .line 1100
    const/4 v1, 0x1

    invoke-virtual {v7, v1}, Lcom/google/android/music/store/PlayList$Item;->setNeedsSync(Z)V

    .line 1102
    const/4 v8, 0x0

    .line 1103
    .local v8, "newPosition":I
    invoke-static {p0, p1, p2}, Lcom/google/android/music/store/PlayList;->getLastItemPosition(Landroid/database/sqlite/SQLiteDatabase;J)Landroid/util/Pair;

    move-result-object v0

    .line 1104
    .local v0, "lastPosition":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Integer;>;"
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v7, v1}, Lcom/google/android/music/store/PlayList$Item;->setServerPosition(Ljava/lang/String;)V

    .line 1105
    iget-object v1, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/high16 v2, -0x80000000

    if-le v1, v2, :cond_2

    .line 1106
    iget-object v1, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/high16 v2, 0x20000

    add-int v8, v1, v2

    .line 1108
    iget-object v1, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-gt v8, v1, :cond_2

    .line 1109
    if-eqz p5, :cond_1

    .line 1110
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-static {p0, p1, p2, v1}, Lcom/google/android/music/store/PlayList;->resetClientPositions(Landroid/database/sqlite/SQLiteDatabase;JLjava/lang/String;)V

    .line 1111
    const/4 v6, 0x0

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    invoke-static/range {v1 .. v6}, Lcom/google/android/music/store/PlayList;->appendItem(Landroid/database/sqlite/SQLiteDatabase;JJZ)J

    move-result-wide v2

    goto :goto_0

    .line 1113
    :cond_1
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to append item to playlist "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1118
    :cond_2
    int-to-long v2, v8

    invoke-virtual {v7, v2, v3}, Lcom/google/android/music/store/PlayList$Item;->setClientPosition(J)V

    .line 1120
    invoke-static {p0}, Lcom/google/android/music/store/PlayList$Item;->compileItemInsertStatement(Landroid/database/sqlite/SQLiteDatabase;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v9

    .line 1122
    .local v9, "statement":Landroid/database/sqlite/SQLiteStatement;
    :try_start_0
    invoke-virtual {v7, v9}, Lcom/google/android/music/store/PlayList$Item;->insertItem(Landroid/database/sqlite/SQLiteStatement;)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v2

    .line 1124
    invoke-static {v9}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/sqlite/SQLiteProgram;)V

    goto :goto_0

    :catchall_0
    move-exception v1

    invoke-static {v9}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/sqlite/SQLiteProgram;)V

    throw v1
.end method

.method static appendItems(Landroid/database/sqlite/SQLiteDatabase;JLandroid/database/Cursor;IZLjava/lang/Integer;ZZZJ)I
    .locals 27
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "playlistId"    # J
    .param p3, "musicCursor"    # Landroid/database/Cursor;
    .param p4, "maxToAdd"    # I
    .param p5, "canResetPositions"    # Z
    .param p6, "sourceAccount"    # Ljava/lang/Integer;
    .param p7, "yieldIfCongested"    # Z
    .param p8, "allOrNone"    # Z
    .param p9, "sync"    # Z
    .param p10, "playGroupId"    # J

    .prologue
    .line 1307
    if-nez p3, :cond_0

    .line 1308
    const/4 v8, 0x0

    .line 1379
    :goto_0
    return v8

    .line 1311
    :cond_0
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getCount()I

    move-result v4

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getPosition()I

    move-result v5

    sub-int/2addr v4, v5

    add-int/lit8 v8, v4, -0x1

    .line 1312
    .local v8, "toAdd":I
    move/from16 v0, p4

    if-le v8, v0, :cond_1

    .line 1313
    move/from16 v8, p4

    .line 1316
    :cond_1
    const/4 v4, 0x1

    if-ge v8, v4, :cond_2

    .line 1317
    const/4 v8, 0x0

    goto :goto_0

    .line 1320
    :cond_2
    invoke-static/range {p0 .. p2}, Lcom/google/android/music/store/PlayList;->getItemCount(Landroid/database/sqlite/SQLiteDatabase;J)I

    move-result v4

    rsub-int v0, v4, 0x3e8

    move/from16 v25, v0

    .line 1321
    .local v25, "spaceAvailable":I
    move/from16 v0, v25

    if-ge v0, v8, :cond_5

    .line 1322
    if-nez p8, :cond_3

    if-nez v25, :cond_4

    .line 1323
    :cond_3
    const/high16 v8, -0x80000000

    goto :goto_0

    .line 1325
    :cond_4
    move/from16 v8, v25

    .line 1328
    :cond_5
    invoke-static/range {p0 .. p2}, Lcom/google/android/music/store/PlayList;->getLastItemPosition(Landroid/database/sqlite/SQLiteDatabase;J)Landroid/util/Pair;

    move-result-object v20

    .line 1329
    .local v20, "lastPosition":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Integer;>;"
    move-object/from16 v0, v20

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    move-object/from16 v24, v0

    check-cast v24, Ljava/lang/String;

    .line 1330
    .local v24, "serverPosition":Ljava/lang/String;
    move-object/from16 v0, v20

    iget-object v4, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v19

    .line 1334
    .local v19, "lastClientPosition":I
    if-lez v19, :cond_7

    const v4, 0x7fffffff

    sub-int v4, v4, v19

    const/high16 v5, 0x20000

    div-int/2addr v4, v5

    add-int/lit8 v5, v8, -0x1

    if-ge v4, v5, :cond_7

    .line 1338
    if-eqz p5, :cond_6

    .line 1339
    move-object/from16 v0, p0

    move-wide/from16 v1, p1

    move-object/from16 v3, v24

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/music/store/PlayList;->resetClientPositions(Landroid/database/sqlite/SQLiteDatabase;JLjava/lang/String;)V

    .line 1340
    const/4 v9, 0x0

    move-object/from16 v4, p0

    move-wide/from16 v5, p1

    move-object/from16 v7, p3

    move-object/from16 v10, p6

    move/from16 v11, p7

    move/from16 v12, p8

    move/from16 v13, p9

    move-wide/from16 v14, p10

    invoke-static/range {v4 .. v15}, Lcom/google/android/music/store/PlayList;->appendItems(Landroid/database/sqlite/SQLiteDatabase;JLandroid/database/Cursor;IZLjava/lang/Integer;ZZZJ)I

    move-result v8

    goto :goto_0

    .line 1343
    :cond_6
    new-instance v4, Ljava/lang/RuntimeException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed to add "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " items to playlist "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, p1

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 1349
    :cond_7
    invoke-static/range {p0 .. p0}, Lcom/google/android/music/store/PlayList$Item;->compileItemInsertStatement(Landroid/database/sqlite/SQLiteDatabase;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v18

    .line 1351
    .local v18, "insert":Landroid/database/sqlite/SQLiteStatement;
    const/high16 v4, 0x20000

    add-int v16, v19, v4

    .line 1352
    .local v16, "clientPosition":I
    :try_start_0
    new-instance v21, Lcom/google/android/music/store/PlayList$Item;

    invoke-direct/range {v21 .. v21}, Lcom/google/android/music/store/PlayList$Item;-><init>()V

    .line 1353
    .local v21, "newItem":Lcom/google/android/music/store/PlayList$Item;
    const/16 v17, 0x0

    .line 1354
    .local v17, "count":I
    :cond_8
    :goto_1
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_9

    .line 1355
    const/4 v4, 0x0

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v22

    .line 1357
    .local v22, "musicId":J
    invoke-virtual/range {v21 .. v21}, Lcom/google/android/music/store/PlayList$Item;->reset()V

    .line 1358
    invoke-virtual/range {v21 .. v23}, Lcom/google/android/music/store/PlayList$Item;->setMusicId(J)V

    .line 1359
    move-object/from16 v0, v21

    move-wide/from16 v1, p1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/PlayList$Item;->setListId(J)V

    .line 1360
    move-object/from16 v0, v21

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/google/android/music/store/PlayList$Item;->setServerPosition(Ljava/lang/String;)V

    .line 1361
    move/from16 v0, v16

    int-to-long v4, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v4, v5}, Lcom/google/android/music/store/PlayList$Item;->setClientPosition(J)V

    .line 1362
    move-object/from16 v0, v21

    move-wide/from16 v1, p10

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/store/PlayList$Item;->setPlayGroupId(J)V

    .line 1363
    move-object/from16 v0, v21

    move/from16 v1, p9

    invoke-virtual {v0, v1}, Lcom/google/android/music/store/PlayList$Item;->setNeedsSync(Z)V

    .line 1364
    move-object/from16 v0, v21

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/google/android/music/store/PlayList$Item;->insertItem(Landroid/database/sqlite/SQLiteStatement;)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1366
    const/high16 v4, 0x20000

    add-int v16, v16, v4

    .line 1367
    add-int/lit8 v17, v17, 0x1

    .line 1368
    move/from16 v0, v17

    if-lt v0, v8, :cond_a

    .line 1376
    .end local v22    # "musicId":J
    :cond_9
    invoke-static/range {v18 .. v18}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/sqlite/SQLiteProgram;)V

    goto/16 :goto_0

    .line 1371
    .restart local v22    # "musicId":J
    :cond_a
    if-eqz p7, :cond_8

    :try_start_1
    move/from16 v0, v17

    rem-int/lit16 v4, v0, 0x200

    if-nez v4, :cond_8

    .line 1372
    const-wide/16 v4, 0xc8

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->yieldIfContendedSafely(J)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 1376
    .end local v17    # "count":I
    .end local v21    # "newItem":Lcom/google/android/music/store/PlayList$Item;
    .end local v22    # "musicId":J
    :catchall_0
    move-exception v4

    invoke-static/range {v18 .. v18}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/sqlite/SQLiteProgram;)V

    throw v4
.end method

.method static appendItems(Landroid/database/sqlite/SQLiteDatabase;JLandroid/database/Cursor;ZLjava/lang/Integer;ZZ)I
    .locals 13
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "playlistId"    # J
    .param p3, "musicCursor"    # Landroid/database/Cursor;
    .param p4, "canResetPositions"    # Z
    .param p5, "sourceAccount"    # Ljava/lang/Integer;
    .param p6, "yieldIfCongested"    # Z
    .param p7, "allOrNone"    # Z

    .prologue
    .line 1271
    const v4, 0x7fffffff

    const/4 v9, 0x1

    const-wide/16 v10, 0x0

    move-object v0, p0

    move-wide v1, p1

    move-object/from16 v3, p3

    move/from16 v5, p4

    move-object/from16 v6, p5

    move/from16 v7, p6

    move/from16 v8, p7

    invoke-static/range {v0 .. v11}, Lcom/google/android/music/store/PlayList;->appendItems(Landroid/database/sqlite/SQLiteDatabase;JLandroid/database/Cursor;IZLjava/lang/Integer;ZZZJ)I

    move-result v0

    return v0
.end method

.method static appendItemsToSharedPlaylist(Landroid/database/sqlite/SQLiteDatabase;JIILjava/util/List;Ljava/util/List;)Z
    .locals 15
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "playlistId"    # J
    .param p3, "firstPosition"    # I
    .param p4, "sourceAccount"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "JII",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1433
    .local p5, "musicIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    .local p6, "entries":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;>;"
    const/4 v6, 0x0

    .line 1434
    .local v6, "isDatabaseUpdated":Z
    invoke-static {p0}, Lcom/google/android/music/store/PlayList$Item;->compileItemInsertStatement(Landroid/database/sqlite/SQLiteDatabase;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v5

    .line 1436
    .local v5, "insert":Landroid/database/sqlite/SQLiteStatement;
    if-eqz p5, :cond_0

    if-eqz p6, :cond_0

    :try_start_0
    invoke-interface/range {p5 .. p5}, Ljava/util/List;->size()I

    move-result v10

    invoke-interface/range {p6 .. p6}, Ljava/util/List;->size()I

    move-result v11

    if-eq v10, v11, :cond_1

    .line 1437
    :cond_0
    const-string v10, "PlayList"

    const-string v11, "Invalid collections musicIds and entries"

    invoke-static {v10, v11}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1438
    const/4 v10, 0x0

    .line 1459
    invoke-static {v5}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/sqlite/SQLiteProgram;)V

    .line 1461
    :goto_0
    return v10

    .line 1440
    :cond_1
    move/from16 v2, p3

    .line 1441
    .local v2, "clientPosition":I
    :try_start_1
    new-instance v7, Lcom/google/android/music/store/PlayList$Item;

    invoke-direct {v7}, Lcom/google/android/music/store/PlayList$Item;-><init>()V

    .line 1442
    .local v7, "newItem":Lcom/google/android/music/store/PlayList$Item;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    invoke-interface/range {p5 .. p5}, Ljava/util/List;->size()I

    move-result v10

    if-ge v4, v10, :cond_2

    .line 1443
    move-object/from16 v0, p5

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Long;

    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    .line 1444
    .local v8, "musicId":J
    move-object/from16 v0, p6

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;

    .line 1445
    .local v3, "entry":Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;
    invoke-virtual {v7}, Lcom/google/android/music/store/PlayList$Item;->reset()V

    .line 1446
    move/from16 v0, p4

    invoke-virtual {v7, v0}, Lcom/google/android/music/store/PlayList$Item;->setSourceAccount(I)V

    .line 1447
    invoke-virtual {v7, v8, v9}, Lcom/google/android/music/store/PlayList$Item;->setMusicId(J)V

    .line 1448
    move-wide/from16 v0, p1

    invoke-virtual {v7, v0, v1}, Lcom/google/android/music/store/PlayList$Item;->setListId(J)V

    .line 1449
    int-to-long v10, v2

    invoke-virtual {v7, v10, v11}, Lcom/google/android/music/store/PlayList$Item;->setClientPosition(J)V

    .line 1450
    iget-object v10, v3, Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;->mAbsolutePosition:Ljava/lang/String;

    invoke-virtual {v7, v10}, Lcom/google/android/music/store/PlayList$Item;->setServerPosition(Ljava/lang/String;)V

    .line 1451
    const/4 v10, 0x0

    invoke-virtual {v7, v10}, Lcom/google/android/music/store/PlayList$Item;->setNeedsSync(Z)V

    .line 1453
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    const-wide/16 v12, 0x3e8

    mul-long/2addr v10, v12

    invoke-static {v10, v11}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v10

    iput-object v10, v7, Lcom/google/android/music/store/PlayList$Item;->mSourceVersion:Ljava/lang/String;

    .line 1454
    invoke-virtual {v7, v5}, Lcom/google/android/music/store/PlayList$Item;->insertItem(Landroid/database/sqlite/SQLiteStatement;)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1455
    const/4 v6, 0x1

    .line 1456
    add-int/lit8 v2, v2, 0x1

    .line 1442
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 1459
    .end local v3    # "entry":Lcom/google/android/music/sync/google/model/SyncablePlaylistEntry;
    .end local v8    # "musicId":J
    :cond_2
    invoke-static {v5}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/sqlite/SQLiteProgram;)V

    move v10, v6

    .line 1461
    goto :goto_0

    .line 1459
    .end local v2    # "clientPosition":I
    .end local v4    # "i":I
    .end local v7    # "newItem":Lcom/google/android/music/store/PlayList$Item;
    :catchall_0
    move-exception v10

    invoke-static {v5}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/sqlite/SQLiteProgram;)V

    throw v10
.end method

.method static appendItemsToTransientPlaylist(Landroid/database/sqlite/SQLiteDatabase;JIILjava/util/List;)V
    .locals 9
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "playlistId"    # J
    .param p3, "firstPosition"    # I
    .param p4, "sourceAccount"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "JII",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1395
    .local p5, "musicIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    invoke-static {p0}, Lcom/google/android/music/store/PlayList$Item;->compileItemInsertStatement(Landroid/database/sqlite/SQLiteDatabase;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v2

    .line 1401
    .local v2, "insert":Landroid/database/sqlite/SQLiteStatement;
    move v0, p3

    .line 1402
    .local v0, "clientPosition":I
    :try_start_0
    new-instance v3, Lcom/google/android/music/store/PlayList$Item;

    invoke-direct {v3}, Lcom/google/android/music/store/PlayList$Item;-><init>()V

    .line 1403
    .local v3, "newItem":Lcom/google/android/music/store/PlayList$Item;
    invoke-interface {p5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 1404
    .local v4, "musicId":J
    invoke-virtual {v3}, Lcom/google/android/music/store/PlayList$Item;->reset()V

    .line 1405
    invoke-virtual {v3, p4}, Lcom/google/android/music/store/PlayList$Item;->setSourceAccount(I)V

    .line 1406
    invoke-virtual {v3, v4, v5}, Lcom/google/android/music/store/PlayList$Item;->setMusicId(J)V

    .line 1407
    invoke-virtual {v3, p1, p2}, Lcom/google/android/music/store/PlayList$Item;->setListId(J)V

    .line 1408
    int-to-long v6, v0

    invoke-virtual {v3, v6, v7}, Lcom/google/android/music/store/PlayList$Item;->setClientPosition(J)V

    .line 1409
    const/4 v6, 0x0

    invoke-virtual {v3, v6}, Lcom/google/android/music/store/PlayList$Item;->setNeedsSync(Z)V

    .line 1410
    invoke-virtual {v3, v2}, Lcom/google/android/music/store/PlayList$Item;->insertItem(Landroid/database/sqlite/SQLiteStatement;)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1412
    add-int/lit8 v0, v0, 0x1

    .line 1413
    goto :goto_0

    .line 1415
    .end local v4    # "musicId":J
    :cond_0
    invoke-static {v2}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/sqlite/SQLiteProgram;)V

    .line 1417
    return-void

    .line 1415
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v3    # "newItem":Lcom/google/android/music/store/PlayList$Item;
    :catchall_0
    move-exception v6

    invoke-static {v2}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/sqlite/SQLiteProgram;)V

    throw v6
.end method

.method static appendPlaylist(Landroid/database/sqlite/SQLiteDatabase;JJ)I
    .locals 9
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "playlistId"    # J
    .param p3, "sourcePlaylistId"    # J

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    const/4 v0, 0x1

    .line 1250
    const-string v1, "LISTITEMS"

    sget-object v2, Lcom/google/android/music/store/PlayList;->LISTITEMS_MUSICID_COLUMN:[Ljava/lang/String;

    const-string v3, "ListId=?"

    new-array v4, v0, [Ljava/lang/String;

    invoke-static {p3, p4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v6

    const-string v7, "ServerOrder, ClientPosition"

    move-object v0, p0

    move-object v6, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4

    .line 1261
    .local v4, "c":Landroid/database/Cursor;
    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x1

    move-object v1, p0

    move-wide v2, p1

    :try_start_0
    invoke-static/range {v1 .. v8}, Lcom/google/android/music/store/PlayList;->appendItems(Landroid/database/sqlite/SQLiteDatabase;JLandroid/database/Cursor;ZLjava/lang/Integer;ZZ)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 1263
    invoke-static {v4}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    return v0

    :catchall_0
    move-exception v0

    invoke-static {v4}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method public static compilePlayListInsertStatement(Landroid/database/sqlite/SQLiteDatabase;)Landroid/database/sqlite/SQLiteStatement;
    .locals 1
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 179
    const-string v0, "insert into LISTS ( MediaStoreId, Name, _sync_version, _sync_dirty, SourceAccount, SourceId, ListType, ListArtworkLocation, ShareToken, OwnerName, Description,OwnerProfilePhotoUrl,NameSort,EditorArtworkUrl) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v0

    return-object v0
.end method

.method public static compilePlayListUpdateStatement(Landroid/database/sqlite/SQLiteDatabase;)Landroid/database/sqlite/SQLiteStatement;
    .locals 1
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 186
    const-string v0, "update LISTS set MediaStoreId=?, Name=?, _sync_version=?, _sync_dirty=?, SourceAccount=?, SourceId=?, ListType=?, ListArtworkLocation=?, ShareToken=?, OwnerName=?, Description=?, OwnerProfilePhotoUrl=?, NameSort=?, EditorArtworkUrl=?  where Id=?"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v0

    return-object v0
.end method

.method public static compilePlaylistDeleteStatement(Landroid/database/sqlite/SQLiteDatabase;)Landroid/database/sqlite/SQLiteStatement;
    .locals 1
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 193
    const-string v0, "delete from LISTS where SourceAccount=? AND SourceId=?"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v0

    return-object v0
.end method

.method private static copySideloadedPlaylistItems(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/Collection;)V
    .locals 14
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 813
    .local p1, "ids":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Long;>;"
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .local v9, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    .line 814
    .local v12, "syncedListId":J
    const-string v1, "LISTITEMS JOIN MUSIC ON (LISTITEMS.MusicId=MUSIC.Id) "

    sget-object v2, Lcom/google/android/music/store/PlayList;->LISTITEMS_MUSICID_COLUMN:[Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ListId=? AND MUSIC.SourceAccount="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v3, Lcom/google/android/music/store/MusicFile;->MEDIA_STORE_SOURCE_ACCOUNT_AS_STRING:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static {v12, v13}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string v7, "ServerOrder, ClientPosition"

    move-object v0, p0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4

    .line 824
    .local v4, "itemCursor":Landroid/database/Cursor;
    if-eqz v4, :cond_0

    :try_start_0
    invoke-interface {v4}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 827
    const/4 v0, 0x0

    invoke-static {p0, v12, v13, v0}, Lcom/google/android/music/store/PlayList;->readPlayList(Landroid/database/sqlite/SQLiteDatabase;JLcom/google/android/music/store/PlayList;)Lcom/google/android/music/store/PlayList;

    move-result-object v11

    .line 828
    .local v11, "syncedPlaylist":Lcom/google/android/music/store/PlayList;
    invoke-virtual {v11}, Lcom/google/android/music/store/PlayList;->getName()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/google/android/music/store/PlayList;->createPlayList(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/music/store/PlayList;

    move-result-object v10

    .line 830
    .local v10, "newList":Lcom/google/android/music/store/PlayList;
    invoke-virtual {v10}, Lcom/google/android/music/store/PlayList;->getId()J

    move-result-wide v2

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v1, p0

    invoke-static/range {v1 .. v8}, Lcom/google/android/music/store/PlayList;->appendItems(Landroid/database/sqlite/SQLiteDatabase;JLandroid/database/Cursor;ZLjava/lang/Integer;ZZ)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 834
    .end local v10    # "newList":Lcom/google/android/music/store/PlayList;
    .end local v11    # "syncedPlaylist":Lcom/google/android/music/store/PlayList;
    :cond_0
    invoke-static {v4}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v4}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0

    .line 837
    .end local v4    # "itemCursor":Landroid/database/Cursor;
    .end local v12    # "syncedListId":J
    :cond_1
    return-void
.end method

.method static createPlayList(Landroid/database/sqlite/SQLiteDatabase;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/music/store/PlayList;
    .locals 5
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "sourceAccount"    # I
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "description"    # Ljava/lang/String;
    .param p4, "ownerName"    # Ljava/lang/String;
    .param p5, "type"    # I
    .param p6, "shareToken"    # Ljava/lang/String;
    .param p7, "artUrl"    # Ljava/lang/String;
    .param p8, "ownerProfilePhotoUrl"    # Ljava/lang/String;
    .param p9, "editorArtworkUrl"    # Ljava/lang/String;

    .prologue
    .line 397
    new-instance v1, Lcom/google/android/music/store/PlayList;

    invoke-direct {v1}, Lcom/google/android/music/store/PlayList;-><init>()V

    .line 398
    .local v1, "list":Lcom/google/android/music/store/PlayList;
    invoke-virtual {v1, p1}, Lcom/google/android/music/store/PlayList;->setSourceAccount(I)V

    .line 399
    invoke-virtual {v1, p2}, Lcom/google/android/music/store/PlayList;->setName(Ljava/lang/String;)V

    .line 400
    invoke-virtual {v1, p3}, Lcom/google/android/music/store/PlayList;->setDescription(Ljava/lang/String;)V

    .line 401
    invoke-virtual {v1, p4}, Lcom/google/android/music/store/PlayList;->setOwnerName(Ljava/lang/String;)V

    .line 403
    sparse-switch p5, :sswitch_data_0

    .line 419
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unrecognized playlist type value: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 407
    :sswitch_0
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/android/music/store/PlayList;->setNeedsSync(Z)V

    .line 422
    :goto_0
    invoke-virtual {v1, p5}, Lcom/google/android/music/store/PlayList;->setType(I)V

    .line 423
    invoke-virtual {v1, p6}, Lcom/google/android/music/store/PlayList;->setShareToken(Ljava/lang/String;)V

    .line 424
    invoke-virtual {v1, p7}, Lcom/google/android/music/store/PlayList;->setArtworkLocation(Ljava/lang/String;)V

    .line 425
    invoke-virtual {v1, p8}, Lcom/google/android/music/store/PlayList;->setOwnerProfilePhotoUrl(Ljava/lang/String;)V

    .line 426
    invoke-virtual {v1, p9}, Lcom/google/android/music/store/PlayList;->setEditorArtworkUrl(Ljava/lang/String;)V

    .line 427
    invoke-static {p0}, Lcom/google/android/music/store/PlayList;->compilePlayListInsertStatement(Landroid/database/sqlite/SQLiteDatabase;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v0

    .line 429
    .local v0, "insert":Landroid/database/sqlite/SQLiteStatement;
    :try_start_0
    invoke-virtual {v1, v0}, Lcom/google/android/music/store/PlayList;->insertList(Landroid/database/sqlite/SQLiteStatement;)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 431
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteStatement;->close()V

    .line 434
    return-object v1

    .line 414
    .end local v0    # "insert":Landroid/database/sqlite/SQLiteStatement;
    :sswitch_1
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/music/store/PlayList;->setNeedsSync(Z)V

    goto :goto_0

    .line 417
    :sswitch_2
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Shared with me playlists are not persisted"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 431
    .restart local v0    # "insert":Landroid/database/sqlite/SQLiteStatement;
    :catchall_0
    move-exception v2

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteStatement;->close()V

    throw v2

    .line 403
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_0
        0xa -> :sswitch_1
        0x32 -> :sswitch_1
        0x33 -> :sswitch_1
        0x46 -> :sswitch_2
        0x47 -> :sswitch_0
        0x50 -> :sswitch_1
    .end sparse-switch
.end method

.method static createPlayList(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/music/store/PlayList;
    .locals 1
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "description"    # Ljava/lang/String;

    .prologue
    .line 352
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Lcom/google/android/music/store/PlayList;->createPlayList(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/music/store/PlayList;

    move-result-object v0

    return-object v0
.end method

.method static createPlayList(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/music/store/PlayList;
    .locals 10
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "description"    # Ljava/lang/String;
    .param p3, "type"    # I

    .prologue
    const/4 v4, 0x0

    .line 369
    const/4 v1, 0x0

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move v5, p3

    move-object v6, v4

    move-object v7, v4

    move-object v8, v4

    move-object v9, v4

    invoke-static/range {v0 .. v9}, Lcom/google/android/music/store/PlayList;->createPlayList(Landroid/database/sqlite/SQLiteDatabase;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/music/store/PlayList;

    move-result-object v0

    return-object v0
.end method

.method static deleteAllMediaStorePlaylists(Landroid/content/Context;)I
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 754
    const/4 v1, 0x0

    .line 755
    .local v1, "ids":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Long;>;"
    invoke-static {p0}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v2

    .line 756
    .local v2, "store":Lcom/google/android/music/store/Store;
    invoke-virtual {v2}, Lcom/google/android/music/store/Store;->beginRead()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 758
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    invoke-static {v0}, Lcom/google/android/music/store/PlayList;->getMediaStorePlaylistIds(Landroid/database/sqlite/SQLiteDatabase;)Ljava/util/Collection;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 760
    invoke-virtual {v2, v0}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 763
    invoke-static {p0, v1}, Lcom/google/android/music/store/PlayList;->deletePlaylistsAndItems(Landroid/content/Context;Ljava/util/Collection;)I

    move-result v3

    return v3

    .line 760
    :catchall_0
    move-exception v3

    invoke-virtual {v2, v0}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    throw v3
.end method

.method public static deleteById(Landroid/database/sqlite/SQLiteDatabase;J)I
    .locals 3
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "localListId"    # J

    .prologue
    .line 496
    const/4 v1, 0x1

    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 497
    .local v0, "args":[Ljava/lang/String;
    const-string v1, "LISTS"

    const-string v2, "Id=?"

    invoke-virtual {p0, v1, v2, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    return v1
.end method

.method public static deleteBySourceInfo(Landroid/database/sqlite/SQLiteStatement;ILjava/lang/String;)V
    .locals 4
    .param p0, "deleteStatement"    # Landroid/database/sqlite/SQLiteStatement;
    .param p1, "sourceAccount"    # I
    .param p2, "sourceId"    # Ljava/lang/String;

    .prologue
    .line 484
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    .line 486
    const/4 v0, 0x1

    int-to-long v2, p1

    invoke-virtual {p0, v0, v2, v3}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 487
    const/4 v0, 0x2

    invoke-virtual {p0, v0, p2}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 489
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteStatement;->execute()V

    .line 490
    return-void
.end method

.method static deleteLocalMusic(Landroid/database/sqlite/SQLiteDatabase;J)I
    .locals 11
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "musicId"    # J

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 1745
    const/4 v9, 0x0

    .line 1746
    .local v9, "count":I
    const-string v1, "LISTITEMS JOIN MUSIC ON (LISTITEMS.MusicId=MUSIC.Id) "

    # getter for: Lcom/google/android/music/store/PlayList$Item;->ITEM_PROJECTION:[Ljava/lang/String;
    invoke-static {}, Lcom/google/android/music/store/PlayList$Item;->access$300()[Ljava/lang/String;

    move-result-object v2

    const-string v3, "MUSIC.SourceAccount=0 AND LISTITEMS.MusicId=?"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v6

    move-object v0, p0

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 1756
    .local v8, "c":Landroid/database/Cursor;
    if-eqz v8, :cond_0

    .line 1757
    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v9

    .line 1758
    new-instance v10, Lcom/google/android/music/store/PlayList$Item;

    invoke-direct {v10}, Lcom/google/android/music/store/PlayList$Item;-><init>()V

    .line 1759
    .local v10, "item":Lcom/google/android/music/store/PlayList$Item;
    :goto_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1760
    invoke-virtual {v10, v8}, Lcom/google/android/music/store/PlayList$Item;->populateFromFullProjectionCursor(Landroid/database/Cursor;)V

    .line 1761
    const/4 v0, 0x0

    invoke-virtual {v10, p0, v0}, Lcom/google/android/music/store/PlayList$Item;->delete(Landroid/database/sqlite/SQLiteDatabase;Z)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1765
    .end local v10    # "item":Lcom/google/android/music/store/PlayList$Item;
    :catchall_0
    move-exception v0

    invoke-static {v8}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0

    :cond_0
    invoke-static {v8}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 1768
    return v9
.end method

.method static deleteMediaStorePlaylists(Landroid/database/sqlite/SQLiteDatabase;)I
    .locals 1
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 778
    invoke-static {p0}, Lcom/google/android/music/store/PlayList;->getMediaStorePlaylistIds(Landroid/database/sqlite/SQLiteDatabase;)Ljava/util/Collection;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/music/store/PlayList;->deletePlaylistsAndItems(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/Collection;)I

    move-result v0

    return v0
.end method

.method static deletePlaylistsAndItems(Landroid/content/Context;Ljava/util/Collection;)I
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 848
    .local p1, "playlistIds":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Long;>;"
    const/4 v1, 0x0

    .line 849
    .local v1, "deleteCount":I
    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 850
    invoke-static {p0}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v2

    .line 851
    .local v2, "store":Lcom/google/android/music/store/Store;
    const/4 v3, 0x0

    .line 852
    .local v3, "success":Z
    invoke-virtual {v2}, Lcom/google/android/music/store/Store;->beginWriteTxn()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 854
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    invoke-static {v0, p1}, Lcom/google/android/music/store/PlayList;->deletePlaylistsAndItems(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/Collection;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    .line 855
    const/4 v3, 0x1

    .line 857
    invoke-virtual {v2, v0, v3}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    .line 861
    .end local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v2    # "store":Lcom/google/android/music/store/Store;
    .end local v3    # "success":Z
    :cond_0
    if-lez v1, :cond_1

    .line 862
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Lcom/google/android/music/store/MusicContent;->CONTENT_URI:Landroid/net/Uri;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v6, v7}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    .line 864
    :cond_1
    return v1

    .line 857
    .restart local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .restart local v2    # "store":Lcom/google/android/music/store/Store;
    .restart local v3    # "success":Z
    :catchall_0
    move-exception v4

    invoke-virtual {v2, v0, v3}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    throw v4
.end method

.method static deletePlaylistsAndItems(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/Collection;)I
    .locals 6
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;)I"
        }
    .end annotation

    .prologue
    .local p1, "playlistIds":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Long;>;"
    const/4 v5, 0x0

    const/4 v3, 0x0

    .line 874
    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1

    move v0, v3

    .line 895
    :cond_0
    :goto_0
    return v0

    .line 879
    :cond_1
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v4

    mul-int/lit8 v4, v4, 0x6

    invoke-direct {v1, v4}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 880
    .local v1, "selectionBuffer":Ljava/lang/StringBuffer;
    const-string v4, "Id"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 881
    invoke-static {v1, p1}, Lcom/google/android/music/utils/DbUtils;->appendIN(Ljava/lang/StringBuffer;Ljava/util/Collection;)Ljava/lang/StringBuffer;

    .line 883
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    .line 884
    .local v2, "whereClause":Ljava/lang/String;
    const-string v4, "LISTS"

    invoke-virtual {p0, v4, v2, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 888
    .local v0, "deletedPlaylists":I
    if-lez v0, :cond_0

    .line 889
    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 890
    const-string v3, "ListId"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 891
    invoke-static {v1, p1}, Lcom/google/android/music/utils/DbUtils;->appendIN(Ljava/lang/StringBuffer;Ljava/util/Collection;)Ljava/lang/StringBuffer;

    .line 892
    const-string v3, "LISTITEMS"

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v3, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0
.end method

.method static deleteSyncedPlaylists(Landroid/database/sqlite/SQLiteDatabase;)I
    .locals 12
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 791
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 792
    .local v9, "ids":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Long;>;"
    const-string v1, "LISTS"

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "Id"

    aput-object v0, v2, v3

    const-string v3, "SourceAccount != 0"

    move-object v0, p0

    move-object v5, v4

    move-object v6, v4

    move-object v7, v4

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 796
    .local v8, "c":Landroid/database/Cursor;
    if-eqz v8, :cond_0

    .line 797
    :goto_0
    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 798
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 799
    .local v10, "playlistId":J
    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v9, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 803
    .end local v10    # "playlistId":J
    :catchall_0
    move-exception v0

    invoke-static {v8}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0

    :cond_0
    invoke-static {v8}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 808
    invoke-static {p0, v9}, Lcom/google/android/music/store/PlayList;->copySideloadedPlaylistItems(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/Collection;)V

    .line 809
    invoke-static {p0, v9}, Lcom/google/android/music/store/PlayList;->deletePlaylistsAndItems(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/Collection;)I

    move-result v0

    return v0
.end method

.method static disconnectFromMediaStore(Landroid/database/sqlite/SQLiteDatabase;J)V
    .locals 7
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "playlistId"    # J

    .prologue
    const/4 v3, 0x1

    .line 728
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0, v3}, Landroid/content/ContentValues;-><init>(I)V

    .line 729
    .local v0, "values":Landroid/content/ContentValues;
    const-string v1, "MediaStoreId"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 730
    const-string v1, "SourceId"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 731
    const-string v1, "_sync_dirty"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 732
    const-string v1, "LISTS"

    const-string v2, "Id=? AND MediaStoreId NOT NULL"

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {p0, v1, v0, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 735
    return-void
.end method

.method static getFirstItemId(Landroid/database/sqlite/SQLiteDatabase;J)Lcom/google/android/music/download/ContentIdentifier;
    .locals 15
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "playlistId"    # J

    .prologue
    .line 988
    const/4 v11, 0x0

    .line 990
    .local v11, "id":Lcom/google/android/music/download/ContentIdentifier;
    const-string v1, "LISTITEMS JOIN MUSIC ON (LISTITEMS.MusicId=MUSIC.Id) "

    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "MUSIC.Id"

    aput-object v3, v2, v0

    const/4 v0, 0x1

    const-string v3, "Domain"

    aput-object v3, v2, v0

    const-string v3, "ListId=?"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static/range {p1 .. p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string v7, "ServerOrder, ClientPosition"

    const-string v8, "1"

    move-object v0, p0

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 1001
    .local v9, "c":Landroid/database/Cursor;
    if-eqz v9, :cond_0

    :try_start_0
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1002
    const/4 v0, 0x0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    .line 1003
    .local v12, "musicId":J
    const/4 v0, 0x1

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Lcom/google/android/music/download/ContentIdentifier$Domain;->fromDBValue(I)Lcom/google/android/music/download/ContentIdentifier$Domain;

    move-result-object v10

    .line 1004
    .local v10, "domain":Lcom/google/android/music/download/ContentIdentifier$Domain;
    new-instance v11, Lcom/google/android/music/download/ContentIdentifier;

    .end local v11    # "id":Lcom/google/android/music/download/ContentIdentifier;
    invoke-direct {v11, v12, v13, v10}, Lcom/google/android/music/download/ContentIdentifier;-><init>(JLcom/google/android/music/download/ContentIdentifier$Domain;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1007
    .end local v10    # "domain":Lcom/google/android/music/download/ContentIdentifier$Domain;
    .end local v12    # "musicId":J
    .restart local v11    # "id":Lcom/google/android/music/download/ContentIdentifier;
    :cond_0
    invoke-static {v9}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 1010
    return-object v11

    .line 1007
    .end local v11    # "id":Lcom/google/android/music/download/ContentIdentifier;
    :catchall_0
    move-exception v0

    invoke-static {v9}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method private static getFirstItemPosition(Landroid/database/sqlite/SQLiteDatabase;J)Landroid/util/Pair;
    .locals 13
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "playlistId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "J)",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 1015
    const-string v11, ""

    .line 1016
    .local v11, "serverPosition":Ljava/lang/String;
    const/high16 v10, -0x80000000

    .line 1018
    .local v10, "clientPosition":I
    const-string v1, "LISTITEMS"

    # getter for: Lcom/google/android/music/store/PlayList$Item;->ORDER_PROJECTION:[Ljava/lang/String;
    invoke-static {}, Lcom/google/android/music/store/PlayList$Item;->access$000()[Ljava/lang/String;

    move-result-object v2

    const-string v3, "ListId=?"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v0

    const-string v7, "ServerOrder, ClientPosition"

    const-string v8, "1"

    move-object v0, p0

    move-object v6, v5

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 1029
    .local v9, "c":Landroid/database/Cursor;
    if-eqz v9, :cond_0

    :try_start_0
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1030
    # getter for: Lcom/google/android/music/store/PlayList$Item;->ORDER_PROJECTION_INDEX_SERVER:I
    invoke-static {}, Lcom/google/android/music/store/PlayList$Item;->access$100()I

    move-result v0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 1031
    # getter for: Lcom/google/android/music/store/PlayList$Item;->ORDER_PROJECTION_INDEX_CLIENT:I
    invoke-static {}, Lcom/google/android/music/store/PlayList$Item;->access$200()I

    move-result v0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v10

    .line 1034
    :cond_0
    invoke-static {v9}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 1037
    new-instance v0, Landroid/util/Pair;

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v11, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0

    .line 1034
    :catchall_0
    move-exception v0

    invoke-static {v9}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method static getItemCount(Landroid/database/sqlite/SQLiteDatabase;J)I
    .locals 11
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "playlistId"    # J

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 1519
    const/4 v8, 0x0

    .line 1520
    .local v8, "count":I
    const-string v1, "LISTITEMS"

    sget-object v2, Lcom/google/android/music/store/PlayList;->COUNT_COLUMNS:[Ljava/lang/String;

    const-string v3, "ListId=?"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v6

    move-object v0, p0

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 1524
    .local v9, "countCursor":Landroid/database/Cursor;
    if-eqz v9, :cond_0

    :try_start_0
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1525
    const/4 v0, 0x0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v8

    .line 1529
    :cond_0
    invoke-static {v9}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 1531
    return v8

    .line 1529
    :catchall_0
    move-exception v0

    invoke-static {v9}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method private static getItemPosition(Landroid/database/sqlite/SQLiteDatabase;JJ)Landroid/util/Pair;
    .locals 9
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "playlistId"    # J
    .param p3, "itemId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "JJ)",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 1042
    const-string v1, "LISTITEMS"

    # getter for: Lcom/google/android/music/store/PlayList$Item;->ORDER_PROJECTION:[Ljava/lang/String;
    invoke-static {}, Lcom/google/android/music/store/PlayList$Item;->access$000()[Ljava/lang/String;

    move-result-object v2

    const-string v3, "ListId=? AND Id=?"

    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v0

    const/4 v0, 0x1

    invoke-static {p3, p4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v0

    move-object v0, p0

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 1049
    .local v8, "c":Landroid/database/Cursor;
    if-eqz v8, :cond_0

    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1050
    new-instance v0, Landroid/util/Pair;

    # getter for: Lcom/google/android/music/store/PlayList$Item;->ORDER_PROJECTION_INDEX_SERVER:I
    invoke-static {}, Lcom/google/android/music/store/PlayList$Item;->access$100()I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    # getter for: Lcom/google/android/music/store/PlayList$Item;->ORDER_PROJECTION_INDEX_CLIENT:I
    invoke-static {}, Lcom/google/android/music/store/PlayList$Item;->access$200()I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1058
    invoke-static {v8}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    return-object v0

    .line 1054
    :cond_0
    :try_start_1
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Item "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not found in playlist "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1058
    :catchall_0
    move-exception v0

    invoke-static {v8}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method private static getLastItemPosition(Landroid/database/sqlite/SQLiteDatabase;J)Landroid/util/Pair;
    .locals 13
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "playlistId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "J)",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 961
    const-string v11, ""

    .line 962
    .local v11, "serverPosition":Ljava/lang/String;
    const/high16 v10, -0x80000000

    .line 964
    .local v10, "clientPosition":I
    const-string v1, "LISTITEMS"

    # getter for: Lcom/google/android/music/store/PlayList$Item;->ORDER_PROJECTION:[Ljava/lang/String;
    invoke-static {}, Lcom/google/android/music/store/PlayList$Item;->access$000()[Ljava/lang/String;

    move-result-object v2

    const-string v3, "ListId=?"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v0

    const-string v7, "ServerOrder DESC, ClientPosition DESC"

    const-string v8, "1"

    move-object v0, p0

    move-object v6, v5

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 975
    .local v9, "c":Landroid/database/Cursor;
    if-eqz v9, :cond_0

    :try_start_0
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 976
    # getter for: Lcom/google/android/music/store/PlayList$Item;->ORDER_PROJECTION_INDEX_SERVER:I
    invoke-static {}, Lcom/google/android/music/store/PlayList$Item;->access$100()I

    move-result v0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 977
    # getter for: Lcom/google/android/music/store/PlayList$Item;->ORDER_PROJECTION_INDEX_CLIENT:I
    invoke-static {}, Lcom/google/android/music/store/PlayList$Item;->access$200()I

    move-result v0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v10

    .line 980
    :cond_0
    invoke-static {v9}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 983
    new-instance v0, Landroid/util/Pair;

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v11, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0

    .line 980
    :catchall_0
    move-exception v0

    invoke-static {v9}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method static getMediaStorePlaylistIds(Landroid/database/sqlite/SQLiteDatabase;)Ljava/util/Collection;
    .locals 10
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            ")",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 738
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 739
    .local v9, "ids":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Long;>;"
    const-string v1, "LISTS"

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "Id"

    aput-object v0, v2, v3

    const-string v3, "MediaStoreId NOT NULL"

    move-object v0, p0

    move-object v5, v4

    move-object v6, v4

    move-object v7, v4

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 742
    .local v8, "c":Landroid/database/Cursor;
    if-eqz v8, :cond_0

    .line 743
    :goto_0
    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 744
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v9, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 748
    :catchall_0
    move-exception v0

    invoke-static {v8}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0

    :cond_0
    invoke-static {v8}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 750
    return-object v9
.end method

.method private static getNextClientPosition(Landroid/database/sqlite/SQLiteDatabase;JLjava/lang/String;I)I
    .locals 11
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "playlistId"    # J
    .param p3, "serverPosition"    # Ljava/lang/String;
    .param p4, "clientPosition"    # I

    .prologue
    const/4 v5, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1066
    const-string v1, "LISTITEMS"

    new-array v2, v7, [Ljava/lang/String;

    const-string v0, "ClientPosition"

    aput-object v0, v2, v6

    const-string v3, "ListId=? AND ServerOrder=? AND ClientPosition>?"

    const/4 v0, 0x3

    new-array v4, v0, [Ljava/lang/String;

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v6

    aput-object p3, v4, v7

    const/4 v0, 0x2

    invoke-static {p4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v0

    const-string v7, "ClientPosition"

    const-string v8, "1"

    move-object v0, p0

    move-object v6, v5

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 1075
    .local v9, "c":Landroid/database/Cursor;
    if-eqz v9, :cond_0

    :try_start_0
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1076
    const/4 v0, 0x0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 1081
    invoke-static {v9}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    :goto_0
    return v0

    .line 1078
    :cond_0
    const/high16 v0, -0x80000000

    .line 1081
    invoke-static {v9}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v9}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method public static getPlaylistTombstones(Landroid/database/sqlite/SQLiteDatabase;)Landroid/database/Cursor;
    .locals 8
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    const/4 v3, 0x0

    .line 954
    const-string v1, "LIST_TOMBSTONES"

    sget-object v2, Lcom/google/android/music/store/PlayList;->PLAYLIST_TOMBSTONE_PROJECTION:[Ljava/lang/String;

    move-object v0, p0

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public static getPlaylistsToSync(Landroid/database/sqlite/SQLiteDatabase;)Landroid/database/Cursor;
    .locals 8
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    const/4 v4, 0x0

    .line 933
    const-string v1, "LISTS"

    sget-object v2, Lcom/google/android/music/store/PlayList;->FULL_PROJECTION:[Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "_sync_dirty=1 AND (SourceAccount<>"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v3, Lcom/google/android/music/store/MusicFile;->MEDIA_STORE_SOURCE_ACCOUNT_AS_STRING:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " OR "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "EXISTS(SELECT 1 FROM "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "LISTITEMS"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " JOIN MUSIC ON (LISTITEMS.MusicId=MUSIC.Id) "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " WHERE "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "ListId"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "LISTS.Id"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " AND "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "MUSIC.SourceAccount"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "<>"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v3, Lcom/google/android/music/store/MusicFile;->MEDIA_STORE_SOURCE_ACCOUNT_AS_STRING:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " LIMIT 1)"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ")"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v0, p0

    move-object v5, v4

    move-object v6, v4

    move-object v7, v4

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method static modify(Landroid/database/sqlite/SQLiteDatabase;JLjava/lang/String;Ljava/lang/String;)Z
    .locals 9
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "playlistId"    # J
    .param p3, "newName"    # Ljava/lang/String;
    .param p4, "newDescription"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 712
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 713
    .local v1, "values":Landroid/content/ContentValues;
    const-string v4, "Name"

    invoke-virtual {v1, v4, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 714
    const-string v4, "Description"

    invoke-virtual {v1, v4, p4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 715
    const-string v4, "NameSort"

    invoke-virtual {p3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 716
    const-string v4, "_sync_dirty"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 717
    const-string v4, "LISTS"

    const-string v5, "Id=?"

    new-array v6, v2, [Ljava/lang/String;

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v3

    invoke-virtual {p0, v4, v1, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 720
    .local v0, "numberOfUpdatedPlaylists":I
    if-lez v0, :cond_0

    :goto_0
    return v2

    :cond_0
    move v2, v3

    goto :goto_0
.end method

.method static moveItem(Landroid/database/sqlite/SQLiteDatabase;JJJZZ)Z
    .locals 31
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "playlistId"    # J
    .param p3, "itemId"    # J
    .param p5, "desiredPreviousItemId"    # J
    .param p7, "canResetPositions"    # Z
    .param p8, "sync"    # Z

    .prologue
    .line 1616
    move-object/from16 v0, p0

    move-wide/from16 v1, p1

    move-wide/from16 v3, p5

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/music/store/PlayList;->getItemPosition(Landroid/database/sqlite/SQLiteDatabase;JJ)Landroid/util/Pair;

    move-result-object v30

    .line 1618
    .local v30, "previousPosition":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Integer;>;"
    move-object/from16 v0, v30

    iget-object v12, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v12, Ljava/lang/String;

    .line 1619
    .local v12, "serverPosition":Ljava/lang/String;
    move-object/from16 v0, v30

    iget-object v7, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v19

    .line 1620
    .local v19, "previousClientPosition":I
    move-object/from16 v0, p0

    move-wide/from16 v1, p1

    move/from16 v3, v19

    invoke-static {v0, v1, v2, v12, v3}, Lcom/google/android/music/store/PlayList;->getNextClientPosition(Landroid/database/sqlite/SQLiteDatabase;JLjava/lang/String;I)I

    move-result v6

    .line 1622
    .local v6, "nextClientPosition":I
    const/high16 v7, -0x80000000

    if-ne v6, v7, :cond_2

    .line 1624
    const/high16 v7, 0x20000

    add-int v13, v19, v7

    .line 1627
    .local v13, "clientPosition":I
    move/from16 v0, v19

    if-gt v13, v0, :cond_1

    .line 1628
    if-eqz p7, :cond_0

    .line 1629
    move-object/from16 v0, p0

    move-wide/from16 v1, p1

    invoke-static {v0, v1, v2, v12}, Lcom/google/android/music/store/PlayList;->resetClientPositions(Landroid/database/sqlite/SQLiteDatabase;JLjava/lang/String;)V

    .line 1630
    const/4 v14, 0x0

    move-object/from16 v7, p0

    move-wide/from16 v8, p1

    move-wide/from16 v10, p3

    move-wide/from16 v12, p5

    move/from16 v15, p8

    invoke-static/range {v7 .. v15}, Lcom/google/android/music/store/PlayList;->moveItem(Landroid/database/sqlite/SQLiteDatabase;JJJZZ)Z

    .end local v12    # "serverPosition":Ljava/lang/String;
    move-result v7

    .line 1652
    .end local v13    # "clientPosition":I
    :goto_0
    return v7

    .line 1632
    .restart local v12    # "serverPosition":Ljava/lang/String;
    .restart local v13    # "clientPosition":I
    :cond_0
    new-instance v7, Ljava/lang/RuntimeException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Failed to move item in playlist "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-wide/from16 v0, p1

    invoke-virtual {v8, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v7

    :cond_1
    move-object/from16 v7, p0

    move-wide/from16 v8, p1

    move-wide/from16 v10, p3

    move/from16 v14, p8

    .line 1635
    invoke-static/range {v7 .. v14}, Lcom/google/android/music/store/PlayList;->setItemPosition(Landroid/database/sqlite/SQLiteDatabase;JJLjava/lang/String;IZ)V

    .line 1636
    const/4 v7, 0x1

    goto :goto_0

    .line 1638
    .end local v13    # "clientPosition":I
    :cond_2
    sub-int v7, v6, v19

    const/4 v8, 0x1

    if-le v7, v8, :cond_5

    .line 1640
    sub-int v7, v6, v19

    div-int/lit8 v7, v7, 0x2

    add-int v13, v19, v7

    .line 1644
    .restart local v13    # "clientPosition":I
    move/from16 v0, v19

    if-le v13, v0, :cond_3

    if-lt v13, v6, :cond_4

    .line 1645
    :cond_3
    new-instance v7, Ljava/lang/RuntimeException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Error in calculating playlist item position."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, v30

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v7

    :cond_4
    move-object/from16 v7, p0

    move-wide/from16 v8, p1

    move-wide/from16 v10, p3

    move/from16 v14, p8

    .line 1648
    invoke-static/range {v7 .. v14}, Lcom/google/android/music/store/PlayList;->setItemPosition(Landroid/database/sqlite/SQLiteDatabase;JJLjava/lang/String;IZ)V

    .line 1649
    const/4 v7, 0x1

    goto :goto_0

    .line 1650
    .end local v13    # "clientPosition":I
    :cond_5
    if-eqz p7, :cond_6

    .line 1651
    const/16 v20, 0x0

    move-object/from16 v15, p0

    move-wide/from16 v16, p1

    move-object/from16 v18, v12

    invoke-static/range {v15 .. v20}, Lcom/google/android/music/store/PlayList;->resetPositions(Landroid/database/sqlite/SQLiteDatabase;JLjava/lang/String;IZ)V

    .line 1652
    const/16 v28, 0x0

    move-object/from16 v21, p0

    move-wide/from16 v22, p1

    move-wide/from16 v24, p3

    move-wide/from16 v26, p5

    move/from16 v29, p8

    invoke-static/range {v21 .. v29}, Lcom/google/android/music/store/PlayList;->moveItem(Landroid/database/sqlite/SQLiteDatabase;JJJZZ)Z

    move-result v7

    goto/16 :goto_0

    .line 1654
    :cond_6
    new-instance v7, Ljava/lang/RuntimeException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Failed to move item in playlist "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-wide/from16 v0, p1

    invoke-virtual {v8, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v7
.end method

.method static moveItemToTop(Landroid/database/sqlite/SQLiteDatabase;JJZZ)Z
    .locals 11
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "playlistId"    # J
    .param p3, "itemId"    # J
    .param p5, "canResetPositions"    # Z
    .param p6, "sync"    # Z

    .prologue
    .line 1569
    invoke-static {p0, p1, p2}, Lcom/google/android/music/store/PlayList;->getFirstItemPosition(Landroid/database/sqlite/SQLiteDatabase;J)Landroid/util/Pair;

    move-result-object v9

    .line 1570
    .local v9, "firstPosition":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Integer;>;"
    iget-object v6, v9, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v6, Ljava/lang/String;

    .line 1571
    .local v6, "serverPosition":Ljava/lang/String;
    iget-object v1, v9, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1573
    .local v0, "firstClientPosition":I
    const/4 v7, 0x0

    .line 1574
    .local v7, "newClientPosition":I
    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 1575
    const/high16 v1, 0x20000

    sub-int v7, v0, v1

    .line 1578
    :cond_0
    if-ge v7, v0, :cond_1

    const/high16 v1, -0x80000000

    if-ne v7, v1, :cond_3

    .line 1579
    :cond_1
    invoke-static {p0, p1, p2, v6}, Lcom/google/android/music/store/PlayList;->resetClientPositions(Landroid/database/sqlite/SQLiteDatabase;JLjava/lang/String;)V

    .line 1580
    if-eqz p5, :cond_2

    .line 1581
    const/4 v6, 0x0

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    move/from16 v7, p6

    invoke-static/range {v1 .. v7}, Lcom/google/android/music/store/PlayList;->moveItemToTop(Landroid/database/sqlite/SQLiteDatabase;JJZZ)Z

    .end local v6    # "serverPosition":Ljava/lang/String;
    .end local v7    # "newClientPosition":I
    move-result v1

    .line 1587
    :goto_0
    return v1

    .line 1583
    .restart local v6    # "serverPosition":Ljava/lang/String;
    .restart local v7    # "newClientPosition":I
    :cond_2
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to move item in playlist "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_3
    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    move/from16 v8, p6

    .line 1586
    invoke-static/range {v1 .. v8}, Lcom/google/android/music/store/PlayList;->setItemPosition(Landroid/database/sqlite/SQLiteDatabase;JJLjava/lang/String;IZ)V

    .line 1587
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private prepareInsertOrFullUpdate(Landroid/database/sqlite/SQLiteStatement;)V
    .locals 9
    .param p1, "statement"    # Landroid/database/sqlite/SQLiteStatement;

    .prologue
    const/16 v8, 0x9

    const/16 v7, 0x8

    const/4 v6, 0x6

    const/4 v5, 0x3

    const/4 v4, 0x1

    .line 197
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    .line 199
    iget-wide v0, p0, Lcom/google/android/music/store/PlayList;->mMediaStoreId:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 200
    invoke-virtual {p1, v4}, Landroid/database/sqlite/SQLiteStatement;->bindNull(I)V

    .line 204
    :goto_0
    const/4 v1, 0x2

    iget-object v0, p0, Lcom/google/android/music/store/PlayList;->mName:Ljava/lang/String;

    if-nez v0, :cond_1

    const-string v0, ""

    :goto_1
    invoke-virtual {p1, v1, v0}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 205
    const/16 v1, 0xd

    iget-object v0, p0, Lcom/google/android/music/store/PlayList;->mName:Ljava/lang/String;

    if-nez v0, :cond_2

    const-string v0, ""

    :goto_2
    invoke-virtual {p1, v1, v0}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 207
    const/4 v0, 0x7

    iget v1, p0, Lcom/google/android/music/store/PlayList;->mType:I

    int-to-long v2, v1

    invoke-virtual {p1, v0, v2, v3}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 208
    iget-object v0, p0, Lcom/google/android/music/store/PlayList;->mArtworkLocation:Ljava/lang/String;

    if-nez v0, :cond_3

    .line 209
    invoke-virtual {p1, v7}, Landroid/database/sqlite/SQLiteStatement;->bindNull(I)V

    .line 213
    :goto_3
    iget-object v0, p0, Lcom/google/android/music/store/PlayList;->mSourceVersion:Ljava/lang/String;

    if-nez v0, :cond_4

    .line 214
    invoke-virtual {p1, v5}, Landroid/database/sqlite/SQLiteStatement;->bindNull(I)V

    .line 218
    :goto_4
    const/4 v2, 0x4

    iget-boolean v0, p0, Lcom/google/android/music/store/PlayList;->mNeedsSync:Z

    if-eqz v0, :cond_5

    const-wide/16 v0, 0x1

    :goto_5
    invoke-virtual {p1, v2, v0, v1}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 220
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/android/music/store/PlayList;->mSourceAccount:I

    int-to-long v2, v1

    invoke-virtual {p1, v0, v2, v3}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 221
    iget-object v0, p0, Lcom/google/android/music/store/PlayList;->mSourceId:Ljava/lang/String;

    if-nez v0, :cond_6

    .line 222
    invoke-virtual {p1, v6}, Landroid/database/sqlite/SQLiteStatement;->bindNull(I)V

    .line 227
    :goto_6
    iget-object v0, p0, Lcom/google/android/music/store/PlayList;->mShareToken:Ljava/lang/String;

    if-nez v0, :cond_7

    .line 228
    invoke-virtual {p1, v8}, Landroid/database/sqlite/SQLiteStatement;->bindNull(I)V

    .line 233
    :goto_7
    iget-object v0, p0, Lcom/google/android/music/store/PlayList;->mOwnerName:Ljava/lang/String;

    if-nez v0, :cond_8

    .line 234
    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteStatement;->bindNull(I)V

    .line 239
    :goto_8
    iget-object v0, p0, Lcom/google/android/music/store/PlayList;->mDescription:Ljava/lang/String;

    if-nez v0, :cond_9

    .line 240
    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteStatement;->bindNull(I)V

    .line 244
    :goto_9
    iget-object v0, p0, Lcom/google/android/music/store/PlayList;->mOwnerProfilePhotoUrl:Ljava/lang/String;

    if-nez v0, :cond_a

    .line 245
    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteStatement;->bindNull(I)V

    .line 251
    :goto_a
    return-void

    .line 202
    :cond_0
    iget-wide v0, p0, Lcom/google/android/music/store/PlayList;->mMediaStoreId:J

    invoke-virtual {p1, v4, v0, v1}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    goto :goto_0

    .line 204
    :cond_1
    iget-object v0, p0, Lcom/google/android/music/store/PlayList;->mName:Ljava/lang/String;

    goto :goto_1

    .line 205
    :cond_2
    iget-object v0, p0, Lcom/google/android/music/store/PlayList;->mName:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 211
    :cond_3
    iget-object v0, p0, Lcom/google/android/music/store/PlayList;->mArtworkLocation:Ljava/lang/String;

    invoke-virtual {p1, v7, v0}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    goto :goto_3

    .line 216
    :cond_4
    iget-object v0, p0, Lcom/google/android/music/store/PlayList;->mSourceVersion:Ljava/lang/String;

    invoke-virtual {p1, v5, v0}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    goto :goto_4

    .line 218
    :cond_5
    const-wide/16 v0, 0x0

    goto :goto_5

    .line 224
    :cond_6
    iget-object v0, p0, Lcom/google/android/music/store/PlayList;->mSourceId:Ljava/lang/String;

    invoke-virtual {p1, v6, v0}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    goto :goto_6

    .line 230
    :cond_7
    iget-object v0, p0, Lcom/google/android/music/store/PlayList;->mShareToken:Ljava/lang/String;

    invoke-virtual {p1, v8, v0}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    goto :goto_7

    .line 236
    :cond_8
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/android/music/store/PlayList;->mOwnerName:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    goto :goto_8

    .line 242
    :cond_9
    const/16 v0, 0xb

    iget-object v1, p0, Lcom/google/android/music/store/PlayList;->mDescription:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    goto :goto_9

    .line 247
    :cond_a
    const/16 v0, 0xc

    iget-object v1, p0, Lcom/google/android/music/store/PlayList;->mOwnerProfilePhotoUrl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    goto :goto_a
.end method

.method public static readPlayList(Landroid/content/Context;JLcom/google/android/music/store/PlayList;)Lcom/google/android/music/store/PlayList;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "playlistId"    # J
    .param p3, "playList"    # Lcom/google/android/music/store/PlayList;

    .prologue
    .line 661
    invoke-static {p0}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v1

    .line 662
    .local v1, "store":Lcom/google/android/music/store/Store;
    invoke-virtual {v1}, Lcom/google/android/music/store/Store;->beginRead()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 664
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    invoke-static {v0, p1, p2, p3}, Lcom/google/android/music/store/PlayList;->readPlayList(Landroid/database/sqlite/SQLiteDatabase;JLcom/google/android/music/store/PlayList;)Lcom/google/android/music/store/PlayList;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 666
    invoke-virtual {v1, v0}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    return-object v2

    :catchall_0
    move-exception v2

    invoke-virtual {v1, v0}, Lcom/google/android/music/store/Store;->endRead(Landroid/database/sqlite/SQLiteDatabase;)V

    throw v2
.end method

.method public static readPlayList(Landroid/database/sqlite/SQLiteDatabase;JLcom/google/android/music/store/PlayList;)Lcom/google/android/music/store/PlayList;
    .locals 11
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "playlistId"    # J
    .param p3, "playList"    # Lcom/google/android/music/store/PlayList;

    .prologue
    const/4 v5, 0x0

    .line 681
    const-string v1, "LISTS"

    sget-object v2, Lcom/google/android/music/store/PlayList;->FULL_PROJECTION:[Ljava/lang/String;

    const-string v3, "Id=?"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v0

    move-object v0, p0

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 687
    .local v8, "c":Landroid/database/Cursor;
    if-eqz v8, :cond_1

    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 688
    if-nez p3, :cond_0

    .line 689
    new-instance v9, Lcom/google/android/music/store/PlayList;

    invoke-direct {v9}, Lcom/google/android/music/store/PlayList;-><init>()V

    .end local p3    # "playList":Lcom/google/android/music/store/PlayList;
    .local v9, "playList":Lcom/google/android/music/store/PlayList;
    move-object p3, v9

    .line 691
    .end local v9    # "playList":Lcom/google/android/music/store/PlayList;
    .restart local p3    # "playList":Lcom/google/android/music/store/PlayList;
    :cond_0
    invoke-virtual {p3, v8}, Lcom/google/android/music/store/PlayList;->populateFromFullProjectionCursor(Landroid/database/Cursor;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 697
    invoke-static {v8}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    move-object v5, p3

    :goto_0
    return-object v5

    :cond_1
    invoke-static {v8}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v8}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method public static readPlayList(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Lcom/google/android/music/store/PlayList;)Lcom/google/android/music/store/PlayList;
    .locals 10
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "shareToken"    # Ljava/lang/String;
    .param p2, "playList"    # Lcom/google/android/music/store/PlayList;

    .prologue
    const/4 v5, 0x0

    .line 631
    const-string v1, "LISTS"

    sget-object v2, Lcom/google/android/music/store/PlayList;->FULL_PROJECTION:[Ljava/lang/String;

    const-string v3, "ShareToken=?"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p1, v4, v0

    move-object v0, p0

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 637
    .local v8, "c":Landroid/database/Cursor;
    if-eqz v8, :cond_1

    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 638
    if-nez p2, :cond_0

    .line 639
    new-instance v9, Lcom/google/android/music/store/PlayList;

    invoke-direct {v9}, Lcom/google/android/music/store/PlayList;-><init>()V

    .end local p2    # "playList":Lcom/google/android/music/store/PlayList;
    .local v9, "playList":Lcom/google/android/music/store/PlayList;
    move-object p2, v9

    .line 641
    .end local v9    # "playList":Lcom/google/android/music/store/PlayList;
    .restart local p2    # "playList":Lcom/google/android/music/store/PlayList;
    :cond_0
    invoke-virtual {p2, v8}, Lcom/google/android/music/store/PlayList;->populateFromFullProjectionCursor(Landroid/database/Cursor;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 647
    invoke-static {v8}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    move-object v5, p2

    :goto_0
    return-object v5

    :cond_1
    invoke-static {v8}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v8}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method public static readPlayList(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/music/store/PlayList;)Lcom/google/android/music/store/PlayList;
    .locals 10
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "sourceAccount"    # Ljava/lang/String;
    .param p2, "sourceId"    # Ljava/lang/String;
    .param p3, "playList"    # Lcom/google/android/music/store/PlayList;

    .prologue
    const/4 v5, 0x0

    .line 601
    const-string v1, "LISTS"

    sget-object v2, Lcom/google/android/music/store/PlayList;->FULL_PROJECTION:[Ljava/lang/String;

    const-string v3, "SourceAccount=? AND SourceId=?"

    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p1, v4, v0

    const/4 v0, 0x1

    aput-object p2, v4, v0

    move-object v0, p0

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 607
    .local v8, "c":Landroid/database/Cursor;
    if-eqz v8, :cond_1

    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 608
    if-nez p3, :cond_0

    .line 609
    new-instance v9, Lcom/google/android/music/store/PlayList;

    invoke-direct {v9}, Lcom/google/android/music/store/PlayList;-><init>()V

    .end local p3    # "playList":Lcom/google/android/music/store/PlayList;
    .local v9, "playList":Lcom/google/android/music/store/PlayList;
    move-object p3, v9

    .line 611
    .end local v9    # "playList":Lcom/google/android/music/store/PlayList;
    .restart local p3    # "playList":Lcom/google/android/music/store/PlayList;
    :cond_0
    invoke-virtual {p3, v8}, Lcom/google/android/music/store/PlayList;->populateFromFullProjectionCursor(Landroid/database/Cursor;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 617
    invoke-static {v8}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    move-object v5, p3

    :goto_0
    return-object v5

    :cond_1
    invoke-static {v8}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v8}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method static removeLocalOrphanedItems(Landroid/content/Context;)I
    .locals 15
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1772
    invoke-static {p0}, Lcom/google/android/music/store/Store;->getInstance(Landroid/content/Context;)Lcom/google/android/music/store/Store;

    move-result-object v13

    .line 1773
    .local v13, "store":Lcom/google/android/music/store/Store;
    const/4 v11, 0x0

    .line 1774
    .local v11, "count":I
    const/4 v14, 0x0

    .line 1775
    .local v14, "success":Z
    invoke-virtual {v13}, Lcom/google/android/music/store/Store;->beginWriteTxn()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1777
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v10, 0x0

    .line 1781
    .local v10, "chunkCount":I
    :cond_0
    :try_start_0
    const-string v1, "LISTITEMS LEFT  JOIN MUSIC ON (LISTITEMS.MusicId=MUSIC.Id) "

    # getter for: Lcom/google/android/music/store/PlayList$Item;->ITEM_PROJECTION:[Ljava/lang/String;
    invoke-static {}, Lcom/google/android/music/store/PlayList$Item;->access$300()[Ljava/lang/String;

    move-result-object v2

    const-string v3, "MUSIC.SourceAccount=0 AND MUSIC.Id IS NULL"

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    sget-object v8, Lcom/google/android/music/store/Store;->OPERATIONS_PER_TXN_AS_STRING:Ljava/lang/String;

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v9

    .line 1792
    .local v9, "c":Landroid/database/Cursor;
    if-eqz v9, :cond_1

    .line 1793
    :try_start_1
    invoke-interface {v9}, Landroid/database/Cursor;->getCount()I

    move-result v10

    .line 1794
    add-int/2addr v11, v10

    .line 1795
    new-instance v12, Lcom/google/android/music/store/PlayList$Item;

    invoke-direct {v12}, Lcom/google/android/music/store/PlayList$Item;-><init>()V

    .line 1796
    .local v12, "item":Lcom/google/android/music/store/PlayList$Item;
    :goto_0
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1797
    invoke-virtual {v12, v9}, Lcom/google/android/music/store/PlayList$Item;->populateFromFullProjectionCursor(Landroid/database/Cursor;)V

    .line 1798
    const/4 v1, 0x1

    invoke-virtual {v12, v0, v1}, Lcom/google/android/music/store/PlayList$Item;->delete(Landroid/database/sqlite/SQLiteDatabase;Z)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1803
    .end local v12    # "item":Lcom/google/android/music/store/PlayList$Item;
    :catchall_0
    move-exception v1

    :try_start_2
    invoke-static {v9}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1810
    .end local v9    # "c":Landroid/database/Cursor;
    :catchall_1
    move-exception v1

    invoke-virtual {v13, v0, v14}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    throw v1

    .line 1803
    .restart local v9    # "c":Landroid/database/Cursor;
    :cond_1
    :try_start_3
    invoke-static {v9}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 1805
    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->yieldIfContendedSafely(J)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1806
    const/4 v14, 0x1

    .line 1807
    if-gtz v10, :cond_0

    .line 1810
    invoke-virtual {v13, v0, v14}, Lcom/google/android/music/store/Store;->endWriteTxn(Landroid/database/sqlite/SQLiteDatabase;Z)V

    .line 1813
    if-lez v11, :cond_2

    .line 1815
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/google/android/music/store/MusicContent;->CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    .line 1819
    :cond_2
    return v11
.end method

.method static removeOrphanedItems(Landroid/database/sqlite/SQLiteDatabase;)I
    .locals 3
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 1846
    const-string v0, "LISTITEMS"

    const-string v1, "Id IN (SELECT LISTITEMS.Id FROM LISTITEMS LEFT  JOIN MUSIC ON (LISTITEMS.MusicId=MUSIC.Id)  WHERE MUSIC.Id IS NULL)"

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method private static resetClientPositions(Landroid/database/sqlite/SQLiteDatabase;JLjava/lang/String;)V
    .locals 7
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "playlistId"    # J
    .param p3, "serverPosition"    # Ljava/lang/String;

    .prologue
    .line 1661
    const/high16 v5, -0x80000000

    const/4 v6, 0x1

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    invoke-static/range {v1 .. v6}, Lcom/google/android/music/store/PlayList;->resetPositions(Landroid/database/sqlite/SQLiteDatabase;JLjava/lang/String;IZ)V

    .line 1662
    return-void
.end method

.method private static resetPositions(Landroid/database/sqlite/SQLiteDatabase;JLjava/lang/String;IZ)V
    .locals 25
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "playlistId"    # J
    .param p3, "serverPosition"    # Ljava/lang/String;
    .param p4, "startingPosition"    # I
    .param p5, "fullReset"    # Z

    .prologue
    .line 1668
    new-instance v23, Ljava/util/ArrayList;

    invoke-direct/range {v23 .. v23}, Ljava/util/ArrayList;-><init>()V

    .line 1669
    .local v23, "toUpdate":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    const/4 v4, 0x2

    new-array v6, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "Id"

    aput-object v5, v6, v4

    const/4 v4, 0x1

    const-string v5, "ClientPosition"

    aput-object v5, v6, v4

    .line 1672
    .local v6, "columns":[Ljava/lang/String;
    const/4 v12, 0x0

    .line 1673
    .local v12, "c":Landroid/database/Cursor;
    const/high16 v4, 0x20000

    add-int v14, p4, v4

    .line 1674
    .local v14, "desiredStartingPosition":I
    const/high16 v4, -0x80000000

    move/from16 v0, p4

    if-ne v0, v4, :cond_1

    .line 1675
    const/4 v14, 0x0

    .line 1676
    const-string v5, "LISTITEMS"

    const-string v7, "ListId=? AND ServerOrder=? "

    const/4 v4, 0x2

    new-array v8, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static/range {p1 .. p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v4

    const/4 v4, 0x1

    aput-object p3, v8, v4

    const/4 v9, 0x0

    const/4 v10, 0x0

    const-string v11, "ClientPosition"

    move-object/from16 v4, p0

    invoke-virtual/range {v4 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 1691
    :goto_0
    if-eqz v12, :cond_0

    .line 1699
    move v13, v14

    .line 1700
    .local v13, "desiredPosition":I
    :goto_1
    :try_start_0
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1701
    const/4 v4, 0x0

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v18

    .line 1702
    .local v18, "itemId":J
    const/4 v4, 0x1

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v21

    .line 1703
    .local v21, "position":I
    if-nez p5, :cond_2

    move/from16 v0, v21

    if-gt v13, v0, :cond_2

    .line 1715
    .end local v13    # "desiredPosition":I
    .end local v18    # "itemId":J
    .end local v21    # "position":I
    :cond_0
    invoke-static {v12}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 1719
    new-instance v24, Landroid/content/ContentValues;

    invoke-direct/range {v24 .. v24}, Landroid/content/ContentValues;-><init>()V

    .line 1720
    .local v24, "values":Landroid/content/ContentValues;
    const-string v22, "ListId=? AND Id=?"

    .line 1721
    .local v22, "selection":Ljava/lang/String;
    const/4 v4, 0x2

    new-array v0, v4, [Ljava/lang/String;

    move-object/from16 v20, v0

    const/4 v4, 0x0

    invoke-static/range {p1 .. p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v20, v4

    const/4 v4, 0x1

    const/4 v5, 0x0

    aput-object v5, v20, v4

    .line 1724
    .local v20, "params":[Ljava/lang/String;
    move v13, v14

    .line 1725
    .restart local v13    # "desiredPosition":I
    invoke-virtual/range {v23 .. v23}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v15

    .local v15, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    .line 1726
    .local v16, "id":J
    const-string v4, "ClientPosition"

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v24

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1727
    const/4 v4, 0x1

    invoke-static/range {v16 .. v17}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v20, v4

    .line 1729
    const-string v4, "LISTITEMS"

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    move-object/from16 v2, v22

    move-object/from16 v3, v20

    invoke-virtual {v0, v4, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1730
    const/high16 v4, 0x20000

    add-int/2addr v13, v4

    .line 1731
    goto :goto_2

    .line 1682
    .end local v13    # "desiredPosition":I
    .end local v15    # "i$":Ljava/util/Iterator;
    .end local v16    # "id":J
    .end local v20    # "params":[Ljava/lang/String;
    .end local v22    # "selection":Ljava/lang/String;
    .end local v24    # "values":Landroid/content/ContentValues;
    :cond_1
    const-string v5, "LISTITEMS"

    const-string v7, "ListId=? AND ServerOrder=? AND ClientPosition>?"

    const/4 v4, 0x3

    new-array v8, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static/range {p1 .. p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v4

    const/4 v4, 0x1

    aput-object p3, v8, v4

    const/4 v4, 0x2

    invoke-static/range {p4 .. p4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v4

    const/4 v9, 0x0

    const/4 v10, 0x0

    const-string v11, "ClientPosition"

    move-object/from16 v4, p0

    invoke-virtual/range {v4 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    goto/16 :goto_0

    .line 1708
    .restart local v13    # "desiredPosition":I
    .restart local v18    # "itemId":J
    .restart local v21    # "position":I
    :cond_2
    :try_start_1
    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, v23

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1711
    const/high16 v4, 0x20000

    add-int/2addr v13, v4

    .line 1712
    goto/16 :goto_1

    .line 1715
    .end local v18    # "itemId":J
    .end local v21    # "position":I
    :catchall_0
    move-exception v4

    invoke-static {v12}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v4

    .line 1732
    .restart local v15    # "i$":Ljava/util/Iterator;
    .restart local v20    # "params":[Ljava/lang/String;
    .restart local v22    # "selection":Ljava/lang/String;
    .restart local v24    # "values":Landroid/content/ContentValues;
    :cond_3
    return-void
.end method

.method private static setItemPosition(Landroid/database/sqlite/SQLiteDatabase;JJLjava/lang/String;IZ)V
    .locals 7
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "playlistId"    # J
    .param p3, "itemId"    # J
    .param p5, "serverPosition"    # Ljava/lang/String;
    .param p6, "clientPosition"    # I
    .param p7, "sync"    # Z

    .prologue
    const/4 v6, 0x1

    .line 1594
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1595
    .local v0, "values":Landroid/content/ContentValues;
    if-nez p5, :cond_1

    .line 1596
    const-string v1, "ServerOrder"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1600
    :goto_0
    const-string v1, "ClientPosition"

    invoke-static {p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1601
    if-eqz p7, :cond_0

    .line 1602
    const-string v1, "_sync_dirty"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1604
    :cond_0
    const-string v1, "LISTITEMS"

    const-string v2, "ListId=? AND Id=?"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {p3, p4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {p0, v1, v0, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1607
    return-void

    .line 1598
    :cond_1
    const-string v1, "ServerOrder"

    invoke-virtual {v0, v1, p5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method delete(Landroid/database/sqlite/SQLiteDatabase;ZZ)Z
    .locals 10
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "sync"    # Z
    .param p3, "deleteItems"    # Z

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 509
    iget-wide v6, p0, Lcom/google/android/music/store/PlayList;->mId:J

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-nez v5, :cond_0

    .line 510
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "Cannot delete object that was not loaded or created"

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 513
    :cond_0
    iget-wide v6, p0, Lcom/google/android/music/store/PlayList;->mId:J

    invoke-static {p1, v6, v7}, Lcom/google/android/music/store/PlayList;->deleteById(Landroid/database/sqlite/SQLiteDatabase;J)I

    move-result v1

    .line 515
    .local v1, "numberOfDeletedLists":I
    if-lez v1, :cond_4

    move v2, v3

    .line 516
    .local v2, "playlistDeleted":Z
    :goto_0
    if-le v1, v3, :cond_1

    .line 517
    const-string v5, "PlayList"

    const-string v6, "Deleted multiple objects"

    invoke-static {v5, v6}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 520
    :cond_1
    if-eqz p2, :cond_2

    if-eqz v2, :cond_2

    .line 521
    const-string v5, "LIST_TOMBSTONES"

    invoke-virtual {p0, p1, v5}, Lcom/google/android/music/store/PlayList;->createTombstoneIfNeeded(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Z

    .line 524
    :cond_2
    if-eqz p3, :cond_3

    if-eqz v2, :cond_3

    .line 525
    new-array v0, v3, [Ljava/lang/String;

    iget-wide v6, p0, Lcom/google/android/music/store/PlayList;->mId:J

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v4

    .line 526
    .local v0, "args":[Ljava/lang/String;
    const-string v3, "LISTITEMS"

    const-string v4, "ListId=?"

    invoke-virtual {p1, v3, v4, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 531
    .end local v0    # "args":[Ljava/lang/String;
    :cond_3
    return v2

    .end local v2    # "playlistDeleted":Z
    :cond_4
    move v2, v4

    .line 515
    goto :goto_0
.end method

.method public getArtworkLocation()Ljava/lang/String;
    .locals 1

    .prologue
    .line 287
    iget-object v0, p0, Lcom/google/android/music/store/PlayList;->mArtworkLocation:Ljava/lang/String;

    return-object v0
.end method

.method public final getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, Lcom/google/android/music/store/PlayList;->mDescription:Ljava/lang/String;

    return-object v0
.end method

.method public final getId()J
    .locals 2

    .prologue
    .line 267
    iget-wide v0, p0, Lcom/google/android/music/store/PlayList;->mId:J

    return-wide v0
.end method

.method public final getMediaStoreId()J
    .locals 2

    .prologue
    .line 295
    iget-wide v0, p0, Lcom/google/android/music/store/PlayList;->mMediaStoreId:J

    return-wide v0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 271
    iget-object v0, p0, Lcom/google/android/music/store/PlayList;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public final getOwnerName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 311
    iget-object v0, p0, Lcom/google/android/music/store/PlayList;->mOwnerName:Ljava/lang/String;

    return-object v0
.end method

.method public final getOwnerProfilePhotoUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 327
    iget-object v0, p0, Lcom/google/android/music/store/PlayList;->mOwnerProfilePhotoUrl:Ljava/lang/String;

    return-object v0
.end method

.method public final getShareToken()Ljava/lang/String;
    .locals 1

    .prologue
    .line 303
    iget-object v0, p0, Lcom/google/android/music/store/PlayList;->mShareToken:Ljava/lang/String;

    return-object v0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 279
    iget v0, p0, Lcom/google/android/music/store/PlayList;->mType:I

    return v0
.end method

.method public insertList(Landroid/database/sqlite/SQLiteStatement;)J
    .locals 6
    .param p1, "insertStatement"    # Landroid/database/sqlite/SQLiteStatement;

    .prologue
    .line 446
    iget-wide v2, p0, Lcom/google/android/music/store/PlayList;->mId:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 447
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Playlist has a local id.  Forgot to call reset()?"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 449
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/music/store/PlayList;->prepareInsertOrFullUpdate(Landroid/database/sqlite/SQLiteStatement;)V

    .line 451
    :try_start_0
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteStatement;->executeInsert()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/music/store/PlayList;->mId:J
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_0 .. :try_end_0} :catch_0

    .line 458
    iget-wide v2, p0, Lcom/google/android/music/store/PlayList;->mId:J

    :goto_0
    return-wide v2

    .line 452
    :catch_0
    move-exception v0

    .line 454
    .local v0, "e":Landroid/database/sqlite/SQLiteConstraintException;
    const-string v1, "PlayList"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to insert into LISTS. Name: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/music/store/PlayList;->mName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", ShareToken: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/music/store/PlayList;->mShareToken:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 456
    const-wide/16 v2, -0x1

    goto :goto_0
.end method

.method public populateFromFullProjectionCursor(Landroid/database/Cursor;)V
    .locals 7
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x3

    const/4 v1, 0x0

    const/4 v0, 0x1

    const/4 v4, 0x0

    .line 535
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/music/store/PlayList;->mId:J

    .line 536
    const/4 v2, 0x2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/store/PlayList;->mName:Ljava/lang/String;

    .line 537
    const/4 v2, 0x7

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, p0, Lcom/google/android/music/store/PlayList;->mType:I

    .line 538
    invoke-interface {p1, v6}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 539
    iput-object v4, p0, Lcom/google/android/music/store/PlayList;->mArtworkLocation:Ljava/lang/String;

    .line 543
    :goto_0
    invoke-interface {p1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 544
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/google/android/music/store/PlayList;->mMediaStoreId:J

    .line 548
    :goto_1
    invoke-interface {p1, v5}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 549
    iput v1, p0, Lcom/google/android/music/store/PlayList;->mSourceAccount:I

    .line 553
    :goto_2
    const/4 v2, 0x4

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/store/PlayList;->mSourceVersion:Ljava/lang/String;

    .line 554
    const/4 v2, 0x5

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/store/PlayList;->mSourceId:Ljava/lang/String;

    .line 555
    const/4 v2, 0x6

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-ne v2, v0, :cond_3

    :goto_3
    iput-boolean v0, p0, Lcom/google/android/music/store/PlayList;->mNeedsSync:Z

    .line 556
    const/16 v0, 0x9

    invoke-interface {p1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 557
    iput-object v4, p0, Lcom/google/android/music/store/PlayList;->mShareToken:Ljava/lang/String;

    .line 561
    :goto_4
    const/16 v0, 0xa

    invoke-interface {p1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 562
    iput-object v4, p0, Lcom/google/android/music/store/PlayList;->mOwnerName:Ljava/lang/String;

    .line 566
    :goto_5
    const/16 v0, 0xb

    invoke-interface {p1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 567
    iput-object v4, p0, Lcom/google/android/music/store/PlayList;->mDescription:Ljava/lang/String;

    .line 571
    :goto_6
    const/16 v0, 0xc

    invoke-interface {p1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 572
    iput-object v4, p0, Lcom/google/android/music/store/PlayList;->mOwnerProfilePhotoUrl:Ljava/lang/String;

    .line 576
    :goto_7
    const/16 v0, 0xe

    invoke-interface {p1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 577
    iput-object v4, p0, Lcom/google/android/music/store/PlayList;->mEditorArtworkUrl:Ljava/lang/String;

    .line 581
    :goto_8
    return-void

    .line 541
    :cond_0
    invoke-interface {p1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/music/store/PlayList;->mArtworkLocation:Ljava/lang/String;

    goto :goto_0

    .line 546
    :cond_1
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/music/store/PlayList;->mMediaStoreId:J

    goto :goto_1

    .line 551
    :cond_2
    invoke-interface {p1, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, p0, Lcom/google/android/music/store/PlayList;->mSourceAccount:I

    goto :goto_2

    :cond_3
    move v0, v1

    .line 555
    goto :goto_3

    .line 559
    :cond_4
    const/16 v0, 0x9

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/store/PlayList;->mShareToken:Ljava/lang/String;

    goto :goto_4

    .line 564
    :cond_5
    const/16 v0, 0xa

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/store/PlayList;->mOwnerName:Ljava/lang/String;

    goto :goto_5

    .line 569
    :cond_6
    const/16 v0, 0xb

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/store/PlayList;->mDescription:Ljava/lang/String;

    goto :goto_6

    .line 574
    :cond_7
    const/16 v0, 0xc

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/store/PlayList;->mOwnerProfilePhotoUrl:Ljava/lang/String;

    goto :goto_7

    .line 579
    :cond_8
    const/16 v0, 0xe

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/store/PlayList;->mEditorArtworkUrl:Ljava/lang/String;

    goto :goto_8
.end method

.method public populateFromTombstoneProjectionCursor(Landroid/database/Cursor;)V
    .locals 2
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    .line 584
    sget v0, Lcom/google/android/music/store/PlayList;->TOMBSTONE_PROJECTION_INDEX_ID:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/music/store/PlayList;->mId:J

    .line 585
    sget v0, Lcom/google/android/music/store/PlayList;->TOMBSTONE_PROJECTION_SOURCE_ID:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 586
    sget v0, Lcom/google/android/music/store/PlayList;->TOMBSTONE_PROJECTION_SOURCE_ID:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/store/PlayList;->mSourceId:Ljava/lang/String;

    .line 588
    :cond_0
    sget v0, Lcom/google/android/music/store/PlayList;->TOMBSTONE_PROJECTION_SOURCE_VERSION:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 589
    sget v0, Lcom/google/android/music/store/PlayList;->TOMBSTONE_PROJECTION_SOURCE_VERSION:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/store/PlayList;->mSourceVersion:Ljava/lang/String;

    .line 591
    :cond_1
    return-void
.end method

.method public reset()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 254
    invoke-super {p0}, Lcom/google/android/music/store/Syncable;->reset()V

    .line 255
    iput-wide v2, p0, Lcom/google/android/music/store/PlayList;->mId:J

    .line 256
    iput-wide v2, p0, Lcom/google/android/music/store/PlayList;->mMediaStoreId:J

    .line 257
    iput-object v1, p0, Lcom/google/android/music/store/PlayList;->mName:Ljava/lang/String;

    .line 258
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/music/store/PlayList;->mType:I

    .line 259
    iput-object v1, p0, Lcom/google/android/music/store/PlayList;->mArtworkLocation:Ljava/lang/String;

    .line 260
    iput-object v1, p0, Lcom/google/android/music/store/PlayList;->mShareToken:Ljava/lang/String;

    .line 261
    iput-object v1, p0, Lcom/google/android/music/store/PlayList;->mOwnerName:Ljava/lang/String;

    .line 262
    iput-object v1, p0, Lcom/google/android/music/store/PlayList;->mDescription:Ljava/lang/String;

    .line 263
    iput-object v1, p0, Lcom/google/android/music/store/PlayList;->mOwnerProfilePhotoUrl:Ljava/lang/String;

    .line 264
    return-void
.end method

.method public setArtworkLocation(Ljava/lang/String;)V
    .locals 0
    .param p1, "artworkLocation"    # Ljava/lang/String;

    .prologue
    .line 291
    iput-object p1, p0, Lcom/google/android/music/store/PlayList;->mArtworkLocation:Ljava/lang/String;

    .line 292
    return-void
.end method

.method public final setDescription(Ljava/lang/String;)V
    .locals 0
    .param p1, "description"    # Ljava/lang/String;

    .prologue
    .line 323
    iput-object p1, p0, Lcom/google/android/music/store/PlayList;->mDescription:Ljava/lang/String;

    .line 324
    return-void
.end method

.method public final setEditorArtworkUrl(Ljava/lang/String;)V
    .locals 0
    .param p1, "editorArtworkUrl"    # Ljava/lang/String;

    .prologue
    .line 339
    iput-object p1, p0, Lcom/google/android/music/store/PlayList;->mEditorArtworkUrl:Ljava/lang/String;

    .line 340
    return-void
.end method

.method public final setMediaStoreId(J)V
    .locals 1
    .param p1, "mediaStoreId"    # J

    .prologue
    .line 299
    iput-wide p1, p0, Lcom/google/android/music/store/PlayList;->mMediaStoreId:J

    .line 300
    return-void
.end method

.method public final setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 275
    iput-object p1, p0, Lcom/google/android/music/store/PlayList;->mName:Ljava/lang/String;

    .line 276
    return-void
.end method

.method public final setOwnerName(Ljava/lang/String;)V
    .locals 0
    .param p1, "ownerName"    # Ljava/lang/String;

    .prologue
    .line 315
    iput-object p1, p0, Lcom/google/android/music/store/PlayList;->mOwnerName:Ljava/lang/String;

    .line 316
    return-void
.end method

.method public final setOwnerProfilePhotoUrl(Ljava/lang/String;)V
    .locals 0
    .param p1, "ownerProfilePhotoUrl"    # Ljava/lang/String;

    .prologue
    .line 331
    iput-object p1, p0, Lcom/google/android/music/store/PlayList;->mOwnerProfilePhotoUrl:Ljava/lang/String;

    .line 332
    return-void
.end method

.method public final setShareToken(Ljava/lang/String;)V
    .locals 0
    .param p1, "shareToken"    # Ljava/lang/String;

    .prologue
    .line 307
    iput-object p1, p0, Lcom/google/android/music/store/PlayList;->mShareToken:Ljava/lang/String;

    .line 308
    return-void
.end method

.method public setType(I)V
    .locals 0
    .param p1, "type"    # I

    .prologue
    .line 283
    iput p1, p0, Lcom/google/android/music/store/PlayList;->mType:I

    .line 284
    return-void
.end method

.method public update(Landroid/database/sqlite/SQLiteStatement;)V
    .locals 4
    .param p1, "updateStatement"    # Landroid/database/sqlite/SQLiteStatement;

    .prologue
    .line 468
    iget-wide v0, p0, Lcom/google/android/music/store/PlayList;->mId:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 469
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot update object that has not been created"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 472
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/music/store/PlayList;->prepareInsertOrFullUpdate(Landroid/database/sqlite/SQLiteStatement;)V

    .line 473
    const/16 v0, 0xf

    iget-wide v2, p0, Lcom/google/android/music/store/PlayList;->mId:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 474
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteStatement;->execute()V

    .line 475
    return-void
.end method
