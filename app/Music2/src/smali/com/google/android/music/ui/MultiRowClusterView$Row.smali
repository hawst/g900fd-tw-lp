.class Lcom/google/android/music/ui/MultiRowClusterView$Row;
.super Landroid/widget/LinearLayout;
.source "MultiRowClusterView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/ui/MultiRowClusterView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Row"
.end annotation


# instance fields
.field protected mCards:[Lcom/google/android/music/ui/cardlib/layout/PlayCardView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 194
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 195
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/music/ui/MultiRowClusterView$Row;->setOrientation(I)V

    .line 196
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 198
    .local v0, "params":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {p0, v0}, Lcom/google/android/music/ui/MultiRowClusterView$Row;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 199
    return-void
.end method


# virtual methods
.method public putCard(Lcom/google/android/music/ui/cardlib/layout/PlayCardView;I)V
    .locals 1
    .param p1, "view"    # Lcom/google/android/music/ui/cardlib/layout/PlayCardView;
    .param p2, "index"    # I

    .prologue
    .line 207
    iget-object v0, p0, Lcom/google/android/music/ui/MultiRowClusterView$Row;->mCards:[Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    aput-object p1, v0, p2

    .line 208
    invoke-virtual {p0, p1, p2}, Lcom/google/android/music/ui/MultiRowClusterView$Row;->addView(Landroid/view/View;I)V

    .line 209
    return-void
.end method

.method public reset(I)V
    .locals 1
    .param p1, "capacity"    # I

    .prologue
    .line 202
    invoke-virtual {p0}, Lcom/google/android/music/ui/MultiRowClusterView$Row;->removeAllViews()V

    .line 203
    new-array v0, p1, [Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    iput-object v0, p0, Lcom/google/android/music/ui/MultiRowClusterView$Row;->mCards:[Lcom/google/android/music/ui/cardlib/layout/PlayCardView;

    .line 204
    return-void
.end method
