.class public final Lcom/google/android/music/store/MusicContent;
.super Lcom/google/android/music/api/MusicContentApi;
.source "MusicContent.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/music/store/MusicContent$1;,
        Lcom/google/android/music/store/MusicContent$SituationRadios;,
        Lcom/google/android/music/store/MusicContent$Situations;,
        Lcom/google/android/music/store/MusicContent$SituationsHeader;,
        Lcom/google/android/music/store/MusicContent$MusicUserContentArt;,
        Lcom/google/android/music/store/MusicContent$PlaylistShareState;,
        Lcom/google/android/music/store/MusicContent$Queue;,
        Lcom/google/android/music/store/MusicContent$SharedWithMePlaylist;,
        Lcom/google/android/music/store/MusicContent$RadioStations;,
        Lcom/google/android/music/store/MusicContent$Explore;,
        Lcom/google/android/music/store/MusicContent$DownloadContainers;,
        Lcom/google/android/music/store/MusicContent$SuggestedSeeds;,
        Lcom/google/android/music/store/MusicContent$Mainstage;,
        Lcom/google/android/music/store/MusicContent$Recent;,
        Lcom/google/android/music/store/MusicContent$KeepOn;,
        Lcom/google/android/music/store/MusicContent$UrlArt;,
        Lcom/google/android/music/store/MusicContent$PlaylistArt;,
        Lcom/google/android/music/store/MusicContent$AlbumArt;,
        Lcom/google/android/music/store/MusicContent$SearchSuggestedQuery;,
        Lcom/google/android/music/store/MusicContent$Search;,
        Lcom/google/android/music/store/MusicContent$Genres;,
        Lcom/google/android/music/store/MusicContent$Artists;,
        Lcom/google/android/music/store/MusicContent$Albums;,
        Lcom/google/android/music/store/MusicContent$XAudio;,
        Lcom/google/android/music/store/MusicContent$Playlists;,
        Lcom/google/android/music/store/MusicContent$AutoPlaylists;,
        Lcom/google/android/music/store/MusicContent$PlaylistMemberColumns;,
        Lcom/google/android/music/store/MusicContent$PlaylistColumns;,
        Lcom/google/android/music/store/MusicContent$CommonColumns;
    }
.end annotation


# static fields
.field private static final COUNT_COLUMNS:[Ljava/lang/String;

.field public static final DOWNLOAD_QUEUE_URI:Landroid/net/Uri;

.field public static final KEEP_ON_URI:Landroid/net/Uri;

.field public static final PARAM_FILTER_USER_ALL:Ljava/lang/String;

.field public static final PARAM_FILTER_USER_AND_EXTERNAL_ALL:Ljava/lang/String;

.field public static final PARAM_FILTER_USER_AND_EXTERNAL_LOCAL_AND_KEPT:Ljava/lang/String;

.field public static final PARAM_FILTER_USER_AND_EXTERNAL_LOCAL_KEPT_AND_CACHED:Ljava/lang/String;

.field public static final PARAM_FILTER_USER_LOCAL_AND_KEPT:Ljava/lang/String;

.field public static final PARAM_FILTER_USER_LOCAL_KEPT_AND_CACHED:Ljava/lang/String;

.field public static final STREAM_URI:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 43
    sget-object v0, Lcom/google/android/music/store/MusicContent;->CONTENT_URI:Landroid/net/Uri;

    const-string v1, "play"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/store/MusicContent;->STREAM_URI:Landroid/net/Uri;

    .line 133
    sget-object v0, Lcom/google/android/music/store/MusicContent;->CONTENT_URI:Landroid/net/Uri;

    const-string v1, "KeepOn"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/store/MusicContent;->KEEP_ON_URI:Landroid/net/Uri;

    .line 135
    sget-object v0, Lcom/google/android/music/store/MusicContent;->CONTENT_URI:Landroid/net/Uri;

    const-string v1, "DownloadQueue"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/store/MusicContent;->DOWNLOAD_QUEUE_URI:Landroid/net/Uri;

    .line 193
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/store/MusicContent;->PARAM_FILTER_USER_AND_EXTERNAL_ALL:Ljava/lang/String;

    .line 195
    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/store/MusicContent;->PARAM_FILTER_USER_AND_EXTERNAL_LOCAL_AND_KEPT:Ljava/lang/String;

    .line 197
    const/4 v0, 0x2

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/store/MusicContent;->PARAM_FILTER_USER_AND_EXTERNAL_LOCAL_KEPT_AND_CACHED:Ljava/lang/String;

    .line 199
    const/4 v0, 0x3

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/store/MusicContent;->PARAM_FILTER_USER_ALL:Ljava/lang/String;

    .line 201
    const/4 v0, 0x4

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/store/MusicContent;->PARAM_FILTER_USER_LOCAL_AND_KEPT:Ljava/lang/String;

    .line 203
    const/4 v0, 0x5

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/store/MusicContent;->PARAM_FILTER_USER_LOCAL_KEPT_AND_CACHED:Ljava/lang/String;

    .line 226
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "_count"

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/android/music/store/MusicContent;->COUNT_COLUMNS:[Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Landroid/net/Uri$Builder;II)V
    .locals 0
    .param p0, "x0"    # Landroid/net/Uri$Builder;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 37
    invoke-static {p0, p1, p2}, Lcom/google/android/music/store/MusicContent;->appendWidthAndHeightIfAvailable(Landroid/net/Uri$Builder;II)V

    return-void
.end method

.method public static addExternalSongsToStore(Landroid/content/Context;Landroid/net/Uri;Z)[J
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "addToLibrary"    # Z

    .prologue
    const/4 v3, 0x0

    .line 340
    if-nez p1, :cond_1

    .line 341
    const-string v6, "MusicContent"

    const-string v7, "Uri is empty."

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 370
    :cond_0
    :goto_0
    return-object v3

    .line 344
    :cond_1
    invoke-virtual {p1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v6

    const-string v7, "addToLibrary"

    invoke-static {p2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object p1

    .line 347
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    .line 348
    .local v5, "resolver":Landroid/content/ContentResolver;
    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    invoke-virtual {v5, p1, v6}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v4

    .line 349
    .local v4, "newUri":Landroid/net/Uri;
    if-nez v4, :cond_2

    .line 350
    const-string v6, "MusicContent"

    const-string v7, "New Uri is null"

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 355
    :cond_2
    invoke-virtual {v4}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    .line 356
    .local v1, "lastSeg":Ljava/lang/String;
    const/4 v3, 0x0

    .line 357
    .local v3, "longLocalIds":[J
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 358
    const-string v6, ",\\s*"

    invoke-virtual {v1, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 361
    .local v2, "localIds":[Ljava/lang/String;
    array-length v6, v2

    new-array v3, v6, [J

    .line 362
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    array-length v6, v2

    if-ge v0, v6, :cond_0

    .line 363
    aget-object v6, v2, v0

    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    aput-wide v6, v3, v0

    .line 362
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 366
    .end local v0    # "i":I
    .end local v2    # "localIds":[Ljava/lang/String;
    :cond_3
    const-string v6, "MusicContent"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Last path segment doesn\'t contain the localIds: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 367
    const/4 v6, 0x0

    new-array v3, v6, [J

    goto :goto_0
.end method

.method public static addLimitParam(Landroid/net/Uri;II)Landroid/net/Uri;
    .locals 4
    .param p0, "uri"    # Landroid/net/Uri;
    .param p1, "offset"    # I
    .param p2, "length"    # I

    .prologue
    .line 385
    invoke-virtual {p0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "limit"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static addNotifyParam(Landroid/net/Uri;)Landroid/net/Uri;
    .locals 3
    .param p0, "uri"    # Landroid/net/Uri;

    .prologue
    .line 398
    invoke-virtual {p0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "notify_on_delete"

    const-string v2, "false"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static appendFilter(Landroid/content/Context;Landroid/net/Uri;ZZ)Landroid/net/Uri;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "shouldFilter"    # Z
    .param p3, "includeExternal"    # Z

    .prologue
    .line 324
    if-nez p2, :cond_0

    if-nez p3, :cond_1

    .line 325
    :cond_0
    invoke-virtual {p1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "filter"

    invoke-static {p0, p2, p3}, Lcom/google/android/music/store/Filters;->getMusicFilterIndex(Landroid/content/Context;ZZ)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object p1

    .line 329
    .end local p1    # "uri":Landroid/net/Uri;
    :cond_1
    return-object p1
.end method

.method private static appendWidthAndHeightIfAvailable(Landroid/net/Uri$Builder;II)V
    .locals 2
    .param p0, "builder"    # Landroid/net/Uri$Builder;
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 2356
    if-lez p1, :cond_0

    if-lez p2, :cond_0

    .line 2357
    const-string v0, "w"

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2358
    const-string v0, "h"

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2360
    :cond_0
    return-void
.end method

.method public static deleteAllRemoteContent(Landroid/content/ContentResolver;)V
    .locals 3
    .param p0, "resolver"    # Landroid/content/ContentResolver;

    .prologue
    const/4 v2, 0x0

    .line 307
    sget-object v0, Lcom/google/android/music/store/MusicContent;->CONTENT_URI:Landroid/net/Uri;

    const-string v1, "remote"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v0, v2, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 308
    return-void
.end method

.method public static deletePersistentNautilusContentFromAlbum(Landroid/content/Context;J)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "albumId"    # J

    .prologue
    const/4 v5, 0x0

    .line 311
    invoke-static {p1, p2}, Lcom/google/android/music/store/MusicContent$Albums;->getAlbumsUri(J)Landroid/net/Uri;

    move-result-object v1

    .line 312
    .local v1, "uri":Landroid/net/Uri;
    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "deleteMode"

    const-string v4, "NAUTILUS"

    invoke-virtual {v2, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 315
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 316
    .local v0, "resolver":Landroid/content/ContentResolver;
    invoke-virtual {v0, v1, v5, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 317
    return-void
.end method

.method public static existsContent(Landroid/content/Context;Landroid/net/Uri;Z)Z
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "shouldIncludeExternal"    # Z

    .prologue
    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v9, 0x0

    .line 287
    sget-object v2, Lcom/google/android/music/store/MusicContent$CommonColumns;->EXISTS_PROJECTION:[Ljava/lang/String;

    move-object v0, p0

    move-object v1, p1

    move-object v4, v3

    move-object v5, v3

    move v7, p2

    invoke-static/range {v0 .. v7}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZZ)Landroid/database/Cursor;

    move-result-object v8

    .line 290
    .local v8, "c":Landroid/database/Cursor;
    if-nez v8, :cond_0

    .line 296
    :goto_0
    return v9

    .line 294
    :cond_0
    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-ne v0, v6, :cond_1

    .line 296
    :goto_1
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    move v9, v6

    goto :goto_0

    :cond_1
    move v6, v9

    .line 294
    goto :goto_1

    .line 296
    :catchall_0
    move-exception v0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public static getCount(Landroid/content/Context;Landroid/net/Uri;ZZ)I
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "shouldFilter"    # Z
    .param p3, "includeExternal"    # Z

    .prologue
    const/4 v3, 0x0

    .line 235
    const/4 v9, 0x0

    .line 236
    .local v9, "count":I
    sget-object v2, Lcom/google/android/music/store/MusicContent;->COUNT_COLUMNS:[Ljava/lang/String;

    move-object v0, p0

    move-object v1, p1

    move-object v4, v3

    move-object v5, v3

    move v6, p2

    move v7, p3

    invoke-static/range {v0 .. v7}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZZ)Landroid/database/Cursor;

    move-result-object v8

    .line 239
    .local v8, "c":Landroid/database/Cursor;
    if-eqz v8, :cond_0

    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 240
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v9

    .line 243
    :cond_0
    invoke-static {v8}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    .line 245
    return v9

    .line 243
    :catchall_0
    move-exception v0

    invoke-static {v8}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method

.method protected static getNautilusCount(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Z)I
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "metajamColumnId"    # Ljava/lang/String;
    .param p3, "shouldFilter"    # Z

    .prologue
    const/4 v7, 0x1

    const/4 v9, 0x0

    const/4 v3, 0x0

    .line 257
    new-array v2, v7, [Ljava/lang/String;

    aput-object p2, v2, v9

    move-object v0, p0

    move-object v1, p1

    move-object v4, v3

    move-object v5, v3

    move v6, p3

    invoke-static/range {v0 .. v7}, Lcom/google/android/music/utils/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZZ)Landroid/database/Cursor;

    move-result-object v8

    .line 260
    .local v8, "c":Landroid/database/Cursor;
    if-eqz v8, :cond_0

    .line 261
    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 266
    invoke-static {v8}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    :goto_0
    return v0

    :cond_0
    invoke-static {v8}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    move v0, v9

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v8}, Lcom/google/android/music/store/Store;->safeClose(Landroid/database/Cursor;)V

    throw v0
.end method
