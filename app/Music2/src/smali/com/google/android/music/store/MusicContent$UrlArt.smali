.class public final Lcom/google/android/music/store/MusicContent$UrlArt;
.super Ljava/lang/Object;
.source "MusicContent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/store/MusicContent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "UrlArt"
.end annotation


# static fields
.field private static final PARAM_MODE_VALUE_ASYNC_DOWNLOAD:Ljava/lang/String;

.field private static final PARAM_MODE_VALUE_NO_DOWNLOAD:Ljava/lang/String;

.field private static final URL_ART_CONTENT_URI:Landroid/net/Uri;

.field private static final sArtLoaderModeMap:Lcom/google/common/collect/BiMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/BiMap",
            "<",
            "Lcom/google/android/music/art/ArtLoader$DownloadMode;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2437
    sget-object v0, Lcom/google/android/music/store/MusicContent;->CONTENT_URI:Landroid/net/Uri;

    const-string v1, "urlart"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/store/MusicContent$UrlArt;->URL_ART_CONTENT_URI:Landroid/net/Uri;

    .line 2445
    sget-object v0, Lcom/google/android/music/art/ArtLoader$DownloadMode;->NO_DOWNLOAD:Lcom/google/android/music/art/ArtLoader$DownloadMode;

    invoke-virtual {v0}, Lcom/google/android/music/art/ArtLoader$DownloadMode;->ordinal()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/store/MusicContent$UrlArt;->PARAM_MODE_VALUE_NO_DOWNLOAD:Ljava/lang/String;

    .line 2447
    sget-object v0, Lcom/google/android/music/art/ArtLoader$DownloadMode;->ASYNC_DOWNLOAD:Lcom/google/android/music/art/ArtLoader$DownloadMode;

    invoke-virtual {v0}, Lcom/google/android/music/art/ArtLoader$DownloadMode;->ordinal()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/store/MusicContent$UrlArt;->PARAM_MODE_VALUE_ASYNC_DOWNLOAD:Ljava/lang/String;

    .line 2450
    const-class v0, Lcom/google/android/music/art/ArtLoader$DownloadMode;

    invoke-static {v0}, Lcom/google/common/collect/EnumHashBiMap;->create(Ljava/lang/Class;)Lcom/google/common/collect/EnumHashBiMap;

    move-result-object v0

    sput-object v0, Lcom/google/android/music/store/MusicContent$UrlArt;->sArtLoaderModeMap:Lcom/google/common/collect/BiMap;

    .line 2454
    sget-object v0, Lcom/google/android/music/store/MusicContent$UrlArt;->sArtLoaderModeMap:Lcom/google/common/collect/BiMap;

    sget-object v1, Lcom/google/android/music/art/ArtLoader$DownloadMode;->NO_DOWNLOAD:Lcom/google/android/music/art/ArtLoader$DownloadMode;

    sget-object v2, Lcom/google/android/music/store/MusicContent$UrlArt;->PARAM_MODE_VALUE_NO_DOWNLOAD:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/google/common/collect/BiMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2455
    sget-object v0, Lcom/google/android/music/store/MusicContent$UrlArt;->sArtLoaderModeMap:Lcom/google/common/collect/BiMap;

    sget-object v1, Lcom/google/android/music/art/ArtLoader$DownloadMode;->ASYNC_DOWNLOAD:Lcom/google/android/music/art/ArtLoader$DownloadMode;

    sget-object v2, Lcom/google/android/music/store/MusicContent$UrlArt;->PARAM_MODE_VALUE_ASYNC_DOWNLOAD:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/google/common/collect/BiMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2456
    return-void
.end method

.method static getMode(Landroid/net/Uri;)Lcom/google/android/music/art/ArtLoader$DownloadMode;
    .locals 2
    .param p0, "uri"    # Landroid/net/Uri;

    .prologue
    .line 2492
    const-string v1, "mode"

    invoke-virtual {p0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2493
    .local v0, "modeString":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2494
    const/4 v1, 0x0

    .line 2497
    :goto_0
    return-object v1

    :cond_0
    sget-object v1, Lcom/google/android/music/store/MusicContent$UrlArt;->sArtLoaderModeMap:Lcom/google/common/collect/BiMap;

    invoke-interface {v1}, Lcom/google/common/collect/BiMap;->inverse()Lcom/google/common/collect/BiMap;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/common/collect/BiMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/music/art/ArtLoader$DownloadMode;

    goto :goto_0
.end method

.method static getModeString(Lcom/google/android/music/art/ArtLoader$DownloadMode;)Ljava/lang/String;
    .locals 2
    .param p0, "mode"    # Lcom/google/android/music/art/ArtLoader$DownloadMode;

    .prologue
    .line 2505
    sget-object v1, Lcom/google/android/music/store/MusicContent$UrlArt;->sArtLoaderModeMap:Lcom/google/common/collect/BiMap;

    invoke-interface {v1, p0}, Lcom/google/common/collect/BiMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2506
    .local v0, "result":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2508
    sget-object v0, Lcom/google/android/music/store/MusicContent$UrlArt;->PARAM_MODE_VALUE_ASYNC_DOWNLOAD:Ljava/lang/String;

    .line 2511
    :cond_0
    return-object v0
.end method

.method public static getRemoteUrl(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p0, "uri"    # Landroid/net/Uri;

    .prologue
    .line 2501
    const-string v0, "url"

    invoke-virtual {p0, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static openFileDescriptor(Landroid/content/Context;Ljava/lang/String;II)Landroid/os/ParcelFileDescriptor;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "remoteUrl"    # Ljava/lang/String;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 2463
    sget-object v0, Lcom/google/android/music/art/ArtLoader$DownloadMode;->ASYNC_DOWNLOAD:Lcom/google/android/music/art/ArtLoader$DownloadMode;

    invoke-static {p0, p1, v0, p2, p3}, Lcom/google/android/music/store/MusicContent$UrlArt;->openFileDescriptor(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/music/art/ArtLoader$DownloadMode;II)Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    return-object v0
.end method

.method public static openFileDescriptor(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/music/art/ArtLoader$DownloadMode;II)Landroid/os/ParcelFileDescriptor;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "remoteUrl"    # Ljava/lang/String;
    .param p2, "mode"    # Lcom/google/android/music/art/ArtLoader$DownloadMode;
    .param p3, "width"    # I
    .param p4, "height"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 2471
    sget-object v3, Lcom/google/android/music/store/MusicContent$UrlArt;->URL_ART_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 2472
    .local v0, "builder":Landroid/net/Uri$Builder;
    const-string v3, "url"

    invoke-virtual {v0, v3, p1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2473
    const-string v3, "w"

    invoke-static {p3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2474
    const-string v3, "h"

    invoke-static {p4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2475
    sget-object v3, Lcom/google/android/music/store/MusicContent$UrlArt;->sArtLoaderModeMap:Lcom/google/common/collect/BiMap;

    invoke-interface {v3, p2}, Lcom/google/common/collect/BiMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 2476
    .local v2, "modeString":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2478
    sget-object v2, Lcom/google/android/music/store/MusicContent$UrlArt;->PARAM_MODE_VALUE_ASYNC_DOWNLOAD:Ljava/lang/String;

    .line 2480
    :cond_0
    const-string v3, "mode"

    invoke-virtual {v0, v3, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2482
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v4

    const-string v5, "r"

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentResolver;->openFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 2487
    :goto_0
    return-object v3

    .line 2483
    :catch_0
    move-exception v1

    .line 2487
    .local v1, "e":Ljava/lang/NullPointerException;
    const/4 v3, 0x0

    goto :goto_0
.end method
