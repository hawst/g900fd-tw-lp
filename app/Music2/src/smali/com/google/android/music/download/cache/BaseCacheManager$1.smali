.class Lcom/google/android/music/download/cache/BaseCacheManager$1;
.super Ljava/lang/Object;
.source "BaseCacheManager.java"

# interfaces
.implements Lcom/google/android/music/download/cache/FilteredFileDeleter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/download/cache/BaseCacheManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/download/cache/BaseCacheManager;


# direct methods
.method constructor <init>(Lcom/google/android/music/download/cache/BaseCacheManager;)V
    .locals 0

    .prologue
    .line 67
    iput-object p1, p0, Lcom/google/android/music/download/cache/BaseCacheManager$1;->this$0:Lcom/google/android/music/download/cache/BaseCacheManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getFilteredIds()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/music/download/ContentIdentifier;",
            ">;"
        }
    .end annotation

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/music/download/cache/BaseCacheManager$1;->this$0:Lcom/google/android/music/download/cache/BaseCacheManager;

    # invokes: Lcom/google/android/music/download/cache/BaseCacheManager;->handleGetFilteredIds()Ljava/util/HashSet;
    invoke-static {v0}, Lcom/google/android/music/download/cache/BaseCacheManager;->access$100(Lcom/google/android/music/download/cache/BaseCacheManager;)Ljava/util/HashSet;

    move-result-object v0

    return-object v0
.end method

.method public requestDeleteFile(Ljava/io/File;)Z
    .locals 1
    .param p1, "file"    # Ljava/io/File;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/music/download/cache/BaseCacheManager$1;->this$0:Lcom/google/android/music/download/cache/BaseCacheManager;

    # invokes: Lcom/google/android/music/download/cache/BaseCacheManager;->handleDeleteFile(Ljava/io/File;)Z
    invoke-static {v0, p1}, Lcom/google/android/music/download/cache/BaseCacheManager;->access$000(Lcom/google/android/music/download/cache/BaseCacheManager;Ljava/io/File;)Z

    move-result v0

    return v0
.end method
