.class Lcom/google/android/music/leanback/LeanbackPlayActivity$9;
.super Ljava/lang/Object;
.source "LeanbackPlayActivity.java"

# interfaces
.implements Lcom/google/android/music/utils/async/AsyncRunner;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/leanback/LeanbackPlayActivity;->handleSongId(JLjava/lang/String;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mMixDescriptor:Lcom/google/android/music/mix/MixDescriptor;

.field final synthetic this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

.field final synthetic val$songList:Lcom/google/android/music/medialist/SongList;

.field final synthetic val$trackName:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/music/leanback/LeanbackPlayActivity;Lcom/google/android/music/medialist/SongList;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 902
    iput-object p1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$9;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

    iput-object p2, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$9;->val$songList:Lcom/google/android/music/medialist/SongList;

    iput-object p3, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$9;->val$trackName:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public backgroundTask()V
    .locals 2

    .prologue
    .line 907
    iget-object v0, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$9;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$9;->val$songList:Lcom/google/android/music/medialist/SongList;

    invoke-static {v0, v1}, Lcom/google/android/music/utils/MusicUtils;->getSongListRadioMixDescriptor(Landroid/content/Context;Lcom/google/android/music/medialist/SongList;)Lcom/google/android/music/mix/MixDescriptor;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$9;->mMixDescriptor:Lcom/google/android/music/mix/MixDescriptor;

    .line 910
    return-void
.end method

.method public taskCompleted()V
    .locals 10

    .prologue
    .line 914
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$9;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$9;->mMixDescriptor:Lcom/google/android/music/mix/MixDescriptor;

    invoke-static {v1, v2}, Lcom/google/android/music/leanback/LeanbackUtils;->newPlayIntent(Landroid/content/Context;Lcom/google/android/music/mix/MixDescriptor;)Landroid/content/Intent;

    move-result-object v7

    .line 916
    .local v7, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$9;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

    # getter for: Lcom/google/android/music/leanback/LeanbackPlayActivity;->mSecondaryActionsAdapter:Landroid/support/v17/leanback/widget/ArrayObjectAdapter;
    invoke-static {v1}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->access$2100(Lcom/google/android/music/leanback/LeanbackPlayActivity;)Landroid/support/v17/leanback/widget/ArrayObjectAdapter;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$9;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

    # getter for: Lcom/google/android/music/leanback/LeanbackPlayActivity;->mRadioAction:Lcom/google/android/music/leanback/IntentAction;
    invoke-static {v2}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->access$1600(Lcom/google/android/music/leanback/LeanbackPlayActivity;)Lcom/google/android/music/leanback/IntentAction;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;->remove(Ljava/lang/Object;)Z

    .line 917
    iget-object v8, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$9;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

    new-instance v1, Lcom/google/android/music/leanback/IntentAction;

    const-wide/16 v2, 0x0

    iget-object v4, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$9;->val$trackName:Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$9;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

    invoke-virtual {v6}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v9, 0x7f020131

    invoke-virtual {v6, v9}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-direct/range {v1 .. v7}, Lcom/google/android/music/leanback/IntentAction;-><init>(JLjava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/graphics/drawable/Drawable;Landroid/content/Intent;)V

    # setter for: Lcom/google/android/music/leanback/LeanbackPlayActivity;->mRadioAction:Lcom/google/android/music/leanback/IntentAction;
    invoke-static {v8, v1}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->access$1602(Lcom/google/android/music/leanback/LeanbackPlayActivity;Lcom/google/android/music/leanback/IntentAction;)Lcom/google/android/music/leanback/IntentAction;

    .line 919
    const/4 v0, 0x2

    .line 920
    .local v0, "radioIndex":I
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$9;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$9;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

    # getter for: Lcom/google/android/music/leanback/LeanbackPlayActivity;->mSecondaryActionsAdapter:Landroid/support/v17/leanback/widget/ArrayObjectAdapter;
    invoke-static {v2}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->access$2100(Lcom/google/android/music/leanback/LeanbackPlayActivity;)Landroid/support/v17/leanback/widget/ArrayObjectAdapter;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$9;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

    # getter for: Lcom/google/android/music/leanback/LeanbackPlayActivity;->mRepeatAction:Landroid/support/v17/leanback/widget/PlaybackControlsRow$RepeatAction;
    invoke-static {v3}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->access$1400(Lcom/google/android/music/leanback/LeanbackPlayActivity;)Landroid/support/v17/leanback/widget/PlaybackControlsRow$RepeatAction;

    move-result-object v3

    # invokes: Lcom/google/android/music/leanback/LeanbackPlayActivity;->findActionIndex(Landroid/support/v17/leanback/widget/ObjectAdapter;Landroid/support/v17/leanback/widget/Action;)I
    invoke-static {v1, v2, v3}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->access$2200(Lcom/google/android/music/leanback/LeanbackPlayActivity;Landroid/support/v17/leanback/widget/ObjectAdapter;Landroid/support/v17/leanback/widget/Action;)I

    move-result v1

    if-gez v1, :cond_0

    .line 921
    add-int/lit8 v0, v0, -0x1

    .line 923
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$9;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$9;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

    # getter for: Lcom/google/android/music/leanback/LeanbackPlayActivity;->mSecondaryActionsAdapter:Landroid/support/v17/leanback/widget/ArrayObjectAdapter;
    invoke-static {v2}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->access$2100(Lcom/google/android/music/leanback/LeanbackPlayActivity;)Landroid/support/v17/leanback/widget/ArrayObjectAdapter;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$9;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

    # getter for: Lcom/google/android/music/leanback/LeanbackPlayActivity;->mShuffleAction:Landroid/support/v17/leanback/widget/PlaybackControlsRow$ShuffleAction;
    invoke-static {v3}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->access$1200(Lcom/google/android/music/leanback/LeanbackPlayActivity;)Landroid/support/v17/leanback/widget/PlaybackControlsRow$ShuffleAction;

    move-result-object v3

    # invokes: Lcom/google/android/music/leanback/LeanbackPlayActivity;->findActionIndex(Landroid/support/v17/leanback/widget/ObjectAdapter;Landroid/support/v17/leanback/widget/Action;)I
    invoke-static {v1, v2, v3}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->access$2200(Lcom/google/android/music/leanback/LeanbackPlayActivity;Landroid/support/v17/leanback/widget/ObjectAdapter;Landroid/support/v17/leanback/widget/Action;)I

    move-result v1

    if-gez v1, :cond_1

    .line 924
    add-int/lit8 v0, v0, -0x1

    .line 926
    :cond_1
    iget-object v1, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$9;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

    # getter for: Lcom/google/android/music/leanback/LeanbackPlayActivity;->mSecondaryActionsAdapter:Landroid/support/v17/leanback/widget/ArrayObjectAdapter;
    invoke-static {v1}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->access$2100(Lcom/google/android/music/leanback/LeanbackPlayActivity;)Landroid/support/v17/leanback/widget/ArrayObjectAdapter;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/music/leanback/LeanbackPlayActivity$9;->this$0:Lcom/google/android/music/leanback/LeanbackPlayActivity;

    # getter for: Lcom/google/android/music/leanback/LeanbackPlayActivity;->mRadioAction:Lcom/google/android/music/leanback/IntentAction;
    invoke-static {v2}, Lcom/google/android/music/leanback/LeanbackPlayActivity;->access$1600(Lcom/google/android/music/leanback/LeanbackPlayActivity;)Lcom/google/android/music/leanback/IntentAction;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;->add(ILjava/lang/Object;)V

    .line 927
    return-void
.end method
