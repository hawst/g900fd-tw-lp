.class public Lcom/google/android/music/xdi/MusicProjections;
.super Ljava/lang/Object;
.source "MusicProjections.java"


# static fields
.field public static final PLAYLIST_COLUMNS:[Ljava/lang/String;

.field public static final PROJECTION_ENTITY_GROUPS:[Ljava/lang/String;

.field public static final PROJECTION_GENRES:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 14
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v2

    const-string v1, "genreServerId"

    aput-object v1, v0, v3

    const-string v1, "name"

    aput-object v1, v0, v4

    const-string v1, "parentGenreId"

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/music/xdi/MusicProjections;->PROJECTION_GENRES:[Ljava/lang/String;

    .line 26
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v2

    const-string v1, "title"

    aput-object v1, v0, v3

    const-string v1, "description"

    aput-object v1, v0, v4

    const-string v1, "size"

    aput-object v1, v0, v5

    const-string v1, "groupType"

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/android/music/xdi/MusicProjections;->PROJECTION_ENTITY_GROUPS:[Ljava/lang/String;

    .line 40
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v2

    const-string v1, "playlist_name"

    aput-object v1, v0, v3

    const-string v1, "playlist_type"

    aput-object v1, v0, v4

    const-string v1, "playlist_description"

    aput-object v1, v0, v5

    const-string v1, "playlist_owner_name"

    aput-object v1, v0, v6

    const/4 v1, 0x5

    const-string v2, "playlist_share_token"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "playlist_art_url"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "playlist_owner_profile_photo_url"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/music/xdi/MusicProjections;->PLAYLIST_COLUMNS:[Ljava/lang/String;

    return-void
.end method
