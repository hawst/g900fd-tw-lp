.class Lcom/google/android/music/eventlog/MusicEventLogger$4;
.super Ljava/lang/Object;
.source "MusicEventLogger.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/eventlog/MusicEventLogger;->logSkipForwardEventAsync(ZJLcom/google/android/music/store/ContainerDescriptor;JJ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/eventlog/MusicEventLogger;

.field final synthetic val$containerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

.field final synthetic val$curPosition:J

.field final synthetic val$isRemote:Z

.field final synthetic val$lastUserExplicitPlayTime:J

.field final synthetic val$musicId:J


# direct methods
.method constructor <init>(Lcom/google/android/music/eventlog/MusicEventLogger;ZJLcom/google/android/music/store/ContainerDescriptor;JJ)V
    .locals 0

    .prologue
    .line 500
    iput-object p1, p0, Lcom/google/android/music/eventlog/MusicEventLogger$4;->this$0:Lcom/google/android/music/eventlog/MusicEventLogger;

    iput-boolean p2, p0, Lcom/google/android/music/eventlog/MusicEventLogger$4;->val$isRemote:Z

    iput-wide p3, p0, Lcom/google/android/music/eventlog/MusicEventLogger$4;->val$musicId:J

    iput-object p5, p0, Lcom/google/android/music/eventlog/MusicEventLogger$4;->val$containerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    iput-wide p6, p0, Lcom/google/android/music/eventlog/MusicEventLogger$4;->val$curPosition:J

    iput-wide p8, p0, Lcom/google/android/music/eventlog/MusicEventLogger$4;->val$lastUserExplicitPlayTime:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    .line 503
    iget-object v1, p0, Lcom/google/android/music/eventlog/MusicEventLogger$4;->this$0:Lcom/google/android/music/eventlog/MusicEventLogger;

    iget-boolean v2, p0, Lcom/google/android/music/eventlog/MusicEventLogger$4;->val$isRemote:Z

    iget-wide v3, p0, Lcom/google/android/music/eventlog/MusicEventLogger$4;->val$musicId:J

    iget-object v5, p0, Lcom/google/android/music/eventlog/MusicEventLogger$4;->val$containerDescriptor:Lcom/google/android/music/store/ContainerDescriptor;

    iget-wide v6, p0, Lcom/google/android/music/eventlog/MusicEventLogger$4;->val$curPosition:J

    iget-wide v8, p0, Lcom/google/android/music/eventlog/MusicEventLogger$4;->val$lastUserExplicitPlayTime:J

    # invokes: Lcom/google/android/music/eventlog/MusicEventLogger;->logSkipForwardEvent(ZJLcom/google/android/music/store/ContainerDescriptor;JJ)V
    invoke-static/range {v1 .. v9}, Lcom/google/android/music/eventlog/MusicEventLogger;->access$200(Lcom/google/android/music/eventlog/MusicEventLogger;ZJLcom/google/android/music/store/ContainerDescriptor;JJ)V

    .line 505
    return-void
.end method
