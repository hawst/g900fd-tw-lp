.class public Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;
.super Lcom/google/android/music/ui/cardlib/layout/PlayCardView;
.source "PlayCardViewSmall.java"

# interfaces
.implements Lcom/google/android/music/ui/ShareableElement;


# instance fields
.field private final mExtraVSpace:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 25
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Lcom/google/android/music/ui/cardlib/layout/PlayCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 31
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f00ed

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mExtraVSpace:I

    .line 33
    return-void
.end method

.method private getBackgroundColor()I
    .locals 2

    .prologue
    .line 288
    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c00c9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    return v0
.end method

.method private getSharedElementPadding()Landroid/graphics/RectF;
    .locals 2

    .prologue
    .line 292
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    .line 293
    .local v0, "padding":Landroid/graphics/RectF;
    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->getPaddingLeft()I

    move-result v1

    int-to-float v1, v1

    iput v1, v0, Landroid/graphics/RectF;->left:F

    .line 294
    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->getPaddingTop()I

    move-result v1

    int-to-float v1, v1

    iput v1, v0, Landroid/graphics/RectF;->top:F

    .line 295
    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->getPaddingRight()I

    move-result v1

    int-to-float v1, v1

    iput v1, v0, Landroid/graphics/RectF;->right:F

    .line 296
    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->getPaddingBottom()I

    move-result v1

    int-to-float v1, v1

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    .line 297
    return-object v0
.end method


# virtual methods
.method public getSnapshotElement()Landroid/view/View;
    .locals 0

    .prologue
    .line 273
    return-object p0
.end method

.method public getTransitionInfo()Lcom/google/android/music/ui/TransitionInfo;
    .locals 2

    .prologue
    .line 280
    new-instance v0, Lcom/google/android/music/ui/AlbumTransitionInfo;

    invoke-direct {v0}, Lcom/google/android/music/ui/AlbumTransitionInfo;-><init>()V

    .line 281
    .local v0, "info":Lcom/google/android/music/ui/AlbumTransitionInfo;
    invoke-direct {p0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->getBackgroundColor()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/AlbumTransitionInfo;->setBackgroundColor(I)V

    .line 282
    invoke-direct {p0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->getSharedElementPadding()Landroid/graphics/RectF;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/AlbumTransitionInfo;->setSharedElementPadding(Landroid/graphics/RectF;)V

    .line 284
    return-object v0
.end method

.method protected onLayout(ZIIII)V
    .locals 44
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 144
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->getPaddingLeft()I

    move-result v18

    .line 145
    .local v18, "paddingLeft":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->getPaddingRight()I

    move-result v19

    .line 146
    .local v19, "paddingRight":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->getPaddingTop()I

    move-result v20

    .line 147
    .local v20, "paddingTop":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->getPaddingBottom()I

    move-result v17

    .line 149
    .local v17, "paddingBottom":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->getWidth()I

    move-result v37

    .line 150
    .local v37, "width":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->getHeight()I

    move-result v8

    .line 151
    .local v8, "height":I
    sub-int v7, p5, p3

    .line 155
    .local v7, "givenHeight":I
    sub-int v6, v7, v8

    .line 157
    .local v6, "gap":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mThumbnail:Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;

    move-object/from16 v38, v0

    invoke-virtual/range {v38 .. v38}, Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v29

    check-cast v29, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 158
    .local v29, "thumbnailLp":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mTitle:Landroid/widget/TextView;

    move-object/from16 v38, v0

    invoke-virtual/range {v38 .. v38}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v34

    check-cast v34, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 159
    .local v34, "titleLp":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mReason1:Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;

    move-object/from16 v38, v0

    invoke-virtual/range {v38 .. v38}, Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v23

    check-cast v23, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 160
    .local v23, "reasonLp":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mOverflow:Lcom/google/android/music/ui/cardlib/layout/PlayCardOverflowView;

    move-object/from16 v38, v0

    invoke-virtual/range {v38 .. v38}, Lcom/google/android/music/ui/cardlib/layout/PlayCardOverflowView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v14

    check-cast v14, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 161
    .local v14, "overflowLp":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mKeepOn:Lcom/google/android/music/KeepOnViewSmall;

    move-object/from16 v38, v0

    invoke-virtual/range {v38 .. v38}, Lcom/google/android/music/KeepOnViewSmall;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v10

    check-cast v10, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 163
    .local v10, "keeponLp":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mThumbnail:Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;

    move-object/from16 v38, v0

    invoke-virtual/range {v38 .. v38}, Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;->getMeasuredHeight()I

    move-result v27

    .line 164
    .local v27, "thumbnailHeight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mThumbnail:Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;

    move-object/from16 v38, v0

    invoke-virtual/range {v38 .. v38}, Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;->getMeasuredWidth()I

    move-result v31

    .line 165
    .local v31, "thumbnailWidth":I
    move-object/from16 v0, v29

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v38, v0

    add-int v28, v18, v38

    .line 166
    .local v28, "thumbnailLeft":I
    move-object/from16 v0, v29

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v38, v0

    add-int v30, v20, v38

    .line 167
    .local v30, "thumbnailTop":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mThumbnail:Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;

    move-object/from16 v38, v0

    add-int v39, v28, v31

    add-int v40, v30, v27

    move-object/from16 v0, v38

    move/from16 v1, v28

    move/from16 v2, v30

    move/from16 v3, v39

    move/from16 v4, v40

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;->layout(IIII)V

    .line 171
    move/from16 v13, v27

    .line 172
    .local v13, "offsetHeight":I
    add-int v38, v20, v13

    move-object/from16 v0, v34

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v39, v0

    add-int v35, v38, v39

    .line 173
    .local v35, "titleTop":I
    move-object/from16 v0, v34

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v38, v0

    add-int v33, v18, v38

    .line 174
    .local v33, "titleLeft":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mTitle:Landroid/widget/TextView;

    move-object/from16 v38, v0

    invoke-virtual/range {v38 .. v38}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v32

    .line 175
    .local v32, "titleHeight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mTitle:Landroid/widget/TextView;

    move-object/from16 v38, v0

    invoke-virtual/range {v38 .. v38}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v36

    .line 176
    .local v36, "titleWidth":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mTitle:Landroid/widget/TextView;

    move-object/from16 v38, v0

    add-int v39, v33, v36

    add-int v40, v35, v32

    move-object/from16 v0, v38

    move/from16 v1, v33

    move/from16 v2, v35

    move/from16 v3, v39

    move/from16 v4, v40

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->layout(IIII)V

    .line 178
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mOverflow:Lcom/google/android/music/ui/cardlib/layout/PlayCardOverflowView;

    move-object/from16 v38, v0

    invoke-virtual/range {v38 .. v38}, Lcom/google/android/music/ui/cardlib/layout/PlayCardOverflowView;->getVisibility()I

    move-result v38

    const/16 v39, 0x8

    move/from16 v0, v38

    move/from16 v1, v39

    if-eq v0, v1, :cond_0

    .line 179
    add-int v38, v20, v13

    iget v0, v14, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v39, v0

    add-int v16, v38, v39

    .line 180
    .local v16, "overflowTop":I
    sub-int v38, v37, v19

    iget v0, v14, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    move/from16 v39, v0

    sub-int v15, v38, v39

    .line 181
    .local v15, "overflowRight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mOverflow:Lcom/google/android/music/ui/cardlib/layout/PlayCardOverflowView;

    move-object/from16 v38, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mOverflow:Lcom/google/android/music/ui/cardlib/layout/PlayCardOverflowView;

    move-object/from16 v39, v0

    invoke-virtual/range {v39 .. v39}, Lcom/google/android/music/ui/cardlib/layout/PlayCardOverflowView;->getMeasuredWidth()I

    move-result v39

    sub-int v39, v15, v39

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mOverflow:Lcom/google/android/music/ui/cardlib/layout/PlayCardOverflowView;

    move-object/from16 v40, v0

    invoke-virtual/range {v40 .. v40}, Lcom/google/android/music/ui/cardlib/layout/PlayCardOverflowView;->getMeasuredHeight()I

    move-result v40

    add-int v40, v40, v16

    move-object/from16 v0, v38

    move/from16 v1, v39

    move/from16 v2, v16

    move/from16 v3, v40

    invoke-virtual {v0, v1, v2, v15, v3}, Lcom/google/android/music/ui/cardlib/layout/PlayCardOverflowView;->layout(IIII)V

    .line 187
    .end local v15    # "overflowRight":I
    .end local v16    # "overflowTop":I
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mSubtitle:Landroid/widget/TextView;

    move-object/from16 v38, v0

    if-nez v38, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mReason1:Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;

    move-object/from16 v38, v0

    invoke-virtual/range {v38 .. v38}, Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;->getVisibility()I

    move-result v38

    const/16 v39, 0x8

    move/from16 v0, v38

    move/from16 v1, v39

    if-ne v0, v1, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mKeepOn:Lcom/google/android/music/KeepOnViewSmall;

    move-object/from16 v38, v0

    invoke-virtual/range {v38 .. v38}, Lcom/google/android/music/KeepOnViewSmall;->getVisibility()I

    move-result v38

    const/16 v39, 0x8

    move/from16 v0, v38

    move/from16 v1, v39

    if-eq v0, v1, :cond_1

    .line 189
    add-int v9, v35, v32

    .line 190
    .local v9, "keeponBottom":I
    sub-int v38, v37, v19

    iget v0, v10, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    move/from16 v39, v0

    sub-int v11, v38, v39

    .line 191
    .local v11, "keeponRight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mKeepOn:Lcom/google/android/music/KeepOnViewSmall;

    move-object/from16 v38, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mKeepOn:Lcom/google/android/music/KeepOnViewSmall;

    move-object/from16 v39, v0

    invoke-virtual/range {v39 .. v39}, Lcom/google/android/music/KeepOnViewSmall;->getMeasuredWidth()I

    move-result v39

    sub-int v39, v11, v39

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mKeepOn:Lcom/google/android/music/KeepOnViewSmall;

    move-object/from16 v40, v0

    invoke-virtual/range {v40 .. v40}, Lcom/google/android/music/KeepOnViewSmall;->getMeasuredHeight()I

    move-result v40

    sub-int v40, v9, v40

    move-object/from16 v0, v38

    move/from16 v1, v39

    move/from16 v2, v40

    invoke-virtual {v0, v1, v2, v11, v9}, Lcom/google/android/music/KeepOnViewSmall;->layout(IIII)V

    .line 196
    .end local v9    # "keeponBottom":I
    .end local v11    # "keeponRight":I
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mSubtitle:Landroid/widget/TextView;

    move-object/from16 v38, v0

    if-eqz v38, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mSubtitle:Landroid/widget/TextView;

    move-object/from16 v38, v0

    invoke-virtual/range {v38 .. v38}, Landroid/widget/TextView;->getVisibility()I

    move-result v38

    const/16 v39, 0x8

    move/from16 v0, v38

    move/from16 v1, v39

    if-eq v0, v1, :cond_2

    .line 197
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mSubtitle:Landroid/widget/TextView;

    move-object/from16 v38, v0

    invoke-virtual/range {v38 .. v38}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v25

    check-cast v25, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 198
    .local v25, "subtitleLp":Landroid/view/ViewGroup$MarginLayoutParams;
    add-int v38, v35, v32

    move-object/from16 v0, v34

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    move/from16 v39, v0

    add-int v13, v38, v39

    .line 199
    move-object/from16 v0, v25

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v38, v0

    add-int v26, v13, v38

    .line 200
    .local v26, "subtitleTop":I
    move-object/from16 v0, v25

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v38, v0

    add-int v24, v18, v38

    .line 201
    .local v24, "subtitleLeft":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mSubtitle:Landroid/widget/TextView;

    move-object/from16 v38, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mSubtitle:Landroid/widget/TextView;

    move-object/from16 v39, v0

    invoke-virtual/range {v39 .. v39}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v39

    add-int v39, v39, v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mSubtitle:Landroid/widget/TextView;

    move-object/from16 v40, v0

    invoke-virtual/range {v40 .. v40}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v40

    add-int v40, v40, v26

    move-object/from16 v0, v38

    move/from16 v1, v24

    move/from16 v2, v26

    move/from16 v3, v39

    move/from16 v4, v40

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->layout(IIII)V

    .line 204
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mReason1:Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;

    move-object/from16 v38, v0

    invoke-virtual/range {v38 .. v38}, Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;->getVisibility()I

    move-result v38

    const/16 v39, 0x8

    move/from16 v0, v38

    move/from16 v1, v39

    if-ne v0, v1, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mKeepOn:Lcom/google/android/music/KeepOnViewSmall;

    move-object/from16 v38, v0

    invoke-virtual/range {v38 .. v38}, Lcom/google/android/music/KeepOnViewSmall;->getVisibility()I

    move-result v38

    const/16 v39, 0x8

    move/from16 v0, v38

    move/from16 v1, v39

    if-eq v0, v1, :cond_2

    .line 205
    iget v0, v10, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v38, v0

    add-int v12, v26, v38

    .line 206
    .local v12, "keeponTop":I
    sub-int v38, v37, v19

    iget v0, v10, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    move/from16 v39, v0

    sub-int v11, v38, v39

    .line 207
    .restart local v11    # "keeponRight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mKeepOn:Lcom/google/android/music/KeepOnViewSmall;

    move-object/from16 v38, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mKeepOn:Lcom/google/android/music/KeepOnViewSmall;

    move-object/from16 v39, v0

    invoke-virtual/range {v39 .. v39}, Lcom/google/android/music/KeepOnViewSmall;->getMeasuredWidth()I

    move-result v39

    sub-int v39, v11, v39

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mKeepOn:Lcom/google/android/music/KeepOnViewSmall;

    move-object/from16 v40, v0

    invoke-virtual/range {v40 .. v40}, Lcom/google/android/music/KeepOnViewSmall;->getMeasuredHeight()I

    move-result v40

    add-int v40, v40, v12

    move-object/from16 v0, v38

    move/from16 v1, v39

    move/from16 v2, v40

    invoke-virtual {v0, v1, v12, v11, v2}, Lcom/google/android/music/KeepOnViewSmall;->layout(IIII)V

    .line 213
    .end local v11    # "keeponRight":I
    .end local v12    # "keeponTop":I
    .end local v24    # "subtitleLeft":I
    .end local v25    # "subtitleLp":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v26    # "subtitleTop":I
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mReason1:Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;

    move-object/from16 v38, v0

    invoke-virtual/range {v38 .. v38}, Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;->getVisibility()I

    move-result v38

    const/16 v39, 0x8

    move/from16 v0, v38

    move/from16 v1, v39

    if-eq v0, v1, :cond_3

    .line 214
    sub-int v38, v8, v17

    move-object/from16 v0, v23

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    move/from16 v39, v0

    sub-int v38, v38, v39

    div-int/lit8 v39, v6, 0x2

    sub-int v21, v38, v39

    .line 215
    .local v21, "reasonBottom":I
    move-object/from16 v0, v23

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v38, v0

    add-int v22, v18, v38

    .line 216
    .local v22, "reasonLeft":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mReason1:Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;

    move-object/from16 v38, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mReason1:Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;

    move-object/from16 v39, v0

    invoke-virtual/range {v39 .. v39}, Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;->getMeasuredHeight()I

    move-result v39

    sub-int v39, v21, v39

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mReason1:Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;

    move-object/from16 v40, v0

    invoke-virtual/range {v40 .. v40}, Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;->getMeasuredWidth()I

    move-result v40

    add-int v40, v40, v22

    move-object/from16 v0, v38

    move/from16 v1, v22

    move/from16 v2, v39

    move/from16 v3, v40

    move/from16 v4, v21

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;->layout(IIII)V

    .line 219
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mKeepOn:Lcom/google/android/music/KeepOnViewSmall;

    move-object/from16 v38, v0

    invoke-virtual/range {v38 .. v38}, Lcom/google/android/music/KeepOnViewSmall;->getVisibility()I

    move-result v38

    const/16 v39, 0x8

    move/from16 v0, v38

    move/from16 v1, v39

    if-eq v0, v1, :cond_3

    .line 220
    sub-int v38, v8, v17

    iget v0, v10, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    move/from16 v39, v0

    sub-int v38, v38, v39

    div-int/lit8 v39, v6, 0x2

    sub-int v9, v38, v39

    .line 221
    .restart local v9    # "keeponBottom":I
    sub-int v38, v37, v19

    iget v0, v10, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    move/from16 v39, v0

    sub-int v11, v38, v39

    .line 222
    .restart local v11    # "keeponRight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mKeepOn:Lcom/google/android/music/KeepOnViewSmall;

    move-object/from16 v38, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mKeepOn:Lcom/google/android/music/KeepOnViewSmall;

    move-object/from16 v39, v0

    invoke-virtual/range {v39 .. v39}, Lcom/google/android/music/KeepOnViewSmall;->getMeasuredWidth()I

    move-result v39

    sub-int v39, v11, v39

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mKeepOn:Lcom/google/android/music/KeepOnViewSmall;

    move-object/from16 v40, v0

    invoke-virtual/range {v40 .. v40}, Lcom/google/android/music/KeepOnViewSmall;->getMeasuredHeight()I

    move-result v40

    sub-int v40, v9, v40

    move-object/from16 v0, v38

    move/from16 v1, v39

    move/from16 v2, v40

    invoke-virtual {v0, v1, v2, v11, v9}, Lcom/google/android/music/KeepOnViewSmall;->layout(IIII)V

    .line 227
    .end local v9    # "keeponBottom":I
    .end local v11    # "keeponRight":I
    .end local v21    # "reasonBottom":I
    .end local v22    # "reasonLeft":I
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mAccessibilityOverlay:Landroid/view/View;

    move-object/from16 v38, v0

    invoke-virtual/range {v38 .. v38}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    check-cast v5, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 229
    .local v5, "accessibilityLp":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mAccessibilityOverlay:Landroid/view/View;

    move-object/from16 v38, v0

    iget v0, v5, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v39, v0

    add-int v39, v39, v18

    iget v0, v5, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v40, v0

    add-int v40, v40, v20

    iget v0, v5, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v41, v0

    add-int v41, v41, v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mAccessibilityOverlay:Landroid/view/View;

    move-object/from16 v42, v0

    invoke-virtual/range {v42 .. v42}, Landroid/view/View;->getMeasuredWidth()I

    move-result v42

    add-int v41, v41, v42

    iget v0, v5, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v42, v0

    add-int v42, v42, v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mAccessibilityOverlay:Landroid/view/View;

    move-object/from16 v43, v0

    invoke-virtual/range {v43 .. v43}, Landroid/view/View;->getMeasuredHeight()I

    move-result v43

    add-int v42, v42, v43

    invoke-virtual/range {v38 .. v42}, Landroid/view/View;->layout(IIII)V

    .line 239
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mOverflow:Lcom/google/android/music/ui/cardlib/layout/PlayCardOverflowView;

    move-object/from16 v38, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mOverflowArea:Landroid/graphics/Rect;

    move-object/from16 v39, v0

    invoke-virtual/range {v38 .. v39}, Lcom/google/android/music/ui/cardlib/layout/PlayCardOverflowView;->getHitRect(Landroid/graphics/Rect;)V

    .line 240
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mOverflowArea:Landroid/graphics/Rect;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v39, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mOverflowTouchExtend:I

    move/from16 v40, v0

    sub-int v39, v39, v40

    move/from16 v0, v39

    move-object/from16 v1, v38

    iput v0, v1, Landroid/graphics/Rect;->top:I

    .line 241
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mOverflowArea:Landroid/graphics/Rect;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v39, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mOverflowTouchExtend:I

    move/from16 v40, v0

    add-int v39, v39, v40

    move/from16 v0, v39

    move-object/from16 v1, v38

    iput v0, v1, Landroid/graphics/Rect;->bottom:I

    .line 242
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mOverflowArea:Landroid/graphics/Rect;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v39, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mOverflowTouchExtend:I

    move/from16 v40, v0

    sub-int v39, v39, v40

    move/from16 v0, v39

    move-object/from16 v1, v38

    iput v0, v1, Landroid/graphics/Rect;->left:I

    .line 243
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mOverflowArea:Landroid/graphics/Rect;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v39, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mOverflowTouchExtend:I

    move/from16 v40, v0

    add-int v39, v39, v40

    move/from16 v0, v39

    move-object/from16 v1, v38

    iput v0, v1, Landroid/graphics/Rect;->right:I

    .line 244
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mOverflowArea:Landroid/graphics/Rect;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v38, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mOldOverflowArea:Landroid/graphics/Rect;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v39, v0

    move/from16 v0, v38

    move/from16 v1, v39

    if-ne v0, v1, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mOverflowArea:Landroid/graphics/Rect;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v38, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mOldOverflowArea:Landroid/graphics/Rect;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v39, v0

    move/from16 v0, v38

    move/from16 v1, v39

    if-ne v0, v1, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mOverflowArea:Landroid/graphics/Rect;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v38, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mOldOverflowArea:Landroid/graphics/Rect;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v39, v0

    move/from16 v0, v38

    move/from16 v1, v39

    if-ne v0, v1, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mOverflowArea:Landroid/graphics/Rect;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v38, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mOldOverflowArea:Landroid/graphics/Rect;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v39, v0

    move/from16 v0, v38

    move/from16 v1, v39

    if-ne v0, v1, :cond_4

    .line 253
    :goto_0
    return-void

    .line 251
    :cond_4
    new-instance v38, Landroid/view/TouchDelegate;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mOverflowArea:Landroid/graphics/Rect;

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mOverflow:Lcom/google/android/music/ui/cardlib/layout/PlayCardOverflowView;

    move-object/from16 v40, v0

    invoke-direct/range {v38 .. v40}, Landroid/view/TouchDelegate;-><init>(Landroid/graphics/Rect;Landroid/view/View;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->setTouchDelegate(Landroid/view/TouchDelegate;)V

    .line 252
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mOldOverflowArea:Landroid/graphics/Rect;

    move-object/from16 v38, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mOverflowArea:Landroid/graphics/Rect;

    move-object/from16 v39, v0

    invoke-virtual/range {v38 .. v39}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 34
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 37
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->getPaddingLeft()I

    move-result v14

    .line 38
    .local v14, "paddingLeft":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->getPaddingRight()I

    move-result v15

    .line 39
    .local v15, "paddingRight":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->getPaddingTop()I

    move-result v16

    .line 40
    .local v16, "paddingTop":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->getPaddingBottom()I

    move-result v13

    .line 43
    .local v13, "paddingBottom":I
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v21

    .line 46
    .local v21, "resultWidth":I
    add-int v20, v13, v16

    .line 48
    .local v20, "resultHeight":I
    sub-int v30, v21, v14

    sub-int v7, v30, v15

    .line 50
    .local v7, "innerWidth":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mThumbnail:Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v25

    check-cast v25, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 51
    .local v25, "thumbnailLp":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mTitle:Landroid/widget/TextView;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v28

    check-cast v28, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 52
    .local v28, "titleLp":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mReason1:Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v19

    check-cast v19, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 53
    .local v19, "reasonLp":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mOverflow:Lcom/google/android/music/ui/cardlib/layout/PlayCardOverflowView;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/google/android/music/ui/cardlib/layout/PlayCardOverflowView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v12

    check-cast v12, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 55
    .local v12, "overflowLp":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mKeepOn:Lcom/google/android/music/KeepOnViewSmall;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/google/android/music/KeepOnViewSmall;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v10

    check-cast v10, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 57
    .local v10, "keeponLp":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, v25

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v30, v0

    sub-int v30, v7, v30

    move-object/from16 v0, v25

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    move/from16 v31, v0

    sub-int v26, v30, v31

    .line 58
    .local v26, "thumbnailWidth":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mThumbnailAspectRatio:F

    move/from16 v30, v0

    move/from16 v0, v26

    int-to-float v0, v0

    move/from16 v31, v0

    mul-float v30, v30, v31

    move/from16 v0, v30

    float-to-int v0, v0

    move/from16 v24, v0

    .line 59
    .local v24, "thumbnailHeight":I
    move/from16 v0, v24

    move-object/from16 v1, v25

    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 60
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mThumbnail:Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    move/from16 v1, v26

    move/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;->setThumbnailMetrics(II)V

    .line 62
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mThumbnail:Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;

    move-object/from16 v30, v0

    const/high16 v31, 0x40000000    # 2.0f

    move/from16 v0, v26

    move/from16 v1, v31

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v31

    const/high16 v32, 0x40000000    # 2.0f

    move/from16 v0, v24

    move/from16 v1, v32

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v32

    invoke-virtual/range {v30 .. v32}, Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;->measure(II)V

    .line 65
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mThumbnail:Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/google/android/music/ui/cardlib/layout/PlayCardThumbnail;->getMeasuredHeight()I

    move-result v30

    move-object/from16 v0, v25

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    move/from16 v31, v0

    add-int v30, v30, v31

    move-object/from16 v0, v25

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v31, v0

    add-int v30, v30, v31

    add-int v20, v20, v30

    .line 73
    move-object/from16 v0, v28

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v30, v0

    sub-int v30, v7, v30

    move-object/from16 v0, v28

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    move/from16 v31, v0

    sub-int v29, v30, v31

    .line 74
    .local v29, "titleWidth":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mTitle:Landroid/widget/TextView;

    move-object/from16 v30, v0

    const/high16 v31, 0x40000000    # 2.0f

    move/from16 v0, v29

    move/from16 v1, v31

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v31

    const/16 v32, 0x0

    invoke-virtual/range {v30 .. v32}, Landroid/widget/TextView;->measure(II)V

    .line 76
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mTitle:Landroid/widget/TextView;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v30

    move-object/from16 v0, v28

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v31, v0

    add-int v30, v30, v31

    move-object/from16 v0, v28

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    move/from16 v31, v0

    add-int v27, v30, v31

    .line 78
    .local v27, "titleHeight":I
    const/4 v11, 0x0

    .line 83
    .local v11, "overflowHeight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mOverflow:Lcom/google/android/music/ui/cardlib/layout/PlayCardOverflowView;

    move-object/from16 v30, v0

    const/16 v31, 0x0

    const/16 v32, 0x0

    invoke-virtual/range {v30 .. v32}, Lcom/google/android/music/ui/cardlib/layout/PlayCardOverflowView;->measure(II)V

    .line 84
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mOverflow:Lcom/google/android/music/ui/cardlib/layout/PlayCardOverflowView;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/google/android/music/ui/cardlib/layout/PlayCardOverflowView;->getMeasuredHeight()I

    move-result v30

    iget v0, v12, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v31, v0

    add-int v30, v30, v31

    iget v0, v12, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    move/from16 v31, v0

    add-int v11, v30, v31

    .line 87
    move/from16 v0, v27

    invoke-static {v0, v11}, Ljava/lang/Math;->max(II)I

    move-result v30

    add-int v20, v20, v30

    .line 89
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mKeepOn:Lcom/google/android/music/KeepOnViewSmall;

    move-object/from16 v30, v0

    const/16 v31, 0x0

    const/16 v32, 0x0

    invoke-virtual/range {v30 .. v32}, Lcom/google/android/music/KeepOnViewSmall;->measure(II)V

    .line 92
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mSubtitle:Landroid/widget/TextView;

    move-object/from16 v30, v0

    if-eqz v30, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mSubtitle:Landroid/widget/TextView;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Landroid/widget/TextView;->getVisibility()I

    move-result v30

    const/16 v31, 0x8

    move/from16 v0, v30

    move/from16 v1, v31

    if-eq v0, v1, :cond_0

    .line 93
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mSubtitle:Landroid/widget/TextView;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v23

    check-cast v23, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 94
    .local v23, "subtitleLp":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mSubtitle:Landroid/widget/TextView;

    move-object/from16 v30, v0

    const/high16 v31, 0x40000000    # 2.0f

    move/from16 v0, v29

    move/from16 v1, v31

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v31

    const/16 v32, 0x0

    invoke-virtual/range {v30 .. v32}, Landroid/widget/TextView;->measure(II)V

    .line 96
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mSubtitle:Landroid/widget/TextView;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v30

    move-object/from16 v0, v23

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    move/from16 v31, v0

    add-int v30, v30, v31

    move-object/from16 v0, v23

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v31, v0

    add-int v22, v30, v31

    .line 99
    .local v22, "subtitleHeight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mKeepOn:Lcom/google/android/music/KeepOnViewSmall;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/google/android/music/KeepOnViewSmall;->getMeasuredHeight()I

    move-result v30

    iget v0, v10, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v31, v0

    add-int v8, v30, v31

    .line 100
    .local v8, "keeponHeight":I
    move/from16 v0, v22

    invoke-static {v0, v8}, Ljava/lang/Math;->max(II)I

    move-result v30

    add-int v20, v20, v30

    .line 104
    .end local v8    # "keeponHeight":I
    .end local v22    # "subtitleHeight":I
    .end local v23    # "subtitleLp":Landroid/view/ViewGroup$MarginLayoutParams;
    :cond_0
    const/16 v17, 0x0

    .line 105
    .local v17, "reason1Height":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mReason1:Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;->getVisibility()I

    move-result v30

    const/16 v31, 0x8

    move/from16 v0, v30

    move/from16 v1, v31

    if-eq v0, v1, :cond_1

    .line 106
    move-object/from16 v0, v19

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v30, v0

    sub-int v30, v7, v30

    move-object/from16 v0, v19

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    move/from16 v31, v0

    sub-int v18, v30, v31

    .line 109
    .local v18, "reason1Width":I
    move-object/from16 v0, v19

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    move/from16 v30, v0

    if-lez v30, :cond_2

    .line 110
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mReason1:Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;

    move-object/from16 v30, v0

    const/high16 v31, 0x40000000    # 2.0f

    move/from16 v0, v18

    move/from16 v1, v31

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v31

    move-object/from16 v0, v19

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    move/from16 v32, v0

    const/high16 v33, 0x40000000    # 2.0f

    invoke-static/range {v32 .. v33}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v32

    invoke-virtual/range {v30 .. v32}, Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;->measure(II)V

    .line 118
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mReason1:Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;->getMeasuredHeight()I

    move-result v30

    move-object/from16 v0, v19

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v31, v0

    add-int v30, v30, v31

    move-object/from16 v0, v19

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    move/from16 v31, v0

    add-int v17, v30, v31

    .line 121
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mKeepOn:Lcom/google/android/music/KeepOnViewSmall;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/google/android/music/KeepOnViewSmall;->getMeasuredHeight()I

    move-result v30

    iget v0, v10, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    move/from16 v31, v0

    add-int v9, v30, v31

    .line 122
    .local v9, "keeponHeightRow3":I
    move/from16 v0, v17

    invoke-static {v9, v0}, Ljava/lang/Math;->max(II)I

    move-result v30

    add-int v20, v20, v30

    .line 125
    .end local v9    # "keeponHeightRow3":I
    .end local v18    # "reason1Width":I
    :cond_1
    move-object/from16 v0, p0

    move/from16 v1, v21

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->setMeasuredDimension(II)V

    .line 128
    sub-int v30, v20, v16

    sub-int v6, v30, v13

    .line 131
    .local v6, "innerHeight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mAccessibilityOverlay:Landroid/view/View;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 133
    .local v4, "accessibilityLp":Landroid/view/ViewGroup$MarginLayoutParams;
    iget v0, v4, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v30, v0

    sub-int v30, v7, v30

    iget v0, v4, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    move/from16 v31, v0

    sub-int v5, v30, v31

    .line 135
    .local v5, "accessibilityWidth":I
    iget v0, v4, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v30, v0

    sub-int v30, v6, v30

    iget v0, v4, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    move/from16 v31, v0

    sub-int v3, v30, v31

    .line 137
    .local v3, "accessibilityHeight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mAccessibilityOverlay:Landroid/view/View;

    move-object/from16 v30, v0

    const/high16 v31, 0x40000000    # 2.0f

    move/from16 v0, v31

    invoke-static {v5, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v31

    const/high16 v32, 0x40000000    # 2.0f

    move/from16 v0, v32

    invoke-static {v3, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v32

    invoke-virtual/range {v30 .. v32}, Landroid/view/View;->measure(II)V

    .line 140
    return-void

    .line 114
    .end local v3    # "accessibilityHeight":I
    .end local v4    # "accessibilityLp":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v5    # "accessibilityWidth":I
    .end local v6    # "innerHeight":I
    .restart local v18    # "reason1Width":I
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->mReason1:Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;

    move-object/from16 v30, v0

    const/high16 v31, 0x40000000    # 2.0f

    move/from16 v0, v18

    move/from16 v1, v31

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v31

    const/16 v32, 0x0

    invoke-virtual/range {v30 .. v32}, Lcom/google/android/music/ui/cardlib/layout/PlayCardReason;->measure(II)V

    goto/16 :goto_0
.end method

.method public setSharedElementSnapshotInfo(Landroid/graphics/Matrix;Landroid/graphics/RectF;)V
    .locals 2
    .param p1, "viewToGlobalMatrix"    # Landroid/graphics/Matrix;
    .param p2, "screenBounds"    # Landroid/graphics/RectF;

    .prologue
    .line 257
    if-eqz p2, :cond_0

    .line 263
    iget v0, p2, Landroid/graphics/RectF;->top:F

    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->getPaddingTop()I

    move-result v1

    int-to-float v1, v1

    add-float/2addr v0, v1

    iput v0, p2, Landroid/graphics/RectF;->top:F

    .line 264
    iget v0, p2, Landroid/graphics/RectF;->right:F

    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->getPaddingRight()I

    move-result v1

    int-to-float v1, v1

    sub-float/2addr v0, v1

    iput v0, p2, Landroid/graphics/RectF;->right:F

    .line 265
    iget v0, p2, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->getPaddingBottom()I

    move-result v1

    int-to-float v1, v1

    sub-float/2addr v0, v1

    iput v0, p2, Landroid/graphics/RectF;->bottom:F

    .line 266
    iget v0, p2, Landroid/graphics/RectF;->left:F

    invoke-virtual {p0}, Lcom/google/android/music/ui/cardlib/layout/PlayCardViewSmall;->getPaddingLeft()I

    move-result v1

    int-to-float v1, v1

    add-float/2addr v0, v1

    iput v0, p2, Landroid/graphics/RectF;->left:F

    .line 268
    :cond_0
    return-void
.end method
