.class Lcom/google/android/music/ui/NowPlayingScreenFragment$6;
.super Ljava/lang/Object;
.source "NowPlayingScreenFragment.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/ui/NowPlayingScreenFragment;->initializeView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;


# direct methods
.method constructor <init>(Lcom/google/android/music/ui/NowPlayingScreenFragment;)V
    .locals 0

    .prologue
    .line 655
    iput-object p1, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$6;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "v"    # Landroid/view/View;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 658
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$6;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # getter for: Lcom/google/android/music/ui/NowPlayingScreenFragment;->mMediaRouteManager:Lcom/google/android/music/ui/mrp/MediaRouteManager;
    invoke-static {v0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$1100(Lcom/google/android/music/ui/NowPlayingScreenFragment;)Lcom/google/android/music/ui/mrp/MediaRouteManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    .line 659
    iget-object v0, p0, Lcom/google/android/music/ui/NowPlayingScreenFragment$6;->this$0:Lcom/google/android/music/ui/NowPlayingScreenFragment;

    # getter for: Lcom/google/android/music/ui/NowPlayingScreenFragment;->mMediaRouteManager:Lcom/google/android/music/ui/mrp/MediaRouteManager;
    invoke-static {v0}, Lcom/google/android/music/ui/NowPlayingScreenFragment;->access$1100(Lcom/google/android/music/ui/NowPlayingScreenFragment;)Lcom/google/android/music/ui/mrp/MediaRouteManager;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Lcom/google/android/music/ui/mrp/MediaRouteManager;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    .line 661
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
