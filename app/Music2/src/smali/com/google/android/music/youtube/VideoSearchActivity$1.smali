.class Lcom/google/android/music/youtube/VideoSearchActivity$1;
.super Ljava/lang/Object;
.source "VideoSearchActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/music/youtube/VideoSearchActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/youtube/VideoSearchActivity;


# direct methods
.method constructor <init>(Lcom/google/android/music/youtube/VideoSearchActivity;)V
    .locals 0

    .prologue
    .line 173
    iput-object p1, p0, Lcom/google/android/music/youtube/VideoSearchActivity$1;->this$0:Lcom/google/android/music/youtube/VideoSearchActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 176
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/music/youtube/YouTubeVideo;

    .line 177
    .local v0, "video":Lcom/google/android/music/youtube/YouTubeVideo;
    iget-object v1, p0, Lcom/google/android/music/youtube/VideoSearchActivity$1;->this$0:Lcom/google/android/music/youtube/VideoSearchActivity;

    # getter for: Lcom/google/android/music/youtube/VideoSearchActivity;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/google/android/music/youtube/VideoSearchActivity;->access$000(Lcom/google/android/music/youtube/VideoSearchActivity;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/music/youtube/YouTubeVideo;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/music/utils/MusicUtils;->startVideoPlayerActivity(Landroid/content/Context;Ljava/lang/String;)V

    .line 178
    iget-object v1, p0, Lcom/google/android/music/youtube/VideoSearchActivity$1;->this$0:Lcom/google/android/music/youtube/VideoSearchActivity;

    invoke-virtual {v1}, Lcom/google/android/music/youtube/VideoSearchActivity;->finish()V

    .line 179
    return-void
.end method
