.class Lcom/google/android/music/download/cache/BaseCacheService$1;
.super Ljava/lang/Object;
.source "BaseCacheService.java"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/music/download/cache/BaseCacheService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/music/download/cache/BaseCacheService;


# direct methods
.method constructor <init>(Lcom/google/android/music/download/cache/BaseCacheService;)V
    .locals 0

    .prologue
    .line 88
    iput-object p1, p0, Lcom/google/android/music/download/cache/BaseCacheService$1;->this$0:Lcom/google/android/music/download/cache/BaseCacheService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 6
    .param p1, "sharedPreferences"    # Landroid/content/SharedPreferences;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 93
    iget-object v1, p0, Lcom/google/android/music/download/cache/BaseCacheService$1;->this$0:Lcom/google/android/music/download/cache/BaseCacheService;

    # getter for: Lcom/google/android/music/download/cache/BaseCacheService;->mMusicPreferences:Lcom/google/android/music/preferences/MusicPreferences;
    invoke-static {v1}, Lcom/google/android/music/download/cache/BaseCacheService;->access$300(Lcom/google/android/music/download/cache/BaseCacheService;)Lcom/google/android/music/preferences/MusicPreferences;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/music/preferences/MusicPreferences;->isCachedStreamingMusicEnabled()Z

    move-result v0

    .line 94
    .local v0, "allowCaching":Z
    iget-object v1, p0, Lcom/google/android/music/download/cache/BaseCacheService$1;->this$0:Lcom/google/android/music/download/cache/BaseCacheService;

    # getter for: Lcom/google/android/music/download/cache/BaseCacheService;->mCacheManager:Lcom/google/android/music/download/cache/BaseCacheManager;
    invoke-static {v1}, Lcom/google/android/music/download/cache/BaseCacheService;->access$400(Lcom/google/android/music/download/cache/BaseCacheService;)Lcom/google/android/music/download/cache/BaseCacheManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/music/download/cache/BaseCacheManager;->setAllowCaching(Z)V

    .line 95
    if-eqz v0, :cond_0

    .line 96
    iget-object v1, p0, Lcom/google/android/music/download/cache/BaseCacheService$1;->this$0:Lcom/google/android/music/download/cache/BaseCacheService;

    # invokes: Lcom/google/android/music/download/cache/BaseCacheService;->cancelClearCacheMessage()V
    invoke-static {v1}, Lcom/google/android/music/download/cache/BaseCacheService;->access$500(Lcom/google/android/music/download/cache/BaseCacheService;)V

    .line 101
    :goto_0
    return-void

    .line 99
    :cond_0
    iget-object v1, p0, Lcom/google/android/music/download/cache/BaseCacheService$1;->this$0:Lcom/google/android/music/download/cache/BaseCacheService;

    const/4 v2, -0x1

    const-wide/16 v4, 0x2710

    # invokes: Lcom/google/android/music/download/cache/BaseCacheService;->sendClearCachedMessage(IJ)V
    invoke-static {v1, v2, v4, v5}, Lcom/google/android/music/download/cache/BaseCacheService;->access$600(Lcom/google/android/music/download/cache/BaseCacheService;IJ)V

    goto :goto_0
.end method
